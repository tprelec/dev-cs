({
	generateVodafone: function (component, event) {
        // Get the list of uploaded files
        console.log(component.get("v.recordId"));
        var action = component.get("c.generateFrameworkForVodafoneAccount");
        action.setParams({
            accountId : component.get("v.recordId")
        });
        action.setCallback(this, function(a){
        	if(a.getState() === "SUCCESS"){
                console.log('succes');
            	var resultToast = $A.get("e.force:showToast");
        		resultToast.setParams({
                            "title": "Success!",
                            "message": "Framework generated and updated on Account"
                        });
        		resultToast.fire();;
            } else {
                console.log('failure');
            	var resultToast = $A.get("e.force:showToast");
        		resultToast.setParams({
                            "title": "Failed!",
                            "message": "Framework not generated"
                        });
        		resultToast.fire();;
            }
        });
    	$A.enqueueAction(action);
    },
    generateVodafoneZiggo: function (component, event) {
      // Get the list of uploaded files
      console.log(component.get("v.recordId"));
      var action = component.get("c.generateFrameworkForVodafoneZiggoAccount");
      action.setParams({
          accountId : component.get("v.recordId")
      });
      action.setCallback(this, function(a){
        if(a.getState() === "SUCCESS"){
              console.log('succes');
            var resultToast = $A.get("e.force:showToast");
          resultToast.setParams({
                          "title": "Success!",
                          "message": "Framework generated and updated on Account"
                      });
          resultToast.fire();;
          } else {
              console.log('failure');
            var resultToast = $A.get("e.force:showToast");
          resultToast.setParams({
                          "title": "Failed!",
                          "message": "Framework not generated"
                      });
          resultToast.fire();;
          }
      });
    $A.enqueueAction(action);
  }
})