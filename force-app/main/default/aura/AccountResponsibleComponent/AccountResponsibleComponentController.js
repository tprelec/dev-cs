({
	init : function(component, event, helper) {
		console.log(component.get("v.recordId"));
		helper.doInit(component, event);
	},
	claimBtn : function(component, event, helper) {
		helper.claimRecord(component, event);
	},
	closeBtn : function(component, event, helper) {
		helper.closeModal();
	},
	resetClaimBtn : function(component, event, helper) {
		helper.resetClaim(component);
	}
})