({
    fetchOpportunity: function(component, event, helper) {

        var action = component.get("c.fetchBasketDetails");

        action.setParams({
            "basketId" : component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var oppDetails = response.getReturnValue();
            if (oppDetails.csbb__Account__c !== undefined) {
                component.set("v.accountId", oppDetails.csbb__Account__c);
            }
            if (oppDetails.cscfga__Opportunity__c !== undefined) {
                component.set("v.oppId", oppDetails.cscfga__Opportunity__c);
            }
        });

        $A.enqueueAction(action);

    },

    fetchListOfRecordTypes: function(component, event, helper) {

        var action = component.get("c.fetchRecordTypeValues");

        action.setParams({
            "objectName" : "Case"
        });

        action.setCallback(this, function(response) {
            var mapOfRecordTypes = response.getReturnValue();
            component.set("v.options", mapOfRecordTypes);
        });

        $A.enqueueAction(action);

    },

    showCreateRecordModal : function(component, recordTypeId, entityApiName) {

        debugger;

        var createRecordEvent = $A.get("e.force:createRecord");
        if(createRecordEvent){ //checking if the event is supported
            if(recordTypeId){//if recordTypeId is supplied, then set recordTypeId parameter
                createRecordEvent.setParams({
                    "entityApiName": entityApiName,
                    "recordTypeId": recordTypeId,
                    "defaultFieldValues": {
                        "Product_Basket__c": component.get("v.recordId"),
                        "Account__c": component.get("v.accountId"),
                        "Opportunity__c": component.get("v.oppId")
                    }
                });

            } else{//else create record under master recordType

                createRecordEvent.setParams({
                    "entityApiName": entityApiName,
                    "defaultFieldValues": {
                        "Product_Basket__c": component.get("v.recordId"),
                        "Account__c": component.get("v.accountId"),
                        "Opportunity__c": component.get("v.oppId")
                    }

                });

            }

            createRecordEvent.fire();

        } else{

            alert('This event is not supported');

        }

    },



    /*

     * closing quickAction modal window

     * */

    closeModal : function(){

        var closeEvent = $A.get("e.force:closeQuickAction");

        if(closeEvent){

            closeEvent.fire();

        } else{

            alert('force:closeQuickAction event is not supported in this Ligthning Context');

        }

    },

})