({
    doInit : function(component, event, helper){
        var checked = component.get("v.itemDetail.complete");
        helper.toggleCssClass(component, 'itemComplete', checked);
    },
    onChange : function(component, event, helper){
        var recordId = component.get("v.itemDetail.recordId");
        var checked = event.getSource().get('v.checked');

        var action = component.get("c.updateRecord");
        action.setParams({
            "recordId" : recordId,
            "checkboxValue" : checked
        })

        // set call back instructions
        action.setCallback(this, function(a){

        });

        // queue action on the server
        $A.enqueueAction(action);
    }
});