({
    doInit : function(component,event,helper) {},
    onClick:function (component,event,handler) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.closeCase");
        action.setParams({
            "recordId" : recordId
        })
        action.setCallback(this, function(a) {
            var response = a.getReturnValue();
        });
        $A.enqueueAction(action);
    },
    showConfirmation : function(cmp) {
        $A.createComponent(
            "c:CS_ModalConfirmation",
            {
                "title": "Close Case",
                "tagline": "",
                "message": "Are you sure you want to change the status of the Case to Closed?",
                "confirm": cmp.getReference("c.handleConfirm"),
                "param": cmp.get("v.recordId")
            },
            function(modalWindow, status, errorMessage) {
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(modalWindow);
                    cmp.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                }
            }
        );
    },
    handleConfirm : function(cmp, event, helper) {
        var recordId = cmp.get('v.recordId');
        var action = cmp.get("c.closeCase");
        action.setParams({
            "recordId" : recordId
        })
        action.setCallback(this, function(a) {
            if(a.getState() == 'SUCCESS') {
                var result = a.getReturnValue().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ').pop();
                if(result !== '') {
                    helper.errorMessage(result);
                } else {
                    helper.successMessage('Case Closed!');
                }
            }
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    }
});