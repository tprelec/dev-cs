({
    doInit: function(component, event, helper) {
        var recordTypeId = component.get("v.recordId");
        
        console.log('got component '+recordTypeId);
        var	basketId = component.get("v.basketId");
		console.log('got component '+basketId);
		
		var action = component.get("c.getBasketData");
		action.setParams({basketId : basketId});
		
		action.setCallback(this, function(response) {
			console.log('callback');	
			var state = response.getState();
			if (state === "SUCCESS") {		
				console.log('SUCCESS');	
				var basketData = response.getReturnValue();
				
				component.set("v.accountId",basketData.csbb__Account__c);
				component.set("v.account",basketData.csbb__Account__r.Name);
                component.set("v.opportunityId",basketData.cscfga__Opportunity__c);
				component.set("v.opportunity",basketData.cscfga__Opportunity__r.Name);
				
                component.set("v.basket", basketData.Basket_Number__c);
			    
			}
			else {
				console.log('FAIL');
			}
		});
		$A.enqueueAction(action);
       
    },
    insertNewCase:function(component, event, helper) {
	    var action = component.get("c.createNewCase");
	    var newCaseFields = {};
	    
	    newCaseFields.Account = component.get("v.accountId");
	    newCaseFields.Opportunity__c = component.get("v.opportunityId");
	    newCaseFields.Product_Basket__c = component.get("v.basketId");
	    newCaseFields.Description = component.get("v.description");
	    newCaseFields.Subject = component.get("v.subject");
	    newCaseFields.RecordTypeId = component.get("v.recordId");
	    
	    var recordTypeId = component.get("v.recordId");
	    
	    
	    if(newCaseFields.Subject=='' ||newCaseFields.Subject==null || newCaseFields.Description=='' ||newCaseFields.Description==null){
	        helper.errorMessage('Subject and Description are mandatory!');
	    }
	    else{
    		action.setParams({newCaseFields : newCaseFields,recordTypeId:recordTypeId});
    		
    		action.setCallback(this, function(response) {
    			console.log('callback');	
    			var state = response.getState();
    			if (state === "SUCCESS") {		
    				console.log('SUCCESS');	
    				var basketData = response.getReturnValue();
    				
    				helper.successfulCaseCreation();
            	    var	basketId = component.get("v.basketId");
            	    
            	    if(window.location.href.indexOf("partnerportal") > -1) {
                        window.open('/partnerportal/'+basketId,'_top');
                	         
                	         
                    } else {
                         window.open('/'+basketId,'_top');
                    }
    			}
    			else {
    			    helper.errorMessage('Case not created - please contact your administrator!');
    				console.log('FAIL');
    			}
    		});
    		$A.enqueueAction(action);
	    }
	},
    gotoURL: function(component, event, helper) {
	    var	basketId = component.get("v.basketId");
	    
	    if(window.location.href.indexOf("partnerportal") > -1) {
            window.open('/partnerportal/'+basketId,'_top');
    	         
        } else {
             window.open('/'+basketId,'_top');
        }
	}
    
})