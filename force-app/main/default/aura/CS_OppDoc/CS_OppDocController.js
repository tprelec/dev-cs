({
    doInit : function(component, event, helper) {
        var hasRun = component.get("v.hasRun");
        var portalName = component.get("v.portalName");
        var pageName = '/';
        if (portalName !== '' && portalName !== undefined) {
            pageName = '/'+portalName+'/';
        }
        component.set("v.pageName", pageName);
        if (hasRun !== true) {
            helper.getApexDocuments(component);
        }
    }
})