({
    getApexDocuments : function(component) {
        component.set("v.spinner", true);
        var objId = component.get("v.recordId");
        var docList = component.get("v.docList");
        var sdList = component.get("v.sdList");
        var tariffList = component.get("v.tariffList");
        var gcList = component.get("v.gcList");
        var action = component.get("c.getDocuments");

        var docRecordsFound = false;
        var sdRecordsFound = false;
        var tariffRecordsFound = false;
        var gcRecordsFound = false;
        action.setParams({ objId: objId });
        action.setCallback(this, function(data) {
            if (data.getReturnValue().length > 0) {
                var tmpSdList = [];
                var tmpDocList = [];
                var tmpTariffList = [];
                var tmpGcListList = [];

                var res = data.getReturnValue();

                for(var i = 0; i < res.length; i++){
                    console.log('##',res[i]);
                    if(res[i].isServiceDescription === true){
                        tmpSdList.push(res[i]);
                        sdRecordsFound = true;
                    } else if(res[i].isTariffDescription === true){
                        tmpTariffList.push(res[i]);
                        tariffRecordsFound = true;
                    } else if(res[i].isGeneralCondition === true){
                        tmpGcListList.push(res[i]);
                        gcRecordsFound = true;
                    } else  {
                        tmpDocList.push(res[i]);
                        docRecordsFound = true;
                    }
                }
                component.set("v.sdRecordsFound", sdRecordsFound);
                component.set("v.tariffsRecordsFound", tariffRecordsFound);
                component.set("v.gcRecordsFound", gcRecordsFound);
                component.set("v.recordsFound", docRecordsFound);
                component.set("v.docList", tmpDocList);
                component.set("v.sdList", tmpSdList);
                component.set("v.tariffsList", tmpTariffList);
                component.set("v.gcList", tmpGcListList);
            }
            component.set("v.hasRun", true);
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
    }
})