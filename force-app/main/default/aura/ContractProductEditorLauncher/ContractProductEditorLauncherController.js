/* eslint-disable no-unused-expressions -- func-names */
({
	doInit: function (component) {
		console.log("in the launcher");
		console.log(
			component.get("v.recordId") +
				" " +
				component.get("v.orderId") +
				" " +
				component.get("v.vfContractId")
		);
		let pageReference = {
			type: "standard__component",
			attributes: {
				componentName: "c__ContractedProductContainer"
			},
			state: {
				c__recordId: component.get("v.recordId"),
				c__orderId: component.get("v.orderId"),
				c__vfContractId: component.get("v.vfContractId")
			}
		};

		const navService = component.find("navService");
		const handleUrl = (url) => {
			console.log("url: " + JSON.stringify(url));
			window.open(url, "_self");
		};
		const handleError = (error) => {
			console.log(error);
		};

		navService.generateUrl(pageReference).then(handleUrl, handleError);
	}
});