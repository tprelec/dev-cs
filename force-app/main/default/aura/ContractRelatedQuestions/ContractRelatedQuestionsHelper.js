({
	helperMethod: function () {
	},
	errorMessage: function (textOfMessage) {
		textOfMessage = textOfMessage.replace("Failed to fetch the value of", "Please fill in");

		iziToast.error({
			title: 'Ups, something went wrong',
			message: textOfMessage,
			position: 'topCenter',
			progressBar: false
		});

	},
	basketUpdateFields: function (component, event) {
		var basketFieldsMap = {};

		basketFieldsMap['Sign_on_behalf_of_customer__c'] = component.find("signOnBehalfOfCustomer") !== undefined ? component.find("signOnBehalfOfCustomer").get("v.value") : null;
		basketFieldsMap['Contract_Language__c'] = component.find("contractLanguage") !== undefined ? component.find("contractLanguage").get("v.value") : null;

		basketFieldsMap['Fiber_connection_in_different_agreement__c'] = component.find("existingContractSelect") !== undefined ? component.find("existingContractSelect").get("v.value") : null;
		basketFieldsMap['Existing_Contract_ID__c'] = component.find("contractIDExistingContract") !== undefined ? component.find("contractIDExistingContract").get("v.value") : null;
		basketFieldsMap['Existing_Contract_Date__c'] = component.find("contractDate") !== undefined ? component.find("contractDate").get("v.value") : null;

		basketFieldsMap['One_Mobile_install_base__c'] = component.find("existingMobileContractSelect") !== undefined ? component.find("existingMobileContractSelect").get("v.value") : null;
		basketFieldsMap['Existing_Mobile_Contract_ID__c'] = component.find("contractIDMobileExistingContract") !== undefined ? component.find("contractIDMobileExistingContract").get("v.value") : null;
		basketFieldsMap['Existing_Mobile_Contract_Date__c'] = component.find("contractDateMobile") !== undefined ? component.find("contractDateMobile").get("v.value") : null;

		basketFieldsMap['Vodafone_fixed_phone_service__c'] = component.find("existingFixedContractSelect") !== undefined ? component.find("existingFixedContractSelect").get("v.value") : null;
		basketFieldsMap['Existing_Fixed_Contract_ID__c'] = component.find("contractIDFixedExistingContract") !== undefined ? component.find("contractIDFixedExistingContract").get("v.value") : null;
		basketFieldsMap['Existing_Fixed_Contract_Date__c'] = component.find("contractDateFixed") !== undefined ? component.find("contractDateFixed").get("v.value") : null;

		basketFieldsMap['OneNetE_with_OneMobileOneBusiness__c'] = component.find("integratedOneMobileBusiness") !== undefined ? component.find("integratedOneMobileBusiness").get("v.value") : null;

		basketFieldsMap['Tracking_purposes__c'] = component.find("locate") !== undefined ? component.find("locate").get("v.value") : null;

		basketFieldsMap['Mobile_integrated_with_OneFixed__c'] = component.find("integretedOneFixed") !== undefined ? component.find("integretedOneFixed").get("v.checked") : null;
		basketFieldsMap['Create_new_FWA__c'] = component.find("newFWA") !== undefined ? component.find("newFWA").get("v.checked") : null;
		basketFieldsMap['Latest_TandC__c'] = component.find("latestGeneralTandC") !== undefined ? component.find("latestGeneralTandC").get("v.checked") : null;
		basketFieldsMap['Support_use_of_fax__c'] = component.find("useFax") !== undefined ? component.find("useFax").get("v.checked") : null;
		basketFieldsMap['Sellthrough_connectivity__c'] = component.find("sellthroughConnectivity") !== undefined ? component.find("sellthroughConnectivity").get("v.checked") : null;

		basketFieldsMap['Optional_Clauses__c'] = this.fillInOptionalClauses(component);

		return basketFieldsMap;
	},
	fillInOptionalClauses: function (component) {
		var optionalClauses = '';
		var flexMobileMobileHardwareAccDiscount = component.find("mobileHardwareAccDiscountSelect") !== undefined ? component.find("mobileHardwareAccDiscountSelect").get("v.value") : null;

		if (flexMobileMobileHardwareAccDiscount == 'Yes') {
			optionalClauses += '[Flex Mobile Hardware and Accessory Discount]';
		}

		if (optionalClauses.length == 0) {
			optionalClauses = '[]';
		}

		return optionalClauses;
	},
	getRequirements: function (contractSettings, basket) {
		var requireDirectV1 = false;
		var requireDirectV2 = false;
		var isDirect = basket.cscfga__Opportunity__r.Direct_Indirect__c == 'Direct';

		if (!isDirect) {
			return null;
		}

		for (var i = 0; i < contractSettings.length; i++) {
			if (contractSettings[i].Document_template__c == 'Old') {
				requireDirectV1 = true;
			}
			if (contractSettings[i].Document_template__c == 'New') {
				requireDirectV2 = true;
			}
		}

		var requirements = {
			'reqV1': requireDirectV1,
			'reqV2': requireDirectV2
		};

		return requirements;

	},
	enableGenerateContract: function (component, contractSettings) {
		var basket = component.get('v.basket');

		if (!(basket.cscfga__Opportunity__r.Account.Authorized_to_sign_1st__c !== undefined && basket.cscfga__Basket_Status__c == 'Approved')) {
			return false;
		}

		if (basket.cscfga__Opportunity__r.Direct_Indirect__c == 'Indirect') {
			return true;
		}

		var enableButtonV1 = true;
		var enableButtonV2 = true;

		var requirements = this.getRequirements(contractSettings, basket);

		if (requirements['reqV1']) {
			enableButtonV1 = basket.cscfga__Opportunity__r.Direct_Indirect__c == 'Direct' && basket.cscfga__Opportunity__r.Account.Frame_Work_Agreement__c !== undefined;
		}
		if (requirements['reqV2']) {
			enableButtonV2 = basket.cscfga__Opportunity__r.Direct_Indirect__c == 'Direct' && basket.cscfga__Opportunity__r.Account.VZ_Framework_Agreement__c !== undefined;
		}

		return enableButtonV1 && enableButtonV2;
	},
	enableLanguageChange: function (contractSettings) {
		var enableLanguageChange = true;

		for (var i = 0; i < contractSettings.length; i++) {
			if (contractSettings[i].English_contract__c == false) {
				enableLanguageChange = false;
			}
		}

		return enableLanguageChange;
	},
	getOptionalClausesJSON: function (basket, currentUser, contractSettings) {
		var isInternalUser = currentUser.Partner_User__c != null && currentUser.Partner_User__c == true;
		var isDirect = basket.DirectIndirect__c == 'Direct';
		var hasFlexMobile = false;

		for (var i = 0; i < contractSettings.length; i++) {
			if (contractSettings[i].Mobile_Scenario__c != null && contractSettings[i].Mobile_Scenario__c == '[Flex Mobile]') {
				hasFlexMobile = true;
			}
		}

		var optionalClausesJSON = {
			'flexMobile': hasFlexMobile && !isInternalUser && isDirect
		};

		return optionalClausesJSON;
	},
	redirectToOppOnSuccess: function (response, action, basket) {
		var state = response.getState();
		var retValue = response.getReturnValue();
		if (state == 'SUCCESS' && retValue != null) {
			this.redirectToOpp(basket);
		} else {
			this.callbackError(action);
		}
	},
	callbackError: function(action) {
		var errorText = '';
		var errors = action.getError();
		errors = JSON.parse(JSON.stringify(errors));
		if (errors) {
			if (errors[0] && errors[0].pageErrors) {
				for (var xx in errors[0].pageErrors) {
					errorText += errors[0].pageErrors[xx].message;
				}
			}
		}
		this.errorMessage(errorText);
	},
	redirectToOpp: function (basket) {
		if (window.location.href.indexOf("partnerportal") > -1) {
			window.location.replace('/partnerportal/s/journey-path?opportunityId=' + basket.cscfga__Opportunity__c);
		} else {
			window.location.replace('/' + basket.cscfga__Opportunity__c);
		}
	}
})