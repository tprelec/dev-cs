/* eslint-disable no-unused-expressions -- func-names */
({
	doInit: function (component, event, helper) {
		console.log("init in container");
		let pageReference = component.get("v.pageReference");
		console.log("opening: " + pageReference.state.c__opptyId);
		component.set("v.recordId", pageReference.state.c__recordId);
		component.set("v.orderId", pageReference.state.c__orderId);
		component.set("v.vfContractId", pageReference.state.c__vfContractId);
	}
});