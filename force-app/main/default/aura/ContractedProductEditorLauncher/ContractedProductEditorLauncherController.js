({
	doInit: function (component, event, helper) {
		let modalBody;
        
		/*
		$A.createComponent(
			"c:opportunityProductEditor",
			{ recordId: component.get("v.recordId") },
			function (content, status, errorMessage) {
				if (status === "SUCCESS") {
					modalBody = content;
					component.find("overlayLib").showCustomModal({
						/*header: "Add / Edit products",* /
						body: modalBody,
						showCloseButton: true,
						cssClass: "slds-modal_large",
						closeCallback: function () {}
					});
				} else {
					console.log("status: " + status);
					console.log("errorMessage " + errorMessage);
				}
			}
		);
		setTimeout(() => {
			let stdModal = document.getElementsByClassName(
				"uiModal forceModal"
			);
			stdModal.forEach(function (a, b) {
				$A.util.addClass(a, "slds-hide");
			});

			let dismissActionPanel = $A.get("e.force:closeQuickAction");
			dismissActionPanel.fire();
        }, 1000);
        */
       
		let pageReference = {
            
			type: "standard__component",
			attributes: {
				componentName: "c__contractedProductEditorContainer"
			},
			state: {
				c__opptyId: component.get("v.oppId"),
                c__orderId: component.get("v.recordId"),
                c__vfContractId: component.get("v.vfContractId")
                
                
			}
		};
		component.set("v.pageReference", pageReference);
		const navService = component.find("navService");
		const pageRef = pageReference; //component.get("v.pageReference");
		const handleUrl = (url) => {
			console.log("opening: " + url);
			window.open(url, "_self");
		};
		const handleError = (error) => {
			console.log(error);
		};
		navService.generateUrl(pageRef).then(handleUrl, handleError);
	}
});