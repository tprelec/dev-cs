({
  closeQA : function(component, event, helper) {
    $A.get("e.force:closeQuickAction").fire();
  },
  handleSave : function(component, event, helper) {
    component.find('emp_SelectVFAssets').selectSaveOLI();
  },
  handleClose : function(component, event, helper) {
    component.find('emp_SelectVFAssets').close();
  },
  disableSave : function(component, event, helper) {
    component.set('v.disabled',true);
  }
})