({
    fetchOpportunity: function(component, event, helper) {

        var action = component.get("c.fetchHBODetails");

        action.setParams({
            "HBOId" : component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var cnDetails = response.getReturnValue();
            if (cnDetails.hbo_Account__c !== undefined) {
                component.set("v.accountId", cnDetails.hbo_Account__c);
            }

        });

        $A.enqueueAction(action);

    },

    fetchListOfRecordTypes: function(component, event, helper) {

        var action = component.get("c.fetchRecordTypeValues");

        action.setParams({
            "objectName" : "Case"
        });

        action.setCallback(this, function(response) {
            var mapOfRecordTypes = response.getReturnValue();
            component.set("v.options", mapOfRecordTypes);
        });

        $A.enqueueAction(action);

    },

    showCreateRecordModal : function(component, recordTypeId, entityApiName) {

        debugger;

        var createRecordEvent = $A.get("e.force:createRecord");
        if(createRecordEvent){ //checking if the event is supported
            if(recordTypeId){//if recordTypeId is supplied, then set recordTypeId parameter
                createRecordEvent.setParams({
                    "entityApiName": entityApiName,
                    "recordTypeId": recordTypeId,
                    "defaultFieldValues": {
                        "HBO__c": component.get("v.recordId"),
                        "HBO_Account__c": component.get("v.accountId")
                    }
                });

            } else{//else create record under master recordType

                createRecordEvent.setParams({
                    "entityApiName": entityApiName,
                    "defaultFieldValues": {
                        "HBO__c": component.get("v.recordId"),
                        "HBO_Account__c": component.get("v.accountId")
                    }

                });

            }

            createRecordEvent.fire();

        } else{

            alert('This event is not supported');

        }

    },



    /*

     * closing quickAction modal window

     * */

    closeModal : function(){

        var closeEvent = $A.get("e.force:closeQuickAction");

        if(closeEvent){

            closeEvent.fire();

        } else{

            alert('force:closeQuickAction event is not supported in this Ligthning Context');

        }

    },

})