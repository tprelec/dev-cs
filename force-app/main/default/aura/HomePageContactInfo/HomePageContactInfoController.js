({
    openModel : function(component, event, helper) {
        component.set('v.showModalRecordType', true);
    },

    navigateTraining : function(component, event, helper) {

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": '/apex/TrainingRegistration'
        });
        urlEvent.fire();
    },
    closeModal : function(component, event, helper){
        component.set('v.showModalRecordType', false);
    },

    startInit: function(component, event, helper) {
        helper.fetchListOfRecordTypes(component, event, helper);
    },

    createRecord: function(component, event, helper, sObjectRecord) {
        var selectedRecordTypeId = component.get("v.value");
        console.log('##selectedRecordTypeId: '+selectedRecordTypeId);
        if(selectedRecordTypeId !== 'undefined') {
            console.log('hier');
            helper.showCreateRecordModal(component, selectedRecordTypeId, "Case");
            component.set('v.showModalRecordType', false);
        } else{
            alert('You did not select any record type');
        }
    },
})