({
	doInit : function(component, event, helper) {
		component.set("v.overrideExists", false);
	},
	justCancel : function(component, event, helper) {
		component.set("v.isOpen", false);
	},
	createNewAndClose : function(component, event, helper) {
		helper.advanceClone(component, false);
		component.set("v.isOpen", false);
	},
	overrideAndClose : function(component, event, helper) {
		helper.advanceClone(component, true);
		component.set("v.isOpen", false);
		
	},
	openModel: function(component, event, helper) {

		//console.log('ANA TEST == '+selSites);
		var selSites =JSON.parse(JSON.stringify(component.get('v.selectedSites')));


		console.log('SELECTED SITES == '+JSON.stringify(selSites));
		var exists = false;
		for(var idx in selSites.sites){
			//console.log('SELECTED SITES selSites.sites[idx]== '+JSON.stringify(selSites.sites[idx]));
			//console.log('SELECTED SITES selSites.sites== '+JSON.stringify(selSites.sites));
			//console.log('SELECTED SITES == selSites.sites[idx].rootPCR='+JSON.stringify(selSites.sites[idx].rootPCRSelected));
			
			if(selSites.sites[idx].rootPCRSelected != '' && selSites.sites[idx].rootPCRSelected!=undefined){
				exists = true;
			}
		}
		if(exists){
			component.set("v.overrideExists", true);
		}


		component.set("v.isOpen", true);
		//console.log('Modal --> selected sites');
		//console.log(JSON.stringify(component.get('v.selectedSites')));
	

		var packageConfig = component.get("v.packageConfig");

		console.log('packageConfig IN MODAL =='+packageConfig);

		var basketId = component.get("v.basketId");
		var pcrId = component.get("v.pcrId");
		var sites = component.get("v.selectedSites");

		var referenceAccessId = component.get("v.referencePCConfigId");

		var originSiteId = component.get("v.configOrigin");
		console.log('originSiteId == '+JSON.stringify(originSiteId.siteId));

		var originPBX = '';
		var pcrsForDeletion = {};

	}
})