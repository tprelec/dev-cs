({
	doInit: function (component, event, helper) {
		console.log("init in container");
		let pageReference = component.get("v.pageReference");
		console.log("opening: " + pageReference.state.c__opptyId);
		component.set("v.opptyId", pageReference.state.c__opptyId);
	}
});