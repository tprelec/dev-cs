({
	init: function (component, event, helper) {
		let pageReference = component.get("v.pageReference");
		if (pageReference && pageReference.state) {
			component.set("v.recordId", pageReference.state.c__oppId);
		}
	}
});