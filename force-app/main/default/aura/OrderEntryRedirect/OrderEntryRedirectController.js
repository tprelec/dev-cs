({
	doInit: function (component) {
		let action = component.get("c.isCommunity");

		action.setCallback(this, function (response) {
			let isCommunity = response.getReturnValue();

			let pageRef;
			console.log("rec", component.get("v.recordId"));
			if (isCommunity) {
				pageRef = {
					type: "comm__namedPage",
					attributes: {
						name: "Order_Entry__c"
					},
					state: {
						c__oppId: component.get("v.recordId")
					}
				};
			} else {
				pageRef = {
					type: "standard__component",
					attributes: {
						componentName: "c__OrderEntry"
					},
					state: {
						c__oppId: component.get("v.recordId")
					}
				};
			}
			let navService = component.find("navService");

			navService.navigate(pageRef);
			$A.get("e.force:closeQuickAction").fire();
		});
		$A.enqueueAction(action);
	}
});