({
    fetchOpportunity: function(component, event, helper) {

        var action = component.get("c.fetchOrderDetails");

        action.setParams({
            "orderId" : component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var contractDetails = response.getReturnValue();
            if (contractDetails.Account__c !== undefined) {
                component.set("v.accountId", contractDetails.Account__c);
            }
            if (contractDetails.VF_Contract__c !== undefined) {
                component.set("v.contractvfId", contractDetails.VF_Contract__c);
            }
            if (contractDetails.VF_Contract__r.Opportunity__r.Id !== undefined) {
                component.set("v.oppId", contractDetails.VF_Contract__r.Opportunity__r.Id);
            }
        });

        $A.enqueueAction(action);

    },

    fetchListOfRecordTypes: function(component, event, helper) {

        var action = component.get("c.fetchRecordTypeValues");

        action.setParams({
            "objectName" : "Case"
        });

        action.setCallback(this, function(response) {
            var mapOfRecordTypes = response.getReturnValue();
            component.set("v.options", mapOfRecordTypes);
        });

        $A.enqueueAction(action);

    },

    showCreateRecordModal : function(component, recordTypeId, entityApiName) {

        debugger;

        var createRecordEvent = $A.get("e.force:createRecord");
        if(createRecordEvent){ //checking if the event is supported
            if(recordTypeId){//if recordTypeId is supplied, then set recordTypeId parameter
                createRecordEvent.setParams({
                    "entityApiName": entityApiName,
                    "recordTypeId": recordTypeId,
                    "defaultFieldValues": {
                        "Order__c": component.get("v.recordId"),
                        "Account__c": component.get("v.accountId"),
                        "Contract_VF__c": component.get("v.contractvfId"),
                        "Opportunity__c": component.get("v.oppId")
                    }
                });

            } else{//else create record under master recordType

                createRecordEvent.setParams({
                    "entityApiName": entityApiName,
                    "defaultFieldValues": {
                        "Order__c": component.get("v.recordId"),
                        "Account__c": component.get("v.accountId"),
                        "Contract_VF__c": component.get("v.contractvfId"),
                        "Opportunity__c": component.get("v.oppId")
                    }

                });

            }

            createRecordEvent.fire();

        } else{

            alert('This event is not supported');

        }

    },



    /*

     * closing quickAction modal window

     * */

    closeModal : function(){

        var closeEvent = $A.get("e.force:closeQuickAction");

        if(closeEvent){

            closeEvent.fire();

        } else{

            alert('force:closeQuickAction event is not supported in this Ligthning Context');

        }

    },

})