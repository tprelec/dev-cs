({
	doInit : function(component, event, helper) {

		var isPrevSelected =  component.get("v.siteRecord.selectedPrev");
		var isOrigin =  component.get("v.siteRecord.isOrigin");
		var applicable =  component.get("v.siteRecord.applicable");
		if(isPrevSelected == true && isOrigin == false && applicable == true){
		//if(isPrevSelected == true && isOrigin == false){
			component.set('v.itemSelected', true);
			

			var idSelected = component.get('v.siteRecord.siteId');
		 	var rootPCRSelected = component.get('v.siteRecord.rootPcrId');

		 	var siteObjSel = component.get("v.siteRecord");

		 	console.log('Site siteObjSel =='+JSON.stringify(siteObjSel));

		 	var siteSelected = {};
		 	siteSelected.idSelected = idSelected;
		 	siteSelected.rootPCRSelected = rootPCRSelected;

		 	siteSelected.pbx = component.get('v.siteRecord.pbx');

			var cmpEvent = component.getEvent("siteItemSelected");


	 		cmpEvent.setParams({
            "selectedSiteItem" :  siteSelected,
            "unSelectedSiteItem": ''
	 		});

	 		cmpEvent.fire();
		}

	},
	onChangeCheckbox : function(component, event, helper) {
	 	var selected = event.getSource().get("v.value");
	 	console.log('Selected '+component.get('v.siteRecord.siteId'));
	 	var cmpEvent = component.getEvent("siteItemSelected");
	 	var idSelected = component.get('v.siteRecord.siteId');
	 	var rootPCRSelected = component.get('v.siteRecord.rootPcrId');

	 	var siteObjSel = component.get("v.siteRecord");

	 	console.log('Site siteObjSel =='+JSON.stringify(siteObjSel));

	 	var siteSelected = {};
	 	siteSelected.idSelected = idSelected;
	 	siteSelected.rootPCRSelected = rootPCRSelected;


	 	siteSelected.pbx = component.get('v.siteRecord.pbx');

	 	var siteAddress = '';
	 	for(var idx in siteObjSel.siteFields){
	 		if(siteObjSel.siteFields[idx].name == 'Site__r.Name'){
				siteAddress = siteObjSel.siteFields[idx].value;
	 		}
	 	}

	 	siteSelected.address = siteAddress;
	 	
	 	console.log('Site Address =='+siteAddress);

	 	if(selected == true){

	 		console.log('Selected =='+idSelected);

	 		cmpEvent.setParams({
            "selectedSiteItem" :  siteSelected,
            "unSelectedSiteItem": ''
	 	});
	 	} else{
	 		console.log('UNSelected =='+idSelected);
	        cmpEvent.setParams({
	            "selectedSiteItem" : '',
	            "unSelectedSiteItem" :idSelected
	        
			 });
		}

		console.log('Fired select event');
        cmpEvent.fire();  
		
	},

	applySelectAllSelect: function(component, event, helper) {

		console.log('SELECT ALL item');


		var currentSelectAllVal = component.get("v.selectAllVal");
		var isOrigin =  component.get("v.siteRecord.isOrigin");
		var isApplicable =  component.get("v.siteRecord.applicable");

		//console.log('Current select all val == '+currentSelectAllVal);

		if(currentSelectAllVal == true &&  isOrigin == false && isApplicable == true){
			component.set('v.itemSelected', true);
			console.log('Is origin == '+isOrigin);
			console.log('Is isApplicable == '+isApplicable);


			var idSelected = component.get('v.siteRecord.siteId');
		 	var rootPCRSelected = component.get('v.siteRecord.rootPcrId');

		 	var siteObjSel = component.get("v.siteRecord");

		 	console.log('Site siteObjSel =='+JSON.stringify(siteObjSel));

		 	var siteSelected = {};
		 	siteSelected.idSelected = idSelected;
		 	siteSelected.rootPCRSelected = rootPCRSelected;

		 	siteSelected.pbx = component.get('v.siteRecord.pbx');

			var cmpEvent = component.getEvent("siteItemSelected");


	 		cmpEvent.setParams({
            "selectedSiteItem" :  siteSelected,
            "unSelectedSiteItem": ''
	 		});

	 		cmpEvent.fire();
		}
	}
})