({
    runGetOrderSummary : function(cmp) {
        var action = cmp.get('c.buttonGetOrderSummaryBulk');
        action.setParams({orderId : cmp.get('v.recordId')});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('Success: ' + response.getReturnValue());
                $A.get("e.force:closeQuickAction").fire(); // if you want to self-close
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})