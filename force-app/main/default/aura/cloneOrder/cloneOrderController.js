({
	doInit: function (component, event, helper) {
		var action = component.get("c.getOpportunity");
		action.setParams({ oppId: component.get("v.recordId"), fields: ["Name"] });

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.oppName", response.getReturnValue().Name);
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						throw new Error(errors[0].message);
					}
				} else {
					throw new Error("Unknown error");
				}
			}
		});

		$A.enqueueAction(action);
	},

	cloneClick: function (component, event, helper) {
		component.set("v.showSpinner", true);
		var action = component.get("c.cloneOrder");
		action.setParams({
			oppId: component.get("v.recordId"),
			oppName: component.get("v.oppName")
		});

		action.setCallback(this, function (response) {
			var state = response.getState();
			component.set("v.showSpinner", false);
			if (state === "SUCCESS") {
				component.set("v.oppId", response.getReturnValue().Id);
				$A.get("e.force:closeQuickAction").fire();
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						throw new Error(errors[0].message);
					}
				} else {
					throw new Error("Unknown error");
				}
			}
		});

		$A.enqueueAction(action);
	},

	close: function (component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	}
});