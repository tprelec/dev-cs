({
	doInit: function (component, event, helper) {
		console.log("init in container");
        
		let pageReference = component.get("v.pageReference");
		console.log("opening: " + pageReference.state.c__opptyId);
		console.log("opening: " + pageReference.state.c__orderId);
		console.log("opening: " + pageReference.state.c__vfContractId);
		component.set("v.opptyId", pageReference.state.c__opptyId);
	   component.set("v.orderId", pageReference.state.c__orderId);
		//component.set("v.orderId", pageReference.state.c__opptyId);
		component.set("v.vfContractId", pageReference.state.c__vfContractId);
	}
});