<aura:component controller="PP_SearchAccountController" implements="forceCommunity:availableForAllPageTypes"
                access="global">

    <aura:handler name="init" value="{! this }" action="{! c.onInit }" />

    <aura:attribute name="currentStep" type="String" required="true" />
    <aura:attribute name="selectedAccount" type="Account" required="true" />
    <aura:attribute name="selectedAccountId" type="String" required="true" />
    <aura:attribute name="showSpinner" type="Boolean" required="true" />

    <aura:handler name="change" value="{! v.selectedAccount }" action="{! c.onSelectedAccountChange }" />

    <aura:attribute name="foundAccounts" type="Account[]" access="private" />
    <aura:attribute name="foundAccountsMap" type="Map" access="private" />
    <aura:attribute name="accountColumns" type="List" access="private" />

    <aura:registerEvent name="stepCompletedEvent" type="c:pp_stepCompleted" />

    <aura:attribute name="accountName" type="String" access="private" default="" />
    <aura:attribute name="banNumber" type="String" access="private" default="" />
    <aura:attribute name="kvkNumber" type="String" access="private" default="" />
    <aura:attribute name="zipCode" type="String" access="private" default="" />

    <aura:attribute name="searchOffset" type="Integer" access="private" default="0" />
    <aura:attribute name="searchMoreAvailable" type="Boolean" access="private" default="false" />


    <aura:if isTrue="{! v.selectedAccount }">
        <header class="slds-media slds-media_center slds-has-flexi-truncate slds-m-vertical--medium">
            <div class="slds-media__figure">
                <lightning:icon size="medium" alternativeText="Account" title="Account"
                                iconName="standard:account" />
            </div>
            <div class="slds-media__body">
                <h2>
                <span class="slds-text-heading_small">
                    {! v.selectedAccount.Name }</span>
                </h2>
            </div>
        </header>
    </aura:if>


    <div class="slds-form slds-form_compound slds-size_1-of-1 slds-large-size_1-of-2">

        <figure class="slds-box slds-m-bottom--medium slds-hide" aura:id="two-fields-required-error">
            <div class="slds-inline_icon_text slds-grid slds-text-color_error slds-icon-text-error">
                <lightning:icon iconName="utility:warning" size="small" variant="error" title="Error"
                                class="slds-m-right--small slds-col slds-no-flex" />
                <div class="slds-col slds-align-middle">
                    <p>{# $Label.c.ERROR_at_least_2_search_fields_filled }</p>
                </div>
            </div>
        </figure>

        <div class="slds-form-element__group slds-m-bottom_large">

            <div class="slds-form-element__row">
                <div class="slds-form-element slds-size_1-of-2">
                    <div class="slds-form-element__control slds-size_1-of-1">
                        <lightning:input aura:id="account_name" name="account_name"
                                         label="{# $Label.c.pp_Customer_Name }"
                                         placeholder=""
                                         type="text"  minlength="2" required="true"
                                         value="{! v.accountName }"
                                         messageWhenBadInput="{# $Label.c.ERROR_customer_name_required }" />
                    </div>
                </div>
            </div>

            <div class="slds-form-element__row">

                <div class="slds-form-element slds-size_1-of-3">
                    <div class="slds-form-element__control">
                        <lightning:input aura:id="ban_number" name="ban_number"
                                         label="{# $Label.c.pp_BAN_Number }"
                                         placeholder=""
                                         type="text" pattern="[0-9]{9}"
                                         value="{! v.banNumber }" />
                    </div>
                </div>

                <div class="slds-form-element slds-size_1-of-3">
                    <div class="slds-form-element__control">
                        <lightning:input aura:id="kvk_number" name="kvk_number"
                                         label="{# $Label.c.pp_KvK_Number }"
                                         placeholder=""
                                         type="text" pattern="[0-9]{8}"
                                         value="{! v.kvkNumber }" />
                    </div>
                </div>

                <div class="slds-form-element slds-size_1-of-3">
                    <div class="slds-form-element__control">
                        <lightning:input aura:id="zip_code" name="zip_code"
                                         label="{# $Label.c.pp_ZIP_Code }"
                                         placeholder=""
                                         type="text" pattern="[1-9][0-9]{3}\s?([a-zA-Z]{2})?"
                                         value="{! v.zipCode }" />
                    </div>
                </div>
            </div>
        </div>


        <div class="slds-form-element slds-m-bottom_large">
            <div class="slds-form-element__control">
                <lightning:button label="{# $Label.c.LABEL_Search }" title="{# $Label.c.LABEL_Search }"
                                  onclick="{! c.searchAccounts }" variant="brand" class="vf-btn" />
                <lightning:button label="{# $Label.c.pp_Clear }" title="{# $Label.c.pp_Clear }"
                                  onclick="{! c.clearForm }" variant="neutral" class="vf-btn" />
            </div>
        </div>
    </div><!-- // .slds-form -->


    <div aura:id="no-accounts-found-box" role="status"
            class="slds-scoped-notification slds-media slds-media_center slds-scoped-notification_light slds-hide" >
        <div class="slds-media__figure">
            <lightning:icon iconName="utility:info" size="x-small" alternativeText="Information" title="Information" />
        </div>
        <div class="slds-media__body">
            <p>No Accounts Found</p>
        </div>
    </div>

    <aura:if isTrue="{! v.foundAccounts.length }">
        <table aura:id="found-accounts-table" class="slds-table slds-table_bordered slds-table_cell-buffer">
            <thead>
            <tr class="slds-text-title_caps">
                <aura:iteration items="{# v.accountColumns }" var="column">
                    <th scope="col">
                        <div class="slds-truncate" title="{# column.label }">{# column.label }</div>
                    </th>
                </aura:iteration>
            </tr>
            </thead>
            <tbody>

            <aura:iteration items="{! v.foundAccounts }" var="account">
                <tr class="{# 'clickable ' + (account.Id == v.selectedAccountId ? ' slds-is-selected' : '' ) }"
                    onclick="{! c.selectAccount }" data-account-id="{# account.Id }">
                    <th scope="row" data-label="Account Name">
                        <div class="slds-truncate" title="{# account.Name }">
                            {# account.Name }
                        </div>
                    </th>
                    <td data-label="KVK Number">
                        <div class="slds-truncate" title="{# account.KVK_number__c }">
                            {# account.KVK_number__c }</div>
                    </td>
                    <td data-label="Billing Zip/Postal Code">
                        <div class="slds-truncate" title="{# account.BillingPostalCode }">
                            {# account.BillingPostalCode }</div>
                    </td>
                    <td data-label="Visiting Postal Code">
                        <div class="slds-truncate" title="{# account.Visiting_Postal_Code__c }">
                            {# account.Visiting_Postal_Code__c }</div>
                    </td>
                    <td data-label="Visiting Housenumber1">
                        <div class="slds-truncate" title="{# account.Visiting_Housenumber1__c }">
                            {# account.Visiting_Housenumber1__c }</div>
                    </td>
                    <td data-label="Visiting Housenumber Suffix">
                        <div class="slds-truncate" title="{# account.Visiting_Housenumber_suffix__c }">
                            {# account.Visiting_Housenumber_suffix__c }</div>
                    </td>
                    <td data-label="Score">
                        <div class="slds-truncate" title="{# account.score }">
                            {# account.score }</div>
                    </td>
                </tr>
            </aura:iteration>


            </tbody>
        </table>

        <aura:if isTrue="{! v.searchMoreAvailable }">
            <lightning:button label="Load More" class="slds-m-around_medium" onclick="{! c.loadMore }" />
        </aura:if>
    </aura:if>


</aura:component>