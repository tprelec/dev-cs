({

    onInit : function(component, event, helper) {
        component.set('v.accountColumns', [
            {label: $A.get("$Label.c.pp_Account_Name"), fieldName: 'Name', type: 'text', headerActions: null},
            {label: $A.get("$Label.c.pp_KvK_Number"), fieldName: 'KVK_number__c', type: 'text'},
            {label: $A.get("$Label.c.pp_Billing_Postal_Code"), fieldName: 'BillingPostalCode', type: 'text'},
            {label: $A.get("$Label.c.pp_ZIP_Code"), fieldName: 'Visiting_Postal_Code__c', type: 'text'},
            {label: $A.get("$Label.c.pp_House_Number"), fieldName: 'Visiting_Housenumber1__c', type: 'text'},
            {label: $A.get("$Label.c.pp_House_Number_Suffix"), fieldName: 'Visiting_Housenumber_suffix__c', type: 'text'},
            {label: $A.get("$Label.c.pp_Score"), fieldName: 'score', type: 'text'}
        ]);
    },


    searchAccounts : function(cmp, event, helper) {
        try {
            if (helper.formValid(cmp)) {
                //helper.clearSelectedAccount(cmp);
                helper.searchAccounts(cmp);
            }
        }
        catch (exc) {
            console.error(exc);
        }
    },


    loadMore: function(cmp, event, helper) {
        try {
            helper.loadMoreAccounts(cmp);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    clearForm: function (cmp, event, helper) {
        var fieldIds = helper.getFormFieldIds();
        for (var i=0; i<fieldIds.length; i++) {
            var field = cmp.find(fieldIds[i]);
            if (field) {
                field.set("v.value", "");
            }
        }

        cmp.set('v.foundAccounts', []);
        cmp.set('v.foundAccountsMap', {});
        //helper.clearSelectedAccount(cmp);
    },


    onSelectedAccountChange: function (cmp, event, helper) {
        // Account was set with URL parameter
        var selectedAccount = cmp.get("v.selectedAccount");
        var foundAccounts = cmp.get("v.foundAccounts") ? cmp.get("v.foundAccounts") : [];
        if (selectedAccount && foundAccounts.length === 0) {
            foundAccounts = [selectedAccount];
            cmp.set("v.foundAccounts", foundAccounts);
            cmp.set('v.foundAccountsMap', helper.getMapFromList(foundAccounts, 'Id'));
        }
    },


    selectAccount: function(component, event, helper) {
        var selectedAccountId = event.currentTarget.dataset.accountId;
        var selectedAccount = component.get('v.foundAccountsMap')[selectedAccountId];

        helper.createAccountSharing(component, selectedAccount);
    }
})