({
    getFormFieldIds : function() {
        var fieldIds = ['account_name', 'ban_number', 'kvk_number', 'zip_code'];
        return fieldIds;
    },


    clearSelectedAccount: function (cmp) {
        cmp.set("v.foundAccounts", []);
        cmp.set("v.foundAccountsMap", {});
        cmp.set("v.selectedAccount", null);
        cmp.set("v.selectedAccountId", "");
    },


    formValid: function (cmp) {
        var valid = true;
        var fieldIds = this.getFormFieldIds();

        var fieldsEntered = 0;

        for (var i=0; i<fieldIds.length; i++) {
            var field = cmp.find(fieldIds[i]);

            if (!field) {
                continue;
            }

            if (field.get("v.value")) {
                fieldsEntered++;
            }

            valid = valid && field.get('v.validity').valid;
        }

        // At least two fields should be entered
        if (fieldsEntered < 2) { //!cmp.get("v.banNumber") && !cmp.get("v.banNumber") && !cmp.get("v.banNumber")) {
            $A.util.removeClass(cmp.find("two-fields-required-error"), 'slds-hide');
            valid = false;
        }
        else {
            $A.util.addClass(cmp.find("two-fields-required-error"), 'slds-hide');
        }

        return valid;
    },
    
    
    createAccountSharing: function(cmp, account) {
        //console.log(':::createAccountSharing');
        try {
            var action = cmp.get("c.createAccountSharing");
            action.setParams({ "accountId" : account['Id'] });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    
                    if (cmp.isValid() && result.success) {
                        cmp.set('v.selectedAccountId', account['Id']);
                        cmp.set('v.selectedAccount', account);
                        
                        var stepCompletedEvent = cmp.getEvent('stepCompletedEvent');
                        stepCompletedEvent.setParam('stepName', 'account');
                        stepCompletedEvent.fire();
                        
                        cmp.set('v.showSpinner', false);
                        //console.log(':::shared');
                    }
                }
                else if (state === "INCOMPLETE") {
                    console.error("state INCOMPLETE");
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                          errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }
            });
            
            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },
    

    searchAccounts : function(component) {
        component.set('v.searchOffset', 0);
        component.set('v.showSpinner', true);

        var action = component.get("c.searchAccountsServer");
        var params = this.getParams(component);

        action.setParams({ input : params });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var searchAccountResult = response.getReturnValue();
                //console.log(searchAccountResult);
                component.set('v.searchMoreAvailable', searchAccountResult.moreAvailable);

                if (component.isValid() && searchAccountResult.success
                        && searchAccountResult.searchResults) {
                    var sr = searchAccountResult.searchResults;
                    var accts = [];
                    for (var i = 0; i < sr.length; i++) {
                        var acct = sr[i].account;
                        acct['score'] = sr[i].score;
                        accts.push( sr[i].account );
                    }
                    component.set('v.foundAccounts', accts);
                    component.set('v.foundAccountsMap', this.getMapFromList(accts, 'Id'));
                    component.set('v.searchOffset', searchAccountResult.moreAvailable ? accts.length : 0);
                    //console.log('new offset: ' + component.get('v.searchOffset'));

                    if (accts.length > 0) {
                        var noAccountsBox = component.find('no-accounts-found-box');
                        $A.util.addClass(noAccountsBox, 'slds-hide');
                    }
                    else {
                        var noAccountsBox = component.find('no-accounts-found-box');
                        $A.util.removeClass(noAccountsBox, 'slds-hide');
                    }
                    //console.log(accts);
                }
            }
            else if (state === "INCOMPLETE") {
                console.error("state INCOMPLETE");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.error("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.error("Unknown error");
                }
            }

            if (component.isValid()) {
                component.set('v.showSpinner', false);
            }
        });


        $A.enqueueAction(action);
    },


    loadMoreAccounts : function(component) {
        component.set('v.showSpinner', true);

        var action = component.get("c.searchAccountsServer");
        var params = this.getParams(component, true);
        //console.log(params);

        action.setParams({ input : params });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var searchAccountResult = response.getReturnValue();
                //console.log(searchAccountResult);

                component.set('v.searchMoreAvailable', searchAccountResult.moreAvailable);

                if (component.isValid() && searchAccountResult.success
                        && searchAccountResult.searchResults) {
                    var sr = searchAccountResult.searchResults;
                    var accts = component.get('v.foundAccounts');

                    for (var i = 0; i < sr.length; i++) {
                        var acct = sr[i].account;
                        acct['score'] = sr[i].score;
                        accts.push( sr[i].account );
                    }

                    component.set('v.foundAccounts', accts);
                    component.set('v.foundAccountsMap', this.getMapFromList(accts, 'Id'));

                    component.set('v.searchOffset', searchAccountResult.moreAvailable ? accts.length : 0);
                    //console.log('new offset: ' + component.get('v.searchOffset'));

                    if (accts.length > 0) {
                        var noAccountsBox = component.find('no-accounts-found-box');
                        $A.util.addClass(noAccountsBox, 'slds-hide');
                    }
                    else {
                        var noAccountsBox = component.find('no-accounts-found-box');
                        $A.util.removeClass(noAccountsBox, 'slds-hide');
                    }
                }
            }
            else if (state === "INCOMPLETE") {
                console.error("state INCOMPLETE");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.error("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.error("Unknown error");
                }
            }

            if (component.isValid()) {
                component.set('v.showSpinner', false);
            }
        });


        $A.enqueueAction(action);
    },


    getParams: function(cmp, isLoadMore) {
        var accountName = cmp.get("v.accountName");
        var banNumber = cmp.get("v.banNumber");
        var kvkNumber = cmp.get("v.kvkNumber");
        var zipCode = cmp.get("v.zipCode");

        var params = {
            accountName: accountName,
            banNumber: banNumber,
            kvkNumber: kvkNumber,
            zipCode: zipCode
        };

        if (isLoadMore) {
            params['searchOffset'] = cmp.get("v.searchOffset");
        }

        return JSON.stringify(params);
    },


    getMapFromList: function(objList, keyField) {
        var objMap = {};

        for (var i=0; i<objList.length; i++) {
            objMap[ objList[i][keyField] ] = objList[i];
        }

        return objMap;
    }

})