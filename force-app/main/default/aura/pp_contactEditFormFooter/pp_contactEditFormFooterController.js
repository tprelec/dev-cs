({
    handleCancel : function(component, event, helper) {
        //closes the modal or popover from the component
        component.find("contactEditModal").notifyClose();
    },
    handleSave : function(component, event, helper) {
        var event = $A.get("e.c:pp_contactEditFormButtonClicked");
        event.setParam("isSave", true);
        event.fire();
    },

    handleModalButtonClicked: function(component, event, helper) {
        //console.log(':::handleModalButtonClicked');
        var isSave = event.getParam('isSave');
        if (!isSave) {
            component.find("contactEditModal").notifyClose();
        }

        //component.find("contactEditModal").notifyClose();
    },
})