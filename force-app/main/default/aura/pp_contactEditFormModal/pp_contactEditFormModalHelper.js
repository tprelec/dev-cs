({
    doInit: function (cmp) {
        console.log('::: contactEditFormModal doInit');
        try {
            var action = cmp.get("c.getContactFields");

            var contactId = cmp.get("v.contactId") ? cmp.get("v.contactId") : null;
            action.setParam("contactId", contactId);

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var fields = response.getReturnValue();

                    for (var i=0; i<fields.length; i++) {
                        var field = fields[i];
                        console.log(field);
                        if (field.name === 'Salutation') {
                            cmp.set('v.salutation', field);
                        }
                        else if (field.name === 'Gender__c') {
                            cmp.set('v.gender', field);
                        }
                        else if (field.name === 'RecordTypeId') {
                            cmp.set('v.recordTypeId', field.value);
                        }
                    }

                    cmp.set('v.modalSpinner', false);
                }
                else if (state === "INCOMPLETE") {
                    console.error("Network error");
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }
            });

            cmp.set('v.modalSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    doSubmit: function (cmp, submitEvent) {
        console.log(':::handleSubmit');
        var fields = submitEvent.getParam("fields");

        var salutation = cmp.get("v.salutation") ? cmp.get("v.salutation").value : null;
        if (salutation) {
            fields.Salutation = salutation;
        }

        var gender = cmp.get("v.gender") ? cmp.get("v.gender").value : null;
        console.log('##gender: '+gender);
        if (gender) {
            fields.Gender__c = gender;
        }

        submitEvent.setParam("fields", fields);
    }
})