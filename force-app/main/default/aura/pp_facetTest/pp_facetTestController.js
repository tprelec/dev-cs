({
    onInit: function(cmp, event, helper) {
        console.log(':::onInit');

        try {
            var action = cmp.get("c.getContactFields");

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var fields = response.getReturnValue();

                    for (var i=0; i<fields.length; i++) {
                        var field = fields[i];
                        if (field.name === 'Salutation') {
                            cmp.set('v.salutation', field);
                        }
                        else if (field.name === 'Gender__c') {
                            cmp.set('v.gender', field);
                        }
                    }

                    cmp.set("v.dataReady", true);
                    cmp.set('v.showSpinner', false);
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },

    handleSubmit: function (cmp, event, helper) {
        console.log(':::handleSubmit');
        helper.doSubmit(cmp, event);
    },


    handleSuccess: function (cmp, event, helper) {
        console.log(':::handleSuccess');

        var response = event.getParam("response");
        console.log('::: id = ' + response.id);
        cmp.set("v.recordId", response.id);

        cmp.set("v.modalSpinner", false);
        cmp.set("v.open", false);
    },

    handleError: function(cmp, event, helper) {
        console.log(':::handleError');

        var response = event.getParam("error");
        console.error(error);
        cmp.set("v.modalSpinner", false);
    },


    handleOnload: function (cmp, event, helper) {
        console.log(':::handleOnload');
        //cmp.set('v.showSpinner', false);
        cmp.set("v.modalSpinner", false);
    },


    openModal: function (cmp, event, helper) {
        cmp.set("v.open", true);
        cmp.set("v.modalSpinner", true);
    },


    closeModal: function (cmp, event, helper) {
        helper.closeModal(cmp);
    },


    handleModalButtonClicked: function (cmp, event, helper) {

        //cmp.set("v.modalSpinner", true);

        var buttonName = event.getParam("buttonName");
        if (buttonName === 'close') {
            console.log('Close!!!');
            helper.closeModal(cmp);
        }
        else if (buttonName === 'save') {
            console.log('Save!!!');
            cmp.find("recordEditForm").submit();
        }



        /*
        window.setTimeout(
            $A.getCallback(function() {

                cmp.set("v.modalSpinner", false);

                var buttonName = event.getParam("buttonName");
                if (buttonName === 'close') {
                    cmp.set("v.open", false);
                    console.log('Close!!!');
                }
                else if (buttonName === 'save') {
                    console.log('Save!!!');
                }

            }), 2000
        );
        */
    }
})