({
    doSubmit: function (cmp, submitEvent) {
        console.log(':::handleSubmit');
        var fields = submitEvent.getParam("fields");

        var salutation = cmp.get("v.salutation") ? cmp.get("v.salutation").value : null;
        if (salutation) {
            fields.Salutation = salutation;
        }

        var gender = cmp.get("v.gender") ? cmp.get("v.gender").value : null;
        if (gender) {
            fields.Gender__c = gender;
        }

        submitEvent.setParam("fields", fields);
    },


    closeModal: function (cmp) {
        cmp.set("v.open", false);
        cmp.set("v.modalSpinner", false);
    },
})