({
    onInit: function (cmp, event, helper) {
        var steps = {
            account: 'Account',
            sites: 'Sites'
        };

        cmp.set('v.steps', steps);
    }
})