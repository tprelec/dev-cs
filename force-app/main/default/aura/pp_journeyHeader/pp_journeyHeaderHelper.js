({
    updateStepHelpText: function(cmp, currentStep) {
        //console.log(':::updateStepHelpText');
        var stepHelpText = '';

        switch (currentStep) {
            case 'account': { stepHelpText = $A.get("$Label.c.pp_Guidance_Search_Account"); break; }
            case 'sites': { stepHelpText = $A.get("$Label.c.pp_Guidance_Sites"); break; }
            case 'contacts': { stepHelpText = $A.get("$Label.c.pp_Guidance_Contacts"); break; }
            case 'opportunity': { stepHelpText = $A.get("$Label.c.pp_Guidance_Product_Basket"); break; }
            case 'order': { stepHelpText = $A.get("$Label.c.pp_Guidance_Orders"); break; }
            case 'finish': { stepHelpText = $A.get("$Label.c.pp_Guidance_Finish"); break; }
            default: { stepHelpText = ""; break; }
        }

        cmp.set("v.stepHelpText", stepHelpText);
    }
})