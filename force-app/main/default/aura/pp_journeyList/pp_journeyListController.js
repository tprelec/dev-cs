({
    doInit: function (component, event, helper) {
        // the function that reads the url parameters
        var getUrlParameter = function getUrlParameter() {

            var sPageURL = decodeURIComponent(window.location),
                sURLVariables = sPageURL.split('/'),
                sParameterName,
                i;

            console.log('##sURLVariables.length : '+sURLVariables.length );
            if (sURLVariables.length > 0) {
                console.log('##',sURLVariables);
                return sURLVariables[sURLVariables.length-1];
            }
        };

        //set the src param value to my src attribute
        //var pageName = getUrlParameter();
        var pageName = component.get('v.viewPageValue');

        component.set("v.pageName", pageName );
        var viewAccessValue = component.get('v.viewAccessValue');
        var viewListValue = component.get('v.viewListValue');
        var viewPageValue = component.get('v.viewPageValue');
        var emptyArray = [];
        component.set('v.oppties', emptyArray);

        var viewAccessOptions = [
            { value: "mine", label: "Mijn bestellingen" },
            { value: "all", label: "Alle bestellingen" }
        ];
        component.set("v.viewAccessOptions", viewAccessOptions);

        var viewListOptions = [
            { value: "opp", label: "Opportunity" }
        ];
        console.log('##viewPageValue: '+viewPageValue);
        if (viewPageValue !== 'completed-orders') {
            viewListOptions.push({value: "basket", label: "Winkelmand"});
        } else {
            component.set('v.viewListValue', 'opp');
            viewListValue = 'opp';
        }
        component.set("v.viewListOptions", viewListOptions);

        var viewPageOptions = [
            { value: "incomplete-orders", label: "Openstaande bestellingen" },
            { value: "openstaande-bestelling-bij-tes", label: "Openstaande bestellingen pending VZ" },
            { value: "completed-orders", label: "Gesloten bestellingen" }
        ];

        component.set("v.viewPageOptions", viewPageOptions);

        var filterName ='';
        if (component.get('v.filterName') !== undefined) {
            filterName = component.get('v.filterName');
        }

        console.log('##'+filterName);


        helper.retrieveRecentOpportunities(component, 0, pageName, viewAccessValue, viewListValue, filterName);
    },


    loadMore: function(cmp, event, helper) {
        cmp.set('v.searchMoreAvailable', false);
        var viewAccessValue = cmp.get('v.viewAccessValue');
        var viewListValue = cmp.get('v.viewListValue');
        var filterName ='';
        if (cmp.get('v.filterName') !== undefined) {
            filterName = cmp.get('v.filterName');
        }
        helper.retrieveRecentOpportunities(cmp, cmp.get('v.searchOffset'), cmp.get('v.pageName'), viewAccessValue, viewListValue, filterName);

    },


    goToJourney: function (cmp, event, helper) {
        var opptyId = event.getSource().get('v.value');
        if (!opptyId) {
            console.error('Invalid opportunity Id in journeyList => goToJourney');
            return;
        }
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/journey-path/?opportunityId=" + opptyId
        });
        urlEvent.fire();
    }
})