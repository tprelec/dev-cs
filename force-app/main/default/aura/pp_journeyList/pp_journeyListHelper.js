({
    retrieveRecentOpportunities: function (component, offset, pageName, viewAccessValue, viewListValue, filterName) {
        component.set('v.notSearching', false);
        try {
            var action = component.get("c.retrieveOrdersByView");

            action.setParams({
                'offset': '' + offset,
                'pageName': '' + pageName,
                'viewAccessValue': '' + viewAccessValue,
                'viewListValue': ''+viewListValue,
                'filterName' : ''+filterName
            });

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var retrievedOppties = response.getReturnValue();

                    var limit = component.get('v.searchLimit');
                    //component.set('v.searchMoreAvailable', (retrievedOppties && retrievedOppties.length === limit));
                    console.log('##retrievedOppties.length:'+retrievedOppties.length);
                    if (retrievedOppties.length < 15) {
                        component.set('v.searchMoreAvailable', false);
                    } else {
                        component.set('v.searchMoreAvailable', true);
                    }
                    //component.set('v.searchMoreAvailable', (retrievedOppties && retrievedOppties.length >= 0));

                    //console.log(retrievedOppties);

                    var oppties = component.get('v.oppties');
                    if (oppties === null || oppties === undefined) {
                        oppties = [];
                    }
                    oppties = oppties.concat(retrievedOppties);

                    component.set('v.searchOffset', oppties.length);
                    //console.log(oppties.length);
                    console.log('##',oppties);

                    component.set('v.oppties', oppties);
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }
                component.set('v.notSearching', true);
            });

            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
            component.set('v.notSearching', true);
        }
    }
})