({
    onInit: function (cmp, event, helper) {
        helper.doInit(cmp);
    },


    handleAccountIdChange:function (cmp, event, helper) {
        var newAccountId = event.getParam('value');
        var oldAccountId = event.getParam('oldValue');
        //console.log('::: handleAccountIdChange: ' + event.getParam('oldValue') + ' => ' + newAccountId);

        // form cleared or new account search executed
        if (!newAccountId) {
            helper.initiateSteps(cmp);
        }
    },
    

    handleStepCompletedEvent: function (cmp, event, helper) {
        var completedStep = event.getParam('stepName');
        //console.log(':::handleStepCompletedEvent: => ' + completedStep);
        helper.completeStep(cmp, completedStep);

        var moveNext = event.getParam('moveNext');
        if (moveNext) {
            var currentStep = cmp.get('v.currentStep');
            helper.moveStep(cmp, currentStep, true);
        }
    }, 


    handlePathNavigationButtonEvent: function(cmp, event, helper) {
        var isNext = event.getParam('next');
        var currentStep = cmp.get('v.currentStep');
        //console.log(':::handlePathNavigationButtonEvent: => ' + isNext + ';' + currentStep);
        helper.moveStep(cmp, currentStep, isNext);
    },


    onPathItemClick: function(cmp, event, helper) {
        var selectedStepName = event.currentTarget.dataset.stepName;
        var prevStep = cmp.get("v.currentStep");
        //console.log(':::prevStep = ' + prevStep + '; selectedStepName = ' + selectedStepName + '; cmp valid = ' + cmp.isValid());

        //cmp.set("v.currentStep", selectedStepName);
        //helper.updateCurrentStepOld(cmp, selectedStepName);

        helper.updateCurrentStep(cmp, prevStep, selectedStepName);
    },


    handleRouteChange: function (cmp, event, helper) {
        //console.log('routeChanged');
        //console.log(event);
    }
})