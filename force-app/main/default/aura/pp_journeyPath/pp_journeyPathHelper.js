({
    doInit: function (cmp) {
        //console.log('path init');

        var urlParams = this.getUrlParams(cmp);

        if (urlParams['opportunityId']) {
            this.retrieveOpportunity(cmp, urlParams['opportunityId']);
            return;
        }
        else if (urlParams['accountId']) {
            this.retrieveAccount(cmp, urlParams['accountId']);
            return;
        }
        else {
            cmp.set('v.opportunityId', "");
            this.initiateSteps(cmp);
            return;
        }
    },


    retrieveOpportunity: function(cmp, opportunityId) {
        console.log(':::Retrieving oppty = ' + opportunityId);
        try {
            var action = cmp.get("c.getOpportunity");
            action.setParam('opportunityId', opportunityId);

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var oppty = response.getReturnValue();
                    cmp.set('v.opportunity', oppty);
                    cmp.set('v.opportunityId', oppty.Id);

                    var account = oppty['Account'];
                    cmp.set('v.selectedAccount', account);
                    cmp.set('v.selectedAccountId', account['Id']);

                    cmp.set('v.currentStep', 'opportunity'); // TODO: check this

                    this.initiateSteps(cmp, account, oppty);
                    // get oracle CPQ Site Id // TODO: check this
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }

                cmp.set('v.showSpinner', false);
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    retrieveAccount: function(cmp, accountId) {
        //console.log(':::Retrieving account = ' + accountId);
        try {
            var action = cmp.get("c.getAccount");
            action.setParam('accountId', accountId);

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var account = response.getReturnValue();
                    cmp.set('v.selectedAccount', account);
                    cmp.set('v.selectedAccountId', account['Id']);

                    cmp.set('v.currentStep', 'opportunity'); // TODO: check this

                    cmp.set('v.showSpinner', false);

                    this.initiateSteps(cmp, account, null);
                    // get oracle CPQ Site Id // TODO: check this
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    showStep: function (cmp) {
        try {
            var currentStep = cmp.get('v.currentStep');
            //console.log(':::path helper init: currentStep = ' + currentStep);
            var currentPathContent = cmp.find('path-content-' + currentStep);
            $A.util.removeClass(currentPathContent, "slds-hide");
        }
        catch (exc) {
            console.log(exc);
        }
    },


    initiateSteps: function (cmp, account, oppty) {
        //console.log(':::initiateSteps');
        var accountSelected = account ? true : false;
        var opptyExists = oppty ? true : false; 

        var steps = [
            {name: 'account', title: $A.get("$Label.c.pp_Step_Account"), assistText: '', /* Current Stage/Stage Complete */
                isComplete: accountSelected, isCurrent: !accountSelected, isActive: !accountSelected},
            {name: 'sites', title: $A.get("$Label.c.pp_Step_Sites"), assistText: '',
                isComplete: accountSelected, isCurrent: false, isActive: false},
            {name: 'contacts', title: $A.get("$Label.c.pp_Step_Contacts"), assistText: '',
                isComplete: accountSelected, isCurrent: false, isActive: false},
            {name: 'opportunity', title: $A.get("$Label.c.pp_Step_Product_Basket"), assistText: '',
                isComplete: false, isCurrent: accountSelected, isActive: accountSelected},
            {name: 'order', title: $A.get("$Label.c.pp_Step_Orders"), assistText: '',
                isComplete: false, isCurrent: false, isActive: false},
            {name: 'finish', title: $A.get("$Label.c.pp_Step_Finish"), assistText: '',
                isComplete: false, isCurrent: false, isActive: false}
        ];


        cmp.set('v.steps', steps);

        this.showStep(cmp);
    },


    completeStep: function (cmp, completedStep) {
        //console.log(':::completeStep');
        var opptyId = cmp.get('v.opportunityId');

        var steps = cmp.get('v.steps');
        if (!steps) {
            this.doInit(cmp);
        }

        var steps = cmp.get('v.steps');

        for (var i=0; i<steps.length; i++) {
            var step = steps[i];

            if (step.name === completedStep) {
                step.isComplete = true;
                break;
            }
        }

        cmp.set('v.steps', steps);
    },


    moveStep: function(cmp, currentStep, isNext) {
        //console.log(':::moveStep: currentStep = ' + currentStep + '; isNext = ' + isNext);
        var steps = cmp.get('v.steps');

        var currentIndex = 0;
        for (var i=0; i<steps.length; i++) {
            var step = steps[i];

            if (step.name === currentStep) {
                currentIndex = i;
                break;
            }
        }

        var nextCurrentIndex = isNext ? currentIndex + 1 : currentIndex - 1;
        if (nextCurrentIndex < 0 || nextCurrentIndex >= steps.length) {
            console.error('nextCurrentIndex is out of bound');
            return;
        }


        if (steps[currentIndex].isComplete && isNext) {
            steps[currentIndex].isCurrent = false;
            steps[nextCurrentIndex].isCurrent = true;
        }

        cmp.set('v.steps', steps);

        this.updateCurrentStep(cmp, steps[currentIndex].name, steps[nextCurrentIndex].name);
    },


    updateCurrentStep: function(cmp, prevStep, nextStep) {
        try {
            if (prevStep === nextStep) {
                return;
            }

            //console.log(':::updateCurrentStep');
            var steps = cmp.get('v.steps');

            var currentIndex = 0;
            var nextCurrentIndex = 0;
            for (var i=0; i<steps.length; i++) {
                var step = steps[i];

                if (step.name === prevStep) {
                    currentIndex = i;
                }
                if (step.name === nextStep) {
                    nextCurrentIndex = i;
                }
            }

            steps[currentIndex].isActive = false;
            steps[nextCurrentIndex].isActive = true;
            cmp.set('v.steps', steps);

            var currentPathContent = cmp.find('path-content-' + prevStep);
            $A.util.addClass(currentPathContent, "slds-hide");

            var nextPathContent = cmp.find('path-content-' + nextStep);
            $A.util.removeClass(nextPathContent, "slds-hide");

            cmp.set('v.currentStep', nextStep);
        }
        catch (error) {
            console.error(error);
        }
    },


    getUrlParams: function (cmp) {
        //You get the whole decoded URL of the page.
        var sPageURL = decodeURIComponent(window.location.search.substring(1));

        //Split by & so that you get the key value pairs separately in a list
        var sURLVariables = sPageURL.split('&');
        //console.log(':::sURLVariables = ' + sURLVariables);

        var params = {
            //'opportunityId': '',
            //'accountId': '',
            //'currentStep': ''
        };

        var sParameterName;
        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.

            var paramName = sParameterName[0];
            if (paramName === 'opportunityId' || paramName === 'accountId') {
                var paramValue = sParameterName[1];
                //sParameterName[1] === undefined ? 'Not found' : sParameterName[1];

                if (paramValue) {
                    params[paramName] = paramValue;
                }
            }

            //console.log('urlParams = ' + JSON.stringify(params));
            return params;
        }
    },


    // TODO:
    setUrlParams: function (cmp, paramName, paramValue) {
        //You get the whole decoded URL of the page.
        var sPageURL = decodeURIComponent(window.location.search.substring(1));

        //Split by & so that you get the key value pairs separately in a list
        var sURLVariables = sPageURL.split('&');
        //console.log(':::sURLVariables = ' + sURLVariables);

        var params = {
            //'opportunityId': '',
            //'accountId': '',
            //'currentStep': ''
        };

        var sParameterName;
        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.

            var paramName = sParameterName[0];
            if (paramName === 'opportunityId' || paramName === 'accountId') {
                var paramValue = sParameterName[1];
                //sParameterName[1] === undefined ? 'Not found' : sParameterName[1];

                if (paramValue) {
                    params[paramName] = paramValue;
                }
            }

            //console.log('urlParams = ' + JSON.stringify(params));
            return params;
        }
    }
})