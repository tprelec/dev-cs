({
    onInit: function (cmp, event, helper) {
        try {
            helper.checkIfSitesExist(cmp);
        }
        catch (exc) {
            console.error(exc);
        }
    }
})