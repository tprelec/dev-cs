({
    /**
     * Checks if there are any site exists and mark the step as completed if at least one site exists
     */
    checkIfSitesExist: function(component) {
        var selectedAccountId = component.get('v.selectedAccountId');

        var action = component.get("c.checkAccountSitesExist");
        action.setParams({ 'accountId' : selectedAccountId });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if ("SUCCESS" === state && component.isValid()) {
                var sitesExists = response.getReturnValue();
                component.set('v.accountSitesExist', sitesExists);

                if (sitesExists) {
                    var stepCompletedEvent = component.getEvent('stepCompletedEvent');
                    stepCompletedEvent.setParam('stepName', 'sites');
                    stepCompletedEvent.fire();
                }
                // TODO: what if there are no site, then one added, then we need to complete step somehow
            }
            else if ("INCOMPLETE" === state) {
                console.error("state INCOMPLETE");
            }
            else if ("ERROR" === state) {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.error("Error message: " +
                            errors[0].message);
                    }
                }
                else {
                    console.error("Unknown error");
                }
            }

            this.showSpinner(component, false);
        });

        this.showSpinner(component, true);
        $A.enqueueAction(action);
    },


    showSpinner: function (cmp, enable) {
        if (cmp.isValid()) {
            cmp.set("v.backgroundJobs", cmp.get("v.backgroundJobs") + (enable ? 1 : -1));
        }
    }
})