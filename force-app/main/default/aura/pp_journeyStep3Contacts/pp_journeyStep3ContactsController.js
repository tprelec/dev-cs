({
    onInit: function (cmp, event, helper) {
        helper.retrieveContacts(cmp);
    },


    loadMoreContacts: function(cmp, event, helper) {
        helper.retrieveContacts(cmp, true);
    },


    createRecord : function (component, event, helper) {
        //var windowHash = window.location.hash;
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Contact",
            "defaultFieldValues": {
                "AccountId": component.get("v.selectedAccountId")
                //,"panelOnDestroyCallback": function(event) { window.location.hash = windowHash; }
            }
        });
        createRecordEvent.fire();
    },


    handleContactEditFormEvent: function(cmp, event, helper) {
        //console.log(':::handleContactEditFormEvent');
        var contactId = event.getParam("contactId");
        var isNew = event.getParam("isNew");

        helper.retrieveContact(cmp, contactId, isNew);
    },


    handleContactMenuSelect: function(cmp, event, helper) {
        try {
            var selectedMenuItemValue = event.getParam("value");
            var contactId = event.getSource().get('v.value');

            if ('edit' === selectedMenuItemValue) {
                //helper.editRecord(cmp, contactId);
                helper.openEditContactModal(cmp, contactId);
            }
            else if ('authorize' === selectedMenuItemValue) {
                var accountId = cmp.get('v.selectedAccountId');
                helper.authorizeToSignFirst(cmp, accountId, contactId);
            }
        }
        catch (exc) {
            console.log(exc);
        }
    },


    openEditContactModal: function (cmp, event, helper) {
        helper.openEditContactModal(cmp, null);
    },


    handleContactEditFormModalEvent: function (cmp, event, helper) {
        //console.log(':::handleContactEditFormModalEvent');

        var eventName = event.getParam("eventName");
        if (eventName === 'close') {
            helper.closeEditContactModal(cmp); 
        }
        else if (eventName === 'save') {
            /*
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success!",
                "type": "success",
                "message": "The record has been updated successfully."
            });
            toastEvent.fire();

            helper.retrieveContacts(cmp);
            */
            //


            var contactId = event.getParam("contactId");
            var isNew = event.getParam("isNew");
            helper.retrieveContact(cmp, contactId, isNew);
        }
    }
})