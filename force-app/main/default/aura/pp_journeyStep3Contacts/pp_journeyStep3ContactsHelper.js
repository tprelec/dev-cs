({
    reloadContacts: function(cmp) {
        cmp.set('v.searchOffset', 0);
        cmp.set('v.searchMoreAvailable', false);
        cmp.set('v.contacts', []);
        cmp.get('v.contactsMap', {});
        this.retrieveContacts(cmp);
    },

    retrieveContacts: function(component, isMore) {
        var action = component.get("c.getContacts");
        var offset = component.get('v.searchOffset');
        var params = {
            'accountId'    : component.get('v.selectedAccountId'),
            'offsetString' : offset + ''
        };
        action.setParams(params);

        //console.log(params);

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var retrievedContacts = response.getReturnValue();

                if (component.isValid()) {
                    var limit = component.get('v.searchLimit');
                    component.set('v.searchMoreAvailable', retrievedContacts.length === limit);

                    var contacts = component.get('v.contacts');
                    var contactsMap = component.get('v.contactsMap');

                    for (var i=0; i<retrievedContacts.length; i++) {
                        var cont = retrievedContacts[i];
                        contactsMap[cont.Id] = cont;
                    }

                    contacts = this.getMapValues(contactsMap);

                    component.set('v.searchOffset', contacts.length);
                    component.set('v.contacts', contacts);
                    component.set('v.contactsMap', contactsMap);

                    var noContactsBox = component.find('no-contacts-found-box');
                    if (contacts.length > 0) {
                        $A.util.addClass(noContactsBox, 'slds-hide');

                        var stepCompletedEvent = component.getEvent('stepCompletedEvent');
                        stepCompletedEvent.setParam('stepName', 'contacts');
                        stepCompletedEvent.fire();
                    }
                    else {
                        $A.util.removeClass(noContactsBox, 'slds-hide');
                    }

                    this.closeEditContactModal(component);
                }
            }
            else if (state === "INCOMPLETE") {
                console.error("state INCOMPLETE");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.error("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.error("Unknown error");
                }
            }

            if (component.isValid()) {
                component.set('v.showSpinner', false);
            }
        });

        component.set('v.showSpinner', true);
        $A.enqueueAction(action);
    },


    retrieveContact: function(component, contactId, isNew) {
        var action = component.get("c.getContact");
        action.setParams({ 'contactId' : contactId });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && component.isValid()) {
                var contact = response.getReturnValue();
                console.log('##contact:',contact);
                var contacts = component.get('v.contacts');
                var contactsMap = component.get('v.contactsMap');

                contactsMap[contact.Id] = contact;
                contacts = this.getMapValues(contactsMap);

                component.set('v.contacts', contacts);
                component.set('v.contactsMap', contactsMap);

                this.closeEditContactModal(component);

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "The record has been updated successfully."
                });
                toastEvent.fire();
            }
            else if (state === "INCOMPLETE") {
                console.error("state INCOMPLETE");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.error("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.error("Unknown error");
                }
            }

            if (component.isValid()) {
                component.set('v.showSpinner', false);
            }
        });

        component.set('v.showSpinner', true);
        $A.enqueueAction(action);
    },


    editRecord : function (cmp, recordId) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": recordId
        });
        editRecordEvent.fire();
    },


    authorizeToSignFirst: function(cmp, accountId, contactId) {
        try {
            var action = cmp.get("c.authorizeToSignFirst");

            var params = {
                'accountId' : accountId,
                'contactId' : contactId
            };
            action.setParams(params);

            action.setCallback(this, function(response) {
                cmp.set('v.showSpinner', false);

                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();

                    if (result.success) {
                        this.toastMessage('Success!', 'Record Updated', 'success');
                        this.reloadContacts(cmp);
                    }
                    else {
                        this.toastMessage('Error!', result.message.message, 'error');
                    }
                }
                else if (state === "INCOMPLETE") {
                    console.error("state INCOMPLETE");
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }


            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    openEditContactModal: function (cmp, contactId) {
        console.log(':::openEditContactModal');

        cmp.set("v.selectedContactId", contactId);

        cmp.set("v.showContactForm", true);
    },


    closeEditContactModal: function (cmp) {
        cmp.set("v.showContactForm", false);
        //$A.util.addClass(cmp.find('contactEditForm'), 'slds-hide');
    },


    getMapValues: function (mapData) {
        var values = [];
        for (var key in mapData) {
            if (mapData.hasOwnProperty(key)) {
                values.push(mapData[key]);
            }
        }
        return values;
    },


    toastMessage: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})