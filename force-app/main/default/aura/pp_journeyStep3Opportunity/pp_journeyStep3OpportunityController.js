({
    onInit: function(cmp, event, helper) {
        var iFrameMessageReceivedCallback = $A.getCallback(function(frameEvent) {
            //console.log('message received');
            //console.dir(frameEvent.data);
            if (frameEvent.origin !== vfOrigin) {
                // Not the expected origin: Reject the message!
                return;
            }
            var messageType = frameEvent.data.type;
            if (messageType === 'frameEvent') {
                var pageName = frameEvent.data.pageName;
                var actionName = frameEvent.data.actionName;
                if (pageName === 'OpportunityClosedWon') {
                    if (actionName === 'close') {
                        console.log('close OpportunityClosedWon');
                        helper.closeCloseWonFrame(cmp);
                    }
                    else if (actionName === 'updateHeight' && frameEvent.data.value) {
                        console.log('updateHeight OpportunityClosedWon => ' + frameEvent.data.value);
                        //cmp.set('v.closeWonHeight', frameEvent.data.value);
                    }
                }
                else if (pageName === 'OpportunityAttachmentManager') {
                    if (actionName === 'updateHeight' && frameEvent.data.value) {
                        console.log('updateHeight OpportunityAttachmentManager => ' + frameEvent.data.value);
                        //cmp.set('v.manageAttachsHeight', frameEvent.data.value);
                    }
                }
            }
        });

        var vfOrigin = "https://" + cmp.get("v.vfHost");
        window.addEventListener("message", iFrameMessageReceivedCallback, false);

        var opptyId = cmp.get("v.opportunityId");

        if (!opptyId) {
            return;
        }

        helper.retrieveOppty(cmp, opptyId);
    },


    onCurrentStepChange: function(component, event, helper) {
        var currentStep = event.getParam('value');
        var accountId = component.get('v.selectedAccountId');

        if (currentStep === 'opportunity' && accountId) {
            //var stepCompletedEvent = component.getEvent('stepCompletedEvent');
            //stepCompletedEvent.setParam('stepName', 'opportunity');
            //stepCompletedEvent.fire();
        }
    },

    createOppty: function (cmp, event, helper) {
        var createOpptyEvent = $A.get("e.force:createRecord");
        createOpptyEvent.setParams({
            "entityApiName": "Opportunity",
            "defaultFieldValues": {
                'AccountId' : cmp.get('v.selectedAccountId')
            }
        });
        createOpptyEvent.fire();
    },


    onEditOpptyClick: function (cmp, event, helper) {
        cmp.set("v.editMode", true);
        //console.log(cmp.get("v.editMode"));
    },


    onCloseWonClick: function (cmp, event, helper) {
        helper.openCloseWonFrame(cmp);
    },

    handleCloseCloseWon: function(cmp, event, helper) {
        helper.closeCloseWonFrame(cmp);
    },


    handleOpptySavedEvent: function (cmp, event, handler) {
        try {
            var isCreate = event.getParam("isCreate");
            var opptyId = event.getParam("opptyId") || cmp.get("v.opportunityId");
            console.log(':::opptyId = ' + opptyId);

            var toastEvent = $A.get("e.force:showToast");
            var successMessage = isCreate ? $A.get("$Label.c.pp_Opportunity_Created") : $A.get("$Label.c.pp_Record_Saved");
            toastEvent.setParams({
                "title": "Success!",
                "message": successMessage,
                "type": "success"
            });
            toastEvent.fire();

            // fix for "Error in $A.getCallback() [Cannot read property 'querySelectorAll' of null]"
            if (opptyId) {
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": "/journey-path/?opportunityId=" + opptyId
                });
                setTimeout(urlEvent.fire(), 1000);
                
            }
            else {
                $A.get("e.force:refreshView").fire();
            }

            cmp.set("v.editMode", false);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    onOpptyIdChange: function (cmp, event, helper) {
        console.log('onOpptyIdChange');
        var opptyId = event.getParam("value");

        if (!opptyId) {
            return;
        }

        helper.retrieveOppty(cmp, opptyId);

        // get Contract

        // get Orders
    },

    downloadContract: function (cmp, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/ProposalPage?Id=a4A9E0000004bqb"
        });
        urlEvent.fire();
    },


    onContractChange: function (cmp, event, helper) {
        //var contract = event.getParam("value");
        //console.log(':::contract appeared!');
    }, 
    
    onAddMobileProductClick: function (cmp, event, helper) {
        var opptyId = cmp.get("v.opportunityId");
        var action = cmp.get("c.getBANforAddMobileJourney");
        action.setParam('opportunityId', opptyId);
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && cmp.isValid()) {
                let oppty = response.getReturnValue();
                if (oppty.BAN__r && !isNaN(parseInt(oppty.BAN__r.Name,10))) {
                    helper.openProductMobileChangeSetting( cmp, event, true , false);
                } else {
                    let messageAMP = $A.get("$Label.c.EMP_NoValidBanSelected");
                    helper.showToastAMP( messageAMP );   
                }
            }
            else if ("INCOMPLETE" === state) {
                console.error("Network Error / Server is Down");
            }
            else if ("ERROR" === state) {
                var message = "Response State is ERROR; Unknown error";
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    message = "Error message: " + errors[0].message;
                }
                console.error(message);
            }
        });
        $A.enqueueAction(action);
    },
    
    onChangeProductSettingsClick: function (cmp, event, helper) {
        helper.openProductMobileChangeSetting( cmp, event , false , true );
    },
    returnToOppCS: function (cmp, event, helper) {
        cmp.set('v.showProductMobileSetting', false);
        cmp.set('v.showChangeSetting', false);
    },
    returnToOppAMP: function (cmp, event, helper) {
        cmp.set('v.showProductMobileSetting', false);
        cmp.set('v.showAddMobileProduct', false);
    },
})