({
    openCloseWonFrame: function(cmp) {
        console.log('openCloseWonFrame');
        cmp.set('v.showCloseWon', true);


        console.log(cmp.get('v.showCloseWon'));
    },

    closeCloseWonFrame: function(cmp) {
        //console.log(cmp.isValid());
        cmp.set('v.showCloseWon', false);

        var opptyId = cmp.get("v.opportunityId");
        if (!opptyId) {
            return;
        }

        this.retrieveOppty(cmp, opptyId);
    },


    retrieveOppty: function (cmp, opptyId) {
        try {
            var action = cmp.get("c.getOpportunity");
            action.setParam('opportunityId', opptyId);
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var oppty = response.getReturnValue();
                    console.log(oppty);

                    if (!oppty) {
                        console.error("Failed to get oppty " + opptyId);
                        cmp.set('v.showSpinner', false);
                        return;
                    }

                    cmp.set("v.opportunity", oppty);

                    var opptyStepCompleted = false;

                    var contract = oppty.Contract_VF__r;
                    if (contract) {
                        cmp.set("v.contract", oppty.Contract_VF__r);
                        opptyStepCompleted = true;
                    }


                    // TODO: RT is not supported
                    var recordTypeIsMac = oppty.Record_Type_is_MAC__c;
                    cmp.set("v.recordTypeIsMac", recordTypeIsMac);
                    if (recordTypeIsMac) {
                        opptyStepCompleted = true;
                    }

                    if (opptyStepCompleted) {
                        var stepCompletedEvent = cmp.getEvent('stepCompletedEvent');
                        stepCompletedEvent.setParam('stepName', 'opportunity');
                        stepCompletedEvent.setParam('moveNext', !!contract);
                        stepCompletedEvent.fire();
                    }

                    // TODO:
                    // 1) add Closed status processing: Lost - red, Won - go to next
                    // 2) process 'Closing by SAG' Stage
                }
                else if ("INCOMPLETE" === state) {
                    console.error("Network Error / Server is Down");
                }
                else if ("ERROR" === state) {
                    var message = "Response State is ERROR; Unknown error";
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        message = "Error message: " + errors[0].message;
                    }
                    console.error(message);
                }

                if (cmp.isValid()) {
                    cmp.set('v.showSpinner', false);
                }
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },

    /**
     * <!-- lightning:button class = "vf-btn slds-m-bottom--medium" variant="neutral" type="submit" label="{! $Label.c.pp_Edit_Opportunity }" onclick="{! c.onEditOpptyClick }" / -->
     *
     *     <aura:if isTrue="{! v.recordTypeIsMac == false }">
     <div class="{! if(or(empty(v.opportunityId), v.editMode), 'slds-show', 'slds-hide') }">
     <c:pp_opportunityEditForm accountId="{! v.selectedAccountId }"
     opportunityId="{! v.opportunityId }"
     editMode="{! v.editMode }" />
     </div>
     </aura:if>
     */

    //EMP project Helper functions
    openProductMobileChangeSetting: function( cmp , event, isAddMobile , isChangeSetting ){
        cmp.set('v.showProductMobileSetting', true);
        cmp.set('v.showAddMobileProduct', isAddMobile);
        cmp.set('v.showChangeSetting', isChangeSetting);
    },

    showToastAMP : function (labelMessage) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": labelMessage,
            "type": "warning"
        });
        toastEvent.fire();
    }
})