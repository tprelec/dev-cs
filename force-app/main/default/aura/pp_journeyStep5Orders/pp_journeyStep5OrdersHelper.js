({
    retrieveOrders: function (cmp) {
        try {
            var contractId = cmp.get("v.contract").Id;

            var action = cmp.get("c.getOrders");
            action.setParam('contractId', contractId);

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var orders = response.getReturnValue();

                    cmp.set("v.orders", orders);
                    
                    var allOrdersCompleted = orders.length > 0;
                    for (var i=0; i<orders.length; i++) {
                        var orderStatus = orders[i].Status__c;
                        //'Accepted', 'Processed manually'
                        if (orderStatus !== 'Accepted' && orderStatus !== 'Processed manually') {
                            allOrdersCompleted = false;
                            break;
                        }
                    }


                    console.log('orders compl = ' + allOrdersCompleted);

                    if (allOrdersCompleted) {
                        console.log('orders compl');
                        var stepCompletedEvent = cmp.getEvent('stepCompletedEvent');
                        stepCompletedEvent.setParam('stepName', 'order');
                        stepCompletedEvent.setParam('moveNext', true);
                        stepCompletedEvent.fire();
                    }
                }
                else if ("INCOMPLETE" === state) {
                    console.error("Network Error / Server is Down");
                }
                else if ("ERROR" === state) {
                    var message = "Response State is ERROR; Unknown error";
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        message = "Error message: " + errors[0].message;
                    }
                    console.error(message);
                }

                cmp.set('v.showSpinner', false);
            });


            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    }
})