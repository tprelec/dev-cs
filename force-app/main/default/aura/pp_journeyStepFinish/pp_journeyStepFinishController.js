({
    goToOpenJourneys: function (cmp, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/open-journeys"
        });
        urlEvent.fire();
    }
})