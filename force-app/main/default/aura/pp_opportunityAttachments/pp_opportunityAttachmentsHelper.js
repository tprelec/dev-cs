({
    retrieveAttachments: function (cmp, opptyId) {
        try {
            var action = cmp.get("c.getOpportunityAttachments");
            action.setParam('opportunityId', opptyId);

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var attachs = response.getReturnValue();
                    cmp.set('v.attachments', attachs);
                    //console.log('got attachs');

                    this.setIframeUrl(cmp, opptyId);
                }
                else if ("INCOMPLETE" === state) {
                    console.error("Network Error / Server is Down");
                }
                else if ("ERROR" === state) {
                    var message = "Response State is ERROR; Unknown error";
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        message = "Error message: " + errors[0].message;
                    }
                    console.error(message);
                }

                if (cmp.isValid()) {
                    cmp.set('v.showSpinner', false);
                }
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    setIframeUrl: function (cmp, opportunityId) {
        cmp.set("v.iframeURL", $A.get("$Label.c.pp_Community_Base_URL") +
            "/apex/OpportunityAttachmentManager?opportunityId=" + opportunityId);
    }
})