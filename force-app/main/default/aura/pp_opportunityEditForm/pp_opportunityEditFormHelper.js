({
    getDefaultOpptyRecordType: function (cmp) {
        try {
            var action = cmp.get("c.getOpportunityDefaultRecordTypeId");

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var recordTypeId = response.getReturnValue();
                    cmp.set('v.defaultRecordTypeId', recordTypeId);
                    //console.log(recordTypeId);
                }
                else if (state === "INCOMPLETE") {
                    console.error("state INCOMPLETE");
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }

                if (cmp.isValid()) {
                    cmp.set('v.showSpinner', false);
                }
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },

    getJourneyData: function (cmp) {
        try {
            var action = cmp.get("c.getJourneyLock");
            var opptyId = cmp.get("v.opportunityId");
            action.setParam('opportunityId', opptyId);
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var lockJourney = response.getReturnValue();
                    cmp.set('v.lockJourney', lockJourney);
                    console.log(lockJourney);
                }
                else if (state === "INCOMPLETE") {
                    console.error("state INCOMPLETE");
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }

                if (cmp.isValid()) {
                    cmp.set('v.showSpinner', false);
                }
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    }
})