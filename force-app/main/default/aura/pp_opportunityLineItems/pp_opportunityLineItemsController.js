({
    onInit: function (cmp, event, helper) {
        var opptyId = cmp.get("v.opportunityId");

        helper.retrieveOLIs(cmp, opptyId);
    },


    refreshList: function(cmp, event, helper) {
        var opptyId = cmp.get("v.opportunityId");
        helper.retrieveOLIs(cmp, opptyId);
    },


    hideIframe: function (cmp, event, helper) {
        cmp.set('v.showIframe', false);

        var opptyId = cmp.get("v.opportunityId");
        helper.retrieveOLIs(cmp, opptyId);
    },


    handleManageAttachmentsClick: function (cmp, event, helper) {
        cmp.set('v.showIframe', true);
    }
})