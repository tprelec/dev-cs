({
    retrieveOLIs: function (cmp, opptyId) {
        try {
            var action = cmp.get("c.getOpportunityLineItems");
            action.setParam('opportunityId', opptyId);

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var olis = response.getReturnValue();
                    cmp.set('v.opportunityLineItems', olis);
                    console.log('got olis');
                }
                else if ("INCOMPLETE" === state) {
                    console.error("Network Error / Server is Down");
                }
                else if ("ERROR" === state) {
                    var message = "Response State is ERROR; Unknown error";
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        message = "Error message: " + errors[0].message;
                    }
                    console.error(message);
                }

                if (cmp.isValid()) {
                    cmp.set('v.showSpinner', false);
                }
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },
})