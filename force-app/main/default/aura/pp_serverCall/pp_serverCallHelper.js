({
    serverCall: function (cmp, methodName, parameters, successCallback, errorCallback) {
        try {
            var action = cmp.get('c.' + methodName);

            if (parameters) {
                action.setParams(parameters);
            }

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    successCallback(cmp, response);
                }
                else {
                    var errorMessage = 'Unknown error';

                    if (state === "INCOMPLETE") {
                        errorMessage = "The server didn’t return a response. The server might be down or the client might be offline.";
                    }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors && errors[0] && errors[0].message) {
                            errorMessage = errors[0].message;
                        }
                    }
                    console.error(errorMessage);
                    this.toastMessage('Error!', 'error', errorMessage);

                    errorCallback(cmp, response);
                }
            });

            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    /**
     * Types: error, warning, success, info.
     */
    toastMessage: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title"   : title,
            "type"    : type,
            "message" : message
        });
        toastEvent.fire();
    }
})