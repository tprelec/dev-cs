public without sharing class ADMTaskTriggerHandler extends TriggerHandler {
	private List<agf__ADM_Task__c> newTasks;
	private Map<Id, agf__ADM_Task__c> oldTaskMap;

	private static Map<Id, Id> userThemeMap;

	private void init() {
		newTasks = (List<agf__ADM_Task__c>) this.newList;
		oldTaskMap = (Map<Id, agf__ADM_Task__c>) this.oldMap;
	}

	public override void afterInsert() {
		init();
		handleThemeAssignment();
	}

	public override void afterUpdate() {
		init();
		handleThemeAssignment();
	}

	public override void afterDelete() {
		init();
		handleThemeassignment();
	}

	private void handleThemeAssignment() {
		List<agf__ADM_Task__c> themeToAdd = new List<agf__ADM_Task__c>();
		List<agf__ADM_Task__c> themeToDelete = new List<agf__ADM_Task__c>();

		if (newTasks != null) {
			// Insert or Update
			for (agf__ADM_Task__c t : newTasks) {
				if (
					t.agf__Assigned_To__c != null &&
					GeneralUtils.isRecordFieldChanged(t, oldTaskMap, 'agf__Assigned_To__c')
				) {
					themeToAdd.add(t);

					if (oldTaskMap != null) {
						themeToDelete.add(oldTaskMap.get(t.id));
					}
				}
			}
		} else {
			// Delete
			themeToDelete = oldTaskMap.values();
		}

		if (!themeToAdd.isEmpty()) {
			assignTheme(themeToAdd);
		}
		if (!themeToDelete.isEmpty()) {
			unassignTheme(themeToDelete);
		}
	}

	// Assignes theme for provided list of Tasks.
	private void assignTheme(List<agf__ADM_Task__c> tasks) {
		List<agf__ADM_Theme_Assignment__c> toCreate = new List<agf__ADM_Theme_Assignment__c>();
		List<agf__ADM_Theme_Assignment__c> existingAssignments = getThemeAssignments(tasks);
		for (agf__ADM_Task__c tsk : tasks) {
			Boolean found = false;
			for (agf__ADM_Theme_Assignment__c assignment : existingAssignments) {
				if (
					assignment.agf__Work__c == tsk.agf__Work__c &&
					assignment.agf__Theme__c == getUserThemes().get(tsk.agf__Assigned_To__c)
				) {
					found = true;
					break;
				}
			}
			if (!found) {
				agf__ADM_Theme_Assignment__c newAssignment = new agf__ADM_Theme_Assignment__c(
					agf__Work__c = tsk.agf__Work__c,
					agf__Theme__c = getUserThemes().get(tsk.agf__Assigned_To__c)
				);
				toCreate.add(newAssignment);
			}
		}
		if (!toCreate.isEmpty()) {
			insert toCreate;
		}
	}

	// Unassigns theme for provided list of Tasks.
	private void unassignTheme(List<agf__ADM_Task__c> tasks) {
		Set<Id> workSet = (Set<Id>) GeneralUtils.getIDSetFromList(tasks, 'agf__Work__c');
		List<agf__ADM_Task__c> remainingTaskList = [
			SELECT Id, agf__Work__c, agf__Assigned_To__c
			FROM agf__ADM_Task__c
			WHERE agf__Work__c IN :workSet
		];
		List<agf__ADM_Task__c> taskForDelete = new List<agf__ADM_Task__c>();

		for (agf__ADM_Task__c tsk : tasks) {
			Boolean found = false;

			for (agf__ADM_Task__c rt : remainingTaskList) {
				if (
					rt.agf__Work__c == tsk.agf__Work__c &&
					rt.agf__Assigned_To__c == tsk.agf__Assigned_To__c
				) {
					found = true;
				}
			}
			if (!found) {
				taskForDelete.add(tsk);
			}
		}
		if (!taskForDelete.isEmpty()) {
			deleteThemeAssignments(tasks, taskforDelete);
		}
	}

	// Provides a map of User IDs and Theme IDs
	private Map<Id, Id> getUserThemes() {
		if (userThemeMap == null) {
			userThemeMap = new Map<Id, Id>();

			for (agf__ADM_Theme__c theme : [
				SELECT Id, User__c
				FROM agf__ADM_Theme__c
				WHERE User__c != NULL
			]) {
				userThemeMap.put(theme.User__c, theme.Id);
			}
		}
		return userThemeMap;
	}

	// Gets a list of existing Theme assignments based on list of provided Tasks.
	private List<agf__ADM_Theme_Assignment__c> getThemeAssignments(List<agf__ADM_Task__c> tasks) {
		Set<Id> workSet = (Set<Id>) GeneralUtils.getIDSetFromList(tasks, 'agf__Work__c');
		return [
			SELECT Id, agf__Work__c, agf__Theme__c
			FROM agf__ADM_Theme_Assignment__c
			WHERE agf__Work__c IN :workSet
		];
	}

	private void deleteThemeAssignments(
		List<agf__ADM_Task__c> tasks,
		List<agf__ADM_Task__c> taskforDelete
	) {
		List<agf__ADM_Theme_Assignment__c> toDelete = new List<agf__ADM_Theme_Assignment__c>();
		List<agf__ADM_Theme_Assignment__c> existingAssignments = getThemeAssignments(tasks);

		for (agf__ADM_Task__c tsk : taskForDelete) {
			for (agf__ADM_Theme_Assignment__c thm : existingAssignments) {
				if (
					thm.agf__Work__c == tsk.agf__Work__c &&
					thm.agf__Theme__c == getUserThemes().get(tsk.agf__Assigned_To__c)
				) {
					toDelete.add(thm);
				}
			}
		}
		if (!toDelete.isEmpty()) {
			delete toDelete;
		}
	}
}