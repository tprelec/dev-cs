public without sharing class ADMWorkTriggerHandler extends TriggerHandler {
	private List<agf__ADM_Work__c> newWorks;
	private Map<Id, agf__ADM_Work__c> oldWorksMap;

	private void init() {
		newWorks = (List<agf__ADM_Work__c>) this.newList;
		oldWorksMap = (Map<Id, agf__ADM_Work__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setInitialStatus();
	}

	public override void beforeUpdate() {
		init();
		setVodafoneStatus();
	}

	public override void afterUpdate() {
		init();
		syncToCase();
		createPlaceholderTask();
	}

	private void setInitialStatus() {
		for (agf__ADM_Work__c newWork : newWorks) {
			newWork.VF_Status__c = newWork.agf__Status__c;
		}
	}

	// Syncs values from Work to Case for fields defined in mapping.
	private void syncToCase() {
		Map<String, String> fieldMapping = SyncUtil.fullMapping('agf__ADM_Work__c -> Case')
			.get('agf__ADM_Work__c -> Case');

		// Loop through the work records and check if any the fields marked for sync have changed
		List<agf__ADM_Work__c> workListForSync = GeneralUtils.filterChangedRecords(
			newWorks,
			oldWorksMap,
			new List<String>(fieldMapping.values()),
			false
		);

		if (workListForSync.isEmpty()) {
			return;
		}

		List<Case> caseList = [
			SELECT Id, agf__ADM_Work__c
			FROM Case
			WHERE agf__ADM_Work__c IN :workListForSync
		];

		if (caseList.isEmpty()) {
			return;
		}

		Map<Id, List<Case>> workToCasesMap = GeneralUtils.groupByIDField(
			caseList,
			'agf__ADM_Work__c'
		);
		Map<String, Schema.SObjectField> workFieldsMap = Schema.SObjectType.agf__ADM_Work__c.fields.getMap();

		// Loop through the work records that have been marked for sync
		for (agf__ADM_Work__c newWork : workListForSync) {
			// Get the related case records
			List<Case> workCases = workToCasesMap.get(newWork.Id);

			if (workCases == null) {
				continue;
			}
			for (Case relatedCase : workCases) {
				// Loop through all the sync fields
				for (String field : fieldMapping.keySet()) {
					String theField = fieldMapping.get(field);
					Schema.DisplayType fieldType = workFieldsMap.get(theField)
						.getDescribe()
						.getType();
					Object theNewValue = newWork.get(theField);

					if (theNewValue != null) {
						if (String.valueof(fieldType) == 'DOUBLE') {
							String d = String.valueof(theNewValue);
							relatedCase.put(field, d);
						} else {
							relatedCase.put(field, theNewValue);
						}
					}
				}
			}
		}
		updateCases(caseList);
	}

	private void updateCases(List<Case> caseList) {
		if (caseList.size() > 0) {
			update caseList;
		}
	}

	// Sets the Vodafone status field which is synced back to Case.
	// Puts the agile accelerator status to the correct internal value.
	private void setVodafoneStatus() {
		List<agf__ADM_Work__c> worksWithChangedStatus = GeneralUtils.filterChangedRecords(
			newWorks,
			oldWorksMap,
			'agf__Status__c'
		);

		if (worksWithChangedStatus.isEmpty()) {
			return;
		}

		Map<String, AgileAccelator__c> mapping = AgileAccelator__c.getAll();

		for (agf__ADM_Work__c newWork : worksWithChangedStatus) {
			// Put the value selected by the user into the field that is synced to Case
			newWork.VF_Status__c = newWork.agf__Status__c;

			// Map as per the custom setting mapping table
			// The idea is to change the value to Closed so agile accelator knows its closed
			if (mapping.containsKey(newWork.agf__Status__c)) {
				newWork.agf__Status__c = mapping.get(newWork.agf__Status__c)
					.Agile_Accelator_Status__c;
				// Unfortunately the AgileAccelerator logic does not fire even though we set the status to Closed
				// So manually set the closed date and username of the user who closed it
				if (newWork.agf__Status__c == 'Closed') {
					newWork.agf__Closed_On__c = System.now();
					newWork.agf__Closed_By__c = GeneralUtils.currentUser.Username;
				}
			}
		}
	}

	// Creates placeholder Task for the new Work Assignee
	private void createPlaceholderTask() {
		List<agf__ADM_Work__c> worksChangedAssignee = GeneralUtils.filterChangedRecords(
			newWorks,
			oldWorksMap,
			'agf__Assignee__c'
		);

		if (worksChangedAssignee.isEmpty()) {
			return;
		}

		// Get all the tasks for work items that have changed assigned value
		List<agf__ADM_Task__c> existingTasks = [
			SELECT agf__Work__c, agf__Assigned_To__c
			FROM agf__ADM_Task__c
			WHERE agf__Work__c IN :worksChangedAssignee
		];

		List<agf__ADM_Task__c> taskForInsert = new List<agf__ADM_Task__c>();

		for (agf__ADM_Work__c newWork : worksChangedAssignee) {
			if (newWork.agf__Assignee__c != null) {
				Boolean hasTask = false;

				for (agf__ADM_Task__c task : existingTasks) {
					if (
						task.agf__Work__c == newWork.Id &&
						task.agf__Assigned_To__c == newWork.agf__Assignee__c
					) {
						hasTask = true;
						break;
					}
				}
				if (!hasTask) {
					agf__ADM_Task__c tsk = new agf__ADM_Task__c(
						agf__Assigned_To__c = newWork.agf__Assignee__c,
						agf__Subject__c = 'Placeholder task to complete work item',
						agf__Work__c = newWork.Id
					);
					taskForInsert.add(tsk);
				}
			}
		}
		if (taskForInsert.size() > 0) {
			insert taskForInsert;
		}
	}
}