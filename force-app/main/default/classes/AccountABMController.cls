/**
 * @description     This class shows a colored block on the visualforce page if a certain condition is met.
 * @author          03-10-2017: Marcel Vreuls
 */
public with sharing class AccountABMController {

	private final Account account;
     public boolean isVis {get; set;}
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public AccountABMController(ApexPages.StandardController stdController) {
        
        Account myAccount = (Account)stdController.getRecord();
        Id AccountID = myAccount.Id;

        this.account = [select Id,
                            Name, 
                            Account_Based_Marketing__C                           
                        from Account
                        where Id = : AccountID];

        if (!account.Account_Based_Marketing__C)
        {
            isVis = false;
        }
        else
        {
            isVis = true;
        }
    }

}