/**
 * @description			This class is responsible for calling, parsing the accountasset update 
 * @author				Marcel Vreuls
 * @History				Aug 2019: changed = to like
 * 
 * @Defaults   			Requires the creation and update of the Olbico Custom settings
 *
 *
 */
global class AccountAssetUpdateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {

	global Database.QueryLocator start(Database.BatchableContext BC) {

		//AccountAssetUpdateBatch myBatchObject = new AccountAssetUpdateBatch(); 
		//Id batchId = Database.executeBatch(myBatchObject,2);
		//AccountAssetUpdateBatch myBatchObject = new AccountAssetUpdateBatch(); 
		//Id batchId = Database.executeBatch(myBatchObject,1);
		// Retrieve any accounts from asset where today is the assets retainability date (from this date on it becomes retainable)
   		List<AccountToUpdate__c> accountToUpdateList = new List<AccountToUpdate__c>();		
		date retention_date = Date.today()+121;
		Map<Id,AggregateResult> results = new Map<id,AggregateResult>([Select Account__c Id from VF_Asset__c WHERE Contract_End_Date__c = :retention_date GROUP BY Account__c]);

		// For all accounts returned try and insert into the table for processing
		for (Id Id : results.keySet() ){
			AccountToUpdate__c atu = new AccountToUpdate__c();				
			atu.AccountId__c = Id;
	    	accountToUpdateList.add(atu);		    	
		}
		// Using insert will fail on any duplicates
		// so by using database.insert we allow the non duplicates to be successfully inserted
		Database.insert(accountToUpdateList,false);

		// Keep the number of records to 100000 and batch size of 2
		// This will result in a max of 50000 executions against the daily limit
		return Database.getQueryLocator('Select AccountID__c from AccountToUpdate__c limit 100000');

	}

	// This is the execute to handle a schedule request (just enables the job to be scheduled)
	global void execute(SchedulableContext sc) {
        AccountAssetUpdateBatch controller = new AccountAssetUpdateBatch();
        database.executebatch(controller,2);   
	}

	// This is the execute for the scope of the batch (ie does the processing)
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		// Note that we will want to limit the batch size to 1 so that only one account is processed in each execute   			
   		processRecords(scope);

	}
	
	global void finish(Database.BatchableContext BC) {
		//Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							TotalJobItems
						  FROM
							AsyncApexJob
						  WHERE
							Id =: BC.getJobId()];

		// Send an email to the running user notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		String[] toAddresses = new String[] {UserInfo.getUserEmail()};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Competitor Assests recalculated ' + a.Status);
		mail.setPlainTextBody('Competitor Assests recalculation job processed ' + a.TotalJobItems +
							  ' batches with '+ a.NumberOfErrors + ' failures.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}

	public void processRecords(List<AccountToUpdate__c> accountsToProcess){

		Integer retainable=0;	//teller to count the retainable assest
		Integer active=0; 		//teller to count the Active assets
		List<Account> accountsToUpdate = new List<Account>();

		for(AccountToUpdate__c atu : accountsToProcess){
			
			// Added limit 50000 to prevent SOQL error
			// Accounts that have more than 50000 are not valid anyway so the counts are not important for those
			List<VF_Asset__c> allVFAsset = [Select Id, Retainable__c, CTN_Status__c,Priceplan_Class__c from VF_Asset__c WHERE Account__c = : atu.AccountId__c AND CTN_Status__c = 'Active' AND Priceplan_Class__c LIKE '%voice%' limit 49000];
			if(!allVFAsset.isEmpty()){
				
				for(VF_Asset__c x : allVFAsset){
                	//System.Debug('Retainable__c' + x.Retainable__c);
					if(x.Retainable__c == true ) {
						retainable++;
					}
					//System.Debug('CTN_Status__c' + x.CTN_Status__c);
					active++;                	
            	}          
				
				Account a = new Account();				
				a.id = atu.AccountID__c;
				a.Count_Retainable_Assets__c = retainable;
				a.Count_Active_Assets__c = active;
				accountsToUpdate.add(a);

			}
			retainable=0; //zet teller op 0, voor nieuwe Account
			active=0;//zet teller op 0, voor nieuwe Account			
		}

		update accountsToUpdate;

		// This deletes the entry in AccountToUpdate__c, not the actual account
		delete accountsToProcess;	

	}
	
}