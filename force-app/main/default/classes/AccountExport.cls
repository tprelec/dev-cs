/**
 * @description			This is the class that contains logic for performing the Account export to other systems
 *						Requires WITHOUT SHARING as data might have to be exported (and thus updated) that is not owned by the user
 * @author				Guy Clairbois
 */
public with sharing class AccountExport {

	public static Set<Id> accountsInExport {
		get{
			if(accountsInExport == null){
				accountsInExport = new Set<Id>();
			}
			return accountsInExport;
		}
		set;
	}

	@future(callout=true)
	public static void exportAccountsOffline(Set<Id> accountIds){
		AccountExport.accountsInExport.addAll(accountIds);            
		exportAccounts(accountIds);
	}
	
	@future(callout=true)
	public static void exportBansOffline(Set<Id> banIds){
		AccountExport.accountsInExport.addAll(banIds);            
		exportBansById(banIds);		
	}

	public static void exportBansById(Set<Id> banIds){
		exportAccounts(null,banIds);
	}

	public static void exportAccounts(Set<Id> acctIds){
		exportAccounts(acctIds,null);
	}

	// Note: Only partner accounts are exported to BOP (isPartner=true)
	// Non partner accounts will instead run a BAN export
	public static void exportAccounts(Set<Id> acctIds, Set<Id> banIds){
		// possibly switch to BAN export instead of Account export
		Set<Id> banAccountIds = new Set<Id>();
		Set<Id> acctAccountIds = new Set<Id>();
		// if banIds are provided (thus individual bans should be exported), use those. If not, look up the accountids and use those to export the full accounts
		if(banIds == null){
			for(Account a : [Select 
								Id, 
								IsPartner
							from 
								Account 
							Where 
								Id in :acctIds]
			){
				if(a.IsPartner){
					acctAccountIds.add(a.Id);
				} else {
					banAccountIds.add(a.Id);
				}
			}	

			exportBans(banAccountIds,null);
		} else {
			exportBans(null,banIds);
		}
		acctIds = acctAccountIds;
		// end of temporary switch to BAN export

		if(!acctIds.isEmpty()){
			List<Account> createCustomerAccounts = new List<Account>();
			List<Account> updateCustomerAccounts = new List<Account>();
			List<Account> createResellerAccounts = new List<Account>();
			List<Account> updateResellerAccounts = new List<Account>();
			Map<String, List<Account>> accountMap = new Map<String, List<Account>>{'createCustomer' => new List<Account>(),
																				   'updateCustomer' => new List<Account>(),
																				   'createReseller' => new List<Account>(),
																				   'updateReseller' => new List<Account>()};
			List<Account> accountsToUpdate = new List<Account>();
			Map<Id,Account> partnerAcctIdToAcct = new Map<Id,Account>();
			Set<Id> newAccounts = new Set<Id>(); 
			// following is a map to temporarily store the fixedowner in order to simplify the code later on
			Map<Id,Account> accountIdToFixedOwnerInfo = new Map<Id,Account>();
			
			for(Account a : [Select 
								Id, 
								Name,
								BAN_Number__c,
								//Corporate_Id__c,
								BOPCode__c,
								BOP_export_Errormessage__c,
								IsPartner,
								Phone,
								Fax,
								KVK_Number__c,
								Website,
								BillingStreet,
								BillingPostalCode,
								BillingCity,
								BillingCountry,
								Dealer_code__c,
								Parent.BAN_Number__c,
								//Parent.Corporate_Id__c,
								Parent.BOPCode__c,
								Parent.Id,
								Owner.UserType,
								Owner.Name,
								Owner.Manager.Name,
								Owner.AccountId,
								Unify_ref_Id__c
							from 
								Account 
							Where 
								Id in :acctIds]
			){
				if(a.IsPartner){
					// partner accounts handling	
					if(a.BOPCode__c != null && a.BOPCode__c != ''){
						updateResellerAccounts.add(a);
						//accountMap.put(a, 'updateReseller');
					} else {
						createResellerAccounts.add(a);
						//accountMap.put(a, 'createReseller');
					}
				} else {
					// customer accounts handling
					// first do some checks that can't be handled by validation rules
					if(a.BAN_Number__c == null ){
						a.BOP_export_Errormessage__c = 'Valid BAN Number is required for exporting an Account';
						accountsToUpdate.add(a);
						continue;
					} else if(!StringUtils.checkBan(a.BAN_Number__c)){
						a.BOP_export_Errormessage__c = 'Valid BAN Number is required for exporting an Account';
						accountsToUpdate.add(a);				
						continue;
					} else if(a.Parent != null){
						if(a.Parent.BAN_Number__c != null){
							if(!StringUtils.checkBan(a.BAN_Number__c)){
								a.BOP_export_Errormessage__c = 'Valid Parent BAN Number is required for exporting an Account';
								accountsToUpdate.add(a);					
								continue;
							}
						} 
					}

					// if bopcode is filled, do update, else do create
					if(a.BOPCode__c != null && a.BOPCode__c != ''){
						updateCustomerAccounts.add(a);
						//accountMap.get('updateCustomer').add(a);
					} else {
						createCustomerAccounts.add(a);
						//accountMap.get('createCustomer').add(a);
					}
					if(a.Owner.AccountId != null){
			            // prepare a map for partner accounts. Temporarily put the client account in (to prevent using 2 variables)
			            partnerAcctIdToAcct.put(a.Owner.AccountId,null);
			        }

				}	
			}
			// create a map of partner account account information
			if(!partnerAcctIdToAcct.isEmpty()){
				for(Account pa : [Select Id, BAN_Number__c, BOPCode__c From Account Where Id in :partnerAcctIdToAcct.keySet()]){
					partnerAcctIdToAcct.put(pa.Id,pa);
				}
			}		
			
			// Generic handling
			if(!accountMap.keySet().isEmpty())
			{
				ECSCompanyService companyService = new ECSCompanyService();
				List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
				Map<String,Id> banToAcctId = new Map<String,Id>();

				Boolean success = false;
			}
			
			if(!createCustomerAccounts.isEmpty()){
	 			
				ECSCompanyService createService = new ECSCompanyService();
				List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
				Set<Id> AcctId = new Set<Id>();
				for(Account a : createCustomerAccounts){
					ECSCompanyService.request req = accountToRequest(a,partnerAcctIdToAcct);
					
					AcctId.add(a.Id);
					requests.add(req);
				}
				createService.setRequest(requests);
				
				Boolean success = false;
				try{
					createService.makeRequest('createCustomer');
					success = true;
				} catch (ExWebServiceCalloutException ce){
					for(Account a : createCustomerAccounts){
						// only update the error if it has not been set before
						if(a.BOP_export_Errormessage__c != ce.get255CharMessage()){
							a.BOP_export_Errormessage__c = ce.get255CharMessage();
							accountsToUpdate.add(a);	
						}
					}
				}
				
				if(success){
					for (ECSCompanyService.response response : createService.getResponse()){
						system.debug(response);
						if(AcctId.contains(response.referenceId)){
							Account a = new Account(Id = response.referenceId);
							if(response.bopcode != null) a.BOPCode__c = response.bopCode;
							if(response.errorCode == null || response.errorCode == '' || response.errorCode == '0'){
								// success
								// add info to account
								a.BOP_export_datetime__c = system.now();
								a.BOP_export_Errormessage__c = null;
								// startup location and user sync
								newAccounts.add(a.Id);
							} else {
								// failure
								// add errormessage to account
								a.BOP_export_Errormessage__c = (response.errorCode+' - '+response.errorMessage).abbreviate(255);
														
								if(response.errorCode == '48' || response.errorCode == '1005'){
									// if error is 'existing customer (48)' then do update instead of insert
									// do this by updating the BOPcode. That will trigger the update process
									a.BOPCode__c = response.bopCode;
									a.BOP_export_datetime__c = system.now();
									// also overwrite the errormessage, since errormsgs starting with a number will not be retried
									a.BOP_export_Errormessage__c = 'awaiting update';

									//if(response.ban != null) a.BAN_Number__c = response.ban;
									//if(response.corporateId != null) a.Corporate_Id__c = response.corporateId;
								}
							}
							accountsToUpdate.add(a);
						} else {
							throw new ExMissingDataException('No referenceId returned by BOP');
						}
					}
				}
			}
			if(!updateCustomerAccounts.isEmpty()){
				ECSCompanyService updateService = new ECSCompanyService();
				List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
				Set<Id> AcctId = new Set<Id>(); 
				 
				for(Account a : updateCustomerAccounts){
					ECSCompanyService.request req = accountToRequest(a,partnerAcctIdToAcct);

					AcctId.add(a.Id);
					requests.add(req);
				}
				updateService.setRequest(requests);

				Boolean success = false;
				try{
					updateService.makeRequest('updateCustomer');
					success = true;
				} catch (ExWebServiceCalloutException ce){
					for(Account a : updateCustomerAccounts){
						// only update the error if it has not been set before
						if(a.BOP_export_Errormessage__c != ce.get255CharMessage()){					
							a.BOP_export_Errormessage__c = ce.get255CharMessage();
							accountsToUpdate.add(a);
						}	
					}
				}
				if(success){
					for (ECSCompanyService.response response : updateService.getResponse()){
						system.debug(response);
						if(AcctId.contains(response.referenceId)){
								Account a = new Account(Id = response.referenceId);
								if(response.errorCode == null || response.errorCode == '' || response.errorCode == '0'){
									// success
									// add info to account
									a.BOP_export_datetime__c = system.now();
									a.BOP_export_Errormessage__c = null;
									// startup location and user sync
									newAccounts.add(a.Id); 
								} else {
									// failure
									// add errormessage to account
									a.BOP_export_Errormessage__c = (response.errorCode+' - '+response.errorMessage).abbreviate(255);
		
								}
								accountsToUpdate.add(a);
						} else {
							throw new ExMissingDataException('No referenceId returned by BOP');
						}
					}
				}			
			}

			if(!createResellerAccounts.isEmpty()){
	 			
				ECSCompanyService createService = new ECSCompanyService();
				List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
				for(Account a : createResellerAccounts){
					ECSCompanyService.request req = accountToRequest(a,partnerAcctIdToAcct);				
					requests.add(req);
				}
				createService.setRequest(requests);
				
				Boolean success = false;
				try{
					createService.makeRequest('createReseller');
					success = true;
				} catch (ExWebServiceCalloutException ce){
					for(Account a : createResellerAccounts){
						// only update the error if it has not been set before
						if(a.BOP_export_Errormessage__c != ce.get255CharMessage()){
							a.BOP_export_Errormessage__c = ce.get255CharMessage();
							accountsToUpdate.add(a);	
						}
					}
				}			
				
				if(success){
					for (ECSCompanyService.response response : createService.getResponse()){
						system.debug(response);					
						Account a = new Account(Id = response.referenceId);
						if(response.bopcode != null) a.BOPCode__c = response.bopCode;
						if(response.errorCode == null || response.errorCode == '' || response.errorCode == '0'){
							// success
							// add info to account
							a.BOP_export_datetime__c = system.now();
							a.BOP_export_Errormessage__c = null;
							// startup location and user sync
							newAccounts.add(a.Id);						
						} else {
							// failure
							// add errormessage to account
							a.BOP_export_Errormessage__c = (response.errorCode+' - '+response.errorMessage).abbreviate(255);
													
							if(response.errorCode == '48' || response.errorCode == '1005'){
								// if error is 'existing customer (48)' then do update instead of insert
								// do this by updating the BOPcode. That will trigger the update process
								a.BOPCode__c = response.bopCode;
								a.BOP_export_datetime__c = system.now();
								// also overwrite the errormessage, since errormsgs starting with a number will not be retried
								a.BOP_export_Errormessage__c = 'awaiting update';

								//if(response.corporateId != null) a.Corporate_Id__c = response.corporateId;
							}
						}
						accountsToUpdate.add(a);					
					}
				}
			}

			if(!updateResellerAccounts.isEmpty()){
				ECSCompanyService updateService = new ECSCompanyService();
				List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
				for(Account a : updateResellerAccounts){
					ECSCompanyService.request req = accountToRequest(a,partnerAcctIdToAcct);				
					requests.add(req);
				}
				updateService.setRequest(requests);

				Boolean success = false;
				try{
					updateService.makeRequest('updateReseller');
					success = true;
				} catch (ExWebServiceCalloutException ce){
					for(Account a : updateResellerAccounts){
						// only update the error if it has not been set before
						if(a.BOP_export_Errormessage__c != ce.get255CharMessage()){					
							a.BOP_export_Errormessage__c = ce.get255CharMessage();
							accountsToUpdate.add(a);
						}	
					}
				}
				if(success){
					for (ECSCompanyService.response response : updateService.getResponse()){
						system.debug(response);					
						Account a = new Account(Id = response.referenceId);
						if(response.errorCode == null || response.errorCode == '' || response.errorCode == '0'){
							// success
							// add info to account
							a.BOP_export_datetime__c = system.now();
							a.BOP_export_Errormessage__c = null;
							// startup location and user sync
							newAccounts.add(a.Id); 
						} else {
							// failure
							// add errormessage to account
							a.BOP_export_Errormessage__c = (response.errorCode+' - '+response.errorMessage).abbreviate(255);
						}
						accountsToUpdate.add(a);
					}
				}			
			}

			// allow partial success
			Database.update(accountsToUpdate,false);		
			// TODO: handle failed updates
				
		}
	}
	
	public static Boolean skipContractCheck {
		get{			
			ExportBatchSchedule__c b = ExportBatchSchedule__c.getInstance();
			return b.skipContractCheck__c;
		}
		set;
	}
	public static void exportBans(Set<Id> acctIds, Set<Id> banIds){
		List<Ban__c> createCustomerAccounts = new List<Ban__c>();
		List<Ban__c> updateCustomerAccounts = new List<Ban__c>();
		Map<String, List<Ban__c>> accountMap = new Map<String, List<Ban__c>>{'createCustomer' => new List<Ban__c>(),
																			   'updateCustomer' => new List<Ban__c>(),
																			   'createReseller' => new List<Ban__c>(),
																			   'updateReseller' => new List<Ban__c>()};
		List<Ban__c> bansToUpdate = new List<Ban__c>();
		Set<ID> banIDSet = new Set<ID>();
		Map<Id,Account> partnerAcctIdToAcct = new Map<Id,Account>();
		Set<Id> newAccounts = new Set<Id>(); 
		
		// fetch the relevant bans (filtering out the ones without a contract)
		List<Ban__c> banList;
		if(!skipContractCheck){
			Set<Id> bansWithContract = new Set<Id>();


			for(Order__c o : [Select VF_Contract__r.Opportunity__r.Ban__c From Order__c 
								Where 
									VF_Contract__r.Opportunity__r.Ban__c != null 
								AND 
									Export_System_Customerdata__c = 'BOP'
								AND 
									(VF_Contract__r.Opportunity__r.Ban__c in :banIds
									OR
									Account__c in :acctIds)])
			{
				bansWithContract.add(o.VF_Contract__r.Opportunity__r.Ban__c);
			}

			banList = [Select 
								Name,
								Account__r.Id, 
								Account__r.Name,
								BAN_Number__c, 
								BOPCode__c, 
								BOP_export_Errormessage__c,
								OwnerId,
								Corporate_Id__c,
								Unify_ref_Id__c,
								Account__r.GT_Fixed__c,								
								Account__r.IsPartner,
								Account__r.Phone,
								Account__r.Fax,
								Account__r.KVK_Number__c,
								Account__r.Website,
								Account__r.BillingStreet,
								Account__r.BillingPostalCode,
								Account__r.BillingCity,
								Account__r.BillingCountry,
								Account__r.Owner.UserType,
								Account__r.Owner.Name,
								Account__r.Owner.Manager.Name,
								Account__r.Owner.AccountId,						
								Account__r.Fixed_Dealer__r.Contact__r.UserId__r.UserType,
								Account__r.Fixed_Dealer__r.Contact__r.UserId__r.Name,
								Account__r.Fixed_Dealer__r.Contact__r.UserId__r.Manager.Name,
								Account__r.Fixed_Dealer__r.Contact__r.UserId__r.AccountId,
								Account__r.Fixed_Dealer__r.Contact__r.Account.Id,
								Account__r.Fixed_Dealer__r.Contact__r.Account.BAN_Number__c,
								Account__r.Fixed_Dealer__r.Contact__r.Account.BOPCode__c,
								//Account__r.Fixed_Dealer__r.Contact__r.Account.Corporate_Id__c,
								Account__r.Mobile_Dealer__r.Contact__r.UserId__r.UserType,
								Account__r.Mobile_Dealer__r.Contact__r.UserId__r.Name,
								Account__r.Mobile_Dealer__r.Contact__r.UserId__r.Manager.Name,
								Account__r.Mobile_Dealer__r.Contact__r.UserId__r.AccountId,
								Account__r.Mobile_Dealer__r.Contact__r.Account.Id,
								Account__r.Mobile_Dealer__r.Contact__r.Account.BAN_Number__c,
								Account__r.Mobile_Dealer__r.Contact__r.Account.BOPCode__c
								//Account__r.Mobile_Dealer__r.Contact__r.Account.Corporate_Id__c

							from 
								Ban__c 
							Where 
								Owner.Type = 'User'
							AND
								(Account__c in :acctIds OR Id in :banIds)
							AND
								(
									Id in :bansWithContract
									OR	
									BOPCode__c != null
									OR
									BOP_export_Errormessage__c != null
								)
							AND Account__c != :GeneralUtils.unifyOrphansAccount.Id
							AND (Account__c != :GeneralUtils.VodafoneAccount.Id OR BopCode__c = 'TNF')
							
			];
		} else {
			banList = [Select 
								Name,
								Account__r.Id, 
								Account__r.Name,
								BAN_Number__c, 
								BOPCode__c, 
								BOP_export_Errormessage__c,
								OwnerId,
								Corporate_Id__c,
								Unify_ref_Id__c,
								Account__r.GT_Fixed__c,
								Account__r.IsPartner,
								Account__r.Phone,
								Account__r.Fax,
								Account__r.KVK_Number__c,
								Account__r.Website,
								Account__r.BillingStreet,
								Account__r.BillingPostalCode,
								Account__r.BillingCity,
								Account__r.BillingCountry,
								Account__r.Owner.UserType,
								Account__r.Owner.Name,
								Account__r.Owner.Manager.Name,
								Account__r.Owner.AccountId,								
								Account__r.Fixed_Dealer__r.Contact__r.UserId__r.UserType,
								Account__r.Fixed_Dealer__r.Contact__r.UserId__r.Name,
								Account__r.Fixed_Dealer__r.Contact__r.UserId__r.Manager.Name,
								Account__r.Fixed_Dealer__r.Contact__r.UserId__r.AccountId,
								Account__r.Fixed_Dealer__r.Contact__r.Account.Id,
								Account__r.Fixed_Dealer__r.Contact__r.Account.BAN_Number__c,
								Account__r.Fixed_Dealer__r.Contact__r.Account.BOPCode__c,
								//Account__r.Fixed_Dealer__r.Contact__r.Account.Corporate_Id__c,
								Account__r.Mobile_Dealer__r.Contact__r.UserId__r.UserType,
								Account__r.Mobile_Dealer__r.Contact__r.UserId__r.Name,
								Account__r.Mobile_Dealer__r.Contact__r.UserId__r.Manager.Name,
								Account__r.Mobile_Dealer__r.Contact__r.UserId__r.AccountId,
								Account__r.Mobile_Dealer__r.Contact__r.Account.Id,
								Account__r.Mobile_Dealer__r.Contact__r.Account.BAN_Number__c,
								Account__r.Mobile_Dealer__r.Contact__r.Account.BOPCode__c
								//Account__r.Mobile_Dealer__r.Contact__r.Account.Corporate_Id__c								

							from 
								Ban__c 
							Where 
								Owner.Type = 'User'
							AND BAN_Status__c = 'Opened'
							AND
								(Account__c in :acctIds OR Id in :banIds)
							AND Account__c != :GeneralUtils.unifyOrphansAccount.Id
							AND (Account__c != :GeneralUtils.VodafoneAccount.Id OR BopCode__c = 'TNF')								
			];

		}

							//skip parent for now, as we don't know which account is the parent of which ban_info
							//Parent.BAN_Number__c,
							//Parent.Corporate_Id__c,
							//Parent.BOPCode__c,
							//Parent.Id		

		// since we cannot fetch the owner fields directly, we need to create a map for it
		Map<Id,User> banOwnerToUser = new Map<Id,User>();
		for(Ban__c b : banList){
			banOwnerToUser.put(b.OwnerId,null);
		}
		
		for(User u : [Select UserType, Name, Manager.Name, AccountId From User Where Id in :banOwnerToUser.keySet()]){
			banOwnerToUser.put(u.Id,u);
		}
		for(Ban__c banInfo : banList){
			system.debug(banInfo);
			User banOwner = banOwnerToUser.get(banInfo.ownerId);

			// customer accounts handling
			// first do some checks that can't be handled by validation rules
			if(baninfo.BAN_Number__c == null ){
				baninfo.BOP_export_Errormessage__c = 'Valid BAN Number is required for exporting an Account';
				if (!banIDSet.contains(baninfo.id)) {
					bansToUpdate.add(baninfo);	
					banIDSet.add(baninfo.id);			
				}

				continue;
			} else if(!StringUtils.checkBan(baninfo.BAN_Number__c)){
				baninfo.BOP_export_Errormessage__c = 'Valid BAN Number is required for exporting an Account';
				if (!banIDSet.contains(baninfo.id)) {
					bansToUpdate.add(baninfo);	
					banIDSet.add(baninfo.id);			
				}			
				continue;
			} 

			// if bopcode is filled, do update, else do create
			if(baninfo.BOPCode__c != null && baninfo.BOPCode__c != ''){
				updateCustomerAccounts.add(baninfo);
				//accountMap.get('updateCustomer').add(a);
			} else {
				// GC 4-7-2017  No BOP Accounts should be created anymore directly from SFDC. Always should go via SIAS
				// However a flag can be switched to permit creates if needed
				ExportBatchSchedule__c b = ExportBatchSchedule__c.getInstance();
				if (b.createBOPAccount__c) {
					createCustomerAccounts.add(baninfo);
				}
			}
			if(banOwner.AccountId != null){
				// prepare a map for partner accounts. Temporarily put the client account in (to prevent using 2 variables)
				partnerAcctIdToAcct.put(banOwner.AccountId,null);
			}
				
		}
		// create a map of partner account account information
		if(!partnerAcctIdToAcct.isEmpty()){
			for(Account pa : [Select Id, BAN_Number__c, BOPCode__c From Account Where Id in :partnerAcctIdToAcct.keySet()]){
				partnerAcctIdToAcct.put(pa.Id,pa);
			}
		}		
		
		// Generic handling
		if(!accountMap.keySet().isEmpty())
		{
			ECSCompanyService companyService = new ECSCompanyService();
			List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
			Map<String,Id> banToAcctId = new Map<String,Id>();

			Boolean success = false;
		}
		
		
		if(!createCustomerAccounts.isEmpty()){
 			
			ECSCompanyService createService = new ECSCompanyService();
			List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
			Map<String,Id> unifyRefIdToBanId = new Map<String,Id>();
			Set<Id> exportedBanIds = new Set<Id>();
			for(Ban__c b : createCustomerAccounts){
				User banOwner = banOwnerToUser.get(b.ownerId);
				ECSCompanyService.request req = banToRequest(b,partnerAcctIdToAcct,banOwner);
				
				exportedBanIds.add(b.Id);
				if(b.Unify_ref_Id__c != null) unifyRefIdToBanId.put(b.Unify_ref_Id__c,b.Id);
				requests.add(req);
			}
			createService.setRequest(requests);
			
			Boolean success = false;
			try{
				createService.makeRequest('createCustomer');
				success = true;
			} catch (ExWebServiceCalloutException ce){
				for(Ban__c b : createCustomerAccounts){
					// only update the error if it has not been set before
					if(b.BOP_export_Errormessage__c != ce.get255CharMessage()){
						b.BOP_export_Errormessage__c = ce.get255CharMessage();
						if (!banIDSet.contains(b.id)) {
							bansToUpdate.add(b);	
							banIDSet.add(b.id);			
						}						
					}
				}
			}
			
			
			if(success){
				for (ECSCompanyService.response response : createService.getResponse()){
					system.debug(response);
					Ban__c b;
					if(unifyRefIdToBanId.containsKey(response.referenceId)){
						b = new Ban__c(Id = unifyRefIdToBanId.get(response.referenceId));
					} else if(exportedBanIds.contains(response.referenceId)){
						b = new Ban__c(Id = response.referenceId);
					} 
					if(b != null){
						if(response.bopcode != null) b.BOPCode__c = response.bopCode;
						if(response.errorCode == null || response.errorCode == '' || response.errorCode == '0'){
							// success
							// add info to account
							b.BOP_export_datetime__c = system.now();
							b.BOP_export_Errormessage__c = null;
							// startup location and user sync
							newAccounts.add(b.Id);

							
						} else {
							// failure
							// add errormessage to account
							b.BOP_export_Errormessage__c = (response.errorCode+' - '+response.errorMessage).abbreviate(255);
													
							if(response.errorCode == '48' || response.errorCode == '1005'){
								// if error is 'existing customer (48)' then do update instead of insert
								// do this by updating the BOPcode. That will trigger the update process
								b.BOPCode__c = response.bopCode;
								b.BOP_export_datetime__c = system.now();
								// also overwrite the errormessage, since errormsgs starting with a number will not be retried
								b.BOP_export_Errormessage__c = 'awaiting update';
								
								//if(response.ban != null) a.BAN_Number__c = response.ban;
								//if(response.corporateId != null) a.Corporate_Id__c = response.corporateId;
							}
						}
						if (!banIDSet.contains(b.id)) {
							bansToUpdate.add(b);	
							banIDSet.add(b.id);			
						}	
					} else {
						throw new ExMissingDataException('No referenceId returned by BOP');
					}
				}
			}
		}
		if(!updateCustomerAccounts.isEmpty()){
			ECSCompanyService updateService = new ECSCompanyService();
			List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
			//Map<String,Id> unifyRefIdToBanId = new Map<String,Id>();
			//Set<Id> exportedBanIds = new Set<Id>();
			Map<String,ID> bopCodeToBanID = new Map<String,ID>();
			 
			for(Ban__c b : updateCustomerAccounts){
				User banOwner = banOwnerToUser.get(b.ownerId);
				ECSCompanyService.request req = banToRequest(b,partnerAcctIdToAcct,banOwner);

				//exportedBanIds.add(b.Id);
				//if(b.Unify_ref_Id__c != null) unifyRefIdToBanId.put(b.Unify_ref_Id__c,b.Id);
				bopCodeToBanID.put(b.BOPCode__c,b.id);
				requests.add(req);
			}
			updateService.setRequest(requests);

			Boolean success = false;
			try{
				updateService.makeRequest('updateCustomer');
				success = true;
			} catch (ExWebServiceCalloutException ce){
				for(Ban__c b : updateCustomerAccounts){
					// only update the error if it has not been set before
					if(b.BOP_export_Errormessage__c != ce.get255CharMessage()){					
						b.BOP_export_Errormessage__c = ce.get255CharMessage();
						if (!banIDSet.contains(b.id)) {
							bansToUpdate.add(b);	
							banIDSet.add(b.id);			
						}	
					}	
				}
			}
			if(success){
				for (ECSCompanyService.response response : updateService.getResponse()){
					system.debug(response);
					Ban__c b;
					//if(unifyRefIdToBanId.containsKey(response.referenceId)){
					//	b = new Ban__c(Id = unifyRefIdToBanId.get(response.referenceId));
					//} else if(exportedBanIds.contains(response.referenceId)){
					//	b = new Ban__c(Id = response.referenceId);
					//}
					if(bopCodeToBanID.containsKey(response.bopCode)){
						b = new Ban__c(Id = bopCodeToBanID.get(response.bopCode));
						if(response.errorCode == null || response.errorCode == '' || response.errorCode == '0'){
							// success
							// add info to account
							b.BOP_export_datetime__c = system.now();
							b.BOP_export_Errormessage__c = null;
							// startup location and user sync
							newAccounts.add(b.Id); 
						} else {
							// failure
							// add errormessage to account
							b.BOP_export_Errormessage__c = (response.errorCode+' - '+response.errorMessage).abbreviate(255);

						}

						if (!banIDSet.contains(b.id)) {
							bansToUpdate.add(b);	
							banIDSet.add(b.id);			
						}	
					} else {
						throw new ExMissingDataException('No bopCode returned by BOP');
					}
					
				}
			}			
		}

		// allow partial success
			
        list<Database.SaveResult> IR = Database.update(bansToUpdate,false);
        for (Integer i = 0; i < IR.size(); i++) {
            if(!IR[i].isSuccess()){
                System.Debug('Error:'+String.Valueof(IR[i].getErrors()));
            }
        }
		
		// now do the trigger of the related sites and contacts
		if(!newAccounts.isEmpty()){
			SiteExport.scheduleSiteExportFromBanIds(newAccounts);
			ContactExport.scheduleContactExportFromBanIds(newAccounts);			
		}
	}
	
	private static ECSCompanyService.request accountToRequest(Account a,Map<Id,Account> partnerAcctIdToAcct){
		ECSCompanyService.request req = new ECSCompanyService.request();
		req.banNumber = a.BAN_Number__c;
		//req.corporateId = a.Corporate_Id__c;
		req.bopCode = a.BOPCode__c;
		req.referenceId = a.Id;
		req.name = a.Name;
		
        req.phone = a.Phone;
		req.fax = a.Fax;
		req.countryId = null; //not used (yet)
		req.kvkNumber = a.KVK_number__c;
		req.website = a.Website;
		req.billingStreet = a.BillingStreet;
		if(a.BillingPostalCode != null)
			req.billingPostalCode = a.BillingPostalCode.replaceAll(' ','');
		req.billingCity = a.BillingCity;
		req.billingCountry = a.BillingCountry;
		req.dealerCode = a.Dealer_code__c;
	    if(a.Owner.UserType == 'PowerPartner'){
			if(partnerAcctIdToAcct.containsKey(a.owner.AccountId)){
				Account partner = partnerAcctIdToAcct.get(a.owner.AccountId);
				    req.resellerBanNumber = partner.BAN_Number__c; 
				    //req.resellerCorporateId = partner.Corporate_Id__c;
				    req.resellerBopCode = partner.BOPCode__c;
				    req.resellerReferenceId = partner.Id;
			}

			if(a.Owner.Manager.Name != null) {
				req.accountManagerEbu = a.Owner.Manager.Name;
			}

	    } else {
	      req.accountManagerEbu = a.Owner.Name;      
	    }

		if(a.Parent.BAN_Number__c != null || a.parent.BOPCode__c != null){
	        req.parentCompanyBanNumber = a.Parent.BAN_Number__c;
	        //req.parentCompanyCorporateId = a.Parent.Corporate_Id__c;
	        req.parentCompanyBopCode = a.Parent.BOPCode__c;
	        req.parentCompanyReferenceId = a.Parent.Id;
	    }
        
        req.internalInvoicing = null; //not used?
		return req;
	}

	private static ECSCompanyService.request banToRequest(Ban__c b,Map<Id,Account> partnerAcctIdToAcct, User banOwner){
		ECSCompanyService.request req = new ECSCompanyService.request();
		req.banNumber = b.BAN_Number__c;
		req.bopCode = b.BOPCode__c;

		// On advice from BOP this should only pass the salesforce id when creating an account
		// Once the account is created, it is identified in BOP by using the banNumber and bopCode
		//req.referenceId = b.Unify_ref_Id__c!=null?b.Unify_ref_Id__c:b.Id;
		//if (b.BAN_Number__c==null && b.BOPCode__c==null) {		
		if (b.BOPCode__c==null) {
			req.referenceId = b.Id;
		}

		req.name = b.Account__r.Name;
		
        req.phone = b.Account__r.Phone;
		req.fax = b.Account__r.Fax;
		req.countryId = null; //not used (yet)
		req.kvkNumber = b.Account__r.KVK_number__c;
		req.website = b.Account__r.Website;
		req.billingStreet = b.Account__r.BillingStreet;
		if(b.Account__r.BillingPostalCode != null)
			req.billingPostalCode = b.Account__r.BillingPostalCode.replaceAll(' ','');
		req.billingCity = b.Account__r.BillingCity;
		req.billingCountry = b.Account__r.BillingCountry;

		// fill reseller with fixed owner
		Account partner;
		User fixedAccountManagerUser;
		if(b.Account__r.Fixed_Dealer__c != null){
			if(b.Account__r.Fixed_Dealer__r.Contact__r.UserId__r.UserType == 'PowerPartner'){
				partner = b.Account__r.Fixed_Dealer__r.Contact__r.Account;	
				fixedAccountManagerUser = b.Account__r.Fixed_Dealer__r.Contact__r.UserId__r.Manager;
			} else {
				fixedAccountManagerUser = b.Account__r.Fixed_Dealer__r.Contact__r.UserId__r;
			}
		} else if(b.Account__r.Mobile_Dealer__c != null){
			if(b.Account__r.Mobile_Dealer__r.Contact__r.UserId__r.UserType == 'PowerPartner'){
				partner = b.Account__r.Mobile_Dealer__r.Contact__r.Account;
				fixedAccountManagerUser = b.Account__r.Mobile_Dealer__r.Contact__r.UserId__r.Manager;
			} else {
				fixedAccountManagerUser = b.Account__r.Mobile_Dealer__r.Contact__r.UserId__r;
			}
		} else {
			if(b.Account__r.Owner.UserType == 'PowerPartner'){
				partner = partnerAcctIdToAcct.get(b.Account__r.Owner.AccountId);
				fixedAccountManagerUser = b.Account__r.Owner.Manager;	
			} else {
				fixedAccountManagerUser = b.Account__r.Owner;	
			}
		}
			
		if(partner != null){
	       	req.resellerBanNumber = partner.BAN_Number__c; 
	       	req.resellerBopCode = partner.BOPCode__c;
	       	req.resellerReferenceId = partner.Id;
		}
		if(fixedAccountManagerUser != null)
			req.accountManagerEbu = fixedAccountManagerUser.Name;

        req.internalInvoicing = null; //not used?

        // GT Fixed
        if (b.Account__r.GT_Fixed__c) {
        	req.companyGroup='GT Vast';
        }

		return req;
	}

 	/*
 	 *	Description:	This method can be used to schedule the export of BANs for a set of ban ids
 	 */
	public static void scheduleBANExportFromBanIds(Set<Id> banIds){
		// By putting a 'dummy' error in the ban export results, the scheduled export is triggered
		List<Ban__c> bansToUpdate = new List<Ban__c>();

		for(Ban__c b : [Select Id, Account__c From Ban__c Where Id in :banIds]){
			b.BOP_export_Errormessage__c = 'Pending scheduled export..';
			bansToUpdate.add(b);
		}

		if(!bansToUpdate.isEmpty()){
			try{
			 	update bansToUpdate;
			} catch (dmlException e){
				ExceptionHandler.handleException(e);
			}
		}
	}

}