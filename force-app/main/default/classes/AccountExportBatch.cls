/**
 * @description			This is the class that contains logic for running the account export in batches
 * @author				Guy Clairbois
 */
global class AccountExportBatch implements Database.Batchable <sObject>, Database.AllowsCallouts{
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,BOP_export_datetime__c,BOP_Export_ErrorMessage__c FROM Account Where BOP_Export_ErrorMessage__c != null AND isPartner = true AND BOP_Export_Errormessage__c != \'Valid BAN Number is required for exporting an Account\' ');
    } 
 
    global void execute(Database.BatchableContext BC, List<Account> scope){
    	// do Account exporting here
        set<Id> acctIds = new Set<Id>();
        // only process the first 100 records because any more will cause issues (10 accounts per call, 10 calls per transaction)
        Integer size = math.min(scope.size(),100); 

        for(Integer i=0;i<size;i++){
            // only process if 2 first characters are non-numeric (so not coming from BOP)
            if(!scope[i].BOP_Export_Errormessage__c.left(1).isNumeric()) {
        	   acctIds.add(scope[i].Id);
            }
    	}
    	
    	AccountExport.exportAccounts(acctIds);
    }
 
    global void finish(Database.BatchableContext BC){
        ExportUtils.startNextJob('Bans');

    }
 
}