/*
    Controller for AccountMassSharing page and is responsible for changing Queue/User Assigned lookup field on selected Account records.
*/

public with sharing class AccountMassSharingController {

    private ApexPages.StandardSetController con;

    public Queue_User_Sharing__c objQueueUserSharing { get; set; }

    public AccountMassSharingController(ApexPages.StandardSetController controller) {
        this.con = controller;
        this.objQueueUserSharing = new Queue_User_Sharing__c();
    }

    public Pagereference save() {

        System.Savepoint savepoint = Database.setSavepoint();
        try {
            this.objQueueUserSharing.Id = null;

            if(this.objQueueUserSharing.OwnerId == null) {
                return null;
            }

            List<Queue_User_Sharing__c> lstQueueUserSharing = [SELECT Id, OwnerId FROM Queue_User_Sharing__c WHERE
                OwnerId = :this.objQueueUserSharing.OwnerId LIMIT 1];

            if(lstQueueUserSharing.isEmpty()) {
                insert this.objQueueUserSharing;
            } else {
                this.objQueueUserSharing = lstQueueUserSharing[0];
            }

            this.changeUserAssignedLookupField();

            return this.con.cancel();

        } catch(Exception objEx) { return handleException(objEx, savepoint); }
    }

    private void changeUserAssignedLookupField() {
        List<Account> lstAccountToUpdate = (List<Account>) this.con.getSelected();

        for(Account objAccount: lstAccountToUpdate) {
            objAccount.Queue_User_Assigned__c = this.objQueueUserSharing.Id;
        }

        update lstAccountToUpdate;
    }

    @Testvisible
    private Pagereference handleException(Exception objEx, System.Savepoint savepoint) {
        ApexPages.addMessages(objEx);
        Database.rollback(savepoint);
        return null;
    }
}