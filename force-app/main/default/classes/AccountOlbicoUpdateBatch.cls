/**
 * @description			This class is responsible for calling, parsing the Olbico JSON Webservice in bulk
 * @author				Marcel Vreuls
 * @History				Okt 2017: changes for W-000269. Calling new version of API service 
 *						jan 2019: removed email activvation...
 * 
 * @Defaults   			Requires the creation and update of the Olbico Custom settings
 *
 *
 */
global class AccountOlbicoUpdateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

	public List<Account> accountsProcessed = new List<Account>();

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('Select Id, Name, KVK_Number__c From Account Where Trigger_Olbico_Update__c = true AND KVK_Number__c != null limit 200');
	}

	global void execute(Database.BatchableContext BC, List<Account> scope) {
		
		List<Account> accountsToUpsert = new List<Account>();
		OlbicoServiceJSON jService = new OlbicoServiceJSON();
		jService.setBatch();
		
		for(Account acc : scope){
			System.Debug('AccountOlbicoUpdateBatch acc.Name : ' + acc.Name);

			jService.setRequestRestJson(acc.KVK_number__c);
			jService.makeReqestRestJson(acc.KVK_number__c);

			accountsToUpsert.add(jService.getAcc());
			accountsProcessed.add(jService.getAcc());	

		}
			
		try{					
			Database.upsert(accountsToUpsert, Account.Fields.KVK_number__c, false);
	 	} catch (DmlException e) {
	        System.debug(e.getMessage());
	    }
	}

	global void finish(Database.BatchableContext BC) {

		

		//Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							TotalJobItems
						  FROM
							AsyncApexJob
						  WHERE
							Id =: BC.getJobId()];

		// Send an email to the running user notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		String[] toAddresses = new String[] {UserInfo.getUserEmail()};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Account Olbico Update ' + a.Status);
		string Processed = 'Accounts processed: \n';
		for(Account ac : accountsProcessed)
		{
			System.Debug('AccountOlbicoUpdateBatch ac.Name : ' + ac.Name);
			 Processed = Processed + ac.Name  +'\n';
		}
		mail.setPlainTextBody('The Account Olbico Update job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures. ' + Processed );
		// stopped sending mail. to much	
		//Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

		
		ExportUtils.startNextJob('OlbicoUpdate');
    	// cleanup any remaining jobs
		/*for(CronTrigger ct : [SELECT Id, State, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType 
		    					FROM CronTrigger 
		    					WHERE State = 'DELETED' 
		    					AND CronJobDetail.JobType = '7'  
		    					AND cronjobdetail.name LIKE 'Account Olbico update%']){
			System.abortJob(ct.Id);
		} */ 

	}
}