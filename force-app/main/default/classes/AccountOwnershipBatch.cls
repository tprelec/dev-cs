/**
 * @description			This is the class that contains logic for running the account ownership calculation in batches
 * @author				Guy Clairbois
 */
global class AccountOwnershipBatch implements Database.Batchable <sObject>, Database.AllowsCallouts{

/*		USE THIS CODE TO SCHEDULE THE BATCH JOB:
 			AccountOwnershipBatch aoBatch = new AccountOwnershipBatch();
 			Id batchprocessId = Database.executeBatch(aoBatch,5);
 
 */
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,Name,Mobile_Dealer__c From Account');
    } 
 
    global void execute(Database.BatchableContext BC, List<Account> scope){
    	// collect account id's in scope
    	Set<Id> accountIds = new Set<Id>();
    	for(Account a : scope){
    		accountIds.add(a.Id);
    	}

    	Id rtId = GeneralUtils.recordTypeMap.get('VF_Asset__c').get('CTN');

    	// collect assets (summed by dealercode)
    	// create 3 separate maps to keep the code readable
    	Map<Id,Map<String,AggregateResult>> accountIdToDealerCodeToMobileVoiceAssetCount = new Map<Id,Map<String,AggregateResult>>();
    	Map<Id,Map<String,AggregateResult>> accountIdToDealerCodeToMobileDataAssetCount = new Map<Id,Map<String,AggregateResult>>();
    	Map<Id,Map<String,AggregateResult>> accountIdToDealerCodeToM2MAssetCount = new Map<Id,Map<String,AggregateResult>>();

    	for(AggregateResult ar : [SELECT 
    								Account__c, 
    								Dealer_Code__c, 
    								ProductMatrix__c,
    								Max(Contract_Start_Date__c) maxDate, 
    								Count(Id) counter 
    							FROM 
    								VF_Asset__c 
    							WHERE 
    								Account__c in: accountIds 
    							AND
    								CTN_Status__c = 'Active'
    							AND
    								Dealer_Code__c != null
    							AND 
    								ProductMatrix__c in ('VOICE','DATA','M2M')
    							//AND
    								//RecordTypeId = :rtId		
    							GROUP BY 
    								Account__c,Dealer_Code__c, ProductMatrix__c ])
    	{
			Id accountId = (Id) ar.get('Account__c');
			String dealerCode = ((String) ar.get('Dealer_Code__c')).removeStart('00');
			String ProductMatrix = (String) ar.get('ProductMatrix__c');

			if(ProductMatrix == 'VOICE'){
				if(!accountIdToDealerCodeToMobileVoiceAssetCount.containsKey(accountId)){
					Map<String,AggregateResult> tempMap = new Map<String,AggregateResult>();
					tempMap.put(dealerCode,ar);
					accountIdToDealerCodeToMobileVoiceAssetCount.put(accountId,tempMap);
				} else {
					accountIdToDealerCodeToMobileVoiceAssetCount.get(accountId).put(dealerCode,ar);
				}
			} else if(ProductMatrix == 'DATA'){
				if(!accountIdToDealerCodeToMobileDataAssetCount.containsKey(accountId)){
					Map<String,AggregateResult> tempMap = new Map<String,AggregateResult>();
					tempMap.put(dealerCode,ar);
					accountIdToDealerCodeToMobileDataAssetCount.put(accountId,tempMap);
				} else {
					accountIdToDealerCodeToMobileDataAssetCount.get(accountId).put(dealerCode,ar);
				}
			} else if(ProductMatrix == 'M2M'){
				if(!accountIdToDealerCodeToM2MAssetCount.containsKey(accountId)){
					Map<String,AggregateResult> tempMap = new Map<String,AggregateResult>();
					tempMap.put(dealerCode,ar);
					accountIdToDealerCodeToM2MAssetCount.put(accountId,tempMap);
				} else {
					accountIdToDealerCodeToM2MAssetCount.get(accountId).put(dealerCode,ar);
				}
			}
		}
		system.debug(accountIdToDealerCodeToMobileVoiceAssetCount);

		Map<Id,String> accountIdToWinningDealerCode = new Map<Id,String>();
		Map<String,Id> dealerCodeToId = new Map<String,Id>();

    	// decide which one has most
    	for(Id aId : accountIds){
    		String winningDealerCode;
	    	// first count mobile voice
	    	if(accountIdToDealerCodeToMobileVoiceAssetCount.containsKey(aId)){
	    		winningDealerCode = calculateWinningDealer(accountIdToDealerCodeToMobileVoiceAssetCount.get(aId));
	    	// if no mobile voice then count mobile data	
		    } else if(accountIdToDealerCodeToMobileDataAssetCount.containsKey(aId)){
	    		winningDealerCode = calculateWinningDealer(accountIdToDealerCodeToMobileDataAssetCount.get(aId));
			// if no mobile data then count m2m
		    } else if(accountIdToDealerCodeToM2MAssetCount.containsKey(aId)){
	    		winningDealerCode = calculateWinningDealer(accountIdToDealerCodeToM2MAssetCount.get(aId));
		    }
		    accountIdToWinningDealerCode.put(aId,winningDealerCode);
		}

    	system.debug(accountIdToWinningDealerCode);

    	//system.debug(dealerCodeToId);

    	// derive owner from dealercode
    	for(Dealer_Information__c di : [Select Id,Name,Dealer_Code__c From Dealer_Information__c Where Dealer_Code__c in :accountIdToWinningDealerCode.values()]){
    		dealerCodeToId.put(di.Dealer_Code__c,di.Id);
    	}

    	// update (mobile) owner on Account
    	List<Account> accountsToUpdate = new List<Account>();
    	for(Account a : scope){
    		if(accountIdToWinningDealerCode.containskey(a.Id)){
    			String maxDealerCode = accountIdToWinningDealerCode.get(a.Id);//.removeStart('00');
    			
    			if(maxDealerCode != null && dealerCodeToId.containsKey(maxDealerCode)){

    				if(a.Mobile_Dealer__c != dealerCodeToId.get(maxDealerCode)){
    					a.Mobile_Dealer__c = dealerCodeToId.get(maxDealerCode);
    					accountsToUpdate.add(a);
    				}	
    			}
    			
    		}
    	}
    	update accountsToUpdate;
    }
 
    global void finish(Database.BatchableContext BC){

    	// send email?

    }

    global String calculateWinningDealer(Map<String,AggregateResult> dealerCodeToAssetCount){
    	//Map<Id,String> accountIdToWinningDealerCode = new Map<Id,String>();
    	String winningDealerCode;

	   // for(Id accountId : accountIdToDealerCodeToAssetCount.keySet()){
    		// define max for each account
    		Integer max = 0;
    		//String maxDealerCode;
    		for(String dealerCode : dealerCodeToAssetCount.keySet()){
    			if(max == 0 || (Integer) dealerCodeToAssetCount.get(dealerCode).get('counter') > max) {
    				//maxDealerCode = dealercode;
    				max = (Integer) dealerCodeToAssetCount.get(dealerCode).get('counter');
    			} 
    		}
    		// remove all dealers that are not the max
    		for(String dealerCode : dealerCodeToAssetCount.keySet()){
    			if((Integer) dealerCodeToAssetCount.get(dealerCode).get('counter') < max) {
    				dealerCodeToAssetCount.remove(dealerCode);
    			} 
    		}
    		// check if there are any draws 
    		if(dealerCodeToAssetCount.values().size() > 1){
    			Date maxDate;
    			String maxDealerCode;
    			for(String dealerCode : dealerCodeToAssetCount.keySet()){
    				if(maxDate == null || (Date) dealerCodeToAssetCount.get(dealerCode).get('maxDate') > maxDate){
    					maxDate = (Date) dealerCodeToAssetCount.get(dealerCode).get('maxDate');
    					maxDealerCode = dealerCode;
    				}
    			} 
		    	// in case of draw, latest contracted wins
    			for(String dealerCode : dealerCodeToAssetCount.keySet()){
    				if(dealerCode != maxDealerCode){
    					dealerCodeToAssetCount.remove(dealerCode);		
    				}
    			}   
    			
    		}
    		// collect dealercodes and add winning dealercode to resultmap
   			for(String dealerCode : dealerCodeToAssetCount.keySet()){
	    		//dealerCodeToId.put(dealerCode.removeStart('00'),null); 			
	    		//dealerCodeToId.put(dealerCode,null); 			
	    		winningDealerCode = dealerCode;
	    	}
    	//}   	

    	return winningDealerCode;
    }
 
}