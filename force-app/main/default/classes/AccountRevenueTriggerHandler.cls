/**
 * @description         This is the trigger handler for the AccountRevenue
 */
public with sharing class AccountRevenueTriggerHandler extends TriggerHandler {

	/**
	 * @description			This handles the before insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void BeforeInsert(){
		List<Account_Revenue__c> newAccountRevenues = (List<Account_Revenue__c>) this.newList;
		setOwnerAndAccount(newAccountRevenues);
	}

	/**
	 * @description         This handles the after insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void AfterInsert(){
	}

	/**
	 * @description         This fills some fields on AccountRevenue based on the Account link.
	 */
	private void setOwnerAndAccount(List<Account_Revenue__c> newAccountRevenues){
		// when creating a new Account Revenue, link to Account based on BAN Number and fill Owner with Account Owner
		//Set<String> bans = new Set<String>();
		//Set<String> bops = new Set<String>();
		Set<String> acctIds = new Set<String>();
		//Map<String,Account> banToAcct = new Map<String,Account>();
		//Map<String,Account> bopToAcct = new Map<String,Account>();
		Map<String,Account> idToAcct = new Map<String,Account>();

		for(Account_Revenue__c ar : newAccountRevenues){
			if(ar.Account__c == null){
				ar.addError('AccountId needs to be provided in Account Revenue upload.');
			} else {
				//if(ar.BOP_Code__c != null) bops.add(ar.BOP_Code__c);
				//if(ar.AR_Billing_Account_Num__c != null) bans.add(ar.AR_Billing_Account_Num__c);
				if(ar.Account__c != null) acctIds.add(ar.Account__c);
			}
		}

		if(!acctIds.isEmpty()){
			// first retrieve the default 'Vodafone' owner for is no active account owner is found
			User vf = [Select Id From User Where Name = 'Vodafone' AND IsActive = true]; //TODO: move this to a custom setting

			for(Account a : [Select Id, OwnerId, Owner.IsActive From Account Where Id in :acctIds]){
				//if(a.BAN_Number__c != null) banToAcct.put(a.BAN_Number__c,a);
				//if(a.BOPCode__c != null) bopToAcct.put(a.BOPCode__c,a);
				if(a.Id != null) idToAcct.put(a.Id,a);
			}
			for(Account_Revenue__c ar : newAccountRevenues){
				system.debug(ar);
				if(idToAcct.containsKey(ar.Account__c)){
					if(idToAcct.get(ar.Account__c).Owner.IsActive){ 
						ar.OwnerId = idToAcct.get(ar.Account__c).OwnerId;
					} else {
						ar.OwnerId = vf.Id;
					}
				}
				system.debug(ar);
			}
		}
	}
}