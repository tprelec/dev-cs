public inherited sharing class AccountService {
    private static final Map<FrameworkAgreementType, String> FWA_TYPE_FIELD_MAP = new Map<FrameworkAgreementType, String>{
        FrameworkAgreementType.VODAFONE => 'Frame_Work_Agreement__c',
        FrameworkAgreementType.VODAFONE_ZIGGO => 'VZ_Framework_Agreement__c'
    };
    private static final Map<FrameworkAgreementType, String> FWA_TYPE_DATE_FIELD_MAP = new Map<FrameworkAgreementType, String>{
        FrameworkAgreementType.VODAFONE => 'Framework_agreement_date__c',
        FrameworkAgreementType.VODAFONE_ZIGGO => 'VZ_Framework_agreement_date__c'
    };
    private static final Map<FrameworkAgreementType, String> FWA_TYPE_CREATE_FWA_FIELD_MAP = new Map<FrameworkAgreementType, String>{
        FrameworkAgreementType.VODAFONE => 'Create_Framework_Agreement__c',
        FrameworkAgreementType.VODAFONE_ZIGGO => 'Create_VZ_Framework_Agreement__c'
    };

    private Framework__c frameworkSetting;

    @TestVisible
    private static AccountService instance;

    public static AccountService getInstance() {
        if (instance == null) {
            instance = new AccountService();
        }
        return instance;
    }

    public void generateFrameworkAgreementIdAndInvokeFrameworkAgreementCreation(
        Set<Id> accountIdsWithNewFWA,
        Set<Id> accountIdsWithNewVZFWA
    ) {
        Set<Id> accIds = new Set<Id>();
        accIds.addAll(accountIdsWithNewFWA);
        accIds.addAll(accountIdsWithNewVZFWA);
        Map<Id, Account> accountsMap = new Map<Id, Account>(
            [
            SELECT
                Id,
                Active_Ban_Count__c,
                Frame_Work_Agreement__c,
                Framework_agreement_date__c,
                VZ_Framework_Agreement__c,
                VZ_Framework_agreement_date__c
                FROM Account
                WHERE Id IN :accIds
            ]
        );
        // Create FWA requests and get IDs
        List<FrameworkAgreement> agreements = createFrameworkAgreementRequests(
            accountsMap.values(),
            accountIdsWithNewFWA,
            accountIdsWithNewVZFWA
        );
        agreements = generateFrameworkIds(agreements);
        Map<Id, Account> accountsToUpdate = new Map<Id, Account>();

            for (FrameworkAgreement agreement : agreements) {
            Account a = accountsMap.get(agreement.accountId);
            a.put(FWA_TYPE_FIELD_MAP.get(agreement.agreementType), agreement.agreementId);
            if (a.get(FWA_TYPE_DATE_FIELD_MAP.get(agreement.agreementType)) == null) {
                a.put(FWA_TYPE_CREATE_FWA_FIELD_MAP.get(agreement.agreementType), true);
                    }
            accountsToUpdate.put(a.Id, a);
                }
        if (!accountsToUpdate.isEmpty()) {
            update accountsToUpdate.values();
            }
            }

    // Create Framework Agreement 'Requests'
    private List<FrameworkAgreement> createFrameworkAgreementRequests(
        List<Account> accounts,
        Set<Id> accountIdsWithNewFWA,
        Set<Id> accountIdsWithNewVZFWA
    ) {
        List<FrameworkAgreement> agreements = new List<FrameworkAgreement>();
        for (Account a : accounts) {
            // The check on Active_Ban_Count__c is only for old 'Vodafone' framework agreements
            if (
                accountIdsWithNewFWA.contains(a.Id) &&
                a.Frame_Work_Agreement__c == null &&
                (a.Active_Ban_Count__c == null ||
                a.Active_Ban_Count__c < 1)
            ) {
                agreements.add(new FrameworkAgreement(a.Id, FrameworkAgreementType.VODAFONE, null));
            }
            if (accountIdsWithNewVZFWA.contains(a.Id) && a.VZ_Framework_Agreement__c == null) {
                agreements.add(
                    new FrameworkAgreement(a.Id, FrameworkAgreementType.VODAFONE_ZIGGO, null)
                );
        }
        }
        return agreements;
    }

    // Generates Framework Agreement IDs. Using a custom setting to generate the number rather than using an autonumber
    // because legal can manually enter framework id's which may later cause a duplicate. Checks for a duplicate before saving
    public List<FrameworkAgreement> generateFrameworkIds(List<FrameworkAgreement> requests) {
        Map<String, FrameworkAgreement> agreements = new Map<String, FrameworkAgreement>();
        for (FrameworkAgreement request : requests) {
            request.agreementId = getNextFrameworkId(request.agreementType);
            agreements.put(request.agreementId, request);
        }
        // Verify if there are already Accounts with these Agreements
        List<Account> existingAccounts = [
            SELECT Frame_Work_Agreement__c
            FROM Account
            WHERE Frame_Work_Agreement__c IN :agreements.keySet()
        ];
        Set<String> existingIds = GeneralUtils.getStringSetFromList(
            existingAccounts,
            'Frame_Work_Agreement__c'
        );
        List<FrameworkAgreement> result = new List<FrameworkAgreement>();
        List<FrameworkAgreement> getNewIds = new List<FrameworkAgreement>();
        for (FrameworkAgreement agreement : agreements.values()) {
            if (existingIds.contains(agreement.agreementId)) {
                getNewIds.add(agreement);
            } else {
                result.add(agreement);
            }
        }
        // If all agreements have received ID, update setting and retur, otherwise get additional IDs
        if (getNewIds.isEmpty()) {
            if (frameworkSetting != null) {
                update frameworkSetting;
            }
            return result;
        } else {
            result.addAll(generateFrameworkIds(getNewIds));
            return result;
        }
    }

    public FrameworkAgreement generateFrameworkId(FrameworkAgreement request) {
        List<FrameworkAgreement> requests = new List<FrameworkAgreement>{request};
        return generateFrameworkIds(requests)[0];
    }

    public String generateFrameworkId(FrameworkAgreementType agreementType) {
        return generateFrameworkId(new FrameworkAgreement(null, agreementType, null)).agreementId;
    }

    private String getNextFrameworkId(FrameworkAgreementType agreementType) {
        // Get setting if not read already
        if (frameworkSetting == null) {
            frameworkSetting = Framework__c.getOrgDefaults();
        }
        // Build ID
        Integer num;
        String prefix;
        if (agreementType == FrameworkAgreementType.VODAFONE) {
            num = Integer.valueOf(++frameworkSetting.Framework_Sequence_Number__c);
            prefix = 'VFZA';
        }
        if (agreementType == FrameworkAgreementType.VODAFONE_ZIGGO) {
            num = Integer.valueOf(++frameworkSetting.VodafoneZiggo_Framework_Sequence_Number__c);
            prefix = 'VZ';
        }
        return prefix + '-' + System.today().year() + '-' + String.valueof(num).leftPad(5, '0');
    }

    public void updateFrameworkAgreementDateAndVersionForNewContract(
        Map<Id, Date> accountToChangedSignedDate
    ) {
        List<Account> accountsWithNewFWA = [
            SELECT
                Framework_agreement_date__c,
                VZ_Framework_Agreement_Date__c,
                Create_Framework_Agreement__c,
                Create_VZ_Framework_Agreement__c,
                Version_FWA__c,
                VZ_Framework_Agreement_Version__c
            FROM Account
            WHERE
                Id IN :accountToChangedSignedDate.keySet()
                AND (Create_Framework_Agreement__c = TRUE
                OR Create_VZ_Framework_Agreement__c = TRUE)
        ];

        if (accountsWithNewFWA.isEmpty()) {
            return;
        }

        for (Account a : accountsWithNewFWA) {
            if (a.Create_Framework_Agreement__c) {
                a.Framework_agreement_date__c = accountToChangedSignedDate.get(a.id);
                a.Version_FWA__c = 1;
                a.Create_Framework_Agreement__c = false;
            }
            if (a.Create_VZ_Framework_Agreement__c == true) {
                a.VZ_Framework_Agreement_Date__c = accountToChangedSignedDate.get(a.id);
                a.VZ_Framework_Agreement_Version__c = 1;
                a.Create_VZ_Framework_Agreement__c = false;
            }
        }
        update accountsWithNewFWA;
    }

    public void setExpectedDeliveryDateForVoiceAndVoiceDataAssets(Set<Id> accountIds) {
        if (accountIds == null || accountIds.isEmpty()) {
            return;
        }
        List<VF_Asset__c> voiceDataAssets = [
            SELECT Id, Account__c, PricePlan_Class__c, Contract_End_Date__c
            FROM VF_Asset__c
            WHERE
                Account__c = :accountIds
                AND CTN_Status__c = 'Active'
                AND Contract_End_Date__c != NULL
                AND (PricePlan_Class__c = 'Mobile Voice'
                OR PricePlan_Class__c = 'Data Only')
            ORDER BY Contract_End_Date__c ASC
        ];

        List<VF_Asset__c> voiceAssets = new List<VF_Asset__c>();
        for (VF_Asset__c asset : voiceDataAssets) {
            if (asset.PricePlan_Class__c == 'Mobile Voice') {
                voiceAssets.add(asset);
            }
        }

        Map<Id, Date> accountsToExpectedDeliveryDateForVoiceDataAssets = getExpectedDeliveryDateByAccountId(
            voiceDataAssets
        );
        Map<Id, Date> accountsToExpectedDeliveryDateForVoiceAssets = getExpectedDeliveryDateByAccountId(
            voiceAssets
        );

        Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
        for (Id accountId : accountsToExpectedDeliveryDateForVoiceDataAssets.keySet()) {
            accountsToUpdate.put(
                accountId,
                new Account(
                    Id = accountId,
                    Expected_Delivery_Date_Voice_Data__c = accountsToExpectedDeliveryDateForVoiceDataAssets.get(
                        accountId
                    )
                )
            );
        }
        for (Id accountId : accountsToExpectedDeliveryDateForVoiceAssets.keySet()) {
            Account account = accountsToUpdate.get(accountId);
            account.Expected_Delivery_Date_Voice__c = accountsToExpectedDeliveryDateForVoiceAssets.get(
                accountId
            );
            accountsToUpdate.put(accountId, account);
        }
        if (!accountsToUpdate.isEmpty()) {
            update accountsToUpdate.values();
        }
    }

    private Map<Id, Date> getExpectedDeliveryDateByAccountId(List<VF_Asset__c> inputAssets) {
        Map<Id, Date> ret = new Map<Id, Date>();
        Map<Id, List<VF_Asset__c>> accountsToAssets = new Map<Id, List<VF_Asset__c>>();
        for (VF_Asset__c asset : inputAssets) {
            if (asset.Account__c == null) {
                continue;
            }
            if (!accountsToAssets.containsKey(asset.Account__c)) {
                accountsToAssets.put(asset.Account__c, new List<VF_Asset__c>{asset});
                continue;
            }
            List<VF_Asset__c> assetsByAccount = accountsToAssets.get(asset.Account__c);
            assetsByAccount.add(asset);
            accountsToAssets.put(asset.Account__c, assetsByAccount);
        }

        // ExpectedDeliveryDate is when 70% of the Account's Assets are End of Contract. The input parameter is
        // sorted ascending on Contract_End_Date__c.
        for (Id accountId : accountsToAssets.keyset()) {
            List<VF_Asset__c> assets = accountsToAssets.get(accountId);
            Integer indexOfFirstAssetAfter80Percent = Math.Round(assets.size() * 0.70) - 1;
            ret.put(accountId, assets[indexOfFirstAssetAfter80Percent].Contract_End_Date__c);
        }

        return ret;
    }

    public class FrameworkAgreement {
        public Id accountId;
        public FrameworkAgreementType agreementType;
        public String agreementId;
        public FrameworkAgreement(
            Id accountId,
            FrameworkAgreementType agreementType,
            String agreementId
        ) {
            this.accountId = accountId;
            this.agreementType = agreementType;
            this.agreementId = agreementId;
        }
    }

    public enum FrameworkAgreementType {
        VODAFONE,
        VODAFONE_ZIGGO
    }

    // Updates Accounts from Prospect to Customer
    public void updateAccountsToCustomer(Set<Id> accountIds) {
        if (accountIds.isEmpty()) {
            return;
        }
        List<Account> accountsToUpdate = new List<Account>();
        for (Account a : [
            SELECT Id, Type
            FROM Account
            WHERE Id IN :accountIds AND Type = 'Prospect' AND IsPartner = FALSE
        ]) {
            a.Type = 'Customer';
            accountsToUpdate.add(a);
        }
        if (!accountsToUpdate.isEmpty()) {
            update accountsToUpdate;
        }
    }
}