global class AccountServicesInvalidationPlugin extends csutil.PluginManager.ABasePlugin {
    
    @TestVisible
    global override Object invoke (Object parameter) {
        
        List<String> relevantASPAttributeNames = new List<String>{'ReferentQuantity','Category','Product'};
        List<String> relevantConfigNames = new List<String>{'Account Services','Account Services Product'};
        Map<Id, List<cscfga__Attribute__c>> configAttrConnection = new Map<Id, List<cscfga__Attribute__c>>();
        Boolean changed = false;
        List<cscfga__Attribute__c> attrToUpdate = new List<cscfga__Attribute__c>();

        List<cscfga__Product_Configuration__c> savedConfig = [SELECT Id, Name, cscfga__Product_Basket__c FROM cscfga__Product_Configuration__c WHERE Id = :(Id)parameter];
        
        if (savedConfig != null) {
            cscfga__Product_Basket__c basket = [SELECT Id, Number_Of_CTN_SUM_Voice__c, Number_Of_CTN_SUM_Data__c FROM cscfga__Product_Basket__c WHERE Id = : savedConfig[0].cscfga__Product_Basket__c];
            
            Id basketId = basket.Id;
            if (basket != null) {
                Map<Id,cscfga__Product_Configuration__c> relevantConfigs = new Map<Id,cscfga__Product_Configuration__c> ([SELECT Id, Name, cscfga__Product_Definition__r.Name, cscfga__Product_Basket__c, cscfga__Configuration_Status__c  
                                                                            FROM cscfga__Product_Configuration__c 
                                                                            WHERE cscfga__Product_Basket__c = : basket.Id and cscfga__Product_Definition__r.Name in : relevantConfigNames]);
                                
                List<cscfga__Attribute__c> relevantAttributes = [SELECT Id, Name, cscfga__Product_Configuration__c, cscfga__Value__c 
                                                FROM cscfga__Attribute__c
                                                WHERE cscfga__Product_Configuration__c in: relevantConfigs.keyset() AND Name in: relevantASPAttributeNames ];
                
                for (cscfga__Product_Configuration__c config :relevantConfigs.values())
                {
                    List<cscfga__Attribute__c> tempAttrList = new List<cscfga__Attribute__c>();
                    configAttrConnection.put(config.Id, tempAttrList);
                    for (cscfga__Attribute__c attr : relevantAttributes)
                    {
                        if (attr.cscfga__Product_Configuration__c == config.Id)
                        {
                            List<cscfga__Attribute__c> tempAttrListForMap = new List<cscfga__Attribute__c>();
                            tempAttrListForMap = configAttrConnection.get(config.Id);
                            tempAttrListForMap.add(attr);
                            configAttrConnection.put(config.Id,tempAttrListForMap );
                        }
                    }
                }
                
                for (List<cscfga__Attribute__c> attrList: configAttrConnection.values() )
                {
                    Integer quantity;
                    String category;
                    
                    for (cscfga__Attribute__c attr: attrList)
                    {
                        if (attr.Name == 'Category')
                        {
                            category = attr.cscfga__Value__c;
                        }
                        else if (attr.Name == 'ReferentQuantity')
                        {
                            quantity = Integer.valueOf(attr.cscfga__Value__c);
                        }
                    }
                    
                    switch on category{
                        when 'VPN'
                        {
                            if (quantity != basket.Number_Of_CTN_SUM_Voice__c)
                            {
                                changed = true;
                            }
                        }
                        when 'Choose Your Own Device'
                        {
                            if (quantity != basket.Number_Of_CTN_SUM_Voice__c)
                            {
                                changed = true;
                            }
                        }
                        when 'Managed Services'
                        {
                            if (quantity != (basket.Number_Of_CTN_SUM_Voice__c + basket.Number_Of_CTN_SUM_Data__c))
                            {
                                changed = true;
                            }
                        }
                        when 'Managed Workplace'
                        {
                            if (quantity != (basket.Number_Of_CTN_SUM_Voice__c + basket.Number_Of_CTN_SUM_Data__c))
                            {
                                changed = true;
                            }
                        }
                        when 'Expense Management'
                        {
                            if (quantity != (basket.Number_Of_CTN_SUM_Voice__c + basket.Number_Of_CTN_SUM_Data__c))
                            {
                                changed = true;
                            }
                        }
                        
                    }
                    
                    if (changed == true)
                    {
                        for (cscfga__Attribute__c attr: attrList)
                        {
                            if (attr.Name == 'Product')
                            {
                                attr.cscfga__Value__c = null;
                                attrToUpdate.add(attr);
                            }
                        }
                        
                    }
                    
                }
                
                if (changed == true)
                {
                    
                    List<cscfga__Product_Configuration__c> ConfigsToUpdateList = new List<cscfga__Product_Configuration__c>();
                    configsToUpdateList.addAll(relevantConfigs.values());
                    for (cscfga__Product_Configuration__c config :configsToUpdateList)
                    {
                        if (config.Name == 'Account Services')
                        {
                            config.cscfga__Configuration_Status__c = 'Requires Update';
                        }
                    }
                    try {
                        update attrToUpdate;
                        update (configsToUpdateList);
                        System.debug('ASIP update');
                    }
                    catch(DmlException e) {
                        System.debug('The following exception has occurred: ' + e.getMessage());
                    }

                }
            }
        }
        return '';
    }
}