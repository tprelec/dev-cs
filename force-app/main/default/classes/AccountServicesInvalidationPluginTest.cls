@isTest
public with sharing class AccountServicesInvalidationPluginTest {
    @TestSetup
  private static void testSetup()
  {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            PriceReset__c priceResetSetting = new PriceReset__c();
            
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
            
            insert priceResetSetting;

            Account testAccount = CS_DataTest.createAccount('Test Account 1');
            Account testAccount2 = CS_DataTest.createAccount('Test Account 2');
            List<Account> accList = new List<Account>{testAccount,testAccount2};
            insert accList;
            
            Contact contact1 = new Contact(
                AccountId = testAccount.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'   
            );
            insert contact1;
            
            testAccount.Authorized_to_sign_1st__c = contact1.Id;
            testAccount.Contract_rule_no_mailing__c = true;
            testAccount.Frame_Work_Agreement__c = 'VFZA-2018-8';
            testAccount.Version_FWA__c = 4;
            testAccount.Framework_agreement_date__c = Date.today();
            update testAccount;
            
            Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Contract opportunity',simpleUser.id);
            Opportunity opp2 = new Opportunity();
            opp2.Name = 'testName';
            opp2.Account = testAccount2;
            opp2.StageName= 'Qualification';
            opp2.CloseDate = Date.Today()+10;
            
            List<Opportunity> oppList = new List<Opportunity>{opp, opp2};
            insert oppList;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Contract Basket');
            basket.Name = 'Contract Basket';
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = testAccount.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 24;
            basket.OneNet_Scenario__c = 'One Net Enterprise';
            basket.Fixed_Scenario__c = 'One Fixed Enterprise';
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 02, 02);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 02, 02);
            basket.Number_of_SIP__c = 12;
            basket.Approved_Date__c = Date.today() - 4;
            basket.Number_Of_CTN_SUM_Voice__c = 15;
            basket.Number_Of_CTN_SUM_Data__c = 20;

            insert basket;
            
            cscfga__Product_Definition__c accServDef = CS_DataTest.createProductDefinition('Account Services');
            accServDef.Product_Type__c = 'Fixed';

            cscfga__Product_Definition__c accServProdDef = CS_DataTest.createProductDefinition('Account Services Product');
            accServProdDef.Product_Type__c = 'Fixed';
            
            List<cscfga__Product_Definition__c> prodDefList = new List<cscfga__Product_Definition__c>{accServDef,accServProdDef};
            insert prodDefList;
            
            Site__c testSite = CS_DataTest.createSite('testSite', testAccount, '1234AA', 'teststreet', 'testTown', 3);
            insert testSite;
            
            Site_Availability__c testSA = new Site_Availability__c();
            testSA.Name = 'testSA';
            testSA.Site__c = testSite.Id;
            testSA.Vendor__c = 'ZIGGO'; 
            testSA.Premium_Vendor__c = 'TELECITY'; 
            testSA.Access_Infrastructure__c ='Coax'; 
            testSA.Bandwith_Down_Entry__c = 1; 
            testSA.Bandwith_Up_Entry__c = 10; 
            testSA.Bandwith_Down_Premium__c = 1; 
            testSA.Bandwith_Up_Premium__c = 10;
            insert testSA;
            
            PBX_Type__c PBXType = new PBX_Type__c();
            PBXType.Name = 'Test PBX Type';
            PBXType.Software_version__c = '3.5';
            PBXType.Vendor__c = 'ZIGGO';
            PBXType.Product_Name__c = 'test';
            insert PBXType;

            cscfga__Product_Configuration__c accServConf = CS_DataTest.createProductConfiguration(accServDef.Id, 'Account Services',basket.Id);
            accServConf.Site_Availability_Id__c = testSA.Id;
            accServConf.cscfga__Contract_Term__c = 24;
            accServConf.cscfga__total_one_off_charge__c = 100;
            accServConf.cscfga__Total_Price__c = 20;
            accServConf.cspl__Type__c = 'Mobile Voice Services';
            accServConf.cscfga__Contract_Term_Period__c = 12;
            accServConf.cscfga__Configuration_Status__c = 'Valid';
            accServConf.cscfga__Quantity__c = 1;
            accServConf.cscfga__total_recurring_charge__c = 22;
            accServConf.RC_Cost__c = 0;
            accServConf.NRC_Cost__c = 0;
            accServConf.OneNet_Scenario__c = 'One Fixed';
            accServConf.Deal_Type__c = 'Acquisition';
            accServConf.cscfga__one_off_charge_product_discount_value__c = 0;
            accServConf.cscfga__recurring_charge_product_discount_value__c = 0;
            accServConf.Mobile_Scenario__c = 'test';
            accServConf.cscfga__discounts__c = '{"discounts":[{"type":"absolute","source":"Negotiation","discountCharge":"oneOff","description":"22","amount":208},{"type":"absolute","source":"Negotiation","discountCharge":"recurring","description":"22","amount":23}]}';
            accServConf.OneFixed__c = 'test';
            accServConf.Fixed_Scenario__c = 'test';
            accServConf.IPVPN__c = 'a7Z2p0000000000000';
            accServConf.cscfga__Product_Family__c = 'Access Infrastructure';
            accServConf.PBXId__c = PBXType.Id;
    
            cscfga__Product_Configuration__c AccServProdConf = CS_DataTest.createProductConfiguration(accServProdDef.Id, 'Account Services Product',basket.Id);
            AccServProdConf.Deal_Type__c = 'Acquisition';
            AccServProdConf.cscfga__Contract_Term__c = 24;
            AccServProdConf.cscfga__total_one_off_charge__c = 100;
            AccServProdConf.cscfga__Total_Price__c = 20;
            AccServProdConf.Number_of_SIP__c = 20;
            AccServProdConf.cscfga__Parent_Configuration__c = accServConf.Id;
            AccServProdConf.Number_of_SIP_Clone_Multiplicator__c = 2;
            AccServProdConf.cscfga__Contract_Term_Period__c = 12;
            AccServProdConf.cscfga__Configuration_Status__c = 'Valid';
            AccServProdConf.cscfga__Quantity__c = 1;
            AccServProdConf.cscfga__total_recurring_charge__c = 22;
            AccServProdConf.RC_Cost__c = 0;
            AccServProdConf.NRC_Cost__c = 0;
            AccServProdConf.cscfga__one_off_charge_product_discount_value__c = 0;
            AccServProdConf.cscfga__recurring_charge_product_discount_value__c = 0;
            AccServProdConf.cscfga__discounts__c = '{"discounts":[{"type":"absolute","source":"Negotiation","discountCharge":"oneOff","description":"22","amount":208},{"type":"absolute","source":"Negotiation","discountCharge":"recurring","description":"22","amount":23}]}';
            AccServProdConf.Mobile_Scenario__c = 'test';
            
            List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>{accServConf, AccServProdConf};
            insert pcList;
            
            cscfga__Attribute__c refQuan = CS_DataTest.createAttribute(AccServProdConf.id, 'ReferentQuantity', '9');

            cscfga__Attribute__c category = CS_DataTest.createAttribute(AccServProdConf.id, 'Category', 'VPN');

            cscfga__Attribute__c product = CS_DataTest.createAttribute(AccServProdConf.id, 'Product', 'testFakeValue');

            List<cscfga__Attribute__c> attrList = new List<cscfga__Attribute__c>{refQuan,category,product};
            insert attrList;

            }
    }
    
    @isTest
    static void testInvoke() {
        List<cscfga__Product_Configuration__c> configList = [SELECT Id, Name, cscfga__Configuration_Status__c FROM cscfga__Product_Configuration__c];
        
        AccountServicesInvalidationPlugin testObject = new AccountServicesInvalidationPlugin();
        Test.startTest();
        String res = (String)testObject.invoke(configList[0].Id);
        Test.stopTest();

        List<cscfga__Product_Configuration__c> configListRes = [SELECT Id, Name, cscfga__Configuration_Status__c FROM cscfga__Product_Configuration__c WHERE Name = 'Account Services'];
        
        System.assertEquals('Requires Update', configListRes[0].cscfga__Configuration_Status__c);
    }
}