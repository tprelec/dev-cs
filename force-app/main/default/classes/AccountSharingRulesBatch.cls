/**
 * @description         This class updates the VF_Asset__c sharing rules in batch
 * @author              Gerhard Newman
 */
global class AccountSharingRulesBatch implements Database.Batchable<sObject> {
    //The set of accounts that are to be processed
    Set<Id> accountIds;

    //Constructor initialization and removal of old sharing rules
    global AccountSharingRulesBatch(Set<Id> accounts) {
        accountIds = accounts;
    }

    //Query method to set the scope of assets that need to be processed
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return DataBase.getQueryLocator([Select 
                                            Id, 
                                            OwnerId, 
                                            Account__c, 
                                            Account__r.OwnerId
                                        From 
                                            VF_Asset__c 
                                        Where 
                                            Account__c in :accountIds]);
    }

    //Execute Method that inserts new sharing rules for the assets
    global void execute(Database.BatchableContext BC,List<VF_Asset__c> assets) {
        //remove all existing (manual) rules for these assets 
        //In some situations the share cannot be deleted but don't let that fail the batch
        List<VF_Asset__Share> deleteList = [select id From VF_Asset__Share Where ParentId IN :assets AND RowCause = 'Manual'];
        Database.delete(deleteList,false);

        List<VF_Asset__share> sharingRulesToInsert = new List<VF_Asset__share>();
        for (VF_Asset__c vfa:assets) {
            if(vfa.OwnerId != vfa.Account__r.OwnerId){
                VF_Asset__share vfas = new VF_Asset__share();
                vfas.UserOrGroupId = vfa.Account__r.OwnerId;
                vfas.RowCause = 'Manual';
                vfas.ParentId = vfa.Id;
                vfas.AccessLevel = 'Read';
                sharingRulesToInsert.add(vfas);
            }
        }
        insert sharingRulesToInsert;
    }

    //Finish method to execute at the end
    global void finish(Database.BatchableContext BC) {
    }
}