/**
 * @description         This is the trigger handler for the cspmb__Add_On_Price_Item__c sObject. 
 * @author              Guy Clairbois
 */
public with sharing class AddOnPriceItemTriggerHandler extends TriggerHandler {

    /**
     * @description         This handles the before insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeInsert(){
        List<cspmb__Add_On_Price_Item__c> newPriceitems = (List<cspmb__Add_On_Price_Item__c>) this.newList;   
        updateProductLink(newPriceitems,null);
    
    }

    
    /**
     * @description         This handles the after insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterInsert(){
        List<cspmb__Add_On_Price_Item__c> newPriceitems = (List<cspmb__Add_On_Price_Item__c>) this.newList;   
        
        
    }

    /**
     * @description         This handles the before update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeUpdate(){
        List<cspmb__Add_On_Price_Item__c> newPriceitems = (List<cspmb__Add_On_Price_Item__c>) this.newList;   
        Map<Id,cspmb__Add_On_Price_Item__c> oldPriceitemsMap = (Map<Id,cspmb__Add_On_Price_Item__c>) this.oldMap;

        updateProductLink(newPriceitems,oldPriceitemsMap);

    }
    

    /**
     * @description         This handles the after update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterUpdate(){
        List<cspmb__Add_On_Price_Item__c> newPriceitems = (List<cspmb__Add_On_Price_Item__c>) this.newList;   
        Map<Id,cspmb__Add_On_Price_Item__c> oldPriceitemsMap = (Map<Id,cspmb__Add_On_Price_Item__c>) this.oldMap;
        
    }   

    /**
     * @description         This method updates the prduct2 lookup fields by taking the productcodes from the external id fields
     */ 
    private void updateProductLink(List<cspmb__Add_On_Price_Item__c> newPriceitems,Map<Id,cspmb__Add_On_Price_Item__c> oldPriceitemsMap){
        // check if there are any priceitems without a product__c but with cspmb__One_Off_Charge_External_Id__c or cspmb__Recurring_Charge_External_Id__c
        // if so, store the id's for lookup
        Map<String,Id> productCodeToId = new Map<String,Id>();
        for(cspmb__Add_On_Price_Item__c pi : newPriceitems){
            if(oldPriceitemsMap == null){
                // insert
                if(pi.cspmb__One_Off_Charge_External_Id__c != null || pi.cspmb__Recurring_Charge_External_Id__c != null){
                    productCodeToId.put(pi.cspmb__One_Off_Charge_External_Id__c,null);
                    productCodeToId.put(pi.cspmb__Recurring_Charge_External_Id__c,null);                
                }
            } else {
                // update
                if(pi.cspmb__One_Off_Charge_External_Id__c != oldPriceitemsMap.get(pi.Id).cspmb__One_Off_Charge_External_Id__c || pi.cspmb__Recurring_Charge_External_Id__c != oldPriceitemsMap.get(pi.Id).cspmb__Recurring_Charge_External_Id__c){
                    productCodeToId.put(pi.cspmb__One_Off_Charge_External_Id__c,null);  
                    productCodeToId.put(pi.cspmb__Recurring_Charge_External_Id__c,null);    
                }               
            }
            
        }
        if(!productCodeToId.isEmpty()){
            for(Product2 p : [Select Id,ProductCode From Product2 Where ProductCode in :productCodeToId.keySet() AND CLC__c = null AND IsActive = true]){
                productCodeToId.put(p.ProductCode,p.Id);
            }
            system.debug(productCodeToId);
            
            for(cspmb__Add_On_Price_Item__c pi : newPriceitems){
                if(pi.cspmb__One_Off_Charge_External_Id__c != null && pi.cspmb__One_Off_Charge_External_Id__c != ''){
                    if(productCodeToId.containsKey(pi.cspmb__One_Off_Charge_External_Id__c) && productCodeToId.get(pi.cspmb__One_Off_Charge_External_Id__c) != null){
                        pi.One_Off_Charge_Product__c = productCodeToId.get(pi.cspmb__One_Off_Charge_External_Id__c);    
                    } else {
                        pi.cspmb__One_Off_Charge_External_Id__c.addError('Productcode does not exist or product is not active.');
                    }
                } else {
                    pi.One_Off_Charge_Product__c = null;
                }
                if(pi.cspmb__Recurring_Charge_External_Id__c != null && pi.cspmb__Recurring_Charge_External_Id__c != ''){
                    if(productCodeToId.containsKey(pi.cspmb__Recurring_Charge_External_Id__c) && productCodeToId.get(pi.cspmb__Recurring_Charge_External_Id__c) != null){
                        pi.Recurring_Charge_Product__c = productCodeToId.get(pi.cspmb__Recurring_Charge_External_Id__c);
                    } else {
                        pi.cspmb__Recurring_Charge_External_Id__c.addError('Productcode does not exist or product is not active.');
                    }
                } else {
                    pi.Recurring_Charge_Product__c = null;
                }
            }            
        }
                
    }    
}