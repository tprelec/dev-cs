/**
* 
* Test for AddressCheck service
* 
* @author Petar Miletic
* @ticket SFDT-15
* @since  07/01/2016Test
*/
@IsTest
public class AddressCheckTest {

    @testSetup 
    private static void setupTestData() {
        
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;
        
        LG_GeneralTest.setupServiceRequestUrls();
        
        List<cscrm__Address__c> addressList = new List<cscrm__Address__c>();
        
        for (integer i = 1; i < 6; i++) {
            
            cscrm__Address__c a = new cscrm__Address__c(
                cscrm__Street__c = 'Street No ' + i,
                LG_HouseNumber__c = String.valueOf(i + 10),
                LG_HouseNumberExtension__c = 'A',
                LG_AddressID__c = 'TestID',
                cscrm__Zip_Postal_Code__c = String.valueOf(10000 * i),
                cscrm__City__c = 'Agram');
                
            addressList.add(a);
        }
        
        Account acc = uth_Account.Create(true);
        
        // Generate Address (Premise)
        cscrm__Address__c address = LG_GeneralTest.crateAddress('Test', 'Test', 'Agram', '99', 'AB2', '10000', 'Netherlands', acc, false);
        address.LG_AddressID__c = '99999999999999';
        addressList.add(address);
        
        insert addressList;
        
        noTriggers.Flag__c = false;
        upsert noTriggers;
    }
    
    @isTest
    public static void addressCheckServiceTest() {
        
        List<cscrm__Address__c> addressList;
        List<cscrm__Address__c> addressListAll;
        
        test.startTest();
        
        string addressListJSON = AddressCheck.getAddresses('Street No 1', '11', 'A', '10000', 'Agram', '');
        string addressListAllJSON = AddressCheck.getAddresses('', '', '', '', '', '');

        addressList = (List<cscrm__Address__c>)System.JSON.deserialize(addressListJSON, List<cscrm__Address__c>.class);
        addressListAll = (List<cscrm__Address__c>)System.JSON.deserialize(addressListAllJSON, List<cscrm__Address__c>.class);

        test.stopTest();
        
        System.assertEquals(1, addressList.size(), 'Invalid result for given request');
        System.assertEquals(6, addressListAll.size(), 'Invalid result for given request');
    }
    
    @isTest
    public static void setAddressInsertTest() {
        
        Account acc = [SELECT Id FROM Account LIMIT 1];
        
        List<cscrm__Address__c> addressList;

        test.startTest();

        AddressCheck.setAddress('Test', '99', 'AB2', '10000', 'Agram', String.valueOf(acc.Id), '99999999999999', 'Croatia');

        string addressListJSON = AddressCheck.getAddresses('Test', '99', 'AB2', '10000', 'Agram', String.valueOf(acc.Id));
        addressList = (List<cscrm__Address__c>)System.JSON.deserialize(addressListJSON, List<cscrm__Address__c>.class);

        test.stopTest();
        
        System.assertEquals(1, addressList.size(), 'Invalid result for given request');
    }
    
    @isTest
    public static void setAddressUpsertTest() {

        Account acc = [SELECT Id FROM Account LIMIT 1];
        
        List<cscrm__Address__c> addressListBefore;
        List<cscrm__Address__c> addressListAfter;

        test.startTest();

        string addressListJSON = AddressCheck.getAddresses('Test', '99', 'AB2', '10000', 'Agram', String.valueOf(acc.Id));
        
        addressListBefore = (List<cscrm__Address__c>)System.JSON.deserialize(addressListJSON, List<cscrm__Address__c>.class);
        
        AddressCheck.setAddress('Test Update', '99', 'AB2', '10000', 'Agram', String.valueOf(acc.Id), '99999999999999', 'Netherlands');
        
        List<cscrm__Address__c> adressgae = [SELECT cscrm__Street__c,LG_HouseNumber__c,LG_HouseNumberExtension__c,cscrm__Zip_Postal_Code__c,cscrm__City__c, LG_AddressID__c, cscrm__Account__c FROM cscrm__Address__c];

        addressListJSON = AddressCheck.getAddresses('Test Update', '99', 'AB2', '10000', 'Agram', String.valueOf(acc.Id));
        addressListAfter = (List<cscrm__Address__c>)System.JSON.deserialize(addressListJSON, List<cscrm__Address__c>.class);
    
        test.stopTest();

        System.assertEquals(1, addressListBefore.size(), 'Invalid result for given request');
        System.assertEquals(1, addressListAfter.size(), 'Invalid result for given request');
    }
    
    @isTest
    public static void getSameAddressTest() {
        
        Account acc = [SELECT Id FROM Account LIMIT 1];
        
        cscrm__Address__c address = new cscrm__Address__c();
        
        address.cscrm__Street__c = 'First Street';
        address.LG_HouseNumber__c = '20';
        address.LG_HouseNumberExtension__c = 'A';
        address.cscrm__Zip_Postal_Code__c = '10000';
        address.cscrm__City__c = 'Agram';
        address.cscrm__Account__c = acc.Id;
        address.cscrm__Country__c = 'Netherlands';

        Test.startTest();
        
        string expected = AddressCheck.getSameAddress('First Street', '20', 'A', '10000', 'Agram', acc.Id, 'Netherlands');

        string actual = JSON.serialize(new List<cscrm__Address__c>{ address });
        
        Test.stopTest();
        
        System.assertEquals(expected, actual, 'Invalid data');
    }


    @isTest
    public static void setAddressWithMoreUpsertTest() {

        Account acc = [SELECT Id FROM Account LIMIT 1];
        
        List<cscrm__Address__c> addressListBefore;
        List<cscrm__Address__c> addressListAfter;

        test.startTest();

        AddressCheck.getAddresses(null, null, null, null, null, null);
        string addressListJSON = AddressCheck.getAddresses('Test', '99', 'AB2', '10000', 'Agram', String.valueOf(acc.Id));
        
        addressListBefore = (List<cscrm__Address__c>)System.JSON.deserialize(addressListJSON, List<cscrm__Address__c>.class);
        
        AddressCheck.setAddress('Test Update', '999', 'AB23', '100000', 'Agramit', String.valueOf(acc.Id), '99999999999999', 'Netherlands');
        
        List<cscrm__Address__c> adressgae = [
            SELECT
                cscrm__Street__c, LG_HouseNumber__c, LG_HouseNumberExtension__c, 
                cscrm__Zip_Postal_Code__c,cscrm__City__c, LG_AddressID__c, cscrm__Account__c 
            FROM cscrm__Address__c
        ];

        addressListJSON = AddressCheck.getAddresses('Test Update', '99', 'AB2', '10000', 'Agram', String.valueOf(acc.Id));
        addressListAfter = (List<cscrm__Address__c>)System.JSON.deserialize(addressListJSON, List<cscrm__Address__c>.class);
    
        test.stopTest();
    }


    @isTest
    public static void getSameAddressFoundTest() {
        
        Account acc = [SELECT Id FROM Account LIMIT 1];
        
        cscrm__Address__c address = new cscrm__Address__c();
        
        address.cscrm__Street__c = 'First Street';
        address.LG_HouseNumber__c = '20';
        address.LG_HouseNumberExtension__c = 'A';
        address.cscrm__Zip_Postal_Code__c = '10000';
        address.cscrm__City__c = 'Agram';
        address.cscrm__Account__c = acc.Id;
        address.cscrm__Country__c = 'Netherlands';


        LG_PostalCode__c postalCode = new LG_PostalCode__c (
            Name = address.cscrm__Zip_Postal_Code__c + address.LG_HouseNumber__c,
            LG_HouseNumber__c = decimal.valueOf(address.LG_HouseNumber__c),
            LG_City__c = address.cscrm__City__c,
            LG_PostalCode__c = address.cscrm__Zip_Postal_Code__c,
            LG_StreetName__c = address.cscrm__Street__c,
            LG_AreaCode__c = address.LG_HouseNumberExtension__c,
            LG_Country__c = address.cscrm__Country__c
        );

        insert postalCode;

        Test.startTest();
        
        string expected = AddressCheck.getSameAddress('First Street', '20', 'A', '10000', 'Agram', acc.Id, 'Netherlands');

        string actual = JSON.serialize(new List<cscrm__Address__c>{ address });
        
        Test.stopTest();
    }

}