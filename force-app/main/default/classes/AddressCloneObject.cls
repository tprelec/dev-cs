public with sharing class AddressCloneObject {


    @AuraEnabled
    public List<SiteDetails> sites;
    @AuraEnabled
    public List<FieldValues> fields;
    
     @AuraEnabled
    public String selectedAvailableSiteId;
    
     @AuraEnabled
    public String selectedPBXId;
    
    @AuraEnabled
    public String packageConfig;
    
     @AuraEnabled
    public String referencePCConfigId;
    
    public AddressCloneObject(){
    	
    }

    public AddressCloneObject(List<FieldValues> fields, List<SiteDetails> sites, String selectedAvailableSiteId, String selectedPBXId, String referencePCConfigId, String packageConfig) {
        this.sites = sites;
        this.fields = fields;
        this.selectedAvailableSiteId = selectedAvailableSiteId;
        this.selectedPBXId = selectedPBXId;
        this.referencePCConfigId = referencePCConfigId;
        this.packageConfig = packageConfig;
    }
}