@isTest
private class AddressCreationTest
{
     static testmethod void TestMethod_setAddress1() {
         AddressCreation.setAddress(null, null, 'test', '2', '2', 'test', 'test');
     }
     
     static testmethod void TestMethod_setAddress2() {
         
         Account acc = LG_TestDataUtility.CreateAccount('Test123','12345678', 'Ziggo','123','9999 PK','123','9999 PK');
         
         Insert acc;
         
         cscrm__Address__c addr = LG_TestDataUtility.createAddress('9999PK123','123','9999PK',acc);
         addr.LG_AddressID__c ='123456';
         insert addr;
         AddressCreation.setAddress(acc.Id, '123456', 'test', '2', '2', 'test', 'test');
     }
}