global  class AfterLastSelectProductPcCreatedObserver implements csbb.ObserverApi.IObserver {
    global static void execute (csbb.ObserverApi.Observable o, Object arg) {
        csbb.ProductConfigurationObservable observable = (csbb.ProductConfigurationObservable) o;
        
        Callable call = (Callable) new csbb.API_V1();
        Map<String, Object> args = new Map<String, Object>();
        args.put('ProductConfigurationObservable', observable);

        cscfga__Product_Basket__c basket = (cscfga__Product_Basket__c) call.call('ProductConfigurationObservable.getBasket', args);

        //basket.Error_message__c = 'test errror mes';
        //update basket;

        //List<cscfga__Product_Configuration__c> selectionContext = (List<cscfga__Product_Configuration__c>) call.call('ProductConfigurationObservable.getProductConfigurationSelectionContext', args);

        executeMethod(basket);
    }

    public static void executeMethod(cscfga__Product_Basket__c basket) {
        Map<Id, cscfga__Product_Configuration__c> allConfigs = new Map<Id, cscfga__Product_Configuration__c> ([SELECT Id, cscfga__Product_Basket__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basket.Id]);

        // moved from ProductConfigurationTriggerHandler
        if (CS_Brand_Resolver.isVodafoneRecord(allConfigs.values())) {
            CS_CustomPostConfiguringController.resetPricesInTriggerConfig(allConfigs.values(), false);
        }
        //

        //CS_PCPackageSubscriber.setItemsInBasketFields(basket.Id);

        // moved from CS_PCPackageSubscriber
        cspl.AttachmentUtil.upsertAttachments(allConfigs.keySet());
        //ID jobID = System.enqueueJob(new PLUpsertAttachmentQueueable(allConfigs.keySet(), basket.Id));
        //
    }
}