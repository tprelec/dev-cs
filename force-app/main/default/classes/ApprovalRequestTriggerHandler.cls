public with sharing class ApprovalRequestTriggerHandler extends TriggerHandler {
    private List<appro__Approval_Request__c> oldRequests;
    private List<appro__Approval_Request__c> newRequests;
    private Map<Id, appro__Approval_Request__c> oldRequestsMap;
    private Map<Id, appro__Approval_Request__c> newRequestsMap;

    private void init() {
        oldRequests = (List<appro__Approval_Request__c>) this.oldList;
        newRequests = (List<appro__Approval_Request__c>) this.newList;
        oldRequestsMap = (Map<Id, appro__Approval_Request__c>) this.oldMap;
        newRequestsMap = (Map<Id, appro__Approval_Request__c>) this.newMap;
    }

    public override void beforeInsert() {
        init();
        updateOwner();
        updateBasketLookup();
    }

    // Update Owner of Request related to Product Basket
    private void updateOwner() {
        Map<Id, Id> basketIdToOwner = new Map<Id, Id>();
        for (appro__Approval_Request__c req : newRequests) {
            // collect basket id's
            if (req.appro__Record_Object_Label__c == 'Product Basket') {
                basketIdToOwner.put(req.appro__RID__c, null);
            }
        }
        // collect basket owners
        for (cscfga__Product_Basket__c pb : [
            SELECT Id, OwnerId
            FROM cscfga__Product_Basket__c
            WHERE Id IN :basketIdToOwner.keySet()
        ]) {
            basketIdToOwner.put(pb.Id, pb.OwnerId);
        }
        // update request owner
        for (appro__Approval_Request__c req : newRequests) {
            if (basketIdToOwner.containsKey(req.appro__RID__c)) {
                req.OwnerId = basketIdToOwner.get(req.appro__RID__c);
            }
        }
    }

    // Filling the Product Basket lookup with appro__RID__c ID, only when the ID is a Product Basket ID
    private void updateBasketLookup() {
        for (appro__Approval_Request__c req : newRequests) {
            if (!String.isEmpty(req.appro__RID__c)) {
                Id rId = req.appro__RID__c;
                String objectName = rId.getSObjectType().getDescribe().getName();
                if (objectName == 'cscfga__Product_Basket__c') {
                    req.Product_Basket_lookup__c = req.appro__RID__c;
                }
            }
        }
    }
}