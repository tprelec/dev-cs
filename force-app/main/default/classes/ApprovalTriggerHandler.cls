public without sharing class ApprovalTriggerHandler extends TriggerHandler {
    private List<appro__approval__c> newApp;
    private Map<Id, appro__approval__c> oldAppMap;

    private void init() {
        newApp = (List<appro__approval__c>) this.newList;
        oldAppMap = (Map<Id, appro__approval__c>) this.oldMap;
    }

    public override void afterInsert() {
        init();

        if (isTriggerActive()) {
            autoApprove();
            setApproverEditor();
        }
    }

    public override void afterUpdate() {
        init();

        if (isTriggerActive()) {
            autoApprove();
            setApproverEditor();
        }
    }

    private void autoApprove() {
        List<appro__Approval_Request__c> requestsToUpdate = new List<appro__Approval_Request__c>();

        for (appro__approval__c appr : newApp) {
            if (appr.isUsageTrigger__c && appr.appro__Status__c == 'Assigned') {
                // mark the request as 'usage needed' so the correct email can be sent
                appro__Approval_Request__c updReq = new appro__Approval_Request__c(
                    Id = appr.appro__Approval_Request__c,
                    Usage_Required__c = true
                );
                requestsToUpdate.add(updReq);
            }
        }

        if (!requestsToUpdate.isEmpty()) {
            update requestsToUpdate;

            for (appro__approval__c appr : newApp) {
                if (appr.isUsageTrigger__c && appr.appro__Status__c == 'Assigned') {
                    // approve the step
                    appro.ApprovalUtilsController.updateApprovals(
                        new List<String>{ appr.Id },
                        new List<String>(),
                        'Please provide Usage information and re-submit',
                        'Approved',
                        appr.appro__Approver_User__c
                    );
                }
            }
        }
    }

    private void setApproverEditor() {
        Map<Id, cscfga__Product_Basket__c> basketsToUpdate = new Map<Id, cscfga__Product_Basket__c>();

        for (appro__approval__c appr : newApp) {
            if (
                appr.Approver_gets_edit_access__c &&
                appr.Approval_Record_Id__c != null &&
                (oldAppMap == null ||
                appr.appro__Status__c != oldAppMap.get(appr.Id).appro__Status__c)
            ) {
                if (appr.appro__Status__c == 'Assigned') {
                    // set field on basket to edit user(s)
                    addNewBasketToMap(appr, basketsToUpdate);

                    cscfga__Product_Basket__c updBasket = basketsToUpdate.get(
                        appr.Approval_Record_Id__c
                    );
                    String theString = updBasket.Users_with_edit_access__c;
                    theString += appr.appro__Approver_User__c + ';';
                    updBasket.Users_with_edit_access__c = theString.abbreviate(255);
                    basketsToUpdate.put(appr.Approval_Record_Id__c, updBasket);
                } else {
                    // empty field on basket to edit user
                    cscfga__Product_Basket__c updBasket = new cscfga__Product_Basket__c(
                        Id = appr.Approval_Record_Id__c,
                        Users_with_edit_access__c = null
                    );
                    basketsToUpdate.put(appr.Approval_Record_Id__c, updBasket);
                }
            }
        }

        if (!basketsToUpdate.isEmpty()) {
            update basketsToUpdate.values();
        }
    }

    private void addNewBasketToMap(
        appro__approval__c approval,
        Map<Id, cscfga__Product_Basket__c> basketsToUpdate
    ) {
        if (!basketsToUpdate.containsKey(approval.Approval_Record_Id__c)) {
            basketsToUpdate.put(
                approval.Approval_Record_Id__c,
                new cscfga__Product_Basket__c(
                    Id = approval.Approval_Record_Id__c,
                    Users_with_edit_access__c = ''
                )
            );
        }
    }
}