global class ApprovalsController{
    public static boolean runningInTestMode = false;
    public static boolean showDelegated = false;
    public  List<PendingApproval> pendingApprovalsList {get;set;}

    global class PendingApproval{
        public Id recordId {get;set;}
        public Id actorId {get;set;}
        public Boolean selected {get;set;}
        public String recordName {get;set;}
        public String actorName {get;set;}
        public String approvalInstanceId {get;set;}
        public String approvalStepId {get;set;}
        public String approvalWorkitemId {get;set;}
        public String sObjectLabel {get;set;}
        public String sObjectName {get;set;}
        public String submitterName {get;set;}
        public String submitterPhotoUrl {get;set;}
        public String submitDate {get;set;}
        public Datetime submitDateDate {get;set;}
        public boolean firstOfSObjectType {get;set;}
        
        public PendingApproval(ProcessInstanceWorkitem p){
            recordId = p.ProcessInstance.TargetObjectId;
            actorId = p.actorId;
            actorName = p.actor.Name;
            recordName = p.ProcessInstance.TargetObject.Name;
            approvalInstanceId = p.ProcessInstanceId;
            approvalWorkitemId = p.Id;
            //approvalStepId = p.ProcessInstanceId;
            sObjectName = p.ProcessInstance.TargetObject.Type;
            submitterName = p.ProcessInstance.CreatedBy.Name;      
            submitterPhotoUrl = p.ProcessInstance.CreatedBy.SmallPhotoUrl;  
            submitDateDate = p.ProcessInstance.CreatedDate;  
        }
    }
    
    public ApprovalsController(){
        //pendingApprovals = getPendingApprovalsList();
    }
    
    public  List<PendingApproval> getPendingDelegatedApprovals(){
        showDelegated = true;
        if(pendingApprovalsList == null) pendingApprovalsList = getPendingApprovals();
        return pendingApprovalsList;
    }

    public  List<PendingApproval> getSelectedApprovals(){
        List<PendingApproval> selectedApprovals = new List<PendingApproval>();
        for(PendingApproval pa : pendingApprovalsList){
            if(pa.selected){
                selectedApprovals.add(pa);
            }

        }
        return selectedApprovals;
    }    


    @RemoteAction
    global static List<PendingApproval> getPendingApprovals(){
        List<PendingApproval> pendingApprovals = new List<PendingApproval>();
        Set<Id> processInstanceIds = new Set<Id>();
        Map<String, String> sObjectName2Label = new Map<String, String>();

        String prevSObjectType;

        
        Set<Id> memberOfQueueIds = new Set<Id>();
        Id runningUserId = UserInfo.getUserId();

        if(!showDelegated){

            // show approvals of the current user
            memberOfQueueIds.add(runningUserId);
            for (GroupMember m : [  select GroupId from GroupMember where 
                                    UserOrGroupId= :runningUserId 
                                    and Group.Type = 'Queue']){
                memberOfQueueIds.add(m.GroupId);                    
            }
        } else {
            // show delegated approvals
            // first collect all users for which current user is delegated approver
            Set<Id> delegatedUsers = new Set<Id>();
            for(User u : [Select Id From User Where DelegatedApproverId = :runningUserId]){
                delegatedUsers.add(u.Id);
                memberOfQueueIds.add(u.Id);
            }
            // add all the queues as well
            for (GroupMember m : [  select GroupId from GroupMember where 
                                    UserOrGroupId in :delegatedUsers 
                                    and Group.Type = 'Queue']){
                memberOfQueueIds.add(m.GroupId);                    
            }            

        }

        List<ProcessInstanceWorkitem> approvals;
        if (runningInTestMode){
            ProcessInstance testInstance = new ProcessInstance();
            testInstance.targetObjectId = new Contact(LastName = 'Test').id;
            ProcessInstanceWorkitem testP = new ProcessInstanceWorkitem(ActorId = UserInfo.getUserId(), 
                                                                        ProcessInstance = testInstance);
            approvals = new List<ProcessInstanceWorkitem>();
            approvals.add(testP);
        }else{
            approvals = [select Id, ActorId, Actor.Name, ProcessInstanceId, ProcessInstance.TargetObjectId, 
                                         ProcessInstance.TargetObject.Name,
                                         ProcessInstance.TargetObject.Type, ProcessInstance.CreatedBy.Name,
                                         ProcessInstance.CreatedDate, ProcessInstance.CreatedBy.SmallPhotoUrl 
                                         from ProcessInstanceWorkitem 
                                         where isDeleted=false and ActorId IN :memberOfQueueIds and 
                                         ProcessInstance.status='Pending' order by 
                                         ProcessInstance.TargetObject.Type, SystemModstamp desc];
        }
                                             
        for(ProcessInstanceWorkitem p : approvals){
            PendingApproval pa = new PendingApproval(p);
            
            if (p.ProcessInstance.CreatedDate != null){
                pa.submitDate = p.ProcessInstance.CreatedDate.format('MMM dd');
            }
            
            if (p.ProcessInstance.TargetObject.Type != prevSObjectType){
                pa.firstOfSObjectType = true;
            }else{
                pa.firstOfSObjectType = false;
            }

                        
            prevSObjectType = p.ProcessInstance.TargetObject.Type;
            pa.sObjectLabel = sObjectName2Label.get(p.ProcessInstance.TargetObject.Type);

            if (pa.sObjectLabel == null){
                String sObjectType = p.ProcessInstance.TargetObject.Type;
                if (sObjectType != null){
                    if (sObjectType.endsWith('__kav')){
                        sObjectType = sObjectType.left(sObjectType.length()-1);
                    }
    
                    pa.sObjectLabel = Schema.describeSObjects(new String[]{sObjectType})[0].getLabelPlural();
                    sObjectName2Label.put(p.ProcessInstance.TargetObject.Type, pa.sObjectLabel);
                }
            }

            pendingApprovals.add(pa);
            processInstanceIds.add(p.ProcessInstanceId);
        }
        
        Map<Id, Id> processInstance2Step = new Map<Id, Id>();
        for(ProcessInstanceStep step : [select id, ProcessInstanceId, Actor.Name, 
                                        StepStatus from ProcessInstanceStep 
                                        where ProcessInstanceId in :processInstanceIds]){
            processInstance2Step.put(step.ProcessInstanceId, step.Id);
        }
        
        List<PendingApproval> finalPendingApprovals = new List<PendingApproval>();
        for (PendingApproval p : pendingApprovals){
            p.approvalStepId = processInstance2Step.get(p.approvalInstanceId);
            
            if (p.approvalStepId != null) {
                finalPendingApprovals.add(p);
            }
        }

        return finalPendingApprovals;
    }

    public pageReference manageAll(){
        PageReference p = page.MyDelegatedApprovals;
        p.setRedirect(false);
        return p;
    }

    public  String comments {get;set;}
    // dummy record used for capturing next approver (user lookup)
    public  Contact dummyRecord {
        get{
            if(dummyRecord == null) dummyRecord = new Contact();
            return dummyRecord;
        }
        set;
    }
    public pageReference approveRejectSelected(){

        PageReference pr = page.MyDelegatedApprovalsMassApproveReject;
        pr.setRedirect(false);
        return pr;
    }

    public pageReference reassignSelected(){
        PageReference pr = page.MyDelegatedApprovalsMassReassign;
        pr.setRedirect(false);
        return pr;
    }

    public pageReference massApprove(){

        //Savepoint sp = Database.setSavepoint();

        // for all approvals, instantiate the new ProcessWorkitemRequest object and populate it
        for(PendingApproval pa : pendingApprovalsList){
            if(pa.selected){
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments(comments);
                req2.setAction('Approve');
                if(dummyRecord.ownerId != null)
                    req2.setNextApproverIds(new Id[] {dummyRecord.ownerId});
            
                // Use the ID from the newly created item to specify the item to be worked
                req2.setWorkitemId(pa.approvalWorkitemId);
            
                // Submit the request for approval
                Approval.ProcessResult result2 =  Approval.process(req2);            

                // Verify the results
                if(result2.isSuccess()){
                    system.debug('succes');
                } else {
                    system.debug('failure');
                }
                system.debug(result2.getInstanceStatus());
            }
        }

        //Database.rollback(sp);
        pendingApprovalsList.clear();
        pendingApprovalsList = getPendingDelegatedApprovals();
        return page.MyDelegatedApprovals;
    }

    public pageReference massReject(){

        //Savepoint sp = Database.setSavepoint();

        // for all approvals, instantiate the new ProcessWorkitemRequest object and populate it
        for(PendingApproval pa : pendingApprovalsList){
            if(pa.selected){
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments(comments);
                req2.setAction('Reject');
                if(dummyRecord.ownerId != null)
                    req2.setNextApproverIds(new Id[] {dummyRecord.ownerId});
            
                // Use the ID from the newly created item to specify the item to be worked
                req2.setWorkitemId(pa.approvalWorkitemId);
            
                // Submit the request for approval
                Approval.ProcessResult result2 =  Approval.process(req2);            

                // Verify the results
                if(result2.isSuccess()){
                    system.debug('succes');
                } else {
                    system.debug('failure');
                }
                system.debug(result2.getInstanceStatus());
            }
        }

        //Database.rollback(sp);
        pendingApprovalsList.clear();
        pendingApprovalsList = getPendingDelegatedApprovals();
        return page.MyDelegatedApprovals;    
    }    

    public pageReference massReassign(){

        //Savepoint sp = Database.setSavepoint();

        List<ProcessInstanceWorkitem> workItemsToUpdate = new List<ProcessInstanceWorkitem>();
        Set<Id> piwiIds = new Set<Id>();
        // collect processinstanceworkitemid's
        for(PendingApproval pa : pendingApprovalsList){
            if(pa.selected){
                piwiIds.add(pa.approvalWorkitemId);
            }
        }
        // requery the piwi's to update them
        for(ProcessInstanceWorkItem piwi : [select Id, ActorId, OriginalActorId, ProcessInstanceId
                                         from ProcessInstanceWorkitem 
                                         where Id in :piwiIds ]){
            piwi.actorId = dummyRecord.ownerId;
            workItemsToUpdate.add(piwi);
        }
        
        update workItemsToUpdate;
        
        //Database.rollback(sp);
        pendingApprovalsList.clear();
        pendingApprovalsList = getPendingDelegatedApprovals();
        return page.MyDelegatedApprovals;      
    }    

    public pageReference cancel(){

        return page.MyDelegatedApprovals;
    }        
}