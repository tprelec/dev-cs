global class ApprovedOrUsageEmailController {

    global Id entryId { get; set; }
    global Boolean usageRequired {get;set;}
    global appro__Email_Activity__c emailActivity {
        get{

            return [
                SELECT appro__Approval_Request__r.appro__RID__c, 
                	appro__Approver_User__r.Name,
                    appro__Approval_Request__r.appro__Approval_Process__r.Owner.Name, 
                	appro__Approval_Request__r.Usage_Required__c,
                    appro__Approval_Request__r.LastModifiedBy.Name, 
                	appro__Approval_Request__r.appro__Record_Name__c,
                	appro__Approval_Request__r.appro__Rejection_Message__c,
                	Email_Record_URL__c
                FROM appro__Email_Activity__c
                WHERE Id = : entryId
                LIMIT 1
            ];
        } set;
    }

    global String urlRecord {
        get{
            return Url.getOrgDomainUrl().toExternalForm() + '/';
        }
    }

}