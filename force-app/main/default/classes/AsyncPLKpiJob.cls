public class AsyncPLKpiJob implements Queueable {
    
    private String basketId; 
    private Id newFutureId;
    private List<String> propositions = new List<String>();
    private List<cscfga__Product_Configuration__c> configurations = new List<cscfga__Product_Configuration__c>();
    private CS_Future_Controller futureController;
    
    public AsyncPLKpiJob(String basketId, List<String> propositions, List<cscfga__Product_Configuration__c> configurations, Id newFutureId, CS_Future_Controller futureController) {
        this.basketId = basketId;
        this.propositions = propositions;
        this.configurations = configurations; 
        this.newFutureId = newFutureId;
        this.futureController = futureController;
    }
    
    public void execute(QueueableContext context) {
        try {
            CS_Future_Controller.startFutureJob(newFutureId);
            if(propositions.size() > 0) {
                String proposition = propositions[0];
                
                List<Id> idList = new List<Id>();
                for(cscfga__Product_Configuration__c config : configurations) {
                    // if(proposition == config.Proposition__c) {
                    if(proposition == config.cspl__Type__c) {
                        idList.add(config.id);
                    } else if(proposition == 'Total') {
                        idList.add(config.id);
                    }
                }
                updateKpiFieldGeneric(basketId, proposition, idList);
                
                List<String> remainingPropositions = new List<String>();
                
                for(String p : propositions) {
                    if(p != proposition) {
                        remainingPropositions.add(p);
                    }
                }
                
                if(remainingPropositions.size() > 0) {
                    Id newFutureId = futureController.defineNewFutureJob('Update KPI fields for: ' + remainingPropositions[0]);
                    AsyncPLKpiJob kpiJob = new AsyncPLKpiJob(basketId, remainingPropositions, configurations, newFutureId, futureController);
                    ID jobID = System.enqueueJob(kpiJob);
                    futureController.setQueueableJobId(newFutureId, jobID);
                }
                CS_Future_Controller.endFutureJob(newFutureId);
            }
        } catch (Exception e) {
            CS_Future_Controller.failFutureJob(newFutureId, e.getMessage());
        }
    }
    
    public List<Object> createBasketCalculationList(String basketId) {
        List<Object> results = new List<Object>();
        try {
            results = cspl.CreateCalculationList.basketList(basketId, null);
        } catch (Exception e) {
            System.debug('Error while calling cspl.CreateCalculationList.basketList(basketId, null); method: ' + e);
        }
        return results;
    }
    
    public List<Object> createPropositionCalculationList(String basketId, List<Id> idList) {
        List<Object> results = new List<Object>();
        try {
            results = cspl.CreateCalculationList.calcList(basketId, idList, null);
        } catch (Exception e) {
            System.debug('Error while calling cspl.CreateCalculationList.calcList(basketId, null); method: ' + e);
        }
        return results;
    }
    
    public Decimal changeStringToDecimal(String totalString) {
        totalString = totalString.replace('.', '');
        
        if (totalString.substring(totalString.length() - 3, totalString.length() - 2) == ',') {
            totalString = totalString.replace(',', '.');
        }
        Decimal total = Decimal.valueOf(totalString);
        return total;
    }
    
    public void updateKpiFieldGeneric(String basketId, String proposition, List<Id> idList) {
        cscfga__Product_Basket__c productBasket = [SELECT id,KPI_JSON_Values__c,KPI_EBITDA_Net_revenue__c,KPI_Free_cash_flow_Net_revenue__c,KPI_Net_Incremental_Billed_Revenue__c,KPI_NPV__c,KPI_Payback_Period_Months__c,KPI_Sales_Margin_Net_revenue__c FROM cscfga__Product_Basket__c WHERE id=:basketId];
        
        List<CS_Kpi_Json_Element> listKpiElements = new List<CS_Kpi_Json_Element>();
            
        if(productBasket.KPI_JSON_Values__c != null) {
            listKpiElements = (List<CS_Kpi_Json_Element>)JSON.deserialize(productBasket.KPI_JSON_Values__c, List<CS_Kpi_Json_Element>.class);
        } 
        
        List<Object> results = new List<Object>();
        results = createPropositionCalculationList(basketId, idList);
        
        CS_Kpi_Json_Element element = new CS_Kpi_Json_Element();
        
        Decimal totalNetRevenue = 0;
        Decimal totalGrossMargin = 0;
        Decimal ebitda = 0;
        Decimal netResult = 0;

        Map<String,String> pAndLTotals = new Map<String,String>();
        
        for (Object obj : results) {
            Map<String, Object> objMap = (Map<String, Object>) obj;
            String name = (String) objMap.get('Name');
            String total = (String) objMap.get('Total');

            pAndLTotals.put(name, total);
                
            if(name == 'Total Net Revenue') {
                totalNetRevenue = changeStringToDecimal(total);
            } else if(name == 'Total Gross Margin') {
                totalGrossMargin = changeStringToDecimal(total);
            } else if(name == 'EBITDA') {
                ebitda = changeStringToDecimal(total);
            } else if(name == 'NET RESULT') {
                netResult = changeStringToDecimal(total);
            }
        }
        
        if(totalNetRevenue != 0) {
            element.salesMarginDivNetRevenue = totalGrossMargin / totalNetRevenue;
            element.ebitdaDivNetRevenue = ebitda / totalNetRevenue;
            element.freeCashFlowDivNetRevenue = netResult / totalNetRevenue;
        } else {
            element.salesMarginDivNetRevenue = 0;
            element.ebitdaDivNetRevenue = 0;
            element.freeCashFlowDivNetRevenue = 0;
        }
        
                            
        element.netIncrementalBilledRevenue = 0;
        element.NPV = 0;
        element.paybackPeriod = 0;
        element.category = proposition;
        listKpiElements.add(element);
        productBasket.KPI_JSON_Values__c = JSON.Serialize(listKpiElements);
        productBasket.ProfitLoss_JSON__c = JSON.Serialize(pAndLTotals);
        update productBasket;
    }
}