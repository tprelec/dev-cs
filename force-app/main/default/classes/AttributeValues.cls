public with sharing class AttributeValues {

	public String attributeName;
	public String attributeVal;
	public Boolean attrValueSet;

	public AttributeValues() {
		this.attrValueSet = false;
	}

	public AttributeValues(String name, String val) {
		
		this.attributeName = name;
		this.attributeVal = val;
		if(this.attributeVal ==null || this.attributeVal==''){
			this.attrValueSet = false;
		}
	}

}