/**
 * @tests AuraTest
 */
public without sharing class AuraActionResult
{
    @AuraEnabled
    public Boolean     success { get; set; }

    @AuraEnabled
    public AuraMessage message { get; set; }


    public AuraActionResult(Boolean success)

    {
        this.success = success;
    }


    public AuraActionResult(Boolean success, AuraMessage message)
    {
        this(success);
        this.message = message;
    }
}