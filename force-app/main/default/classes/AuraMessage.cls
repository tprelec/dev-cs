/**
 * @tests AuraTest
 */
public without sharing class AuraMessage
{
    @AuraEnabled
    public String message { get; set; }


    @AuraEnabled
    public String severity { get; private set; }


    public AuraMessageSeverity messageSeverity { get; set; }


    public AuraMessage(String message)
    {
        this.message = message;
    }


    public AuraMessage(String message, AuraMessageSeverity messageSeverity)
    {
        this(message);
        this.messageSeverity = messageSeverity;
        this.severity = messageSeverity.name();
    }
}