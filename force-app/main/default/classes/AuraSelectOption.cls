/**
 * @tests AuraTest
 */
public without sharing class AuraSelectOption
{
    @AuraEnabled
    public String value { get; set; }


    @AuraEnabled
    public String label { get; private set; }


    @AuraEnabled
    public Boolean isActive { get; private set; }


    @AuraEnabled
    public Boolean isDefault { get; private set; }


    public AuraSelectOption(String value, String label)
    {
        this.value = value;
        this.label = label;
        this.isActive = true;
        this.isDefault = false;
    }


    public AuraSelectOption(String value, String label, Boolean isActive)
    {
        this(value, label);
        this.isActive = isActive;
        this.isDefault = false;
    }


    public AuraSelectOption(String value, String label, Boolean isActive, Boolean isDefault)
    {
        this(value, label, isActive);
        this.isDefault = isDefault;
    }
}