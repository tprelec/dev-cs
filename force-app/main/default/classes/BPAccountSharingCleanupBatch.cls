/**
 * @description         This class contains a batch cleanup mechanism for Business Partner Account sharing rules
 * @author              Guy Clairbois
 */
global class BPAccountSharingCleanupBatch implements Database.Batchable<sObject> {

	String query = 'SELECT Id, UserOrGroupId, AccountId FROM AccountShare Where RowCause = \'Manual\' AND CaseAccessLevel = \'None\' AND OpportunityAccessLevel = \'None\' AND AccountAccessLevel = \'Read\' ';
	

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<AccountShare> scope) {
    	// do sharing rules cleanup here

    	// collect userids and accountids
    	Set<Id> userIds = new Set<Id>();
    	Set<Id> accountIds = new Set<Id>();
		for (AccountShare accs : scope) {
    		userIds.add(accs.UserOrGroupId);
    		accountIds.add(accs.AccountId);
    	}
    	system.debug(userIds);
    	system.debug(accountids);
    	// check if the users are partnerusers. If not, remove them from the set
    	Map<Id,Id> partnerIdToAccountId = new Map<Id,Id>();
    	for(User u : [Select Id, UserType, AccountId From User Where Id in :userIds]){
    		if(GeneralUtils.isPartnerUser(u)){
				partnerIdToAccountId.put(u.Id,u.AccountId);
    		}
    	}
    	system.debug(partnerIdToACcountId);

    	// check if any opportunities exist for that partner for the account. 
    	Map<Id,Set<Id>> customerAccountIdToPartnerAccountIds = new Map<Id,Set<Id>>();
    	for(Opportunity o : [Select Id, Owner.UserType, Owner.AccountId, Owner.Id, AccountId From Opportunity Where AccountId in :accountIds AND Owner.AccountId in :partnerIdToAccountId.values()]){

			if(customerAccountIdToPartnerAccountIds.containsKey(o.AccountId)){
				customerAccountIdToPartnerAccountIds.get(o.AccountId).add(o.Owner.AccountId);
			} else {
				customerAccountIdToPartnerAccountIds.put(o.AccountId,new Set<Id>{o.Owner.AccountId});
			}
    	}

    	// remove the sharing if it is for a partner user who's account does not own any other opportunities for that customer account
    	List<AccountShare> accountShareToDelete = new List<AccountShare>();
    	for(Accountshare accs : scope){
			// if it is a partner user
			if(partnerIdToAccountId.containsKey(accs.UserOrGroupId)){
				system.debug(partnerIdToAccountId.get(accs.UserOrGroupId));
				// if the customer account has no opps for this partner account
				if(customerAccountIdToPartnerAccountIds.containsKey(accs.AccountId)){
					// if the partner user account is not equal to the customer's partner account
    				if(!customerAccountIdToPartnerAccountIds.get(accs.AccountId).contains(partnerIdToAccountId.get(accs.UserOrGroupId) )){
    					accountShareToDelete.add(accs);
    				}
    			}
			}
    	}
    	system.debug(accountShareToDelete);
    	Delete accountShareToDelete;
    	

	}
	
	global void finish(Database.BatchableContext BC) {
		// nothing required here at this time
	}
	
}