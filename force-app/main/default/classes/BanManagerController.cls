public with sharing class BanManagerController {
	public BanManagerController() {
		
	}

	public BanManagerData bm {get;set;}

	public pageReference banChanged(){
		bm.banChanged = true;
		bm.errorText = null;
		bm.infoText = null;
		return null;
	}
}