@SuppressWarnings('pmd')
public without sharing class BanManagerData {
	public String banSelected { get; set; }
	public String newBan { get; set; }
	public static Boolean isPair { get; set; }
	public static Boolean isSingle = true;
	public static map<String, String> noOfPair = new Map<String, String>();

	public List<SelectOption> bans {
		get {
			isPair = false;
			bans = new List<SelectOption>();
			bans.add(new SelectOption('empty', Label.LABEL_Not_specified));

			// check if a bopcode is found. In that case don't allow new bans for fixed orders [GC cancelled by BonP]
			//Boolean bopCodeFound = false;
			Boolean unifyRequestFound = false;
			for (ban__c b : visibleBanList) {
				//if(b.BAN_Status__c != 'Closed' && b.BOPCode__c != null){
				//  bopCodeFound = true;
				//}
				if (b.Name.startsWith('Requested New Unify BAN')) {
					unifyRequestFound = true;
				}
			}
			if (hasFixed == null) {
				hasFixed = false;
			}
			if (hasOnenet == null) {
				hasOnenet = false;
			}

			for (ban__c b : visibleBanList) {
				// add to options
				if (b.BAN_Status__c == 'Closed') {
					// in case of closed ban, only allow SAG to use the BAN
					if (
						!Special_Authorizations__c.getInstance().Close_NonStandard_Opportunities__c
					) {
						bans.add(new SelectOption(b.Name, b.Name + ' (closed)', true));
					} else {
						//if(bopCodeFound && hasFixed && b.BOPCode__c == null){ // in case this is a fixed order and this ban doesn't have the bopcode, disable selection
						//  bans.add(new SelectOption(b.Name,b.Name+' (closed)',true));
						//} else {
						bans.add(new SelectOption(b.Name, b.Name + ' (closed)', false));
						//}
					}
				} else {
					//if(bopCodeFound && hasFixed && b.BOPCode__c == null){ // in case this is a fixed order and this ban doesn't have the bopcode, disable selection
					//  bans.add(new SelectOption(b.Name,b.Name,true));
					//} else {
					//bans.add(new SelectOption(b.Name,b.Name+(b.Has_Fixed__c?' (has fixed)':''),false));
					if (
						(b.ExternalAccount__c != null &&
						b.ExternalAccount__r.External_Account_Id__c != null) &&
						(b.ExternalAccount__r.Related_External_Account__c != null &&
						b.ExternalAccount__r.Related_External_Account__r.External_Account_Id__c !=
						null)
					) {
						bans.add(
							new SelectOption(
								b.Name,
								b.Name +
								'(' +
								b.ExternalAccount__r.External_Account_Id__c +
								',' +
								b.ExternalAccount__r.Related_External_Account__r.External_Account_Id__c +
								')' +
								(b.Has_Fixed__c ? ' (has fixed)' : ''),
								false
							)
						);
						// isSingle  = false;
						noOfPair.put(
							b.ExternalAccount__r.External_Account_Id__c,
							b.ExternalAccount__r.Related_External_Account__r.External_Account_Id__c
						);
					} else if (
						(b.ExternalAccount__c != null &&
						b.ExternalAccount__r.External_Account_Id__c != null) &&
						(b.ExternalAccount__r.Related_External_Account__c == null)
					) {
						bans.add(
							new SelectOption(
								b.Name,
								b.Name +
								'(' +
								b.ExternalAccount__r.External_Account_Id__c +
								')' +
								(b.Has_Fixed__c ? ' (has fixed)' : ''),
								false
							)
						);
					} else if (b.ExternalAccount__c == null) {
						bans.add(
							new SelectOption(
								b.Name,
								b.Name + (b.Has_Fixed__c ? ' (has fixed)' : ''),
								false
							)
						);
					}

					//}
				}
			}

			if (!preventNewBans) {
				// if this is a fixed order and there is a ban with a bopcode, disable creation of a new ban
				//if(!bopCodeFound || !hasFixed){
				// for onenet orders, allow to enter existing ban.
				//if(hasOnenet){ // REMOVE THIS LINE ONCE ONENET CUSTOMERS CAN'T BE ENTERED ANYMORE
				//  bans.add(new SelectOption('new','Enter Unify/Gemini BAN')); // REMOVE THIS LINE ONCE ONENET CUSTOMERS CAN'T BE ENTERED ANYMORE
				//} else { // REMOVE THIS LINE ONCE ONENET CUSTOMERS CAN'T BE ENTERED ANYMORE
				// only allow 1 unify request at a time
				if (!unifyRequestFound) {
					bans.add(new SelectOption('Request New Unify BAN', 'Request New Unify BAN'));
				}
				//} // REMOVE THIS LINE ONCE ONENET CUSTOMERS CAN'T BE ENTERED ANYMORE
				//}
			}
			//  system.debug('**********noOfPair' + noOfPair);
			//  system.debug('**********noOfPair' + noOfPair.size());
			if (!noOfPair.isEmpty() && noOfPair.size() > 1) {
				isPair = true;
			}
			//system.debug('**********isPair' + isPair);
			return bans;
		}
		private set;
	}

	public Boolean banChanged { get; set; }
	public String errorText { get; set; }
	public String infoText { get; set; }
	public List<Ban__c> visibleBanList { get; set; }
	private Boolean hasFixed;
	private Boolean hasOnenet;
	private Boolean preventNewBans;

	public BanManagerData(
		Set<Id> accountIds,
		Id banIdSelected,
		Boolean fixed,
		Boolean onenet,
		Boolean preventNew,
		Boolean blockClosedBans
	) {
		this.hasFixed = fixed;
		this.hasOnenet = onenet;
		this.preventNewBans = preventNew;

		Set<String> excludedStatuses = new Set<String>();
		if (blockClosedBans) {
			excludedStatuses.add('Closed');
		}

		// fetch account bans and create a bandata instance
		this.visibleBanList = [
			SELECT
				Id,
				Name,
				Ban_Number__c,
				BAN_Status__c,
				Account__c,
				BOPCode__c,
				Has_Fixed__c,
				ExternalAccount__c,
				ExternalAccount__r.External_Account_Id__c,
				ExternalAccount__r.Related_External_Account__c,
				ExternalAccount__r.Related_External_Account__r.External_Account_Id__c
			FROM Ban__c
			WHERE
				Account__c IN :accountIds
				AND BAN_Status__c NOT IN :excludedStatuses
				AND Usable__c = TRUE
			LIMIT 990
		];
		//this.visibleBanList = visibleBans;
		if (banIdSelected != null) {
			List<Ban__c> existingBan = [
				SELECT Id, Name, BAN_Status__c, BOPCode__c, Account__c, Has_Fixed__c
				FROM Ban__c
				WHERE Id = :banIdSelected
			];
			banSelected = existingBan[0].Name;
		} else if (banSelected == null) {
			// if none selected yet,
			banSelected = bans[0].getValue();
		}
	}

	public Ban__c insertNewBan(Id acctId) {
		if (
			newBan != null &&
			newBan.length() == 9 &&
			(newBan.startsWith('3') || newBan.startsWith('5'))
		) {
			Ban__c banToInsert = new Ban__c();
			banToInsert.Name = newBan;
			banToInsert.Account__c = acctId;
			insert banToInsert;

			return banToInsert;
		} else {
			throw new ExMissingDataException(Label.ERROR_Valid_Ban_is_required);
			return null;
		}
	}

	public Ban__c getBan(Id acctId) {
		Ban__c banToInsert = new Ban__c();

		// create a new ban if 'new' is selected
		if (banSelected == 'new') {
			// check if the ban already exists. If so, only create sharing, if not, create it
			List<Ban__c> existingBan = [
				SELECT Id, Name, BAN_Status__c, BOPCode__c, Account__c, Has_Fixed__c
				FROM Ban__c
				WHERE Name = :newBan
			];
			if (!existingBan.isEmpty()) {
				for (Ban__c b : visibleBanList) {
					if (existingBan[0].BAN_Status__c == 'Closed') {
						// Ban exists but has status closed. Raise error.
						throw new ExMissingDataException(
							Label.ERROR_Ban_already_exists_with_status_close
						);
						return null;
					}

					if (b.Id == existingBan[0].Id) {
						// Already in list. Raise error.
						throw new ExMissingDataException(Label.ERROR_Ban_already_exists);
						return null;
					}
				}
				if (existingBan[0].Account__c == acctId) {
					// exists under this account; create sharing on account for partners
					if (GeneralUtils.currentUserIsPartnerUser()) {
						createAccountSharing(existingBan[0].Account__c, Userinfo.getUserId());
					}
					// and create sharing on BAN
					createBanSharing(existingBan[0], Userinfo.getUserId());

					banToInsert = existingBan[0];
				} else {
					// exists under other account. Raise error.
					throw new ExMissingDataException(Label.ERROR_Invalid_Ban_for_account);
					return null;
				}
			} else {
				// create new ban
				banToInsert = insertNewBan(acctId);
			}
			banSelected = newBan;
			// add it to the dropdown to properly show the new selected ban
			visibleBanList.add(banToInsert);
		} else if (banSelected == 'Request New Unify BAN') {
			// create Unify BAN placeholder
			banToInsert = new Ban__c(
				Name = 'Requested New Unify BAN ' + system.now(),
				Account__c = acctId,
				BAN_Status__c = 'Requested'
			);
			insert banToInsert;
			banSelected = banToInsert.Name;
			//banSelected = newBan;
			// add it to the dropdown to properly show the new selected ban
			visibleBanList.add(banToInsert);
		} else if (banSelected == 'empty') {
			// ban emptied
			return null;
		} else {
			banToInsert = [
				SELECT Id, Account__c, BAN_Status__c, Name
				FROM Ban__c
				WHERE Name = :banSelected
			];

			// no ban is created. For partner user we need to create manual sharing on the account if the current user is not the owner
			if (GeneralUtils.currentUserIsPartnerUser()) {
				Account a = [SELECT Id, OwnerId FROM Account WHERE Id = :banToInsert.Account__c];
				if (
					GeneralUtils.getPartnerUserRoleGroupIdFromUserId(a.OwnerId) !=
					GeneralUtils.getPartnerUserRoleGroupIdFromUserId(System.Userinfo.getUserId())
				) {
					createAccountSharing(banToInsert.Account__c, Userinfo.getUserId());
				}
			}
		}

		return banToInsert;
	}

	public static void createAccountSharing(Id accountId, String userId) {
		AccountShare accShare = new AccountShare();
		accShare.AccountId = accountId;
		accShare.UserOrGroupId = GeneralUtils.getPartnerUserRoleGroupIdFromUserId(userId);
		accShare.CaseAccessLevel = 'None';
		accShare.OpportunityAccessLevel = 'None';
		accShare.AccountAccessLevel = 'Edit';

		insert accShare;
	}

	public static void createBanSharing(Ban__c existingBan, String userId) {
		Ban__share banShare = new Ban__share(RowCause = Schema.Ban__share.RowCause.Opportunity__c);
		banShare.parentId = existingBan.Id;
		banShare.UserOrGroupId = userId;
		banShare.AccessLevel = 'read';
		insert banShare;
	}
}