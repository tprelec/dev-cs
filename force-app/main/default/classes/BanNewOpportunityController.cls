public with sharing class BanNewOpportunityController {
	Id banId {get;set;}
	Ban__c theBan {get;set;}
	public BanNewOpportunityController(ApexPages.StandardSetController controller) {
		banId = ApexPages.currentPage().getParameters().get('id');
		system.debug(banID);

		theBan = [Select Id, Account__c, Account__r.Name From Ban__c Where Id = :banId];

	}

	public pageReference loadOpp(){
		Opportunity o = new Opportunity();
		o.Name = theBan.Account__r.Name;
		o.StageName = 'Prospect';
		o.CloseDate = system.today();
		o.BAN__c = theBan.Id;
		o.AccountId = theBan.Account__c;
		o.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('MAC');
		insert o;

		return new pageReference('/'+o.Id+'/e?saveURL=/'+o.Id+'&cancelURL=/'+theBan.Id+'&retURL=/'+theBan.Id);

	}
}