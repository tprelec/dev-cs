public with sharing class BanTriggerHandler extends TriggerHandler {
    private List<Ban__c> oldBans;
    private List<Ban__c> newBans;
    private Map<Id, Ban__c> oldBanMap;
    private Map<Id, Ban__c> newBanMap;

    private void init() {
        oldBans = (List<Ban__c>) this.oldList;
        newBans = (List<Ban__c>) this.newList;
        oldBanMap = (Map<Id, Ban__c>) this.oldMap;
        newBanMap = (Map<Id, Ban__c>) this.newMap;
    }

    public override void afterInsert() {
        init();
        updateAccountToCustomer();
        createSharingRules();
        updateAccountBanCount();
    }

    public override void afterUpdate() {
        init();
        updateSharingRules();
        updateAccountBanCount();
    }

    public override void afterDelete() {
        init();
        updateAccountBanCount();
    }

    // If this is the first ban for an account, possibly change the account type to 'customer'
    private void updateAccountToCustomer() {
        Set<Id> accountIds = GeneralUtils.getIDSetFromList(newBans, 'Account__c');
        AccountService.getInstance().updateAccountsToCustomer(accountIds);
    }

    // Updates BAN count on Parent Account
    private void updateAccountBanCount() {
        List<Ban__c> bans = newBans;
        Map<Id, Ban__c> bansMap = oldBanMap;
        if (newBans == null) {
            bans = oldBans;
            bansMap = newBanMap;
        }
        List<Ban__c> changedBans = GeneralUtils.filterChangedRecords(
            bans,
            bansMap,
            new List<String>{ 'Account__c', 'BAN_Status__c' },
            false
        );
        Set<Id> accountIds = GeneralUtils.getIDSetFromList(changedBans, 'Account__c');

        Map<Id, Account> accountsToUpdate = new Map<Id, Account>();

        AggregateResult[] results = [
            SELECT Account__c accountId, Count(Id) cnt, Max(Account__r.Active_Ban_Count__c) max
            FROM Ban__c
            WHERE Account__c IN :accountIds AND BAN_Status__c = 'Opened'
            GROUP BY Account__c
        ];

        for (AggregateResult ar : results) {
            Integer banCount = (Integer) ar.get('cnt');
            Decimal currentBanCount = (Decimal) ar.get('max');

            // remove from set so we can determine left-over accounts later
            Id accId = (Id) ar.get('accountId');
            accountIds.remove(accId);

            if (banCount != currentBanCount) {
                accountsToUpdate.put(
                    accId,
                    new Account(Id = accId, Active_Ban_Count__c = banCount)
                );
            }
        }

        // if no opened bans remain, set to 0
        for (Id acctID : accountIds) {
            accountsToUpdate.put(acctID, new Account(Id = acctID, Active_Ban_Count__c = 0));
        }

        if (!accountsToUpdate.isEmpty()) {
            update accountsToUpdate.values();
        }
    }

    // Creates new Sharing Rules for inserted BANs
    // BAN is shared to Fixed Owner of the Account
    private void createSharingRules() {
        // Share BANs with Fixed Dealer
        List<Ban__Share> banSharesToInsert = createFixedDealerBanSharings(newBans);

        // Share BANs and Accounts with Partner users
        banSharesToInsert.addAll(createPartnerBanSharings(newBans));

        if (!banSharesToInsert.isEmpty()) {
            SharingUtils.insertRecordsWithoutSharing(banSharesToInsert);
        }
    }

    // Creates new Sharing Rules for updated BANs in a case it is reparented to new Account
    private void updateSharingRules() {
        // Check which Bans have been reparented and for which Fixed Dealer is changed
        Map<Id, Ban__c> reparentedBans = new Map<Id, Ban__c>();
        Map<Id, Ban__c> bansWithNewFixedDealer = new Map<Id, Ban__c>();
        Set<Id> accountIds = new Set<Id>();

        for (Ban__c ban : newBans) {
            Ban__c oldBan = oldBanMap.get(ban.Id);
            if (ban.Account__c != oldBan.Account__c) {
                accountIds.add(ban.Account__c);
                accountIds.add(oldBan.Account__c);
                reparentedBans.put(ban.Id, ban);
            }
        }

        Map<Id, Account> accountMap = getBanParentAccounts(accountIds);
        Set<Id> oldDealerIds = new Set<Id>();
        for (Ban__c ban : reparentedBans.values()) {
            Account newAcc = accountMap.get(ban.Account__c);
            Account oldAcc = accountMap.get(oldBanMap.get(ban.Id).Account__c);
            if (newAcc.Fixed_Dealer__c != oldAcc.Fixed_Dealer__c) {
                bansWithNewFixedDealer.put(ban.Id, ban);
                oldDealerIds.add(oldAcc.Fixed_Dealer__c);
            }
        }

        // Share BANs with Fixed Dealer
        List<Ban__Share> banSharesToInsert = createFixedDealerBanSharings(
            bansWithNewFixedDealer.values()
        );
        List<Ban__Share> banSharesToDelete = getFixedDealerBanSharingsToDelete(
            bansWithNewFixedDealer,
            oldDealerIds
        );

        // Share BANs and Accounts with Partner users
        List<Ban__c> bansWithNewOwner = GeneralUtils.filterChangedRecords(
            newBans,
            oldBanMap,
            'OwnerId'
        );
        banSharesToInsert.addAll(createPartnerBanSharings(bansWithNewOwner));

        // Insert and delete Ban Shares
        if (!banSharesToInsert.isEmpty()) {
            SharingUtils.insertRecordsWithoutSharing(banSharesToInsert);
        }
        if (!banSharesToDelete.isEmpty()) {
            SharingUtils.deleteRecordsWithoutSharing(banSharesToDelete);
        }
    }

    // Creates Ban__Share records based on the Account Fixed Dealer
    private List<Ban__Share> createFixedDealerBanSharings(List<Ban__c> bans) {
        List<Ban__Share> fixedBanShares = new List<Ban__Share>();
        if (bans.isEmpty()) {
            return fixedBanShares;
        }

        Set<Id> accountIds = GeneralUtils.getIDSetFromList(newBans, 'Account__c');
        Map<Id, Account> accountMap = getBanParentAccounts(accountIds);

        List<Ban__c> banForFixedShare = new List<Ban__c>();
        Set<Id> dealerSet = new Set<Id>();
        for (Ban__c ban : newBans) {
            Account acc = accountMap.get(ban.Account__c);
            if (acc.Fixed_Dealer__c != null) {
                banForFixedShare.add(ban);
                dealerSet.add(acc.Fixed_Dealer__c);
            }
        }

        if (dealerSet.isEmpty() || banForFixedShare.isEmpty()) {
            return fixedBanShares;
        }

        SharingUtils su = new SharingUtils();
        su.dealerShareHelper(dealerSet);
        for (Ban__c ban : banForFixedShare) {
            Account acc = accountMap.get(ban.Account__c);
            fixedBanShares.add(
                createBanShareRecord(ban, su.dealerToGroupMap.get(acc.Fixed_Dealer__c))
            );
        }

        return fixedBanShares;
    }

    // Gets Ban__Share records that were previously created for old Fixed Dealer
    private List<Ban__Share> getFixedDealerBanSharingsToDelete(
        Map<Id, Ban__c> bansWithNewFixedDealer,
        Set<Id> oldDealerIds
    ) {
        oldDealerIds.remove(null);
        if (bansWithNewFixedDealer.isEmpty() || oldDealerIds.isEmpty()) {
            return new List<Ban__Share>();
        }
        SharingUtils su = new SharingUtils();
        su.dealerShareHelper(oldDealerIds);
        return [
            SELECT Id
            FROM Ban__Share
            WHERE
                ParentId IN :bansWithNewFixedDealer.keySet()
                AND (UserOrGroupId IN :su.groupList
                OR UserOrGroupId IN :su.partnerGroups)
                AND RowCause = :Schema.Ban__share.RowCause.Dealer_Hierarchy__c
        ];
    }

    // Gets Parent Accounts based on Account IDs
    private Map<Id, Account> getBanParentAccounts(Set<Id> accountIds) {
        if (accountIds.isEmpty()) {
            return new Map<Id, Account>();
        }
        return new Map<Id, Account>(
            [
                SELECT Id, Fixed_Dealer__c, Fixed_Dealer__r.ContactUserId__c
                FROM account
                WHERE Id IN :accountIds
            ]
        );
    }

    // Creates Ban__Share records based on the Ban Owner and Partner Hierarchy
    // Handles Account Sharing for Partners
    private List<Ban__Share> createPartnerBanSharings(List<Ban__c> bans) {
        List<Ban__Share> partnerBanShares = new List<Ban__Share>();
        if (bans.isEmpty()) {
            return partnerBanShares;
        }

        // Get Partner User Ids based on the Owner
        Set<Id> ownerIds = GeneralUtils.getIdSetFromList(bans, 'OwnerId');
        Map<Id, Id> parentPartnerUserIds = GeneralUtils.getParentPartnerUserIdsFromUserIds(
            ownerIds
        );
        // Get Map of Account Ids and Ban Owners
        Map<Id, Id> accountToBanOwner = new Map<Id, Id>();
        for (Ban__c ban : bans) {
            accountToBanOwner.put(ban.Account__c, ban.OwnerId);
        }
        // Build list of Partner Ban_Share records
        if (!parentPartnerUserIds.isEmpty()) {
            for (Ban__c ban : bans) {
                if (parentPartnerUserIds.containsKey(ban.OwnerId)) {
                    partnerBanShares.add(
                        createBanShareRecord(ban, parentPartnerUserIds.get(ban.OwnerId))
                    );
                }
            }
            // Add the parent owners to the owner set, preparing for account sharing rules creation
            ownerIds.addAll(parentPartnerUserIds.values());
        }
        // Share account with partners
        SharingUtils.createAccountSharing(accountToBanOwner, ownerIds, parentPartnerUserIds);
        return partnerBanShares;
    }

    // Creates Ban__Share record for provided Ban and User/Group that should have access to the Ban
    private Ban__Share createBanShareRecord(Ban__c ban, Id userOrGroupId) {
        return new Ban__Share(
            RowCause = Schema.Ban__Share.RowCause.Dealer_Hierarchy__c,
            ParentId = ban.Id,
            UserOrGroupId = userOrGroupId,
            AccessLevel = 'read'
        );
    }
}