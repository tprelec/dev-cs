/**
 * Created by henk on 2018-11-21.
 */
global class BatchMedalliaSyncHandler implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {

    String query = 'SELECT Id FROM Medallia_Sync_Records__c WHERE Status__c = \'Pending\' ORDER BY CreatedDate';

    /************************************************************
    *  constructor
    ************************************************************/
    global BatchMedalliaSyncHandler() {
    }

    /*********************************************************************
    *  execute(SchedulableContext SC)
    **********************************************************************/
    global void execute(SchedulableContext SC) {
        BatchMedalliaSyncHandler blt = new BatchMedalliaSyncHandler();
        ID batchprocessid = Database.executeBatch(blt, 5);
    }

    /****************************************************************
    *  start(Database.BatchableContext BC)
    *****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(this.query);
    }

    /*********************************************************************
    *  execute(Database.BatchableContext BC, List scope)
    **********************************************************************/
    global void execute(Database.BatchableContext BC, List<Medallia_Sync_Records__c> scope) {
        Set<String> msrSet = new Set<String>();
        for(Medallia_Sync_Records__c msr : scope) {
            msrSet.add(msr.Id);
        }
        BatchMedalliaSyncHandler.transferMedalliaRecords(msrSet);
    }

    /****************************************************
    *  finish(Database.BatchableContext BC)
    *****************************************************/
    global void finish(Database.BatchableContext BC) {

    }

    global static void transferMedalliaRecords(Set<String> msrSet) {
        Map<Id, MedalliaCreateAccountJson> accClassMap = new Map<Id, MedalliaCreateAccountJson>();
        List<MedalliaCreateJson> mcjList = new List<MedalliaCreateJson>();
        Set<Id> accSet = new Set<Id>();
        List<Contact> conList = [
                            SELECT
                                    Id
                                    , FirstName
                                    , LastName
                                    , Email
                                    , Phone
            						, MobilePhone
                                    , Gender__c
                                    , NPS_Response_Date__c
                                    , Title
                                    , Account.Global_DUNS_Number__c
                                    , Account.KVK_number__c
                                    , Account.CSM_Customer__c
                                    , Account.Converged_Service_Manager__c
                                    , Account.Converged_Service_Manager__r.Name
                                    , Account.CA_Customer__c
                                    , Account.NumberOfEmployees
                                    , Account.Industry
                                    , Account.NPS_Survey_Send__c
                                    , Account.Id
                                    , Account.Name
                                    , Account.Owner.Id
                                    , Account.Owner.Name
                                    , Account.Owner.Email
                                    , Account.Owner.UserRole.Name
                                    , Account.Owner.Manager.Id
                                    , Account.Owner.Manager.Name
                                    , Account.Owner.Manager.Email
                                    , Account.Owner.Manager.UserRole.Name
                                    , Account.Brand__c
                                    , Account.NPS_Pulse_Survey__c
                                    , Account.NPS_Survey__c
                            From Contact
                            Where
                                NPS_contact__c = 'Yes'
                                    AND
                                    AccountId IN
                                (Select Account__c From Medallia_Sync_Records__c WHere Id IN: msrSet)];
        system.debug('##conList: '+conList);
        for (Contact con : conList) {

            /** If Pulse = true, check if survey date is filled. Otherwise don't send */
            if ((con.Account.NPS_Pulse_Survey__c == true && con.NPS_Response_Date__c != null) || con.Account.NPS_Survey__c == true) {

                String contactFirstName = '';
                if (!String.isEmpty(con.FirstName)) contactFirstName = con.FirstName;
                String contactLastName = '';
                if (!String.isEmpty(con.LastName)) contactLastName = con.LastName;

                /** Contact_Phone */
                String contactPhone = '';
                if (!String.isEmpty(con.Phone)) {
                    contactPhone = con.Phone;
                } else if (!String.isEmpty(con.MobilePhone)) {
                    contactPhone = con.MobilePhone;
                }
                //contactPhone = '355692224755';

                /** WaveDate  */
                String dateToday = String.valueOf(Date.Today().Year());
                dateToday += '-';
                if (Date.Today().Month() < 10) {
                    dateToday += '0';
                }
                dateToday += String.valueOf(Date.Today().Month());
                dateToday += '-';
                if (Date.Today().Day() < 10) {
                    dateToday += '0';
                }
                dateToday += String.valueOf(Date.Today().Day());

                /** vodafoneCustomer */
                String vodafoneCustomer = '0';
                if (con.Account.Brand__c != 'Ziggo') vodafoneCustomer = '1';

                /** ziggoCustomer */
                String ziggoCustomer = '0';
                if (con.Account.Brand__c != 'Vodafone') ziggoCustomer = '1';

                /** isPulse */
                String isPulse = '0';
                if (con.Account.NPS_Pulse_Survey__c) isPulse = '1';

                /** Gender */
                String gender = 'M';
                if (con.Gender__c == 'Female') gender = 'F';

                /** contact Job Title */
                String contactJobTitle = '';
                if (!String.isEmpty(con.Title)) {
                    contactJobTitle = con.Title;
                }

                /** Account_Global_DUNS_Number */
                String dunsNumber = ''; //Global_DUNS_Number__c
                if (!String.isEmpty(con.Account.Global_DUNS_Number__c)) {
                    dunsNumber = con.Account.Global_DUNS_Number__c;
                }

                /** KVK */
                String kvk = ''; //KVK_number__c
                if (!String.isEmpty(con.Account.KVK_number__c)) {
                    kvk = con.Account.KVK_number__c;
                }

                /** CSM_Customer */
                String csmCustomer = 'No'; //CSM_Customer__c 0 1
                if (con.Account.CSM_Customer__c == true) {
                    csmCustomer = 'Yes';
                }

                /** CSM_Full_Name */
                String csmFullName = ''; //Converged_Service_Manager__c
                if (con.Account.Converged_Service_Manager__c != null) {
                    csmFullName = con.Account.Converged_Service_Manager__r.Name;
                }

                /** CA_Customer */
                String caCustomer = '0'; //CA_Customer__c 0 1
                if (con.Account.CA_Customer__c == true) {
                    csmCustomer = '1';
                }

                /** Account_Employees */
                String amountEmployees = '0'; //NumberOfEmployees
                if (con.Account.NumberOfEmployees != null) {
                    amountEmployees = String.valueOf(con.Account.NumberOfEmployees);
                }

                /** Industry */
                String industry = ''; //Industry
                if (!String.isEmpty(con.Account.Industry)) {
                    industry = con.Account.Industry;
                }

                /** Last_Completed_Survey */
                String lastCompletedSurvey = ''; //NPS_Survey_Send__c
                if (con.Account.NPS_Survey_Send__c != null) {
                    lastCompletedSurvey = String.valueOf(con.Account.NPS_Survey_Send__c.Year());
                    lastCompletedSurvey += '-';
                    if (con.Account.NPS_Survey_Send__c.Month() < 10) {
                        lastCompletedSurvey += '0';
                    }
                    lastCompletedSurvey += String.valueOf(con.Account.NPS_Survey_Send__c.Month());
                    lastCompletedSurvey += '-';
                    if (con.Account.NPS_Survey_Send__c.Day() < 10) {
                        lastCompletedSurvey += '0';
                    }
                    lastCompletedSurvey += String.valueOf(con.Account.NPS_Survey_Send__c.Day());
                }

                /** Brand */
                String brand = con.Account.Brand__c;
                if (brand == 'Both') brand = 'Vodafone and Ziggo';


                /** First Create the account record */
                if (!accClassMap.containsKey(con.Account.Id)) {
                    MedalliaCreateAccountJson accClass = new MedalliaCreateAccountJson();
                    accClass.CSM_Full_Name = csmFullName;
                    accClass.CSM_ID = '';
                    if (!String.isEmpty(con.Account.Converged_Service_Manager__c)) {
                        accClass.CSM_ID = con.Account.Converged_Service_Manager__c;
                    }
                    accClass.Account_ID = con.Account.Id;
                    accClass.Account_Name = con.Account.Name;
                    accClass.Account_Manager = con.Account.Owner.Name;
                    accClass.Account_Manager_Role_Name = con.Account.Owner.UserRole.Name;
                    accClass.Account_Global_DUNS_Number = dunsNumber;
                    accClass.Account_Manager_ID = con.Account.Owner.Id;
                    accClass.Account_Manager_Mail = con.Account.Owner.Email;
                    accClass.Sales_Manager_Name = con.Account.Owner.Manager.Name;
                    accClass.Sales_Manager_Role_Name = con.Account.Owner.Manager.UserRole.Name;
                    accClass.Sales_Manager_ID = con.Account.Owner.Manager.Id;
                    accClass.Sales_Manager_Mail = con.Account.Owner.Manager.Email;
                    accClassMap.put(con.Account.Id, accClass);
                }

                /** Create Survey Record */
                MedalliaCreateJson mClass = new MedalliaCreateJson();
                mClass.Contact_First_Name = contactFirstName;
                mClass.Contact_Last_Name = contactLastName;
                mClass.Contact_Mail = con.Email;
                mClass.Contact_ID = con.Id;
                mClass.Wave_Date = dateToday;
                mClass.Account_ID = con.Account.Id;
                mClass.Account_Name = con.Account.Name;
                mClass.Account_Manager = con.Account.Owner.Name;
                mClass.Account_Manager_Role_Name = con.Account.Owner.UserRole.Name;
                mClass.Sales_Manager_Name = con.Account.Owner.Manager.Name;
                mClass.Sales_Manager_Role_Name = con.Account.Owner.Manager.UserRole.Name;
                mClass.Brand = brand;
                mClass.Vodafone_Customer = vodafoneCustomer;
                mClass.Ziggo_Customer = ziggoCustomer;
                mClass.Is_Pulse = isPulse;

                /** Non req fields */
                mClass.Contact_Gender = gender;
                mClass.Contact_Job_Title = contactJobTitle;
                mClass.Contact_Phone = contactPhone;
                mClass.Account_Global_DUNS_Number = dunsNumber;
                mClass.Account_KVK_Number = kvk;
                mClass.CSM_Customer = csmCustomer;
                mClass.CSM_Full_Name = csmFullName;
                mClass.CA_Customer = caCustomer;
                mClass.Account_Employees = amountEmployees;
                mClass.Industry = industry;
                mClass.Last_Completed_Survey = lastCompletedSurvey;
                mClass.UNIQUE_ID = '';
                mClass.LANGUAGE = '';

                mcjList.add(mClass);

                /** accSet adding */
                accSet.add(con.Account.Id);
            }
        }

        List<Medallia_Sync_Records__c> msrList = [
                Select
                        Id,
                        Status__c,
                        Callout_Body__c,
                        Callout_Start__c,
                        Response_Body__c
                From Medallia_Sync_Records__c
                Where Account__c IN : accSet];

        /** Instance new Datetime */
        Datetime dt = Datetime.now();


        /** Account creation callout */
        List<MedalliaCreateAccountJson> accClassList = new List<MedalliaCreateAccountJson>();
        for (Id accId : accClassMap.keySet()) {
            accClassList.add(accClassMap.get(accId));
        }
        if (accClassList.size() > 0) {
            String outputBodyAccClass = Json.serializePretty(accClassList);
            system.debug('##outputBodyAccClass : ' + outputBodyAccClass);

            Http hAcc = new Http();
            HttpRequest reqAcc = new HttpRequest();
            HttpResponse resAcc;

            reqAcc.setTimeout(6000);
            reqAcc.setMethod('POST');
            reqAcc.setEndpoint('https://vodafoneziggo.medallia.eu/vodafoneziggo.feed?sfdc_create_org_b2b');
            reqAcc.setBody(outputBodyAccClass);

            String resBodyAcc = '';
            try {
                resAcc = hAcc.send(reqAcc);
                system.debug('##res_onlineAcc: '+resAcc.getBody());
                resBodyAcc = resAcc.getBody();
            } catch(Exception e) {
                system.debug('#Callout ErrorAcc#'+e.getMessage());
                resAcc = null;
            }
        }

        /** Survey Callout */
        String outputBody = Json.serializePretty(mcjList);
        system.debug('##outputBody : '+outputBody );

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res;

        req.setTimeout(6000);
        req.setMethod('POST');
        req.setEndpoint('https://vodafoneziggo.medallia.eu/vodafoneziggo.feed?sfdc_create_survey_b2b');
        req.setBody(outputBody);


        String resBody = '';
        try {
            res = h.send(req);
            system.debug('##res_online: '+res.getBody());
            resBody = res.getBody();
        } catch(Exception e) {
            system.debug('#Callout Error#'+e.getMessage());
            resBody = e.getMessage();
            res = null;
        }

        for (Medallia_Sync_Records__c msr : msrList) {
            msr.Status__c = 'Completed';
            if (outputBody.length() < 32000) {
	            msr.Callout_Body__c = outputBody;
            } else {
                msr.Callout_Body__c = 'too big';
            }
            msr.Callout_Start__c = dt;
            msr.Response_Body__c = resBody;
        }
        update msrList;

        List<Account> accList = [Select Id, NPS_Survey_Send__c From Account Where Id IN : accClassMap.keySet()];
        system.debug('##accList: '+accList);
        for (Account acc: accList) {
            acc.NPS_Survey_Send__c = dt;
            acc.NPS_Survey__c =  false;
            acc.NPS_Pulse_Survey__c = false;
        }
        try {
            update accList;
        } catch(Exception e) {
            system.debug('##error account update: '+e.getMessage());
        }



    }

    /**
     *
     */
    public class MedalliaCreateAccountJson {
        public String Account_ID;/** Req */
        public String Account_Name;/** Req */
        public String Account_Global_DUNS_Number;/** Req */
        public String Account_Manager_ID;/** Req */
        public String Account_Manager; /** Req */
        public String Account_Manager_Role_Name; /** Req */
        public String Account_Manager_Mail;/** Req */
        public String Sales_Manager_ID; /** Req */
        public String Sales_Manager_Name; /** Req */
        public String Sales_Manager_Role_Name; /** Req */
        public String Sales_Manager_Mail; /** Req */
        public String CSM_Full_Name;/** Req */
        public String CSM_ID;/** Req */
    }

    public class MedalliaCreateJson {

        public String Contact_First_Name; /** Req */
        public String Contact_Last_Name; /** Req */
        public String Contact_Gender;
        public String Contact_Job_Title;
        public String Contact_Phone;
        public String Contact_Mail; /** Req */
        public String Contact_ID; /** Req */
        public String Wave_Date; /** Req */
        public String Account_Global_DUNS_Number;
        public String Account_KVK_Number;
        public String CSM_Customer;
        public String CSM_Full_Name;
        public String CA_Customer;
        public String Account_Employees;
        public String Industry;
        public String Account_ID; /** Req */
        public String Account_Name; /** Req */
        public String Account_Manager; /** Req */
        public String Account_Manager_Role_Name; /** Req */
        public String Sales_Manager_Name; /** Req */
        public String Sales_Manager_Role_Name; /** Req */
        public String Brand; /** Req */
        public String Vodafone_Customer; /** Req */
        public String Ziggo_Customer; /** Req */
        public String Is_Pulse; /** Req */
        public String Last_Completed_Survey;
        public String LANGUAGE;
        public String UNIQUE_ID;
    }
}