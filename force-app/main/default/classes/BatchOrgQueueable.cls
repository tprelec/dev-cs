public class BatchOrgQueueable implements Queueable, Database.AllowsCallouts {

    public void execute(QueueableContext context) {
        String query = 'SELECT ID,Company, Fiber__C,LG_DateOfEstablishment__c,LG_InternetCustomer__c,LG_LegalForm__c,LG_MobileCustomer__c,LG_Segment__c,LG_VisitPostalCode__c FROM lead WHERE LG_NewSalesChannel__c =  \'D2D\'  AND status != \'Qualified\' and  ein_outcome_conversion__c  = null limit 100';

        WebserviceHttpRest myCall = new WebserviceHttpRest();
        myCall.doSoqlQuery(query);

    }
}