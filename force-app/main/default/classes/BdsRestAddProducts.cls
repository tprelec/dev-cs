@RestResource(urlMapping='/addproducts/*')
global with sharing class BdsRestAddProducts {
    @HttpPost
    global static void doPost(String opportunityId, List<Product> products) {

        RestResponse res = RestContext.response;
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
            res.addHeader('Content-Type', 'text/json');
            res.statusCode = 200;
        }

        /** Validate the incoming body values */
        List<BdsResponseClasses.ValidationError> errFields = BdsRestAddProducts.validateValues(opportunityId, products);
        system.debug('### errFields: ' + errFields);
        /** Query Products */
        Map<String, Id> productCodeId = new Map<String, Id>();
        Set<String> productsCodeSet = new Set<String>();
        for (Product prod : products) {
            productsCodeSet.add(prod.productCode);
        }
        if (productsCodeSet.size() > 0) {
            List<PriceBookEntry> prodSearch;
            if (Test.isRunningTest()) {
                prodSearch = [SELECT Product2.Id, Product2.Name, Product2.ProductCode, PriceBook2.Name from PriceBookEntry where Product2.ProductCode IN : productsCodeSet];
            } else {
                prodSearch = [SELECT Product2.Id, Product2.Name, Product2.ProductCode, PriceBook2.Name from PriceBookEntry where Product2.ProductCode IN : productsCodeSet AND PriceBook2.Name ='Standard Price Book' AND PriceBook2.isActive = true];
            }
            //List<PriceBookEntry> prodSearch = [SELECT Product2.Id, Product2.Name, Product2.ProductCode, PriceBook2.Name from PriceBookEntry where Product2.BigMachines__Part_Number__c IN : productsCodeSet AND PriceBook2.Name ='Standard Price Book' AND PriceBook2.isActive = true];
            //List<Product2> prodSearch = [Select Id, ProductCode From Product2 Where ProductCode in :productsCodeSet];
            for (PriceBookEntry prod : prodSearch) {
                productCodeId.put(prod.Product2.ProductCode, prod.Product2.Id);
            }
        }


        if (errFields.size() > 0) {
            /** return Error */
            BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
            returnClass.errors = errFields;
            res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
        } else if (productsCodeSet.size() != productCodeId.size()){
            /** return Error */
            BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
            for (String productCode : productsCodeSet) {
                if (!productCodeId.containsKey(productCode)) {
                    BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('productCode', 'Productcode: '+ productCode + ' not found');
                    errFields.add(errField);
                }
            }
            returnClass.errors = errFields;
            //returnClass.errorMessage = 'Not all listed products were found, provided: '+productsCodeSet.size() + ' productcodes, found: '+productCodeId.size();
            res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
        } else {

            /** Get Account Id */
            system.debug('##opportunityId: '+opportunityId);
            Id accId = [Select AccountId From Opportunity Where Id = :opportunityId].AccountId;
            /** Sum up all Sites */
            List<Site__c> siteList = [Select Id, Site_House_Number__c, Site_House_Number_Suffix__c, Site_Postal_Code__c, Site_City__c From Site__c Where Site_Account__c = :accId ORDER BY Site_House_Number_Suffix__c DESC];


            List<OpportunityLineItem> opliList = new List<OpportunityLineItem>();
            Boolean siteError = false;
            for (Product prod : products) {

                /** Check existing sites */
                if (prod.sites != null) {

                    for (Site st : prod.sites) {
                        OpportunityLineItem newOpli = new OpportunityLineItem();
                        /** Check Site values to find match */
                        Boolean siteFound = false;
                        Id siteId;
                        for (Site__c existingSite : siteList) {
                            if (!siteFound) {
                                if (existingSite.Site_House_Number__c == st.housenumber && existingSite.Site_Postal_Code__c == st.zipcode) {
                                    system.debug('##zipcode the same: ');
                                    system.debug('##st.housenumberExt: '+st.housenumberExt);
                                    system.debug('##st.housenumberExt: '+String.isEmpty(st.housenumberExt));
                                    system.debug('##existingSite.Site_House_Number_Suffix__c: '+existingSite.Site_House_Number_Suffix__c);
                                    system.debug('##existingSite.Site_House_Number_Suffix__c: '+String.isEmpty(existingSite.Site_House_Number_Suffix__c));
                                    if (!String.isEmpty(st.housenumberExt)) {
                                        if (existingSite.Site_House_Number_Suffix__c == st.housenumberExt) {
                                            siteFound = true;
                                            siteId = existingSite.Id;
                                        }
                                    } else if (String.isEmpty(existingSite.Site_House_Number_Suffix__c)) {
                                        siteFound = true;
                                        siteId = existingSite.Id;

                                    }
                                }
                            }
                        }
                        if (!siteFound) {
                            siteError = true;
                        } else {
                            newOpli.OpportunityId = opportunityId;
                            newOpli.Location__c = siteId;
                            newOpli.Deal_Type__c = 'New';
                            newOpli.Quantity = prod.quantity;
                            newOpli.CLC__c = 'Acq';
                            newOpli.Site_List__c = siteId;
                            newOpli.Product2Id = productCodeId.get(prod.productCode);
                            newOpli.Gross_List_Price__c = prod.price;
                            if (prod.discount > 0) {
                                newOpli.Product_Arpu_Value__c = prod.price * ((100 - prod.discount) / 100.0);
                            } else {
                                newOpli.Product_Arpu_Value__c = prod.price;
                            }
                            newOpli.TotalPrice = (prod.price * prod.quantity);
                            newOpli.Duration__c = prod.duration;
                            newOpli.DiscountNew__c = prod.discount;
                            opliList.add(newOpli);
                        }


                    }
                }
            }
            if (siteError) {
                BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
                returnClass.errors = new List<BdsResponseClasses.ValidationError> { new BdsResponseClasses.ValidationError('site', 'site not found')};
                res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
            } else if (opliList.size() > 0) {
                try {
                    /**Check if Opportunity has a pricebook chosen */
                    Opportunity opp = [Select Id, Pricebook2Id From Opportunity Where Id =: opportunityId];
                    if (opp.Pricebook2Id == null) {
                        Pricebook2 pb = [Select Id From Pricebook2 Where IsActive = true and Name = 'Standard Price Book'];
                        opp.Pricebook2Id = pb.Id;
                        update opp;
                    }
                    insert opliList;
                    res.responseBody = Blob.valueOf(JSON.serializePretty(new BdsResponseClasses.SuccessReturnClass()));

                } catch (Exception e) {
                    BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
                    returnClass.errors = new List<BdsResponseClasses.ValidationError> { new BdsResponseClasses.ValidationError('Error', e.getMessage())};
                    res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
                }

            }
        }

    }

    global static List<BdsResponseClasses.ValidationError> validateValues(String opportunityId, List<Product> products) {

        List<BdsResponseClasses.ValidationError> errFields = new List<BdsResponseClasses.ValidationError>();

        /** opportunityId */
        if (String.isEmpty(opportunityId)) {
            BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('opportunityId', 'opportunityId missing');
            errFields.add(errField);
        }

        if (products == null) {
            BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('products', 'no product(s) found');
            errFields.add(errField);
        } else {
            for (Product prod : products) {
                /** productCode */
                if (String.isEmpty(prod.productCode)) {
                    BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('productCode', 'productCode missing');
                    errFields.add(errField);
                }
                /** quantity */
                if (prod.quantity == null) {
                    BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('quantity', 'quantity missing');
                    errFields.add(errField);
                }
                /** price */
                if (prod.price == null) {
                    BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('price', 'price missing');
                    errFields.add(errField);
                } else {
                    if (prod.price < 0) {
                        BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('price', 'Price can\'t be a negative number');
                        errFields.add(errField);
                    }
                }
                /** discount */
                if (prod.discount == null) {
                    BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('discount', 'discount missing');
                    errFields.add(errField);
                } else {
                    if (prod.discount < 0) {
                        BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('discount', 'Discount can\'t be a negative number');
                        errFields.add(errField);
                    }
                }
                /** duration */
                if (prod.duration == null) {
                    BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('duration', 'duration missing');
                    errFields.add(errField);
                }
            }
        }


        return errFields;
    }

    global class Product {
        public List<Site> sites;
        public String productCode;
        public Decimal quantity;
        public Decimal price;
        public Decimal discount;
        public Integer duration;

    }

    global class Site {
        public Integer housenumber;
        public String housenumberExt;
        public String zipcode;
    }




}