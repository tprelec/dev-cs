@RestResource(urlMapping='/getaccount/*')
global without sharing class BdsRestGetAccount {

    @HttpPost
    global static void doPost(String kvk) {
        RestResponse res = RestContext.response;
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
            res.addHeader('Content-Type', 'text/json');
            res.statusCode = 200;
        }

        String field = 'kvk';
        if (String.isEmpty(kvk)) {
            res.responseBody = getErrorResponse(res, field, 'Kvk missing');
        } else {
            if (!Pattern.compile('^[0-9]{8}$').matcher(kvk).matches()) {
                res.responseBody = getErrorResponse(res, field, Label.LABEL_FLEX_InvalidKVK);
                return;
            }

            BdsRestCreateAccount.AccountClass returnClass = new BdsRestCreateAccount.AccountClass();
            returnClass = BdsRestCreateAccount.getAccount(kvk);
            if (String.isEmpty(returnClass.kvk)) {
                res.statusCode = 404;
                res.responseBody = getErrorResponse(res, field, 'Kvk is valid but account with this kvk is not found in SF');
            } else {
                res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
            }
        }

    }

    public static Blob getErrorResponse(RestResponse res, String field, String msg) {
        BdsResponseClasses.ValidationErrorReturnClass errorClass = new BdsResponseClasses.ValidationErrorReturnClass();
        List<BdsResponseClasses.ValidationError> errFields = new List<BdsResponseClasses.ValidationError>{new BdsResponseClasses.ValidationError(field, msg)};
        errorClass.errors = errFields;
        return Blob.valueOf(JSON.serializePretty(errorClass));
    }

}