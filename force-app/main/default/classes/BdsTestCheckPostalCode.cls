@RestResource(urlMapping='/checkpostalcode/*')
global without sharing class BdsTestCheckPostalCode {

    /**
     *
     *
     */
    @HttpPost
    global static void doPost(String postalcode, Integer housenumber, String housenumberExtension) {
        RestResponse res = RestContext.response;
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
            res.addHeader('Content-Type', 'text/json');
            res.statusCode = 200;
        }

        /** Validate the incoming body values */
        List<BdsTestCheckPostalCode.ErrorMissingFields> errFields = BdsTestCheckPostalCode.validateValues(postalcode, housenumber);
        if (errFields.size() > 0) {
            /** return Error */
            BdsTestCheckPostalCode.ErrorReturnClass returnClass     = new BdsTestCheckPostalCode.ErrorReturnClass();
            returnClass.missingFields                               = errFields;
            res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
        } else {

            List<Site__c> siteList = new List<Site__c>();
            Site__c st = new Site__c();
            st.Site_House_Number__c = housenumber;
            if (!String.isEmpty(housenumberExtension)) {
                st.Site_House_Number_Suffix__c = housenumberExtension;
            }
            st.Site_Postal_Code__c = postalcode;
            siteList.add(st);
            List<Site_Postal_Check__c> postalCodeList = new List<Site_Postal_Check__c>();
            postalCodeList.addAll(PostalCodeCheckController.doPostalCheck(siteList, 'dsl'));
            postalCodeList.addAll(PostalCodeCheckController.doPostalCheck(siteList, 'fiber'));

            List<BdsRestCreateAccount.SitePostalCodeCheckClass> returnClassList = new List<BdsRestCreateAccount.SitePostalCodeCheckClass>();
            for (Site_Postal_Check__c spc : postalCodeList) {
                system.debug('##spc: '+spc);
                if (spc.Access_Active__c) {
                    BdsRestCreateAccount.SitePostalCodeCheckClass pcc = new BdsRestCreateAccount.SitePostalCodeCheckClass();
                    pcc.vendor = spc.Access_Vendor__c;
                    pcc.accessMaxUpSpeed = Integer.ValueOf(spc.Access_Max_Up_Speed__c);
                    pcc.accessMaxDownSpeed = Integer.ValueOf(spc.Access_Max_Down_Speed__c);
                    pcc.technologyType = spc.Technology_Type__c;
                    pcc.accessClass = spc.Accessclass__c;
                    pcc.accessInfrastructure = spc.Access_Infrastructure__c;
                    pcc.accessRegion = spc.Access_Region__c;
                    if (spc.Access_Priority__c != null) {
                        pcc.accessPriority = Integer.valueOf(spc.Access_Priority__c);
                    }
                    pcc.accessResultCheck = spc.Access_Result_Check__c;
                    pcc.remarks = spc.Remark__c;
                    returnClassList.add(pcc);
                }
            }
            res.responseBody = Blob.valueOf(JSON.serializePretty(returnClassList));
        }
    }

    /**
     *
     *
     * @param postalcode
     * @param housenumber
     *
     * @return
     */
    public static List<BdsTestCheckPostalCode.ErrorMissingFields> validateValues(String postalcode, Integer housenumber) {
        List<BdsTestCheckPostalCode.ErrorMissingFields> errFields = new List<BdsTestCheckPostalCode.ErrorMissingFields>();

        /** postalcode */
        if (String.isEmpty(postalcode)) {
            BdsTestCheckPostalCode.ErrorMissingFields errField = new BdsTestCheckPostalCode.ErrorMissingFields('postalcode');
            errFields.add(errField);
        }

        /** housenumber */
        if (housenumber == null) {
            BdsTestCheckPostalCode.ErrorMissingFields errField = new BdsTestCheckPostalCode.ErrorMissingFields('housenumber');
            errFields.add(errField);
        }

        return errFields;
    }

    /**
      *
      */
    global class ErrorReturnClass {
        public Boolean isSuccess = false;
        public String errorMessage = System.Label.BDS_API_Json_incomplete;
        public List<ErrorMissingFields> missingFields;
    }

    /**
     * class defining the missing fields
     */
    global class ErrorMissingFields {
        public String field;

        public ErrorMissingFields(String fieldName) {
            field = fieldName;
        }
    }
}