@RestResource(urlMapping='/checkpostalcodekvk/*')
global without sharing class BdsTestCheckPostalCodeKVK {

    @HttpPost
    global static void doPost(String postalcode, String housenumber, String housenumberExtension, String kvk) {
        //variable for account, site and site postal check creation on future method.
        String existingAccId;
        String serializeAcc;
        String serializeSite;
        String serializelstSPC;
        //variable for BOP check
        Boolean callBOP = false;
        Boolean isNewAcc = false;
        //global variable
        List<Site_Postal_Check__c> postalCodeList = new List<Site_Postal_Check__c>();
        List<Site__c> siteList = new List<Site__c>();
        RestResponse res = RestContext.response;

        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
            res.addHeader('Content-Type', 'text/json');
            res.statusCode = 200;
        }

        List<BdsTestCheckPostalCodeKVK.ErrorMissingFields> errFields = BdsTestCheckPostalCodeKVK.validateValues(postalcode, housenumber, kvk);
        if (!errFields.isEmpty()) {
            BdsTestCheckPostalCodeKVK.ErrorReturnClass returnClass = new BdsTestCheckPostalCodeKVK.ErrorReturnClass();
            returnClass.missingFields = errFields;
            res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
            return;
        }
        if (!Pattern.compile('^[0-9]{8}$').matcher(kvk).matches()) {
            res.responseBody = getErrorResponse(res, Label.LABEL_FLEX_InvalidKVK);
            return;
        }
        postalcode = postalcode.deleteWhitespace();
        if (!Pattern.compile('[1-9][0-9]{3}\\s?[a-zA-Z]{2}$').matcher(postalcode).matches()) {
            res.responseBody = getErrorResponse(res, Label.LABEL_FLEX_InvalidPostalCode);
            return;
        }
        if (!Pattern.compile('^[0-9]*$').matcher(housenumber).matches()) {
            res.responseBody = getErrorResponse(res, Label.LABEL_FLEX_InvalidHouseNo);
            return;
        }
        Integer houseNo = Integer.valueOf(housenumber);

        List<Account> lstAcc = [
            SELECT Id, KVK_number__c
            FROM Account
            WHERE KVK_number__c =: kvk
        ]; //query the account using the KVK number

        if (!lstAcc.isEmpty()) {
            existingAccId = lstAcc[0].Id;
            //if found account with KVK then proceed with query the site
            String soql = 'SELECT Id, Name, Site_Account__r.KVK_Number__c, Site_House_Number_Suffix__c, Site_House_Number__c, Site_Postal_Code__c, ';
            soql += 'Blocked_checks__c, Canvas_Postal_Code__c, Canvas_House_Number__c, Canvas_House_Number_Suffix__c, ';
            soql += '(SELECT Id, Name, CreatedDate, Accessclass__c, Access_Site_ID__c, Access_Active__c, Access_Max_Down_Speed__c, Access_Max_Up_Speed__c,';
            soql += 'Access_Priority__c, Access_Region__c, Access_Result_Check__c, Technology_Type__c,Access_Vendor__c, Access_Infrastructure__c, Remark__c ';
            soql += 'FROM Site_Postal_Checks__r WHERE CreatedDate = LAST_N_DAYS:30 ORDER BY Createddate DESC)';
            soql += 'FROM Site__c ';
            soql += 'WHERE Site_Account__r.KVK_Number__c =\'' + String.escapeSingleQuotes(kvk) + '\' AND ';
            soql += 'Site_House_Number__c =' + String.escapeSingleQuotes(housenumber) + ' AND ';
            soql += 'Site_Postal_Code__c = \'' + String.escapeSingleQuotes(postalcode) + '\' ';
            soql += String.isNotEmpty(housenumberExtension) ? 'AND Site_House_Number_Suffix__c = \'' + String.escapeSingleQuotes(housenumberExtension) + '\''  : 'AND Site_House_Number_Suffix__c = null';
            siteList = (List<Site__c>) Database.query(soql);

            if (siteList.isEmpty()) {
                OlbicoServiceJSON db = new OlbicoServiceJSON(); //call Olbico REST Endpoint
                db.setRequestRestJsonStreet(postalcode + ' ' + housenumber);
                db.makeRequestRestJsonStreet(postalcode + ' ' + housenumber);

                if (db.AdresInfo().isEmpty()) {
                    res.responseBody = getErrorResponse(res, LABEL.LABEL_FLEX_NoAddress);
                    return;
                }

                siteList.add(addObjSiteToListFromBOP(postalcode, houseNo, housenumberExtension, kvk, db.AdresInfo()));
            }

            if (siteList.size() > 1) {
                res.responseBody = getErrorResponse(res, LABEL.LABEL_FLEX_MoreThenOneSite + siteList.size());
                return;
            }

            postalCodeList = siteList[0].Site_Postal_Checks__r;
            callBOP = postalCodeList.isEmpty();
        } else {
            isNewAcc = true;
            //call OLBICO and get the account data without Id for future creation
            OlbicoServiceJSON db = new OlbicoServiceJSON(); //call Olbico REST Endpoint
            db.setBatch();
            db.setRequestRestJson(kvk);
            db.makeReqestRestJson(kvk);// make request
            Account olbicoAcc = db.getAcc();
            //serialize Account object from OLBICO for future insert
            serializeAcc = JSON.serialize(olbicoAcc);
            //check if OLBICO returned an Account object
            if (olbicoAcc == null || String.isNotBlank(olbicoAcc.Name)) {
                res.responseBody = getErrorResponse(res, Label.LABEL_FLEX_NoKvk);
                return;
            }

            //call Olbico REST Endpoint for site creation
            db.setRequestRestJsonStreet(postalcode + ' ' + housenumber);
            db.makeRequestRestJsonStreet(postalcode + ' ' + housenumber);

            if (db.AdresInfo().isEmpty()) {
                res.responseBody = getErrorResponse(res, LABEL.LABEL_FLEX_NoAddress);
                return;
            }

            //create dummy site object for BOP
            siteList = addObjSiteToList(postalcode, houseNo, housenumberExtension);
            //serialize site object from OLBICO for future insert
            serializeSite = JSON.serialize(addObjSiteToListFromBOP(postalcode, houseNo, housenumberExtension, kvk, db.AdresInfo()));
            callBOP = true;
        }

        if (callBOP) {
            postalCodeList.addAll(PostalCodeCheckController.doPostalCheck(siteList, 'dsl'));
            postalCodeList.addAll(PostalCodeCheckController.doPostalCheck(siteList, 'fiber'));
        }

        if (postalCodeList.isEmpty()) {
            res.responseBody = getErrorResponse(res, LABEL.Label_FLEX_NoPostalChecksTryLater);
            return;
        }

        //possible check if BOP not return values
        //return list with the new site postal check list information
        List<SitePostalCodeCheckClass> returnClassList = new List<SitePostalCodeCheckClass>();
        Set<String> deduplicateResults = new Set<String>();
        for (Site_Postal_Check__c spc : postalCodeList) {
            String typeOfAccess = spc.Access_Vendor__c + ' : ' + spc.Technology_Type__c + ' : ' + spc.Accessclass__c + ' : ' + spc.Access_Result_Check__c;
            if (!deduplicateResults.contains(typeOfAccess)) {
                system.debug('##spc: ' + spc);
                SitePostalCodeCheckClass pcc = new SitePostalCodeCheckClass();
                pcc.active                  = spc.Access_Active__c;
                pcc.vendor                  = spc.Access_Vendor__c;
                pcc.accessMaxUpSpeed        = Integer.valueOf(spc.Access_Max_Up_Speed__c);
                pcc.accessMaxDownSpeed      = Integer.valueOf(spc.Access_Max_Down_Speed__c);
                pcc.technologyType          = spc.Technology_Type__c;
                pcc.accessClass             = spc.Accessclass__c;
                pcc.accessInfrastructure    = spc.Access_Infrastructure__c;
                pcc.accessRegion            = spc.Access_Region__c;
                pcc.accessResultCheck       = spc.Access_Result_Check__c;
                pcc.remarks                 = spc.Remark__c;
                pcc.createdDate             = spc.Createddate;
                if (spc.Access_Priority__c != null) {
                    pcc.accessPriority      = Integer.valueOf(spc.Access_Priority__c);
                }
                deduplicateResults.add(typeOfAccess);
                returnClassList.add(pcc);
            }
        }

        //send response to flex
        res.responseBody = Blob.valueOf(JSON.serializePretty(returnClassList));
        System.debug('res.responseBody: ' + res.responseBody.toString());

        //after all callouts are finished create account, site and site postal check when needed in a future method
        if (callBOP) {
            //only when BOP is called the hierarchy for Acc--<Site__c--<Site_Postal_Check__c, because there's new info needed for salesforce
            insertHierarchySite(isNewAcc, existingAccId, serializeAcc , serializeSite , JSON.serialize(siteList), JSON.serialize(postalCodeList));
        }
        return;
    }

    public static Blob getErrorResponse(RestResponse res, String msg) {
        BdsTestCheckPostalCodeKVK.ErrorReturnClass returnClass = new BdsTestCheckPostalCodeKVK.ErrorReturnClass();
        returnClass.errorMessage = msg;
        return Blob.valueOf(JSON.serializePretty(returnClass));
    }

    @future
    public static void insertHierarchySite(Boolean createAcc, String existingAccId, String strJSONAcc, String strJSONSiteOlbico, String strJSONSiteSFDC, String strJSONSPCBOP) {
        List<Site_Postal_Check__c> deserializedSPC = (List<Site_Postal_Check__c>) JSON.deserialize(strJSONSPCBOP, List<Site_Postal_Check__c>.class);
        Id siteId;
        if (createAcc) {
            Account deserializedAcc = (Account) JSON.deserialize(strJSONAcc, Account.class);
            insert deserializedAcc;
            Site__c deserializedSite = (Site__c) JSON.deserialize(strJSONSiteOlbico, Site__c.class);
            deserializedSite.Site_Account__c = deserializedAcc.Id;
            insert deserializedSite;
            siteId = deserializedSite.Id;
        } else {
            List<Site__c> deserializedListSite = (List<Site__c>) JSON.deserialize(strJSONSiteSFDC, List<Site__c>.class);
            // If Site is newly created, first do an insert to get the Id
            if (deserializedListSite[0].Id == null) {
                deserializedListSite[0].Site_Account__c = existingAccId;
                insert deserializedListSite[0];
            }
            siteId = deserializedListSite[0].Id;
        }
        for (Site_Postal_Check__c objSPC : deserializedSPC) {
            objSPC.Access_Site_ID__c = siteId;
        }
        // First check for any existing duplicates (based on UniqueId) and deactivate those
        List<Site_Postal_Check__c> spcsOld = deactivateExistingChecks(siteId, deserializedSPC);
        update spcsOld;
        insert deserializedSPC;
    }

	private static List<Site_Postal_Check__c> deactivateExistingChecks(Id siteId, List<Site_Postal_Check__c> spcList) {
        List<Site_Postal_Check__c> postalChecksToUpdate = new List<Site_Postal_Check__c>();
        String queryString = 	'SELECT Id, Technology_Type__c, Access_Vendor__c, Access_Max_Up_Speed__c, ';
        queryString +=       	'Access_Max_Down_Speed__c, Access_Active__c, Access_Site_ID__c ';
		queryString += 			'FROM Site_Postal_Check__c ';
		queryString += 			'WHERE Access_Active__c = true ';
		queryString += 			'AND Access_Site_ID__c = :siteId ';
		for (Site_Postal_Check__c spc : Database.query(queryString)) {
            for (Site_Postal_Check__c bopSpc : spcList) {
                if (spc.Technology_Type__c == bopSpc.Technology_Type__c &&
                    spc.Access_Vendor__c == bopSpc.Access_Vendor__c &&
                    spc.Access_Max_Up_Speed__c == bopSpc.Access_Max_Up_Speed__c &&
                    spc.Access_Max_Down_Speed__c == bopSpc.Access_Max_Down_Speed__c) {

                    spc.Access_Active__c = false;
                    postalChecksToUpdate.add(spc);
                    break;
                }
            }
		}
		return postalChecksToUpdate;
	}

    public static List<Site__c> addObjSiteToList(String postalcode, Integer housenumber, String housenumberExtension) {
        Site__c objSite = new Site__c();
        objSite.Site_House_Number__c = housenumber;
        objSite.Site_Postal_Code__c = postalcode;
        if (!String.isEmpty(housenumberExtension)) {
            objSite.Site_House_Number_Suffix__c = housenumberExtension;
        }
        return new List<Site__c>{ objSite };
    }

    public static Site__c addObjSiteToListFromBOP(String postalcode, Integer housenumber, String housenumberExtension, String kvk, List<SelectOption> address) {
        Integer i = 0;
        String straat;
        Integer huisnummer;
        String huisletter;
        String huisnummertoevoeging;
        String postcode;
        String woonplaats;
        String[] arrTest = new String[]{};

        for (SelectOption addr : address) {
            String[] addrSplit = addr.getValue().split('\\|');
            if (String.isNotBlank(housenumberExtension) && addrSplit[3].trim() == housenumberExtension.trim()) {
                arrTest = addrSplit;
                break;
            }
        }

        if (arrTest.isEmpty()) {
            // If no house number extension is provided or no match is found, just use the first entry
            arrTest = address[0].getvalue().split('\\|');
        }
        for (String sValue : arrTest) {
            if (i == 0) {
                straat = sValue;
            }
            if (i == 1) {
                huisnummer =  Integer.valueof(sValue.Trim());
            }
            if (i == 2) {
                huisletter = sValue;
            }
            if (i == 3) {
                huisnummertoevoeging = sValue;
            }
            if (i == 4) {
                postcode = sValue;
            }
            if (i == 5) {
                woonplaats = sValue;
            }
            if (i == 6) {
                woonplaats = woonplaats + ' - ' + sValue;
            }
            i++;
        }

        Site__c objSite                      = new Site__c();
        objSite.KVK_Number__c                = kvk;
        objSite.Site_Street__c               = straat;
        objSite.Site_House_Number__c         = huisnummer;
        objSite.Site_House_Number_Suffix__c  = huisletter + ' ' + huisnummertoevoeging;
        objSite.Site_Postal_Code__c          = postcode;
        objSite.Site_City__c                 = woonplaats;
        objSite.Olbico__c                    = true;
        objSite.Name                         = straat;
        return objSite;
    }

    public static List<BdsTestCheckPostalCodeKVK.ErrorMissingFields> validateValues(String postalcode, String housenumber, String kvk) {
        List<BdsTestCheckPostalCodeKVK.ErrorMissingFields> errFields = new List<BdsTestCheckPostalCodeKVK.ErrorMissingFields>();

        if (String.isEmpty(postalcode)) {
            BdsTestCheckPostalCodeKVK.ErrorMissingFields errField = new BdsTestCheckPostalCodeKVK.ErrorMissingFields('postalcode');
            errFields.add(errField);
        }
        if (housenumber == null) {
            BdsTestCheckPostalCodeKVK.ErrorMissingFields errField = new BdsTestCheckPostalCodeKVK.ErrorMissingFields('housenumber');
            errFields.add(errField);
        }
        if (String.isEmpty(kvk)) {
            BdsTestCheckPostalCodeKVK.ErrorMissingFields errField = new BdsTestCheckPostalCodeKVK.ErrorMissingFields('kvk');
            errFields.add(errField);
        }

        return errFields;
    }

    global class ErrorReturnClass {
        public Boolean isSuccess = false;
        public String errorMessage = System.Label.BDS_API_Json_incomplete;
        public List<ErrorMissingFields> missingFields;
    }

    global class ErrorMissingFields {
        public String field;
        public ErrorMissingFields(String fieldName) {
            field = fieldName;
        }
    }

    global class SitePostalCodeCheckClass {
        public String vendor;
        public Integer accessMaxUpSpeed;
        public Integer accessMaxDownSpeed;
        public String technologyType;
        public String accessClass;
        public String accessInfrastructure;
        public String accessRegion;
        public Integer accessPriority;
        public String accessResultCheck;
        public String remarks;
        public Boolean active;
        public Datetime createdDate;
    }
}