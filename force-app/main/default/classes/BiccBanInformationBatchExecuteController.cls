public with sharing class BiccBanInformationBatchExecuteController {

	public BiccBanInformationBatchExecuteController(ApexPages.StandardSetController ignored) {

	}

	public PageReference callBatch(){
		BiccBanInformationBatch controller = new BiccBanInformationBatch();
		database.executebatch(controller);
		Schema.DescribeSObjectResult prefix = BICC_BAN_Information__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}

	public PageReference callBatchChain(){
		BiccBanInformationBatch controller = new BiccBanInformationBatch();
		controller.chain = true;
		database.executebatch(controller);
		Schema.DescribeSObjectResult prefix = BICC_BAN_Information__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}
}