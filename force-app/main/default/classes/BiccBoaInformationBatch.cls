global class BiccBoaInformationBatch implements Database.Batchable <sObject>, Database.Stateful{

	global final map<String,String> biccBanFieldMapping = SyncUtil.fullMapping('BICC_BOA_Information__c -> Ban__c').get('BICC_BOA_Information__c -> Ban__c');
	global final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	global final Schema.SObjectType biccSchema = schemaMap.get('BICC_BOA_Information__c');
	global final map<String, Schema.SObjectField> biccFieldMap = biccSchema.getDescribe().fields.getMap();
	global final set<String> queriedFields = SyncUtil.getQueriedFields(biccBanFieldMapping);
	global final list<String> sortList = SyncUtil.getSortedList(biccFieldMap);
	global String header = SyncUtil.getHeader(sortList, queriedFields);
	global String dataError = '';
	global Boolean chain = false;

	global Database.QueryLocator start(Database.BatchableContext BC){

		//Create a dynamic query with info from the Field Mapping Object
		String query = 'SELECT ';
		set<String> testset = new set<String>();
		testset.addall(biccBanFieldMapping.values());
		for (String biccField : testset) {
			query += biccField + ',';
		}
		query = query.subString(0, query.length() - 1);
		query += ' FROM BICC_BOA_Information__c';

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<BICC_BOA_Information__c> scope){

		if(Job_Management__c.getOrgDefaults().Cancel_Batch__c == true){
			System.abortJob(BC.getJobId());
		}

		map<String, BICC_BOA_Information__c> scopeMap = new map<String, BICC_BOA_Information__c>();
		list<BICC_BOA_Information__c> biccErrorList = new list<BICC_BOA_Information__c>();
		list<BICC_BOA_Information__c> biccDeleteList = new list<BICC_BOA_Information__c>();
		list<Ban__c> banList = new list<Ban__c>();
		map<String, Dealer_Information__c> ownerMap = new map<String, Dealer_Information__c>();

		//Put scope in a map for deletion
		for(BICC_BOA_Information__c bicc : scope){
			Boolean match = StringUtils.checkBan(bicc.BAN__c);
			if(match){
				scopeMap.put(bicc.BAN__c, bicc);
				ownerMap.put(bicc.Account_owner_cd__c.removeStart('00'), null);
			} else {
				bicc.Error__c = 'The BAN number is not valid';
				biccErrorList.add(bicc);
			}
		}

		//Get owner Id's
		for(Dealer_Information__c di : [SELECT Id, Dealer_code__c, Contact__r.UserId__c, Name FROM Dealer_Information__c WHERE Dealer_code__c in: ownerMap.keySet()]){
			if(di.Contact__r.UserId__c != null){
				ownerMap.put(di.Dealer_code__c, di);
			}
		}

		//Create Bans from BICC
		User owner = [SELECT Id FROM User WHERE Name =: 'Vodafone'];
		for (BICC_BOA_Information__c biccBan : scopeMap.values()) {
			Ban__c ban = new Ban__c();
			for (String banField : biccBanFieldMapping.keySet()) {
				String biccBanField = biccBanFieldMapping.get(banField);
				Object biccBanValue = biccBan.get(biccBanField);
				ban.put(banField,biccBanValue);
			}
			String dc = biccBan.Account_owner_cd__c.removeStart('00');
			if(ownerMap.get(dc) != null){
				ban.Ownerid = ownerMap.get(dc).Contact__r.UserId__c;
				ban.Dealer_Name__c = ownerMap.get(dc).Name;
			} else {
				ban.Ownerid = owner.Id;
			}
			banList.add(ban);
		}

		//Upsert Accounts in the database
		list<Database.UpsertResult> UR = Database.upsert(banList, Ban__c.Fields.Name, false);
		for (Integer i = 0; i < UR.size(); i++) {
			if(UR[i].isSuccess() && !UR[i].isCreated()){
				biccDeleteList.add(scopeMap.get(banList[i].Name));
			} else {
				scopeMap.get(banList[i].Name).Error__c = 'Error while creating Ban';
				biccErrorList.add(scopeMap.get(banList[i].Name));
			}
		}

		for(BICC_BOA_Information__c error : biccErrorList){
			for(String field : sortList){
				if(queriedFields.contains(field)){
					dataError += '"' + error.get(field) + '",';
				}
			}
			dataError += '"' + error.Error__c + '"\n';
		}

		//Delete succesful matches from BICC
		delete biccErrorList;
		delete biccDeleteList;
		
	}

	global void finish(Database.BatchableContext BC){

		//Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							TotalJobItems
						  FROM
							AsyncApexJob
						  WHERE
							Id =: BC.getJobId()];

		// Send an email to EBUSalesForce.nl@vodafone.com notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		header = header.subString(0, header.length() - 1) + ',"Error__c"\n';
		Blob b = blob.valueOf(header + dataError);
		Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
		efa.setFileName('BiccBoaInformationErrors.csv');
		efa.setBody(b);

		String[] toAddresses = new String[] {'EBUSalesForce.nl@vodafone.com'};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Bicc Boa Information database import ' + a.Status);
		mail.setPlainTextBody('The Bicc Boa Information database import job processed ' + a.TotalJobItems +
							  ' batches with '+ a.NumberOfErrors + ' failures.');
		mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

		if(chain == true){
			BiccVodacomDataBanBatch controller = new BiccVodacomDataBanBatch();
			controller.chain = true;
			database.executebatch(controller);
		}
	}
}