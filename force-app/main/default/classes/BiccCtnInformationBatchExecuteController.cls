public with sharing class BiccCtnInformationBatchExecuteController {

	public BiccCtnInformationBatchExecuteController(ApexPages.StandardSetController ignored) {

	}

	public PageReference callBatch(){
		BiccCtnInformationBatch controller = new BiccCtnInformationBatch();
		database.executebatch(controller, 100);  //SP: added batching in parts of 100, so the heap limit does not get reached
		Schema.DescribeSObjectResult prefix = BICC_CTN_Information__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}
}