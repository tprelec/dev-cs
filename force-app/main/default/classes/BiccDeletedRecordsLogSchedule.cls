global class BiccDeletedRecordsLogSchedule implements Schedulable{
	
	// use this line to start the schedule (on an hourly basis)
	// System.schedule('Hourly BICC Deleted Records Log', '0 0 * * * ?', new BiccDeletedRecordsLogSchedule(UserInfo.getSessionId()) ); 
	// or ad-hoc:  new BiccDeletedRecordsLogSchedule(UserInfo.getSessionId()).execute();

	private String sessionId;
	private Datetime startDateTime;
	private Datetime endDateTime;

    global BiccDeletedRecordsLogSchedule( String sessionId, DateTime startDateTime, DateTime endDateTime ) {
        this.sessionId = sessionId;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

	global void execute(SchedulableContext sc){

		runDeletedRecordsCallout(sessionId, startDateTime, endDateTime);
	}
	global void execute(){

		runDeletedRecordsCallout(sessionId, startDateTime, endDateTime);
	}	


 
    @future(callout=true)
    public static void runDeletedRecordsCallout(String sessionId,DateTime startDateTime, DateTime endDateTime) {

    	Set<String> biccObjects = new Set<String>{
			'Dealer_Information__c',
			'Lead',
			'Contact',
			'Account',
			'Opportunity',
			'OpportunityLineItem',
			'Product2',
			//'User',
			//'UserRole',
			'PriceBookEntry',
			'Ban__c',
			'Site__c',
			'VF_Contract__c',
			'Contracted_Products__c',
			'Order__c',
			'Campaign',
			'CampaignMember',
			//'Period',
			//'Recordtype',
			'Case',
			'Sales_Target__c',
			'Role_Segment_Mapping__c'			
		};

		List<String> bdrToUpsert = new List<String>();

		Job_Management__c jm = Job_Management__c.getOrgDefaults();

		DateTime thisRunStartTime = startDateTime;
		if(startDateTime == null){
			DateTime lastRunEndTime = jm.BICC_Deleted_Records_Last_run__c;
			// if this is the first time this runs on this instance, query the last 24 hrs only
			if(lastRunEndTime == null) lastRunEndTime = system.now().addHours(-24);
			// in order to be sure not to miss any records, create an overlap
			thisRunStartTime = lastRunEndTime.addMinutes(-5);
		}

		DateTime thisRunEndTime = endDateTime;
		if(endDateTime == null){
			thisRunEndTime = system.now();
		}


		//system.debug(lastRunEndTime);
		//system.debug(thisRunEndTime);
		//system.debug(DateTime.parse(lastRunEndTime.format()));
		//system.debug(DateTime.parse(thisRunEndTime.format()));

		// convert to utc for the getDeleted function
		DateTime utcStartTime = DateTime.parse(thisRunStartTime.format());
		DateTime utcEndTime = DateTime.parse(thisRunEndTime.format());
		system.debug(utcStartTime);
		system.debug(utcEndTime);

		 
		for(String objectName : biccObjects){    
			system.debug(objectName);

			for(Database.DeletedRecord deletedRecord : Database.getDeleted(objectName,utcStartTime,utcEndTime).getDeletedRecords()){
				bdrToUpsert.add(deletedRecord.getId()+''+deletedRecord.getDeletedDate());
			}
		}    
		
		//system.debug(bdrToUpsert);
		// remove the ones that already exist (to save some DML's) 
		/*for(BICC_Deleted_Records_Log__c bdrl : [Select Id, Name From BICC_Deleted_Records_Log__c Where Name in :bdrToUpsert.KeySet()]){
			bdrToUpsert.remove(bdrl.Name);
		}*/

		//List<List<BICC_Deleted_Records_Log__c>> bdrToInsert = new List<List<BICC_Deleted_Records_Log__c>>();
		//List<BICC_Deleted_Records_Log__c> tempList = new List<BICC_Deleted_Records_Log__c>();

		/*for(BICC_Deleted_Records_Log__c bdr : bdrToUpsert.values()){
			
			tempList.add(bdr);
			if(tempList.size() > 9999){
				bdrToInsert.add(tempList.clone());
				tempList.clear();
			}
		}*/
		// insert last list
		//if(!tempList.isEmpty()) bdrToInsert.add(tempList);
		//system.debug(bdrToInsert);
		if(!bdrToUpsert.isEmpty()){
			// then call the enqueue job with the list of lists
			System.enqueueJob(new BiccDeletedRecordsLogUpdateQueue(bdrToUpsert));
		}
		
		// update the lastruntime
		jm.BICC_Deleted_Records_Last_run__c = thisRunEndTime;
		upsert jm;
	}

  
}