public class BiccDeletedRecordsLogUpdateQueue implements Queueable {

	public List<String> objectsToUpsert;

	public BiccDeletedRecordsLogUpdateQueue (List<String> objectsToUpsert) {
	   this.objectsToUpsert = objectsToUpsert;
	}

	public void execute(QueueableContext context) {
	    system.debug(objectsToUpsert);
	    system.debug(objectsToUpsert.size());
	    List<BICC_Deleted_Records_Log__c> bdrToUpsert = new List<BICC_Deleted_Records_Log__c>();

	    Integer totalSize = objectsToUpsert.size();
	    Integer maxSize = totalSize; // 3
	    if(maxSize>10000) maxSize = 10000;

	    for(Integer i=totalSize;i>(totalSize-maxSize);i--){
	    	// example: 0036E00000EJ8nLQAT2017-07-12 00:01:20
	    	Id recordId = objectsToUpsert[i-1].subString(0,18);
	    	String timeStamp = objectsToUpsert[i-1].subString(18,37);
	    	// create biccdeletedrecordslog
	    	bdrToUpsert.add(new BICC_Deleted_Records_Log__c(
					Object__c = recordId.getSobjectType().getDescribe().getName(),
					Timestamp__c = DateTime.valueOfGmt(timeStamp),
					Record_Id__c = recordId,
					Name = recordId+''+timeStamp
					)
				);

	    }
	    system.debug(objectsToUpsert);
	    system.debug(totalSize);
	    // then remove them from the list
	    for(Integer i=totalSize;i>(totalSize-maxSize);i--){
	    	objectsToUpsert.remove(i-1);
	    }

	    upsert bdrToUpsert Name;

	    // schedule a new job for the rest
	    if(!objectsToUpsert.isEmpty()){
	    	System.enqueueJob(new BiccDeletedRecordsLogUpdateQueue(objectsToUpsert));		      
	    }
	}
}