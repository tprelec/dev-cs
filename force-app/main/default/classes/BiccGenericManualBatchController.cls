public without sharing class BiccGenericManualBatchController {
    
    ApexPages.StandardSetController controller;
    BiccGenericBatch biccBatch;
    String objectName;

    public BiccGenericManualBatchController (ApexPages.StandardSetController controller) {
        
        this.controller = controller;

        objectName = controller.getRecord().getSObjectType().getDescribe().getName();

        biccBatch = new BiccGenericBatch(objectName);

        List<String> fieldList = new List<String>();
        for (String destinationField : biccBatch.biccFieldMapping.keySet()) {
            fieldList.add(biccBatch.biccFieldMapping.get(destinationField).Source_Field_Name__c);
        }

        if (!test.isRunningTest()) {
            controller.addFields(fieldList);
            controller.addFields(biccBatch.listOfExtraFieldsBasedOnObjectType(objectName));           
        }
    }


    public PageReference updateBiccSobjectBatch(){
        Integer batchSize=200;
        if (objectName=='BICC_Account') batchSize=100;

        Database.executeBatch(biccBatch, batchSize);
        return new pageReference('/' + biccBatch.sourceObjectSchema.getDescribe().getKeyPrefix());
    }

    public PageReference chainBatch(){
        biccBatch.chain=true;
        // batch size cannot be greated than 100 for BICC_Account which is the first job (due to callouts)
        Database.executeBatch(biccBatch, 100);
        return new pageReference('/' + biccBatch.sourceObjectSchema.getDescribe().getKeyPrefix());
    }    

    public PageReference updateBiccSobjectSelected(){

        List<Sobject> myList = new List<Sobject>();

        myList = controller.getSelected();

        biccBatch.processRecords(myList);
        
        return new pageReference('/' + biccBatch.sourceObjectSchema.getDescribe().getKeyPrefix());
    }
}