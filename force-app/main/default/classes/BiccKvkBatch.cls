global class BiccKvkBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

	global final map<String,String> biccBanFieldMapping = SyncUtil.fullMapping('BICC_BAN_Information__c -> Ban__c').get('BICC_BAN_Information__c -> Ban__c');
	global final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	global final Schema.SObjectType biccSchema = schemaMap.get('BICC_BAN_Information__c');
	global final map<String, Schema.SObjectField> biccFieldMap = biccSchema.getDescribe().fields.getMap();
	global final set<String> queriedFields = SyncUtil.getQueriedFields(biccBanFieldMapping);
	global final list<String> sortList = SyncUtil.getSortedList(biccFieldMap);
	global String header = SyncUtil.getHeader(sortList, queriedFields);
	global String dataError = '';
	global Integer count = 0;
	global Boolean chain = false;

	global Database.QueryLocator start(Database.BatchableContext BC) {

		String error = 'New Ban with not existing KVK';

		//Create a dynamic query with info from the Field Mapping Object
		String query = 'SELECT ';
		set<String> testset = new set<String>();
		testset.addall(biccBanFieldMapping.values());
		for (String biccField : testset) {
			query += biccField + ',';
		}
		query = query.subString(0, query.length() - 1);
		query += ' FROM BICC_BAN_Information__c WHERE Error__c =: error LIMIT 100';

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<BICC_BAN_Information__c> scope) {

		if(Job_Management__c.getOrgDefaults().Cancel_Batch__c == true){
			System.abortJob(BC.getJobId());
		}

		count = scope.size();
		map<String, BICC_BAN_Information__c> scopeMap = new map<String, BICC_BAN_Information__c>();
		for(BICC_BAN_Information__c bicc : scope){
			scopeMap.put(bicc.COC_Number__c, bicc);
		}

		OlbicoService db = new OlbicoService();

		db.setBatch();

		list<Account> dnbAccounts = new list<Account>();
		list<BICC_BAN_Information__c> biccErrorList = new list<BICC_BAN_Information__c>();
		list<BICC_BAN_Information__c> biccDeleteList = new list<BICC_BAN_Information__c>();
		set<String> kvkSet = new set<String>();

		for(BICC_BAN_Information__c bicc : scope){
			db.setRequest(bicc.COC_Number__c);
			db.makeRequest(bicc.COC_Number__c);
			if(db.getAcc() != null){
				dnbAccounts.add(db.getAcc());
			} else {
				kvkSet.add(bicc.COC_Number__c);
			}
		}

		//Create new Accounts for kvkNumbers
		for(String kvk : kvkSet){
			Account acc = new Account(Status__c = 'Unverified',
									  Name = scopeMap.get(kvk).BAN_name__c,
									  KVK_Number__c = kvk);
			dnbAccounts.add(acc);
		}

		list<Database.SaveResult> UR = Database.insert(dnbAccounts, false);
		for (Integer i = 0; i < UR.size(); i++) {
			if(!UR[i].isSuccess()){
				scopeMap.get(dnbAccounts[i].KVK_Number__c).Error__c = 'Error while inserting the Account from D&B';
				biccErrorList.add(scopeMap.get(dnbAccounts[i].KVK_Number__c));
				for(Database.Error err : UR[i].getErrors()) {
					System.debug('The following error has occurred.');
					System.debug(err.getStatusCode() + ': ' + err.getMessage());
					System.debug('Account fields that affected this error: ' + err.getFields());
				}
			} else {
				scopeMap.get(dnbAccounts[i].KVK_Number__c).Error__c = '';
				biccDeleteList.add(scopeMap.get(dnbAccounts[i].KVK_Number__c));
			}
		}

		for(BICC_BAN_Information__c error : biccErrorList){
			for(String field : sortList){
				if(queriedFields.contains(field)){
					dataError += '"' + error.get(field) + '",';
				}
			}
			dataError += '"' + error.Error__c + '","' + error.COC_Number__c + '"\n';
		}

		update biccDeleteList;
		delete biccErrorList;
	}

	global void finish(Database.BatchableContext BC) {

		if(count == 100){
			BiccKvkBatch controller = new BiccKvkBatch();
			controller.dataError = dataError;
			controller.chain = chain;
			Database.executeBatch(controller);
		} else {
			//Get the batch job for reference in the email.
			AsyncApexJob a = [SELECT
								Status,
								NumberOfErrors,
								TotalJobItems
							  FROM
								AsyncApexJob
							  WHERE
								Id =: BC.getJobId()];

			// Send an email to the running user notifying of job completion.
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

			header = header.subString(0, header.length() - 1) + ',"Error","KVK Number"\n';
			Blob b = blob.valueOf(header + dataError);
			Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
			efa.setFileName('BiccKVKErrors.csv');
			efa.setBody(b);

			String[] toAddresses = new String[] {UserInfo.getUserEmail()};
			mail.setToAddresses(toAddresses);
			mail.setSubject('Bicc KVK Batch ' + a.Status);
			mail.setPlainTextBody('The Bicc KVK job processed ' + a.TotalJobItems +
								  ' batches with '+ a.NumberOfErrors + ' failures.');
			mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

			BiccBanInformationBatch controller = new BiccBanInformationBatch();
			controller.chain = chain;
			database.executebatch(controller);
		}
	}
}