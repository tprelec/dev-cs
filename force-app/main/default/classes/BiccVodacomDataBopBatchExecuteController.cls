public with sharing class BiccVodacomDataBopBatchExecuteController {

	public BiccVodacomDataBopBatchExecuteController(ApexPages.StandardSetController ignored) {

	}

	public PageReference callBatch(){
		BiccVodacomDataBopBatch controller = new BiccVodacomDataBopBatch();
		database.executebatch(controller, 100);
		Schema.DescribeSObjectResult prefix = BICC_Vodacom_Data_BOP__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}
}