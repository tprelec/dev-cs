@isTest
private class BillingAccountTriggerHandlerTest {
    
    private static String accDefBillingName = 'AccWithOneDef';
    private static String accUpdateName = 'AccWithTwoDef';
    
    @testsetup
    private static void setupTestData()
    {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;

        List<Account> accounts = new List<Account>();
        Account accDefBilling = LG_GeneralTest.CreateAccount(accDefBillingName, '12345678', 'Ziggo', false);
        Account accUpdate = LG_GeneralTest.CreateAccount(accUpdateName, '12345679', 'Ziggo', false);
        accounts.add(accDefBilling);
        accounts.add(accUpdate);
        insert accounts;        

        List<csconta__Billing_Account__c> billAccounts = new List<csconta__Billing_Account__c>();
        billAccounts.add(LG_GeneralTest.createBillingAccount('SFDT-59 Bill1', accDefBilling.Id, true, false));
        billAccounts.add(LG_GeneralTest.createBillingAccount('SFDT-59 Bill2', accUpdate.Id, false, false));
        billAccounts.add(LG_GeneralTest.createBillingAccount('SFDT-59 Bill3', accUpdate.Id, false, false));
        insert billAccounts;

        noTriggers.Flag__c = false;
        upsert noTriggers;
    }
    
    private static testmethod void testSetDefaultBillingAccount()
    {
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE Name = :accDefBillingName OR Name = :accUpdateName];
        Account accDefBilling;
        Account accUpdate;
        
        csconta__Billing_Account__c updateBillAcc = [SELECT Id, csconta__Financial_Account_Number__c, LG_Default__c 
                                                        FROM csconta__Billing_Account__c
                                                        WHERE csconta__Financial_Account_Number__c = 'SFDT-59 Bill2'];
        
        for (Account account : accounts)
        {
            if (account.Name.equals(accDefBillingName))
            {
                accDefBilling = account;
            }
            else if (account.Name.equals(accUpdateName))
            {
                accUpdate = account;
            }
        }
        
        Test.startTest();
            List<csconta__Billing_Account__c> billAccounts = new List<csconta__Billing_Account__c>();
            billAccounts.add(LG_GeneralTest.createBillingAccount('SFDT-59 Bill22', accDefBilling.Id, true, false));
            billAccounts.add(LG_GeneralTest.createBillingAccount('SFDT-59 Bill13', accDefBilling.Id, true, false));
            updateBillAcc.LG_Default__c = true;
            billAccounts.add(updateBillAcc);
            upsert billAccounts;
        Test.stopTest();
        
        for (Account account : [SELECT Id, Name, (SELECT csconta__Financial_Account_Number__c
                                FROM csconta__Billing_Accounts__r WHERE LG_Default__c = true)
                                FROM Account WHERE Id IN :accounts])
        {
            System.assertEquals(1, account.csconta__Billing_Accounts__r.size(), 'There should be only one default Billing Account per Account');
            if (account.Name.equals(accUpdateName))
            {
                System.assertEquals('SFDT-59 Bill2', account.csconta__Billing_Accounts__r[0].csconta__Financial_Account_Number__c,
                                    'Default Billing Account should be SFDT-59 Bill2');
            }
            else
            {
                System.assertNotEquals('SFDT-59 Bill1', account.csconta__Billing_Accounts__r[0].csconta__Financial_Account_Number__c,
                                    'Default Billing Account should not be SFDT-59 Bill1 anymore');
            }
        }
    }
     //Test for valid House no. and valid Postal code
    private static testmethod void testPopulateAddress1()
    {
    List<LG_PostalCode__c> postCodes=LG_TestDataUtility.createPostalCodes();
    insert postCodes;
     List<Account> account_List=new List<Account>(); 
    Account customerAccount=LG_TestDataUtility.CreateAccount('Test customer Account', '00001235','Ziggo','9','9999 XK','9','9999 XK');
    Account customerAccount1=LG_TestDataUtility.CreateAccount('Test customer Account1', '00001236','Ziggo','9','9999 XK','9','9999 XK');
    account_List.add(customerAccount);
    account_List.add(customerAccount1);
    insert account_List;
    List<csconta__Billing_Account__c> billAccount_List=new List<csconta__Billing_Account__c>();
    csconta__Billing_Account__c billingAccountTest=LG_TestDataUtility.createBillingAccount('Bill1',customerAccount.id,'9','9999 XK');
    csconta__Billing_Account__c billingAccountTest1=LG_TestDataUtility.createBillingAccount('Bill2',customerAccount1.id,'9','9999 XK');
    billAccount_List.add(billingAccountTest);
    billAccount_List.add(billingAccountTest1);
    insert billAccount_List;
    List<csconta__Billing_Account__c> accFetched = new List<csconta__Billing_Account__c>();
    accFetched.add([Select Id,csconta__Street__c,csconta__Country__c from csconta__Billing_Account__c  where Id =: billingAccountTest.id]);
    accFetched.add([Select Id,csconta__Street__c,csconta__Country__c from csconta__Billing_Account__c  where Id =: billingAccountTest1.id]);
    System.assertEquals('Stiel',accFetched[0].csconta__Street__c);
    System.assertEquals('Stiel',accFetched[1].csconta__Street__c);
    //Added 04-Feb-2019
    billingAccountTest.csconta__Postcode__c = '9997 PR6';
    update billingAccountTest;
    //End
    }
     //Test code for invalid House no. and valid Postal code
    private static testmethod void testPopulateAddress2()
    {
    try{
    List<LG_PostalCode__c> postCodes=LG_TestDataUtility.createPostalCodes();
    insert postCodes;
     List<Account> account_List=new List<Account>(); 
    Account customerAccount=LG_TestDataUtility.CreateAccount('Test customer Account', '00001235','Ziggo','9','9999 XK','9','9999 XK');
    Account customerAccount1=LG_TestDataUtility.CreateAccount('Test customer Account1', '00001236','Ziggo','9','9999 XK','9','9999 XK');
    account_List.add(customerAccount);
    account_List.add(customerAccount1);
    insert account_List;
    List<csconta__Billing_Account__c> billAccount_List=new List<csconta__Billing_Account__c>();
    csconta__Billing_Account__c billingAccountTest=LG_TestDataUtility.createBillingAccount('Bill1',customerAccount.id,'1','9999 XK');
    csconta__Billing_Account__c billingAccountTest1=LG_TestDataUtility.createBillingAccount('Bill2',customerAccount1.id,'1','9999 XK');
    billAccount_List.add(billingAccountTest);
    billAccount_List.add(billingAccountTest1);
    insert billAccount_List;
    List<csconta__Billing_Account__c> accFetched = new List<csconta__Billing_Account__c>();
    accFetched.add([Select Id,csconta__Street__c,csconta__Country__c from csconta__Billing_Account__c  where Id =: billingAccountTest.id]);
    accFetched.add([Select Id,csconta__Street__c,csconta__Country__c from csconta__Billing_Account__c  where Id =: billingAccountTest1.id]);
    }
    catch(exception e)
    {
    System.assertEquals(e.getmessage().contains('House Number is not valid'),true);
    }
    }
    //Test code for valid House no. and invalid Postal code
    private static testmethod void testPopulateAddress3()
    {
    List<LG_PostalCode__c> postCodes=LG_TestDataUtility.createPostalCodes();
    insert postCodes;
     List<Account> account_List=new List<Account>(); 
    Account customerAccount=LG_TestDataUtility.CreateAccount('Test customer Account', '00001235','Ziggo','9','9999 XK','9','9999 XK');
    Account customerAccount1=LG_TestDataUtility.CreateAccount('Test customer Account1', '00001236','Ziggo','9','9999 XK','9','9999 XK');
    account_List.add(customerAccount);
    account_List.add(customerAccount1);
    insert account_List;
    List<csconta__Billing_Account__c> billAccount_List=new List<csconta__Billing_Account__c>();
    csconta__Billing_Account__c billingAccountTest=LG_TestDataUtility.createBillingAccount('Bill1',customerAccount.id,'9','9399 XK');
    csconta__Billing_Account__c billingAccountTest1=LG_TestDataUtility.createBillingAccount('Bill2',customerAccount1.id,'9','9399 XK');
    billAccount_List.add(billingAccountTest);
    billAccount_List.add(billingAccountTest1);
    insert billAccount_List;
    List<csconta__Billing_Account__c> accFetched = new List<csconta__Billing_Account__c>();
    accFetched.add([Select Id,csconta__Street__c,csconta__Country__c from csconta__Billing_Account__c  where Id =: billingAccountTest.id]);
    accFetched.add([Select Id,csconta__Street__c,csconta__Country__c from csconta__Billing_Account__c  where Id =: billingAccountTest1.id]);
    System.assertEquals(null,accFetched[0].csconta__Street__c);
    System.assertEquals(null,accFetched[1].csconta__Street__c);
    }
     //Test code for House no. null and valid Postal code
    private static testmethod void testPopulateAddress4()
    {
    try{
    List<LG_PostalCode__c> postCodes=LG_TestDataUtility.createPostalCodes();
    insert postCodes;
     List<Account> account_List=new List<Account>(); 
    Account customerAccount=LG_TestDataUtility.CreateAccount('Test customer Account', '00001235','Ziggo','9','9999 XK','9','9999 XK');
    Account customerAccount1=LG_TestDataUtility.CreateAccount('Test customer Account1', '00001236','Ziggo','9','9999 XK','9','9999 XK');
    account_List.add(customerAccount);
    account_List.add(customerAccount1);
    insert account_List;
    List<csconta__Billing_Account__c> billAccount_List=new List<csconta__Billing_Account__c>();
    csconta__Billing_Account__c billingAccountTest=LG_TestDataUtility.createBillingAccount('Bill1',customerAccount.id,null,'9999 XK');
    csconta__Billing_Account__c billingAccountTest1=LG_TestDataUtility.createBillingAccount('Bill2',customerAccount1.id,null,'9999 XK');
    billAccount_List.add(billingAccountTest);
    billAccount_List.add(billingAccountTest1);
    insert billAccount_List;
    List<csconta__Billing_Account__c> accFetched = new List<csconta__Billing_Account__c>();
    accFetched.add([Select Id,csconta__Street__c,csconta__Country__c from csconta__Billing_Account__c  where Id =: billingAccountTest.id]);
    accFetched.add([Select Id,csconta__Street__c,csconta__Country__c from csconta__Billing_Account__c  where Id =: billingAccountTest1.id]);
    }
    catch(exception e)
    {
    /*CRQ000000769283*/System.assertEquals(e.getmessage().contains('REQUIRED_FIELD_MISSING'),true);/*CRQ000000769283*/
    }
    }
}