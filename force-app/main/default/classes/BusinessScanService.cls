@RestResource(urlMapping='/business-scan/*')
global with sharing class BusinessScanService {
	@HttpPost
	global static void create() {
		RestRequest req = RestContext.request;
		Map<String, Object> data = (Map<String, Object>) JSON.deserializeUntyped(
			req.requestBody.toString()
		);
		BusinessScanBuilder builder = new BusinessScanBuilder(data);
		RestResponse res = RestContext.response;

		Savepoint sp = Database.setSavepoint();

		try {
			Business_Scan__c scan = builder.buildRecords();

			res.statusCode = 200;
			res.responseBody = Blob.valueOf(JSON.serialize(scan));
		} catch (System.DmlException e) {
			res.statusCode = 400;
			List<BusinessScanException> errs = new List<BusinessScanException>{
				new BusinessScanException(e.getMessage())
			};
			res.responseBody = Blob.valueOf(JSON.serialize(errs));

			Database.rollback(sp);
		} catch (Exception e) {
			res.statusCode = 500;
			List<BusinessScanException> errs = new List<BusinessScanException>{
				new BusinessScanException(e.getMessage())
			};
			res.responseBody = Blob.valueOf(JSON.serialize(errs));

			Database.rollback(sp);
		}
	}

	private class BusinessScanBuilder {
		private Business_Scan__c bs;
		private List<Business_Scan_Q_A__c> bsqaItems;
		private Map<String, Business_Scan_Mapping__mdt> mapping;
		private Map<String, Object> data;
		private Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();

		// business scan object schema and fields map
		private Schema.SObjectType bsSchema = schemaMap.get('Business_Scan__c');
		private Map<String, Schema.SObjectField> bsFieldMap = bsSchema.getDescribe()
			.fields.getMap();

		// business scan Q&A object schema and fields map
		private Schema.SObjectType bsqaSchema = schemaMap.get('Business_Scan_Q_A__c');
		private Map<String, Schema.SObjectField> bsqaFieldMap = bsqaSchema.getDescribe()
			.fields.getMap();

		private Map<String, Schema.SObjectField> currentFieldsMap;

		// mapping of JSON answers field with Business_Scan_Q_A__c type field values
		private Map<String, String> answerTypeMapping = new Map<String, String>{
			'results' => 'Question',
			'statements' => 'Statement'
		};

		// mapping of JSON answers field with mappings from custom metadata
		private Map<String, String> answerFieldMapping = new Map<String, String>{
			'results' => 'Result_Field__c',
			'statements' => 'Statement_Field__c'
		};

		BusinessScanBuilder(Map<String, Object> data) {
			this.bs = new Business_Scan__c();
			this.bsqaItems = new List<Business_Scan_Q_A__c>();
			this.data = data;
			this.setMapping();
		}

		/**
		 * Fetch field mappings from custom metadata type
		 */
		private void setMapping() {
			this.mapping = new Map<String, Business_Scan_Mapping__mdt>();
			List<Business_Scan_Mapping__mdt> mappings = [
				SELECT
					DeveloperName,
					Business_Scan_Field__c,
					Contact_Field__c,
					Address_Field__c,
					Result_Field__c,
					Statement_Field__c
				FROM Business_Scan_Mapping__mdt
			];

			for (Business_Scan_Mapping__mdt m : mappings) {
				this.mapping.put(m.DeveloperName, m);
			}
		}

		/**
		 * Build Business_Scan__c and Business_Scan_Q_A__c records
		 */
		public Business_Scan__c buildRecords() {
			this.buildBusinessScan();
			this.buildAnswers();

			return this.bs;
		}

		/**
		 * Build Business_Scan__c record
		 */
		private void buildBusinessScan() {
			// set business scan fields map as current one
			this.currentFieldsMap = this.bsFieldMap;

			// iterate each property in parsed JSON and execute function that creates BS record
			for (String fieldName : this.data.keySet()) {
				Object fieldData = this.data.get(fieldName);

				this.buildBusinessScanFields(fieldName, fieldData);
			}

			insert this.bs;
		}

		/**
		 * Build Business_Scan__c record fields from JSON properties
		 */
		private void buildBusinessScanFields(String fieldName, Object fieldData) {
			if (fieldName == 'address') {
				this.setBusinessScanNestedValues(
					(Map<String, Object>) fieldData,
					'Address_Field__c'
				);
			} else if (fieldName == 'contact') {
				this.setBusinessScanNestedValues(
					(Map<String, Object>) fieldData,
					'Contact_Field__c'
				);
			} else if (fieldName == 'recommendation') {
				this.setBusinessScanNestedValues(
					(Map<String, Object>) fieldData,
					'Business_Scan_Field__c'
				);
			} else if (fieldName != 'results' && fieldName != 'statements') {
				this.setBusinessScanFieldValue(fieldName, 'Business_Scan_Field__c', fieldData);
			}
		}

		/**
		 * Build Busines_Scan_Q_A__c records
		 */
		private void buildAnswers() {
			// set business scan Q&A fields map as current one
			this.currentFieldsMap = this.bsqaFieldMap;

			this.buildAnswersList('results');
			this.buildAnswersList('statements');

			insert this.bsqaItems;
		}

		/**
		 * Process list of questions and answers and execute building of each record
		 */
		private void buildAnswersList(String type) {
			List<Object> answers = (List<Object>) this.data.get(type);

			for (Object answer : answers) {
				Map<String, Object> answerData = (Map<String, Object>) answer;
				this.buildAnswer(type, answerData);
			}
		}

		/**
		 * Build single Business_Scan_Q_A__c record, populate its fields and add it to list to be saved
		 */
		private void buildAnswer(String type, Map<String, Object> answerData) {
			Business_Scan_Q_A__c bsqa = new Business_Scan_Q_A__c();
			bsqa.Business_Scan__c = this.bs.Id;
			bsqa.Type__c = this.answerTypeMapping.get(type);

			for (String fieldName : answerData.keySet()) {
				Object fieldData = answerData.get(fieldName);
				String mappedFieldName = this.getMappedFieldName(
					fieldName,
					this.answerFieldMapping.get(type)
				);
				if (mappedFieldName != null) {
					Object fieldValue = this.formatFieldValue(mappedFieldName, fieldData);
					bsqa.put(mappedFieldName, fieldValue);
				}
			}

			this.bsqaItems.add(bsqa);
		}

		/**
		 * Set field values on Business_Scan__c record from nested JSON properties
		 */
		private void setBusinessScanNestedValues(
			Map<String, Object> data,
			String fieldsMappingColumn
		) {
			for (String fieldName : data.keySet()) {
				Object fieldData = data.get(fieldName);
				this.setBusinessScanFieldValue(fieldName, fieldsMappingColumn, fieldData);
			}
		}

		/**
		 * Get field mapping and set formatted field value
		 */
		private void setBusinessScanFieldValue(
			String fieldName,
			String fieldsMappingColumn,
			Object fieldData
		) {
			String mappedFieldName = this.getMappedFieldName(fieldName, fieldsMappingColumn);
			if (mappedFieldName != null) {
				Object fieldValue = this.formatFieldValue(mappedFieldName, fieldData);
				this.bs.put(mappedFieldName, fieldValue);
			}
		}

		private String getMappedFieldName(String fieldName, String fieldsMappingColumn) {
			Business_Scan_Mapping__mdt mappedField = this.mapping.get(fieldName);

			if (mappedField != null) {
				return (String) mappedField.get(fieldsMappingColumn);
			}

			return null;
		}

		/**
		 * Format field values by its type
		 */
		private Object formatFieldValue(String fieldName, Object fieldValue) {
			Object val = fieldValue;
			String fieldType = this.currentFieldsMap.get(fieldName).getDescribe().getType().name();

			if (fieldType == 'CURRENCY' || fieldType == 'DOUBLE') {
				val = Decimal.valueOf(String.valueOf(fieldValue));
			} else if (fieldType == 'INTEGER' || fieldType == 'NUMBER') {
				val = Integer.valueOf(fieldValue);
			} else if (fieldType == 'BOOLEAN') {
				val = Boolean.valueOf(fieldValue);
			} else if (fieldType == 'DATE') {
				val = Date.valueOf(fieldValue);
			} else if (fieldType == 'MULTIPICKLIST') {
				List<Object> fieldData = (List<Object>) fieldValue;
				List<String> mpValues = new List<String>();
				for (Object mpv : fieldData) {
					mpValues.add(String.valueOf(mpv));
				}
				val = String.join(mpValues, ';');
			} else {
				val = String.valueOf(fieldValue);
			}

			return val;
		}
	}

	public class BusinessScanException extends Exception {
	}
}