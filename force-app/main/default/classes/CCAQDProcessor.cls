/**
 * Created by bojan.zunko on 30/06/2020.
 */

global with sharing class CCAQDProcessor {
    global static final String AQDFileName = 'AdditionalQualificationData.json';

    global static Map<ID, CCAQDStructure> extractPCAqdData(List<Id> basketIds) {
        Map<Id, cscfga__Product_Configuration__c> pcsInBasketWithAttachments = new Map<ID, cscfga__Product_Configuration__c>(
            [
                SELECT Id, Name, cscfga__charges__c, cscfga__discounts__c, cscfga__Quantity__c
                FROM cscfga__Product_Configuration__c
                WHERE cscfga__Product_Basket__c IN :basketIds
            ]
        );

        Map<ID, CCAQDStructure> pcsWithParsedAQD = new Map<Id, CCAQDStructure>();

        for (Attachment aqd : [
            SELECT ID, Name, Body, ParentId
            FROM Attachment
            WHERE ParentId IN :pcsInBasketWithAttachments.keySet() AND Name = :AQDFileName
        ]) {
            System.debug(aqd.Body.toString());
            CCAQDStructure aqdStructure = (CCAQDStructure) JSON.deserialize(
                aqd.Body.toString(),
                CCAQDStructure.class
            );
            pcsWithParsedAQD.put(aqd.ParentId, aqdStructure);
        }

        return pcsWithParsedAQD;
    }

    global class CCAQDStructure {
        global List<CCCharge> charges;
        global List<CCDiscount> discounts;
        global List<OEEntry> OE;
        //global Map<String, String> customData;
        global String CS_QuantityStrategy;
        global List<ConnectionType> connectionTypes;
        global Integer csQuantity;
    }
    
    global class CustomData {
        global List<AppliedPromotion> appliedPromotions;
    }
    
    global class AppliedPromotion {
        global String promoId;
        global String billingCode;
    }

    global class ConnectionType {
        global String type;
        // online sends Quantity as Integer
        global Integer quantity;
    }

    /*These need to be described with override Discounts if they exist*/

    global class OEEntry {
        global String commercialConfigurationID;
        global List<AttributeValuePair> attributes;
    }

    /*These need to be described with override Discounts if they exist*/
    global class AttributeValuePair {
        global String attributeName;
        global String attributeValue;
    }

    /*These need to be described with override Discounts if they exist*/
    global class CCCharge {
        global String source;
        global Decimal listPrice;
        global Decimal salesPrice;
        global Decimal rootPrice;
        global String name; // this is either LineItemAttr or charge name
        global String chargeType;
        global String description;
        global Boolean isProductLevelPricing;
        global Integer quantity;
    }

    global class CCDiscount {
        global String source;
        global String type; //percetage|absolute|override
        global Decimal amount;
        global String discountPrice; //list | sales
        global String version; //will be 2-0-0
        global String recordType; //single
        global String discountCharge; //name of charge being discount - either LIA name or Charge set through override
        global String chargeType;
    }
}