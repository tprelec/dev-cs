/**
 * Created by bojan.zunko on 30/06/2020.
 */

global with sharing class CCOrderRequestProcessing implements csam.ObserverApi.IObserver {

    global CCOrderRequestProcessing() {

    }

    global void execute(List<Id> basketIds) {
        if(basketIds.size() > 0) {

            Map<ID,CCAQDProcessor.CCAQDStructure> pcsWithAQD = CCAQDProcessor.extractPCAqdData(basketIds);

            List<SObject> bulkCustomDataStore = new List<SObject>();
            List<Id> rootConfigs = new List<Id>();
            Map<Id, Id> rootPcIdBasketId = new Map<Id, Id>();
            Map<Id, Integer> rootConfigIdQuantity = new Map<Id, Integer>();
            Map<Id, Decimal> basketIdNmbOfDisconnects = new Map<Id, Decimal>();

            for(cscfga__Product_Configuration__c targetPC : [
                    SELECT
                            Id,
                            cscfga__discounts__c,
                            cscfga__Root_Configuration__c,
                            cscfga__Product_Basket__c,
                            Downgrade_allowance__c
                    FROM cscfga__Product_Configuration__c
                    where id in :pcsWithAQD.keySet()
            ]) {

                if(targetPC.cscfga__Root_Configuration__c == null) {
                    rootConfigs.add(targetPC.Id);
                    rootPcIdBasketId.put(targetPC.Id, targetPC.cscfga__Product_Basket__c);
                    rootConfigIdQuantity.put(targetPC.Id, 0);
                }

                CCAQDProcessor.CCAQDStructure workingAQD = pcsWithAQD.get(targetPC.Id);

                /*if (rootConfigIdQuantity.containsKey(targetPC.cscfga__Root_Configuration__c)) {
                    rootConfigIdQuantity.put(targetPC.cscfga__Root_Configuration__c, workingAQD.csQuantity);
                }*/

                //List<SObject> customDataAttach = CustomDataProcessor.processCustomData(targetPC.Id, workingAQD.customData);
                //bulkCustomDataStore.addAll(customDataAttach);

                if (workingAQD.charges == null)
                    workingAQD.charges = new List<CCAQDProcessor.CCCharge>();

                if (workingAQD.discounts == null)
                    workingAQD.discounts = new List<CCAQDProcessor.CCDiscount>();

                workingAQD.CS_QuantityStrategy = ChargesProcessor.QUANTITY_STRATEGY_DECOMPOSITION;

                List<SObject> pricingInformation = ChargesProcessor.processPricing(targetPC, workingAQD.charges, workingAQD.discounts, workingAQD.CS_QuantityStrategy);
                bulkCustomDataStore.addAll(pricingInformation);

                // not needed now - 11.05.2021 written
                //List<SObject> baskets = BasketChargesProcessor.processBasketPricing(basketIds);
                //bulkCustomDataStore.addAll(baskets);
            }

            List<cscfga__Attribute__c> rootConfigAttributes = [SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c, cscfga__Product_Configuration__r.cscfga__Product_Basket__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c IN :rootConfigs];

            Map<Id, String> basketIdContractDurationMobile = new Map<Id, String>();
            Map<Id, Decimal> basketIdConnectionTypesQuantity = new Map<Id, Decimal>(); //Number_of_ctn__c

            if(rootConfigs != null && rootConfigs.size() > 0) {
                for(Id rootConfigId : rootConfigs) {
                    if (pcsWithAQD.get(rootConfigId) != null) {

                        if (pcsWithAQD.get(rootConfigId).connectionTypes != null) {
                            for(CCAQDProcessor.ConnectionType connTypeItem : pcsWithAQD.get(rootConfigId).connectionTypes) {
                                Decimal quantityTmp = Decimal.valueOf(connTypeItem.quantity);
								Integer tmpQuantity = rootConfigIdQuantity.get(rootConfigId);
								rootConfigIdQuantity.put(rootConfigId, (Integer)tmpQuantity + (Integer)quantityTmp);

                                if(basketIdConnectionTypesQuantity.containsKey(rootPcIdBasketId.get(rootConfigId))) {
                                    Decimal quantityMapValue = basketIdConnectionTypesQuantity.get(rootPcIdBasketId.get(rootConfigId));
                                    quantityMapValue += quantityTmp;
                                    basketIdConnectionTypesQuantity.put(rootPcIdBasketId.get(rootConfigId), quantityMapValue);
                                } else {
                                    basketIdConnectionTypesQuantity.put(rootPcIdBasketId.get(rootConfigId), quantityTmp);
                                }
                            }
                        }
                    }
                }
            }

            if (rootConfigAttributes != null && rootConfigAttributes.size() > 0) {
                for(cscfga__Attribute__c attrItem : rootConfigAttributes) {
                    if(attrItem.Name == 'contractDuration') {
                        basketIdContractDurationMobile.put(attrItem.cscfga__Product_Configuration__r.cscfga__Product_Basket__c, attrItem.cscfga__Value__c);
                    }
                }
            }

            List<SObject> baskets = [SELECT Id, Number_of_ctn__c, Contract_duration_Mobile__c, Expected_delivery_date_for_Mobile__c, cscfga__Basket_Status__c, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id IN :basketIds];
            for(SObject basket : baskets) {
                Integer basketContractDuration = 24;
                basket.put('Primary__c', true);
                basket.put('Is_Online_Basket__c', true);
                basket.put('Expected_delivery_date_for_Mobile__c', Date.today());
                basket.put('cscfga__Basket_Status__c', 'Contract created');

                if(basketIdContractDurationMobile.containsKey(basket.Id)) {
                    basketContractDuration = Integer.valueOf(basketIdContractDurationMobile.get(basket.Id));
                    basket.put('Contract_duration_Mobile__c', String.valueOf(basketContractDuration));
                }

                if (basketContractDuration >= 24) {
                    for (Id rootPCId : rootConfigIdQuantity.keySet()) {
                        Integer tmpQuantity = rootConfigIdQuantity.get(rootPCId);
                        Id tmpBasketId = rootPcIdBasketId.get(rootPCId);

                        if (basketIdNmbOfDisconnects.containsKey(tmpBasketId)) {
                            Decimal tmpNmbDisconnects = basketIdNmbOfDisconnects.get(tmpBasketId);
                            Decimal tmpCurrentCalculation = tmpQuantity * 0.1;
                            System.debug('tmpCurrentCalculation - ' + tmpCurrentCalculation);
                            tmpNmbDisconnects += tmpCurrentCalculation.round(System.RoundingMode.HALF_UP);
                            System.debug('tmpNmbDisconnects - ' + tmpNmbDisconnects);
                            basketIdNmbOfDisconnects.put(tmpBasketId, tmpNmbDisconnects);
                        } else {
                            Decimal tmpCurrentCalculation = tmpQuantity * 0.1;
                            System.debug('tmpCurrentCalculation - ' + tmpCurrentCalculation);
                            Decimal tmpNmbDisconnects = tmpCurrentCalculation.round(System.RoundingMode.HALF_UP);
                            System.debug('tmpNmbDisconnects - ' + tmpNmbDisconnects);
                            basketIdNmbOfDisconnects.put(tmpBasketId, tmpNmbDisconnects);
                        }
                    }
                    
                    System.debug('basketIdNmbOfDisconnects - ' + basketIdNmbOfDisconnects);
                }

                if (basketIdConnectionTypesQuantity.containsKey(basket.Id)) {
                    Decimal tmpDisconnect = basketIdNmbOfDisconnects.get(basket.Id) == null ? 0 : basketIdNmbOfDisconnects.get(basket.Id);
                    basket.put('Number_of_ctn__c', basketIdConnectionTypesQuantity.get(basket.Id) - tmpDisconnect);
                    basket.put('Number_of_CTN_Disconnect__c', tmpDisconnect);
                }
            }
            bulkCustomDataStore.addAll(baskets);

            Map<Id, cscfga__Product_Configuration__c> rootPCsForUpdate = new Map<Id, cscfga__Product_Configuration__c>();
            for (Id rootPCId : rootConfigIdQuantity.keySet()) {
				Decimal tmpQuantity = rootConfigIdQuantity.get(rootPCId);
				if (tmpQuantity == 0)
					tmpQuantity = 1;
                cscfga__Product_Configuration__c pcSObject = new cscfga__Product_Configuration__c(Id = rootPCId, cscfga__Quantity__c = rootConfigIdQuantity.get(rootPCId));
                rootPCsForUpdate.put(rootPCId, pcSObject);
            }
            //List<SObject> sobjlist = new List<SObject>(rootPCsForUpdate);
            //bulkCustomDataStore.addAll(sobjlist);

            List<SObject> insertList = new List<SObject>();
            List<SObject> updateList = new List<SObject>();

            for(SObject iter : bulkCustomDataStore) {
                if (iter.Id == null) {
                    insertList.add(iter);
                } else {
                    updateList.add(iter);
                }
            }

            for (Id rootPcId : rootPCsForUpdate.keySet()) {
                Boolean itemFound = false;
                for(SObject iter : updateList) {
                    if (iter.Id == rootPcId) {
                        itemFound = true;
                        iter.put('cscfga__Quantity__c', rootPCsForUpdate.get(rootPcId).cscfga__Quantity__c);
                    }
                }

                if (!itemFound) {
                    updateList.add(rootPCsForUpdate.get(rootPcId));
                }
            }

            insert insertList;
            update updateList;
            Set<Id> basketIdsSet = new Set<Id>();
            basketIdsSet.addAll(basketIds);
            cscfga.ProductConfigurationBulkActions.calculateTotals(basketIdsSet);
        }
    }

    global static void generateCtnRecordsOnlineScenario(List<cscfga__Product_Basket__c> baskets) {
        List<NetProfit_Information__c> npInfoToInsert = new List<NetProfit_Information__c>();
        List<NetProfit_CTN__c> npCtnToInsert = new List<NetProfit_CTN__c>();
        Map<Id, Opportunity> oppsById = new Map<Id, Opportunity>();
        Map<Id, NetProfit_Information__c> netProfitInfoByOppId = new Map<Id, NetProfit_Information__c>();

        for (cscfga__Product_Basket__c basket : baskets) {
            oppsById.put(basket.cscfga__Opportunity__c, null);
        }

        for (NetProfit_Information__c existingNpInfo : [
                SELECT Id, Opportunity__c
                FROM NetProfit_Information__c
                WHERE Opportunity__c IN :oppsById.keySet()
        ]) {
            netProfitInfoByOppId.put(existingNpInfo.Opportunity__c, existingNpInfo);
        }

        oppsById = new Map<Id, Opportunity>(
        [
                SELECT Id, Segment__c, Type_of_service__c
                FROM Opportunity
                WHERE Id IN :oppsById.keySet()
        ]);

        for (Opportunity opp : oppsById.values()) {
            if (
                    opp.Segment__c == 'SoHo' &&
                            opp.Type_of_service__c == 'Mobile' &&
                            !netProfitInfoByOppId.containsKey(opp.Id)
                    ) {
                NetProfit_Information__c npInfo = new NetProfit_Information__c(
                        Opportunity__c = opp.Id,
                        Status__c = 'New'
                );
                npInfoToInsert.add(npInfo);
            }
        }
        System.debug('npInfoToInsert -> ' + npInfoToInsert);
        insert npInfoToInsert;

        for (NetProfit_Information__c npInfo : npInfoToInsert) {
            NetProfitCTNGenerator generator = new NetProfitCTNGenerator(npInfo.Opportunity__c);
            generator.npInfoId = npInfo.Id;
            npCtnToInsert.addAll(generator.generateCTNInfo());
        }

        System.debug('npCtnToInsert -> ' + npCtnToInsert);
        insert npCtnToInsert;
    }

    global void execute(csam.ObserverApi.Observable o, Object arg) {
        String debug = 'CC ORDER Observer execute, observable: ' + o + ', args: ' + arg;
        System.debug(debug);

        if (o instanceof csb2c.ProductBasketObservable) {

            try {
                csb2c.ProductBasketObservable observable = (csb2c.ProductBasketObservable) o;
                execute(observable.getBasketIds());
            } catch (Exception e) {
                System.debug(e.getMessage() + ' | ' + e.getStackTraceString());
                Attachment errorOut = new Attachment();
                errorOut.Name = 'Error during observer';
                csb2c.ProductBasketObservable observable = (csb2c.ProductBasketObservable) o;

                if(observable.getBasketIds() != null)
                    errorOut.ParentId = observable.getBasketIds()[0];

                errorOut.Body = Blob.valueOf(e.getMessage() + ' | ' + e.getStackTraceString());

                if(errorOut.ParentId != null)
                    insert errorOut;
            }
        }
    }
}