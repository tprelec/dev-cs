@isTest
public class CCOrderRequestProcessingTest {

    private static testMethod void testExecute() {
        Framework__c frameworkSetting = new Framework__c();
            frameworkSetting.Framework_Sequence_Number__c = 2;
            insert frameworkSetting;

            PriceReset__c priceResetSetting = new PriceReset__c();

            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';

            insert priceResetSetting;

            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;

            Account account = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Account',
                Type = 'End Customer'
            );
            insert account;

            Contact contact = new Contact(
                AccountId = account.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'
            );
            insert contact;

            Opportunity opportunity = new Opportunity(
                Name = 'New Opportunity',
                OwnerId = UserInfo.getUserId(),
                StageName = 'Qualification',
                Probability = 0,
                CloseDate = system.today(),
                AccountId = account.id,
                Segment__c = 'SoHo',
                Type_of_service__c = 'Mobile'
            );
            insert opportunity;

            cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
                Name = 'New Basket',
                Primary__c = false,
                OwnerId = UserInfo.getUserId(),
                cscfga__Opportunity__c = opportunity.Id,
                Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
            );
            insert basket;

            cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
                cscfga__Product_Basket__c = basket.Id
            );
            insert config;

            cscfga__Attribute__c attr = new cscfga__Attribute__c(
                Name = 'contractDuration', cscfga__Product_Configuration__c = config.Id, cscfga__Value__c = '24'
            );
            insert attr;

            String body = '{"connectionTypes": [{ "type": "New", "quantity": 50 }, { "type": "Port-in", "quantity": 50 }], "charges":[{"source":"recurring","salesPrice":34,"listPrice":34,"quantity":1,"name":"recurring","description":"Override charge value forrecurring","chargeType":"recurring","isProductLevelPricing":true}],"discounts":[{"type":"PER","amount":5,"chargeType":"recurring","discountPrice":"sales","version":"2-0-0","recordType":"single","discountCharge":"recurring","source":"promo-test;promotion"}],"customData":{},"OE":[],"CS_QuantityStrategy":"Multiplier","csQuantity":1}';

            Attachment att = new Attachment(
                Name = 'AdditionalQualificationData.json',
                Body = Blob.valueOf(body),
                ParentId = config.Id
            );
            insert att;

            Test.startTest();

            List<Id> basketIdList = new List<Id>();
            basketIdList.add(basket.Id);
            CCOrderRequestProcessing processingObj = new CCOrderRequestProcessing();
            processingObj.execute(basketIdList);

            Map<ID,CCAQDProcessor.CCAQDStructure> pcsWithAQD = CCAQDProcessor.extractPCAqdData(basketIdList);

            CCAQDProcessor.CCAQDStructure workingAQD = pcsWithAQD.get(config.Id);

            ChargesProcessor.processCharges(config, new cscfga.ProductConfiguration.ProductDiscount(), workingAQD.charges, workingAQD.CS_QuantityStrategy);

            Test.stopTest();
    }
    
    private static testMethod void testExecute2() {
        Framework__c frameworkSetting = new Framework__c();
            frameworkSetting.Framework_Sequence_Number__c = 2;
            insert frameworkSetting;

            PriceReset__c priceResetSetting = new PriceReset__c();

            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';

            insert priceResetSetting;

            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;

            Account account = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Account',
                Type = 'End Customer'
            );
            insert account;

            Contact contact = new Contact(
                AccountId = account.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'
            );
            insert contact;

            Opportunity opportunity = new Opportunity(
                Name = 'New Opportunity',
                OwnerId = UserInfo.getUserId(),
                StageName = 'Qualification',
                Probability = 0,
                CloseDate = system.today(),
                AccountId = account.id,
                Segment__c = 'SoHo',
                Type_of_service__c = 'Mobile'
            );
            insert opportunity;

            cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
                Name = 'New Basket',
                Primary__c = false,
                OwnerId = UserInfo.getUserId(),
                cscfga__Opportunity__c = opportunity.Id,
                Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
            );
            insert basket;

            cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
                cscfga__Product_Basket__c = basket.Id
            );
            insert config;

            cscfga__Attribute__c attr = new cscfga__Attribute__c(
                Name = 'contractDuration', cscfga__Product_Configuration__c = config.Id, cscfga__Value__c = '24'
            );
            insert attr;

            String body = '{"connectionTypes": [{ "type": "New", "quantity": 50 }, { "type": "Port-in", "quantity": 50 }], "charges":[{"source":"recurring","salesPrice":34,"listPrice":34,"quantity":1,"name":"recurring","description":"Override charge value forrecurring","chargeType":"recurring","isProductLevelPricing":true}],"discounts":[{"type":"PER","amount":5,"chargeType":"recurring","discountPrice":"sales","version":"2-0-0","recordType":"single","discountCharge":"recurring","source":"promo-test;promotion"}],"customData":{},"OE":[],"CS_QuantityStrategy":"Multiplier","csQuantity":1}';

            Attachment att = new Attachment(
                Name = 'AdditionalQualificationData.json',
                Body = Blob.valueOf(body),
                ParentId = config.Id
            );
            insert att;

            Test.startTest();

            List<Id> basketIdList = new List<Id>();
            basketIdList.add(basket.Id);
        	List<cscfga__Product_Basket__c> baskets = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id ];
            CCOrderRequestProcessing.generateCtnRecordsOnlineScenario(baskets);
            Test.stopTest();
    }

    private static testMethod void testFailure() {
        List<Profile> pList = [
        SELECT Id, Name
        FROM Profile
        WHERE Name = 'System Administrator'
        LIMIT 1
        ];

        List<UserRole> roleList = [
        SELECT Id, Name, DeveloperName
        FROM UserRole u
        WHERE ParentRoleId = NULL
        ];

        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs(simpleUser) {
            Framework__c frameworkSetting = new Framework__c();
            frameworkSetting.Framework_Sequence_Number__c = 2;
            insert frameworkSetting;

            PriceReset__c priceResetSetting = new PriceReset__c();

            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';

            insert priceResetSetting;

            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;

            Account account = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Account',
                Type = 'End Customer'
            );
            insert account;

            Contact contact = new Contact(
                AccountId = account.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'
            );
            insert contact;

            Opportunity opportunity = new Opportunity(
                Name = 'New Opportunity',
                OwnerId = UserInfo.getUserId(),
                StageName = 'Qualification',
                Probability = 0,
                CloseDate = system.today(),
                AccountId = account.id
            );
            insert opportunity;

            cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
                Name = 'New Basket',
                Primary__c = false,
                OwnerId = UserInfo.getUserId(),
                cscfga__Opportunity__c = opportunity.Id,
                Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
            );
            insert basket;

            cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
                cscfga__Product_Basket__c = basket.Id
            );
            insert config;

            String body = '{"charges":[{"source":"recurring","salesPrice":34,"listPrice":34,"quantity":1,"name":"recurring","description":"Override charge value forrecurring","chargeType":"recurring","isProductLevelPricing":true}],"discounts":[],"customData":{},"OE":[],"CS_QuantityStrategy":"Multiplier","csQuantity":1}';

            Attachment att = new Attachment(
                Name = 'AdditionalQualificationData.json',
                Body = Blob.valueOf(body),
                ParentId = config.Id
            );
            insert att;

            Test.startTest();

            List<Id> basketIdList = new List<Id>();
            basketIdList.add(basket.Id);
            CCOrderRequestProcessing processingObj = new CCOrderRequestProcessing();
            csb2c.ProductBasketObservable observable = (csb2c.ProductBasketObservable) JSON.deserialize('{}', csb2c.ProductBasketObservable.class);
            processingObj.execute(observable, null);

            Test.stopTest();
        }
    }
}