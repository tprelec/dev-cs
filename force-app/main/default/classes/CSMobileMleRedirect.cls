/**
 * Class defines actions that are going to be performed after Mobile MLE has called return URL.
 */
public class CSMobileMleRedirect {
	cscfga__Product_Basket__c productBasket;
	ApexPages.StandardController stdController;

    /**
     * Constructor.
     */
	public CSMobileMleRedirect(ApexPages.StandardController stdController) {
		this.stdController = stdController;
		this.productBasket = (cscfga__Product_Basket__c)stdController.getRecord();
	}
    
    /**
     * Method that is going to add product configuration requests to all product configurations.
     */
    public void addProductConfigurationRequestToAllProductConfigurations() {
        // Get all product configurations in a basket
        List<cscfga__Product_Configuration__c> allProductConfigurations = [SELECT id, cscfga__Product_Basket__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c =:productBasket.Id];
        
        // Get all product configuration requests
        List<csbb__Product_Configuration_Request__c> allProductConfigurationRequests = [SELECT id, csbb__Product_Configuration__c, csbb__Product_Basket__c FROM csbb__Product_Configuration_Request__c WHERE csbb__Product_Basket__c =:productBasket.Id];
        
        // What product configuration requests have to be added
        List<csbb__Product_Configuration_Request__c> listOfProductConfigurationRequestsToAdd = new List<csbb__Product_Configuration_Request__c>();
        
        // What product configuration requests have to be deleted
        List<csbb__Product_Configuration_Request__c> listOfProductConfigurationRequestsToDelete = new List<csbb__Product_Configuration_Request__c>();
        
        /**
         * Go through all product configurations and check if they all have PCR's.
         */
        for (csbb__Product_Configuration_Request__c pcr : allProductConfigurationRequests) {
            if(pcr.csbb__Product_Configuration__c == null) {
                // If PC is deleted with MLE, then we have to manually delete PCR.
                listOfProductConfigurationRequestsToDelete.add(pcr);
            } 
        }
        
        for (cscfga__Product_Configuration__c pc : allProductConfigurations) {
            boolean configurationFound = false;
            for (csbb__Product_Configuration_Request__c pcr : allProductConfigurationRequests) {
                if (pcr.csbb__Product_Configuration__c == pc.Id) {
                    configurationFound = true;
                }
            }
            
            /**
             * If there is no PCR and configuration exists, add PCR.
             */
            if(configurationFound == false) {
                csbb__Product_Configuration_Request__c newProductConfigurationRequest = new csbb__Product_Configuration_Request__c(csbb__Product_Configuration__c=pc.Id, csbb__Product_Basket__c=productBasket.Id);
                listOfProductConfigurationRequestsToAdd.add(newProductConfigurationRequest);
            }
        }
        upsert listOfProductConfigurationRequestsToAdd;
        delete listOfProductConfigurationRequestsToDelete;
    }
    
    /**
    * Updates certain fields once the Product Configuration is finished.
    */
	public PageReference redirect() {
        addProductConfigurationRequestToAllProductConfigurations();
        return stdController.view();
	}
	
}