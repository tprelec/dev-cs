public class CSTest{

@future(callout=true)
public static void insertQuoteTemplate(Id parentId) {
    PageReference installationNotesPDF = new PageReference('/apex/csclmcb__GenerateDocument?scontrolCaching=1&id='+parentId);
        
        Blob body ;
            
        body=installationNotesPDF.getContentAsPdf();
        
        System.debug('body');

        System.debug(body);
        List<csclm__Agreement__c> agList = [Select id, Name from csclm__Agreement__c where Id = :parentId];
         
        Attachment attachment = new Attachment();
        attachment.Body = body;
        attachment.Name = 'CSTest.pdf';
        attachment.ParentId = parentId; 
        insert attachment;
        
        attachment.ContentType = 'application/pdf';
        update attachment;
}


        
        
}