/**
 * Created by bojan.zunko on 11/05/2020.
 */

public with sharing class CSUtil {

    public String SHARED_SECRET_API_KEY;
    public String CLIENT_API_KEY;



    public static String calculateRsaPayloadHash(String payload, String contentType) {
        String content = 'cs-elastic.payload\n' + parseContentType(contentType) + '\n';
        content += payload + '\n';

        Blob hash = Crypto.generateDigest('SHA-256', Blob.valueOf(content));
        return EncodingUtil.base64Encode(hash);
    }

    public static String parseContentType(String header) {
        return header != null ? header.split(';')[0].trim().toLowerCase() : '';
    }

    public static String generateRsaAuthorizationHeader(String privateKey, String clientId, String method, String url, String hash) {
        Long ts = System.currentTimeMillis();
        String nonce = genNonce();
        String sig = generateSignature(privateKey, ts, nonce, method, url, hash);

        String result = 'CS-Elastic ' +
                'ts=' + quote(ts) +
                ', clientId=' + quote(clientId) +
                ', nonce=' + quote(nonce) +
                ', sig=' + quote(sig);


        if (hash != null) {
            result += ', hash=' + quote(hash);
        }

        return result;
    }

    public static String quote(Object value) {
        return '\"' + String.valueOf(value) + '\"';
    }

    public static String generateSignature(String privateKey, Long ts, String nonce, String method, String url, String hash) {
        if (method == null) {
            method = '';
        }
        if (url == null) {
            url = '';
        }

        String normalized = 'cs-elastic' + '\n' +
                ts + '\n' +
                nonce + '\n' +
                method.toUpperCase() + '\n' +
                url + '\n';

        if (hash != null) {
            normalized += hash + '\n';
        }

        privateKey = privateKey
                .replaceAll('-----BEGIN PRIVATE KEY-----', '')
                .replaceAll('-----END PRIVATE KEY-----', '')
                .replaceAll('\\n', '');

        Blob key = EncodingUtil.base64Decode(privateKey);
        Blob signature = Crypto.sign('RSA-SHA256', Blob.valueOf(normalized), key);
        return EncodingUtil.base64Encode(signature);
    }

    public static String genNonce() {
        Blob key = Crypto.GenerateAESKey(128);
        return EncodingUtil.convertToHex(key);
    }

    public static Map<String, String> buildHeaders(String privateKey, String clientId, String method, String url, String payload) {
        if (payload != null) {
            Map<String, String> headers = new Map<String, String>{
                    'Authorization' => generateRsaAuthorizationHeader(privateKey, clientId, method, url, calculateRsaPayloadHash(payload, 'application/json')),
                    'Content-Type' => 'application/json'
            };
            headers.put('Content-Length', String.valueOf(payload.length()));
            return headers;
        } else {
            Map<String, String> headers = new Map<String, String>{
                    'Authorization' => generateRsaAuthorizationHeader(privateKey, clientId, method, url, null)
            };
            headers.put('Content-Length', '0');
            return headers;
        }
    }

}