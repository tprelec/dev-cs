@isTest
public class CSUtilTest {

    private static testMethod void testExecute() {

        String privatekey = '-----BEGIN PRIVATE KEY-----\nMIICXAIBAAKBgQCQZFafoxmRqsnrwR9d4nNkP+tjmcsoo3zHnTmHtnc9cJZlRXre\nwYN8LRVJSyudrP4w/hcOkIaHz7pGHnTsyLIkcSJn5V27IbqaA/laNUbGTmv8evG0\nWTeMM+g/YZcpa7wR6Qqy7e6G5Yb9vlKfvfb1tgMAH3eV3ONrWSdkfEHv/wIDAQAB\nAoGAEWIjE6D9KQ5YtOtRLWpf3gfb/Oe9D61vDlEdZftPq7PsR1DwE/VUMRT0ZRiJ\nrbfv+lH80KIRsDQQBvUPupoG6DGRL7kYYCgz7kJMqpjEkslKl3ME4ezf8e5K4BCJ\nTfon605tboGJ3QvxvEAauGClRDkDUGMHbqn1lSHlWKFSczECQQDardSO5TOq72cP\nTgSn8F3QDQ+Y3dKtEgY5usgZU/pf37tBunJQVXIiy5B+YEhsl0oOPFCS9blUQ0Z9\nvFajU8S9AkEAqQjgnYZPghUIDfJ97vZh0GoxSs2Sk58a9lKjO8I55p2rhXsND0b5\nMAzcOIR2fJp/6K70Y2+zmz3clYOAXE1ZawJAOJWOUMNiETMNWdp4PfRmcfVPOeD8\nKKhnT/zS1iryuGguDh3ugnb5p6NA6lKDqwWitfhJaR2ILROTY+meCXkdiQJAW6m/\nvEyAQydKhd2w4uvi4RUlA850pO7LvTXU5OwPX/qyb6Dp1RYEvUTMCy3x3uIdaL2v\nM1j4ib/DJaT475TWXwJBANS8Xyh0jCROLAjh+Fb011SSC0t8kBmYGu24hHn6bQ+i\nSsnS7K8UnZ2e1WYDmuBcXx2DQBruAlMqbjhP76B6leo=\n-----END PRIVATE KEY-----';

        Test.startTest();

        CSUtil.generateRsaAuthorizationHeader(privatekey, 'xxx', null, null, 'xxx');
        Map<String, String> testMap = CSUtil.buildHeaders(privatekey, 'xxx', null, null, 'xxx');

        System.assert(testMap.size() > 0, 'Expected equal');

        Test.stopTest();
    }
}