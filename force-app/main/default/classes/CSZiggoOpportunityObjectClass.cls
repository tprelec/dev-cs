public class CSZiggoOpportunityObjectClass {

    public class Account {
        public Attributes attributes {get;set;}
        public String Name {get;set;}
        public String LG_ChamberOfCommerceNumber {get;set;}

        public Account(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'attributes') {
                            attributes = new Attributes(parser);
                        } else if (text == 'Name') {
                            Name = parser.getText();
                        } else if (text == 'LG_ChamberOfCommerceNumber') {
                            LG_ChamberOfCommerceNumber = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Account consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Owner {
        public Attributes attributes {get;set;}
        public String Name {get;set;}

        public Owner(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'attributes') {
                            attributes = new Attributes(parser);
                        } else if (text == 'Name') {
                            Name = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Owner consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }


    public Integer totalSize {get;set;}
    public Boolean done {get;set;}
    public List<Records> records {get;set;}

    public CSZiggoOpportunityObjectClass(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'totalSize') {
                        totalSize = parser.getIntegerValue();
                    } else if (text == 'done') {
                        done = parser.getBooleanValue();
                    } else if (text == 'records') {
                        records = arrayOfRecords(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'CSZiggoOpportunityObjectClass consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }

    public class Attributes {
        public String type_Z {get;set;} // in json: type
        public String url {get;set;}

        public Attributes(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'type') {
                            type_Z = parser.getText();
                        } else if (text == 'url') {
                            url = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Attributes consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class OpportunityLineItems {
        public Integer totalSize {get;set;}
        public Boolean done {get;set;}
        public List<Owner> records {get;set;}

        public OpportunityLineItems(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'totalSize') {
                            totalSize = parser.getIntegerValue();
                        } else if (text == 'done') {
                            done = parser.getBooleanValue();
                        } else if (text == 'records') {
                            records = arrayOfOwner(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'OpportunityLineItems consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Records {
        public Attributes attributes {get;set;}
        public String Id {get;set;}
        public Owner Owner {get;set;}
        public String StageName {get;set;}
        public String Name {get;set;}
        public String Owner_Company {get;set;}
        public String LG_NEWSalesChannel {get;set;}
        public Account Account {get;set;}
        public Decimal Probability {get;set;}
        public String CloseDate {get;set;}
        public String Type_Z {get;set;} // in json: Type
        public Decimal Amount {get;set;}
        public String LG_AccntPostalCity {get;set;}
        public String LG_AccntPostalHouseNumber {get;set;}
        public String LG_AccntPostalHouseNumberExtension {get;set;}
        public String LG_AccntPostalPostalCode {get;set;}
        public String LG_AccntPostalStreet {get;set;}
        public OpportunityLineItems OpportunityLineItems {get;set;}

        public Records(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'attributes') {
                            attributes = new Attributes(parser);
                        } else if (text == 'Id') {
                            Id = parser.getText();
                        } else if (text == 'Owner') {
                            Owner = new Owner(parser);
                        } else if (text == 'StageName') {
                            StageName = parser.getText();
                        } else if (text == 'Name') {
                            Name = parser.getText();
                        } else if (text == 'Owner_Company') {
                            Owner_Company = parser.getText();
                        } else if (text == 'LG_NEWSalesChannel') {
                            LG_NEWSalesChannel = parser.getText();
                        } else if (text == 'Account') {
                            Account = new Account(parser);
                        } else if (text == 'Probability') {
                            Probability = parser.getDoubleValue();
                        } else if (text == 'CloseDate') {
                            CloseDate = parser.getText();
                        } else if (text == 'Type') {
                            Type_Z = parser.getText();
                        } else if (text == 'Amount') {
                            Amount = parser.getDoubleValue();
                        } else if (text == 'LG_AccntPostalCity') {
                            LG_AccntPostalCity = parser.getText();
                        } else if (text == 'LG_AccntPostalHouseNumber') {
                            LG_AccntPostalHouseNumber = parser.getText();
                        } else if (text == 'LG_AccntPostalHouseNumberExtension') {
                            LG_AccntPostalHouseNumberExtension = parser.getText();
                        } else if (text == 'LG_AccntPostalPostalCode') {
                            LG_AccntPostalPostalCode = parser.getText();
                        } else if (text == 'LG_AccntPostalStreet') {
                            LG_AccntPostalStreet = parser.getText();
                        } else if (text == 'OpportunityLineItems') {
                            OpportunityLineItems = new OpportunityLineItems(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Records consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }


    public static CSZiggoOpportunityObjectClass parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new CSZiggoOpportunityObjectClass(parser);
    }

    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT ||
                    curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                    curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }




    private static List<Records> arrayOfRecords(System.JSONParser p) {
        List<Records> res = new List<Records>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Records(p));
        }
        return res;
    }














    private static List<Owner> arrayOfOwner(System.JSONParser p) {
        List<Owner> res = new List<Owner>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Owner(p));
        }
        return res;
    }



}