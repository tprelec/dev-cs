global class CSZiggoSync implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts {

    /**
     *
     */
    global ResponseLeadWrapper leadWrapper;

    /**
     *
     */
    global ResponseCampaignWrapper campaignWrapper;

    /**
     *
     */
    global ResponseCampaignMemberWrapper campaignMemberWrapper;

    /**
     *
     */
    global ResponseWrapper wrapper;

    /**
     *
     */
    global String objectName;

    /**
     *
     *
     * @param objectName
     */
    global CSZiggoSync(String objectName) {
        if (!String.isEmpty(objectName)) {
            this.objectName = objectName;
        } else {
            this.objectName = 'Opportunity';
        }
    }

    /**
     *
     *
     * @param BC
     *
     * @return
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        List<ZiggoSync__c> ziggoSyncList = [Select Id From ZiggoSync__c Where ObjectTranferred__c =: this.objectName Order BY LastModifiedDate DESC Limit 1];
        system.debug('##ziggoSyncList: '+ziggoSyncList);
        if (ziggoSyncList.size() == 0) {
            ZiggoSync__c newRec = new ZiggoSync__c();
            newRec.LastModifiedDate__c = Date.Today().addYears(-10);
            newRec.ObjectTranferred__c = this.objectName;
            newRec.NumberOfRecordsTransferred__c = 0;
            insert newRec;
        }
        return Database.getQueryLocator('Select Id From ZiggoSync__c Where ObjectTranferred__c = \''+this.objectName+'\' Order BY LastModifiedDate DESC Limit 1');
    }

    /**
     *
     *
     * @param BC
     * @param scope
     */
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        if (this.checkSetting()) {
            this.runAll();
        }
    }

    /**
     *
     *
     * @param BC
     */
    global void finish(Database.BatchableContext BC){
        ZiggoSync__c zsRec = getLastSyncRecord();
        if (zsRec != null) {
            if (!String.isEmpty(zsRec.NextUrl__c)) {
                Database.executeBatch(new CSZiggoSync(this.objectName), 200);
            }
        }
    }

    /**
     *
     *
     * @param SC
     */
    global void execute(SchedulableContext SC) {
        if (this.checkSetting()) {
            this.runAll();
        }
    }

    /**
     * Check a custom metadata record to see if this is allowed to run
     *
     * @return
     */
    public Boolean checkSetting() {
        Boolean returnValue = false;
        if (Test.isRunningTest()) {
            returnValue = true;
        }
        List<CsSyncSetting__mdt> syncSettingList = [Select DeveloperName, Active__c From CsSyncSetting__mdt Where DeveloperName = 'CsSyncRun'];
        if (syncSettingList.size() == 1) {
            if (syncSettingList[0].Active__c) {
                returnValue = true;
            }
        }
        return returnValue;
    }

    /**
     *
     */
    public void runAll() {
        /** Generating Log Record */
        DateTime newLastModifiedDate;
        Integer numberRecordProcessed = 0;
        String nextUrl = '';

        if (this.objectName == 'Opportunity') {
            String response = runCallout(this.objectName, null);
            JSONParser jp = JSON.createParser(response);
            this.wrapper = (ResponseWrapper) jp.readValueAs(ResponseWrapper.class);
            if (this.wrapper.records != null) {
                if (this.wrapper.records.size() > 0) {
                    system.debug('##ziggoOppList.size(): '+this.wrapper.records.size());
                    system.debug('##ziggoOppList: '+this.wrapper.records);
                    List<ZiggoOpportunity__c> zoList = CSZiggoSync.populateLocalObject(this.wrapper.records);
                    if (zoList.size() > 0) {
                        Schema.SObjectField f = ZiggoOpportunity__c.Fields.Ziggo_Id__c;
                        List<Database.UpsertResult> duResultList = Database.upsert(zoList, f, false);

                    }
                }
                newLastModifiedDate = getLastModifiedDate();
                if (this.wrapper.records != null) {
                    numberRecordProcessed = this.wrapper.records.size();
                }
                if (!wrapper.done) {
                    if (!String.isEmpty(wrapper.nextRecordsUrl)) {
                        nextUrl = wrapper.nextRecordsUrl;
                    }
                }
            }
        } else if (this.objectName == 'Task'){
            String response = runCallout(this.objectName, null);
            JSONParser jp = JSON.createParser(response);
            this.leadWrapper = (ResponseLeadWrapper) jp.readValueAs(ResponseLeadWrapper.class);
            system.debug('##this.taskWrapper: '+this.leadWrapper);
            if (this.leadWrapper.records != null) {
                if (this.leadWrapper.records.size() > 0) {
                    system.debug('##ziggoOppList.size(): '+this.leadWrapper.records.size());
                    system.debug('##ziggoOppList: '+this.leadWrapper.records);
                    List<ZiggoActivities__c> zaList = CSZiggoSync.populateLocalTaskObject(this.leadWrapper.records);
                    if (zaList.size() > 0) {
                        Schema.SObjectField f = ZiggoActivities__c.Fields.External_ID__c;
                        List<Database.UpsertResult> duResultList = Database.upsert(zaList, f, false);

                    }
                }
            }
            newLastModifiedDate = getLastModifiedDateFromTask();
            if (this.leadWrapper.records != null) {
                numberRecordProcessed = this.leadWrapper.records.size();
            }
            if (!leadWrapper.done) {
                if (!String.isEmpty(leadWrapper.nextRecordsUrl)) {
                    nextUrl = leadWrapper.nextRecordsUrl;
                }
            }
        } else if (this.objectName == 'Lead'){
            /*String response = runCallout(this.objectName, Set<Id> idSet);
            JSONParser jp = JSON.createParser(response);
            this.leadWrapper = (ResponseLeadWrapper) jp.readValueAs(ResponseLeadWrapper.class);
            system.debug('##this.taskWrapper: '+this.taskWrapper);*/

        } else if (this.objectName == 'LeadStatus') {
            String response = runCallout(this.objectName, null);
            JSONParser jp = JSON.createParser(response);
            this.leadWrapper = (ResponseLeadWrapper) jp.readValueAs(ResponseLeadWrapper.class);
            system.debug('##this.leadWrapper: '+this.leadWrapper);
            if (this.leadWrapper.records != null) {
                if (this.leadWrapper.records.size() > 0) {
                    List<ZiggoLeadStatus__c> zaList = CSZiggoSync.populateLocalLeadStatusObject(this.leadWrapper.records);
                    if (zaList.size() > 0) {
                        Schema.SObjectField f = ZiggoLeadStatus__c.Fields.External_ID__c;
                        List<Database.UpsertResult> duResultList = Database.upsert(zaList, f, false);

                    }
                }
            }
            newLastModifiedDate = getLastModifiedDateFromTask();
            if (this.leadWrapper.records != null) {
                numberRecordProcessed = this.leadWrapper.records.size();
            }
            if (!leadWrapper.done) {
                if (!String.isEmpty(leadWrapper.nextRecordsUrl)) {
                    nextUrl = leadWrapper.nextRecordsUrl;
                }
            }
        } else if (this.objectName == 'Campaign') {
            String response = runCallout(this.objectName, null);
            JSONParser jp = JSON.createParser(response);
            this.campaignWrapper = (ResponseCampaignWrapper) jp.readValueAs(ResponseCampaignWrapper.class);
            system.debug('##this.leadWrapper: '+this.campaignWrapper);
            if (this.campaignWrapper.records != null) {
                if (this.campaignWrapper.records.size() > 0) {
                    List<ZiggoCampaign__c> zaList = CSZiggoSync.populateLocalCampaignStatusObject(this.campaignWrapper.records);
                    if (zaList.size() > 0) {
                        Schema.SObjectField f = ZiggoCampaign__c.Fields.External_ID__c;
                        List<Database.UpsertResult> duResultList = Database.upsert(zaList, f, false);

                    }
                }
            }
            newLastModifiedDate = getLastModifiedDateFromCampaign();
            if (this.campaignWrapper.records != null) {
                numberRecordProcessed = this.campaignWrapper.records.size();
            }
            if (!campaignWrapper.done) {
                if (!String.isEmpty(campaignWrapper.nextRecordsUrl)) {
                    nextUrl = campaignWrapper.nextRecordsUrl;
                }
            }

        } else if (this.objectName == 'CampaignMember') {
            String response = runCallout(this.objectName, null);
            JSONParser jp = JSON.createParser(response);
            this.campaignMemberWrapper = (ResponseCampaignMemberWrapper) jp.readValueAs(ResponseCampaignMemberWrapper.class);
            system.debug('##this.campaignMemberWrapper: '+this.campaignMemberWrapper);
            if (this.campaignMemberWrapper.records != null) {
                if (this.campaignMemberWrapper.records.size() > 0) {
                    List<ZiggoCampaignMember__c> zaList = CSZiggoSync.populateLocalCampaignMemberObject(this.campaignMemberWrapper.records);
                    if (zaList.size() > 0) {
                        Schema.SObjectField f = ZiggoCampaignMember__c.Fields.External_ID__c;
                        List<Database.UpsertResult> duResultList = Database.upsert(zaList, f, false);

                    }
                }
            }
            newLastModifiedDate = getLastModifiedDateFromCampaignMember();
            if (this.campaignMemberWrapper.records != null) {
                numberRecordProcessed = this.campaignMemberWrapper.records.size();
            }
            if (!campaignMemberWrapper.done) {
                if (!String.isEmpty(campaignMemberWrapper.nextRecordsUrl)) {
                    nextUrl = campaignMemberWrapper.nextRecordsUrl;
                }
            }

        }

        ZiggoSync__c zsRec = logLatestRun(DateTime.now(), this.objectName, numberRecordProcessed, newLastModifiedDate, nextUrl);
        try {
            insert zsRec;
        } catch(Exception e) {
            system.debug('##error: '+e.getMessage());
        }

        //if (!wrapper.done) {
            //CSZiggoSync c = new CSZiggoSync();
            //c.execute(null);
        //}
    }

    /**
     *
     *
     * @return
     */
    private Datetime getLastModifiedDate() {
        Datetime newLastModifiedDate;
        if (this.wrapper.records != null) {
            if (this.wrapper.records.size() > 0) {
                for (mZiggo_Opportunity_Object zObj : this.wrapper.records) {
                    if (newLastModifiedDate == null) {
                        newLastModifiedDate = zObj.LastModifiedDate;
                    } else {
                        if (zObj.LastModifiedDate > newLastModifiedDate) {
                            newLastModifiedDate = zObj.LastModifiedDate;
                        }
                    }
                }
            }
        }
        return newLastModifiedDate;
    }

    private Datetime getLastModifiedDateFromTask() {
        Datetime newLastModifiedDate;
        if (this.leadWrapper.records != null) {
            if (this.leadWrapper.records.size() > 0) {
                for (LeadClass zObj : this.leadWrapper.records) {
                    if (newLastModifiedDate == null) {
                        newLastModifiedDate = zObj.LastModifiedDate;
                    } else {
                        if (zObj.LastModifiedDate > newLastModifiedDate) {
                            newLastModifiedDate = zObj.LastModifiedDate;
                        }
                    }
                }
            }
        }
        return newLastModifiedDate;
    }

    private Datetime getLastModifiedDateFromCampaign() {
        Datetime newLastModifiedDate;
        if (this.campaignWrapper.records != null) {
            if (this.campaignWrapper.records.size() > 0) {
                for (CampaignClass zObj : this.campaignWrapper.records) {
                    if (newLastModifiedDate == null) {
                        newLastModifiedDate = zObj.LastModifiedDate;
                    } else {
                        if (zObj.LastModifiedDate > newLastModifiedDate) {
                            newLastModifiedDate = zObj.LastModifiedDate;
                        }
                    }
                }
            }
        }
        return newLastModifiedDate;
    }

    private Datetime getLastModifiedDateFromCampaignMember() {
        Datetime newLastModifiedDate;
        if (this.campaignMemberWrapper.records != null) {
            if (this.campaignMemberWrapper.records.size() > 0) {
                for (CampaignMemberClass zObj : this.campaignMemberWrapper.records) {
                    if (newLastModifiedDate == null) {
                        newLastModifiedDate = zObj.LastModifiedDate;
                    } else {
                        if (zObj.LastModifiedDate > newLastModifiedDate) {
                            newLastModifiedDate = zObj.LastModifiedDate;
                        }
                    }
                }
            }
        }
        return newLastModifiedDate;
    }

    /**
     *
     *
     * @return
     */
    private String runCallout(String objName, Set<Id> idSet) {
        String instanceUrl = '';
        String query = '';
        String sDt = '';
        Boolean useNextUrl = false;

        ZiggoSync__c zsRec = getLastSyncRecord();
        if (zsRec != null) {
            if (!String.isEmpty(zsRec.NextUrl__c)) {
                useNextUrl = true;
            } else {
                Datetime dt = zsRec.LastModifiedDate__c;
                sDt = dt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
            }
        }

        if (useNextUrl) {
            instanceUrl = zsRec.NextUrl__c;
        } else {
            if (objName == 'Opportunity') {
                query = 'SELECT Id, LastModifiedDate, Owner.Name, StageName, Name, Owner_Company__c, LG_NEWSalesChannel__c, Account.Name, Probability';
                query += ', CloseDate, Type, Amount, LG_AccntPostalCity__c, LG_AccntPostalHouseNumber__c, LG_AccntPostalHouseNumberExtension__c';
                query += ', LG_AccntPostalPostalCode__c, LG_AccntPostalStreet__c, Account.LG_ChamberOfCommerceNumber__c, LG_ProductsInBasketHidden__c';
                query += ', (Select Id, Name, Product__c, LG_OneOffPrice__c, Quantity, LG_ContractTermTextFormula__c From OpportunityLineItems)';
                query += ' FROM Opportunity';
                if (!String.isEmpty(sDt)) {
                    query += ' Where LastModifiedDate >= ' + sDt;
                }
            } else if (objName == 'Task') {
                query = 'SELECT Id, Street, City, PostalCode, Country, Name, LastModifiedDate, LG_ChamberOfCommerceNumber__c, (SELECT Id, Subject, Status, ActivityDate, Owner.Name, LG_Result__c FROM OpenActivities Order BY LastModifiedDate ASC), (SELECT Id,  Subject, Status, ActivityDate, Owner.Name, LG_Result__c FROM ActivityHistories Order BY LastModifiedDate ASC) FROM Lead';
                if (!String.isEmpty(sDt)) {
                    query += ' WHERE LastModifiedDate >= ' + sDt;
                }
            } else if (objName == 'LeadStatus') {
                query = 'SELECT Id, Status, LG_ChamberOfCommerceNumber__c, LastModifiedDate From Lead';
                if (!String.isEmpty(sDt)) {
                    query += ' WHERE LastModifiedDate >= ' + sDt;
                }

            } else if (objName == 'Lead') {
                query = 'SELECT Id, LastModifiedDate, LG_ChamberOfCommerceNumber__c From Lead';
            } else if (objName == 'Campaign') {
                query = 'Select Id, Name, Type, Status, Parent.Name, Parent.Id, LastModifiedDate From Campaign';
                if (!String.isEmpty(sDt)) {
                    query += ' WHERE LastModifiedDate >= ' + sDt;
                }
            } else if (objName == 'CampaignMember') {
                query = 'Select Id, Name, CampaignId, LeadId, Lead.LG_ChamberOfCommerceNumber__c, LastModifiedDate From CampaignMember';
                if (!String.isEmpty(sDt)) {
                    query += ' WHERE LastModifiedDate >= ' + sDt;
                }
            }

            query += ' Order BY LastModifiedDate ASC';
            query += ' LIMIT 100000';
        }
        system.debug('##query:'+query);
        WebserviceHttpRest whttp = new WebserviceHttpRest();
        HttpResponse resp = whttp.doSoqlQueryLite(instanceUrl, query);
        system.debug('##resp: '+resp.getBody());

        String response = resp.getBody();
        response = resp.getBody();
        response = response.replace('__c', '');
        return response;
    }

    /**
     *
     *
     * @return
     */
    private ZiggoSync__c getLastSyncRecord() {
        List<ZiggoSync__c> ziggoSyncList = [Select Id, LastModifiedDate__c, NextUrl__c From ZiggoSync__c WHERE ObjectTranferred__c =: this.objectName Order BY LastRun__c DESC Limit 1];
        if (ziggoSyncList.size() > 0) {
            return ziggoSyncList[0];
        }
        return null;
    }

    public static List<ZiggoActivities__c> populateLocalTaskObject(List<LeadClass> zObjList) {
        List<ZiggoActivities__c> zoList = new List<ZiggoActivities__c>();
        if (zObjList != null) {
            if (zObjList.size() > 0) {
                for (LeadClass zo : zObjList) {
                    if (zo.OpenActivities != null) {
                        for (mZiggo_Task_Object act : zo.OpenActivities.records) {
                            ZiggoActivities__c zaRec = new ZiggoActivities__c();
                            zaRec.External_ID__c = act.Id;
                            zaRec.Kvk__c = zo.LG_ChamberOfCommerceNumber;
                            zaRec.Result__c = act.LG_Result;
                            zaRec.Activity_Date__c = act.ActivityDate;
                            zaRec.Status__c = act.Status;
                            zaRec.Subject__c = act.Subject;
                            zaRec.Owner_Name__c = act.Owner.Name;
                            zaRec.City__c = zo.City;
                            zaRec.Country__c = zo.Country;
                            zaRec.Street__c = zo.Street;
                            zaRec.PostalCode__c = zo.PostalCode;
                            zaRec.Activity_Type__c = 'Open Activity';
                            zoList.add(zaRec);
                        }
                    }

                    if (zo.ActivityHistories != null) {
                        for (mZiggo_Task_Object act : zo.ActivityHistories.records) {
                            ZiggoActivities__c zaRec = new ZiggoActivities__c();
                            zaRec.External_ID__c = act.Id;
                            zaRec.Kvk__c = zo.LG_ChamberOfCommerceNumber;
                            zaRec.Result__c = act.LG_Result;
                            zaRec.Status__c = act.Status;
                            zaRec.Subject__c = act.Subject;
                            zaRec.Owner_Name__c = act.Owner.Name;
                            zaRec.Activity_Date__c = act.ActivityDate;
                            zaRec.City__c = zo.City;
                            zaRec.Country__c = zo.Country;
                            zaRec.Street__c = zo.Street;
                            zaRec.PostalCode__c = zo.PostalCode;
                            zaRec.Activity_Type__c = 'Activity History';
                            zoList.add(zaRec);
                        }
                    }

                    /*zaRec.Owner_Name__c = zo.Owner.Name;
                    zaRec.Partner_Account__c = zo.Partner_Account;
                    zaRec.Result__c = zo.Result;
                    zaRec.Status__c = zo.Status;
                    zaRec.Subject__c = zo.Subject;
                    zaRec.Type__c = zo.Type;
                    */



                }

            }
        }
        return zoList;
    }

    /**
     *
     *
     * @param zObjList
     *
     * @return
     */
    public static List<ZiggoLeadStatus__c> populateLocalLeadStatusObject(List<LeadClass> zObjList) {
        List<ZiggoLeadStatus__c> zoList = new List<ZiggoLeadStatus__c>();
        if (zObjList != null) {
            if (zObjList.size() > 0) {
                for (LeadClass zo : zObjList) {
                    ZiggoLeadStatus__c zaRec = new ZiggoLeadStatus__c();
                    zaRec.External_ID__c = zo.Id;
                    zaRec.Status__c = zo.Status;
                    zaRec.Kvk__c = zo.LG_ChamberOfCommerceNumber;
                    zoList.add(zaRec);
                }

            }
        }
        return zoList;
    }

    public static List<ZiggoCampaign__c> populateLocalCampaignStatusObject(List<CampaignClass> zObjList) {
        List<ZiggoCampaign__c> zoList = new List<ZiggoCampaign__c>();
        if (zObjList != null) {
            if (zObjList.size() > 0) {
                for (CampaignClass zo : zObjList) {
                    ZiggoCampaign__c zaRec = new ZiggoCampaign__c();
                    zaRec.External_ID__c = zo.Id;
                    zaRec.Status__c = zo.Status;
                    zaRec.Name__c = zo.Name;
                    zaRec.Type__c = zo.Type;
                    if (zo.Parent != null) {
                        zaRec.Parent_ID__c = zo.Parent.Id;
                    }
                    zoList.add(zaRec);
                }

            }
        }
        return zoList;
    }

    public static List<ZiggoCampaignMember__c> populateLocalCampaignMemberObject(List<CampaignMemberClass> zObjList) {
        List<ZiggoCampaignMember__c> zoList = new List<ZiggoCampaignMember__c>();
        if (zObjList != null) {
            if (zObjList.size() > 0) {
                for (CampaignMemberClass zo : zObjList) {
                    ZiggoCampaignMember__c zaRec = new ZiggoCampaignMember__c();
                    zaRec.External_ID__c = zo.Id;
                    zaRec.Name__c = zo.Name;
                    zaRec.ZiggoCampaign_ID__c = zo.CampaignId;
                    if (zo.Lead != null) {
                        zaRec.Kvk__c = zo.Lead.LG_ChamberOfCommerceNumber;
                    }
                    zoList.add(zaRec);
                }

            }
        }
        return zoList;
    }





    /**
     *
     *
     * @param zObjList
     *
     * @return
     */
    public static List<ZiggoOpportunity__c> populateLocalObject(List<mZiggo_Opportunity_Object> zObjList) {
        List<ZiggoOpportunity__c> zoList = new List<ZiggoOpportunity__c>();
        if (zObjList != null) {
            if (zObjList.size() > 0) {
                for (mZiggo_Opportunity_Object zo : zObjList) {
                    ZiggoOpportunity__c zoRec = new ZiggoOpportunity__c();
                    zoRec.Name = zo.Name;
                    zoRec.Ziggo_Id__c = zo.Id;

                    /** Owner Info */
                    zoRec.OwnerName__c = zo.Owner.Name;

                    /** StageName */
                    zoRec.StageName__c = zo.StageName;

                    /** Owner_Company */
                    zoRec.Owner_Company__c = zo.Owner_Company;

                    /** LG_NEWSalesChannel */
                    zoRec.Scoring_Sales_Channel__c = zo.LG_NEWSalesChannel;

                    /** AccountName */
                    zoRec.Account_Name__c = zo.Account.Name;

                    /** Probability */
                    zoRec.Probability__c = zo.Probability;

                    /** CloseDate */
                    zoRec.CloseDate__c = zo.CloseDate;

                    /** Type */
                    zoRec.Type__c = zo.Type;

                    /** Amount */
                    zoRec.Amount__c = zo.Amount;

                    /** LG_AccntPostalPostalCode */
                    zoRec.LG_Account_PostalPostalCode__c = zo.LG_AccntPostalPostalCode;

                    /** LG_AccntPostalCity */
                    zoRec.LG_Account_Postal_City__c = zo.LG_AccntPostalCity;

                    /** LG_AccntPostalHouseNumber */
                    zoRec.LG_Account_Postal_House_Number__c = zo.LG_AccntPostalHouseNumber;

                    /** LG_AccntPostalHouseNumberExtension */
                    zoRec.LG_Account_Postal_House_Number_Extension__c = zo.LG_AccntPostalHouseNumberExtension;

                    /** LG_AccntPostalStreet */
                    zoRec.LG_Account_Postal_Street__c = zo.LG_AccntPostalStreet;

                    /** LG_AccntPostalStreet */
                    zoRec.LG_Account_Postal_Street__c = zo.LG_AccntPostalStreet;

                    /** Account.LG_ChamberOfCommerceNumber */
                    zoRec.Account_LG_ChamberOfCommerceNumber__c = zo.Account.LG_ChamberOfCommerceNumber;

                    /** */
                    zoRec.LG_ProductsInBasket__c = zo.LG_ProductsInBasketHidden;

                    zoRec.Products__c = CSZiggoSync.getProductSummary(zo.OpportunityLineItems);
                    /** add to the list */
                    zoList.add(zoRec);
                }
            }
        }
        return zoList;
    }

    /**
     *
     *
     * @param oppClass
     *
     * @return
     */
    public static String getProductSummary(OpportunityLineItemCustomClass oppClass) {
        String returnValue = '';
        List<ZiggoOpportunityLineItem> zliList = new List<ZiggoOpportunityLineItem>();
        if (oppClass != null) {
            if (oppClass.records != null) {
                if (oppClass.records.size() > 0) {
                    for (OpportunityLineItemRecords rec : oppClass.records) {
                        /**returnValue += 'Name: '+ rec.Name+'\n';
                        returnValue += 'Product: '+ rec.Product+'\n';
                        returnValue += 'One Off Price: '+ rec.LG_OneOffPrice+'\n';
                        returnValue += 'Quantity: '+ rec.Quantity+'\n';
                        String contractTerm = '';
                        if (!String.isEmpty(rec.LG_ContractTermTextFormula)) {
                            contractTerm = rec.LG_ContractTermTextFormula;
                        }
                        returnValue += 'Contract Term: '+ contractTerm+'\n';
                        returnValue += '------------\n';*/


                        ZiggoOpportunityLineItem zli = new ZiggoOpportunityLineItem();
                        zli.externalId = rec.Id;
                        zli.product = rec.Product;
                        zli.oneOffPrice = rec.LG_OneOffPrice;
                        zli.quantity = String.valueOf(rec.Quantity);
                        zli.contractTerm = rec.LG_ContractTermTextFormula;
                        zliList.add(zli);
                    }
                }
            }
        }
        returnValue = JSON.serialize(zliList);
        return returnValue;
    }

    private class ResponseCampaignWrapper {
        Integer totalSize {get;set;}
        Boolean done {get;set;}
        String nextRecordsUrl {get;set;}
        List<CampaignClass> records {get;set;}
    }

    private class ResponseCampaignMemberWrapper {
        Integer totalSize {get;set;}
        Boolean done {get;set;}
        String nextRecordsUrl {get;set;}
        List<CampaignMemberClass> records {get;set;}
    }

    private class ResponseLeadWrapper {
        Integer totalSize {get;set;}
        Boolean done {get;set;}
        String nextRecordsUrl {get;set;}
        List<LeadClass> records {get;set;}
    }

    private class ResponseTaskWrapper {
        Integer totalSize {get;set;}
        Boolean done {get;set;}
        String nextRecordsUrl {get;set;}
        List<mZiggo_Task_Object> records {get;set;}
    }


    private class ResponseWrapper {
        Integer totalSize {get;set;}
        Boolean done {get;set;}
        String nextRecordsUrl {get;set;}
        List<mZiggo_Opportunity_Object> records {get;set;}
    }

    private class LeadClass {
        Id Id {set;get;}
        String Name {set;get;}
        String Status {set;get;}
        String LG_ChamberOfCommerceNumber {set;get;}
        Datetime LastModifiedDate {get;set;}
        String Street {set;get;}
        String City {set;get;}
        String PostalCode {set;get;}
        String Country {set;get;}

        private ResponseTaskWrapper OpenActivities {get;set;}
        private ResponseTaskWrapper ActivityHistories {get;set;}
    }

    private class CampaignClass {
        Id Id {set;get;}
        String Name {set;get;}
        String Status {set;get;}
        String Type {get;set;}
        Datetime LastModifiedDate {get;set;}
        private OwnerInfo Parent {get;set;}
    }

    private class CampaignMemberClass {
        Id Id {set;get;}
        String CampaignId {set;get;}
        String LeadId {set;get;}
        String Name {get;set;}
        Datetime LastModifiedDate {get;set;}
        private LeadClass Lead {get;set;}
    }

    private class mZiggo_Task_Object {
        Id Id {set;get;}
        String Name {set;get;}
        String LG_Result {set;get;}
        String Status {set;get;}
        String Subject {set;get;}
        String Type {set;get;}
        String Partner_Account {set;get;}
        Datetime LastModifiedDate {get;set;}
        Date ActivityDate {get;set;}
        private OwnerInfo Owner {get;set;}
    }

    /**
     *
     */
    private class mZiggo_Opportunity_Object{
        Id Id {set;get;}
        String Name {set;get;}
        String OwnerName {set;get;}
        String StageName {set;get;}
        String Owner_Company {set;get;}
        String LG_NEWSalesChannel {set;get;}
        Decimal Probability {set;get;}
        Date CloseDate {set;get;}
        String Type {set;get;}
        Decimal  Amount {set;get;}
        String LG_AccntPostalPostalCode {set;get;}
        String LG_AccntPostalCity {set;get;}
        String LG_AccntPostalHouseNumber {set;get;}
        String LG_AccntPostalHouseNumberExtension {set;get;}
        String LG_AccntPostalStreet {set;get;}
        String LG_ProductsInBasketHidden {set;get;}
        Datetime LastModifiedDate {get;set;}

        private AccountInfo Account {get;set;}
        private OwnerInfo Owner {get;set;}
        private OpportunityLineItemCustomClass OpportunityLineItems {get;set;}
    }

    private class OpportunityLineItemCustomClass {
        List<OpportunityLineItemRecords> records {get;set;}
    }

    private class OpportunityLineItemRecords {
        String Id {get;set;}
        String Name {get;set;}
        String Product {get;set;}
        String LG_OneOffPrice {get;set;}
        Decimal Quantity {get;set;}
        String LG_ContractTermTextFormula {get;set;}
    }

    private class AccountInfo {
        String Name {get;set;}
        String LG_ChamberOfCommerceNumber {get;set;}
    }

    private class OwnerInfo {
        String Name {get;set;}
        String Id {get;set;}
    }

    public class ZiggoOpportunityLineItem {
        public String externalId {get;set;}
        public String product {get;set;}
        public String oneOffPrice {get;set;}
        public String quantity {get;set;}
        public String contractTerm {get;set;}
    }

    /**
     *
     *
     * @param ts
     * @param objName
     * @param amount
     * @param lastModifiedDate
     * @param nextUrl
     *
     * @return
     */
    private ZiggoSync__c logLatestRun(Datetime ts, String objName, Integer amount, DateTime lastModifiedDate, String nextUrl) {
        ZiggoSync__c syncRecord = new ZiggoSync__c();
        syncRecord.LastRun__c = ts;
        syncRecord.ObjectTranferred__c = objName;
        syncRecord.NumberOfRecordsTransferred__c = amount;
        syncRecord.LastModifiedDate__c = lastModifiedDate;
        syncRecord.NextUrl__c = nextUrl;
        return syncRecord;
    }

    public static void addProducts(List<ZiggoOpportunity__c> newZiggoOppList) {
        List<ZiggoOpportunityLineItem__c> insertItems = new List<ZiggoOpportunityLineItem__c>();
        Set<Id> ziggoOppIdSet = new Set<Id>();
        //Set<Id> oldProdListToDelete = new Set<Id>();
        for (ZiggoOpportunity__c zo : newZiggoOppList) {
            ziggoOppIdSet.add(zo.Id);
            if (!String.isEmpty(zo.Products__c)) {
                try {
                    JSONParser jp = JSON.createParser(zo.Products__c);
                    List<CSZiggoSync.ZiggoOpportunityLineItem> zoliList = (List<CSZiggoSync.ZiggoOpportunityLineItem>) jp.readValueAs(List<CSZiggoSync.ZiggoOpportunityLineItem>.class);
                    for (CSZiggoSync.ZiggoOpportunityLineItem zoli : zoliList) {
                        ZiggoOpportunityLineItem__c zoliNew = new ZiggoOpportunityLineItem__c();
                        zoliNew.ZiggoOpportunity__c = zo.Id;
                        zoliNew.External_ID__c = zoli.externalId;
                        zoliNew.Contract_Term__c = zoli.contractTerm;
                        zoliNew.One_Off_Price__c = zoli.oneOffPrice;
                        zoliNew.Product__c = zoli.product;
                        zoliNew.Quantity__c = zoli.quantity;
                        insertItems.add(zoliNew);
                    }
                } catch(Exception e) {}
            }
        }

        /** Query prod to delete */
        List<ZiggoOpportunityLineItem__c> zoLiToDelete = [Select Id From ZiggoOpportunityLineItem__c Where ZiggoOpportunity__c IN : ziggoOppIdSet];
        system.debug('##zoLiToDelete: '+zoLiToDelete);
        if (zoLiToDelete.size() > 0) {
            delete zoLiToDelete;
        }
        system.debug('##insertItems: '+insertItems);
        if (insertItems.size() > 0) {
            insert insertItems;
        }
    }

    /**
     *
     *
     * @param kvkList
     *
     * @return
     */
    public static Map<String, Map<String,Id>> seachKvk(List<String> kvkList) {
        Map<String, Map<String,Id>> returnValue = new Map<String, Map<String,Id>>();
        Set<String> excludeKVK = new Set<String>();
        excludeKVK.add('00000000');

        List<Lead> lList = [Select Id, KVK_number__c From Lead Where KVK_number__c IN : kvkList and KVK_number__c not in :excludeKVK];
        List<Account>  aList = [Select Id, KVK_number__c From Account Where KVK_number__c IN : kvkList and KVK_number__c not in :excludeKVK];

        for (Lead l : lList) {
            Map<String, Id> kvkMap = new Map<String, Id>();
            kvkMap.put('Lead', l.Id);
            returnValue.put(l.KVK_number__c, kvkMap);
        }

        for (Account a : aList) {
            if (returnValue.containsKey(a.KVK_number__c)) {
                Map<String, Id> kvkMap = returnValue.get(a.KVK_number__c);
                kvkMap.put('Account', a.Id);
                returnValue.put(a.KVK_number__c, kvkMap);
            } else {
                Map<String, Id> kvkMap = new Map<String, Id>();
                kvkMap.put('Account', a.Id);
                returnValue.put(a.KVK_number__c, kvkMap);
            }
        }
        return returnValue;
    }
}