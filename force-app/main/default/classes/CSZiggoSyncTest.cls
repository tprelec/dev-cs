@isTest
public class CSZiggoSyncTest {

    @TestSetup
    static void setup() {
        TestUtils.autoCommit = false;

        // Data preparation for destination records
        TestUtils.autoCommit = true;
        User admin = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;
        //Account acc = TestUtils.createAccount(admin);
        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c='12345678';
        upsert acc;

        Lead l = TestUtils.createLead();
        l.KVK_number__c='12345678';
        upsert l;

        Campaign c = new Campaign(Name='Test');
        insert c;

        CampaignMember cm = new CampaignMember();
        cm.CampaignId = c.Id;
        cm.LeadId = l.Id;
        insert cm;
    }

    @isTest static void checkDefaultObjectName(){
        Test.startTest();
        CSZiggoSync c = new CSZiggoSync('');
        System.assertEquals('Opportunity', c.objectName);
        Test.stopTest();
    }

    @isTest static void checkSetting(){
        Test.startTest();
        CSZiggoSync c = new CSZiggoSync('');
        System.assertEquals(true, c.checkSetting());
        Test.stopTest();
    }

    @isTest static void runAllOpportunty(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        CSZiggoSync c = new CSZiggoSync('');
        c.runAll();
        Test.stopTest();
    }

    @isTest static void runAllOpportuntyUpdate(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        CSZiggoSync c = new CSZiggoSync('');
        c.runAll();

        List<ZiggoOpportunity__c> tmpList = [Select Id From ZiggoOpportunity__c];
        update tmpList;
        Test.stopTest();
    }

    @isTest static void runAllLeadStatus(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        CSZiggoSync c = new CSZiggoSync('LeadStatus');
        c.runAll();
        Test.stopTest();
    }

    @isTest static void runAllLeadStatusUpdate(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        CSZiggoSync c = new CSZiggoSync('LeadStatus');
        c.runAll();

        List<ZiggoLeadStatus__c> tmpList = [Select Id From ZiggoLeadStatus__c];
        update tmpList;
        Test.stopTest();
    }

    @isTest static void runAllTask(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        CSZiggoSync c = new CSZiggoSync('Task');
        c.runAll();
        Test.stopTest();
    }

    @isTest static void runAllTaskUpdate(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        CSZiggoSync c = new CSZiggoSync('Task');
        c.runAll();

        List<ZiggoActivities__c> tmpList = [Select Id From ZiggoActivities__c];
        update tmpList;
        Test.stopTest();
    }

    /**@isTest static void runDirect(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        Database.executeBatch(new CSZiggoSync('Opportunity'), 200);
        Test.stopTest();
    }*/

    @isTest static void runSchedule(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        CSZiggoSync c = new CSZiggoSync('Opportunity');
        c.execute(null);
        Test.stopTest();
    }

    @isTest static void test_CsZiggoOpportunitySchedule(){

        Test.startTest();

        CsZiggoOpportunitySchedule c = new CsZiggoOpportunitySchedule();
        c.testData();
        Test.stopTest();
    }

    @isTest static void test_CsZiggoTaskSchedule(){

        Test.startTest();

        CsZiggoTaskSchedule c = new CsZiggoTaskSchedule();
        c.testData();
        Test.stopTest();
    }

    @isTest static void test_CsZiggoLeadStatusSchedule(){

        Test.startTest();

        CsZiggoLeadStatusSchedule c = new CsZiggoLeadStatusSchedule();
        c.testData();
        Test.stopTest();
    }

    @isTest static void runAllCampaign() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        CSZiggoSync c = new CSZiggoSync('Campaign');
        c.runAll();
        Test.stopTest();
    }

    @isTest static void runAllCampaignMember() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CsZiggoSyncMock());
        CSZiggoSync c1 = new CSZiggoSync('CampaignMember');
        c1.runAll();

        ZiggoCampaignMemberTriggerHandler c2 = new ZiggoCampaignMemberTriggerHandler();
        c2.testDataCoverage();
        Test.stopTest();
    }

    @isTest static void test_CsZiggoCampaign(){

        Test.startTest();

        CsZiggoCampaign c = new CsZiggoCampaign();
        c.testData();
        Test.stopTest();
    }

    @isTest static void test_CsZiggoCampaignMember(){

        Test.startTest();

        CsZiggoCampaignMember c = new CsZiggoCampaignMember();
        c.testData();
        Test.stopTest();
    }
}