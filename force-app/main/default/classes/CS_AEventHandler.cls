global with sharing class CS_AEventHandler extends cscfga.AEventHandler {

    public Id basketId;
   
    public CS_AEventHandler(){
    }
    
    public CS_AEventHandler(Id basketId){
        this.basketId = basketId;
    }
    public override void handleEvent(String eventName, Object payload){
        System.debug('handleEvent');
        if(eventName == 'revalidateConfigurations_finished'){
            Set<Id> ids = new Set<Id>();
            ids.add(basketId);
            cscfga.ProductConfigurationBulkActions.calculateTotals(ids);
        }
    }
    
    public override System.Type getClass(){
        return CS_AEventHandler.class;
    }
    
}