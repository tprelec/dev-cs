public class CS_AaJSON_switch {
    
    public static void transformEligibleAttributesToJSON(){
        Id pdRecordType = getRecordType();
        List<cscfga__Product_Definition__c> pDefs = getProductDefinitionIds(pdRecordType);
        List<cscfga__Attribute_Definition__c> attDefinitions = getAttributeDefinitions(pDefs);
        if(attDefinitions.size()>0){
            List<cscfga__Attribute_Definition__c> attsToUpdate = new List<cscfga__Attribute_Definition__c>();
            for (cscfga__Attribute_Definition__c attDef : attDefinitions) {
                    
                if(eligibleForJSON(attDef,pDefs)==true){
                    attDef.cscfga__store_as_json__c = true;
                    attsToUpdate.add(attDef);
                }
                
                
            }
            
            if(attsToUpdate!=null && attsToUpdate.size()>0){
                update attsToUpdate;
                System.debug('These attributes will be updated');
                System.debug(attsToUpdate);
            }
        }
    }
    
    private static List<cscfga__Attribute_Definition__c> getAttributeDefinitions(List<cscfga__Product_Definition__c> pDefs){
        List<cscfga__Attribute_Definition__c> attDefinitions;
        
        if(pDefs.size()>0){
            Set<Id> pdIds = new Set<Id>();
            
            for(cscfga__Product_Definition__c pd : pDefs){
                pdIds.add(pd.Id);
            }
            attDefinitions = [select id, Name,cscfga__Type__c,cscfga__Is_Line_Item__c, cscfga__store_as_json__c,cscfga__Product_Definition__c from cscfga__Attribute_Definition__c where cscfga__Product_Definition__c in :pdIds];
        }
        
        return attDefinitions;
    }
    
    private static Id getRecordType(){
        Id productDefRecTypeId = [SELECT id from RecordType where Name ='Product Definition'].Id;
        return productDefRecTypeId;
    }
    
    private static List<cscfga__Product_Definition__c> getProductDefinitionIds(Id productDefRecTypeId){
        List<cscfga__Product_Definition__c> pdIds = [select Id,Name from cscfga__Product_Definition__c where RecordTypeId = :productDefRecTypeId];
        return pdIds;
    }
    
    private static Boolean eligibleForJSON(cscfga__Attribute_Definition__c attrDef, List<cscfga__Product_Definition__c> pDefs){
        Boolean result = false;
        if((attrDef.cscfga__Type__c !='Related Product') && (attrDef.cscfga__Is_Line_Item__c !=true)){
            result = additionalCheck(attrDef,pDefs);
        }
        else{
            //System.debug('---Related product ir Line Item');
        }
        
        return result;
    }
    
    private static String getPDName(Id pdId, List<cscfga__Product_Definition__c> pdList){
        String result;
        for(cscfga__Product_Definition__c pd : pdList){
            if(pd.Id == pdId){
                result = pd.Name;
            }
        }
        return result;
    }
    
    private static Boolean additionalCheck(cscfga__Attribute_Definition__c attDef, List<cscfga__Product_Definition__c> pDefs){
        Boolean result = false;
        if(getPDName(attDef.cscfga__Product_Definition__c,pDefs) == 'Access Infrastructure'){
            
            if((attDef.Name !='KeepInvalid')&&
            (attDef.Name !='CodecRefresh')&&
            (attDef.Name !='Codec')&&
            (attDef.Name !='Codec select')
            &&(attDef.Name !='OneFixed Configuration select')
            &&(attDef.Name !='Site Check')
            &&(attDef.Name !='Clear Configuration')
            &&(attDef.Name !='One Net')
            &&(attDef.Name !='IPVPN')
            &&(attDef.Name !='Managed Internet')
            &&(attDef.Name !='One Fixed')
            &&(attDef.Name !='RequiresReClone')
            &&(attDef.Name !='Site Check Id')
            &&(attDef.Name !='PBX')
            &&(attDef.Name !='Total Bandwidth Down')
            &&(attDef.Name !='Total Bandwidth Down Entry')
            &&(attDef.Name !='Total Bandwidth Down Premium')
            &&(attDef.Name !='Total Bandwidth Up')
            &&(attDef.Name !='Total Bandwidth Up Entry')
            &&(attDef.Name !='Total Bandwidth Up Premium')
            &&(attDef.Name !='Result Check')
            &&(attDef.Name !='Premium Applicable')
            &&(attDef.Name !='Vendor')
            &&(attDef.Name !='Region')
            &&(attDef.Name !='Access Type')){
                result = true;
            }
            else{
                //System.debug('---Special condition -- Access');
            }
        }
        else if(getPDName(attDef.cscfga__Product_Definition__c,pDefs) == 'Mobile CTN profile'){
            if((attDef.Name !='Mobile CTN addons')){
                result = true;
            }
            else{
                //System.debug('---Special condition -- Mobile CTN profile');
            }
        }
        else if(getPDName(attDef.cscfga__Product_Definition__c,pDefs) == 'IP Pin'){
            if((attDef.Name !='Add On Recurring Charge')){
                result = true;
            }
            else{
                //System.debug('---Special condition -- IP Pin');
            }
        }
        else{
            result = true;
        }
        return result;
    }
    
}