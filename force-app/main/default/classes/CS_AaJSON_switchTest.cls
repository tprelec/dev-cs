@isTest
public class CS_AaJSON_switchTest {
    private static testMethod void test() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList);  
        insert simpleUser;
	    System.runAs (simpleUser) { 
	    
	    Framework__c frameworkSetting = new Framework__c();
	    frameworkSetting.Framework_Sequence_Number__c = 2;
	    insert frameworkSetting;
	    
	    Contract_Generation_Settings__c contGenSettings = new Contract_Generation_Settings__c();
	    contGenSettings.Document_template_name_indirect__c = 'TEST INDIRECT';
	    contGenSettings.Document_template_name_direct__c = 'TEST DIRECT';
	    insert contGenSettings;
	    
	    PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
	    insert priceResetSetting;
	    
	    Account testAccount = CS_DataTest.createAccount('Test Account');
	    insert testAccount;
	    
	    Account testAccount2 = CS_DataTest.createAccount('Test2 Account2');
	    insert testAccount2;
	    
	    Contact contact1 = new Contact(
            AccountId = testAccount.id,
            LastName = 'Last',
            FirstName = 'First',
            Contact_Role__c = 'Consultant',
            
            Email = 'test@vf.com'   
        );
        insert contact1;
        
        
        Contact contact2 = new Contact(
            AccountId = testAccount2.id,
            LastName = 'Last',
            FirstName = 'Second',
            Contact_Role__c = 'Consultant',
            
            Email = 'test2@vf.com'   
        );
        insert contact2;
	    
	    testAccount.Authorized_to_sign_1st__c = contact1.Id;
	    testAccount.Authorized_to_sign_2nd__c = contact1.Id;
	    testAccount.Contract_rule_no_mailing__c = true;
	    testAccount.Frame_Work_Agreement__c = 'VFZA-2018-8';
	    testAccount.Version_FWA__c = 3;
	    testAccount.Framework_agreement_date__c = Date.today();
	    update testAccount;
	    
	    
	    
	    Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
	    insert testOpp;
	    
	    cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
        basket.cscfga__Basket_Status__c = 'Valid';
        basket.Primary__c = true;
        basket.csbb__Synchronised_with_Opportunity__c = true;
        basket.csbb__Account__c = testAccount.Id;
        
        insert basket;
        
        
        cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        testDef.Product_Type__c = 'Fixed';
        insert testDef;
        
        cscfga__Product_Configuration__c testConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Access Infrastructure',basket.Id);
        insert testConf;
        
        cscfga__Attribute_Definition__c jsonAttDef = new cscfga__Attribute_Definition__c();
        jsonAttDef.cscfga__Type__c = 'Calculation';
        jsonAttDef.cscfga__Data_Type__c = 'String';
        jsonAttDef.Name = 'JSON';
        jsonAttDef.cscfga__store_as_json__c = true;
        jsonAttDef.cscfga__Is_Line_Item__c = false;
        jsonAttDef.cscfga__Product_Definition__c = testDef.Id;
        insert jsonAttDef;
        
        cscfga__Attribute__c jsonAtt= new cscfga__Attribute__c();
        jsonAtt.Name = 'JSON';
        jsonAtt.cscfga__Value__c = 'TEST';
        jsonAtt.cscfga__Product_Configuration__c = testConf.Id;
        insert jsonAtt;
        
        cscfga__Attribute_Definition__c nonJsonAttDef = new cscfga__Attribute_Definition__c();
        nonJsonAttDef.cscfga__Type__c = 'Calculation';
        nonJsonAttDef.cscfga__Data_Type__c = 'String';
        nonJsonAttDef.Name = 'No';
        nonJsonAttDef.cscfga__store_as_json__c = false;
        nonJsonAttDef.cscfga__Is_Line_Item__c = true;
        nonJsonAttDef.cscfga__Product_Definition__c = testDef.Id;
        insert nonJsonAttDef;
        
        cscfga__Attribute__c nonJsonAtt= new cscfga__Attribute__c();
        nonJsonAtt.Name = 'No';
        nonJsonAtt.cscfga__Value__c = 'TEST';
        nonJsonAtt.cscfga__Product_Configuration__c = testConf.Id;
        insert nonJsonAtt;
        
         cscfga__Product_Definition__c mobileDef = CS_DataTest.createProductDefinition('Mobile CTN profile');
         mobileDef.Product_Type__c = 'Mobile';
        insert mobileDef;
        
         cscfga__Product_Configuration__c mobileConf = CS_DataTest.createProductConfiguration(mobileDef.Id, 'Mobile CTN profile',basket.Id);
        insert mobileConf;
        
        cscfga__Attribute_Definition__c mobileJsonAttDef = new cscfga__Attribute_Definition__c();
        mobileJsonAttDef.cscfga__Type__c = 'Calculation';
        mobileJsonAttDef.cscfga__Data_Type__c = 'String';
        mobileJsonAttDef.Name = 'Mobile';
        mobileJsonAttDef.cscfga__store_as_json__c = true;
        mobileJsonAttDef.cscfga__Is_Line_Item__c = true;
        mobileJsonAttDef.cscfga__Product_Definition__c = mobileDef.Id;
        insert mobileJsonAttDef;
        
        cscfga__Attribute__c mobileNonJsonAtt= new cscfga__Attribute__c();
        mobileNonJsonAtt.Name = 'Mobile';
        mobileNonJsonAtt.cscfga__Value__c = 'TEST';
        mobileNonJsonAtt.cscfga__Product_Configuration__c = mobileConf.Id;
        insert mobileNonJsonAtt;
        
        cscfga__Product_Definition__c ipPinDef = CS_DataTest.createProductDefinition('IP Pin');
        ipPinDef.Product_Type__c = 'Fixed';
        insert ipPinDef;
        
         cscfga__Product_Configuration__c ipPinConf = CS_DataTest.createProductConfiguration(ipPinDef.Id, 'IP Pin',basket.Id);
        insert ipPinConf;
        
        cscfga__Attribute_Definition__c ipPinJsonAttDef = new cscfga__Attribute_Definition__c();
        ipPinJsonAttDef.cscfga__Type__c = 'Calculation';
        ipPinJsonAttDef.cscfga__Data_Type__c = 'String';
        ipPinJsonAttDef.Name = 'IP';
        ipPinJsonAttDef.cscfga__store_as_json__c = true;
        ipPinJsonAttDef.cscfga__Is_Line_Item__c = true;
        ipPinJsonAttDef.cscfga__Product_Definition__c = mobileDef.Id;
        insert ipPinJsonAttDef;
        
        cscfga__Attribute__c ipPinAtt= new cscfga__Attribute__c();
        ipPinAtt.Name = 'IP';
        ipPinAtt.cscfga__Value__c = 'TEST';
        ipPinAtt.cscfga__Product_Configuration__c = ipPinConf.Id;
        insert ipPinAtt;
        
        CS_AaJSON_switch.transformEligibleAttributesToJSON();
        
        // attDefinitions = [select id, Name,cscfga__Type__c,cscfga__Is_Line_Item__c, cscfga__store_as_json__c,cscfga__Product_Definition__c from cscfga__Attribute_Definition__c where cscfga__Product_Definition__c in :pdIds];
	    }
    }
}