@isTest
public class CS_AccessPriceItemLookupTest {
private static testMethod void testRequiredAttributes(){
         CS_AccessPriceItemLookup caLookup = new  CS_AccessPriceItemLookup();
        caLookup.getRequiredAttributes();
    }
  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
     System.runAs (simpleUser) { 
      
     
      Framework__c frameworkSetting = new Framework__c();
      frameworkSetting.Framework_Sequence_Number__c = 2;
      insert frameworkSetting;
      
      PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
     insert priceResetSetting;
      Account testAccount = CS_DataTest.createAccount('Test Account');
      insert testAccount;
      
      Sales_Settings__c ssettings = new Sales_Settings__c();
      ssettings.Postalcode_check_validity_days__c = 2;
      ssettings.Max_Daily_Postalcode_Checks__c = 2;
      ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
      ssettings.Postalcode_check_block_period_days__c = 2;
      ssettings.Max_weekly_postalcode_checks__c = 15;
      insert ssettings;
      
      
      //price items
      OrderType__c orderType = CS_DataTest.createOrderType();
      insert orderType;
      Product2 product1 = CS_DataTest.createProduct('Access Infrastructure', orderType);
      insert product1;
      
      Category__c accessCategory = CS_DataTest.createCategory('Access');
      insert accessCategory;
      
      Vendor__c vendor1 = CS_DataTest.createVendor('KPNWEAS');
      insert vendor1;
    
        
    
        cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(product1, accessCategory, 20480.0, 20480.0, vendor1, 'ONNET', 'OfficeTTR2BD');
        
        insert priceItem1;
      
      
      
      Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
      insert testOpp;
      
      
      cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        insert basket;
        
       
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
        
        cscfga__Product_Definition__c accessProductDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        accessProductDef.Product_Type__c = 'Fixed';
        accessProductDef.RecordTypeId = productDefinitionRecordType;
        insert accessProductDef;
        
        
        
        
        cscfga__Product_Configuration__c accessProductConf = CS_DataTest.createProductConfiguration(accessProductDef.Id, 'Access Infrastructure',basket.Id);
        accessProductConf.cscfga__Root_Configuration__c = null;
        accessProductConf.cscfga__Parent_Configuration__c = null;
        insert accessProductConf;
        
        
        
       
        csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(accessProductConf);
        pcr.csbb__Product_Configuration__c = accessProductConf.Id;
        insert pcr;
        
        
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('Access Type Calc', ',EthernetOverFiber,');
        searchFields.put('Region', 'RegionA');
        searchFields.put('Infra Sla', 'Best Effort');
        searchFields.put('Available bandwidth up', '20480.0');
        searchFields.put('Available bandwidth down', '20480.0');
        searchFields.put('Contract Duration', '36');
        searchFields.put('Proposition', 'IPVPN');
        searchFields.put('Line Type', 'New Line');
        
        
        
        
        CS_AccessPriceItemLookup caLookup = new CS_AccessPriceItemLookup();
        caLookup.doLookupSearch(searchFields, String.valueOf(accessProductDef.Id),null, 0, 0);
        
     
     }
  }

}