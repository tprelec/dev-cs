public with sharing class CS_ActivityTimelineController {
    @AuraEnabled
    public static CS_ActivityTimelineModel getActivityTimeline(Id recordId) {
        CS_ActivityTimelineModel returnValue = new CS_ActivityTimelineModel();
        List<CS_ActivityTimelineItemModel> itemsList = new List<CS_ActivityTimelineItemModel>();
        List<String> groupsList = new List<String>();
        addTasks(itemsList, recordId);
        itemsList.sort();
        addGroups(itemsList, groupsList);
        returnValue.items = itemsList;
        returnValue.groups = groupsList;
        return returnValue;
    }

    @AuraEnabled
    public static void addTasks(List<CS_ActivityTimelineItemModel> returnList, Id recordId) {
        List<Task> taskList = [
            SELECT Id,
                Type,
                Owner.Name,
                Status,
                Who.Name,
                Subject,
                Description,
                ActivityDate,
                Main_Task__c,
                Sequence__c,
                Internal_Number__c
            FROM Task
            WHERE WhatId = :recordId AND (Type = 'Other' OR Type = null)
        ];

        if (taskList.size() > 0) {
            for (Task t : taskList) {
                CS_ActivityTimelineItemModel taskItem = new CS_ActivityTimelineItemModel();
                taskItem.recordId = t.Id;
                taskItem.activityTimelineType = 'Task';
                taskItem.subject = t.Subject;
                taskItem.detail = t.Description;
                taskItem.actualDate = t.ActivityDate;
                taskItem.shortDate = t.ActivityDate != null ? t.ActivityDate.format() : null;
                taskItem.recipients = t.Who.Name;
                taskItem.assigned = t.Owner.Name;
                taskItem.mainTask = t.Main_Task__c;
                taskItem.sequence = Integer.valueOf(t.Sequence__c);
                taskItem.internalNumber = t.Internal_Number__c;
                if (t.status == 'Completed') {
                    taskItem.complete = true;
                } else {
                    taskItem.complete = false;
                }
                returnList.add(taskItem);
            }
        }
    }

    @AuraEnabled
    public static void addGroups(List<CS_ActivityTimelineItemModel> returnList, List<String> groups) {
        Set<String> groupsSet = new Set<String>();
        for (CS_ActivityTimelineItemModel item : returnList) {
            groupsSet.add(item.detail);
        }
        groups.addAll(groupsSet);
    }
}