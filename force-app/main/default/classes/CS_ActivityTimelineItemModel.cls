public class CS_ActivityTimelineItemModel implements Comparable {

    @AuraEnabled
    public Id recordId { get; set; }

    @AuraEnabled
    public String activityTimelineType { get; set; }

    @AuraEnabled
    public String subject { get; set; }

    @AuraEnabled
    public String detail { get; set; }

    @AuraEnabled
    public String shortDate { get; set; }

    @AuraEnabled
    public String eventTime { get; set; }

    @AuraEnabled
    public String recipients { get; set; }

    @AuraEnabled
    public String assigned { get; set; }

    @AuraEnabled
    public boolean complete { get; set; }

    public DateTime actualDate { get; set; }

    @AuraEnabled
    public String internalNumber { get; set; }

    @AuraEnabled
    public Integer sequence { get; set; }

    @AuraEnabled
    public Boolean mainTask { get; set; }

    public Integer compareTo(Object objToCompare) {
        Integer otherSequence = objToCompare != null ? ((CS_ActivityTimelineItemModel) objToCompare).Sequence : 0;
        if (this.sequence > otherSequence) {
            return 1;
        }
        if (this.sequence == otherSequence) {
            return 0;
        }
        return -1;
    }
}