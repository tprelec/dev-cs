public class CS_ActivityTimelineModel {
    @AuraEnabled
    public List<CS_ActivityTimelineItemModel> items { get; set; }

    @AuraEnabled
    public List<String> groups { get; set; }
}