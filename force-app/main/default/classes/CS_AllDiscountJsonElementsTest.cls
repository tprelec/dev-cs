@isTest
public class CS_AllDiscountJsonElementsTest {
    private static testMethod void callConstructorWithArguments() {
        // CS_DiscountCustomData testCustomData = new CS_DiscountCustomData('CustomDataString');
        Map<String, String> testCustomData = new Map<String, String>();
        testCustomData.put('test', 'test');
        CS_DiscountJsonElement discountJsonElement = new CS_DiscountJsonElement(0.0, 'type', 'source', 'discountCharge', 'description', testCustomData);
        List<CS_DiscountJsonElement> listOfDiscountJsonElements = new List<CS_DiscountJsonElement>();
        listOfDiscountJsonElements.add(discountJsonElement);
        CS_AllDiscountJsonElements allDiscountJsonElements = new CS_AllDiscountJsonElements(listOfDiscountJsonElements);
        System.assertNotEquals(null, allDiscountJsonElements);
    }
}