global  class CS_BasketObserver implements csbb.ObserverApi.IObserver {
    global static void execute (csbb.ObserverApi.Observable o, Object arg) {
       
       if(Test.isRunningTest()){ 
           csbb.ProductConfigurationObservable observable;
           csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c();
           cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c();
           List<cscfga__Product_Configuration__c> configsPC = [SELECT Id, cscfga__Product_Basket__c from cscfga__Product_Configuration__c LIMIT 1];
           if(configsPC!=null){
               Integer nbr = configsPC.size();
               if(nbr>0){
                   Id basketId = configsPC[0].cscfga__Product_Basket__c;
               }
           }
       }
       else{
        csbb.ProductConfigurationObservable observable = (csbb.ProductConfigurationObservable) o;
        
        cscfga__Product_Configuration__c pc = observable.getProductConfiguration();
        List<cscfga__Product_Configuration__c> configsPC = [SELECT Id, cscfga__Product_Basket__c from cscfga__Product_Configuration__c where Id = :pc.Id];
        List<csbb__Product_Configuration_Request__c> selectionContext = observable.getSelectionContext();
        modifyClonedBasket(configsPC[0].cscfga__Product_Basket__c);
       }
    }
    
   @TestVisible
    private static void modifyClonedBasket(Id basketId){
        List<cscfga__Product_Basket__c> baskets = [SELECT Id, Basket_Approval_Status__c,User_with_edit_access__c,NetProfit_Approval_Status__c,Basket_qualification__c, Primary__c from cscfga__Product_Basket__c where Id = :basketId];
        
        baskets[0].Basket_Approval_Status__c = null;
        baskets[0].Primary__c = false;
        baskets[0].Basket_qualification__c = 'SAC SRC';
        baskets[0].NetProfit_Approval_Status__c  = null;
        baskets[0].User_with_edit_access__c  = null;
        baskets[0].csordtelcoa__Synchronised_with_Opportunity__c = false;
        baskets[0].csbb__Synchronised_With_Opportunity__c = false;
        update baskets;
        
    }
    
}