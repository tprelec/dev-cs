@isTest
private class CS_BasketSnapshotManagerTest 
{   
    testMethod static void CS_BasketSnapshotManagerTest1() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            
            Framework__c frameworkSetting = new Framework__c();
        frameworkSetting.Framework_Sequence_Number__c = 2;
        insert frameworkSetting;
        
        PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
           
       insert priceResetSetting;
        
        Sales_Settings__c ssettings = new Sales_Settings__c();
        ssettings.Postalcode_check_validity_days__c = 2;
        ssettings.Max_Daily_Postalcode_Checks__c = 2;
        ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
        ssettings.Postalcode_check_block_period_days__c = 2;
        ssettings.Max_weekly_postalcode_checks__c = 15;
        insert ssettings;
            
            CS_Basket_Snapshot_Settings__c pcSettings = new CS_Basket_Snapshot_Settings__c();
            pcSettings.Output_Strategy__c = 'Product Configuration';
            pcSettings.Cleanup_Process__c = 'In Transaction';
            pcSettings.Use_Field_Type_Conversion__c = true;
            insert pcSettings;
            Account account = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Account',
                Type = 'End Customer'
            );
            insert account;

            Contact contact = new Contact(
                AccountId = account.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                
                Email = 'test@vf.com'   
            );
            insert contact;

            Opportunity opportunity = new Opportunity(
                Name = 'New Opportunity',
                OwnerId = UserInfo.getUserId(),
                StageName = 'Qualification',
                Probability = 0,
                CloseDate = system.today(),
                //ForecastCategoryName = 'Pipeline',
                AccountId = account.id
            );
            insert opportunity;

            cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
                Name = 'PD1',
                cscfga__Description__c = 'PD1 Desc',
                Snapshot_Object__c ='CS_Basket_Snapshot_Transactional__c'
            );
            pd.Product_Type__c = 'Mobile';
            insert pd;

            cscfga__Attribute_Definition__c ad = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'AD1',
                Snapshot_Attribute_Value_Field__c = 'OneOffPrice__c'
               
            );
            insert ad;
             cscfga__Attribute_Definition__c adQ = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'Quantity',
                Snapshot_Attribute_Value_Field__c = 'Quantity__c'
               
            );
            insert adQ;
            
            cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
                Name = 'New Basket',
                OwnerId = UserInfo.getUserId(),
                cscfga__Opportunity__c = opportunity.Id,
                Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
            );
            insert basket;
            
            cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 50.00,
                cscfga__total_recurring_charge__c = 10.00,
                Name='PCParent'
            );
            insert pc;

            cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 40.00,
                cscfga__total_recurring_charge__c = 10.00,
                cscfga__Parent_Configuration__c = pc.Id,
                cscfga__Root_Configuration__c = pc.Id,
                Name='PC1'
            );
            insert pc1;

            cscfga__Attribute__c att = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '10',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = ad.Id,
                cscfga__Price__c = 50.00
            );
            insert att;
            
            cscfga__Attribute__c attQ = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '2',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = adQ.Id,
                cscfga__Price__c = 50.00
            );
            insert attQ;
            
             
            
            List<Id> basketList = new List<Id>{basket.Id};
            List<cscfga__Product_Basket__c> basketList2 = new List<cscfga__Product_Basket__c>{basket};
            Test.startTest();
            CS_BasketSnapshotManager.TakeBasketSnapshotById(basket.Id,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshotById(basketList,true);
            CS_BasketSnapshotManager.TakeBasketSnapshot(basket,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshot(basketList2,true);
            
            List<CS_Basket_Snapshot_Settings__c> pcSettingsNew = [SELECT Id, Output_Strategy__c from CS_Basket_Snapshot_Settings__c where Id = :pcSettings.Id];
            pcSettingsNew[0].Output_Strategy__c = 'Attribute';
            update pcSettingsNew;
            
            CS_BasketSnapshotManager.TakeBasketSnapshotById(basket.Id,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshotById(basketList,true);
            CS_BasketSnapshotManager.TakeBasketSnapshot(basket,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshot(basketList2,true);
            Test.stopTest();
        }
    }

    @IsTest
    static void CS_BasketSnapshotManagerTest2(){
        
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            Framework__c frameworkSetting = new Framework__c();
            frameworkSetting.Framework_Sequence_Number__c = 2;
            insert frameworkSetting;
            
            PriceReset__c priceResetSetting = new PriceReset__c();
           
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
               
            insert priceResetSetting;
            
            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;
                
            CS_Basket_Snapshot_Settings__c pcSettings = new CS_Basket_Snapshot_Settings__c();
            pcSettings.Output_Strategy__c = 'Product Configuration';
            pcSettings.Cleanup_Process__c = 'In Transaction';
            pcSettings.Use_Field_Type_Conversion__c = true;
            insert pcSettings;
            Account account = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Account',
                Type = 'End Customer'
            );
            insert account;

            Contact contact = new Contact(
                AccountId = account.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                
                Email = 'test@vf.com'   
            );
            insert contact;

            Opportunity opportunity = new Opportunity(
                Name = 'New Opportunity',
                OwnerId = UserInfo.getUserId(),
                StageName = 'Qualification',
                Probability = 0,
                CloseDate = system.today(),
                //ForecastCategoryName = 'Pipeline',
                AccountId = account.id
            );
            insert opportunity;

            cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
                Name = 'PD1',
                cscfga__Description__c = 'PD1 Desc',
                Snapshot_Object__c ='CS_Basket_Snapshot_Transactional__c'
            );
            pd.Product_Type__c = 'Mobile';
            insert pd;

            cscfga__Attribute_Definition__c attributeDeffinitionOneOff = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'OneOffPrice',
                Snapshot_Attribute_Value_Field__c = 'OneOffPrice__c'
                
            );
            insert attributeDeffinitionOneOff;
            
            cscfga__Attribute_Definition__c attributeDeffinitionQuantity = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'Quantity',
                Snapshot_Attribute_Value_Field__c = 'Quantity__c'
                
            );
            insert attributeDeffinitionQuantity;
            
            cscfga__Attribute_Definition__c attributeDeffinitionRecurring = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'RecurringPrice',
                Snapshot_Attribute_Value_Field__c = 'RecurringPrice__c'
                
            );

            insert attributeDeffinitionRecurring;

            cscfga__Product_Basket__c prodbasket = new cscfga__Product_Basket__c(
                Name = 'New Basket',
                OwnerId = UserInfo.getUserId(),
                cscfga__Opportunity__c = opportunity.Id,
                Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
            );
            insert prodbasket;
            
            cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = prodBasket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 50.00,
                cscfga__total_recurring_charge__c = 10.00,
                Discount_allowed__c = true,
                cscfga__discounts__c = '{"discounts":[{"version":"3-0-0","recordType":"group","memberDiscounts":[{"version":"3-0-0","type":"percentage","source":"Manual 1","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 10%","chargeType":"recurring","amount":10.0000}],"evaluationOrder":"serial"}]}',
                Name='PCParent'
            );
            insert pc;

            cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = prodBasket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 40.00,
                cscfga__total_recurring_charge__c = 10.00,
                cscfga__Parent_Configuration__c = pc.Id,
                cscfga__Root_Configuration__c = pc.Id,
                Name='PC1'
            );
            insert pc1;

            cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = prodBasket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 40.00,
                cscfga__total_recurring_charge__c = 10.00,
                cscfga__Parent_Configuration__c = pc.Id,
                cscfga__Root_Configuration__c = pc.Id,
                Discount_allowed__c = true,
                cscfga__discounts__c = '{"discounts":[{"version":"3-0-0","recordType":"group","memberDiscounts":[{"version":"3-0-0","type":"percentage","source":"Manual 1","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 10%","chargeType":"recurring","amount":10.0000}],"evaluationOrder":"serial"}]}',
                Name='PC2'
            );
            insert pc2;

            cscfga__Attribute__c attributeOneOff = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '10',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = attributeDeffinitionOneOff.Id,
                cscfga__Price__c = 50.00
            );
            insert attributeOneOff;
            
            cscfga__Attribute__c attributeQuantity = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '2',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = attributeDeffinitionQuantity.Id,
                cscfga__Price__c = 50.00
            );
            insert attributeQuantity;
            

            cscfga__Attribute__c attributeRecurring = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '2',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = attributeDeffinitionRecurring.Id,
                cscfga__Price__c = 50.00
            );
            insert attributeRecurring;

            List<Id> basketList = new List<Id>{prodbasket.Id};
            List<cscfga__Product_Basket__c> basketList2 = new List<cscfga__Product_Basket__c>{prodbasket};

            Test.startTest();
            CS_BasketSnapshotManager.TakeBasketSnapshotById(prodBasket.Id,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshotById(basketList,true);
            CS_BasketSnapshotManager.TakeBasketSnapshot(prodBasket,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshot(basketList2,true);
            
            List<CS_Basket_Snapshot_Settings__c> pcSettingsNew = [SELECT Id, Output_Strategy__c from CS_Basket_Snapshot_Settings__c where Id = :pcSettings.Id];
            pcSettingsNew[0].Output_Strategy__c = 'Attribute';
            update pcSettingsNew;
            
            CS_BasketSnapshotManager.TakeBasketSnapshotById(prodBasket.Id,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshotById(basketList,true);
            CS_BasketSnapshotManager.TakeBasketSnapshot(prodBasket,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshot(basketList2,true);

            List<CS_Basket_Snapshot_Transactional__c> snapshotList = [SELECT Id FROM CS_Basket_Snapshot_Transactional__c];
            System.assertEquals(3, snapshotList.size());

            Test.stopTest();

        }
    }

    @IsTest
    static void CS_BasketSnapshotManagerTest3(){
        
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            Framework__c frameworkSetting = new Framework__c();
            frameworkSetting.Framework_Sequence_Number__c = 2;
            insert frameworkSetting;
            
            PriceReset__c priceResetSetting = new PriceReset__c();
           
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
               
            insert priceResetSetting;
            
            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;
                
            CS_Basket_Snapshot_Settings__c pcSettings = new CS_Basket_Snapshot_Settings__c();
            pcSettings.Output_Strategy__c = 'Product Configuration';
            pcSettings.Cleanup_Process__c = 'In Transaction';
            pcSettings.Use_Field_Type_Conversion__c = true;
            insert pcSettings;
            Account account = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Account',
                Type = 'End Customer'
            );
            insert account;

            Contact contact = new Contact(
                AccountId = account.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                
                Email = 'test@vf.com'   
            );
            insert contact;

            Opportunity opportunity = new Opportunity(
                Name = 'New Opportunity',
                OwnerId = UserInfo.getUserId(),
                StageName = 'Qualification',
                Probability = 0,
                CloseDate = system.today(),
                //ForecastCategoryName = 'Pipeline',
                AccountId = account.id
            );
            insert opportunity;

            cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
                Name = 'PD1',
                cscfga__Description__c = 'PD1 Desc',
                Snapshot_Object__c ='CS_Basket_Snapshot_Transactional__c'
            );
            pd.Product_Type__c = 'Mobile';
            insert pd;

            cscfga__Attribute_Definition__c attributeDeffinitionOneOff = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'OneOffPrice',
                Snapshot_Attribute_Value_Field__c = 'OneOffPrice__c'
                
            );
            insert attributeDeffinitionOneOff;
            
            cscfga__Attribute_Definition__c attributeDeffinitionQuantity = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'Quantity',
                Snapshot_Attribute_Value_Field__c = 'Quantity__c'
                
            );
            insert attributeDeffinitionQuantity;
            
            cscfga__Attribute_Definition__c attributeDeffinitionRecurring = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'RecurringPrice',
                Snapshot_Attribute_Value_Field__c = 'RecurringPrice__c'
                
            );

            insert attributeDeffinitionRecurring;

            cscfga__Product_Basket__c prodbasket = new cscfga__Product_Basket__c(
                Name = 'New Basket',
                OwnerId = UserInfo.getUserId(),
                cscfga__Opportunity__c = opportunity.Id,
                Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
            );
            insert prodbasket;
            
            cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = prodBasket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 50.00,
                cscfga__total_recurring_charge__c = 10.00,
                Discount_allowed__c = true,
                cscfga__discounts__c = '{"discounts":[{"type":"percentage","source":"Manual 1","discountCharge":"recurring","description":"Recurring Charge - 13.1579%","amount":"13.1579"}]}',
                Name='PCParent'
            );
            insert pc;

            cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = prodBasket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 40.00,
                cscfga__total_recurring_charge__c = 10.00,
                cscfga__Parent_Configuration__c = pc.Id,
                cscfga__Root_Configuration__c = pc.Id,
                Name='PC1'
            );
            insert pc1;

            cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = prodBasket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 40.00,
                cscfga__total_recurring_charge__c = 10.00,
                cscfga__Parent_Configuration__c = pc.Id,
                cscfga__Root_Configuration__c = pc.Id,
                Discount_allowed__c = true,
                cscfga__discounts__c = '{"discounts":[{"type":"percentage","source":"Manual 1","discountCharge":"recurring","description":"Recurring Charge - 13.1579%","amount":"13.1579"}]}',
                Name='PC2'
            );
            insert pc2;

            cscfga__Attribute__c attributeOneOff = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '10',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = attributeDeffinitionOneOff.Id,
                cscfga__Price__c = 50.00
            );
            insert attributeOneOff;
            
            cscfga__Attribute__c attributeQuantity = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '2',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = attributeDeffinitionQuantity.Id,
                cscfga__Price__c = 50.00
            );
            insert attributeQuantity;
            

            cscfga__Attribute__c attributeRecurring = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '2',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = attributeDeffinitionRecurring.Id,
                cscfga__Price__c = 50.00
            );
            insert attributeRecurring;

            List<Id> basketList = new List<Id>{prodbasket.Id};
            List<cscfga__Product_Basket__c> basketList2 = new List<cscfga__Product_Basket__c>{prodbasket};

            Test.startTest();
            CS_BasketSnapshotManager.TakeBasketSnapshotById(prodBasket.Id,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshotById(basketList,true);
            CS_BasketSnapshotManager.TakeBasketSnapshot(prodBasket,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshot(basketList2,true);
            
            List<CS_Basket_Snapshot_Settings__c> pcSettingsNew = [SELECT Id, Output_Strategy__c from CS_Basket_Snapshot_Settings__c where Id = :pcSettings.Id];
            pcSettingsNew[0].Output_Strategy__c = 'Attribute';
            update pcSettingsNew;
            
            CS_BasketSnapshotManager.TakeBasketSnapshotById(prodBasket.Id,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshotById(basketList,true);
            CS_BasketSnapshotManager.TakeBasketSnapshot(prodBasket,true);
            CS_BasketSnapshotManager.TakeBasketsSnapshot(basketList2,true);

            List<CS_Basket_Snapshot_Transactional__c> snapshotList = [SELECT Id FROM CS_Basket_Snapshot_Transactional__c];
            System.assertEquals(3, snapshotList.size());

            Test.stopTest();

        }
    }
}