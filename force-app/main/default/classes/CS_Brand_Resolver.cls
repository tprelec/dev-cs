public without sharing class CS_Brand_Resolver {
	private static final String ERROR_MESSAGE_UNABLE_TO_RESOLVE = 'Unable to resolve brand for: ';
	private static final String ERROR_MESAGGE_TYPE_NOT_SUPPORTED = 'Type not supported: ';
	public static final Boolean ONLY_ONE_TYPE_OF_BRAND_AT_TIME = true;

	private static Map<Id, String> resolvedObjectCache = new Map<Id, String>();

	/**
	 * Checks if records are Vodafone brand
	 *
	 * @param  objectsToResolve List of objects to resolve
	 */
	public static Boolean isVodafoneRecord(List<sObject> objectsToResolve) {
		return getBrandForSObjectList(objectsToResolve) == 'Vodafone';
	}

	/**
	 * Determine the brand based on the first record of the list.
	 * This decision was made to optimize and reduce the number of queries in trigger context.
	 *
	 * @param   objectToResolve    Object to resolve
	 */
	public static String getBrandForObject(sObject objectToResolve) {
		if (ONLY_ONE_TYPE_OF_BRAND_AT_TIME && resolvedObjectCache.size() > 0) {
			return resolvedObjectCache.values()[0];
		}
		if (objectToResolve == null) {
			throw new BrandException(ERROR_MESSAGE_UNABLE_TO_RESOLVE + objectToResolve);
		}
		if (
			objectToResolve != null &&
			resolvedObjectCache != null &&
			resolvedObjectCache.get(objectToResolve.Id) != null
		) {
			return resolveFromCache(objectToResolve);
		} else {
			return resolveFromQuery(objectToResolve);
		}
	}

	public static String getBrandForSObjectList(List<sObject> listOfSObjects) {
		if (listOfSObjects != null && listOfSObjects.size() > 0) {
			return getBrandForObject(listOfSObjects[0]);
		}
		return null;
	}

	private static String resolveFromCache(sObject objectToResolve) {
		return resolvedObjectCache.get(objectToResolve.Id);
	}

	private static String resolveFromQuery(sObject objectToResolve) {
		resolvedObjectCache = new Map<Id, String>();
		if (objectToResolve instanceof Opportunity) {
			return getBrandForOpportunity((Opportunity) objectToResolve);
		} else if (objectToResolve instanceof cscfga__Product_Basket__c) {
			return getBrandForProductBasket((cscfga__Product_Basket__c) objectToResolve);
		} else if (objectToResolve instanceof cscfga__Product_Configuration__c) {
			return getBrandForProductConfiguration(
				(cscfga__Product_Configuration__c) objectToResolve
			);
		} else if (objectToResolve instanceof csbb__Product_Configuration_Request__c) {
			return getBrandForProductConfigurationRequest(
				(csbb__Product_Configuration_Request__c) objectToResolve
			);
		} else if (objectToResolve instanceof cscfga__Attribute__c) {
			return getBrandForAttribute((cscfga__Attribute__c) objectToResolve);
		} else if (objectToResolve instanceof OpportunityLineItem) {
			return getBrandOpportunityLineItem((OpportunityLineItem) objectToResolve);
		} else {
			throw new BrandException(
				ERROR_MESAGGE_TYPE_NOT_SUPPORTED +
				objectToResolve.getSObjectType().getDescribe().getName()
			);
		}
	}

	public static String getBrandForOpportunity(Opportunity opportunity) {
		// If ID is not present, it´s a BeforeInsert trigger action
		if (opportunity != null && String.isEmpty(opportunity.Id)) {
			return getBrandForOpportunityBeforeInsert(opportunity);
		}
		List<Opportunity> queriedOpportunity = [
			SELECT Brand__c
			FROM Opportunity
			WHERE id = :opportunity.Id
		];
		if (queriedOpportunity.size() > 0) {
			resolvedObjectCache.put(opportunity.Id, queriedOpportunity[0].Brand__c);
			return queriedOpportunity[0].Brand__c;
		}
		throw new BrandException(ERROR_MESSAGE_UNABLE_TO_RESOLVE + opportunity);
	}

	public static String getBrandForOpportunityBeforeInsert(Opportunity opportunity) {
		if ((String.isEmpty(opportunity.Brand__c))) {
			throw new BrandException(ERROR_MESSAGE_UNABLE_TO_RESOLVE + opportunity);
		}
		resolvedObjectCache.put(opportunity.Id, opportunity.Brand__c);
		return opportunity.Brand__c;
	}

	public static String getBrandForOpportunity(String opportunityId) {
		if (String.IsEmpty(opportunityId)) {
			String oppErrorMsg = 'Opportunity with ID ' + opportunityId;
			throw new BrandException(ERROR_MESSAGE_UNABLE_TO_RESOLVE + oppErrorMsg);
		}
		List<Opportunity> queriedOpportunity = [
			SELECT Brand__c
			FROM Opportunity
			WHERE id = :opportunityId
		];
		if (queriedOpportunity.IsEmpty()) {
			String oppErrorMsg = 'Opportunity with ID ' + opportunityId;
			throw new BrandException(ERROR_MESSAGE_UNABLE_TO_RESOLVE + oppErrorMsg);
		}
		resolvedObjectCache.put(opportunityId, queriedOpportunity[0].Brand__c);
		return queriedOpportunity[0].Brand__c;
	}

	public static String getBrandForProductBasket(cscfga__Product_Basket__c productBasket) {
		if (String.isEmpty(productBasket.Id)) {
			return getBrandProductBasketBeforeInsert(productBasket);
		}
		List<Opportunity> queriedOpportunity = [
			SELECT Brand__c
			FROM Opportunity
			WHERE
				id IN (
					SELECT cscfga__Opportunity__c
					FROM cscfga__Product_Basket__c
					WHERE id = :productBasket.Id
				)
		];

		if (queriedOpportunity.size() > 0) {
			resolvedObjectCache.put(productBasket.Id, queriedOpportunity[0].Brand__c);
			return queriedOpportunity[0].Brand__c;
		} else {
			return 'Ziggo';
		}
	}

	public static String getBrandProductBasketBeforeInsert(
		cscfga__Product_Basket__c productBasket
	) {
		if (String.isEmpty(productBasket.cscfga__Opportunity__c)) {
			if (
				productBasket != null &&
				productBasket.LG_CreatedFrom__c != null &&
				productBasket.LG_CreatedFrom__c == 'Tablet'
			) {
				resolvedObjectCache.put(productBasket.Id, 'Ziggo');
				return 'Ziggo';
			}
		}

		List<Opportunity> queriedOpportunity = [
			SELECT Brand__c
			FROM Opportunity
			WHERE Id = :productBasket.cscfga__Opportunity__c
		];

		if (queriedOpportunity.size() > 0) {
			resolvedObjectCache.put(productBasket.Id, queriedOpportunity[0].Brand__c);
			return queriedOpportunity[0].Brand__c;
		} else {
			return 'Ziggo';
		}
	}

	public static String getBrandForProductConfiguration(
		cscfga__Product_Configuration__c productConfiguration
	) {
		if (productConfiguration != null && String.isEmpty(productConfiguration.Id)) {
			return getBrandProductConfigurationBeforeInsert(productConfiguration);
		}

		List<cscfga__Product_Configuration__c> queriedProductConfiguration = [
			SELECT id, cscfga__Product_Basket__c
			FROM cscfga__Product_Configuration__c
			WHERE id = :productConfiguration.Id
		];

		// Is this an offer? if yes then return Ziggo?
		if (queriedProductConfiguration.size() == 0) {
			return 'Ziggo';
		}

		if (queriedProductConfiguration != null && queriedProductConfiguration.size() > 0) {
			List<Opportunity> queriedOpportunity = [
				SELECT Brand__c
				FROM Opportunity
				WHERE
					id IN (
						SELECT cscfga__Opportunity__c
						FROM cscfga__Product_Basket__c
						WHERE id = :queriedProductConfiguration[0].cscfga__Product_Basket__c
					)
			];
			if (queriedOpportunity.size() > 0) {
				resolvedObjectCache.put(productConfiguration.Id, queriedOpportunity[0].Brand__c);
				return queriedOpportunity[0].Brand__c;
			} else {
				return 'Ziggo';
			}
		}
		throw new BrandException(ERROR_MESSAGE_UNABLE_TO_RESOLVE + productConfiguration);
	}

	public static String getBrandProductConfigurationBeforeInsert(
		cscfga__Product_Configuration__c productConfiguration
	) {
		if (productConfiguration.cscfga__Product_Basket__c == null) {
			return 'Ziggo';
		}

		List<Opportunity> queriedOpportunity = [
			SELECT Brand__c
			FROM Opportunity
			WHERE
				Id IN (
					SELECT cscfga__Opportunity__c
					FROM cscfga__Product_Basket__c
					WHERE Id = :productConfiguration.cscfga__Product_Basket__c
				)
		];

		if (queriedOpportunity.size() > 0) {
			resolvedObjectCache.put(productConfiguration.Id, queriedOpportunity[0].Brand__c);
			return queriedOpportunity[0].Brand__c;
		} else {
			return 'Ziggo';
		}
	}

	public static String getBrandForProductConfiguration(String productConfigurationId) {
		if (String.IsEmpty(productConfigurationId)) {
			String errorMsg = 'Product Configuration record with ID: ' + productConfigurationId;
			throw new BrandException(ERROR_MESSAGE_UNABLE_TO_RESOLVE + errorMsg);
		}

		cscfga__Product_Configuration__c queriedProductConfiguration = [
			SELECT id, cscfga__Product_Basket__c
			FROM cscfga__Product_Configuration__c
			WHERE id = :productConfigurationId
		];

		if (
			queriedProductConfiguration != null &&
			String.isEmpty(queriedProductConfiguration.cscfga__Product_Basket__c)
		) {
			return 'Ziggo';
		}

		if (queriedProductConfiguration != null) {
			List<Opportunity> queriedOpportunity = [
				SELECT Brand__c
				FROM Opportunity
				WHERE
					id IN (
						SELECT cscfga__Opportunity__c
						FROM cscfga__Product_Basket__c
						WHERE id = :queriedProductConfiguration.cscfga__Product_Basket__c
					)
			];

			if (queriedOpportunity.size() > 0) {
				resolvedObjectCache.put(productConfigurationId, queriedOpportunity[0].Brand__c);
				return queriedOpportunity[0].Brand__c;
			} else {
				return 'Ziggo';
			}
		}

		throw new BrandException(ERROR_MESSAGE_UNABLE_TO_RESOLVE + queriedProductConfiguration);
	}

	public static String getBrandForProductConfigurationRequest(
		csbb__Product_Configuration_Request__c productConfigurationRequest
	) {
		if (String.IsEmpty(productConfigurationRequest.csbb__Opportunity_ID__c)) {
			return 'Ziggo';
		}
		return getBrandForOpportunity(productConfigurationRequest.csbb__Opportunity_ID__c);
	}

	public static String getBrandForAttribute(cscfga__Attribute__c attribute) {
		if (String.IsEmpty(attribute.cscfga__Product_Configuration__c)) {
			throw new BrandException(ERROR_MESSAGE_UNABLE_TO_RESOLVE + attribute);
		}

		return getBrandForProductConfiguration(attribute.cscfga__Product_Configuration__c);
	}

	public static String getBrandOpportunityLineItem(OpportunityLineItem oppProduct) {
		if (oppProduct == null) {
			throw new BrandException(
				ERROR_MESSAGE_UNABLE_TO_RESOLVE +
				' Opportunity Product is blank: ' +
				oppProduct
			);
		}

		if (String.isEmpty(oppProduct.OpportunityId)) {
			throw new BrandException(
				ERROR_MESSAGE_UNABLE_TO_RESOLVE +
				' Opportunity Product OpportunityId field is blank: ' +
				oppProduct
			);
		}
		return getBrandForOpportunity(oppProduct.OpportunityId);
	}

	public class BrandException extends Exception {
	}
}