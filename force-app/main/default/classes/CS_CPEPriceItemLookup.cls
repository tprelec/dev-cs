global with sharing class CS_CPEPriceItemLookup extends cscfga.ALookupSearch {
     
 public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit){
       
    System.debug('**** searchFields: ' + JSON.serializePretty(searchFields));   
    system.debug('productDefinitionID: ' + productDefinitionID);
    system.debug('pageOffset: ' + pageOffset);
    system.debug('pageLimit: ' + pageLimit);
       
    final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1; 
    Integer recordOffset = pageOffset * pageLimit;   
    
    List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>();

    Decimal availableBandwidthUp = ((searchFields.get('Available bandwidth up') == null) ||(searchFields.get('Available bandwidth up') == '')) ? 0 : Decimal.valueOf(searchFields.get('Available bandwidth up'));
    Decimal availableBandwidthDown = ((searchFields.get('Available bandwidth down') == null) ||(searchFields.get('Available bandwidth down') == '')) ? 0 : Decimal.valueOf(searchFields.get('Available bandwidth down'));
    String overbookingType = searchFields.get('Overbooking Type');
    String technologyType = searchFields.get('Technology type');
    Decimal sipChannels = ((searchFields.get('SIP Channels') == null) ||(searchFields.get('SIP Channels') == '')) ? 0 : Decimal.valueOf(searchFields.get('SIP Channels'));
    String codec = searchFields.get('Codec');

    
    String accessType = '%,' + searchFields.get('Access Type') + ',%';
    system.debug('accessType='+accessType);
    String qualityOfService = String.valueOf(searchFields.get('Quality of Service'));
    system.debug('qualityOfService='+qualityOfService);
    Integer numOfLAN = Integer.valueOf(searchFields.get('Num LAN'));
     system.debug('numOfLAN='+numOfLAN);
    Integer numOfWAN = Integer.valueOf(searchFields.get('Num WAN'));
    system.debug('numOfWAN='+numOfWAN);
    Integer numOfLANWAN = Integer.valueOf(searchFields.get('Num WANLAN'));
    system.debug('numOfLANWAN='+numOfLANWAN);
    String wirelessBackup = String.valueOf(searchFields.get('Wireless backup'));
    system.debug('wirelessBackup='+wirelessBackup);
    Boolean wirelessBackupBoolean = ((wirelessBackup == 'Yes') || (wirelessBackup == 'true'))? true:false;
    Integer numOfSfp = Integer.valueOf(searchFields.get('Num SFP'));
    system.debug('numOfSfp='+numOfSfp);
    
    system.debug('technologyType='+technologyType);
    system.debug('overbookingType='+overbookingType);
    
    
    
    String searchValue = searchFields.get('searchValue')==''?'%':'%'+searchFields.get('searchValue')+'%';   
    
    Integer sumLANWAN = numOfLAN+numOfWAN;
    
    if(overbookingType == 'IAD'){
        priceItems = [SELECT Id, Name, Vendor__c, Vendor_lookup__r.Name, Region__c,Duration__c,Service__c,SLA_Method__c,Hardware_Toeslag__c,CPE_SLA__c,Category__c, Category_lookup__r.Name, Infra_SLA__c,Overbooking__c,cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, 
        				Number_of_LAN__c, Number_of_WAN__c, Wireless_compatible__c, Number_of_LANWAN__c, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, cspmb__recurring_charge__c, cspmb__one_off_cost__c, cspmb__recurring_cost__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c,Product__c, WAN_LANWAN_M_LAN__c, 
        				Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c,
                        Extra_Product__c, ProductConfigNameText__c, Deal_Type_Text__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c,
                        Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c,Max_Quantity__c,Min_Quantity__c
                                             FROM cspmb__Price_Item__c
                                             WHERE cspmb__Is_Active__c = true AND OneFixed_Technology_Type__c = :technologyType AND Category_lookup__r.Name = 'CPE' AND Product__c != ''
                                             ORDER BY Name
                                             LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
                                             
       return priceItems;
    }
    else if (overbookingType == 'CUBE'){
        priceItems = [SELECT Id, Name, Vendor__c, Vendor_lookup__r.Name, Region__c,Duration__c,Service__c,SLA_Method__c,Hardware_Toeslag__c,CPE_SLA__c,Category__c, Category_lookup__r.Name, Infra_SLA__c,Overbooking__c,cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Number_of_LAN__c, Number_of_WAN__c, Wireless_compatible__c, Number_of_LANWAN__c, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, cspmb__recurring_charge__c, cspmb__one_off_cost__c, cspmb__recurring_cost__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c,Product__c, WAN_LANWAN_M_LAN__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c,
                        Extra_Product__c, ProductConfigNameText__c, Deal_Type_Text__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c,Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c,Max_Quantity__c,Min_Quantity__c
                                             FROM cspmb__Price_Item__c
                                             WHERE cspmb__Is_Active__c = true AND OneFixed_Technology_Type__c = :technologyType AND Category_lookup__r.Name = 'CPE' AND Product__c != '' AND OneFixed_Codec__c = :codec AND OneFixed_SIP_Channels__c = :sipChannels
                                             ORDER BY Name
                                             LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
        return priceItems;
    }
    else {
        if(qualityOfService == 'Yes'){
    system.debug('AN--HERE ');
        priceItems = [SELECT Id, Name, Vendor__c, Vendor_lookup__r.Name, Region__c,Duration__c,Service__c,SLA_Method__c,Hardware_Toeslag__c,CPE_SLA__c,Category__c, Category_lookup__r.Name, Infra_SLA__c,Overbooking__c,cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Number_of_LAN__c, Number_of_WAN__c, Wireless_compatible__c, Number_of_LANWAN__c, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, cspmb__recurring_charge__c, cspmb__one_off_cost__c, cspmb__recurring_cost__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c,
                        Product__c, WAN_LANWAN_M_LAN__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c,
                        Extra_Product__c, ProductConfigNameText__c, Deal_Type_Text__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c,Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c,Max_Quantity__c,Min_Quantity__c
                                             FROM cspmb__Price_Item__c
                                             WHERE cspmb__Is_Active__c = true AND (Access_Type_Text__c LIKE :accessType OR Access_Type_Text__c = '*') AND (Available_bandwidth_QoS_up__c >=:availableBandwidthUp AND Available_bandwidth_QoS_down__c >=:availableBandwidthDown) AND 
                                             (((Number_of_LAN__c >= :numOfLAN) AND (Number_of_WAN__c >= :numOfWAN)) 
                                             OR ((Number_of_LAN__c >= :numOfLAN) AND (WAN_LANWAN__c >= :numOfWAN))  
                                             OR ((LAN_LANWAN__c >= :numOfLAN) AND (Number_of_WAN__c >= :numOfWAN))
                                             OR ((WAN_LANWAN_M_LAN__c	 >= :numOfWAN) AND (LAN_LANWAN_M_WAN__c >= :numOfLAN)))
                                             AND (Wireless_compatible__c = :wirelessBackupBoolean) AND Name LIKE :searchValue AND Product__c != '' AND SFP_Slots__c >= :numOfSfp AND Category_lookup__r.Name = 'CPE'
                                             ORDER BY Name
                                             LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
 
      
    
    }
    if(qualityOfService == 'No'){
        priceItems = [SELECT Id, Name, Vendor__c, Vendor_lookup__r.Name, Region__c,Duration__c,Service__c,SLA_Method__c,Hardware_Toeslag__c,CPE_SLA__c,Category__c, Category_lookup__r.Name, Infra_SLA__c,Overbooking__c,cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Number_of_LAN__c, Number_of_WAN__c, Wireless_compatible__c, Number_of_LANWAN__c, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, cspmb__recurring_charge__c, cspmb__one_off_cost__c, cspmb__recurring_cost__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c,
                        Product__c, WAN_LANWAN_M_LAN__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c,
                        Extra_Product__c, ProductConfigNameText__c, Deal_Type_Text__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c,Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c,Max_Quantity__c,Min_Quantity__c
                                             FROM cspmb__Price_Item__c
                                             WHERE cspmb__Is_Active__c = true AND (Access_Type_Text__c LIKE :accessType OR Access_Type_Text__c = '*') AND (Available_bandwidth_up__c >=:availableBandwidthUp AND Available_bandwidth_down__c >=:availableBandwidthDown) AND
                                             (((Number_of_LAN__c >= :numOfLAN) AND (Number_of_WAN__c >= :numOfWAN)) 
                                             OR ((Number_of_LAN__c >= :numOfLAN) AND (WAN_LANWAN__c >= :numOfWAN))  
                                             OR ((LAN_LANWAN__c >= :numOfLAN) AND (Number_of_WAN__c >= :numOfWAN))
                                             OR ((WAN_LANWAN_M_LAN__c	 >= :numOfWAN) AND (LAN_LANWAN_M_WAN__c >= :numOfLAN)))
                                             AND (Wireless_compatible__c = :wirelessBackupBoolean) AND Name LIKE :searchValue AND Product__c != '' AND SFP_Slots__c >= :numOfSfp AND Category_lookup__r.Name = 'CPE'
                                             ORDER BY Name
                                             LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
 
      
    }
    
           
    System.debug('*****Number of priceItem results: '+ priceItems.size());
    return priceItems;  
    }
    
    }


  public override String getRequiredAttributes(){ 
  
      return '["Available bandwidth down","Quality of Service","Access Type","Available bandwidth up","Num LAN","Num WAN","Num WANLAN","Wireless backup","Num SFP", "Overbooking Type", "Technology type", "Codec", "SIP Channels"]';
      
  }  
}