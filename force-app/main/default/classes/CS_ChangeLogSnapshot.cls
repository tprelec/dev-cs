public with sharing class CS_ChangeLogSnapshot {
    
    public Datetime SnapshotTime { get; private set; }

    public Id BasketId { get; private set; }

    public Id UserId { get; private set; }

    public CS_ChangeLogSnapshot(Id basketId, Id userId) {
        SnapshotTime = Datetime.now();
        BasketId = basketId;
        UserId = userId;
    }

    // Following data needs to go to the snapshot
    // - mobile usage: attribute on Mobile CTN profile PD
    // - fixed usage: attribute on Company Level Fixed Voice PD
    // - account bundle: related product information on Mobile CTN subscription (connected to particular parent Mobile CTN profile configuration)
    // - CTNs: 
    // - discounts: JSON field cscfga__discounts__c associated available on all configurations
    public void createChangeLogSnapshot()
    {

    }
}