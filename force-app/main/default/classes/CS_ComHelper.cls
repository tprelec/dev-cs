public with sharing class CS_ComHelper {

    private static final String INSTALL_SOLUTION_JEOPARDY_RECORD_TYPE = 'CS_COM_Install_Solution_Jeopardy';

    public CS_ComHelper() {
    }

    public static Map<String, String> getServiceSpecificationSimpleAttribute(CS_EDMServiceSpecification serviceSpecification, Set<String> simpleAttributeNames) {
        Map<String, String> retValue = new Map<String, String>();

        for (CS_EDMServiceSpecification.SimpleAttribute sa : serviceSpecification.simpleAttributes) {
            if (simpleAttributeNames.contains(sa.Name)) {
                retValue.put(sa.name, sa.value);
            }
        }
        return retValue;
    }

    public static Map<String, String> getServiceSpecificationAdditionalAttributes(CS_EDMServiceSpecification serviceSpecification, Set<String> additionalAttributeNames) {
        Map<String, String> retValue = new Map<String, String>();

        for (CS_EDMServiceSpecification.SimpleAttribute sa : serviceSpecification.additionalSimpleAttributes) {
            if (additionalAttributeNames.contains(sa.Name)) {
                retValue.put(sa.name, sa.value);
            }
        }
        return retValue;
    }
   
    public static void createMobileFlowProcess(Map<Id, VF_Contract__c> oldMap, Map<Id, VF_Contract__c> newMap) {
        Map<Id, SObject> objectsMap = new Map<Id, SObject>();

        if (oldMap == null) {
            for (VF_Contract__c vfContract : newMap.values()) {
                if (newMap.get(vfContract.Id).Eligible_for_CS_Mobile_Flow__c) {
                    objectsMap.put(vfContract.Id, newMap.get(vfContract.Id));
                }
            }
        } else {
            for (VF_Contract__c vfContract : newMap.values()) {
                if ((oldMap.get(vfContract.Id).Ready_for_Implementation__c == false &&
                        newMap.get(vfContract.Id).Ready_for_Implementation__c == true &&
                        newMap.get(vfContract.Id).Eligible_for_CS_Mobile_Flow__c == true)
                        || (oldMap.get(vfContract.Id).Eligible_for_CS_Mobile_Flow__c == false &&
                        newMap.get(vfContract.Id).Eligible_for_CS_Mobile_Flow__c == true &&
                        newMap.get(vfContract.Id).Ready_for_Implementation__c == true)) {
                    objectsMap.put(vfContract.Id, newMap.get(vfContract.Id));
                }
            }
        }

        if (objectsMap.values().size() > 0) {
            CS_ComProcessCreator cpc = new CS_ComProcessCreator(objectsMap.values());
            cpc.run();
        }
    }

    public static String generateRandomIpAddress(String numOfIpAddresses) {
        String retValue = '';

        if (numOfIpAddresses.equalsIgnoreCase('Connect MKB Internet 1 IP V4')) {
            retValue = '192.168.12.25/32';
        } else if (numOfIpAddresses.equalsIgnoreCase('Connect MKB Internet 5 IP V4')) {
            retValue = '192.168.12.25/32;192.168.23.32/22;192.168.85.25/32;192.168.88.32/22;192.168.3.32/22';
        }

        return retValue;
    }

    public static String generateRandomIpV6Address(String numOfIpAddresses) {
        if (numOfIpAddresses.equalsIgnoreCase('Connect MKB Internet IP V6')) {
            return '2001:0db8:85a3:0000:0000:8a2e:0370:7334';
        } else {
            return '';
        }
    }

    public static void orchestratorSubscribeToEvent(Map<Id, VF_Contract__c> oldContractVFMap, Map<Id, VF_Contract__c> newContractVFMap) {
        Set<Id> objectIds = new Set<Id>();

        for (Id newContract : newContractVFMap.keySet()) {
            if (oldContractVFMap.get(newContract) != null && oldContractVFMap.get(newContract).Implementation_Status__c != 'Renewed'
                    && newContractVFMap.get(newContract).Implementation_Status__c == 'Renewed') {
                objectIds.add(newContract);
            }
        }

        if (objectIds.size() > 0) {
            CSPOFA.Events.emit('update', objectIds);
        }
    }
}