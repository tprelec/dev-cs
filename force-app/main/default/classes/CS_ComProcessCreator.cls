public with sharing class CS_ComProcessCreator {
    static final String PRODUCT_TEMPLATE_NAME = 'Product Template';
    static final String COMPONENT_TEMPLATE_NAME = 'Component Template';
    static final String ORDER_TEMPLATE_NAME = 'Order Provisioning';
    static final String MOBILE_FLOW_TEMPLATE_NAME = 'Mobile Flow';

    private List<SObject> objectList;
    private Map<Id, Id> objectIdRecordTypeIdMap;
    private List<CSPOFA.ProcessRequest> processRequests;

    public CS_ComProcessCreator(List<SObject> objects) {
        this.objectList = objects;
    }

    public void run() {
        this.objectIdRecordTypeIdMap = filterObjects(this.objectList);
        this.processRequests = createProcessRequests(this.objectIdRecordTypeIdMap);
        logResult(createProcess(this.processRequests));
    }

    private List<CSPOFA.ProcessRequest> createProcessRequests(Map<Id, Id> objectIdRecordTypeId) {
        List<CSPOFA.ProcessRequest> processRequests = new List<CSPOFA.ProcessRequest>();

        for (Id recordTypeId : objectIdRecordTypeId.keySet()) {
            String processTemplateName = getProcessTemplateName(recordTypeId, this.objectIdRecordTypeIdMap.get(recordTypeId));
            Map<SObjectField, Id> objectFieldMap = getObjectFieldMap(recordTypeId);

            processRequests.add(
                new CSPOFA.ProcessRequest().templateName(processTemplateName).relationships(objectFieldMap)
            );
        }
        return processRequests;
    }

    private List<CSPOFA.ApiResult> createProcess(List<CSPOFA.ProcessRequest> processRequests) {
        List<CSPOFA.ApiResult> results = CSPOFA.API_V1.processes.create(processRequests);

        return results;
    }

    private Map<SObjectField, Id> getObjectFieldMap(Id objectId) {
        String sobjectType = objectId.getSObjectType().getDescribe().getName();
        Map<SObjectField, Id> retValue;

        if (sobjectType == 'csord__Order__c') {
            retValue = new Map<SObjectField, Id>{
                CSPOFA__Orchestration_Process__c.Order__c => objectId
            };
        } else if (sobjectType == 'VF_Contract__c') {
            retValue = new Map<SObjectField, Id>{
                CSPOFA__Orchestration_Process__c.Contract_VF__c => objectId
            };
        } else {
            retValue = new Map<SObjectField, Id>{};
        }

        return retValue;
    }

    private String getProcessTemplateName(Id objectId, Id recordTypeId) {
        String retValue;
        String sobjectType = objectId.getSObjectType().getDescribe().getName();

        if (sobjectType == 'csord__Order__c') {
            retValue = ORDER_TEMPLATE_NAME;
        } else if (sobjectType == 'VF_Contract__c') {
            retValue = MOBILE_FLOW_TEMPLATE_NAME;
        } else {
            retValue = '';
        }

        return retValue;
    }

    private Map<Id, Id> filterObjects(List<SObject> objects) {
        Map<Id, Id> results = new Map<Id, Id>();
        Set<Id> vfContractsWithExistingProcesses = new Set<Id>();
        Set<Id> vfContractIds = new Set<Id>();
        List<cspofa__Orchestration_Process__c> vfContractOrchestrationProcesses = new List<cspofa__Orchestration_Process__c>();

        for (SObject obj : objects) {
            String sobjectType = obj.Id.getSObjectType().getDescribe().getName();

            if (sobjectType == 'VF_Contract__c') {
                vfContractIds.add(obj.Id);
            }
        }

        if(!vfContractIds.isEmpty()){
            vfContractOrchestrationProcesses = [SELECT Id,
                                                        Contract_VF__c
                                                FROM cspofa__Orchestration_Process__c
                                                WHERE Contract_VF__c IN :vfContractIds
            ];
        }

        for (cspofa__Orchestration_Process__c orchProcess :vfContractOrchestrationProcesses) {
            vfContractsWithExistingProcesses.add(orchProcess.Contract_VF__c);
        }

        for (SObject obj : objects) {
            String sobjectType = obj.Id.getSObjectType().getDescribe().getName();

            if (sobjectType == 'csord__Service__c') {
                csord__Service__c ser = (csord__Service__c) obj;
            } else if (sobjectType == 'csord__Order__c') {
                csord__Order__c ord = (csord__Order__c) obj;
                results.put(ord.Id, ord.RecordTypeId);
            } else if (sobjectType == 'VF_Contract__c') {
                VF_Contract__c vfContract = (VF_Contract__c) obj;
                if(vfContractOrchestrationProcesses.isEmpty() || !vfContractsWithExistingProcesses.contains(vfContract.Id)){
                    results.put(vfContract.Id, vfContract.RecordTypeId);
                }
            }
        }

        return results;
    }

    private void logResult(List<CSPOFA.ApiResult> results) {
        for (CSPOFA.ApiResult result : results) {
            if (result.isSuccess()) {
                String resourceId = result.getId();
            } else {
                List<CSPOFA.ApiError> errors = result.getErrors();
            }
        }
    }
}