public with sharing class CS_ConfigTriggerHandler{

    public static void resetPrices(object[] newTrigger){
    
        Set<Id> baskets = new Set<Id>();
        Set<Id> productConfigurations = new Set<Id>();
        
        Decimal maxSumRecurring = 20.00;
        Decimal sumRecurring = 0.00;
        
        List<cscfga__Product_Configuration__c> newConfigs = (List<cscfga__Product_Configuration__c>) newTrigger;
    
        for(cscfga__Product_Configuration__c conf: newConfigs){    
            baskets.add(conf.cscfga__Product_Basket__c);
            productConfigurations.add(conf.id);
        }
             
        List<cscfga__Attribute__c> records =[SELECT cscfga__Price__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c in (SELECT Id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c in :baskets and cscfga__Parent_Configuration__c in :productConfigurations and Name LIKE 'IP Pin:%') and Name = 'Add On Recurring Charge'];
          
        if(sumRecurring >= maxSumRecurring){
           for(cscfga__Attribute__c attr : records){
            attr.cscfga__Price__c = 0.00;
            } 
            System.debug('UPDATED =='+records);
            update records;
           String basketId = String.valueOf((new List<Id>(baskets)[0]));
            createConfigIPPin(basketId);
            cscfga.ProductConfigurationBulkActions.calculateTotals(baskets);
        }
        
    }

    public static void createConfigIPPin(String basketId){
        cscfga__Product_Definition__c pDef  = [SELECT Id from cscfga__Product_Definition__c where name ='Price Item IP Pin'];
        ID recordId = pDef.Id;
        DescribeSObjectResult describeResult = recordId.getSObjectType().getDescribe();
            List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
            String query =  ' SELECT ' +String.join( fieldNames, ',' ) +' FROM ' +describeResult.getName() +' WHERE ' +' id = :recordId ' +' LIMIT 1 ';
            
            // return generic list of sobjects or typecast to expected type
            List<SObject> records = Database.query( query );
            pDef = (cscfga__Product_Definition__c)records[0];
            
            //create configuration
            cscfga.API_1.ApiSession configApi = cscfga.Api_1.getApiSession(pDef);
            //cscfga.API_1.ApiSession configApi = cscfga.API_1.ApiSession.getApiSession(pDef);
            configApi.persistConfiguration();
            cscfga.ProductConfiguration myConfig= configApi.getConfiguration();
            cscfga__Product_Configuration__c pConf  = [SELECT Id, cscfga__Product_Basket__c from cscfga__Product_Configuration__c where Id = :myConfig.getId()];
            
            //delete session basket
            cscfga__Product_Basket__c pBasket = [SELECT Id from cscfga__Product_Basket__c where Id = :pConf.cscfga__Product_Basket__c];
                delete pBasket;
             
            cscfga__Configuration_Offer__c offer= [SELECT Id from cscfga__Configuration_Offer__c where Name = 'IP Pin Flatt' LIMIT 1];
            
            createPCR(offer.Id, basketId, pConf.Id);   
                
    }
    
    
    public static void createPCR(String offerID, String basketID, String configurationID){
        
        csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c();
        pcr.csbb__Offer__c = offerID;
        pcr.csbb__Product_Basket__c = basketID;
        pcr.csbb__Product_Configuration__c = configurationID;
        insert pcr;
        
    }
    public CS_ConfigTriggerHandler(){}
}