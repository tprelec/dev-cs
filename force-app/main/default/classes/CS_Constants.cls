public with sharing class CS_Constants {
    public static final Map<String, String> DEAL_TYPES = new Map<String, String> {
        'ACQUISITION' => 'Acquisition',
        'RETENTION' => 'Retention',
        'MIGRATION' => 'Migration',
        'PORTING' => 'Porting',
        'DISCONNECT' => 'Disconnect'
    };
    
    // Group__c field on Commercial Products used to differentiate between BMS products.
    public static final String LOOKOUT_GROUP = 'Lookout';
    public static final String MANAGED_SERVICE_1_GROUP = 'Managed Service 1';
    public static final String MANAGED_SERVICE_2_GROUP = 'Managed Service 2';
    public static final String MANAGED_SERVICE_EXPRESS_GROUP = 'Managed Service Express';
    public static final String MANAGED_SERVICE_OPTIE_GROUP = 'Managed Service Optie';
    public static final String MICROSOFT_EMS_INTUNE_GROUP = 'Microsoft EMS-Intune';
    public static final String MOBILE_IRON_GROUP = 'MobileIron';
    public static final String PROFESSIONAL_SERVICES_GROUP = 'Professional Services';
    public static final String SERVICE_MANAGEMENT_GROUP = 'Service Management';
    public static final String TREND_MICRO_GROUP = 'Trend Micro';
    public static final String VERVANGENDE_TOESTEL_SERVICE_GROUP = 'Vervangende Toestel Service';
    public static final String VSDM_GROUP = 'VSDM';
    public static final String WANDERA_GROUP = 'Wandera';

    //new RedPro scenarios
    public static final String REDPRO_SCENARIO_NEW_PORTING = 'New-Porting RedPro';
    public static final String REDPRO_SCENARIO_RETENTION_MIGRATION = 'Retention-Migration RedPro';
    
    // One Net Harmonization
    public static final String ACCESS_INFRASTRUCTURE = 'Access Infrastructure';
    public static final List<String> ONE_NET_SEAT_LICENCES = new List<String> { 'ONE Combi Seat 2', 'One Flex', 'One Mobiel', 'One Vast' };
    public static final List<String> TRIGGER_LICENCE_NAMES = new List<String> { 'One Call Centre licentie', 'IVR / Keuzemenu', 'One Integrate Premium' };
    public static final String ONSITE_PROJECT_MANAGEMENT = 'Onsite of telefonische project management ondersteuning';
    public static final String ONE_NET_PRODUCT_IN_BASKET = '[One Net]';
    
    // OneMobile4
    public static final List<String> OM4_ABBONEMENTEN = new List<String>{
        'BO_ONE_MOBILE_4_0_SIM_ONLY',
        'BO_ONE_MOBILE_4_0_REGULAR',
        'BO_ONE_MOBILE_4_0_DATA_ONLY',
        'AO_ONE_MONTH_CONTRACT_DURATION'
    };
    public static final List<String> OM4_UPGRADES_VOICE_AND_DATA = new List<String>{
        'AO_2GB_EU_AND_WF_EU_VOICE_REG',
        'AO_7GB_EU_AND_WF_EU_VOICE_REG',
        'AO_21GB_EU_AND_WF_EU_VOICE_REG',
        'AO_2GB_EU_AND_WF_EU_VOICE_SO',
        'AO_7GB_EU_AND_WF_EU_VOICE_SO',
        'AO_21GB_EU_AND_WF_EU_VOICE_SO',
        'AO_WF_GB_EU_and_WF_EU_voice_REG',
        'AO_WF_GB_EU_AND_WF_EU_VOICE_SO',
        'RCAOWORLDHTAPER',
        'AO_UNL_EU_AND_WF_EU_VOICE_SO'
    };
    public static final List<String> OM4_UPGRADES_DATA_ONLY = new List<String> {
        'AO_2GB_EU_DATA_ONLY',
        'AO_7GB_EU_DATA_ONLY',
        'AO_21GB_EU_DATA_ONLY'
    };
}