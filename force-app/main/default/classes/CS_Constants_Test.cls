@isTest
private class CS_Constants_Test {
    @isTest
    private static void test() {
        Test.startTest();
        System.assertEquals(true, CS_Constants.LOOKOUT_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.MANAGED_SERVICE_1_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.MANAGED_SERVICE_2_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.MANAGED_SERVICE_EXPRESS_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.MANAGED_SERVICE_OPTIE_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.MICROSOFT_EMS_INTUNE_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.MOBILE_IRON_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.PROFESSIONAL_SERVICES_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.SERVICE_MANAGEMENT_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.TREND_MICRO_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.VERVANGENDE_TOESTEL_SERVICE_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.VSDM_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.WANDERA_GROUP.length() > 0);
        System.assertEquals(true, CS_Constants.DEAL_TYPES.keySet().size() > 0);
        System.assertEquals(true, CS_Constants.ACCESS_INFRASTRUCTURE.length() > 0);
        System.assertEquals(true, CS_Constants.ONE_NET_SEAT_LICENCES.size() > 0);
        System.assertEquals(true, CS_Constants.TRIGGER_LICENCE_NAMES.size() > 0);
        System.assertEquals(true, CS_Constants.ONSITE_PROJECT_MANAGEMENT.length() > 0);
        System.assertEquals(true, CS_Constants.ONE_NET_PRODUCT_IN_BASKET.length() > 0);
        System.assertEquals(true, CS_Constants.REDPRO_SCENARIO_NEW_PORTING.length() > 0);
        System.assertEquals(true, CS_Constants.REDPRO_SCENARIO_RETENTION_MIGRATION.length() > 0);
        System.assertEquals(true, CS_Constants.OM4_ABBONEMENTEN.size() > 0);
        System.assertEquals(true, CS_Constants.OM4_UPGRADES_VOICE_AND_DATA.size() > 0);
        System.assertEquals(true, CS_Constants.OM4_UPGRADES_DATA_ONLY.size() > 0);
        Test.stopTest();
    }
}