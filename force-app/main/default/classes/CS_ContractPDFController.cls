public with sharing class CS_ContractPDFController {
    
    public Map<String,String> dynamicTables { get; set; }
    public Set<String> keys { get; set; }
    public List<String> pageHeaders { get; set;}
    public List<Boolean> hideLogo { get; set;}
    public List<Boolean> pageBreaks { get; set;}
    public csclm__Agreement__c agreement { get; set;}
    public List<String> addendumNumbers { get; set; }
    public Boolean isBPA { get; set; }
    public Boolean isMantal { get; set; }
    public Boolean newFooter { get; set; }

    public CS_ContractPDFController(ApexPages.StandardController stdController) {

        isBPA = false;
        pageHeaders = new List<String>();
        addendumNumbers = new List<String>();
        hideLogo = new List<Boolean>();
        pageBreaks = new List<Boolean>();
        pageBreaks.add(false);
        
        
        agreement = (csclm__Agreement__c)stdController.getRecord();

        agreement = [SELECT Id, Name,
                            csclm__Opportunity__r.Account.Name, Product_Basket__c, Product_Basket__r.Basket_Number__c, Created_Date_text__c, Product_Basket__r.OwnerAccount__c,
                            Product_Basket__r.cscfga__Opportunity__r.Ban__r.Ban_Number__c, csclm__Opportunity__r.Account.VZ_Frame_Work_Agreement_Calculated__c,
                            csclm__Opportunity__r.Account.Frame_Work_Agreement_Calculated__c, Product_Basket__r.Contract_Language__c, Product_Basket__r.TodayFormatted__c, 
                            Product_Basket__r.TodayFormatted_English__c, csclm__Document_Template__r.Name, Product_Basket__r.DirectIndirect__c
                        FROM csclm__Agreement__c
                        WHERE Id = : agreement.Id
                    ];
                    
        cscfga__Product_Basket__c basket = [
            SELECT Id, cscfga__Products_In_Basket__c, MobileScenarios__c, BMS_Products__c 
            FROM cscfga__Product_Basket__c 
            WHERE Id = :agreement.Product_Basket__c
        ];
        List<Contract_Custom_Settings__mdt> contractSettings = ContractRelatedQuestionsController.getContractCustomSettings(basket);
            
        Id basketId = agreement.Product_Basket__c;
        
        if (agreement != null) {
            addHeaders(agreement, contractSettings);
        }
        
        if (agreement.name.contains('-BPA')) {
            isBPA = true;
        }
        
        populateData(basketId);
    }
    
    @TestVisible
    private void addHeaders(csclm__Agreement__c agreement, List<Contract_Custom_Settings__mdt> contractSettings) {
        
        List<csclm__Transactional_Document__c> transitionalDocuments = [SELECT Id FROM csclm__Transactional_Document__c WHERE csclm__Agreement__c =: agreement.Id];
        isMantal = false;
        newFooter = false;
        
        if (transitionalDocuments != null) {
            List<csclm__Transactional_Section__c> transitionalSections = [SELECT Id FROM csclm__Transactional_Section__c WHERE csclm__Transactional_Document__c IN :transitionalDocuments ORDER BY csclm__Sequence__c ASC];
            
            if (transitionalSections != null) {
                List<csclm__Transactional_Clause__c> transitionalClauses = [SELECT Id, csclm__Final_Rich_Text__c FROM csclm__Transactional_Clause__c WHERE csclm__Transactional_Section__c IN :transitionalSections];
        
                String lastHeader = '';
                Boolean shouldHideLogo = false;

                for (csclm__Transactional_Clause__c clause : transitionalClauses) {
                    if (clause != null && clause.csclm__Final_Rich_Text__c != null && clause.csclm__Final_Rich_Text__c.contains('centerMe')) {
                        lastHeader = clause.csclm__Final_Rich_Text__c.stripHtmlTags().replace('\n',' ').trim().normalizeSpace();
                        if (pageBreaks.size() > 1) {
                            pageBreaks.add(true);
                        } 
                    } else {
                        pageBreaks.add(false);
                    }
                    
                    if (agreement.Product_Basket__r.DirectIndirect__c == 'Indirect') {
                        shouldHideLogo = true;
                    } else {
                        for (Contract_Custom_Settings__mdt contractSetting : contractSettings) {
                            if (lastHeader.containsIgnoreCase(contractSetting.Product_Name__c)) {
                                if ((contractSetting.Product_Name__c == 'One Business' && lastHeader.containsIgnoreCase('One Business IoT')) 
                                    || (contractSetting.Product_Name__c == 'ONE Mobile' && lastHeader.containsIgnoreCase('ONE Mobile 4.0'))
                                    || (contractSetting.Product_Name__c == 'One Net' && lastHeader.containsIgnoreCase('One Net Express'))) {
                                    continue;
                                } else {
                                    if (contractSetting.Document_template__c == 'New') {
                                        shouldHideLogo = true;
                                        newFooter = true;
                                    }
                                    if (contractSetting.Product_Code__c == 'Mantelovereenkomst' && contractSetting.Document_template__c == 'Old') {
                                        isMantal = true;
                                    }   
                                }
                            }
                        }
                    }
                    
                    addendumNumbers.add(CalculateAddendumNumber(agreement, lastHeader, contractSettings));
                    pageHeaders.add(lastHeader);
                    hideLogo.add(shouldHideLogo);
                }
            }
        }
        pageBreaks.add(false);
    }

    @TestVisible
    private String CalculateAddendumNumber(csclm__Agreement__c agreement, String header, List<Contract_Custom_Settings__mdt> contractSettings) {
        String productCode = 'DZG';
        Boolean newTemplate = false;
                
        for (Contract_Custom_Settings__mdt contractSetting : contractSettings) {
            if (headerContainsProductName(header, contractSetting)) {
                if ((contractSetting.Product_Name__c == 'One Business' && header.containsIgnoreCase('One Business IoT')) 
                    || (contractSetting.Product_Name__c == 'ONE Mobile' && header.containsIgnoreCase('ONE Mobile 4.0'))
                    || (contractSetting.Product_Name__c == 'One Net' && header.containsIgnoreCase('One Net Express'))) {
                    continue;
                } else {
                    productCode = contractSetting.Product_Code__c;
                    if (contractSetting.Document_template__c == 'New') {
                        newTemplate = true;
                    }
                }
            }
        }
        
        return agreement.Product_Basket__r.Basket_Number__c + (newTemplate ? '/2.1 ' : '/5.1 ') + productCode;
    }

    private Boolean headerContainsProductName(String header, Contract_Custom_Settings__mdt contractSetting) {
        // contractSetting.Product_Name__c can contain multiple values separated by semicolon
        // this is needed when product name is not the same in Dutch and English contract versions

        if (contractSetting.Product_Name__c == null) {
            throw new IllegalArgumentException('contractSetting.Product_Name__c cannot be null');
        }

        List<String> productNames = contractSetting.Product_Name__c.split(';');
        for (String productName : productNames) {
            if (header.containsIgnoreCase(productName)) {
                return true;
            }
        }

        return false;
    }
    
    @future
    public static void populateDataAsync(Id basketId, Id newFutureId) {
        
        CS_Future_Controller.startFutureJob(newFutureId);
        
        List<cscfga__Product_Basket__c> baskets = [
            SELECT Id, Contract_Async_Data__c, Minimum_Value_Mobile__c, Minimum_Volume_Mobile__c, Minimum_Value_Fixed__c,
                Minimum_Volume_Fixed__c, OneNet_Flex_Scenario__c, OneNet_Scenario__c, PortingPercentage__c, Has_Fixed__c, 
                Basket_Number__c, DirectIndirect__c, Contract_Language__c, TodayFormatted__c, TodayFormatted_English__c, MobileScenarios__c,
                Data_Capping__c
            FROM cscfga__Product_Basket__c 
            WHERE Id = :basketId
        ];
            
        cscfga__Product_Basket__c basket = baskets != null ? baskets[0] : null;
        
        try {
            Set<String> keys = new Set<String>();
            Map<String,String> dynamicTables = new Map<String,String>();
            
            List<cscfga__Product_Configuration__c> basketConfigs= CS_SnapshotHelper.getBasketConfigs(basketId);
            List<CS_Basket_Snapshot_Transactional__c> basketSnaps = CS_SnapshotHelper.getBasketSnapshots(basketId);
            Map<Id, Map<String,List<CS_Basket_Snapshot_Transactional__c>>> groupedSnapshots = CS_SnapshotHelper.procesPerParentPerGroup(basketSnaps);
        
            List<Contract_Conditions__c> contractConditions = [SELECT Id, Name, Channel__c, Proposition__c, Type__c, Version_Number__c, URL_Link__c, Start_Date__c, End_Date__c FROM Contract_Conditions__c WHERE Start_Date__c <= TODAY AND End_Date__c >= TODAY];
            Map<String, String> contracConditionFilters = new Map<String,String>();
            contracConditionFilters.put('Type__c','AVW');
            List<Contract_Conditions__c> contractConditionResults = new List<Contract_Conditions__c>();
    
            
            //Site Availability Id, List of Access, One Net, One Fixed, Managed Internet, IPVPN on that site availability
            Map<Id, List<Id>> mainProductsPerSite = CS_SnapshotHelper.getMainProductPerSiteMapping(basketConfigs);
            
            //Site Availabilities grouped per site
            Map<Id, List<Site_Availability__c>> siteAv = CS_SnapshotHelper.getSiteNamePerSiteAvailability(mainProductsPerSite.keySet());
            
            dynamicTables = CS_ContractsTableHelper.getTableMap(basket,basketConfigs,basketSnaps,groupedSnapshots, mainProductsPerSite, siteAv);    
            contractConditionResults = CS_ContractsTableHelper.getContractConditions(contractConditions, contracConditionFilters);
    
            String avw = System.Label.Contract_Condition_Dutch;
            if (contractConditionResults.size() > 0) {
                avw += '<br>' + contractConditionResults[0].URL_Link__c;
            }
    
            if (!dynamicTables.containsKey('avw')) {
                dynamicTables.put('avw', avw);
            }
            
            keys = dynamicTables.keySet();
            
            List<Attachment> attachmentsToDelete = [SELECT Id FROM Attachment WHERE Name = 'contractBuffer.json' AND parentId = :basketId];
            delete attachmentsToDelete;
            
            Attachment att = new Attachment();
            att.Body = Blob.valueOf(JSON.serialize(dynamicTables));
            att.Name = 'contractBuffer' + '.json';
            att.parentId = basketId;
            insert att;
            
            CS_Future_Controller.endFutureJob(newFutureId);
        } catch (Exception e) {
            basket.Contract_Async_Data__c = '';
            CS_Future_Controller.failFutureJob(newFutureId, e.getMessage());
        }
        
        update basket;
    }
    
    private void populateData(Id basketId) {
        
        CS_Future_Controller futureController;
        Map<String, Future_Optimisation__c> optimisationSettingsMap = Future_Optimisation__c.getAll();
        Future_Optimisation__c optimisationSettings = optimisationSettingsMap.get('Standard Optimisation');
        
        List<AggregateResult> countProductConfigurations = [SELECT count(Id) total FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basketId];
        Integer countProductConfigurationsInteger = Integer.valueOf(countProductConfigurations[0].get('total'));
        
        if (optimisationSettings != null && optimisationSettings.Optimisation_Enabled__c == true && optimisationSettings.Configuration_number_limit__c < countProductConfigurationsInteger) {
            List<cscfga__Product_Basket__c> baskets = [
                SELECT Id, Contract_Async_Data__c, Minimum_Value_Mobile__c, Minimum_Volume_Mobile__c, Minimum_Value_Fixed__c, Minimum_Volume_Fixed__c,
                    OneNet_Flex_Scenario__c, OneNet_Scenario__c, PortingPercentage__c, Has_Fixed__c, Basket_Number__c, DirectIndirect__c, Contract_Language__c,
                    TodayFormatted__c, TodayFormatted_English__c, MobileScenarios__c
                FROM cscfga__Product_Basket__c 
                WHERE Id = :basketId
            ];
            cscfga__Product_Basket__c basket = (baskets != null ? baskets[0] : null);
            
            List<Attachment> att = [SELECT id, Body, Name, parentId FROM Attachment WHERE Name = 'contractBuffer.json' AND parentId =: basketId ];
            
            if (att.size() > 0 && att[0] != null) {
                dynamicTables = (Map<String,String>)JSON.deserialize(att[0].Body.toString(), Map<String,String>.class);
                keys = dynamicTables.keySet();
            }
            return;
        }

        List<cscfga__Product_Configuration__c> basketConfigs= CS_SnapshotHelper.getBasketConfigs(basketId);
        List<CS_Basket_Snapshot_Transactional__c> basketSnaps = CS_SnapshotHelper.getBasketSnapshots(basketId);
        Map<Id, Map<String,List<CS_Basket_Snapshot_Transactional__c>>> groupedSnapshots = CS_SnapshotHelper.procesPerParentPerGroup(basketSnaps);

        List<Contract_Conditions__c> contractConditions = [SELECT Id, Name, Channel__c, Proposition__c, Type__c, Version_Number__c, URL_Link__c, Start_Date__c, End_Date__c FROM Contract_Conditions__c WHERE Start_Date__c <= TODAY AND End_Date__c >= TODAY];
        Map<String, String> contracConditionFilters = new Map<String,String>();
        contracConditionFilters.put('Type__c', 'AVW');
        List<Contract_Conditions__c> contractConditionResults = new List<Contract_Conditions__c>();

        
        //Site Availability Id, List of Access, One Net, One Fixed, Managed Internet, IPVPN on that site availability
        Map<Id, List<Id>> mainProductsPerSite = CS_SnapshotHelper.getMainProductPerSiteMapping(basketConfigs);
        
        //Site Availabilities grouped per site
        Map<Id, List<Site_Availability__c>> siteAv = CS_SnapshotHelper.getSiteNamePerSiteAvailability(mainProductsPerSite.keySet());
        
        List<cscfga__Product_Basket__c> baskets = [
            SELECT Id, Minimum_Value_Mobile__c, Minimum_Volume_Mobile__c, Minimum_Value_Fixed__c, Minimum_Volume_Fixed__c,
                OneNet_Flex_Scenario__c, OneNet_Scenario__c, PortingPercentage__c, Has_Fixed__c, Basket_Number__c, 
                DirectIndirect__c, Contract_Language__c, TodayFormatted__c, TodayFormatted_English__c, MobileScenarios__c,
                Data_Capping__c
            FROM cscfga__Product_Basket__c 
            WHERE Id = :basketId
        ];
        cscfga__Product_Basket__c basket = (baskets != null ? baskets[0] : null);        
        dynamicTables = CS_ContractsTableHelper.getTableMap(basket, basketConfigs, basketSnaps, groupedSnapshots, mainProductsPerSite, siteAv);
        contractConditionResults = CS_ContractsTableHelper.getContractConditions(contractConditions, contracConditionFilters);
        String avw = System.Label.Contract_Condition_Dutch_Old;
        if (contractConditionResults.size() > 0) {
            avw += '<br>' + contractConditionResults[0].URL_Link__c;
        }
        if (!dynamicTables.containsKey('avw')) {
            dynamicTables.put('avw', avw);
        }
        keys = dynamicTables.keySet();
    }
}