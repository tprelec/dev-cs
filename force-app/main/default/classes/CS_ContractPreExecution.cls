global with sharing class CS_ContractPreExecution implements csclmcb.IPreExecutionHandler 
{
    public CS_ContractPreExecution() {

    }

    public csclmcb.Result run (Id agreementId) {

        System.debug('START PRE EXECUTION');
        List<csclm__Agreement__c> agreements = [select Id, csclm__Opportunity__c from csclm__Agreement__c where Id = :agreementId];

        List<Opportunity> opportunities = [select Id, Framework_agreement_date_text_calculated__c, Framework_agreement_date__c, 
        New_Framework_Agreement__c from Opportunity where Id = :agreements[0].csclm__Opportunity__c];

        if (opportunities.size() == 0)
        {
            System.debug('Unexpected null on Opportunity for agreement with Id ' + agreementId);
            return csclmcb.Result.failure('The Agreement doesn\'t have an associated Opportunity! Document cannot be generated.');
        }

        if (opportunities[0].New_Framework_Agreement__c == true 
        || opportunities[0].Framework_agreement_date__c == null)
         {
            opportunities[0].Framework_agreement_date_text_calculated__c = CalculateAgreementDate(Date.today());
         }
        else 
         {
             opportunities[0].Framework_agreement_date_text_calculated__c = CalculateAgreementDate(opportunities[0].Framework_agreement_date__c);
         }

         update opportunities[0];
        return csclmcb.Result.success(agreements[0]);
    }

    private String CalculateAgreementDate(Date inputDate)
    {
        String result = '';
        result += inputDate.day();
        result += ' ';
        String monthString;
        switch on inputDate.month() {
            when 1 { monthString = 'januari'; }
            when 2 { monthString = 'februari'; }
            when 3 { monthString = 'maart'; }
            when 4 { monthString = 'april'; }
            when 5 { monthString = 'mei'; }
            when 6 { monthString = 'juni'; }
            when 7 { monthString = 'juli'; }
            when 8 { monthString = 'augustus'; }
            when 9 { monthString = 'september'; }
            when 10 { monthString = 'oktober'; }
            when 11 { monthString = 'november'; }
            when 12 { monthString = 'december'; }
            when else { monthString = 'ERROR'; }
        }
        result += monthString;
        result += ' ';
        result += inputDate.year();
        return result;
    }
}