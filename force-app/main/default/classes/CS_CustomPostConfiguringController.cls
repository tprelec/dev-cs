public with sharing class CS_CustomPostConfiguringController {
    ApexPages.StandardController controller;
    String basketId;
    Boolean isRecursive;
    String configId;
    List<Id> lstCfgId;
    Boolean redirectToBBApp = true;

    public CS_CustomPostConfiguringController(ApexPages.StandardController stdController) {
        this.controller = stdController;
        basketId= this.controller.getId();
        isRecursive = false;
        configId = ApexPages.currentPage().getParameters().get('configId');
        lstCfgId = new List<Id>();
        lstCfgId.add(configId);

        if (ApexPages.currentPage().getParameters().get('cfgfinish') != null) {
            redirectToBBApp = false;
        }
    }

    /**
     * redirects to Basket
     * @return PageReference
     */
    public PageReference redirectToPage() {
        PageReference pr = new PageReference('/apex/csbb__basketbuilderapp?id=' + controller.getId());
        System.debug('redirectToPage');

        if (redirectToBBApp) {
            pr = new PageReference('/apex/csbb__basketbuilderapp?id=' + controller.getId());
        } else {
            pr = new PageReference('/apex/csbb__CSBasketRedirect?id=' + controller.getId());
        }
        System.debug('AN _ before location deal type');
        propagateLocationDealType(basketID, configId);
        System.debug('AN _ before resetPrices');
        resetPrices(basketId);
        

        return pr;
    }
    
    //UPDATE AFTER NEW config is added or deleted
    public static void resetPricesInTriggerConfig(object[] objectTrigger, Boolean isDelete){
        
        Integer basketNumOfSIP = 0;    
    
       PriceReset__c priceResetSetting = PriceReset__c.getOrgDefaults();
        Set<Id> basketsForRecalculate = new Set<Id>();
        //List<cscfga__Product_Basket__c> basketsForUpdate = new List<cscfga__Product_Basket__c>()
        Decimal maxSumRecurring = priceResetSetting.MaxRecurringPrice__c;
        Decimal sumRecurring = 0.00;
        
        String ConfigName = priceResetSetting.ConfigurationName__c;
        List<cscfga__Product_Configuration__c> newDeletedConfigs;
        
        Set<ID> configIds;
        List<cscfga__Product_Basket__c> baskets;
        if(!isDelete){
            newDeletedConfigs = (List<cscfga__Product_Configuration__c>) objectTrigger;
            configIds = new Map<Id, cscfga__Product_Configuration__c>(newDeletedConfigs).keySet();
            baskets = [
                SELECT Id, Error_message__c,Number_of_SIP__c, Fixed_Scenario__c, OneNet_Scenario__c, Has_Configurations__c 
                FROM cscfga__Product_Basket__c 
                WHERE Id IN (SELECT cscfga__Product_Basket__c FROM cscfga__Product_Configuration__c WHERE Id IN :configIds)
            ];
        }
        else{
            List<cscfga__Product_Configuration__c> lstTriggerOld = (List<cscfga__Product_Configuration__c>)objectTrigger;

            // List<cscfga__Product_Configuration__c> lstAllRecords = [SELECT Id FROM cscfga__Product_Configuration__c WHERE ID != NULL]; CSP - T-46278, commented out to prevent Too many query rows error
            // newDeletedConfigs = [SELECT Id, cscfga__Product_Basket__c FROM cscfga__Product_Configuration__c WHERE Id IN :lstTriggerOld AND Id NOT IN : lstAllRecords  ALL ROWS]; CSP - T-46278 - changed the where clause as the lstAllRecords is not longer fetched or needed 
            newDeletedConfigs = [SELECT Id, cscfga__Product_Basket__c FROM cscfga__Product_Configuration__c WHERE Id IN :lstTriggerOld and isDeleted = true ALL ROWS];
            Set<Id> affectedBasket = new Set<Id>();
            for(cscfga__Product_Configuration__c cc : newDeletedConfigs){
                affectedBasket.add(cc.cscfga__Product_Basket__c);
            }
            configIds = new Map<Id, cscfga__Product_Configuration__c>(newDeletedConfigs).keySet();
            baskets = [
                SELECT Id, Error_message__c,Number_of_SIP__c,Fixed_Scenario__c, OneNet_Scenario__c, Has_Configurations__c
                FROM cscfga__Product_Basket__c 
                WHERE Id IN :affectedBasket
            ];
        }
        //Set<ID> configIds = new Map<Id, cscfga__Product_Configuration__c>(newDeletedConfigs).keySet();
        
        //List<cscfga__Product_Basket__c> baskets = [SELECT Id, Error_message__c from cscfga__Product_Basket__c where Id in (SELECT cscfga__Product_Basket__c from cscfga__Product_Configuration__c where Id in :configIds)];
        Set<Id> basketIds = (new Map<Id,SObject>(baskets)).keySet();

        //all product configurations from all baskets
        List<cscfga__Product_Configuration__c> basketConfigs = [SELECT name, Id,cscfga__Parent_Configuration__c,cscfga__Root_Configuration__c,cscfga__Product_Basket__c, Number_of_SIP__c, Fixed_Scenario__c, OneNet_Scenario__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketIds];
        
        List<cscfga__Product_Configuration__c> productConfigurationList = [SELECT Id, Name, cscfga__Product_Basket__c,Number_of_SIP__c,Fixed_Scenario__c,OneNet_Scenario__c, cscfga__Product_Definition__r.RecordTypeId from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketIds];
        //Set<Id> productConfigurations = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketIds]).keySet();
        Set<Id> productConfigurations = new Set<Id>();
        
        for(cscfga__Product_Configuration__c pc : productConfigurationList){
            productConfigurations.add(pc.Id);
        }
        
        
        
        
        //all attributes for sum from all baskets
        String configNameForLike = ConfigName+'%';
        List<cscfga__Attribute__c> attributesForSum =[SELECT cscfga__Price__c,Backup_price__c,id,cscfga__Product_Configuration__c,name FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c in (SELECT Id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c in :basketIds and cscfga__Parent_Configuration__c != null and Name LIKE :configNameForLike) and Name = :priceResetSetting.RecurringAttributeName__c];
        List<cscfga__Attribute__c> attributesForUpdate = new List<cscfga__Attribute__c>();
       
        List<cscfga__Product_Basket__c> basketsForErrorMessageUpdate = new List<cscfga__Product_Basket__c>();
       
       
       List<cscfga__Product_Basket__c> fixedBasketUpdate = new List<cscfga__Product_Basket__c>();
       
       for(cscfga__Product_Basket__c basket: baskets){
           sumRecurring = 0.00;
           
           //NUM of SIP and Fixed scenation
           //AN 27-6-2018
           //basketNumOfSIP = getBasketNumOfSIP(basket.Id, basketConfigs);
           //System.debug('AN -- numOfSIP='+basketNumOfSIP);
           
           String basketScenario = getBasketScenario(basket.Id, basketConfigs);

           Boolean basketHasConfigurations = basketHasConfigurations(basket.Id, basketConfigs);
           if (basket.Has_Configurations__c != basketHasConfigurations) {
               basket.Has_Configurations__c = basketHasConfigurations;
               // added to this list to avoid multiple updates of the same basket(s)
               fixedBasketUpdate.add(basket);
           }
           
           //AN - 27-6-2018
           /*
           if(basket.Number_of_SIP__c != basketNumOfSIP){
               basket.Number_of_SIP__c = basketNumOfSIP;
               fixedBasketUpdate.add(basket);
           }
           */
           if(basket.Fixed_Scenario__c != basketScenario){
               basket.Fixed_Scenario__c = basketScenario;
               Boolean contains = false;
               for(cscfga__Product_Basket__c bas : fixedBasketUpdate){
                   if(bas.Id == basket.Id){
                       bas.Fixed_Scenario__c = basketScenario;
                       contains = true;
                   }
               }
               
               if(!contains){
                   fixedBasketUpdate.add(basket);
               }
           }
           
           
           //attributesForUpdate = new List<cscfga__Attribute__c>();
           Boolean basketContainsIPPinFlat = containsIPPinFlatList(basket.Id, ConfigName, basketConfigs);
       
       
           //if(!basketContainsIPPinFlat){
               sumRecurring = getSumPerBasket(basket.Id, attributesForSum, basketConfigs);
           //}
        
        
        
            //Company level fixed voice!!!!
            
        
            if(((sumRecurring>0.00) && (sumRecurring >= maxSumRecurring)) || basketContainsIPPinFlat){
                 List<cscfga__Attribute__c> updatedRecords = resetAndBackupAttributes(basket.Id, attributesForSum, basketConfigs);
                if(updatedRecords!=null && !updatedRecords.isEmpty()){
                    basketsForRecalculate.add(basket.Id);
                    attributesForUpdate.addall(updatedRecords);
                    
                    if(!basketContainsIPPinFlat){
                        basket.Error_message__c = priceResetSetting.ErrorMessage__c;
                        basketsForErrorMessageUpdate.add(basket);
                        }
                }
                
                
                //reset the message because IP PIn Flat is added
                 if((sumRecurring >= maxSumRecurring) && basketContainsIPPinFlat && (basket.Error_message__c!='')){
                    basket.Error_message__c = '';
                    basketsForErrorMessageUpdate.add(basket);
                }
                
                //new message because IP Pin flatt needs to be removed and price no longer is above the max value
                else if((sumRecurring < maxSumRecurring) && basketContainsIPPinFlat){
                    basket.Error_message__c = priceResetSetting.ErrorMessage2__c;
                    basketsForErrorMessageUpdate.add(basket);
                }
            
            }
        //revert to previous price, remove error message
            else{
                List<cscfga__Attribute__c> updatedRecords = revertToBackupAttributes(basket.Id, attributesForSum, basketConfigs);
                if(updatedRecords!=null && !updatedRecords.isEmpty()){
                    basketsForRecalculate.add(basket.Id);
                    attributesForUpdate.addall(updatedRecords);
                    basket.Error_message__c = '';
                    basketsForErrorMessageUpdate.add(basket);
                }
                if(basket.Error_message__c!=''){
                    basket.Error_message__c = '';
                    basketsForErrorMessageUpdate.add(basket);
                }
                
            }
       }
       
       if((basketsForRecalculate!=null) && (basketsForRecalculate.size()>0) && (attributesForUpdate.size()>0) && (attributesForUpdate!=null)){
           update attributesForUpdate;
           cscfga.ProductConfigurationBulkActions.calculateTotals(basketsForRecalculate);
           update basketsForErrorMessageUpdate;
           
       }
       else{
           if((basketsForErrorMessageUpdate!=null)&&(!basketsForErrorMessageUpdate.isEmpty())){
               update basketsForErrorMessageUpdate;
           }
       }
       
       
       //Company Level Fixed Voice
        List<String> companyLevelFixedVoice = new List<String>();
       
       companyLevelFixedVoice.add('One Fixed');
       companyLevelFixedVoice.add('One Net Enterprise');
       companyLevelFixedVoice.add('Skype for Business Express');
       companyLevelFixedVoice.add('One Net');
       
       Boolean updateBasket = false;
       if(basketIds!=null && basketIds.size()>0){
           String basketIdStr = (String)(new list<Id>(basketIds))[0];
           
           
           List<cscfga__Product_Configuration__c> pcListWithNoOneNetIntegrate = new List<cscfga__Product_Configuration__c>();
           for(cscfga__Product_Configuration__c pc : productConfigurationList){
               if(pc.Name != 'One Net Enterprise Enabler') {
                    pcListWithNoOneNetIntegrate.add(pc);
               }
           }
           
           Boolean containsCompanyFixedVoice = containsConfigPerName(basketIdStr, 'Company Level Fixed Voice', pcListWithNoOneNetIntegrate);
           Boolean containsFixedVoiceProduct = containsFixedVoiceProduct(basketIdStr,companyLevelFixedVoice, pcListWithNoOneNetIntegrate);
           
           if(!containsCompanyFixedVoice && containsFixedVoiceProduct){
                baskets[0].Error_message__c = 'Company Level Fixed Voice needs to be added if basket contains Fixed voice product';
                update baskets[0];
           } 
    
       }
       
       //NumberOfSIP per basket
       if(fixedBasketUpdate.size()>0){
           update fixedBasketUpdate;
       }
        
    }
    
   
    
    //update after EDIT CONFIGURATION!!
     public static void resetPrices(String basketId){
         
         System.debug('**** AN DEBUG LOGGGGG');
        PriceReset__c priceResetSetting = PriceReset__c.getOrgDefaults();
        
        Decimal maxSumRecurring = priceResetSetting.MaxRecurringPrice__c;
        Decimal sumRecurring = 0.00;
        
        String ConfigName = priceResetSetting.ConfigurationName__c + '%';
        
        List<cscfga__Product_Configuration__c> productConfigurations = [SELECT Id, Name, cscfga__Product_Basket__c, Number_of_SIP__c,Fixed_Scenario__c,OneNet_Scenario__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId];
        
        List<cscfga__Attribute__c> records =[SELECT cscfga__Price__c,Backup_price__c,id FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c in (SELECT Id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Parent_Configuration__c in :productConfigurations and Name LIKE :ConfigName) and Name = :priceResetSetting.RecurringAttributeName__c];
        
        List<cscfga__Attribute__c> attributesForUpdate = new List<cscfga__Attribute__c>();
       
       Boolean basketContainsIPPinFlat = containsIPPinFlat(basketId, ConfigName);
       
       cscfga__Product_Basket__c basket= [SELECT Id, Error_message__c,Number_of_SIP__c,Fixed_Scenario__c,OneNet_Scenario__c from cscfga__Product_Basket__c where Id = :basketId LIMIT 1];
       //if(!basketContainsIPPinFlat){
       
       Boolean updateBasket = false;
       
       if(records!=null && records.size()>0)
       {
       
       for(cscfga__Attribute__c attr : records){
            if(attr.cscfga__Price__c!=null){
            sumRecurring+=attr.cscfga__Price__c;
            }
            else if(attr.Backup_price__c != null){
                sumRecurring+=attr.Backup_price__c;
            }
        } 
     
      
       //Boolean updateBasket = false;
       system.debug('***records: ' + JSON.serializePretty(records));
       system.debug('***sumRecurring: ' + sumRecurring + ':' + maxSumRecurring + ':' + basketContainsIPPinFlat);
        if(((sumRecurring>0.00) && (sumRecurring >= maxSumRecurring)) || basketContainsIPPinFlat){
           for(cscfga__Attribute__c attr : records){
                if(attr.cscfga__Price__c != null && attr.cscfga__Price__c != 0.00){
                    attr.Backup_price__c = attr.cscfga__Price__c;
                    attr.cscfga__Price__c = 0.00;
                    attributesForUpdate.add(attr);
                }
            
            } 
            
            if(attributesForUpdate != null && !attributesForUpdate.isEmpty()){
                update attributesForUpdate;
                Set<Id> baskets = new Set<Id>();
                baskets.add(basketId);
                cscfga.ProductConfigurationBulkActions.calculateTotals(baskets);
                if(!basketContainsIPPinFlat){
                    basket.Error_message__c = priceResetSetting.ErrorMessage__c;
                    updateBasket = true;
                   
                }
            }
            
             //reset the message because IP PIn Flat is added
                 if((sumRecurring >= maxSumRecurring) && basketContainsIPPinFlat && (basket.Error_message__c!='')){
                    basket.Error_message__c = '';
                   updateBasket = true;
                }
                
                //new message because IP Pin flatt needs to be removed and price no longer is above the max value
                else if((sumRecurring < maxSumRecurring) && basketContainsIPPinFlat){
                    basket.Error_message__c = priceResetSetting.ErrorMessage2__c;
                    updateBasket = true;
                }
            
            
        }
        //revert to previous price, remove error message
        else{
            for(cscfga__Attribute__c attr : records){
                if((attr.Backup_price__c != null) && (attr.cscfga__Price__c != attr.Backup_price__c)){
                    attr.cscfga__Price__c = attr.Backup_price__c;
                    attr.Backup_price__c = null;
                    attributesForUpdate.add(attr);
                }
            } 
            
            if(attributesForUpdate != null && !attributesForUpdate.isEmpty()){
                update attributesForUpdate;
                
                //cscfga__Product_Basket__c basket= [SELECT Id, Error_message__c from cscfga__Product_Basket__c where Id = :basketId LIMIT 1];
                basket.Error_message__c = '';
                //update basket;
                updateBasket = true;
                
                Set<Id> baskets = new Set<Id>();
                baskets.add(basketId);
                cscfga.ProductConfigurationBulkActions.calculateTotals(baskets);
                
            }
        }
     }
     
     //check for Number of SIP and Fixed scenario
     
     //Integer basketNumberOfSIP = getBasketNumOfSIP(basket.Id, productConfigurations);
     
     String basketScenario = getBasketScenario(basket.Id, productConfigurations);
     
     //AN-27-6-2018
     /*
     if(basket.Number_of_SIP__c != basketNumberOfSIP){
         updateBasket = true;
         basket.Number_of_SIP__c = basketNumberOfSIP;
     }
     */
     
     if(basket.Fixed_Scenario__c != basketScenario){
         updateBasket = true;
         basket.Fixed_Scenario__c = basketScenario;
     }
        
        
    if(updateBasket){
        update basket;
    }
        
    }
    
    
    public static Boolean containsConfigPerName(Id basketID, String configName, List<cscfga__Product_Configuration__c> configurations){
        List<cscfga__Product_Configuration__c>  sampleConfigs = new List<cscfga__Product_Configuration__c>();
        
        Integer len = ConfigName.length();
        for(cscfga__Product_Configuration__c pc : configurations){
            if((pc.cscfga__Product_Basket__c == basketID) && (pc.name.indexOf(configName)!=-1)){
                sampleConfigs.add(pc);
            }
        }
        
        if(sampleConfigs!= null && !sampleConfigs.isEmpty()){
            return true;
        }
        else{
            return false;
        }
        
    }
    
    public static Boolean containsFixedVoiceProduct(Id basketID, List<String> voiceConfigsNames, List<cscfga__Product_Configuration__c> configurations){
        List<cscfga__Product_Configuration__c>  voiceConfigs = new List<cscfga__Product_Configuration__c>();

        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
        for(cscfga__Product_Configuration__c pc : configurations){
            for(String voiceConfig : voiceConfigsNames){
                if((pc.cscfga__Product_Basket__c == basketID) && (pc.cscfga__Product_Definition__r.RecordTypeId == productDefinitionRecordType) &&(pc.name.indexOf(voiceConfig)!=-1)){
                    voiceConfigs.add(pc);
                }
            }
        }
        
        if(voiceConfigs!= null && !voiceConfigs.isEmpty()){
            return true;
        }
        else{
            return false;
        }
        
    }

     public static Boolean containsIPPinFlatList(Id basketID, String ConfigName, List<cscfga__Product_Configuration__c> configurations){
        List<cscfga__Product_Configuration__c> ipPinProductConfigurations = new List<cscfga__Product_Configuration__c>();
        for(cscfga__Product_Configuration__c pc : configurations){
            System.debug('pc.cscfga__Product_Basket__c='+pc.cscfga__Product_Basket__c);
            System.debug('pc.cscfga__Parent_Configuration__c='+pc.cscfga__Parent_Configuration__c);
            System.debug('pc.cscfga__Root_Configuration__c='+pc.cscfga__Root_Configuration__c);
            System.debug('ConfigName = '+ConfigName);
            if((pc.cscfga__Product_Basket__c == basketID) && (pc.cscfga__Parent_Configuration__c==null) && (pc.cscfga__Root_Configuration__c ==null) && (ConfigName != null && pc.name.indexOf(ConfigName)!=-1)){
                ipPinProductConfigurations.add(pc);
            }
        }
        
        if(ipPinProductConfigurations!= null && !ipPinProductConfigurations.isEmpty()){
            return true;
        }
        else{
            return false;
        }
        
    }
    
    public static Decimal getSumPerBasket(Id basketId, List<cscfga__Attribute__c> listAttributes, List<cscfga__Product_Configuration__c> configurationsList){
        Decimal sum = 0.00;
        for(cscfga__Product_Configuration__c pc : configurationsList){
            if(pc.cscfga__Product_Basket__c == basketId){
                for (cscfga__Attribute__c attr : listAttributes){
                    if(attr.cscfga__Product_Configuration__c == pc.Id){
                        if(attr.cscfga__Price__c!=0.00){
                            sum+=attr.cscfga__Price__c;
                        }
                        else if(attr.Backup_price__c!=null){
                            sum+=attr.Backup_price__c;
                        }
                    }
                }
            }
        }
        
        return sum;
        
    }
    /* AN-27-6-2018
     public static Integer getBasketNumOfSIP(Id basketId, List<cscfga__Product_Configuration__c> configurationsList){
        Integer numOfSIP = 0;
        
        System.debug('AN --- Basket num of sip = '+numOfSIP);
        for(cscfga__Product_Configuration__c pc : configurationsList){
            if(pc.cscfga__Product_Basket__c == basketId){
                System.debug('AN --- Basket num of sip = '+numOfSIP +'   pc='+pc.name);
                if(pc.Number_of_SIP__c != null && pc.Number_of_SIP__c>0){
                    numOfSIP = numOfSIP + (Integer)pc.Number_of_SIP__c;
                    System.debug('AN --- Basket num of sip UPDATED = '+numOfSIP +'   pc='+pc.name);
                }
            }
        }
        return numOfSIP;
    }
    */
    
    public static String getBasketScenario(Id basketId, List<cscfga__Product_Configuration__c> configurationsList){
        Set<String> scenariosInBasket = new Set<String>();
        List<cscfga__Attribute__c> attrList = [SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c FROM cscfga__Attribute__c where cscfga__Product_Configuration__c in :configurationsList AND (name = 'One Net' OR name = 'One Fixed')];
        for(cscfga__Product_Configuration__c pc : configurationsList){
            if(pc.cscfga__Product_Basket__c == basketId){
                cscfga__Attribute__c oneNetAttr = getAttributePerName(pc.Id, 'One Net', attrList);
                cscfga__Attribute__c oneFixedAttr = getAttributePerName(pc.Id, 'One Fixed', attrList);
                if(oneFixedAttr != null && oneFixedAttr.cscfga__Value__c != null){
                    if(pc.Fixed_Scenario__c != null && pc.Fixed_Scenario__c != ''){
                        scenariosInBasket.add(pc.Fixed_Scenario__c);
                    }
                } 
                
                if(oneNetAttr != null && oneNetAttr.cscfga__Value__c != null){
                    if(pc.OneNet_Scenario__c != null && pc.OneNet_Scenario__c != ''){
                        scenariosInBasket.add(pc.OneNet_Scenario__c);
                    }
                }        
            }
        }
        
        if(scenariosInBasket.size()>1){
            return 'ERROR';
        }
        else if(scenariosInBasket.size() == 0){
            return 'NONE';
        }
        else if(scenariosInBasket.size() == 1){
            String value = (new List<String>(scenariosInBasket) )[0];
            return value;
        }
        
        return 'ERROR';
        
    }

    public static Boolean basketHasConfigurations(Id basketId, List<cscfga__Product_Configuration__c> configurationsList) {
        for (cscfga__Product_Configuration__c pc : configurationsList) {
            if (pc.cscfga__Product_Basket__c == basketId) {
                return true;
            }
        }
        return false;
    }
    
    public static List<cscfga__Attribute__c> resetAndBackupAttributes(Id basketId, List<cscfga__Attribute__c> listAttributes, List<cscfga__Product_Configuration__c> configurationsList){
        List<cscfga__Attribute__c> attributesForUpdate = new List<cscfga__Attribute__c>();
        for(cscfga__Product_Configuration__c pc : configurationsList){
            if(pc.cscfga__Product_Basket__c == basketId){
                for (cscfga__Attribute__c attr : listAttributes){
                    if(attr.cscfga__Product_Configuration__c == pc.Id){
                        if((attr.cscfga__Price__c != 0.00) && (attr.Backup_price__c != 0.00) && (attr.cscfga__Price__c != attr.Backup_price__c)){
                            attr.Backup_price__c = attr.cscfga__Price__c;
                            attr.cscfga__Price__c = 0.00;
                            attributesForUpdate.add(attr);
                        }
                    }
                }
            }
        }
        return attributesForUpdate;
        
    }
    
    public static List<cscfga__Attribute__c> revertToBackupAttributes(Id basketId, List<cscfga__Attribute__c> listAttributes, List<cscfga__Product_Configuration__c> configurationsList){
        List<cscfga__Attribute__c> attributesForUpdate = new List<cscfga__Attribute__c>();
        for(cscfga__Product_Configuration__c pc : configurationsList){
            if(pc.cscfga__Product_Basket__c == basketId){
                for (cscfga__Attribute__c attr : listAttributes){
                    if(attr.cscfga__Product_Configuration__c == pc.Id){
                        if((attr.Backup_price__c != null) && (attr.Backup_price__c != 0.00) && (attr.cscfga__Price__c != attr.Backup_price__c)){
                        attr.cscfga__Price__c = attr.Backup_price__c;
                        attr.Backup_price__c = null;
                        attributesForUpdate.add(attr);
                        }
                    }
                }
            }
        }
        
        return attributesForUpdate;
    }
    
    private static cscfga__Attribute__c getAttributePerName(Id configId, String attributeName,List<cscfga__Attribute__c> attributesFromRefConfig) {

        for(cscfga__Attribute__c attr :attributesFromRefConfig) {
            if((attr.Name == attributeName) && (attr.cscfga__Product_Configuration__c == configId)) {
                return attr;
            }
        }
        
        return null;                                                                
    }
    
    
    public static Boolean containsIPPinFlat(Id basketID, String ConfigName){
        List<cscfga__Product_Configuration__c> ipPinProductConfigurations = [SELECT name, Id,cscfga__Parent_Configuration__c,cscfga__Root_Configuration__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and name LIKE :ConfigName and cscfga__Parent_Configuration__c = '' and cscfga__Root_Configuration__c = ''];
        
        if(ipPinProductConfigurations!= null && !ipPinProductConfigurations.isEmpty()){
            return true;
        }
        else{
            return false;
        }
        
    }
    /*public static void createConfigIPPin(String basketId){
        cscfga__Product_Definition__c pDef  = [SELECT Id from cscfga__Product_Definition__c where name ='Price Item IP Pin'];
        ID recordId = pDef.Id;
        DescribeSObjectResult describeResult = recordId.getSObjectType().getDescribe();
            List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
            String query =  ' SELECT ' +String.join( fieldNames, ',' ) +' FROM ' +describeResult.getName() +' WHERE ' +' id = :recordId ' +' LIMIT 1 ';
            
            // return generic list of sobjects or typecast to expected type
            List<SObject> records = Database.query( query );
            pDef = (cscfga__Product_Definition__c)records[0];
            
            //create configuration
            cscfga.API_1.ApiSession configApi = cscfga.Api_1.getApiSession(pDef);
            configApi.persistConfiguration();
            cscfga.ProductConfiguration myConfig= configApi.getConfiguration();
            cscfga__Product_Configuration__c pConf  = [SELECT Id, cscfga__Product_Basket__c from cscfga__Product_Configuration__c where Id = :myConfig.getId()];
            
            //delete session basket
            cscfga__Product_Basket__c pBasket = [SELECT Id from cscfga__Product_Basket__c where Id = :pConf.cscfga__Product_Basket__c];
            delete pBasket;
             
            cscfga__Configuration_Offer__c offer= [SELECT Id from cscfga__Configuration_Offer__c where Name = 'IP Pin Flatt' LIMIT 1];
            
            createPCR(offer.Id, basketId, pConf.Id);   
                
    }*/
    
    
    /*public static void createPCR(String offerID, String basketID, String configurationID){
        
        csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c();
        pcr.csbb__Offer__c = offerID;
        pcr.csbb__Product_Basket__c = basketID;
        pcr.csbb__Product_Configuration__c = configurationID;
        insert pcr;
        
    }*/
    
    //Deal Type -> until packages
    //remove before deployment
    public static void propagateLocationDealType(String basketID, String configId){
        system.debug('**** basketId: ' + basketID + ': configId' + configId);
        //On finish of Access Infrastructure config (non redundant,standalone) set attribute value on IPVPN in the same package
        //For now modeled with lookup
        //for packages later
        // 'Package Slot' field on product configuration 
        // needs to point to the same package
        //
        
        DealTypeSettings__c dealTypeSetting = DealTypeSettings__c.getOrgDefaults();
        //Deal Type dealTypeSetting.Deal_Type_Attribute_Name__c;
        //Location Deal Type on IPVPN dealTypeSetting.Location_Configuration_Name__c;
        //dealTypeSetting.Deal_Type_Propagate_Attribute_Name__c;
        
        //IPVPN, Internet,onenet + deal type on this config
        Set<String> productConfigNames = new Set<String>();
        productConfigNames.add('IPVPN');
        productConfigNames.add(dealTypeSetting.Deal_Type_Attribute_Name__c);
        productConfigNames.add('ConfigName');
        productConfigNames.add('Secondary');
        
        
        //Get Deal Type value
        //Get child configs for this location
        //get value of deal type
        //get config name to determine weather to move forward
        Map<Id, cscfga__Attribute__c> attributesWithConfigValues = new Map<Id, cscfga__Attribute__c>([Select Id, cscfga__Product_Configuration__c, cscfga__Attribute_Definition__r.Name, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c  = :configId and cscfga__Attribute_Definition__r.Name in :productConfigNames]);
        
        System.debug('Attributes with values =='+attributesWithConfigValues);
        Map<String, cscfga__Attribute__c> arrangedAttributes = new Map<String, cscfga__Attribute__c>();
        //decision to move forward
        Boolean isLocationConfig = false;
        for(Id key : attributesWithConfigValues.keySet()){
            arrangedAttributes.put(attributesWithConfigValues.get(key).cscfga__Attribute_Definition__r.Name, attributesWithConfigValues.get(key));
            
            System.debug('Adding arranged attribute =='+attributesWithConfigValues.get(key).cscfga__Attribute_Definition__r.Name+' value='+attributesWithConfigValues.get(key));
        }
        
        System.debug('arrangedAttributes with values =='+arrangedAttributes);
        
        if(arrangedAttributes.containsKey('ConfigName')){
            if(arrangedAttributes.get('ConfigName').cscfga__Value__c == dealTypeSetting.Location_Configuration_Name__c){
                if(arrangedAttributes.containsKey('Secondary')){
                    if(arrangedAttributes.get('Secondary').cscfga__Value__c == 'No'){
                        isLocationConfig = true;
                    }
                    
                }
            }
        }
        
        //this is location config
        if(isLocationConfig){
            
            System.debug('isLocationConfig =='+isLocationConfig);
            String dealTypeValueToPropagate = '';
            Set<Id> childConfigIds = new Set<Id>();
            
            System.debug('arrangedAttributes i nlocal=='+arrangedAttributes);
            
            if(arrangedAttributes.containsKey(dealTypeSetting.Deal_Type_Attribute_Name__c)){
                dealTypeValueToPropagate = arrangedAttributes.get(dealTypeSetting.Deal_Type_Attribute_Name__c).cscfga__Value__c;
                
                System.debug('dealTypeValueToPropagate =='+dealTypeValueToPropagate);
                
                if(arrangedAttributes.containsKey('IPVPN')){
                    childConfigIds.add(arrangedAttributes.get('IPVPN').cscfga__Value__c);
                }
                
                System.debug('childConfigIds =='+childConfigIds);
                
                List<cscfga__Attribute__c> attributesEligibleForUpdate = [Select Id,cscfga__Product_Configuration__c, cscfga__Attribute_Definition__r.Name, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c in :childConfigIds and cscfga__Attribute_Definition__r.Name = :dealTypeSetting.Deal_Type_Propagate_Attribute_Name__c];
                
                System.debug('isLocationConfig =='+attributesEligibleForUpdate);
                
                List<cscfga__Attribute__c> attributesRequiringUpdate = new List<cscfga__Attribute__c>();
                for(cscfga__Attribute__c attr: attributesEligibleForUpdate){
                    if(attr.cscfga__Value__c != dealTypeValueToPropagate){
                        attr.cscfga__Value__c = dealTypeValueToPropagate;
                        attributesRequiringUpdate.add(attr);
                    }
                }
                
                if(attributesRequiringUpdate.size() > 0){
                    update attributesRequiringUpdate;

                }
            }
                
        }
        
    }
    
}