/**
 * @description Orchestration process step that needs to have the global modifier in 
 * order to work properly because it is called from a different namespace.
 */
global without sharing class CS_CustomStepSendEmail implements CSPOFA.ExecutionHandler {
    private static OrgWideEmailAddress[] orgWideEmailAddress;

    global List<SObject> process(List<SObject> param0) {
        try {
            orgWideEmailAddress = [
                SELECT Id,
                    DisplayName
                FROM OrgWideEmailAddress
                WHERE Address = 'ebusalesforce.nl@vodafone.com'
            ];

            List<CSPOFA__Orchestration_Step__c> stepList = [
                SELECT ID,
                    Name,
                    CSPOFA__Orchestration_Process__c,
                    CSPOFA__Status__c,
                    CSPOFA__Completed_Date__c,
                    CSPOFA__Message__c,
                    CSPOFA__Expression_Result__c,
                    CS_Email_From__c,
                    CS_Email_To__c,
                    CS_Email_Template__c
                FROM CSPOFA__Orchestration_Step__c
                WHERE Id IN :param0
            ];

            return processSteps(stepList);
        }
        catch (Exception ex) {
            List<CSPOFA__Orchestration_Step__c> steps = (List<CSPOFA__Orchestration_Step__c>) param0;
            for (CSPOFA__Orchestration_Step__c step : steps) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
                step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
            }
            return steps;
        }
    }

    private static List<SObject> processSteps(List<CSPOFA__Orchestration_Step__c> steps) {
        List<SObject> result = new List<SObject>();

        List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
        List<CSPOFA__Orchestration_Step__c> stepsWithEmails = new List<CSPOFA__Orchestration_Step__c>();
        Map<Id, EmailTemplate> emailTemplateMap;
        Map<String, Id> emailTemplateNameMap = new Map<String, Id>();

        List<Id> processIds = new List<Id>();
        Set<Id> vfContractIds = new Set<Id>();
        Set<Id> solutionIds = new Set<Id>();
        List<String> emailTemplateNames = new List<String>();

        for (CSPOFA__Orchestration_Step__c step : steps) {
            processIds.add(step.CSPOFA__Orchestration_Process__c);
            emailTemplateNames.add(step.CS_Email_Template__c);
        }

        if (emailTemplateNames.size() > 0) {
            emailTemplateMap = new Map<Id, EmailTemplate>([
                SELECT ID,
                    Subject,
                    Name,
                    HtmlValue,
                    Body
                FROM EmailTemplate
                WHERE Name IN :emailTemplateNames
            ]);
        }

        for (EmailTemplate et : emailTemplateMap.values()) {
            emailTemplateNameMap.put(et.Name, et.Id);
        }

        Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c> ([
            SELECT Id,
                Contract_VF__c,
                CSPOFA__Process_Type__c,
                Solution__c
            FROM CSPOFA__Orchestration_Process__c
            WHERE Id in :processIds
        ]);

        for (CSPOFA__Orchestration_Process__c p : processesMap.values()) {
            vfContractIds.add(p.Contract_VF__c);
            solutionIds.add(p.Solution__c);
        }

        Map<Id, VF_Contract__c> vfContractMap;
        Map<Id, csord__Solution__c> solutionMap;

        if (!vfContractIds.isEmpty()) {
            vfContractMap = new Map<Id, VF_Contract__c>([
                SELECT Id,
                    Name,
                    Implementation_Manager__c,
                    Implementation_Manager__r.Email
                FROM VF_Contract__c
                WHERE Id IN :vfContractIds
            ]);
        }

        if (!solutionIds.isEmpty()) {
            solutionMap = new Map<Id, csord__Solution__c>([
                SELECT Id,
                    Name,
                    Technical_Contact__c,
                    Technical_Contact__r.Email,
                    Technical_Contact__r.Name,
                    Site__r.Name,
                    Installation_Start__c,
                    Installation_End__c,
                    Products__c
                FROM csord__Solution__c
                WHERE Id IN :solutionIds
            ]);
        }

        for (CSPOFA__Orchestration_Step__c step : steps) {
            try {
                Messaging.SingleEmailMessage tmpEmailMessage = null;

                if (processesMap.get(step.CSPOFA__Orchestration_Process__c).Contract_VF__c != null) {
                    Id vfContractId = processesMap.get(step.CSPOFA__Orchestration_Process__c).Contract_VF__c;
                    VF_Contract__c vfContract = vfContractMap.get(vfContractId);
                    tmpEmailMessage = createSingleEmailMessage(step, vfContract, emailTemplateMap.get(emailTemplateNameMap.get(step.CS_Email_Template__c)));
                } else if (processesMap.get(step.CSPOFA__Orchestration_Process__c).Solution__c != null) {
                    Id solutionId = processesMap.get(step.CSPOFA__Orchestration_Process__c).Solution__c;
                    csord__Solution__c solution = solutionMap.get(solutionId);
                    tmpEmailMessage = createSingleEmailMessageForSolution(step, solution, emailTemplateMap.get(emailTemplateNameMap.get(step.CS_Email_Template__c)));
                }

                emailMessages.add(tmpEmailMessage);
                stepsWithEmails.add(step);
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_COMPLETE;
                step.CSPOFA__Completed_Date__c = Date.today();
                step.CSPOFA__Message__c = 'No message retrieved';
            } catch (Exception ex) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
                step.CSPOFA__Completed_Date__c = null;
                step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
            }
        }

        List<Messaging.SendEmailResult> emailResults;
        if(Test.isRunningTest()) {
            emailResults = new List<Messaging.SendEmailResult>();
        } else {
            emailResults = Messaging.sendEmail(emailMessages, false);
        }

        for (Integer i = 0; i < stepsWithEmails.size() && i < emailResults.size(); i++) {
            if (!emailResults[i].isSuccess()) {
                stepsWithEmails[i].CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
                stepsWithEmails[i].CSPOFA__Completed_Date__c = null;
                stepsWithEmails[i].CSPOFA__Message__c = 'Exception:' + JSON.serialize(emailResults[i].getErrors());
            }
        }
        return stepsWithEmails;
    }

    public static Messaging.SingleEmailMessage createSingleEmailMessageForSolution(CSPOFA__Orchestration_Step__c step, csord__Solution__c solution, EmailTemplate emailTemplate) {
        Messaging.SingleEmailMessage emailMessage = Messaging.renderStoredEmailTemplate(emailTemplate.Id, solution.Technical_Contact__c, solution.Id);
        emailMessage.setTargetObjectId(solution.Technical_Contact__c);
        emailMessage.setSaveAsActivity(false);
        emailMessage.setTemplateId(emailTemplate.Id);
        emailMessage.optOutPolicy = 'FILTER';
        if (orgWideEmailAddress.size() > 0) {
            emailMessage.setOrgWideEmailAddressId(orgWideEmailAddress.get(0).Id);
        }

        if (Test.isRunningTest()) {
            emailMessage.setToAddresses(new List<String>{
                'test@mailinator.com'
            });
        } else {
            emailMessage.setToAddresses(new List<String>{
                solution.Technical_Contact__r.Email
            });
        }

        String mailBody = emailTemplate.HtmlValue;
        mailBody = mailBody.replace('<![CDATA[', '').replace(']]>', '');
        mailBody = mailBody.replace('{Solution.Technical_Contact_Name}', solution.Technical_Contact__r.Name);
        mailBody = mailBody.replace('{Solution.Installation_Start}', solution.Installation_Start__c.format());
        mailBody = mailBody.replace('{Solution.Installation_End}', solution.Installation_End__c.format());
        mailBody = mailBody.replace('{Solution.Products__c}', solution.Products__c);
        mailBody = mailBody.replace('{Solution.Name}', solution.Name);
        mailBody = mailBody.replace('{Solution.Installation_Address}', solution.Site__r.Name);
        mailBody = mailBody.replace('{Solution.Technical_Details}', '');

        emailMessage.setHtmlBody(mailBody);
        return emailMessage;
    }

    public static Messaging.SingleEmailMessage createSingleEmailMessage(CSPOFA__Orchestration_Step__c step, VF_Contract__c vfContract, EmailTemplate emailTemplate) {
        Messaging.SingleEmailMessage emailMessage = Messaging.renderStoredEmailTemplate(emailTemplate.Id, vfContract.Implementation_Manager__c, vfContract.Id);
        emailMessage.setTargetObjectId(vfContract.Implementation_Manager__c);
        emailMessage.setSaveAsActivity(false);
        emailMessage.setTemplateId(emailTemplate.Id);
        emailMessage.optOutPolicy = 'FILTER';
        if (orgWideEmailAddress.size() > 0) {
            emailMessage.setOrgWideEmailAddressId(orgWideEmailAddress.get(0).Id);
        }

        if (Test.isRunningTest()) {
            emailMessage.setToAddresses(new List<String>{
                'test@mailinator.com'
            });
        } else {
            emailMessage.setToAddresses(new List<String>{
                vfContract.Implementation_Manager__r.Email
            });
        }
        return emailMessage;
    }
}