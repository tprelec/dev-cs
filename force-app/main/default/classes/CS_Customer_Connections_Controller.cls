global class CS_Customer_Connections_Controller {
    public Map<String, List<CS_Customer_Connection__c>> mapOfAllCustomerConnectionsPerProposition  = new Map<String, List<CS_Customer_Connection__c>>();
    
    public CS_Customer_Connections_Controller(Set<String> propositionList, Set<String> dealItemSet) {
        refreshCustomerConnectionsBuffer(propositionList, dealItemSet);
    }
    
    public void refreshCustomerConnectionsBuffer(Set<String> propositionList, Set<String> dealItemSet) {
        this.mapOfAllCustomerConnectionsPerProposition = getAllCustomerConnections(propositionList, dealItemSet);
    }
    
    private Map<String, List<CS_Customer_Connection__c>> getAllCustomerConnections(Set<String> propositionList, Set<String> dealItemSet) {
        List<CS_Customer_Connection__c> customerConnectionsList = [SELECT Allocation__c, Charge_Type__c, Cost_Type__c, Deal_Item__c, Deal_Item__r.Name, Maximum_Quantity__c, 
                       Minimum_Quantity__c, Proposition__c, Value__c, Year__c
        FROM CS_Customer_Connection__c WHERE Proposition__c IN :propositionList AND Deal_Item__r.Name IN :dealItemSet];
        
        Map<String, List<CS_Customer_Connection__c>> customerConnectionsPerPropositionMap = new Map<String, List<CS_Customer_Connection__c>>();
        
        for(CS_Customer_Connection__c customerConnection : customerConnectionsList) {
            if(customerConnectionsPerPropositionMap.get(customerConnection.Proposition__c) == null) {
                List<CS_Customer_Connection__c> temporaryCustomerConnectionList = new List<CS_Customer_Connection__c>();
                temporaryCustomerConnectionList.add(customerConnection);
                customerConnectionsPerPropositionMap.put(customerConnection.Proposition__c, temporaryCustomerConnectionList);
            } else {
                List<CS_Customer_Connection__c> temporaryCustomerConnectionList = customerConnectionsPerPropositionMap.get(customerConnection.Proposition__c);
                temporaryCustomerConnectionList.add(customerConnection);
            }
        }
        
        return customerConnectionsPerPropositionMap;
    }
    
    public CS_Customer_Connection__c getCustomerConnection(String proposition, String year, String costType, String chargeType, Integer dealItemQuantity, String dealItem) {
        if(proposition != null && mapOfAllCustomerConnectionsPerProposition.get(proposition) != null) {
            for(CS_Customer_Connection__c customerConnection : mapOfAllCustomerConnectionsPerProposition.get(proposition)) {
                if(customerConnection.Year__c == year && customerConnection.Cost_Type__c == costType && customerConnection.Charge_Type__c == chargeType && customerConnection.Deal_Item__r.Name == dealItem) {
                    if(dealItemQuantity >= customerConnection.Minimum_Quantity__c && dealItemQuantity <= customerConnection.Maximum_Quantity__c) {
                        return customerConnection;
                    }
                }
            }
        }
        return null;
    }
    
    public Decimal calculateGeneralFormula(Integer dealItemQuantity, CS_Customer_Connection__c customerConnection) {
        if(customerConnection != null) {
            return dealItemQuantity * (customerConnection.Allocation__c / 100) * customerConnection.Value__c;
        }
        return 0;
    }
}