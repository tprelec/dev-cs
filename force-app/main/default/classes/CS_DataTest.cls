@IsTest
public class CS_DataTest {
    public static User createUser(List<Profile> profiles, List<UserRole> roleList) {
        User simpleUser = new User(
                UserRoleId = roleList[0].Id,
                ProfileId = profiles[0].Id,
                Alias = 'standard',
                Email = String.valueOf(Math.random()) + 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Testing',
                LanguageLocaleKey = 'nl_NL',
                LocaleSidKey = 'nl_NL',
                TimeZoneSidKey = 'Europe/Amsterdam',
                UserName = String.valueOf(Math.random()) + 'testUserABC@testorganise.com'
        );

        return simpleUser;
    }
    
    public static User createSystemAdministratorUser() {
        List<Profile> pList = CS_DataTest.returnSystemAdminProfile();
        List<UserRole> roleList = CS_DataTest.returnUserRoleWithoutParentRole();
        
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;
        
        return simpleUser;
    }

    public static User createUserName(List<Profile> profiles, List<UserRole> roleList, String userName) {
        User simpleUser = new User(
                UserRoleId = roleList[0].Id,
                ProfileId = profiles[0].Id,
                Alias = 'standard',
                Email = String.valueOf(Math.random()) + 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Testing',
                LanguageLocaleKey = 'nl_NL',
                LocaleSidKey = 'nl_NL',
                TimeZoneSidKey = 'Europe/Amsterdam',
                UserName = String.valueOf(Math.random()) + userName
        );

        return simpleUser;
    }

    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx + 1);
        }
        return randStr;
    }

    public static CS_Advance_Clone_Configuration__c createCSAdvanceCloneConfiguration(String name, String value) {
        CS_Advance_Clone_Configuration__c adcv = new CS_Advance_Clone_Configuration__c();
        adcv.Name = name;
        adcv.Value__c = value;
        adcv.ExternalID__c = generateRandomString(10);
        return adcv;
    }

    public static csord__Subscription__c createSubscription(Id config) {
        csord__Subscription__c result = new csord__Subscription__c();
        result.csordtelcoa__Product_Configuration__c = config;

        return result;
    }

    public static csord__Service__c createService(Id configId, csord__Subscription__c subscription) {
        csord__Service__c result = new csord__Service__c();

        return result;
    }

    public static OrderType__c createOrderType() {
        OrderType__c order = new OrderType__c();
        order.ExportSystem__c = 'EMAIL';
        order.Status__c = 'New';
        order.name = 'Test';
        order.ExternalID__c = generateRandomString(10);
        return order;
    }

    public static OrderType__c createOrderType(String name) {
        OrderType__c order = createOrderType();
        order.name = name;
        return order;
    }

    public static Product2 createProduct(String name, OrderType__c orderType) {
        Product2 product = new Product2();
        product.name = name;
        product.OrderType__c = orderType.Id;
        product.Product_Line__c = 'fVodafone';
        product.Quantity_type__c = 'Each';
        product.ExternalID__c = generateRandomString(10);
        return product;
    }

    public static Product2 createProduct2(string Name, string ProductFamily, OrderType__c orderType, Boolean IsActive) {
        Product2 tmpProduct2 = new Product2();
        tmpProduct2.OrderType__c = orderType.Id;
        tmpProduct2.Family = ProductFamily;
        tmpProduct2.Name = Name;
        tmpProduct2.IsActive = IsActive;
        tmpProduct2.Product_Line__c = 'fVodafone';
        tmpProduct2.Quantity_type__c = 'Each';

        return tmpProduct2;
    }

    public static Category__c createCategory(String name) {
        Category__c category = new Category__c();
        category.name = name;
        category.ExternalID__c = generateRandomString(10);
        return category;
    }

    public static Vendor__c createVendor(String name) {
        Vendor__c vendor = new Vendor__c();
        vendor.name = name;
        vendor.ExternalID__c = generateRandomString(10);
        return vendor;
    }

    public static cspmb__Price_Item__c createPriceItem(Product2 product, Category__c category, Decimal bandwidthUp, Decimal bandwidthDown, Vendor__c vendor, String resultCheck, String cpeSla) {
        cspmb__Price_Item__c result = new cspmb__Price_Item__c();
        result.cspmb__Is_Active__c = true;
        result.Min_Duration__c = 1;
        result.Max_Duration__c = 100;
        result.Result_check__c = resultCheck;
        result.Vendor__c = vendor.name;
        result.Available_bandwidth_up__c = bandwidthUp;
        result.Available_bandwidth_down__c = bandwidthDown;
        result.Category__c = category.name;
        result.CPE_SLA__c = cpeSla;
        result.One_Off_Charge_Product__c = product.Id;
        return result;
    }

    public static cspmb__Price_Item__c createPriceItemMobile(Product2 product, Category__c category, String mobileScenario, String subscription) {
        cspmb__Price_Item__c result = new cspmb__Price_Item__c();
        result.cspmb__Is_Active__c = true;
        result.Min_Duration__c = 1;
        result.Max_Duration__c = 100;
        result.One_Off_Charge_Product__c = product.Id;
        result.Category__c = category.name;
        result.Mobile_Scenario__c = mobileScenario;
        result.Mobile_Subscription__c = subscription;
        return result;
    }

    public static Site__c createSite(String name, Account siteAccount, String postalCode, String street, String city, Decimal houseNumber) {
        Site__c site = new Site__c();
        site.Name = name;
        site.Site_Postal_Code__c = postalCode;
        site.Site_Account__c = siteAccount.Id;
        site.Site_Street__c = street;
        site.Site_City__c = city;
        site.Site_House_Number__c = houseNumber;
        return site;
    }

    public static Site_Postal_Check__c createSite(Site__c site) {
        Site_Postal_Check__c spc = new Site_Postal_Check__c();
        spc.Access_Site_ID__c = site.Id;
        spc.Access_Active__c = true;
        return spc;
    }

    public static Site_Availability__c createSiteAvailability(String name, Site__c site, Site_Postal_Check__c spc) {
        Site_Availability__c siteAv = new Site_Availability__c();
        siteAv.Name = name;
        siteAv.Vendor__c = 'KPNWEAS';
        siteAv.Access_Infrastructure__c = 'EthernetOverCopper';
        siteAv.Site__c = site.Id;
        siteAv.Region__c = 'Test';
        siteAv.Bandwith_Up_Entry__c = 20480.0;
        siteAv.Bandwith_Down_Entry__c = 20480.0;
        siteAv.Premium_Vendor__c = 'KPNWEAS';
        siteAv.Bandwith_Down_Premium__c = 20480.0;
        siteAv.Bandwith_Up_Premium__c = 20480.0;
        siteAv.Result_Check__c = 'ONNET';
        siteAv.Existing_Infra__c = false;
        siteAv.Source_Check_Entry__c = spc.Id;
        siteAv.CreatedDate = Date.today().addDays(-1);
        return siteAv;
    }

    public static cscfga__Product_Definition__c createProductDefinitionRegular(String productDefName) {
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c();
        Id recordTypeIdContact = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

        prodDef.Name = productDefName;
        prodDef.cscfga__Description__c = productDefName;
        prodDef.Product_Type__c = 'Fixed';
        return prodDef;
    }

    public static cscfga__Product_Definition__c createProductDefinition(String productDefName) {
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c();
        Id recordTypeIdContact = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Package Definition').getRecordTypeId();

        prodDef.Name = productDefName;
        prodDef.cscfga__Description__c = 'testProdDefDescription';
        prodDef.Product_Type__c = 'Package';

        return prodDef;
    }

    public static csbb__Product_Configuration_Request__c createPCR(cscfga__Product_Configuration__c pc) {
        csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c();
        pcr.csbb__Product_Configuration__c = pc.Id;

        return pcr;
    }

    public static Opportunity createZiggoOpportunity(Account acc, String name, Id ownerId) {
        Opportunity opp = new Opportunity();
        opp.Name = name;
        opp.Account = acc;
        opp.AccountId = acc.Id;
        opp.OwnerId = ownerId;
        opp.StageName = 'Qualification';
        opp.CloseDate = system.today();
        Id opportunityRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ziggo').getRecordTypeId();
        opp.RecordTypeId = opportunityRecordType;
        return opp;
    }

    public static Opportunity createVodafoneOpportunity(Account acc, String name, Id ownerId) {
        Opportunity opp = new Opportunity();
        opp.Name = name;
        opp.Account = acc;
        opp.AccountId = acc.Id;
        opp.OwnerId = ownerId;
        opp.StageName = 'Qualification';
        opp.CloseDate = system.today();
        Id opportunityRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('MAC').getRecordTypeId();
        opp.RecordTypeId = opportunityRecordType;
        return opp;
    }

    public static Opportunity createOpportunity(Account acc, String name, Id ownerId) {
        Opportunity opp = new Opportunity();
        opp.Name = name;
        opp.Account = acc;
        opp.AccountId = acc.Id;
        opp.OwnerId = ownerId;
        opp.StageName = 'Qualification';
        opp.CloseDate = system.today();
        Id opportunityRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('MAC').getRecordTypeId();
        opp.RecordTypeId = opportunityRecordType;
        opp.LG_ExternalID__c = generateRandomString(10);
        return opp;
    }

    public static cscfga__Product_Basket__c createProductBasketOnly(String name) {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.Name = name;
        return basket;
    }

    public static cscfga__Product_Basket__c createProductBasket(Opportunity opp, String name) {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.Name = name;
        basket.cscfga__Opportunity__c = opp.Id;
        return basket;
    }

    public static cscfga__Product_Basket__c createProductBasket(Opportunity opp, String name, Account acc) {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.Name = name;
        basket.cscfga__Opportunity__c = opp.Id;
        basket.csordtelcoa__Account__c = acc.Id;
        return basket;
    }

    public static cscfga__Product_Definition__c createProductPackageDefinition(String productDefName) {
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c();
        prodDef.RecordTypeId = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Package Definition').getRecordTypeId();
        prodDef.Name = productDefName;
        prodDef.cscfga__Description__c = 'testProdDefDescription';

        return prodDef;
    }

    public static cscfga__Product_Configuration__c createProductConfiguration(Id productDefId, String productConfigName, Id basketId) {
        cscfga__Product_Configuration__c prodConfig = new cscfga__Product_Configuration__c();
        prodConfig.Name = productConfigName;
        prodConfig.cscfga__Product_Basket__c = basketId;
        prodConfig.Package_Name__c = '';
        prodConfig.Site_Name__c = '';
        prodConfig.cscfga__Product_Definition__c = productDefId;
        prodConfig.cscfga__Configuration_Status__c = 'Valid';
        return prodConfig;
    }

    public static cscfga__Attribute_Field_Definition__c createAttributeFieldDefinition(Id attributeDefId, String attFName) {
        cscfga__Attribute_Field_Definition__c attF = new cscfga__Attribute_Field_Definition__c(Name = attFName, cscfga__Attribute_Definition__c = attributeDefId);
        return attF;
    }

    public static cscfga__Attribute_Field__c createAttributeField(Id attributeId, String val, String attFName) {
        cscfga__Attribute_Field__c attF = new cscfga__Attribute_Field__c(Name = attFName, cscfga__Attribute__c = attributeId, cscfga__Value__c = val);
        return attF;
    }

    public static cscfga__Attribute__c createAttribute(Id productConfigurationId, String attName, String attValue) {
        cscfga__Attribute__c att = new cscfga__Attribute__c(Name = attName, cscfga__Value__c = attValue, cscfga__Product_Configuration__c = productConfigurationId);
        return att;
    }

    public static cscfga__Attribute__c createAttribute(string Name, cscfga__Attribute_Definition__c AttributeDefinition, Boolean IsLineItem, double Price,
            cscfga__Product_Configuration__c ProductConfiguration, Boolean Recurring, string Value) {
        cscfga__Attribute__c attribute = new cscfga__Attribute__c();
        attribute.Name = Name;
        attribute.cscfga__Attribute_Definition__c = AttributeDefinition.Id;
        attribute.cscfga__Is_Line_Item__c = IsLineItem;
        attribute.cscfga__Price__c = Price;
        attribute.cscfga__Product_Configuration__c = ProductConfiguration.Id;
        attribute.cscfga__Recurring__c = Recurring;
        attribute.cscfga__Value__c = Value;
        attribute.cscfga__Line_Item_Description__c = Name;

        return attribute;
    }

    public static cscfga__Attribute_Definition__c createAttributeDefinition(Id productDefinitionId, String attName, String attValue) {
        cscfga__Attribute_Definition__c att = new cscfga__Attribute_Definition__c(Name = attName, cscfga__Type__c = 'Package slot', cscfga__Product_Definition__c = productDefinitionId);
        return att;
    }

    public static cscfga__Attribute_Definition__c createAttributeDefinition(string name, cscfga__Product_Definition__c productDef, string AttribType, string DataType) {
        cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
        attDef.Name = name;
        attDef.cscfga__Type__c = AttribType;
        attDef.cscfga__Data_Type__c = DataType;
        attDef.cscfga__Product_Definition__c = productDef.Id;

        return attDef;
    }

    public static cscfga__Attribute_Definition__c createPackageSlotAttributeDefinition(Id productDefinitionId, String attName, String attValue) {
        cscfga__Attribute_Definition__c att = new cscfga__Attribute_Definition__c(Name = attName, cscfga__Type__c = 'Package slot', cscfga__Product_Definition__c = productDefinitionId);
        return att;
    }

    public static Site__c createSite(Id accountId, String street, String city, String postalCode, Decimal houseNumber, String houseNumberSuffix, String building) {
        Site__c site = new Site__c();
        site.Site_Account__c = accountId;
        site.Site_City__c = city;
        site.Site_Postal_Code__c = postalCode;
        site.Site_Street__c = street;
        site.Building__c = building;
        site.Site_House_Number__c = houseNumber;

        return site;
    }

    public static Site_Availability__c createSiteAvailability(Site__c site, String accessInfrastructure, Integer bandwidthUpEntry, Integer bandwidthDownEntry, Integer bandwidthDownPremium, Integer bandwidthUpPremium, Boolean existingInfra, String vendor, String resultCheck, String region) {
        Site_Availability__c sa = new Site_Availability__c();
        sa.Site__c = site.Id;
        sa.Access_Infrastructure__c = accessInfrastructure;
        sa.Bandwith_Down_Entry__c = bandwidthDownEntry;
        sa.Bandwith_Up_Entry__c = bandwidthUpEntry;
        sa.Bandwith_Down_Premium__c = bandwidthDownPremium;
        sa.Bandwith_Up_Premium__c = bandwidthUpPremium;
        sa.Existing_Infra__c = existingInfra;
        sa.Vendor__c = vendor;
        sa.Result_Check__c = resultCheck;
        sa.Region__c = region;

        return sa;
    }

    public static Account createAccount(String name) {
        Account acc = new Account();
        acc.Name = name;
        acc.Type = 'End Customer';
        return acc;
    }

    public static Contact createContact(String firstName, String lastName, String role, String email, Id accId) {
        Contact contact = new Contact();
        contact.AccountId = accId;
        contact.LastName = 'Last';
        contact.FirstName = 'First';
        contact.Contact_Role__c = 'Consultant';
        contact.Email = 'test@tele2.com';

        return contact;
    }

    public static csclm__Document_Definition__c createDocumentDefinition() {
        csclm__Document_Definition__c docDefinition = new csclm__Document_Definition__c();
        docDefinition.csclm__Document_Type__c = 'Contract';
        docDefinition.csclm__Linked_Object__c = 'cscfga__Product_Basket__c';
        docDefinition.ExternalID__c = generateRandomString(10);

        return docDefinition;
    }

    public static csclm__Agreement__c createAgreement(String agreementName) {
        csclm__Agreement__c newAgreement = new csclm__Agreement__c();

        newAgreement.Name = agreementName;
        newAgreement.csclm__Output_Format__c = 'pdf';

        return newAgreement;
    }

    public static csclm__Document_Template__c createDocumentTemplate(String docTemplateName) {
        csclm__Document_Template__c newDocumentTemplate = new csclm__Document_Template__c();

        newDocumentTemplate.Name = docTemplateName;
        newDocumentTemplate.csclm__Active__c = true;
        newDocumentTemplate.csclm__Valid__c = true;
        newDocumentTemplate.csclm__Effective_From__c = Date.today().addDays(-1);
        newDocumentTemplate.ExternalID__c = generateRandomString(10);

        return newDocumentTemplate;
    }

    public static CS_Basket_Snapshot_Transactional__c createBasketSnapshotTransactional(String basketSnapName, Id basketId, Id parentConfig, String productName, String productGroup) {
        CS_Basket_Snapshot_Transactional__c basketSnapTrans = new CS_Basket_Snapshot_Transactional__c();
        basketSnapTrans.Name = basketSnapName;
        basketSnapTrans.Product_Basket__c = basketId;
        basketSnapTrans.ProductGroup__c = productGroup;
        basketSnapTrans.Parent_Product_Configuration__c = parentConfig;
        basketSnapTrans.ProductName__c = productName;
        basketSnapTrans.FinalPriceOneOff__c = 15;
        basketSnapTrans.FinalPriceRecurring__c = 2;
        basketSnapTrans.DiscountRecurringPercentage__c = 1;
        basketSnapTrans.Quantity__c = 1;
        basketSnapTrans.OneOffPrice__c = 5;
        basketSnapTrans.RecurringPrice__c = 6;
        basketSnapTrans.DiscountOneOffPercentage__c = 2;
        basketSnapTrans.DiscountOneOff__c = 1;

        return basketSnapTrans;
    }

    public static CS_Basket_Snapshot_Transactional__c createBasketSnapshotTransactional(String basketSnapName, Id basketId, Id parentConfig, Id productConfig) {
        CS_Basket_Snapshot_Transactional__c basketSnapTrans = new CS_Basket_Snapshot_Transactional__c();
        basketSnapTrans.Name = basketSnapName;
        basketSnapTrans.Product_Basket__c = basketId;
        basketSnapTrans.ProductGroup__c = 'PG1';
        basketSnapTrans.Parent_Product_Configuration__c = parentConfig;
        basketSnapTrans.Product_Configuration__c = productConfig;
        basketSnapTrans.FinalPriceOneOff__c = 15;
        basketSnapTrans.FinalPriceRecurring__c = 2;
        basketSnapTrans.DiscountRecurringPercentage__c = 1;
        basketSnapTrans.Quantity__c = 1;
        basketSnapTrans.OneOffPrice__c = 5;
        basketSnapTrans.RecurringPrice__c = 6;
        basketSnapTrans.DiscountOneOffPercentage__c = 2;
        basketSnapTrans.DiscountOneOff__c = 1;

        return basketSnapTrans;
    }

    public static CSPOFA__Orchestration_Process_Template__c createOrchProcessTemplate(String name, String estimatedTimeToComplete, Boolean save) {
        CSPOFA__Orchestration_Process_Template__c testProcessTemplate = new CSPOFA__Orchestration_Process_Template__c();
        testProcessTemplate.Name = name;

        if (save) {
            insert testProcessTemplate;
        }

        return testProcessTemplate;
    }

    public static CSPOFA__Orchestration_Step_Template__c createOrchStepTemplate(Id processTemplate, String name, String estimatedTimeToComplete, Boolean save) {
        CSPOFA__Orchestration_Step_Template__c stepTemplate = new CSPOFA__Orchestration_Step_Template__c();
        stepTemplate.CSPOFA__Orchestration_Process_Template__c = processTemplate;
        stepTemplate.Name = name;

        if (save) {
            insert stepTemplate;
        }
        return stepTemplate;
    }

    public static CSPOFA__Orchestration_Process__c createOrchProcess(Id template, Id service, Datetime startDatetime, Datetime endDatetime, Boolean save) {
        CSPOFA__Orchestration_Process__c testProcess = new CSPOFA__Orchestration_Process__c();
        testProcess.CSPOFA__Orchestration_Process_Template__c = template;
        testProcess.csordtelcoa__Service__c = service;

        if (save) {
            insert testProcess;
        }
        return testProcess;
    }

    public static CSPOFA__Orchestration_Step__c createOrchStep(Id stepTemplate, Id process, Boolean save) {
        CSPOFA__Orchestration_Step__c step1 = new CSPOFA__Orchestration_Step__c();
        step1.CSPOFA__Orchestration_Step_Template__c = stepTemplate;
        step1.CSPOFA__Orchestration_Process__c = process;

        if (save) {
            insert step1;
        }
        return step1;
    }

    public static CSPOFA__Orchestration_Step__c createOrchStep(Id stepTemplate, Id process, Boolean milestone, String milestoneLabel, Boolean save) {
        CSPOFA__Orchestration_Step__c step1 = new CSPOFA__Orchestration_Step__c();
        step1.CSPOFA__Orchestration_Step_Template__c = stepTemplate;
        step1.CSPOFA__Orchestration_Process__c = process;
        step1.CSPOFA__Milestone__c = milestone;
        step1.CSPOFA__Milestone_Label__c = milestoneLabel;

        if (save) {
            insert step1;
        }
        return step1;
    }

    public static Case createCase(String subject, String recordType, User owner, Boolean save) {
        Case returnValue;

        returnValue = new Case();
        returnValue.Subject = subject;
        returnValue.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        returnValue.ownerId = owner.Id;

        if (save) {
            insert returnValue;
        }

        return returnValue;
    }
    
    public static Case createCase(String subject, String recordType, User owner, Boolean save, String status, Id contractId, Id accountId) {
        Case returnValue;

        returnValue = new Case();
        returnValue.Subject = subject;
        returnValue.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        returnValue.ownerId = owner.Id;
        returnValue.Status = status;
        returnValue.Contract_VF__c = contractId;
        returnValue.Account__c = accountId;

        if (save) {
            insert returnValue;
        }

        return returnValue;
    }

    public static List<Task> createTasks(Contact contact, SObject what, String internalNumber, Boolean mainTask, String description, String subject, String status, User owner, Integer numberOfRecords, Boolean save) {
        List<Task> taskList = new List<Task>();

        for (Integer i = 0; i < numberOfRecords; i++) {
            Task t = new Task();
            t.Type = 'Other';
            t.WhoId = contact.Id;
            t.OwnerId = owner.Id;
            t.WhatId = what.Id;
            if (i == numberOfRecords - 1) {
                t.Status = 'Completed';
            } else {
                t.Status = status;
            }
            t.Subject = subject;
            t.Description = description;
            t.Main_Task__c = mainTask;
            t.Sequence__c = i;
            t.Internal_Number__c = internalNumber;
            t.ActivityDate = Date.newInstance(2020, i + 1, i + 1);

            taskList.add(t);
        }

        if (save) {
            insert taskList;
        }

        return taskList;
    }

    public static List<Contracts_Mobile_Flow_Checklist__mdt> createMobileFlowMetadata(Set<String> recordTypes, List<String> groups) {
        List<Contracts_Mobile_Flow_Checklist__mdt> returnValue = new List<Contracts_Mobile_Flow_Checklist__mdt>();

        for (String rt : recordTypes) {
            Integer sequenceNumber = 0;
            for (String g : groups) {
                Contracts_Mobile_Flow_Checklist__mdt mobileFlowMetadada = new Contracts_Mobile_Flow_Checklist__mdt();
                mobileFlowMetadada.Case_Record_Type__c = rt;
                mobileFlowMetadada.Internal_Number__c = 'ABCD' + String.valueOf(sequenceNumber);
                mobileFlowMetadada.Main_Task__c = true;
                mobileFlowMetadada.Sequence__c = sequenceNumber;
                mobileFlowMetadada.Task_Name__c = 'Task: ' + String.valueOf(sequenceNumber);
                mobileFlowMetadada.Group__c = g;
                sequenceNumber++;

                returnValue.add(mobileFlowMetadada);
            }
        }
        return returnValue;
    }

    public static VF_Contract__c createVfContract(Decimal totalConnection, String implementationStatus, String rejectionReason, String rejectionComment,
            Date actualMigrationFrom, Date actualMigrationTo, Date expectedMigrationFrom, Date expectedMigrationTo,
            String fqcResult, String fqcComment, String fqcFailReason, Boolean sendKTO, String ktoEmail,
            User implementationManager, Boolean save) {
        VF_Contract__c returnValue = new VF_Contract__c();

        returnValue.Total_Connection__c = totalConnection;
        returnValue.Implementation_Status__c = implementationStatus;
        returnValue.Contract_Rejection_Reason__c = rejectionReason;
        returnValue.Contract_Rejection_Comment__c = rejectionComment;
        returnValue.Expected_Migration_Date_from__c = expectedMigrationFrom < Date.today() ? Date.today() : expectedMigrationFrom;
        returnValue.Expected_Migration_Date_to__c = expectedMigrationTo < Date.today() ? Date.today() : expectedMigrationFrom;
        returnValue.Actual_Migration_Date_from__c = actualMigrationFrom < Date.today() ? Date.today() : expectedMigrationFrom;
        returnValue.Actual_Migration_Date_to__c = actualMigrationTo < Date.today() ? Date.today() : expectedMigrationFrom;
        returnValue.FQC_Result__c = fqcResult;
        returnValue.FQC_Comment__c = fqcComment;
        returnValue.Implementation_Manager__c = implementationManager.Id;

        if (save) {
            insert returnValue;
        }

        return returnValue;
    }
    
    public static VF_Contract__c createDefaultVfContract(User simpleUser, Boolean save) {
        return CS_DataTest.createVfContract(10.0, 'Open', 'Contract not signed by customer', 'Reject Comment', Date.newInstance(2020, 3, 27), Date.newInstance(2020, 3, 29), Date.newInstance(2020, 3, 29), Date.newInstance(2020, 3, 29), 'Passed', 'fqc Comment', 'Wrong discounts', true, 'test@test.com', simpleUser, save);
    }

    public static EmailTemplate createEmailTemplate(String name, String developerName, String subject, String html, String body, Boolean save) {
        EmailTemplate et = new EmailTemplate();
        et.Name = name;
        et.Subject = subject;
        et.HtmlValue = html;
        et.Body = body;
        et.DeveloperName = developerName;
        et.FolderId = UserInfo.getUserId();
        et.TemplateType = 'text';

        if (save) {
            insert et;
        }
        return et;
    }

    public static csord__Solution__c createSolution(String name, String orderId, Boolean save) {
        csord__Solution__c solution = new csord__Solution__c();
        solution.Name = name;
        solution.csord__Order__c = orderId;
        solution.csord__Identification__c = orderId;

        if (save) {
            insert solution;
        }
        return solution;
    }

    public static List<Profile> returnSystemAdminProfile(){
        List<Profile> sysAdminProfileList = [
        SELECT Id, Name
        FROM Profile
        WHERE Name = 'System Administrator'
        LIMIT 1];

        return sysAdminProfileList;
    }

    public static List<UserRole> returnUserRoleWithoutParentRole(){
        List<UserRole> roleList = [
            SELECT Id, Name, DeveloperName
            FROM UserRole u
            WHERE ParentRoleId = null
        ];

        return roleList;
    }
    
    public static List<VF_Contract__c> createVfContracts(Integer numberOfContractsToCreate, User simpleUser) {
        List<VF_Contract__c> listOfContracts = new List<VF_Contract__c>();

        for (Integer contractCreationIteration = 1; contractCreationIteration <= numberOfContractsToCreate; contractCreationIteration++) {
            VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(simpleUser, false);
            vfContract.Eligible_for_CS_Mobile_Flow__c = false;
            vfContract.RecordTypeId = Schema.SObjectType.VF_Contract__c.getRecordTypeInfosByName().get('Contract Implementation').getRecordTypeId();
            listOfContracts.add(vfContract);
        }
        insert listOfContracts;
        return listOfContracts;
    }

    public static cspmb__Add_On_Price_Item__c createAddOnPriceItem(String aopiName) {
        cspmb__Add_On_Price_Item__c testPI = new cspmb__Add_On_Price_Item__c();
        testPI.Name = aopiName;
        testPI.cspmb__Is_Active__c = true;
        return testPI;
    }
}