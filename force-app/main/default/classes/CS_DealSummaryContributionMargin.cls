public with sharing class CS_DealSummaryContributionMargin {

	@AuraEnabled
	public Decimal minValue;
	@AuraEnabled
	public Decimal maxValue;
	@AuraEnabled
	public String type;

	public CS_DealSummaryContributionMargin() {
		
	}

	public CS_DealSummaryContributionMargin(Decimal minValue, Decimal maxValue, String type) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.type = type;
	}
}