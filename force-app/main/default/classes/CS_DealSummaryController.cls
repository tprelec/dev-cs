public class CS_DealSummaryController {
    @AuraEnabled
    public static DealSumaryDataObject getDealSummaryData(Id basketId) {
        List<CS_Basket_Snapshot_Transactional__c> snapshots = CS_SnapshotHelper.getBasketSnapshots(String.valueOf(basketId));
        List<cscfga__Product_Basket__c> pbList = new List<cscfga__Product_Basket__c>([SELECT Id, Name, cscfga__Basket_Status__c, DirectIndirect__c, Contribution_Margin__c FROM cscfga__Product_Basket__c WHERE Id = :basketId]);
        
        Set<Id> configIds = new Set<Id>();
        for (CS_Basket_Snapshot_Transactional__c snap : snapshots) {
            configIds.add(snap.Product_Configuration__c);
            // we have to take parent configurations into account because we don't have snapshots for them
            if (snap.Parent_Product_Configuration__c != null)
                configIds.add(snap.Parent_Product_Configuration__c);
        }

        Map<Id, Integer> configurationSiteCount = CS_SiteUtility.getConfigurationSiteQuantity(configIds);

        // update snaps which are cloned on multiple sites
        for (CS_Basket_Snapshot_Transactional__c snap : snapshots) {
            if (configurationSiteCount.containsKey(snap.Product_Configuration__c)) {
                Integer siteCount = configurationSiteCount.get(snap.Product_Configuration__c);
                snap.Quantity__c = snap.Quantity__c * siteCount;
                if (snap.FinalPriceOneOff__c != null)
                    snap.FinalPriceOneOff__c = snap.FinalPriceOneOff__c * siteCount;
                if (snap.FinalPriceRecurring__c != null)
                    snap.FinalPriceRecurring__c = snap.FinalPriceRecurring__c * siteCount;
                if (snap.DiscountOneOff__c != null)
                    snap.DiscountOneOff__c = snap.DiscountOneOff__c * siteCount;
                if (snap.DiscountRecurring__c != null)
                    snap.DiscountRecurring__c = snap.DiscountRecurring__c * siteCount;
            }
        }
        
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String userProfileName = PROFILE[0].Name;
        
        List<CS_DealSummarySnapshotFields> snapFields = getDataFromConfig('snaphotFields', userProfileName);
        
        DealSumaryDataObject data = new DealSumaryDataObject(snapshots,basketId,pbList[0],snapFields);
        return data;
    }
    
    @AuraEnabled
    public static List<CS_DealSummarySnapshotFields> getDataFromConfig(String configName, String profileName) {
        List<CSDealSummary_Configuration_Association__c> dealSummaryConfField = [
            select name, CS_Deal_Summary_Configuration__c,CS_Deal_Summary_Configuration__r.Value__c
            from CSDealSummary_Configuration_Association__c
            where name = :configName and Profile_Name__c = :profileName
        ];
        List<CS_DealSummarySnapshotFields> result = new List<CS_DealSummarySnapshotFields>();
        
        if(dealSummaryConfField.isEmpty()){
            dealSummaryConfField = [
            select name,CS_Deal_Summary_Configuration__c,CS_Deal_Summary_Configuration__r.Value__c
            from CSDealSummary_Configuration_Association__c
            where name = :configName and Profile_Name__c = ''
        ];
        }
        
        if(!dealSummaryConfField.isEmpty()){
            List<CS_DealSummarySnapshotFields> objectFields = (List<CS_DealSummarySnapshotFields>)JSON.deserialize(dealSummaryConfField[0].CS_Deal_Summary_Configuration__r.Value__c, List<CS_DealSummarySnapshotFields>.class);
    
            for(CS_DealSummarySnapshotFields fv :objectFields){
                 CS_DealSummarySnapshotFields fa =  new CS_DealSummarySnapshotFields(fv.label,fv.name,fv.type, fv.visibleIndirect, fv.visibleDirect, fv.sequence, fv.recurringOneOff);
                result.add(fa);
            }
        }
        return result;
    }
    
    @AuraEnabled
    public static String getProfileName() {
        try {
            String profileId = UserInfo.getProfileId();
            Profile profile = [SELECT Id, Name FROM Profile WHERE Id = :profileId];
            return profile.Name;
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage()); 
        }
    }
}