global class CS_DiscountConfigurationResultObserver implements csdiscounts.IDiscountConfigurationResultObserver{
     
    public CS_DiscountConfigurationResultObserver () {
         
    }
 
    public csdiscounts.ConfigurationQueryResult execute(Id basketId, csdiscounts.ConfigurationQueryResult configurationQueryResult){
 
        system.debug('Update configurationQueryResult:: '+configurationQueryResult);
 
        return configurationQueryResult;
    }
}