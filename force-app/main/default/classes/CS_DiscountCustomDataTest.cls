@isTest
public class CS_DiscountCustomDataTest {
    private static testMethod void callConstructorWithArgument() {
        String stringToTest = 'Test String';
        CS_DiscountCustomData discountData = new CS_DiscountCustomData(stringToTest);
        System.assertEquals(stringToTest, discountData.key);
    }
}