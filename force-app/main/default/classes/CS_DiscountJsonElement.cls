public class CS_DiscountJsonElement {
    public Decimal duration;
    public String type;
    public String source;
    public String discountCharge;
    public String description;
    public Map<String,String> customData;
    public Decimal amount;
    public String chargeType;
    public String recordType;
    public String version;
    
    public CS_DiscountJsonElement(Decimal duration, String type, String source, String discountCharge, String description, Map<String,String> customData) {
        this.duration = duration;
        this.type = type;
        this.source = source;
        this.discountCharge = discountCharge;
        this.description = description;
        this.customData = customData;
    }

    public class CS_DiscountJsonElementNew {
        public CS_DiscountJsonElement[] memberDiscounts;
        public String evaluationOrder;
        public String version;
        public String recordType;
    }

    public class CS_DiscountJsonElementRoot {
        public CS_DiscountJsonElement.CS_DiscountJsonElementNew[] discounts;
    }
}