public class CS_DocItem {
    @AuraEnabled
    public String name { get; set; }

    @AuraEnabled
    public String contractConditionId { get; set; }

    @AuraEnabled
    public String docId { get; set; }

    @AuraEnabled
    public Boolean isServiceDescription { get; set; }

    @AuraEnabled
    public Boolean isTariffDescription { get; set; }

    @AuraEnabled
    public Boolean isGeneralCondition { get; set; }

    @AuraEnabled
    public String serviceDescriptionUrl { get; set; }

    public CS_DocItem() {}

    public CS_DocItem(CS_DocItem docItemToClone) {
        this.name = docItemToClone.name;
        this.contractConditionId = docItemToClone.contractConditionId;
        this.docId = docItemToClone.docId;
        this.isServiceDescription = docItemToClone.isServiceDescription;
        this.isTariffDescription = docItemToClone.isTariffDescription;
        this.isGeneralCondition = docItemToClone.isGeneralCondition;
        this.serviceDescriptionUrl = docItemToClone.serviceDescriptionUrl;
    }
}