global with sharing class CS_EVCPriceItemLookup extends cscfga.ALookupSearch {
     
 public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit){

    System.debug('**** searchFields: ' + JSON.serializePretty(searchFields));   
    system.debug('productDefinitionID: ' + productDefinitionID);
    system.debug('pageOffset: ' + pageOffset);
    system.debug('pageLimit: ' + pageLimit);
       
    final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1; 
    Integer recordOffset = pageOffset * pageLimit;   
    
    Map<Id,cspmb__Price_Item__c> priceItems;
    List<cspmb__Price_Item__c> priceItemsFinal = new List<cspmb__Price_Item__c>();
    Set<Id> priceItemsToRemove = new Set<Id>();

    Decimal availableBandwidthUp = ((searchFields.get('Available bandwidth up') == null)||(searchFields.get('Available bandwidth up') == ''))?0:Decimal.valueOf(searchFields.get('Available bandwidth up'));
    Decimal availableBandwidthDown = ((searchFields.get('Available bandwidth down') == null)||(searchFields.get('Available bandwidth down') == ''))?0:Decimal.valueOf(searchFields.get('Available bandwidth down'));
    Decimal minBandwidthUp1 = ((searchFields.get('Min. Upstream Bandwidth') == null)||(searchFields.get('Min. Upstream Bandwidth') == ''))?0:Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth'));	
    Decimal minBandwidthUp2 = ((searchFields.get('Min. Upstream Bandwidth 2') == null)||(searchFields.get('Min. Upstream Bandwidth 2') == ''))?0:Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth 2'));
    Decimal minBandwidthDown1 = ((searchFields.get('Min.Downstream Bandwidth') == null)||(searchFields.get('Min.Downstream Bandwidth') == ''))?0:Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth'));
    Decimal minBandwidthDown2 = ((searchFields.get('Min.Downstream Bandwidth 2') == null)||(searchFields.get('Min.Downstream Bandwidth 2') == ''))?0:Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth 2')); 
    Decimal minBandwidthUpInternet = ((searchFields.get('Min. Upstream Bandwidth Internet') == null)||(searchFields.get('Min. Upstream Bandwidth Internet') == ''))?0:Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth Internet'));	
    Decimal minBandwidthDownInternet = ((searchFields.get('Min.Downstream Bandwidth Internet') == null)||(searchFields.get('Min.Downstream Bandwidth Internet') == ''))?0:Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth Internet'));
    Decimal minBandwidthDownOneFixed = ((searchFields.get('Min Bandwidth OneFixed') == null)||(searchFields.get('Min Bandwidth OneFixed') == ''))?0:Decimal.valueOf(searchFields.get('Min Bandwidth OneFixed'));
    Decimal minBandwidthUpOneFixed = ((searchFields.get('Min Bandwidth OneFixed') == null)||(searchFields.get('Min Bandwidth OneFixed') == ''))?0:Decimal.valueOf(searchFields.get('Min Bandwidth OneFixed'));
    Decimal minBandwidthUpOneNet = ((searchFields.get('Min Bandwidth OneNet') == null)||(searchFields.get('Min Bandwidth OneNet') == ''))?0:Decimal.valueOf(searchFields.get('Min Bandwidth OneNet'));
    Decimal minBandwidthDownOneNet = ((searchFields.get('Min Bandwidth OneNet') == null)||(searchFields.get('Min Bandwidth OneNet') == ''))?0:Decimal.valueOf(searchFields.get('Min Bandwidth OneNet'));
	
    Decimal totalBandwidthUp = ((searchFields.get('Total Bandwidth Up') == null)||(searchFields.get('Total Bandwidth Up') == ''))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Up'));
    Decimal totalBandwidthDown = ((searchFields.get('Total Bandwidth Down') == null)||(searchFields.get('Total Bandwidth Down') == ''))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Down'));
	
    Decimal totalBandwidthUpEntry = ((searchFields.get('Total Bandwidth Up Entry') == null)||(searchFields.get('Total Bandwidth Up Entry') == ''))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Up Entry'));
    Decimal totalBandwidthDownEntry = ((searchFields.get('Total Bandwidth Down Entry') == null)||(searchFields.get('Total Bandwidth Down Entry') == ''))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Down Entry'));
	
	Decimal totalBandwidthUpPremium = ((searchFields.get('Total Bandwidth Up Premium') == null)||(searchFields.get('Total Bandwidth Up Premium') == ''))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Up Premium'));
    Decimal totalBandwidthDownPremium = ((searchFields.get('Total Bandwidth Down Premium') == null)||(searchFields.get('Total Bandwidth Down Premium') == ''))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Down Premium'));
	
	String premiumApplicable = searchFields.get('Premium Applicable');
	
    Decimal minBandwidthUp = 0.0;
    Decimal minBandwidthDown = 0.0;
    Decimal upBandwidthRange = availableBandwidthUp - totalBandwidthUp;
    Decimal downBandwidthRange = availableBandwidthDown - totalBandwidthDown;
    Decimal upBandwidthRange10Percent = availableBandwidthUp - totalBandwidthUp*0.1;
    Decimal downBandwidthRange10Percent = availableBandwidthDown - totalBandwidthDown*0.1;

    
    String accessType = '%,' + searchFields.get('Access Type') + ',%';
    String accessTypeCalc = searchFields.get('Access Type');
    Decimal duration = ((searchFields.get('Contract Duration') == null)||(searchFields.get('Contract Duration') == ''))?0:Decimal.valueOf(searchFields.get('Contract Duration'));
    String infraSLA = searchFields.get('Infra SLA');
    
    String overbookingType = searchFields.get('Overbooking Type');
    String overbookingData = (searchFields.get('Overbooking Data') == null ||(searchFields.get('Overbooking Data') == ''))?'':searchFields.get('Overbooking Data');  
    String overbookingVoip = (searchFields.get('Overbooking Voip') == null ||(searchFields.get('Overbooking Voip') == ''))?'':searchFields.get('Overbooking Voip'); 
    String overbookingInternet = (searchFields.get('Overbooking Internet') == null ||(searchFields.get('Overbooking Internet') == ''))?'':searchFields.get('Overbooking Internet');
    String overbookingOneFixed = (searchFields.get('Overbooking OneFixed') == null ||(searchFields.get('Overbooking OneFixed') == '')) ?'':searchFields.get('Overbooking OneFixed');
    String overbookingOneNet = (searchFields.get('Overbooking OneNet') == null ||(searchFields.get('Overbooking OneNet') == '')) ?'':searchFields.get('Overbooking OneNet');
    
    //OneFixed overbooking is always Premium
    Boolean overbookingEntry = false;
    if(overbookingData.equalsIgnoreCase('Entry') || overbookingVoip.equalsIgnoreCase('Entry') || overbookingInternet.equalsIgnoreCase('Entry') || overbookingOneNet.equalsIgnoreCase('Entry')){
        overbookingEntry = true;
    }
    
    String overbookingTransport = searchFields.get('Overbooking Transport');
    String overbooking = '';
    String service = '';
    String vendor = searchFields.get('Vendor');
    String category = '';
    String category1 = '';
    
    String additionType = searchFields.get('AdditionType');
  
    String searchValue = searchFields.get('searchValue')==''?'%':'%'+searchFields.get('searchValue')+'%';
    
    if(overbookingType == 'Data'){
        overbooking = overbookingData;
        minBandwidthUp = minBandwidthUp1;
        minBandwidthDown = minBandwidthDown1;
        service = searchFields.get('Proposition');
        category = 'EVC';
        category1 = 'PVC';
    } 
    else if(overbookingType == 'Voip'){
        overbooking = overbookingVoip;
        minBandwidthUp = minBandwidthUp2;
        minBandwidthDown = minBandwidthDown2;
        service = searchFields.get('Proposition');
        category = 'EVC';
        category1 = 'PVC';
    }
    else if(overbookingType == 'Internet'){
        overbooking = overbookingInternet;
        minBandwidthUp = minBandwidthUpInternet;
        minBandwidthDown = minBandwidthDownInternet;
        service = searchFields.get('Proposition Internet');
        category = 'EVC';
        category1 = 'PVC';
    }
    else if(overbookingType == 'Transport Data'){
        overbooking = overbookingData;
        minBandwidthUp = minBandwidthUp1;
        minBandwidthDown = minBandwidthDown1;
        service = searchFields.get('Proposition');
        category = 'VLAN';
    } 
    else if(overbookingType == 'Transport Voip'){
        overbooking = overbookingVoip;
        minBandwidthUp = minBandwidthUp2;
        minBandwidthDown = minBandwidthDown2;
        service = searchFields.get('Proposition');
        category = 'VLAN';
    }
    else if(overbookingType == 'Transport Internet'){
        overbooking = overbookingInternet;
        minBandwidthUp = minBandwidthUpInternet;
        minBandwidthDown = minBandwidthDownInternet;
        service = searchFields.get('Proposition Internet');
        category = 'VLAN';
    }
    else if(overbookingType == 'Transport EVC'){
        overbooking = overbookingTransport;
        minBandwidthUp = minBandwidthUp1 + minBandwidthUp2 + minBandwidthUpInternet + minBandwidthUpOneFixed;
        minBandwidthDown = minBandwidthDown1 + minBandwidthDown2 + minBandwidthDownInternet + minBandwidthDownOneFixed;
        service = 'Transport';
        category = 'EVC';
        category1 = 'PVC';
    }
    else if(overbookingType == 'OneFixed'){
        overbooking = overbookingOneFixed;
        minBandwidthUp = minBandwidthUpOneFixed;
        minBandwidthDown = minBandwidthDownOneFixed;
        service = searchFields.get('Proposition OneFixed');
        category = 'EVC';
        category1 = 'PVC';
    }
    else if(overbookingType == 'OneNet'){
        overbooking = overbookingOneNet;
        minBandwidthUp = minBandwidthUpOneNet;
        minBandwidthDown = minBandwidthDownOneNet;
        service = searchFields.get('Proposition OneNet');
        category = 'EVC';
        category1 = 'PVC';
    }
     else if(overbookingType == 'Extra EVC'){
        category = 'EVC';
        category1 = 'PVC';    
    }
    
    Boolean extraProductValue = false;
    
    if(overbookingEntry){
        extraProductValue = true;
        
        System.debug('AN - TRUE =='+extraProductValue);
    }
	
	
	Boolean ipvpn = (searchFields.get('IPVPN') == null || (searchFields.get('IPVPN') == ''))? false : true;
    Boolean managedInternet = (searchFields.get('Managed Internet') == null ||(searchFields.get('Managed Internet') == ''))? false : true;
    Boolean oneFixed = (searchFields.get('One Fixed') == null ||(searchFields.get('One Fixed') == ''))? false : true;
    Boolean oneNet = (searchFields.get('One Net') == null ||(searchFields.get('One Net') == ''))? false : true;
    Boolean bandwidthLowerThan6MB = (searchFields.get('Entry BW 6MB') == null ||(searchFields.get('Entry BW 6MB') == '')) ? false : (searchFields.get('Entry BW 6MB') == 'No' || searchFields.get('Entry BW 6MB') == 'false') ? false : true;  
    
	system.debug('availableBandwidthUp: ' + availableBandwidthUp);
    system.debug('availableBandwidthDown: ' + availableBandwidthDown);
    system.debug('minBandwidthUp1: ' + minBandwidthUp1);
    system.debug('minBandwidthUp2: ' + minBandwidthUp2);
    system.debug('minBandwidthDown1: ' + minBandwidthDown1);
    system.debug('minBandwidthUpInternet: ' + minBandwidthUpInternet);
    system.debug('minBandwidthDownInternet: ' + minBandwidthDownInternet);
    system.debug('minBandwidthDown2: ' + minBandwidthDown2);
    system.debug('totalBandwidthUp: ' + totalBandwidthUp);
    system.debug('totalBandwidthDown: ' + totalBandwidthDown);
    system.debug('accessType: ' + accessType);
    system.debug('duration: ' + duration);
    system.debug('infraSLA: ' + infraSLA);
    system.debug('overbookingType: ' + overbookingType);
    system.debug('overbookingData: ' + overbookingData);
    system.debug('overbookingVoip: ' + overbookingVoip);
    system.debug('overbookingInternet: ' + overbookingInternet);
    system.debug('service: ' + service);
    system.debug('vendor: ' + vendor);
	system.debug('premiumApplicable: ' + premiumApplicable);
    system.debug('ipvpn: ' + ipvpn);
    system.debug('managedInternet: ' + managedInternet);
	system.debug('oneFixed: ' + oneFixed);
	system.debug('bandwidthLowerThan6MB: ' + bandwidthLowerThan6MB);
	system.debug('overbookingEntry: ' + overbookingEntry);
	
	//if(overbookingType != 'Extra EVC'){
    	if(premiumApplicable == 'true'){
    		if(overbooking == 'Entry'){
    
    			totalBandwidthUp = ((searchFields.get('Total Bandwidth Up Entry') == null)||(searchFields.get('Total Bandwidth Up Entry') == ''))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Up Entry'));
    			totalBandwidthDown = ((searchFields.get('Total Bandwidth Down Entry') == null)||(searchFields.get('Total Bandwidth Down Entry') == null))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Down Entry'));
    			
    			availableBandwidthUp = ((searchFields.get('Available bandwidth up') == null)||(searchFields.get('Available bandwidth up') == ''))?0:Decimal.valueOf(searchFields.get('Available bandwidth up'));
    			availableBandwidthDown = ((searchFields.get('Available bandwidth down') == null)||(searchFields.get('Available bandwidth down') == ''))?0:Decimal.valueOf(searchFields.get('Available bandwidth down'));	
    			
    			upBandwidthRange = availableBandwidthUp - totalBandwidthUp;
    			downBandwidthRange = availableBandwidthDown - totalBandwidthDown;
    		}
    		else if(overbooking == 'Premium'){
    			totalBandwidthUp = ((searchFields.get('Total Bandwidth Up Premium') == null)||(searchFields.get('Total Bandwidth Up Premium') == ''))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Up Premium'));
    			totalBandwidthDown = ((searchFields.get('Total Bandwidth Down Premium') == null)||(searchFields.get('Total Bandwidth Down Premium') == ''))?0:Decimal.valueOf(searchFields.get('Total Bandwidth Down Premium'));
    			
    			availableBandwidthUp = ((searchFields.get('Available bandwidth up') == null)||(searchFields.get('Available bandwidth up') == ''))?0:Decimal.valueOf(searchFields.get('Available bandwidth up premium'));
    			availableBandwidthDown = ((searchFields.get('Available bandwidth down') == null)||(searchFields.get('Available bandwidth down') == ''))?0:Decimal.valueOf(searchFields.get('Available bandwidth down premium'));	
    			
    			upBandwidthRange = availableBandwidthUp - totalBandwidthUp;
    			downBandwidthRange = availableBandwidthDown - totalBandwidthDown;
    		}
    		
    	/*	system.debug('***totalBandwidthUp: ' + totalBandwidthUp);
    		system.debug('***totalBandwidthDown: ' + totalBandwidthDown);
    
    		system.debug('***availableBandwidthUp: ' + availableBandwidthUp);
    		system.debug('***availableBandwidthDown: ' + availableBandwidthDown);
    		
    		system.debug('***upBandwidthRange: ' + upBandwidthRange);
    		system.debug('***downBandwidthRange: ' + downBandwidthRange);*/
    		
    	       priceItems = new Map<Id,cspmb__Price_Item__c>([SELECT Id, Name, cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Infra_SLA__c, Overbooking__c, Vendor__c, Vendor_lookup__r.Name, Service__c, Duration__c, cspmb__recurring_charge__c, 
                    	       cspmb__one_off_cost__c, cspmb__recurring_cost__c,Product__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c, Region__c, SLA_Method__c, CPE_SLA__c, Category__c, Category_lookup__r.Name, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, 
                    	       Number_of_LAN__c, Number_of_LANWAN__c, Number_of_WAN__c, Wireless_compatible__c, Hardware_Toeslag__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c, WAN_LANWAN_M_LAN__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, 
                    	       One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c, Extra_Product__c, ProductConfigNameText__c, Deal_Type_Text__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c,
                    	       Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c,cspmb__Recurring_Charge_Code__c,Max_Quantity__c,Min_Quantity__c, Promo__c
                                 FROM cspmb__Price_Item__c
                                 WHERE cspmb__Is_Active__c = true AND Access_Type_Text__c LIKE :accessType
                                 AND (Infra_SLA__c =:infraSLA OR Infra_SLA__c = '*')
                                 AND (Overbooking__c =:overbooking OR Overbooking__c = '*') AND Vendor_lookup__r.Name =:vendor AND (Service__c =:service OR Service__c = '*') 
                                 AND (Max_Duration__c > :duration AND Min_Duration__c <= :duration) 
                                 AND Name LIKE :searchValue AND Product__c !=''
                                 AND (Category_lookup__r.Name = :category OR Category_lookup__r.Name = :category1)
                				 AND Available_bandwidth_up__c >=:minBandwidthUp AND Available_bandwidth_up__c <=:upBandwidthRange
                                 AND Available_bandwidth_down__c >=:minBandwidthDown AND Available_bandwidth_down__c <=:downBandwidthRange
                                 ORDER BY Name]); 	
    		
    	}
    	else {
    	    if(overbookingType == 'Transport EVC'){
    	        priceItems = new Map<Id,cspmb__Price_Item__c>([SELECT Id, Name, cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Infra_SLA__c, Overbooking__c, Vendor__c, Vendor_lookup__r.Name, Service__c, Duration__c, cspmb__recurring_charge__c, 
            	        cspmb__one_off_cost__c, cspmb__recurring_cost__c,Product__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c, Region__c, SLA_Method__c, CPE_SLA__c, Category__c, Category_lookup__r.Name, Available_bandwidth_QoS_down__c, 
            	        Available_bandwidth_QoS_up__c, Number_of_LAN__c, Number_of_LANWAN__c, Number_of_WAN__c, Wireless_compatible__c, Hardware_Toeslag__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c, WAN_LANWAN_M_LAN__c, Line_Type__c, 
            	        Router_Type__c, Subscription_Profile__c, SFP_Slots__c, One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c, Extra_Product__c, Deal_Type_Text__c, ProductConfigNameText__c, 
            	        OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c, 
            	        Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c,cspmb__Recurring_Charge_Code__c,Max_Quantity__c,Min_Quantity__c, Promo__c
                             FROM cspmb__Price_Item__c
                             WHERE cspmb__Is_Active__c = true AND Access_Type_Text__c LIKE :accessType 
                             AND Available_bandwidth_up__c >=:minBandwidthUp AND Available_bandwidth_up__c <=:availableBandwidthUp AND Available_bandwidth_up__c <=:upBandwidthRange
                             AND Available_bandwidth_down__c >=:minBandwidthDown AND Available_bandwidth_down__c <=:availableBandwidthDown AND Available_bandwidth_down__c <=:downBandwidthRange 
                             AND (Infra_SLA__c =:infraSLA OR Infra_SLA__c = '*')
                             AND (Overbooking__c =:overbooking OR Overbooking__c = '*') AND Vendor_lookup__r.Name =:vendor AND Service__c =:service 
                             AND (Max_Duration__c > :duration AND Min_Duration__c <= :duration) 
                             AND Name LIKE :searchValue AND Product__c !=''
                             AND (Category_lookup__r.Name = :category OR Category_lookup__r.Name = :category1)
                             ORDER BY Name]);
    	    }
    	    else {
              priceItems = new Map<Id,cspmb__Price_Item__c>([SELECT Id, Name, cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Infra_SLA__c, Overbooking__c, Vendor__c, Vendor_lookup__r.Name, Service__c, Duration__c, cspmb__recurring_charge__c, 
                              cspmb__one_off_cost__c, cspmb__recurring_cost__c,Product__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c, Region__c, SLA_Method__c, CPE_SLA__c, Category__c, Category_lookup__r.Name, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, 
                              Number_of_LAN__c, Number_of_LANWAN__c, Number_of_WAN__c, Wireless_compatible__c, Hardware_Toeslag__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c, WAN_LANWAN_M_LAN__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, 
                              One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c, Extra_Product__c, Deal_Type_Text__c, ProductConfigNameText__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, 
                              Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c, Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c,cspmb__Recurring_Charge_Code__c,Max_Quantity__c,Min_Quantity__c,Promo__c
                                 FROM cspmb__Price_Item__c
                                 WHERE cspmb__Is_Active__c = true AND Access_Type_Text__c LIKE :accessType 
                                 AND Available_bandwidth_up__c >=:minBandwidthUp AND Available_bandwidth_up__c <=:availableBandwidthUp AND Available_bandwidth_up__c <=:upBandwidthRange
                                 AND Available_bandwidth_down__c >=:minBandwidthDown AND Available_bandwidth_down__c <=:availableBandwidthDown AND Available_bandwidth_down__c <=:downBandwidthRange 
                                 AND (Infra_SLA__c =:infraSLA OR Infra_SLA__c = '*')
                                 AND (Overbooking__c =:overbooking OR Overbooking__c = '*') AND Vendor_lookup__r.Name =:vendor AND (Service__c =:service OR Service__c = '*') 
                                 AND (Max_Duration__c > :duration AND Min_Duration__c <= :duration) 
                                 AND Name LIKE :searchValue AND Product__c !=''
                                 AND (Category_lookup__r.Name = :category OR Category_lookup__r.Name = :category1)
                                 ORDER BY Name]);    
    	    }
          
        }	    
	/*} else {
	    priceItems = new Map<Id,cspmb__Price_Item__c>([SELECT Id, Name, cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Infra_SLA__c, Overbooking__c, Vendor__c, Vendor_lookup__r.Name, Service__c, Duration__c, cspmb__recurring_charge__c, 
                              cspmb__one_off_cost__c, cspmb__recurring_cost__c,Product__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c, Region__c, SLA_Method__c, CPE_SLA__c, Category__c, Category_lookup__r.Name, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, 
                              Number_of_LAN__c, Number_of_LANWAN__c, Number_of_WAN__c, Wireless_compatible__c, Hardware_Toeslag__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c, WAN_LANWAN_M_LAN__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, 
                              One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Extra_Product__c, Deal_Type_Text__c, ProductConfigNameText__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c
                                 FROM cspmb__Price_Item__c
                                 WHERE cspmb__Is_Active__c = true AND Access_Type_Text__c LIKE :accessType 
                                 AND (Infra_SLA__c =:infraSLA OR Infra_SLA__c = '*')
                                 AND Overbooking__c = 'Premium' AND Vendor_lookup__r.Name =:vendor
                                 AND ((Max_Duration__c > :duration AND Min_Duration__c <= :duration) OR (Max_Duration__c = 9999999 AND Min_Duration__c = 0)) 
                                 AND Name LIKE :searchValue AND Product__c !=''
                                 AND (Category_lookup__r.Name = :category OR Category_lookup__r.Name = :category1)
                                 AND Extra_Product__c = true
                                 ORDER BY Name]);
	}*/

    //special rulling
    for(cspmb__Price_Item__c pi :priceItems.values()){
        
        if(!(managedInternet && !ipvpn && !oneFixed && !oneNet)){
            if(vendor.containsIgnoreCase('Tele2') && accessType.equalsIgnoreCase('EthernetOverCopper')){
                if(bandwidthLowerThan6MB && priceItems.get(pi.Id).Extra_Product__c == true){
                    priceItemsToRemove.add(pi.Id);
                    system.debug('**** remove 6Mb: ' + pi);
                } 
            } else {
                if(!overbookingEntry && priceItems.get(pi.Id).Extra_Product__c == true){
                    priceItemsToRemove.add(pi.Id);
                    system.debug('**** remove !overbookingEntry: ' + pi);
                }    
            }
        }
        

    }
    //end special rulling
    
        priceItemsFinal = [SELECT Id, Name, cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Infra_SLA__c, Overbooking__c, Vendor__c, Vendor_lookup__r.Name, Service__c, Duration__c, cspmb__recurring_charge__c, 
                          cspmb__one_off_cost__c, cspmb__recurring_cost__c,Product__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c, Region__c, SLA_Method__c, CPE_SLA__c, Category__c, Category_lookup__r.Name, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, 
                          Number_of_LAN__c, Number_of_LANWAN__c, Number_of_WAN__c, Wireless_compatible__c, Hardware_Toeslag__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c, WAN_LANWAN_M_LAN__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, 
                          One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c, Extra_Product__c, Deal_Type_Text__c, ProductConfigNameText__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c,
                          Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c,cspmb__Recurring_Charge_Code__c,Max_Quantity__c,Min_Quantity__c,Promo__c
                             FROM cspmb__Price_Item__c
                             WHERE Id IN :priceItems.keySet() AND Id NOT IN :priceItemsToRemove
                             ORDER BY Available_bandwidth_up__c ASC, Available_bandwidth_down__c ASC NULLS LAST
                             LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
             
        System.debug('*****Number of priceItem results: '+ priceItemsFinal.size());
        return priceItemsFinal;   
    }


  public override String getRequiredAttributes(){ 
  
      return '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType", "Min. Upstream Bandwidth", "Min. Upstream Bandwidth 2", "Min.Downstream Bandwidth", "Min.Downstream Bandwidth 2", "Total Bandwidth Up", "Total Bandwidth Down", "Total Bandwidth Up Entry", "Total Bandwidth Down Entry", "Total Bandwidth Up Premium", "Total Bandwidth Down Premium", "Available bandwidth up premium", "Available bandwidth down premium", "Premium Applicable", "Overbooking Internet", "Min. Upstream Bandwidth Internet", "Min.Downstream Bandwidth Internet", "Proposition Internet", "Overbooking Transport", "Proposition OneFixed", "Overbooking OneFixed", "Min Bandwidth OneFixed", "IPVPN", "Managed Internet", "OneFixed", "Entry BW 6MB","Overbooking OneNet","Min Bandwidth OneNet","Proposition OneNet", "One Net"]';
      
  }  
}