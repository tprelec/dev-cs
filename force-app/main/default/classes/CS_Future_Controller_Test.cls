@isTest
public class CS_Future_Controller_Test {
    private static testMethod void test() {
        
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) { 
            Test.startTest();
        
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;
            
            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
            insert tmpOpp;
            
            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc); 
            
            CS_Future_Controller ctrl = new CS_Future_Controller(tmpProductBasket.Id, 'Main parent process for testing', '', '');
            
            Id newFutureId = ctrl.defineNewFutureJob('Test for 10 seconds future job');
            fakeFutureMethod01(newFutureId);
            
            newFutureId = ctrl.defineNewFutureJob('Test for 5 seconds future job');
            fakeFutureMethod02(newFutureId);
            
            newFutureId = ctrl.defineNewFutureJob('Test for 1 second future job');
            fakeFutureMethod03(newFutureId);
            
            newFutureId = ctrl.defineNewFutureJob('Test for random seconds future job');
            fakeFutureMethod04(newFutureId);
            
            newFutureId = ctrl.defineNewFutureJob('Test for fail method');
            fakeFutureMethod05(newFutureId);
            
            ctrl.setQueueableJobId(newFutureId, newFutureId);
            Test.stopTest();
            
        }
    }
    
    public static void fakeFutureMethod01(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(1000);
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    public static void fakeFutureMethod02(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(5000);
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    public static void fakeFutureMethod03(Id newFutureId) {
       CS_Future_Controller.startFutureJob(newFutureId);
        sleep(1000);
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    public static void fakeFutureMethod04(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
         sleep(Integer.valueof((Math.random() * 3000)));
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    public static void fakeFutureMethod05(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(1000);
        CS_Future_Controller.failFutureJob(newFutureId, 'Failed because of the error');
    }
    
    private static void sleep(Integer milliseconds) 
    {
        Long timeDiff = 0;
        DateTime firstTime = System.now();
        do
        {
            timeDiff = System.now().getTime() - firstTime.getTime();
        }
        while(timeDiff <= milliseconds);      
    }
}