public class CS_Future_Controller_Tests {
    
    public static String basketId = 'a4A9E000000tEx3';

    public static void mainMethod() {
        // Init controller
        CS_Future_Controller ctrl = new CS_Future_Controller(basketId, 'Main parent process for testing');
        
        Id newFutureId = ctrl.defineNewFutureJob('Test for 10 seconds future job');
        fakeFutureMethod01(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for 5 seconds future job');
        fakeFutureMethod02(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for 1 second future job');
        fakeFutureMethod03(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for random seconds future job');
        fakeFutureMethod04(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for fail method');
        fakeFutureMethod05(newFutureId);
        
        
    }
    
    @Future
    public static void fakeFutureMethod01(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(10000);
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    @Future
    public static void fakeFutureMethod02(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(50000);
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    @Future
    public static void fakeFutureMethod03(Id newFutureId) {
       CS_Future_Controller.startFutureJob(newFutureId);
        sleep(10000);
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    @Future
    public static void fakeFutureMethod04(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
         sleep(Integer.valueof((Math.random() * 100000)));
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    @Future
    public static void fakeFutureMethod05(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(10000);
        CS_Future_Controller.failFutureJob(newFutureId, 'Failed because of the error');
    }
    
    private static void sleep(Integer milliseconds) 
    {
        Long timeDiff = 0;
        DateTime firstTime = System.now();
        do
        {
            timeDiff = System.now().getTime() - firstTime.getTime();
        }
        while(timeDiff <= milliseconds);      
    }
}