public class CS_Future_Controller_Tests_DO_NOT_DEPLOY {
    
    public static String basketId = 'a4A9E000000tEx3';

    public static void mainMethod() {
        // Init controller
        CS_Future_Controller ctrl = new CS_Future_Controller(basketId, 'Main parent process for testing', '/apex/appro__VFSubmitPreview?id=a4A9E000000tEx3UAE', '/a4A9E000000tEx3UAE');
        
        Id newFutureId = ctrl.defineNewFutureJob('Test for 10 seconds future job');
        fakeFutureMethod01(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for 5 seconds future job');
        fakeFutureMethod02(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for 1 second future job');
        fakeFutureMethod03(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for random seconds future job');
        fakeFutureMethod04(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for fail method');
        fakeFutureMethod05(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for exception fail method');
        fakeFutureMethod06(newFutureId);
        
        newFutureId = ctrl.defineNewFutureJob('Test for Queueable Parent');
        ID jobID = System.enqueueJob(new AsyncExecutionExample(ctrl, newFutureId, true));
        ctrl.setQueueableJobId(newFutureId, jobID);
        
    }
    
    @Future
    public static void fakeFutureMethod01(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(10000);
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    @Future
    public static void fakeFutureMethod02(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(5000);
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    @Future
    public static void fakeFutureMethod03(Id newFutureId) {
       CS_Future_Controller.startFutureJob(newFutureId);
        sleep(1000);
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    @Future
    public static void fakeFutureMethod04(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(Integer.valueof((Math.random() * 30000)));
        CS_Future_Controller.endFutureJob(newFutureId);
    }
    
    @Future
    public static void fakeFutureMethod05(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        sleep(10000);
        CS_Future_Controller.endFutureJob(newFutureId);
        CS_Future_Controller.failFutureJob(newFutureId, 'Failed because of the error');
    }
    
    @Future
    public static void fakeFutureMethod06(Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);
        try {
            sleep(10000);
            throw new futureException('Future exception.');
        } catch (Exception e) {
            CS_Future_Controller.endFutureJob(newFutureId);
            CS_Future_Controller.failFutureJob(newFutureId, e.getMessage());    
        }
    }
    
    private static void sleep(Integer milliseconds) 
    {
        Long timeDiff = 0;
        DateTime firstTime = System.now();
        do
        {
            timeDiff = System.now().getTime() - firstTime.getTime();
        }
        while(timeDiff <= milliseconds);      
    }
    
    public class futureException extends Exception {}
    
    public class AsyncExecutionExample implements Queueable {
        
        public Boolean repeat = false;
        public Id futureId;
        public CS_Future_Controller ctrl;
        
        public AsyncExecutionExample(CS_Future_Controller ctrl, Id futureId, Boolean repeat) {
            this.futureId = futureId;
            this.repeat = repeat;
            this.ctrl = ctrl;
        }
        
        public void execute(QueueableContext context) {
            CS_Future_Controller.startFutureJob(futureId);
            sleep(20000);
            if(repeat == true) {
                Id childFutureId = ctrl.defineNewFutureJob('Test for Queueable Child');
                ID jobID = System.enqueueJob(new AsyncExecutionExample(ctrl, childFutureId, false));
                ctrl.setQueueableJobId(childFutureId, jobID);
            }
            CS_Future_Controller.endFutureJob(futureId);
        }
    }
}