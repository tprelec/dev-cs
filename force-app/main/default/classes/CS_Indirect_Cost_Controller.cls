global class CS_Indirect_Cost_Controller {
    
    // TODO if needed move to object
    public enum YearOrder { First, Second, Third, Fourth, Fifth }
    public List<Indirect_Cost__c> indirectCostBuffer = new List<Indirect_Cost__c>();
    
    public CS_Indirect_Cost_Controller(String propositionName) {
        refreshIndirectCostsBuffer(propositionName);
    }
    
    public Integer calculateQuantityBasedOnDealItem(Integer monthOrderNumber, Id id, CS_Profit_And_Loss_Helper helper) {
        if(indirectCostBuffer.size() > 0) {
            if(indirectCostBuffer[0].Deal_Item__c != null && indirectCostBuffer[0].Deal_Item__c != '') {
                if(indirectCostBuffer[0].Deal_Item__c == 'CTN Closing Base') {
                    return helper.getClosingBase(monthOrderNumber + 1, id);
                }
            }
        }
        return 0;
    }
    
    public void refreshIndirectCostsBuffer(String propositionName) {
        this.indirectCostBuffer = getAllIndirectCosts(propositionName);
    }
    
    public YearOrder getYearOrderBasedOnDealItem(Integer monthOrderNumber, CS_Profit_And_Loss_Helper helper) {
         if(indirectCostBuffer[0].Deal_Item__c == 'CTN Closing Base') {
             return getYearOrderByNumber(helper.calculateYearOrderForMobile(monthOrderNumber));
         }
         return getYearOrderByNumber(helper.calculateYearOrderForFixed(monthOrderNumber));
    }
    
    public YearOrder getYearOrderByNumber(Integer orderNumber) {
        if(orderNumber == 1) {
            return YearOrder.First;
        } else if (orderNumber == 2) {
            return YearOrder.Second;
        } else if (orderNumber == 3) {
            return YearOrder.Third;
        } else if (orderNumber == 4) {
            return YearOrder.Fourth;
        } else if (orderNumber == 5) {
            return YearOrder.Fifth;
        }
        return null;
    }

    public List<Indirect_Cost__c> getAllIndirectCosts(String propositionName) {
        return [SELECT Name, Cost_Category__c, Customers_Connections__c, Deal_Item__c, Proposition__c, Primary_Driver_First_Year__c, Primary_Driver_Second_Year__c, Primary_Driver_Third_Year__c,  
                    Primary_Driver_Fourth_Year__c, Primary_Driver_Fifth_Year__c, Allocated_Primary__c, Allocated_Secondary__c, Secondary_Driver_First_Year__c,
                    Secondary_Driver_Second_Year__c, Secondary_Driver_Third_Year__c, Secondary_Driver_Fourth_Year__c, Secondary_Driver_Fifth_Year__c FROM Indirect_Cost__c WHERE Proposition__c =:propositionName
        ];
    }
    
    /** 
     * = ([Input deal Sheet].[Opex fVF (fixed)].[Product Family].[Primary Driver] in Year * [Customer & Connections].[Customer & Connections].[Allocated to primary allocation]
     * + [Input deal Sheet].[Opex fVF (fixed)].[Product Family].[Secondary Driver] in Year * [Customer & Connections].[Customer & Connections].[Allocated to Second driver]
     * + [Input deal Sheet].[Opex fZiggo (fixed)].[Product Family].[Primary Driver] in Year * [Customer & Connections].[Customer & Connections].[Allocated to primary allocation]
     * + [Input deal Sheet].[Opex fZiggo (fixed)].[Product Family].[Secondary Driver] in Year * [Customer & Connections].[Customer & Connections].[Allocated to Second driver])
     * * Quantity [# [Customer & Connections].[Customer & Connections].[Deal Item]]
     */
    public Decimal calculateNetworkOpex(List<Indirect_Cost__c> indirectCosts, YearOrder yearOrder, Integer quantity) {
        Decimal networkOpex = 0;
        
        networkOpex = (
                         getAllPrimaryDriversPerCost(indirectCosts, yearOrder, 'Opex fVF (fixed)')
                         + getAllSecondaryDriversPerCost(indirectCosts, yearOrder, 'Opex fVF (fixed)')
                         + getAllPrimaryDriversPerCost(indirectCosts, yearOrder, 'Opex fZiggo (fixed)')
                         + getAllSecondaryDriversPerCost(indirectCosts, yearOrder, 'Opex fZiggo (fixed)')
                      )
                      * quantity;
        
        
        return networkOpex;
    }
    
    public Decimal calculateOverheadAllocation(List<Indirect_Cost__c> indirectCosts, YearOrder yearOrder, Integer quantity) {
        Decimal overheadAllocation = 0;
        
        overheadAllocation = (
                                 getAllPrimaryDriversPerCost(indirectCosts, yearOrder, 'SG&A')
                                 + getAllSecondaryDriversPerCost(indirectCosts, yearOrder, 'SG&A')
                                 + getAllPrimaryDriversPerCost(indirectCosts, yearOrder, 'COPS')
                                 + getAllSecondaryDriversPerCost(indirectCosts, yearOrder, 'COPS')
                             )
                             * quantity;
        
        
        return overheadAllocation;
    }
    
    public Decimal calculateNetworkCapex(List<Indirect_Cost__c> indirectCosts, YearOrder yearOrder, Integer quantity) {
        Decimal networkCapex = 0;
        
        networkCapex = (
                             getAllPrimaryDriversPerCost(indirectCosts, yearOrder, 'Capex fVF (fixed)')
                             + getAllSecondaryDriversPerCost(indirectCosts, yearOrder, 'Capex fVF (fixed)')
                             + getAllPrimaryDriversPerCost(indirectCosts, yearOrder, 'Capex fZiggo (fixed)')
                             + getAllSecondaryDriversPerCost(indirectCosts, yearOrder, 'Capex fZiggo (fixed)')
                       )
                       * quantity;
        
        
        return networkCapex;
    }
    
    private Decimal getAllPrimaryDriversPerCost(List<Indirect_Cost__c> indirectCosts, YearOrder yearOrder, String costCategory) {
        Decimal allPrimaryDriversPerCost = 0;
        if(indirectCosts.size() > 0) {
            for(Indirect_Cost__c indirectCost : indirectCosts) {
                if(indirectCost.Cost_Category__c == costCategory) {
                    allPrimaryDriversPerCost += getPrimaryDriver(indirectCost, yearOrder);
                }
            }
            allPrimaryDriversPerCost = allPrimaryDriversPerCost * indirectCosts[0].Allocated_Primary__c / 100;
        }
        System.debug('>>>>>>>>>YearOrder: ' + yearOrder + ' CostCategory: ' + costCategory + ' allPrimaryDriversPerCost: ' + allPrimaryDriversPerCost);
        return allPrimaryDriversPerCost;
    }
    
    private Decimal getAllSecondaryDriversPerCost(List<Indirect_Cost__c> indirectCosts, YearOrder yearOrder, String costCategory) {
        Decimal allSecondaryDriversPerCost = 0;
        if(indirectCosts.size() > 0) {
            for(Indirect_Cost__c indirectCost : indirectCosts) {
                if(indirectCost.Cost_Category__c == costCategory) {
                    allSecondaryDriversPerCost += getSecondaryDriver(indirectCost, yearOrder);
                }
            }
            allSecondaryDriversPerCost = allSecondaryDriversPerCost * indirectCosts[0].Allocated_Secondary__c / 100;
        }
        System.debug('>>>>>>>>>YearOrder: ' + yearOrder + ' CostCategory: ' + costCategory + ' allSecondaryDriversPerCost: ' + allSecondaryDriversPerCost);
        return allSecondaryDriversPerCost;
    }
    
    private Decimal getPrimaryDriver(Indirect_Cost__c indirectCost, YearOrder yearOrder) {
        if(indirectCost == null || yearOrder == null) {
            return 0;
        }
        
        if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.First) {
            return indirectCost.Primary_Driver_First_Year__c;
        } else if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.Second) {
            return indirectCost.Primary_Driver_Second_Year__c;
        } else if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.Third) {
            return indirectCost.Primary_Driver_Third_Year__c;
        } else if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.Fourth) {
            return indirectCost.Primary_Driver_Fourth_Year__c;
        } else if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.Fifth) {
            return indirectCost.Primary_Driver_Fifth_Year__c;
        }
        return 0;
    }
    
    private Decimal getSecondaryDriver(Indirect_Cost__c indirectCost, YearOrder yearOrder) {
        if(indirectCost == null || yearOrder == null) {
            return 0;
        }
        
        if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.First) {
            return indirectCost.Secondary_Driver_First_Year__c;
        } else if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.Second) {
            return indirectCost.Secondary_Driver_Second_Year__c;
        } else if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.Third) {
            return indirectCost.Secondary_Driver_Third_Year__c;
        } else if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.Fourth) {
            return indirectCost.Secondary_Driver_Fourth_Year__c;
        } else if(yearOrder == CS_Indirect_Cost_Controller.YearOrder.Fifth) {
            return indirectCost.Secondary_Driver_Fifth_Year__c;
        }
        return 0;
    }
    
    public List<Indirect_Cost__c> filterIndirectCostsBasedOnProductFamily(List<Indirect_Cost__c> indirectCosts) {
        // TODO
        return null;
    }
    
}