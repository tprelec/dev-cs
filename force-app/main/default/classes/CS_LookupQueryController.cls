public with sharing class CS_LookupQueryController {
    
    @RemoteAction
    public static String getNumberOfResults(String vendor, String accessType, Decimal bandwidthDown, Decimal bandwidthUp, String overbooking, String service, Boolean infraSLA, String contractTerm){
        String numberOfResults = '';
        
        System.debug('vendor: ' + vendor);
        System.debug('accessType: ' + accessType);
        System.debug('bandwidthDown: ' + bandwidthDown);
        System.debug('bandwidthUp: ' + bandwidthUp);
        System.debug('overbooking: ' + overbooking);
        System.debug('service: ' + service);
        System.debug('infraSLA: ' + infraSLA);
        System.debug('contractTerm: ' + contractTerm);
        
        /*List<cspmb__Price_Item__c> priceItemList = [SELECT Id, Name
                                                FROM cspmb__Price_Item__c
                                                WHERE Vendor__c = :vendor AND Access_Type__c = :accessType AND Available_bandwidth_down__c = :bandwidthDown AND Available_bandwidth_up__c =:bandwidthUp 
                                                AND Overbooking__c = :overbooking AND Service__c = :service AND Infra_SLA__c = :infraSLA AND cspmb__Contract_Term__c = :contractTerm]; */
        
        numberOfResults = String.valueOf(0);
        return numberOfResults;
    }

}