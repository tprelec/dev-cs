public with sharing class CS_MFChangeOwnerForSmallImplCase {

    @InvocableMethod(label = 'Small Implementation Case Owner Update')
    public static void updateOwner(List<Id> caseList){
        if (!caseList.isEmpty()) {
            Map<Id, List<Case>> contractVfCases = new Map<Id, List<Case>>();
            Set<Id> contractIdsSet = new Set<Id>();
            List<Case> casesToUpdate = new List<Case>();

            for (Case objCase: [SELECT Contract_VF__c FROM Case WHERE Id IN :caseList AND Contract_VF__c != null]) {
                contractIdsSet.add(objCase.Contract_VF__c);
            }

            Map<Id, Case> casesMap = new Map<Id, Case>([
                    SELECT Id,
                            OwnerId,
                            RecordType.Name,
                            Contract_VF__c,
                            Contract_VF__r.Rework_Sequence_Nr__c,
                            Comments_for_FQC__c,
                            Contract_VF__r.Deal_Type__c,
                            Contract_VF__r.Responsible_Quality_Officer__c,
                            FQC_Comment__c,
                            FQC_Failure_Reason__c,
                            FQC_Quality_Score__c,
                            Responsible_Quality_Officer__c,
                            KTO_Contact_Email__c,
                            Send_KTO__c,
                            Contract_VF__r.Type_of_Service__c,
                            Contract_VF__r.is_small_implementation__c,
                            IsClosed
                    FROM Case
                    WHERE Contract_VF__c IN :contractIdsSet AND RecordType.Name LIKE 'CS_MF_%'
                    ORDER BY CreatedDate DESC
            ]);

            for (Case c : casesMap.values()) {
                if (contractVfCases.get(c.Contract_VF__c) == null) {
                    contractVfCases.put(c.Contract_VF__c, new List<Case>{ c });
                } else {
                    contractVfCases.get(c.Contract_VF__c).add(c);
                }
            }

            if (casesMap.get(caseList[0]) != null && casesMap.get(caseList[0]).RecordType.Name.equals('CS MF Small Implementation')) {
                Case returnCase;
                Boolean intakeDone = false;

                if (casesMap.get(caseList[0]).Contract_VF__r.is_small_implementation__c && casesMap.get(caseList[0]).Contract_VF__r.Type_of_Service__c == 'Mobile') {
                    returnCase = new Case();
                    returnCase.Id = caseList[0];
                    for (Case otherCase : casesMap.values()) {
                        if (otherCase.RecordType.Name.equals('CS MF Project Intake and Preparation') && otherCase.IsClosed && !intakeDone) {
                            returnCase.OwnerId = otherCase.OwnerId;
                            intakeDone = true;
                        }
                    }
                    casesToUpdate.add(returnCase);
                }

            }

            if (!casesToUpdate.isEmpty()){
                update casesToUpdate;
            }
        }
    }
}