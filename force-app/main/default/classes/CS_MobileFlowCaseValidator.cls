public with sharing class CS_MobileFlowCaseValidator {
    static final String MFRECORDTYPEPREFIX = 'CS MF ';
    private Map<Id, Case> oldCasesMap = new Map<Id, Case>();
    private Map<Id, Case> caseMap = new Map<Id, Case>();
    private Map<Id, VF_Contract__c> caseVfContractMap = new Map<Id, VF_Contract__c>();
    private Map<Id, Case> orderCleaningCasesMap = new Map<Id, Case>();

    public CS_MobileFlowCaseValidator(Map<Id, Case> oldCasesMap, Map<Id, Case> newCasesMap) {
        this.oldCasesMap = oldCasesMap;
        filterCases(newCasesMap);
    }

    public void run() {
        getContractVfRecordsForCases(this.caseMap);
        updateVfContractImplementationStatus(this.caseMap);
    }

    private void getContractVfRecordsForCases(Map<Id, Case> cases) {
        Set<Id> contractVfIds = new Set<Id>();

        for (Case c : cases.values()) {
            contractVfIds.add(c.Contract_VF__c);
        }

        Map<Id, VF_Contract__c> contractVFMap = new Map<Id, VF_Contract__c>([
            SELECT Id,
                Name,
                Total_Connection__c,
                Implementation_Status__c,
                Contract_Rejection_Reason__c,
                Contract_Rejection_Comment__c,
                Expected_Migration_Date_from__c,
                Expected_Migration_Date_to__c,
                Actual_Migration_Date_from__c,
                Actual_Migration_Date_to__c,
                FQC_Result__c,
                FQC_Comment__c,
                Contract_Cleaning_Result__c,
                Implementation_Manager__c,
                Responsible_Quality_Officer__c
            FROM VF_Contract__c
            WHERE Id IN :contractVfIds
        ]);

        for (Case c : cases.values()) {
            if (contractVFMap.get(c.Contract_VF__c) != null) {
                this.caseVfContractMap.put(c.Id, contractVFMap.get(c.Contract_VF__c));
            }
        }
    }

    private void updateVfContractImplementationStatus(Map<Id, Case> cases) {
        List<VF_Contract__c> vfContractsToUpdate = new List<VF_Contract__c>();

        for (Case c : cases.values()) {
            String recordTypeName = LG_Util.getRecordTypeNameById('Case', c.RecordTypeId);
            if (recordTypeName != 'CS MF Order Cleaning' && recordTypeName != 'CS MF Perform Rework') {
                if (this.oldCasesMap.get(c.Id).Status != 'On Hold' && cases.get(c.Id).Status == 'On Hold') {
                    vfContractsToUpdate.add(new VF_Contract__c(
                        Id = this.caseVfContractMap.get(c.Id).Id, Implementation_Status__c = 'On Hold')
                    );
                } else if (this.oldCasesMap.get(c.Id).Status == 'On Hold' && cases.get(c.Id).Status != 'On Hold') {
                    vfContractsToUpdate.add(new VF_Contract__c(
                        Id = this.caseVfContractMap.get(c.Id).Id, Implementation_Status__c = 'In Progress')
                    );
                }
            }
        }

        if (vfContractsToUpdate.size() > 0) {
            Set<VF_Contract__c> vfVontractToUpadteSet = new Set<VF_Contract__c>(vfContractsToUpdate);
            update new List<VF_Contract__c>(vfVontractToUpadteSet);
        }
    }

    private void filterCases(Map<Id, Case> cases) {
        for (Case c : cases.values()) {
            String recordTypeName = LG_Util.getRecordTypeNameById('Case', c.RecordTypeId);
            if (recordTypeName.startsWith(MFRECORDTYPEPREFIX)) {
                this.caseMap.put(c.Id, c);
            }
        }
    }
}