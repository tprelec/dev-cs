public with sharing class CS_MobileFlowService {

    public static final String recordTypePrefix = 'CS MF ';
    public static List<Contracts_Mobile_Flow_Checklist__mdt> mobileFlowMetadata;

    public CS_MobileFlowService() {
    }

    public static void createTasksForCases(List<Case> cases) {
        Set<String> caseRecordTypes = new Set<String>();

        for (Case c : cases) {
            String recordTypeName = LG_Util.getRecordTypeNameById('Case', c.RecordTypeId);
            if (recordTypeName.startsWith(recordTypePrefix)) {
                caseRecordTypes.add(recordTypeName.substringAfter(recordTypePrefix));
            }
        }

        if (mobileFlowMetadata == null) {
            if (!Test.isRunningTest()) {
                mobileFlowMetadata = [
                        SELECT Id, Case_Record_Type__c, DeveloperName, Internal_Number__c, Main_Task__c, Sequence__c, Task_Name__c, Group__c
                        FROM Contracts_Mobile_Flow_Checklist__mdt
                        WHERE Case_Record_Type__c IN :caseRecordTypes
                ];
            } else {
                Set<String> testCaseRecordTypes = new Set<String>();
                testCaseRecordTypes.add('Product Cleanup');
                List<String> groups = new List<String>{
                        'Product Cleanup'
                };
                mobileFlowMetadata = CS_DataTest.createMobileFlowMetadata(testCaseRecordTypes, groups);
            }
        }

        if (mobileFlowMetadata.size() > 0) {
            Map<String, List<Contracts_Mobile_Flow_Checklist__mdt>> mobileFlowMetadataMap = getMobileFlowMetadataMap(mobileFlowMetadata);
            createTasks(cases, mobileFlowMetadataMap);
        }
    }

    public static void updateTaskOwnership(Map<Id, Case> cases) {
        List<Task> tasks = [
            SELECT Id,
                OwnerId,
                Status,
                WhatId
            FROM Task
            WHERE WhatId IN :cases.keySet() AND Status != 'Completed'
        ];

        for (Task t : tasks) {
            if (cases.get(t.WhatId) != null) {
                t.OwnerId = cases.get(t.WhatId).OwnerId;
            }
        }

        dmlOperations(tasks, 'update', false);
    }

    private static void createTasks(List<Case> cases, Map<String, List<Contracts_Mobile_Flow_Checklist__mdt>> mobileFlowMetadataMap) {
        List<Task> newTasks = new List<Task>();
        List<Case> casesToUpdate = new List<Case>();

        for (Case c : cases) {
            if (!c.Mobile_Flow_Tasks_Created__c) {
                String recordTypeName = LG_Util.getRecordTypeNameById('Case', c.RecordTypeId);
                if (mobileFlowMetadataMap.get(recordTypeName.substringAfter(recordTypePrefix).trim()) != null) {
                    for (Contracts_Mobile_Flow_Checklist__mdt cmfc : mobileFlowMetadataMap.get(recordTypeName.substringAfter(recordTypePrefix))) {
                        Task t = new Task();
                        t.WhatId = c.Id;
                        t.Type = 'Other';
                        t.Subject = cmfc.Task_Name__c;
                        t.Description = cmfc.Group__c;
                        t.Sequence__c = cmfc.Sequence__c;
                        t.Main_Task__c = cmfc.Main_Task__c;
                        t.Internal_Number__c = cmfc.Internal_Number__c;
                        t.OwnerId = c.OwnerId;

                        newTasks.add(t);
                    }
                    casesToUpdate.add(new Case(
                        Id = c.Id,
                        Mobile_Flow_Tasks_Created__c = true)
                    );
                }
            }
        }

        if (newTasks.size() > 0) {
            dmlOperations(newTasks, 'insert', false);
            dmlOperations(casesToUpdate, 'update', false);
        }
    }

    private static Map<String, List<Contracts_Mobile_Flow_Checklist__mdt>> getMobileFlowMetadataMap(List<Contracts_Mobile_Flow_Checklist__mdt> mobileFlowMetadata) {
        Map<String, List<Contracts_Mobile_Flow_Checklist__mdt>> returnMap = new Map<String, List<Contracts_Mobile_Flow_Checklist__mdt>>();

        for (Contracts_Mobile_Flow_Checklist__mdt mfc : mobileFlowMetadata) {
            if (returnMap.get(mfc.Case_Record_Type__c) != null) {
                returnMap.get(mfc.Case_Record_Type__c).add(mfc);
            } else {
                List<Contracts_Mobile_Flow_Checklist__mdt> checklistItemsList = new List<Contracts_Mobile_Flow_Checklist__mdt>();
                checklistItemsList.add(mfc);
                returnMap.put(mfc.Case_Record_Type__c, checklistItemsList);
            }
        }

        return returnMap;
    }

    private static void dmlOperations(List<SObject> objects, String operation, Boolean triggerUserEmail) {
        Database.DMLOptions dmlo = new Database.DMLOptions();
        dmlo.EmailHeader.triggerUserEmail = triggerUserEmail;

        switch on operation {
            when 'insert' {
                database.insert(objects, dmlo);
            } when 'update' {
                database.update(objects, dmlo);
            }
        }
    }

    public static void setCaseFields(List<Case> newCases) {
        List<Case> casesToUpdate = new List<Case>();
        List<Case> newCasesMF = new List<Case>();
        Set<Id> contractVFs = new Set<Id>();
        Map<Id, List<Case>> contractVfCases = new Map<Id, List<Case>>();
        Boolean containsMobileFlowCase = false;

        for (Case c : newCases) {
            if (c.Case_Record_Type_Text__c.startsWith('CS_MF_')) {
                newCasesMF.add(c);
                containsMobileFlowCase = true;
                contractVFs.add(c.Contract_VF__c);
            }
        }

        if (containsMobileFlowCase) {
            Map<Id, Case> casesMap = new Map<Id, Case>([
                SELECT Id,
                    RecordType.Name,
                    Contract_VF__c,
                    Contract_VF__r.Rework_Sequence_Nr__c,
                    Comments_for_FQC__c,
                    Contract_VF__r.Deal_Type__c,
                    Contract_VF__r.Responsible_Quality_Officer__c,
                    FQC_Comment__c,
                    FQC_Failure_Reason__c,
                    FQC_Quality_Score__c,
                    Responsible_Quality_Officer__c,
                    KTO_Contact_Email__c,
                    Send_KTO__c,
                    Contract_VF__r.Type_of_Service__c,
                    Contract_VF__r.is_small_implementation__c,
                    IsClosed
                FROM Case
                WHERE Contract_VF__c IN :contractVFs AND RecordType.Name LIKE 'CS_MF_%'
                ORDER BY CreatedDate DESC
            ]);

            for (Case c : casesMap.values()) {
                if (contractVfCases.get(c.Contract_VF__c) == null) {
                    contractVfCases.put(c.Contract_VF__c, new List<Case>{ c });
                } else {
                    contractVfCases.get(c.Contract_VF__c).add(c);
                }
            }

            for (Case c : newCasesMF) {
                if (casesMap.get(c.Id) != null && casesMap.get(c.Id).RecordType.Name.equals('CS MF Final Quality Check')) {
                    Case tmpCase = handleFinalQualityCheckCaseUpdates(casesMap.get(c.Id), contractVfCases.get(casesMap.get(c.Id).Contract_VF__c));
                    casesToUpdate.add(tmpCase);
                } else if (casesMap.get(c.Id) != null && casesMap.get(c.Id).RecordType.Name.equals('CS MF Process Rework')) {
                    Case tmpCase = handleProcessReworkUpdates(casesMap.get(c.Id), contractVfCases.get(casesMap.get(c.Id).Contract_VF__c));
                    casesToUpdate.add(tmpCase);
                } else if (casesMap.get(c.Id) != null && casesMap.get(c.Id).RecordType.Name.equals('CS MF Provide Mobile Subscribers')) {
                    Case tmpCase = handleProvideMobileSubscribersUpdate(casesMap.get(c.Id), contractVfCases.get(casesMap.get(c.Id).Contract_VF__c));
                    casesToUpdate.add(tmpCase);
                } else if (casesMap.get(c.Id) != null && casesMap.get(c.Id).RecordType.Name.equals('CS MF Product Cleanup')) {
                    Case tmpCase = handleProductCleanup(casesMap.get(c.Id), contractVfCases.get(casesMap.get(c.Id).Contract_VF__c));
                    casesToUpdate.add(tmpCase);
                } else if (casesMap.get(c.Id) != null && casesMap.get(c.Id).RecordType.Name.equals('CS MF Small Implementation')) {
                    Case tmpCase = handleSmallImplementation(casesMap.get(c.Id), contractVfCases.get(casesMap.get(c.Id).Contract_VF__c));
                    casesToUpdate.add(tmpCase);
                }
            }

            for (Integer i = 0; i < casesToUpdate.size(); i++) {
                if (casesToUpdate[i] == null) {
                    casesToUpdate.remove(i);
                    i--;
                }
            }

            if (!casesToUpdate.isEmpty()) {
                update casesToUpdate;
            }
        }
    }

    private static Case handleSmallImplementation(Case c, List<Case> otherCasesForVfContract) {
        Case returnCase;
        Boolean intakeDone = false;

        if (c.Contract_VF__r.is_small_implementation__c && c.Contract_VF__r.Type_of_Service__c == 'Mobile') {
            returnCase = new Case();
            returnCase.Id = c.Id;
            for (Case otherCase : otherCasesForVfContract) {
                if (otherCase.RecordType.Name.equals('CS MF Project Intake and Preparation') && otherCase.IsClosed && !intakeDone) {
                    returnCase.KTO_Contact_Email__c = otherCase.KTO_Contact_Email__c;
                    returnCase.Send_KTO__c = otherCase.Send_KTO__c;
                    intakeDone = true;
                }
            }
        }

        return returnCase;
    }

    private static Case handleProductCleanup(Case c, List<Case> otherCasesForVfContract) {
        Case returnCase;
        Boolean intakeDone = false;

        if (c.Contract_VF__r.Deal_Type__c != 'Acquisition' && c.Contract_VF__r.Type_of_Service__c == 'Mobile' && !c.Contract_VF__r.is_small_implementation__c) {
            returnCase = new Case();
            returnCase.Id = c.Id;
            for (Case otherCase : otherCasesForVfContract) {
                if (otherCase.RecordType.Name.equals('CS MF Project Intake and Preparation') && otherCase.IsClosed && !intakeDone) {
                    returnCase.KTO_Contact_Email__c = otherCase.KTO_Contact_Email__c;
                    returnCase.Send_KTO__c = otherCase.Send_KTO__c;
                    intakeDone = true;
                }
            }
        }
        return returnCase;
    }

    private static Case handleProvideMobileSubscribersUpdate(Case c, List<Case> otherCasesForVfContract) {
        Case returnCase;
        Boolean intakeDone = false;

        if (c.Contract_VF__r.Deal_Type__c == 'Acquisition' && c.Contract_VF__r.Type_of_Service__c == 'Mobile' && !c.Contract_VF__r.is_small_implementation__c) {
            returnCase = new Case();
            returnCase.Id = c.Id;
            for (Case otherCase : otherCasesForVfContract) {
                if (otherCase.RecordType.Name.equals('CS MF Project Intake and Preparation') && otherCase.IsClosed && !intakeDone) {
                    returnCase.KTO_Contact_Email__c = otherCase.KTO_Contact_Email__c;
                    returnCase.Send_KTO__c = otherCase.Send_KTO__c;
                    intakeDone = true;
                }
            }
        }

        return returnCase;
    }

    private static Case handleProcessReworkUpdates(Case c, List<Case> otherCasesForVfContract) {
        Case returnCase;
        Boolean fqcDone = false;

        for (Case otherCase : otherCasesForVfContract) {
            if (otherCase.RecordType.Name.equals('CS MF Final Quality Check') && otherCase.IsClosed && !fqcDone) {
                returnCase = new Case();
                returnCase.Id = c.Id;
                returnCase.FQC_Comment__c = otherCase.FQC_Comment__c;
                returnCase.FQC_Failure_Reason__c = otherCase.FQC_Failure_Reason__c;
                fqcDone = true;
            }
        }

        return returnCase;
    }

    private static Case handleFinalQualityCheckCaseUpdates(Case c, List<Case> otherCasesForVfContract) {
        Case returnCase;
        Boolean processReworkDone = false;
        Boolean provideMobileSubscribersDone = false;
        Boolean fqcDone = false;
        Boolean productCleanupDone = false;
        Boolean smallImplementation = false;

        if ((c.Contract_VF__r.Deal_Type__c == 'Acquisition' && c.Contract_VF__r.Type_of_Service__c == 'Mobile' && !c.Contract_VF__r.is_small_implementation__c) && (c.Contract_VF__r.Rework_Sequence_Nr__c == null || c.Contract_VF__r.Rework_Sequence_Nr__c == 0)) {
            for (Case otherCase : otherCasesForVfContract) {
                if (otherCase.RecordType.Name.equals('CS MF Provide Mobile Subscribers') && otherCase.IsClosed && !provideMobileSubscribersDone) {
                    returnCase = new Case();
                    returnCase.Id = c.Id;
                    returnCase.Comments_for_FQC__c = otherCase.Comments_for_FQC__c;
                    provideMobileSubscribersDone = true;
                }
            }
        } else if ((c.Contract_VF__r.Deal_Type__c != 'Acquisition' && c.Contract_VF__r.Type_of_Service__c == 'Mobile' && !c.Contract_VF__r.is_small_implementation__c) && (c.Contract_VF__r.Rework_Sequence_Nr__c == null || c.Contract_VF__r.Rework_Sequence_Nr__c == 0)) {
            for (Case otherCase : otherCasesForVfContract) {
                if (otherCase.RecordType.Name.equals('CS MF Product Cleanup') && otherCase.IsClosed && !productCleanupDone) {
                    returnCase = new Case();
                    returnCase.Id = c.Id;
                    returnCase.Comments_for_FQC__c = otherCase.Comments_for_FQC__c;
                    productCleanupDone = true;
                }
            }
        } else if ((c.Contract_VF__r.is_small_implementation__c || !(c.Contract_VF__r.Type_of_Service__c == 'Mobile')) && (c.Contract_VF__r.Rework_Sequence_Nr__c == null || c.Contract_VF__r.Rework_Sequence_Nr__c == 0)) {
            for (Case otherCase : otherCasesForVfContract) {
                if (otherCase.RecordType.Name.equals('CS MF Small Implementation') && otherCase.IsClosed && !smallImplementation) {
                    returnCase = new Case();
                    returnCase.Id = c.Id;
                    returnCase.Comments_for_FQC__c = otherCase.Comments_for_FQC__c;
                    smallImplementation = true;
                }
            }
        } else if (c.Contract_VF__r.Rework_Sequence_Nr__c > 0) {
            returnCase = new Case();
            returnCase.Id = c.Id;
            for (Case otherCase : otherCasesForVfContract) {
                if (otherCase.RecordType.Name.equals('CS MF Process Rework') && otherCase.IsClosed && !processReworkDone) {
                    returnCase.Comments_for_FQC__c = otherCase.Comments_for_FQC__c;
                    processReworkDone = true;
                } else if (otherCase.RecordType.Name.equals('CS MF Final Quality Check') && otherCase.IsClosed && !fqcDone) {
                    returnCase.FQC_Comment__c = otherCase.FQC_Comment__c;
                    returnCase.FQC_Failure_Reason__c = otherCase.FQC_Failure_Reason__c;
                    returnCase.FQC_Quality_Score__c = otherCase.FQC_Quality_Score__c;
                    returnCase.Responsible_Quality_Officer__c = otherCase.Contract_VF__r.Responsible_Quality_Officer__c;
                    fqcDone = true;
                }
            }
        }

        return returnCase;
    }

    private static List<Contracts_Mobile_Flow_Checklist__mdt> createCustomMetadata(String recordType, String groupName, String internalNum, Boolean mainTask, Decimal sequence, String taskName){
        List<Contracts_Mobile_Flow_Checklist__mdt> returnValue = new List<Contracts_Mobile_Flow_Checklist__mdt>();

        Contracts_Mobile_Flow_Checklist__mdt metadataRecord = new Contracts_Mobile_Flow_Checklist__mdt();
        metadataRecord.Case_Record_Type__c = recordType;
        metadataRecord.Group__c = groupName;
        metadataRecord.Internal_Number__c = internalNum;
        metadataRecord.Main_Task__c = mainTask;
        metadataRecord.Sequence__c = sequence;
        metadataRecord.Task_Name__c = taskName;

        returnValue.add(metadataRecord);
        return returnValue;
    }
}