global with sharing class CS_MobileHardwarePriceItemLookup extends cscfga.ALookupSearch {
     
 public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit){

    system.debug('****searchfields: ' + JSON.serializePretty(searchFields));
    
    final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1; 
    Integer recordOffset = pageOffset * pageLimit;  
    
    string subscription = searchfields.get('Subscription');
    string scenario = searchFields.get('Scenario');
    List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>();
    
    if(scenario == CS_Constants.REDPRO_SCENARIO_NEW_PORTING || scenario == CS_Constants.REDPRO_SCENARIO_RETENTION_MIGRATION ){
        priceItems = [SELECT Id, Name, mobile_vendor__c, mobile_subscription__c, mobile_scenario__c, mobile_add_on_category__c, cspmb__recurring_cost__c, cspmb__recurring_charge__c, cspmb__one_off_cost__c, 
                        cspmb__one_off_charge__c, Recurring_Charge_Product__c, One_Off_Charge_Product__c, cspmb__Price_Item_Code__c, One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c
                 FROM cspmb__Price_Item__c
                 WHERE cspmb__Is_Active__c = true AND mobile_add_on_category__c = 'Mobile hardware'
                 AND mobile_subscription__c INCLUDES (:subscription)
                 LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];         
    } else {
        priceItems = [SELECT Id, Name, mobile_vendor__c, mobile_subscription__c, mobile_scenario__c, mobile_add_on_category__c, cspmb__recurring_cost__c, cspmb__recurring_charge__c, cspmb__one_off_cost__c, 
                        cspmb__one_off_charge__c, Recurring_Charge_Product__c, One_Off_Charge_Product__c, cspmb__Price_Item_Code__c, One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c
                 FROM cspmb__Price_Item__c
                 WHERE cspmb__Is_Active__c = true AND mobile_add_on_category__c = 'Mobile hardware'
                 LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset]; 
    }
    

             
        System.debug('*****Number of priceItem results: '+ priceItems.size());
        return priceItems; 
       
    }


  public override String getRequiredAttributes(){ 
  
      return '["Subscription","Scenario"]';
      
  }  
}