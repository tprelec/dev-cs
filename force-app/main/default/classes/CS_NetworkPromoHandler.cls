public class CS_NetworkPromoHandler {

    //relevant attributes: -> 'Basket qualification', 'Network Promo OneOff', 'Network Promo Recurring', 'Recurring price', 'One off price'
    public static final String Basket_qualification = 'Basket qualification';
    public static final String Network_Promo_OneOff = 'Network Promo OneOff';
    public static final String Network_Promo_Recurring = 'Network Promo Recurring';
    public static final String Network_Promo_RecurringPrice = 'Recurring price';
    public static final String Network_Promo_OneOffPrice = 'One off price';
    public static final String CTN_quantity = 'CTN quantity';
    
    public static final String OriginalOneOff = 'PriceItemOriginalOneOff';
    public static final String OriginalRecurring = 'PriceItemOriginalRecurring';
    //
    public static final String Net_profit = 'Net profit';
    public static final String SAC_SRC = 'SAC SRC';
    
    
    public static void updateAndRecalculate(List<cscfga__Product_Basket__c> newPB, Map<Id,cscfga__Product_Basket__c> oldPBMap){
        Set<String> relevantNames = new Set<String>();
        relevantNames.add(Basket_qualification);
        relevantNames.add(Network_Promo_OneOff);
        relevantNames.add(Network_Promo_Recurring);
        relevantNames.add(Network_Promo_RecurringPrice);
        relevantNames.add(Network_Promo_OneOffPrice);
        relevantNames.add(CTN_quantity);
        relevantNames.add(OriginalOneOff);
        relevantNames.add(OriginalRecurring);
        
        Boolean changeHappened = checkIfBasketQualificationChanged(newPB,oldPBMap);
        
        if(changeHappened){
            Set<Id> basketIds = new Set<Id>();
            for(cscfga__Product_Basket__c basket : newPB){
                basketIds.add(basket.Id);
            }
            
            Map<Id, cscfga__Product_Configuration__c> basketConfigs= getConfigData(basketIds);
            
            
            Map<Id, List<cscfga__Attribute__c>> configAttributes = getAttributeData(basketConfigs.keySet(), relevantNames);
            
            basketIds.addAll(processBaskets(newPB, oldPBMap,configAttributes,basketConfigs,relevantNames));
            
            
            if(basketIds.size()>0){
                recalculateAll(basketIds);
            }
        }
    }
    
    private static Boolean checkIfBasketQualificationChanged(List<cscfga__Product_Basket__c> newPB, Map<Id,cscfga__Product_Basket__c> oldPBMap){
        Boolean result = false;
        for(cscfga__Product_Basket__c basket : newPB){
            String newVal = getBasketQualificationValue(basket);
            String oldVal = getBasketQualificationValue(oldPBMap.get(basket.Id));
            if(newVal != oldVal){
                result = true;
            }
        }
        return result;
    }
    
    private static Map<Id, cscfga__Product_Configuration__c> getConfigData(Set<Id> basketId){
        List<cscfga__Product_Definition__c> prodDefs = [SELECT Id from cscfga__Product_Definition__c where name = 'Mobile CTN addons'];
        Map<Id, cscfga__Product_Configuration__c> configMap = getRelevantConfigurations(basketId,prodDefs[0].Id);
        return configMap;
    }
    
    private static Map<Id, List<cscfga__Attribute__c>> getAttributeData(Set<Id> configIds, Set<String> relAttribute){
        Map<Id, List<cscfga__Attribute__c>> configAttributes = getRelevantAttributes(relAttribute, configIds);
        return configAttributes;
    }
    
    private static Map<Id, List<cscfga__Attribute__c>> getBasketConfigAttributes(Set<Id> basketId, Map<Id, List<cscfga__Attribute__c>> configAttributes,Set<String> relAttribute){
        List<cscfga__Product_Definition__c> prodDefs = [SELECT Id from cscfga__Product_Definition__c where name = 'Mobile CTN addons'];
        Map<Id, cscfga__Product_Configuration__c> configMap = getRelevantConfigurations(basketId,prodDefs[0].Id);
        Set<Id> configIds = configMap.keySet();
        Map<Id, List<cscfga__Attribute__c>> resultMap= getRelevantAttributes(relAttribute, configIds);
        return resultMap;
    }
    
    private static Map<Id, List<cscfga__Attribute__c>> fetchBasketConfigAttributes(Id basketId, Map<Id, List<cscfga__Attribute__c>> configAttributes, Map<Id, cscfga__Product_Configuration__c> configsMap){
       Map<Id, List<cscfga__Attribute__c>> resultMap = new Map<Id, List<cscfga__Attribute__c>>();
       //get config attributes for configs belonging to single basket
        for( Id confId : configAttributes.keySet()){
            //check if config from this basket an add to result map
            if(configsMap.get(confId).cscfga__Product_Basket__c == basketId){
                List<cscfga__Attribute__c> resultList = configAttributes.get(confId);
                resultMap.put(confId, resultList);
            }
        }
        return configAttributes;
    }
    
    private static Set<Id> processBaskets(List<cscfga__Product_Basket__c> newPB, Map<Id,cscfga__Product_Basket__c> oldPBMap, Map<Id, List<cscfga__Attribute__c>> configAttributes,Map<Id,cscfga__Product_Configuration__c> basketConfigs, Set<String> relevantNames){
        Set<Id> updatedBaskets = new Set<Id>();
        List<cscfga__Attribute__c> updatedAttributes = new List<cscfga__Attribute__c>();
        //check if new value is SAC SRC of Net Profit
        for(cscfga__Product_Basket__c basket : newPB){
            String newVal = getBasketQualificationValue(basket);
            String oldVal = getBasketQualificationValue(oldPBMap.get(basket.Id));
            
            Map<Id, List<cscfga__Attribute__c>> basketAttributes = fetchBasketConfigAttributes(basket.Id,configAttributes,basketConfigs);
            if(newVal != oldVal){
                if((newVal == SAC_SRC) && (oldVal == Net_profit)){
                    List<cscfga__Attribute__c> updatedAttrs = includeNetworkPromo(basket.Id, relevantNames,basketAttributes);
                    if(updatedAttrs!=null && updatedAttrs.size()>0){
                        updatedAttributes.addAll(updatedAttrs);
                        updatedBaskets.add(basket.Id);
                    }
                }
                if((newVal == Net_profit) && (oldVal == SAC_SRC)){
                    List<cscfga__Attribute__c> updatedAttrs = removeNetworkPromo(basket.Id, relevantNames,basketAttributes);
                    if(updatedAttrs!=null && updatedAttrs.size()>0){
                        updatedAttributes.addAll(updatedAttrs);
                        updatedBaskets.add(basket.Id);
                    }
                }
            }
            
        }
        
        if(updatedAttributes.size()>0){
            update updatedAttributes;
        }
        
        return updatedBaskets;
    }

    private static String getBasketQualificationValue(cscfga__Product_Basket__c basket){
        return basket.Basket_qualification__c;
    }
    private static List<cscfga__Attribute__c> removeNetworkPromo(Id basketId, Set<String> relAttribute, Map<Id, List<cscfga__Attribute__c>> configAttributes){
        List<cscfga__Attribute__c> attributesToUpdate = new List<cscfga__Attribute__c>();
        
        for(Id confId : configAttributes.keySet()){
            List<cscfga__Attribute__c> atts = configAttributes.get(confId);
            List<cscfga__Attribute__c> possibleUpdates = configureRullingNetProfit(confId, atts);
            if(possibleUpdates!=null && possibleUpdates.size()>1){
                attributesToUpdate.addAll(possibleUpdates);
            }
        }
        
        return attributesToUpdate;
    }
    
     private static List<cscfga__Attribute__c> includeNetworkPromo(Id basketId, Set<String> relAttribute, Map<Id, List<cscfga__Attribute__c>> configAttributes){
       List<cscfga__Attribute__c> attributesToUpdate = new List<cscfga__Attribute__c>();
        
        for(Id confId : configAttributes.keySet()){
            List<cscfga__Attribute__c> atts = configAttributes.get(confId);
            List<cscfga__Attribute__c> possibleUpdates = configureRullingSACSRC(confId, atts);
            if(possibleUpdates!=null && possibleUpdates.size()>1){
                attributesToUpdate.addAll(possibleUpdates);
            }
        }
        
        return attributesToUpdate;
        
    }

    private static cscfga__Attribute__c getAttributeByName(String name, List<cscfga__Attribute__c> attrs){
        cscfga__Attribute__c attribute;
        for(cscfga__Attribute__c att : attrs){
            if(att.Name == name){
                attribute = att;
            }
        }
        return attribute;
    }
    
    private static List<cscfga__Attribute__c> configureRullingSACSRC(Id configId, List<cscfga__Attribute__c> configAttributes){
        List<cscfga__Attribute__c> updates = new List<cscfga__Attribute__c>();
        
        updates.add(setAttributeValueString(SAC_SRC, getAttributeByName(Basket_qualification,configAttributes)));
        
        cscfga__Attribute__c netPromoOneOff = getAttributeByName(Network_Promo_OneOff, configAttributes);
        cscfga__Attribute__c netPromoRecurring = getAttributeByName(Network_Promo_Recurring, configAttributes);
        cscfga__Attribute__c ctnQuantity = getAttributeByName(CTN_quantity, configAttributes);
        cscfga__Attribute__c oneOffPrice = getAttributeByName(Network_Promo_OneOffPrice, configAttributes);
        cscfga__Attribute__c recurringPrice = getAttributeByName(Network_Promo_RecurringPrice, configAttributes);
        cscfga__Attribute__c oneOffPriceOrigin = getAttributeByName(OriginalOneOff, configAttributes);
        cscfga__Attribute__c recurringPriceOrigin = getAttributeByName(OriginalRecurring, configAttributes);
        
        
        //if network promo one off is set
        if(netPromoOneOff.cscfga__Value__c !=null && netPromoOneOff.cscfga__Value__c !=''){
            Decimal netPromoOneOffVal = Decimal.valueOf(netPromoOneOff.cscfga__Value__c);
            Decimal quantity;
            Decimal singlePrice;
            if(ctnQuantity.cscfga__Value__c !=null && ctnQuantity.cscfga__Value__c !=''){
                quantity = Decimal.valueOf(ctnQuantity.cscfga__Value__c);
            }
            
            if(oneOffPriceOrigin.cscfga__Value__c !=null && oneOffPriceOrigin.cscfga__Value__c !=''){
                singlePrice = Decimal.valueOf(oneOffPriceOrigin.cscfga__Value__c);
            }
            
            if(singlePrice!=null && quantity!=null && netPromoOneOffVal!=null){
                Decimal oneOffPriceDecimal = singlePrice*quantity*(1-(netPromoOneOffVal/100.0));
                oneOffPrice.cscfga__Value__c = String.valueOf(oneOffPriceDecimal);
                updates.add(setAttributeValueString(String.valueOf(oneOffPriceDecimal), oneOffPrice));
            }
        }
        
        //if network promo recurring is set
        if(netPromoRecurring.cscfga__Value__c !=null && netPromoRecurring.cscfga__Value__c !=''){
            Decimal netPromoRecurringVal = Decimal.valueOf(netPromoRecurring.cscfga__Value__c);
            Decimal quantity;
            Decimal singlePrice;
            if(ctnQuantity.cscfga__Value__c !=null && ctnQuantity.cscfga__Value__c !=''){
                quantity = Decimal.valueOf(ctnQuantity.cscfga__Value__c);
            }
            
            if(recurringPriceOrigin.cscfga__Value__c !=null && recurringPriceOrigin.cscfga__Value__c !=''){
                singlePrice = Decimal.valueOf(recurringPriceOrigin.cscfga__Value__c);
            }
            
            if(singlePrice!=null && quantity!=null && netPromoRecurringVal!=null){
                Decimal recurringPriceDecimal = singlePrice*quantity*(1-(netPromoRecurringVal/100.0));
                recurringPrice.cscfga__Value__c = String.valueOf(recurringPriceDecimal);
                updates.add(setAttributeValueString(String.valueOf(recurringPriceDecimal), recurringPrice));
            }
        }
        return updates;
    }
    
    
    private static List<cscfga__Attribute__c> configureRullingNetProfit(Id configId, List<cscfga__Attribute__c> configAttributes){
        List<cscfga__Attribute__c> updates = new List<cscfga__Attribute__c>();
        
        updates.add(setAttributeValueString(Net_profit, getAttributeByName(Basket_qualification,configAttributes)));
        
        cscfga__Attribute__c netPromoOneOff = getAttributeByName(Network_Promo_OneOff, configAttributes);
        cscfga__Attribute__c netPromoRecurring = getAttributeByName(Network_Promo_Recurring, configAttributes);
        cscfga__Attribute__c ctnQuantity = getAttributeByName(CTN_quantity, configAttributes);
        cscfga__Attribute__c oneOffPrice = getAttributeByName(Network_Promo_OneOffPrice, configAttributes);
        cscfga__Attribute__c recurringPrice = getAttributeByName(Network_Promo_RecurringPrice, configAttributes);
        cscfga__Attribute__c oneOffPriceOrigin = getAttributeByName(OriginalOneOff, configAttributes);
        cscfga__Attribute__c recurringPriceOrigin = getAttributeByName(OriginalRecurring, configAttributes);
        
        Decimal quantity;
        Decimal singlePrice;
        if(ctnQuantity.cscfga__Value__c !=null && ctnQuantity.cscfga__Value__c !=''){
            quantity = Decimal.valueOf(ctnQuantity.cscfga__Value__c);
        }
        
        if(oneOffPriceOrigin.cscfga__Value__c !=null && oneOffPriceOrigin.cscfga__Value__c !=''){
            singlePrice = Decimal.valueOf(oneOffPriceOrigin.cscfga__Value__c);
        }
        
        if(singlePrice!=null && quantity!=null){
            Decimal oneOffPriceDecimal = singlePrice*quantity;
            oneOffPrice.cscfga__Value__c = String.valueOf(oneOffPriceDecimal);
            updates.add(setAttributeValueString(String.valueOf(oneOffPriceDecimal), oneOffPrice));
        }

        if(recurringPriceOrigin.cscfga__Value__c !=null && recurringPriceOrigin.cscfga__Value__c !=''){
            singlePrice = Decimal.valueOf(recurringPriceOrigin.cscfga__Value__c);
            if(singlePrice!=null && quantity!=null){
                Decimal recurringPriceDecimal = singlePrice*quantity;
                recurringPrice.cscfga__Value__c = String.valueOf(recurringPriceDecimal);
                updates.add(setAttributeValueString(String.valueOf(recurringPriceDecimal), recurringPrice));
            }
        }
        return updates;
    }
    
    private static void recalculateAll(Set<Id> basketIds){
        cscfga.ProductConfigurationBulkActions.calculateTotals(basketIds);
    }
    
    private static cscfga__Attribute__c setAttributeValueString(String value, cscfga__Attribute__c att){
        att.cscfga__Value__c = value;
        att.cscfga__Display_Value__c = value;
        if(att.cscfga__Is_Line_Item__c){
            att.cscfga__Price__c = Decimal.valueOf(value);
        }
        return att;
    }
    
    
    private static Map<Id, List<cscfga__Attribute__c>> getRelevantAttributes(Set<String> relevantAttributes, Set<Id> configIds){
        List<cscfga__Attribute__c> attfromConfigList= [SELECT cscfga__Value__c, cscfga__Display_Value__c, cscfga__Price__c,cscfga__Product_Configuration__c, Name,cscfga__Is_Line_Item__c from cscfga__Attribute__c where Name in :relevantAttributes and cscfga__Product_Configuration__c in :configIds];
        
        Map<Id, List<cscfga__Attribute__c>> attrPerConfigMap = new Map<Id, List<cscfga__Attribute__c>>();
        
        for(cscfga__Attribute__c att : attfromConfigList){
            if(attrPerConfigMap.get(att.cscfga__Product_Configuration__c)!=null){
                attrPerConfigMap.get(att.cscfga__Product_Configuration__c).add(att);
            }
            else{
                List<cscfga__Attribute__c> attList = new List<cscfga__Attribute__c>();
                attList.add(att);
                attrPerConfigMap.put(att.cscfga__Product_Configuration__c, attList);
            }
        }
        return attrPerConfigMap;
    }
    
    private static Map<Id, cscfga__Product_Configuration__c> getRelevantConfigurations(Set<Id> basketId, Id productDefId){
        Map<Id, cscfga__Product_Configuration__c> pcMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id,cscfga__Product_Basket__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c in :basketId and cscfga__Product_Definition__c = :productDefId]);
        return pcMap;
    }
}