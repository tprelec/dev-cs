@isTest
public class CS_NetworkPromoHandlerTest {
     private static testMethod void test() {
          List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
         System.runAs (simpleUser) { 
            CS_NetworkPromoHandler co = new CS_NetworkPromoHandler();
            
            Framework__c frameworkSetting = new Framework__c();
              frameworkSetting.Framework_Sequence_Number__c = 2;
              insert frameworkSetting;
              
              PriceReset__c priceResetSetting = new PriceReset__c();
               
                priceResetSetting.MaxRecurringPrice__c = 200.00;
                priceResetSetting.ConfigurationName__c = 'IP Pin';
                 
             insert priceResetSetting;
              Account testAccount = CS_DataTest.createAccount('Test Account');
              insert testAccount;
            
            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;
            
            
            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;
          
          
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            basket.Basket_qualification__c = 'SAC SRC';
            insert basket;
            
            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
           
            cscfga__Product_Definition__c addonDef = CS_DataTest.createProductDefinition('Mobile CTN addons');
            addonDef.Product_Type__c = 'Mobile';
            addonDef.RecordTypeId = productDefinitionRecordType;
            insert addonDef;
            
            cscfga__Product_Basket__c basketNP = CS_DataTest.createProductBasket(testOpp, 'Basket NP');
            basketNP.Basket_qualification__c = 'Net profit';
            insert basketNP;
            
            cscfga__Product_Configuration__c addonConf = CS_DataTest.createProductConfiguration(addonDef.Id, 'Mobile CTN addons',basketNP.Id);
            addonConf.cscfga__Product_Basket__c = basketNP.Id;
            addonConf.cscfga__Product_Definition__c = addonDef.Id;
            insert addonConf;
            
            List<cscfga__Product_Basket__c> sagBasket = new List<cscfga__Product_Basket__c>();
            List<cscfga__Product_Basket__c> netProfitBasket = new List<cscfga__Product_Basket__c>();
            
            sagBasket.add(basket);
            
           
            
            netProfitBasket.add(basketNP);
            
            //List<cscfga__Attribute__c> attfromConfigList= [SELECT cscfga__Value__c, cscfga__Display_Value__c, cscfga__Price__c,cscfga__Product_Configuration__c, Name,cscfga__Is_Line_Item__c from cscfga__Attribute__c where Name in :relevantAttributes and cscfga__Product_Configuration__c in :configIds];
            
            //Basket qualification
            cscfga__Attribute_Definition__c basketQAttDef = new cscfga__Attribute_Definition__c();
            basketQAttDef.cscfga__Type__c = 'Display Value';
            basketQAttDef.cscfga__Data_Type__c = 'String';
            basketQAttDef.Name = 'Basket qualification';
            basketQAttDef.cscfga__Product_Definition__c = addonDef.Id;
            insert basketQAttDef;
            
            cscfga__Attribute__c basketQAtt = new cscfga__Attribute__c();
            basketQAtt.Name = 'Basket qualification';
            basketQAtt.cscfga__Value__c = 'SAC SRC';
            basketQAtt.cscfga__Attribute_Definition__c = basketQAttDef.Id;
            basketQAtt.cscfga__Display_Value__c = 'SAC SRC';
            basketQAtt.cscfga__Price__c = null;
            basketQAtt.cscfga__Is_Line_Item__c = false;
            basketQAtt.cscfga__Product_Configuration__c = addonConf.Id;
            insert basketQAtt;
            
            
            //CTN_quantity
            cscfga__Attribute_Definition__c ctnQAttDef = new cscfga__Attribute_Definition__c();
            ctnQAttDef.cscfga__Type__c = 'User Input';
            ctnQAttDef.cscfga__Data_Type__c = 'Integer';
            ctnQAttDef.Name = 'CTN quantity';
            ctnQAttDef.cscfga__Product_Definition__c = addonDef.Id;
            insert ctnQAttDef;
            
            cscfga__Attribute__c ctnQAttConf = new cscfga__Attribute__c();
            ctnQAttConf.Name = 'CTN quantity';
            ctnQAttConf.cscfga__Attribute_Definition__c = ctnQAttDef.Id;
            ctnQAttConf.cscfga__Value__c = '1';
            ctnQAttConf.cscfga__Display_Value__c = '1';
            ctnQAttConf.cscfga__Price__c = null;
            ctnQAttConf.cscfga__Is_Line_Item__c = false;
            ctnQAttConf.cscfga__Product_Configuration__c = addonConf.Id;
            insert ctnQAttConf;
            
            //netPromoOneOffConf
            cscfga__Attribute_Definition__c netPromoOneOffDef = new cscfga__Attribute_Definition__c();
            netPromoOneOffDef.cscfga__Type__c = 'User Input';
            netPromoOneOffDef.cscfga__Data_Type__c = 'Decimal';
            netPromoOneOffDef.Name = 'Network Promo OneOff';
            netPromoOneOffDef.cscfga__Product_Definition__c = addonDef.Id;
            insert netPromoOneOffDef;
            
            cscfga__Attribute__c netPromoOneOffConf = new cscfga__Attribute__c();
            netPromoOneOffConf.Name = 'Network Promo OneOff';
            netPromoOneOffConf.cscfga__Attribute_Definition__c = netPromoOneOffDef.Id;
            netPromoOneOffConf.cscfga__Value__c = '1';
            netPromoOneOffConf.cscfga__Display_Value__c = '1';
            netPromoOneOffConf.cscfga__Price__c = null;
            netPromoOneOffConf.cscfga__Is_Line_Item__c = false;
            netPromoOneOffConf.cscfga__Product_Configuration__c = addonConf.Id;
            insert netPromoOneOffConf;
            
            //Network_Promo_Recurring
            cscfga__Attribute_Definition__c netPromoRecDef = new cscfga__Attribute_Definition__c();
            netPromoRecDef.cscfga__Type__c = 'User Input';
            netPromoRecDef.cscfga__Data_Type__c = 'Decimal';
            netPromoRecDef.Name = 'Network Promo Recurring';
            netPromoRecDef.cscfga__Product_Definition__c = addonDef.Id;
            insert netPromoRecDef;
            
            cscfga__Attribute__c netPromoRecConf = new cscfga__Attribute__c();
            netPromoRecConf.cscfga__Attribute_Definition__c = netPromoRecDef.Id;
            netPromoRecConf.Name = 'Network Promo Recurring';
            netPromoRecConf.cscfga__Value__c = '1';
            netPromoRecConf.cscfga__Display_Value__c = '1';
            netPromoRecConf.cscfga__Price__c = null;
            netPromoRecConf.cscfga__Is_Line_Item__c = false;
            netPromoRecConf.cscfga__Product_Configuration__c = addonConf.Id;
            insert netPromoRecConf;
            
            //Network_Promo_RecurringPrice
            cscfga__Attribute_Definition__c netPromoRecPriceDef = new cscfga__Attribute_Definition__c();
            netPromoRecPriceDef.cscfga__Type__c = 'User Input';
            netPromoRecPriceDef.cscfga__Data_Type__c = 'Decimal';
            netPromoRecPriceDef.Name = 'Recurring price';
            netPromoRecPriceDef.cscfga__Product_Definition__c = addonDef.Id;
            insert netPromoRecPriceDef;
            
            cscfga__Attribute__c netPromoRecPriceConf = new cscfga__Attribute__c();
            netPromoRecPriceConf.cscfga__Attribute_Definition__c = netPromoRecPriceDef.Id;
            netPromoRecPriceConf.Name = 'Recurring price';
            netPromoRecPriceConf.cscfga__Value__c = '1';
            netPromoRecPriceConf.cscfga__Display_Value__c = '1';
            netPromoRecPriceConf.cscfga__Price__c = 5;
            netPromoRecPriceConf.cscfga__Is_Line_Item__c = true;
            netPromoRecPriceConf.cscfga__Product_Configuration__c = addonConf.Id;
            insert netPromoRecPriceConf;
            
            //Network_Promo_OneOffPrice
            cscfga__Attribute_Definition__c netPromoOneOffPriceDef = new cscfga__Attribute_Definition__c();
            netPromoOneOffPriceDef.cscfga__Type__c = 'User Input';
            netPromoOneOffPriceDef.cscfga__Data_Type__c = 'Decimal';
            netPromoOneOffPriceDef.Name = 'One off price';
            netPromoOneOffPriceDef.cscfga__Product_Definition__c = addonDef.Id;
            insert netPromoOneOffPriceDef;
            
            cscfga__Attribute__c netPromoOneOffPriceConf = new cscfga__Attribute__c();
            netPromoOneOffPriceConf.cscfga__Attribute_Definition__c = netPromoOneOffPriceDef.Id;
            netPromoOneOffPriceConf.Name = 'One off price';
            netPromoOneOffPriceConf.cscfga__Value__c = '1';
            netPromoOneOffPriceConf.cscfga__Display_Value__c = '1';
            netPromoOneOffPriceConf.cscfga__Price__c = 10;
            netPromoOneOffPriceConf.cscfga__Is_Line_Item__c = false;
            netPromoOneOffPriceConf.cscfga__Product_Configuration__c = addonConf.Id;
            insert netPromoOneOffPriceConf;
            
            
            //OriginalOneOff
            cscfga__Attribute_Definition__c oriOneOffDef = new cscfga__Attribute_Definition__c();
            oriOneOffDef.cscfga__Type__c = 'User Input';
            oriOneOffDef.cscfga__Data_Type__c = 'Decimal';
            oriOneOffDef.Name = 'PriceItemOriginalOneOff';
            oriOneOffDef.cscfga__Product_Definition__c = addonDef.Id;
            insert oriOneOffDef;
            
            cscfga__Attribute__c oriOneOffConf = new cscfga__Attribute__c();
            oriOneOffConf.cscfga__Attribute_Definition__c = oriOneOffDef.Id;
            oriOneOffConf.Name = 'PriceItemOriginalOneOff';
            oriOneOffConf.cscfga__Value__c = '1';
            oriOneOffConf.cscfga__Display_Value__c = '1';
            oriOneOffConf.cscfga__Price__c = 15;
            oriOneOffConf.cscfga__Is_Line_Item__c = false;
            oriOneOffConf.cscfga__Product_Configuration__c = addonConf.Id;
            insert oriOneOffConf;
            
            
            //OriginalOneOff
            cscfga__Attribute_Definition__c oriRecDef = new cscfga__Attribute_Definition__c();
            oriRecDef.cscfga__Type__c = 'User Input';
            oriRecDef.cscfga__Data_Type__c = 'Decimal';
            oriRecDef.Name = 'PriceItemOriginalRecurring';
            oriRecDef.cscfga__Product_Definition__c = addonDef.Id;
            insert oriRecDef;
            
            cscfga__Attribute__c oriRecConf = new cscfga__Attribute__c();
            oriRecConf.cscfga__Attribute_Definition__c = oriRecDef.Id;
            oriRecConf.Name = 'PriceItemOriginalRecurring';
            oriRecConf.cscfga__Value__c = '1';
            oriRecConf.cscfga__Display_Value__c = '1';
            oriRecConf.cscfga__Price__c = 10;
            oriRecConf.cscfga__Is_Line_Item__c = false;
            oriRecConf.cscfga__Product_Configuration__c = addonConf.Id;
            insert oriRecConf;
            
            
            Map<Id, cscfga__Product_Basket__c> mapOld = new Map<Id,cscfga__Product_Basket__c>();
            mapOld.put(basketNP.Id, basket);
            
            CS_NetworkPromoHandler cnh = new CS_NetworkPromoHandler();
            CS_NetworkPromoHandler.updateAndRecalculate(netProfitBasket,mapOld);
            
            netProfitBasket[0].Basket_qualification__c = 'SAC SRC';
            basket.Basket_qualification__c = 'Net profit';
            mapOld.put(basketNP.Id, basket);
            
            CS_NetworkPromoHandler.updateAndRecalculate(netProfitBasket,mapOld);
            
           
         }
     }
}