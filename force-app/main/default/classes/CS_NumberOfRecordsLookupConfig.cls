public class CS_NumberOfRecordsLookupConfig extends cscfga.ALookupSearch {

	 public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){    
        
        String vendor = searchFields.get('Vendor__c');
        String accessType = searchFields.get('Access_Type__c');
        Decimal bandwidthDown = Decimal.valueOf(searchFields.get('Available_bandwidth_down__c'));
        Decimal bandwidthUp = Decimal.valueOf(searchFields.get('Available_bandwidth_up__c'));
        String overbooking = searchFields.get('Overbooking__c');
        String service = searchFields.get('Service__c');
        Boolean infraSLA = Boolean.valueOf(searchFields.get('Infra_SLA__c'));
        String contractTerm = searchFields.get('cspmb__Contract_Term__c');
        
        System.debug('vendor: ' + vendor);
        System.debug('accessType: ' + accessType);
        System.debug('bandwidthDown: ' + bandwidthDown);
        System.debug('bandwidthUp: ' + bandwidthUp);
        System.debug('overbooking: ' + overbooking);
        System.debug('service: ' + service);
        System.debug('infraSLA: ' + infraSLA);
        System.debug('contractTerm: ' + contractTerm);
        
        /*List<cspmb__Price_Item__c> priceItemList = [SELECT Id, Name
                                                FROM cspmb__Price_Item__c
                                                WHERE Vendor__c = :vendor AND Access_Type__c = :accessType AND Available_bandwidth_down__c = :bandwidthDown AND Available_bandwidth_up__c =:bandwidthUp 
                                                AND Overbooking__c = :overbooking AND Service__c = :service AND Infra_SLA__c = :infraSLA AND cspmb__Contract_Term__c = :contractTerm]; */
        
        
        return null;
    }

	public override String getRequiredAttributes(){ 
	    return '[]';
	}
	
}