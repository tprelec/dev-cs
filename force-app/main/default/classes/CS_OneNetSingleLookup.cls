global with sharing class CS_OneNetSingleLookup extends cscfga.ALookupSearch{

public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit){

    final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1; 
    Integer recordOffset = pageOffset * pageLimit;
    
    
    String scenario = searchFields.get('Scenario');
    
    Set<String> flexCCodes = new Set<String>();
    flexCCodes.add('C108749');
    flexCCodes.add('C107973');
   
    
    Set<String> mobileCCodes = new Set<String>();
    mobileCCodes.add('C108751');

    
    
    String oneNetParent = scenario == 'One Net Express' ? 'One_Net_Express_addon_parent' : 'One_Net_Enterprise_addon_parent';
    Integer duration = (searchFields.get('Contract Duration') == null ||(searchFields.get('Contract Duration') == '')) ? 0 : Integer.valueOf(searchFields.get('Contract Duration'));
    String dealType = searchFields.get('Deal Type');
    
    List<cspmb__Price_Item_Add_On_Price_Item_Association__c> addonsOfMain = [SELECT Id, cspmb__Add_On_Price_Item__c,cspmb__Price_Item__r.cspmb__Price_Item_Code__c
                         FROM cspmb__Price_Item_Add_On_Price_Item_Association__c
                         WHERE cspmb__Price_Item__r.cspmb__Price_Item_Code__c = :oneNetParent];
                         
    Set<Id> addonsOfParent = new Set<Id>();
    
    for(cspmb__Price_Item_Add_On_Price_Item_Association__c addItm : addonsOfMain){
        addonsOfParent.add(addItm.cspmb__Add_On_Price_Item__c);
    }
    
    List<cspmb__Add_On_Price_Item__c> addonItems = [SELECT Id, Name, Discount_allowed__c,cspmb__Recurring_Charge__c,cspmb__One_Off_Charge__c, cspmb__Is_Active__c,Min_Duration__c,Max_Duration__c,cspmb__Recurring_Charge_External_Id__c,cspmb__One_Off_Charge_External_Id__c
                         FROM cspmb__Add_On_Price_Item__c
                         WHERE Min_Duration__c <= :duration AND Max_Duration__c > :duration and Id in :addonsOfParent 
                         AND ( cspmb__Recurring_Charge_External_Id__c in :flexCCodes OR cspmb__Recurring_Charge_External_Id__c in :mobileCCodes or cspmb__One_Off_Charge_External_Id__c in :flexCCodes OR cspmb__One_Off_Charge_External_Id__c in :mobileCCodes)LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
                         
    
    System.debug('Addon Items = '+addonItems);
    
    return addonItems;
    
}

  public override String getRequiredAttributes(){ 
  
      return '["Scenario", "Contract Duration", "Deal Type"]';
      
  } 

}