public without sharing class CS_OppDocController {
    private static List<CS_DocItem> returnList = new List<CS_DocItem>();
	private static final String VAL_SEPARATOR = '|';
	private static final String DEFAULT_LANGUAGE = 'Dutch';
    
    @AuraEnabled
    public static List<CS_DocItem> getDocuments(Id objId) {
        String oppId = objId;
        if (objId == null) {
            throw new CS_OppDocControllerException('No Object Id is defined');
        }
        String sobjectType = String.valueOf(objId.getsobjecttype());
        if (sobjectType == 'Opportunity') {
            oppId = objId;
        }
        if (sobjectType == 'VF_Contract__c') {
            oppId = [SELECT Opportunity__c FROM VF_Contract__c WHERE Id = :objId].Opportunity__c;
            if (oppId == null) {
                return returnList; //W-3998: throw error and handle it in component
            }
        }
        Set<Id> agreementSet = new Set<Id>();
        Set<Id> applicableContractConditions = new Set<Id>();

        try {
            Opportunity opp = [
                SELECT
                    Id,
                    CloseDate,
                    Direct_Indirect__c,
                    Primary_Basket__c,
                    (SELECT Id, Proposition__c, Category__c, Product2Id FROM OpportunityLineItems)
                FROM Opportunity
                WHERE Id = :oppId
            ];
            Date validDateForContractConditions = opp.CloseDate != null ? opp.CloseDate : Date.today();
            String directIndrect = String.isNotEmpty(opp.Direct_Indirect__c) ? opp.Direct_Indirect__c : '';
            // W-3730/W-3954
			Id primaryBasket = opp.Primary_Basket__c;
            String docLanguage = DEFAULT_LANGUAGE;
            if(primaryBasket != null) {
                docLanguage = [SELECT Contract_Language__c FROM cscfga__Product_Basket__c WHERE Id = :primaryBasket].Contract_Language__c;
            }
                
            Set<String> propSet = new Set<String>();
            Set<String> categorySetSet = new Set<String>();
            Set<Id> productSet = new Set<Id>();
            for (OpportunityLineItem opli : opp.OpportunityLineItems) {
                if (String.isNotEmpty(opli.Proposition__c)) {
                    propSet.add(opli.Proposition__c);
                }
                if (String.isNotEmpty(opli.Category__c)) {
                    categorySetSet.add(opli.Category__c);
                }
                productSet.add(opli.Product2Id);
            }
            Set<Id> applibleContractConditionsFromProductAssociations = getContractConditionsFromProducts(productSet);

            List<csclm__Agreement__c> agreementList = [
                SELECT Id
                FROM csclm__Agreement__c
                WHERE csclm__Opportunity__c = :oppId
            ];
            for (csclm__Agreement__c agree : agreementList) {
                agreementSet.add(agree.Id);
            }
            if (!agreementSet.isEmpty()) {
                List<Attachment> attachmentList = [
                    SELECT Id, Name, CreatedDate
                    FROM Attachment
                    WHERE ParentId IN :agreementSet
                    ORDER BY CreatedDate DESC
                ];
                for (Attachment attach : attachmentList) {
                    CS_DocItem di = new CS_DocItem();
                    di.name = attach.Name;
                    di.isServiceDescription = false;
                    di.isTariffDescription = false;
                    di.isGeneralCondition = false;
                    di.docId = attach.Id;
                    returnList.add(di);
                }
            }

            List<CS_Basket_Snapshot_Transactional__c> snapShotList = [
                SELECT Id, cs_Proposition_Scenario_Matrix__c 
                FROM CS_Basket_Snapshot_Transactional__c
                WHERE Product_Basket__r.cscfga__Opportunity__c = :oppId AND Product_Basket__r.Primary__c = TRUE
            ];

            List<Contract_Conditions__c> ccList = [
                SELECT Name, Channel__c, Proposition1__c, Type__c, URL_Link__c, Version_Number__c, Category__c, Proposition__c, Language__c
                FROM Contract_Conditions__c
                WHERE
                    Start_Date__c <= :validDateForContractConditions
                    AND End_Date__c >= :validDateForContractConditions
                    AND (Channel__c = 'Direct/Indirect'
                    OR Channel__c = :directIndrect)
            ];

            // W-3730/W-3954 TEMPORARY PATCH ...
            Map<String, String> ccCheckInMap = new Map<String, String>();
            Map<Id, CS_DocItem> docItemsMap = new Map<Id, CS_DocItem>();
            //
            for (Contract_Conditions__c cc : ccList) {
                Boolean addRecord = false;
                if (cc.Type__c == 'AVW') {
                    if (String.isEmpty(cc.Proposition1__c)) {
                        addRecord = true;
                    } else if (propSet.contains(cc.Proposition1__c)) {
                        addRecord = true;
                    }
                }

                if (propSet.contains(cc.Proposition1__c)) {
                    /** Category check */
                    if (String.isEmpty(cc.Category__c) || categorySetSet.contains(cc.Category__c)) {
                        addRecord = true;
                    }
                }

                if (applibleContractConditionsFromProductAssociations.contains(cc.Id)) {
                    addRecord = true;
                }

                for (CS_Basket_Snapshot_Transactional__c snap : snapShotList) {
                    if (
                        !String.isEmpty(snap.cs_Proposition_Scenario_Matrix__c) && !String.isEmpty(cc.Proposition1__c)
                    ) {
                        String matrix = snap.cs_Proposition_Scenario_Matrix__c.toLowerCase().deleteWhitespace();
                        String prop = cc.Proposition1__c.toLowerCase().deleteWhitespace();
                        if (matrix.contains(prop)) {
                            addRecord = true;
                        }
                    }
                }
                
                // W-3730/W-3954 TEMPORARY PATCH ...
                // If the current document is on the map ...
                // // The saved one already has the language set? Don't add it nor replace it.
                // // The saved one doesn't has the language set?
                // // // The actual record has the language set ? Replace it (it implies to be deleted from returnList)
                // 
                // Else
                // // The actual record has the Dutch or the language set? Add it.
                String mapKey = cc.Type__c + VAL_SEPARATOR + cc.Category__c + VAL_SEPARATOR + cc.Proposition__c + VAL_SEPARATOR + cc.Proposition1__c;
                String mapItemValue = cc.Language__c + VAL_SEPARATOR + cc.Id;
                if (!(cc.Language__c == DEFAULT_LANGUAGE || cc.Language__c == docLanguage || String.isBlank(cc.Language__c)))  {
                    addRecord = false;
                } else {
                    if (ccCheckInMap.containsKey(mapKey)) {
                        String mapValues = ccCheckInMap.get(mapKey);
                        List<String> keyInList = mapValues.split('\\' + VAL_SEPARATOR); 
                        if(keyInList[0] == docLanguage) { 
                            addRecord = false;
                        } else if (cc.Language__c == docLanguage) {
                            // Replace it (it implies to be deleted from returnList)
                            Id oldDocId = Id.valueOf(keyInList[1]);
                            docItemsMap.remove(oldDocId);
                            applicableContractConditions.remove(oldDocId);
                            ccCheckInMap.put(mapKey, mapItemValue);
                        }
                    } else if (addRecord == true) { 
                        ccCheckInMap.put(mapKey, mapItemValue);
                    }
                }

                if (addRecord) {
                    applicableContractConditions.add(cc.Id);
                    CS_DocItem di = new CS_DocItem();
                    di.contractConditionId = cc.Id;
                    di.isTariffDescription = false;
                    di.isServiceDescription = false;
                    di.isGeneralCondition = false;
                    di.name = cc.Name;
                    switch on cc.Type__c {
                        when 'AVW' {
                            di.isGeneralCondition = true;
                        }
                        when 'Tarievenoverzicht' {
                            di.isTariffDescription = true;
                        }
                        when 'Dienstbeschrijving' {
                            di.isServiceDescription = true;
                        }
                        when else {
                            di.isServiceDescription = true;
                        }
                    }
                    di.serviceDescriptionUrl = cc.URL_Link__c;
                    docItemsMap.put(cc.Id, di);
                }
            }
           returnList.addAll(docItemsMap.values());
            // END ... W-3730/W-3954
            linkDocumentsToDocumentItems(applicableContractConditions);

        } catch (Exception e) {
            System.debug(LoggingLevel.DEBUG, 'Exception: ' + e.getMessage() + e.getStackTraceString());
        }
        return returnList;
    }

    @testVisible
    private static Set<Id> getContractConditionsFromProducts(Set<Id> products) {
        List<Contract_Condition_Product_Association__c> contractConditionProductAssociations = [
            SELECT Contract_Conditions__c
            FROM Contract_Condition_Product_Association__c
            WHERE Product2__c IN :products
        ];

        Set<Id> applicableContractConditions = new Set<Id>();
        for (Contract_Condition_Product_Association__c junctionObject : contractConditionProductAssociations) {
            applicableContractConditions.add(junctionObject.Contract_Conditions__c);
        }

        return applicableContractConditions;
    }

    @testVisible
    private static void linkDocumentsToDocumentItems(Set<Id> applicableContractConditions) {
        Map<Id, List<Id>> applicableContractConditionsToContentVersions = new Map<Id, List<Id>>();
        for (ContentDocumentLink cdl : [
            SELECT LinkedEntityId, ContentDocument.LatestPublishedVersionId, ContentDocument.FileExtension
            FROM ContentDocumentLink
            WHERE LinkedEntityId IN :applicableContractConditions AND ContentDocument.FileExtension = 'pdf'
        ]) {
            if (!applicableContractConditionsToContentVersions.containsKey(cdl.LinkedEntityId)) {
                applicableContractConditionsToContentVersions.put(
                    cdl.LinkedEntityId,
                    new List<Id>{ cdl.ContentDocument.LatestPublishedVersionId }
                );
            } else {
                applicableContractConditionsToContentVersions.get(cdl.LinkedEntityId)
                    .add(cdl.ContentDocument.LatestPublishedVersionId);
            }
        }

        List<CS_DocItem> toAdd = new List<CS_DocItem>();
        for (CS_DocItem docItem : returnList) {
            if (docItem.contractConditionId == null) {
                continue;
            }
            List<Id> contentVersionsToApply = applicableContractConditionsToContentVersions.get(
                docItem.contractConditionId
            );

            if (contentVersionsToApply == null || contentVersionsToApply.isEmpty()) {
                docItem.serviceDescriptionUrl = ''; // TODO: W-3998: throw error and handle it in component
                continue;
            }
            if (contentVersionsToApply.size() == 1) {
                docItem.serviceDescriptionUrl = createContentVersionDownloadLink(contentVersionsToApply[0]);
                continue;
            }
            if (contentVersionsToApply.size() > 1) {
                docItem.serviceDescriptionUrl = createContentVersionDownloadLink(contentVersionsToApply[0]);
                contentVersionsToApply.remove(0);
                for (Id contentVersionId : contentVersionsToApply) {
                    CS_DocItem clonedDocItem = new CS_DocItem(docItem);
                    clonedDocItem.serviceDescriptionUrl = createContentVersionDownloadLink(contentVersionId);
                    toAdd.add(clonedDocItem);
                }
            }
        }
        returnList.addAll(toAdd);
    }

    @testVisible
    private static String createContentVersionDownloadLink(Id contentVersion) {
        String baseURL = System.URL.getSalesforcebaseUrl().toExternalForm();
        String pathToContentVersionDownload =
            '/sfc/servlet.shepherd/version/download/' +
            contentVersion +
            '?operationContext=S1';
        if (!GeneralUtils.currentUserIsPartnerUser()) {
            return baseURL + pathToContentVersionDownload;
        }
        String pathFromCurrentURL = System.URL.getCurrentRequestUrl().getPath();
        String communityName = String.isNotBlank(pathFromCurrentURL) ? '/' + pathFromCurrentURL.split('/')[1] : '';
        return baseURL + communityName + pathToContentVersionDownload;
    }

    public class CS_OppDocControllerException extends Exception {
    }
}