global with sharing class CS_OrderGenerationObserverClass implements csordcb.ObserverApi.IObserver {

    private static String mainConfigurationJsonId = '0000';
    
    global void execute(csordcb.ObserverApi.Observable observableInst, Object arg) {
        csordtelcoa.OrderGenerationObservable observable = (csordtelcoa.OrderGenerationObservable)observableInst;
        siteAndPbxDecomposition(observable.getServiceIds());
    }
    
    /**
     * Example structure of JSON's:
     * clonedSiteIds: {"0000":"a069E000004iKXf", "0002":"a069E000004iKXZ","0001":""}
     * clonedPbxIds: {"0000":"null", "0001":"null","0001":"null"}
     */
    private List<CS_Site_Pbx_Result> getSitePbxResults(String clonedSiteIds, String clonedPbxIds) {
        List<CS_Site_Pbx_Result> results = new List<CS_Site_Pbx_Result>();
        
        JSONParser parserClonedSites = JSON.createParser(clonedSiteIds);

        while (parserClonedSites.nextToken() != null) {
            if(parserClonedSites.getText() != '{' && parserClonedSites.getText() != '}') {
                // Get key.
                String key = parserClonedSites.getText().trim();
                // Get next JSON token which should be value.
                parserClonedSites.nextToken();
                // Get value.
                String value = parserClonedSites.getText().trim();
                
                // We are ignoring main configuration and taking into account only clonned configurations.
                //if(key != mainConfigurationJsonId) {
                if(value != '') {
                    CS_Site_Pbx_Result result = new CS_Site_Pbx_Result(value);
                    JSONParser parserPbx = JSON.createParser(clonedPbxIds);
                    while (parserPbx.nextToken() != null) {
                        // Every SITE does not have to have PBX.
                        if(parserPbx.getText().trim() == key) {
                            parserPbx.nextToken();
                            if(parserPbx.getText().trim() != '') {
                                result.setPbxId(parserPbx.getText().trim());
                            }    
                        }
                    }
                    results.add(result);
                }
                //}
            } 
        }
        
        return results;
    }
    
    public void siteAndPbxDecomposition(List<Id> serviceIds) {
        List<csord__Service__c> services = [SELECT Id, CS_Site__c, CS_Pbx__c, csordtelcoa__Product_Configuration__c, csord__Subscription__r.csord__Identification__c, csord__Subscription__r.csordtelcoa__Product_Configuration__r.ClonedSiteIds__c FROM csord__Service__c WHERE Id IN :serviceIds AND csordtelcoa__Product_Configuration__r.Name = 'Access Infrastructure'];
        
        
        // If we have at least one service that is based on 'Access Infrastructure'
        if(services.size() > 0) {
            // Get the configuration on which the service is base.
            cscfga__Product_Configuration__c pc = [SELECT id, ClonedSiteIds__c, ClonedPBXIds__c FROM cscfga__Product_Configuration__c WHERE id = :services[0].csordtelcoa__Product_Configuration__c];
            // INIT stack.
            CS_Stack resultStack = new CS_Stack();
            
            // If field is not NULL.
            if(pc.ClonedSiteIds__c != null) {
                // For every site/pbx result, push to stack.
                for(CS_Site_Pbx_Result result : getSitePbxResults(pc.ClonedSiteIds__c, pc.ClonedPBXIds__c)) {
                    resultStack.push(result); 
                }
                // Update every CS_Site__c field on service.
                for (csord__Service__c service : services) {
                    while (!resultStack.isEmpty()) { 
                        CS_Site_Pbx_Result sitePbxResult = (CS_Site_Pbx_Result)resultStack.pop();
                        service.CS_Site__c = sitePbxResult.getSiteId();
                        service.CS_Pbx__c = sitePbxResult.getPbxId();
                        break;
                    }
                }
                update services;
            }
        }
    }
}