@isTest
public class CS_PBX_LookupTest {
 private static testMethod void testRequiredAttributes(){
        CS_PBX_Lookup caLookup = new CS_PBX_Lookup();
        caLookup.getRequiredAttributes();
    }
  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
     System.runAs (simpleUser) { 
      
     
      Framework__c frameworkSetting = new Framework__c();
      frameworkSetting.Framework_Sequence_Number__c = 2;
      insert frameworkSetting;
      
      PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
     insert priceResetSetting;
      Account testAccount = CS_DataTest.createAccount('Test Account');
      insert testAccount;
      
      Sales_Settings__c ssettings = new Sales_Settings__c();
      ssettings.Postalcode_check_validity_days__c = 2;
      ssettings.Max_Daily_Postalcode_Checks__c = 2;
      ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
      ssettings.Postalcode_check_block_period_days__c = 2;
      ssettings.Max_weekly_postalcode_checks__c = 15;
      insert ssettings;
      
      //createSite(String name, Account siteAccount, String postalCode, String street, String city, Decimal houseNumber)
      Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112',testAccount, '1111AA', 'Breestraat','LEIDEN', 112); 
      insert site;
      
      Site_Postal_Check__c spc = CS_DataTest.createSite(site);
      insert spc;
      
      Site__c site2 = CS_DataTest.createSite('Eindhoven, Esp 130',testAccount,'2222AA', 'Esp','Eindhoven', 130); 
      insert site2;
      
      Site_Postal_Check__c spc2 = CS_DataTest.createSite(site2);
      insert spc2;
       Site__c site3 = CS_DataTest.createSite('Eindhoven, Esp 123',testAccount,'2232AA', 'Esp','Eindhoven', 123); 
      insert site3;
      
      Site_Postal_Check__c spc3 = CS_DataTest.createSite(site3);
      insert spc3;
      Site_Availability__c siteAvailability = CS_DataTest.createSiteAvailability('LEIDEN, Breestraat 112', site,spc); 
      insert siteAvailability;
      
      
      
      Site_Availability__c siteAvailability2 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 130', site,spc2); 
      insert siteAvailability2;
      
      Site_Availability__c siteAvailability3 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 123', site,spc3);
      siteAvailability3.Access_Infrastructure__c = 'ADSL';
      insert siteAvailability3;
      
      
      Site_Availability__c siteAvailability4 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 123', site,spc3);
      siteAvailability4.Access_Infrastructure__c = 'EthernetOverFiber';
      insert siteAvailability4;
      
      
      Site_Availability__c siteAvailability5 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 123', site,spc3);
      siteAvailability5.Access_Infrastructure__c = 'Coax';
      insert siteAvailability5;
      
      List<Site_Postal_Check__c> spcs = [SELECT Id, Access_Active__c from Site_Postal_Check__c where Id = :spc.Id or Id = :spc2.Id or Id = :spc3.Id];
      
      Competitor_Asset__c ca = new Competitor_Asset__c();
      ca.RecordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByName().get('PABX').getRecordTypeId();
      ca.Account__c = testAccount.Id;
      ca.Site__c = site.Id;
      insert ca;
      
      Competitor_Asset__c ca2 = new Competitor_Asset__c();
      ca2.RecordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByName().get('PABX').getRecordTypeId();
      ca2.Site__c = site2.Id;
      ca2.Account__c = testAccount.Id;
      //ca2.Active_PBX__c = true;
      insert ca2;
      
      Competitor_Asset__c ca3 = new Competitor_Asset__c();
      ca3.RecordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByName().get('PABX').getRecordTypeId();
      ca3.Site__c = site2.Id;
      ca3.Account__c = testAccount.Id;
      //ca3.Active_PBX__c = true;
      insert ca3;
      
      
      Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
      insert testOpp;
      
      
      cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        insert basket;
        
       
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        Id packageDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Package Definition').getRecordTypeId(); 
        
        
        
        cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        accessInfraDef.Product_Type__c = 'Fixed';
        accessInfraDef.RecordTypeId = productDefinitionRecordType;
        insert accessInfraDef;
        
        
        
        cscfga__Product_Definition__c managedInternetDef = CS_DataTest.createProductDefinition('Managed Internet');
        managedInternetDef.Product_Type__c = 'Fixed';
        managedInternetDef.RecordTypeId = productDefinitionRecordType;
        insert managedInternetDef;
        
        cscfga__Product_Configuration__c managedInternetConf = CS_DataTest.createProductConfiguration(managedInternetDef.Id, 'Managed Internet',basket.Id);
        managedInternetConf.cscfga__Root_Configuration__c = null;
        managedInternetConf.cscfga__Parent_Configuration__c = null;
        insert managedInternetConf;
        
        cscfga__Product_Definition__c ipvpnDef = CS_DataTest.createProductDefinition('IPVPN');
        ipvpnDef.Product_Type__c = 'Fixed';
         ipvpnDef.RecordTypeId = productDefinitionRecordType;
        
        insert ipvpnDef;
        cscfga__Product_Configuration__c ipvpnConf = CS_DataTest.createProductConfiguration(ipvpnDef.Id, 'IPVPN',basket.Id);
        ipvpnConf.cscfga__Root_Configuration__c = null;
        ipvpnConf.cscfga__Parent_Configuration__c = null;
        
        insert ipvpnConf;
        
        cscfga__Product_Definition__c ipvpnPackageDef = CS_DataTest.createProductPackageDefinition('IPVPN package');
        ipvpnDef.RecordTypeId = packageDefinitionRecordType;
        ipvpnPackageDef.Product_Type__c = 'Fixed';
        insert ipvpnPackageDef;
        
        cscfga__Product_Configuration__c ipvpnPackageConf = CS_DataTest.createProductConfiguration(ipvpnPackageDef.Id, 'IPVPN package',basket.Id);
        insert ipvpnPackageConf;
        
        cscfga__Attribute_Definition__c accessPackageAttributeDef = new cscfga__Attribute_Definition__c();
        accessPackageAttributeDef.cscfga__Type__c = 'Package Slot';
        accessPackageAttributeDef.cscfga__Data_Type__c = 'String';
        accessPackageAttributeDef.Name = 'Access Infrastructure';
        accessPackageAttributeDef.cscfga__Product_Definition__c = ipvpnPackageDef.Id;
        insert accessPackageAttributeDef;
        
        cscfga__Attribute__c accessPackageAttribute = new cscfga__Attribute__c();
        accessPackageAttribute.Name = 'Access Infrastructure';
        accessPackageAttribute.cscfga__Product_Configuration__c = ipvpnPackageConf.Id;
        insert accessPackageAttribute;
        
        cscfga__Product_Configuration__c accessInfraConf = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        accessInfraConf.Site_Name__c = 'ESP 130';
        accessInfraConf.cscfga__Root_Configuration__c = null;
        accessInfraConf.cscfga__Parent_Configuration__c = null;
        accessInfraConf.cscfga__package_slot__c = accessPackageAttribute.Id;
        
        insert accessInfraConf;
        
       
        
        csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(ipvpnPackageConf);
        pcr.csbb__Product_Configuration__c = ipvpnPackageConf.Id;
        insert pcr;
        
        
        Map<String, String> searchFields = new Map<String, String>();
        
        
        CS_PBX_Lookup caLookup = new CS_PBX_Lookup();
        searchFields.put('Site Check SiteID',String.valueOf(siteAvailability2.Id));
        caLookup.doLookupSearch(searchFields, String.valueOf(accessInfraDef.Id),null, 0, 0);
        
     }
  }

}