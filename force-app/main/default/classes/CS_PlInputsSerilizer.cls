public with sharing class CS_PlInputsSerilizer {

    public static String getPlInputsSerialized(Set<Id> configs) {

        CS_PlInputs inputs = new CS_PlInputs();

        for (Orderform_Proposition__mdt prop : [SELECT MasterLabel, Flexibility_Margin__c FROM Orderform_Proposition__mdt]) {
            if (prop.Flexibility_Margin__c != null) {
                inputs.flexibilityMargins.put(prop.MasterLabel, prop.Flexibility_Margin__c);
            }
        }

        inputs.siteCountForConfig = CS_SiteUtility.getConfigurationSiteQuantity(configs);
        return JSON.Serialize(inputs);
    }

    public static String getPlInputsSerialized(Id basketId) {
        Set<Id> configs = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basketId]).keySet();
        return getPlInputsSerialized(configs);
    }

    public static CS_PlInputs getPlInputsFromSerializedString(String serializedValue) {
        if (serializedValue == null || serializedValue == '') {
            return null;
        }

        return (CS_PlInputs)JSON.deserialize(serializedValue, CS_PlInputs.class);
    }

}