@isTest
public with sharing class CS_PlInputsSerilizerTest {

    @isTest
    static void testSerialize() {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
            
        insert simpleUser;
        System.runAs (simpleUser) { 
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;
            
            Contact contact1 = new Contact(
                AccountId = testAccount.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'   
            );
            insert contact1;
            
            testAccount.Authorized_to_sign_1st__c = contact1.Id;
            update testAccount;
            
            Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', simpleUser.id);
            insert opp;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Test Basket');
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = testAccount.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 24;
            basket.Contract_duration_BMS__c = '24';
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 09, 22);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 09, 25);
            insert basket;

            cscfga__Product_Definition__c pd = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
            pd.Product_Type__c = 'Mobile';
            pd.Name = 'Mobile Test';
            insert pd;

            cscfga__Product_Configuration__c pc = CS_DataTest.createProductConfiguration(pd.Id, 'Mobile CTN Subscription',basket.Id);
            pc.cscfga__Contract_Term__c = 24;
            pc.cscfga__total_one_off_charge__c = 100;
            pc.OneOff_Product_List_Price__c = 100.00;
            pc.cscfga__Total_Price__c = 20;
            pc.cspl__Type__c = 'Mobile CTN profile';
            pc.Proposition__c = 'Proposition';
            pc.cscfga__Contract_Term_Period__c = 12;
            pc.cscfga__Configuration_Status__c = 'Valid';
            pc.cscfga__Quantity__c = 10;
            pc.cscfga__total_recurring_charge__c = 22;
            pc.Recurring_Product_List_Price__c = 10;
            pc.RC_Cost__c = 0;
            pc.NRC_Cost__c = 0;
            pc.cscfga__one_off_charge_product_discount_value__c = 0;
            pc.cscfga__recurring_charge_product_discount_value__c = 0;
            pc.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
            insert pc;

            String result = CS_PlInputsSerilizer.getPlInputsSerialized(basket.Id);
            System.assertNotEquals(null, result);
        }
    }

    @isTest
    static void testDeserialize() {
        String serializedInput = '{"siteCountForConfig":{"a7Z1X0000009iHoUAI":3,"a7Z1X0000009iHnUAI":3,"a7Z1X0000009iHmUAI":3,"a7Z1X0000009iHlUAI":3,"a7Z1X0000009iHiUAI":3,"a7Z1X0000009iHhUAI":3},"flexibilityMargins":{"Flex Mobile":50.0}}';
        CS_PlInputs inputs = CS_PlInputsSerilizer.getPlInputsFromSerializedString(serializedInput);
        System.assertNotEquals(null, inputs.flexibilityMargins);
    }
}