public with sharing class CS_PollApexClassController {
	public CS_PollApexClassController() {
		
	}

	/*
	get batch job status
	*/
	@AuraEnabled
	public static String getJobStatus(Id jobId){
		List<AsyncApexJob> batchJobList = new List<AsyncApexJob>([SELECT id, CreatedDate, CreatedById, JobType, ApexClassId, Status, 
				JobItemsProcessed, TotalJobItems, NumberOfErrors, CompletedDate, MethodName, 
				ExtendedStatus, ParentJobId, LastProcessed, LastProcessedOffset 
			FROM AsyncApexJob WHERE Id = :jobId]);

		return batchJobList[0].Status;
	}

	@AuraEnabled
	public static String getSomeBasketData(Id basketId){
		List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>([SELECT id, Name, cscfga__Basket_Status__c FROM cscfga__Product_Basket__c WHERE Id = :basketId]);

		return basketList[0].cscfga__Basket_Status__c;
	}
}