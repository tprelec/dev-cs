public class CS_ProductConfigurationController {

    private final List<cscfga__Product_Configuration__c> configList;
    public String productDefinition {get;set;}
    public Integer numberOfUpdates {get;set;}

    public CS_ProductConfigurationController() {
    }
    
    public List<SelectOption> getProductDefinitionItems() {
        
        List<cscfga__Product_Definition__c> productDefinitionsList = [SELECT Name FROM cscfga__Product_Definition__c];
        
        List<SelectOption> options = new List<SelectOption>();
        for(cscfga__Product_Definition__c pd : productDefinitionsList) {
            options.add(new SelectOption(pd.Name, pd.Name));    
        }
        
        return options;
    }

    public List<cscfga__Product_Configuration__c> getProductConfigurationList() {
        return configList;
    }
    
    public PageReference testAllOffers() {
        numberOfUpdates = CS_ProductConfigurationUpgrader.testConfigurationsOffers();
        return null;
    }
    
    public PageReference upgradeAllOffersSync() {
        CS_ProductConfigurationUpgrader.upgradeOffers();
        return null;
    }
    
    public PageReference upgradeAllOffersAsync() {
        CS_ProductConfigurationUpgrader.upgradeOffersFuture();
        return null;
    }
    
    public PageReference testAllOpenBaskets() {
        numberOfUpdates = CS_ProductConfigurationUpgrader.testConfigurationsOpenBaskets();
        return null;
    }
    
    public PageReference upgradeOpenBasketsSync() {
        CS_ProductConfigurationUpgrader.upgradeOpenBaskets();
        return null;
    }
    
    public PageReference upgradeOpenBasketsAsync() {
        CS_ProductConfigurationUpgrader.upgradeOpenBasketsFuture();
        return null;
    }
    
    public PageReference testOffersForPD() {
        if(productDefinition != null) {
            numberOfUpdates = CS_ProductConfigurationUpgrader.testConfigurationsPD(productDefinition);
        }
        return null;
    }
    
    public PageReference upgradeOffersForPDSync() {
        if(productDefinition != null) {
            CS_ProductConfigurationUpgrader.upgradeOffersForSpecificPd(productDefinition);
        }
        return null;
    }
    
    public PageReference upgradeOffersForPDAsync() {
        if(productDefinition != null) {
            CS_ProductConfigurationUpgrader.upgradeOffersForSpecificPdFuture(productDefinition);
        }
        return null;
    }
}