public with sharing class CS_ProductConfigurationUpgrader {
    
    @future
    public static void upgradeOffersFuture() {
        upgradeOffers();
    }
    
    // Upgrades all offers -> risk of hitting governor limits if there are too many configs in the offer.
	public static void upgradeOffers() {
		List<cscfga__Product_Configuration__c> configList = [
			select id, cscfga__Product_Definition__r.cscfga__IsArchived__c, cscfga__product_definition_iteration__c, cscfga__product_definition__r.cscfga__iteration__c
			from cscfga__Product_Configuration__c
			where cscfga__Configuration_Offer__c != null 
			and cscfga__Configuration_Offer__r.cscfga__Active__c = true
			// and UPC_Eligible_for_Upgrade__c = true
			and cscfga__Parent_Configuration__c = null
		];

		upgradeConfigurations(configList);
	}
	
	@future
    public static void upgradeOffersForSpecificPdFuture(String pdName) {
        upgradeOffersForSpecificPd(pdName);
    }

	// Upgrade only offers belonging to specific PD. Use if upgradeOffers hits limits.
	public static void upgradeOffersForSpecificPd(String pdName) {
		String queryPdName = '%' + pdName + '%';
		List<cscfga__Product_Configuration__c> configList = [
			select id, cscfga__Product_Definition__r.cscfga__IsArchived__c, cscfga__product_definition_iteration__c, cscfga__product_definition__r.cscfga__iteration__c
			from cscfga__Product_Configuration__c
			where cscfga__Configuration_Offer__c != null 
			and cscfga__Configuration_Offer__r.cscfga__Active__c = true
			//and UPC_Eligible_for_Upgrade__c = true
			and cscfga__Parent_Configuration__c = null
			and cscfga__Product_Definition__r.name like :queryPdName
		];

		upgradeConfigurations(configList);
	}
	
	@future
    public static void upgradeOpenBasketsFuture() {
        upgradeOpenBaskets();
    }

	// Upgrades all open baskets -> risk of hitting governor limits if there are too many configs in the basket.
	public static void upgradeOpenBaskets() {
		List<cscfga__Product_Configuration__c> configList = [
			select id, cscfga__Product_Definition__r.cscfga__IsArchived__c, cscfga__product_definition_iteration__c, cscfga__product_definition__r.cscfga__iteration__c
			from cscfga__Product_Configuration__c
			where cscfga__Product_Basket__c != null 
			and cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != 'Closed - TPV Pending'
			and cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != 'Closed Won'
			and cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != 'Closed Lost'
			// and UPC_Eligible_for_Upgrade__c = true
			and cscfga__Parent_Configuration__c = null and id = 'a4D9E000000HWdRUAW'
		];

		upgradeConfigurations(configList);
	}
	
	@future
    public static void upgradelimitedNumberOfOpenBasketsFuture(Integer limitNumber) {
        upgradelimitedNumberOfOpenBaskets(limitNumber);
    }

	// Upgrade certain amount of baskets. Use if upgradeOpenBaskets hits limits.
	public static void upgradelimitedNumberOfOpenBaskets(Integer limitNumber) {
		List<cscfga__Product_Configuration__c> configList = [
			select id, cscfga__Product_Definition__r.cscfga__IsArchived__c, cscfga__product_definition_iteration__c, cscfga__product_definition__r.cscfga__iteration__c
			from cscfga__Product_Configuration__c
			where cscfga__Product_Basket__c != null 
			and cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != 'Closed - TPV Pending'
			and cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != 'Closed Won'
			and cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != 'Closed Lost'
			// and UPC_Eligible_for_Upgrade__c = true
			and cscfga__Parent_Configuration__c = null
			limit :limitNumber
		];

		upgradeConfigurations(configList);
	}
	
	public static Integer testConfigurationsOffers() {
	    
	    List<cscfga__Product_Configuration__c> configList = [
			select id, cscfga__Product_Definition__r.cscfga__IsArchived__c, cscfga__product_definition_iteration__c, cscfga__product_definition__r.cscfga__iteration__c
			from cscfga__Product_Configuration__c
			where cscfga__Configuration_Offer__c != null 
			and cscfga__Configuration_Offer__r.cscfga__Active__c = true
			// and UPC_Eligible_for_Upgrade__c = true
			and cscfga__Parent_Configuration__c = null
		];
	    
		List<Id> configIdsForVersionUpgrade = new List<Id>();
		List<Id> configIdsForStructureUpgrade = new List<Id>();
		Set<Id> configIds = new Set<Id>();

		for(cscfga__Product_Configuration__c configuration : configList) {
			if(configuration.cscfga__Product_Definition__r.cscfga__IsArchived__c 
				&& configuration.cscfga__product_definition_iteration__c != configuration.cscfga__product_definition__r.cscfga__iteration__c) {
				configIdsForVersionUpgrade.add(configuration.id);
				configIds.add(configuration.id);
			} else if(!configuration.cscfga__Product_Definition__r.cscfga__IsArchived__c) {
				configIdsForStructureUpgrade.add(configuration.id);
				configIds.add(configuration.id);
			}
		}
		
		return configIdsForVersionUpgrade.size() + configIdsForStructureUpgrade.size();
	}
	
	public static Integer testConfigurationsOpenBaskets() {
	    
	    List<cscfga__Product_Configuration__c> configList = [
			select id, cscfga__Product_Definition__r.cscfga__IsArchived__c, cscfga__product_definition_iteration__c, cscfga__product_definition__r.cscfga__iteration__c
			from cscfga__Product_Configuration__c
			where cscfga__Product_Basket__c != null 
			and cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != 'Closed - TPV Pending'
			and cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != 'Closed Won'
			and cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != 'Closed Lost'
			// and UPC_Eligible_for_Upgrade__c = true
			and cscfga__Parent_Configuration__c = null and id = 'a4D9E000000HWdRUAW'
		];
	    
		List<Id> configIdsForVersionUpgrade = new List<Id>();
		List<Id> configIdsForStructureUpgrade = new List<Id>();
		Set<Id> configIds = new Set<Id>();

		for(cscfga__Product_Configuration__c configuration : configList) {
			if(configuration.cscfga__Product_Definition__r.cscfga__IsArchived__c 
				&& configuration.cscfga__product_definition_iteration__c != configuration.cscfga__product_definition__r.cscfga__iteration__c) {
				configIdsForVersionUpgrade.add(configuration.id);
				configIds.add(configuration.id);
			} else if(!configuration.cscfga__Product_Definition__r.cscfga__IsArchived__c) {
				configIdsForStructureUpgrade.add(configuration.id);
				configIds.add(configuration.id);
			}
		}
		
        return configIdsForVersionUpgrade.size() + configIdsForStructureUpgrade.size();
	}
	
	public static Integer testConfigurationsPD(String pdName) {
	    
	    String queryPdName = '%' + pdName + '%';
		List<cscfga__Product_Configuration__c> configList = [
			select id, cscfga__Product_Definition__r.cscfga__IsArchived__c, cscfga__product_definition_iteration__c, cscfga__product_definition__r.cscfga__iteration__c
			from cscfga__Product_Configuration__c
			where cscfga__Configuration_Offer__c != null 
			and cscfga__Configuration_Offer__r.cscfga__Active__c = true
			//and UPC_Eligible_for_Upgrade__c = true
			and cscfga__Parent_Configuration__c = null
			and cscfga__Product_Definition__r.name like :queryPdName
		];
	    
		List<Id> configIdsForVersionUpgrade = new List<Id>();
		List<Id> configIdsForStructureUpgrade = new List<Id>();
		Set<Id> configIds = new Set<Id>();

		for(cscfga__Product_Configuration__c configuration : configList) {
			if(configuration.cscfga__Product_Definition__r.cscfga__IsArchived__c 
				&& configuration.cscfga__product_definition_iteration__c != configuration.cscfga__product_definition__r.cscfga__iteration__c) {
				configIdsForVersionUpgrade.add(configuration.id);
				configIds.add(configuration.id);
			} else if(!configuration.cscfga__Product_Definition__r.cscfga__IsArchived__c) {
				configIdsForStructureUpgrade.add(configuration.id);
				configIds.add(configuration.id);
			}
		}
		
		
		return configIdsForVersionUpgrade.size() + configIdsForStructureUpgrade.size();
	}

	public static void upgradeConfigurations(List<cscfga__Product_Configuration__c> configList) {
		List<Id> configIdsForVersionUpgrade = new List<Id>();
		List<Id> configIdsForStructureUpgrade = new List<Id>();
		Set<Id> configIds = new Set<Id>();

		for(cscfga__Product_Configuration__c configuration : configList) {
			if(configuration.cscfga__Product_Definition__r.cscfga__IsArchived__c 
				&& configuration.cscfga__product_definition_iteration__c != configuration.cscfga__product_definition__r.cscfga__iteration__c) {
				configIdsForVersionUpgrade.add(configuration.id);
				configIds.add(configuration.id);
			} else if(!configuration.cscfga__Product_Definition__r.cscfga__IsArchived__c) {
				configIdsForStructureUpgrade.add(configuration.id);
				configIds.add(configuration.id);
			}
		}

		cfgug1.ProductConfigurationUpgrader.upgradeConfigurations(configIdsForVersionUpgrade);
		cfgug1.ProductConfigurationUpgrader.structureUpgrade(configIdsForStructureUpgrade);

		for(Id configId : configIds) {
			Set<Id> revalidationIds = new Set<Id>();
			revalidationIds.add(configId);
			cscfga.ProductConfigurationBulkActions.revalidateConfigurationsAsync(revalidationIds);
		}
	}
}