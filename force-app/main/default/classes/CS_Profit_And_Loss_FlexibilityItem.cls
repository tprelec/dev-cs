public with sharing class CS_Profit_And_Loss_FlexibilityItem {

    public Integer Quantity { get; set; }
    public Decimal RecurringPrice { get; set; }
    public Integer Duration { get; set; }
    public String Proposition { get; set; }
    public Decimal Margin { get; set; }

    public CS_Profit_And_Loss_FlexibilityItem() {

    }

    public CS_Profit_And_Loss_FlexibilityItem(Integer quantity, Decimal recurringPrice, Integer duration, String proposition, Decimal margin) {
        this.Quantity = quantity;
        this.RecurringPrice = recurringPrice;
        this.Duration = duration;
        this.Proposition = proposition;
        this.Margin = margin;
    }
}