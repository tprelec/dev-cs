@isTest
public with sharing class CS_Profit_And_Loss_FlexibilityItemTest {
    public CS_Profit_And_Loss_FlexibilityItemTest() {

    }

    @isTest
    static void simpleTest() {
        CS_Profit_And_Loss_FlexibilityItem newItem = new CS_Profit_And_Loss_FlexibilityItem(10, 20.5, 36, 'Flex Mobile', 0.5);
        System.assertEquals(10, newItem.Quantity);
    }
}