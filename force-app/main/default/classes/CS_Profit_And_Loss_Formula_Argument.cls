global class CS_Profit_And_Loss_Formula_Argument {
    public Decimal price {get; set;}
    public Decimal priceOneOffAmount {get; set;}
    public Decimal pricePerMonth {get; set;}
    public Decimal quantity {get; set;}
    public Decimal quantityAverageBase {get; set;}
    public Integer quantityAverageCTNPerYearForFlatFeeEuRowOther {get; set;}
    public Integer quantityExpectedMinutesAcrossMultipleProviders {get; set;}
    public Integer quantityNumberOfLocationsOneOffAmountFirstMonthFirstYear {get; set;}
    public Decimal netTariffPerMinutePerMonth {get; set;}
    public Integer duration {get; set;}
    public Integer durationMonthsInContractForCurrentYear {get; set;}
    public Integer durationMonthsInContract {get; set;}
    public Integer durationMonthsInContractPerConnectionType {get; set;}
    public Integer sumOfClosingBasesOverAllMonthsInContractForCurrentYear {get; set;}
    public Integer sumOfClosingBasesOverAllMonthsInContract {get; set;}
    public Decimal recurringCharge {get; set;}
    public Decimal usageValue {get; set;}
    
    public CS_Profit_And_Loss_Formula_Argument() {
        
    }
    
    public static CS_Profit_And_Loss_Formula_Argument getDummyData() {
        CS_Profit_And_Loss_Formula_Argument dummyDataObject = new CS_Profit_And_Loss_Formula_Argument();
        dummyDataObject.price = 50;
        dummyDataObject.priceOneOffAmount = 100;
        dummyDataObject.pricePerMonth = 100;
        dummyDataObject.quantity = 100;
        dummyDataObject.duration = 36;
        dummyDataObject.quantityAverageBase = 2;
        dummyDataObject.quantityAverageCTNPerYearForFlatFeeEuRowOther = 100;
        dummyDataObject.quantityExpectedMinutesAcrossMultipleProviders = 100;
        dummyDataObject.quantityNumberOfLocationsOneOffAmountFirstMonthFirstYear = 100;
        dummyDataObject.netTariffPerMinutePerMonth = 100;
        dummyDataObject.durationMonthsInContractForCurrentYear = 13;
        dummyDataObject.durationMonthsInContract = 100;
        dummyDataObject.durationMonthsInContractPerConnectionType = 100;
        dummyDataObject.sumOfClosingBasesOverAllMonthsInContractForCurrentYear = 100;
        dummyDataObject.sumOfClosingBasesOverAllMonthsInContract = 100;
        dummyDataObject.usageValue = 5;
        dummyDataObject.recurringCharge = 7;
        return dummyDataObject;
    }
}