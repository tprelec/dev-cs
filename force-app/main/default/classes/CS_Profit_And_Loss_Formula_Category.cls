global class CS_Profit_And_Loss_Formula_Category {
    public List<String> profitAndLossSubElementTypes {get; set;}
    public Integer profitAndLossFormulaCategory {get; set;}
    public Integer profitAndLossFormulaSubCategory {get; set;}
    
    
    public CS_Profit_And_Loss_Formula_Category(List<String> profitAndLossSubElementTypes, Integer profitAndLossFormulaCategory) {
        this.profitAndLossSubElementTypes = profitAndLossSubElementTypes;
        this.profitAndLossFormulaCategory = profitAndLossFormulaCategory;
        this.profitAndLossFormulaSubCategory = 0;
    }
    
    public CS_Profit_And_Loss_Formula_Category(List<String> profitAndLossSubElementTypes, Integer profitAndLossFormulaCategory, Integer profitAndLossFormulaSubCategory) {
        this.profitAndLossSubElementTypes = profitAndLossSubElementTypes;
        this.profitAndLossFormulaCategory = profitAndLossFormulaCategory;
        this.profitAndLossFormulaSubCategory = profitAndLossFormulaSubCategory;
    }
}