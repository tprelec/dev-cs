global class CS_Profit_And_Loss_Formula_Impl {
    public CS_Profit_And_Loss_Formula_Impl() {
        
    }
    
    public static Decimal returnFormulaCalculation(CS_Profit_And_Loss_Formula_Category formulaCategory, CS_Profit_And_Loss_Formula_Argument formulaArgument) {
        if (formulaCategory.profitAndLossFormulaCategory == 1) {
            return categoryOneFormula(formulaArgument);
        } else if (formulaCategory.profitAndLossFormulaCategory == 2) {
            return categoryTwoFormula(formulaArgument);
        } else if (formulaCategory.profitAndLossFormulaCategory == 3) {
            return categoryThreeFormula(formulaArgument);
        } else if (formulaCategory.profitAndLossFormulaCategory == 4) {
            return categoryFourFormula(formulaArgument);
        } else if (formulaCategory.profitAndLossFormulaCategory == 6) {
            return categorySixFormula(formulaArgument);
        } else if (formulaCategory.profitAndLossFormulaCategory == 7) {
            return categorySevenFormula(formulaArgument);
        } else if (formulaCategory.profitAndLossFormulaCategory == 8) {
            return categoryEightFormula(formulaArgument);
        } else if (formulaCategory.profitAndLossFormulaCategory == 10) {
            return categoryTenFormula(formulaArgument);
        } else if (formulaCategory.profitAndLossFormulaCategory == 0) {
            return categoryZeroFormula(formulaArgument);
        }
        
        return 0;
    }
    
    // Generic formula - argument helper should fit price/quantity/duration depending on the formula category.
    public static Decimal categoryZeroFormula(CS_Profit_And_Loss_Formula_Argument argument) {
        return argument.recurringCharge * argument.quantity;
    }
    
    // Generic formula - argument helper should fit price/quantity/duration depending on the formula category.
    public static Decimal categoryOneFormula(CS_Profit_And_Loss_Formula_Argument argument) {
        Decimal result = argument.price * argument.quantity * argument.duration;
        return result;
    }
    
    // Optional voice addon - unlimited RoW voice, RoW IDD, RoW SMS and RoW MMS
    // Optional data priceplan - data only simcard
    // Other voice usage
    // Functional / Integration addon
    // = Total revenue Optional Voice addons [over entire contract, i.e. Price * Quantity * Duration contract in months per connection type] * ( Sum of closing bases over all months in contract for current year / Sum of closing bases over all months in contract )
    public static Decimal categoryTwoFormula (CS_Profit_And_Loss_Formula_Argument argument) {
        // TODO check values validation
        Decimal result = (argument.price * argument.quantity * argument.duration);
        Decimal resultOfDiv = argument.sumOfClosingBasesOverAllMonthsInContract != 0 ? argument.sumOfClosingBasesOverAllMonthsInContractForCurrentYear / argument.sumOfClosingBasesOverAllMonthsInContract : 0;
        Decimal resultTotal = result * resultOfDiv;
        return resultTotal;
    }
    
    // Data account bundle type EU
    // Data account bundle type RoW
    // = Total revenue Data account bundle type EU [over entire contract, i.e. Price * Quantity * Duration contract in months per connection type] * ( # months in contract for current year / # months in contract )
    public static Decimal categoryThreeFormula(CS_Profit_And_Loss_Formula_Argument argument) {
        // TODO check values validation
        Decimal result = (argument.price * argument.quantity * argument.duration);
        Decimal resultOfDiv = argument.durationMonthsInContract != 0 ? Decimal.valueOf(argument.durationMonthsInContractForCurrentYear) / argument.durationMonthsInContract : 0;
        Decimal resultTotal = result * resultOfDiv;
        return resultTotal;
    }
    
    // = Sum of all Gross Revenue for all CTN's, calculated through: Quantity [average # CTN / year for flat fee EU, flat fee Row and other optionals] * Quantity [# expected minutes, across multiple providers] * Net Tariff [/minute / month] [calculated for Voice Nationaal, SMS Nationaal, Voice Internationaal, SMS Internationaal, Voice Roaming, SMS roaming - i.e EU, RoW exceptions] * Quantity [# months in contract in current year]
    public static Decimal categoryFourFormula(CS_Profit_And_Loss_Formula_Argument argument) {
        // TODO check values validation
        Decimal result = argument.quantityAverageCTNPerYearForFlatFeeEuRowOther * argument.quantityExpectedMinutesAcrossMultipleProviders * argument.netTariffPerMinutePerMonth * argument.durationMonthsInContractForCurrentYear;
        return result;
    }
    
    // = Price * Quantity [number of locations] [one-off amount in first month/first year]
    public static Decimal categorySixFormula(CS_Profit_And_Loss_Formula_Argument argument) {
        // TODO check values validation
        Decimal result = argument.usageValue / argument.duration;
        return result;
    }
    
    // = Fixed price [yes or no] [one-off amount in first month/first year]
    public static Decimal categorySevenFormula(CS_Profit_And_Loss_Formula_Argument argument) {
        // TODO check values validation
        Decimal result = argument.price;
        return result;
    }
    
    // = Price [one-off amount]
    public static Decimal categoryEightFormula(CS_Profit_And_Loss_Formula_Argument argument) {
        // TODO check values validation
        Decimal result = argument.price;
        return result;
    }
    
    // = Price [/ month : MAF ] * Quantity [average base for # months in contract for current year]
    public static Decimal categoryTenFormula(CS_Profit_And_Loss_Formula_Argument argument) {
        // TODO check values validation
        Decimal result = argument.pricePerMonth * argument.quantityAverageBase;
        return result;
    }
}