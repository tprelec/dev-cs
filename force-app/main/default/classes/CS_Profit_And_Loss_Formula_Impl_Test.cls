@isTest
public class CS_Profit_And_Loss_Formula_Impl_Test {
    private static testMethod void test() {
        
        CS_Profit_And_Loss_Formula_Impl testObject = new CS_Profit_And_Loss_Formula_Impl();
        CS_Profit_And_Loss_Formula_Category category = new CS_Profit_And_Loss_Formula_Category(new List<String>(), 1);
        
        category.profitAndLossFormulaCategory = 1;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
        
        category.profitAndLossFormulaCategory = 2;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
        
        category.profitAndLossFormulaCategory = 3;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
        
        category.profitAndLossFormulaCategory = 4;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
        
        category.profitAndLossFormulaCategory = 6;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
        
        category.profitAndLossFormulaCategory = 7;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
        
        category.profitAndLossFormulaCategory = 8;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
        
        category.profitAndLossFormulaCategory = 10;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
        
        category.profitAndLossFormulaCategory = 0;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
        
        category.profitAndLossFormulaCategory = 99;
        CS_Profit_And_Loss_Formula_Impl.returnFormulaCalculation(category, CS_Profit_And_Loss_Formula_Argument.getDummyData());
    }
}