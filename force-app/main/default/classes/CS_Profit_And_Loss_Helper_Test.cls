@isTest
public class CS_Profit_And_Loss_Helper_Test {

    private static cscfga__Product_Basket__c BasketDetails {get; set;}
    private static Map<Id, cspl.ProductConfiguration> ConfigDetails {get; set;}
    private static Map<String, cspl.Calculation> Calculations {get; set;}
    
    @isTest
    static void testHelpers()
    {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
            
        insert simpleUser;
        System.runAs (simpleUser) {  
        
            PriceReset__c priceResetSetting = new PriceReset__c();
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
            insert priceResetSetting;
            
            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;
            
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;
            
            Contact contact1 = new Contact(
                AccountId = testAccount.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                
                Email = 'test@vf.com'   
            );
            insert contact1;
            
            
            Contact contact2 = new Contact(
                AccountId = testAccount.id,
                LastName = 'Last',
                FirstName = 'Second',
                Contact_Role__c = 'Consultant',
                
                Email = 'test2@vf.com'   
            );
            insert contact2;
            
            testAccount.Authorized_to_sign_1st__c = contact1.Id;
            testAccount.Authorized_to_sign_2nd__c = contact2.Id;
            testAccount.Contract_rule_no_mailing__c = true;
            testAccount.Frame_Work_Agreement__c = 'VFZA-2018-8';
            testAccount.Version_FWA__c = 3;
            testAccount.Framework_agreement_date__c = Date.today();
            update testAccount;
            
            Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert opp;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Test Basket');
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = testAccount.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 24;
            basket.Contract_duration_BMS__c = '24';
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 09, 22);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 09, 25);
            insert basket;

            BasketDetails = basket;
            
            cscfga__Product_Definition__c pd = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
            pd.Product_Type__c = 'Mobile';
            pd.Name = 'Mobile Test';
            insert pd;
            
            cscfga__Product_Configuration__c pcParent = CS_DataTest.createProductConfiguration(pd.Id, 'Company Level Fixed Voice',basket.Id);
            insert pcParent;
            
            cscfga__Product_Configuration__c pc = CS_DataTest.createProductConfiguration(pd.Id, 'ONE Business Data',basket.Id);
            pc.cscfga__Contract_Term__c = 24;
            pc.cscfga__total_one_off_charge__c = 100;
            pc.OneOff_Product_List_Price__c = 100.00;
            pc.cscfga__Total_Price__c = 20;
            pc.cspl__Type__c = 'Mobile CTN profile';
            pc.Proposition__c = 'Proposition';
            pc.cscfga__Contract_Term_Period__c = 12;
            pc.cscfga__Configuration_Status__c = 'Valid';
            pc.cscfga__Parent_Configuration__c = pcParent.Id;
            pc.cscfga__Quantity__c = 1;
            pc.cscfga__total_recurring_charge__c = 22;
            pc.Recurring_Product_List_Price__c = 10;
            pc.RC_Cost__c = 0;
            pc.NRC_Cost__c = 0;
            pc.cscfga__one_off_charge_product_discount_value__c = 0;
            pc.cscfga__recurring_charge_product_discount_value__c = 0;
            pc.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
            insert pc;
            
            List<cscfga__Product_Configuration__c> listPC = [select Id, cscfga__Product_Definition__r.Name, cscfga__Parent_Configuration__c, cscfga__Parent_Configuration__r.Name, Name, NRC_Cost__c,RC_Cost__c,
            Proposition__c, cspl__Type__c, cscfga__total_one_off_charge__c, cscfga__Contract_Term__c, cscfga__Total_Price__c, OneOff_Product_List_Price__c, OneOff_List_Price__c, Recurring_Product_List_Price__c,
            cscfga__Contract_Term_Period__c, cscfga__Configuration_Status__c, cscfga__Quantity__c, cscfga__total_recurring_charge__c, cscfga__discounts__c, cscfga__recurring_charge_product_discount_value__c,
            cscfga__one_off_charge_product_discount_value__c from cscfga__Product_Configuration__c where id = :pc.Id];
            
            pc = listPC[0];
            
            cscfga__Attribute_Definition__c adTaxonomy = new cscfga__Attribute_Definition__c(
                Name = 'Taxonomy',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'String'
            );
            insert adTaxonomy;
            
            cscfga__Attribute__c attrTaxonomy = new cscfga__Attribute__c(
                Name = 'Taxonomy',
                cscfga__Attribute_Definition__c = adTaxonomy.Id,
                cscfga__Product_Configuration__c = pc.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = 'Mobile CTN profile',
                cscfga__Price__c = 0
            );
            insert attrTaxonomy;
            
            cscfga__Attribute_Definition__c adAverageBaseContainer = new cscfga__Attribute_Definition__c(
                Name = 'AverageBaseContainer',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'String'
            );
            insert adAverageBaseContainer;
            
            cscfga__Attribute__c attrAverageBaseContainer = new cscfga__Attribute__c(
                Name = 'AverageBaseContainer',
                cscfga__Attribute_Definition__c = adAverageBaseContainer.Id,
                cscfga__Product_Configuration__c = pc.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = 'Total;144_1;3_2;6_3;6_4;6_5;6_6;6_7;6_8;6_9;6_10;6_11;6_12;6_13;6_14;6_15;6_16;6_17;6_18;6_19;6_20;6_21;6_22;6_23;6_24;6_25;3',
                cscfga__Price__c = 0
            );
            insert attrAverageBaseContainer;
            
            cscfga__Attribute_Definition__c adClosingBaseContainer = new cscfga__Attribute_Definition__c(
                Name = 'ClosingBaseContainer',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'String'
            );
            insert adClosingBaseContainer;
            
            cscfga__Attribute__c attrClosingBaseContainer = new cscfga__Attribute__c(
                Name = 'ClosingBaseContainer',
                cscfga__Attribute_Definition__c = adClosingBaseContainer.Id,
                cscfga__Product_Configuration__c = pc.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = 'Total;144_1;6_2;6_3;6_4;6_5;6_6;6_7;6_8;6_9;6_10;6_11;6_12;6_13;6_14;6_15;6_16;6_17;6_18;6_19;6_20;6_21;6_22;6_23;6_24;6_25;0',
                cscfga__Price__c = 0
            );
            insert attrClosingBaseContainer;
            
            cscfga__Attribute_Definition__c adCTNQuantity = new cscfga__Attribute_Definition__c(
                Name = 'CTN quantity',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'Integer'
            );
            insert adCTNQuantity;
            
            cscfga__Attribute__c attrCTNQuantity = new cscfga__Attribute__c(
                Name = 'CTN quantity',
                cscfga__Attribute_Definition__c = adCTNQuantity.Id,
                cscfga__Product_Configuration__c = pc.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = '6',
                cscfga__Price__c = 0
            );
            insert attrCTNQuantity;
            
            cscfga__Attribute_Definition__c adOneOffPriceSingle = new cscfga__Attribute_Definition__c(
                Name = 'One off price single',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'Decimal'
            );
            insert adOneOffPriceSingle;
            
            cscfga__Attribute__c attrOneOffPriceSingle = new cscfga__Attribute__c(
                Name = 'One off price single',
                cscfga__Attribute_Definition__c = adOneOffPriceSingle.Id,
                cscfga__Product_Configuration__c = pc.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = '',
                cscfga__Price__c = 0
            );
            insert attrOneOffPriceSingle;
            
            cscfga__Attribute_Definition__c adOneOffPrice = new cscfga__Attribute_Definition__c(
                Name = 'One off price',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'Decimal'
            );
            insert adOneOffPrice;
            
            cscfga__Attribute__c attrOneOffPrice = new cscfga__Attribute__c(
                Name = 'One off price',
                cscfga__Attribute_Definition__c = adOneOffPrice.Id,
                cscfga__Product_Configuration__c = pc.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = '30',
                cscfga__Price__c = 30
            );
            insert attrOneOffPrice;
            
            cscfga__Attribute_Definition__c adRecurringPriceSingle = new cscfga__Attribute_Definition__c(
                Name = 'Recurring price single',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'Decimal'
            );
            insert adRecurringPriceSingle;
            
            cscfga__Attribute__c attrRecurringPriceSingle = new cscfga__Attribute__c(
                Name = 'Recurring price single',
                cscfga__Attribute_Definition__c = adRecurringPriceSingle.Id,
                cscfga__Product_Configuration__c = pc.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = '5',
                cscfga__Price__c = 5
            );
            insert attrRecurringPriceSingle;
            
            cscfga__Attribute_Definition__c adRecurringPrice = new cscfga__Attribute_Definition__c(
                Name = 'Recurring price',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'Decimal'
            );
            insert adRecurringPrice;
            
            cscfga__Attribute__c attrRecurringPrice = new cscfga__Attribute__c(
                Name = 'Recurring price',
                cscfga__Attribute_Definition__c = adRecurringPrice.Id,
                cscfga__Product_Configuration__c = pc.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = '',
                cscfga__Price__c = 0
            );
            insert attrRecurringPrice;
            
            cscfga__Attribute_Definition__c adPLFormula = new cscfga__Attribute_Definition__c(
                Name = 'PLFormula',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'String'
            );
            insert adPLFormula;
            
            cscfga__Attribute__c attrPLFormula = new cscfga__Attribute__c(
                Name = 'PLFormula',
                cscfga__Attribute_Definition__c = adPLFormula.Id,
                cscfga__Product_Configuration__c = pc.Id,
                cscfga__is_active__c = true,
                //cscfga__Value__c = '',
                cscfga__Price__c = 0
            );
            insert attrPLFormula;
            
            Map<Id, cspl.ProductConfiguration> configDetails = new Map<Id, cspl.ProductConfiguration>();
            cspl.ProductConfiguration.CS_Attribute attributeDetails1 = new cspl.ProductConfiguration.CS_Attribute(attrTaxonomy, adTaxonomy);
            cspl.ProductConfiguration.CS_Attribute attributeDetails2 = new cspl.ProductConfiguration.CS_Attribute(attrAverageBaseContainer, adAverageBaseContainer);
            cspl.ProductConfiguration.CS_Attribute attributeDetails3 = new cspl.ProductConfiguration.CS_Attribute(attrClosingBaseContainer, adClosingBaseContainer);
            cspl.ProductConfiguration.CS_Attribute attributeDetails4 = new cspl.ProductConfiguration.CS_Attribute(attrCTNQuantity, adCTNQuantity);
            cspl.ProductConfiguration.CS_Attribute attributeDetails5 = new cspl.ProductConfiguration.CS_Attribute(attrOneOffPriceSingle, adOneOffPriceSingle);
            cspl.ProductConfiguration.CS_Attribute attributeDetails6 = new cspl.ProductConfiguration.CS_Attribute(attrOneOffPrice, adOneOffPrice);
            cspl.ProductConfiguration.CS_Attribute attributeDetails7 = new cspl.ProductConfiguration.CS_Attribute(attrRecurringPriceSingle, adRecurringPriceSingle);
            cspl.ProductConfiguration.CS_Attribute attributeDetails8 = new cspl.ProductConfiguration.CS_Attribute(attrRecurringPrice, adRecurringPrice);
            cspl.ProductConfiguration.CS_Attribute attributeDetails9 = new cspl.ProductConfiguration.CS_Attribute(attrPLFormula, adPLFormula);
            List<cspl.ProductConfiguration.CS_Attribute> attributDetailsList = new List<cspl.ProductConfiguration.CS_Attribute>();
            attributDetailsList.add(attributeDetails1);
            attributDetailsList.add(attributeDetails2);
            attributDetailsList.add(attributeDetails3);
            attributDetailsList.add(attributeDetails4);
            attributDetailsList.add(attributeDetails5);
            attributDetailsList.add(attributeDetails6);
            attributDetailsList.add(attributeDetails7);
            attributDetailsList.add(attributeDetails8);
            attributDetailsList.add(attributeDetails9);
            configDetails.put(pc.Id, new cspl.ProductConfiguration(pc, attributDetailsList));

            ConfigDetails = configDetails;
            
            Map<String, cspl.Calculation> calculations = new Map<String, cspl.Calculation>();
            List<Decimal> data = new List<Decimal>();
            for (Integer i = 0; i < 24; i++) {
                data.add(1);
            }
        
            calculations.put('Total Gross Revenue', new cspl.Calculation(data));
            calculations.put('Total Gross Revenue for Operator other costs / other migration costs', new cspl.Calculation(data));
            calculations.put('Operator other costs / other migration costs Mobile', new cspl.Calculation(data));
            calculations.put('Opportunity cost Mobile', new cspl.Calculation(data));
            calculations.put('Opportunity cost Fixed', new cspl.Calculation(data));
            calculations.put('Other Costs', new cspl.Calculation(data));
            calculations.put('Operator other costs / other migration costs', new cspl.Calculation(data));
            calculations.put('Benchmark Mobile', new cspl.Calculation(data));
            calculations.put('Operator other costs / other migration costs Fixed', new cspl.Calculation(data));
            calculations.put('Benchmark Fixed', new cspl.Calculation(data));
            calculations.put('Total credit amount / loyalty bonus', new cspl.Calculation(data));
            calculations.put('A&R', new cspl.Calculation(data));
            calculations.put('Discounts', new cspl.Calculation(data));
            calculations.put('Network Opex', new cspl.Calculation(data));
            calculations.put('Overhead allocation', new cspl.Calculation(data));
            calculations.put('Capex', new cspl.Calculation(data));
            calculations.put('Network Depreciation', new cspl.Calculation(data));
            calculations.put('Incremental OPEX', new cspl.Calculation(data));
            calculations.put('Direct Capex', new cspl.Calculation(data));
            calculations.put('Total cost of sales', new cspl.Calculation(data));
            calculations.put('KPI Sales Margin / Net revenue', new cspl.Calculation(data));
            calculations.put('KPI EBITDA / Net revenue', new cspl.Calculation(data));
            calculations.put('KPI Free cash flow / Net revenue', new cspl.Calculation(data));
            calculations.put('KPI Net Incremental Billed Revenue', new cspl.Calculation(data));
            calculations.put('KPI Payback Period(Months)', new cspl.Calculation(data));
            calculations.put('KPI NPV', new cspl.Calculation(data));
            calculations.put('Benchmark', new cspl.Calculation(data));
            calculations.put('Revenue share', new cspl.Calculation(data));

            Calculations = calculations;

            OrderType__c orderTypeTest = CS_DataTest.createOrderType();
            orderTypeTest.Name = 'Order Type Test';
            orderTypeTest.ExportSystem__c = 'SIAS';
            orderTypeTest.Status__c = 'New';
            insert orderTypeTest;

            Product2 productTest = CS_DataTest.createProduct('test product', orderTypeTest);
            productTest.Product_Line__c = 'fVodafone';
            productTest.OrderType__c = orderTypeTest.Id;
            productTest.Not_on_contract__c = false;
            insert productTest;

            CS_Basket_Snapshot_Transactional__c basketSnapTrans = CS_DataTest.createBasketSnapshotTransactional('Snap 1', basket.Id, null, pc.Id);
        	basketSnapTrans.Name = 'Access Infrastructure';
        	basketSnapTrans.Proposition__c = 'test';
        	basketSnapTrans.RecurringProduct__c = productTest.Id;
        	basketSnapTrans.OneOffProduct__c = productTest.Id;
        	basketSnapTrans.Opportunity__c = opp.id;
        	basketSnapTrans.Product_Basket__c = basket.id;
        	basketSnapTrans.Quantity__c = 1;
        	basketSnapTrans.DiscountOneOff__c = 10;
        	basketSnapTrans.DiscountRecurringPercentage__c = 5;
        	basketSnapTrans.Service__c = 'test';
        	basketSnapTrans.One_Off_Charge_DocArea__c = 'test';
        	basketSnapTrans.ProductGroup__c = 'test';
        	basketSnapTrans.One_Off_Charge_DocArea__c = 'test';
        	basketSnapTrans.OneOffPrice__c = 100;
        	basketSnapTrans.RecurringPrice__c = 20;
        	basketSnapTrans.FinalPriceRecurring__c = 240;
        	basketSnapTrans.FinalPriceOneOff__c = 90;
        	basketSnapTrans.ProductName__c = productTest.Name;
        	basketSnapTrans.CCodeOneOff__c = 'test';
        	basketSnapTrans.CCodeRecurring__c = 'test';
        	basketSnapTrans.ConnectionType__c = 'test';
        	basketSnapTrans.OneOffName__c = 'test';
        	basketSnapTrans.DiscountOneOffPercentage__c = 5;
            basketSnapTrans.Treshold_Amount__c = 3;
            insert basketSnapTrans;

            Test.startTest();
            CS_Profit_And_Loss_Helper helper = new CS_Profit_And_Loss_Helper(BasketDetails, ConfigDetails, Calculations);

            System.assertEquals(false, helper.isIndirectSales());
            System.assertEquals('2020', helper.getCurrentYear(pc.Id, 1));
            System.assertEquals(0, helper.getSubscriptionHardwareCommision(pc.Id));
            System.assertEquals(0, helper.getAddonHardwareCommision(pc.Id));
            System.assertEquals(3, helper.getThresholdAmount(pc.Id));
            helper.getAllDealItems();
            System.assertEquals(0, helper.getRecurringCost(pc.Id));
            System.assertEquals(0, helper.getOneOffCost(pc.Id));
            System.assertEquals(null, helper.getRecurringProductId(basketSnapTrans.Id));
            System.assertEquals(null, helper.getDealItemRecurring(basketSnapTrans.Id));
            System.assertEquals(null, helper.getDealItemOneOff(basketSnapTrans.Id));
            System.assertNotEquals(null, helper.getCalculation('Benchmark'));
            System.assertNotEquals(null, helper.getTypeOf(pc.Id));
            System.assertNotEquals(null, helper.getParentConfigurationId(pc.Id));

            List<Decimal> decimals = new List<Decimal>();
            decimals = helper.addListZeroElements(decimals, 5);
            System.assertEquals(5, decimals.size());

            helper.setListValuesToZero(decimals);
            System.assertEquals(0, decimals[0]);

            decimals = helper.getDiscountValue(configDetails.get(pc.Id));
            System.assertNotEquals(null, decimals);

            Map<String, CS_Profit_And_Loss_Formula_Category> retVal = helper.getTaxonomyPerProposition(configDetails, 'Mobile CTN profile');
            System.assertEquals(1, retVal.keySet().size());

            retVal = helper.getTaxonomyMap(new List<cspl.ProductConfiguration> {configDetails.get(pc.Id)});
            System.assertEquals(1, retVal.keySet().size());
            System.assertEquals('ONE Business Data', helper.getName(pc.Id));
            System.assertNotEquals(null, helper.getPropositionList(configDetails));

            System.assertEquals(12, helper.numberOfMonthsInYear(2));
            System.assertEquals(1, helper.calculateYearOrderForMobile(2));
            System.assertEquals(1, helper.calculateYearOrderForFixed(2));
            System.assertEquals(6, helper.getAverageBase(2, pc.Id));
            System.assertEquals(1, helper.getQuantity(pc.Id));
            System.assertEquals(6, helper.getClosingBase(2, pc.Id));
            System.assertEquals(false, helper.isConfigurationAddon(pc.Id));
            System.assertEquals(true, helper.isParentConfigurationCLFV(pc.Id));
            System.assertEquals(false, helper.isConfigurationMobileAddon(pc.Id));
            System.assertEquals(false, helper.isBmsMobile(pc.Id));
            System.assertEquals(false, helper.isConfigurationCustomerServices(pc.Id));
            System.assertEquals(6, helper.calculateQuantity(2, pc.Id));
            System.assertEquals(144, helper.getSumClosingBase());
            //System.assertEquals(1, helper.getSumOfAllFieldValuesForType());
            System.assertEquals(24, helper.getMobileDuration());
            System.assertEquals(24, helper.getBMSDuration());
            System.assertEquals(24, helper.getFixedDuration());
            System.assertEquals(24, helper.getDuration());
            System.assertEquals(0, helper.getSumOfClosingBasesOverAllMonthsInContract());
            System.assertEquals(24, helper.getContractTerm(pc.Id));
            
            System.assertEquals(100, helper.getOneOffListPriceValue(pc.Id));
            System.assertEquals(100, helper.getOneOffValue(pc.Id));
            System.assertEquals(10, helper.getPricePerMonthValue(pc.Id));
            List<Decimal> flexibility = helper.calculateFlexibility(30, 4, 24, 0.5);
            System.assertEquals(1.25, flexibility[0]);

            Map<String, Double> propositions = new Map<String, Double>();
            propositions.put('Flex Mobile', 50);
            System.assertEquals(0.5, helper.getFlexibilityMargin('Flex Mobile', propositions));


            CS_Profit_And_Loss_Formula_Category category1 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 1);
            CS_Profit_And_Loss_Formula_Category category2 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 2);
            CS_Profit_And_Loss_Formula_Category category3 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 3);
            CS_Profit_And_Loss_Formula_Category category8 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 4);
            CS_Profit_And_Loss_Formula_Category category4 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 8);
            CS_Profit_And_Loss_Formula_Category category6 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 7);
            CS_Profit_And_Loss_Formula_Category category7 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 10);
            CS_Profit_And_Loss_Formula_Argument formulaArgument = helper.getFormulaArgumentForCategory(category1, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            formulaArgument = helper.getFormulaArgumentForCategory(category2, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            formulaArgument = helper.getFormulaArgumentForCategory(category3, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            formulaArgument = helper.getFormulaArgumentForCategory(category4, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            formulaArgument = helper.getFormulaArgumentForCategory(category6, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            formulaArgument = helper.getFormulaArgumentForCategory(category7, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            formulaArgument = helper.getFormulaArgumentForCategory(category8, 1, pc.Id);

            CS_Profit_And_Loss_Formula_Category category12 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 1, 1);
            CS_Profit_And_Loss_Formula_Category category13 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 1, 2);
            CS_Profit_And_Loss_Formula_Category category14 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 1, 3);
            CS_Profit_And_Loss_Formula_Category category15 = new CS_Profit_And_Loss_Formula_Category(new List<String> {'OneOff_Product_List_Price__c'}, 1, 4);
            formulaArgument = helper.getFormulaArgumentForCategory(category12, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            formulaArgument = helper.getFormulaArgumentForCategory(category13, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            formulaArgument = helper.getFormulaArgumentForCategory(category14, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            formulaArgument = helper.getFormulaArgumentForCategory(category15, 1, pc.Id);
            System.assertNotEquals(null, formulaArgument);
            Test.stopTest();
    	}
        
    }

}