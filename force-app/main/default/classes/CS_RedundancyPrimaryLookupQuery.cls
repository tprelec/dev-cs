global with sharing class  CS_RedundancyPrimaryLookupQuery extends cscfga.ALookupSearch{
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit){
           
        System.debug('**** searchFields: ' + JSON.serializePretty(searchFields));   
        system.debug('basketId: ' + productDefinitionID);
        system.debug('pageOffset: ' + pageOffset);
        system.debug('pageLimit: ' + pageLimit);
           
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1; 
        Integer recordOffset = pageOffset * pageLimit;   

        List<cscfga__Product_Configuration__c> primaryConfigs = new List<cscfga__Product_Configuration__c>();
        
        List<cscfga__Product_Configuration__c> basketAccessConfigsNotThisOne = new List<cscfga__Product_Configuration__c>();
        
        String configId = searchFields.get('ConfigId');
        String basketId = searchFields.get('BasketId');
        
        System.debug('ANA TEST cnfigId='+configId+' basketId='+basketId);
        
        String siteAvailabilityIdSelected = searchFields.get('Site Check');
        String siteIdSelected = searchFields.get('Site Check SiteID');
        String vendorSelected = searchFields.get('Vendor');
        String accessTypeSelected = searchFields.get('Access Type');
        
        //accesses marked as primary
        //primaryConfigs1 = [SELECT Id,Name, Site_Name__c,Site_Check_SiteID__c, cscfga__Product_Basket__c,cscfga__Configuration_Offer__c,Primary__c,AdvanceCloned__c,Secondary__c From cscfga__Product_Configuration__c Where AdvanceCloned__c = false and cscfga__Product_Basket__c = :basketId and Name = 'Access Infrastructure' and Site_Name__c != null and Id != :configId and Secondary__c = false and Primary__c = true];
        
        basketAccessConfigsNotThisOne = [SELECT Id,Name, Access_Type__c, Vendor__c,Site_Name__c,Site_Check_SiteID__c, cscfga__Product_Basket__c,cscfga__Configuration_Offer__c,Primary__c,AdvanceCloned__c,Secondary__c From cscfga__Product_Configuration__c Where AdvanceCloned__c = false and cscfga__Product_Basket__c = :basketId and Name = 'Access Infrastructure' and Site_Name__c != null and Id != :configId];
    
        System.debug('Selected basketAccessConfigsNotThisOne == '+basketAccessConfigsNotThisOne);
        //get secondary configs
        Set<Id> secondaryConfigs = new Set<Id>();
        for(cscfga__Product_Configuration__c pc :basketAccessConfigsNotThisOne){
            if(pc.Secondary__c == true){
                secondaryConfigs.add(pc.Id);
            }
        }
        
        Set<Id> primaryConfigIds = new Set<Id>();
        for(cscfga__Product_Configuration__c pc :basketAccessConfigsNotThisOne){
            if(pc.Primary__c == true){
                primaryConfigIds.add(pc.Id);
            }
        }
        
        //get primary accesses for secondary configs
        List<cscfga__Attribute__c> selectedPrimaryAccesses = [SELECT Id, Name,cscfga__Product_Configuration__c, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c in :secondaryConfigs and (Name = 'Primary' or Name = 'Access Type' or Name='Vendor')];
        
        List<cscfga__Attribute__c> selectedPrimaryVendorAccess = [SELECT Id, Name,cscfga__Product_Configuration__c, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c in :primaryConfigIds and (Name = 'Access Type' or Name='Vendor')];
        
        Set<String> selectedAccessIds = new Set<String>();
        for(cscfga__Attribute__c attr : selectedPrimaryAccesses){
            if(attr.Name == 'Primary' && attr.cscfga__Value__c != null && attr.cscfga__Value__c!=''){
                selectedAccessIds.add(attr.cscfga__Value__c);
            }
        }
        
        
        Set<String> selectedPrimarySites = new Set<String>();
        for(cscfga__Product_Configuration__c pc :basketAccessConfigsNotThisOne){
            if(selectedAccessIds.contains(String.valueOf(pc.Id))){
                selectedPrimarySites.add(pc.Site_Check_SiteID__c);
            }
        }
        
        System.debug('Selected primary sites == '+selectedPrimarySites);
        
         System.debug('Selected basketAccessConfigsNotThisOne == '+basketAccessConfigsNotThisOne);
        
        //remove access infrastructures with primary true which have Site_Check_SiteID__c already selected
         for(cscfga__Product_Configuration__c pc :basketAccessConfigsNotThisOne){
              if((pc.Primary__c == true)&&(!selectedPrimarySites.contains(pc.Site_Check_SiteID__c))){
                    //primaryConfigs.add(pc);
                    //if different site or no site selected
                    
                    if(pc.Site_Check_SiteID__c != siteIdSelected){
                         primaryConfigs.add(pc);
                    }
                    //if same site -> has to be different access type and vendor combination
                    else if((siteIdSelected != '') && (pc.Site_Check_SiteID__c == siteIdSelected)){
                        String accessType = '';
                        for(cscfga__Attribute__c attr : selectedPrimaryVendorAccess){
                            if((attr.cscfga__Product_Configuration__c == pc.Id) && (attr.Name == 'Access Type') && (attr.cscfga__Value__c != '') && (attr.cscfga__Value__c != null)){
                                accessType = attr.cscfga__Value__c;
                            }
                        }
                        String vendor = '';
                        for(cscfga__Attribute__c attr : selectedPrimaryVendorAccess){
                            if((attr.cscfga__Product_Configuration__c == pc.Id) && (attr.Name == 'Vendor') && (attr.cscfga__Value__c != '') && (attr.cscfga__Value__c != null)){
                                vendor = attr.cscfga__Value__c;
                            }
                        }
                        //if bot access and vendor match
                        if(((accessTypeSelected!='') && (accessTypeSelected == accessType)) && ((vendorSelected == vendor)&&(vendorSelected!=''))){
                            system.debug('Not allowed');
                        }
                        else{
                            primaryConfigs.add(pc);
                        }
                    }
              }
         }
        
        return primaryConfigs;   
    }

    

    public override String getRequiredAttributes(){
      return '["BasketId","ConfigId","Site Check","Site Check SiteID","Vendor","Access Type"]';
    }  
}