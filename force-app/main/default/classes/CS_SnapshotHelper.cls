global with sharing class CS_SnapshotHelper {
    public CS_SnapshotHelper(){}
    
    public static List<csclm__Transactional_Clause__c> getTransactionalClausePerTableId(List<csclm__Transactional_Clause__c> clauses, String tableId){
       List<csclm__Transactional_Clause__c> result = new List<csclm__Transactional_Clause__c>();
       
       for(csclm__Transactional_Clause__c clause : clauses){
           if(clause.TableIdFormula__c == tableId){
               result.add(clause);
           }
       }
       return result;
    }
    
    public static List<csclm__Transactional_Clause__c> queryTransactionalClauses(Set<String> tableIds){
        Map<String, Schema.SObjectField> fieldMap = csclm__Transactional_Clause__c.sObjectType.getDescribe().fields.getMap();
        Set<String> fieldNamesSet = fieldMap.keySet();
        List<String> fieldNames = new List<String>(fieldNamesSet);
        
        String allFields = String.join(fieldNames, ',');
        String query = 'select ' + allFields + ' from csclm__Transactional_Clause__c where TableIdFormula__c in :tableIds ORDER BY CreatedDate DESC LIMIT 1';
        //String query = 'select ' + allFields + ' from csclm__Transactional_Clause__c where TableIdFormula__c in :tableIds';
        List<csclm__Transactional_Clause__c> importantClauses = Database.query(query);
        
        System.debug('--importantClause---'+importantClauses);
        
        return importantClauses;
    }
    
    
    public static List<Competitor_Asset__c> queryPBX(Set<Id> pbxId){
        List<Competitor_Asset__c> pbxS = new List<Competitor_Asset__c>();
        Map<String, Schema.SObjectField> fieldMap = Competitor_Asset__c.sObjectType.getDescribe().fields.getMap();
        Set<String> fieldNamesSet = fieldMap.keySet();
        List<String> fieldNames = new List<String>(fieldNamesSet);
        
        String allFields = String.join(fieldNames, ',');
        allFields += ',PBX_type__r.Name, PBX_type__r.Software_version__c, Site__r.Name';
        String query = 'select ' + allFields + ' from Competitor_Asset__c where Id in :pbxId';
        pbxS = Database.query(query);
        
        return pbxS;
    }
   
    
    public static Map<Id, List<Competitor_Asset__c>> getPBXPerSiteAvailability(Map<Id, List<Id>> siteAvailabilityPBX){
        
        Map<Id, List<Competitor_Asset__c>> result = new Map<Id, List<Competitor_Asset__c>>();
        
        Set<Id> pbxAllIds = new Set<Id>();
        
        for(Id saId : siteAvailabilityPBX.keySet()){
            pbxAllIds.addAll(siteAvailabilityPBX.get(saId));
        }
        
        Map<Id, Competitor_Asset__c> pbxS = new Map<Id, Competitor_Asset__c>(queryPBX(pbxAllIds));
        
        for(Id saId : siteAvailabilityPBX.keySet()){
            for(Id pbxId : siteAvailabilityPBX.get(saId)){
                if(result.containsKey(saId)){
                    result.get(saId).add(pbxS.get(pbxId));
                }
                else{
                    List<Competitor_Asset__c> pbxList = new List<Competitor_Asset__c>();
                    pbxList.add(pbxS.get(pbxId));
                    result.put(saId, pbxList);
                }
            }
        }
        
        return result;
        
    }
    
    public static Map<Id, Site__c> getAllSiteNamesPerSiteId(Set<Id> siteIds){
        Map<Id, Site__c> siteList = new Map<Id, Site__c>([SELECT Id, Name from Site__c where Id in :siteIds]);
        
        return siteList;
        
    }
    
    public static Set<Id> getAllMainSites (List<cscfga__Product_Configuration__c> basketConfigs){
        Set<Id> result = new Set<Id>();
        
        for(cscfga__Product_Configuration__c config : basketConfigs){
            if(config.cscfga__Root_Configuration__c == null && config.Site_Check_SiteID__c!=null){
                result.add(config.Site_Check_SiteID__c);
            }
        }
        
        return result;
    }
    
    
    public static Map<Id, List<Site_Availability__c>> getSiteNamePerSiteAvailability(Set<Id> siteAvailabilityIds){
        List<Site_Availability__c> siteList = [SELECT Id, Site__c, Site__r.Name, Site__r.Site_Postal_Code__c from Site_Availability__c where Id in :siteAvailabilityIds];
        
        //site Id, List<Site_Availability__c> (all site availabilities in that site)>
        Map<Id, List<Site_Availability__c>> mapSiteSiteAv = new Map<Id, List<Site_Availability__c>>();
        
        for(Site_Availability__c sa : siteList){
            if(mapSiteSiteAv.containsKey(sa.Site__c)){
                mapSiteSiteAv.get(sa.Site__c).add(sa);
            }
            else{
                List<Site_Availability__c> saList = new List<Site_Availability__c>();
                saList.add(sa);
                mapSiteSiteAv.put(sa.Site__c, saList);
            }
        }
        
        return mapSiteSiteAv;
    }
    
    //
    
    public static Map<Id, List<Competitor_Asset__c>> getSiteAvailabilityPBXSorted(List<cscfga__Product_Configuration__c> basketConfigs){
         Map<Id, List<Competitor_Asset__c>> results = new Map<Id, List<Competitor_Asset__c>>();
         
         Map<Id, List<Id>> siteAVPBX = getSiteAvailabilityPBX(basketConfigs);
         
         results = getPBXPerSiteAvailability(siteAVPBX);
         return results;
         
    }
    
    public static Map<Id, List<Id>> getSiteAvailabilityPBX(List<cscfga__Product_Configuration__c> basketConfigs){
        /**
         * Map --> SiteAVailabilityId, PBX Id
         * */
        
        Map<Id, List<Id>> resultMap = new Map<Id, List<Id>>();
        
        List<cscfga__Product_Configuration__c> accessConfigs = getConfigPerName(basketConfigs, 'Access Infrastructure');
        
        for(cscfga__Product_Configuration__c config : accessConfigs){
            //check if advance cloned:
            if(config.ClonedSiteIds__c !=null && config.ClonedSiteIds__c!=''){
                //is advance cloned
                String jsonSites = config.ClonedSiteIds__c;
                Map<String, Object> jsonUns = (Map<String, Object>)JSON.deserializeUntyped(jsonSites);
                
                String jsonPBXs = config.ClonedPBXIds__c;
                Map<String, Object> jsonUnsPBX = (Map<String, Object>)JSON.deserializeUntyped(jsonPBXs);
                
                for(String s:jsonUns.keyset()){
                  Id siteId=(Id)jsonUns.get(s);
                  if(resultMap.containsKey(siteId)){
                      if(jsonUnsPBX.get(s) != 'null'){
                        Id pbxId = (Id)jsonUnsPBX.get(s);
                        resultMap.get(siteId).add(pbxId);                          
                      } 
                  }
                  else{
                      List<Id> pbxIds  = new List<Id>();
                    
                      if(jsonUnsPBX.get(s) != 'null'){
                        Id pbxId = (Id)jsonUnsPBX.get(s);
                        pbxIds.add(pbxId);
                        resultMap.put(siteId, pbxIds);
                      }                            
                      
                  }
                }
            }
            else{
                if(config.Site_Availability_Id__c != null && config.Site_Availability_Id__c!=''){
                    Id siteId=(Id)config.Site_Availability_Id__c;
                    if(resultMap.containsKey(siteId)){
                      resultMap.get(siteId).add(config.PBXId__c);
                    }
                    else{
                      List<Id> pbxId  = new List<Id>();
                      pbxId.add((Id)config.PBXId__c);
                      resultMap.put(siteId, pbxId);
                    }
                }
            }
        }
                
       return resultMap;    
    }
    //
    
    public static Map<Id, List<Id>> getMainProductPerSiteMapping(List<cscfga__Product_Configuration__c> basketConfigs){
        /**
         * Map --> SiteId, all Main products per configuration
         * */
        
        Map<Id, List<Id>> resultMap = new Map<Id, List<Id>>();
        
        List<cscfga__Product_Configuration__c> accessConfigs = getConfigPerName(basketConfigs, 'Access Infrastructure');
        
        for(cscfga__Product_Configuration__c config : accessConfigs){
            //check if advance cloned:
            if(config.ClonedSiteIds__c !=null && config.ClonedSiteIds__c!=''){
                //is advance cloned
                String jsonSites = config.ClonedSiteIds__c;
                Map<String, Object> jsonUns = (Map<String, Object>)JSON.deserializeUntyped(jsonSites);
                for(String s:jsonUns.keyset()){
                  Id siteId=(Id)jsonUns.get(s);
                  if(resultMap.containsKey(siteId)){
                      resultMap.get(siteId).add(config.Id);
                  }
                  else{
                      List<Id> siteConf  = new List<Id>();
                      siteConf.add(config.Id);
                      resultMap.put(siteId, siteConf);
                  }
                  //Products on Access: - OneNet, OneFixed, IPVPN, ManagedInterent
                  if(config.OneNet__c!=null && config.OneNet__c!=''){
                      resultMap.get(siteId).add((Id)config.OneNet__c);
                  }
                  if(config.OneFixed__c!=null && config.OneFixed__c!=''){
                      resultMap.get(siteId).add((Id)config.OneFixed__c);
                  }
                  if(config.IPVPN__c!=null && config.IPVPN__c!=''){
                      resultMap.get(siteId).add((Id)config.IPVPN__c);
                  }
                  
                  if(config.ManagedInternet__c!=null && config.ManagedInternet__c!=''){
                      resultMap.get(siteId).add((Id)config.ManagedInternet__c);
                  }
                }
            }
            else{
                if(config.Site_Availability_Id__c != null && config.Site_Availability_Id__c!=''){
                    Id siteId=(Id)config.Site_Availability_Id__c;
                    if(resultMap.containsKey(siteId)){
                      resultMap.get(siteId).add(config.Id);
                    }
                    else{
                      List<Id> siteConf  = new List<Id>();
                      siteConf.add(config.Id);
                      resultMap.put(siteId, siteConf);
                    }
                  //Products on Access: - OneNet, OneFixed, IPVPN, ManagedInterent
                    if(config.OneNet__c!=null && config.OneNet__c!=''){
                      resultMap.get(siteId).add((Id)config.OneNet__c);
                    }
                    if(config.OneFixed__c!=null && config.OneFixed__c!=''){
                      resultMap.get(siteId).add((Id)config.OneFixed__c);
                    }
                    if(config.IPVPN__c!=null && config.IPVPN__c!=''){
                      resultMap.get(siteId).add((Id)config.IPVPN__c);
                    }
                  
                    if(config.ManagedInternet__c!=null && config.ManagedInternet__c!=''){
                      resultMap.get(siteId).add((Id)config.ManagedInternet__c);
                    }
                }
            }
        }
                
       return resultMap;    
    }
    
    public static List<cscfga__Product_Configuration__c> getConfigPerName(List<cscfga__Product_Configuration__c> basketConfigs, String name){
        List<cscfga__Product_Configuration__c> result = new List<cscfga__Product_Configuration__c>();
        
        for(cscfga__Product_Configuration__c pc: basketConfigs){
            if(pc.Name == name){
                result.add(pc);
            }
        }
        
        return result;
    }
    
    public static cscfga__Product_Configuration__c getConfigPerId(Id configId, List<cscfga__Product_Configuration__c> basketConfigs){
        cscfga__Product_Configuration__c result = new cscfga__Product_Configuration__c();
        
        for(cscfga__Product_Configuration__c pc: basketConfigs){
            if(pc.Id == configId){
                result = pc;
            }
        }
        
        return result;
    }
    
     public static List<cscfga__Product_Configuration__c> getBasketConfigs(String basketId){
        Map<String, Schema.SObjectField> fieldMap = cscfga__Product_Configuration__c.sObjectType.getDescribe().fields.getMap();
        Set<String> fieldNamesSet = fieldMap.keySet();
        List<String> fieldNames = new List<String>(fieldNamesSet);
        
        String allFields = String.join(fieldNames, ',');
        String query = 'select ' + allFields + ' from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId';
        List<cscfga__Product_Configuration__c> configsPerBasket = Database.query(query);
        
        return configsPerBasket;
    }
    
    public static List<CS_Basket_Snapshot_Transactional__c> getBasketSnapshots(String basketId){
        Map<String, Schema.SObjectField> fieldMap = CS_Basket_Snapshot_Transactional__c.sObjectType.getDescribe().fields.getMap();
        Set<String> fieldNamesSet = fieldMap.keySet();
        List<String> fieldNames = new List<String>(fieldNamesSet);
        
        String allFields = String.join(fieldNames, ',');
        String query = 'SELECT ' + allFields + ', Parent_Product_Configuration__r.Name, RecurringProduct__r.Name, Product_Configuration__r.Deal_type__c, Product_Configuration__r.Commercial_Product__r.Mobile_Subscription__c,' +
            'Product_Configuration__r.Commercial_Product__r.Mobile_Scenario__c, Product_Configuration__r.Commercial_Product__r.Mobile_Add_on_category__c, Product_Configuration__r.Commercial_Product__r.Group__c, '+
            'Product_Configuration__r.cscfga__Quantity__c, Product_Configuration__r.cscfga__Contract_Term__c, Product_Configuration__r.Proposition__c, Product_Configuration__r.Mobile_Scenario__c, Product_Configuration__r.Name ' + 
            'FROM CS_Basket_Snapshot_Transactional__c WHERE Product_Basket__c = :basketId';
        List<CS_Basket_Snapshot_Transactional__c> snapshotsPerBasket = Database.query(query);
        
        return snapshotsPerBasket;
    }
    
    
    public static Map<Id, List<CS_Basket_Snapshot_Transactional__c>> summarizePerParent(List<CS_Basket_Snapshot_Transactional__c> basketSnapshots){
        Map<Id, List<CS_Basket_Snapshot_Transactional__c>> mapPerParent = new Map<Id, List<CS_Basket_Snapshot_Transactional__c>>();
        
        for(CS_Basket_Snapshot_Transactional__c snap : basketSnapshots){
            if(mapPerParent.containsKey(snap.Parent_Product_Configuration__c)){
                mapPerParent.get(snap.Parent_Product_Configuration__c).add(snap);
            }
            else{
                List<CS_Basket_Snapshot_Transactional__c> snapList = new List<CS_Basket_Snapshot_Transactional__c>();
                snapList.add(snap);
                mapPerParent.put(snap.Parent_Product_Configuration__c, snapList);
            }
        }
        
        return mapPerParent;
    }

    /*
        Groups snapshots two-fold
        First according to Connection type, which is actually DealType from related configuration, and then products pertaining same CTN subscription are kept together within same deal type
        Since CTN addons are created based on subscription, reference to that subscription is held in Originating_mobile_subscription__c and for them that is used as a reference for grouping
        Intended for mobile table creation - products are groupped by connection type, one off and recurring components are kept together
        Only specified connectionTypes are taken into account
        Parameter removeOneOffProducts - one off products are optionally removed
    */
    public static Map<String, List<CS_Basket_Snapshot_Transactional__c>> groupByConnectionType(List<CS_Basket_Snapshot_Transactional__c> basketSnapshots,
                    List<String> connectionTypes, Boolean removeOneOffProducts, String importantProductGroup){
        Map<String, Map<String, List<CS_Basket_Snapshot_Transactional__c>>> mapPerConnectionType = new Map<String, Map<String, List<CS_Basket_Snapshot_Transactional__c>>>();

        System.debug('Snap count: ' + basketSnapshots.size());
        // ensure that "important" snapshots are first
        List<CS_Basket_Snapshot_Transactional__c> basketSnapshotsSorted = new List<CS_Basket_Snapshot_Transactional__c>();
        if(importantProductGroup != null)
        {
            for(CS_Basket_Snapshot_Transactional__c singleSnap : basketSnapshots)
            {
                System.debug('Snap deal type: ' + singleSnap.Product_Configuration__r.Deal_type__c);
                if(singleSnap.ProductGroup__c == importantProductGroup  && basketSnapshotsSorted.size() > 0)
                    basketSnapshotsSorted.add(0,singleSnap);
                else
                    basketSnapshotsSorted.add(singleSnap);
            }
        }
        else {
            basketSnapshotsSorted.addAll(basketSnapshots);
        }

        for(CS_Basket_Snapshot_Transactional__c snap : basketSnapshotsSorted){
            // skip snapshot if connection type is not needed
            if(!connectionTypes.contains(snap.Product_Configuration__r.Deal_type__c))
                continue;

            // skip OneOff products in case of Retention, Migration and Disconnect
            if(removeOneOffProducts && snap.RecurringProduct__c == null && snap.OneOffProduct__c != null && (snap.Product_Configuration__r.Deal_type__c== 'Retention' ||
                snap.Product_Configuration__r.Deal_type__c== 'Disconnect' || snap.Product_Configuration__r.Deal_type__c== 'Migration'))
                continue;

            if(mapPerConnectionType.containsKey(snap.Product_Configuration__r.Deal_type__c)){
                Map<String, List<CS_Basket_Snapshot_Transactional__c>> existingMap = mapPerConnectionType.get(snap.Product_Configuration__r.Deal_type__c);
                // not all snaps have Originating_mobile_subscription__c - only CTN Addons
                String comparisonItem = snap.Originating_mobile_subscription__c != null ? snap.Originating_mobile_subscription__c : snap.Product_Configuration__c;
                if(existingMap.containsKey(comparisonItem))
                {
                    if(snap.ProductGroup__c == importantProductGroup  && existingMap.get(comparisonItem).size() > 0)
                        existingMap.get(comparisonItem).add(0,snap);
                    else
                        existingMap.get(comparisonItem).add(snap);
                }
                else {
                    List<CS_Basket_Snapshot_Transactional__c> snapList = new List<CS_Basket_Snapshot_Transactional__c>();
                    if(snap.ProductGroup__c == importantProductGroup  && snapList.size() > 0)
                        snapList.add(0,snap);
                    else
                        snapList.add(snap);
                    existingMap.put(comparisonItem, snapList);
                }
            }
            else{
                Map<String, List<CS_Basket_Snapshot_Transactional__c>> newMap = new Map<String, List<CS_Basket_Snapshot_Transactional__c>>();
                mapPerConnectionType.put(snap.Product_Configuration__r.Deal_type__c, newMap);
                List<CS_Basket_Snapshot_Transactional__c> snapList = new List<CS_Basket_Snapshot_Transactional__c>();
                    if(snap.ProductGroup__c == importantProductGroup && snapList.size() > 0)
                        snapList.add(0,snap);
                    else
                        snapList.add(snap);
                String comparisonItem = snap.Originating_mobile_subscription__c != null ? snap.Originating_mobile_subscription__c : snap.Product_Configuration__c;
                newMap.put(comparisonItem, snapList);
            }
        }

        Map<String, List<CS_Basket_Snapshot_Transactional__c>> sortedPerConnectionType = new Map<String, List<CS_Basket_Snapshot_Transactional__c>>();
        // sort according to input list of connection types, flatten the snapshots in single list for connection type
        for(String connectionType : connectionTypes)
        {
            Map<String, List<CS_Basket_Snapshot_Transactional__c>> mappedSnaps = mapPerConnectionType.get(connectionType);
            if(mappedSnaps == null) 
                continue;
            List<CS_Basket_Snapshot_Transactional__c> organizedSnaps = new List<CS_Basket_Snapshot_Transactional__c>();
            for(Id configId : mappedSnaps.keySet())
            {
                organizedSnaps.addAll(mappedSnaps.get(configId));
            }
            sortedPerConnectionType.put(connectionType, organizedSnaps);
        }
        
        return sortedPerConnectionType;
    }
    
    public static Map<String, List<CS_Basket_Snapshot_Transactional__c>> perGroup(List<CS_Basket_Snapshot_Transactional__c> basketSnapshots){
        Map<String, List<CS_Basket_Snapshot_Transactional__c>> mapPerGroup = new  Map<String, List<CS_Basket_Snapshot_Transactional__c>>();
        
        for(CS_Basket_Snapshot_Transactional__c snap : basketSnapshots){
            if(mapPerGroup.containsKey(snap.ProductGroup__c)){
                mapPerGroup.get(snap.ProductGroup__c).add(snap);
            }
            else{
                List<CS_Basket_Snapshot_Transactional__c> snapList = new List<CS_Basket_Snapshot_Transactional__c>();
                snapList.add(snap);
                mapPerGroup.put(snap.ProductGroup__c, snapList);
            }
        }
        
        return mapPerGroup;
    }
    
    
    public static Map<Id, Map<String,List<CS_Basket_Snapshot_Transactional__c>>> procesPerParentPerGroup(List<CS_Basket_Snapshot_Transactional__c> snapsPerBasket){
        Map<Id, Map<String,List<CS_Basket_Snapshot_Transactional__c>>> result = new Map<Id, Map<String,List<CS_Basket_Snapshot_Transactional__c>>>();
        Map<Id, List<CS_Basket_Snapshot_Transactional__c>> resultPerParent = summarizePerParent(snapsPerBasket);
        
        for(Id parent : resultPerParent.keySet()){
            if(!result.containsKey(parent)){
                result.put(parent, perGroup(resultPerParent.get(parent)));
            }
        }
        return result;
    }
    
    public static List<CS_Basket_Snapshot_Transactional__c> getSnapshotsByParentConfigName(List<CS_Basket_Snapshot_Transactional__c> snaps, String parentConfigName)
    {
        List<CS_Basket_Snapshot_Transactional__c> resultSnaps = new List<CS_Basket_Snapshot_Transactional__c>();
        for(CS_Basket_Snapshot_Transactional__c snap : snaps)
        {
            if(snap.Parent_Product_Configuration__r.Name == parentConfigName)
                resultSnaps.add(snap);
        }

        return resultSnaps;
    }

    public static List<CS_Basket_Snapshot_Transactional__c> getSnapshotsByProductGroup(List<CS_Basket_Snapshot_Transactional__c> snaps, String productGroup)
    {
        List<CS_Basket_Snapshot_Transactional__c> resultSnaps = new List<CS_Basket_Snapshot_Transactional__c>();
        for(CS_Basket_Snapshot_Transactional__c snap : snaps)
        {
            if(snap.ProductGroup__c == productGroup)
                resultSnaps.add(snap);
        }

        return resultSnaps;
    }
    
    public static List<CS_Basket_Snapshot_Transactional__c> getSnapshotsByGroup(List<CS_Basket_Snapshot_Transactional__c> snaps, String productGroup) {
        List<CS_Basket_Snapshot_Transactional__c> resultSnaps = new List<CS_Basket_Snapshot_Transactional__c>();
        
        for (CS_Basket_Snapshot_Transactional__c snap : snaps) {
            if (snap.Product_Configuration__r.Commercial_Product__r.Group__c != null && snap.Product_Configuration__r.Commercial_Product__r.Group__c.contains(productGroup)) {
                resultSnaps.add(snap);
            }
        }
        
        return resultSnaps;
    }  
}