@isTest
public with sharing class CS_SnapshotHelperTest {
    public CS_SnapshotHelperTest() {}
    
    @TestSetup
	private static void testSetup()
	{
	    List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            OrderType__c orderTypeTest = CS_DataTest.createOrderType('Order Type Test');
            orderTypeTest.ExportSystem__c = 'SIAS';
            orderTypeTest.Status__c = 'New';
            insert orderTypeTest;
            
            Product2 productTest = CS_DataTest.createProduct('test product', orderTypeTest);
            productTest.Not_on_contract__c = false;
            insert productTest;
            
            Category__c categoryTest = CS_DataTest.createCategory('test');
            insert categoryTest;
            
            Vendor__c vendorTest = CS_DataTest.createVendor('test');
            insert vendorTest;
            
            cspmb__Price_Item__c priceItemTest = CS_DataTest.createPriceItem(productTest, categoryTest, 100, 10, vendorTest, 'test', 'test');
            insert priceItemTest;
            
            PriceReset__c priceResetSetting = new PriceReset__c();
            
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
            
            insert priceResetSetting;

            Account testAccount = CS_DataTest.createAccount('Test Account 1');
            Account testAccount2 = CS_DataTest.createAccount('Test Account 2');
            List<Account> accList = new List<Account>{testAccount,testAccount2};
            insert accList;
            
            Contact contact1 = new Contact(
                AccountId = testAccount.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'   
            );
            insert contact1;
        
            testAccount.Authorized_to_sign_1st__c = contact1.Id;
            testAccount.Contract_rule_no_mailing__c = true;
            testAccount.Frame_Work_Agreement__c = 'VFZA-2018-8';
            testAccount.Version_FWA__c = 4;
            testAccount.Framework_agreement_date__c = Date.today();
            update testAccount;
            
            Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Contract opportunity',simpleUser.id);
            Opportunity opp2 = new Opportunity();
            opp2.Name = 'testName';
            opp2.Account = testAccount2;
            opp2.StageName= 'Qualification';
            opp2.CloseDate = Date.Today()+10;
            
            List<Opportunity> oppList = new List<Opportunity>{opp, opp2};
            insert oppList;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Contract Basket');
            basket.Name = 'Contract Basket';
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = testAccount.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 24;
            basket.OneNet_Scenario__c = 'One Net Enterprise';
            basket.Fixed_Scenario__c = 'One Fixed Enterprise';
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 02, 02);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 02, 02);
            basket.Number_of_SIP__c = 12;
            basket.Approved_Date__c = Date.today() - 4;

            insert basket;
            
            cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
            accessInfraDef.Product_Type__c = 'Fixed';

            cscfga__Product_Definition__c managedInternetDef = CS_DataTest.createProductDefinition('Managed Internet');
            managedInternetDef.Product_Type__c = 'Fixed';
            
            cscfga__Product_Definition__c ipvpnDef = CS_DataTest.createProductDefinition('IPVPN');
            ipvpnDef.Product_Type__c = 'Fixed';

            cscfga__Product_Definition__c oneFixedDef = CS_DataTest.createProductDefinition('One Fixed');
            oneFixedDef.Product_Type__c = 'Fixed';
            
            cscfga__Product_Definition__c vodAccessDefn = CS_DataTest.createProductDefinition('Vodafone Access');
            vodAccessDefn.Product_Type__c = 'Fixed';
            
            cscfga__Product_Definition__c mobCTNSubs = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
            mobCTNSubs.Product_Type__c = 'Fixed';
            
            List<cscfga__Product_Definition__c> prodDefList = new List<cscfga__Product_Definition__c>{accessInfraDef,managedInternetDef,ipvpnDef,oneFixedDef,vodAccessDefn,mobCTNSubs};
            insert prodDefList;
            
            Site__c testSite = CS_DataTest.createSite('testSite', testAccount, '1234AA', 'teststreet', 'testTown', 3);
            insert testSite;
            
            Site_Availability__c testSA = new Site_Availability__c();
            testSA.Name = 'testSA';
            testSA.Site__c = testSite.Id;
            testSA.Vendor__c = 'ZIGGO'; 
            testSA.Premium_Vendor__c = 'TELECITY'; 
            testSA.Access_Infrastructure__c ='Coax'; 
            testSA.Bandwith_Down_Entry__c = 1; 
            testSA.Bandwith_Up_Entry__c = 10; 
            testSA.Bandwith_Down_Premium__c = 1; 
            testSA.Bandwith_Up_Premium__c = 10;
            insert testSA;
            
            PBX_Type__c PBXType = new PBX_Type__c();
            PBXType.Name = 'Test PBX Type';
            PBXType.Software_version__c = '3.5';
            PBXType.Vendor__c = 'ZIGGO';
            PBXType.Product_Name__c = 'test';
            insert PBXType;
            
            cscfga__Product_Configuration__c managedInternetConf = CS_DataTest.createProductConfiguration(managedInternetDef.Id, 'Mobile CTN Profile',basket.Id);
            managedInternetConf.Deal_Type__c = 'Acquisition';
            managedInternetConf.Mobile_Scenario__c = 'test';
            managedInternetConf.IPVPN__c = 'a7Z2p0000000000000';
            managedInternetConf.ManagedInternet__c = CS_DataTest.generateRandomString(10);
    
            cscfga__Product_Configuration__c pcAccessInf = createProductConfig(accessInfraDef.Id, 'Access Infrastructure', basket.Id,
            null, 'test');
            pcAccessInf.OneNet_Scenario__c = 'One Fixed';
            pcAccessInf.OneFixed__c = 'test';
            pcAccessInf.Fixed_Scenario__c = 'test';
            pcAccessInf.IPVPN__c = 'a7Z2p0000000000000';
            pcAccessInf.cscfga__Product_Family__c = 'Access Infrastructure';
            pcAccessInf.PBXId__c = PBXType.Id;
            pcAccessInf.cspl__Type__c = 'Mobile Voice Services';
            pcAccessInf.Site_Availability_Id__c = testSA.Id;

    
            cscfga__Product_Configuration__c pcOneFixed = createProductConfig(oneFixedDef.Id, 'One Fixed', basket.Id,
            pcAccessInf.Id, 'test');
            
            cscfga__Product_Configuration__c pcSkype = createProductConfig(oneFixedDef.Id, 'Skype for Business', basket.Id,
            pcAccessInf.Id, 'test');
            
            cscfga__Product_Configuration__c pcBMS = createProductConfig(oneFixedDef.Id, 'Business Managed Services', basket.Id,
            pcAccessInf.Id, 'test');
            
            cscfga__Product_Configuration__c pcMobCTN = createProductConfig(oneFixedDef.Id, 'Mobile CTN profile', basket.Id,
            pcAccessInf.Id, 'OneBusiness');
            
            cscfga__Product_Configuration__c pcMobCTN2 = createProductConfig(oneFixedDef.Id, 'Mobile CTN profile', basket.Id,
            pcAccessInf.Id, 'Data only');
            
            cscfga__Product_Configuration__c pcMobCTN3 = createProductConfig(oneFixedDef.Id, 'Mobile CTN profile', basket.Id,
            pcAccessInf.Id, CS_Constants.REDPRO_SCENARIO_NEW_PORTING);
            
            cscfga__Product_Configuration__c pcMobCTN4 = createProductConfig(oneFixedDef.Id, 'Mobile CTN profile', basket.Id,
            pcAccessInf.Id, 'OneBusiness IOT');
            
            cscfga__Product_Configuration__c pcMobCTN5 = createProductConfig(oneFixedDef.Id, 'Mobile CTN profile', basket.Id,
            pcAccessInf.Id, 'OneMobile');
            
            cscfga__Product_Configuration__c pcMobCTN6 = createProductConfig(mobCTNSubs.Id, 'Mobile CTN profile', basket.Id,
            pcAccessInf.Id, 'Mobile BAN profile');
            
            cscfga__Product_Configuration__c pcVodCal = createProductConfig(vodAccessDefn.Id, 'Vodafone Calling', basket.Id,
            pcAccessInf.Id, 'test');
            
            cscfga__Product_Configuration__c pcOneNet = createProductConfig(vodAccessDefn.Id, 'One Net', basket.Id,
            pcAccessInf.Id, 'test');
            pcOneNet.OneNet_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcCLFV = createProductConfig(vodAccessDefn.Id, 'Company Level Fixed Voice', basket.Id,
            pcAccessInf.Id, 'test');
            pcCLFV.OneNet_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcWOS = createProductConfig(vodAccessDefn.Id, 'Wireless Only Standalone', basket.Id,
            pcAccessInf.Id, 'test');
            pcWOS.OneNet_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcMobBAN = createProductConfig(mobCTNSubs.Id, 'Mobile BAN profile', basket.Id,
            pcAccessInf.Id, 'Mobile BAN profile');

            cscfga__Product_Configuration__c pcMobCTN7 = createProductConfig(oneFixedDef.Id, 'Mobile CTN profile', basket.Id,
            pcAccessInf.Id, CS_Constants.REDPRO_SCENARIO_RETENTION_MIGRATION);
            
            List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>{managedInternetConf, pcAccessInf, pcOneFixed, pcSkype, pcBMS, pcMobCTN, pcMobCTN2, pcMobCTN3, pcMobCTN4, pcMobCTN5, pcMobCTN6, pcVodCal, pcOneNet, pcCLFV, pcWOS, pcMobBAN, pcMobCTN7};
            insert pcList;
            
            pcAccessInf.OneFixed__c = pcOneFixed.id;
            update pcAccessInf;
    
            CS_Basket_Snapshot_Transactional__c accessSnap = CS_DataTest.createBasketSnapshotTransactional('Snap 1', basket.Id, null, pcAccessInf.Id);
            insert accessSnap;
            
            Contract_Conditions__c contractcond1 = new Contract_Conditions__c();
            contractcond1.Name = 'test1';
            contractcond1.Category__c = 'test1';
            contractcond1.Version_Number__c = 1.0;
            contractcond1.ExternalID__c = CS_DataTest.generateRandomString(10);
            
            insert contractcond1;
            
            CS_Basket_Snapshot_Transactional__c basketSnapTrans = createBasketSnapshot('Access Infrastructure', basket.id, pcOneFixed.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);

        	CS_Basket_Snapshot_Transactional__c basketSnapTrans2 = createBasketSnapshot('Vodafone Calling', basket.id, pcVodCal.id, productTest.Name, 'WorryFree', 
            productTest.Id, opp.id, accessInfraDef.id);
            
            CS_Basket_Snapshot_Transactional__c basketSnapTrans3 = createBasketSnapshot('Mobile BAN profile', basket.id, pcMobBAN.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	CS_Basket_Snapshot_Transactional__c basketSnapTrans4 = createBasketSnapshot('Mobile CTN profile', basket.id, pcMobCTN6.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	CS_Basket_Snapshot_Transactional__c basketSnapTrans5 = createBasketSnapshot('Mobile CTN profile2', basket.id, pcMobCTN5.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	CS_Basket_Snapshot_Transactional__c basketSnapTrans6 = createBasketSnapshot('Mobile CTN profile3', basket.id, pcMobCTN4.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	CS_Basket_Snapshot_Transactional__c basketSnapTrans7 = createBasketSnapshot('Mobile CTN profile4', basket.id, pcMobCTN3.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	CS_Basket_Snapshot_Transactional__c basketSnapTrans8 = createBasketSnapshot('Mobile CTN profile5', basket.id, pcMobCTN2.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	CS_Basket_Snapshot_Transactional__c basketSnapTrans9 = createBasketSnapshot('Mobile CTN profile6', basket.id, pcMobCTN.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	CS_Basket_Snapshot_Transactional__c basketSnapTrans10 = createBasketSnapshot('Business Managed Services', basket.id, pcBMS.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	CS_Basket_Snapshot_Transactional__c basketSnapTrans11 = createBasketSnapshot('Skype for Business', basket.id, pcSkype.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	CS_Basket_Snapshot_Transactional__c basketSnapTrans12 = createBasketSnapshot('Access Infrastructure', basket.id, pcAccessInf.id, productTest.Name, 'test', 
            productTest.Id, opp.id, accessInfraDef.id);
        	
        	List<CS_Basket_Snapshot_Transactional__c> BSTList = new List<CS_Basket_Snapshot_Transactional__c>{basketSnapTrans, basketSnapTrans2, basketSnapTrans3, basketSnapTrans4,basketSnapTrans5, basketSnapTrans6, 
        	                                                                                               basketSnapTrans7, basketSnapTrans8, basketSnapTrans9, basketSnapTrans10, basketSnapTrans11, basketSnapTrans12};
        	insert BSTList;
        	
            csclm__Clause_Definition__c clauseDefn = new csclm__Clause_Definition__c();
            clauseDefn.ExternalID__c = CS_DataTest.generateRandomString(10);
        	clauseDefn.Promo_start_date__c = Date.today() - 10;
        	clauseDefn.Promo_end_date__c = Date.today() - 3;
        	clauseDefn.DynamicId__c = 'test';
        	insert clauseDefn;
        	
        	csclm__Transactional_Clause__c transClauseTest = new csclm__Transactional_Clause__c();
        	transClauseTest.csclm__Clause_Definition__c = clauseDefn.Id;
        	transClauseTest.csclm__Final_Rich_Text__c= '#!skype1!# and #!skype2!# and #!fup1!# and #!fup2!# and One Mobile and ONE Mobile';
        	insert transClauseTest;
        	
            csclm__Section_Definition__c secDefn = new csclm__Section_Definition__c();
            secDefn.ExternalID__c = CS_DataTest.generateRandomString(10);
        	secDefn.csclm__Section_Name__c = 'test';
        	secDefn.Start_on_new_page__c = true;
        	insert secDefn;
        	
        	csclm__Section_Clause_Definition_Association__c secClaDefAss = new csclm__Section_Clause_Definition_Association__c();
        	secClaDefAss.csclm__Clause_Definition__c= clauseDefn.id;
        	secClaDefAss.csclm__Section_Definition__c= secDefn.Id;
            secClaDefAss.ExternalID__c = CS_DataTest.generateRandomString(10);
        	insert secClaDefAss;
        	
            csclm__Document_Definition__c docDefn = CS_DataTest.createDocumentDefinition();
            docDefn.ExternalID__c = CS_DataTest.generateRandomString(10);
        	docDefn.Name = 'test';
        	insert docDefn;
        	
        	csclm__Document_Template__c docTemp = CS_DataTest.createDocumentTemplate('Direct Sales Template');
        	docTemp.csclm__Approval_Type__c = 'No Approval';
        	docTemp.csclm__Document_Definition__c = docDefn.Id;
        	
        	csclm__Document_Template__c docTemp2 = CS_DataTest.createDocumentTemplate('Direct Sales Template');
        	docTemp2.csclm__Approval_Type__c = 'No Approval';
        	docTemp2.csclm__Document_Definition__c = docDefn.Id;
        	
        	csclm__Document_Template__c docTemp3 = CS_DataTest.createDocumentTemplate('Direct Sales Template');
        	docTemp3.csclm__Approval_Type__c = 'No Approval';
        	docTemp3.csclm__Document_Definition__c = docDefn.Id;
        	
        	List<csclm__Document_Template__c> docTempList = new List<csclm__Document_Template__c> {docTemp, docTemp2, docTemp3};
        	insert docTempList;
        	
        	pcWOS.Site_Check_SiteID__c = String.valueOf(testSite.Id);
        	update pcWOS;
        	
        	Competitor_Asset__c compAss = new Competitor_Asset__c();
        	compAss.Account__c = testAccount.Id;
        	compAss.Site__c = testSite.Id;
        	compAss.PBX_type__c = PBXType.Id;
            insert compAss;
            }
    }
    
    private static cscfga__Product_Configuration__c createProductConfig(Id productDefId, String productConfigName, Id basketId,
    Id parentConfigId, String mobileScenario) {
        cscfga__Product_Configuration__c config = CS_DataTest.createProductConfiguration(productDefId, productConfigName, basketId);
        config.Deal_Type__c = 'Acquisition';
        config.cscfga__Contract_Term__c = 24;
        config.cscfga__total_one_off_charge__c = 100;
        config.cscfga__Total_Price__c = 20;
        config.Number_of_SIP__c = 20;
        if (parentConfigId != null) config.cscfga__Parent_Configuration__c = parentConfigId;
        config.Number_of_SIP_Clone_Multiplicator__c = 2;
        config.cscfga__Contract_Term_Period__c = 12;
        config.cscfga__Configuration_Status__c = 'Valid';
        config.cscfga__Quantity__c = 1;
        config.cscfga__total_recurring_charge__c = 22;
        config.RC_Cost__c = 0;
        config.NRC_Cost__c = 0;
        config.cscfga__one_off_charge_product_discount_value__c = 0;
        config.cscfga__recurring_charge_product_discount_value__c = 0;
        config.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
        config.Mobile_Scenario__c = mobileScenario;
        
        return config;
    }
    
    private static CS_Basket_Snapshot_Transactional__c createBasketSnapshot(String basketSnapName, Id basketId, Id parentConfig, String productName, String productGroup, 
    Id productId, Id opportunityId, Id productDefId) {
        CS_Basket_Snapshot_Transactional__c snap = CS_DataTest.createBasketSnapshotTransactional(basketSnapName, basketId, parentConfig, productName, productGroup);
        snap.Proposition__c = 'test';
        snap.RecurringProduct__c = productId;
        snap.OneOffProduct__c = productId;
        snap.Opportunity__c = opportunityId;
        snap.Product_Definition__c = productDefId;
        snap.Quantity__c = 1;
        snap.DiscountOneOff__c = 10;
        snap.DiscountRecurringPercentage__c = 5;
        snap.Service__c = 'test';
        snap.One_Off_Charge_DocArea__c = 'test';
        snap.One_Off_Charge_DocArea__c = 'test';
        snap.OneOffPrice__c = 100;
        snap.RecurringPrice__c = 20;
        snap.FinalPriceRecurring__c = 240;
        snap.FinalPriceOneOff__c = 90;
        snap.CCodeOneOff__c = 'test';
        snap.CCodeRecurring__c = 'test';
        snap.ConnectionType__c = 'test';
        snap.OneOffName__c = 'test';
        snap.DiscountOneOffPercentage__c = 5;
        snap.Treshold_Amount__c = 3;
        
        return snap;
    }
    
    @isTest
    static void testGetTransactionalClausePerTableId()
    {
        List<csclm__Transactional_Clause__c> clauseList = [SELECT id, TableIdFormula__c FROM csclm__Transactional_Clause__c];
        String tableId = clauseList[0].TableIdFormula__c;
        List<csclm__Transactional_Clause__c> res;
        Test.startTest();
        res = CS_SnapshotHelper.getTransactionalClausePerTableId(clauseList, tableId);
        Test.stopTest();
        
        System.assertNotEquals(res.size(), 0);
    }
    
    @isTest
    static void testQueryTransactionalClauses()
    {
        List<csclm__Transactional_Clause__c> clauseList = [SELECT id, TableIdFormula__c FROM csclm__Transactional_Clause__c];
        String tableId = clauseList[0].TableIdFormula__c;
        Set<String> tableIds = new Set<String>{tableId};
        List<csclm__Transactional_Clause__c> res;
        Test.startTest();
        res = CS_SnapshotHelper.queryTransactionalClauses(tableIds);
        Test.stopTest();
        
        System.assertNotEquals(res.size(), 0);
    }
    
    @isTest
    static void testQueryPBX()
    {
        List<Competitor_Asset__c> clauseList = [SELECT id FROM Competitor_Asset__c];
        String pbxid = clauseList[0].Id;
        Set<Id> pbxids = new Set<Id>{pbxid};
        List<Competitor_Asset__c> res;
        Test.startTest();
        res = CS_SnapshotHelper.queryPBX(pbxids);
        Test.stopTest();
        
        System.assertNotEquals(res.size(), 0);
    }
    
    @isTest
    static void testGetAllSiteNamesPerSiteId()
    {
        Map<Id, Site__c> siteMap = new Map<Id, Site__c>([SELECT Id, Name FROM Site__c]);
        Test.startTest();
        Map<Id, Site__c> res = CS_SnapshotHelper.getAllSiteNamesPerSiteId(siteMap.keySet());
        Test.stopTest();
        System.assert(res.keySet().size() > 0);
    }
    
    @isTest
    static void testGetAllMainSites()
    {
        List<cscfga__Product_Configuration__c> basketConfigs = [SELECT Id, Name, cscfga__Root_Configuration__c, Site_Check_SiteID__c FROM cscfga__Product_Configuration__c];
        Test.startTest();
        Set<Id> res = CS_SnapshotHelper.getAllMainSites(basketConfigs);
        Test.stopTest();
        System.assertNotEquals(res.size(), 0);
    }
    
    @isTest
    static void testGetSiteNamePerSiteAvailability()
    {
        Map<Id, Site_Availability__c> siteAvailabilityMap = new Map<Id, Site_Availability__c>([SELECT Id, Name FROM Site_Availability__c]);
        Test.startTest();
        Map<Id, List<Site_Availability__c>> res = CS_SnapshotHelper.getSiteNamePerSiteAvailability(siteAvailabilityMap.keySet());
        Test.stopTest();
        System.assertNotEquals(res.keySet().size(), 0);
    }
    
    @isTest
    static void testGetSiteAvailabilityPBXSorted()
    {
        List<cscfga__Product_Configuration__c> basketConfigs = [SELECT Id, ClonedSiteIds__c, Site_Availability_Id__c, PBXId__c, Name FROM cscfga__Product_Configuration__c];
        Test.startTest();
        Map<Id, List<Competitor_Asset__c>> res = CS_SnapshotHelper.getSiteAvailabilityPBXSorted(basketConfigs);
        Test.stopTest();
        System.assertNotEquals(0, res.size());
    }
    
    @isTest
    static void testGetSiteAvailabilityPBX() 
    {
        List<cscfga__Product_Configuration__c> basketConfigs = [SELECT Id, ClonedSiteIds__c, Site_Availability_Id__c, PBXId__c, Name FROM cscfga__Product_Configuration__c];
        Test.startTest();
        Map<Id, List<Id>> res = CS_SnapshotHelper.getSiteAvailabilityPBX(basketConfigs);
        Test.stopTest();
        System.assertNotEquals(res.keySet().size(), 0);
    }
    
    @isTest
    static void testGetMainProductPerSiteMapping()
    {
        List<cscfga__Product_Configuration__c> basketConfigs = [SELECT Id, Site_Availability_Id__c, PBXId__c, Name, ClonedSiteIds__c, OneNet__c, OneFixed__c, IPVPN__c, ManagedInternet__c FROM cscfga__Product_Configuration__c];
        Test.startTest();
        Map<Id, List<Id>> res = CS_SnapshotHelper.getMainProductPerSiteMapping(basketConfigs);
        Test.stopTest();
        System.assertNotEquals(res.keySet().size(), 0);
    }
    
    @isTest
    static void testGetConfigPerName() 
    {
        List<cscfga__Product_Configuration__c> basketConfigs = [SELECT Id, Name FROM cscfga__Product_Configuration__c];
        Test.startTest();
        List<cscfga__Product_Configuration__c> res = CS_SnapshotHelper.getConfigPerName(basketConfigs, 'Access Infrastructure');
        Test.stopTest();
        System.assertEquals('Access Infrastructure', res[0].Name);
    }
    
    @isTest
    static void testGetConfigPerId()
    {
        List<cscfga__Product_Configuration__c> basketConfigs = [SELECT Id, Name FROM cscfga__Product_Configuration__c];
        Test.startTest();
        cscfga__Product_Configuration__c res = CS_SnapshotHelper.getConfigPerId(basketConfigs[0].Id, basketConfigs);
        Test.stopTest();
        System.assertEquals(basketConfigs[0].Id, res.Id);
    }
    
    @isTest
    static void testGetBasketConfigs()
    {
        List<cscfga__Product_Basket__c> basket = [SELECT Id FROM cscfga__Product_Basket__c];
        Test.startTest();
        List<cscfga__Product_Configuration__c> res = CS_SnapshotHelper.getBasketConfigs(basket[0].Id);
        Test.stopTest();
        System.assertNotEquals(0, res.size());
    }
    
    @isTest
    static void testGetBasketSnapshots()
    {
        List<cscfga__Product_Basket__c> basket = [SELECT Id FROM cscfga__Product_Basket__c];
        Test.startTest();
        List<CS_Basket_Snapshot_Transactional__c> res = CS_SnapshotHelper.getBasketSnapshots(basket[0].Id);
        Test.stopTest();
        System.assertNotEquals(0, res.size());
    }
    
    @isTest
    static void testSummarizePerParent()
    {
        List<CS_Basket_Snapshot_Transactional__c> basketSnapshots = [SELECT Id, Name, Parent_Product_Configuration__c FROM CS_Basket_Snapshot_Transactional__c];
        Test.startTest();
        Map<Id, List<CS_Basket_Snapshot_Transactional__c>> res = CS_SnapshotHelper.summarizePerParent(basketSnapshots);
        Test.stopTest();
        System.assertNotEquals(res.keySet().size(), 0);
    }
    
    @isTest
    static void testGroupByConnectionType() 
    {
        List<CS_Basket_Snapshot_Transactional__c> basketSnapshots = [SELECT Id, Name, Parent_Product_Configuration__c, Product_Configuration__r.Deal_type__c, 
                                                                    ProductGroup__c, RecurringProduct__c, OneOffProduct__c, Originating_mobile_subscription__c, 
                                                                    Product_Configuration__c FROM CS_Basket_Snapshot_Transactional__c];
        List<String> connectionTypes = new List<String>();
        connectionTypes.add('test');
        connectionTypes.add('test2');
        connectionTypes.add('Acquisition');
        Test.startTest();
        Map<String, List<CS_Basket_Snapshot_Transactional__c>> res = CS_SnapshotHelper.groupByConnectionType(basketSnapshots, connectionTypes, false, null);
        Map<String, List<CS_Basket_Snapshot_Transactional__c>> res2 = CS_SnapshotHelper.groupByConnectionType(basketSnapshots, connectionTypes, true, 'test');
        Test.stopTest();
        System.assertNotEquals(0, res2.keySet().size());
        System.assertNotEquals(0, res.keySet().size());
    }
    
    @isTest
    static void testPerGroup() 
    {
        List<CS_Basket_Snapshot_Transactional__c> basketSnapshots = [SELECT Id, Name, ProductGroup__c FROM CS_Basket_Snapshot_Transactional__c];
        Test.startTest();
        Map<String, List<CS_Basket_Snapshot_Transactional__c>> res = CS_SnapshotHelper.perGroup(basketSnapshots);
        Test.stopTest();
        System.assertNotEquals(res.keySet().size(), 0);
    }
    
    @isTest
    static void testProcesPerParentPerGroup()
    {
        List<CS_Basket_Snapshot_Transactional__c> basketSnapshots = [SELECT Id, Name, Parent_Product_Configuration__c, ProductGroup__c FROM CS_Basket_Snapshot_Transactional__c];
        Test.startTest();
        Map<Id, Map<String,List<CS_Basket_Snapshot_Transactional__c>>> res = CS_SnapshotHelper.procesPerParentPerGroup(basketSnapshots);
        Test.stopTest();
        System.assertNotEquals(res.keySet().size(), 0);
    }
    
    @isTest
    static void testGetSnapshotsByParentConfigName()
    {
        List<CS_Basket_Snapshot_Transactional__c> basketSnapshots = [SELECT Id, Name, Parent_Product_Configuration__r.Name FROM CS_Basket_Snapshot_Transactional__c];
        Test.startTest();
        List<CS_Basket_Snapshot_Transactional__c> res = CS_SnapshotHelper.getSnapshotsByParentConfigName(basketSnapshots, basketSnapshots[0].Parent_Product_Configuration__r.Name);
        Test.stopTest();
        System.assertNotEquals(0, res.size());
    }
    
    @isTest
    static void testGetSnapshotsByProductGroup() {
        List<CS_Basket_Snapshot_Transactional__c> basketSnapshots = [SELECT Id, Name, ProductGroup__c FROM CS_Basket_Snapshot_Transactional__c];
        Test.startTest();
        List<CS_Basket_Snapshot_Transactional__c> res = CS_SnapshotHelper.getSnapshotsByProductGroup(basketSnapshots, 'test');
        Test.stopTest();
        System.assertNotEquals(0, res.size());
    }
    
    @isTest
    static void testGetSnapshotsByGroup() {
        List<CS_Basket_Snapshot_Transactional__c> basketSnapshots = [
            SELECT Product_Configuration__c
            FROM CS_Basket_Snapshot_Transactional__c
            LIMIT 1
        ];
        cspmb__Price_Item__c priceItem = [SELECT Group__c FROM cspmb__Price_Item__c LIMIT 1];
        cscfga__Product_Configuration__c productConfig = [SELECT Commercial_Product__c FROM cscfga__Product_Configuration__c LIMIT 1];
        
        productConfig.Commercial_Product__c = priceItem.Id;
        
        update productConfig;
        
        basketSnapshots[0].Product_Configuration__c = productConfig.Id;
        
        update basketSnapshots;
        
        basketSnapshots = [
            SELECT Product_Configuration__r.Commercial_Product__r.Group__c
            FROM CS_Basket_Snapshot_Transactional__c
            WHERE Id = :basketSnapshots[0].Id
        ];
        
        basketSnapshots[0].Product_Configuration__r.Commercial_Product__r.Group__c = 'test';
        
        List<CS_Basket_Snapshot_Transactional__c> res = CS_SnapshotHelper.getSnapshotsByGroup(basketSnapshots, 'test');
        
        System.assertEquals(true, res.size() > 0);   
    }   
}