public with sharing class CS_TransactionalClauseTriggerHandler {
    public CS_TransactionalClauseTriggerHandler() {

    }

    public static void beforeInsert(object[] objectTrigger){
        List<csclm__Transactional_Clause__c> transactionalClauses = (List<csclm__Transactional_Clause__c>) objectTrigger;

        //system.debug('**** transactionalClauses before: ' + JSON.serializePretty(transactionalClauses, true));

        for(csclm__Transactional_Clause__c tc : transactionalClauses){
            if(tc.Promo_dates_filled__c && tc.Promo_active__c){
                tc.csclm__Marked_For_Inclusion__c = false;
            }
        }

        system.debug('**** transactionalClauses before insert: ' + JSON.serializePretty(transactionalClauses, true));
    }

    public static void afterInsert(object[] objectTrigger){
        List<csclm__Transactional_Clause__c> transactionalClauses = (List<csclm__Transactional_Clause__c>) objectTrigger;

        system.debug('**** transactionalClauses after insert: ' + JSON.serializePretty(transactionalClauses, true));
    }

    public static void beforeUpdate(object[] objectTrigger){
        List<csclm__Transactional_Clause__c> transactionalClauses = (List<csclm__Transactional_Clause__c>) objectTrigger;

        for(csclm__Transactional_Clause__c tc : transactionalClauses){
            if(tc.Promo_dates_filled__c && tc.Promo_active__c){
                tc.csclm__Marked_For_Inclusion__c = false;
            }
        }

        system.debug('**** transactionalClauses before update: ' + JSON.serializePretty(transactionalClauses, true));

    }

    public static void afterUpdate(object[] objectTrigger){
         List<csclm__Transactional_Clause__c> transactionalClauses = (List<csclm__Transactional_Clause__c>) objectTrigger;

        system.debug('**** transactionalClauses after update: ' + JSON.serializePretty(transactionalClauses, true));

    }
}