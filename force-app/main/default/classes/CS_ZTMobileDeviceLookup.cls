// used as a lookup query on Zakelijke Toestelbetaling PD
global with sharing class CS_ZTMobileDeviceLookup extends cscfga.ALookupSearch {

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit) {

        System.debug('****searchfields CS_ZTMobileDeviceLookup: ' + JSON.serializePretty(searchFields));
    
        final Integer selectListLookupPageLimit = pageLimit + 1; 
        Integer recordOffset = pageOffset * pageLimit;  
        Decimal duration = Decimal.valueOf(searchFields.get('Calculated Mobile Contract Duration'));
        String scenario = searchFields.get('Scenario');
        String segment = searchFields.get('Segment');
        String simType = searchFields.get('SIM Type');
        scenario = '%' + scenario + '%';
        simType= '%' + simType + '%';

        List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>();

        // Segment__c = 'SoHo' is allowed only when Calculated Mobile Contract Duration = 24 and Opportunity "type" is SoHo
        if (segment.contains('SoHo') && duration == 24) {
            priceItems = [SELECT Id, Name, Max_Duration__c, Min_Duration__c, cspmb__Is_Active__c, cspmb__One_Off_Charge__c, Category__c, cspmb__One_Off_Charge_External_Id__c,
            One_Off_Charge_Product__c, cspmb__one_off_cost__c, mobile_scenario_text__c,  mobile_add_on_category__c, mobile_vendor__c, Segment__c,
            RecurringName__c, OneOffName__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c, Recurring_Charge_Product__r.PLFormula__c, One_Off_Charge_Product__r.PLFormula__c,
            One_Off_Charge_Product__r.ThresholdGroup__c, cspmb__Recurring_Charge_External_Id__c, Discount_allowed__c, SimType__c,
            Commision_allowed__c, mobile_scenario__c, mobile_subscription__c, cspmb__recurring_charge__c, cspmb__recurring_cost__c, cspmb__Price_Item_Code__c, Recurring_Charge_Product__c,
            mobile_subscription_text__c, Min_Quantity__c, Max_Quantity__c, Recurring_Charge_Product__r.ThresholdGroup__c, Group__c, Sales_Channel__c,
            One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name
            FROM cspmb__Price_Item__c
            WHERE cspmb__Is_Active__c = true AND (Segment__c like '%SoHo%' OR  Segment__c = '') AND mobile_add_on_category__c = 'Mobile hardware' AND mobile_scenario_text__c like :scenario AND SimType__c like :simType
            ORDER BY Name ASC
            LIMIT :selectListLookupPageLimit OFFSET :recordOffset]; 
        }
        else {
            priceItems = [SELECT Id, Name, Max_Duration__c, Min_Duration__c, cspmb__Is_Active__c, cspmb__One_Off_Charge__c, Category__c, cspmb__One_Off_Charge_External_Id__c,
            One_Off_Charge_Product__c, cspmb__one_off_cost__c, mobile_scenario_text__c,  mobile_add_on_category__c, mobile_vendor__c, Segment__c,
            RecurringName__c, OneOffName__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c, Recurring_Charge_Product__r.PLFormula__c, One_Off_Charge_Product__r.PLFormula__c,
            One_Off_Charge_Product__r.ThresholdGroup__c, cspmb__Recurring_Charge_External_Id__c, Discount_allowed__c, SimType__c,
            Commision_allowed__c, mobile_scenario__c, mobile_subscription__c, cspmb__recurring_charge__c, cspmb__recurring_cost__c, cspmb__Price_Item_Code__c, Recurring_Charge_Product__c,
            mobile_subscription_text__c, Min_Quantity__c, Max_Quantity__c, Recurring_Charge_Product__r.ThresholdGroup__c, Group__c, Sales_Channel__c,
            One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name
            FROM cspmb__Price_Item__c
            WHERE cspmb__Is_Active__c = true AND (NOT Segment__c like '%SoHo%') AND mobile_add_on_category__c = 'Mobile hardware' AND mobile_scenario_text__c like :scenario AND SimType__c like :simType
            ORDER BY Name ASC
            LIMIT :selectListLookupPageLimit OFFSET :recordOffset]; 
        }

        return priceItems;

    }

    public override String getRequiredAttributes() { 
        return '["Calculated Mobile Contract Duration", "Scenario", "Segment", "SIM Type"]';
    }  
}