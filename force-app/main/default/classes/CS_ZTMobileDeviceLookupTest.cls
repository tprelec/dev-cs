@isTest
public with sharing class CS_ZTMobileDeviceLookupTest {
    @isTest
    static void lookupTest() {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];

        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;

        System.runAs (simpleUser) { 
            OrderType__c orderType = CS_DataTest.createOrderType();
            insert orderType;
            Product2 product1 = CS_DataTest.createProduct('Samsung Galaxy S21', orderType);
            insert product1;
      
            Category__c mobileHardwareCategory = CS_DataTest.createCategory('Mobile hardware');
            insert mobileHardwareCategory;
    
            cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItemMobile(product1, mobileHardwareCategory, 'Flex Mobile', 'EU voice priceplan');
            priceItem1.cspmb__One_Off_Charge__c = 612;
            priceItem1.mobile_add_on_category__c = 'Mobile hardware';
            priceItem1.SimType__c = 'PSIM';
            insert priceItem1;

            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;
    
            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;
        
            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
            cscfga__Product_Definition__c ztDef = CS_DataTest.createProductDefinition('Zakelijke Toestelbetaling');
            ztDef.Product_Type__c = 'Mobile';
            ztDef.RecordTypeId = productDefinitionRecordType;
            insert ztDef;
        
            cscfga__Product_Configuration__c ztConf = CS_DataTest.createProductConfiguration(ztDef.Id, 'Zakelijke Toestelbetaling',basket.Id);
            ztConf.cscfga__Root_Configuration__c = null;
            ztConf.cscfga__Parent_Configuration__c = null;
            insert ztConf;
       
            csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(ztConf);
            pcr.csbb__Product_Configuration__c = ztConf.Id;
            insert pcr;

            Map<String, String> searchFields = new Map<String, String>();
            searchFields.put('Calculated Mobile Contract Duration', '12');
            searchFields.put('Scenario', 'Flex Mobile');
            searchFields.put('Segment', '');
            searchFields.put('SIM Type', 'PSIM');
        
            CS_ZTMobileDeviceLookup ztDeviceLookup = new CS_ZTMobileDeviceLookup();
            ztDeviceLookup.getRequiredAttributes();
            List<Object> result = ztDeviceLookup.doLookupSearch(searchFields, String.valueOf(ztDef.Id),null, 0, 0);

            System.assertNotEquals(0, result.size());
        }
    }
}