// used as a lookup query on Zakelijke Toestelbetaling PD
global with sharing class CS_ZakelijkeToestelbetalingLookup extends cscfga.ALookupSearch {

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit) {

        System.debug('****searchfields CS_ZakelijkeToestelbetalingLookup: ' + JSON.serializePretty(searchFields));
    
        final Integer selectListLookupPageLimit = pageLimit + 1; 
        Integer recordOffset = pageOffset * pageLimit;  
        Decimal duration = Decimal.valueOf(searchFields.get('Contract Duration'));

        List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>();

        priceItems = [SELECT Id, Name, Max_Duration__c, Min_Duration__c, cspmb__Is_Active__c, cspmb__Recurring_Charge__c, Category__c, cspmb__Recurring_Charge_External_Id__c,
            Recurring_Charge_Product__c, cspmb__recurring_cost__c
            FROM cspmb__Price_Item__c
            WHERE cspmb__Is_Active__c = true AND Max_Duration__c >= :duration AND Min_Duration__c < :duration AND Category__c = 'Zakelijke Toestelbetaling'
            ORDER BY cspmb__Recurring_Charge__c ASC
            LIMIT :selectListLookupPageLimit OFFSET :recordOffset];    

        return priceItems;

    }

    public override String getRequiredAttributes() { 
        return '["Contract Duration"]';
    }  
}