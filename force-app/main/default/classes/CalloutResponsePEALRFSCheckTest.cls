@isTest
private class CalloutResponsePEALRFSCheckTest {
    
    @testsetup
    private static void setupTestData()
    {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;
        
        List<LG_RfsCheckVariables__c> rfsVariables = new List<LG_RfsCheckVariables__c>();
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'AnalogueTv', LG_Value__c = 'Catv'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DigitalTv', LG_Value__c = 'DMM'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownBasic', LG_Value__c = '130'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownFp200', LG_Value__c = '300'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownFp500', LG_Value__c = '500'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Fp200', LG_Value__c = 'UPC Fiber Power Internet'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Fp500', LG_Value__c = 'UPC_superfast'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Internet', LG_Value__c = 'UPC Internet'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Mobile', LG_Value__c = 'mobile'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'MobileInternet', LG_Value__c = 'mobile_internet'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Qos', LG_Value__c = 'QoS'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Telephony', LG_Value__c = 'telephony'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpBasic', LG_Value__c = '30'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpFp200', LG_Value__c = '40'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpFp500', LG_Value__c = '40'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'ZakelijkInternetGiga' , LG_Value__c = 'Ziggo_Giga'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'GigaDownFp1000' , LG_Value__c = '1000'));
        insert rfsVariables;
        
        noTriggers.Flag__c = false;
        upsert noTriggers;
    }
    
    private static testmethod void testWith200Ok()
    {
        CalloutResponsePEALRFSCheck rfsCheck = new CalloutResponsePEALRFSCheck();
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('httpStatusCode', '200');
        inputMap.put('availabilityResponseRaw', '   "response" : ' + LG_RfsCheckUtility.buildRfsResponse(true, true, true, true, true, true) + ' ');
        rfsCheck.processResponseRaw(inputMap);
        rfsCheck.productResponse = new csbb.CalloutProduct.ProductResponse();
        
        
        rfsCheck.productResponse.fields = new Map<String, String>();
        rfsCheck.crPrimary = new csbb.CalloutResponse();
        rfsCheck.crPrimary.mapDynamicFields = new Map<String, String>();
        rfsCheck.runBusinessRules('Something');
        csbb.Result result = rfsCheck.canOffer(new Map<String, String>(), new Map<String, String>{'topspeed' => '500'}, null);
        
        System.assertEquals('OK', result.status, 'Result should be ok');
    }
    
    private static testmethod void testWith200OkIgnoreRfs()
    {
        CalloutResponsePEALRFSCheck rfsCheck = new CalloutResponsePEALRFSCheck();
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('httpStatusCode', '200');
        inputMap.put('availabilityResponseRaw', '   "response" : ' + LG_RfsCheckUtility.buildRfsResponse(true, true, true, true, true, true) + ' ');
        rfsCheck.processResponseRaw(inputMap);
        rfsCheck.productResponse = new csbb.CalloutProduct.ProductResponse();
        
        
        rfsCheck.productResponse.fields = new Map<String, String>();
        rfsCheck.crPrimary = new csbb.CalloutResponse();
        rfsCheck.crPrimary.mapDynamicFields = new Map<String, String>();
        rfsCheck.runBusinessRules('Something');
        csbb.Result result = rfsCheck.canOffer(new Map<String, String>{'Ignore RFS Check' => 'Yes'}, new Map<String, String>(), null);
        
        System.assertEquals('OK', result.status, 'Result should be ok');
    }
    
    private static testmethod void testWith400()
    {
        CalloutResponsePEALRFSCheck rfsCheck = new CalloutResponsePEALRFSCheck();
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('httpStatusCode', '400');
        rfsCheck.processResponseRaw(inputMap);
        rfsCheck.productResponse = new csbb.CalloutProduct.ProductResponse();
        
        rfsCheck.productResponse.fields = new Map<String, String>();
        rfsCheck.crPrimary = new csbb.CalloutResponse();
        rfsCheck.crPrimary.mapDynamicFields = new Map<String, String>();
        rfsCheck.runBusinessRules('Something');
        csbb.Result result = rfsCheck.canOffer(new Map<String, String>(), new Map<String, String>{'rfsCheckFailureMessage' => 'Error'}, null);
        
        System.assertNotEquals('OK', result.status, 'Result should not be ok');
        System.assertEquals('Error', result.status, 'Result should be Error');
    }
    
    private static testmethod void testWith500()
    {
        CalloutResponsePEALRFSCheck rfsCheck = new CalloutResponsePEALRFSCheck();
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('httpStatusCode', '500');
        rfsCheck.processResponseRaw(inputMap);
        rfsCheck.productResponse = new csbb.CalloutProduct.ProductResponse();
        
        rfsCheck.productResponse.fields = new Map<String, String>();
        rfsCheck.crPrimary = new csbb.CalloutResponse();
        rfsCheck.crPrimary.mapDynamicFields = new Map<String, String>();
        rfsCheck.runBusinessRules('Something');
        csbb.Result result = rfsCheck.canOffer(new Map<String, String>(), new Map<String, String>{'rfsCheckFailureMessage' => 'Error'}, null);
        
        System.assertNotEquals('OK', result.status, 'Result should not be ok');
        System.assertEquals('Error', result.status, 'Result should be Error');
    }
    
    private static testmethod void testWith404()
    {
        CalloutResponsePEALRFSCheck rfsCheck = new CalloutResponsePEALRFSCheck();
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('httpStatusCode', '404');
        rfsCheck.processResponseRaw(inputMap);
        rfsCheck.productResponse = new csbb.CalloutProduct.ProductResponse();
        
        rfsCheck.productResponse.fields = new Map<String, String>();
        rfsCheck.crPrimary = new csbb.CalloutResponse();
        rfsCheck.crPrimary.mapDynamicFields = new Map<String, String>();
        rfsCheck.runBusinessRules('Something');
        csbb.Result result = rfsCheck.canOffer(new Map<String, String>(), new Map<String, String>{'rfsCheckFailureMessage' => 'Error'}, null);
        
        System.assertNotEquals('OK', result.status, 'Result should not be ok');
        System.assertEquals('Error', result.status, 'Result should be Error');
    }
    
    //BU-27, BU-28
      private static testmethod void testWithPostcode()
    {
        CalloutResponsePEALRFSCheck rfsCheck = new CalloutResponsePEALRFSCheck();
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('postcode', '3572RE');
        rfsCheck.getDynamicRequestParameters(inputMap);
        rfsCheck.productResponse = new csbb.CalloutProduct.ProductResponse();
        
        rfsCheck.productResponse.fields = new Map<String, String>();
        rfsCheck.crPrimary = new csbb.CalloutResponse();
        rfsCheck.crPrimary.mapDynamicFields = new Map<String, String>();
        //rfsCheck.runBusinessRules('Something');
        csbb.Result result = rfsCheck.canOffer(new Map<String, String>(), new Map<String, String>{'rfsCheckFailureMessage' => 'Error'}, null);
        
        System.assertNotEquals('OK', result.status, 'Result should not be ok');
        System.assertEquals('Error', result.status, 'Result should be Error');  
        system.debug('Postcode is displayed in Upper letters' +inputMap);
    }
    //BU-27, BU-28
}