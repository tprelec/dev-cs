/**
 * @description: This class is responsible for invoking the CreateCredit REST service
 * @author: Jurgen van Westreenen
 */
public without sharing class CancelBSSOrderRest implements Queueable, Database.AllowsCallouts {
	public static final String BILLING_STATUS_REQUESTED = 'Requested';
	public static final String BILLING_STATUS_REQUEST_FAILED = 'Failed';
	public static final Integer MAX_NR_OF_FAILED_REQUESTS = 3;

	@TestVisible
	private static final String INTEGRATION_SETTING_NAME = 'SIASRESTCancelBSSOrder';
	@TestVisible
	private static final String MOCK_URL = 'http://example.com/CancelBSSOrderRest';

	private List<Contracted_Products__c> updContProds = new List<Contracted_Products__c>();
	private List<Order_Billing_Transaction__c> newBillTrans = new List<Order_Billing_Transaction__c>();
	private String transactionId = 'TR-' + Datetime.now().getTime();

	private List<Id> contProdIds = new List<Id>();

	public CancelBSSOrderRest(List<Id> cpIdList) {
		this.contProdIds = cpIdList;
	}

	public void execute(QueueableContext context) {
		try {
			List<Contracted_Products__c> prodList = [
				SELECT
					Id,
					Customer_Asset__c,
					Customer_Asset__r.Assigned_Product_Id__c,
					Billing_Order_Id__c,
					Number_of_Failed_Requests__c,
					Order__c,
					Order__r.Account__c
				FROM Contracted_Products__c
				WHERE Id IN :contProdIds
			];
			HttpRequest reqData = buildReqHeader();
			sendRequest(reqData, prodList);
		} catch (Exception e) {
			throw e;
		} finally {
			insert newBillTrans;
			update updContProds;
		}
	}

	private void sendRequest(HttpRequest reqData, List<Contracted_Products__c> prodList) {
		String reqBody = buildReqBody(prodList);
		reqData.setBody(reqBody);
		reqData.setMethod('POST');

		Http http = new Http();
		HTTPResponse response = http.send(reqData);
		Order_Billing_Transaction__c billingTransaction = createBillingTransaction(
			transactionId,
			prodList
		);
		if (response.getStatusCode() == 200) {
			for (Contracted_Products__c cp : prodList) {
				cp.Billing_Status__c = BILLING_STATUS_REQUESTED;
				cp.Number_of_Failed_Requests__c = 0; // Reset counter
				updContProds.add(cp);
			}
		} else {
			String errorResponse = response.getBody();
			for (Contracted_Products__c cp : prodList) {
				cp.Number_of_Failed_Requests__c++; // Increase counter
				// Set status to 'Request Failed' after 3 failed attempts
				if (cp.Number_of_Failed_Requests__c >= MAX_NR_OF_FAILED_REQUESTS) {
					cp.Billing_Status__c = BILLING_STATUS_REQUEST_FAILED;
				}
				updContProds.add(cp);
			}
			billingTransaction.Error_Info__c = errorResponse;
		}
		newBillTrans.add(billingTransaction);
	}

	// Using Named Credentials will be part of a larger scale refactoring; suppress for now
	@SuppressWarnings('PMD.ApexSuggestUsingNamedCred')
	private HttpRequest buildReqHeader() {
		// Retrieve the credentials from the custom setting
		External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(
			INTEGRATION_SETTING_NAME
		);
		String endpointURL = webServiceConfig.URL__c;
		String authorizationHeaderString =
			webServiceConfig.Username__c +
			':' +
			webServiceConfig.Password__c;
		String authorizationHeader =
			'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(authorizationHeaderString));

		HttpRequest reqData = new HttpRequest();

		reqData.setHeader('Content-Type', 'application/json');
		reqData.setHeader('Connection', 'keep-alive');
		reqData.setHeader('Content-Length', '0');
		reqData.setHeader('Authorization', authorizationHeader);
		reqData.setTimeout(120000);
		reqData.setEndpoint(endpointURL);

		if (String.isNotEmpty(webServiceConfig.Certificate_Name__c)) {
			reqData.setClientCertificateName(webServiceConfig.Certificate_Name__c);
		}

		return reqData;
	}

	private String buildReqBody(List<Contracted_Products__c> cpList) {
		// Generate the request JSON message
		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeFieldName('cancelBSSOrderRequest');
		generator.writeStartObject();
		generator.writeStringField('transactionID', transactionId);
		generator.writeFieldName('orderLinesList');
		generator.writeStartArray();
		for (Contracted_Products__c contProd : cpList) {
			generator.writeStartObject();
			generator.writeStringField(
				'componentAPID',
				contProd?.Customer_Asset__r.Assigned_Product_Id__c != null
					? String.valueOf(contProd?.Customer_Asset__r.Assigned_Product_Id__c)
					: ''
			);
			generator.writeStringField(
				'unifyOrderId',
				contProd.Billing_Order_Id__c != null
					? String.valueOf(contProd.Billing_Order_Id__c)
					: ''
			);
			generator.writeStringField('reasonCode', 'CREQ'); // Hardcoded
			generator.writeEndObject();
		}
		generator.writeEndArray();
		generator.writeEndObject();
		generator.writeEndObject();

		return generator.getAsString();
	}

	private Order_Billing_Transaction__c createBillingTransaction(
		String transactionId,
		List<Contracted_Products__c> contractedProducts
	) {
		List<Id> contractedProductIds = new List<Id>();
		List<Id> assetIds = new List<Id>();
		Id orderId;
		Id accountId;
		for (Contracted_Products__c contractedProduct : contractedProducts) {
			contractedProductIds.add(contractedProduct.Id);
			if (String.isNotEmpty(contractedProduct.Customer_Asset__c)) {
				assetIds.add(contractedProduct.Customer_Asset__c);
			}
			if (contractedProduct.Order__c != null && orderId == null) {
				orderId = contractedProduct.Order__c;
				accountId = contractedProduct.Order__r.Account__c;
			}
		}
		Order_Billing_Transaction__c billingTransaction = new Order_Billing_Transaction__c(
			Contracted_Products__c = JSON.serialize(contractedProductIds),
			Customer_Assets__c = JSON.serialize(assetIds),
			Order__c = orderId,
			Account__c = accountId,
			Transaction_Id__c = transactionId
		);
		return billingTransaction;
	}
}