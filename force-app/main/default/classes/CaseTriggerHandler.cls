public with sharing class CaseTriggerHandler extends TriggerHandler {
    private List<Case> newCases = (List<Case>) this.newList;
    private Map<Id, Case> newCaseMap = (Map<Id, Case>) this.newMap;
    private Map<Id, Case> oldCaseMap = (Map<Id, Case>) this.oldMap;

    private Set<Id> parentIds;
    private Set<Id> childIds;
    private Set<Id> updateSet;
    private Set<String> statusSet;
    private List<Case> updateList;
    private Map<Id, Integer> aggrMap;
    private Map<String, Id> productTagMap;
    private Set<Id> functionalTeamSet;

    private void init() {
        newCases = (List<Case>) this.newList;
        newCaseMap = (Map<Id, Case>) this.newMap;
        oldCaseMap = (Map<Id, Case>) this.oldMap;
    }

    public override void beforeInsert() {
        init();
        fillAccount();
        fillContact();
        syncToAgileAccelerator();
        updateRoleCreatorField();
    }

    public override void beforeUpdate() {
        init();
        fillAccount();
        fillContact();

        // This should run only once
        if (!isAlreadyModified()) {
            setAlreadyModified();
            syncToAgileAccelerator();
        }

        CS_MobileFlowCaseValidator mfCaseValidator = new CS_MobileFlowCaseValidator(
            oldCaseMap,
            newCaseMap
        );
        mfCaseValidator.run();
    }

    public override void afterInsert() {
        init();
        checkOpenChildCases();
        CS_MobileFlowService.setCaseFields(newCases);
    }

    public override void afterUpdate() {
        init();
        checkOpenChildCases();
        createMobileFlowTasks();
        updateMobileFlowTaskOwnership();
        setFinalQualityCheckScore();
    }

    public override void afterDelete() {
        init();
        checkOpenChildcases();
    }

    private void fillAccount() {
        Map<Id, Id> oppIdToAccountId = new Map<Id, Id>();
        Map<Id, Id> contrIdToAccountId = new Map<Id, Id>();

        for (Case c : newCases) {
            if (c.Opportunity__c != null) {
                oppIdToAccountId.put(c.Opportunity__c, null);
            } else if (c.Contract_VF__c != null) {
                contrIdToAccountId.put(c.Contract_VF__c, null);
            }
        }

        getAccounts(oppIdToAccountId, contrIdToAccountId);

        for (Case c : newCases) {
            if (c.Opportunity__c != null) {
                c.Account__c = oppIdToAccountId.get(c.Opportunity__c);
            } else if (c.Contract_VF__c != null) {
                c.Account__c = contrIdToAccountId.get(c.Contract_VF__c);
            }
        }
    }

    private void getAccounts(Map<Id, Id> oppIdToAccountId, Map<Id, Id> contrIdToAccountId) {
        if (!oppIdToAccountId.isEmpty()) {
            for (Opportunity opp : [
                SELECT Id, AccountId
                FROM Opportunity
                WHERE Id IN :oppIdToAccountId.keySet()
            ]) {
                oppIdToAccountId.put(opp.Id, opp.AccountId);
            }
        }

        if (!contrIdToAccountId.isEmpty()) {
            for (VF_Contract__c c : [
                SELECT Id, Account__c
                FROM VF_Contract__c
                WHERE Id IN :contrIdToAccountId.keySet()
            ]) {
                contrIdToAccountId.put(c.Id, c.Account__c);
            }
        }
    }

    private void fillContact() {
        Id targetRecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName(
            'Case',
            'MLE_Sales_Support_Case'
        );
        Set<Id> oppIds = new Set<Id>();
        Map<Id, Opportunity> caseIdToOpp = new Map<Id, Opportunity>();
        Map<Id, Id> userIdToContactId = new Map<Id, Id>();

        getRelatedOpportunities(oppIds, caseIdToOpp);

        for (Case c : newCases) {
            if (c.Opportunity__c != null && c.RecordTypeId == targetRecordTypeId) {
                userIdToContactId.put(caseIdToOpp.get(c.Id).OwnerId, null);
            } else if (c.ContactId == null) {
                userIdToContactId.put(UserInfo.getUserId(), null);
            }
        }

        if (!userIdToContactId.isEmpty()) {
            for (Contact c : [
                SELECT Id, Userid__c
                FROM Contact
                WHERE Userid__c IN :userIdToContactId.keySet()
            ]) {
                userIdToContactId.put(c.Userid__c, c.Id);
            }
        }

        setContactsForCases(targetRecordTypeId, caseIdToOpp, userIdToContactId);
    }

    private void getRelatedOpportunities(Set<Id> oppIds, Map<Id, Opportunity> caseIdToOpp) {
        for (Case c : newCases) {
            if (c.Opportunity__c != null) {
                oppIds.add(c.Opportunity__c);
            }
        }

        List<Opportunity> relatedOpps = [SELECT Id, OwnerId FROM Opportunity WHERE Id IN :oppIds];

        for (Case c : newCases) {
            for (Opportunity opp : relatedOpps) {
                if (c.Opportunity__c == opp.Id) {
                    caseIdToOpp.put(c.Id, opp);
                }
            }
        }
    }

    private void setContactsForCases(
        Id targetRecordTypeId,
        Map<Id, Opportunity> caseIdToOpp,
        Map<Id, Id> userIdToContactId
    ) {
        for (Case c : newCases) {
            if (
                c.Opportunity__c != null &&
                c.RecordTypeId == targetRecordTypeId &&
                userIdToContactId.containsKey(caseIdToOpp.get(c.Id).OwnerId)
            ) {
                c.ContactId = userIdToContactId.get(caseIdToOpp.get(c.Id).OwnerId);
            } else if (c.ContactId == null && userIdToContactId.containsKey(UserInfo.getUserId())) {
                c.ContactId = userIdToContactId.get(UserInfo.getUserId());
            }
        }
    }

    private void checkOpenChildCases() {
        parentIds = new Set<Id>();
        childIds = new Set<Id>();
        updateSet = new Set<Id>();
        statusSet = new Set<String>();
        updateList = new List<Case>();
        aggrMap = new Map<Id, Integer>();

        for (CaseStatus cs : [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = :true]) {
            statusSet.add(cs.MasterLabel);
        }

        if (oldCaseMap == null) {
            checkOpenSubcasesOnInsert();
        } else if (newCases == null) {
            checkOpenSubcasesOnDelete();
        } else {
            checkOpenSubcasesOnUpdate();
        }

        update updateList;
    }

    private void checkOpenSubcasesOnInsert() {
        for (Case c : newCases) {
            if (c.ParentId != null) {
                parentIds.add(c.ParentId);
            }
        }

        for (Case c : [SELECT Open_subcases__c FROM Case WHERE Id IN :parentIds]) {
            if (!c.Open_subcases__c) {
                c.Open_subcases__c = true;
                updateList.add(c);
            }
        }
    }

    private void checkOpenSubcasesOnDelete() {
        for (Case c : oldCaseMap.values()) {
            if (c.ParentId != null) {
                parentIds.add(c.ParentId);
                childIds.add(c.Id);
            }
        }

        for (AggregateResult ar : [
            SELECT ParentId, Count(Id)
            FROM Case
            WHERE ParentId IN :parentIds AND Id NOT IN :childIds AND Status NOT IN :statusSet
            GROUP BY ParentId
        ]) {
            aggrMap.put((Id) ar.get('parentId'), (Integer) ar.get('expr0'));
        }

        for (Id parent : parentIds) {
            if (!aggrMap.containsKey(parent) || aggrMap.get(parent) == 0) {
                updateSet.add(parent);
            }
        }

        for (Case c : [SELECT Open_subcases__c FROM Case WHERE Id IN :updateSet]) {
            if (c.Open_subcases__c) {
                c.Open_subcases__c = false;
                updateList.add(c);
            }
        }
    }

    private void checkOpenSubcasesOnUpdate() {
        Set<Id> unclosedCasesIds = new Set<Id>();
        groupCasesIds(unclosedCasesIds);

        for (AggregateResult ar : [
            SELECT ParentId, Count(Id)
            FROM Case
            WHERE
                ParentId IN :parentIds
                AND ParentId NOT IN :unclosedCasesIds
                AND Id NOT IN :childIds
                AND Status NOT IN :statusSet
            GROUP BY ParentId
        ]) {
            aggrMap.put((Id) ar.get('parentId'), (Integer) ar.get('expr0'));
        }

        for (Id parent : parentIds) {
            if (!aggrMap.containsKey(parent) || aggrMap.get(parent) == 0) {
                updateSet.add(parent);
            }
        }

        for (Case c : [SELECT Open_subcases__c FROM Case WHERE Id IN :unclosedCasesIds]) {
            if (!c.Open_subcases__c) {
                c.Open_subcases__c = true;
                updateList.add(c);
            }
        }

        for (Case c : [SELECT Open_subcases__c FROM Case WHERE Id IN :updateSet]) {
            if (c.Open_subcases__c) {
                c.Open_subcases__c = false;
                updateList.add(c);
            }
        }
    }

    private void groupCasesIds(Set<Id> unclosedCasesIds) {
        for (Case c : newCases) {
            if (c.ParentId != null) {
                if (
                    statusSet.contains(c.Status) && !statusSet.contains(oldCaseMap.get(c.Id).Status)
                ) {
                    parentIds.add(c.ParentId);
                    childIds.add(c.Id);
                } else if (
                    !statusSet.contains(c.Status) && statusSet.contains(oldCaseMap.get(c.Id).Status)
                ) {
                    unclosedCasesIds.add(c.ParentId);
                }
            }
        }
    }

    private void syncToAgileAccelerator() {
        Map<Id, agf__ADM_Work__c> workToInsert = new Map<Id, agf__ADM_Work__c>();
        List<agf__ADM_Task__c> tasksToInsert = new List<agf__ADM_Task__c>();
        WorkLookupsWrapper workLookups;

        if (checkForBugsToCreate()) {
            // This data is used only for creating bugs, so we're doing SOQL queries only for that
            workLookups = new WorkLookupsWrapper();
            getFunctionalTeamSet();
        }

        getProductTags();

        for (Case c : newCases) {
            agf__ADM_Work__c work;

            if (oldCaseMap == null || c.agf__ADM_Work__c == null) {
                if (c.Sprint_Case__c) {
                    // Create a user story from sprint case
                    work = createUserStory(c, workToInsert);
                    addCommonFields(c, work);
                } else if (c.Blocking_Issue__c) {
                    // Create a Bug from blocking issue
                    // Check that the case is not assigned to a group
                    String sObjectType = c.OwnerId.getSObjectType().getDescribe().getName();
                    User caseOwner = GeneralUtils.userMap.get(c.OwnerId);

                    if (
                        sObjectType != 'Group' &&
                        caseOwner != null &&
                        caseOwner.Profile.Name == 'System Administrator'
                    ) {
                        // Create a Bug from blocking issue
                        work = createBug(c, workLookups, workToInsert);
                        addCommonFields(c, work);
                    }
                }
            }
        }

        if (workToInsert != null) {
            insert workToInsert.values();
            updateCasesAndCreateTasks(workToInsert, tasksToInsert);
        }
    }

    private void updateCasesAndCreateTasks(
        Map<Id, agf__ADM_Work__c> workToInsert,
        List<agf__ADM_Task__c> tasksToInsert
    ) {
        for (Case c : newCases) {
            if (workToInsert.containsKey(c.Id) && workToInsert.get(c.Id) != null) {
                agf__ADM_Work__c w = workToInsert.get(c.Id);
                c.agf__ADM_Work__c = w.Id;

                if (w.agf__Sprint__c != null) {
                    c.ADM_Sprint_End_Date__c = w.Sprint_End_Date__c;

                    // Create a task on the Bug for the assignee
                    agf__ADM_Task__c tsk = new agf__ADM_Task__c(
                        agf__Assigned_To__c = c.OwnerId,
                        agf__Subject__c = 'Placeholder task to fix bug',
                        agf__Work__c = w.Id
                    );
                    tasksToInsert.add(tsk);
                }
            }
        }

        if (tasksToInsert.size() > 0) {
            insert tasksToInsert;
        }
    }

    private agf__ADM_Work__c createUserStory(Case c, Map<Id, agf__ADM_Work__c> workToInsert) {
        agf__ADM_Work__c work = new agf__ADM_Work__c(
            RecordTypeId = GeneralUtils.recordTypeMap.get('agf__ADM_Work__c').get('User_Story'),
            agf__Details__c = c.Description,
            agf__Status__c = 'Backlog',
            Classification__c = c.HyperCare_Classification__c,
            agf__Product_Tag__c = productTagMap.get('Undefined'),
            agf__Type__c = 'User Story'
        );

        // Getting rid of the formatting. Didn't make Internal_comments__c a rich text field as editting the field does not work correctly in AA.
        if (c.Internal_comments__c != null) {
            work.Internal_comments__c = c.Internal_comments__c.replaceAll('<[^>]+>', ' ')
                .replaceAll('&#39;', '`');
        }

        workToInsert.put(c.Id, work);
        return work;
    }

    private Boolean checkForBugsToCreate() {
        Boolean createBugs = false;
        for (Case c : newCases) {
            if (c.Blocking_Issue__c) {
                createBugs = true;
                break;
            }
        }
        return createBugs;
    }

    private agf__ADM_Work__c createBug(
        Case c,
        WorkLookupsWrapper workLookups,
        Map<Id, agf__ADM_Work__c> workToInsert
    ) {
        agf__ADM_Work__c work = new agf__ADM_Work__c(
            RecordTypeId = GeneralUtils.recordTypeMap.get('agf__ADM_Work__c').get('Bug'),
            agf__Assignee__c = c.OwnerId,
            agf__Details_and_Steps_to_Reproduce__c = c.Description,
            agf__Found_in_Build__c = workLookups.buildId,
            agf__Sprint__c = workLookups.sprintId,
            agf__Priority_Rank__c = 1,
            agf__Sprint_Rank__c = 1,
            agf__Type__c = 'Bug',
            agf__Priority__c = 'P1'
        );

        if (functionalTeamSet.contains(c.OwnerId)) {
            work.agf__Product_Tag__c = productTagMap.get('First Line Support');
        } else {
            work.agf__Product_Tag__c = productTagMap.get('Dev SFDC');
        }

        // Getting rid of the formatting. Didn't make Internal_comments__c a rich text field as editting the field does not work correctly in AA.
        if (c.Internal_comments__c != null) {
            work.Internal_comments__c = c.Internal_comments__c.replaceAll('<[^>]+>', ' ')
                .replaceAll('&#39;', '`');
        }

        workToInsert.put(c.Id, work);
        return work;
    }

    // Populate common fields to sync
    private static void addCommonFields(Case c, agf__ADM_Work__c work) {
        Map<String, String> fieldMapping = SyncUtil.fullMapping('Case -> agf__ADM_Work__c')
            .get('Case -> agf__ADM_Work__c');
        Map<String, Schema.SObjectField> caseFieldDescribe = Schema.SObjectType.Case.fields.getMap();

        // Loop through all the sync fields
        for (String field : fieldMapping.keySet()) {
            String theField = fieldMapping.get(field);
            Schema.SObjectField f = caseFieldDescribe.get(theField);
            Schema.DisplayType fieldType = f.getDescribe().getType();
            Object theNewValue = c.get(theField);

            // sync the data from case to work
            if (theNewValue != null) {
                if (String.valueof(fieldType) == 'DOUBLE') {
                    String d = String.valueof(theNewValue);
                    work.put(field, d);
                } else {
                    work.put(field, theNewValue);
                }
            }
        }
    }

    private void getProductTags() {
        productTagMap = new Map<String, Id>();

        for (agf__ADM_Product_Tag__c pt : [SELECT Id, Name FROM agf__ADM_Product_Tag__c]) {
            productTagMap.put(pt.Name, pt.Id);
        }
    }

    private void getFunctionalTeamSet() {
        Id functionGroupId = [SELECT Id FROM Group WHERE DeveloperName = 'Functioneel_Beheer_Team']
        .Id;

        functionalTeamSet = new Set<Id>();

        for (GroupMember gm : [
            SELECT UserOrGroupId
            FROM GroupMember
            WHERE GroupId = :functionGroupId
        ]) {
            functionalTeamSet.add(gm.UserOrGroupId);
        }
    }

    private without sharing class WorkLookupsWrapper {
        private Id buildId { get; set; }
        private Id sprintId { get; set; }

        private workLookupsWrapper() {
            List<agf__ADM_Build__c> builds = [
                SELECT Id
                FROM agf__ADM_Build__c
                WHERE Name = 'Production'
            ];

            List<agf__ADM_Sprint__c> sprints = [
                SELECT Id
                FROM agf__ADM_Sprint__c
                WHERE agf__Start_Date__c <= :Date.today() AND agf__End_Date__c >= :Date.today()
                LIMIT 1
            ];

            if (builds.size() > 0) {
                buildId = builds[0].Id;
            }
            if (sprints.size() > 0) {
                sprintId = sprints[0].Id;
            }
        }
    }

    private void updateMobileFlowTaskOwnership() {
        Map<Id, Case> caseMap = new Map<Id, Case>();

        for (Case oldCase : oldCaseMap.values()) {
            if (
                oldCase.OwnerId.getSobjectType() == User.SObjectType &&
                newCaseMap.get(oldCase.Id).OwnerId.getSobjectType() == User.SObjectType &&
                oldCase.OwnerId != newCaseMap.get(oldCase.Id).OwnerId &&
                newCaseMap.get(oldCase.Id).Mobile_Flow_Tasks_Created__c
            ) {
                caseMap.put(oldCase.Id, newCaseMap.get(oldCase.Id));
            }
        }

        if (caseMap.keySet().size() > 0) {
            CS_MobileFlowService.updateTaskOwnership(caseMap);
        }
    }

    private void createMobileFlowTasks() {
        List<Case> casesForTaskCreation = new List<Case>();
        Map<Id, Case> caseTaskMapForOwnershipChange = new Map<Id, Case>();

        for (Case oldCase : oldCaseMap.values()) {
            if (
                newCaseMap.get(oldCase.Id).OwnerId.getSobjectType() == User.SObjectType &&
                oldCase.OwnerId != newCaseMap.get(oldCase.Id).OwnerId
            ) {
                if (newCaseMap.get(oldCase.Id).Mobile_Flow_Tasks_Created__c) {
                    caseTaskMapForOwnershipChange.put(oldCase.Id, newCaseMap.get(oldCase.Id));
                } else {
                    casesForTaskCreation.add(newCaseMap.get(oldCase.Id));
                }
            }
        }

        if (casesForTaskCreation.size() > 0) {
            CS_MobileFlowService.createTasksForCases(casesForTaskCreation);
        }

        if (caseTaskMapForOwnershipChange.keySet().size() > 0) {
            CS_MobileFlowService.updateTaskOwnership(caseTaskMapForOwnershipChange);
        }
    }

    private void updateRoleCreatorField() {
        Set<Id> contractVfIds = new Set<Id>();

        for (Case c : newCases) {
            contractVfIds.add(c.Contract_VF__c);
        }

        Map<Id, VF_Contract__c> contractVfMap = new Map<Id, VF_Contract__c>(
            [SELECT Id, Name, ImplementationGroup__c FROM VF_Contract__c WHERE Id IN :contractVfIds]
        );

        for (Case c : newCases) {
            if (contractVFMap.get(c.Contract_VF__c) != null) {
                c.RoleCreatorChangeOwnerCreated__c = contractVfMap.get(c.Contract_VF__c)
                    .ImplementationGroup__c;
            }
        }
    }

    private void setFinalQualityCheckScore() {
        List<Case> cases = new List<Case>();

        for (Case newCase : newCases) {
            Case oldCase = oldCaseMap.get(newCase.Id);

            if (
                GeneralUtils.recordTypeMap.get('Case').get('CS_MF_Final_Quality_Check') ==
                newCase.recordTypeId &&
                newCase.Contract_VF__c != null &&
                newCase.ClosedDate != null &&
                newCase.ClosedDate != oldCase.ClosedDate
            ) {
                cases.add(newCase);
            }
        }

        Map<Id, VF_Contract__c> contractsToUpdateMap = new Map<Id, VF_Contract__c>();

        for (Case c : [
            SELECT Id, Contract_VF__c, FQC_Quality_Score__c
            FROM Case
            WHERE Id IN :cases AND Contract_VF__r.FQC_Quality_Score__c = NULL
        ]) {
            Decimal fqcQualityScore = (c.FQC_Quality_Score__c == null ||
                c.FQC_Quality_Score__c < 0)
                ? 0
                : c.FQC_Quality_Score__c;

            contractsToUpdateMap.put(
                c.Contract_VF__c,
                new VF_Contract__c(Id = c.Contract_VF__c, FQC_Quality_Score__c = fqcQualityScore)
            );
        }

        update contractsToUpdateMap.values();
    }
}