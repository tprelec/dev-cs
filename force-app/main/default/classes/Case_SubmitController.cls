public without sharing class Case_SubmitController {

  //marcel removed with sharing because of the fieldupdate
  private Case myCase;
  private ApexPages.StandardController controller;
  public Boolean alreadySubmitted {get;set;}

  public Case_SubmitController(ApexPages.StandardController stdController) {
    system.debug('init');
    stdController.addFields(new List<String> {'Owner.Name','HiddenSubmit__c','AB_Recall_Approval__c', 'CreatedById'});
    this.controller = stdController;
    this.myCase = (Case)this.controller.getRecord();
  	system.debug('init');
    alreadySubmitted = myCase.HiddenSubmit__c;
    
    if(alreadySubmitted) Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.WARNING,'Case was already submitted earlier. Current Case Owner is: '+myCase.Owner.Name));
  	system.debug('init');
  }

  public pageReference verifyAndSave() {
    if(myCase.HiddenSubmit__c){
      try{    
        update myCase; 
        return new PageReference('/'+myCase.Id);
      } catch(DmlException ex){
        ApexPages.addMessages(ex);
        return null;
      }
    } else {
      Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'Please tick the checkbox to confirm that you agree!'));
      return null;
    }
  }

  public pageReference submit(){
    myCase.HiddenSubmit__c = true;  
    try{      
      update myCase;
      return new PageReference('/'+myCase.Id);
    } catch(DmlException ex){
      ApexPages.addMessages(ex);
      return null;
    }
  }

  public pageReference recall(){
    
    //W-00964: recall can only be done by case creator
    if(myCase.CreatedById ==  UserInfo.getUserId() || isSysAdmin(UserInfo.getUserId())  ){
      
      Boolean Result = System.Approval.unlock(myCase).isSuccess();
      system.debug('Vreuls unlock' + Result);  
      if(Result)
      {        
          myCase.AB_Recall_Approval__c = true;       
          try{      
            update myCase;
            return new PageReference('/'+myCase.Id);
          } catch(DmlException ex){
            ApexPages.addMessages(ex);
            return null;
          }
      }

      return new PageReference('/'+myCase.Id);

    }
    else
    {
      Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'Case can only be recalled by the Case creator'));
      return null;  
    } 

  }

   public boolean isSysAdmin(String thisUserId) {
        boolean isAdmin = false;
        User thisUser;
        try {
            thisUser = [SELECT Profile.PermissionsModifyAllData
                FROM User WHERE Id =: thisUserId];
            isAdmin = thisUser.Profile.PermissionsModifyAllData;
        } catch(Exception exc) {
            //do nothing
        }
        return isAdmin;
    }
}