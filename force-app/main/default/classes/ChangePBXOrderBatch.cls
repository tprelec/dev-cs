/**
 * @description         This batch class is responsible for invoking the SiasService class and updating PBX info on products
 */
public class ChangePBXOrderBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
    public Set<Id> contractedProductSet = new Set<Id>();

    public ChangePBXOrderBatch(Set<Id> contractedProductSet) {
        this.contractedProductSet = contractedProductSet;
    }

    /**
     * only select applicable cp's. Do not update PBX in Unify for OneNet.
     */
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            [
                SELECT Id
                FROM Contracted_Products__c
                WHERE
                    Id IN :this.contractedProductSet
                    AND Delivery_Status__c = :Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED
                    AND Order__r.O2C_Order__c = TRUE
                    AND PBX__c != NULL
                    AND PBX__r.PABX_Id__c != NULL
                    AND (Product__r.Family_Tag__r.Name = :Constants.PRODUCT2_FAMILYTAG_PBX_Trunking
                    OR Product__r.Family_Tag__r.Name = :Constants.PRODUCT2_FAMILYTAG_PBX_Trunking_SIP_Charging)
                    AND Proposition_Contains_OneNet__c = FALSE
                    AND Do_not_export__c = FALSE
            ]
        );
    }

    public void execute(
        Database.BatchableContext bc,
        List<Contracted_Products__c> contractedProducts
    ) {
        for (Contracted_Products__c contractedProduct : contractedProducts) {
            requestPABXOrderChange(contractedProduct);
        }
    }

    private void requestPABXOrderChange(Contracted_Products__c contractedProduct) {
        // TODO: Change this to desired format later
        String transactionId = 'TR-' + contractedProduct.Id;
        SiasService.changeOrderPBX(transactionId, contractedProduct.Id);
    }

    @SuppressWarnings(
        'PMD.EmptyStatementBlock'
    ) // finish method has no logic but must be implemented because of interface
    public void finish(Database.BatchableContext bc) {
    }

    /**
     * this method is intended to be called in context of test method
     */
    @future(callout=true)
    public static void executeCalloutAsFuture(Set<Id> contractedProductSet) {
        ChangePBXOrderBatch batchInstance = new ChangePBXOrderBatch(contractedProductSet);
        // Get an iterator
        Database.QueryLocatorIterator iterator = batchInstance.start(null).iterator();

        List<Contracted_Products__c> contractedProducts = new List<Contracted_Products__c>();
        // Collect the first record
        if (iterator.hasNext()) {
            contractedProducts.add((Contracted_Products__c) iterator.next());
        }
        // call execute method to make callout and process the record
        if (!contractedProducts.isEmpty()) {
            batchInstance.execute(null, contractedProducts);
        }
    }
}