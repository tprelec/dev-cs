/**
* @description: This class is responsible for invoking the ChangePBXOrder REST service
* @author: Jurgen van Westreenen
*/
public with sharing class ChangePBXOrderRest {

    @TestVisible 
    private static final String INTEGRATION_SETTING_NAME = 'SIASRESTChangePBXOrder';
    @TestVisible 
    private static final String MOCK_URL = 'http://example.com/ChangePBXOrderRest';

    public static void changeOrderPBX(String transactionId, String contProdId) {

        /*
        // rahul: used a generic method to fetch the integration details
        IWebServiceConfig webServiceConfig;
        if(!Test.isRunningTest()) {
            webServiceConfig = WebServiceConfigLocator.getConfig(INTEGRATION_SETTING_NAME);
        } else {
            webServiceConfig = WebServiceConfigLocator.createConfig();
            webServiceConfig.setEndpoint(MOCK_URL);
        }

        string endpointURL 	= webServiceConfig.getEndpoint(); 
        // TODO: Rahul add username and password here
        string authorizationHeader = 'Bearer ' + webServiceConfig.getAuthorizationHeader();

        if(String.isNotEmpty(webServiceConfig.getCertificateName())) {
            reqData.setClientCertificateName(webServiceConfig.getCertificateName());
        }
        */

        // Retrieve the credentials from the custom setting
        External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(INTEGRATION_SETTING_NAME);

        string endpointURL 	= webServiceConfig.URL__c; 
        // String authorizationHeaderString = webServiceConfig.Authorization_Header__c;
        // string authorizationHeader = authorizationHeaderString; // 'Bearer ' + authorizationHeaderString;
        String authorizationHeaderString = webServiceConfig.Username__c +':' + webServiceConfig.Password__c;
        String authorizationHeader = 'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(authorizationHeaderString));
        System.debug('authorizationHeader: ' + authorizationHeader);

        HttpRequest reqData = new HttpRequest();
        Http http = new Http();
            
        reqData.setHeader('Content-Type','application/json');
        reqData.setHeader('Connection','keep-alive');
        reqData.setHeader('Content-Length','0');

        reqData.setHeader('Authorization', authorizationHeader);
        // rahul: increased the timeout
        reqData.setTimeout(120000); 
        reqData.setEndpoint(endpointURL);

        if(String.isNotEmpty(webServiceConfig.Certificate_Name__c)) {
            reqData.setClientCertificateName(webServiceConfig.Certificate_Name__c);
        }

        try {
            // Retrieve the necessary product data based on passed Contracted Product Id
            List<Contracted_Products__c> prodList = [SELECT Id,
                                                    PBX__r.Assigned_Product_Id__c,
                                                    Site__r.Assigned_Product_Id__c,
                                                    Customer_Asset__r.Assigned_Product_Id__c,                                                    
                                                    Order__r.Billing_Arrangement__c,
                                                    PBX__r.Billing_Arrangement__r.Unify_Ref_Id__c,
                                                    PBX__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c,
                                                    Unify_Product_Family__c,
                                                    Unify_Billing_Offer__c,
                                                    PBX__c,
                                                    PBX_Change_Error_Info__c,
                                                    Billing_Offer_synced_to_Unify_timestamp__c
                                                    FROM Contracted_Products__c WHERE Id = :contProdId LIMIT 1];
            if (prodList.size() > 0) {
                Contracted_Products__c contProd = prodList[0];
                // Generate the request JSON message
                JSONGenerator generator = JSON.createGenerator(false);
                generator.writeStartObject();
                generator.writeFieldName('changePBXOrderRequest');
                generator.writeStartObject();
                generator.writeStringField('transactionID', transactionId);    
                generator.writeStringField('APID', contProd.PBX__r.Assigned_Product_Id__c != null ? string.valueOf(contProd.PBX__r.Assigned_Product_Id__c) : '');    
                generator.writeStringField('SLSAPID', contProd.Site__r.Assigned_Product_Id__c != null ? string.valueOf(contProd.Site__r.Assigned_Product_Id__c) : '');    
                generator.writeFieldName('orderLinesList');
                generator.writeStartArray();
                    generator.writeStartObject();
                    generator.writeStringField('BAID', contProd.PBX__r.Billing_Arrangement__r.Unify_Ref_Id__c != null ? string.valueOf(contProd.PBX__r.Billing_Arrangement__r.Unify_Ref_Id__c) : '');
                    generator.writeStringField('BCID', contProd.PBX__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c != null ? string.valueOf(contProd.PBX__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c) : '');
                    generator.writeStringField('chargeDescription', ''); // Not used
                    generator.writeStringField('chargeAmount', ''); // Not used
                    generator.writeStringField('unifyFamilyTag', contProd.Unify_Product_Family__c != null ? string.valueOf(contProd.Unify_Product_Family__c) : '');
                    generator.writeFieldName('listOfBORefs');
                    generator.writeStartArray();
                        generator.writeString(contProd.Unify_Billing_Offer__c != null ? string.valueOf(contProd.Unify_Billing_Offer__c) : '');
                    generator.writeEndArray();
                    generator.writeStringField('externalReferenceID', contProd.PBX__c != null ? string.valueOf(contProd.PBX__c) : '');
                    generator.writeEndObject();
                generator.writeEndArray();
                generator.writeEndObject();   
                generator.writeEndObject();            
                system.debug('### Request body: ' + generator.getAsString());

                reqData.setBody(generator.getAsString());
                reqData.setMethod('POST');

                HTTPResponse response = http.send(reqData);

                if(String.isEmpty(contProd.PBX_Change_Error_Info__c)) {
                    contProd.PBX_Change_Error_Info__c = '';
                }
                
                System.debug('####Response body: ' + response.getBody());
                // If the request is successful, parse the JSON response.
                if (response.getStatusCode() == 200) {
                    // Deserializes the JSON string.
                    ChangePBXOrderSuccessResponse resultSuccess = (ChangePBXOrderSuccessResponse) JSON.deserializeStrict(response.getBody(), ChangePBXOrderSuccessResponse.class);
                    ChangePBXOrderRespData result = resultSuccess.changePBXOrderResponse;
                    OrderLineListType orderLine = result.orderLinesList[0];
                    
                    // When successfull set the processing timestamp
                    if (contProd.PBX__r.Assigned_Product_Id__c == orderLine.APID && contProd.PBX__c == orderLine.externalReferenceID) {
                        String assetId = contProd.PBX__c;
                        DateTime timestamp = datetime.now();
                        Competitor_Asset__c compAsset = [SELECT Id FROM Competitor_Asset__c WHERE Id = :assetId];
                        if (compAsset != null) {
                            compAsset.Billing_Offer_synced_to_Unify_timestamp__c = timestamp;
                            compAsset.PBX_Trunking_Done__c = true;
                            update compAsset;       
                        }
                        contProd.Billing_Offer_synced_to_Unify_timestamp__c = timestamp;
                    }
                    // Otherwise write the error response to the Contracted Product
                    else if (orderLine.errorInfo != null) {
                        String errInfo = orderLine.errorInfo.errorCode + ': ' + orderLine.errorInfo.errorDescription + ' (' + orderLine.errorInfo.targetSystemName + ': ' + orderLine.errorInfo.targetServiceName + ')';
                        contProd.PBX_Change_Error_Info__c = errInfo.replace('&quot;', '"').left(255);
                    }
                    update contProd;
                }
                // Otherwise write the error response to the Contracted Product
                else {
                    //ChangePBXOrderErrorResponse result = (ChangePBXOrderErrorResponse) JSON.deserializeStrict(response.getBody(), ChangePBXOrderErrorResponse.class);
                    // String result = (String) JSON.deserialize(response.getBody(), String.class);
                    String result = response.getBody();
                    contProd.PBX_Change_Error_Info__c = result.replace('&quot;', '"').left(255);
                    update contProd;
                }
            }
        }
        catch(Exception e) {
            throw e;
        }
        finally {
            // Nothing to do here
        }
    }

    public class ChangePBXOrderSuccessResponse {
        public ChangePBXOrderRespData changePBXOrderResponse;
        public ChangePBXOrderSuccessResponse(ChangePBXOrderRespData changePBXOrderResponse) {
            this.changePBXOrderResponse = changePBXOrderResponse;
        }
    }
    public class ChangePBXOrderRespData {
        List<OrderLineListType> orderLinesList;
        public ChangePBXOrderRespData(List<OrderLineListType> orderLinesList) {
            this.orderLinesList = orderLinesList;
        }
    }
    public class OrderLineListType {
        public String APID;
        public String externalReferenceID;
        ErrorInfo errorInfo;
        public OrderLineListType(String APID, String externalReferenceID, ErrorInfo errorInfo) {
            this.APID = APID;
            this.externalReferenceID = externalReferenceID;
            this.errorInfo = errorInfo;
        }
    }
/*  private class ChangePBXOrderErrorResponse {
        ErrorInfo errorInfo;
    } */
    public class ErrorInfo {
        public String errorCode;
        public String errorDescription;
        public String targetSystemName;
        public String targetServiceName;
        public ErrorInfo(String errorCode,
                String errorDescription,
                String targetSystemName,
                String targetServiceName) {
            this.errorCode = errorCode;
            this.errorDescription = errorDescription;
            this.targetSystemName = targetSystemName;
            this.targetServiceName = targetServiceName;
        }
    }
}