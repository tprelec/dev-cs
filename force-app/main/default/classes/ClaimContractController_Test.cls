@IsTest
public class ClaimContractController_Test {
    
    public static testMethod void controller_test() {
        Account acct = TestUtils.createAccount(GeneralUtils.currentUser);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        VF_Contract__c c = TestUtils.createVFContract(acct, opp);

        Test.startTest();
            User claimer = TestUtils.createDealer();
            GeneralUtils.userMap.put(claimer.Id, claimer);
            Contact claimerContact = TestUtils.createContact(acct);
            Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(claimerContact.id, '123321');
        
            ApexPages.StandardController sc = new ApexPages.StandardController(acct);       
            ClaimContractController testCCC = new ClaimContractController(sc);
            ClaimContractController.cVFContractc testWrap2 = new ClaimContractController.cVFContractc(c,true);
            
            PageReference pageRef = Page.ClaimContract;     
            Test.setCurrentPage(pageRef);           
            testCCC.getContracts();
            System.assertEquals(testCCC.VFCDetail, null);
            testCCC.showDetail();
            testCCC.showDetail();
            testCCC.cancelSelected();
            
            acct.VGE__c = true;
            update acct;
            ApexPages.StandardController ctrl = new ApexPages.StandardController(acct);     
            ClaimContractController ccCtrl = new ClaimContractController(sc);
            PageReference pageRef2 = Page.ClaimContract;        
            Test.setCurrentPage(pageRef);
            testCCC.getContracts();
        Test.stopTest();
    }
    
    public static testMethod void controller_IND_test() {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TestUtils.createCompleteContract();
        TestUtils.autoCommit = true;
        VF_Contract__c c = TestUtils.theContract;
        Test.startTest();
            Account acct = TestUtils.theAccount;
            c.Contract_Activation_Date__c = System.today().addMonths(1);
            acct.VGE__c = false;
            acct.Ziggo_Customer__c = true;
            acct.Invoiced_Vodafone_Fixed_Converged__c = true;
            acct.Vodafone_Customer__c = false;
            update acct;
            Account partnerAcc = TestUtils.createPartnerAccount();
            User u = TestUtils.createPortalUser(partnerAcc);
            GeneralUtils.userMap.put(u.Id, u);
            update new Contact(Id = u.ContactId, UserId__c = u.Id);
            Dealer_Information__c dealerInfoPortal = TestUtils.createDealerInformation(u.ContactId, '123324');
            c.Dealer_Information__c = dealerInfoPortal.Id;
            c.OwnerId = u.Id;
            update c;
            Id ziggoRec = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName().get('CTN').getRecordTypeId();
            VF_Asset__c asset = new VF_Asset__c(RecordTypeId = ziggoRec, Account__c = acct.id, Account_KVK__c = '12345678', Priceplan_Class__c = 'fZiggo IB', CTN_Status__c = 'Active', Contract_VF__c = c.Id);
            insert asset;

            Contact currentContact = new Contact();
            currentContact.AccountId = [SELECT Id FROM Account LIMIT 1].Id;
            currentContact.LastName = 'Current';
            currentContact.FirstName = 'User';
            currentContact.UserId__c = UserInfo.getUserId();
            insert currentContact;
            Dealer_Information__c dealerInfoCU = TestUtils.createDealerInformation(currentContact.Id, '123322');
            System.assertEquals([SELECT Id, ContactUserId__c FROM Dealer_Information__c WHERE Id = :dealerInfoCU.Id].ContactUserId__c, UserInfo.getUserId());
            
            ApexPages.StandardController sc = new ApexPages.StandardController(acct);       
            ClaimContractController testCCC = new ClaimContractController(sc);
            PageReference pageRef = Page.ClaimContract;     
            Test.setCurrentPage(pageRef);
            
            // test no selected contracts
            testCCC.getContracts();
            testCCC.processSelected();
            testCCC.showDetail();

            // select contracts and cancel
            for (ClaimContractController.cVFContractc cc : testCCC.getContracts()) {
                cc.selected = true;
            }
            testCCC.processSelected();
            testCCC.cancelSelected();
            System.assertEquals(testCCC.selectedVFs.size(), 0);
            testCCC.selectAll();
            testCCC.cancelSelected();
            testCCC.selected = true;
            testCCC.selectAll();
            testCCC.cancelSelected();

            // select contracts and confirm
            for (ClaimContractController.cVFContractc cc : testCCC.getContracts()) {
                cc.selected = true;
            }
            testCCC.processSelected();
            System.assertEquals(testCCC.selectedVFs.size(), 1);
            testCCC.confirmSelected();

            List<Claimed_Contract_Approval_Request__c> ccar = [
                SELECT Id, Account__c, Approver__c, CreatedById, 
                    First_Approval__c, Old_Owner__c, OwnerId
                FROM Claimed_Contract_Approval_Request__c
            ];
            System.assertEquals(1, ccar.size());
            
            for (Claimed_Contract_Approval_Request__c cr : ccar) {
                cr.First_Approval__c = 'Approved';
            }
            update ccar;
            System.assertEquals(ccar[0].OwnerId, UserInfo.getUserId());
        Test.stopTest();
    }
    
    public static testMethod void testBMPClaim() {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TestUtils.createCompleteContract();
        TestUtils.autoCommit = true;
    
        VF_Contract__c c = TestUtils.theContract;
        Test.startTest();
        Account partnerAcc = TestUtils.createPartnerAccount();
        User u = TestUtils.createPortalUser(partnerAcc);
        GeneralUtils.userMap.put(u.Id, u);
        update new Contact(Id = u.ContactId, UserId__c = u.Id);
        c.OwnerId = u.Id;
        Dealer_Information__c dealerInfoPortal = TestUtils.createDealerInformation(u.ContactId, '123324');
        c.Dealer_Information__c = dealerInfoPortal.Id;
        c.Contract_Activation_Date__c = System.today().addMonths(1);
        update c;

        Account acc = TestUtils.theAccount;
        acc.VGE__c = false;
        acc.Ziggo_Customer__c = true;
        acc.Invoiced_Vodafone_Fixed_Converged__c = true;
        acc.Vodafone_Customer__c = false;
        update acc;

        Id ziggoRec = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName().get('CTN').getRecordTypeId();
        VF_Asset__c asset = new VF_Asset__c(RecordTypeId = ziggoRec, Account__c = acc.id, Account_KVK__c = '12345678', Priceplan_Class__c = 'fZiggo IB', CTN_Status__c = 'Active', Contract_VF__c = c.Id);
        insert asset;
        
        Contact currentContact = new Contact(AccountId = acc.Id, LastName = 'Current', FirstName = 'User', UserId__c = UserInfo.getUserId());
        insert currentContact;
        Dealer_Information__c dealerInfoCU = TestUtils.createDealerInformation(currentContact.Id, '123322');

        Profile p = [SELECT Id FROM Profile WHERE Name = 'VF Business Partner Manager' LIMIT 1];
        UserRole r = [SELECT Id FROM UserRole WHERE Name = 'Business Partner Manager' LIMIT 1];
        User bpm = TestUtils.generateTestUser('BPM', 'BPM', p.Id, r.Id);
        System.runAs(GeneralUtils.currentUser) {
            insert bpm;
            GeneralUtils.userMap.put(bpm.Id, bpm);
        }

        System.runAs(bpm) {
            ApexPages.StandardController sc = new ApexPages.StandardController(acc);        
            ClaimContractController testCCC = new ClaimContractController(sc);
            PageReference pageRef = Page.ClaimContract;     
            Test.setCurrentPage(pageRef);

            
            System.debug('isBPM ' + testCCC.isBPM);
            for (ClaimContractController.cVFContractc cc : testCCC.getContracts()) {
                cc.selected = true;
            }
            testCCC.processSelected();
            testCCC.confirmSelected();
            testCCC.cancelSelected();

            testCCC.selectAll();
            testCCC.confirmSelected();
            testCCC.cancelSelected();
            
            testCCC.dealerCR.Dealer_Info__c = null;
            testCCC.setCurrentDealerInfo();
            testCCC.dealerCR.Dealer_Info__c = dealerInfoCU.Id;
            testCCC.setCurrentDealerInfo();
        }
        Test.stopTest();
    }
}