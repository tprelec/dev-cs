public with sharing class CompetitorAssetTriggerHandler extends TriggerHandler {
    private List<Competitor_Asset__c> newAssets;
    private List<Competitor_Asset__c> oldAssets;
    private Map<Id, Competitor_Asset__c> newAssetsMap;
    private Map<Id, Competitor_Asset__c> oldAssetsMap;

    private void init() {
        oldAssets = (List<Competitor_Asset__c>) this.oldList;
        newAssets = (List<Competitor_Asset__c>) this.newList;
        oldAssetsMap = (Map<Id, Competitor_Asset__c>) this.oldMap;
        newAssetsMap = (Map<Id, Competitor_Asset__c>) this.newMap;
    }

    public override void afterUpdate() {
        init();
        submitSIPCharging();
    }

    /**
     * @description         This submits any pending SIP Charging items
     */
    private void submitSIPCharging() {
        Set<Id> cpsToUpdate = new Set<Id>();
        Set<Id> compAssetIds = new Set<Id>();
        for (Competitor_Asset__c ca : newAssets) {
            if (GeneralUtils.isRecordFieldChanged(ca, oldAssetsMap, 'PBX_Trunking_Done__c')) {
                compAssetIds.add(ca.Id);
            }
        }
        if (!compAssetIds.isEmpty()) {
            for (Contracted_Products__c cp : [
                SELECT Id
                FROM Contracted_Products__c
                WHERE
                    PBX__c IN :compAssetIds
                    AND Delivery_Status__c = :Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED
                    AND Order__r.O2C_Order__c = TRUE
                    AND Product__r.Family_Tag__r.Name = :Constants.PRODUCT2_FAMILYTAG_PBX_Trunking_SIP_Charging
                    AND Proposition_Contains_OneNet__c = FALSE
                    AND Do_not_export__c = FALSE
                    AND Billing_Offer_synced_to_Unify_timestamp__c = NULL
                    AND PBX_Change_Error_Info__c = NULL
            ]) {
                cpsToUpdate.add(cp.Id);
            }
            // Do not execute when invoked from test class, because of DML before Callout error
            if (!cpsToUpdate.isEmpty() && !Test.isRunningTest()) {
                Database.executeBatch(new ChangePBXOrderBatch(cpsToUpdate), 1);
            }
        }
    }
}