public with sharing class Constants {
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_NEW = 'New';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_REQUESTED = 'Requested';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_PROCESSED = 'Processed';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_FAILED = 'Failed';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_NO_BILLING = 'No Billing';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_SPECIAL_BILLING = 'Special Billing';

	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_NEW = 'New';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_IN_DELIVERY = 'In Delivery';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_AWAITING_APPROVAL = 'Awaiting Approval';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED = 'Approved';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_CANCELLED = 'Cancelled';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_OPEN = 'OPEN';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_ROLLBACK = 'Rollback Requested';

	public static final String CONTRACTED_PRODUCT_BOP_STATUS_CLOSED = 'CLOSED';

	public static final String CONTRACTED_PRODUCT_CLC_ACQ = 'Acq';

	public static final String CONTRACTED_PRODUCT_QUANTITY_TYPE_MONTHLY = 'Monthly';

	public static final String CONTRACTED_PRODUCT_ROW_TYPE_ADD = 'Add';
	public static final String CONTRACTED_PRODUCT_ROW_TYPE_REMOVE = 'Remove';
	public static final String CONTRACTED_PRODUCT_ROW_TYPE_PRICE_CHANGE = 'Price_change';

	public static final Set<String> CONTRACTED_PRODUCTS_PROPOSITION_ONE_NET = new Set<String>{
		'One Net',
		'One Net Enterprise',
		'One Net Express'
	};
	public final static Set<String> FAMILYTAGSTHATREQUIREPBX = new Set<String>{
		'PBX_Trunking',
		'PBX_Number_Charging',
		'PBX_Trunking_SIP_CHARGING'
	};

	public static final String CUSTOMER_ASSET_BILLING_STATUS_NEW = 'New';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_ACTIVE = 'Active';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_COMPLETED = 'Completed';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_CANCELLED = 'Cancelled';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_NO_BILLING = 'No Billing';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_SPECIAL_BILLING = 'Special Billing';

	public static final String CUSTOMER_ASSET_TYPE_NORMAL = 'Normal';

	public static final String ORDER_STATUS_NEW = 'New';
	public static final String ORDER_STATUS_SUBMITTED = 'Submitted';
	public static final String ORDER_STATUS_ACCEPTED = 'Accepted';
	public static final String ORDER_BOP_ORDER_STATUS_PROJECT_CREATED = 'Project Created';
	public static final String ORDER_STATUS_PROCESSED_MANUALLY = 'Processed manually';
	public static final String ORDER_TYPE_BMS = 'Business Managed Service';

	public static final String PRODUCT2_BILLINGTYPE_SPECIAL = 'Special';
	public static final String PRODUCT2_BILLINGTYPE_STANDARD = 'Standard';

	public static final String PRODUCT2_FAMILYTAG_PBX_TRUNKING_SIP_CHARGING = 'PBX_Trunking_SIP_Charging';
	public static final String PRODUCT2_FAMILYTAG_PBX_TRUNKING = 'PBX_Trunking';

	public static final String ORDERTYPE_EXPORT_SYSTEM_BOP = 'BOP';
	public static final String OPPORTUNITY_DEALTYPE_NEW = 'NEW';
	public static final String OPPORTUNITY_DEALTYPE_ACQUISITION = 'Acquisition';

	public static final String PROFILE_INDIRECT_SALES = 'VF Indirect Inside Sales';
	public static final String PROFILE_DIRECT_SALES = 'VF Direct Inside Sales';
	public static final String PROFILE_BPM = 'VF Business Partner Manager';

	public static final String ORCHESTRATOR_STEP_COMPLETE = 'Complete';
	public static final String ORCHESTRATOR_STEP_ERROR = 'Error';

	public static final String DOCUSIGN_ATTACHMENT_STATUS_DEFAULT = 'DEFAULT';
	public static final String DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED = 'ATTACHMENTS ATTACHED';
	public static final String DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_TO_BE_REMOVED = 'ATTACHMENTS TO BE REMOVED';
	public static final String DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_REMOVED = 'ATTACHMENTS REMOVED';

	public static final String ACCOUNT_APPROVAL_STATUS_REJECTED = 'Rejected';

	public static final String CONNECTION_TYPE_VOICE = 'Voice';
	public static final String CONNECTION_TYPE_DATA = 'Data';
	public static final String CONNECTION_TYPE_VOICE_CODE = '06';
	public static final String CONNECTION_TYPE_DATA_CODE = '097';

	public static final String VF_ASSET_CTN_STATUS_ACTIVE = 'Active';
	public static final String VF_ASSET_CTN_STATUS_SUSPENDED = 'Suspended';
	public static final String VF_ASSET_CTN_STATUS_CANCELLED = 'Cancelled';

	public static final String VF_CONTRACT_STATUS_ACTIVE = 'Active';
	public static final String VF_CONTRACT_STATUS_UNLOCKED = 'Unlocked';
	public static final String VF_CONTRACT_STATUS_TERMINATED = 'Terminated';
	public static final String VF_CONTRACT_MOBILE_SERVICE = 'Mobile';
	public static final String VF_CONTRACT_STATUS_NOT_SIGNED = 'Not Signed';
	public static final String VF_CONTRACT_STATUS_IMPLEMENTATION = 'Implementation';
	public static final String VF_CONTRACT_MOBILE_IMPLEMENTATION_INDIRECT_SALES = 'Implementation Indirect Sales';

	public static final String SNAPSHOT_CONNECTION_TYPE_DISCONNECT = 'Disconnect';
	public static final String CUSTOMER_ASSET_PENDING_CHANGE = 'Pending Change';

	public static final List<String> DEAL_SUMMARY_PROFILES_FOR_DISCONNECTS = new List<String>{
		'System Administrator',
		'VF EBU/WBU Control',
		'VF Financial Sales Control'
	};
	
}