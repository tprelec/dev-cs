/**
 * @description         This is the class that contains logic for running the Contact export in batches
 * @author              Guy Clairbois
 */
global class ContactExportBatch implements Database.Batchable <sObject>, Database.AllowsCallouts{
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,BOP_export_datetime__c,BOP_Export_Errormessage__c FROM Contact Where BOP_Export_Errormessage__c != null AND BOP_Export_Errormessage__c != \'Email is required for triggering the export to BOP\' ');
    } 
 
    global void execute(Database.BatchableContext BC, List<Contact> scope){
        // do Contact exporting here
        set<Id> createContactIds = new Set<Id>();
        set<Id> updateContactIds = new Set<Id>();
        // only process the first 100 records because any more will cause issues (10 accounts per call, 10 calls per transaction)
        Integer size = math.min(scope.size(),100); 

        for(Integer i=0;i<size;i++){
            // only process if 2 first characters are non-numeric (so not coming from BOP)
            if(!scope[i].BOP_Export_Errormessage__c.left(1).isNumeric()) {
                if(scope[i].BOP_export_datetime__c == null)
                    createContactIds.add(scope[i].Id);
                else
                    updateContactIds.add(scope[i].Id);
            }
        }
        
        List<Contact> contactExportResults = ContactExport.exportContacts(createContactIds,'create');
        contactExportResults.addAll(ContactExport.exportContacts(updateContactIds,'update'));
        Database.update(contactExportResults,false);
    }
 
    global void finish(Database.BatchableContext BC){
        ExportUtils.startNextJob('ContactRoles');
    }
 
}