/**
 * @description         This is the class that contains logic for running the ContactRole export in batches
 * @author              Guy Clairbois
 */
global class ContactRoleExportBatch implements Database.Batchable <sObject>, Database.AllowsCallouts{
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,BOP_export_datetime__c,BOP_Export_Errormessage__c,Active__c FROM Contact_Role__c Where BOP_Export_Errormessage__c != null ');
    } 
 
    global void execute(Database.BatchableContext BC, List<Contact_Role__c> scope){
        // do Contactrole exporting here
        set<Id> createContactRoleIds = new Set<Id>();
        set<Id> deleteContactRoleIds = new Set<Id>();
        // only process the first 100 records because any more will cause issues (10 accounts per call, 10 calls per transaction)
        Integer size = math.min(scope.size(),100); 

        for(Integer i=0;i<size;i++){
            // only process if 2 first characters are non-numeric (so not coming from BOP)
            if(!scope[i].BOP_Export_Errormessage__c.left(1).isNumeric()) {
                if(scope[i].Active__c)
                    createContactRoleIds.add(scope[i].Id);
                else
                    deleteContactRoleIds.add(scope[i].Id);
            }
        }
        
        List<Contact_Role__c> contactRoleExportResults = ContactExport.exportContactRoles(createContactRoleIds,'create');
        contactRoleExportResults.addAll(ContactExport.exportContactRoles(deleteContactRoleIds,'delete'));
        Database.update(contactRoleExportResults,false);
    }
 
    global void finish(Database.BatchableContext BC){
        ExportUtils.startNextJob('Users');
    }
 
}