/**
 * @description			This is the trigger handler for the Contact
 */
public with sharing class ContactTriggerHandler extends TriggerHandler {

	/**
	 * @description			This handles the before insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeInsert(){
		List<Contact> newContacts = (List<Contact>) this.newList;	
		CleanUp(newContacts);
	}

    
	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterInsert(){
		List<Contact> newContacts = (List<Contact>) this.newList;	
		
		checkOverlap(newContacts);
		triggerContactExport(newContacts, null);
		SharingUtils.rollUpNpsContacts(newContacts, false);

	}

	/**
	 * @description			This handles the aster insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeUpdate(){
		List<Contact> newContacts = (List<Contact>) this.newList;
		CleanUp(newContacts);
	}
	

	/**
	 * @description			This handles the after update trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterUpdate(){
		List<Contact> newContacts = (List<Contact>) this.newList;	
		List<Contact> oldContacts = (List<Contact>) this.oldList;
		Map<Id,Contact> newContactsMap = (Map<Id,Contact>) this.newMap;
		Map<Id,Contact> oldContactsMap = (Map<Id,Contact>) this.oldMap;
		
		checkOverlap(newContacts);
		triggerContactExport(newContacts, oldContactsMap);
		contactUserSync(newContactsMap, oldContactsMap);
		updateAccountLeadHotnessSummary(newContacts, oldContactsMap);
		SharingUtils.rollUpNpsContacts(newContacts, false);
	}

	/**
	 * Before Delete
	 */
	public override void BeforeDelete() {
		List<Contact> oldContacts = (List<Contact>) this.oldList;
		SharingUtils.rollUpNpsContacts(oldContacts, true);
	}

	/**
     * @description    W-000572 Cleanup phonenumbers
     */ 
    private void CleanUp(List<Contact> newContacts){
     	
        for (Contact mContact : newContacts){

            if (mContact.Phone != null)
            {
                mContact.Phone = StringUtils.cleanNLPhoneNumberInternal(mContact.Phone);
            }
            if (mContact.HomePhone != null)
            {
                mContact.HomePhone = StringUtils.cleanNLPhoneNumberInternal(mContact.HomePhone);
            }   
            if (mContact.MobilePhone != null)
            {
                mContact.MobilePhone = StringUtils.cleanNLPhoneNumberInternal(mContact.MobilePhone);
            }   
            if (mContact.Fax != null)
            {
                mContact.Fax = StringUtils.cleanNLPhoneNumberInternal(mContact.Fax);
            }
            if (mContact.AssistantPhone != null)
            {
                mContact.AssistantPhone = StringUtils.cleanNLPhoneNumberInternal(mContact.AssistantPhone);
            }
            if (mContact.OtherPhone != null)
            {
                mContact.OtherPhone = StringUtils.cleanNLPhoneNumberInternal(mContact.OtherPhone);
            }
            if (mContact.Direct_Phone_Number__c != null)
            {
                mContact.Direct_Phone_Number__c = StringUtils.cleanNLPhoneNumberInternal(mContact.Direct_Phone_Number__c);
            }
        }
    }




	/**
	 * @description			This checks if a contact with the same email already exists for this customer.
	 */	
	private void checkOverlap(List<Contact> newContacts){
		Set<Id> acctIds = new Set<Id>();
		Set<Id> newContactIds = new Set<Id>();
		Set<String> emails = new Set<String>();
		
		// These emails are used on more than 1 contact/user in production
		// Unless the data changes on these records the emails need to be excluded from this validation
		Set<String> exceptionEmailSet = new Set<String>();
		exceptionEmailSet.add('dealerfinancecommissions.nl@vodafone.com');
		exceptionEmailSet.add('insidesales.nl@vodafone.com');
		exceptionEmailSet.add('cvmops.nl@vodafone.com');

		for(Contact c : newContacts){
			if(c.Email != null){
				if (!exceptionEmailSet.contains(c.Email)) {
					acctIds.add(c.AccountId);
					newContactIds.add(c.Id);
					emails.add(c.Email);					
				}
			}
		}
		
		Set<String> acctIdPlusEmail = new Set<String>();
		// make a list of all existing
		for(Contact c : [Select Id, AccountId, Email From Contact Where Email in:emails AND AccountId in :acctIds AND Id NOT IN :newContactIds]){
			acctIdPlusEmail.add(c.AccountId+c.Email);
		}

		for(Contact c : newContacts){
			if(c.Email != null){
				if(acctIdPlusEmail.contains(c.AccountId+c.Email)){
					c.addError('There is already a Contact with this Email address for this Account.');
					System.Debug('Updating Contact:'+c.id);
					System.Debug('With AccountID:'+c.accountID);
					System.Debug('With Email:'+c.Email);
					System.Debug('Set:'+acctIdPlusEmail);
					System.Debug('Set Size'+acctIdPlusEmail.size());
					System.Debug('Matching criteria was:'+c.AccountId+c.Email);
				} else {
					acctIdPlusEmail.add(c.AccountId+c.Email);
					System.Debug('Adding:'+c.AccountId+c.Email);
					System.Debug('For ContactID:'+c.id);
				}
			}
		}
	}


	/**
	 * @description			This triggers the export to BOP of contacts.
	 */	
	private void triggerContactExport(List<Contact> newContacts, Map<Id,Contact> oldContactsMap){
		Set<Id> changedContactIds = new Set<Id>();
		Set<Id> newContactIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();
		
		if(oldContactsMap != null){
			// update
			for(Contact c : newContacts){
				if(
					// check if relevant fields changed
					c.AccountId != oldContactsMap.get(c.Id).AccountId // Is this allowed?
					|| c.Email != oldContactsMap.get(c.Id).Email
					|| c.Firstname != oldContactsMap.get(c.Id).Firstname
					|| c.Lastname != oldContactsMap.get(c.Id).Lastname
					|| c.Phone != oldContactsMap.get(c.Id).Phone
					|| c.MobilePhone != oldContactsMap.get(c.Id).MobilePhone
					|| c.Fax != oldContactsMap.get(c.Id).Fax
					|| c.Title != oldContactsMap.get(c.Id).Title
					|| c.IsActive__c != oldContactsMap.get(c.Id).IsActive__c
				){
					// filter out updates that are actually inserts but failed the first time
					if(c.BOP_export_datetime__c == null){
						newContactIds.add(c.Id);	
						accountIds.add(c.AccountId);
					} else {
						changedContactIds.add(c.Id);
						accountIds.add(c.AccountId);
					}
				}
			}
		} else {
			// insert
			for(Contact c : newContacts){			
				newContactIds.add(c.Id);
				accountIds.add(c.AccountId);
			}
		}
		
		Set<Id> contactIdsForExport = new Set<Id>();
		if(!newContactIds.isEmpty() || !changedContactIds.isEmpty()){
			// check if Contact's account or one of the BANs has BOPCode (otherwise do not export)
			Set<Id> accountIdsWithBopCode = new Set<Id>();
			/*for(Ban__c b : [Select BOPCode__c, Account__c From Ban__c Where Account__c in :accountIds AND BOPCode__c != null]){
				accountIdsWithBopCode.add(b.Account__c);
			}*/
			for(Account a : [Select BOPCode__c From Account Where Id in :accountIds AND BOPCode__c != null]){
				accountIdsWithBopCode.add(a.Id);
			}
			for(Contact c : [Select 
								Id
							From 
								Contact 
							Where 
								(Id in :changedContactIds OR Id in :newContactIds) 
								AND
								Status_Contact__c = 'Active SF User'
								AND
								AccountId in :accountIdsWithBopCode]
			){
				contactIdsForExport.add(c.Id);
			}
		}
		
		if(!contactIdsForExport.isEmpty()){
			newContactIds.retainAll(contactIdsForExport);
			if(!newContactIds.isEmpty()){
				newContactIds.removeAll(ContactExport.contactsInExport);
				ContactExport.contactsInExport.addAll(newContactIds);
				if(!newContactIds.isEmpty()){
					if(system.isFuture() || System.isBatch()){
						ContactExport.scheduleContactExportFromContactIds(newContactIds);
					} else {
						ContactExport.exportNewContactsOffline(newContactIds);
					}
				}
			}
			changedContactIds.retainAll(contactIdsForExport);
			if(!changedContactIds.isEmpty()){
				changedContactIds.removeAll(ContactExport.contactsInExport);
				ContactExport.contactsInExport.addAll(changedContactIds);
				if(!changedContactIds.isEmpty()){		
					if(system.isFuture() || System.isBatch()){
						ContactExport.scheduleContactExportFromContactIds(changedContactIds);
					} else {
						ContactExport.exportUpdatedContactsOffline(changedContactIds);
					}
				}
			}
		}
	}

	/**
	 * @description			This copies contact data to the User object for partner users.
	 */
	private void contactUserSync(Map<Id,Contact> newContactsMap, Map<Id,Contact> oldContactsMap){
		Set<Id> contactIds = new Set<Id>();
		List<User> usersToUpdate = new List<User>();
		Map<ID,Contact> internalUserMap = new Map<ID,Contact>();
		
		for(Contact c : newContactsMap.values()){
			if(c.Firstname != oldContactsMap.get(c.Id).Firstname || c.LastName != oldContactsMap.get(c.Id).LastName || 
			   c.Email != oldContactsMap.get(c.Id).Email || c.Phone != oldContactsMap.get(c.Id).Phone || 
			   c.MobilePhone != oldContactsMap.get(c.Id).MobilePhone || c.BOP_Reference__c != oldContactsMap.get(c.Id).BOP_Reference__c){
				contactIds.add(c.Id);
				// If this contact is for an internal user
				if (c.userid__c!=null) {
					internalUserMap.put(c.userid__c,c);					
				}
			}
		}
        if(!contactIds.isEmpty() || !internalUserMap.isEmpty()){
            for(User u : [SELECT BOP_Reference__c, ContactId, FirstName, LastName, Email, Phone, MobilePhone from User WHERE contactId in: contactIds or id in: internalUserMap.keyset()]){
                Boolean okToUpdate=false;
                // We only want to update the users if value on user is different
                // Internal user - for internal we only sync the bop reference
                if (u.contactID==null) {
                    if (u.BOP_Reference__c != internalUserMap.get(u.id).BOP_Reference__c) {
                        u.BOP_Reference__c = internalUserMap.get(u.id).BOP_Reference__c;
                        okToUpdate=true;
                    }
                } else {
                    // Partner User
                    if (u.FirstName != newContactsMap.get(u.ContactId).FirstName ||
                            u.LastName != newContactsMap.get(u.ContactId).LastName ||
                            u.Phone != newContactsMap.get(u.ContactId).Phone ||
                            u.MobilePhone != newContactsMap.get(u.ContactId).MobilePhone ||
                            u.BOP_Reference__c != newContactsMap.get(u.ContactId).BOP_Reference__c) {
    
                        u.FirstName = newContactsMap.get(u.ContactId).FirstName;
                        u.LastName = newContactsMap.get(u.ContactId).LastName;
                        u.Phone = newContactsMap.get(u.ContactId).Phone;
                        u.MobilePhone = newContactsMap.get(u.ContactId).MobilePhone;
                        u.BOP_Reference__c = newContactsMap.get(u.ContactId).BOP_Reference__c;	
                        okToUpdate=true;										
                    }
                }
                if (okToUpdate) {
                    usersToUpdate.add(u);
                }
            }
    
            // This was previously not run if we are in a future event, however when testing I can not find a problem
            // If a problem is found here, please document (or even better - fix in the usertriggerhandler) - Gerhard.
            update usersToUpdate;
        }

	}

	/**
     * @description         This method updates the Account Lead Hotness summary. 
     *                      It summarizes all contacts on that account and checks if any of them has any of the scoring models 
     *                      marked high enough to mark Account as hot as well.
     *						
     * 						This method sucks as it updates accounts even if the contact hotness did not change on the contact - fixing
     * 
     * @author              Stjepan Pavuna
     */ 
    private void updateAccountLeadHotnessSummary(List<Contact> newContacts,Map<Id,Contact> oldContactsMap){
        

        Map<Id, Account> accountsToUpdateMap = new Map<Id, Account> ();
        Map<Id, List<Contact>> contactsByAccountId = new Map<Id, List<Contact>>();
        
        //find all accounts that are related to contacts being changed
        for(Contact myContact : newContacts){
        	// Adding check to see if the contact hotness has actually changed. If not we do nothing!
        	if (myContact.Contact_Hotness_Summary__c!=oldContactsMap.get(myContact.id).Contact_Hotness_Summary__c) {
	        	Id accId = myContact.AccountId;
	            if(accId != null)
	                //first put just the Id, later we will fill out the map fully
	                accountsToUpdateMap.put(accId, null);        		
	            }
        }
        if(!accountsToUpdateMap.isEmpty()){
            Set<Id> accIds = accountsToUpdateMap.keySet();
    
            accountsToUpdateMap = new Map<Id, Account> ([select id, name, Account_Lead_Hotness_Summary__c from Account where id in : accountsToUpdateMap.keyset()]);
    
            //find all contacts for all those accounts and put them in a map
    
            //we are using fieldset to define which fields are we using for data models
    
            List<Schema.FieldSetMember> allDataModelFields = SObjectType.Contact.FieldSets.Silverpop_Contact_Score_Fields.getFields();
    
            String sqlQuery = 'select id, AccountId ';
    
            for(FieldSetMember membr : allDataModelFields){
                sqlQuery += ', ' + membr.getFieldPath();
            }
    
            sqlQuery += ' from Contact where AccountId in : accIds';
    
            List<Contact> allContacts = Database.query(sqlQuery);
    
             //get the values that we are checking from the custom settings
            Silverpop_Lead_Settings__c mySettings = Silverpop_Lead_Settings__c.getInstance();
    
    
            for(Contact myContact : allContacts){
    
                Id accId = myContact.AccountId;
    
                List<Contact> contactList = contactsByAccountId.get(accId);
    
                if(contactList == null){
                    contactList = new List<Contact>();
                    contactsByAccountId.put(accId, contactList);
                }
                contactList.add(myContact);
    
    
            }
    
    
    
    
            //for each account, go through it's contacts and see if there is any contact 
            //that has any of the scoring fields with value that is needed to trigger the account hotness (as per custom settings)
    
           
            for(Id accId : contactsByAccountId.keySet()){
    
                List<Contact> contactsForAccount = contactsByAccountId.get(accId);
    
                Boolean foundHotness1 = false;
                Boolean foundHotness2 = false;
                Boolean foundHotness3 = false;
                Boolean foundHotness4 = false;
    
                for(Contact myContact : contactsForAccount){
                    for(FieldSetMember membr : allDataModelFields){
                        if(myContact.get(membr.getFieldPath()) ==  mySettings.SilverPop_Level_1_Lead_Hotness_Label__c){
                            //this field contains a level 1 hotness label
                            foundHotness1 = true;
                        }
                        else if(myContact.get(membr.getFieldPath()) ==  mySettings.SilverPop_Level_2_Lead_Hotness_Label__c){
                            //this field contains a level 2 hotness label
                            foundHotness2 = true;
                        }
                        else if(myContact.get(membr.getFieldPath()) ==  mySettings.SilverPop_Level_3_Lead_Hotness_Label__c){
                            //this field contains a level 3 hotness label
                            foundHotness3 = true;
                        }
                        else if(myContact.get(membr.getFieldPath()) ==  mySettings.SilverPop_Level_4_Lead_Hotness_Label__c){
                            //this field contains a level 4 hotness label
                            foundHotness4 = true;
                        }
                    }
                }
    
                Account acc = accountsToUpdateMap.get(accId);
                if(acc != null)
                {
                    if(foundHotness4){
                        acc.Account_Contact_Hotness_Summary__c = mySettings.SilverPop_Level_4_Lead_Hotness_Label__c;
                    }
                    else if(foundHotness3){
                        acc.Account_Contact_Hotness_Summary__c = mySettings.SilverPop_Level_3_Lead_Hotness_Label__c;
                    }
                    else if (foundHotness2){
                        acc.Account_Contact_Hotness_Summary__c = mySettings.SilverPop_Level_2_Lead_Hotness_Label__c;
                    }
                    else if (foundHotness1){
                        acc.Account_Contact_Hotness_Summary__c = mySettings.SilverPop_Level_1_Lead_Hotness_Label__c;
                    }
                    else{
                         acc.Account_Contact_Hotness_Summary__c = '';
                    }
                }
            }
    
            //update the account
            //update accountsToUpdateMap.values();
            //update without sharing
            SharingUtils.updateObjectsWithoutSharingStatic(accountsToUpdateMap.values());
        }
    }


}