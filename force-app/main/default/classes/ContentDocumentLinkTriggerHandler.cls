public with sharing class ContentDocumentLinkTriggerHandler extends TriggerHandler {
    private static final Set<String> OBJECT_TYPES = new Set<String>{
        'Opportunity_Attachment__c',
        'Contract_Attachment__c',
        'Product_Basket_Attachment__c'
    };

    private List<ContentDocumentLink> newCDLs;

    private void init() {
        newCDLs = (List<ContentDocumentLink>) this.newList;
    }

    public override void beforeInsert() {
        init();
        // This is required to set the visibility to AllUsers so that partner community users can see the documents
        setVisibilityToAllUsers();
    }

    public override void afterInsert() {
        init();
        updateIdOnParent(newCDLs);
    }

    private void setVisibilityToAllUsers() {
        for (ContentDocumentLink cdl : newCDLs) {
            cdl.Visibility = 'AllUsers';
        }
    }

    private void updateIdOnParent(List<ContentDocumentLink> newCDLs) {
        List<sObject> objectsToUpdate = new List<sObject>();

        for (ContentDocumentLink cdl : newCDLs) {
            if (
                OBJECT_TYPES.contains(cdl.LinkedEntityId.getSobjectType().getDescribe().getName())
            ) {
                sObject obj = cdl.LinkedEntityId.getSobjectType().newSObject(cdl.LinkedEntityId);
                obj.put('ContentDocumentId__c', cdl.ContentDocumentId);
                objectsToUpdate.add(obj);
            }
        }
        update objectsToUpdate;
    }
}