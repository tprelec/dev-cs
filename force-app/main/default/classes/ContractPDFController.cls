public with sharing class ContractPDFController {

	public csclm__Agreement__c agreement {get;set;}
    //public opportunity opp {get;set;}

    public ContractPDFController(ApexPages.StandardController stdController) {
        agreement = (csclm__Agreement__c)stdController.getRecord();

        agreement = [select Id,
                            csclm__Opportunity__r.Account.Name                   
                        from csclm__Agreement__c
                        where Id = : agreement.id];                               
    }

}