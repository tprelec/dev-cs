global with sharing class ContractRelatedQuestionsController {
    
    @AuraEnabled
    global static cscfga__Product_Basket__c getBasketDetails(String basketId) {
        List<cscfga__Product_Basket__c> basketList = [
            SELECT Id, Name, csordtelcoa__Basket_Stage__c, cscfga__Basket_Status__c,OwnerAccount__c, Primary__c, csbb__Synchronised_with_Opportunity__c, cscfga__Opportunity__r.Name, 
                csbb__Account__r.Authorized_to_sign_1st__r.Name, csbb__Account__r.Authorized_to_sign_2nd__r.Name, 
                csbb__Account__r.Contract_rule_no_mailing__c, csbb__Account__r.Frame_Work_Agreement__c, 
                csbb__Account__r.Version_FWA__c, csbb__Account__r.Framework_agreement_date__c, cscfga__Opportunity__c, cscfga__Opportunity__r.Direct_Indirect__c,
                Tracking_purposes__c, Sign_on_behalf_of_customer__c, Fiber_connection_in_different_agreement__c, Existing_Contract_ID__c, Existing_Contract_Date__c,
                One_Mobile_install_base__c, Existing_Mobile_Contract_ID__c, Existing_Mobile_Contract_Date__c,
                Vodafone_fixed_phone_service__c, Existing_Fixed_Contract_ID__c, Existing_Fixed_Contract_Date__c,
                OneNetE_with_OneMobileOneBusiness__c, Mobile_integrated_with_OneFixed__c, Create_new_FWA__c, Latest_TandC__c,
                Support_use_of_fax__c, Sellthrough_connectivity__c,cscfga__Opportunity__r.Account.Authorized_to_sign_1st__c, MobileScenarios__c, cscfga__Products_In_Basket__c,
                cscfga__Opportunity__r.Account.Frame_Work_Agreement__c, cscfga__Opportunity__r.Account.VZ_Framework_Agreement__c, Contract_Language__c, BMS_Products__c, 
                DirectIndirect__c, Optional_Clauses__c,
                (SELECT Id, Name, Mobile_Scenario__c, Fixed_Scenario__c, OneNet_Scenario__c FROM cscfga__Product_Configurations__r)
            FROM cscfga__Product_Basket__c
            WHERE Id = :basketId
        ];
        
        if (!basketList.isEmpty()) {
            return basketList[0];           
        } else {
            return null;
        }
    }
    
    @AuraEnabled
    global static List<Contract_Custom_Settings__mdt> getContractCustomSettings(cscfga__Product_Basket__c productBasket) {
        
        List<Contract_Custom_Settings__mdt> contractSettings = [
            SELECT BMS_Grouping__c, Document_template__c, English_contract__c, 
                Mobile_Scenario__c, Product_Configuration__c, Product_Name__c, Product_Code__c 
            FROM Contract_Custom_Settings__mdt
        ];
        List<cscfga__Product_Configuration__c> configs = getProductsFromBasket(productBasket.Id);
        String productsInBasket = '';
        
        for (cscfga__Product_Configuration__c config : configs) {
            productsInBasket += '[' + config.Name + ']';
        }
        
        List<Contract_Custom_Settings__mdt> applicableSettings = new List<Contract_Custom_Settings__mdt>();
        String mobileScenarios = productBasket.MobileScenarios__c;
        String bmsProducts = productBasket.BMS_Products__c;
        
        for (Contract_Custom_Settings__mdt contractSetting : contractSettings) {
            
            Boolean isApplicable = false;
            
            if (contractSetting.Product_Configuration__c != null) {
                
                List<String> productConfigurations = new List<String>();
                
                if (contractSetting.Product_Configuration__c.contains(';')) {
                    productConfigurations.addAll(contractSetting.Product_Configuration__c.split(';'));
                } else {
                    productConfigurations.add(contractSetting.Product_Configuration__c);
                }
                
                for (String productConfiguration : productConfigurations) {
                    if (productsInBasket != '' && productsInBasket.containsIgnoreCase(productConfiguration)) {
                        if (productConfiguration.equalsIgnoreCase('Mobile CTN Profile') && contractSetting.Mobile_Scenario__c != null && mobileScenarios != null && mobileScenarios.containsIgnoreCase(contractSetting.Mobile_Scenario__c)) {
                            isApplicable = true;
                        } 
                        if (productConfiguration.equalsIgnoreCase('Business Managed Services') && contractSetting.BMS_Grouping__c != null && bmsProducts != null && bmsProducts.containsIgnoreCase(contractSetting.BMS_Grouping__c)) {
                            isApplicable = true;
                        }
                        if (!productConfiguration.equalsIgnoreCase('Mobile CTN Profile') && !productConfiguration.equalsIgnoreCase('Business Managed Services')) {
                            isApplicable = true;
                        }
                    }
                }
            }
            
            if (isApplicable) {
                applicableSettings.add(contractSetting);
            }
        }
        
        return applicableSettings;
    }
    
    @AuraEnabled
    global static User getCurrentUser() {
        User currentUser = [SELECT Id, Name, Partner_User__c FROM User WHERE Id = :userinfo.getUserId()];
        return currentUser;
    }
    
    @AuraEnabled
    global static csclm__Agreement__c createBPAIfNeeded(cscfga__Product_Basket__c basket) {
        
        String documentTemplateName = '';
        
        csclm__Agreement__c agreement;
        
        if (basket.cscfga__Opportunity__r.Direct_Indirect__c == 'Indirect') {
            documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_BPA__c;
            List<cscfga__Product_Configuration__c> configsInBasket = getProductsFromBasket(String.valueOf(basket.Id));
            Boolean mobileExists = checkIfMobileExists(configsInBasket);
            
            if (mobileExists == true){
                agreement =  createBPAAgreement(basket);
            }
        }
        
          
        if (agreement != null) {
            return agreement;
        } else {
            return null;
        }
    }
    
    public static Boolean checkIfMobileExists(List<cscfga__Product_Configuration__c> configs) {
        Boolean result = false;
        
        for(cscfga__Product_Configuration__c config : configs){
            if(config.cscfga__Product_Definition__r.Name == 'Mobile CTN addons' || config.cscfga__Product_Definition__r.Name == 'Mobile CTN subscription'){
                result = true;
            }
        }
        
        return result;
    }
    
    global static csclm__Agreement__c createBPAAgreement(cscfga__Product_Basket__c basket) {
        
        
        String documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_BPA__c;
            
        
        List<csclm__Agreement__c> basketAgreementList = new List<csclm__Agreement__c>([SELECT Id, Name, csclm__Document_Template__r.Name, Product_Basket__c FROM csclm__Agreement__c WHERE Product_Basket__c = :basket.Id]);
                
        csclm__Agreement__c agreement = getAgreementWithDocumentTemplateName(documentTemplateName, basketAgreementList);
            
        if (agreement != null) {
            agreement.csclm__Approval_Status__c = 'Approved';
            update agreement;
            return agreement;
        } else {
            return createAgreementAndLinkToBasket(basket, documentTemplateName, true);
        }
    }
    
    @AuraEnabled
    global static csclm__Agreement__c createAgreement(cscfga__Product_Basket__c basket) {
        
        String documentTemplateName = '';
        
        if (basket.cscfga__Opportunity__r.Direct_Indirect__c == 'Direct') {
            documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_direct__c;
        } else if (basket.cscfga__Opportunity__r.Direct_Indirect__c == 'Indirect'){
            documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_indirect__c;
        }
        
        List<csclm__Agreement__c> basketAgreementList = new List<csclm__Agreement__c>([SELECT Id, Name, csclm__Document_Template__r.Name, Product_Basket__c FROM csclm__Agreement__c WHERE Product_Basket__c = :basket.Id]);

        csclm__Agreement__c agreement = getAgreementWithDocumentTemplateName(documentTemplateName, basketAgreementList);
            
        if (agreement != null){
            agreement.csclm__Approval_Status__c = 'Approved';
            update agreement;
            return agreement;
        } else {
            return createAgreementAndLinkToBasket(basket, documentTemplateName, false);
        }
    }

    @AuraEnabled
    global static csclm__Agreement__c createAgreement2(cscfga__Product_Basket__c basket, String template){
        
        // since April 2020
        // new template is applied to products following new "document structure" including new Mantelovereenkomst --> Direct 2
       
        String documentTemplateName = '';
        
        switch on template {
            when 'Indirect' {
                documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_indirect__c;
            }
            when 'Direct' {
                documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_direct__c;
            }
            when 'Direct 2' {
                documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_direct_2__c;
            }
            when 'Direct English' {
                documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_direct_English__c;
            }
            when 'Indirect English' {
                documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_indirect_English__c;
            }
            when else {
                documentTemplateName = Contract_Generation_Settings__c.getInstance().Document_template_name_direct__c;
            }
        }
                
        List<csclm__Agreement__c> basketAgreementList = new List<csclm__Agreement__c>([SELECT Id, Name, csclm__Document_Template__r.Name, Product_Basket__c FROM csclm__Agreement__c WHERE Product_Basket__c = :basket.Id]);
                
        csclm__Agreement__c agreement = getAgreementWithDocumentTemplateName(documentTemplateName, basketAgreementList);
            
        if (agreement != null) {
            agreement.csclm__Approval_Status__c = 'Approved';
            update agreement;
            return agreement;
        } else {
            return createAgreementAndLinkToBasket(basket, documentTemplateName, false);
        }
    }
    
    @AuraEnabled
    global static Boolean updateBasketStatus(cscfga__Product_Basket__c basket, String value) {

        basket.cscfga__Basket_Status__c = value;
        
        Database.SaveResult saveResult = Database.Update(basket, false);
        
        if (saveResult.isSuccess()) {
            return true;
        } else {
            return false;
        }
    }
    
    @AuraEnabled
    global static Boolean updateBasket(cscfga__Product_Basket__c basket, Map<String, Object> fieldsToUpdate) {    
                
        for (String key : fieldsToUpdate.keySet()) { 
            Schema.DisplayType dt = getFieldType('cscfga__Product_Basket__c', key);
            if (dt == Schema.DisplayType.Date) {
                if (fieldsToUpdate.get(key) == null) {
                    basket.put(key, null);
                } else {
                    String d = fieldsToUpdate.get(key).toString();
                    String[] strList = d.split('-');
                    Date dte = Date.newInstance(Integer.valueOf(strList[0]), Integer.valueOf(strList[1]), Integer.valueOf(strList[2])); 
                    basket.put(key, dte);
                }
            } else if (dt == Schema.DisplayType.Boolean) {
                if (fieldsToUpdate.get(key) == null) {
                    basket.put(key, false);
                } else {
                    basket.put(key, Boolean.valueOf(fieldsToUpdate.get(key)));
                }
            } else {
                if (fieldsToUpdate.get(key) == null) {
                    basket.put(key, null);
                } else {
                    basket.put(key, fieldsToUpdate.get(key));
                }
            }   
        }       
        
        
        
        Database.SaveResult saveResult = Database.Update(basket, false);
        
        if (saveResult.isSuccess()) {
            return true;
        } else {
            return false;
        }
    }

    @AuraEnabled
    global static List<cscfga__Product_Configuration__c> getProductsFromBasket(String basketId) {
        List<cscfga__Product_Configuration__c> pcList = [
            SELECT ID, Name, cscfga__Product_Basket__c, Mobile_Scenario__c, Fixed_Scenario__c, OneNet_Scenario__c, cscfga__Product_Definition__c,cscfga__Product_Definition__r.Name 
            FROM cscfga__Product_Configuration__c
            WHERE cscfga__Product_Basket__c = :basketId
        ];
        
        return pcList;                                                                                          
    }
    
    @future(callout=true)
    private static void createPDF(Id parentId) {
        if (Test.isRunningTest()) {
            System.debug('**** This is a test' + parentId);
        } else {
            PageReference ref = new PageReference('/apex/csclmcb__GenerateDocument?id=' + parentId + '&s=1');
            ref.getContent();
        }
    }
    
    private static csclm__Agreement__c createAgreementAndLinkToBasket(cscfga__Product_Basket__c basket, String documentTemplateName, Boolean isBPA) {   
        
        List<csclm__Document_Template__c> documentTemplate = [
            SELECT Id, Name,csclm__Valid__c,csclm__Active__c
            FROM csclm__Document_Template__c
            WHERE Name = :documentTemplateName AND csclm__Valid__c = true AND csclm__Active__c = true LIMIT 1
        ];
       
        csclm__Agreement__c newAgreement = new csclm__Agreement__c();
        newAgreement.Name = basket.Name;
        if (isBPA) {
            newAgreement.Name = basket.Name + '-BPA';
        }
        
        if (documentTemplateName.endsWith('2'))
            newAgreement.Name = basket.Name + '--2';
       
        newAgreement.csclm__Document_Template__c = documentTemplate[0].Id;
        newAgreement.csclm__Output_Format__c = 'pdf';
        newAgreement.Product_Basket__c = basket.Id;
        newAgreement.csclm__Opportunity__c = basket.cscfga__Opportunity__c;
        newAgreement.csclm__Requires_Update__c = false;
        
        
        documentTemplate[0].csclm__Valid__c = true;
        documentTemplate[0].csclm__Active__c = true;
        
        Id RecordTypeIdAgreement = Schema.SObjectType.csclm__Agreement__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        newAgreement.RecordTypeId = RecordTypeIdAgreement;
         
        
        
        newAgreement.csclm__Approval_Status__c = 'Approved';
        insert newAgreement;
        newAgreement.csclm__Approval_Status__c = 'Approved';
        update newAgreement;
        
        createPDF(newAgreement.Id);
        
        return newAgreement;
    }
    
    private static csclm__Agreement__c getAgreementWithDocumentTemplateName(String templateName, List<csclm__Agreement__c> agreementList) {
        for (csclm__Agreement__c agreement : agreementList){
            if (agreement.csclm__Document_Template__r.Name == templateName){
                return agreement;
            }
        }
        
        return null;
    }
    
    private static Schema.DisplayType getFieldType(String sObjectType, String fieldName) {
        Schema.DisplayType fieldType;
        
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType schema = schemaMap.get(sObjectType);
        Map<String, Schema.SObjectField> fieldMap = schema.getDescribe().fields.getMap();
        
        Schema.sObjectField fld = fieldMap.get(fieldName.toLowerCase());
        fieldType = fld.getDescribe().getType();
        
        return fieldType;
    }   
}