@isTest
private class ContractRelatedQuestionsControllerTest {

    private static testMethod void test() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<Profile> pList2 = [select Id, Name from Profile where Name = 'VF Partner Portal User' LIMIT 1];
        
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        ID rId = roleList[0].Id;
        ID pId = pList[0].Id;
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;
        
        System.runAs (simpleUser) {
            Test.startTest();
            
            No_Triggers__c notriggers = new No_Triggers__c();
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;
            
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;

            Account tmpAcc2 = CS_DataTest.createAccount('Test Account 2');
            insert tmpAcc2;
            
            Contact con = CS_DataTest.createContact('Contact Name', 'String lastName', 'not so importan role', 'mail@address.com', tmpAcc.Id);
            con.Email = 'mail@address.com';
            insert con;
            
            User simpleUser2 = CS_DataTest.createUserName(pList, roleList, 'testUserABCD@testorganise.com');
            simpleUser2.Email = 'test@testorg.com';
            insert simpleUser2;
            //simpleUser2.ContactId = con.Id;
            //update simpleUser2;
            

            Contract_Generation_Settings__c contractGenSettings = new Contract_Generation_Settings__c();
            contractGenSettings.Document_template_name_direct__c = 'Direct Sales Template';
            contractGenSettings.Document_template_name_direct_2__c = 'Direct Sales Template 2';
            contractGenSettings.Document_template_name_BPA__c = 'BPA Template new';
            insert contractGenSettings;
            
            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
            tmpOpp.Owner = simpleUser;
            insert tmpOpp;

            Opportunity tmpOpp2 = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser2.Id);
            tmpOpp.Owner = simpleUser2;
            insert tmpOpp2;
            
            for(Opportunity opp : [select Direct_Indirect__c from Opportunity where Id = :tmpOpp.Id]) {
                system.debug('**** opp : ' + opp.Direct_Indirect__c );   
                update opp;
            }

            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);  
            insert tmpProductBasket;
            
            for(cscfga__Product_Basket__c basket : [select cscfga__Opportunity__r.Direct_Indirect__c from cscfga__Product_Basket__c where Id = :tmpProductBasket.Id]) {
                system.debug('**** basket : ' + basket.cscfga__Opportunity__r.Direct_Indirect__c );   
                tmpProductBasket = basket;
                update tmpProductBasket;
            }
            
            system.debug('**** tmpProductBasket: ' + tmpProductBasket);
            system.debug('**** tmpProductBasket.cscfga__Opportunity__r.Direct_Indirect__c: ' + tmpProductBasket.cscfga__Opportunity__r.Direct_Indirect__c);
            
            cscfga__Product_Basket__c tmpProductBasket2 = CS_DataTest.createProductBasket(tmpOpp2, 'Test basket2', tmpAcc2);  
            insert tmpProductBasket2;
            for(cscfga__Product_Basket__c basket : [select Name, cscfga__Opportunity__r.Direct_Indirect__c from cscfga__Product_Basket__c where Id = :tmpProductBasket2.Id]) {
                system.debug('**** basket : ' + basket.cscfga__Opportunity__r.Direct_Indirect__c );   
                tmpProductBasket2 = basket;
                update tmpProductBasket2;
            }
            
            csclm__Document_Definition__c testDocumentDefinition = CS_DataTest.createDocumentDefinition();
            testDocumentDefinition.ExternalID__c = 'SomeId41';
            insert testDocumentDefinition;
            
            csclm__Document_Template__c docTemplate = CS_DataTest.createDocumentTemplate('Direct Sales Template');
            docTemplate.csclm__Document_Definition__c = testDocumentDefinition.Id;
            docTemplate.csclm__Valid__c = true;
            docTemplate.csclm__Active__c = true;
            docTemplate.ExternalID__c = 'SomeId4';
            insert docTemplate;
            system.debug('++++docTemplate: ' + docTemplate);

            csclm__Document_Template__c docTemplateBPA = CS_DataTest.createDocumentTemplate('BPA Template new');
            docTemplateBPA.csclm__Document_Definition__c = testDocumentDefinition.Id;
            docTemplateBPA.csclm__Valid__c = true;
            docTemplateBPA.csclm__Active__c = true;
            docTemplateBPA.ExternalID__c = 'SomeId5';
            insert docTemplateBPA;

            csclm__Document_Template__c docTemplate2 = CS_DataTest.createDocumentTemplate('Direct Sales Template 2');
            docTemplate2.csclm__Document_Definition__c = testDocumentDefinition.Id;
            docTemplate2.csclm__Valid__c = true;
            docTemplate2.csclm__Active__c = true;
            docTemplate2.ExternalID__c = 'SomeId42';
            insert docTemplate2;
            
            csclm__Agreement__c testAgreement = CS_DataTest.createAgreement('TestAgreement');
            testAgreement.csclm__Document_Template__c = docTemplate.Id;
            testAgreement.Product_Basket__c = tmpProductBasket.Id;
            insert testAgreement;

            csclm__Agreement__c testAgreementBPA = CS_DataTest.createAgreement('TestAgreement');
            testAgreementBPA.csclm__Document_Template__c = docTemplateBPA.Id;
            testAgreementBPA.Product_Basket__c = tmpProductBasket.Id;
            insert testAgreementBPA;

            csclm__Agreement__c testAgreement2 = CS_DataTest.createAgreement('TestAgreement 2');
            testAgreement2.csclm__Document_Template__c = docTemplate2.Id;
            testAgreement2.Product_Basket__c = tmpProductBasket.Id;
            insert testAgreement2;
            
            cscfga__Product_Definition__c tmpPD = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
            tmpPD.Product_Type__c = 'Mobile';
            insert tmpPD;

            cscfga__Product_Configuration__c tmpPc = CS_DataTest.createProductConfiguration(tmpPD.Id, 'Config Name', tmpProductBasket2.Id);
            insert tmpPc;


            ContractRelatedQuestionsController.getBasketDetails(tmpProductBasket.Id);
            ContractRelatedQuestionsController.createBPAIfNeeded(tmpProductBasket);

            //ContractRelatedQuestionsController.createBPAIfNeeded(tmpProductBasket2);
            
            ContractRelatedQuestionsController.updateBasketStatus(tmpProductBasket, 'Valid');
            ContractRelatedQuestionsController.updateBasket(tmpProductBasket, new Map<String, Object>{  'cscfga__Basket_Status__c' => 'Valid', 
                                                                                                        'Contract_end_Fixed__c' => DateTime.newInstance(Date.today().year(), Date.today().month(), Date.today().day()).format('yyyy-MM-dd'),
                                                                                                        'Block_Expiration__c' => false});

                ContractRelatedQuestionsController.updateBasket(tmpProductBasket, new Map<String, Object>{  'cscfga__Basket_Status__c' => null, 
                                                                                                        'Contract_end_Fixed__c' => null,
                                                                                                        'Block_Expiration__c' => null});                                                                                                        
            ContractRelatedQuestionsController.createAgreement(tmpProductBasket);
            ContractRelatedQuestionsController.createAgreement(tmpProductBasket2);

            ContractRelatedQuestionsController.createAgreement2(tmpProductBasket, 'Direct 2');

            ContractRelatedQuestionsController.checkIfMobileExists(new List<cscfga__Product_Configuration__c>{tmpPc});
            ContractRelatedQuestionsController.createBPAAgreement(tmpProductBasket);
            
            ContractRelatedQuestionsController.getProductsFromBasket(tmpProductBasket.Id);

            //ContractRelatedQuestionsController.SaveContractPdf(testAgreement.Id);
            
            User currentUser = ContractRelatedQuestionsController.getCurrentUser();
            System.assertEquals(true, currentUser != null);

            String mobileScenarios = '[Flex Mobile][OneBusiness]';
            String bmsProducts = '[OBMS]';
                        
            tmpProductBasket.MobileScenarios__c = mobileScenarios;
            tmpProductBasket.BMS_Products__c = bmsProducts;
            
            cscfga__Product_Configuration__c VCConfig = CS_DataTest.createProductConfiguration(tmpPD.Id, 'Vodafone Calling', tmpProductBasket.Id);
            insert VCConfig;
            
            cscfga__Product_Configuration__c CTNConfig = CS_DataTest.createProductConfiguration(tmpPD.Id, 'Mobile CTN Profile', tmpProductBasket.Id);
            insert CTNConfig;

            List<Contract_Custom_Settings__mdt> getContractCustomSettingsRes = ContractRelatedQuestionsController.getContractCustomSettings(tmpProductBasket);
            System.assertEquals(true, getContractCustomSettingsRes.size() > 0);
            
            Test.stopTest();
        }
    }
}