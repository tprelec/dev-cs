global class ContractTermUpdatePlugin extends csutil.PluginManager.ABasePlugin {
    
    @TestVisible
    global override Object invoke (Object parameter) {
        List<cscfga__Product_Configuration__c> accessInfra = [SELECT Id, Name, cscfga__Contract_Term__c, ManagedInternet__c, OneFixed__c, IPVPN__c FROM cscfga__Product_Configuration__c WHERE Id = :(Id)parameter];
        if (accessInfra != null) {
            
            List<Id> configIds = new List<Id>();
            double contractTerm;
            
            for (cscfga__Product_Configuration__c pc : accessInfra) {
                if (pc.ManagedInternet__c != null) configIds.add(pc.ManagedInternet__c);
                if (pc.OneFixed__c != null) configIds.add(pc.OneFixed__c);
                if (pc.IPVPN__c != null) configIds.add(pc.IPVPN__c);
                if (contractTerm == null || contractTerm != pc.cscfga__Contract_Term__c)
                    contractTerm = pc.cscfga__Contract_Term__c;
            }        
            
            List<cscfga__Product_Configuration__c> pcs = [SELECT Id, cscfga__Contract_Term__c FROM cscfga__Product_Configuration__c WHERE Id IN :configIds];
            List<cscfga__Product_Configuration__c> pcsForUpdate = new List<cscfga__Product_Configuration__c>();
        
            for (cscfga__Product_Configuration__c pc : pcs) {
                if (pc.cscfga__Contract_Term__c != contractTerm) {
                    pc.cscfga__Contract_Term__c = contractTerm;
                    pcsForUpdate.add(pc);
                }
            }
            
            update pcsForUpdate;
        }
        return '';
    }
}