public with sharing class ConvertLeadAction {
	@InvocableMethod(
		label='Convert Lead'
		description='Converts Lead to Account, Contact and Opportunity. Uses KVK and Email matching'
		category='Lead'
	)
	public static List<Id> convertLeads(List<Lead> leads) {
		ConvertLeadsService cls = new ConvertLeadsService(leads);
		List<Database.LeadConvertResult> lcrs = cls.convertLeads(
			GeneralUtils.getRecordTypeIdByDeveloperName('Lead', 'CSADetails')
		);
		List<Id> leadIds = new List<Id>();
		for (Database.LeadConvertResult lcr : lcrs) {
			leadIds.add(lcr.getLeadId());
		}
		return leadIds;
	}
}