public without sharing class ConvertLeadsService {
	private List<Lead> leads;
	private Map<Id, List<Lead>> leadsByRecordType;
	private Map<Id, Lead> leadsMap;
	private List<Lead> currentLeads;
	private List<Database.LeadConvertResult> leadConvertResults;

	private Map<Id, Site__c> leadIdToSite;
	private Map<Id, OpportunityContactRole> leadIdToContactRole;

	private Map<String, Account> accsWithConsByKVK;

	public ConvertLeadsService(List<Lead> leads) {
		this.leads = leads;
		this.leadsByRecordType = GeneralUtils.groupByIDField(leads, 'RecordTypeId');
		this.leadsMap = new Map<Id, Lead>();
		for (Lead l : leads) {
			leadsMap.put(l.Id, l);
		}
		this.currentLeads = leads;
	}

	public List<Database.LeadConvertResult> convertLeads(Id recordTypeId) {
		currentLeads = leadsByRecordType.get(recordTypeId);
		convertLeads();
		if (recordTypeId == GeneralUtils.getRecordTypeIdByDeveloperName('Lead', 'CSADetails')) {
			handleRelatedZiggoRecords();
		}
		return leadConvertResults;
	}

	public List<Database.LeadConvertResult> convertLeads() {
		getAccountsWithContacts();
		List<Database.LeadConvert> leadConverts = new List<Database.LeadConvert>();
		for (Lead l : currentLeads) {
			Account acc = accsWithConsByKVK.get(l.KVK_number__c);
			Contact con = getMatchingContact(l, acc?.Contacts);
			leadConverts.add(buildLeadConvert(l, acc, con));
		}
		leadConvertResults = Database.convertLead(leadConverts);
		return leadConvertResults;
	}

	public static void createTask(Set<Id> opportunityIds) {

		List<Task> taskList = new List<Task>();
		for (Id oppId : opportunityIds) {
			Task task = new Task();
				task.Type = 'Result of D2D Visit';
        		task.Subject = 'D2D Visit';
        		task.ActivityDate = Date.today();
        		task.Status = 'Completed';
        		task.WhatId = oppId;
        		task.LG_Result__c =  'Qualified';
			taskList.add(task);
		}
		insert taskList;
	}

	private void getAccountsWithContacts() {
		Set<String> kvkNumbers = GeneralUtils.getStringSetFromList(currentLeads, 'KVK_number__c');
		// Get Accounts and Contacts
		List<Account> accounts = [
			SELECT Id, KVK_number__c, (SELECT Id, Email FROM Contacts)
			FROM Account
			WHERE KVK_number__c IN :kvkNumbers
		];
		accsWithConsByKVK = new Map<String, Account>();
		for (Account acc : accounts) {
			accsWithConsByKVK.put(acc.KVK_number__c, acc);
		}
	}

	private Contact getMatchingContact(Lead l, List<Contact> contacts) {
		if (contacts == null) {
			return null;
		}
		for (Contact c : contacts) {
			if (
				l.Email != null &&
				c.Email != null &&
				l.Email.toLowerCase() == c.Email.toLowerCase()
			) {
				return c;
			}
		}
		return null;
	}

	private Database.LeadConvert buildLeadConvert(Lead lead, Account acc, Contact con) {
		Database.LeadConvert leadConvert = new Database.LeadConvert();
		leadConvert.setOwnerId(UserInfo.getUserId());
		leadConvert.setLeadId(lead.Id);
		leadConvert.setConvertedStatus(leadConvertStatus.MasterLabel);

		// Attach to existing Account and Contact if it was found
		if (acc != null) {
			leadConvert.setAccountId(acc.Id);
		}
		if (con != null) {
			leadConvert.setContactId(con.Id);
		}

		return leadConvert;
	}

	private static LeadStatus leadConvertStatus {
		private get {
			if (leadConvertStatus == null) {
				return [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1];
			}
			return leadConvertStatus;
		}
	}

	private void handleRelatedZiggoRecords() {
		handleOpportunityContactRoles();
		handleSites();
		handleOpportunities();
	}

	private void handleOpportunityContactRoles() {
		leadIdToContactRole = new Map<Id, OpportunityContactRole>();
		// Get all Contact Roles related to Opportunitites
		Set<Id> oppIds = new Set<Id>();
		for (Database.LeadConvertResult lcr : leadConvertResults) {
			oppIds.add(lcr.getOpportunityId());
		}
		List<OpportunityContactRole> allContactRoles = [
			SELECT Id, OpportunityId, ContactId, Role
			FROM OpportunityContactRole
			WHERE OpportunityId IN :oppIds
		];
		// Create new Roles or update existing
		List<OpportunityContactRole> contactRolesForUpsert = new List<OpportunityContactRole>();
		for (Database.LeadConvertResult lcr : leadConvertResults) {
			OpportunityContactRole contactRole;
			for (OpportunityContactRole ocr : allContactRoles) {
				if (
					lcr.getOpportunityId() == ocr.OpportunityId &&
					lcr.getContactId() == ocr.ContactId
				) {
					contactRole = ocr;
					break;
				}
			}
			if (contactRole == null) {
				contactRole = new OpportunityContactRole(
					OpportunityId = lcr.getOpportunityId(),
					ContactId = lcr.getContactId()
				);
			}
			contactRole.Role = 'Administrative Contact';
			contactRole.IsPrimary = true;
			contactRolesForUpsert.add(contactRole);
			leadIdToContactRole.put(lcr.getLeadId(), contactRole);
		}
		upsert contactRolesForUpsert;
	}

	private void handleSites() {
		leadIdToSite = new Map<Id, Site__c>();
		// Get all Sites related to Accounts
		Set<Id> accountIds = new Set<Id>();
		for (Database.LeadConvertResult lcr : leadConvertResults) {
			accountIds.add(lcr.getAccountId());
		}
		List<Site__c> allSites = [
			SELECT Id, Site_Unique_ID__c
			FROM Site__c
			WHERE Site_Account__c IN :accountIds
		];
		Map<String, Site__c> allSitesMap = new Map<String, Site__c>();
		for (Site__c site : allSites) {
			allSitesMap.put(site.Site_Unique_ID__c, site);
		}

		// Create new Sites if they don't exist already
		List<Site__c> newSites = new List<Site__c>();
		for (Database.LeadConvertResult lcr : leadConvertResults) {
			Lead l = leadsMap.get(lcr.getLeadId());
			String siteKey = buildSiteKey(l);
			Site__c site = allSitesMap.get(siteKey);
			if (site == null) {
				site = new Site__c(
					Site_Account__c = lcr.getAccountId(),
					KVK_Number__c = l.KVK_number__c,
					Active__c = true,
					Site_Street__c = l.Street__c,
					Site_House_Number__c = l.Visiting_Housenumber1__c,
					Site_House_Number_Suffix__c = l.House_Number_Suffix__c,
					Site_Postal_Code__c = clearPostcode(l.Visiting_postalcode_merged_input__c),
					Site_City__c = l.Town__c,
					Country__c = l.Visiting_Country__c
				);
				newSites.add(site);
			}
			leadIdToSite.put(l.Id, site);
		}
		if (!newSites.isEmpty()) {
			insert newSites;
		}
	}

	private String buildSiteKey(Lead l) {
		// Build Site Key
		String siteKey = '';
		siteKey += nullSafeString(l.KVK_number__c);
		siteKey += clearPostcode(l.Visiting_postalcode_merged_input__c);
		siteKey += nullSafeString(String.valueOf(l.Visiting_Housenumber1__c)).substringBefore('.');
		siteKey += nullSafeString(l.House_Number_Suffix__c);
		return siteKey;
	}

	private String nullSafeString(String s) {
		if (s == null) {
			return '';
		} else {
			return s;
		}
	}

	private String clearPostcode(String postcode) {
		return nullSafeString(postcode).toUpperCase().replaceAll(' ', '');
	}

	private void handleOpportunities() {
		List<Opportunity> opps = new List<Opportunity>();
		List<Attachment> atts = new List<Attachment>();
		Set<Id> oppIds = new Set<Id>();
		Id ziggoOppRecordTypeId = GeneralUtils.getRecordTypeIdByName('Opportunity', 'Ziggo');

		for (Database.LeadConvertResult lcr : leadConvertResults) {
			Opportunity opp = new Opportunity(Id = lcr.getOpportunityId());
			oppIds.add (lcr.getOpportunityId());
			opp.RecordTypeId = ziggoOppRecordTypeId;
			opp.LG_PrimaryContact__c = lcr.getContactId();
			opp.StageName = 'Awareness of interest';
			opps.add(opp);

			Id leadId = lcr.getLeadId();
			OrderEntryData oed = new OrderEntryData();
			oed.accountId = lcr.getAccountId();
			oed.opportunityId = lcr.getOpportunityId();
			oed.primaryContactId = leadIdToContactRole.get(leadId)?.ContactId;
			oed.siteId = leadIdToSite.get(leadId)?.Id;
			oed.b2cInternetCustomer = leadsMap.get(leadId).LG_InternetCustomer__c;
			oed.init();

			Attachment att = new Attachment(
				Name = 'OrderEntryData.json',
				Body = Blob.valueOf(JSON.serializePretty(oed)),
				ParentId = opp.Id
			);
			atts.add(att);
		}
		createTask(oppIds);
		update opps;
		insert atts;
	}


}