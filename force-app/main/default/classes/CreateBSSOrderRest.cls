/**
 * @description: This class is responsible for invoking the CreateBSSOrder REST service
 * @author: Jurgen van Westreenen
 */
@SuppressWarnings('PMD.NcssMethodCount, PMD.CyclomaticComplexity')
public without sharing class CreateBSSOrderRest {
	public static final String BILLING_STATUS_REQUESTED = 'Requested';
	public static final String BILLING_STATUS_REQUEST_FAILED = 'Failed';
	public static final Integer MAX_NR_OF_FAILED_REQUESTS = 3;
	@TestVisible
	private static final String INTEGRATION_SETTING_NAME = 'SIASRESTCreateBSSOrder';
	@TestVisible
	private static final String STATUS_OK = 'OK';
	@TestVisible
	private static final String MOCK_URL = 'http://example.com/CreateBSSOrderRest';

	private static Map<Id, String> extSiteMap;

	public static CollectionWrapper createBSSOrder(String transactionId, List<Id> contProdIds) {
		CollectionWrapper collectionWrapper = new CollectionWrapper();

		List<Contracted_Products__c> prodList = [
			SELECT
				Id,
				Site__r.Assigned_Product_Id__c,
				Customer_Asset__c,
				Customer_Asset__r.Installed_Base_Id__c,
				Customer_Asset__r.Quantity__c,
				Customer_Asset__r.Price__c,
				Customer_Asset__r.PO_Number__c,
				Totalprice__c,
				Gross_List_Price__c,
				Billing_Arrangement__r.Unify_Ref_Id__c,
				Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c,
				Delivery_Date__c,
				Activation_Date__c,
				Start_Invoicing_Date__c,
				Is_Recurring__c,
				Unify_Charge_Description__c,
				Net_Unit_Price__c,
				Quantity__c,
				Discount__c,
				Discount_description__c,
				Product__r.ProductCode,
				Product__r.Unify_Charge_Code__c,
				Product__r.Taxonomy__r.Name,
				Product__r.Taxonomy__r.Product_Family__c,
				Product__r.Taxonomy__r.Portfolio__c,
				External_Reference_Id__c,
				Error_Info__c,
				Billing_Status__c,
				Number_of_Failed_Requests__c,
				Order__c,
				Order__r.Account__c,
				PO_Number__c,
				Cost_Center__c,
				OC_Charge__c,
				OC_Charge__r.Installed_Base_Id__c,
				OC_Charge__r.Quantity__c,
				OC_Charge__r.Price__c,
				OC_Charge__r.PO_Number__c,
				Credit__c,
				Row_Type__c,
				Site__c,
				Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.ExternalAccount__c
			FROM Contracted_Products__c
			WHERE Id IN :contProdIds
		];
		if (prodList.size() > 0) {
			HTTPResponse response = makeCallout(transactionId, prodList);
			Order_Billing_Transaction__c billingTransaction = createBillingTransaction(
				transactionId,
				prodList
			);
			if (response.getStatusCode() == 200) {
				List<Contracted_Products__c> updContProds = new List<Contracted_Products__c>();
				for (Contracted_Products__c contProd : prodList) {
					contProd.Billing_Status__c = BILLING_STATUS_REQUESTED;
					contProd.Number_of_Failed_Requests__c = 0; // Reset counter
					contProd.Activation_Date__c = getActivationDate(contProd);
					updContProds.add(contProd);
				}
				collectionWrapper.contractedProductsToUpdate = updContProds;
			} else {
				List<Contracted_Products__c> updContProds = new List<Contracted_Products__c>();
				for (Contracted_Products__c contProd : prodList) {
					contProd.Number_of_Failed_Requests__c++; // Increase counter
					// Set status to 'Request Failed' after 3 failed attempts
					if (contProd.Number_of_Failed_Requests__c >= MAX_NR_OF_FAILED_REQUESTS) {
						contProd.Billing_Status__c = BILLING_STATUS_REQUEST_FAILED;
					}
					updContProds.add(contProd);
				}
				collectionWrapper.contractedProductsToUpdate = updContProds;
				billingTransaction.Error_Info__c = response.getBody();
			}
			collectionWrapper.billingTransactionToInsert = billingTransaction;
		}
		return collectionWrapper;
	}

	private static HTTPResponse makeCallout(
		String transactionId,
		List<Contracted_Products__c> prodList
	) {
		extSiteMap = createExtSiteMap(prodList);

		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeFieldName('createBSSOrderRequest');
		generator.writeStartObject();
		generator.writeStringField('transactionID', transactionId);
		generator.writeFieldName('orderLinesList');
		generator.writeStartArray();
		for (Contracted_Products__c contProd : prodList) {
			generator.writeStartObject();
			generator.writeStringField('SLSAPID', nullCheck(extSiteMap.get(contProd.Id)));
			generator.writeStringField(
				'SFIBID',
				nullCheck(contProd.Customer_Asset__r.Installed_Base_Id__c)
			);
			generator.writeStringField(
				'BAID',
				nullCheck(contProd.Billing_Arrangement__r.Unify_Ref_Id__c)
			);
			generator.writeStringField(
				'BCID',
				nullCheck(
					contProd.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c
				)
			);
			generator.writeStringField('componentAPID', ''); // Not required
			generator.writeStringField('activityType', string.valueOf(getActivityType(contProd)));
			generator.writeStringField(
				'activationDate',
				string.valueOf(getActivationDate(contProd))
			);
			generator.writeStringField(
				'invoicingStartDate',
				nullCheck(contProd.Start_Invoicing_Date__c)
			);
			generator.writeStringField('typeOfCharge', contProd.Is_Recurring__c ? 'RC' : 'OC');
			generator.writeStringField(
				'chargeDescription',
				replaceSpecialCharacters(nullCheck(contProd.Unify_Charge_Description__c))
			);
			generator.writeStringField('chargeAmount', nullCheck(contProd.Gross_List_Price__c));
			generator.writeNumberField(
				'quantity',
				contProd.Quantity__c != null ? Integer.valueOf(contProd.Quantity__c) : 0
			);
			generator.writeStringField('discountAmount', nullCheck(contProd.Discount__c));
			generator.writeStringField(
				'discountDescription',
				replaceSpecialCharacters(nullCheck(contProd.Discount_description__c))
			);
			generator.writeStringField('discountMethod', 'Percentage'); // For now hardcoded
			generator.writeStringField(
				'chargeCode',
				nullCheck(contProd.Product__r.Unify_Charge_Code__c)
			);
			if (String.isNotBlank(contProd.Product__c)) {
				generator.writeStringField('BItag1', nullCheck(contProd.Product__r.ProductCode));
				generator.writeStringField(
					'BItag2',
					nullCheck(contProd.Product__r.Taxonomy__r.Name)
				);
				generator.writeStringField(
					'BItag3',
					nullCheck(contProd.Product__r.Taxonomy__r.Product_Family__c)
				);
				generator.writeStringField(
					'BItag4',
					nullCheck(contProd.Product__r.Taxonomy__r.Portfolio__c)
				);
			}
			generator.writeStringField('externalReferenceID', contProd.Id);
			generator.writeStringField('ceaseAutomatically', 'FALSE');
			generator.writeStringField('PONumber', nullCheck(contProd.PO_Number__c));
			generator.writeStringField('allocationCode', nullCheck(contProd.Cost_Center__c));
			generator.writeEndObject();
			if (contProd.OC_Charge__c != null) {
				addOCCharge(generator, contProd);
			}
		}
		generator.writeEndArray();
		generator.writeEndObject();
		generator.writeEndObject();

		String requestBody = generator.getAsString();

		HttpRequest reqData = buildRequest(requestBody);

		HTTPResponse response = new HttpResponse();
		Http http = new Http();
		response = http.send(reqData);

		return response;
	}

	private static void addOCCharge(JSONGenerator generator, Contracted_Products__c contProd) {
		Date activationDate = getActivationDate(contProd);
		Decimal priceDiff = 0;
		Decimal calcAmount = 0;
		// Below Boolean was defined to avoid PMD Cyclomatic Complexity error
		Boolean priceComplete =
			contProd.Net_Unit_Price__c != null &&
			contProd.Quantity__c != null &&
			contProd.OC_Charge__r.Price__c != null &&
			contProd.OC_Charge__r.Quantity__c != null;
		if (priceComplete) {
			priceDiff =
				(contProd.OC_Charge__r.Price__c * contProd.OC_Charge__r.Quantity__c) +
				(contProd.Net_Unit_Price__c * contProd.Quantity__c);
		}
		if (contProd.Start_Invoicing_Date__c != null) {
			calcAmount =
				(priceDiff / 30.413) * contProd.Start_Invoicing_Date__c.daysBetween(activationDate);
		}
		String description =
			contProd.OC_Charge__r.PO_Number__c +
			' - ' +
			contProd.Unify_Charge_Description__c +
			' FROM ' +
			String.valueOf(contProd.Start_Invoicing_Date__c) +
			' TO ' +
			String.valueOf(activationDate);

		generator.writeStartObject();
		generator.writeStringField('SLSAPID', nullCheck(extSiteMap.get(contProd.Id)));
		generator.writeStringField('SFIBID', nullCheck(contProd.OC_Charge__r.Installed_Base_Id__c));
		generator.writeStringField(
			'BAID',
			nullCheck(contProd.Billing_Arrangement__r.Unify_Ref_Id__c)
		);
		generator.writeStringField(
			'BCID',
			nullCheck(contProd.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c)
		);
		generator.writeStringField('componentAPID', ''); // Not required
		generator.writeStringField('activityType', 'Provide'); // Hardcoded for OC Charge
		generator.writeStringField('activationDate', string.valueOf(activationDate));
		generator.writeStringField(
			'invoicingStartDate',
			nullCheck(contProd.Start_Invoicing_Date__c)
		);
		generator.writeStringField('typeOfCharge', 'OC'); // Hardcoded for OC Charge
		generator.writeStringField('chargeDescription', replaceSpecialCharacters(description));
		generator.writeStringField('chargeAmount', string.valueOf(calcAmount));
		generator.writeNumberField('quantity', 1); // Hardcoded for OC Charge
		generator.writeStringField('discountAmount', nullCheck(contProd.Discount__c));
		generator.writeStringField(
			'discountDescription',
			replaceSpecialCharacters(nullCheck(contProd.Discount_description__c))
		);
		generator.writeStringField('discountMethod', 'Percentage');
		generator.writeStringField(
			'chargeCode',
			nullCheck(contProd.Product__r.Unify_Charge_Code__c)
		);
		if (String.isNotBlank(contProd.Product__c)) {
			generator.writeStringField('BItag1', nullCheck(contProd.Product__r.ProductCode));
			generator.writeStringField('BItag2', nullCheck(contProd.Product__r.Taxonomy__r.Name));
			generator.writeStringField(
				'BItag3',
				nullCheck(contProd.Product__r.Taxonomy__r.Product_Family__c)
			);
			generator.writeStringField(
				'BItag4',
				nullCheck(contProd.Product__r.Taxonomy__r.Portfolio__c)
			);
		}
		generator.writeStringField('externalReferenceID', contProd.Id);
		generator.writeStringField('ceaseAutomatically', 'TRUE');
		generator.writeStringField('PONumber', nullCheck(contProd.PO_Number__c));
		generator.writeStringField('allocationCode', nullCheck(contProd.Cost_Center__c));
		generator.writeEndObject();
	}

	// Using Named Credentials will be part of a larger scale refactoring; suppress for now
	@SuppressWarnings('PMD.ApexSuggestUsingNamedCred')
	private static HttpRequest buildRequest(String reqBody) {
		External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(
			INTEGRATION_SETTING_NAME
		);

		string endpointURL = webServiceConfig.URL__c;
		String authorizationHeaderString =
			webServiceConfig.Username__c +
			':' +
			webServiceConfig.Password__c;
		String authorizationHeader =
			'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(authorizationHeaderString));

		HttpRequest reqData = new HttpRequest();
		reqData.setHeader('Content-Type', 'application/json');
		reqData.setHeader('Connection', 'keep-alive');
		reqData.setHeader('Content-Length', '0');
		reqData.setHeader('Authorization', authorizationHeader);
		reqData.setTimeout(120000);
		reqData.setEndpoint(endpointURL);
		if (String.isNotEmpty(webServiceConfig.Certificate_Name__c)) {
			reqData.setClientCertificateName(webServiceConfig.Certificate_Name__c);
		}
		reqData.setBody(reqBody);
		reqData.setMethod('POST');

		return reqData;
	}

	private static Map<Id, String> createExtSiteMap(List<Contracted_Products__c> cpList) {
		Map<Id, String> esMap = new Map<Id, String>();
		Set<Id> extAcctIds = new Set<Id>();
		for (Contracted_Products__c cp : cpList) {
			if (
				cp.Order__r
					?.Billing_Arrangement__r
					?.Financial_Account__r
					?.Ban__r
					?.ExternalAccount__c != null
			) {
				extAcctIds.add(
					cp.Order__r
						?.Billing_Arrangement__r
						?.Financial_Account__r
						?.Ban__r
						?.ExternalAccount__c
				);
			}
		}
		if (extAcctIds.size() > 0) {
			List<External_Site__c> extSiteList = [
				SELECT Id, SLSAPID__c, Site__c, External_Account__c
				FROM External_Site__c
				WHERE External_Account__c IN :extAcctIds
			];
			for (Contracted_Products__c contProd : cpList) {
				for (External_Site__c extSite : extSiteList) {
					if (
						contProd.Site__c == extSite.Site__c &&
						contProd.Order__r
							?.Billing_Arrangement__r
							?.Financial_Account__r
							?.Ban__r
							?.ExternalAccount__c == extSite.External_Account__c
					) {
						esMap.put(contProd.Id, extSite.SLSAPID__c);
					}
				}
			}
		}
		return esMap;
	}

	private static Order_Billing_Transaction__c createBillingTransaction(
		String transactionId,
		List<Contracted_Products__c> contractedProducts
	) {
		List<Id> contractedProductIds = new List<Id>();
		List<Id> assetIds = new List<Id>();
		Id orderId;
		Id accountId;
		for (Contracted_Products__c contractedProduct : contractedProducts) {
			contractedProductIds.add(contractedProduct.Id);
			if (String.isNotEmpty(contractedProduct.Customer_Asset__c)) {
				assetIds.add(contractedProduct.Customer_Asset__c);
			}
			if (contractedProduct.Order__c != null && orderId == null) {
				orderId = contractedProduct.Order__c;
				accountId = contractedProduct.Order__r.Account__c;
			}
		}
		Order_Billing_Transaction__c billingTransaction = new Order_Billing_Transaction__c(
			Contracted_Products__c = JSON.serialize(contractedProductIds),
			Customer_Assets__c = JSON.serialize(assetIds),
			Order__c = orderId,
			Account__c = accountId,
			Transaction_Id__c = transactionId
		);
		return billingTransaction;
	}

	private static Order_Billing_Transaction__c createBillingTransaction(
		String transactionId,
		List<Customer_Asset__c> customerAssets
	) {
		List<Id> assetIds = new List<Id>();
		Id orderId;
		Id accountId;
		for (Customer_Asset__c customerAsset : customerAssets) {
			assetIds.add(customerAsset.Id);
			if (customerAsset.Order__c != null && orderId == null) {
				orderId = customerAsset.Order__c;
			}
            if (customerAsset.Account__c != null && accountId == null) {
				accountId = customerAsset.Account__c;
			}
		}
		Order_Billing_Transaction__c billingTransaction = new Order_Billing_Transaction__c(
			Customer_Assets__c = JSON.serialize(assetIds),
			Order__c = orderId,
			Account__c = accountId,
			Transaction_Id__c = transactionId
		);
		return billingTransaction;
	}

	private static String nullCheck(Object inp) {
		if (inp != null) {
			return String.valueOf(inp);
		}
		return '';
	}

	private static String getActivityType(Contracted_Products__c cp) {
		String actType = 'Provide'; // Default value
		String rowType = cp.Row_Type__c;
		Boolean addMore =
			rowType == Constants.CONTRACTED_PRODUCT_ROW_TYPE_ADD &&
			cp.Quantity__c < cp.Customer_Asset__r.Quantity__c;
		if (addMore) {
			actType = 'Change';
		}
		if (rowType == Constants.CONTRACTED_PRODUCT_ROW_TYPE_REMOVE) {
			if (cp.Customer_Asset__r.Quantity__c == 0) {
				actType = 'Cease';
			} else {
				actType = 'Change';
			}
		}
		if (rowType == Constants.CONTRACTED_PRODUCT_ROW_TYPE_PRICE_CHANGE) {
			actType = 'Change';
		}
		return actType;
	}

	private static String replaceSpecialCharacters(String input) {
		String output = input;
		Map<String, String> replaceMap = new Map<String, String>{ '+' => ' plus ', ';' => ',' };
		if (input.containsAny(String.join(new List<String>(replaceMap.keySet()), ''))) {
			for (String toReplace : replaceMap.keySet()) {
				output = output.replace(toReplace, replaceMap.get(toReplace));
			}
		}
		return output;
	}

	@TestVisible
	private static Date getActivationDate(Contracted_Products__c cp) {
		Boolean futureInvoice =
			getActivityType(cp) != 'Provide' &&
			cp.Start_Invoicing_Date__c > System.today();
		if (futureInvoice) {
			return cp.Start_Invoicing_Date__c;
		} else if (cp.Credit__c) {
			return System.today().addMonths(1).toStartOfMonth().addDays(-1);
		} else {
			return System.today().day() == 1
				? System.today()
				: System.today().addMonths(1).toStartOfMonth();
		}
	}

	public static CollectionWrapper createBSSOrder(String transactionId, List<Customer_Asset__c> customerAssets) {
		CollectionWrapper collectionWrapper = new CollectionWrapper();

		if (customerAssets.size() > 0) {
			HTTPResponse response = makeCallout(transactionId, customerAssets);
			Order_Billing_Transaction__c billingTransaction = createBillingTransaction(
				transactionId,
				customerAssets
			);
			if (response.getStatusCode() != 200) {
				billingTransaction.Error_Info__c = response.getBody();
			}
			collectionWrapper.billingTransactionToInsert = billingTransaction;
		}
		return collectionWrapper;
	}

	private static HTTPResponse makeCallout(
		String transactionId,
		List<Customer_Asset__c> customerAssets
	) {

		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeFieldName('createBSSOrderRequest');
		generator.writeStartObject();
		generator.writeStringField('transactionID', transactionId);
		generator.writeFieldName('orderLinesList');
		generator.writeStartArray();
		for (Customer_Asset__c customerAsset : customerAssets) {
			generator.writeStartObject();
			//generator.writeStringField('SLSAPID', nullCheck(extSiteMap.get(contProd.Id)));
			generator.writeStringField(
				'SFIBID',
				nullCheck(customerAsset.Installed_Base_Id__c)
			);
			generator.writeStringField('componentAPID', customerAsset.Assigned_Product_Id__c); // Not required
			generator.writeStringField('activityType', 'Change');
			generator.writeStringField(
				'activationDate',
				string.valueOf(customerAsset.Latest_Activation_Date__c)
			);
			generator.writeStringField('externalReferenceID', customerAsset.External_Reference_Id__c);
			generator.writeEndObject();
		}
		generator.writeEndArray();
		generator.writeEndObject();
		generator.writeEndObject();

		String requestBody = generator.getAsString();

		HttpRequest reqData = buildRequest(requestBody);

		HTTPResponse response = new HttpResponse();
		Http http = new Http();
		response = http.send(reqData);

		return response;
	}

	public class CollectionWrapper {
		public Order_Billing_Transaction__c billingTransactionToInsert = new Order_Billing_Transaction__c();
		public List<Contracted_Products__c> contractedProductsToUpdate = new List<Contracted_Products__c>();
	}
}