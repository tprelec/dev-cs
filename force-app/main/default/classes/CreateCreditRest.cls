/**
 * @description: This class is responsible for invoking the CreateCredit REST service
 * @author: Jurgen van Westreenen
 */
public class CreateCreditRest implements Queueable, Database.AllowsCallouts {
	@TestVisible
	private static final String INTEGRATION_SETTING_NAME = 'SIASRESTCreateCredit';
	@TestVisible
	private static final String MOCK_URL = 'http://example.com/CreateCreditRest';

	private List<Id> credNoteIds = new List<Id>();

	private Map<Id, List<Credit_Line__c>> clMap = new Map<Id, List<Credit_Line__c>>();

	private List<Credit_Line__c> updCreditLines = new List<Credit_Line__c>();

	public CreateCreditRest(Set<Credit_Note__c> credNoteSet) {
		for (Credit_Note__c cn : credNoteSet) {
			this.credNoteIds.add(cn.Id);
		}
	}

	public void execute(QueueableContext context) {
		try {
			// Retrieve the necessary Credit Line data based on passed Credit Note Id
			List<Credit_Line__c> clList = [
				SELECT
					Id,
					Name,
					Error_Info__c,
					Credit_Note__c,
					Credit_Note__r.Credit_Type__c,
					Customer_Asset__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c,
					Customer_Asset__r.Billing_Arrangement__r.Unify_Ref_Id__c,
					Calculated_Amount__c,
					Description__c
				FROM Credit_Line__c
				WHERE Credit_Note__c IN :credNoteIds
			];
			if (clList.size() > 0) {
				// Group all Credit Lines by Credit Note
				for (Credit_Line__c cl : clList) {
					if (clMap.containsKey(cl.Credit_Note__c)) {
						clMap.get(cl.Credit_Note__c).add(cl);
					} else {
						clMap.put(cl.Credit_Note__c, new List<Credit_Line__c>{ cl });
					}
				}
				HttpRequest reqData = buildReqHeader();
				for (Id cnId : clMap.keySet()) {
					sendRequest(reqData, cnId);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			update updCreditLines;
		}
	}

	private void sendRequest(HttpRequest reqData, Id cnId) {
		String reqBody = buildReqBody(clMap.get(cnId));
		reqData.setBody(reqBody);
		reqData.setMethod('POST');

		Http http = new Http();
		HTTPResponse response = http.send(reqData);

		if (response.getStatusCode() == 200) {
			for (Credit_Line__c credLine : clMap.get(cnId)) {
				credLine.Status__c = 'Billing Requested';
				updCreditLines.add(credLine);
			}
		} else {
			String errorResponse = response.getBody();
			for (Credit_Line__c credLine : clMap.get(cnId)) {
				credLine.Status__c = 'Billing Error';
				credLine.Error_Info__c = String.isNotBlank(credLine.Error_Info__c)
					? credLine.Error_Info__c + '\n' + errorResponse
					: errorResponse;
				updCreditLines.add(credLine);
			}
		}
	}

	// Using Named Credentials will be part of a larger scale refactoring; suppress for now
	@SuppressWarnings('PMD.ApexSuggestUsingNamedCred')
	private HttpRequest buildReqHeader() {
		// Retrieve the credentials from the custom setting
		External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(
			INTEGRATION_SETTING_NAME
		);
		string endpointURL = webServiceConfig.URL__c;
		String authorizationHeaderString =
			webServiceConfig.Username__c +
			':' +
			webServiceConfig.Password__c;
		String authorizationHeader =
			'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(authorizationHeaderString));

		HttpRequest reqData = new HttpRequest();

		reqData.setHeader('Content-Type', 'application/json');
		reqData.setHeader('Connection', 'keep-alive');
		reqData.setHeader('Content-Length', '0');
		reqData.setHeader('Authorization', authorizationHeader);
		reqData.setTimeout(120000);
		reqData.setEndpoint(endpointURL);

		if (String.isNotEmpty(webServiceConfig.Certificate_Name__c)) {
			reqData.setClientCertificateName(webServiceConfig.Certificate_Name__c);
		}

		return reqData;
	}

	private String buildReqBody(List<Credit_Line__c> clList) {
		// Generate the request JSON message
		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeFieldName('createCredit');
		generator.writeStartObject();
		generator.writeStringField('transactionID', 'CT-' + Datetime.now().getTime());
		generator.writeFieldName('creditLinesList');
		generator.writeStartArray();
		for (Credit_Line__c creditLine : clList) {
			generator.writeStartObject();
			generator.writeStringField('sfdcCreditId', creditLine.Name);
			generator.writeStringField(
				'financialAccountID',
				creditLine.Customer_Asset__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c !=
					null
					? string.valueOf(
							creditLine.Customer_Asset__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c
					  )
					: ''
			);
			generator.writeStringField(
				'BAID',
				creditLine.Customer_Asset__r.Billing_Arrangement__r.Unify_Ref_Id__c != null
					? string.valueOf(
							creditLine.Customer_Asset__r.Billing_Arrangement__r.Unify_Ref_Id__c
					  )
					: ''
			);
			generator.writeStringField(
				'creditReason',
				creditLine.Credit_Note__r.Credit_Type__c != null
					? string.valueOf(creditLine.Credit_Note__r.Credit_Type__c)
					: ''
			);
			generator.writeStringField(
				'amount',
				creditLine.Calculated_Amount__c != null
					? string.valueOf(creditLine.Calculated_Amount__c)
					: ''
			);
			generator.writeStringField('taxIncludedInd', '0');
			generator.writeStringField('creditType', 'Pending');
			generator.writeStringField('description', creditLine.Description__c);
			generator.writeEndObject();
		}
		generator.writeEndArray();
		generator.writeEndObject();
		generator.writeEndObject();

		return generator.getAsString();
	}
}