public without sharing class CreateMobileInformationController {
	@AuraEnabled(cacheable=true)
	public static Boolean basketContractIsCreated(String oppId) {
		Boolean result = false;
		List<String> validStatusses = new List<String>{ 'Contract created', 'Approved' };
		List<cscfga__Product_Basket__c> productBaskets = [
			SELECT Id
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c = :oppId AND cscfga__Basket_Status__c IN :validStatusses
		];
		List<Opportunity> opportunities = [
			SELECT Id
			FROM Opportunity
			WHERE Id = :oppId AND Select_Journey__c = 'Add Mobile Product'
		];

		if (productBaskets.size() >= 1 || opportunities.size() >= 1) {
			result = true;
		}
		return result;
	}

	@AuraEnabled(cacheable=true)
	public static Opportunity getOpportunity(String oppId) {
		Opportunity theOpp = [
			SELECT
				Account.Name,
				Owner.Name,
				Main_Contact_Person__r.Name,
				DS_Authorized_to_sign_1st__r.Name,
				Account.BAN_Number__c,
				Ban__r.Ban_Number__c
			FROM Opportunity
			WHERE Id = :oppId
		];

		return theOpp;
	}

	@AuraEnabled(cacheable=true)
	public static List<String> getQuoteProfiles(String oppId) {
		List<String> toReturn = new List<String>();
		try {
			NetProfit_Information__c npInfo = [
				SELECT Id, Quote_Profiles_Details__c
				FROM NetProfit_Information__c
				WHERE Opportunity__c = :oppId
			];
			if (npInfo != null && npInfo.Quote_Profiles_Details__c != null) {
				toReturn = npInfo.Quote_Profiles_Details__c.split(';');
			}
		} catch (Exception e) {
			System.debug('Error splitting String');
		}
		return toReturn;
	}

	@AuraEnabled(cacheable=true)
	public static List<NetProfit_CTN__c> getNetProfitCtn(
		String oppId,
		String sortField,
		String sortDirection
	) {
		NetProfit_Settings__c npSettings = NetProfit_Settings__c.getInstance();
		Set<String> filteredProductGroups = StringUtils.deDelimitStringsToSet(
			npSettings.Filtered_Product_Groups__c,
			';'
		);
		Set<String> filteredProductQuantityTypes = StringUtils.deDelimitStringsToSet(
			npSettings.Filtered_Product_Quantity_types__c,
			';'
		);
		List<NetProfit_CTN__c> toReturn = new List<NetProfit_CTN__c>();
		Boolean contractCreated = basketContractIsCreated(oppId);

		if (contractCreated) {
			String query = 'SELECT Id, NetProfit_Information__c, Quote_Profile__c, Price_Plan_Description__c, Discount__c, MAF__c, Product_Quantity_type__c, Generated_Counter__c, ';
			query += 'Duration__c, Action__c, CTN_Number__c, Simcard_number_VF__c, Sharing__c, Data_Limit__c, dataBlock__c, Contract_Number_Current_Provider__c, Gross_List_Price__c, ';
			query += 'Current_Provider_name__c, Service_Provider_Name__c, Billing_Arrangement__c, Contract_Enddate__c, Display_Discount__c, OpportunityLineItem__c ';
			query +=
				'FROM NetProfit_CTN__c WHERE NetProfit_Information__r.Opportunity__c = \'' +
				oppId +
				'\'';
			query += 'AND Product_Group__c IN :filteredProductGroups AND (Product_Quantity_type__c IN :filteredProductQuantityTypes OR Product_Quantity_type__c = null)';
			query += 'ORDER BY ' + sortField + ' ' + sortDirection;

			try {
				if (Test.isRunningTest()) {
					toReturn.addAll(
						[
							SELECT
								Id,
								NetProfit_Information__c,
								Quote_Profile__c,
								Price_Plan_Description__c,
								Discount__c,
								MAF__c,
								Product_Quantity_type__c,
								Duration__c,
								Action__c,
								CTN_Number__c,
								Simcard_number_VF__c,
								Sharing__c,
								Data_Limit__c,
								dataBlock__c,
								Contract_Number_Current_Provider__c,
								Generated_Counter__c,
								Current_Provider_name__c,
								Service_Provider_Name__c,
								Billing_Arrangement__c,
								Contract_Enddate__c,
								Display_Discount__c,
								OpportunityLineItem__c,
								Gross_List_Price__c
							FROM NetProfit_CTN__c
						]
					);
				}
				toReturn.addAll((List<NetProfit_CTN__c>) Database.query(query));
				if (toReturn.isEmpty()) {
					NetProfitCTNGenerator generator = new NetProfitCTNGenerator(oppId);
					toReturn = generator.generateCTNInfo();
				}
			} catch (Exception e) {
				throw new CTNComponentException(e.getMessage());
			}
		}

		//For some reason SF won't display percentages properly in LWC so had to apply this bandaind
		for (NetProfit_CTN__c npCtn : toReturn) {
			if (npCtn.Discount__c != null) {
				npCtn.Discount__c = npCtn.Discount__c / 100;
			}
		}
		return toReturn;
	}

	@AuraEnabled
	public static Boolean insertNetProfitCtn(List<NetProfit_CTN__c> ctns, String oppId) {
		Boolean success = false;
		Boolean insertRecords = false;

		//change discount back from readable to actual value
		for (NetProfit_CTN__c ctn : ctns) {
			if (ctn.Discount__c != null) {
				ctn.Discount__c = ctn.Discount__c * 100;
			}
		}

		try {
			NetProfit_Information__c npInfo = getNetProfitInformation(oppId);
			if (npInfo.Id == null) {
				insert npInfo;
				insertRecords = true;
				for (NetProfit_CTN__c npCtn : ctns) {
					npCtn.NetProfit_Information__c = npInfo.Id;
				}
				if (insertRecords) {
					insert ctns;
				}
				success = true;
			}
		} catch (Exception e) {
			System.debug(LoggingLevel.INFO, e.getMessage());
			throw new CTNComponentException(e.getMessage());
		}

		return success;
	}

	@AuraEnabled
	public static Boolean save(
		List<NetProfit_CTN__c> original,
		List<NetProfit_CTN__c> valuesToSaveList,
		String oppId
	) {
		Boolean success = false;
		NetProfit_Information__c npInfo = getNetProfitInformation(oppId);
		Map<Id, NetProfit_CTN__c> toSaveByOppLineItem = new Map<Id, NetProfit_CTN__c>();
		Map<Id, NetProfit_CTN__c> toUpsert = new Map<Id, NetProfit_CTN__c>();
		Map<String, List<NetProfit_CTN__c>> groupedCTNs = new Map<String, List<NetProfit_CTN__c>>();

		for (NetProfit_CTN__c npCTN : [
			SELECT
				Id,
				NetProfit_Information__c,
				Quote_Profile__c,
				Price_Plan_Description__c,
				Discount__c,
				MAF__c,
				Gross_List_Price__c,
				Product_Quantity_type__c,
				Duration__c,
				Action__c,
				CTN_Number__c,
				Simcard_number_VF__c,
				Sharing__c,
				Data_Limit__c,
				dataBlock__c,
				Contract_Number_Current_Provider__c,
				Current_Provider_name__c,
				Service_Provider_Name__c,
				Billing_Arrangement__c,
				Contract_Enddate__c,
				Display_Discount__c,
				OpportunityLineItem__c,
				Generated_Counter__c
			FROM NetProfit_CTN__c
			WHERE NetProfit_Information__r.Opportunity__c = :oppId
		]) {
			String key =
				npCTN.Quote_Profile__c +
				' ' +
				npCTN.Generated_Counter__c +
				' ' +
				npCTN.Action__c;
			if (groupedCTNs.containsKey(key)) {
				groupedCTNs.get(key).add(npCTN);
			} else {
				List<NetProfit_CTN__c> theList = new List<NetProfit_CTN__c>();
				theList.add(npCTN);
				groupedCTNs.put(key, theList);
			}
		}

		if (valuesToSaveList != null) {
			for (NetProfit_CTN__c npCtn : valuesToSaveList) {
				toSaveByOppLineItem.put(npCtn.Id, npCtn);
			}
		}

		for (NetProfit_CTN__c theSource : original) {
			NetProfit_CTN__c valuesToSave = toSaveByOppLineItem.get(theSource.Id);
			if (valuesToSave != null) {
				String key =
					theSource.Quote_Profile__c +
					' ' +
					theSource.Generated_Counter__c +
					' ' +
					theSource.Action__c;
				for (NetProfit_CTN__c toUpdate : groupedCTNs.get(key)) {
					if (toUpdate.NetProfit_Information__c == null) {
						toUpdate.NetProfit_Information__c = npInfo.Id;
					}
					if (valuesToSave.CTN_Number__c != null) {
						toUpdate.CTN_Number__c = valuesToSave.CTN_Number__c;
					}
					if (valuesToSave.Simcard_number_VF__c != null) {
						toUpdate.Simcard_number_VF__c = valuesToSave.Simcard_number_VF__c;
					}
					if (valuesToSave.Sharing__c != null) {
						toUpdate.Sharing__c = valuesToSave.Sharing__c;
					}
					if (valuesToSave.Data_Limit__c != null) {
						toUpdate.Data_Limit__c = valuesToSave.Data_Limit__c;
					}
					if (valuesToSave.dataBlock__c != null) {
						toUpdate.dataBlock__c = valuesToSave.dataBlock__c;
					}
					if (valuesToSave.Contract_Number_Current_Provider__c != null) {
						toUpdate.Contract_Number_Current_Provider__c = valuesToSave.Contract_Number_Current_Provider__c;
					}
					if (valuesToSave.Current_Provider_name__c != null) {
						toUpdate.Current_Provider_name__c = valuesToSave.Current_Provider_name__c;
					}
					if (valuesToSave.Service_Provider_Name__c != null) {
						toUpdate.Service_Provider_Name__c = valuesToSave.Service_Provider_Name__c;
					}
					if (valuesToSave.Billing_Arrangement__c != null) {
						toUpdate.Billing_Arrangement__c = valuesToSave.Billing_Arrangement__c;
					}
					if (valuesToSave.Contract_Enddate__c != null) {
						toUpdate.Contract_Enddate__c = valuesToSave.Contract_Enddate__c;
					}
					toUpsert.put(toUpdate.Id, toUpdate);
				}
			}
		}

		try {
			upsert toUpsert.values();
			success = true;
		} catch (Exception e) {
			throw new CTNComponentException(e.getMessage());
		}

		return success;
	}

	private static NetProfit_Information__c getNetProfitInformation(String oppId) {
		NetProfit_Information__c toreturn;
		List<NetProfit_Information__c> npInfoList = [
			SELECT Id
			FROM NetProfit_Information__c
			WHERE Opportunity__c = :oppId
		];
		if (!npInfoList.isEmpty()) {
			toReturn = npInfoList[0];
		} else {
			toReturn = new NetProfit_Information__c(Opportunity__c = oppId);
		}
		return toReturn;
	}

	@AuraEnabled
	public static Boolean importCSV(
		String base64,
		String filename,
		String recordId,
		String separator
	) {
		Map<Id, NetProfit_CTN__c> toUpsert = new Map<Id, NetProfit_CTN__c>();
		Map<String, List<NetProfit_CTN__c>> groupedCTNs = new Map<String, List<NetProfit_CTN__c>>();
		NetProfit_Information__c npInfo = getNetProfitInformation(recordId);
		Boolean success = false;
		Blob myCSVBlob = EncodingUtil.base64Decode(base64);
		List<List<String>> parsedCSV = new List<List<String>>();
		//used stringutils parseCSV method instead of trying to parse manually
		parsedCSV = StringUtils.parseCSV(myCSVBlob.toString(), false, separator);

		Map<String, NetProfit_CTN__c> ctnsById = new Map<String, NetProfit_CTN__c>();
		for (NetProfit_CTN__c existingCtn : getNetProfitCtn(recordId, 'Quote_Profile__c', 'asc')) {
			ctnsById.put(existingCtn.Id, existingCtn);
		}

		for (NetProfit_CTN__c npCTN : [
			SELECT
				Id,
				NetProfit_Information__c,
				Quote_Profile__c,
				Price_Plan_Description__c,
				Discount__c,
				MAF__c,
				Gross_List_Price__c,
				Product_Quantity_type__c,
				Duration__c,
				Action__c,
				CTN_Number__c,
				Simcard_number_VF__c,
				Sharing__c,
				Data_Limit__c,
				dataBlock__c,
				Contract_Number_Current_Provider__c,
				Current_Provider_name__c,
				Service_Provider_Name__c,
				Billing_Arrangement__c,
				Contract_Enddate__c,
				Display_Discount__c,
				OpportunityLineItem__c,
				Generated_Counter__c
			FROM NetProfit_CTN__c
			WHERE NetProfit_Information__r.Opportunity__c = :recordId
		]) {
			String key =
				npCTN.Quote_Profile__c +
				' ' +
				npCTN.Generated_Counter__c +
				' ' +
				npCTN.Action__c;
			if (groupedCTNs.containsKey(key)) {
				groupedCTNs.get(key).add(npCTN);
			} else {
				List<NetProfit_CTN__c> theList = new List<NetProfit_CTN__c>();
				theList.add(npCTN);
				groupedCTNs.put(key, theList);
			}
		}

		for (Integer i = 1; i < parsedCSV.size(); i++) {
			list<String> csvRowData = parsedCSV[i];
			NetProfit_CTN__c existingCTN = ctnsById.get(csvRowData[0].replace('"', ''));
			String key =
				existingCTN.Quote_Profile__c +
				' ' +
				existingCTN.Generated_Counter__c +
				' ' +
				existingCTN.Action__c;
			for (NetProfit_CTN__c loopCTN : groupedCTNs.get(key)) {
				NetProfit_CTN__c ctn = toUpsert.containsKey(loopCTN.Id)
					? toUpsert.get(loopCTN.Id)
					: loopCTN;
				if (ctn.NetProfit_Information__c == null) {
					ctn.NetProfit_Information__c = npInfo.Id;
				}
				ctn.CTN_Number__c = compareValues(
					ctn.CTN_Number__c,
					csvRowData[8].replace('"', '')
				);
				ctn.Simcard_number_VF__c = compareValues(
					ctn.Simcard_number_VF__c,
					csvRowData[9].replace('"', '')
				);
				ctn.Sharing__c = compareValues(ctn.Sharing__c, csvRowData[10].replace('"', ''));
				ctn.Data_Limit__c = compareValues(
					ctn.Data_Limit__c,
					csvRowData[11].replace('"', '')
				);
				ctn.dataBlock__c = compareValues(ctn.dataBlock__c, csvRowData[12].replace('"', ''));
				ctn.Contract_Number_Current_Provider__c = compareValues(
					ctn.Contract_Number_Current_Provider__c,
					csvRowData[13].replace('"', '')
				);
				ctn.Current_Provider_name__c = compareValues(
					ctn.Current_Provider_name__c,
					csvRowData[14].replace('"', '')
				);
				ctn.Service_Provider_Name__c = compareValues(
					ctn.Service_Provider_Name__c,
					csvRowData[15].replace('"', '')
				);
				ctn.Billing_Arrangement__c = compareValues(
					ctn.Billing_Arrangement__c,
					csvRowData[16].replace('"', '')
				);
				ctn.Contract_Enddate__c = csvRowData[17].length() == 1
					? null
					: Date.valueOf(convertStringToDate(csvRowData[17].replace('"', '')));
				toUpsert.put(ctn.Id, ctn);
			}
		}

		try {
			update toUpsert.values();
			success = true;
		} catch (Exception e) {
			System.debug(
				LoggingLevel.ERROR,
				e +
				' - ' +
				e.getLineNumber() +
				' - ' +
				e.getStackTraceString()
			);
			throw new CTNComponentException(e.getMessage());
		}

		return success;
	}

	private static String compareValues(String value, String column) {
		String toReturn;
		if (value == null && column != null) {
			toReturn = column;
		} else if (value != null && column == '') {
			toReturn = value;
		} else if (value != column) {
			toReturn = column;
		} else if (value == column) {
			toReturn = column;
		}
		return toReturn;
	}

	@AuraEnabled
	public static Boolean createNumberListPDF(String oppId, List<NetProfit_CTN__c> ctns) {
		//first check if the cnts pass all validations before createing the pdf
		try {
			update ctns;
		} catch (Exception e) {
			throw new CTNComponentException(e.getMessage());
		}

		Boolean success = false;
		Opportunity theOpp = [
			SELECT Id, Direct_Indirect__c, Name
			FROM Opportunity
			WHERE Id = :oppId
		];
		Pagereference numberlist = Page.Numberlist;
		numberlist.getParameters().put('Id', oppId);

		List<cscfga__Product_Basket__c> baskets = [
			SELECT Id, cscfga__Opportunity__r.Direct_Indirect__c, Name
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c = :oppId
		];
		//If we have a basket create an Aggreement with Attachment
		if (!baskets.isEmpty()) {
			csclm__Agreement__c agreement = ContractRelatedQuestionsController.createAgreement(
				baskets[0]
			);
			//If there is an old Numberlist get rid of it
			delete [
				SELECT Id
				FROM Attachment
				WHERE ParentId = :agreement.Id AND Name = 'Numberlist.pdf'
			];

			Attachment attachment = new Attachment();
			attachment.Name = 'Numberlist.pdf';
			attachment.IsPrivate = false;

			if (!Test.isRunningTest()) {
				attachment.Body = numberlist.getContent();
			} else {
				attachment.body = Blob.valueOf('Body');
			}
			attachment.ParentId = agreement.Id;

			try {
				insert attachment;
				success = true;
			} catch (Exception e) {
				throw new CTNComponentException(e.getMessage());
			}
		} else {
			//If there is not basket create a contract attachment
			try {
				createNewOpportunityAttachment(oppId, numberlist);
				success = true;
			} catch (Exception e) {
				throw new CTNComponentException(e.getMessage());
			}
		}
		return success;
	}

	private static void createNewOpportunityAttachment(
		String opportunityId,
		Pagereference numberList
	) {
		Opportunity_Attachment__c oppAttachment = new Opportunity_Attachment__c(
			Opportunity__c = opportunityId,
			Attachment_Type__c = 'Number List'
		);
		insert oppAttachment;

		ContentVersion contentVer = new ContentVersion(
			Title = 'Numberlist',
			PathOnClient = 'Numberlist' + '.pdf',
			VersionData = !Test.isRunningTest() ? numberlist.getContent() : Blob.valueOf('Body'),
			IsMajorVersion = false,
			ReasonForChange = oppAttachment.Id
		);
		insert contentVer;

		List<ContentVersion> contentVersionList = [
			SELECT ContentDocumentId, ReasonForChange
			FROM ContentVersion
			WHERE Id = :contentVer.Id
		];
		ContentDocumentLink cdl = new ContentDocumentLink();
		cdl.ContentDocumentId = contentVersionList[0].ContentDocumentId;
		cdl.LinkedEntityId = contentVersionList[0].ReasonForChange;
		cdl.ShareType = 'V';
		cdl.Visibility = 'Allusers';
		insert cdl;
	}

	@testVisible
	private static Date convertStringToDate(String dateInput) {
		//modified method to support different formats of date (the date string is dynamic due to user locale setting on Excel)
		Date contractEnddate;
		if (!String.isBlank(dateInput)) {
			try {
				contractEnddate = date.parse(dateInput);
			} catch (exception e) {
				// Then try to process using the valueof method (wants YYYY-MM-DD)
				try {
					contractEnddate = date.valueOf(dateInput);
				} catch (exception e2) {
					// Then try to process by adjusting the string (wants DD-MM-YYYY)
					try {
						String day = dateInput.subString(0, 2);
						String month = dateInput.subString(3, 5);
						String year = dateInput.subString(6, 10);
						contractEnddate = Date.valueOf(year + '-' + month + '-' + day);
					} catch (exception e3) {
						System.debug(
							LoggingLevel.ERROR,
							'Error processing date value: ' + dateInput
						);
						throw new CTNComponentException(
							'Error processing date value: ' +
							dateInput +
							'. Please provide in DD-MM-YYYY or YYYY-MM-DD or YYYY/MM/DD format.'
						);
					}
				}
			}
		} else {
			contractEnddate = null;
		}
		return contractEnddate;
	}

	public class CTNComponentException extends Exception {
	}
}