/**
 * @description: This class is responsible for invoking the CreateSiteAndSLS REST service
 * @author: Jurgen van Westreenen
 */
public class CreateSiteAndSLSRest implements Queueable, Database.AllowsCallouts {
	@TestVisible
	private static final String INTEGRATION_SETTING_NAME = 'SIASRESTCreateSiteAndSLS';
	@TestVisible
	private static final String MOCK_URL = 'http://example.com/CreateSiteAndSLSRest';

	private String cpId;

	public CreateSiteAndSLSRest(String contProdId) {
		this.cpId = contProdId;
	}

	public void execute(QueueableContext context) {
		List<Contracted_Products__c> cpList = [
			SELECT
				Id,
				Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.ExternalAccount__c,
				Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.ExternalAccount__r.External_Account_Id__c,
				Site__c,
				Site__r.Name,
				Site__r.Site_House_Number__c,
				Site__r.Site_Street__c,
				Site__r.Site_City__c,
				Site__r.Country__c,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_House_Number_Suffix__c
			FROM Contracted_Products__c
			WHERE Id = :cpId
			LIMIT 1
		];
		if (cpList.size() > 0) {
			HttpRequest reqData = buildReqHeader();
			sendRequest(reqData, cpList[0]);
		}
	}

	private void sendRequest(HttpRequest reqData, Contracted_Products__c contProd) {
		String reqBody = buildReqBody(contProd);
		reqData.setBody(reqBody);
		reqData.setMethod('POST');

		Http http = new Http();
		HTTPResponse response = http.send(reqData);

		if (response.getStatusCode() == 200) {
			CreateSiteAndSLSSuccessResponse result = (CreateSiteAndSLSSuccessResponse) JSON.deserializeStrict(
				response.getBody(),
				CreateSiteAndSLSSuccessResponse.class
			);
			External_Site__c newExtSite = new External_Site__c();
			newExtSite.External_Account__c = contProd.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.ExternalAccount__c;
			newExtSite.External_Site_Id__c = result.UnifySiteID;
			newExtSite.External_Source__c = 'Unify';
			newExtSite.Site__c = contProd.Site__c;
			newExtSite.SLSAPID__c = result.slsAPID;
			insert newExtSite;
		} else {
			String errorResponse = response.getBody();
			contProd.Error_Info__c = errorResponse;
			update contProd;
		}
	}

	// Using Named Credentials will be part of a larger scale refactoring; suppress for now
	@SuppressWarnings('PMD.ApexSuggestUsingNamedCred')
	private HttpRequest buildReqHeader() {
		// Retrieve the credentials from the custom setting
		External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(
			INTEGRATION_SETTING_NAME
		);
		string endpointURL = webServiceConfig.URL__c;
		String authorizationHeaderString =
			webServiceConfig.Username__c +
			':' +
			webServiceConfig.Password__c;
		String authorizationHeader =
			'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(authorizationHeaderString));

		HttpRequest reqData = new HttpRequest();

		reqData.setHeader('Content-Type', 'application/json');
		reqData.setHeader('Connection', 'keep-alive');
		reqData.setHeader('Content-Length', '0');
		reqData.setHeader('Authorization', authorizationHeader);
		reqData.setTimeout(120000);
		reqData.setEndpoint(endpointURL);

		if (String.isNotEmpty(webServiceConfig.Certificate_Name__c)) {
			reqData.setClientCertificateName(webServiceConfig.Certificate_Name__c);
		}

		return reqData;
	}

	private String buildReqBody(Contracted_Products__c cp) {
		// Generate the request JSON message
		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeFieldName('createSiteAndSLS');
		generator.writeStartObject();
		generator.writeStringField(
			'accountId',
			nullCheck(
				cp.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.ExternalAccount__r.External_Account_Id__c
			)
		);
		generator.writeStringField('sfdcSiteId', nullCheck(cp.Site__c));
		generator.writeStringField('siteName', nullCheck(cp.Site__r.Name));
		generator.writeStringField('houseNumber', nullCheck(cp.Site__r.Site_House_Number__c));
		generator.writeStringField('street', nullCheck(cp.Site__r.Site_Street__c));
		generator.writeStringField('city', nullCheck(cp.Site__r.Site_City__c));
		generator.writeStringField('country', nullCheck(cp.Site__r.Country__c));
		generator.writeStringField('postalCode', nullCheck(cp.Site__r.Site_Postal_Code__c));
		generator.writeStringField(
			'houseNumberSuffix',
			nullCheck(cp.Site__r.Site_House_Number_Suffix__c)
		);
		generator.writeEndObject();
		generator.writeEndObject();

		return generator.getAsString();
	}

	private static String nullCheck(Object inp) {
		if (inp != null) {
			return String.valueOf(inp);
		}
		return '';
	}

	private class CreateSiteAndSLSSuccessResponse {
		String status;
		String unifySiteID;
		String slsAPID;
		String unifyOrderId;
	}
}