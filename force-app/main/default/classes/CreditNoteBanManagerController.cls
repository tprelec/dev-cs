public with sharing class CreditNoteBanManagerController {
	
    private final Credit_Note__c creditNote;
    public BanManagerData theBmData{get;private set;}
    public Boolean banIsEditable{get;private set;}
    public Boolean refreshPage {get; set;}
        public List<SelectOption> getNumbers() {
            List<SelectOption> options = new List<SelectOption>();

            for(Integer i=1;i<11;i++){
               options.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
            }
            return options;
        }
    
    public CreditNoteBanManagerController(ApexPages.StandardController stdController) {
    	if(!Test.isRunningTest()) stdController.addFields(New List<String> {'Id','Account__c','Customer_Ban__r.Name','Customer_Ban__c','Status__c'});
        this.creditNote = (Credit_Note__c)stdController.getRecord();

        // fetch account bans and create a bandata instance
        /*List<Ban__c> bans = [SELECT Id, Name, Ban_Number__c, BAN_Status__c, Account__c 
            FROM Ban__c 
            WHERE BAN_Status__c != 'Closed'
            AND Account__c = :this.creditNote.Account__c];
		*/
        //theBmData = new BanManagerData(bans,this.creditNote.Customer_Ban__c,false,false,true);
        theBmData = new BanManagerData(new Set<Id>{this.creditNote.Account__c},this.creditNote.Customer_Ban__c,false,false,true,true);
        system.debug(theBmData);
        // fetch the contract status to determine if the BAN should be editable
        banIsEditable = true;
        Set<String> nonEditableStatuses = new Set<String>{'Approved','Completed','Advice','Advice Completed'};
        if(nonEditableStatuses.contains(creditNote.Status__c)){
        	banIsEditable = false;
        }
        system.debug(banIsEditable);

        refreshPage=false;
    }

    public PageReference updateBan(){
    	theBmData.errorText = null;

		Savepoint sp = Database.setSavepoint();

		Ban__c bansForCreditNote = new Ban__c();

		try{
			bansForCreditNote = theBmData.getBan(this.creditNote.Account__c);
			if(bansForCreditNote != null){
				this.creditNote.Customer_Ban__c = bansForCreditNote.Id;
			} else {
				this.creditNote.Customer_Ban__c = null;
			}
			update this.creditNote;
			theBmData.banChanged = false;
			theBmData.infoText = 'Ban updated';
            refreshPage=true;
		} catch (Exception e){
			//Apexpages.addMessages(e);
			theBmData.errorText = e.getMessage();
			Database.rollback(sp);

		}	

		return null;

    }

    public PageReference cancelUpdateBan(){
    	theBmData.errorText = null;
		theBmData.banChanged = false;
		// requery the current ban to get the correct value
		theBmData.banSelected = [Select Customer_Ban__r.Name From Credit_Note__c Where Id = :this.creditNote.Id].Customer_Ban__r.Name;		
		return null;

    } 

}