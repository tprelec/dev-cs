public class CreditNoteBatch implements Database.Batchable<sObject> {
	public Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(
			[
				SELECT
					Id,
					Order__c,
					Order__r.Name,
					Order__r.Direct_Indirect__c,
					Order__r.Mobile_Fixed__c,
					Order__r.VF_Contract__r.Opportunity__r.OwnerId,
					Customer_Asset__c,
					Order__r.Account__c,
					Activation_Date__c,
					Start_Invoicing_Date__c,
					Unify_Charge_Description__c,
					Row_Type__c,
					Net_Unit_Price__c,
					Quantity__c,
					Customer_Asset__r.Price__c,
					Customer_Asset__r.Quantity__c,
					Customer_Asset__r.PO_Number__c
				FROM Contracted_Products__c
				WHERE Credit__c = TRUE
			]
		);
	}

	public void execute(Database.BatchableContext bc, List<Contracted_Products__c> contProdList) {
		Map<Id, Order__c> orderMap = new Map<Id, Order__c>();
		Map<Id, Id> creditNoteMap = new Map<Id, Id>();
		Set<Order__c> ordersWithoutNote = new Set<Order__c>();
		for (Contracted_Products__c cp : contProdList) {
			orderMap.put(cp.Order__c, cp.Order__r);
		}
		List<Credit_Note__c> cnList = [
			SELECT Id, Credit_Note_Id__c, Order__c, Order__r.Name
			FROM Credit_Note__c
			WHERE Order__c IN :orderMap.keySet()
		];
		for (Credit_Note__c cn : cnList) {
			if (
				cn.Credit_Note_Id__c ==
				cn.Order__r.Name + '-' + String.valueOf(System.today().month())
			) {
				creditNoteMap.put(cn.Order__c, cn.Id);
			}
		}
		for (Id orderId : orderMap.keySet()) {
			if (!creditNoteMap.containsKey(orderId)) {
				ordersWithoutNote.add(orderMap.get(orderId));
			}
		}
		creditNoteMap.putAll(createCreditNotes(ordersWithoutNote));

		createCreditLines(contProdList, creditNoteMap);

		for (Contracted_Products__c cp : contProdList) {
			if (creditNoteMap.get(cp.Order__c) != null) {
				cp.Credit__c = false;
			}
		}
		update contProdList;
	}

	public void finish(Database.BatchableContext bc) {
	}

	private Map<Id, Id> createCreditNotes(Set<Order__c> orders) {
		Map<String, Schema.RecordTypeInfo> cnRecordTypes = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByDeveloperName();
		List<Credit_Note__c> creditNotes = new List<Credit_Note__c>();
		Map<Id, Id> cnMap = new Map<Id, Id>();
		for (Order__c ord : orders) {
			Credit_Note__c cn = new Credit_Note__c();
			cn.Order__c = ord.Id;
			cn.Credit_Note_Id__c = ord.Name + '-' + String.valueOf(System.today().month());
			cn.Account__c = ord.Account__c;
			cn.RecordTypeId = ord.Direct_Indirect__c == 'Indirect'
				? cnRecordTypes.get('Credit_Note_Indirect').getRecordTypeId()
				: cnRecordTypes.get('Credit_Note_Direct').getRecordTypeId();
			cn.Subscription__c = ord.Mobile_Fixed__c;
			cn.Account_Owner__c = ord.VF_Contract__r.Opportunity__r.OwnerId;
			// Populate other fields
			creditNotes.add(cn);
		}
		Database.SaveResult[] cnList = Database.insert(creditNotes, false);
		System.debug(LoggingLevel.DEBUG, 'cnList: ' + cnList);
		for (Integer i = 0; i < cnList.size(); i++) {
			if (cnList[i].isSuccess()) {
				cnMap.put(creditNotes[i].Order__c, creditNotes[i].Id);
			}
		}
		return cnMap;
	}

	private void createCreditLines(List<Contracted_Products__c> contProds, Map<Id, Id> cnMap) {
		List<Credit_Line__c> creditLines = new List<Credit_Line__c>();
		for (Contracted_Products__c cp : contProds) {
			if (cnMap.get(cp.Order__c) != null) {
				Decimal priceDiff = 0;
				Decimal calcAmount = 0;
				// Below Boolean was defined to avoid PMD Cyclomatic Complexity error
				Boolean priceComplete =
					cp.Net_Unit_Price__c != null &&
					cp.Quantity__c != null &&
					cp.Customer_Asset__r.Price__c != null &&
					cp.Customer_Asset__r.Quantity__c != null;
				if (priceComplete) {
					priceDiff = cp.Row_Type__c == 'Price Change'
						? (cp.Customer_Asset__r.Price__c * cp.Customer_Asset__r.Quantity__c) -
						  (cp.Net_Unit_Price__c * cp.Quantity__c)
						: (cp.Row_Type__c == 'Cease' ? cp.Net_Unit_Price__c * cp.Quantity__c : 0);
				}
				Credit_Line__c cl = new Credit_Line__c();
				cl.Customer_Asset__c = cp.Customer_Asset__c;
				cl.Credit_Note__c = cnMap.get(cp.Order__c);
				cl.Contracted_Product__c = cp.Id;
				cl.Price_Difference__c = priceDiff;
				if (cp.Start_Invoicing_Date__c != null && cp.Activation_Date__c != null) {
					calcAmount =
						(priceDiff / 30.413) *
						cp.Start_Invoicing_Date__c.daysBetween(cp.Activation_Date__c);
				}
				cl.Calculated_Amount__c = calcAmount;
				cl.Description__c =
					cp.Customer_Asset__r.PO_Number__c +
					' - ' +
					cp.Unify_Charge_Description__c +
					' FROM ' +
					String.valueOf(cp.Start_Invoicing_Date__c) +
					' TO ' +
					String.valueOf(cp.Activation_Date__c);
				creditLines.add(cl);
			}
		}
		insert creditLines;
	}
}