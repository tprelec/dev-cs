@SuppressWarnings('PMD.CyclomaticComplexity')
public with sharing class CreditNoteTriggerHandler extends TriggerHandler {
	private List<Credit_Note__c> newCreditNotes;
	private List<Credit_Note__c> oldCreditNotes;
	private Map<Id, Credit_Note__c> newCreditNotesMap;
	private Map<Id, Credit_Note__c> oldCreditNotesMap;

	List<Credit_Note__c> creditNoteToShare = new List<Credit_Note__c>();
	Set<Id> creditNoteDeleteShareIds = new Set<Id>();
	Set<String> dealerCodeSet = new Set<String>();

	Map<Id, VF_Contract__c> contractsMap = new Map<Id, VF_Contract__c>();
	Map<Id, Dealer_Information__c> contractForUpdate = new Map<Id, Dealer_Information__c>();
	Map<String, CreditNote_Approvals__c> directors = new Map<String, CreditNote_Approvals__c>();
	Map<String, CreditNote_Approvals__c> salesManagers = new Map<String, CreditNote_Approvals__c>();
	Map<Id, Account> accountMap = new Map<Id, Account>();

	private void init() {
		oldCreditNotes = (List<Credit_Note__c>) this.oldList;
		newCreditNotes = (List<Credit_Note__c>) this.newList;
		oldCreditNotesMap = (Map<Id, Credit_Note__c>) this.oldMap;
		newCreditNotesMap = (Map<Id, Credit_Note__c>) this.newMap;
	}

	public override void beforeInsert() {
		init();
		populateAccountOwnerSalesManager();
		syncBan();
	}

	public override void afterInsert() {
		init();
		updateSharingRules();
	}

	public override void beforeUpdate() {
		init();
		syncBan();
	}

	public override void afterUpdate() {
		init();
		updateSharingRules();
		sendToUnify();
	}

	private void populateAccountOwnerSalesManager() {
		List<Id> accountIdsToFetch = new List<Id>();

		for (Credit_Note__c credNote : newCreditNotes) {
			accountIdsToFetch.add(credNote.Account__c);
		}

		if (!newCreditNotes.isEmpty() && !accountIdsToFetch.isEmpty()) {
			populateAccountOwnerSalesManagerActual(accountIdsToFetch);
		}
	}

	private void populateAccountOwnerSalesManagerActual(List<Id> accountIdsToFetch) {
		Map<Id, User> ownersMap = GeneralUtils.userMap;
		Set<Id> partnerAccIds = new Set<Id>();
		Set<Id> contractIds = new Set<Id>();
		for (Credit_Note__c cn : newCreditNotes) {
			if (cn.Partner__c != null) {
				partnerAccIds.add(cn.Partner__c);
			}
			if (cn.Contract_VF__c != null) {
				contractIds.add(cn.Contract_VF__c);
			}
		}

		User currentUser = ownersMap.get(UserInfo.getUserId());
		this.setAccountsMap(accountIdsToFetch, partnerAccIds, currentUser.AccountId);
		this.setContractsMap(contractIds);
		this.setApprovers();

		for (Credit_Note__c credNote : newCreditNotes) {
			Account acc = accountMap.get(credNote.Account__c);

			if (acc == null) {
				continue;
			}

			// DIRECT - SOHO - ZIGGO
			this.setDirect(credNote);

			// INDIRECT - PARTNER
			this.setIndirect(credNote, ownersMap);
		}

		sendDealerInfoNotification();
	}

	private void setDirect(Credit_Note__c credNote) {
		if (
			this.checkRecordTypes(
				credNote,
				new List<String>{ 'Credit_Note_Direct', 'Credit_Note_SoHo', 'Credit_Note_fZiggo' }
			)
		) {
			CreditNoteApprovalUser cnau = new CreditNoteApprovalUser(credNote);
			Account acc = this.accountMap.get(credNote.Account__c);
			Id ziggoRT = GeneralUtils.getRecordTypeIdByDeveloperName(
				'Credit_Note__c',
				'Credit_Note_fZiggo'
			);
			Dealer_Information__c dealer = credNote.RecordTypeId == ziggoRT
				? acc.Mobile_Dealer__r
				: setDealer(credNote, acc, contractsMap);

			this.setContractsForUpdate(credNote, dealer);

			if (dealer != null && dealer.ContactUserId__c != null) {
				if (dealer.Contact__r.Userid__r.Non_Personal_User__c) {
					cnau.setCreditNoteApprovalUser('Account_Owner__c', salesManagers, 'CVM/CM');

					cnau.setCreditNoteApprovalUser('Sales_Manager__c', salesManagers, 'CVM/CM');
				} else {
					credNote.Account_Owner__c = dealer.Contact__r.UserId__c;
					setCreditNoteSalesManager(credNote, dealer, directors);
				}
			}

			cnau.setDirectors(credNote, dealer, directors);
		}
	}

	private void setIndirect(Credit_Note__c credNote, Map<Id, User> ownersMap) {
		if (
			this.checkRecordTypes(
				credNote,
				new List<String>{ 'Credit_Note_Indirect', 'Credit_Note_Partner' }
			)
		) {
			User currentUser = ownersMap.get(UserInfo.getUserId());
			CreditNoteApprovalUser cnau = new CreditNoteApprovalUser(credNote);
			Account accRec;
			if (this.checkRecordTypes(credNote, new List<String>{ 'Credit_Note_Indirect' })) {
				accRec = accountMap.get(credNote.Partner__c);
			}
			if (this.checkRecordTypes(credNote, new List<String>{ 'Credit_Note_Partner' })) {
				accRec = accountMap.get(currentUser.AccountId);
			}
			if (accRec != null) {
				this.setIndirectContractsToUpdate(credNote, accRec);

				User u = ownersMap.get(accRec.OwnerId);

				if (u.Non_Personal_User__c) {
					cnau.setCreditNoteApprovalUser(
						'Business_Partner_Manager__c',
						salesManagers,
						'CVM/CM'
					);

					cnau.setCreditNoteApprovalUser('Sales_Manager__c', salesManagers, 'CVM/CM');
				} else {
					credNote.Business_Partner_Manager__c = u.Id;
					credNote.Sales_Manager__c = u.ManagerId;
				}

				cnau.setCreditNoteApprovalUser(
					'Director__c',
					directors,
					'(InDirect) Business Partners'
				);
			}
		}
	}

	private void setIndirectContractsToUpdate(Credit_Note__c credNote, Account acc) {
		if (credNote.Contract_VF__c != null) {
			Dealer_Information__c co = this.contractsMap.containsKey(credNote.Contract_VF__c)
				? this.contractsMap.get(credNote.Contract_VF__c).Dealer_Information__r
				: null;
			if (co == null || (co != null && co.Status__c != 'Active')) {
				contractForUpdate.put(credNote.Contract_VF__c, co);
			}
		}
	}

	private void setApprovers() {
		List<CreditNote_Approvals__c> creditApprovalRules = [
			SELECT Name, User__c, Is_Director__c, Is_Partner_Salesmanager__c, Is_Salesmanager__c
			FROM CreditNote_Approvals__c
		];

		for (CreditNote_Approvals__c cnp : creditApprovalRules) {
			if (cnp.Is_Director__c) {
				this.directors.put(cnp.Name, cnp);
			}
			if (cnp.Is_Partner_Salesmanager__c || cnp.Is_Salesmanager__c) {
				this.salesManagers.put(cnp.Name, cnp);
			}
		}
	}

	private void setContractsForUpdate(Credit_Note__c credNote, Dealer_Information__c dealer) {
		if (
			credNote.Contract_VF__c != null &&
			(dealer == null || (dealer != null && dealer.Status__c != 'Active'))
		) {
			contractForUpdate.put(credNote.Contract_VF__c, dealer);
		}
	}

	private Boolean checkRecordTypes(Credit_Note__c credNote, List<String> recordTypeNames) {
		Map<String, Schema.RecordTypeInfo> cnRecordTypes = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByDeveloperName();
		Boolean isRecordType = false;

		for (String rtn : recordTypeNames) {
			if (cnRecordTypes.get(rtn).getRecordTypeId() == credNote.recordTypeId) {
				isRecordType = true;
				break;
			}
		}

		return isRecordType;
	}

	private void setAccountsMap(
		List<Id> accountIdsToFetch,
		Set<Id> partnerAccIds,
		Id currentUserAccountId
	) {
		this.accountMap = new Map<ID, Account>(
			[
				SELECT
					Id,
					OwnerId,
					Channel__c,
					Mobile_Dealer__c,
					Mobile_Dealer__r.Contact__r.OwnerId,
					Mobile_Dealer__r.Contact__r.Owner.ManagerId,
					Mobile_Dealer__r.Contact__r.Owner.Manager.ManagerId,
					Mobile_Dealer__r.ContactUserId__c,
					Mobile_Dealer__r.Status__c,
					Mobile_Dealer__r.Is_Dealer_in_SFDC__c,
					Mobile_Dealer__r.Contact__r.Userid__c,
					Mobile_Dealer__r.Contact__r.Userid__r.Non_Personal_User__c,
					Mobile_Dealer__r.Contact__r.Userid__r.ManagerId,
					Mobile_Dealer__r.Contact__r.Userid__r.Manager.ManagerId,
					Mobile_Dealer__r.Contact__r.Userid__r.Manager.Manager.Email,
					Mobile_Dealer__r.Contact__r.Account.isPartner,
					Mobile_Dealer__r.Contact__r.Account.Owner.Email,
					Mobile_Dealer__r.Contact__r.Account.OwnerId,
					Mobile_Dealer__r.Contact__r.Account.Name,
					Fixed_Dealer__r.ContactUserid__c,
					Fixed_Dealer__r.Status__c,
					Fixed_Dealer__r.Is_Dealer_in_SFDC__c,
					Fixed_Dealer__r.Contact__r.Userid__c,
					Fixed_Dealer__r.Contact__r.Userid__r.Non_Personal_User__c,
					Fixed_Dealer__r.Contact__r.Userid__r.ManagerId,
					Fixed_Dealer__r.Contact__r.Userid__r.Manager.ManagerId,
					Fixed_Dealer__r.Contact__r.Userid__r.Manager.Manager.Email,
					Fixed_Dealer__r.Contact__r.Account.isPartner,
					Fixed_Dealer__r.Contact__r.Account.Owner.Email,
					Fixed_Credit_Approver__r.ContactUserid__c,
					Fixed_Credit_Approver__r.Status__c,
					Fixed_Credit_Approver__r.Contact__r.Userid__c,
					Fixed_Credit_Approver__r.Contact__r.Userid__r.Non_Personal_User__c,
					Fixed_Credit_Approver__r.Contact__r.Userid__r.ManagerId,
					Fixed_Credit_Approver__r.Contact__r.Userid__r.Manager.ManagerId,
					Fixed_Credit_Approver__r.Contact__r.Userid__r.Manager.Manager.Email,
					Fixed_Credit_Approver__r.Contact__r.Account.Owner.Email,
					Fixed_Credit_Approver__c
				FROM Account
				WHERE Id IN :accountIdsToFetch OR Id IN :partnerAccIds OR Id = :currentUserAccountId
			]
		);
	}

	private void setContractsMap(Set<Id> contractIds) {
		contractsMap = new Map<Id, VF_Contract__c>(
			[
				SELECT
					Id,
					OwnerId,
					Dealer_Information__c,
					Dealer_Information__r.ContactUserid__c,
					Dealer_Information__r.Contact__r.Userid__c,
					Dealer_Information__r.Status__c,
					Dealer_Information__r.Contact__r.Userid__r.ManagerId,
					Dealer_Information__r.Contact__r.Userid__r.Manager.ManagerId,
					Dealer_Information__r.Contact__r.Userid__r.Manager.Manager.Email,
					Dealer_Information__r.Contact__r.Userid__r.Non_Personal_User__c,
					Dealer_Information__r.Contact__r.Account.Owner.Email,
					Dealer_Information__r.Is_Dealer_in_SFDC__c
				FROM VF_Contract__c
				WHERE Id IN :contractIds
			]
		);
	}

	private void setCreditNoteSalesManager(
		Credit_Note__c creditNote,
		Dealer_Information__c dealer,
		Map<String, CreditNote_Approvals__c> directors
	) {
		if (dealer.Contact__r.Userid__r.ManagerId != null) {
			creditNote.Sales_Manager__c = dealer.Contact__r.Userid__r.ManagerId;
		}

		if (dealer.Contact__r.Userid__r.ManagerId != null && !directors.values().isEmpty()) {
			for (CreditNote_Approvals__c currentAproval : directors.values()) {
				if (creditNote.Sales_Manager__c == currentAproval.User__c) {
					creditNote.Sales_Manager__c = dealer.Contact__r.Userid__c;
					break;
				} else {
					creditNote.Sales_Manager__c = dealer.Contact__r.Userid__r.ManagerId;
				}
			}
		}
	}

	/**
	 * return dealer information based on subscription type
	 */
	private Dealer_Information__c setDealer(
		Credit_Note__c credNote,
		Account acc,
		Map<Id, VF_Contract__c> contractsMap
	) {
		Dealer_Information__c dealer;

		if (credNote.Subscription__c != 'Mobile') {
			dealer = acc.Fixed_Credit_Approver__r;
		} else if (credNote.Contract_VF__c != null) {
			dealer = contractsMap.get(credNote.Contract_VF__c).Dealer_Information__r;
		} else {
			dealer = acc.Mobile_Dealer__r;
		}

		return dealer;
	}

	/*
	 * Send the email to Direct/Indirect SAG, EBU and CN creator
	 * in case Dealer on Contract is empty or Dealer status is not active
	 */
	private void sendDealerInfoNotification() {
		if (!contractForUpdate.isEmpty()) {
			Id templateId = [
				SELECT Id
				FROM EmailTemplate
				WHERE DeveloperName = 'Contract_Reassign_Request'
				LIMIT 1
			]
			.Id;
			Contact vfContact = [
				SELECT Id
				FROM Contact
				WHERE Email = 'ebusalesforce@vodafoneziggo.com'
				LIMIT 1
			];
			String indirectSales =
				'insidesales@vodafoneziggo.com' +
				(GeneralUtils.runningInASandbox() ? '.invalid' : '');
			String directSales =
				'EnterpriseSolutionsSales@vodafoneziggo.com' +
				(GeneralUtils.runningInASandbox() ? '.invalid' : '');

			List<Messaging.SingleEmailMessage> msgs = new List<Messaging.SingleEmailMessage>();
			for (Id cId : this.contractForUpdate.keySet()) {
				Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
				msg.setTemplateId(templateId);
				msg.setTargetObjectId(vfContact.Id);
				msg.setWhatId(cId);
				msg.setSenderDisplayName('System Admin');

				List<String> toAddresses = new List<String>();
				if (this.contractForUpdate.get(cId) != null) {
					Contact contact = this.contractForUpdate.get(cId).Contact__r;
					String mngrMail = this.contractForUpdate.get(cId).Is_Dealer_in_SFDC__c
						? contact.Account.Owner.Email
						: contact.Userid__r.Manager.Manager.Email;
					String sagMail = this.contractForUpdate.get(cId).Is_Dealer_in_SFDC__c
						? indirectSales
						: directSales;
					toAddresses.add(mngrMail);
					toAddresses.add(sagMail);
				} else {
					toAddresses.add(directSales);
					toAddresses.add(indirectSales);
				}

				msg.setToAddresses(toAddresses);
				msg.setCcAddresses(new List<String>{ UserInfo.getUserEmail() }); // cc current user
				msgs.add(msg);
			}

			if (!Test.isRunningTest()) {
				Messaging.sendEmail(msgs);
			}
		}
	}

	/**
	 * @description         This method updates the sharing rules on Credit_Note__c
	 *                      If a BPM creates a credit note for a Partner, that partner (all users of it) should be able to see that credit note.
	 *                      Sharing credit notes is therefore being done here based on the dealer code stored on the credit note. (W-000234)
	 */
	private void updateSharingRules() {
		List<Credit_Note__share> creditNoteSharingRulesToInsert = new List<Credit_Note__share>();

		// String = the dealer code
		// ID = the ID of the Group associated with the Role of the dealer user
		Map<String, ID> dealerToGroupMap = new Map<String, ID>();

		this.populateSharingRuleMaps();

		// Populate the dealerToGroup map
		// Get the users from the dealers
		List<Dealer_Information__c> dealerList = [
			SELECT ContactUserId__c, Dealer_Code__c
			FROM Dealer_Information__c
			WHERE Dealer_Code__c IN :dealerCodeSet
		];
		Set<ID> dealerUserSet = new Set<ID>();
		for (Dealer_Information__c di : dealerList) {
			dealerUserSet.add(di.ContactUserId__c);
		}
		// Get the developername of the role from the dealer users
		Map<ID, User> userMap = new Map<ID, User>(
			[SELECT UserRole.DeveloperName FROM User WHERE Id IN :dealerUserSet]
		);
		Set<String> developerNameSet = new Set<String>();
		for (ID userID : userMap.keySet()) {
			User u = userMap.get(userID);
			developerNameSet.add(u.UserRole.DeveloperName);
		}
		// Get the group that matches the users userrole
		List<Group> groupList = [
			SELECT Id, DeveloperName
			FROM Group
			WHERE DeveloperName IN :developerNameSet AND Type = 'RoleAndSubordinates'
		];
		Map<String, Id> developerNameToIDMap = new Map<String, Id>();
		for (Group g : groupList) {
			developerNameToIDMap.put(g.DeveloperName, g.id);
		}
		// Now we take all that data to populate the map
		for (Dealer_Information__c di : dealerList) {
			String dealerCode = di.Dealer_Code__c;
			String developerName = userMap.get(di.ContactUserId__c).UserRole.DeveloperName;
			Id groupID = developerNameToIDMap.get(developerName);
			dealerToGroupMap.put(dealerCode, groupID);
		}

		// remove any existing shares on these credit notes
		SharingUtils.deleteRecordsWithoutSharing(
			[
				SELECT id
				FROM Credit_Note__share
				WHERE
					ParentId IN :creditNoteDeleteShareIds
					AND UserOrGroupId IN :groupList
					AND RowCause = 'Manual'
			]
		);

		// Create a new share for each credit note that requires one
		if (!creditNoteToShare.isEmpty()) {
			for (Credit_Note__c cn : creditNoteToShare) {
				Id ugId = dealerToGroupMap.get(cn.Dealercode__c);
				if (ugId != null) {
					Credit_Note__share cns = new Credit_Note__share();
					cns.UserOrGroupId = ugId; // id of the group matching the role of the dealer user
					cns.ParentId = cn.id; // id of the credit note
					cns.AccessLevel = 'Read';
					creditNoteSharingRulesToInsert.add(cns);
				}
			}
			SharingUtils.insertRecordsWithoutSharing(creditNoteSharingRulesToInsert);
		}
	}

	private void populateSharingRuleMaps() {
		if (oldCreditNotesMap == null) {
			// insert. Always create a sharing rule when dealer code exists
			this.populateSharingRuleMapsOnInsert();
		} else if (newCreditNotes != null) {
			//update. check if the dealer changed
			this.populateSharingRuleMapsOnUpdate();
		}
	}

	private void populateSharingRuleMapsOnInsert() {
		for (Credit_Note__c cn : newCreditNotes) {
			if (cn.Dealercode__c != null) {
				// Add this credit note to the list as we need to create a share on it
				creditNoteToShare.add(cn);
				// Add this dealer code to the set so we can populate the map with the group id
				dealerCodeSet.add(cn.Dealercode__c);
			}
		}
	}

	private void populateSharingRuleMapsOnUpdate() {
		for (Credit_Note__c cn : newCreditNotes) {
			// There is a new dealer code so we need to add a share for that dealer
			if (
				GeneralUtils.isRecordFieldChanged(cn, oldCreditNotesMap, 'Dealercode__c') &&
				cn.Dealercode__c != null
			) {
				// Add this credit note to the list as we need to create a share on it
				creditNoteToShare.add(cn);
				// Add this dealer code to the set so we can populate the map with the group id
				dealerCodeSet.add(cn.Dealercode__c);
			}

			// The old dealer has been removed so we need to delete the share for that dealer
			if (
				GeneralUtils.isRecordFieldChanged(cn, oldCreditNotesMap, 'Dealercode__c') &&
				oldCreditNotesMap.get(cn.Id).Dealercode__c != null
			) {
				creditNoteDeleteShareIds.add(cn.ID);
				// Add the old dealer code to the set so we can populate the map with the group id
				dealerCodeSet.add(oldCreditNotesMap.get(cn.Id).Dealercode__c);
			}
		}
	}

	private void syncBan() {
		Map<Id, String> recTypeMap = new Map<Id, String>();
		for (Credit_Note__c cn : newCreditNotes) {
			if (cn.RecordTypeId != null) {
				recTypeMap.put(cn.RecordTypeId, null);
			}
		}
		if (recTypeMap.size() > 0) {
			List<RecordType> recTypeList = [
				SELECT Id, DeveloperName
				FROM RecordType
				WHERE Id IN :recTypeMap.keySet()
			];
			for (RecordType rt : recTypeList) {
				recTypeMap.put(rt.Id, rt.DeveloperName);
			}
		}
	}

	private void sendToUnify() {
		Set<Credit_Note__c> cnList = new Set<Credit_Note__c>();
		for (Credit_Note__c cn : newCreditNotes) {
			if (
				cn.Approved__c &&
				!oldCreditNotesMap.get(cn.Id).Approved__c &&
				cn.Credit_Note_Id__c != null
			) {
				cnList.add(cn);
			}
		}
		if (!cnList.isEmpty()) {
			System.enqueueJob(new CreateCreditRest(cnList));
		}
	}

	private class CreditNoteApprovalUser {
		private Credit_note__c creditNote { get; set; }

		public CreditNoteApprovalUser(Credit_note__c creditNote) {
			this.creditNote = creditNote;
		}

		public void setCreditNoteApprovalUser(
			String fieldName,
			Map<String, CreditNote_Approvals__c> roleRules,
			String ruleName
		) {
			Id uId;
			if (roleRules.containsKey(ruleName)) {
				uId = roleRules.get(ruleName).User__c;
			}
			if (uId != null) {
				this.creditNote.put(fieldName, uId);
			}
		}

		public void setDirectors(
			Credit_Note__c credNote,
			Dealer_Information__c dealer,
			Map<String, CreditNote_Approvals__c> directors
		) {
			if (dealer != null && dealer.ContactUserId__c != null) {
				Map<Id, User> ownersMap = GeneralUtils.userMap;
				User userDetails = ownersMap.get(credNote.Account_Owner__c);
				if (
					!userDetails.UserRole.Name.contains('SoHo') ||
					!userDetails.UserRole.Name.contains('VGE')
				) {
					this.setCreditNoteApprovalUser(
						'Director__c',
						directors,
						'(Direct) Creditnote Corperate'
					);
				}
				if (userDetails.UserRole.Name.contains('VGE')) {
					this.setCreditNoteApprovalUser('Director__c', directors, '(Direct) VGE');
				}
				if (
					userDetails.UserRole.Name.contains('SoHo') ||
					userDetails.UserRole.Name.contains('Small')
				) {
					this.setCreditNoteApprovalUser('Director__c', directors, '(Direct) SoHo');
				}
			}
		}
	}
}