global with sharing class CustomButtonAdvanceClone extends csbb.CustomButtonExt { 
    public String performAction (String basketId) { 
        return '{"status":"ok","title":"Basket button","text":"All OK.","timeout":1000}'; // timeout 0 for infinite 
        
    } 
    public String performAction (String basketId, String pcrIds) {
        
        if(pcrIds == '' || pcrIds== null || basketId == '' || basketId== null || String.valueOf(pcrIds) =='[]'){
            return '{"status":"error","title":"Product button","text":"Please select a package","timeout":1000}'; 
        }
        
        List<String> pcrInList = (List<String>)JSON.deserialize(pcrIds, List<String>.class);
        
       
        
        Integer pcrCount = pcrInList.size();
        //Integer pcrCount = ((List<String>)JSON.deserialize(pcrIds, List<String>.class)).size(); 
        if(pcrCount>1){
        	return '{"status":"error","title":"Product button","text":"You can only advance clone 1 product at a time","timeout":1000}'; 

        }

        Id packageDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Package Definition').getRecordTypeId(); 
        Id pcrId = (Id)pcrInList[0];
        List<csbb__Product_Configuration_Request__c> pcr = [Select csbb__Product_Configuration__c from csbb__Product_Configuration_Request__c where Id = :pcrId];
        if(pcr.size()>0){
            Id configToCloneId = (Id)pcr[0].csbb__Product_Configuration__c;
            List<cscfga__Product_Configuration__c> pc = [Select cscfga__Product_Definition__r.RecordTypeId, Name,cscfga__Product_Basket__c, Id from cscfga__Product_Configuration__c where Id = :configToCloneId];
            if(pc.size()>0){
                 
                  if(pc[0].cscfga__Product_Definition__r.RecordTypeId == packageDefinitionRecordType){
                    //redundancy modifications
                    
                    //check access infrastructure attributes site and primary and secondary
                    Id packageId = pc[0].Id;
                    
                    List<cscfga__Attribute__c> accessAttr = [SELECT cscfga__Value__c, name, cscfga__Product_Configuration__c from cscfga__Attribute__c where name like 'Access%' and cscfga__Product_Configuration__c = :packageId];
                    
                    if(accessAttr.size()>0){
                        List<cscfga__Product_Configuration__c> pcAcc = [SELECT Id, Secondary__c, Primary__c, Site_Check_SiteID__c,cscfga__package_slot__c, name from cscfga__Product_Configuration__c where cscfga__package_slot__c = :accessAttr[0].Id];
                        
                        if(pcAcc.size()>0){
                            if(pcAcc[0].Site_Check_SiteID__c == null){
                                return '{"status":"error","title":"Product button","text":"Please perform site check before performing advance clone.","timeout":1000}';
                            }
                            else if((pcAcc[0].Secondary__c == true) || (pcAcc[0].Primary__c == true)){
                                return '{"status":"error","title":"Product button","text":"Redundancy configurations - primary or secondary are not eligible for advance clone","timeout":1000}';
                            }
                            
                            else{
                                CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();
                                PageReference editPage = new PageReference(url.URL__c+'/apex/AdvanceClone?pcrId='+pcrIds.removeStart('[').removeEnd(']').removeEnd('\"').removeStart('\"')+'&basketId='+basketId);
                                
                                Id profileId = UserInfo.getProfileId();
                                String profileName = [Select Id, Name from Profile where Id =: profileId].Name;
                        
                                if(profileName == 'VF Partner Portal User') {
                                    editPage = new PageReference('/partnerportal/apex/AdvanceClone?pcrId='+pcrIds.removeStart('[').removeEnd(']').removeEnd('\"').removeStart('\"')+'&basketId='+basketId);
                                }
                                
                                return '{"status":"ok","redirectURL":"' + editPage.getUrl() + '"}';
                            }
                        }
                        else{
                            return '{"status":"error","title":"Product button","text":"Please select a package with access defined - no access config","timeout":1000}'; 
                        }
                    }
                    else{
                        return '{"status":"error","title":"Product button","text":"Please select a package with access defined -- no access attribute","timeout":1000}'; 
                    }
                    
                    //end mod
                    /*
                    CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();
                    System.debug('PCRIDS = '+pcrIds);
                    PageReference editPage = new PageReference(url.URL__c+'/apex/AdvanceClone?pcrId='+pcrIds.removeStart('[').removeEnd(']').removeEnd('\"').removeStart('\"')+'&basketId='+basketId);
                    return '{"status":"ok","redirectURL":"' + editPage.getUrl() + '"}';
                    */
                  }
            
                   else{
                        return '{"status":"error","title":"Product button","text":"Please select a package","timeout":1000}';  
                    }
            }
            else{
                return '{"status":"error","title":"Product button","text":"Please select a package","timeout":1000}';  
            }
        }
        else{
           return '{"status":"error","title":"Product button","text":"Please select a package","timeout":1000}';  
        }
        
    } 
}