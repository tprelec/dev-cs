global with sharing class CustomButtonAttachment extends csbb.CustomButtonExt 
{
  public String performAction(String basketId)
    {
        String newUrl = CustomButtonAttachment.redirectToAttachment(basketId);
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    }
    
    // Set url for redirect after action
    public static String redirectToAttachment(String basketId)
    {   
        String urlInstance = String.valueof(System.URL.getSalesforceBaseURL()).replace('Url:[delegate=','').replace(']','');
        //String[] instance = urlInstance.split('\\.');        
        
         CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();
                                                      
        //PageReference editPage = new PageReference('https://vfcloudsense--devcs.cs88.my.salesforce.com/apex/ProductBasketAttachmentManager?basketId='+basketId);
        
        PageReference editPage = new PageReference(url.URL__c + '/apex/ProductBasketAttachmentManager?basketId=' + basketId);
        
        Id profileId = UserInfo.getProfileId();
        String profileName = [Select Id, Name from Profile where Id =: profileId].Name;

        if(profileName == 'VF Partner Portal User') {
            editPage = new PageReference('/partnerportal/apex/ProductBasketAttachmentManager?basketId=' + basketId);
        }
        
        //https://vfcloudsense--devcs.cs88.my.salesforce.com/
        return editPage.getUrl();
    }
  
  
}