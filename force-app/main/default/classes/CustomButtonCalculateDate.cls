global without sharing class CustomButtonCalculateDate extends csbb.CustomButtonExt {

    public String performAction (String basketId) {
        Savepoint sp = Database.setSavepoint();
        try {
            cscfga__Product_Basket__c basket = [SELECT Id, csbb__Account__c FROM cscfga__Product_Basket__c WHERE Id = :basketId LIMIT 1];
            AccountService.getInstance().setExpectedDeliveryDateForVoiceAndVoiceDataAssets(new Set<Id>{basket.csbb__Account__c});
            return JSON.serialize(new CustomButtonResponse('ok', System.Label.SUCCESS, System.Label.SUCCESS_Expected_Delivery_Date_Calculation), true);
        } catch (Exception ex) {
            Database.rollback(sp);
            return JSON.serialize(new CustomButtonResponse('error', System.Label.ERROR, System.Label.ERROR_Expected_Delivery_Date_Calculation), true);
        }
    }

}