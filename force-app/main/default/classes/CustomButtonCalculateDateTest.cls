@isTest
public with sharing class CustomButtonCalculateDateTest {

    @testSetup
    static void makeData(){
        Account tmpAcc = CS_DataTest.createAccount('Test Account');
        insert tmpAcc;

        Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', UserInfo.getUserId());
        insert tmpOpp;

        cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
        insert tmpProductBasket;
    }

    @isTest
    private static void testPositive() {
        Test.startTest();
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
        CustomButtonCalculateDate calculateDate = new CustomButtonCalculateDate();
        String res = calculateDate.performAction(basket.Id);
        Test.stopTest();
        CustomButtonResponse response = (CustomButtonResponse)JSON.deserialize(res, CustomButtonResponse.class);
        System.assertEquals(System.Label.SUCCESS_Expected_Delivery_Date_Calculation, response.text);
    }

    @isTest
    private static void testNegative() {
        Test.startTest();
        CustomButtonCalculateDate calculateDate = new CustomButtonCalculateDate();
        String res = calculateDate.performAction(null);
        Test.stopTest();
        CustomButtonResponse response = (CustomButtonResponse)JSON.deserialize(res, CustomButtonResponse.class);
        System.assertEquals(System.Label.ERROR_Expected_Delivery_Date_Calculation, response.text);
    }
}