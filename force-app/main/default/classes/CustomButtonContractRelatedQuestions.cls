global with sharing class CustomButtonContractRelatedQuestions extends csbb.CustomButtonExt {
	
	public String performAction (String basketId) { 
		CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();
        //PageReference editPage = new PageReference(url.URL__c+'/apex/ContractRelatedQuestions?basketId=' + basketId);
        //if Expected locations is set
		List<cscfga__Product_Basket__c> basket = [SELECT Id, Total_Number_of_Expected_Locations__c, DirectIndirect__c, Approved_Date__c, Clauses_in_promotion__c from cscfga__Product_Basket__c where Id = :basketId];
		String enabledContract = 'false';
		
		if(basket.size()>0){
		    if (basket[0].Total_Number_of_Expected_Locations__c == null){
		        enabledContract = 'true';
		    }
		}
        PageReference editPage = new PageReference(url.URL__c+'/apex/ContractRelatedQuestions?basketId=' + basketId+'&enabledContract='+enabledContract);

        if(basket != null && basket[0] != null)
            CS_ContractsTableHelper.setPromotionClauses(basket[0]);
        
        Id profileId = UserInfo.getProfileId();
        String profileName = [Select Id, Name from Profile where Id =: profileId].Name;
        
        String redirectUrl = '/' + basketId;
        if(profileName == 'VF Partner Portal User') {
        	editPage = new PageReference('/partnerportal/apex/ContractRelatedQuestions?basketId=' + basketId+'&enabledContract='+enabledContract+'&showheader=false&sidebar=false');
        }
        
        editPage = new PageReference(checkBasketComplexity(basketId, editPage.getUrl(), redirectUrl));
        
        return '{"status":"ok","redirectURL":"' + editPage.getUrl() + '"}'; 
    } 
    
    public String performAction (String basketId, String pcrIds) { 
		CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();
		
		//if Expected locations is set
		List<cscfga__Product_Basket__c> basket = [SELECT Id, Total_Number_of_Expected_Locations__c from cscfga__Product_Basket__c where Id = :basketId];
		String enabledContract = 'false';
		
		if(basket.size()>0){
		    if (basket[0].Total_Number_of_Expected_Locations__c == null){
		        enabledContract = 'true';
		    }
		}
        PageReference editPage = new PageReference(url.URL__c+'/apex/ContractRelatedQuestions?basketId=' + basketId+'&enabledContract='+enabledContract);
        
        Id profileId = UserInfo.getProfileId();
        String profileName = [Select Id, Name from Profile where Id =: profileId].Name;
        
        String redirectUrl = '/' + basketId;
        if(profileName == 'VF Partner Portal User') {
        	editPage = new PageReference('/partnerportal/apex/ContractRelatedQuestions?basketId=' + basketId+'&enabledContract='+enabledContract+'&showheader=false&sidebar=false');
        }
        
        editPage = new PageReference(checkBasketComplexity(basketId, editPage.getUrl(), redirectUrl));
        
        return '{"status":"ok","redirectURL":"' + editPage.getUrl() +'"}'; 
    } 
    
    public String checkBasketComplexity(String basketId, String successUrl, String failureUrl) {
        CS_Future_Controller futureController;
        Map<String, Future_Optimisation__c> optimisationSettingsMap = Future_Optimisation__c.getAll();
        Future_Optimisation__c optimisationSettings = optimisationSettingsMap.get('Standard Optimisation');
        
        List<AggregateResult> countProductConfigurations = [SELECT count(id) total from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId];
        Integer countProductConfigurationsInteger = Integer.valueOf(countProductConfigurations[0].get('total'));

        if(optimisationSettings != null && optimisationSettings.Optimisation_Enabled__c == true && optimisationSettings.Configuration_number_limit__c < countProductConfigurationsInteger) {
            //String successUrl = '/apex/appro__VFSubmitPreview?id=' + basketId;
            //String failureUrl = '/' + basketId;
            futureController = new CS_Future_Controller(basketId, 'Parent process for preparing Contract Data.', successUrl, failureUrl);
            String newUrl = '/apex/c__CS_Future_Waiter_Page?basketId=' + basketId + '&parentProcessId=' + futureController.parentId;
            
            Id newFutureId = futureController.defineNewFutureJob('Collect contract data');
            CS_ContractPDFController.populateDataAsync(basketId, newFutureId);
            
            return newUrl;
        }
        
        return successUrl;
    }
}