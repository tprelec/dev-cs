@isTest
private class CustomButtonContractRelatedQuestionsTest {

	private static testMethod void test() {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            Test.startTest();
            
            No_Triggers__c notriggers = new No_Triggers__c();
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;
            
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;
            
            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', simpleUser.Id);
            insert tmpOpp;
            
            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);  
            insert tmpProductBasket;
            
            CustomButtonContractRelatedQuestions contractQuestions = new CustomButtonContractRelatedQuestions();
            contractQuestions.performAction(tmpProductBasket.Id);
            contractQuestions.performAction(tmpProductBasket.Id,'');
            
            Test.stopTest();
        }
    }
}