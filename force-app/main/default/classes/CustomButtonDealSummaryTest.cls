@isTest
private class CustomButtonDealSummaryTest {

    private static testMethod void test() {

    List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            Test.startTest();
            
            No_Triggers__c notriggers = new No_Triggers__c();
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;
            
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;
            
            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
            insert tmpOpp;
            
            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);  
            tmpProductBasket.Primary__c = false;
            tmpProductBasket.cscfga__Basket_Status__c = 'Valid';
            tmpProductBasket.Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]';
            insert tmpProductBasket;
            
            CustomButtonDealSummary dealSummary = new CustomButtonDealSummary();
            dealSummary.performAction(tmpProductBasket.Id);
            
            cscfga__Product_Basket__c tmpProductBasket2 = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);  
            tmpProductBasket2.Primary__c = false;
            tmpProductBasket2.cscfga__Basket_Status__c = 'Approved';
            tmpProductBasket2.Used_Snapshot_Objects__c = '';
            insert tmpProductBasket2;
            
            dealSummary.performAction(tmpProductBasket2.Id);
            
            Test.stopTest();
        }
            
    }

}