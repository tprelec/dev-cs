global with sharing class CustomButtonDiscount extends csbb.CustomButtonExt {
  /*
  public String performAction (String basketId) {
    String newUrl = '/apex/c__DiscountPage?basketId=' + basketId;
    return '{"status":"ok","redirectURL":"' + newUrl + '"}';
  }
  */
  
  public String performAction(String basketId)
    {
        try{
            String newUrl = CustomButtonDiscount.redirectToDiscount(basketId);
            return '{"status":"ok","redirectURL":"' + newUrl + '"}';
        }
        catch(System.CalloutException e){
            System.debug('ERROR:' + e);
            return '{"status":"error"}';
        }
        
    }
    
    // Set url for redirect after action
    public static String redirectToDiscount(String basketId)
    {   
        String urlInstance = String.valueof(System.URL.getSalesforceBaseURL()).replace('Url:[delegate=','').replace(']','');
        
        CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();
                                                      
        PageReference editPage = new PageReference(url.URL__c+'/apex/csdiscounts__DiscountPage?basketId=' + basketId);
        
        Id profileId = UserInfo.getProfileId();
        String profileName = [Select Id, Name from Profile where Id =: profileId].Name;
        
        if(profileName == 'VF Partner Portal User') {
        	editPage = new PageReference('/partnerportal/apex/csdiscounts__DiscountPage?basketId=' + basketId);
        }
        
        return editPage.getUrl();
    }
  
}