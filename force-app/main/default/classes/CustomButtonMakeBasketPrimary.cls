global with sharing class CustomButtonMakeBasketPrimary extends csbb.CustomButtonExt 
{
     public String performAction (String basketId){
         
         cscfga__Product_Basket__c basket = [SELECT Id, Name, Primary__c, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
         
         if(basket.Primary__c){
             return '{"status":"ok","title":"","text":"Basket is already primary!"}';
         } else{
             return makeBasketPrimary(basket);
         }
     }
     
     //make the selected Basket primary and make all the other baskets not primary
     private String makeBasketPrimary(cscfga__Product_Basket__c basket){
         List<cscfga__Product_Basket__c> basketUpdateList = new List<cscfga__Product_Basket__c>();
         
         //get all the other Baskets from the Opportunity
         List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>([SELECT Id, Name, cscfga__Opportunity__c, Primary__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :basket.cscfga__Opportunity__c AND Id != :basket.Id]);
         system.debug('**** makeBasketPrimary, basketList: ' + basketList);
         
         for(cscfga__Product_Basket__c tmpBasket : basketList){
             if(tmpBasket.Primary__c){
                 tmpBasket.Primary__c = false;
                 basketUpdateList.add(tmpBasket);
             }
         }
         
         basket.Primary__c = true;
         basketUpdateList.add(basket);
         
         try{
            if(basketUpdateList.size() > 0){
                update basketUpdateList;   
                return '{"status":"ok","title":"","text":"Basket set to Primary!","redirectURL":"/'+basket.Id+'"}';
            } else {
                return '{"status":"error","title":"","text":"Basket not set to Primary"}';
            }
         }catch(Exception e){
             return '{"status":"error","text":"' + e.getMessage() + '"}';
         }
     }
}