global with sharing class CustomButtonManageApprovals extends csbb.CustomButtonExt 
{
  public String performAction(String basketId)
    {
        String newUrl = CustomButtonManageApprovals.redirectToManageApprovals(basketId);
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    }
    
    // Set url for redirect after action
    public static String redirectToManageApprovals(String basketId)
    {   String urlInstance = String.valueof(System.URL.getSalesforceBaseURL()).replace('Url:[delegate=','').replace(']','');
        //String[] instance = urlInstance.split('\\.');        
        
        CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();                                                     
        //PageReference editPage = new PageReference(url.URL__c+'/apex/ProductBasketApproval?Id='+basketId);
        PageReference editPage = new PageReference(url.URL__c+'/apex/appro__VFSubmitPreview?id='+basketId);
        System.debug('new link');
        return editPage.getUrl();
    }
  
  
}