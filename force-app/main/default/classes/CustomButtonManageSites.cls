global with sharing class CustomButtonManageSites extends csbb.CustomButtonExt 
{
  public String performAction(String basketId)
    {
        String newUrl = CustomButtonManageSites.redirectToManageSites(basketId);
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    }
    
    // Set url for redirect after action
    public static String redirectToManageSites(String basketId)
    {   String urlInstance = String.valueof(System.URL.getSalesforceBaseURL()).replace('Url:[delegate=','').replace(']','');
        //String[] instance = urlInstance.split('\\.');        
        
        CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();                                                     
        PageReference editPage = new PageReference(url.URL__c + '/apex/SiteManager?basketId=' + basketId);
        
        Id profileId = UserInfo.getProfileId();
        String profileName = [Select Id, Name from Profile where Id =: profileId].Name;
        
        if(profileName == 'VF Partner Portal User') {
        	editPage = new PageReference('/partnerportal/apex/SiteManager?basketId=' + basketId);
        }
        
        return editPage.getUrl();
    }
  
  
}