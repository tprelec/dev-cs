global with sharing class CustomButtonMultiLineEdit extends csbb.CustomButtonExt{

// Perform action implementation

    public String performAction(String basketId){
        String newUrl = CustomButtonMultiLineEdit.openMultiLineEditor(basketId);
        return '{"status":"ok","redirectURL":"' + newUrl + '","text":"Success"}';
    }
    // Set url for redirect after action
    
    public static String openMultiLineEditor(String basketId){
        String urlInstance = String.valueof(System.URL.getSalesforceBaseURL()).replace('Url:[delegate=','').replace(']','');
        String[] instance = urlInstance.split('\\.');
        PageReference editPage = new PageReference('https://vfcloudsense--devcs.cs88.my.salesforce.com/apex/csmle__Editor?id=' + basketId + '&batchSize=5&productDefinitionId=a4F9E0000009Qxi');    
        return editPage.getUrl();
    }
}