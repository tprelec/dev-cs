global with sharing class CustomButtonPandLVF extends csbb.CustomButtonExt  
{
    public String performAction (String basketId) 
    { 
        CS_Future_Controller futureController;
        Map<String, Future_Optimisation__c> optimisationSettingsMap = Future_Optimisation__c.getAll();
        Future_Optimisation__c optimisationSettings = optimisationSettingsMap.get('Standard Optimisation');
        
        List<AggregateResult> countProductConfigurations = [SELECT count(id) total from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId];
        Integer countProductConfigurationsInteger = Integer.valueOf(countProductConfigurations[0].get('total'));

        String plUrl = '/apex/cspl__PLReportNew?id=' + basketId;

        if (optimisationSettings != null && optimisationSettings.Optimisation_Enabled__c == true && optimisationSettings.Configuration_number_limit__c < countProductConfigurationsInteger) {
            futureController = new CS_Future_Controller(basketId, 'Parent process for P&L', plUrl, plUrl);
            String newUrl = '/apex/c__CS_Future_Waiter_Page?basketId=' + basketId + '&parentProcessId=' + futureController.parentId;
            
            Id newFutureId = futureController.defineNewFutureJob('P&L snapshots and inputs');
            createPlPrerequisitiesAsync(basketId, newFutureId);
            
            return '{"status":"ok","redirectURL":"' + newUrl + '"}';
        }
        else {
            createSnapshotRecords(basketId);
            createPlInputs(basketId);
            
            return '{"status":"ok","redirectURL":"' + plUrl + '"}';
        }
    }
    
    private static void createSnapshotRecords(Id basketId){
        /*
        List<CS_Basket_Snapshot_Transactional__c> oldData = [SELECT Id from CS_Basket_Snapshot_Transactional__c where Product_Basket__c = :basket.Id];
        if(oldData.size() > 0){
            SharingUtils.deleteRecordsWithoutSharing(oldData);
            
        }
        */
        cscfga__Product_Basket__c basket = [SELECT Id, Used_Snapshot_Objects__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
        
        if((basket.Used_Snapshot_Objects__c == '') ||(basket.Used_Snapshot_Objects__c == null)||(basket.Used_Snapshot_Objects__c != '[CS_Basket_Snapshot_Transactional__c]')){
            basket.Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]';
            update basket;
        }
        CS_BasketSnapshotManager.TakeBasketSnapshot(basket, true);
    }

    private static void createPlInputs(Id basketId) {
        cscfga__Product_Basket__c basket = [SELECT Id, PL_Inputs__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
        basket.PL_Inputs__c = CS_PlInputsSerilizer.getPlInputsSerialized(basketId);
        update basket;
    }

    @future
    public static void createPlPrerequisitiesAsync(String basketId, Id newFutureId) {
        CS_Future_Controller.startFutureJob(newFutureId);

        try {
            createSnapshotRecords(basketId);
            createPlInputs(basketId);
            CS_Future_Controller.endFutureJob(newFutureId);
        }
        catch (Exception ex) {
            CS_Future_Controller.failFutureJob(newFutureId, ex.getMessage());
        }
    }
}