global with sharing class CustomButtonPartnerValidateAndSubmit extends csbb.CustomButtonExt 
{
    public static final String errorMsgStatus = '{"status":"error","title":"Error",'
                                            + '"text":"Basket must be in status Valid to be submited for Approval."}';
    public static final String errorMsgWarning = '{"status":"error","title":"Error",'
                                            + '"text":"Please correct errors described in the warning section before submitting for approval."}';
                                            
    public static final String errorMsgTotalNumberExpectedLocations = '{"status":"error","title":"Error",'
                                            + '"text":"There is a total number of expected locations specified. Please remove this value and make sure all required locations are included in the configurations."}';
  
  
    private Boolean checkOneNet(List<cscfga__Product_Configuration__c> configs){
        List<cscfga__Product_Configuration__c> needOneNetE = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> needOneNetX = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> oneNet = new List<cscfga__Product_Configuration__c>();
        
        Boolean accessONE = false;
        Boolean accessONX = false;  
        
        Boolean oneError = false;
        Boolean onxError = false;
        Boolean hasAnyMobileCtnProfileWithEnabledIntegrateOneNet = false;
        
        List<cscfga__Attribute__c> attrList = [SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c IN :configs AND Name = 'IntegrateOneNet'];
        
        //23-8-2019-W-002011 - validation only applicable when one net integrate checbox set to yes and basket contains ONLY new and porting subscriptions 
        //                   - if contains other connection types DONt run the validation
        Boolean newOrPortingConnectionTypesExist = false;
        Boolean notNewOrPortingConnectionTypesExist = false;
        
        for(cscfga__Product_Configuration__c pc : configs) {
            if(pc.cscfga__Product_Definition__r.Name == 'Mobile CTN subscription') {
                if(pc.Deal_type__c == 'New' || pc.Deal_type__c == 'Porting') {
                    newOrPortingConnectionTypesExist = true;
                }
                if(pc.Deal_type__c != 'New' && pc.Deal_type__c != 'Porting') {
                    notNewOrPortingConnectionTypesExist = true;
                }
            }
        }
        
        for(cscfga__Product_Configuration__c pc : configs){
            system.debug('>>>>>>>>>>>>>>>>>>>pc.cscfga__Product_Definition__r.Name: ' + pc.cscfga__Product_Definition__r.Name);
            if(pc.cscfga__Product_Definition__r.Name == 'Mobile CTN profile') {
                cscfga__Attribute__c attr = getAttributePerName(pc.Id, 'IntegrateOneNet', attrList);
                system.debug('>>>>>>>>>>>>>>>>>>>attr: ' + attr);
                if (attr != null && attr.cscfga__Value__c != null && attr.cscfga__Value__c == 'Yes' && newOrPortingConnectionTypesExist && !notNewOrPortingConnectionTypesExist){
                    hasAnyMobileCtnProfileWithEnabledIntegrateOneNet = true;
                } 
            }
            system.debug('>>>>>>>>>>>>>>>>>>>hasAnyMobileCtnProfileWithEnabledIntegrateOneNet: ' + hasAnyMobileCtnProfileWithEnabledIntegrateOneNet);
            
            if(pc.Mobile_Scenario__c == 'OneBusiness' || pc.Mobile_Scenario__c == 'OneMobile'){
                needOneNetE.add(pc);
            }
            
            if(pc.Mobile_Scenario__c == CS_Constants.REDPRO_SCENARIO_NEW_PORTING || pc.Mobile_Scenario__c == CS_Constants.REDPRO_SCENARIO_RETENTION_MIGRATION){
                needOneNetX.add(pc);
            }
            
            if(pc.cscfga__Product_Definition__r.Name == 'One Net'){
                oneNet.add(pc);
            }
            
            if(pc.cscfga__Product_Definition__r.Name == 'Access Infrastructure' && pc.OneNet_Scenario__c == 'One Net Enterprise'){
                accessONE = true;
            }
            
            if(pc.cscfga__Product_Definition__r.Name == 'Access Infrastructure' && pc.OneNet_Scenario__c == 'One Net Express'){
                accessONX = true;
            }
        }
        
        system.debug('**** checkOneNet: needOneNetE.size() = ' + needOneNetE.size() + ' needOneNetX.size() = ' + needOneNetX.size()  + ' oneNet.size() = ' + oneNet.size());
        system.debug('**** checkOneNet: accessONE = ' + accessONE + ' accessONX = ' + accessONX);       
        
        if(needOneNetE.size() > 0){
            if(oneNet.size() > 0 && accessONE){
                oneError = false;
            } else if (hasAnyMobileCtnProfileWithEnabledIntegrateOneNet){
                oneError = true;
            }
        }  
        
        if (needOneNetX.size() > 0){
            if(oneNet.size() > 0 && accessONX){
                onxError = false;
            } else if (hasAnyMobileCtnProfileWithEnabledIntegrateOneNet) {
                onxError = true;
            }
        } 
        
        system.debug('**** checkOneNet: oneError = ' + oneError + ' onxError = ' + onxError);
        
        if(oneError || onxError){
            return true;
        } else {
            return false;
        }
    }
      
    //Rule 29 from Mobile Products_Business rules document
    private static Map<Boolean, String> checkBasicVPN(List<cscfga__Product_Configuration__c> configs){
        
        Map<Boolean, String> errorMessageBasicVPN = new Map<Boolean, String>();  

        Decimal totalCTNQuantity = 0;
        Decimal basciVPNQuantity = 0;
        
        for(cscfga__Product_Configuration__c pc : configs){
            if(pc.cscfga__Product_Definition__r.Name == 'Mobile CTN subscription' && pc.Mobile_Scenario__c != 'Data only'){
                totalCTNQuantity += pc.cscfga__Quantity__c;
            }
            
            if(pc.cscfga__Product_Definition__r.Name == 'Mobile BAN product' && pc.Add_On__c == 'Basic VPN'){
                basciVPNQuantity += pc.cscfga__Quantity__c;
            }
        }
        
        if(totalCTNQuantity != basciVPNQuantity && basciVPNQuantity > 0){
            errorMessageBasicVPN.put(false,'There are ' + totalCTNQuantity + ' voice CTNs and the Quantity of Basic VPN is ' + basciVPNQuantity + '. These values should be equal.');
        } else {
            errorMessageBasicVPN.put(true,'');
        }
        
        return errorMessageBasicVPN;
    }  
  
  
    private static Boolean checkCombinations(List<cscfga__Product_Configuration__c> configs){
      Boolean isValid = true;
      
      Map<String, List<Id>> scenarioConfig = new Map<String, List<Id>>();
      for(cscfga__Product_Configuration__c pc : configs){
          if(scenarioConfig.containsKey(pc.Mobile_Scenario__c)){
              scenarioConfig.get(pc.Mobile_Scenario__c).add(pc.Id);
          }
          else{
              List<Id> ids = new List<Id>();
              ids.add(pc.Id);
              scenarioConfig.put(pc.Mobile_Scenario__c, ids);
          }
      }
      
      //rules Do not allow sales to submit a quote with combinations between RedPro and OneMobile or OneBusiness and show error message
      //red pro cannot be sold with one business or onemobile row 11
      if((scenarioConfig.containsKey(CS_Constants.REDPRO_SCENARIO_NEW_PORTING) || scenarioConfig.containsKey(CS_Constants.REDPRO_SCENARIO_RETENTION_MIGRATION)) && (scenarioConfig.containsKey(Label.OneBusinessScenario) || scenarioConfig.containsKey(Label.OneMobileScenario) || scenarioConfig.containsKey(Label.OneBusinessIOTscenario))){
          isValid = false;
          //return isValid;
      }
      
      
      //row 7 Do not allow sales to submit a quote with combinations between OneBusiness and other mobile products  [i.e.OneBusiness IOT, OneMobile, RedPro, Data only (MBB Group Bundles)] and show error message 
       if(scenarioConfig.containsKey(Label.OneBusinessScenario) && (scenarioConfig.containsKey(Label.OneBusinessIOTscenario) || scenarioConfig.containsKey(Label.OneMobileScenario) || (scenarioConfig.containsKey(CS_Constants.REDPRO_SCENARIO_NEW_PORTING) || scenarioConfig.containsKey(CS_Constants.REDPRO_SCENARIO_RETENTION_MIGRATION)) || scenarioConfig.containsKey(Label.DataOnlyScenario))){
          isValid = false;
      }
      
      //row 8 Do not allow sales to submit a quote with combinations between OneBusiness IOT and other mobile products  [i.e.OneBusiness, OneMobile, RedPro, Data only (MBB Group Bundles)] and show error message 
       if(scenarioConfig.containsKey(Label.OneBusinessIOTscenario) && (scenarioConfig.containsKey(Label.OneMobileScenario) || scenarioConfig.containsKey(Label.OneBusinessScenario) || (scenarioConfig.containsKey(CS_Constants.REDPRO_SCENARIO_NEW_PORTING) || scenarioConfig.containsKey(CS_Constants.REDPRO_SCENARIO_RETENTION_MIGRATION)) || scenarioConfig.containsKey(Label.DataOnlyScenario))){
          isValid = false;
      }
      
      //row 10 Do not allow sales to submit a quote with combinations between RedPro and other mobile products  [i.e.OneBusiness, OneMobile, RedPro, Data only (MBB Group Bundles)] and show error message 
       if(scenarioConfig.containsKey(Label.OneMobileScenario) && (scenarioConfig.containsKey(Label.DataOnlyScenario) || scenarioConfig.containsKey(Label.OneBusinessScenario)  || scenarioConfig.containsKey(Label.OneBusinessIOTscenario))){
          isValid = false;
      }
      
      //row 9 Do not allow sales to submit a quote with combinations between RedPro and other mobile products  [i.e.OneBusiness, OneMobile, RedPro, Data only (MBB Group Bundles)] and show error message 
       if(scenarioConfig.containsKey(Label.DataOnlyScenario) && (scenarioConfig.containsKey(Label.OneMobileScenario) || scenarioConfig.containsKey(Label.OneBusinessScenario)  || scenarioConfig.containsKey(Label.OneBusinessIOTscenario))){
          isValid = false;
      }
      
      return isValid;
    }
    
    public Boolean checkVoiceServicesOnAllProfileCTN (String basketId) {
        List<cscfga__Product_Configuration__c> pcList = [SELECT Id, cscfga__Product_Basket__c, cscfga__Product_Definition__r.Name FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Definition__r.Name = 'Mobile CTN profile' and cscfga__Product_Basket__c = :basketId];        
        List<cscfga__Attribute__c> attrList = [SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c IN :pcList AND Name = 'Voice Services Needed'];
        
        Integer numberOfEnabledVoiceService = 0;
        Integer numberOfNotEnabledVoiceService = 0;
        
        for(cscfga__Product_Configuration__c pc : pcList) {
            cscfga__Attribute__c attr = getAttributePerName(pc.Id, 'Voice Services Needed', attrList);
            
            if (attr != null && attr.cscfga__Value__c != null && attr.cscfga__Value__c == 'Yes'){
                numberOfEnabledVoiceService++;
            } else if (attr != null && attr.cscfga__Value__c != null && attr.cscfga__Value__c == 'No') {
                numberOfNotEnabledVoiceService++;
            } 
        }

        if(numberOfEnabledVoiceService > 0 && numberOfNotEnabledVoiceService > 0) {
            return false;
        }
        
        return true;
    }
    
    private cscfga__Attribute__c getAttributePerName (Id configId, String attributeName,List<cscfga__Attribute__c> attributesFromRefConfig) {
        for (cscfga__Attribute__c attr : attributesFromRefConfig) {
            if ((attr.Name == attributeName) && (attr.cscfga__Product_Configuration__c == configId)) {
                return attr;
            }
        }
        
        return null;
    }
    
    private Boolean checkOneFixedCodecs(List<cscfga__Product_Configuration__c> configs){
        Set<String> codecs = new Set<String>();
        
        for(cscfga__Product_Configuration__c pc : configs){
            if(pc.cscfga__Product_Definition__r.Name == 'Access Infrastructure' && pc.Codec__c != '' && pc.Codec__c != null)
            codecs.add(pc.Codec__c);
        }

        if(codecs.size() > 1){
            return true;
        } else{
            return false; 
        }
    }
    
    public String performAction (String basketId) {
        
        String action = '';
        String validationResult = '';
        String newUrl = '';
        Boolean syncResult = false;
        Matcher m = null;
        
        try {

            cscfga__Product_Basket__c pbForSubmit = [select id, Total_Number_of_Expected_Locations__c,Error_message__c, cscfga__Basket_Status__c, Primary__c, Used_Snapshot_Objects__c  from cscfga__Product_Basket__c where id = :basketId];         
            Map<Id,List<cssmgnt.ProductProcessingUtility.Component>> oeData = CustomButtonSynchronizeWithOpportunity.retrieveOeMap(basketId);
            createSnapshotRecords(pbForSubmit);
            
            Boolean voiceServiceCheckPassed = checkVoiceServicesOnAllProfileCTN(basketId);
            
            if (voiceServiceCheckPassed == false) {
                return '{"status":"error","title":"Error","text":"If one Mobile CTN profile has Voice Service enabled then all of them have to have Voice Service enabled."}';
            }
            
            if(pbForSubmit.Total_Number_of_Expected_Locations__c!=null){
                return errorMsgTotalNumberExpectedLocations;
            }
            
            //Basket validatin - mobile
            //AN - mobile basket validation - checks Mobile Scenario fields, please add other fields if needed
            
            //NC - can we remove the where Mobile_Scenario__c != null part so that this is the only place where we select PC's?
            //List<cscfga__Product_Configuration__c> pcList = [select Id, Mobile_Scenario__c, cscfga__Product_Basket__c, CTNQuantity__c, Add_On__c,cscfga__Quantity__c, cscfga__Product_Definition__r.Name from cscfga__Product_Configuration__c where Mobile_Scenario__c != null and cscfga__Product_Basket__c = :basketId];
            List<cscfga__Product_Configuration__c> pcList = [select Id, Mobile_Scenario__c, Codec__c,Deal_type__c, OneNet_Scenario__c, cscfga__Product_Basket__c, CTNQuantity__c, Add_On__c,cscfga__Quantity__c, cscfga__Product_Definition__r.Name from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId];
            
            //check for OneNet if - if OneMobile, OneBusiness or RedPro are in basket then a One Net configuration is required
            Boolean oneNetMissing = checkOneNet(pcList);
            
            if(oneNetMissing){
                return '{"status":"error","title":"Error","text":"Mobile configurations require a One Net configuration for integration!"}';
            }
            //end check for OneNet 
            
            //check One Fixed Codecs - only one Codec is allowed in single Basket
            Boolean multipleCodecs = checkOneFixedCodecs(pcList);
            
            if(multipleCodecs){
                return '{"status":"error","title":"Error","text":"There are several One Fixed Codecs inside this Basket! Make sure only one Codec is used in Basket!"}';
            }
            //end One Fixed Codecs
            
            Boolean mobileRulesValid = checkCombinations(pcList);
            
            if(!mobileRulesValid){
                return '{"status":"error","title":"Error","text":"'+Label.MobileCombinationErrorMessage+'"}';
            }
            
            Map<Boolean,String> errorMessageBasicVPN = checkBasicVPN(pcList);
            if(errorMessageBasicVPN.get(false) != null && errorMessageBasicVPN.get(false) != ''){
                return '{"status":"error","title":"Basic VPN Error","text":"' + errorMessageBasicVPN.get(false) + '"}';
            }
            
            String reqExp = '^/'+ '\\' +'w{15,18}';
            Pattern ptr = Pattern.compile(reqExp);
            
            if(pbForSubmit.Primary__c){
                newUrl = CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(basketId, oeData);
                m = ptr.matcher(newUrl);
                syncResult = true;//m.matches() && m.hitEnd();
            } else {
                return '{"status":"error","title":"Error","text":"Only Primary baskets can be synched!"}';
            }       
            
            validationResult = validateBasket(pbForSubmit);
            
            if(validationResult.containsIgnoreCase('"status":"error"')){
                action = validationResult;
            } else {
                
                
                if(syncResult){
                    
                    Map<String, String> parameter = new Map<String, String>();
                    parameter.put('recordId',basketId);
                    try{
                        pbForSubmit.Basket_Approval_Status__c = 'Submitted';
                        //update pbForSubmit;
                    }
                    catch(Exception e){
                        // no problem if this fails. In that case the batch job has already finished.;
                    }
                    try{
                        
                        if(Test.isRunningTest()){
                            System.debug('Running test');
                        }
                        else{
                            //appro.ApprovalUtilsController.apexApprovalSubmit(parameter);
                            String submitUrl = CustomButtonPartnerValidateAndSubmit.redirectToSubmitForApproval(basketId);
                            return '{"status":"ok","redirectURL":"' + submitUrl + '"}';                            
                        }
                        //CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(basketId);
                        return '{"status":"ok","text":"Synced successfully. Basket submitted for approval"}';
                    }
                    catch(Exception e){
                        return '{"status":"error","title":"Error","text":"'+e.getMessage()+'"}';
                    }
                  
                    
                } else {
                    System.debug('****NO SYNC RESULT');
                    action = '{"status":"error","title":"Error","text":"Basket validated but not synchronized with opportunity!"}';
                    
                }
                
            }
        } catch(Exception e) {
            System.debug('----Errror---- 2 '+e.getMessage() );
            action ='{"status":"error","title":"Error","text":"' + e.getMessage() + '"}';
        }
        return action;
    }
    
    private String validateBasket(cscfga__Product_Basket__c basket){
        String primarySecondaryErrors = checkPrimarySecondary(basket.Id);
    
        System.debug('PRIMARY SECONDARY ERRORS == '+primarySecondaryErrors);
        System.debug('basket.Error_message__c == '+basket.Error_message__c);
        if(primarySecondaryErrors != ''){
            return '{"status":"error","text":"' + primarySecondaryErrors + '"}';
        }else if(basket.cscfga__Basket_Status__c!='Valid'){
            return  errorMsgStatus; 
        }
        else if(basket.Error_message__c!=null){
            String errorMessage = basket.Error_message__c;
            return '{"status":"error","title":"Error","text":"' + errorMessage + '"}';
        } else {
            return '{"status":"ok","text":"Basket validated successfully"}';
        }
    }
    
    
    
    private void createSnapshotRecords(cscfga__Product_Basket__c basket){
        /*
        List<CS_Basket_Snapshot_Transactional__c> oldData = [SELECT Id from CS_Basket_Snapshot_Transactional__c where Product_Basket__c = :basket.Id];
        if(oldData.size() > 0){
            SharingUtils.deleteRecordsWithoutSharing(oldData);
            
        }
        */
        if((basket.Used_Snapshot_Objects__c == '') ||(basket.Used_Snapshot_Objects__c == null)||(basket.Used_Snapshot_Objects__c != '[CS_Basket_Snapshot_Transactional__c]')){
            basket.Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]';
            update basket;
        }
        CS_BasketSnapshotManager.TakeBasketSnapshot(basket, true);
        
    }
    
    private String checkPrimarySecondary (String basketId){
        List<cscfga__Attribute__c> primarySecondaryAttrs = [SELECT cscfga__Value__c, Name, cscfga__Product_Configuration__c from cscfga__Attribute__c where cscfga__Product_Configuration__c in (SELECT Id from cscfga__Product_Configuration__c where Secondary__c = true and cscfga__Product_Basket__c = :basketId) AND Name = 'Primary'];
        //System.debug('primaries='+primarySecondaryAttrs);
        
        Set<String> primaries = new Set<String>();
        Set<String> problemConfigs = new Set<String>();
        Set<String> problemVal = new Set<String>();
    
        Boolean hasDuplicates = false;
    
        String problemMessage = '';
    
        Map<String, List<String>> configsPerPrimary = new Map<String, List<String>>();
        for(cscfga__Attribute__c attr: primarySecondaryAttrs){
            if(configsPerPrimary.keyset().contains(attr.cscfga__Value__c)){
                hasDuplicates = true;
                configsPerPrimary.get(attr.cscfga__Value__c).add(attr.cscfga__Product_Configuration__c);
            }
            else{
                List<String> listForMap = new List<String>();
                listForMap.add(attr.cscfga__Product_Configuration__c);
                configsPerPrimary.put(attr.cscfga__Value__c,listForMap);
            }
        }
    
        if(hasDuplicates){
    
        }
    
        for (String primary : configsPerPrimary.keySet()){
            if(configsPerPrimary.get(primary).size() > 1){
                problemConfigs.addAll(configsPerPrimary.get(primary));
            }
        }
    
        if(problemConfigs.size()>0){
            Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>([Select Site_Name__c, Id, Name from cscfga__Product_Configuration__c where Id in :problemConfigs]);
            for (String primary : configsPerPrimary.keySet()){
                if(configsPerPrimary.get(primary).size() > 1){
                    problemMessage+='Same primary configuration found in ';
    
                    for(String id : configsPerPrimary.get(primary)){
                        problemMessage+=configs.get(id).Site_Name__c+'('+configs.get(id).Name+'), ';
                    }
                    problemMessage = problemMessage.substring(0, problemMessage.length()-2) + '. ';
                }
            }
        }
    
        //problemMessage = problemMessage.substring(0, problemMessage.length()-2) + '.';
    
        return problemMessage;
        
         
      }    
 
     // Set url for redirect after action
    public static String redirectToSubmitForApproval(String basketId)
    {   
    
        PageReference editPage;
//        String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
//        system.debug(baseUrl);
        editPage= new PageReference('/apex/appro__VFSubmitForApproval?id='+basketId);
        editPage.setRedirect(true);
        system.debug(editpage.getUrl());
        //PageReference editPage = new PageReference('/apex/appro__VFSubmitForApproval?id='+basketId);
        return editPage.getUrl();
    }     
  
}