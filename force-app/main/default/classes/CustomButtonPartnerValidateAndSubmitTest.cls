@isTest
public class CustomButtonPartnerValidateAndSubmitTest {

  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        System.runAs (simpleUser) { 
      
     
        Framework__c frameworkSetting = new Framework__c();
        frameworkSetting.Framework_Sequence_Number__c = 2;
        insert frameworkSetting;
      
        PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
        insert priceResetSetting;
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;
      
      
      
        Sales_Settings__c ssettings = new Sales_Settings__c();
        ssettings.Postalcode_check_validity_days__c = 2;
        ssettings.Max_Daily_Postalcode_Checks__c = 2;
        ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
        ssettings.Postalcode_check_block_period_days__c = 2;
        ssettings.Max_weekly_postalcode_checks__c = 15;
        insert ssettings;
      
        Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
        insert testOpp;
        
        
        cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        basket.Primary__c = true;
        basket.cscfga__Basket_Status__c = 'Valid';
        insert basket;
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        Id packageDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Package Definition').getRecordTypeId(); 
        
        
        //one net
        cscfga__Product_Definition__c oneNetDef = CS_DataTest.createProductDefinition('One Net');
        oneNetDef.Product_Type__c = 'Fixed';
        oneNetDef.RecordTypeId = productDefinitionRecordType;
        insert oneNetDef;
        /*
        cscfga__Product_Configuration__c oneNetConf = CS_DataTest.createProductConfiguration(oneNetDef.Id, 'One Net',basket.Id);
        oneNetConf.cscfga__Root_Configuration__c = null;
        oneNetConf.cscfga__Parent_Configuration__c = null;
        oneNetConf.cscfga__Product_Basket__c = basket.Id;
        insert oneNetConf;
        */
        Test.startTest();
        
        //Company Level Fixed Voice
        cscfga__Product_Definition__c clfvDef = CS_DataTest.createProductDefinition('Company Level Fixed Voice');
        clfvDef.Product_Type__c = 'Fixed';
        clfvDef.RecordTypeId = productDefinitionRecordType;
        clfvDef.Name = 'Company Level Fixed Voice';
        insert clfvDef;
        
        cscfga__Product_Configuration__c clfvConf = CS_DataTest.createProductConfiguration(clfvDef.Id, 'Company Level Fixed Voice',basket.Id);
        clfvConf.cscfga__Root_Configuration__c = null;
        clfvConf.cscfga__Parent_Configuration__c = null;
        clfvConf.OneNet_Scenario__c = 'One Net Express';
        clfvConf.cscfga__Product_Basket__c = basket.Id;
        insert clfvConf;
        
        //access infra
        cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        accessInfraDef.Product_Type__c = 'Fixed';
        accessInfraDef.RecordTypeId = productDefinitionRecordType;
        accessInfraDef.Name = 'Access Infrastructure';
        insert accessInfraDef;
        
        cscfga__Product_Configuration__c accessInfraConf = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        accessInfraConf.cscfga__Root_Configuration__c = null;
        accessInfraConf.cscfga__Parent_Configuration__c = null;
        accessInfraConf.OneNet_Scenario__c = 'One Net Express';
        accessInfraConf.cscfga__Product_Basket__c = basket.Id;
        insert accessInfraConf;
        
        cscfga__Product_Configuration__c accessInfraConfE = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        accessInfraConfE.cscfga__Root_Configuration__c = null;
        accessInfraConfE.cscfga__Parent_Configuration__c = null;
        accessInfraConfE.OneNet_Scenario__c = 'One Net Enterprise';
        accessInfraConfE.cscfga__Product_Basket__c = basket.Id;
        insert accessInfraConfE;
        
        //Mobile checks
        cscfga__Product_Definition__c mobileDef = CS_DataTest.createProductDefinition('Mobile CTN Profile');
        mobileDef.Product_Type__c = 'Mobile';
        mobileDef.RecordTypeId = productDefinitionRecordType;
        insert mobileDef;
        
        cscfga__Product_Configuration__c mobileConfOB = CS_DataTest.createProductConfiguration(mobileDef.Id, 'Mobile CTN Profile',basket.Id);
        mobileConfOB.cscfga__Root_Configuration__c = null;
        mobileConfOB.cscfga__Parent_Configuration__c = null;
        mobileConfOB.cscfga__Parent_Configuration__c = null;
        mobileConfOB.Mobile_Scenario__c = 'OneBusiness';
        mobileConfOB.cscfga__Product_Basket__c = basket.Id;
        insert mobileConfOB;
        
        cscfga__Attribute_Definition__c integrateOneNetAttDef = new cscfga__Attribute_Definition__c();
        integrateOneNetAttDef.cscfga__Type__c = 'Calculation';
        integrateOneNetAttDef.cscfga__Data_Type__c = 'String';
        integrateOneNetAttDef.Name = 'IntegrateOneNet';
        integrateOneNetAttDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert integrateOneNetAttDef;
        
        cscfga__Attribute__c integrateOneNetAtt = new cscfga__Attribute__c();
        integrateOneNetAtt.Name = 'IntegrateOneNet';
        integrateOneNetAtt.cscfga__Value__c = 'Yes';
        integrateOneNetAtt.cscfga__Product_Configuration__c = mobileConfOB.Id;
        insert integrateOneNetAtt;
        
        
        cscfga__Product_Definition__c mobileCTNSubsDef = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
        mobileCTNSubsDef.Product_Type__c = 'Mobile';
        mobileCTNSubsDef.RecordTypeId = productDefinitionRecordType;
        insert mobileCTNSubsDef;
        
        cscfga__Product_Configuration__c mobileCTNSubsConf = CS_DataTest.createProductConfiguration(mobileCTNSubsDef.Id, 'Mobile CTN Subscription',basket.Id);
        mobileCTNSubsConf.cscfga__Root_Configuration__c = null;
        mobileCTNSubsConf.cscfga__Parent_Configuration__c = null;
        mobileCTNSubsConf.cscfga__Parent_Configuration__c = null;
        mobileCTNSubsConf.Mobile_Scenario__c = 'OneBusiness';
        mobileCTNSubsConf.cscfga__Product_Basket__c = basket.Id;
        insert mobileCTNSubsConf;
        
        cscfga__Product_Definition__c mobileBANDef = CS_DataTest.createProductDefinition('Mobile BAN Product');
        mobileBANDef.Product_Type__c = 'Mobile';
        mobileBANDef.RecordTypeId = productDefinitionRecordType;
        insert mobileBANDef;
        
        cscfga__Product_Configuration__c mobileConfBAN = CS_DataTest.createProductConfiguration(mobileBANDef.Id, 'Mobile BAN Product',basket.Id);
        mobileConfBAN.cscfga__Root_Configuration__c = null;
        mobileConfBAN.cscfga__Parent_Configuration__c = null;
        mobileConfBAN.cscfga__Parent_Configuration__c = null;
        mobileConfBAN.Add_On__c = 'Basic VPN';
        mobileConfBAN.cscfga__Product_Basket__c = basket.Id;
        insert mobileConfBAN;
        
        
        /*
        cscfga__Product_Configuration__c mobileConfRP = CS_DataTest.createProductConfiguration(mobileDef.Id, 'Mobile CTN Profile',basket.Id);
        mobileConfRP.cscfga__Root_Configuration__c = null;
        mobileConfRP.cscfga__Parent_Configuration__c = null;
        mobileConfRP.cscfga__Parent_Configuration__c = null;
        mobileConfRP.Mobile_Scenario__c = 'RedPro';
        mobileConfRP.cscfga__Product_Basket__c = basket.Id;
        insert mobileConfRP;
        
        cscfga__Product_Configuration__c mobileConfIOT = CS_DataTest.createProductConfiguration(mobileDef.Id, 'Mobile CTN Profile',basket.Id);
        mobileConfIOT.cscfga__Root_Configuration__c = null;
        mobileConfIOT.cscfga__Parent_Configuration__c = null;
        mobileConfIOT.cscfga__Parent_Configuration__c = null;
        mobileConfIOT.Mobile_Scenario__c = 'OneBusiness IOT';
        mobileConfIOT.cscfga__Product_Basket__c = basket.Id;
        insert mobileConfIOT;
        
        cscfga__Product_Configuration__c mobileConfOM = CS_DataTest.createProductConfiguration(mobileDef.Id, 'Mobile CTN Profile',basket.Id);
        mobileConfOM.cscfga__Root_Configuration__c = null;
        mobileConfOM.cscfga__Parent_Configuration__c = null;
        mobileConfOM.cscfga__Parent_Configuration__c = null;
        mobileConfOM.Mobile_Scenario__c = 'OneMobile';
        mobileConfOM.cscfga__Product_Basket__c = basket.Id;
        insert mobileConfOM;
        
        cscfga__Product_Configuration__c mobileConfDO = CS_DataTest.createProductConfiguration(mobileDef.Id, 'Mobile CTN Profile',basket.Id);
        mobileConfDO.cscfga__Root_Configuration__c = null;
        mobileConfDO.cscfga__Parent_Configuration__c = null;
        mobileConfDO.cscfga__Parent_Configuration__c = null;
        mobileConfDO.Mobile_Scenario__c = 'Data only';
        mobileConfDO.cscfga__Product_Basket__c = basket.Id;
        insert mobileConfDO;
       */
        cscfga__Product_Configuration__c oneNetConf = CS_DataTest.createProductConfiguration(oneNetDef.Id, 'One Net',basket.Id);
        oneNetConf.cscfga__Root_Configuration__c = null;
        oneNetConf.cscfga__Parent_Configuration__c = null;
        oneNetConf.cscfga__Product_Basket__c = basket.Id;
        insert oneNetConf;
        
        
        CustomButtonPartnerValidateAndSubmit button = new CustomButtonPartnerValidateAndSubmit();
        
        cscfga__Attribute_Definition__c voiceServiceNeededDef = new cscfga__Attribute_Definition__c();
        voiceServiceNeededDef.cscfga__Type__c = 'Calculation';
        voiceServiceNeededDef.cscfga__Data_Type__c = 'String';
        voiceServiceNeededDef.Name = 'Voice Services Needed';
        voiceServiceNeededDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert voiceServiceNeededDef;
        
        cscfga__Attribute__c voiceServiceNeededAtt = new cscfga__Attribute__c();
        voiceServiceNeededAtt.Name = 'Voice Services Needed';
        voiceServiceNeededAtt.cscfga__Value__c = 'Yes';
        voiceServiceNeededAtt.cscfga__Product_Configuration__c = mobileConfOB.Id;
        insert voiceServiceNeededAtt;
        /*
        cscfga__Attribute__c voiceServiceNotNeededAtt = new cscfga__Attribute__c();
        voiceServiceNotNeededAtt.Name = 'Voice Services Needed';
        voiceServiceNotNeededAtt.cscfga__Value__c = 'Yes';
        voiceServiceNotNeededAtt.cscfga__Product_Configuration__c = mobileConfDO.Id;
        insert voiceServiceNotNeededAtt;
        */
        
        
        button.performAction(String.valueOf(basket.Id));
        
        
        Test.stopTest();
     }
  }
  
  
    private static testMethod void testErrors() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        System.runAs (simpleUser) { 
      
     
        Framework__c frameworkSetting = new Framework__c();
        frameworkSetting.Framework_Sequence_Number__c = 2;
        insert frameworkSetting;
      
        PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
        insert priceResetSetting;
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;
        
        Sales_Settings__c ssettings = new Sales_Settings__c();
        ssettings.Postalcode_check_validity_days__c = 2;
        ssettings.Max_Daily_Postalcode_Checks__c = 2;
        ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
        ssettings.Postalcode_check_block_period_days__c = 2;
        ssettings.Max_weekly_postalcode_checks__c = 15;
        insert ssettings;
      
        Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
        insert testOpp;
        
        
        cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        basket.Primary__c = true;
        basket.cscfga__Basket_Status__c = 'Valid';
        insert basket;
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        Id packageDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Package Definition').getRecordTypeId(); 
        
        
        
        Test.startTest();
        //access infra
        cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        accessInfraDef.Product_Type__c = 'Fixed';
        accessInfraDef.RecordTypeId = productDefinitionRecordType;
        accessInfraDef.Name = 'Access Infrastructure';
        insert accessInfraDef;
        
        cscfga__Product_Configuration__c accessInfraConf = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        accessInfraConf.cscfga__Root_Configuration__c = null;
        accessInfraConf.cscfga__Parent_Configuration__c = null;
        accessInfraConf.cscfga__Product_Basket__c = basket.Id;
        insert accessInfraConf;
        
       
        CustomButtonPartnerValidateAndSubmit button = new CustomButtonPartnerValidateAndSubmit();
        
        //2 secondaries on same primary
        cscfga__Product_Configuration__c primaryAccessConf = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        primaryAccessConf.cscfga__Root_Configuration__c = null;
        primaryAccessConf.cscfga__Parent_Configuration__c = null;
        primaryAccessConf.Primary__c = true;
        primaryAccessConf.cscfga__Product_Basket__c = basket.Id;
        insert primaryAccessConf;
        
        cscfga__Product_Configuration__c secondaryAccessConf1 = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        secondaryAccessConf1.cscfga__Root_Configuration__c = null;
        secondaryAccessConf1.cscfga__Parent_Configuration__c = null;
        secondaryAccessConf1.Secondary__c = true;
        secondaryAccessConf1.cscfga__Product_Basket__c = basket.Id;
        insert secondaryAccessConf1;
        
        cscfga__Product_Configuration__c secondaryAccessConf2 = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        secondaryAccessConf2.cscfga__Root_Configuration__c = null;
        secondaryAccessConf2.cscfga__Parent_Configuration__c = null;
        secondaryAccessConf2.Secondary__c = true;
        secondaryAccessConf2.cscfga__Product_Basket__c = basket.Id;
        insert secondaryAccessConf2;
        
        cscfga__Attribute_Definition__c primaryAttDef = new cscfga__Attribute_Definition__c();
        primaryAttDef.cscfga__Type__c = 'Lookup';
        primaryAttDef.cscfga__Data_Type__c = 'String';
        primaryAttDef.Name = 'Primary';
        primaryAttDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert primaryAttDef;
        
        cscfga__Attribute__c primaryAttConf1 = new cscfga__Attribute__c();
        primaryAttConf1.Name = 'Primary';
        primaryAttConf1.cscfga__Value__c = String.valueOf(primaryAccessConf.Id);
        primaryAttConf1.cscfga__Product_Configuration__c = secondaryAccessConf1.Id;
        insert primaryAttConf1;
        
        cscfga__Attribute__c primaryAttConf2 = new cscfga__Attribute__c();
        primaryAttConf2.Name = 'Primary';
        primaryAttConf2.cscfga__Value__c = String.valueOf(primaryAccessConf.Id);
        primaryAttConf2.cscfga__Product_Configuration__c = secondaryAccessConf2.Id;
        insert primaryAttConf2;
        button.performAction(String.valueOf(basket.Id));
        Test.stopTest();
     }
  }
  
  
  

}