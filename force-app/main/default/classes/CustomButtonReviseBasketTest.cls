@isTest
private class CustomButtonReviseBasketTest {

	private static testMethod void test() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs (simpleUser) {
            Test.startTest();

            No_Triggers__c notriggers = new No_Triggers__c();
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;

            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;

            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
            insert tmpOpp;

            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
            tmpProductBasket.cscfga__Basket_Status__c = 'Approved';
            insert tmpProductBasket;

            List<NetProfit_CTN__c> initCtns = CreateMobileInformationController.getNetProfitCtn(tmpOpp.Id, 'Quote_Profile__c', 'asc');
            CreateMobileInformationController.insertNetProfitCtn(initCtns, tmpOpp.Id);

            cscfga__Product_Basket__c tmpProductBasket2 = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
            tmpProductBasket2.cscfga__Basket_Status__c = 'Pending approval';
            insert tmpProductBasket2;

            CustomButtonReviseBasket reviseBasket = new CustomButtonReviseBasket();
            reviseBasket.performAction((String)tmpProductBasket.Id);
            reviseBasket.performAction((String)tmpProductBasket2.Id);

            Test.stopTest();
        }
	}

}