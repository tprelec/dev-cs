/**
* Used for the Product Basket 'Sync' with opportunity button'
* 
* @author Tomislav Blazek
* @ticket SFDT-164 - fixed the bug in Sync button
* @since  29/1/2016
*/
global with sharing class CustomButtonSynchWithOpportunityZiggo extends csbb.CustomButtonExt {
    
    public static final String errorMsg = '{"status":"error","title":"Error",'
                                            + '"text":"Basket must be in status Valid."}';
    
    //CATGOV-830 start
    public static final String validationMessageForBothNull = '{"status":"error","title":"Error",'
                                            + '"text":"Please populate the Mobile Competitor Contract End Date or the Mobile Contract End Date Unknown checkbox if the date is unknown"}';
                                            
    public static final String validationMessageForBothFilled = '{"status":"error","title":"Error",'
                                            + '"text":"Please only populate either one of the fields Mobile Competitor Contract End Date or the Mobile Contract End Date Unknown"}';        
    //CATGOV-830 end
    
    public static final String validationMessageForCreationOfOLIs = '{"status":"error","title":"Error",'
                                            + '"text":"Delete and Create OLIs method failed"}'; 
    
    //Added as part of BU-18 -for preventing syncing of archived configurations
    public static final String validationMsg = '{"status":"error","title":"Error",'
                                            + '"text":"Basket created prior to November 4th Product Change is invalid and needs to re-created."}';
  
    public String performAction (String basketId) {
        //CATGOV-830 start
        Set<Id> accId = new Set<Id>();
       //CATGOV-830 end
        String action = '';
        
        cscfga__Product_Basket__c basket = [SELECT Id, cscfga__Basket_Status__c,
                                              csordtelcoa__Synchronised_with_Opportunity__c,
                                              csbb__Synchronised_With_Opportunity__c,
                                              cscfga__Opportunity__r.Id,cscfga__Products_In_Basket__c,csbb__Account__c,cscfga__Opportunity__r.AccountId,LG_MobileCompetitorContractEndDate__c,LG_Mobile_Contract_End_Date_Unknown__c,LG_RunningUserSalesChannel__c
                                              FROM cscfga__Product_Basket__c 
                                              WHERE Id = :basketId];
        
        //CATGOV-830 start
        system.debug('comp enddate'+ basket.LG_MobileCompetitorContractEndDate__c);
        system.debug('comp date unknown'+ basket.LG_Mobile_Contract_End_Date_Unknown__c);
        system.debug('validation' + validationMessageForBothNull );
        //CATGOV-830 end
        
        if ('Valid'.equals(basket.cscfga__Basket_Status__c)) {
            if(basket.cscfga__Products_In_Basket__c!=null && (basket.cscfga__Products_In_Basket__c.containsIgnoreCase('Archived')||basket.cscfga__Products_In_Basket__c.containsIgnoreCase('Connect MKB')||basket.cscfga__Products_In_Basket__c.containsIgnoreCase('Connect ZZP'))) {
                 action = validationMsg;
                 //CATGOV-830 start
            } 
            else if ((basket.LG_MobileCompetitorContractEndDate__c == null) && (basket.LG_Mobile_Contract_End_Date_Unknown__c == false) && (basket.LG_RunningUserSalesChannel__c == 'D2D')) {
                 action = validationMessageForBothNull;   
            } 
            else if ((basket.LG_MobileCompetitorContractEndDate__c != null) && (basket.LG_Mobile_Contract_End_Date_Unknown__c == true) && (basket.LG_RunningUserSalesChannel__c == 'D2D')) {
                 action = validationMessageForBothFilled; 
                 //CATGOV-830 end
            }
            else {
                Opportunity opp = basket.cscfga__Opportunity__r;
                //de-sync first and then synce again
                if (basket.csordtelcoa__Synchronised_with_Opportunity__c
                    || basket.csbb__Synchronised_With_Opportunity__c) {
                    basket.csordtelcoa__Synchronised_with_Opportunity__c = false;
                    basket.csbb__Synchronised_With_Opportunity__c = false;
                    update basket;
                }
                
                //CATGOV-830 start
                //accId.add(basket.csbb__Account__c); 
                accId.add(basket.cscfga__Opportunity__r.AccountId);
                system.debug('account'+ basket.csbb__Account__c);
                System.debug('AccId : '+accId);
                //CATGOV-830 end
                
                basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
                basket.csbb__Synchronised_With_Opportunity__c = true;
             
                update basket;
                //CATGOV-830 start if condition
                
                if(deleteAndCreateOLIs(basket.Id) == false) {
                    action = validationMessageForCreationOfOLIs;
                    return action;
                }
               
                Account acc = [Select id, LG_MobileCompetitorContractEndDate__c from Account where id =: accId ];
                System.debug('Acc : '+acc);
                
                if((basket.LG_MobileCompetitorContractEndDate__c != acc.LG_MobileCompetitorContractEndDate__c) && (basket.LG_MobileCompetitorContractEndDate__c != null)) {
                    acc.LG_MobileCompetitorContractEndDate__c = basket.LG_MobileCompetitorContractEndDate__c;
                    update acc;
                }
    
                //System.debug('Account field updated!!');     
                //CATGOV-830 end       
                      
                PageReference oppPage = new ApexPages.StandardController(opp).view();
                action =  '{"status":"ok","redirectURL":"' + oppPage.getUrl() + '"}';   
            }
        }
        else {
            action = errorMsg;
        }
        
        return action;
    }
    
    public Boolean deleteAndCreateOLIs(Id basketId) {
        // ND added 21.01.2020 - Addition of OLIs
        Set<String> basketSet = new Set<String>();
        basketSet.add(basketId);
        try {
            LG_ProductUtility.DeleteHardOLIs(basketSet);
            LG_ProductDetailsUtility.DeleteProductDetails(basketSet);
            LG_ProductUtility.CreateOLIs(basketSet);
            LG_ProductDetailsUtility.CreateProductDetails(basketSet);
        } catch (Exception ex) {
            System.debug('ERROR: Delete and Create OLIs method failed for basket id: ' + basketId);
            return false;
        }
        return true;
    }
}