@isTest
public class CustomButtonSyncheWithOppTest {

    private static testMethod void test() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) { 
            Test.startTest();
            
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;
            
            No_Triggers__c notriggers = new No_Triggers__c();
            //notriggers.Location__c = 'User';
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;

            Framework__c frameworkSetting = new Framework__c();
            frameworkSetting.Framework_Sequence_Number__c = 2;
            insert frameworkSetting;
            
            PriceReset__c priceResetSetting = new PriceReset__c();
           
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
               
           insert priceResetSetting;
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;
            
            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;
        
            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
            insert tmpOpp;
            
            cscfga__Product_Definition__c tmpProductDef = CS_DataTest.createProductDefinition('Test Product Definition');
            tmpProductDef.Product_Type__c = 'Fixed';
            insert tmpProductDef;

            cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('OneMobile');
            prodDefinition.Product_Type__c = 'Mobile';
            insert prodDefinition;
            
            List<cscfga__Attribute_Definition__c> tmpAttDefList = new List<cscfga__Attribute_Definition__c>();
            tmpAttDefList.add(CS_DataTest.createAttributeDefinition('Recurring Price Basic', tmpProductDef, 'Display Value', 'Double'));
            tmpAttDefList.add(CS_DataTest.createAttributeDefinition('One Off Price Basic', tmpProductDef, 'Display Value', 'Double'));
            tmpAttDefList.add(CS_DataTest.createAttributeDefinition('Recurring Price Other', tmpProductDef, 'Display Value', 'Double'));
            tmpAttDefList.add(CS_DataTest.createAttributeDefinition('One Off Price Other', tmpProductDef, 'Display Value', 'Double'));
            
            insert tmpAttDefList;
            
            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
            tmpProductBasket.ProfitLoss_JSON__c = '{"KPI Direct Capex / Net Revenue":"0,00","KPI Payback Period(Months)":"0,00","KPI NPV":"0,00",' +
                    '"KPI Net Incremental Billed Revenue":"0,00","KPI Free cash flow / Net revenue":"1,50","KPI EBITDA / Net revenue":"1,56",' +
                    '"KPI Sales Margin / Net revenue":"1,71","NET RESULT":"56.993,07","NET RESULT first":"58.996,33","Capex":"1.228,43","Network Depreciation":"774,83",' +
                    '"Direct Capex":"0,00","EBITDA":"58.996,33","EBITDA first":"62.203,26","Overhead allocation":"3.206,93","Network Opex":"2.819,96",' +
                    '"Incremental EBITDA":"65.023,22","Incremental OPEX":"0,00","Total Gross Margin":"65.023,22","Total costs of sales":"10.816,78","A&R":"0,00",' +
                    '"Revenue share":"0,00","Other Costs":"0,00","Opportunity cost":"0,00","Opportunity cost Fixed":"0,00","Opportunity cost Mobile":"0,00",' +
                    '"Total cost of sales":"10.816,78","Total Net Revenue":"75.840,00","Total discounts":"0,00","Benchmark":"0,00",' +
                    '"Total credit amount / loyalty bonus":"0,00","Benchmark Fixed":"0,00","Benchmark Mobile":"0,00",' +
                    '"Operator other costs / other migration costs":"0,00","Operator other costs / other migration costs Fixed":"0,00",' +
                    '"Operator other costs / other migration costs Mobile":"0,00","Discounts":"0,00","Total Gross Revenue (SUM)":"75.840,00",' +
                    '"Total Gross Revenue for Operator other costs / other migration costs":"0,00","Total Gross Revenue":"75.840,00"}';
            insert tmpProductBasket;
            
            OrderType__c orderType = CS_DataTest.createOrderType('MAC');
            insert orderType;
      
            List<Product2> tmpProduct2List = new List<Product2>();
            tmpProduct2List.add(CS_DataTest.createProduct2('Recurring Price Basic','Test Product Definition', orderType, true));
            tmpProduct2List.add(CS_DataTest.createProduct2('One Off Price Basic','Test Product Definition', orderType, true));
            insert tmpProduct2List;
            
            cscfga__Product_Configuration__c tmpProductConfig = CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Test Product Configuration 1', tmpProductBasket.Id);
            tmpProductConfig.Recurring_Charge_Product__c = tmpProduct2List[0].Id;
            tmpProductConfig.Mobile_Scenario__c = 'OneBusiness';
            tmpProductConfig.cscfga__Contract_Term__c =1;
            insert tmpProductConfig;
            
            List<cscfga__Attribute__c > tmpAttributeList = new List<cscfga__Attribute__c>();
            tmpAttributeList.add(CS_DataTest.createAttribute('Recurring Price Basic', tmpAttDefList[0], true, 10, tmpProductConfig, true, '10.0'));
            tmpAttributeList.add(CS_DataTest.createAttribute('One off Price Basic', tmpAttDefList[1], true, 20, tmpProductConfig, false, '20.0'));
            tmpAttributeList.add(CS_DataTest.createAttribute('Recurring Price Other', tmpAttDefList[2], true, 10, tmpProductConfig, true, '10.0'));
            tmpAttributeList.add(CS_DataTest.createAttribute('One off Price Other', tmpAttDefList[3], true, 20, tmpProductConfig, false, '20.0'));
            insert tmpAttributeList;    
            
            cscfga__Product_Configuration__c tmpProductConfig2 = CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Test Product Configuration 2', tmpProductBasket.Id);
            tmpProductConfig2.One_Off_Charge_Product__c = tmpProduct2List[1].Id;
            tmpProductConfig2.Mobile_Scenario__c = 'OneBusiness';
            tmpProductConfig2.cscfga__Contract_Term__c =1; 
            insert tmpProductConfig2;
            
            List<cscfga__Attribute__c > tmpAttributeList2 = new List<cscfga__Attribute__c>();
            tmpAttributeList2.add(CS_DataTest.createAttribute('Recurring Price Basic', tmpAttDefList[0], true, 10, tmpProductConfig2, true, '10.0'));
            tmpAttributeList2.add(CS_DataTest.createAttribute('One off Price Basic', tmpAttDefList[1], true, 20, tmpProductConfig2, false, '20.0'));
            tmpAttributeList2.add(CS_DataTest.createAttribute('Recurring Price Other', tmpAttDefList[2], true, 10, tmpProductConfig2, true, '10.0'));
            tmpAttributeList2.add(CS_DataTest.createAttribute('One off Price Other', tmpAttDefList[3], true, 20, tmpProductConfig2, false, '20.0'));
            insert tmpAttributeList2;
            
            OLI_Sync__c OLISync = new OLI_Sync__c(); 
            OLISync.SetupOwnerId = UserInfo.getUserId();      
            insert OLISync;
            
            OLISync = OLI_Sync__c.getInstance(UserInfo.getUserId());
            OLISync.Product_Configuration_Level__c=false;  
            OLISync.Sum_One_Off_And_Recurring__c=false;
            update OLISync;
        
            CustomButtonSynchronizeWithOpportunity cbswo = new CustomButtonSynchronizeWithOpportunity();
            cbswo.performAction(tmpProductBasket.Id);
            
            Test.stopTest();
        }
    }

    private static testMethod void testSyncWithOEData() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs (simpleUser) {
            Test.startTest();

            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;

            No_Triggers__c notriggers = new No_Triggers__c();
            //notriggers.Location__c = 'User';
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;

            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
            insert tmpOpp;

            cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('OneMobile');
            prodDefinition.Product_Type__c = 'Mobile';
            insert prodDefinition;

            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
            tmpProductBasket.ProfitLoss_JSON__c = '{"KPI Direct Capex / Net Revenue":"0,00","KPI Payback Period(Months)":"0,00","KPI NPV":"0,00",' +
                    '"KPI Net Incremental Billed Revenue":"0,00","KPI Free cash flow / Net revenue":"1,50","KPI EBITDA / Net revenue":"1,56",' +
                    '"KPI Sales Margin / Net revenue":"1,71","NET RESULT":"56.993,07","NET RESULT first":"58.996,33","Capex":"1.228,43","Network Depreciation":"774,83",' +
                    '"Direct Capex":"0,00","EBITDA":"58.996,33","EBITDA first":"62.203,26","Overhead allocation":"3.206,93","Network Opex":"2.819,96",' +
                    '"Incremental EBITDA":"65.023,22","Incremental OPEX":"0,00","Total Gross Margin":"65.023,22","Total costs of sales":"10.816,78","A&R":"0,00",' +
                    '"Revenue share":"0,00","Other Costs":"0,00","Opportunity cost":"0,00","Opportunity cost Fixed":"0,00","Opportunity cost Mobile":"0,00",' +
                    '"Total cost of sales":"10.816,78","Total Net Revenue":"75.840,00","Total discounts":"0,00","Benchmark":"0,00",' +
                    '"Total credit amount / loyalty bonus":"0,00","Benchmark Fixed":"0,00","Benchmark Mobile":"0,00",' +
                    '"Operator other costs / other migration costs":"0,00","Operator other costs / other migration costs Fixed":"0,00",' +
                    '"Operator other costs / other migration costs Mobile":"0,00","Discounts":"0,00","Total Gross Revenue (SUM)":"75.840,00",' +
                    '"Total Gross Revenue for Operator other costs / other migration costs":"0,00","Total Gross Revenue":"75.840,00"}';
            insert tmpProductBasket;

            OrderType__c orderType = CS_DataTest.createOrderType('MAC');
            insert orderType;

            List<Product2> tmpProduct2List = new List<Product2>();
            tmpProduct2List.add(CS_DataTest.createProduct2('Recurring Price Basic','Test Product Definition', orderType, true));
            tmpProduct2List.add(CS_DataTest.createProduct2('One Off Price Basic','Test Product Definition', orderType, true));
            insert tmpProduct2List;

            cscfga__Product_Configuration__c parentConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'OneMobile', tmpProductBasket.Id);
            parentConfiguration.Recurring_Charge_Product__c = tmpProduct2List[0].Id;
            parentConfiguration.cscfga__Product_Family__c = 'OneMobile';
            insert parentConfiguration;

            OLI_Sync__c OLISync = new OLI_Sync__c();
            OLISync.SetupOwnerId = UserInfo.getUserId();
            insert OLISync;

            OLISync = OLI_Sync__c.getInstance(UserInfo.getUserId());
            OLISync.Product_Configuration_Level__c=false;
            OLISync.Sum_One_Off_And_Recurring__c=false;
            update OLISync;

            CustomButtonSynchronizeWithOpportunity cbswo = new CustomButtonSynchronizeWithOpportunity();
            cbswo.performAction(tmpProductBasket.Id);

            Test.stopTest();
        }
    }
    
    /*
    **
    **
    * pcOeAttrMap - {a7Z1l0000009gu2EAA={677e5cdc-d065-528f-ef35-4a05620880a3=(Attribute:[displayValue=New, name=ConnectionType, other=, showInUI=true, value=New], Attribute:[displayValue=1, name=Quantity, other=, showInUI=true, value=1]), e005a419-a1b5-f60c-eea4-03beea99c20b=(Attribute:[displayValue=Porting, name=ConnectionType, other=, showInUI=true, value=Porting], Attribute:[displayValue=1, name=Quantity, other=, showInUI=true, value=1])}}
    */
    
    private static testMethod void testMethodOEAttachmentSimplify() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) { 
            Test.startTest();
            
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;
            
            No_Triggers__c notriggers = new No_Triggers__c();
            //notriggers.Location__c = 'User';
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;

            Framework__c frameworkSetting = new Framework__c();
            frameworkSetting.Framework_Sequence_Number__c = 2;
            insert frameworkSetting;
            
            PriceReset__c priceResetSetting = new PriceReset__c();
           
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
               
            insert priceResetSetting;
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;
            
            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;
        
            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
            insert tmpOpp;
            
            cscfga__Product_Definition__c tmpProductDef = CS_DataTest.createProductDefinition('Test Product Definition');
            tmpProductDef.Product_Type__c = 'Fixed';
            insert tmpProductDef;
            
            List<cscfga__Attribute_Definition__c> tmpAttDefList = new List<cscfga__Attribute_Definition__c>();
            tmpAttDefList.add(CS_DataTest.createAttributeDefinition('Recurring Price Basic', tmpProductDef, 'Display Value', 'Double'));
            tmpAttDefList.add(CS_DataTest.createAttributeDefinition('One Off Price Basic', tmpProductDef, 'Display Value', 'Double'));
            tmpAttDefList.add(CS_DataTest.createAttributeDefinition('Recurring Price Other', tmpProductDef, 'Display Value', 'Double'));
            tmpAttDefList.add(CS_DataTest.createAttributeDefinition('One Off Price Other', tmpProductDef, 'Display Value', 'Double'));
            
            insert tmpAttDefList;
            
            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);  
            insert tmpProductBasket;
            
            OrderType__c orderType = CS_DataTest.createOrderType('MAC');
            insert orderType;
      
            List<Product2> tmpProduct2List = new List<Product2>();
            tmpProduct2List.add(CS_DataTest.createProduct2('Recurring Price Basic','Test Product Definition', orderType, true));
            tmpProduct2List.add(CS_DataTest.createProduct2('One Off Price Basic','Test Product Definition', orderType, true));
            insert tmpProduct2List;
            
            cscfga__Product_Configuration__c tmpProductConfig = CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Test Product Configuration 1', tmpProductBasket.Id);
            tmpProductConfig.Recurring_Charge_Product__c = tmpProduct2List[0].Id;
            tmpProductConfig.Mobile_Scenario__c = 'OneBusiness';
            tmpProductConfig.cscfga__Contract_Term__c =1;
            insert tmpProductConfig;
            
            List<cscfga__Attribute__c > tmpAttributeList = new List<cscfga__Attribute__c>();
            tmpAttributeList.add(CS_DataTest.createAttribute('Recurring Price Basic', tmpAttDefList[0], true, 10, tmpProductConfig, true, '10.0'));
            tmpAttributeList.add(CS_DataTest.createAttribute('One off Price Basic', tmpAttDefList[1], true, 20, tmpProductConfig, false, '20.0'));
            tmpAttributeList.add(CS_DataTest.createAttribute('Recurring Price Other', tmpAttDefList[2], true, 10, tmpProductConfig, true, '10.0'));
            tmpAttributeList.add(CS_DataTest.createAttribute('One off Price Other', tmpAttDefList[3], true, 20, tmpProductConfig, false, '20.0'));
            insert tmpAttributeList;    
            
            cscfga__Product_Configuration__c tmpProductConfig2 = CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Test Product Configuration 2', tmpProductBasket.Id);
            tmpProductConfig2.One_Off_Charge_Product__c = tmpProduct2List[1].Id;
            tmpProductConfig2.Mobile_Scenario__c = 'OneBusiness';
            tmpProductConfig2.cscfga__Contract_Term__c =1; 
            insert tmpProductConfig2;
            
            List<cscfga__Attribute__c > tmpAttributeList2 = new List<cscfga__Attribute__c>();
            tmpAttributeList2.add(CS_DataTest.createAttribute('Recurring Price Basic', tmpAttDefList[0], true, 10, tmpProductConfig2, true, '10.0'));
            tmpAttributeList2.add(CS_DataTest.createAttribute('One off Price Basic', tmpAttDefList[1], true, 20, tmpProductConfig2, false, '20.0'));
            tmpAttributeList2.add(CS_DataTest.createAttribute('Recurring Price Other', tmpAttDefList[2], true, 10, tmpProductConfig2, true, '10.0'));
            tmpAttributeList2.add(CS_DataTest.createAttribute('One off Price Other', tmpAttDefList[3], true, 20, tmpProductConfig2, false, '20.0'));
            insert tmpAttributeList2;
            
            OLI_Sync__c OLISync = new OLI_Sync__c(); 
            OLISync.SetupOwnerId = UserInfo.getUserId();      
            insert OLISync;
            
            OLISync = OLI_Sync__c.getInstance(UserInfo.getUserId());
            OLISync.Product_Configuration_Level__c=false;  
            OLISync.Sum_One_Off_And_Recurring__c=false;
            update OLISync;
        
            CustomButtonSynchronizeWithOpportunity cbswo = new CustomButtonSynchronizeWithOpportunity();
            List<ID> pcIds = new List<Id>();
            pcIds.add(tmpProductConfig.Id);
            
            MockResponseGeneratorforConnectionsOE oegenerator = new MockResponseGeneratorforConnectionsOE(pcIds[0]);
            Map<Id,List<cssmgnt.ProductProcessingUtility.Component>> oeMap = oegenerator.createConnectionsTestOE();
            CustomButtonSynchronizeWithOpportunity.solutionOESchemaSimplifier(oeMap);
            
            Test.stopTest();
        }
    }

    private static testMethod void testMethodRetrieveOEMap() {

        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs (simpleUser) {
            Test.startTest();
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;

            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', simpleUser.Id);
            insert tmpOpp;

            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
            insert tmpProductBasket;

            OrderType__c orderType = CS_DataTest.createOrderType('MAC');
            insert orderType;

            cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('OneMobile');
            prodDefinition.Product_Type__c = 'Mobile';
            insert prodDefinition;

            List<Product2> tmpProduct2List = new List<Product2>();
            tmpProduct2List.add(CS_DataTest.createProduct2('Recurring Price Basic','Test Product Definition', orderType, true));
            tmpProduct2List.add(CS_DataTest.createProduct2('One Off Price Basic','Test Product Definition', orderType, true));
            insert tmpProduct2List;

            cscfga__Product_Configuration__c parentConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'OneMobile', tmpProductBasket.Id);
            parentConfiguration.Recurring_Charge_Product__c = tmpProduct2List[0].Id;
            parentConfiguration.cscfga__Product_Family__c = 'OneMobile';
            insert parentConfiguration;

            CustomButtonSynchronizeWithOpportunity.retrieveOeMap(tmpProductBasket.Id);

            Test.stopTest();
        }
    }
}