/**
 * Created by bojan.zunko on 03/07/2020.
 */

global with sharing class CustomDataProcessor {

    global static List<SObject> processCustomData(ID pcID, Map<String,String> customDataAQD) {

        Attachment customData = new Attachment();
        customData.Name = 'CustomData.json';
        customData.ParentId = pcID;
        customData.Body = Blob.valueOf(JSON.serialize(customDataAQD));

        return new List<SObject>{customData};
    }
}