@isTest
private class CustomDataProcessorTest 
{   
    testMethod static void CustomDataProcessorTestMethod() {
        Map<String,String> customDataAQD = new Map<String,String>();
        customDataAQD.put('test', 'test');

        Account account = new Account(
            OwnerId = UserInfo.getUserId(),
            Name = 'Account',
            Type = 'End Customer'
        );
        insert account;

        Contact contact = new Contact(
            AccountId = account.id,
            LastName = 'Last',
            FirstName = 'First',
            Contact_Role__c = 'Consultant',
            Email = 'test@vf.com'
        );
        insert contact;

        Opportunity opportunity = new Opportunity(
            Name = 'New Opportunity',
            OwnerId = UserInfo.getUserId(),
            StageName = 'Qualification',
            Probability = 0,
            CloseDate = system.today(),
            AccountId = account.id
        );
        insert opportunity;

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            Primary__c = false,
            OwnerId = UserInfo.getUserId(),
            cscfga__Opportunity__c = opportunity.Id,
            Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
        );
        insert basket;

        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
            cscfga__Product_Basket__c = basket.Id
        );
        insert config;

        Test.startTest();
        List<SObject> testReturn = CustomDataProcessor.processCustomData(config.Id, customDataAQD);
        Test.stopTest();
    }
}