public with sharing class CustomerAssetTriggerHandler  extends TriggerHandler {
    private List<Customer_Asset__c> oldCustomerAssets;
    private List<Customer_Asset__c> newCustomerAssets;
    private Map<Id, Customer_Asset__c> oldCustomerAssetsMap;
    private Map<Id, Customer_Asset__c> newCustomerAssetsMap;

    private void init() {
        oldCustomerAssets = (List<Customer_Asset__c>) this.oldList;
        newCustomerAssets = (List<Customer_Asset__c>) this.newList;
        oldCustomerAssetsMap = (Map<Id, Customer_Asset__c>) this.oldMap;
        newCustomerAssetsMap = (Map<Id, Customer_Asset__c>) this.newMap;
    }

    public override void afterInsert() {
        init();
        createBSSOrder(newCustomerAssetsMap.keySet());
    }

    @future(callout=true)
    private static void createBSSOrder(Set<ID> customerAssetIds){
        List<Customer_Asset__c> toSend = new List<Customer_Asset__c>();

        for (Customer_Asset__c customerAsset : [SELECT Id, Account__c, Assigned_Product_Id__c, Billing_Arrangement__c, Billing_Status__c, Error_Info__c,
                                                External_Reference_Id__c, Installed_Base_Id__c, Order__c, Site__c, Type__c, Article_Code__c, BOP_Article_Description__c,
                                                BOP_HDN_Services__c, BOP_Managed_Item_ID__c, Billing_Customer_Id__c, Charge_Description__c, Contract_End_Date__c,
                                                Contract_Period__c, Contract_Start_Date__c, Cost_Center__c, Discount__c, Icon_Billing_Status__c,
                                                Icon_Installation_Status__c, Installation_Status__c, Latest_Activation_Date__c, PO_Number__c, Price__c,
                                                Product__c, Quantity__c, RC_OC__c, Is_Virtual__c, Is_Shared__c, IsActive__c, Contract__c, External_Account__c,
                                                Free_Paper_Bill__c, BI_Tag2__c, BI_Tag3__c, BI_Tag4__c, BI_Tag5__c, BI_Tag1__c, Discount_Description__c, Charge_Code__c,
                                                Latest_OMS_Order_Id__c, Contract_VF__c, Created_From_BICC_Feed__c FROM Customer_Asset__c WHERE ID IN:customerAssetIds]) {
            if (customerAsset.Created_From_BICC_Feed__c) {
                toSend.add(customerAsset);
            }
        }

        String transactionId = 'TR-' + Datetime.now().getTime();

        // make callout to interface
        if (!toSend.isEmpty()) {
            CreateBSSOrderRest.CollectionWrapper collectionWrapper = CreateBSSOrderRest.createBSSOrder(
                transactionId,
                toSend
            );
            insert collectionWrapper.billingTransactionToInsert;
        }
    }
}