public without sharing class CvmDataController {

	ApexPages.Standardcontroller controller;

	public Account accountToUpdate{get;set;}

	public String field1{get;set;}



	public CvmDataController(ApexPages.StandardController stdController) {

		controller = stdController;
		accountToUpdate = (Account)stdController.getRecord();
		accountToUpdate = [select id, name, CVM_Signature_Name__c, CVM_Signature_Phone__c, CVM_Signature_Function__c, CVM_Signature_WebAddress__c, CVM_Contact_E_mail__c, CVM_Lead_Owner__c from account where id = :accountToUpdate.id];
		//Apexpages.addMessage (new ApexPages.message(ApexPages.severity.ERROR,'error'));
		//Apexpages.addMessage (new ApexPages.message(ApexPages.severity.INFO,'info'));
		//Apexpages.addMessage (new ApexPages.message(ApexPages.severity.WARNING,'WARNING'));
	}

	public void saveChanges(){

		try{
			update accountToUpdate;
			Apexpages.addMessage (new ApexPages.message(ApexPages.severity.INFO, Label.CVM_Changes_Saved));
		}
		catch(Exception e){
			Apexpages.addMessage (new ApexPages.message(ApexPages.severity.ERROR, Label.CVM_Changes_not_saved));
		}
	}
}