@isTest
public class DWHDeleteBatchTest {

    @TestSetup
    static void setup()
    {
        List<DWH_Account_SF_Interface__c> accList  = new List<DWH_Account_SF_Interface__c>();
        for (Integer i=0;i<200;i++) {
            DWH_Account_SF_Interface__c acc = new DWH_Account_SF_Interface__c();
            acc.CompanyKvkNumber__c = '12345678';
            acc.CompanyLocationCode__c = '1055SC|959||';
            acc.TmeCode__c = 'TME-71162429';
            acc.Processed__c = true;
            acc.LastModifiedDate = Date.today().addDays(-60);
            acc.CreatedDate = Date.today().addDays(-60);
            accList.add(acc);
        }
        insert accList;

        List<DWH_Asset_SF_Interface__c> assList  = new List<DWH_Asset_SF_Interface__c>();
        for (Integer i=0;i<200;i++) {
            DWH_Asset_SF_Interface__c acc = new DWH_Asset_SF_Interface__c();
            acc.KvkNumber__c = '12345678';
            acc.LocationCode__c = '1055SC|959||';
            acc.Processed__c = true;
            acc.LastModifiedDate = Date.today().addDays(-60);
            acc.CreatedDate = Date.today().addDays(-60);
            assList.add(acc);
        }
        insert assList;

        List<DWH_KvkDelta_SF_Interface__c> kvkdeltasList = new List<DWH_KvkDelta_SF_Interface__c>();
        for (Integer i=0;i<200;i++) {
            DWH_KvkDelta_SF_Interface__c acc = new DWH_KvkDelta_SF_Interface__c();
            acc.KvkNumber__c = '12345678';
            acc.Processed__c = true;
            acc.LastModifiedDate = Date.today().addDays(-60);
            acc.CreatedDate = Date.today().addDays(-60);
            kvkdeltasList.add(acc);
        }
        insert kvkdeltasList;

        List<DWH_Relation_SF_Interface__c> relList = new List<DWH_Relation_SF_Interface__c>();
        for (Integer i=0;i<200;i++) {
            DWH_Relation_SF_Interface__c acc = new DWH_Relation_SF_Interface__c();
            acc.KvkNumber__c = '12345678';
            acc.Processed__c = true;
            acc.LastModifiedDate = Date.today().addDays(-60);
            acc.CreatedDate = Date.today().addDays(-60);
            relList.add(acc);
        }
        insert relList;

        List<Customer_Relations_Potential__c> relOgList = new List<Customer_Relations_Potential__c>();
        for (Integer i=0;i<200;i++) {
            Customer_Relations_Potential__c acc = new Customer_Relations_Potential__c();
            acc.Account_KVK__c = '12345678';
            acc.Company__c = 'Test A';
            acc.DWH_SF_Interface_ID__c = '123';
            acc.LastModifiedDate = Date.today().addDays(-60);
            acc.CreatedDate = Date.today().addDays(-60);
            relOgList.add(acc);
        }
        insert relOgList;


    }

    @isTest static void checkDelete(){
        Test.startTest();
        DWHDeleteBatch x = new DWHDeleteBatch();
        database.executeBatch(x);
        Test.stopTest();
    }

    @isTest static void checkSchedule(){
        Test.startTest();
        SchedulableContext sc = null;
        DWHDeleteBatch tsc = new DWHDeleteBatch();
        tsc.execute(sc);
        Test.stopTest();

    }

    @isTest static void checkDeleteOgRelations(){
        Test.startTest();
        DWHDeleteProcessedRelationBatch x = new DWHDeleteProcessedRelationBatch();
        database.executeBatch(x);
        Test.stopTest();
    }
}