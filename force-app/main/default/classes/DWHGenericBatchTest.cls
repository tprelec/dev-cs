@isTest
public class DWHGenericBatchTest {
    @isTest static void setup(){
        //Data preparation for sync table
        TestUtils.autoCommit = false;

        list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
        fsmlist.add(TestUtils.createSync('DWH_Account_SF_Interface__c -> Account', 'TmeActive__c', 'Active_Company__c'));
        fsmlist.add(TestUtils.createSync('DWH_Lead_dummy__c -> Lead', 'CompanyName__c', 'Company'));
        fsmlist.add(TestUtils.createSync('DWH_Asset_SF_Interface__c -> VF_Asset__c', 'CommercialProductCode__c', 'Priceplan_Name__c'));
        fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'CustomerNumber__c', 'Customer_Number__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'PlayLevel__c', 'Playlevel__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'BusinessSubSegment__c', 'Business_segment__c'));
        fsmlist.add(TestUtils.createSync('DWH_KvkDelta_SF_Interface__c -> VF_Asset__c', 'Today__c', 'Retention_Date__c'));
        insert fsmlist;

        // Data preparation for destination records
        TestUtils.autoCommit = true;
        User admin = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;
        //Account acc = TestUtils.createAccount(admin);
        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c='12345678';
        upsert acc;
        Id ziggoRec = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName().get('IB Ziggo').getRecordTypeId();
        VF_Asset__c ass = new VF_Asset__c(RecordTypeId = ziggoRec, account__c = acc.id, account_KVK__c = '12345678', Priceplan_Class__c = 'fZiggo IB');
        insert ass;

        //Data preparation for load table

        list<SObject> loadList = new list<SObject>{
                //existing account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345678',CompanyKvkNumber__c='12345678',CompanyLocationCode__c='9257RN|18||'),
                //lead
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345679',CompanyKvkNumber__c='12345679',CompanyLocationCode__c='9657RN|18||'),
                //new account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345689',CompanyKvkNumber__c='12345689',CompanyLocationCode__c='9658RN|18||',CompanyIsActiveCustomer__c=true),
                //new delta
                new DWH_KvkDelta_SF_Interface__c(KvkNumber__c='12345678'),
                //new asset
                new DWH_Asset_SF_Interface__c(CommercialProductCode__c='CP_IPVPNPRMM5MBPSFBR', CustomerNumber__c='12345678',KvkNumber__c='12345678',LocationCode__c='9257RN|18||', Country__c = 'NL', City__c = 'Amsterdam', Street__c = 'Hoofdstraat') ,
                //new relation
                new DWH_Relation_SF_Interface__c(MatchStart__c='01012001', CustomerNumber__c='12345678',KvkNumber__c='12345678')
        };

        insert loadList;
    }

    /*@isTest static void test_batch_Account() {


        TestUtils.autoCommit = false;

        list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
        fsmlist.add(TestUtils.createSync('DWH_Account_SF_Interface__c -> Account', 'TmeActive__c', 'Active_Company__c'));
        fsmlist.add(TestUtils.createSync('DWH_Lead_dummy__c -> Lead', 'CompanyName__c', 'Company'));
        fsmlist.add(TestUtils.createSync('DWH_Asset_SF_Interface__c -> VF_Asset__c', 'CommercialProductCode__c', 'Priceplan_Name__c'));
        fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'CustomerNumber__c', 'Customer_Number__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'PlayLevel__c', 'Playlevel__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'BusinessSubSegment__c', 'Business_segment__c'));
        fsmlist.add(TestUtils.createSync('DWH_KvkDelta_SF_Interface__c -> VF_Asset__c', 'Today__c', 'Retention_Date__c'));
        insert fsmlist;

        // Data preparation for destination records
        TestUtils.autoCommit = true;
        User admin = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;
        //Account acc = TestUtils.createAccount(admin);
        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c='12345678';
        upsert acc;
        Id ziggoRec = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName().get('IB Ziggo').getRecordTypeId();
        VF_Asset__c ass = new VF_Asset__c(RecordTypeId = ziggoRec, account__c = acc.id, account_KVK__c = '12345678', Priceplan_Class__c = 'fZiggo IB');
        insert ass;

        //Data preparation for load table

        list<SObject> loadList = new list<SObject>{
                //existing account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345678',CompanyKvkNumber__c='12345678',CompanyLocationCode__c='9257RN|18||'),
                //lead
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345679',CompanyKvkNumber__c='12345679',CompanyLocationCode__c='9657RN|18||'),
                //new account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345689',CompanyKvkNumber__c='12345689',CompanyLocationCode__c='9658RN|18||',CompanyIsActiveCustomer__c=true),
                //new delta
                new DWH_KvkDelta_SF_Interface__c(KvkNumber__c='12345678'),
                //new asset
                new DWH_Asset_SF_Interface__c(CommercialProductCode__c='CP_IPVPNPRMM5MBPSFBR', CustomerNumber__c='12345678',KvkNumber__c='12345678',LocationCode__c='9257RN|18||', Country__c = 'NL', City__c = 'Amsterdam', Street__c = 'Hoofdstraat') ,
                //new relation
                new DWH_Relation_SF_Interface__c(MatchStart__c='01012001', CustomerNumber__c='12345678',KvkNumber__c='12345678')
        };

        insert loadList;

        //The actual Test
        //Test.startTest();

        DWHGenericBatch controller = new DWHGenericBatch('DWH_Account_SF_Interface__c');
        controller.chain = false;
        database.executebatch(controller);

        //Test.stopTest();

    }

    @isTest static void test_batch_Asset() {
        TestUtils.autoCommit = false;

        list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
        fsmlist.add(TestUtils.createSync('DWH_Account_SF_Interface__c -> Account', 'TmeActive__c', 'Active_Company__c'));
        fsmlist.add(TestUtils.createSync('DWH_Lead_dummy__c -> Lead', 'CompanyName__c', 'Company'));
        fsmlist.add(TestUtils.createSync('DWH_Asset_SF_Interface__c -> VF_Asset__c', 'CommercialProductCode__c', 'Priceplan_Name__c'));
        fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'CustomerNumber__c', 'Customer_Number__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'PlayLevel__c', 'Playlevel__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'BusinessSubSegment__c', 'Business_segment__c'));
        fsmlist.add(TestUtils.createSync('DWH_KvkDelta_SF_Interface__c -> VF_Asset__c', 'Today__c', 'Retention_Date__c'));
        insert fsmlist;

        // Data preparation for destination records
        TestUtils.autoCommit = true;
        User admin = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;
        //Account acc = TestUtils.createAccount(admin);
        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c='12345678';
        upsert acc;
        Id ziggoRec = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName().get('IB Ziggo').getRecordTypeId();
        VF_Asset__c ass = new VF_Asset__c(RecordTypeId = ziggoRec, account__c = acc.id, account_KVK__c = '12345678', Priceplan_Class__c = 'fZiggo IB');
        insert ass;

        //Data preparation for load table

        list<SObject> loadList = new list<SObject>{
                //existing account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345678',CompanyKvkNumber__c='12345678',CompanyLocationCode__c='9257RN|18||'),
                //lead
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345679',CompanyKvkNumber__c='12345679',CompanyLocationCode__c='9657RN|18||'),
                //new account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345689',CompanyKvkNumber__c='12345689',CompanyLocationCode__c='9658RN|18||',CompanyIsActiveCustomer__c=true),
                //new delta
                new DWH_KvkDelta_SF_Interface__c(KvkNumber__c='12345678'),
                //new asset
                new DWH_Asset_SF_Interface__c(CommercialProductCode__c='CP_IPVPNPRMM5MBPSFBR', CustomerNumber__c='12345678',KvkNumber__c='12345678',LocationCode__c='9257RN|18||', Country__c = 'NL', City__c = 'Amsterdam', Street__c = 'Hoofdstraat') ,
                //new relation
                new DWH_Relation_SF_Interface__c(MatchStart__c='01012001', CustomerNumber__c='12345678',KvkNumber__c='12345678')
        };

        insert loadList;

        //The actual Test
        //Test.startTest();


        DWHGenericBatch controller = new DWHGenericBatch('DWH_Asset_SF_Interface__c');
        controller.chain = false;
        database.executebatch(controller);

        //Test.stopTest();

    }

    @isTest static void test_batch_Delta() {
        TestUtils.autoCommit = false;

        list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
        fsmlist.add(TestUtils.createSync('DWH_Account_SF_Interface__c -> Account', 'TmeActive__c', 'Active_Company__c'));
        fsmlist.add(TestUtils.createSync('DWH_Lead_dummy__c -> Lead', 'CompanyName__c', 'Company'));
        fsmlist.add(TestUtils.createSync('DWH_Asset_SF_Interface__c -> VF_Asset__c', 'CommercialProductCode__c', 'Priceplan_Name__c'));
        fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'CustomerNumber__c', 'Customer_Number__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'PlayLevel__c', 'Playlevel__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'BusinessSubSegment__c', 'Business_segment__c'));
        fsmlist.add(TestUtils.createSync('DWH_KvkDelta_SF_Interface__c -> VF_Asset__c', 'Today__c', 'Retention_Date__c'));
        insert fsmlist;

        // Data preparation for destination records
        TestUtils.autoCommit = true;
        User admin = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;
        //Account acc = TestUtils.createAccount(admin);
        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c='12345678';
        upsert acc;
        Id ziggoRec = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName().get('IB Ziggo').getRecordTypeId();
        VF_Asset__c ass = new VF_Asset__c(RecordTypeId = ziggoRec, account__c = acc.id, account_KVK__c = '12345678', Priceplan_Class__c = 'fZiggo IB');
        insert ass;

        //Data preparation for load table

        list<SObject> loadList = new list<SObject>{
                //existing account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345678',CompanyKvkNumber__c='12345678',CompanyLocationCode__c='9257RN|18||'),
                //lead
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345679',CompanyKvkNumber__c='12345679',CompanyLocationCode__c='9657RN|18||'),
                //new account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345689',CompanyKvkNumber__c='12345689',CompanyLocationCode__c='9658RN|18||',CompanyIsActiveCustomer__c=true),
                //new delta
                new DWH_KvkDelta_SF_Interface__c(KvkNumber__c='12345678'),
                //new asset
                new DWH_Asset_SF_Interface__c(CommercialProductCode__c='CP_IPVPNPRMM5MBPSFBR', CustomerNumber__c='12345678',KvkNumber__c='12345678',LocationCode__c='9257RN|18||', Country__c = 'NL', City__c = 'Amsterdam', Street__c = 'Hoofdstraat') ,
                //new relation
                new DWH_Relation_SF_Interface__c(MatchStart__c='01012001', CustomerNumber__c='12345678',KvkNumber__c='12345678')
        };

        insert loadList;

        //The actual Test
        //Test.startTest();

        DWHGenericBatch controller = new DWHGenericBatch('DWH_KvkDelta_SF_Interface__c');
        controller.chain = false;
        database.executebatch(controller);

        //Test.stopTest();

    }

    @isTest static void test_batch_Relation() {
        TestUtils.autoCommit = false;

        list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
        fsmlist.add(TestUtils.createSync('DWH_Account_SF_Interface__c -> Account', 'TmeActive__c', 'Active_Company__c'));
        fsmlist.add(TestUtils.createSync('DWH_Lead_dummy__c -> Lead', 'CompanyName__c', 'Company'));
        fsmlist.add(TestUtils.createSync('DWH_Asset_SF_Interface__c -> VF_Asset__c', 'CommercialProductCode__c', 'Priceplan_Name__c'));
        fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'CustomerNumber__c', 'Customer_Number__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'PlayLevel__c', 'Playlevel__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'BusinessSubSegment__c', 'Business_segment__c'));
        fsmlist.add(TestUtils.createSync('DWH_KvkDelta_SF_Interface__c -> VF_Asset__c', 'Today__c', 'Retention_Date__c'));
        insert fsmlist;

        // Data preparation for destination records
        TestUtils.autoCommit = true;
        User admin = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;
        //Account acc = TestUtils.createAccount(admin);
        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c='12345678';
        upsert acc;
        Id ziggoRec = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName().get('IB Ziggo').getRecordTypeId();
        VF_Asset__c ass = new VF_Asset__c(RecordTypeId = ziggoRec, account__c = acc.id, account_KVK__c = '12345678', Priceplan_Class__c = 'fZiggo IB');
        insert ass;

        //Data preparation for load table

        list<SObject> loadList = new list<SObject>{
                //existing account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345678',CompanyKvkNumber__c='12345678',CompanyLocationCode__c='9257RN|18||'),
                //lead
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345679',CompanyKvkNumber__c='12345679',CompanyLocationCode__c='9657RN|18||'),
                //new account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345689',CompanyKvkNumber__c='12345689',CompanyLocationCode__c='9658RN|18||',CompanyIsActiveCustomer__c=true),
                //new delta
                new DWH_KvkDelta_SF_Interface__c(KvkNumber__c='12345678'),
                //new asset
                new DWH_Asset_SF_Interface__c(CommercialProductCode__c='CP_IPVPNPRMM5MBPSFBR', CustomerNumber__c='12345678',KvkNumber__c='12345678',LocationCode__c='9257RN|18||', Country__c = 'NL', City__c = 'Amsterdam', Street__c = 'Hoofdstraat') ,
                //new relation
                new DWH_Relation_SF_Interface__c(MatchStart__c='01012001', CustomerNumber__c='12345678',KvkNumber__c='12345678')
        };

        insert loadList;

        //The actual Test
        //Test.startTest();

        DWHGenericBatch controller = new DWHGenericBatch('DWH_Relation_SF_Interface__c');
        controller.chain = false;
        database.executebatch(controller);

        //Test.stopTest();

    }

    @isTest static void test_batch_Chain() {
        TestUtils.autoCommit = false;

        list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
        fsmlist.add(TestUtils.createSync('DWH_Account_SF_Interface__c -> Account', 'TmeActive__c', 'Active_Company__c'));
        fsmlist.add(TestUtils.createSync('DWH_Lead_dummy__c -> Lead', 'CompanyName__c', 'Company'));
        fsmlist.add(TestUtils.createSync('DWH_Asset_SF_Interface__c -> VF_Asset__c', 'CommercialProductCode__c', 'Priceplan_Name__c'));
        fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'CustomerNumber__c', 'Customer_Number__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'PlayLevel__c', 'Playlevel__c'));
        //fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'BusinessSubSegment__c', 'Business_segment__c'));
        fsmlist.add(TestUtils.createSync('DWH_KvkDelta_SF_Interface__c -> VF_Asset__c', 'Today__c', 'Retention_Date__c'));
        insert fsmlist;

        // Data preparation for destination records
        TestUtils.autoCommit = true;
        User admin = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;
        //Account acc = TestUtils.createAccount(admin);
        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c='12345678';
        upsert acc;
        Id ziggoRec = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName().get('IB Ziggo').getRecordTypeId();
        VF_Asset__c ass = new VF_Asset__c(RecordTypeId = ziggoRec, account__c = acc.id, account_KVK__c = '12345678', Priceplan_Class__c = 'fZiggo IB');
        insert ass;

        //Data preparation for load table

        list<SObject> loadList = new list<SObject>{
                //existing account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345678',CompanyKvkNumber__c='12345678',CompanyLocationCode__c='9257RN|18||'),
                //lead
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345679',CompanyKvkNumber__c='12345679',CompanyLocationCode__c='9657RN|18||'),
                //new account
                new DWH_Account_SF_Interface__c(TmeCode__c='TME_12345689',CompanyKvkNumber__c='12345689',CompanyLocationCode__c='9658RN|18||',CompanyIsActiveCustomer__c=true),
                //new delta
                new DWH_KvkDelta_SF_Interface__c(KvkNumber__c='12345678'),
                //new asset
                new DWH_Asset_SF_Interface__c(CommercialProductCode__c='CP_IPVPNPRMM5MBPSFBR', CustomerNumber__c='12345678',KvkNumber__c='12345678',LocationCode__c='9257RN|18||', Country__c = 'NL', City__c = 'Amsterdam', Street__c = 'Hoofdstraat') ,
                //new relation
                new DWH_Relation_SF_Interface__c(MatchStart__c='01012001', CustomerNumber__c='12345678',KvkNumber__c='12345678')
        };

        insert loadList;

        //The actual Test
        //Test.startTest();

        DWHGenericBatch controller = new DWHGenericBatch('DWH_Account_SF_Interface__c');
        controller.chain = true;
        database.executebatch(controller);

        //Test.stopTest();

    }*/

    @isTest static void test_data() {
        Test.startTest();
        String s = DWHGenericBatch.testData();
        Test.stopTest();
    }

}