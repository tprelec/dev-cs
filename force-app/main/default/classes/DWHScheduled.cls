global class DWHScheduled implements Schedulable {
    global void execute(SchedulableContext sc) {
			DWHGenericBatch biccBatch = new DWHGenericBatch('DWH_Account_SF_Interface__c');
			biccBatch.chain = true;
			Database.executeBatch(biccBatch, 50);
        }
}