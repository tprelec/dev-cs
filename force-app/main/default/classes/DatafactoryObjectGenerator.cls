public with sharing class DatafactoryObjectGenerator {

    private static list<String> siteNames = new list<String>{'\'S-HERTOGENBOSCH, Larenweg 50', 'HENDRIK IDO AMBACHT, Langeweg 79', 'EINDHOVEN, Don Boscostraat 4', 'Rotterdam, Driemanssteeweg 700', 'UDEN, Linie 27', 'Utrecht, Westkanaaldijk 2 2', 'Heerlen, Postbus 8', 'Eindhoven, Grasanjelier 18', 'Utrecht, Savannahweg 31	', 'Veghel, Marshallweg 1'};
    private static list<String> siteStreets = new list<String>{'Larenweg', 'Langeweg', 'Don Boscostraat', 'Driemanssteeweg', 'Linie', 'Westkanaaldijk 2', 'Postbus', 'Grasanjelier', 'Savannahweg', 'Marshallweg'};
    private static list<String> sitePostalCodes = new list<String>{'5234KA', '3342LD', '5611KW', '3084CB', '5405AR', '3542DA', '6401DC', '5658GZ', '3542AW', '5466AH'};
    private static list<String> siteCities = new list<String>{'\'S-HERTOGENBOSCH', 'HENDRIK IDO AMBACHT', 'EINDHOVEN', 'Rotterdam', 'UDEN', 'Utrecht', 'Heerlen', 'Eindhoven', 'Utrecht', 'Veghel'};
    private static list<String> sitePhones = new list<String>{'073  6481481', '078  6233200', '	040  2193344', '0413 338811', '0183 643600', '0183 650650', '0481 362300', '0252 462000', '	026  3731111', '0183 643800'};
    private static list<String> siteLocationTypes = new list<String>{'head', 'sub', 'sub', 'sub', 'sub', 'sub', 'sub', 'sub', 'sub', 'sub'};

    private static list<String> postalCodeCheckAccessVendor  = new list<String>{'DLM Entry','DLM Premium','DLM Entry','DLM Premium','DLM Entry','DLM Premium','TELE2-BUS','KPNWEAS','VF-FIBER','EF-FIBER','ZIGGO','TELE2','TELE2-BUS','DLM Entry','DLM Entry','DLM Premium','DLM Premium','DLM Premium','DLM Entry','DLM Premium','DLM Entry','DLM Entry','DLM Premium','DLM Premium','KPNWEAS','KPNWEAS','KPNWEAS','KPNWEAS','KPNWEAS','ZIGGO','ZIGGO','ZIGGO','VF-FIBER','VF-FIBER','ZIGGO','ZIGGO','TELE2-BUS','TELE2-BUS','EF-FIBER','BBNED','TELE2-BUS','DLM Entry','DLM Premium','KPNWEAS','KPNWEAS','TELE2-BUS','DLM Entry','DLM Premium','DLM Entry','DLM Premium'};
    private static list<Decimal> postalCodeCheckAccessMaxUpSpeed = new list<Decimal>{6893,5152,960,960,20348,2048,0,1048576,1048576,1048576,1048576,1048576,0,640,951,640,640,951,5000,5000,19000,20000,19000,20000,1048576,1048577,1048576,1048574,1048577,1048576,1048576,1048577,1048576,1048576,40000,40000,1024,1024,1048576,2355,4096,2304,2304,2048,20480,15360,100000,100000,500000,500000};
    private static list<Decimal> postalCodeCheckAccessMaxDownSpeed = new list<Decimal>{37965,27141,11538,62348,2048,2048,0,1048576,1048576,1048576,1048576,1048576,0,9000,10919,6000,4000,4623,42000,16000,50000,55000,26000,28000,1048577,1048576,1048575,1048577,1048573,1048577,1048576,1048576,1048577,1048576,500000,500001,12288,1024,1048576,2355,4096,2304,2304,2048,20480,15360,100000,100000,500000,500000};
    private static list<String> postalCodeCheckTechnologyType = new list<String>{'VVDSL2_POTS','VVDSL2_POTS','ADSL2+_POTS','ADSL2+_POTS','SDSL','SDSL',null,'Fiber','Fiber','Fiber','Fiber','Fiber',null,'ADSL2+_POTS','ADSL2+_POTS','ADSL2+_POTS','ADSL2+_POTS','ADSL2+_POTS','VDSL2_POTS','VDSL2_POTS','VVDSL2_POTS','VVDSL2_POTS','VVDSL2_POTS','VVDSL2_POTS','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','hfc','hfc','ADSL2+_POTS','ADSL2+_POTS','Fiber','SDSL','SDSL.BIS (single-pair)','SDSL','SDSL','SDSL.BIS (single-pair)','SDSL.BIS (bonded)','SDSL.BIS (bonded)','EoF','EoF','GoF','GoF'};
    private static list<String> postalCodeCheckAccessclass = new list<String>{'E2E','E2E','A2E','A2E','A2E','A2E',null,'Fiber','Fiber','Fiber','Fiber','Fiber',null,'ETH,ATM','A2E','ETH','ETH,ATM','A2E','ETH,ATM','ETH,ATM','ETH','ETH,ATM','ETH','ETH,ATM','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','Fiber','hfc','hfc','ETH','ETH','Fiber','ATM','ETH','A2E','A2E','sdslbis','sdslbis','ETH','ETH','ETH','ETH','ETH'};
    private static list<String> postalCodeCheckAccessResultCheck = new list<String>{'ONNET','ONNET','ONNET','ONNET','ONNET','ONNET','No matching articles found','NEARNET','OFFNET','OFFNET','OFFNET',null,'No matching articles found','Green','Green','Green','Green','Green','Green','Green','Green','Green','Green','Green','ONNET','NEARNET_WITHOUT','NEARNET_WITH','ONNET','NEARNET_WITHOUT','NEARNET_UPTO100M','ONNET','NEARNET_UPTO250M','Oranje','Groen','ONNET','ONNET','Green','Green','Groen','Green','Green','Green','Green','Green','Green','Green','Green','Green','Green','Green'};
    private static list<Decimal> postalCodeCheckAccessPriority = new list<Decimal>{2,3,2,3,null,null,null,5,3,4,0,null,null,2,2,3,3,3,2,3,2,2,3,3,4,5,5,4,5,3,1,3,3,2,2,2,4,4,3,1,3,2,2,2,1,3,2,2,2,2};
    private static list<String> postalCodeCheckAccessInfrastructure = new list<String>{'VVDSL','VVDSL','ADSL','ADSL','SDSL','SDSL',null,'EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber',null,'ADSL','ADSL','ADSL','ADSL','ADSL','VDSL','VDSL','VVDSL','VVDSL','VVDSL','VVDSL','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','EthernetOverFiber','Coax','Coax','ADSL','ADSL','EthernetOverFiber','SDSL','EthernetOverCopperSingle','SDSL','SDSL','EthernetOverCopperSingle','EthernetOverCopper','EthernetOverCopper','FTTH','FTTH','FTTH','FTTH'};
    private static list<Boolean> postalCodeCheckAccessActive = new list<Boolean>{true,true,true,true,false,false,false,true,false,false,false,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true};

    public static Map<Id, Site__c> createSites(Map<Id, List<Competitor_Asset__c>> caByAccountID) {
        Map<Id, Site__c> sitesByPBX = new Map<Id, Site__c>();
        list<Site__c> sites = new list<Site__c>();
        Datetime today = Datetime.now();

        for (Id accountId : caByAccountID.keySet()) {
            Integer i = 0;
            for (Competitor_Asset__c ca : caByAccountID.get(accountId)) {
                Datetime siteDate = today.addDays(-i);

                Site__c s = new Site__c();
                s.name = siteNames[i];
                s.Site_Street__c = siteStreets[i];
                s.Site_Postal_Code__c = sitePostalCodes[i];
                s.Site_City__c = siteCities[i];
                s.Site_Phone__c = sitePhones[i];
                s.Site_Employees__c = getRandomInteger(1000);
                s.Location_Type__c = siteLocationTypes[i];
                s.Last_dsl_Canvas_check_c__c = siteDate;
                s.Last_dsl_check__c = siteDate;
                s.Last_fiber_Canvas_check_c__c = siteDate;
                s.Last_fiber_check__c = siteDate;
                s.Site_House_Number__c = getRandomInteger(1000);
                s.Country__c = 'NL';
                s.Site_Account__c = accountId;
                sites.add(s);
                sitesByPBX.put(ca.Id, s);
                i++;
            }
        }
        insert sites;
        return sitesByPBX;
    }

    public static list<Site_Postal_Check__c> createSitePostalCodeChecks(List<Site__c> sites){
        List<Site_Postal_Check__c> toReturn = new List<Site_Postal_Check__c>();

        for (Site__c site : sites) {
            for (Integer i = 0; i < 50 ; i++) {
                Site_Postal_Check__c spc = new Site_Postal_Check__c();
                spc.Access_Site_ID__c = site.Id;
                spc.Access_Vendor__c = postalCodeCheckAccessVendor[i];
                spc.Access_Max_Up_Speed__c = postalCodeCheckAccessMaxUpSpeed[i];
                spc.Access_Max_Down_Speed__c = postalCodeCheckAccessMaxDownSpeed[i];
                spc.Technology_Type__c = postalCodeCheckTechnologyType[i];
                spc.Accessclass__c = postalCodeCheckAccessclass[i];
                spc.Access_Result_Check__c = postalCodeCheckAccessResultCheck[i];
                spc.Access_Priority__c = postalCodeCheckAccessPriority[i];
                spc.Access_Infrastructure__c = postalCodeCheckAccessInfrastructure[i];
                spc.Access_Active__c = postalCodeCheckAccessActive[i];
                //spc.Access_Region__c = 'A';
                //spc.Access_Vendor__c = 'ZIGGO';
                //spc.Technology_Type__c = 'hfc';
                toReturn.add(spc);
            }
        }

        Database.insert(toReturn, false);
        return toReturn;
    }

    public static Integer getRandomInteger(integer range){
        return Integer.valueof((math.random() * range));
    }
}