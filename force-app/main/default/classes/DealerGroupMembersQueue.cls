public class DealerGroupMembersQueue implements Queueable {
    public Set<Id> removeAcc;
    public Set<Id> addAcc;
    public Group pasuDealer {
        get {
            if (pasuDealer == null) {
                pasuDealer = [SELECT Id FROM Group WHERE DeveloperName = 'PMs_PASU_Dealer' LIMIT 1];
            }
            return pasuDealer;
        }
        set;
    }

    public DealerGroupMembersQueue(Set<Id> removeAcc, Set<Id> addAcc) {
        this.removeAcc = removeAcc;
        this.addAcc = addAcc;  
    }
    
    public void execute(QueueableContext context) {
        if (pasuDealer == null) { return; }
        if (!removeAcc.isEmpty()) { removeGroupMembers(removeAcc, pasuDealer.Id); }
        if (!addAcc.isEmpty()) { addGroupMembers(addAcc, pasuDealer.Id); }
    }
    
    public void removeGroupMembers(Set<Id> removeAcc, Id pasuDealerId) {
        Map<Id, Group> dealerRoleGroup = new Map<Id, Group>(retrieveRoleGroups(removeAcc));
        if (!dealerRoleGroup.isEmpty()) {
            List<GroupMember> toDelete = [SELECT Id FROM GroupMember WHERE GroupId = :pasuDealerId AND UserOrGroupId IN :dealerRoleGroup.keySet()];
            delete toDelete;
        }
    }

    public void addGroupMembers(Set<Id> addAcc, Id pasuDealerId) {
        List<GroupMember> gms = new List<GroupMember>();
        for (Group dealerRoleGroup : retrieveRoleGroups(addAcc)) {
            gms.add(new GroupMember(GroupId = pasuDealerId, UserOrGroupId = dealerRoleGroup.Id));
        }
        if (!gms.isEmpty()) { insert gms; }
    }

    private List<Group> retrieveRoleGroups(Set<Id> accIds) {
        return [
            SELECT Id, DeveloperName, RelatedId 
            FROM Group 
            WHERE Type = 'Role' AND 
                RelatedId IN (
                    SELECT Id 
                    FROM UserRole 
                    WHERE PortalType = 'Partner' AND PortalAccountId IN :accIds
                )
        ];
    }
}