@isTest
private class DealerInfoTriggerHandlerTest {
	@isTest
	static void testDealerInfo() {
		User owner = TestUtils.createAdministrator();
		Account acct1 = TestUtils.createPartnerAccount('ABC');
		acct1.IsPartner = true;
		update acct1;
		Contact cont1 = TestUtils.createContact(acct1);
		User u1 = TestUtils.createManager();
		System.runAs(owner) {
			u1.IsActive = true;
			update u1;
		}
		cont1.Userid__c = u1.Id;
		update cont1;

		Account acct2 = TestUtils.createPartnerAccount('DEF');
		acct2.IsPartner = true;
		update acct2;
		Contact cont2 = TestUtils.createContact(acct2);
		User u2 = TestUtils.createManager();
		System.runAs(owner) {
			u2.IsActive = true;
			update u2;
		}
		cont2.Userid__c = u2.Id;
		update cont2;

		Test.startTest();
		//create dealerinfo1 with user1 and user2
		System.Test.setMock(WebServiceMock.class, new MetadataServiceTest.WebServiceMockImpl());
		TestUtils.autoCommit = false;
		List<Dealer_Information__c> diList = new List<Dealer_Information__c>();
		Dealer_Information__c di1 = TestUtils.createDealerInformation(cont1.Id, '999999');
		diList.add(di1);
		Dealer_Information__c di2 = TestUtils.createDealerInformation(cont2.Id, '767676', '999999');
		diList.add(di2);
		insert diList;
		di2.Status__c = 'Expired';
		update di2;
		Test.stopTest();
	}

	@isTest
	static void changeContact() {
		Account acct1 = TestUtils.createPartnerAccount('ABC');
		Contact cont1 = TestUtils.createContact(acct1);
		Contact cont2 = TestUtils.createContact(acct1);
		User u1 = TestUtils.createManager();
		cont1.Userid__c = u1.Id;
		update cont1;

		Test.startTest();
		//create dealerinfo1 with user1 and user2
		System.Test.setMock(WebServiceMock.class, new MetadataServiceTest.WebServiceMockImpl());
		Dealer_Information__c di1 = TestUtils.createDealerInformation(cont1.Id, '999999');
		di1.Contact__c = cont2.id;
		update di1;
		Test.stopTest();
	}

	@isTest
	static void checkForExistingDI() {
		Account acct1 = TestUtils.createPartnerAccount('ABC');
		Contact cont1 = TestUtils.createContact(acct1);
		User u1 = TestUtils.createPortalUser(acct1);
		cont1.Userid__c = u1.Id;
		update cont1;

		try {
			Test.startTest();
			TestUtils.autoCommit = false;
			List<Dealer_Information__c> diList = new List<Dealer_Information__c>();
			Dealer_Information__c di1 = TestUtils.createDealerInformation(cont1.Id, '999999');
			diList.add(di1);
			Dealer_Information__c di2 = TestUtils.createDealerInformation(cont1.Id, '888888');
			diList.add(di2);
			insert diList;
			Test.stopTest();
		} catch (Exception e) {
			Boolean expectedExceptionThrown = e.getMessage().contains('Dealer Code');
			System.assertEquals(expectedExceptionThrown, true);
		}
	}

	@isTest
	static void reassignDealerRecords() {
		TestUtils.createCompleteContract();

		List<Contact> cnts = new List<Contact>();
		Contact cnt = TestUtils.createContact(TestUtils.theAccount);
		cnt.UserId__c = TestUtils.theAdministrator.Id;
		cnts.add(cnt);
		Contact cnt2 = TestUtils.createContact(TestUtils.theAccount);
		cnt2.UserId__c = UserInfo.getUserId();
		cnts.add(cnt2);
		insert cnts;
		TestUtils.autoCommit = true;

		Dealer_Information__c di = TestUtils.createDealerInformation(cnts[0].Id, '999998');
		Dealer_Information__c newDi = TestUtils.createDealerInformation(cnts[1].Id, '999997');
		Postal_Code_Assignment__c pca = TestUtils.createPCA('1010', di.Id);

		Test.startTest();
		Account acc = [
			SELECT Id, Mobile_Dealer__c, Fixed_Dealer__c, OwnerId
			FROM Account
			WHERE Id = :TestUtils.theAccount.Id
		];
		acc.Mobile_Dealer__c = di.Id;
		acc.Fixed_Dealer__c = di.Id;
		update acc;

		VF_Contract__c c = [
			SELECT Id, Dealer_Information__c, OwnerId
			FROM VF_Contract__c
			WHERE Id = :TestUtils.theContract.Id
		];
		c.Dealer_Information__c = di.Id;
		update c;
		System.runAs(TestUtils.theAdministrator) {
			Opportunity opp = new Opportunity(
				Name = 'Test Open Opportunity',
				StageName = 'Closing',
				CloseDate = system.today(),
				AccountId = acc.Id,
				Type = 'New Business',
				Bespoke__c = false,
				Pricebook2Id = Test.getStandardPricebookId()
			);
			insert opp;
		}

		di.New_Account_Responsible__c = newDi.Id;
		di.New_Contract_Owner__c = newDi.Id;
		di.New_Opportunity_Owner__c = newDi.Id;
		di.New_Postal_code_Area_Responsible__c = newDi.Id;
		update di;

		System.assertEquals(
			[SELECT Id, Mobile_Dealer__c, OwnerId FROM Account WHERE Id = :TestUtils.theAccount.Id]
			.Mobile_Dealer__c,
			newDi.Id
		);
		System.assertEquals(
			[SELECT Id, Fixed_Dealer__c, OwnerId FROM Account WHERE Id = :TestUtils.theAccount.Id]
			.Fixed_Dealer__c,
			newDi.Id
		);
		System.assertEquals(
			[SELECT Id, OwnerId FROM Opportunity WHERE Name = 'Test Open Opportunity']
			.OwnerId,
			UserInfo.getUserId()
		);
		System.assertEquals(
			[
				SELECT Id, Dealer_Information__c, OwnerId
				FROM VF_Contract__c
				WHERE Id = :TestUtils.theContract.Id
			]
			.Dealer_Information__c,
			newDi.Id
		);
		System.assertEquals(
			[
				SELECT Id, Dealer_Information__c, OwnerId
				FROM Postal_Code_Assignment__c
				WHERE Id = :pca.Id
			]
			.Dealer_Information__c,
			newDi.Id
		);
		Test.stopTest();
	}

    @isTest
    static void sendContactChangeNotification(){
        Account acct1 = TestUtils.createPartnerAccount('ABC');
		Contact cont1 = TestUtils.createContact(acct1);
		Contact cont2 = TestUtils.createContact(acct1);
		User u1 = TestUtils.createManager();
		cont1.Userid__c = u1.Id;
		update cont1;
        Dealer_Information__c di1 = TestUtils.createDealerInformation(cont1.Id, '999999');

		Test.startTest();
            di1.Contact__c = cont2.id;
            update di1;
            Integer emailsSent = Limits.getEmailInvocations();
		Test.stopTest();

        System.assertEquals(1, emailsSent,
                            'The system should have sent 1 email');
    }

    @isTest
    static void sendDeactivationNotification(){
        Account acct1 = TestUtils.createPartnerAccount('ABC');
		Contact cont1 = TestUtils.createContact(acct1);
		User u1 = TestUtils.createManager();
		cont1.Userid__c = u1.Id;
		update cont1;
        Dealer_Information__c di1 = TestUtils.createDealerInformation(cont1.Id, '999999');

		Test.startTest();
            di1.Status__c  = 'Expired';
            update di1;
            Integer emailsSent = Limits.getEmailInvocations();
		Test.stopTest();

        System.assertEquals(1, emailsSent,
                            'The system should have sent 1 email');
    }
}