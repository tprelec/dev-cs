public without sharing class DeliveryExport implements Queueable, Database.AllowsCallouts {

    private Set<Id> orderIds = new Set<Id>();
    private Set<Id> contProdIds = new Set<Id>();
    private String exportType = '';
    private String contractedProductStatus = '';

    // called when it must use BOP status of contracted product record
    public DeliveryExport(Set<Id> orderIds, Set<Id> contProdIds, String exportType) {
        this.orderIds = orderIds;
        this.contProdIds = contProdIds;
        this.exportType = exportType;
    }

    // called when contracted product status is explicitly passed
    // an example would be from SIASBatch as the status change is done after this call
    public DeliveryExport(Set<Id> orderIds, Set<Id> contProdIds, String exportType, String contractedProductStatus) {
        this.orderIds = orderIds;
        this.contProdIds = contProdIds;
        this.exportType = exportType;
        this.contractedProductStatus = contractedProductStatus;
    }

    // method is part of queueable implementation
    public void execute(QueueableContext context) {
        exportDeliveries();
    }

    // export deliveries to BOP
    public void exportDeliveries() {
        List<Order__c> updateOrders = new List<Order__c>();
        List<Order__c> ordersToUpdate = new List<Order__c>();
        List<Contracted_Products__c> cpsToUpdate = new List<Contracted_Products__c>();
        
//        Map<Id,List<Contracted_Products__c>> contractedProductsMap = OrderUtils.getContractIdToContractedProducts(null, orderIds);
        Map<Id,List<Contracted_Products__c>> contractedProductsMap = OrderUtils.getOrderIdToContractedProducts(contProdIds);

        for(Order__c o : OrderUtils.getOrderDataByContractId(null,orderIds)){
            updateOrders.add(o);
        }
        
        if(!updateOrders.isEmpty()){
            ECSDeliveryService createService = new ECSDeliveryService();
            List<ECSDeliveryService.request> requests = new List<ECSDeliveryService.request>();
            Map<String,Id> orderNameToId = new Map<String,Id>();
 
            for(Order__c o : updateOrders){
                //ECSDeliveryService.request req =;
                requests.add(orderToRequest(o, contractedProductsMap, contractedProductStatus));
                orderNameToId.put(o.Sales_Order_Id__c,o.Id);
            }
            
            system.debug('##### requests:' + requests);

            createService.setRequest(requests);
            
            Boolean success = false;
            try{
                createService.makeRequest(exportType);
                success = true;
            } catch (ExWebServiceCalloutException ce){
                system.debug(ce);
                system.debug(ce.getMessage());
                for(Order__c o : updateOrders){
                    // cleanup the errormsg and put it in the order
                    o.BOP_Delivery_Errormessage__c = ce.get255charMessage();
                    o.BOP_Delivery_Status__c = 'Refused';
                    OrdersToUpdate.add(o);  
                }
            }
            
            if(success){
                for (ECSDeliveryService.response response : createService.getResponse()){
                    system.debug(response);
                    if(response.deliveryNumber != null){
                        Order__c o = new Order__c(Id = orderNameToId.get(response.deliveryNumber));
                        // indicator for success is the orderId (standard) or the errorcode (legacy)
                        if(response.errorCode == null || response.errorCode == '' || response.errorCode == '0') {
                            // success
                            // add info to order

                            o.BOP_Delivery_Datetime__c = system.now();
                            o.BOP_Delivery_Errormessage__c = null;
                            o.BOP_Delivery_Status__c = 'Accepted';
                            List<ECSSOAPDelivery.deliveryRowResponseType> deliveryRows = response.deliveryRows;
                            system.debug('## contractedProductsMap: ' + contractedProductsMap);
                            system.debug('## order: ' + o);
                            cpsToUpdate.addAll(processContractedProducts(deliveryRows, contractedProductsMap.get(o.Id)));
                        } else {
                            // failure
                            // add errormessage to order and unlock order
                            o.BOP_Delivery_Errormessage__c = (response.errorCode+' - '+response.errorMessage).left(255);
                            o.BOP_Delivery_Status__c = 'Refused';
                        }
                        OrdersToUpdate.add(o);
                    } else {
                        throw new ExMissingDataException('No Order Delivery Number returned by BOP');
                    }
                }
            }
        }

        try{
            update ordersToUpdate;
            update cpsToUpdate;
        } catch (DMLException e){
            ExceptionHandler.handleException(e);
        }

    }
    
    private static ECSDeliveryService.request orderToRequest(Order__c o, 
        Map<Id, List<Contracted_Products__c>> contractedProductsMap, 
        String contractedProductStatus){
        ECSDeliveryService.request req = new ECSDeliveryService.request();

        req.deliveryNumber = o.Sales_Order_Id__c;

        req.deliveryRows = new List<ECSSOAPDelivery.deliveryRowType>();

        for(Contracted_Products__c cp : contractedProductsMap.get(o.Id)){
            ECSSOAPDelivery.deliveryRowType delivRow = new ECSSOAPDelivery.deliveryRowType();
            delivRow.id = cp.External_Reference_Id__c;

            if (cp.Customer_Asset__r.Installed_Base_Id__c != null) {
                delivRow.additionalReference = new ECSSOAPDelivery.additionalRefType();
                ECSSOAPDelivery.customReferenceType addCustRefType = new ECSSOAPDelivery.customReferenceType();
                
                addCustRefType.name = 'installedBaseId';
                addCustRefType.value = cp.Customer_Asset__r.Installed_Base_Id__c;
                delivRow.additionalReference.customReference = addCustRefType;
            }

            delivRow.action = 'update';

            delivRow.location = new ECSSOAPDelivery.locationRefType();
            delivRow.location.housenumber = String.valueOf(cp.Site__r.Site_House_Number__c);
            delivRow.location.housenumberExt = cp.Site__r.Site_House_Number_Suffix__c;
            delivRow.location.zipcode = cp.Site__r.Site_Postal_Code__c;

            delivRow.quantity = Integer.valueOf(cp.Quantity__c);
            if(cp.Group__c != null) delivRow.group_x = Integer.valueOf(cp.Group__c);
            delivRow.costHeading = cp.Cost_Center__c;
            
            // when the method is called from batch, the delivery status would not be committed to database
            delivRow.status = String.isNotEmpty(contractedProductStatus) ?
                contractedProductStatus : cp.BOP_Status__c;

            req.deliveryRows.add(delivRow);
        }

        return req;
    }

    private static List<Contracted_Products__c> processContractedProducts(List<ECSSOAPDelivery.deliveryRowResponseType> deliveryRows, List<Contracted_Products__c> contractedProds){
        List<Contracted_Products__c> cpList = new List<Contracted_Products__c>();
        Map<String, Contracted_Products__c> cpMap = new Map<String, Contracted_Products__c>();
        for (Contracted_Products__c cp : contractedProds) {
            cpMap.put(cp.Customer_Asset__r.Installed_Base_Id__c, cp);
        }
        for (ECSSOAPDelivery.deliveryRowResponseType deliveryRow : deliveryRows) {
            if (deliveryRow.additionalReference != null && deliveryRow.additionalReference.customReference != null && deliveryRow.additionalReference.customReference.name == 'installedBaseId') {
                system.debug('## cpMap: ' + cpMap);
                system.debug('## value: ' + deliveryRow.additionalReference.customReference.value);
                Contracted_Products__c c = cpMap.get(deliveryRow.additionalReference.customReference.value);
                if (deliveryRow.error != null) {
                    c.BOP_Delivery_Errormessage__c = deliveryRow.error.code + ': ' + deliveryRow.error.message;
                }
                else {
                    c.BOP_Delivery_Datetime__c = system.now();
                }
                cpList.add(c);
            }
        }
  
        return cpList;
    }
}