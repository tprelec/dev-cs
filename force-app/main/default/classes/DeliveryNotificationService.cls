@RestResource(urlMapping='/deliverynotification/*')
@SuppressWarnings('PMD.AvoidDeeplyNestedIfStmts')
/**
 * @description: Updates Order data in Salesforce
 * @author: Jurgen van Westreenen
 */
global without sharing class DeliveryNotificationService {
	@HttpPost
	global static void updateOrders() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		List<Error> returnErrors = new List<Error>();
		try {
			// Deserialize JSON
			DeliveryNotification body = (DeliveryNotification) JSON.deserializeStrict(
				req.requestbody.tostring(),
				DeliveryNotification.class
			);
			// Retrieve Order Id
			String orderId = body.orderId;
			if (String.isNotBlank(orderId)) {
				List<Order__c> ords = [
					SELECT
						Id,
						BOP_Order_Status__c,
						PM_Email__c,
						Service_Ready_Date__c,
						PM_First_Name__c,
						PM_Last_Name__c,
						PM_Phone__c
					FROM Order__c
					WHERE BOP_Export_Order_Id__c = :orderId
				];
				if (!ords.isEmpty()) {
					// Update order with supplied data
					Order__c ord = ords[0];
					ord.BOP_Order_Status__c = body.status;
					if (ord.BOP_Order_Status__c != null) {
						String statusCancelled = 'Cancelled';
						String status = ord.BOP_Order_Status__c;
						if (status.contains(statusCancelled)) {
							updateCustomerAssetsStatus(ord);
						}
					}
					if (body.projectManager != null) {
						ord.PM_Email__c = body.projectManager.email;
						ord.PM_First_Name__c = body.projectManager.firstName;
						ord.PM_Last_Name__c = body.projectManager.lastName;
						ord.PM_Phone__c = body.projectManager.phone;
					}
					if (body.serviceReadyDate != null) {
						ord.Service_Ready_Date__c = Date.valueOf(body.serviceReadyDate);
						updateCustomerAssets(ord);
					}
					update ord;
				} else {
					// No Orders found
					Error err = new Error();
					err.errorCode = 'SFEC-0002';
					err.errorMessage = 'There were no records that could be updated or found in Salesforce.';
					returnErrors.add(err);
				}
			} else {
				// No Order Id supplied
				Error err = new Error();
				err.errorCode = 'SFEC-0001';
				err.errorMessage = 'Missing mandatory Input field: orderId';
				returnErrors.add(err);
			}
		} catch (Exception e) {
			// Any other occuring exceptions
			Error err = new Error();
			err.errorCode = 'SFEC-9999';
			err.errorMessage = 'An unhandled exception occured: ' + e.getMessage();
			returnErrors.add(err);
		} finally {
			// Populate response
			Response resp = new Response();
			if (returnErrors.size() > 0) {
				resp.status = 'FAILED';
				resp.errors = returnErrors;
			} else {
				resp.status = 'OK';
			}
			res.addHeader('Content-Type', 'text/json');
			res.statusCode = 200;
			res.responseBody = Blob.valueOf(JSON.serializePretty(resp));
		}
	}
	private static void updateCustomerAssets(Order__c ord) {
		List<Contracted_Products__c> cpList = [
			SELECT Id, Quantity__c, Row_Type__c, Customer_Asset__c, Customer_Asset__r.Quantity__c
			FROM Contracted_Products__c
			WHERE Order__c = :ord.Id AND Delivery_Status__c != 'Cancelled'
		];
		Set<Customer_Asset__c> caToUpdate = new Set<Customer_Asset__c>();
		for (Contracted_Products__c cp : cpList) {
			if (
				ord.Service_Ready_Date__c != null &&
				!(cp.Row_Type__c == Constants.CONTRACTED_PRODUCT_ROW_TYPE_REMOVE &&
				cp.Quantity__c == cp.Customer_Asset__r.Quantity__c)
			) {
				Customer_Asset__c ca = new Customer_Asset__c(
					Id = cp.Customer_Asset__c,
					Contract_Start_Date__c = ord.Service_Ready_Date__c
				);
				caToUpdate.add(ca);
			}
		}
		update new List<Customer_Asset__c>(caToUpdate);
	}
	/**************
	 ****This method update CustomerAssets Status
	 ****
	 ***************/
	private static void updateCustomerAssetsStatus(Order__c ord) {
		List<Contracted_Products__c> cpList = [
			SELECT
				Id,
				Quantity__c,
				Row_Type__c,
				Customer_Asset__c,
				Customer_Asset__r.Quantity__c,
				Customer_Asset__r.Billing_Status__c,
				Customer_Asset__r.Installation_Status__c
			FROM Contracted_Products__c
			WHERE
				Order__c = :ord.Id
				AND (Delivery_Status__c != 'Approved'
				OR Delivery_Status__c != 'Closed'
				OR Delivery_Status__c != 'Rollback Requested'
				OR Delivery_Status__c != 'Waiting for removal')
		];
		Set<Customer_Asset__c> caToUpdate = new Set<Customer_Asset__c>();
		for (Contracted_Products__c cp : cpList) {
			if (
				(cp.Customer_Asset__r.Billing_Status__c != null &&
				cp.Customer_Asset__r.Billing_Status__c == 'Pending Change') &&
				(cp.Customer_Asset__r.Installation_Status__c != null &&
				cp.Customer_Asset__r.Installation_Status__c == 'Pending Change')
			) {
				Customer_Asset__c ca = new Customer_Asset__c(
					Id = cp.Customer_Asset__c,
					Installation_Status__c = 'Active',
					Billing_Status__c = 'Active'
				);
				caToUpdate.add(ca);
			} else if (customerAssetsStatusCheck(cp.Customer_Asset__r.Billing_Status__c)) {
				Customer_Asset__c ca = new Customer_Asset__c(
					Id = cp.Customer_Asset__c,
					Billing_Status__c = 'Active'
				);
				caToUpdate.add(ca);
			} else if (customerAssetsStatusCheck(cp.Customer_Asset__r.Installation_Status__c)) {
				Customer_Asset__c ca = new Customer_Asset__c(
					Id = cp.Customer_Asset__c,
					Installation_Status__c = 'Active'
				);
				caToUpdate.add(ca);
			}
		}
		update new List<Customer_Asset__c>(caToUpdate);
	}

	private static Boolean customerAssetsStatusCheck(String statusStr) {
		Boolean isStatus = false;
		if (statusStr != null && statusStr == 'Pending Change') {
			isStatus = true;
		}
		return isStatus;
	}

	global class DeliveryNotification {
		public String orderId;
		public ProjectManager projectManager;
		public String status;
		public String serviceReadyDate;
	}
	global class ProjectManager {
		public String firstName;
		public String lastName;
		public String phone;
		public String email;
	}
	global class Error {
		public String errorCode;
		public String errorMessage;
	}
	global class Response {
		public String status;
		public List<Error> errors;
	}
}