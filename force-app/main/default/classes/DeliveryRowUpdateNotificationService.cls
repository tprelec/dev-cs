@RestResource(urlMapping='/deliveryrowupdatenotification/*')
@SuppressWarnings('PMD.CyclomaticComplexity, PMD.NcssMethodCount, PMD.AvoidDeeplyNestedIfStmts')
/**
 * @description: Performs MACD operations on Contracted Products
 * @author: Jurgen van Westreenen
 */
global without sharing class DeliveryRowUpdateNotificationService {
	static Savepoint sp;

	static String accountId;
	static String orderId;
	static String orderSFId;

	static Map<String, Contracted_Products__c> contractedProductMap;
	static Map<String, Product2> productMap;

	static List<DeliveryRowResult> deliveryRowResults = new List<DeliveryRowResult>();

	static Set<String> failedAddsForReplace = new Set<String>();
	static String rollbackSource = '';

	static Map<String, Id> barMap {
		get {
			if (barMap == null) {
				barMap = new Map<String, Id>();
				for (Billing_Arrangement__c ba : [
					SELECT Id, Unify_Ref_Id__c
					FROM Billing_Arrangement__c
					WHERE Financial_Account__r.BAN__r.Account__c = :accountId
				]) {
					barMap.put(ba.Unify_Ref_Id__c, ba.Id);
				}
			}
			return barMap;
		}
		set;
	}

	static Map<String, Site__c> siteMap {
		get {
			if (siteMap == null) {
				siteMap = new Map<String, Site__c>(
					[
						SELECT
							Id,
							Unify_Ref_Id__c,
							Site_House_Number__c,
							Site_House_Number_Suffix__c,
							Site_Postal_Code__c
						FROM Site__c
						WHERE Unify_Ref_Id__c != NULL AND Site_Account__c = :accountId
					]
				);
				for (Site__c s : siteMap.values()) {
					String siteKey = s.Site_Postal_Code__c + s.Site_House_Number__c;
					siteKey += String.isNotBlank(s.Site_House_Number_Suffix__c)
						? s.Site_House_Number_Suffix__c
						: '';
					if (!siteMap.containsKey(siteKey)) {
						siteMap.put(siteKey, s);
					}
				}
			}
			return siteMap;
		}
		set;
	}

	@HttpPost
	global static void processContractedProducts() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		List<Error> returnErrors = new List<Error>();

		sp = Database.setSavepoint();

		try {
			DeliveryRowUpdateNotification body = (DeliveryRowUpdateNotification) JSON.deserializeStrict(
				req.requestbody.tostring(),
				DeliveryRowUpdateNotification.class
			);
			// Retrieve Order Id
			orderId = body.orderId;
			if (String.isNotBlank(orderId)) {
				// Check for valid Order Id
				List<Order__c> orderCheck = [
					SELECT Id, Account__c
					FROM Order__c
					WHERE BOP_Export_Order_Id__c = :orderId
				];
				if (orderCheck.size() > 0) {
					contractedProductMap = getContractedProductMap();

					orderSFId = orderCheck[0].Id;
					accountId = orderCheck[0].Account__c;
					List<DeliveryRow> deliveryRowsToAdd = new List<DeliveryRow>();
					List<DeliveryRow> deliveryRowsToRemove = new List<DeliveryRow>();
					List<DeliveryRow> deliveryRowsToUpdate = new List<DeliveryRow>();

					Set<String> productCodes = new Set<String>();
					Set<String> existingCodes = GeneralUtils.getStringSetFromList(
						contractedProductMap.values(),
						'ProductCode__c'
					);
					existingCodes.remove(null);
					Set<String> rowCodes = getProductCodes(body.deliveryRows);
					productCodes.addAll(existingCodes);
					productCodes.addAll(rowCodes);

					productMap = productLookup(productCodes);

					Set<String> externalReferenceSet = new Set<String>();
					Set<String> replacedByExternalReferenceSet = new Set<String>();

					// Group Delivery Rows by type of Action
					for (DeliveryRow delRow : body.deliveryRows) {
						switch on delRow.action {
							when 'ADD' {
								externalReferenceSet.add(delRow.externalReferenceId);
								deliveryRowsToAdd.add(delRow);
							}
							when 'REMOVE' {
								replacedByExternalReferenceSet.add(
									delRow.replacedByExternalReferenceId
								);
								deliveryRowsToRemove.add(delRow);
							}
							when 'UPDATE' {
								deliveryRowsToUpdate.add(delRow);
							}
							when else {
								// Action field not supplied
								returnErrors.add(
									new Error('SFEC-0001', 'Missing mandatory Input field: action')
								);
							}
						}
					}

					if (deliveryRowsToAdd.size() > 0) {
						performAddAction(deliveryRowsToAdd, replacedByExternalReferenceSet);
					}
					if (deliveryRowsToRemove.size() > 0) {
						performRemoveAction(deliveryRowsToRemove, externalReferenceSet);
					}
					if (deliveryRowsToUpdate.size() > 0) {
						performUpdateAction(deliveryRowsToUpdate);
					}
				} else {
					// No Order found
					returnErrors.add(
						new Error(
							'SFEC-0002',
							'There were no records that could be found in Salesforce.'
						)
					);
				}
			} else {
				// No Order Id supplied
				returnErrors.add(new Error('SFEC-0001', 'Missing mandatory Input field: orderId'));
			}
		} catch (Exception e) {
			// Any other occuring exceptions
			returnErrors.add(
				new Error(
					'SFEC-9999',
					e.getLineNumber() +
					': An unhandled exception occured: ' +
					e.getMessage()
				)
			);
		} finally {
			Response resp = buildResponse(returnErrors);
			res.addHeader('Content-Type', 'text/json');
			res.statusCode = 200;
			res.responseBody = Blob.valueOf(JSON.serializePretty(resp));
		}
	}

	private static Response buildResponse(List<Error> returnErrors) {
		if (String.isNotBlank(rollbackSource)) {
			Database.rollback(sp);
			for (DeliveryRowResult deliveryRowResult : deliveryRowResults) {
				String message =
					'An error occured when trying to remove product with installed base id ' +
					rollbackSource +
					', causing a complete rollback.';
				Error err = new Error('SFEC-0009', message);
				deliveryRowResult.errors.add(err);
				deliveryRowResult.status = 'FAILED';
			}
		}
		// Populate response
		Response resp = new Response();
		if (returnErrors.size() > 0) {
			resp.status = 'FAILED';
			resp.errors = returnErrors;
		} else {
			resp.status = 'OK';
			resp.deliveryRowResults = deliveryRowResults;
		}

		return resp;
	}

	private static void performAddAction(
		List<DeliveryRow> deliveryRowsToAdd,
		Set<String> replacedByExternalReferenceSet
	) {
		Map<String, DeliveryRowResult> deliveryRowResultMap = new Map<String, DeliveryRowResult>();

		// Process inserts of new Contracted Products
		String contractId = getContractFromOrder();
		List<Contracted_Products__c> cpToProcess = new List<Contracted_Products__c>();

		for (DeliveryRow deliveryRowToAdd : deliveryRowsToAdd) {
			DeliveryRowResult deliveryRowResult = new DeliveryRowResult();
			deliveryRowResult.externalReferenceId = deliveryRowToAdd.externalReferenceId;
			deliveryRowResultMap.put(deliveryRowResult.externalReferenceId, deliveryRowResult);

			List<Error> cpErrors = new List<Error>();

			// conditions for checking if it's a replace action
			Contracted_Products__c replaceByContractedProduct = contractedProductMap.get(
				deliveryRowToAdd.replacesInstalledBaseId
			);
			Boolean isReplaceAction =
				String.isNotEmpty(deliveryRowToAdd.externalReferenceId) &&
				String.isNotEmpty(deliveryRowToAdd.replacesInstalledBaseId);
			Boolean doesReplaceByIdExistsInRemoveAction = replacedByExternalReferenceSet.contains(
				deliveryRowToAdd.externalReferenceId
			);
			Boolean isBillingStatusOnReplaceByCPNew =
				replaceByContractedProduct != null &&
				(replaceByContractedProduct.Billing_Status__c !=
				Constants.CONTRACTED_PRODUCT_BILLING_STATUS_REQUESTED) &&
				(replaceByContractedProduct.Customer_Asset__r.Billing_Status__c ==
				Constants.CUSTOMER_ASSET_BILLING_STATUS_NEW);

			// check if current row is not for a valid replace action
			Boolean invalidReplace =
				isReplaceAction &&
				!doesReplaceByIdExistsInRemoveAction &&
				!isBillingStatusOnReplaceByCPNew;
			if (invalidReplace) {
				String message = '';
				if (!doesReplaceByIdExistsInRemoveAction) {
					message = 'Action rejected as no delivery row with remove action found which matches the replace by reference external Id: {0}';
				}
				if (!isBillingStatusOnReplaceByCPNew) {
					message = 'Replace action not allowed as billing status on contracted product/customer asset: {0} is not New or the record may be deleted';
				}
				message = String.format(
					message,
					new List<String>{ deliveryRowToAdd.replacesInstalledBaseId }
				);
				cpErrors = new List<Error>{ new Error('SFEC-0009', message) };
			} else {
				// prepare the contracted product to add
				Contracted_Products__c cpAdd = getContractedProductsToAdd(
					deliveryRowToAdd,
					contractId
				);
				Boolean siteNotFound = cpAdd.Site__c != null && !siteMap.containsKey(cpAdd.Site__c);
				if (siteNotFound) {
					cpErrors.add(
						new Error(
							'SFEC-0010',
							'Supplied Site does not exist or does not have a Unify Ref Id'
						)
					);
					failedAddsForReplace.add(cpAdd.External_Reference_Id__c);
				} else {
					Product2 product = productMap.get(cpAdd.ProductCode__c);
					// Check whether a (valid) Product Code is supplied
					if (product != null) {
						cpAdd.Product__c = product.Id;
						if (
							product.Quantity_type__c !=
							Constants.CONTRACTED_PRODUCT_QUANTITY_TYPE_MONTHLY
						) {
							cpAdd.Duration__c = 1;
						}
						// in case of a replace operation, link the replace by product with newly created product
						Boolean validReplace =
							String.isNotEmpty(deliveryRowToAdd.replacesInstalledBaseId) &&
							isReplaceAction &&
							replaceByContractedProduct != null;
						if (validReplace) {
							cpAdd.Replacement_Contracted_Product__c = replaceByContractedProduct.Id;
						}
						cpToProcess.add(cpAdd);
					} else {
						cpErrors.add(
							new Error(
								'SFEC-0007',
								'Incorrect or missing Product Code: ' + cpAdd.ProductCode__c
							)
						);
						failedAddsForReplace.add(cpAdd.External_Reference_Id__c);
					}
				}
			}
			processErrors(deliveryRowResultMap, cpErrors, deliveryRowToAdd.externalReferenceId);
		}

		insertContractedProducts(cpToProcess, deliveryRowResultMap);

		deliveryRowResults.addAll(deliveryRowResultMap.values());
	}

	private static void performRemoveAction(
		List<DeliveryRow> deliveryRowsToRemove,
		Set<String> externalReferenceSet
	) {
		Map<String, DeliveryRowResult> deliveryRowResultMap = new Map<String, DeliveryRowResult>();
		if (deliveryRowsToRemove.size() > 0) {
			// Process deletion of Contracted Products

			List<Contracted_Products__c> cpToProcess = new List<Contracted_Products__c>();
			List<Customer_Asset__c> caToUpdate = new List<Customer_Asset__c>();

			Map<String, String> idToInstBaseMap = new Map<String, String>();
			// Create a map from Installed Base Id to Delivery Row record
			for (DeliveryRow deliveryRowToRemove : deliveryRowsToRemove) {
				List<Error> cpErrors = new List<Error>();

				// conditions for checking if its replace action
				Contracted_Products__c cpMatch = contractedProductMap.get(
					deliveryRowToRemove.installedBaseId
				);
				Boolean isReplaceAction = String.isNotEmpty(
					deliveryRowToRemove.replacedByExternalReferenceId
				);
				Boolean isReplaceByIdInAddAction = externalReferenceSet.contains(
					deliveryRowToRemove.replacedByExternalReferenceId
				);
				Boolean isBillingStatusOnReplaceByCPNew =
					cpMatch != null &&
					(cpMatch.Billing_Status__c !=
					Constants.CONTRACTED_PRODUCT_BILLING_STATUS_REQUESTED) &&
					(cpMatch.Customer_Asset__r.Billing_Status__c ==
					Constants.CUSTOMER_ASSET_BILLING_STATUS_NEW);

				DeliveryRowResult deliveryRowResult = new DeliveryRowResult();
				deliveryRowResult.installedBaseId = deliveryRowToRemove.installedBaseId;
				deliveryRowResultMap.put(deliveryRowResult.installedBaseId, deliveryRowResult);

				if (cpMatch != null) {
					idToInstBaseMap.put(cpMatch.Id, cpMatch.Customer_Asset__r.Installed_Base_Id__c);
					if (
						cpMatch.Customer_Asset__r.Billing_Status__c ==
						Constants.CUSTOMER_ASSET_BILLING_STATUS_CANCELLED
					) {
						deliveryRowResult.status = 'OK';
						deliveryRowResultMap.put(
							deliveryRowResult.installedBaseId,
							deliveryRowResult
						);
					} else if (!isBillingStatusOnReplaceByCPNew) {
						cpErrors = new List<Error>{
							new Error(
								'SFEC-0008',
								'No updates are allowed once the billing is requested.'
							)
						};
					} else if (isReplaceAction && !isReplaceByIdInAddAction) {
						String message = 'Action rejected as no delivery row with add action found which matches the replace by reference external Id: {0}';
						message = String.format(
							message,
							new List<String>{ deliveryRowToRemove.replacedByExternalReferenceId }
						);
						cpErrors = new List<Error>{ new Error('SFEC-0009', message) };
					} else if (
						failedAddsForReplace.contains(
							deliveryRowToRemove.replacedByExternalReferenceId
						)
					) {
						String message = 'Remove can not be performed because the replacement product with reference external Id {0} failed to be inserted.';
						message = String.format(
							message,
							new List<String>{ deliveryRowToRemove.replacedByExternalReferenceId }
						);
						cpErrors = new List<Error>{ new Error('SFEC-0009', message) };
					} else {
						Customer_Asset__c ca = cpMatch.Customer_Asset__r;
						if (
							ca.Billing_Status__c == Constants.CUSTOMER_ASSET_BILLING_STATUS_ACTIVE
						) {
							ca.Installation_Status__c = 'Active';
						} else {
							ca.Installation_Status__c = 'Removed';
							ca.Billing_Status__c = Constants.CUSTOMER_ASSET_BILLING_STATUS_CANCELLED;
						}
						caToUpdate.add(cpMatch.Customer_Asset__r);
					}
				} else {
					deliveryRowResult.status = 'OK';
					deliveryRowResultMap.put(deliveryRowResult.installedBaseId, deliveryRowResult);
				}
				processErrors(deliveryRowResultMap, cpErrors, deliveryRowToRemove.installedBaseId);
			}
			cpToProcess = updateCustomerAssets(caToUpdate, deliveryRowResultMap);
			updateCanceledCPs(cpToProcess, idToInstBaseMap);
		}
		deliveryRowResults.addAll(deliveryRowResultMap.values());
	}

	private static void performUpdateAction(List<DeliveryRow> deliveryRowsToUpdate) {
		if (deliveryRowsToUpdate.size() > 0) {
			List<Contracted_Products__c> cpToProcess = new List<Contracted_Products__c>();

			Map<String, String> idToInstBaseMap = new Map<String, String>();
			// Create a map from Installed Base Id to Delivery Row record
			for (DeliveryRow deliveryRowToUpdate : deliveryRowsToUpdate) {
				// Collect all records to update
				Contracted_Products__c cpMatch = contractedProductMap.get(
					deliveryRowToUpdate.installedBaseId
				);
				// Populate a External Reference Id to Installed Base Id map
				if (cpMatch != null) {
					cpToProcess = updateContractedProducts(
						cpMatch,
						deliveryRowToUpdate,
						idToInstBaseMap
					);
				} else {
					List<Error> cpErrors = new List<Error>{
						new Error(
							'SFEC-0002',
							'There were no records that could be found in Salesforce with installedBaseId: ' +
							deliveryRowToUpdate.installedBaseId
						)
					};
					DeliveryRowResult deliveryRowResult = new DeliveryRowResult();
					deliveryRowResult.installedBaseId = deliveryRowToUpdate.installedBaseId;
					deliveryRowResult.errors = cpErrors;
					deliveryRowResult.status = 'FAILED';
					deliveryRowResults.add(deliveryRowResult);
				}
			}
			// Update contracted products
			Database.SaveResult[] srList = Database.update(cpToProcess, false);
			for (Integer i = 0; i < srList.size(); i++) {
				DeliveryRowResult deliveryRowResult = new DeliveryRowResult();
				if (srList[i].isSuccess()) {
					deliveryRowResult.installedBaseId = idToInstBaseMap.get(cpToProcess[i].Id);
					deliveryRowResult.status = 'OK';
					if (cpToProcess[i].ProductCode__c != null) {
						deliveryRowResult.billingType = productMap.get(
								cpToProcess[i].ProductCode__c
							)
							.Billing_Type__c;
					}
					deliveryRowResults.add(deliveryRowResult);
				} else {
					// Operation failed, so get all errors
					List<Error> cpErrors = new List<Error>();
					for (Database.Error err : srList[i].getErrors()) {
						cpErrors.add(
							new Error(
								'SFEC-9999',
								'An unhandled exception occured: ' + err.getMessage()
							)
						);
					}
					deliveryRowResult.installedBaseId = idToInstBaseMap.get(cpToProcess[i].Id);
					deliveryRowResult.errors = cpErrors;
					deliveryRowResult.status = 'FAILED';
					deliveryRowResults.add(deliveryRowResult);
				}
			}
		}
	}

	private static Contracted_Products__c getContractedProductsToAdd(
		DeliveryRow deliveryRow,
		String contractId
	) {
		// prepare the contracted product to add
		Contracted_Products__c cpAdd = new Contracted_Products__c(
			Order__c = orderSFId,
			External_Reference_Id__c = deliveryRow.externalReferenceId,
			CLC__c = 'Acq', // Required and fixed for BOP
			VF_Contract__c = contractId,
			Last_BOP_Action__c = deliveryRow.action
		);
		// collect the extra fields from attribute
		cpAdd = copyFieldValueFromAttributes(cpAdd, deliveryRow.attributes);

		return cpAdd;
	}

	private static String getContractFromOrder() {
		Order__c order = [
			SELECT VF_Contract__c
			FROM Order__c
			WHERE BOP_Export_Order_Id__c = :orderId
		];
		return order.VF_Contract__c;
	}

	private static Set<String> getProductCodes(List<DeliveryRow> delivRows) {
		// Creates a mapping from Product Code to Product Id
		Set<String> productCodes = new Set<String>();
		for (DeliveryRow delivRow : delivRows) {
			if (delivRow.attributes != null) {
				for (Attribute attr : delivRow.attributes) {
					if (attr.key == 'articleCode') {
						productCodes.add(attr.value);
					}
				}
			}
		}
		return productCodes;
	}

	private static Map<String, Product2> productLookup(Set<String> productCodes) {
		Map<String, Product2> returnMap = new Map<String, Product2>();
		for (Product2 prod : [
			SELECT Id, ProductCode, Billing_Type__c, Quantity_type__c
			FROM Product2
			WHERE ProductCode IN :productCodes
		]) {
			returnMap.put(prod.ProductCode, prod);
		}
		return returnMap;
	}

	private static Map<String, Contracted_Products__c> getContractedProductMap() {
		Map<String, Contracted_Products__c> relatedContractedProductMap = new Map<String, Contracted_Products__c>();

		// prepare map of contracted products with install base
		for (Contracted_Products__c contractedProduct : [
			SELECT
				Id,
				Customer_Asset__r.Installed_Base_Id__c,
				External_Reference_Id__c,
				ProductCode__c,
				Billing_Status__c,
				Delivery_Status__c,
				Customer_Asset__r.Billing_Status__c,
				Customer_Asset__r.Installation_Status__c,
				Site__c
			FROM Contracted_Products__c
			WHERE
				Order__r.BOP_Export_Order_Id__c = :orderId
				AND Customer_Asset__c != NULL
				AND Customer_Asset__r.Installed_Base_Id__c != NULL
		]) {
			relatedContractedProductMap.put(
				contractedProduct.Customer_Asset__r.Installed_Base_Id__c,
				contractedProduct
			);
		}

		return relatedContractedProductMap;
	}

	private static Contracted_Products__c copyFieldValueFromAttributes(
		Contracted_Products__c cp,
		List<Attribute> attribs
	) {
		// Processes any supplied attributes
		String siteKey = '';
		String sitePostalCode = '';
		String siteHouseNo = '';
		String siteHouseNoExt = '';
		for (Attribute attrib : attribs) {
			switch on attrib.key {
				when 'articleCode' {
					cp.ProductCode__c = String.isBlank(cp.ProductCode__c)
						? attrib.value
						: cp.ProductCode__c;
				}
				when 'billingArrangementId' {
					cp.Billing_Arrangement__c = barMap.get(attrib.value);
				}
				when 'deliveryDate' {
					cp.Delivery_Date__c = Date.valueOf(attrib.value);
				}
				when 'discount' {
					cp.Discount__c = Decimal.valueOf(attrib.value);
				}
				when 'duration' {
					cp.Duration__c = Integer.valueOf(attrib.value);
				}
				when 'managedItemId' {
					cp.BOP_Managed_Item_Id__c = attrib.value;
				}
				when 'price' {
					cp.Gross_List_Price__c = Decimal.valueOf(attrib.value);
				}
				when 'quantity' {
					cp.Quantity__c = Integer.valueOf(attrib.value);
				}
				when 'siteId' {
					cp.Site__c = cp.Site__c == null
						? (String.isNotBlank(attrib.value) ? attrib.value : null)
						: cp.Site__c;
				}
				when 'startInvoicingDate' {
					cp.Start_Invoicing_Date__c = Date.valueOf(attrib.value);
				}
				when 'status' {
					cp.Delivery_Status__c = attrib.value;
				}
				when 'rowType' {
					cp.Row_Type__c = attrib.value;
				}
				when 'costCenter' {
					cp.Cost_Center__c = attrib.value;
				}
				when 'purchaseOrder' {
					cp.PO_Number__c = attrib.value;
				}
				when 'sitePostalCode' {
					sitePostalCode = attrib.value;
				}
				when 'siteHouseNumber' {
					siteHouseNo = attrib.value;
				}
				when 'siteHouseNumberSuffix' {
					siteHouseNoExt = attrib.value;
				}
			}
		}

		siteKey = sitePostalCode + siteHouseNo + siteHouseNoExt;
		if (cp.Site__c == null && String.isNotBlank(siteKey) && siteMap.containsKey(siteKey)) {
			cp.Site__c = siteMap.get(siteKey).Id;
		}

		return cp;
	}

	private static void insertContractedProducts(
		List<Contracted_Products__c> cpToProcess,
		Map<String, DeliveryRowResult> deliveryRowResultMap
	) {
		// Write changes on contracted products
		Database.SaveResult[] srList = Database.insert(cpToProcess, false);

		Map<Id, Contracted_Products__c> ibMap = new Map<Id, Contracted_Products__c>(
			[
				SELECT Id, Customer_Asset__r.Installed_Base_Id__c
				FROM Contracted_Products__c
				WHERE Id IN :cpToProcess
			]
		);

		for (Integer i = 0; i < srList.size(); i++) {
			String externalReferenceId = cpToProcess[i].External_Reference_Id__c;
			DeliveryRowResult processedDeliveryRowResult = deliveryRowResultMap.get(
				externalReferenceId
			);
			if (srList[i].isSuccess()) {
				processedDeliveryRowResult.externalReferenceId = cpToProcess[i]
					.External_Reference_Id__c;
				processedDeliveryRowResult.installedBaseId = ibMap.get(cpToProcess[i].Id)
					.Customer_Asset__r.Installed_Base_Id__c;
				processedDeliveryRowResult.status = 'OK';
				processedDeliveryRowResult.billingType = productMap.get(
						cpToProcess[i].ProductCode__c
					)
					.Billing_Type__c;
			} else {
				// Operation failed, so get all errors
				List<Error> cpErrors = new List<Error>();
				for (Database.Error err : srList[i].getErrors()) {
					cpErrors.add(
						new Error(
							'SFEC-9999',
							'An unhandled exception occured: ' + err.getMessage()
						)
					);
				}
				processedDeliveryRowResult.externalReferenceId = cpToProcess[i]
					.External_Reference_Id__c;
				processedDeliveryRowResult.errors = cpErrors;
				processedDeliveryRowResult.status = 'FAILED';

				failedAddsForReplace.add(cpToProcess[i].External_Reference_Id__c);
			}
			deliveryRowResultMap.put(externalReferenceId, processedDeliveryRowResult);
		}
	}

	private static List<Contracted_Products__c> updateContractedProducts(
		Contracted_Products__c cpMatch,
		DeliveryRow deliveryRowToUpdate,
		Map<String, String> idToInstBaseMap
	) {
		List<Contracted_Products__c> cpToProcess = new List<Contracted_Products__c>();

		if (cpMatch.Billing_Status__c != Constants.CONTRACTED_PRODUCT_BILLING_STATUS_REQUESTED) {
			idToInstBaseMap.put(cpMatch.Id, cpMatch.Customer_Asset__r.Installed_Base_Id__c);
			Contracted_Products__c cp = new Contracted_Products__c();
			cp.Id = cpMatch.Id;
			if (String.isBlank(cpMatch.External_Reference_Id__c)) {
				cp.External_Reference_Id__c = deliveryRowToUpdate.externalReferenceId;
			}
			cp.Last_BOP_Action__c = deliveryRowToUpdate.action;
			cp.ProductCode__c = cpMatch.ProductCode__c;
			cp.Site__c = cpMatch.Site__c;
			cp = copyFieldValueFromAttributes(cp, deliveryRowToUpdate.attributes);
			Boolean resetBillingStatus =
				cpMatch.Billing_Status__c == Constants.CONTRACTED_PRODUCT_BILLING_STATUS_FAILED &&
				((String.isNotBlank(cp.Delivery_Status__c) &&
				cp.Delivery_Status__c == Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED) ||
				(String.isBlank(cp.Delivery_Status__c) &&
				cpMatch.Delivery_Status__c ==
				Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED));
			if (resetBillingStatus) {
				cp.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
			}
			// If Product Code is supplied, process the supplied data
			if (cp.ProductCode__c != null) {
				Product2 product = productMap.get(cp.ProductCode__c);
				if (
					product != null &&
					product.Quantity_type__c != Constants.CONTRACTED_PRODUCT_QUANTITY_TYPE_MONTHLY
				) {
					cp.Duration__c = 1;
				}
				if (product != null) {
					cp.Product__c = product.Id;
					cpToProcess.add(cp);
				} else {
					List<Error> cpErrors = new List<Error>{
						new Error(
							'SFEC-0007',
							'Incorrect or missing Product Code: ' + cp.ProductCode__c
						)
					};
					DeliveryRowResult deliveryRowResult = new DeliveryRowResult();
					deliveryRowResult.installedBaseId = cpMatch.Customer_Asset__r.Installed_Base_Id__c;
					deliveryRowResult.errors = cpErrors;
					deliveryRowResult.status = 'FAILED';
					deliveryRowResults.add(deliveryRowResult);
				}
			} else {
				// ...otherwise just continue processing
				cpToProcess.add(cp);
			}
		} else {
			List<Error> cpErrors = new List<Error>{
				new Error('SFEC-0008', 'No updates are allowed once the billing is requested.')
			};
			DeliveryRowResult deliveryRowResult = new DeliveryRowResult();
			deliveryRowResult.installedBaseId = deliveryRowToUpdate.installedBaseId;
			deliveryRowResult.errors = cpErrors;
			deliveryRowResult.status = 'FAILED';
			deliveryRowResults.add(deliveryRowResult);
		}
		return cpToProcess;
	}

	private static List<Contracted_Products__c> updateCustomerAssets(
		List<Customer_Asset__c> caToUpdate,
		Map<String, DeliveryRowResult> deliveryRowResultMap
	) {
		List<Contracted_Products__c> cpToProcess = new List<Contracted_Products__c>();
		Database.SaveResult[] srList = Database.update(caToUpdate, false);
		for (Integer i = 0; i < srList.size(); i++) {
			String installBaseId = caToUpdate[i].Installed_Base_Id__c;
			DeliveryRowResult processedDeliveryRowResult = deliveryRowResultMap.get(installBaseId);
			if (srList[i].isSuccess()) {
				processedDeliveryRowResult.installedBaseId = installBaseId;
				String cpId = contractedProductMap.get(caToUpdate[i].Installed_Base_Id__c).Id;
				cpToProcess.add(new Contracted_Products__c(Id = cpId));
				processedDeliveryRowResult.status = 'OK';
			} else {
				// Operation failed, so get all errors
				List<Error> cpErrors = new List<Error>();
				for (Database.Error err : srList[i].getErrors()) {
					cpErrors.add(
						new Error(
							'SFEC-9999',
							'An unhandled exception occured, causing a complete rollback: ' +
							err.getMessage()
						)
					);
				}

				processedDeliveryRowResult.errors = cpErrors;
				processedDeliveryRowResult.status = 'FAILED';
				rollbackSource = installBaseId;
			}

			deliveryRowResultMap.put(installBaseId, processedDeliveryRowResult);
		}

		for (Contracted_Products__c cpCancel : cpToProcess) {
			cpCancel.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_CANCELLED;
			cpCancel.Billing_Status__c = '';
			cpCancel.Last_BOP_Action__c = 'REMOVE';
		}

		return cpToProcess;
	}

	private static void updateCanceledCPs(
		List<Contracted_Products__c> cpToProcess,
		Map<String, String> idToInstBaseMap
	) {
		Database.SaveResult[] srCancelList = Database.update(cpToProcess, false);
		for (Integer i = 0; i < srCancelList.size(); i++) {
			DeliveryRowResult deliveryRowResult = new DeliveryRowResult();
			if (!srCancelList[i].isSuccess()) {
				// Operation failed, so get all errors
				List<Error> cpErrors = new List<Error>();
				for (Database.Error err : srCancelList[i].getErrors()) {
					cpErrors.add(
						new Error(
							'SFEC-9999',
							'An unhandled exception occured, causing a complete rollback: ' +
							err.getMessage()
						)
					);
				}
				deliveryRowResult.installedBaseId = idToInstBaseMap.get(cpToProcess[i].Id);
				deliveryRowResult.errors = cpErrors;
				deliveryRowResult.status = 'FAILED';
				deliveryRowResults.add(deliveryRowResult);
				rollbackSource = idToInstBaseMap.get(cpToProcess[i].Id);
			}
		}
	}

	private static void processErrors(
		Map<String, DeliveryRowResult> deliveryRowResultMap,
		List<Error> cpErrors,
		String key
	) {
		if (!cpErrors.isEmpty()) {
			DeliveryRowResult failedDeliveryRowResult = deliveryRowResultMap.get(key);
			failedDeliveryRowResult.errors = cpErrors;
			failedDeliveryRowResult.status = 'FAILED';
			deliveryRowResultMap.put(key, failedDeliveryRowResult);
		}
	}

	@TestVisible
	class DeliveryRowUpdateNotification {
		@TestVisible
		String orderId;
		@TestVisible
		List<DeliveryRow> deliveryRows;
	}
	@TestVisible
	class DeliveryRow {
		@TestVisible
		String installedBaseId;
		@TestVisible
		String externalReferenceId;
		@TestVisible
		String replacesInstalledBaseId;
		@TestVisible
		String replacedByExternalReferenceId;
		@TestVisible
		String action;
		@TestVisible
		List<Attribute> attributes;
	}
	@TestVisible
	class Attribute {
		@TestVisible
		String key;
		@TestVisible
		String value;
	}
	@TestVisible
	class Response {
		@TestVisible
		String status;
		@TestVisible
		List<DeliveryRowResult> deliveryRowResults;
		@TestVisible
		List<Error> errors;
	}
	@TestVisible
	class DeliveryRowResult {
		@TestVisible
		String installedBaseId;
		@TestVisible
		String externalReferenceId;
		@TestVisible
		String status;
		@TestVisible
		String billingType;
		@TestVisible
		List<Error> errors;
	}
	@TestVisible
	class Error {
		@TestVisible
		String errorCode;
		@TestVisible
		String errorMessage;
		@TestVisible
		Error(String errorCode, String errorMessage) {
			this.errorCode = errorCode;
			this.errorMessage = errorMessage;
		}
	}
}