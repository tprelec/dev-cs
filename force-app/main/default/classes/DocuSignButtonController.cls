public without sharing class DocuSignButtonController {
	@TestVisible
	private static Id opportunityId;
	@TestVisible
	private static Opportunity opp;

	@AuraEnabled
	public static String prepareOpportunityAndGetURL(Id inputOpportunityId) {
		try {
			opportunityId = inputOpportunityId;

			new Flow.Interview.Docusign_Signer_Update_Autolaunched(
					new Map<String, Object>{ 'recordId' => opportunityId }
				)
				.start();

			opp = [
				SELECT
					Id,
					Docusign_Attachment_Status__c,
					OwnerId,
					Enterprise_Legal_Case__c,
					DS_Mail_Message_Digital_Signing__c,
					DS_Mail_Subject_Digital_Signing__c,
					DS_Authorized_Internal_Signer_1_Mail__c,
					Sales_Director_Name__c,
					DS_Authorized_Internal_Signer_2_Mail__c,
					DS_Authorized_Internal_Signer_2_Name__c,
					DS_Authorized_to_sign_1st_Mail__c,
					DS_Authorized_to_sign_1st_Name__c,
					DS_Authorized_to_sign_2nd_Mail__c,
					DS_Authorized_to_sign_2nd_Name__c,
					DocuSign_On_Click_Option__c
				FROM Opportunity
				WHERE Id = :opportunityId
			];

			if (
				String.isBlank(opp.Docusign_Attachment_Status__c) ||
				opp.Docusign_Attachment_Status__c == Constants.DOCUSIGN_ATTACHMENT_STATUS_DEFAULT
			) {
				List<CS_DocItem> documents = CS_OppDocController.getDocuments(opp.Id);
				if (opp.Enterprise_Legal_Case__c == null) {
					DocumentService.getInstance()
						.attachDocumentsToSObjectForRegularAttachments(opp.Id, documents);
				}
				if (opp.Enterprise_Legal_Case__c != null) {
					Set<Id> caseAttachmentIds = DocumentService.getInstance()
						.getCaseAttachmentIdsByCaseId(opp.Enterprise_Legal_Case__c);
					DocumentService.getInstance()
						.attachDocumentsToSObjectFromSObject(opp.Id, caseAttachmentIds);
				}
				DocumentService.getInstance()
					.attachDocumentsToSObjectForContractConditions(opp.Id, documents);

				opp.Docusign_Attachment_Status__c = Constants.DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED;
				update opp;
			}

			return getDocuSignPagereference().getUrl();
		} catch (DmlException e) {
			if (e.getMessage().contains('INVALID_CROSS_REFERENCE_KEY')) {
				throw getAuraException(
					'Something went wrong in inserting the Opportunity attachments. Make sure the related contract conditions refer to existing records in this org. ' +
					e.getMessage()
				);
			}
			throw getAuraException('Something went wrong: ' + e.getMessage());
		} catch (Exception e) {
			throw getAuraException('Something went wrong: ' + e.getMessage());
		}
	}

	@TestVisible
	private static Pagereference getDocuSignPagereference() {
		validateEmptyParams(
			new Map<String, String>{
				'opp.DS_Authorized_Internal_Signer_1_Mail__c' => opp.DS_Authorized_Internal_Signer_1_Mail__c,
				'opp.Sales_Director_Name__c' => opp.Sales_Director_Name__c,
				'opp.DS_Authorized_to_sign_1st_Mail__c' => opp.DS_Authorized_to_sign_1st_Mail__c,
				'opp.DS_Authorized_to_sign_1st_Name__c' => opp.DS_Authorized_to_sign_1st_Name__c
			}
		);

		Map<String, String> params = new Map<String, String>{
			'SourceID' => Opp.Id,
			'DSEID' => '0',
			'CEM' => opp.DS_Mail_Message_Digital_Signing__c,
			'CES' => opp.DS_Mail_Subject_Digital_Signing__c,
			'OCO' => String.isBlank(opp.DocuSign_On_Click_Option__c)
				? 'Send'
				: opp.DocuSign_On_Click_Option__c,
			'LF' => '1',
			'CRL' => 'Email~' +
			opp.DS_Authorized_Internal_Signer_1_Mail__c +
			';' +
			'LastName~' +
			opp.Sales_Director_Name__c +
			';' +
			'RoutingOrder~1' +
			';' +
			'Role~Signer 1, ' +
			'Email~' +
			opp.DS_Authorized_to_sign_1st_Mail__c +
			';' +
			'LastName~' +
			opp.DS_Authorized_to_sign_1st_Name__c +
			';' +
			'RoutingOrder~2' +
			';' +
			'Role~Signer 3',
			'CCRM' => 'Signer 1~Signer 1;Signer 3~Signer 3',
			'CCTM' => 'Signer 1~Signer;Signer 3~Signer'
		};

		if (String.isNotBlank(opp.DS_Authorized_to_sign_2nd_Name__c)) {
			validateEmptyParams(
				new Map<String, String>{
					'opp.DS_Authorized_to_sign_2nd_Mail__c' => opp.DS_Authorized_to_sign_2nd_Mail__c,
					'opp.DS_Authorized_to_sign_2nd_Name__c' => opp.DS_Authorized_to_sign_2nd_Name__c
				}
			);
			params.put(
				'CRL',
				params.get('CRL') +
				',Email~' +
				opp.DS_Authorized_to_sign_2nd_Mail__c +
				';' +
				'LastName~' +
				opp.DS_Authorized_to_sign_2nd_Name__c +
				';' +
				'RoutingOrder~2' +
				';' +
				'Role~Signer 4'
			);
			params.put('CCRM', params.get('CCRM') + ';Signer 4~Signer 4');
			params.put('CCTM', params.get('CCTM') + ';Signer 4~Signer');
		}

		if (String.isNotBlank(opp.DS_Authorized_Internal_Signer_2_Name__c)) {
			validateEmptyParams(
				new Map<String, String>{
					'opp.DS_Authorized_Internal_Signer_2_Mail__c' => opp.DS_Authorized_Internal_Signer_2_Mail__c,
					'opp.DS_Authorized_Internal_Signer_2_Name__c' => opp.DS_Authorized_Internal_Signer_2_Name__c
				}
			);
			params.put(
				'CRL',
				params.get('CRL') +
				',Email~' +
				opp.DS_Authorized_Internal_Signer_2_Mail__c +
				';' +
				'LastName~' +
				opp.DS_Authorized_Internal_Signer_2_Name__c +
				';' +
				'RoutingOrder~1' +
				';' +
				'Role~Signer 2'
			);
			params.put('CCRM', params.get('CCRM') + ';Signer 2~Signer 2');
			params.put('CCTM', params.get('CCTM') + ';Signer 2~Signer');
		}

		PageReference ref = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope');
		ref.setRedirect(true);
		ref.getParameters().putAll(params);
		return ref;
	}

	@TestVisible
	private static void validateEmptyParams(Map<String, String> paramsToValidateIfEmpty) {
		List<String> emptyParams = new List<String>();
		for (String param : paramsToValidateIfEmpty.keySet()) {
			if (String.isBlank(paramsToValidateIfEmpty.get(param))) {
				emptyParams.add(param);
			}
		}
		if (emptyParams.size() > 0) {
			throw new DocuSignButtonControllerException(
				'Missing required params on the Opportunity: ' + emptyParams
			);
		}
	}

	private static AuraHandledException getAuraException(String errorMessage) {
		AuraHandledException exceptionToThrow = new AuraHandledException(errorMessage);
		exceptionToThrow.setMessage(errorMessage);
		return exceptionToThrow;
	}

	public class DocuSignButtonControllerException extends Exception {
	}
}