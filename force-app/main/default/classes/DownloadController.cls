public class DownloadController {
	
	public String orderId{get; private set;}
	
	public String attachmentId{get; private set;}
	
	public DownloadController(){
		if (orderId == null) orderId = ApexPages.currentPage().getParameters().get('orderId');
		if (attachmentId == null) attachmentId = [SELECT Id 
												  FROM Attachment 
												  WHERE ParentID =: ApexPages.currentPage().getParameters().get('orderId')
												  LIMIT 1].Id;
	}
	
	public pageReference backTo(){
		pageReference pr = new pageReference('/' + orderId);		
		return pr;
	}
}