/**
 * @description			This class is responsible for exporting customers to ECS.
 * @author				Guy Clairbois
 */
public class ECSCompanyService extends ECSSOAPBaseWebService implements IWebService<List<Request>, List<Response>> {

	/**
	 * @description			This is the request which will be sent to ECS update a 
	 *						customer. 
	 */
	public class Request {
        public String banNumber;
        public String corporateId;
        public String bopCode;
        public String referenceId;    	
        public String name;
        public String accountManagerEbu;
        public String phone;
        public String fax;
        public String email;
        public String countryId;
        public String kvkNumber;
        public String website;
        //public String nameAlias1;
        //public String nameAlias2;
        public String billingStreet;
        public String billingPostalCode;
        public String billingCity;
        public String billingCountry;
        public String dealerCode;
        public String companyGroup;
        public String resellerBanNumber;
        public String resellerCorporateId;
        public String resellerBopCode;
        public String resellerReferenceId;
        public String parentCompanyBanNumber;
        public String parentCompanyCorporateId;
        public String parentCompanyBopCode;
        public String parentCompanyReferenceId;
        public String internalInvoicing;
        public String requestType;
	}
	
	
	/**
	 * @description			This is the response that will be returned after making a request to ECS.
	 */
	public class Response {

        //public String ban;
        public String corporateId;
        public String bopCode;
        public String errorCode;
        public String errorMessage;
        public String referenceId;
	}
	
	
	private Request[] requests;
	private Response[] responses;
	
	
	/**
	 * @description			The constructor is responsible for setting the default web service configuration.
	 */
	public ECSCompanyService(){
		if(!Test.isRunningTest()){
			setWebServiceConfig( WebServiceConfigLocator.getConfig('ECSSOAPCompany') );
		}
		else setWebServiceConfig( WebServiceConfigLocator.createConfig());
	}
	
	
	/**
	 * @description			This sets the customer data to create/update customer details in ECS.
	 */
	public void setRequest(Request[] request){
		this.requests = request;
	}
	

	public void makeRequest(String requestType){
		if(requestType=='createCustomer'){
			makeCreateRequest();
		} else if(requestType=='updateCustomer'){
			makeUpdateRequest();
		} else if(requestType=='createReseller'){
			makeResellerCreateRequest();
		} else if(requestType=='updateReseller'){
			makeResellerUpdateRequest();
		}
	}

	/**
	 * @description			This will make the generic request to ECS.
	 */
	public void genericRequest()
	{
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPCompany.companySoap service = new ECSSOAPCompany.companySoap(); 
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPCompany.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
	}
	
	/**
	 * @description			This will make the request to ECS to create customers with the request.
	 */
	public void makeCreateRequest(){
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPCompany.companySoap service = new ECSSOAPCompany.companySoap(); 
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPCompany.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		
		List<ECSSOAPCompany.customerCreateType> createRequest = buildCustomerCreateUsingRequest();
		System.debug('createRequest: ' + JSON.serialize(createRequest));
		
		// create queryResult
		List<ECSSOAPCompany.companyResponseType> createResults;
			
		//if (Test.isRunningTest()) {
			// unit test cannot do a real callout, so create a result
			//createResult = TestCustomerCreateService.setupDummyResponse();
		//} else {
			try {
				system.debug(createRequest);
				createResults = service.createCustomers(createRequest);
				system.debug(createResults);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('Error received from BOP when attempting to create the customer(s): '+ex.getMessage(), ex);
			}
		//}
		
		parseResponse(createResults);
	}
		

	/**
	 * @description			This will make the request to ECS to update customers with the request.
	 */		
	public void makeUpdateRequest(){	
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPCompany.companySoap service = new ECSSOAPCompany.companySoap(); 
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPCompany.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		
		List<ECSSOAPCompany.customerUpdateType> updateRequest = buildCustomerUpdateUsingRequest();
		System.debug('updateRequest: ' + JSON.serialize(updateRequest));
	
		// create queryResult
		List<ECSSOAPCompany.companyResponseType> updateResults;
			
		if (Test.isRunningTest()) {
			// unit test cannot do a real callout, so create a result
			//createResult = TestCustomerCreateService.setupDummyResponse();
		} else {
			try {
				updateResults = service.updateCustomers(updateRequest);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('Error received from BOP when attempting to update the customer(s): '+ex.getMessage(), ex);
			}
		}
		
		parseResponse(updateResults);
	}

	/**
	 * @description			This will make the request to ECS to create customers with the request.
	 */
	public void makeResellerCreateRequest(){
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPCompany.companySoap service = new ECSSOAPCompany.companySoap(); 
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPCompany.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		
		List<ECSSOAPCompany.resellerCreateType> createRequest = buildResellerCreateUsingRequest();
		System.debug('createRequest: ' + JSON.serialize(createRequest));
		
		// create queryResult
		List<ECSSOAPCompany.companyResponseType> createResults;
			
		//if (Test.isRunningTest()) {
			// unit test cannot do a real callout, so create a result
			//createResult = TestCustomerCreateService.setupDummyResponse();
		//} else {
			try {
				system.debug(createRequest);
				createResults = service.createResellers(createRequest);
				system.debug(createResults);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('Error received from BOP when attempting to create the customer(s): '+ex.getMessage(), ex);
			}
		//}
		
		parseResponse(createResults);
	}
		

	/**
	 * @description			This will make the request to ECS to update customers with the request.
	 */		
	public void makeResellerUpdateRequest(){	
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPCompany.companySoap service = new ECSSOAPCompany.companySoap(); 
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPCompany.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		
		List<ECSSOAPCompany.resellerUpdateType> updateRequest = buildResellerUpdateUsingRequest();
		System.debug('updateRequest: ' + JSON.serialize(updateRequest));
	
		// create queryResult
		List<ECSSOAPCompany.companyResponseType> updateResults;
			
		if (Test.isRunningTest()) {
			// unit test cannot do a real callout, so create a result
			//createResult = TestCustomerCreateService.setupDummyResponse();
		} else {
			try {
				updateResults = service.updateResellers(updateRequest);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('Error received from BOP when attempting to update the customer(s): '+ex.getMessage(), ex);
			}
		}
		
		parseResponse(updateResults);
	}
	
	private ECSSOAPCompany.authenticationHeader_element buildHeaderUsingConfig(){
		ECSSOAPCompany.authenticationHeader_element header = new ECSSOAPCompany.authenticationHeader_element();
		header.applicationId = 'sfdc';
		header.username = webServiceConfig.getUsername();
		header.password = webServiceConfig.getPassword();
		return header;
	}
	

	/**
	 * @description			This will build the create request object which will be sent to ECS. 
	 */
	private List<ECSSOAPCompany.customerCreateType> buildCustomerCreateUsingRequest(){
		List<ECSSOAPCompany.customerCreateType> requestList = new List<ECSSOAPCompany.customerCreateType>();
		 
		for(Request request :this.requests){
			ECSSOAPCompany.customerCreateType thisCustomer = new ECSSOAPCompany.customerCreateType(); 
			
			thisCustomer.accountManagerEbu = request.accountManagerEbu;
			thisCustomer.banNumber = request.banNumber;
			
			/*ECSSOAPCompany.billType billingAddress = new ECSSOAPCompany.billType();
			billingAddress.zipcode = request.billingPostalCode;
			billingAddress.name = request.name; 
			billingAddress.address = request.billingStreet;
			billingAddress.city = request.billingCity;
			billingAddress.countryId = request.countryId; // TODO: find out what countryID to use
			billingAddress.contact = null; //TODO: find out what to put here
			billingAddress.phone = null; //TODO: find out what to put here
			thisCustomer.bill = billingAddress;*/
			
			thisCustomer.bopCode = request.bopCode;
			thisCustomer.referenceId = request.referenceId;
			thisCustomer.corporateId = request.corporateId;
			thisCustomer.countryId = request.countryId; // ??
			//thisCustomer.email = request.email;
			thisCustomer.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisCustomer.internalInvoicing = request.internalInvoicing;
			thisCustomer.kvkNumber = request.kvkNumber;
			thisCustomer.name = request.name;
			if(thisCustomer.name.length() > 100) thisCustomer.name = thisCustomer.name.subString(0,100);
			//thisCustomer.nameAlias1 = request.nameAlias1;
			//thisCustomer.nameAlias2 = request.nameAlias2;
			
			if(request.parentCompanyReferenceId != null){
				// only create a parentCompany entry if there is a parent company
				ECSSOAPCompany.companyRefType parentCompany = new ECSSOAPCompany.companyRefType();
				parentCompany.banNumber = request.parentCompanyBanNumber;
				parentCompany.bopCode = request.parentCompanyBopCode;
				parentCompany.referenceId = request.parentCompanyReferenceId;
				parentCompany.corporateId = request.parentCompanyCorporateId;
				thisCustomer.parentCompany = parentCompany;
			}
			
			thisCustomer.phone = StringUtils.cleanNLPhoneNumber(request.phone);
			
			if(request.resellerReferenceId != null){
				// only create a reseller entry if there is a reseller
				ECSSOAPCompany.companyRefType reseller = new ECSSOAPCompany.companyRefType();
				reseller.banNumber = request.resellerBanNumber;
				reseller.bopCode = request.resellerBopCode;
				reseller.referenceId = request.resellerReferenceId;
				reseller.corporateId = request.resellerCorporateId;
				thisCustomer.reseller = reseller;
			}
			thisCustomer.website = request.website;
			
			requestList.add(thisCustomer);			
		}
		
		return requestList;
	}

	
	/**
	 * @description			This will build the update request object which will be sent to ECS. 
	 */
	private List<ECSSOAPCompany.customerUpdateType> buildCustomerUpdateUsingRequest(){
		List<ECSSOAPCompany.customerUpdateType> requestList = new List<ECSSOAPCompany.customerUpdateType>();
		
		for(Request request :this.requests){
			ECSSOAPCompany.customerUpdateType thisCustomer = new ECSSOAPCompany.customerUpdateType(); 
			
			thisCustomer.accountManagerEbu = request.accountManagerEbu;
			thisCustomer.banNumber = request.banNumber;
			
			/*ECSSOAPCompany.billType billingAddress = new ECSSOAPCompany.billType();
			billingAddress.zipcode = request.billingPostalCode;
			billingAddress.name = request.name;
			billingAddress.address = request.billingStreet;
			billingAddress.city = request.billingCity;
			billingAddress.countryId = request.countryId; // TODO: find out what countryID to use
			billingAddress.contact = null; //TODO: find out what to put here
			billingAddress.phone = null; //TODO: find out what to put here
			thisCustomer.bill = billingAddress;*/
			
			thisCustomer.bopCode = request.bopCode;
			thisCustomer.referenceId = request.referenceId;
			thisCustomer.corporateId = request.corporateId;
			thisCustomer.countryId = request.countryId; // ??
			//thisCustomer.email = request.email;
			thisCustomer.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisCustomer.internalInvoicing = request.internalInvoicing;
			thisCustomer.kvkNumber = request.kvkNumber;
			thisCustomer.name = request.name;
			if(thisCustomer.name.length() > 100) thisCustomer.name = thisCustomer.name.subString(0,100);
		
			//thisCustomer.nameAlias1 = request.nameAlias1;
			//thisCustomer.nameAlias2 = request.nameAlias2;
			
			if(request.parentCompanyReferenceId != null){
				// only create a parentCompany entry if there is a parent company
				ECSSOAPCompany.companyRefType parentCompany = new ECSSOAPCompany.companyRefType();
				parentCompany.banNumber = request.parentCompanyBanNumber;
				parentCompany.bopCode = request.parentCompanyBopCode;
				parentCompany.referenceId = request.parentCompanyReferenceId;
				parentCompany.corporateId = request.parentCompanyCorporateId;
				thisCustomer.parentCompany = parentCompany;
			}
			
			thisCustomer.phone = StringUtils.cleanNLPhoneNumber(request.phone);
			
			if(request.resellerReferenceId != null){
				// only create a reseller entry if there is a reseller
				ECSSOAPCompany.companyRefType reseller = new ECSSOAPCompany.companyRefType();
				reseller.banNumber = request.resellerBanNumber;
				reseller.bopCode = request.resellerBopCode;
				reseller.referenceId = request.resellerReferenceId;
				reseller.corporateId = request.resellerCorporateId;
				thisCustomer.reseller = reseller;
			}
			
			thisCustomer.website = request.website;
			thisCustomer.companyGroup = request.companyGroup;
			
			requestList.add(thisCustomer);			
		}
		
		return requestList;			

	}

	/**
	 * @description			This will build the create request object which will be sent to ECS. 
	 */
	private List<ECSSOAPCompany.resellerCreateType> buildResellerCreateUsingRequest(){
		List<ECSSOAPCompany.resellerCreateType> requestList = new List<ECSSOAPCompany.resellerCreateType>();
		 
		for(Request request :this.requests){
			ECSSOAPCompany.resellerCreateType thisCustomer = new ECSSOAPCompany.resellerCreateType(); 
			
			thisCustomer.accountManagerEbu = request.accountManagerEbu;
			thisCustomer.banNumber = request.banNumber;
			
			/*ECSSOAPCompany.billType billingAddress = new ECSSOAPCompany.billType();
			billingAddress.zipcode = request.billingPostalCode;
			billingAddress.name = request.name; 
			billingAddress.address = request.billingStreet;
			billingAddress.city = request.billingCity;
			billingAddress.countryId = request.countryId; // TODO: find out what countryID to use
			billingAddress.contact = null; //TODO: find out what to put here
			billingAddress.phone = null; //TODO: find out what to put here
			thisCustomer.bill = billingAddress;*/
			
			thisCustomer.bopCode = request.bopCode;
			thisCustomer.referenceId = request.referenceId;
			thisCustomer.corporateId = request.corporateId;
			thisCustomer.countryId = request.countryId; // ??
			//thisCustomer.email = request.email;
			thisCustomer.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisCustomer.kvkNumber = request.kvkNumber;
			thisCustomer.name = request.name;
			//thisCustomer.nameAlias1 = request.nameAlias1;
			//thisCustomer.nameAlias2 = request.nameAlias2;
			thisCustomer.dealerCode = request.dealerCode;
			
			thisCustomer.phone = StringUtils.cleanNLPhoneNumber(request.phone);
			thisCustomer.website = request.website;
			
			requestList.add(thisCustomer);			
		}
		
		return requestList;
	}

	
	/**
	 * @description			This will build the update request object which will be sent to ECS. 
	 */
	private List<ECSSOAPCompany.resellerUpdateType> buildResellerUpdateUsingRequest(){
		List<ECSSOAPCompany.resellerUpdateType> requestList = new List<ECSSOAPCompany.resellerUpdateType>();
		
		for(Request request :this.requests){
			ECSSOAPCompany.resellerUpdateType thisCustomer = new ECSSOAPCompany.resellerUpdateType(); 
			
			thisCustomer.accountManagerEbu = request.accountManagerEbu;
			thisCustomer.banNumber = request.banNumber;
			
			/*ECSSOAPCompany.billType billingAddress = new ECSSOAPCompany.billType();
			billingAddress.zipcode = request.billingPostalCode;
			billingAddress.name = request.name;
			billingAddress.address = request.billingStreet;
			billingAddress.city = request.billingCity;
			billingAddress.countryId = request.countryId; // TODO: find out what countryID to use
			billingAddress.contact = null; //TODO: find out what to put here
			billingAddress.phone = null; //TODO: find out what to put here
			thisCustomer.bill = billingAddress;*/
			
			thisCustomer.bopCode = request.bopCode;
			thisCustomer.referenceId = request.referenceId;
			thisCustomer.corporateId = request.corporateId;
			thisCustomer.countryId = request.countryId; // ??
			//thisCustomer.email = request.email;
			thisCustomer.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisCustomer.kvkNumber = request.kvkNumber;
			thisCustomer.name = request.name;
			//thisCustomer.nameAlias1 = request.nameAlias1;
			//thisCustomer.nameAlias2 = request.nameAlias2;
			thisCustomer.dealerCode = request.dealerCode;			
			thisCustomer.phone = StringUtils.cleanNLPhoneNumber(request.phone);
			
			thisCustomer.website = request.website;
			
			requestList.add(thisCustomer);			
		}
		
		return requestList;			

	}	
	
	/**
	 * @description			This will parse the result returned by ECS. 
	 * @param	updateResult	The result object returned by ECS which contains messages and the customer number.
	 */
	private void parseResponse(List<ECSSOAPCompany.companyResponseType> theResults){
		//if(responses == null)
		responses = new Response[]{};
		if(theResults == null) return;
		
		System.debug('##### RAW RESPONSE: ' + theResults);
		
		for(ECSSOAPCompany.companyResponseType result : theResults){
			
			if(result != null){
				Response response = new Response();
				response.errorCode = result.errorCode;
				//response.ban = result.ban;
				response.bopCode = result.bopCode;
				response.corporateId = result.corporateId;
				response.errorMessage = result.errorMessage;
				response.referenceId = result.referenceId;
					
				System.debug('##### Messages: ' + response.errorMessage);
				
				responses.add(response);
			}
		}
	}
	
	
	/**
	 * @description			This will return the response returned by ECS. It will contain the customer
	 *						number and optionally a list of messages that may need to be displayed to the user.
	 */
	public Response[] getResponse(){
		return responses;
	}

}