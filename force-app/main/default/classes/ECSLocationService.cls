/**
 * @description			This class is responsible for creating and updating locations in ECS.
 * @author				Guy Clairbois
 */
public class ECSLocationService extends ECSSOAPBaseWebService implements IWebService<List<Request>, List<Response>> {

	/**
	 * @description			This is the request which will be sent to ECS create or update a 
	 *						location. 
	 */
	public class Request {
		// reference to company
        public String companyBanNumber;
        public String companyCorporateId;
        public String companyBopCode;
        
        // key fields
        public String locationReferenceId;
        public Integer housenumber;
        public String housenumberExt;
        public String zipcode;
		            
		// detail fields		            	
        public String name;
        //public ECSSOAPLocation.companyRefType wholesaleCustomer;
        //public Integer locationGroupId;
        public String locationTypeName;
        public String locationAlias;
        public String building;
        public String sla;
        public String street;
        public String city;
        public String countryId;
        public String phone;
        /*public String fax;
        public String slaCode;
        public String notes;
        public String latitude;
        public String longitude;
        public String locationCustom1;
        public String locationCustom2;
        public String locationCustom3;
        public String locationCustom4;
        public String locationCustom5;
        */
        public String canvasStreet;
        public Integer canvasHousenumber;
        public String canvasHousenumberExt;
        public String canvasZipcode;
        public String canvasCity;
        public String canvasCountryId;
        /*public String vovCode;
        public String warehouseTypeName;*/		
	}
	
	
	/**
	 * @description			This is the response that will be returned after making a request to ECS.
	 */
	public class Response {

        public String ban;
        public String corporateId;
        public String bopCode;
        public String zipcode;
        public String housenumber;
        public String housenumberExt;
        public String errorCode;
        public String errorMessage;
	}
	
	
	private Request[] requests;
	private Response[] responses;
	
	
	/**
	 * @description			The constructor is responsible for setting the default web service configuration.
	 */
	public ECSLocationService(){
		if(!Test.isRunningTest()){
			setWebServiceConfig( WebServiceConfigLocator.getConfig('ECSSOAPLocation') );
		}
		else setWebServiceConfig( WebServiceConfigLocator.createConfig());
	}
	
	
	/**
	 * @description			This sets the location data to create/update location details in ECS.
	 */
	public void setRequest(Request[] request){
		this.requests = request;
	}
	

	public void makeRequest(String requestType){
		if(requestType=='create'){
			makeCreateRequest();
		} else if(requestType=='update'){
			makeUpdateRequest();
		}
	}
	
	/**
	 * @description			This will make the request to ECS to create locations with the request.
	 */
	public void makeCreateRequest(){
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPLocation.locationSoap service = new ECSSOAPLocation.locationSoap(); 
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPLocation.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		
		List<ECSSOAPLocation.locationCreateType> createRequest = buildLocationCreateUsingRequest();
		System.debug('createRequest: ' + JSON.serialize(createRequest));
		
		// create queryResult
		List<ECSSOAPLocation.locationResponseType> createResults;
			
		if (Test.isRunningTest()) {
			// unit test cannot do a real callout, so create a result
			//createResult = TestLocationCreateService.setupDummyResponse();
		} else {
			try {
				system.debug(createRequest);
				createResults = service.createLocations(createRequest);
				system.debug(createResults);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('Error received from BOP when attempting to create the location(s): '+ex.getMessage(), ex);
			}
		}
		
		parseResponse(createResults);
	}
		

	/**
	 * @description			This will make the request to ECS to update locations with the request.
	 */		
	public void makeUpdateRequest(){	
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPLocation.locationSoap service = new ECSSOAPLocation.locationSoap(); 
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPLocation.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		
		List<ECSSOAPLocation.locationUpdateType> updateRequest = buildLocationUpdateUsingRequest();
		System.debug('updateRequest: ' + JSON.serialize(updateRequest));
	
		// create queryResult
		List<ECSSOAPLocation.locationResponseType> updateResults;
			
		if (Test.isRunningTest()) {
			// unit test cannot do a real callout, so create a result
			//createResult = TestLocationCreateService.setupDummyResponse();
		} else {
			try {
				updateResults = service.updateLocations(updateRequest);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('Error received from BOP when attempting to update the location(s): '+ex.getMessage(), ex);
			}
		}
		
		parseResponse(updateResults);
	}

	
	private ECSSOAPLocation.authenticationHeader_element buildHeaderUsingConfig(){
		ECSSOAPLocation.authenticationHeader_element header = new ECSSOAPLocation.authenticationHeader_element();
		header.applicationId = 'sfdc';
		header.username = webServiceConfig.getUsername();
		header.password = webServiceConfig.getPassword();
		return header;
	}
	

	/**
	 * @description			This will build the create request object which will be sent to ECS. 
	 */
	private List<ECSSOAPLocation.locationCreateType> buildLocationCreateUsingRequest(){
		List<ECSSOAPLocation.locationCreateType> requestList = new List<ECSSOAPLocation.locationCreateType>();
		
		for(Request request :this.requests){
			ECSSOAPLocation.locationCreateType thisLocation = new ECSSOAPLocation.locationCreateType(); 
			
			ECSSOAPLocation.companyRefType thisCompany = new ECSSOAPLocation.companyRefType();
			thisCompany.banNumber = request.companyBanNumber;
			thisCompany.bopCode = request.companyBopCode;
			thisCompany.corporateId = request.companyCorporateId;
			thisLocation.company = thisCompany;

			thisLocation.locationReferenceId = request.locationReferenceId;
			thisLocation.city = request.city;
			thisLocation.countryId = request.countryId;
			thisLocation.housenumber = String.valueOf(request.housenumber);
			thisLocation.housenumberExt = request.housenumberExt;
			thisLocation.locationTypeName = request.locationTypeName;
			thisLocation.locationAlias = request.locationAlias;
			thisLocation.building = request.building;
			thisLocation.slaCode = request.sla;
			thisLocation.name = request.name;
			thisLocation.phone = StringUtils.cleanNLPhoneNumber(request.phone);
			thisLocation.street = request.street;
			thisLocation.zipcode = request.zipcode; 
			
			thisLocation.canvasStreet = request.canvasStreet;
			thisLocation.canvasHousenumber = String.valueOf(request.canvasHousenumber);
			thisLocation.canvasHousenumberExt = request.canvasHousenumberExt;
			thisLocation.canvasZipcode = request.canvasZipcode;
			thisLocation.canvasCity = request.canvasCity;
			thisLocation.canvasCountryId = request.canvasCountryId;

			requestList.add(thisLocation);			
		}
		
		return requestList;
	}

	
	/**
	 * @description			This will build the update request object which will be sent to ECS. 
	 */
	private List<ECSSOAPLocation.locationUpdateType> buildLocationUpdateUsingRequest(){
		List<ECSSOAPLocation.locationUpdateType> requestList = new List<ECSSOAPLocation.locationUpdateType>();
		
		for(Request request :this.requests){
			ECSSOAPLocation.locationUpdateType thisLocation = new ECSSOAPLocation.locationUpdateType(); 
			
			ECSSOAPLocation.companyRefType thisCompany = new ECSSOAPLocation.companyRefType();
			thisCompany.banNumber = request.companyBanNumber;
			thisCompany.bopCode = request.companyBopCode;
			thisCompany.corporateId = request.companyCorporateId;
			thisLocation.company = thisCompany;

			thisLocation.locationReferenceId = request.locationReferenceId;
			thisLocation.city = request.city;
			thisLocation.countryId = request.countryId;
			thisLocation.housenumber = String.valueOf(request.housenumber);
			thisLocation.housenumberExt = request.housenumberExt;
			thisLocation.locationTypeName = request.locationTypeName;
			thisLocation.locationAlias = request.locationAlias;
			thisLocation.building = request.building;	
			thisLocation.slaCode = request.sla;
			thisLocation.name = request.name;
			thisLocation.phone = StringUtils.cleanNLPhoneNumber(request.phone);			
			thisLocation.street = request.street;
			thisLocation.zipcode = request.zipcode; 
			
			thisLocation.canvasStreet = request.canvasStreet;
			thisLocation.canvasHousenumber = String.valueOf(request.canvasHousenumber);
			thisLocation.canvasHousenumberExt = request.canvasHousenumberExt;
			thisLocation.canvasZipcode = request.canvasZipcode;
			thisLocation.canvasCity = request.canvasCity;
			thisLocation.canvasCountryId = request.canvasCountryId;
			
			requestList.add(thisLocation);			
		}
		
		return requestList;
	}	
	
	/**
	 * @description			This will parse the result returned by ECS. 
	 * @param	updateResult	The result object returned by ECS which contains messages and the location number.
	 */
	private void parseResponse(List<ECSSOAPLocation.locationResponseType> theResults){
		responses = new Response[]{};
		if(theResults == null) return;
		
		System.debug('##### RAW RESPONSE: ' + theResults);
		
		for(ECSSOAPLocation.locationResponseType result : theResults){
			
			if(result != null){
				Response response = new Response();
				response.errorCode = result.errorCode; 
				response.errorMessage = result.errorMessage;
				response.ban = result.ban;
				response.bopCode = result.bopCode;
				response.corporateId = result.corporateId;
				response.housenumber = result.housenumber;
				response.housenumberExt = result.housenumberExt;
				response.zipcode = result.zipcode;

				System.debug('##### Messages: ' + response.errorMessage);
				
				responses.add(response);
			}	
		}
	}
	
	
	/**
	 * @description			This will return the response returned by ECS. It will contain the location
	 *						number and optionally a list of messages that may need to be displayed to the user.
	 */
	public Response[] getResponse(){
		return responses;
	}

}