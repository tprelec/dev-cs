@SuppressWarnings('PMD') // To be refactored later
/**
 * @description      This class is responsible for exporting Orders to ECS.
 * @author        Guy Clairbois
 */
public class ECSOrderService extends ECSSOAPBaseWebService implements IWebService<List<Request>, List<Response>> {
	/**
	 * @description      This is the request which will be sent to ECS create/update an order
	 */
	public class Request {
		public String referenceId;
		public String bopOrderId;
		public String ben;
		public String remarks;

		// order contract data
		public String contractReferenceId;
		public String contractNumber;
		public Datetime contractSignDate;

		// escalation data
		public String orderEscalationlevel; // normal / high
		public String orderEscalationReason;
		public Datetime orderEscalationDeadline;

		// order customer
		public String customerReferenceId;
		public String customerBan; // JvW 2019-05-21
		public String customerCorporateId;
		public String customerBopCode;
		public String customerAccountId; // JvW 2019-05-21
		public String customerAccountManagerCompanyBan; // JvW 2019-05-21
		public String customerAccountManagerCompanyCorporateId;
		public String customerAccountManagerCompanyBopCode;
		public String customerAccountManagerCompanyAccountId; // JvW 2019-05-21
		public String customerAccountManagerEmail;
		public String customerInsideSalesEmployeeCompanyBan; // JvW 2019-05-21
		public String customerInsideSalesEmployeeCompanyCorporateId;
		public String customerInsideSalesEmployeeCompanyBopCode;
		public String customerInsideSalesEmployeeCompanyAccountId; // JvW 2019-05-21
		public String customerInsideSalesEmployeeEmail;

		// order contacts
		public List<ECSSOAPOrder.orderContactType> orderContacts;

		// pbx(s)
		public ECSSOAPOrder.pbxListType pbxs;

		// vip indication
		public Boolean isVipPartnerOrder;

		//W-000563: TSA Approval
		public Boolean bespoke;

		// Order Type
		public String type_x;

		// order(s)
		public ECSSOAPOrder.createOrderVovProductType vov;
		public ECSSOAPOrder.createOrderNumberportingProductType numberportings;
		public ECSSOAPOrder.createOrderPhonebookRegistrationProductType phonebookRegistrations;
		public ECSSOAPOrder.createOrderInternetProductType internet;
		public ECSSOAPOrder.createOrderOneNetProductType onenet;
		public ECSSOAPOrder.createOrderOneNet12ProductType onenet12;
		public ECSSOAPOrder.createOrderOneNet12ProductType onenetSub30;
		public ECSSOAPOrder.createOrderOneNet12ProductType oneNetExpress;
		public ECSSOAPOrder.createOrderErsType ers;
		public ECSSOAPOrder.createOrderIpvpnProductType ipvpn;

		// Legacy-order-specific fields
		public String shortDescription; // korte omschrijving
		public String description; // opmerkingen overdracht
		public String legacyOrderType;
		public String title; // titel project
		public String titleSuffix; // niet gemapt in bop
		public String locationCount;
		public String documentFolder;
		public List<String> legacyServices;
		// public DateTime cancellationTechnicalEndDate;
		// public DateTime cancellationAdministrativeEndDate;
		// public DateTime cancellationContractualEndDate;
		// public Boolean cancellationLumpsum;
		// public List<String> cancelledItems;
		public List<ECSSOAPOrder.legacyOrderRowType> legacyOrderRows;
		public String customerReference;
	}

	/**
	 * @description      This is the response that will be returned after making a request to ECS.
	 */
	public class Response {
		public String referenceId;
		public String orderId;
		public String salesOrderId;
		public String errorCode;
		public String errorMessage;
	}

	private Request[] requests;
	private Response[] responses;

	/**
	 * @description      The constructor is responsible for setting the default web service configuration.
	 */
	public ECSOrderService() {
		if (!Test.isRunningTest()) {
			setWebServiceConfig(WebServiceConfigLocator.getConfig('ECSSOAPOrder'));
		} else {
			setWebServiceConfig(WebServiceConfigLocator.createConfig());
		}
	}

	/**
	 * @description      This sets the customer data to create/update customer details in ECS.
	 */
	public void setRequest(Request[] request) {
		this.requests = request;
	}

	public void makeRequest(String requestType) {
		if (requestType == 'create') {
			makeCreateRequest();
		} else if (requestType == 'cancel') {
			makeCancelRequest();
		} else if (requestType == 'legacy') {
			makeLegacyRequest();
		}
	}

	/**
	 * @description      This will make the request to ECS to create orders with the request.
	 */
	public void makeCreateRequest() {
		if (webserviceConfig == null) {
			throw new ExWebServiceCalloutException('Missing configuration');
		}
		if (requests == null) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}
		if (requests.isEmpty()) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}

		// WebService callout logic
		ECSSOAPOrder.OrderSOAP service = new ECSSOAPOrder.OrderSOAP();
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;
		service.clientCertName_x = webServiceConfig.getCertificateName();

		// create header element
		ECSSOAPOrder.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		system.debug(header);
		List<ECSSOAPOrder.createOrderType> createRequest = buildOrderCreateUsingRequest();
		System.debug('createRequest: ' + JSON.serialize(createRequest));

		// create queryResult
		List<ECSSOAPOrder.createOrderResponseType> createResults;

		try {
			system.debug(createRequest);
			createResults = service.createOrders(createRequest);
			system.debug(createResults);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException(
				'Error received from BOP when attempting to create the order(s): ' +
				ex.getMessage(),
				ex
			);
		}

		parseResponse(createResults);
	}

	/**
	 * @description      This will make the request to ECS to create orders with the request.
	 */
	public void makeCancelRequest() {
		if (webserviceConfig == null) {
			throw new ExWebServiceCalloutException('Missing configuration');
		}
		if (requests == null) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}
		if (requests.isEmpty()) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}

		// WebService callout logic
		ECSSOAPOrder.OrderSOAP service = new ECSSOAPOrder.OrderSOAP();
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;
		service.clientCertName_x = webServiceConfig.getCertificateName();

		// create header element
		ECSSOAPOrder.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		system.debug(header);
		List<ECSSOAPOrder.cancelOrderType> cancelRequest = buildOrderCancelUsingRequest();
		System.debug('cancelRequest: ' + JSON.serialize(cancelRequest));

		// create queryResult
		List<ECSSOAPOrder.cancelOrderResponseType> cancelResults;

		try {
			system.debug(cancelRequest);
			cancelResults = service.cancelOrders(cancelRequest);
			system.debug(cancelResults);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException(
				'Error received from BOP when attempting to cancel the order(s): ' +
				ex.getMessage(),
				ex
			);
		}

		parseCancelResponse(cancelResults);
	}

	/**
	 * @description      This will make the request to ECS to create legacy orders with the request.
	 */
	public void makeLegacyRequest() {
		if (webserviceConfig == null) {
			throw new ExWebServiceCalloutException('Missing configuration');
		}
		if (requests == null) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}
		if (requests.isEmpty()) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}

		// WebService callout logic
		ECSSOAPOrder.OrderSOAP service = new ECSSOAPOrder.OrderSOAP();
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;
		service.clientCertName_x = webServiceConfig.getCertificateName();

		// create header element
		ECSSOAPOrder.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		system.debug(header);
		List<ECSSOAPOrder.legacyOrderRequestType> legacyRequest = buildLegacyOrderCreateUsingRequest();
		System.debug('legacyRequest: ' + JSON.serialize(legacyRequest));

		// create queryResult
		List<ECSSOAPOrder.legacyOrderResponseType> legacyResults;

		try {
			system.debug(legacyRequest);
			legacyResults = service.legacyOrders(legacyRequest);
			system.debug(legacyResults);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException(
				'Error received from BOP when attempting to create the legacy order(s): ' +
				ex.getMessage(),
				ex
			);
		}

		parseLegacyResponse(legacyResults);
	}

	private ECSSOAPOrder.authenticationHeader_element buildHeaderUsingConfig() {
		ECSSOAPOrder.authenticationHeader_element header = new ECSSOAPOrder.authenticationHeader_element();
		header.applicationId = 'sfdc';
		header.username = webServiceConfig.getUsername();
		header.password = webServiceConfig.getPassword();
		return header;
	}

	/**
	 * @description      This will build the create request object which will be sent to ECS.
	 */
	private List<ECSSOAPOrder.createOrderType> buildOrderCreateUsingRequest() {
		List<ECSSOAPOrder.createOrderType> requestList = new List<ECSSOAPOrder.createOrderType>();

		for (Request request : this.requests) {
			ECSSOAPOrder.createOrderType thisOrder = new ECSSOAPOrder.createOrderType();
			thisOrder.referenceId = request.referenceId;
			thisOrder.shortDescription = request.remarks;
			//thisOrder.ben = request.ben;

			thisOrder.contract = new ECSSOAPOrder.contractType();
			thisOrder.contract.contractReferenceId = request.contractReferenceId;
			thisOrder.contract.contractNumber = request.contractNumber;
			thisOrder.contract.signDate = request.contractSignDate; //json.serialize(request.contractSignDate);

			if (request.orderEscalationDeadline != null) {
				thisOrder.escalation = new ECSSOAPOrder.escalationType();
				// either all 3, or only deadline
				thisOrder.escalation.level = request.orderEscalationLevel; // normal / high
				thisOrder.escalation.reason = request.orderEscalationReason;
				thisOrder.escalation.deadline = request.orderEscalationDeadline; //json.serialize(request.orderEscalationDeadline);
			}

			thisOrder.customer = new ECSSOAPOrder.customerType();

			if (request.customerAccountManagerEmail != null) {
				thisOrder.customer.accountManager = new ECSSOAPOrder.userRefType();
				thisOrder.customer.accountManager.company = new ECSSOAPOrder.companyRefType();
				thisOrder.customer.accountManager.company.banNumber = request.customerAccountManagerCompanyBan; // JvW 2019-05-21
				thisOrder.customer.accountManager.company.bopCode = request.customerAccountManagerCompanyBopCode;
				thisOrder.customer.accountManager.company.corporateId = request.customerAccountManagerCompanyCorporateId;
				thisOrder.customer.accountManager.company.accountId = request.customerAccountManagerCompanyAccountId; // JvW 2019-05-21
				thisOrder.customer.accountManager.email = request.customerAccountManagerEmail;
			}

			if (request.customerInsideSalesEmployeeEmail != null) {
				thisOrder.customer.insideSalesEmployee = new ECSSOAPOrder.userRefType();
				thisOrder.customer.insideSalesEmployee.company = new ECSSOAPOrder.companyRefType();
				thisOrder.customer.insideSalesEmployee.company.banNumber = request.customerInsideSalesEmployeeCompanyBan; // JvW 2019-05-21
				thisOrder.customer.insideSalesEmployee.company.bopCode = request.customerInsideSalesEmployeeCompanyBopCode;
				thisOrder.customer.insideSalesEmployee.company.corporateId = request.customerInsideSalesEmployeeCompanyCorporateId;
				thisOrder.customer.insideSalesEmployee.company.accountId = request.customerInsideSalesEmployeeCompanyAccountId; // JvW 2019-05-21
				thisOrder.customer.insideSalesEmployee.email = request.customerInsideSalesEmployeeEmail;
			}

			thisOrder.customer.companyRef = new ECSSOAPOrder.companyRefType();
			thisOrder.customer.companyRef.referenceId = request.customerReferenceId;
			thisOrder.customer.companyRef.banNumber = request.customerBan; // JvW 2019-05-21
			thisOrder.customer.companyRef.bopCode = request.customerBopCode;
			thisOrder.customer.companyRef.corporateId = request.customerCorporateId;
			thisOrder.customer.companyRef.accountId = request.customerAccountId; // JvW 2019-05-21

			if (request.orderContacts != null) {
				thisOrder.contacts = new ECSSOAPOrder.orderContactListType();
				thisOrder.contacts.contact = new List<ECSSOAPOrder.orderContactType>();
				for (ECSSOAPOrder.orderContactType oct : request.orderContacts) {
					thisOrder.contacts.contact.add(oct);
				}
			}

			// add the list of pbx's
			thisOrder.pbxs = request.pbxs;

			// add the vip indication
			thisOrder.isVfImpPartOrder = request.isVipPartnerOrder;

			//W-000563: TSA Approval
			thisOrder.bespoke = request.bespoke;

			thisOrder.type_x = request.type_x;

			// add the order products
			thisOrder.products = new ECSSOAPOrder.createOrderProductsType();
			thisOrder.products.officeVoice = request.vov;
			thisOrder.products.numberPorting = request.numberportings;
			thisOrder.products.phonebookRegistration = request.phonebookRegistrations;
			thisOrder.products.internet = request.internet;
			thisOrder.products.oneNet = request.onenet;
			thisOrder.products.oneNet12 = request.onenet12;
			thisOrder.products.oneNetSub30 = request.onenetSub30;
			thisOrder.products.oneNetExpress = request.oneNetExpress;
			thisOrder.products.ers = request.ers;
			thisOrder.products.ipvpn = request.ipvpn;

			requestList.add(thisOrder);
		}

		return requestList;
	}

	/**
	 * @description      This will build the cancel request object which will be sent to ECS.
	 */
	private List<ECSSOAPOrder.cancelOrderType> buildOrderCancelUsingRequest() {
		List<ECSSOAPOrder.cancelOrderType> requestList = new List<ECSSOAPOrder.cancelOrderType>();

		for (Request request : this.requests) {
			ECSSOAPOrder.cancelOrderType thisOrder = new ECSSOAPOrder.cancelOrderType();
			thisOrder.referenceId = request.referenceId;
			thisOrder.orderId = Integer.valueOf(request.bopOrderId);

			requestList.add(thisOrder);
		}

		return requestList;
	}

	/**
	 * @description      This will build the create request object which will be sent to ECS.
	 */
	private List<ECSSOAPOrder.legacyOrderRequestType> buildLegacyOrderCreateUsingRequest() {
		List<ECSSOAPOrder.legacyOrderRequestType> requestList = new List<ECSSOAPOrder.legacyOrderRequestType>();

		for (Request request : this.requests) {
			ECSSOAPOrder.legacyOrderRequestType thisOrder = new ECSSOAPOrder.legacyOrderRequestType();
			thisOrder.referenceId = request.referenceId;
			thisOrder.shortDescription = request.remarks;

			thisOrder.contract = new ECSSOAPOrder.contractType();
			thisOrder.contract.contractReferenceId = request.contractReferenceId;
			thisOrder.contract.contractNumber = request.contractNumber;
			thisOrder.contract.signDate = request.contractSignDate; //json.serialize(request.contractSignDate);

			if (request.orderEscalationDeadline != null) {
				thisOrder.escalation = new ECSSOAPOrder.escalationType();
				// either all 3, or only deadline
				thisOrder.escalation.level = request.orderEscalationLevel; // normal / high
				thisOrder.escalation.reason = request.orderEscalationReason;
				thisOrder.escalation.deadline = request.orderEscalationDeadline; //json.serialize(request.orderEscalationDeadline);
			}

			thisOrder.customer = new ECSSOAPOrder.customerType();

			if (request.customerAccountManagerEmail != null) {
				thisOrder.customer.accountManager = new ECSSOAPOrder.userRefType();
				thisOrder.customer.accountManager.company = new ECSSOAPOrder.companyRefType();
				thisOrder.customer.accountManager.company.banNumber = request.customerAccountManagerCompanyBan; // JvW 2019-05-21
				thisOrder.customer.accountManager.company.bopCode = request.customerAccountManagerCompanyBopCode;
				thisOrder.customer.accountManager.company.corporateId = request.customerAccountManagerCompanyCorporateId;
				thisOrder.customer.accountManager.company.accountId = request.customerAccountManagerCompanyAccountId; // JvW 2019-05-21
				thisOrder.customer.accountManager.email = request.customerAccountManagerEmail;
			}

			if (request.customerInsideSalesEmployeeEmail != null) {
				thisOrder.customer.insideSalesEmployee = new ECSSOAPOrder.userRefType();
				thisOrder.customer.insideSalesEmployee.company = new ECSSOAPOrder.companyRefType();
				thisOrder.customer.insideSalesEmployee.company.banNumber = request.customerInsideSalesEmployeeCompanyBan; // JvW 2019-05-21
				thisOrder.customer.insideSalesEmployee.company.bopCode = request.customerInsideSalesEmployeeCompanyBopCode;
				thisOrder.customer.insideSalesEmployee.company.corporateId = request.customerInsideSalesEmployeeCompanyCorporateId;
				thisOrder.customer.insideSalesEmployee.company.accountId = request.customerInsideSalesEmployeeCompanyAccountId; // JvW 2019-05-21
				thisOrder.customer.insideSalesEmployee.email = request.customerInsideSalesEmployeeEmail;
			}

			thisOrder.customer.companyRef = new ECSSOAPOrder.companyRefType();
			thisOrder.customer.companyRef.referenceId = request.customerReferenceId;
			thisOrder.customer.companyRef.banNumber = request.customerBan; // JvW 2019-05-21
			thisOrder.customer.companyRef.bopCode = request.customerBopCode;
			thisOrder.customer.companyRef.corporateId = request.customerCorporateId;
			thisOrder.customer.companyRef.accountId = request.customerAccountId; // JvW 2019-05-21

			if (request.orderContacts != null) {
				thisOrder.contacts = new ECSSOAPOrder.orderContactListType();
				thisOrder.contacts.contact = new List<ECSSOAPOrder.orderContactType>();
				for (ECSSOAPOrder.orderContactType oct : request.orderContacts) {
					thisOrder.contacts.contact.add(oct);
				}
			}

			// Legacy-order-specific fields
			// for legacy order, change allocation of description fields
			if (request.legacyOrderType != null) {
				thisOrder.description = request.remarks;
				thisOrder.shortDescription = request.description;
			}

			thisOrder.legacyOrderType = request.legacyOrderType; // Cease, Change, Expansion, Moving, New
			thisOrder.title = request.title;
			thisOrder.titleSuffix = request.titleSuffix;
			thisOrder.locationCount = request.locationCount; // NA, 1, 2-5, 6-10, 11-20, 20+
			thisOrder.documentFolder = request.documentFolder;

			thisOrder.services = new ECSSOAPOrder.legacyServicesListType();
			thisOrder.services.legacyService = request.legacyServices;

			// if (request.legacyOrderType == 'Cease') {
			// 	thisOrder.cancellationDetails = new ECSSOAPOrder.legacyCancellationDetailsType();
			// 	thisOrder.cancellationDetails.technicalEnddate = request.cancellationTechnicalEndDate;
			// 	thisOrder.cancellationDetails.administrativeEnddate = request.cancellationAdministrativeEndDate;
			// 	thisOrder.cancellationDetails.contractualEnddate = request.cancellationContractualEndDate;
			// 	thisOrder.cancellationDetails.lumpsum = request.cancellationLumpsum;
			// 	thisOrder.cancellationDetails.cancelledItems = new ECSSOAPOrder.legacyCancelledItemListType();
			// 	thisOrder.cancellationDetails.cancelledItems.item = new List<String>();

			// 	if (request.cancelledItems != null) {
			// 		for (String itemtype : request.cancelledItems) {
			// 			thisOrder.cancellationDetails.cancelledItems.item.add(itemtype);
			// 		}
			// 	}
			// }

			if (request.legacyOrderRows != null) {
				thisOrder.orderRows = new ECSSOAPOrder.legacyOrderRowListType();
				thisOrder.orderRows.orderRow = request.legacyOrderRows;
			}

			thisOrder.customerReference = request.customerReference;

			requestList.add(thisOrder);
		}

		return requestList;
	}

	/**
	 * @description      This will parse the result returned by ECS.
	 * @param  updateResult  The result object returned by ECS which contains messages and the customer number.
	 */
	private void parseResponse(List<ECSSOAPOrder.createOrderResponseType> theResults) {
		responses = new List<Response>{};
		if (theResults == null) {
			return;
		}

		System.debug('##### RAW RESPONSE: ' + theResults);

		for (ECSSOAPOrder.createOrderResponseType result : theResults) {
			if (result != null) {
				Response response = new Response();
				response.referenceId = result.referenceId;
				response.orderId = String.valueOf(result.orderId);
				response.salesOrderId = result.salesorderNumber;
				response.errorCode = result.error.code;
				response.errorMessage = result.error.message;

				System.debug('##### Messages: ' + response.errorMessage);

				responses.add(response);
			}
		}
	}

	/**
	 * @description      This will parse the result returned by ECS.
	 * @param  updateResult  The result object returned by ECS which contains messages and the customer number.
	 */
	private void parseCancelResponse(List<ECSSOAPOrder.cancelOrderResponseType> theResults) {
		responses = new List<Response>{};
		if (theResults == null) {
			return;
		}

		System.debug('##### RAW RESPONSE: ' + theResults);

		for (ECSSOAPOrder.cancelOrderResponseType result : theResults) {
			if (result != null) {
				Response response = new Response();
				response.referenceId = result.referenceId;
				response.orderId = String.valueOf(result.orderId);
				response.errorCode = result.error.code;
				response.errorMessage = result.error.message;

				System.debug('##### Messages: ' + response.errorMessage);

				responses.add(response);
			}
		}
	}

	/**
	 * @description      This will parse the result returned by ECS.
	 * @param  updateResult  The result object returned by ECS which contains messages and the customer number.
	 */
	private void parseLegacyResponse(List<ECSSOAPOrder.legacyOrderResponseType> theResults) {
		responses = new List<Response>{};
		if (theResults == null) {
			return;
		}

		System.debug('##### RAW RESPONSE: ' + theResults);

		for (ECSSOAPOrder.legacyOrderResponseType result : theResults) {
			if (result != null) {
				Response response = new Response();
				response.referenceId = result.referenceId;
				response.salesOrderId = result.salesorderNumber;
				if (result.error != null) {
					response.errorCode = result.error.code;
					response.errorMessage = result.error.message;
				}

				System.debug('##### Messages: ' + response.errorMessage);

				responses.add(response);
			}
		}
	}

	/**
	 * @description      This will return the response returned by ECS. It will contain the customer
	 *            number and optionally a list of messages that may need to be displayed to the user.
	 */
	public Response[] getResponse() {
		return responses;
	}
}