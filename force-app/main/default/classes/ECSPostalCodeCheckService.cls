/**
 * @description			This class is responsible for querying the availability of line types by postalcode/housenumber.
 * @author				Guy Clairbois, Marcel Vreuls
 */
public class ECSPostalCodeCheckService extends ECSSOAPBaseWebService implements IWebService<List<Request>, List<Response>> {

	/**
	 * @description			This is the request which will be sent to ECS to do the query
	 */
	public class Request {
        public String postalCode;
        public Integer houseNumber;
        public String houseNumberExtension;
	}
	
	
	/**
	 * @description			This is the response that will be returned after making a request to ECS.
	 */
	public class Response {
        public String postalCode;
        public Integer houseNumber;
        public String houseNumberExtension;
		public lineOption[] lineOptions;
		public lineOptionError[] errorMessages;
		public remark[] remarks;
		
	}
	
	public class remark{
		public String connectionType;
		public String remarkSpecification;
		public String text;
		}

	
	public class lineOption {
		public String accessclass;
		public String classification;
        public String technologytype;
        public Decimal maxUpload;
        public Decimal maxDownload;
        public Date plandate;
        public String serviceable;
        public String portfolio;
        public String linetype;		
	}
	
	public class lineOptionError{
		public String errorCode;
		public String errorMessage;
		public String portfolio;
	}
	
	private Request[] requests;
	private Response[] responses;
	
	
	/**
	 * @description			The constructor is responsible for setting the default web service configuration.
	 */
	public ECSPostalCodeCheckService(){
		if(!Test.isRunningTest()){
			setWebServiceConfig( WebServiceConfigLocator.getConfig('ECSSOAPPostalCodeCheck') );
		}
		else setWebServiceConfig( WebServiceConfigLocator.createConfig());
	}
	
	
	/**
	 * @description			This sets the customer number to use to retrieve the customer details from SAP.
	 */
	public void setRequest(Request[] request){
		this.requests = request;
	}
	
	
	/**
	 * @description			This will make the request to ECS.
	 */
	public void makeRequest(String requestType){
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPPostalCodeCheck.pcavailability service = new ECSSOAPPostalCodeCheck.pcavailability();
		
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPPostalCodeCheck.authenticationHeader_element header = buildHeaderUsingConfig();

		ECSSOAPPostalCodeCheck.locationsRequestType checkRequest = buildCheckUsingRequest();
		System.debug('pccheck: ' + JSON.serialize(checkRequest));
	
		// create queryResult
		ECSSOAPPostalCodeCheck.locationsResponseType checkResult = new ECSSOAPPostalCodeCheck.locationsResponseType();
		
		// initialize responses, but allow for multiple checks to be collected  in 1 response list
		if (responses == null) responses = new Response[]{};
		
		if(requestType == 'fiber'){
			// do 2 checks here
			try {
				checkResult.location = service.fiberCheck(checkRequest.location);
				parseResponse(checkResult);
				system.debug(responses);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('There was an error when attempting to do the fiber check. EoF might have completed OK, though. Please contact your administrator.');
			}

		} else if(requestType == 'sdslbis'){
			ECSSOAPPostalCodeCheck.locationsResponseType checkResult2 = new ECSSOAPPostalCodeCheck.locationsResponseType();
			try{
				checkResult2.location = service.sdslbisCheck(checkRequest.location);
				parseResponse(checkResult2);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('There was an error when attempting to do the EoF check. Fiber might have completed OK, though. Please contact your administrator.');
			}					
			system.debug(responses);
		} else if(requestType == 'coax'){
			try{
				checkResult.location = service.coaxCheck(checkRequest.location);
				parseResponse(checkResult);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('There was an error when attempting to do the coax check. Fiber and EoK might have completed OK, though. Please contact your administrator.');
			}					
			system.debug(responses);					
		} else if(requestType == 'dsl'){
			try{
				checkResult.location = service.dslCheck(checkRequest.location);
				parseResponse(checkResult);					
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('There was an error when attempting to do the dsl check. Please contact your administrator.');
			}	
		} else {
			try{
				checkResult.location = service.combinedCheck(checkRequest.location);	
				parseResponse(checkResult);	
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('There was an error when attempting to do the combined check. Please contact your administrator.');
			}										
		}

	}
	
	
	private ECSSOAPPostalCodeCheck.authenticationHeader_element buildHeaderUsingConfig(){
		ECSSOAPPostalCodeCheck.authenticationHeader_element header = new ECSSOAPPostalCodeCheck.authenticationHeader_element();
		header.applicationId = 'sfdc';
		header.username = webServiceConfig.getUsername();
		header.password = webServiceConfig.getPassword();
		return header;
	}
	
	
	/**
	 * @description			This will build the request object which will be sent to ECS. 
	 */
	private ECSSOAPPostalCodeCheck.locationsRequestType buildCheckUsingRequest(){
		ECSSOAPPostalCodeCheck.locationsRequestType createRequest = new ECSSOAPPostalCodeCheck.locationsRequestType();
		List<ECSSOAPPostalCodeCheck.locationTypeRequest> locations = new List<ECSSOAPPostalCodeCheck.locationTypeRequest>();
		
		for(Request request :this.requests){
			ECSSOAPPostalCodeCheck.locationTypeRequest theRequest = new ECSSOAPPostalCodeCheck.locationTypeRequest(); 
			theRequest.zipcode = request.postalCode;
			theRequest.housenumber = String.valueOf(request.houseNumber);
			theRequest.housenumberext = request.houseNumberExtension; 
			locations.add(theRequest);			
		}
		
		createRequest.location = locations;
		return createRequest;
	}
	
	
	/**
	 * @description			This will parse the result returned by ECS. 
	 * @param	updateResult	The result object returned ECS which contains check results.
	 */
	private void parseResponse(ECSSOAPPostalCodeCheck.locationsResponseType checkResult){
		
		if(checkResult == null || checkResult.location == null) return;
		
		System.debug('##### RAW RESPONSE: ' + checkResult);
		for(ECSSOAPPostalCodeCheck.locationTypeResponse result : checkResult.location){
			if(result != null){
				
				Response response = new Response();
				response.PostalCode = result.zipcode;
				response.houseNumber = Integer.valueOf(result.housenumber);
				response.houseNumberExtension = result.housenumberext;

				// set of uniqueresults to prevent double results in sf
				Map<String,lineOption> keyToMaxDownLineOption = new Map<String,lineOption>();
				Map<String,lineOption> keyToMaxUpLineOption = new Map<String,lineOption>();

				List<lineOption> lineOptionsToReturn = new List<lineOption>();
				List<remark> remarksToReturn = new List<remark>();

				if(result.lineoptions!= null){ 
					for(ECSSOAPPostalCodeCheck.optionType lineOption : result.lineoptions.option){
						// filter out this technology type. This might be parametrized later on.
						if(lineOption.technologytype =='ADSL2+M') continue;

						// this key defines a unique result category
						String uniqueKey = lineOption.portfolio + lineOption.technologytype + lineOption.accessclass; 

						lineOption lo = new lineOption();
						lo.accessclass = lineOption.accessclass;
						lo.classification = lineOption.classification;
						lo.linetype = lineOption.linetype;
						lo.maxDownload = lineOption.download.max;
						lo.maxUpload = lineOption.upload.max;
						lo.plandate = lineOption.planDate==null||lineOption.planDate=='null'||lineOption.planDate==''?null:date.valueOf(lineOption.planDate);
						lo.portfolio = lineOption.portfolio;
						lo.serviceable = lineOption.serviceable;
						lo.technologytype = lineOption.technologytype;

						// only save the maxdown and maxup variants
						if(!keyToMaxDownLineOption.containsKey(uniqueKey)){
							keyToMaxDownLineOption.put(uniqueKey,lo);
						} else {
							if(lo.maxDownload > keyToMaxDownLineOption.get(uniqueKey).maxDownload){
								keyToMaxDownLineOption.put(uniqueKey,lo);
							}
						}
						if(!keyToMaxUpLineOption.containsKey(uniqueKey)){
							keyToMaxUpLineOption.put(uniqueKey,lo);
						} else {
							if(lo.maxUpload > keyToMaxUpLineOption.get(uniqueKey).maxUpload){
								keyToMaxUpLineOption.put(uniqueKey,lo);
							}
						}						

					}
				}

				//w-001096:
				if(result.remarks!= null){ 
					system.debug('Vreuls 1: result.remarks' + result.remarks);
					for(ECSSOAPPostalCodeCheck.remarkType myremark : result.remarks.remark){
						system.debug('Vreuls 1: myremark.Text: ' + myremark.text);
						remark re = new remark();
						re.connectionType = myremark.connectionType;
						re.text = myremark.text;
						remarksToReturn.add(re);
					}
				}

				for(String key : keyToMaxDownLineOption.keySet()){
					lineOptionsToReturn.add(keyToMaxDownLineOption.get(key));
					system.debug(keyToMaxUpLineOption.get(key));
					system.debug(keyToMaxDownLineOption.get(key));
					system.debug(String.valueOf(keyToMaxUpLineOption.get(key)));
					system.debug(String.valueOf(keyToMaxDownLineOption.get(key)));					
					if(keyToMaxUpLineOption.get(key) != keyToMaxDownLineOption.get(key)){
						// if the max up and max down are not the same one, include both in the result
						lineOptionsToReturn.add(keyToMaxUpLineOption.get(key));
					}
				}

				response.lineOptions = lineOptionsToReturn;
				// add any errors that occurred to the resultset
				List<lineOptionError> lineOptionErrors = new List<lineOptionError>();
				if(result.error != null){
					for(ECSSOAPPostalCodeCheck.errorType loError : result.error){
						lineOptionError loe = new lineOptionError();
						loe.errorCode = String.valueOf(loError.code);
						loe.errorMessage = loError.message;
						loe.portfolio = loError.portfolio;
						lineOptionErrors.add(loe); 
					}
				}
				response.errorMessages = lineOptionErrors;
				responses.add(response);
			}	
		}
	}
	
	
	/**
	 * @description			This will return the response returned by ECS. It will contain the postal codes + line info
	 */
	public Response[] getResponse(){
		return responses;
	}

}