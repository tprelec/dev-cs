public abstract class ECSSOAPBaseWebService {
/**
 * @description			This class provides additional functionality common to ECS web services.
 * @author				Guy Clairbois
 */
	
	protected final Integer MAX_TIMEOUT = 120000;
	
	protected IWebServiceConfig webserviceConfig;

	/**
	 * @description			This sets the configuration for the web service.
	 */
	public void setWebServiceConfig(IWebServiceConfig webserviceConfig){
		this.webserviceConfig = webserviceConfig;
	}
	
	
	/** 
	 * @description			This will build a map containing all of the input parameters required for a callout to 
	 *						ECS.
	 */
	protected Map<String, String> getEcsInputHeaders(){
		if(webserviceConfig == null) throw new ExInvalidStateException('Cannot build input headers without the web service configuration being set');
			
		Map<String, String> headers = new Map<String, String>();
		String authHeader = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(webserviceConfig.getUsername() + ':' + webserviceConfig.getPassword()));
		headers.put('Authorization', authHeader);
		headers.put('Accept-encoding', 'gzip');
		return headers;
	}
	
	
	/**
	 * @description			This will attempt to convert a date in a response from ecs to a date primitive. If the 
	 *						value cannot be converted then an exception will be thrown.
	 * @param	sapDate		A date as a string in the format YYYY-MM-DD.
	 * @return				A date primitive set to the value of the date in the parameter passed.
	 */
	protected Date convertToDate(String ecsDate){
			if(ecsDate == null){
				throw new ExInvalidParameterException('Cannot convert a null ECS date into a primitive data type');
			}
		
		Date compiledDate;
			
			try {
				compiledDate = Date.valueOf(ecsDate);
			} catch(TypeException ex){
				throw new ExInvalidParameterException('Unable to convert ECS date into a primitive data type: ' + ecsDate, ex);
			}
		
		return compiledDate;
	}
	
}