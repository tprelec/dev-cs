@isTest
global class ECSSOAPDeliveryMock implements WebServiceMock {
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
        // start - specify the response you want to send
        ECSSOAPDelivery.updateDeliveriesResponse_element respElement = new ECSSOAPDelivery.updateDeliveriesResponse_element();
        List<ECSSOAPDelivery.updateDeliveryResponseType> deliveries = new List<ECSSOAPDelivery.updateDeliveryResponseType>();
        ECSSOAPDelivery.updateDeliveryResponseType deliv = new ECSSOAPDelivery.updateDeliveryResponseType();
        deliv.deliveryNumber = '12345';
        ECSSOAPDelivery.deliveryRowResponseListType delivRows = new ECSSOAPDelivery.deliveryRowResponseListType();
        deliv.deliveryRows = delivRows;

        List<ECSSOAPDelivery.deliveryRowResponseType> deliveryRow = new List<ECSSOAPDelivery.deliveryRowResponseType>();
        ECSSOAPDelivery.deliveryRowResponseType delivRow = new ECSSOAPDelivery.deliveryRowResponseType();
        delivRow.referenceId = '67890';
        ECSSOAPDelivery.additionalRefType additionalReference = new ECSSOAPDelivery.additionalRefType();
        ECSSOAPDelivery.customReferenceType customReference = new ECSSOAPDelivery.customReferenceType();
        customReference.name = 'installedBaseId';
        Customer_Asset__c ca = [SELECT Installed_Base_Id__c FROM Customer_Asset__c LIMIT 1];
        customReference.value = ca.Installed_Base_Id__c;
        additionalReference.customReference = customReference;
        delivRow.additionalReference = additionalReference;
        ECSSOAPDelivery.errorType error = new ECSSOAPDelivery.errorType();
        error.code = '0001';
        error.message = 'Something went wrong';
        delivRow.error = error;
        deliveryRow.add(delivRow);       

        deliv.deliveryRows.deliveryRow = deliveryRow;
/* 
        ECSSOAPDelivery.errorType err = new ECSSOAPDelivery.errorType();
        err.code = 'E404';
        err.message = 'Not Found';
        deliv.error = err;
         */
        Order__c newOrder = [SELECT Id, BOP_Export_Order_Id__c FROM Order__c LIMIT 1];
        deliv.referenceId = newOrder.BOP_Export_Order_Id__c; 

        deliveries.add(deliv);
        respElement.delivery = deliveries;
        response.put('response_x',respElement);
    }
}