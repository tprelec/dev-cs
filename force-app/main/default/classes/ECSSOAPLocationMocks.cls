public with sharing class ECSSOAPLocationMocks {
	public class LocationResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPLocation.locationResponseType theResponse = new ECSSOAPLocation.locationResponseType();
            theResponse.referenceId = 'Test Completed';
			ECSSOAPLocation.locationsResponse_element responseElement = new ECSSOAPLocation.locationsResponse_element();
			responseElement.location = new List<ECSSOAPLocation.locationResponseType>();
			responseElement.location.add(theResponse);
			response.put('response_x', responseElement);
		}
	}
}