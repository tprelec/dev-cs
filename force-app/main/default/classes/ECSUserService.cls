/**
 * @description			This class is responsible for creating and updating users in ECS.
 * @author				Guy Clairbois
 */
public class ECSUserService extends ECSSOAPBaseWebService implements IWebService<List<Request>, List<Response>> {

	/**
	 * @description			This is the request which will be sent to ECS create or update a 
	 *						user. 
	 */
	public class Request {
		// reference to company
        public String companyBanNumber;
        public String companyCorporateId;
        public String companyBopCode;
        
        // key fields
        public String BOPemail; // this field is used for capturing the original email address
        public String email; // this field is used for capturing a changed email address (or the original if it did not change)
		            
		// detail fields		            	
        public String prefix;
        public String firstname;
        public String infix;
        public String lastname;
        public String language;
        public String groupEmail;
        public String jobTitle;
        public String address;
        public String zipcode;
        public String city;
        public String countryId;
        public String phone;
        public String mobile;
        public String fax;
        public String notes;		
	}
	
	
	/**
	 * @description			This is the response that will be returned after making a request to ECS.
	 */
	public class Response {

        public String ban;
        public String corporateId;
        public String bopCode;
        public String email;
        public String errorCode;
        public String errorMessage;
	}
	
	
	private Request[] requests;
	private Response[] responses;
	
	
	/**
	 * @description			The constructor is responsible for setting the default web service configuration.
	 */
	public ECSUserService(){
		if(!Test.isRunningTest()){
			setWebServiceConfig( WebServiceConfigLocator.getConfig('ECSSOAPUser') );
		}
		else setWebServiceConfig( WebServiceConfigLocator.createConfig());
	}
	
	
	/**
	 * @description			This sets the user data to create/update user details in ECS.
	 */
	public void setRequest(Request[] request){
		this.requests = request;
	}
	

	public void makeRequest(String requestType){
		if(requestType=='create'){
			makeCreateRequest();
		} else if(requestType=='update'){
			makeUpdateRequest();
		}
	}
	
	/**
	 * @description			This will make the request to ECS to create users with the request.
	 */
	public void makeCreateRequest(){
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPUser.userSoap service = new ECSSOAPUser.userSoap(); 
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPUser.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		
		List<ECSSOAPUser.userCreateType> createRequest = buildUserCreateUsingRequest();
		System.debug('createRequest: ' + JSON.serialize(createRequest));
		
		// create queryResult
		List<ECSSOAPUser.userResponseType> createResults;
			
		if (Test.isRunningTest()) {
			// unit test cannot do a real callout, so create a result
			//createResult = TestUserCreateService.setupDummyResponse();
		} else {
			try {
				system.debug(createRequest);
				createResults = service.createUsers(createRequest);
				system.debug(createResults);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('Error received from BOP when attempting to create the user(s): '+ex.getMessage(), ex);
			}
		}
		
		parseResponse(createResults);
	}
		

	/**
	 * @description			This will make the request to ECS to update users with the request.
	 */		
	public void makeUpdateRequest(){	
		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(requests == null) throw new ExWebServiceCalloutException('A request has not been set');
		if(requests.isEmpty()) throw new ExWebServiceCalloutException('A request has not been set');
		
		// WebService callout logic
		ECSSOAPUser.userSoap service = new ECSSOAPUser.userSoap(); 
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPUser.authenticationHeader_element header = buildHeaderUsingConfig();
		service.authenticationHeader = header;
		
		List<ECSSOAPUser.userUpdateType> updateRequest = buildUserUpdateUsingRequest();
		System.debug('updateRequest: ' + JSON.serialize(updateRequest));
	
		// create queryResult
		List<ECSSOAPUser.userResponseType> updateResults;
			
		if (Test.isRunningTest()) {
			// unit test cannot do a real callout, so create a result
			//createResult = TestUserCreateService.setupDummyResponse();
		} else {
			try {
				updateResults = service.updateUsers(updateRequest);
			} catch(Exception ex){
				throw new ExWebServiceCalloutException('Error received from BOP when attempting to update the user(s): '+ex.getMessage(), ex);
			}
		}
		
		parseResponse(updateResults);
	}

	
	private ECSSOAPUser.authenticationHeader_element buildHeaderUsingConfig(){
		ECSSOAPUser.authenticationHeader_element header = new ECSSOAPUser.authenticationHeader_element();
		header.applicationId = 'sfdc';
		header.username = webServiceConfig.getUsername();
		header.password = webServiceConfig.getPassword();
		return header;
	}
	

	/**
	 * @description			This will build the create request object which will be sent to ECS. 
	 */
	private List<ECSSOAPUser.userCreateType> buildUserCreateUsingRequest(){
		List<ECSSOAPUser.userCreateType> requestList = new List<ECSSOAPUser.userCreateType>();
		
		for(Request request :this.requests){
			ECSSOAPUser.userCreateType thisUser = new ECSSOAPUser.userCreateType(); 
			
			ECSSOAPUser.companyRefType thisCompany = new ECSSOAPUser.companyRefType();
			thisCompany.banNumber = request.companyBanNumber;
			thisCompany.bopCode = request.companyBopCode;
			thisCompany.corporateId = request.companyCorporateId;
			thisCompany.externalSystem = 'SFDC'; // JvW 01-10-2019 - Added
			thisUser.company = thisCompany;
			thisUser.email = request.email;
			
			thisUser.address = request.address;
			thisUser.city = request.city;
			thisUser.countryId = request.countryId;
			thisUser.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisUser.firstname = request.firstname==null?'-':request.firstname;
			thisUser.groupEmail = request.groupEmail;
			thisUser.infix = request.infix;
			thisUser.jobTitle = request.jobTitle;
			thisUser.language = request.language;
			thisUser.lastname = request.lastname;
			thisUser.mobile = StringUtils.cleanNLPhoneNumber(request.mobile);
			thisUser.notes = request.notes;
			thisUser.phone = StringUtils.cleanNLPhoneNumber(request.phone);
			thisUser.prefix = request.prefix;
			if(request.zipcode != null)
				thisUser.zipcode = (request.zipcode).replaceAll(' ','');
			
			requestList.add(thisUser);			
		}
		
		return requestList;
	}

	
	/**
	 * @description			This will build the update request object which will be sent to ECS. 
	 */
	private List<ECSSOAPUser.userUpdateType> buildUserUpdateUsingRequest(){
		List<ECSSOAPUser.userUpdateType> requestList = new List<ECSSOAPUser.userUpdateType>();
		
		for(Request request :this.requests){
			ECSSOAPUser.userUpdateType thisUser = new ECSSOAPUser.userUpdateType(); 
			
			ECSSOAPUser.companyRefType thisCompany = new ECSSOAPUser.companyRefType();
			thisCompany.banNumber = request.companyBanNumber;
			thisCompany.bopCode = request.companyBopCode;
			thisCompany.corporateId = request.companyCorporateId;
			thisCompany.externalSystem = 'SFDC'; // JvW 01-10-2019 - Added
			thisUser.company = thisCompany;
			thisUser.email = request.BOPemail;
			
			thisUser.newEmail = request.email;
			thisUser.address = request.address;
			thisUser.city = request.city;
			thisUser.countryId = request.countryId;
			thisUser.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisUser.firstname = request.firstname==null?'-':request.firstname;
			thisUser.groupEmail = request.groupEmail;
			thisUser.infix = request.infix;
			thisUser.jobTitle = request.jobTitle;
			thisUser.language = request.language;
			thisUser.lastname = request.lastname;
			thisUser.mobile = StringUtils.cleanNLPhoneNumber(request.mobile);
			thisUser.notes = request.notes;
			thisUser.phone = StringUtils.cleanNLPhoneNumber(request.phone);
			thisUser.prefix = request.prefix;
			if(request.zipcode != null)
				thisUser.zipcode = (request.zipcode).replaceAll(' ','');
						
			requestList.add(thisUser);			
		}
		
		return requestList;
	}	
	
	/**
	 * @description			This will parse the result returned by ECS. 
	 * @param	updateResult	The result object returned by ECS which contains messages and the user number.
	 */
	private void parseResponse(List<ECSSOAPUser.userResponseType> theResults){
		responses = new Response[]{};
		if(theResults == null) return;
		
		System.debug('##### RAW RESPONSE: ' + theResults);
		
		for(ECSSOAPUser.userResponseType result : theResults){
			
			if(result != null){
				Response response = new Response();
				if(result.errors != null && result.errors.error.size() > 0){
					response.errorCode = result.errors.error[0].Code; 
					response.errorMessage = result.errors.error[0].Message;
				}
				response.ban = result.ban;
				response.bopCode = result.bopCode;
				response.corporateId = result.corporateId;
				response.email = result.email;

				System.debug('##### Messages: ' + response.errorMessage);
				
				responses.add(response);
			}	
		}
	}
	
	
	/**
	 * @description			This will return the response returned by ECS. It will contain the user
	 *						number and optionally a list of messages that may need to be displayed to the user.
	 */
	public Response[] getResponse(){
		return responses;
	}

}