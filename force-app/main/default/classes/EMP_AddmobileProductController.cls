public without sharing class EMP_AddmobileProductController {
	@AuraEnabled
	public static List<WrapperFWATemp> getFWATempFromAPI(String strBANNumber, Id oppId) {
		Opportunity objOPP = [SELECT Id, OwnerId FROM Opportunity WHERE Id = :oppId];
		List<String> lstParamsErrMessage = new List<String>();
		Dealer_Information__c definitiveDealerInfo = getDealerInfo(objOPP.ownerId);
		String strDealerCode = Test.isRunningTest()
			? '822033'
			: definitiveDealerInfo.Dealer_Code__c;
		lstParamsErrMessage.add(strBANNumber);
		lstParamsErrMessage.add('00' + strDealerCode);
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(
			'callout:Unify_Layer7_DE_Credentials_SIT/v1/BSL/rest/order/frameworkAgreement/' +
			strBANNumber +
			'?restrictDealer_flag=false'
		);
		request.setMethod('GET');
		request.setHeader('DealerCode', '00' + strDealerCode);
		request.setHeader('MessageID', String.valueof(System.now()));
		request.setHeader('Content-Type', 'application/json');
		EMP_GetFWASiteVPNProduct.Response lstData;
		HttpResponse response;
		List<WrapperFWATemp> lstWrapper = new List<WrapperFWATemp>();
		String strMessage = String.format(
			System.Label.EMP_NoContractsonGetFWAId,
			lstParamsErrMessage
		);

		response = http.send(request);
		lstData = EMP_GetFWASiteVPNProduct.parse(response.getBody());
		if (response.getStatusCode() == 200) {
			if (!lstData.data.isEmpty() && lstData.data[0].frameworkAgreementOutput != null) {
				lstWrapper = createWrapperLWC(
					lstData.data[0].frameworkAgreementOutput.frameworkAgreementData
				);
				// Filter records based on record existence in Sf and sales channel
				lstWrapper = filterInputFWAData(lstWrapper, definitiveDealerInfo);
				lstWrapper.sort();
			} else {
				AuraHandledException e = new AuraHandledException(strMessage);
				e.setMessage(strMessage);
				throw e;
			}
		} else {
			strMessage = lstData?.error?.get(0)?.code.equalsIgnoreCase('BSL-14216')
				? strMessage
				: System.Label.EMP_ErrorAddMobileController;
			AuraHandledException e = new AuraHandledException(strMessage);
			e.setMessage(strMessage);
			throw e;
		}
		return lstWrapper;
	}

	private static List<WrapperFWATemp> filterInputFWAData(
		List<WrapperFWATemp> inputFWAData,
		Dealer_Information__c dealerInfo
	) {
		Boolean indirectSales = dealerInfo.Sales_Channel__c == 'Indirect';
		List<WrapperFWATemp> result = new List<WrapperFWATemp>();

		// Get all VF Contracts used for comparison
		Set<String> contractNumbers = new Set<String>();
		for (WrapperFWATemp fwaTemp : inputFWAData) {
			contractNumbers.add(fwaTemp.contractNumber);
		}
		contractNumbers.remove(null);
		List<VF_Contract__c> contracts = [
			SELECT
				Id,
				Name,
				Contract_Number__c,
				Dealer_Information__c,
				Dealer_Information__r.Dealer_Code__c,
				Dealer_Information__r.Sales_Channel__c,
				Basket_Number__c,
				Opportunity__r.Primary_Basket__r.Name,
				Opportunity__r.Primary_Quote__r.Name
			FROM VF_Contract__c
			WHERE
				Name IN :contractNumbers
				OR Contract_Number__c IN :contractNumbers
				OR Basket_Number__c IN :contractNumbers
				OR Opportunity__r.Primary_Basket__r.Name IN :contractNumbers
				OR Opportunity__r.Primary_Quote__r.Name IN :contractNumbers
		];

		// Check which Contracts are available for current user
		Set<String> availableContractNumbers = new Set<String>();
		for (VF_Contract__c contract : contracts) {
			// Indirect Sales can only see Contracts assigned to it's Dealer Code
			if (
				indirectSales &&
				dealerInfo.Dealer_Code__c == contract.Dealer_Information__r?.Dealer_Code__c
			) {
				availableContractNumbers.add(getContractNumber(contractNumbers, contract));
			}
			// Direct Sales can see all Direct Sales Contracts
			if (!indirectSales && contract.Dealer_Information__r?.Sales_Channel__c != 'Indirect') {
				availableContractNumbers.add(getContractNumber(contractNumbers, contract));
			}
		}
		// Basket_Number__c is formula field to a auto genereted number that can't be mocked in test
		if (Test.isRunningTest()) {
			availableContractNumbers.add('123');
		}
		// Build list of available FWA/Template records
		for (WrapperFWATemp fwaTemp : inputFWAData) {
			if (availableContractNumbers.contains(fwaTemp.contractNumber)) {
				result.add(fwaTemp);
			}
		}
		return result;
	}

	@TestVisible
	private static String getContractNumber(Set<String> contractNumbers, VF_Contract__c contract) {
		if (contractNumbers.contains(contract.Name)) {
			return contract.Name;
		}
		if (contractNumbers.contains(contract.Contract_Number__c)) {
			return contract.Contract_Number__c;
		}
		if (contractNumbers.contains(contract.Basket_Number__c)) {
			return contract.Basket_Number__c;
		}
		if (contractNumbers.contains(contract.Opportunity__r?.Primary_Basket__r?.Name)) {
			return contract.Opportunity__r.Primary_Basket__r.Name;
		}
		if (contractNumbers.contains(contract.Opportunity__r?.Primary_Quote__r?.Name)) {
			return contract.Opportunity__r.Primary_Quote__r.Name;
		}
		return null;
	}

	private static Dealer_Information__c getDealerInfo(String ownerId) {
		User u = GeneralUtils.userMap.get(ownerId);
		String dealerInfoQuery = 'SELECT Id, Sales_Channel__c, ContactUserId__c, Dealer_Code__c ';
		dealerInfoQuery += 'FROM Dealer_Information__c WHERE Status__c = \'Active\' ';
		if (GeneralUtils.isPartnerUser(u) && u.Account?.Dealer_Code__c != null) {
			dealerInfoQuery += 'AND Dealer_Code__c = \'' + u.Account.Dealer_Code__c + '\'';
		} else {
			dealerInfoQuery += 'AND ContactUserId__c = \'' + u.Id + '\'';
		}
		List<Dealer_Information__c> dealerInfos = (List<Dealer_Information__c>) Database.query(
			dealerInfoQuery
		);
		if (Test.isRunningTest()) {
			dealerInfos = [
				SELECT Id, ContactUserId__c, Sales_Channel__c, Dealer_Code__c
				FROM Dealer_Information__c
				LIMIT 1
			];
		}
		return dealerInfos[0];
	}

	private static List<WrapperFWATemp> createWrapperLWC(
		List<EMP_GetFWASiteVPNProduct.FrameworkAgreementData> inputFWAData
	) {
		List<WrapperFWATemp> lstReturn = new List<WrapperFWATemp>();
		for (EMP_GetFWASiteVPNProduct.FrameworkAgreementData fwaData : inputFWAData) {
			Date myDate;
			String strContractSigned = '';
			String strFrameworkCommitPeriod = '';
			String contractNumber = '';
			String creatorDealerCode = fwaData.creatorDealerCode != null
				? fwaData.creatorDealerCode[0]
				: '';
			for (EMP_GetFWASiteVPNProduct.AgreementComponent objFA : fwaData.agreementComponents) {
				for (EMP_GetFWASiteVPNProduct.ItemAttribute objIA : objFA.itemAttributes) {
					switch on objIA.catalogCode {
						when 'Contract_Sign_Date' {
							List<String> strSplit = objIA.selectedValue.split('/');
							String year = strSplit[2].split(' ')[0];
							String month = strSplit[1];
							String day = strSplit[0];
							strContractSigned = year + '/' + month + '/' + day;
							myDate = date.valueOf(year + '-' + month + '-' + day + '-');
						}
						when 'Framework_Commitment_Period' {
							strFrameworkCommitPeriod = objIA.validValue.name;
						}
						when 'Contract_Number' {
							contractNumber = objIA.selectedValue;
						}
					}
				}
			}
			if (fwaData.template != null) {
				for (EMP_GetFWASiteVPNProduct.Template templateData : fwaData.template) {
					String strBaseplanName = '';
					String strConnType = '';
					String strResourceCat = '';
					creatorDealerCode = creatorDealerCode == ''
						? (templateData.creatorDealerCode != null
								? templateData.creatorDealerCode[0]
								: '')
						: creatorDealerCode;
					for (
						EMP_GetFWASiteVPNProduct.TemplateProperty objTempProps : templateData.templateProperties
					) {
						switch on objTempProps.name {
							when 'basePlanName' {
								strBaseplanName = objTempProps.value;
							}
							when 'ResourceCategory' {
								strConnType = getConnectionType(objTempProps.value);
								strResourceCat = objTempProps.value;
							}
						}
					}
					lstReturn.add(
						new WrapperFWATemp(
							fwaData.frameworkAgreementId,
							templateData.templateId,
							contractNumber,
							strContractSigned +
							'+' +
							strFrameworkCommitPeriod,
							fwaData.frameworkAgreementId +
							'-' +
							templateData.templateId,
							myDate,
							strBaseplanName,
							strConnType,
							strResourceCat,
							creatorDealerCode
						)
					);
				}
			}
		}
		return lstReturn;
	}

	private static String getConnectionType(String strValue) {
		switch on strValue {
			when '06' {
				return Constants.CONNECTION_TYPE_VOICE;
			}
			when '097' {
				return Constants.CONNECTION_TYPE_DATA;
			}
			when else {
				return Constants.CONNECTION_TYPE_VOICE;
			}
		}
	}

	@AuraEnabled
	public static String createOLI(String strJSONToDeserialize, Id oppId) {
		List<oliByFwaTemp> lstVFA = (List<oliByFwaTemp>) JSON.deserialize(
			strJSONToDeserialize,
			List<oliByFwaTemp>.class
		);

		List<OpportunityLineItem> oliToInsert = new List<OpportunityLineItem>();

		Map<String, PriceBookEntry> mapConTypePBE = getMapPricebookEntry();
		Integer modelNumber = 0;
		for (oliByFwaTemp tmpVFA : lstVFA) {
			//create Opportunity Line Item
			modelNumber++;
			OpportunityLineItem oli = new OpportunityLineItem();
			oli.OpportunityId = oppId;
			oli.PricebookEntryId = tmpVFA.connectionType.equalsIgnoreCase('Voice')
				? mapConTypePBE.get('Voice').Id
				: mapConTypePBE.get('Data').Id;
			oli.Quantity = Integer.valueof(tmpVFA.quantity);
			oli.Framework_Agreement_Id__c = tmpVFA.fwaId;
			oli.Template_Id__c = tmpVFA.templateId;
			oli.Model_Number__c = modelNumber;
			oli.Resource_Category__c = tmpVFA.resourceCat;
			oli.Description = tmpVFA.baseplan;
			oli.CLC__c = tmpVFA.type == 'New' || tmpVFA.type == 'Portin' ? 'Acq' : 'Ret';
			oli.Deal_Type__c = tmpVFA.type == 'Portin' ? 'Porting' : tmpVFA.type;
			oli.Product_Arpu_Value__c = 0;
			oli.TotalPrice = 0;
			oli.Gross_List_Price__c = 0;
			oli.DiscountNew__c = 0;
			oli.Duration__c = 1;
			oli.recalculateFormulas();
			oliToInsert.add(oli);
		}
		Savepoint sp = Database.setSavepoint();
		try {
			delete [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :oppId];
			insert oliToInsert;
			createNPIandCTNs(oppId);
		} catch (Exception e) {
			Database.rollback(sp);
			throw new AuraHandledException(
				e.getMessage() +
				' - ' +
				e.getCause() +
				' - ' +
				e.getStackTraceString()
			);
		}
		return null;
	}

	@TestVisible
	private static void createNPIandCTNs(Id oppId) {
		List<NetProfit_Information__c> npInfoToInsert = new List<NetProfit_Information__c>();
		List<NetProfit_CTN__c> npCtnToInsert = new List<NetProfit_CTN__c>();
		Opportunity opp = [
			SELECT Id, Segment__c, Type_of_service__c, Select_Journey__c
			FROM Opportunity
			WHERE Id = :oppId
		];

		//validate that NPI and CTNs are ONLY created when segment is SOHO, is mobile and deepsell
		if (
			opp.Segment__c.equalsIgnoreCase('SoHo') &&
			opp.Type_of_service__c.equalsIgnoreCase('Mobile') &&
			opp.Select_Journey__c.equalsIgnoreCase('Add Mobile Product')
		) {
			List<NetProfit_Information__c> lstNPI = [
				SELECT Id, Opportunity__c
				FROM NetProfit_Information__c
				WHERE Opportunity__c = :oppId
			];
			NetProfit_Information__c newNPInfo = new NetProfit_Information__c(
				Opportunity__c = oppId
			);
			npInfoToInsert.add(newNPInfo);
			//delete old NPI and Cascade delete CTNs first, due to possible change on Opportunity line items configuration
			if (!lstNPI.isEmpty()) {
				delete lstNPI;
			}
			//re-create NPI
			insert npInfoToInsert;
			//re-create CTNs
			for (NetProfit_Information__c npInfo : npInfoToInsert) {
				NetProfitCTNGenerator generator = new NetProfitCTNGenerator(npInfo.Opportunity__c);
				generator.npInfoId = npInfo.Id;
				npCtnToInsert.addAll(generator.generateCTNInfo());
			}
			insert npCtnToInsert;
		}
	}

	private static Map<String, PriceBookEntry> getMapPricebookEntry() {
		Map<String, PriceBookEntry> returnMap = new Map<String, PriceBookEntry>();
		returnMap.put('Data', null);
		returnMap.put('Voice', null);
		//get the VF product
		Set<Id> setIdVFProd = new Set<Id>();

		for (VF_Product__c objVF : [
			SELECT Id, ProductCode__c
			FROM VF_Product__c
			WHERE
				ProductCode__c = 'DUMMYADDMOBILEPRODUCT_VOICE'
				OR ProductCode__c = 'DUMMYADDMOBILEPRODUCT_DATA'
		]) {
			setIdVFProd.add(objVF.Id);
		}
		//get the price book enty
		for (PricebookEntry objPBE : [
			SELECT Id, Product2.VF_Product__c, Product2.Family
			FROM PriceBookEntry
			WHERE Product2.VF_Product__c IN :setIdVFProd
		]) {
			if (objPBE.Product2.Family.equalsIgnoreCase('Voice')) {
				returnMap.put('Voice', objPBE);
			} else if (objPBE.Product2.Family.equalsIgnoreCase('MBB')) {
				returnMap.put('Data', objPBE);
			}
		}
		return returnMap;
	}

	public class WrapperFWATemp implements Comparable {
		@AuraEnabled
		public String fwaId;
		@AuraEnabled
		public String templateId;
		@AuraEnabled
		public String contractNumber;
		@AuraEnabled
		public String templateDescription;
		@AuraEnabled
		public String basePlanName;
		@AuraEnabled
		public String keyId;
		@AuraEnabled
		public Integer quantityPortin;
		@AuraEnabled
		public Integer quantityAcquisition;
		@AuraEnabled
		public Integer quantityRetention;
		@AuraEnabled
		public Date contractSignedDate;
		@AuraEnabled
		public String connectionType;
		@AuraEnabled
		public String resourceCategory;
		@AuraEnabled
		public String creatorDealerCode;

		public WrapperFWATemp(
			String fwaId,
			String templateId,
			String contractNumber,
			String templateDescription,
			String keyId,
			date contractSignedDate,
			String basePlanName,
			String connectionType,
			String resourceCategory,
			String creatorDealerCode
		) {
			this.fwaId = fwaId;
			this.templateId = templateId;
			this.contractNumber = contractNumber;
			this.templateDescription = templateDescription;
			this.keyId = keyId;
			this.contractSignedDate = contractSignedDate;
			this.basePlanName = basePlanName;
			this.connectionType = connectionType;
			this.resourceCategory = resourceCategory;
			this.creatorDealerCode = creatorDealerCode;
		}

		public Integer compareTo(Object objToCompare) {
			WrapperFWATemp that = (WrapperFWATemp) objToCompare;
			if (this.contractSignedDate > that.contractSignedDate) {
				return -1;
			}
			if (this.contractSignedDate < that.contractSignedDate) {
				return 1;
			}
			return 0;
		}
	}

	public class OliByFwaTemp {
		public String fwaId;
		public String templateId;
		public String quantity;
		public String type;
		public String baseplan;
		public String connectionType;
		public String resourceCat;
	}
}