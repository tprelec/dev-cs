public with sharing class EMP_ChangeProductSettingsController {
    @AuraEnabled(cacheable=true)
    public static List<VF_Asset__c> getVFAssetList(String oppId, Integer pageSize, Integer pageNumber, String searchKey) {
        Boolean isEmptySearch = String.isEmpty(searchKey);
        String strUserId = UserInfo.getUserId();
        String strQuerySearch = '%' + searchKey + '%';
        Integer pn = pageNumber - 1;
        Integer offset= pageSize * pn;
        //get VF assets
        String accId = [SELECT AccountId FROM Opportunity Where id =: oppId ].AccountId;
        searchKey = String.escapeSingleQuotes(strQuerySearch);
        String strQry = 'SELECT Id, Name, Priceplan_Class__c, ';
            strQry += 'Priceplan_Description__c, ';
            strQry += 'Retainable__c, ';
            strQry += 'Contract_End_Date__c, Contract_VF_Name__c, BAN_Number__c, Assigned_Product_Id__c, CTN__c ';
            strQry += 'FROM VF_Asset__c ';
            strQry += 'WHERE Account__c =: accId ';
            strQry += 'AND CTN__c != null ';
            strQry += 'AND Owner__c =: strUserId ';
            strQry += isEmptySearch?'':' AND CTN__c LIKE: searchKey ';
            strQry += 'LIMIT : pageSize ';
            strQry += 'OFFSET: offset';
        List<VF_Asset__c> lstReturn = (List<VF_Asset__c>) Database.Query( String.escapeSingleQuotes(strQry) );
        return lstReturn;
    }

    @AuraEnabled
    public static String createOLI(String strJSONToDeserialize, Id oppId) {
        List<VF_Asset__c> lstVFA = (List<VF_Asset__c>) JSON.deserialize(strJSONToDeserialize, List<VF_Asset__c>.class);

        List<OpportunityLineItem> oliToInsert = new List<OpportunityLineItem>();
        //get the VF product
        VF_Product__c dummyVFProduct = new VF_Product__c();
        dummyVFProduct = [SELECT Id, ProductCode__c 
                          FROM VF_Product__c 
                          WHERE ProductCode__c = 'DUMMYUPDATEMOBILEPRODUCT' LIMIT 1];
        //get the price book enty
        PriceBookEntry dummyPBE = new PriceBookEntry();
        dummyPBE = [SELECT Id, Product2.CLC__c, Product2.VF_Product__c,Product2.Family,Product2.Quantity_Type__c,Product2.Product_Line__c 
                    FROM PriceBookEntry 
                    WHERE Product2.VF_Product__c = :dummyVFProduct.Id AND Product2.CLC__c = 'Acq' LIMIT 1];
        
        Integer modelNumber = 0;
        for(VF_Asset__c tmpVFA: lstVFA) {
            //create Opportunity Line Item
            modelNumber++;
            OpportunityLineItem oli = new OpportunityLineItem();
            oli.OpportunityId = oppId;
            oli.PricebookEntryId = dummyPBE.Id;
            oli.Quantity = 1;
            oli.Assigned_Product_Id__c = tmpVFA.Assigned_Product_Id__c;//used on the integration as target record on bsl system
            oli.Model_Number__c = modelNumber;
            oli.Description = tmpVFA.Priceplan_Description__c;
            oli.CTN_number__c = tmpVFA.CTN__c;
            oli.Product_Arpu_Value__c = 0;
            oli.TotalPrice = 0;
            oli.Gross_List_Price__c = 0;
            oli.DiscountNew__c = 0;
            oli.Duration__c = 1;
            oli.recalculateFormulas();
            oliToInsert.add(oli);
        }
        try{
            delete [SELECT id FROM OpportunityLineItem Where OpportunityId =: oppId ];
            insert oliToInsert;
        }
        catch(Exception e) {
            System.debug(Logginglevel.ERROR, e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
        return null;
    }
}