public with sharing class EMP_CustomerProductDetails {
	public Response getProductDetailsResponse(String dealerCode, String banNumber) {
		HttpRequest httpRequest = EMP_BSLintegration.createBSLRequest(
			'productInfo/getCustomerProductDetails',
			dealerCode,
			JSON.serialize(new Request(banNumber), true)
		);
		HttpResponse httpResponse = (new Http()).send(httpRequest);
		Response response = (Response) JSON.deserialize(httpResponse.getBody(), Response.class);
		response.setHttpData(httpRequest, httpResponse);
		return response;
	}

	public Response getErrorResponse(String errorMsg) {
		Response response = new Response();
		response.status = 400;
		response.error = new List<Error>();
		response.error.add(new Error('SF custom error', errorMsg));
		return response;
	}

	public Response checkResponseForNoGlpError(Response response) {
		if (response.error.isEmpty() || response.error[0].code != 'BSL-12005') {
			return response;
		}

		String emptyJsonResponse = '{"status":200,"data":[{}],"error":[]}';
		return (Response) JSON.deserialize(emptyJsonResponse, Response.class);
	}

	public class Request {
		public String customerID;
		public String productCode;

		public Request(String customerId) {
			this.customerID = customerId;
			productCode = 'GL_Billing_Services_Main';
		}
	}

	public class Response {
		public Integer status;
		public String httpRequest;
		public String httpResponse;
		public List<Error> error;
		public List<CustomerProductDetailsData> data;

		public void setHttpData(HttpRequest request, HttpResponse response) {
			httpResponse = response.getBody();
			httpRequest =
				'Dealer Code: ' +
				request.getHeader('DealerCode') +
				' MessageId: ' +
				request.getHeader('MessageID') +
				' Body: ' +
				request.getBody();
		}

		public String getErrorMessages() {
			String errorMsg = '';
			for (Error errorDetails : error) {
				errorMsg += 'Status code: ' + errorDetails.code;
				errorMsg += ' => Message: ' + errorDetails.message + ';  ';
			}
			return errorMsg;
		}
	}

	public class CustomerProductDetailsData {
		public CustomerProductDetailsOutputData getCustomerProductDetailsOutputData;
	}

	public class CustomerProductDetailsOutputData {
		public List<CustomerProductDetails> implementedProductData;
	}

	public class CustomerProductDetails {
		public String offerName;
		public Long catalogProductID;
		public String catalogProductCod;
		public Long assignedProducID;
		public List<CustomerProductDetails> childImplementedProducts;
		public List<CustomerProductPricingReference> pricingReferenceData;
	}

	public class CustomerProductPricingReference {
		public Long catalogPricingID;
	}

	public class Error {
		public String code;
		public String message;

		public Error(String errorCode, String errorMsg) {
			code = errorCode;
			message = errorMsg;
		}
	}
}