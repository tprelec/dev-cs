@SuppressWarnings('PMD.ExcessivePublicCount')
public class EMP_GetAvailableResources {
	public class Request {
		public List<String> category;
		public String customerId; // ban number
		//public String dedicatedCustomerPool; // optional only in case of Customer ask for use their own pool this value is set to yes
		public final String type; //mandatory, hardcoded 'MSISDN'
		public PaginationData paginationData;
		public Request(String tempCustomerId, PaginationData tempPaginationData) {
			this.customerId = tempCustomerId;
			this.type = 'MSISDN';
			this.paginationData = tempPaginationData;
		}
	}

	public class PaginationData {
		public Integer pageSize;
		public Integer pageNumber;
		public Integer numberOfRows;
		public PaginationData(
			Integer tempPageSize,
			Integer tempPageNumber,
			String tempDirection,
			Integer tempNumberOfRows
		) {
			this.pageSize = tempPageSize;
			this.pageNumber = tempPageNumber;
			this.numberOfRows = tempNumberOfRows;
		}
		public PaginationData(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'pageSize' {
								pageSize = parser.getIntegerValue();
							}
							when 'pageNumber' {
								pageNumber = parser.getIntegerValue();
							}
							when 'numberOfRows' {
								numberOfRows = parser.getIntegerValue();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'PaginationData consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class PaginationDataRes {
		public String pageSize;
		public String pageNumber;
		public String paginationCursorInfoKey;
		public String numberOfRows;
		public PaginationDataRes(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'pageSize' {
								pageSize = parser.getText();
							}
							when 'pageNumber' {
								pageNumber = parser.getText();
							}
							when 'paginationCursorInfoKey' {
								paginationCursorInfoKey = parser.getText();
							}
							when 'numberOfRows' {
								numberOfRows = parser.getText();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'PaginationData consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class Response {
		public String status;
		public List<Data> data;
		public List<EMP_ResponseError.Error> error;
		public Response(String json) {
			System.JSONParser parser = System.JSON.createParser(json);
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'status' {
								status = parser.getText();
							}
							when 'data' {
								data = arrayOfData(parser);
							}
							when 'error' {
								error = EMP_ResponseError.arrayOfError(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'Response consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class Data {
		public List<Resources> resources;
		public PaginationDataRes paginationData;
		public Data(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'resources' {
								resources = arrayOfResources(parser);
							}
							when 'paginationData' {
								paginationData = new PaginationDataRes(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'Data consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class Resources {
		public String numberCTN;
		public String type;
		public String status;

		public Resources(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'number' {
								numberCTN = parser.getText();
							}
							when 'type' {
								type = parser.getText();
							}
							when 'status' {
								status = parser.getText();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'Resources consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	private static List<Resources> arrayOfResources(System.JSONParser p) {
		List<Resources> res = new List<Resources>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Resources(p));
		}
		return res;
	}

	private static List<Data> arrayOfData(System.JSONParser p) {
		List<Data> res = new List<Data>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Data(p));
		}
		return res;
	}
}