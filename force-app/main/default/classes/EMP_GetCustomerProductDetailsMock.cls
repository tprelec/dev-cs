@isTest
public class EMP_GetCustomerProductDetailsMock implements HttpCalloutMock {
	public HTTPResponse respond(HTTPRequest req) {
		EMP_CustomerProductDetails.CustomerProductDetails cpd = new EMP_CustomerProductDetails.CustomerProductDetails();
		cpd.offerName = 'offer name';
		cpd.catalogProductID = 1234567;
		cpd.catalogProductCod = 'product code';

		EMP_CustomerProductDetails.CustomerProductDetailsOutputData outputData = new EMP_CustomerProductDetails.CustomerProductDetailsOutputData();
		outputData.implementedProductData = new List<EMP_CustomerProductDetails.CustomerProductDetails>{
			cpd
		};

		EMP_CustomerProductDetails.CustomerProductDetailsData cpdData = new EMP_CustomerProductDetails.CustomerProductDetailsData();
		cpdData.getCustomerProductDetailsOutputData = outputData;

		EMP_CustomerProductDetails.Response response = new EMP_CustomerProductDetails.Response();
		response.status = 200;
		response.error = new List<EMP_CustomerProductDetails.Error>();
		response.data = new List<EMP_CustomerProductDetails.CustomerProductDetailsData>();
		response.data.add(cpdData);

		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody(JSON.serialize(response));
		res.setStatusCode(200);
		return res;
	}
}