public class EMP_GetOrderSummary {
	public class Request {
		public String orderID; // comes from NetProfit CTN
		public Boolean skipDealerRestriction; //hardcoded as false
		public Request(String tempOrderID) {
			this.orderID = tempOrderID;
			this.skipDealerRestriction = false;
		}
	}

	public static Response parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new Response(parser);
	}

	public class Response {
		public Integer status { get; set; }
		public List<Data> data { get; set; }
		public List<EMP_ResponseError.Error> error { get; set; }
		public Response(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'status' {
								status = parser.getIntegerValue();
							}
							when 'data' {
								data = arrayOfData(parser);
							}
							when 'error' {
								error = EMP_ResponseError.arrayOfError(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'JSON2Apex consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class Data {
		public List<OrderSummary> orderSummary { get; set; }
		public Data(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'orderSummary' {
								orderSummary = arrayOfOrderSummary(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'Data consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class OrderSummary {
		public OrderHeader orderHeader { get; set; }
		public List<ProvideOrderActionsSummary> provideOrderActionsSummary { get; set; }
		public OrderSummary(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'provideOrderActionsSummary' {
								provideOrderActionsSummary = arrayOfProvideOrderActionsSummary(
									parser
								);
							}
							when 'orderHeader' {
								orderHeader = new OrderHeader(parser);
							}
							when 'totalPrices' {
								parser.skipChildren();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'OrderSummary consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class ProvideOrderActionsSummary {
		public List<OrderActionSummary> orderActionSummary { get; set; }
		public ProvideOrderActionsSummary(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'implementedOffer' {
								parser.skipChildren();
							}
							when 'orderActionSummary' {
								orderActionSummary = arrayOfOrderActionSummary(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'ProvideOrderActionsSummary consuming unrecognized property: ' +
									text
								);
							}
						}
					}
				}
			}
		}
	}

	private static List<ProvideOrderActionsSummary> arrayOfProvideOrderActionsSummary(
		System.JSONParser p
	) {
		List<ProvideOrderActionsSummary> res = new List<ProvideOrderActionsSummary>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new ProvideOrderActionsSummary(p));
		}
		return res;
	}

	public class OrderActionSummary {
		public String portingStatus { get; set; }
		public String rejectionStatus { get; set; }
		public String orderActionType { get; set; }
		public String orderActionStatus { get; set; }
		public Long serviceRequiredDate { get; set; }
		public Long wishDate { get; set; }
		public orderActionSummary(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switchOnAttributesOrderActionSummary(text, parser);
					}
				}
			}
		}
		private void switchOnAttributesOrderActionSummary(String text, JSONParser parser) {
			switch on text {
				when 'implementedProduct' {
					parser.skipChildren();
				}
				when 'portingStatus' {
					portingStatus = parser.getText();
				}
				when 'rejectionStatus' {
					rejectionStatus = parser.getText();
				}
				when 'orderActionType' {
					orderActionType = parser.getText();
				}
				when 'orderActionStatus' {
					orderActionStatus = parser.getText();
				}
				when 'serviceRequiredDate' {
					serviceRequiredDate = parser.getLongValue();
				}
				when 'wishDate' {
					wishDate = parser.getLongValue();
				}
				when else {
					System.debug(
						LoggingLevel.WARN,
						'OrderActionSummary consuming unrecognized property: ' + text
					);
				}
			}
		}
	}

	private static List<OrderActionSummary> arrayOfOrderActionSummary(System.JSONParser p) {
		List<OrderActionSummary> res = new List<OrderActionSummary>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new OrderActionSummary(p));
		}
		return res;
	}

	public class OrderHeader {
		public String orderID { get; set; }
		public Integer daysToExpiration { get; set; }
		public Long applicationDate { get; set; }
		public String orderStatus { get; set; }
		public CustomerProfileHeader customerProfileHeader { get; set; }
		public String creditVettingStatus { get; set; }
		public String creditVettingReason { get; set; }
		public String cancelPonr { get; set; }
		public String amendPonr { get; set; }
		public String deliveryMethod { get; set; }
		public String trackAndTraceId { get; set; }
		public Long contactID { get; set; }
		public OrderHeader(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switchOnAttributesOrderHeader(text, parser);
					}
				}
			}
		}
		private void switchOnAttributesOrderHeader(String text, JSONParser parser) {
			switch on text {
				when 'orderID' {
					orderID = parser.getText();
				}
				when 'daysToExpiration' {
					daysToExpiration = parser.getIntegerValue();
				}
				when 'applicationDate' {
					applicationDate = parser.getLongValue();
				}
				when 'orderStatus' {
					orderStatus = parser.getText();
				}
				when 'customerProfileHeader' {
					customerProfileHeader = new CustomerProfileHeader(parser);
				}
				when 'creditVettingStatus' {
					creditVettingStatus = parser.getText();
				}
				when 'creditVettingReason' {
					creditVettingReason = parser.getText();
				}
				when 'cancelPonr' {
					cancelPonr = parser.getText();
				}
				when 'amendPonr' {
					amendPonr = parser.getText();
				}
				when 'deliveryMethod' {
					deliveryMethod = parser.getText();
				}
				when 'trackAndTraceId' {
					trackAndTraceId = parser.getText();
				}
				when 'contactID' {
					contactID = parser.getLongValue();
				}
				when else {
					System.debug(
						LoggingLevel.WARN,
						'OrderHeader consuming unrecognized property: ' + text
					);
				}
			}
		}
	}

	public class CustomerProfileHeader {
		public String customerName { get; set; }
		public Integer customerID { get; set; }
		public CustomerProfileHeader(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'customerName' {
								customerName = parser.getText();
							}
							when 'customerID' {
								customerID = parser.getIntegerValue();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'CustomerProfileHeader consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	private static List<Data> arrayOfData(System.JSONParser p) {
		List<Data> res = new List<Data>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Data(p));
		}
		return res;
	}

	private static List<OrderSummary> arrayOfOrderSummary(System.JSONParser p) {
		List<OrderSummary> res = new List<OrderSummary>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new OrderSummary(p));
		}
		return res;
	}

	//needed for bulk
	public class GetOrderSummaryBulkRequestTop {
		public GetOrderSummaryBulkRequest getOrderSummaryBulkRequest;
		public GetOrderSummaryBulkRequestTop(
			GetOrderSummaryBulkRequest getOrderSummaryBulkRequest
		) {
			this.getOrderSummaryBulkRequest = getOrderSummaryBulkRequest;
		}
	}

	public class GetOrderSummaryBulkRequest {
		public List<SharedBslHttpHeader> sharedBslHttpHeaders;
		public List<GetOrderSummaryRequest> getOrderSummaryRequests;
		public GetOrderSummaryBulkRequest(
			List<SharedBslHttpHeader> sharedBslHttpHeaders,
			List<GetOrderSummaryRequest> getOrderSummaryRequests
		) {
			this.sharedBslHttpHeaders = sharedBslHttpHeaders;
			this.getOrderSummaryRequests = getOrderSummaryRequests;
		}
	}

	public class GetOrderSummaryRequest {
		public List<BslHttpHeader> bslHttpHeaders;
		public String correlationID; //SF Id
		public Integer priority;
		public String orderID;
		public GetOrderSummaryRequest(
			List<BslHttpHeader> requestHeaders,
			String correlationID,
			Integer priority,
			String orderID
		) {
			this.bslHttpHeaders = requestHeaders;
			this.correlationID = correlationID;
			this.priority = priority;
			this.orderID = orderID;
		}
	}

	public class SharedBslHttpHeader {
		public String name;
		public String value;
		public SharedBslHttpHeader(String name, String value) {
			this.name = name;
			this.value = value;
		}
	}

	public class BslHttpHeader {
		public String name;
		public String value;
		public BslHttpHeader(String name, String value) {
			this.name = name;
			this.value = value;
		}
	}
}