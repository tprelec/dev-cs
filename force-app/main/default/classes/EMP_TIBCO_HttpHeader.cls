public with sharing class EMP_TIBCO_HttpHeader {
	public String name { get; set; }
	public String value { get; set; }

	public EMP_TIBCO_HttpHeader(String name, String value) {
		this.name = name;
		this.value = value;
	}
}