// Provides methods used for integration with TIBCO
public with sharing class EMP_TIBCO_Integration {
	private static final String NAMED_CREDENTIAL_NAME = 'Unify_Layer7_NL_Credentials';
	private static final String VALIDATE_BULK_URL = '/v1/Tibco/b2bmobileprovisioning/v1/rest/order/validate';
	private static String dealerCodeForHeader;

	private static HttpRequest buildHttpRequest(String relativeUrl, String method, Object payload) {
		HttpRequest req = new HttpRequest();
		req.setEndpoint('callout:' + NAMED_CREDENTIAL_NAME + relativeUrl);
		req.setMethod(method);
		if (payload != null) {
			req.setBody(JSON.serialize(payload, true));
		}
		req.setHeader('Content-Type', 'application/json');
		return req;
	}

	// Executes Retention for provided list of CTNs
	public static EMP_TIBCO_ValidateOrderFEBulk.Response executeRetention(Set<Id> ctnIds) {
		List<NetProfit_CTN__c> ctns = getCTNs(ctnIds);
		//no need to validate the list to empty, this class and method can only be invoked when there are CTNs for the order
		String strOpportunity = ctns?.get(0)?.Order__r?.VF_Contract__r?.Opportunity__c;
		//invoke method to retrieve right dealerCode from Opportunity.
		//No check for null on oppId. is better to Orchestrator show the runtime error instead of creating a request incomplete towards BSL
		dealerCodeForHeader = EMP_BSLintegrationHelper.getAccountDealerCode(strOpportunity);
		// Prepare request payload
		List<EMP_TIBCO_ValidateOrderFEBulk.ValidateOrderFERequest> tibcoReqs = new List<EMP_TIBCO_ValidateOrderFEBulk.ValidateOrderFERequest>();
		for (NetProfit_CTN__c ctn : ctns) {
			if (ctn.Product_Group__c == 'Priceplan') {
				tibcoReqs.add(buildRetentionTIBCORequest(ctn));
			}
		}
		EMP_TIBCO_ValidateOrderFEBulk.Request tibcoReq = new EMP_TIBCO_ValidateOrderFEBulk.Request(
			buildHeaders(ctns[0].Order__r),
			tibcoReqs
		);
		// Prepare Http request and send it
		HttpRequest req = buildHttpRequest(VALIDATE_BULK_URL, 'POST', tibcoReq);
		Http h = new Http();
		HttpResponse resp = h.send(req);
		// Handle Response
		EMP_TIBCO_ValidateOrderFEBulk.Response response = (EMP_TIBCO_ValidateOrderFEBulk.Response) JSON.deserialize(
			resp.getBody(),
			EMP_TIBCO_ValidateOrderFEBulk.Response.class
		);
		response.httpRequest =
			'Dealer Code: ' +
			req.getHeader('DealerCode') +
			' MessageId: ' +
			req.getHeader('MessageID') +
			' Body: ' +
			req.getBody();
		response.httpResponse = resp.getBody();
		return response;
	}

	private static List<NetProfit_CTN__c> getCTNs(Set<Id> ctnIds) {
		return [
			SELECT
				Id,
				CTN_Number__c,
				Assigned_Product_Id__c,
				Product_Group__c,
				Framework_Agreement_Id__c,
				Template_Id__c,
				Order__r.VF_Contract__r.Opportunity__c,
				Order__r.VF_Contract__r.BAN__c,
				Order__r.VF_Contract__r.Dealer_Information__r.Dealer_Code__c,
				Order__r.VF_Contract__r.Unify_Framework_Id__c,
				Contracted_Product__r.Unify_Template_Id__c,
				APN__c,
				apn2__c,
				apn3__c,
				apn4__c,
				dataBlock__c,
				outgoingService__c,
				thirdPartyContent__c,
				premiumDestinations__c,
				SMSPremium__c,
				roamingspendingLimit__c
			FROM NetProfit_CTN__c
			WHERE Id IN :ctnIds
		];
	}

	private static EMP_TIBCO_ValidateOrderFEBulk.ValidateOrderFERequest buildRetentionTIBCORequest(
		NetProfit_CTN__c ctn
	) {
		return new EMP_TIBCO_ValidateOrderFEBulk.ValidateOrderFERequest(
			buildHeaders(ctn.Order__r),
			ctn.Id,
			buildRetentionBSLRequest(ctn)
		);
	}

	private static EMP_ValidateOrderFE.Request buildRetentionBSLRequest(NetProfit_CTN__c ctn) {
		String fwaId = String.isNotBlank(ctn.Framework_Agreement_Id__c)
			? ctn.Framework_Agreement_Id__c
			: ctn.Order__r.VF_Contract__r.Unify_Framework_Id__c;
		EMP_ValidateOrderFE.OrderHeader orderHeader = new EMP_ValidateOrderFE.OrderHeader(
			ctn.Id,
			fwaId,
			true
		);
		orderHeader.targetTemplateId = ctn.Contracted_Product__r.Unify_Template_Id__c;
		orderHeader.replaceTemplate = true;
		EMP_ValidateOrderFE.OrderData orderData = new EMP_ValidateOrderFE.OrderData(
			null,
			orderHeader,
			buildRetentionItem(ctn),
			false
		);
		EMP_ValidateOrderFE.OrderInputData orderInputData = new EMP_ValidateOrderFE.OrderInputData(
			orderData
		);
		orderInputData.mode = 'ValidateAndSubmit';
		EMP_ValidateOrderFE.Request req = new EMP_ValidateOrderFE.Request();
		req.validateOrderInput = orderInputData;
		return req;
	}

	private static List<EMP_ValidateOrderFE.Item> buildRetentionItem(NetProfit_CTN__c ctn) {
		EMP_ValidateOrderFE.Item item = new EMP_ValidateOrderFE.Item('MODIFY');
		item.assignedId = ctn.Assigned_Product_Id__c;
		item.activity = 'CHANGE_ASSIGNED_ITEM';
		item.reasonCode = 'RETN';
		List<EMP_ValidateOrderFE.Item> settingsItems = buildRetentionUpdateProductSettings(ctn);
		if (!settingsItems.isEmpty()) {
			item.items = settingsItems;
		}
		return new List<EMP_ValidateOrderFE.Item>{ item };
	}

	private static List<EMP_ValidateOrderFE.Item> buildRetentionUpdateProductSettings(
		NetProfit_CTN__c ctn
	) {
		EMP_UpdateProductSettingsService svc = new EMP_UpdateProductSettingsService(ctn);
		return svc.getUpdateProductSettingsForRetention();
	}

	private static List<EMP_TIBCO_HttpHeader> buildHeaders(Order__c order) {
		List<EMP_TIBCO_HttpHeader> headers = new List<EMP_TIBCO_HttpHeader>();
		headers.add(new EMP_TIBCO_HttpHeader('DealerCode', '00' + dealerCodeForHeader));
		headers.add(new EMP_TIBCO_HttpHeader('MessageID', String.valueOf(System.now())));
		return headers;
	}
}