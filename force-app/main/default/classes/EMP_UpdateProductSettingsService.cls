public inherited sharing class EMP_UpdateProductSettingsService {
	private static final Map<String, String> ATTRIBUTES_TO_CTN_FIELDS = new Map<String, String>{
		// Data
		'APN1' => 'APN__c',
		'APN2' => 'apn2__c',
		'APN3' => 'apn3__c',
		'APN4' => 'apn4__c',
		'Internet_Access' => 'dataBlock__c',
		// Settings
		'Outgoing_Service_Settings' => 'outgoingService__c',
		'Third_Party_Content' => 'thirdPartyContent__c',
		'Premium_Destinations' => 'premiumDestinations__c',
		'SMS_Premium' => 'SMSPremium__c',
		// Data Roaming
		'Roaming_Spending_Limit' => 'roamingspendingLimit__c'
	};

	private static Map<String, EMP_Component_Attribute__mdt> attributesMapCache;
	private NetProfit_CTN__c ctn;
	private Map<String, String> attributeValues;

	public EMP_UpdateProductSettingsService(NetProfit_CTN__c ctn) {
		this.ctn = ctn;
	}

	private Map<String, EMP_Component_Attribute__mdt> getAttributesMap() {
		if (attributesMapCache == null) {
			List<EMP_Component_Attribute__mdt> attributes = [
				SELECT
					Id,
					Attribute_Code__c,
					EMP_Component_Header__r.Catalog_Id__c,
					EMP_Component_Header__r.Catalog_Code__c
				FROM EMP_Component_Attribute__mdt
				WHERE
					Active__c = TRUE
					AND EMP_Component_Header__r.Active__c = TRUE
					AND Attribute_Code__c IN :ATTRIBUTES_TO_CTN_FIELDS.keySet()
			];
			attributesMapCache = new Map<String, EMP_Component_Attribute__mdt>();
			for (EMP_Component_Attribute__mdt attribute : attributes) {
				attributesMapCache.put(attribute.Attribute_Code__c, attribute);
			}
		}
		return attributesMapCache;
	}

	public List<EMP_ValidateOrderFE.Item> getUpdateProductSettingsForRetention() {
		Map<String, EMP_ValidateOrderFE.Item> itemsByCatalogCode = new Map<String, EMP_ValidateOrderFE.Item>();
		getAttributeValues();
		for (String attributeCode : attributeValues.keySet()) {
			EMP_Component_Attribute__mdt attribute = getAttributesMap().get(attributeCode);
			String catalogCode = attribute.EMP_Component_Header__r.Catalog_Code__c;
			if (!itemsByCatalogCode.containsKey(catalogCode)) {
				EMP_ValidateOrderFE.Item item = new EMP_ValidateOrderFE.Item(
					'MODIFY',
					attribute.EMP_Component_Header__r.Catalog_Id__c,
					catalogCode
				);
				item.items = null;
				item.attributes = new List<EMP_ValidateOrderFE.Attributes>();
				itemsByCatalogCode.put(catalogCode, item);
			}
			itemsByCatalogCode.get(catalogCode)
				.attributes.add(
					new EMP_ValidateOrderFE.Attributes(
						attributeCode,
						attributeValues.get(attributeCode)
					)
				);
		}
		return itemsByCatalogCode.values();
	}

	public List<EMP_ActivateSIMForTemplate.UpdateProductSettingsInput> getUpdateProductSettingsForSIMActivation() {
		Map<String, EMP_ActivateSIMForTemplate.UpdateProductSettingsInput> itemsByCatalogCode = new Map<String, EMP_ActivateSIMForTemplate.UpdateProductSettingsInput>();
		getAttributeValues();
		for (String attributeCode : attributeValues.keySet()) {
			EMP_Component_Attribute__mdt attribute = getAttributesMap().get(attributeCode);
			String catalogCode = attribute.EMP_Component_Header__r.Catalog_Code__c;
			if (!itemsByCatalogCode.containsKey(catalogCode)) {
				EMP_ActivateSIMForTemplate.UpdateProductSettingsInput item = new EMP_ActivateSIMForTemplate.UpdateProductSettingsInput(
					catalogCode,
					new List<EMP_ActivateSIMForTemplate.ComponentSetting>()
				);
				itemsByCatalogCode.put(catalogCode, item);
			}
			itemsByCatalogCode.get(catalogCode)
				.component_settings.add(
					new EMP_ActivateSIMForTemplate.ComponentSetting(
						attributeCode,
						attributeValues.get(attributeCode)
					)
				);
		}
		return itemsByCatalogCode.values();
	}

	private void getAttributeValues() {
		attributeValues = new Map<String, String>();
		for (EMP_Component_Attribute__mdt attribute : getAttributesMap().values()) {
			String attributeCode = attribute.Attribute_Code__c;
			String fieldName = ATTRIBUTES_TO_CTN_FIELDS.get(attributeCode);
			String val = (String) ctn.get(fieldName);
			if (val != null) {
				attributeValues.put(attributeCode, val);
			}
		}
	}
}