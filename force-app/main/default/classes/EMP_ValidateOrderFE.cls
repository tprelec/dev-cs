public class EMP_ValidateOrderFE {
	public class Request {
		public OrderInputData validateOrderInput;
	}

	public class OrderInputData {
		public String mode; //mandatory
		public OrderData orderData; //mandatory
		public OrderInputData(OrderData orderData) {
			this.mode = 'ValidateAndSave';
			this.orderData = orderData;
		}
	}

	public class OrderData {
		public CustomerInfo customerInfo;
		public OrderHeader orderHeader;
		public List<Item> items;
		public Boolean isQuotationRequired;
		public Boolean provideTemplate;
		public OrderData(
			CustomerInfo customerInfo,
			OrderHeader orderHeader,
			List<Item> items,
			Boolean provideTemplate
		) {
			this.customerInfo = customerInfo;
			this.orderHeader = orderHeader;
			this.items = items;
			this.isQuotationRequired = false;
			this.provideTemplate = provideTemplate; // enables inheritance values
		}
	}

	public class CustomerInfo {
		public String billingCustomerId;
		public CustomerInfo(String billingCustomerId) {
			this.billingCustomerId = billingCustomerId;
		}
	}

	public class OrderHeader {
		public String externalOrderId;
		public Boolean isSuspicious;
		public String frameworkAgreementId;
		public String productType;
		public String targetTemplateId { get; set; }
		public Boolean replaceTemplate { get; set; }

		public OrderHeader(
			String externalOrderId,
			String frameworkAgreementId,
			Boolean isTemplate
		) {
			this.externalOrderId = externalOrderId;
			this.isSuspicious = false;
			this.frameworkAgreementId = isTemplate ? frameworkAgreementId : null; //checking if is FWA or Template
		}
	}

	//top level item and item are the same, just activity must be populated on the first item
	public class Item {
		public String action; //mandatory and dynamic
		public String activity; //mandatory only on the first item
		public String reasonCode; //mandatory only on the first item
		public String catalogId; //from metadata component
		public String catalogCode; //from metadata component
		public List<Attributes> attributes; //dynamic
		public List<PricingElement> pricingElements; //dynamic
		public List<Item> items;
		public String assignedId;
		public Item(String action, String catalogId, String catalogCode) {
			this(action);
			this.catalogId = catalogId;
			this.catalogCode = catalogCode;
			this.items = new List<Item>();
			this.attributes = new List<Attributes>();
			this.pricingElements = new List<PricingElement>();
		}
		public Item(String action) {
			this.action = action;
		}
	}

	public class Attributes {
		public String name;
		public String value;
		public Attributes(String name, String value) {
			this.name = name;
			this.value = value;
		}
	}

	public class PricingElement {
		public String action;
		public String catalogId;
		public String distributionChannelID;
		public PricingElement(String action, String catalogId, String distributionChannelID) {
			this.action = action;
			this.catalogId = catalogId;
			this.distributionChannelID = distributionChannelID;
		}
	}

	public static Response parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new Response(parser);
	}

	public class Response {
		public Integer status;
		public List<data> data;
		public List<EMP_ResponseError.Error> error;
		public Response(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'status' {
								status = parser.getIntegerValue();
							}
							when 'data' {
								data = arrayOfData(parser);
							}
							when 'error' {
								error = EMP_ResponseError.arrayOfError(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'JSON2Apex consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class Data {
		public orderOutputData orderOutputData;
		public Data(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'orderOutputData' {
								orderOutputData = new OrderOutputData(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'Data consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class OrderOutputData {
		public createdOrderData createdOrderData;
		public OrderOutputData(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'informationMessages' {
								//skipped this children of the parsing due is too long and not nee d
								parser.skipChildren();
							}
							when 'createdOrderData' {
								createdOrderData = new CreatedOrderData(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'OrderOutputData consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class CreatedOrderData {
		public List<ProvideOrderActionsSummary> provideOrderActionsSummary { get; set; }
		public OrderHeaderData orderHeader { get; set; }
		public TotalPrices totalPrices { get; set; }
		public CreatedOrderData(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'provideOrderActionsSummary' {
								provideOrderActionsSummary = arrayOfProvideOrderActionsSummary(
									parser
								);
							}
							when 'orderHeader' {
								orderHeader = new OrderHeaderData(parser);
							}
							when 'totalPrices' {
								//skipped this children of the parsing due is too long and not nee d
								parser.skipChildren();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'CreatedOrderData consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	private static List<ProvideOrderActionsSummary> arrayOfProvideOrderActionsSummary(
		System.JSONParser p
	) {
		List<ProvideOrderActionsSummary> res = new List<ProvideOrderActionsSummary>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new ProvideOrderActionsSummary(p));
		}
		return res;
	}

	public class ProvideOrderActionsSummary {
		public ImplementedOffer implementedOffer { get; set; }
		public List<OrderActionSummary> orderActionSummary { get; set; }
		public ProvideOrderActionsSummary(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'implementedOffer' {
								implementedOffer = new ImplementedOffer(parser);
							}
							when 'orderActionSummary' {
								orderActionSummary = arrayOfOrderActionSummary(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'ProvideOrderActionsSummary consuming unrecognized property: ' +
									text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class AvailableOffer {
		public CatalogOffer catalogOffer { get; set; }
		public List<ChildAvailableProducts> childAvailableProducts { get; set; }
		public AvailableOffer(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'catalogOffer' {
								catalogOffer = new CatalogOffer(parser);
							}
							when 'childAvailableProducts' {
								childAvailableProducts = arrayOfChildAvailableProducts(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'AvailableOffer consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	private static List<ChildAvailableProducts> arrayOfChildAvailableProducts(System.JSONParser p) {
		List<ChildAvailableProducts> res = new List<ChildAvailableProducts>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new ChildAvailableProducts(p));
		}
		return res;
	}

	public class ChildAvailableProducts {
		public Integer availableProductID { get; set; }
		public ChildAvailableProducts(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'availableProductID' {
								availableProductID = parser.getIntegerValue();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'ChildAvailableProducts consuming unrecognized property: ' +
									text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class CatalogOffer {
		public CatalogOfferID catalogOfferID { get; set; }
		public CatalogOffer(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'catalogOfferID' {
								catalogOfferID = new CatalogOfferID(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'CatalogOffer consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class CatalogOfferID {
		public Integer orderID { get; set; }
		public CatalogOfferID(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'orderID' {
								orderID = parser.getIntegerValue();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'CatalogOfferID consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class ImplementedOffer {
		public List<ChildImplementedProducts> childImplementedProducts { get; set; }
		public Long assignedOfferID { get; set; }
		public String offerName { get; set; }
		public AvailableOffer availableOffer { get; set; }
		public ImplementedOffer(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'childImplementedProducts' {
								childImplementedProducts = arrayOfChildImplementedProducts(parser);
							}
							when 'assignedOfferID' {
								assignedOfferID = parser.getLongValue();
							}
							when 'offerName' {
								offerName = parser.getText();
							}
							when 'availableOffer' {
								availableOffer = new AvailableOffer(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'ImplementedOffer consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class TotalPrices {
		public Double totalOC { get; set; }
		public Double totalRC { get; set; }
		public Double previousTotalRC { get; set; }
		public Double originalTotalOC { get; set; }
		public Double discountTotalOC { get; set; }
		public Double overrideTotalOC { get; set; }
		public Double taxTotalOC { get; set; }
		public Double originalTotalRC { get; set; }
		public Double discountTotalRC { get; set; }
		public Double taxTotalRC { get; set; }
		public Double proratedTotalRC { get; set; }
		public Double proratedDiscountTotalRC { get; set; }
		public Double proratedTaxTotalRC { get; set; }
		public Double previousTaxTotalRC { get; set; }
		public TotalPrices(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switchOnAttributesTotalPrices(text, parser);
					}
				}
			}
		}
		private void switchOnAttributesTotalPrices(String text, JSONParser parser) {
			switch on text {
				when 'totalOC' {
					totalOC = parser.getDoubleValue();
				}
				when 'totalRC' {
					totalRC = parser.getDoubleValue();
				}
				when 'previousTotalRC' {
					previousTotalRC = parser.getDoubleValue();
				}
				when 'originalTotalOC' {
					originalTotalOC = parser.getDoubleValue();
				}
				when 'discountTotalOC' {
					discountTotalOC = parser.getDoubleValue();
				}
				when 'overrideTotalOC' {
					overrideTotalOC = parser.getDoubleValue();
				}
				when 'taxTotalOC' {
					taxTotalOC = parser.getDoubleValue();
				}
				when 'originalTotalRC' {
					originalTotalRC = parser.getDoubleValue();
				}
				when 'discountTotalRC' {
					discountTotalRC = parser.getDoubleValue();
				}
				when 'taxTotalRC' {
					taxTotalRC = parser.getDoubleValue();
				}
				when 'proratedTotalRC' {
					proratedTotalRC = parser.getDoubleValue();
				}
				when 'proratedDiscountTotalRC' {
					proratedDiscountTotalRC = parser.getDoubleValue();
				}
				when 'proratedTaxTotalRC' {
					proratedTaxTotalRC = parser.getDoubleValue();
				}
				when 'previousTaxTotalRC' {
					previousTaxTotalRC = parser.getDoubleValue();
				}
				when else {
					System.debug(
						LoggingLevel.WARN,
						'TotalPrices consuming unrecognized property: ' + text
					);
					consumeObject(parser);
				}
			}
		}
	}

	public class CatalogPricingDetails {
		public String name { get; set; }
		public String description { get; set; }
		public String pricePlanType { get; set; }
		public String pricePlanCaption { get; set; }
		public Boolean detault { get; set; }
		public List<DynamicAttribute> dynamicAttribute { get; set; }
		public CatalogPricingDetails(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switchOnAttCatalogPricingDetails(text, parser);
					}
				}
			}
		}
		private void switchOnAttCatalogPricingDetails(String text, JSONParser parser) {
			switch on text {
				when 'name' {
					name = parser.getText();
				}
				when 'description' {
					description = parser.getText();
				}
				when 'pricePlanType' {
					pricePlanType = parser.getText();
				}
				when 'pricePlanCaption' {
					pricePlanCaption = parser.getText();
				}
				when 'detault' {
					detault = parser.getBooleanValue();
				}
				when 'dynamicAttribute' {
					dynamicAttribute = arrayOfDynamicAttributes(parser);
				}
				when else {
					System.debug(
						LoggingLevel.WARN,
						'CatalogPricingDetails consuming unrecognized property: ' + text
					);
					consumeObject(parser);
				}
			}
		}
	}

	public class CatalogPricing {
		public CatalogPricingDetails catalogPricingDetails { get; set; }
		public CatalogPricingID catalogPricingID { get; set; }
		public CatalogPricing(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'catalogPricingDetails' {
								catalogPricingDetails = new CatalogPricingDetails(parser);
							}
							when 'catalogPricingID' {
								catalogPricingID = new CatalogPricingID(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'CatalogPricing consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class CatalogPricingID {
		public String id { get; set; }
		public String relVer { get; set; }
		public String relID { get; set; }
		public String version { get; set; }
		public CatalogPricingID(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'id' {
								id = parser.getText();
							}
							when 'relVer' {
								relVer = parser.getText();
							}
							when 'relID' {
								relID = parser.getText();
							}
							when 'version' {
								version = parser.getText();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'CatalogPricingID consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class ImplementedtedPricings {
		public CatalogPricing catalogPricing { get; set; }
		public TotalPrices totalPrices { get; set; }
		public Integer distributionChannelID { get; set; }
		public String itemActionType { get; set; }
		public String actionType { get; set; }
		public ImplementedtedPricings(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'catalogPricing' {
								catalogPricing = new CatalogPricing(parser);
							}
							when 'totalPrices' {
								totalPrices = new TotalPrices(parser);
							}
							when 'distributionChannelID' {
								distributionChannelID = parser.getIntegerValue();
							}
							when 'itemActionType' {
								itemActionType = parser.getText();
							}
							when 'actionType' {
								actionType = parser.getText();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'ImplementedtedPricings consuming unrecognized property: ' +
									text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	private static List<ImplementedtedPricings> arrayOfImplementedtedPricings(System.JSONParser p) {
		List<ImplementedtedPricings> res = new List<ImplementedtedPricings>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new ImplementedtedPricings(p));
		}
		return res;
	}

	public class ChildImplementedProducts {
		public List<ChildImplementedProducts> childImplementedProducts { get; set; }
		public List<ImplementedAttributes> implementedAttributes { get; set; }
		public String productStatus { get; set; }
		public TotalPrices totalPrices { get; set; }
		public String assignedProducID { get; set; }
		public String mainCompID { get; set; }
		public String serviceID { get; set; }
		public String catalogProductID { get; set; }
		public String catalogProductCod { get; set; }
		public String catalogProductName { get; set; }
		public String itemActionType { get; set; }
		public String actionType { get; set; }
		public String offerName { get; set; }
		public String subscriptionID { get; set; }
		public String provisioningDate { get; set; }
		public String effectiveSinceDate { get; set; }
		public String effectiveUntilDate { get; set; }
		public String assignedOfferID { get; set; }
		public String catalogOfferID { get; set; }
		public String orderActionReason { get; set; }
		public String initialSalesLocation { get; set; }
		public String availableUserAction { get; set; }
		public List<ImplementedtedPricings> implementedtedPricings { get; set; }
		public String name { get; set; }
		public ChildImplementedProducts(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switchOnAttChildImplementedProducts(text, parser);
					}
				}
			}
		}
		private void switchOnAttChildImplementedProducts(String text, JSONParser parser) {
			switch on text {
				when 'childImplementedProducts' {
					childImplementedProducts = arrayOfChildImplementedProducts(parser);
				}
				when 'implementedAttributes' {
					implementedAttributes = arrayOfImplementedAttributes(parser);
				}
				when 'implementedtedPricings' {
					implementedtedPricings = arrayOfImplementedtedPricings(parser);
				}
				when 'productStatus' {
					productStatus = parser.getText();
				}
				when 'mainCompID' {
					mainCompID = parser.getText();
				}
				when 'totalPrices' {
					totalPrices = new TotalPrices(parser);
				}
				when 'assignedProducID' {
					assignedProducID = parser.getText();
				}
				when 'serviceID' {
					serviceID = parser.getText();
				}
				when 'catalogProductID' {
					catalogProductID = parser.getText();
				}
				when 'catalogProductCod' {
					catalogProductCod = parser.getText();
				}
				when 'catalogProductName' {
					catalogProductName = parser.getText();
				}
				when 'itemActionType' {
					itemActionType = parser.getText();
				}
				when 'actionType' {
					actionType = parser.getText();
				}
				when 'offerName' {
					offerName = parser.getText();
				}
				when 'subscriptionID' {
					subscriptionID = parser.getText();
				}
				when 'provisioningDate' {
					provisioningDate = parser.getText();
				}
				when 'effectiveSinceDate' {
					effectiveSinceDate = parser.getText();
				}
				when 'effectiveUntilDate' {
					effectiveUntilDate = parser.getText();
				}
				when 'assignedOfferID' {
					assignedOfferID = parser.getText();
				}
				when 'catalogOfferID' {
					catalogOfferID = parser.getText();
				}
				when 'orderActionReason' {
					orderActionReason = parser.getText();
				}
				when 'initialSalesLocation' {
					initialSalesLocation = parser.getText();
				}
				when 'availableUserAction' {
					availableUserAction = parser.getText();
				}
				when 'name' {
					name = parser.getText();
				}
				when else {
					System.debug(
						LoggingLevel.WARN,
						'ChildImplementedProducts consuming unrecognized property: ' + text
					);
					consumeObject(parser);
				}
			}
		}
	}

	private static List<ChildImplementedProducts> arrayOfChildImplementedProducts(
		System.JSONParser p
	) {
		List<ChildImplementedProducts> res = new List<ChildImplementedProducts>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new ChildImplementedProducts(p));
		}
		return res;
	}

	public class ImplementedAttributes {
		public CatalogAttribute catalogAttribute { get; set; }
		public String selectedValue { get; set; }
		public ImplementedAttributes(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'catalogAttribute' {
								catalogAttribute = new CatalogAttribute(parser);
							}
							when 'selectedValue' {
								selectedValue = parser.getText();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'ImplementedAttributes consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	private static List<ImplementedAttributes> arrayOfImplementedAttributes(System.JSONParser p) {
		List<ImplementedAttributes> res = new List<ImplementedAttributes>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new ImplementedAttributes(p));
		}
		return res;
	}

	public class CatalogAttribute {
		public Integer catalogAttributeID { get; set; }
		public CatalogAttributeDetails catalogAttributeDetails { get; set; }
		public CatalogAttribute(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'catalogAttributeID' {
								catalogAttributeID = parser.getIntegerValue();
							}
							when 'catalogAttributeDetails' {
								catalogAttributeDetails = new CatalogAttributeDetails(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'CatalogAttribute consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class CatalogAttributeDetails {
		public String name { get; set; }
		public String description { get; set; }
		public String defaultValue { get; set; }
		public Boolean isVisible { get; set; }
		public Boolean affectsPrice { get; set; }
		public AttributeDataType attributeDataType { get; set; }
		public List<DynamicAttribute> dynamicAttributes { get; set; }
		public String propertyNameX1 { get; set; }
		public CatalogAttributeDetails(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switchOnAttCatalogAttributeDetails(text, parser);
					}
				}
			}
		}
		private void switchOnAttCatalogAttributeDetails(String text, JSONParser parser) {
			switch on text {
				when 'name' {
					name = parser.getText();
				}
				when 'description' {
					description = parser.getText();
				}
				when 'affectsPrice' {
					affectsPrice = parser.getBooleanValue();
				}
				when 'defaultValue' {
					defaultValue = parser.getText();
				}
				when 'isVisible' {
					isVisible = parser.getBooleanValue();
				}
				when 'attributeDataType' {
					attributeDataType = new AttributeDataType(parser);
				}
				when 'dynamicAttributes' {
					dynamicAttributes = arrayOfDynamicAttributes(parser);
				}
				when 'propertyNameX1' {
					propertyNameX1 = parser.getText();
				}
				when else {
					System.debug(
						LoggingLevel.WARN,
						'CatalogAttributeDetails consuming unrecognized property: ' + text
					);
					consumeObject(parser);
				}
			}
		}
	}

	public class DynamicAttribute {
		public String name { get; set; }
		public String value { get; set; }
		public String localizedName { get; set; }
		public DynamicAttribute(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'name' {
								name = parser.getText();
							}
							when 'value' {
								value = parser.getText();
							}
							when 'localizedName' {
								localizedName = parser.getText();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'DynamicAttribute consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	private static List<DynamicAttribute> arrayOfDynamicAttributes(System.JSONParser p) {
		List<DynamicAttribute> res = new List<DynamicAttribute>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new DynamicAttribute(p));
		}
		return res;
	}

	public class AttributeDataType {
		public String name { get; set; }
		public AttributeDataTypeDetails attributeDataTypeDetails { get; set; }
		public AttributeDataType(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'name' {
								name = parser.getText();
							}
							when 'attributeDataTypeDetails' {
								attributeDataTypeDetails = new AttributeDataTypeDetails(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'AttributeDataType consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class AttributeDataTypeDetails {
		public Integer dataType { get; set; }
		public Integer type { get; set; } // in json: type
		public Integer dataSize { get; set; }
		public List<Ranges> ranges { get; set; }
		public List<ValidValues> validValues { get; set; }
		public AttributeDataTypeDetails(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'dataType' {
								dataType = parser.getIntegerValue();
							}
							when 'type' {
								type = parser.getIntegerValue();
							}
							when 'dataSize' {
								dataSize = parser.getIntegerValue();
							}
							when 'ranges' {
								ranges = arrayOfRanges(parser);
							}
							when 'validValues' {
								validValues = arrayOfValidValues(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'AttributeDataTypeDetails consuming unrecognized property: ' +
									text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	private static List<ValidValues> arrayOfValidValues(System.JSONParser p) {
		List<ValidValues> res = new List<ValidValues>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new ValidValues(p));
		}
		return res;
	}

	public class ValidValues {
		public String code { get; set; }
		public String name { get; set; }
		public ValidValues(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'code' {
								code = parser.getText();
							}
							when 'name' {
								name = parser.getText();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'ValidValues consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class Ranges {
		public Integer minimum { get; set; }
		public Integer maximum { get; set; }
		public Ranges(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'minimum' {
								minimum = parser.getIntegerValue();
							}
							when 'maximum' {
								maximum = parser.getIntegerValue();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'Ranges consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	private static List<Ranges> arrayOfRanges(System.JSONParser p) {
		List<Ranges> res = new List<Ranges>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Ranges(p));
		}
		return res;
	}

	public class OrderActionSummary {
		public ImplementedProduct implementedProduct { get; set; }
		public Integer shippingAddressId { get; set; }
		public String orderActionType { get; set; }
		public String orderActionStatus { get; set; }
		public Long serviceRequiredDate { get; set; }
		public OrderActionSummary(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'implementedProduct' {
								implementedProduct = new ImplementedProduct(parser);
							}
							when 'shippingAddressId' {
								shippingAddressId = parser.getIntegerValue();
							}
							when 'orderActionType' {
								orderActionType = parser.getText();
							}
							when 'orderActionStatus' {
								orderActionStatus = parser.getText();
							}
							when 'serviceRequiredDate' {
								serviceRequiredDate = parser.getLongValue();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'OrderActionSummary consuming unrecognized property: ' + text
								);
								consumeObject(parser);
							}
						}
					}
				}
			}
		}
	}

	public class ImplementedProduct {
		public List<ChildImplementedProducts> childImplementedProducts { get; set; }
		public List<ImplementedAttributes> implementedAttributes { get; set; }
		public List<ImplementedtedPricings> implementedtedPricings { get; set; }
		public String productStatus { get; set; }
		public TotalPrices totalPrices { get; set; }
		public Long assignedProducID { get; set; }
		public String serviceID { get; set; }
		public Integer catalogProductID { get; set; }
		public String catalogProductCod { get; set; }
		public String catalogProductName { get; set; }
		public String itemActionType { get; set; }
		public String actionType { get; set; }
		public ImplementedProduct(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switchOnAttImplementedProduct(text, parser);
					}
				}
			}
		}
		private void switchOnAttImplementedProduct(String text, JSONParser parser) {
			switch on text {
				when 'childImplementedProducts' {
					childImplementedProducts = arrayOfChildImplementedProducts(parser);
				}
				when 'implementedAttributes' {
					implementedAttributes = arrayOfImplementedAttributes(parser);
				}
				when 'implementedtedPricings' {
					implementedtedPricings = arrayOfImplementedtedPricings(parser);
				}
				when 'productStatus' {
					productStatus = parser.getText();
				}
				when 'totalPrices' {
					totalPrices = new TotalPrices(parser);
				}
				when 'assignedProducID' {
					assignedProducID = parser.getLongValue();
				}
				when 'serviceID' {
					serviceID = parser.getText();
				}
				when 'catalogProductID' {
					catalogProductID = parser.getIntegerValue();
				}
				when 'catalogProductCod' {
					catalogProductCod = parser.getText();
				}
				when 'catalogProductName' {
					catalogProductName = parser.getText();
				}
				when 'itemActionType' {
					itemActionType = parser.getText();
				}
				when 'actionType' {
					actionType = parser.getText();
				}
				when else {
					System.debug(
						LoggingLevel.WARN,
						'ImplementedProduct consuming unrecognized property: ' + text
					);
					consumeObject(parser);
				}
			}
		}
	}

	public class OrderHeaderData {
		public String orderID { get; set; }
		public Integer daysToExpiration { get; set; }
		public Long applicationDate { get; set; }
		public Long expiryDate { get; set; }
		public String orderStatus { get; set; }
		public CustomerProfileHeader customerProfileHeader { get; set; }
		public String externalOrderID { get; set; }
		public String creditVettingStatus { get; set; }
		public Long contactID { get; set; }
		public OrderHeaderData(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switchOnAttOrderHeaderData(text, parser);
					}
				}
			}
		}
		private void switchOnAttOrderHeaderData(String text, JSONParser parser) {
			switch on text {
				when 'orderID' {
					orderID = parser.getText();
				}
				when 'daysToExpiration' {
					daysToExpiration = parser.getIntegerValue();
				}
				when 'applicationDate' {
					applicationDate = parser.getLongValue();
				}
				when 'expiryDate' {
					expiryDate = parser.getLongValue();
				}
				when 'orderStatus' {
					orderStatus = parser.getText();
				}
				when 'customerProfileHeader' {
					customerProfileHeader = new CustomerProfileHeader(parser);
				}
				when 'externalOrderID' {
					externalOrderID = parser.getText();
				}
				when 'creditVettingStatus' {
					creditVettingStatus = parser.getText();
				}
				when 'contactID' {
					contactID = parser.getLongValue();
				}
				when else {
					System.debug(
						LoggingLevel.WARN,
						'OrderHeader consuming unrecognized property: ' + text
					);
				}
			}
		}
	}

	public class CustomerProfileHeader {
		public String customerName;
		public Integer customerID;
		public CustomerProfileHeader(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'customerName' {
								customerName = parser.getText();
							}
							when 'customerID' {
								customerID = parser.getIntegerValue();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'CustomerProfileHeader consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	private static List<Data> arrayOfData(System.JSONParser p) {
		List<Data> res = new List<Data>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Data(p));
		}
		return res;
	}

	private static List<OrderActionSummary> arrayOfOrderActionSummary(System.JSONParser p) {
		List<OrderActionSummary> res = new List<OrderActionSummary>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new OrderActionSummary(p));
		}
		return res;
	}

	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT || curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
}