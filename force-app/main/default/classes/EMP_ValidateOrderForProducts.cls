public class EMP_ValidateOrderForProducts {
	public class Request {
		public String mode;
		public CustomerInfo customer_info;
		public OrderDataType order_data;
		public Request(CustomerInfo tempCustomerInfo, OrderDataType tempOrderData) {
			this.mode = 'ValidateAndSubmit';
			this.customer_info = tempCustomerInfo;
			this.order_data = tempOrderData;
		}
	}

	public class CustomerInfo {
		public String kvk_no; // either kvk_no or birth_date
		//public String birth_date; // either kvk_no or birth_date
		public String billing_customer_id; //mandatory, ban number
		public CustomerInfo(String tempBillingCustomerId, String kvk_no) {
			this.billing_customer_id = tempBillingCustomerId;
			this.kvk_no = kvk_no;
		}
	}

	public class OrderDataType {
		public List<GlpItem> glp_item; //mandatory
		public OrderDataType(List<GlpItem> tempGlpItem) {
			this.glp_item = tempGlpItem;
		}
	}

	public class OrderHeader {
		public String orderId; //where does this comes from?
		public String externalOrderId; //SF ID
		public OrderHeader(String tempOrderId, String tempExternalOrderId) {
			this.orderId = tempOrderId;
			this.externalOrderId = tempExternalOrderId;
		}
	}

	public class GlpItem {
		public String distribution_channel_id;
		public String action; //mandatory, add or modify
		public String catalog_id; //Either catalog_id or catalog_code, comes from VF product
		public String catalog_code; //Either catalog_id or catalog_code, comes from VF product
		public String activity; //mandatory
		public String assigned_id; //mandatory on CHANGE_ASSIGNED_ITEM action, else it can be null
		public List<AttributesType> attributes;
		public List<PricingElements> pricing_elements;
		public List<GlpItem> child_items; //dynamic
		public GlpItem(
			String tempAction,
			String tempCatalogId,
			String tempCatalogCode,
			String distributionChannelID
		) {
			this.action = tempAction;
			this.catalog_id = tempCatalogId;
			this.catalog_code = tempCatalogCode;
			this.child_items = new List<GlpItem>();
			this.distribution_channel_id = distributionChannelID;
		}
	}

	public class PricingElements {
		public String action; //mandatory, add or modify
		public String catalog_id; // comes from VF product
		public String distribution_channel_id;
		public PricingElements(
			String tempAction,
			String tempCatalogId,
			String distributionChannelID
		) {
			this.action = tempAction;
			this.catalog_id = tempCatalogId;
			this.distribution_channel_id = distributionChannelID;
		}
	}

	public class AttributesType {
		public String attribute_key; //mandatory
		public String value;
		public AttributesType(String tempAttributeKey, String tempValue) {
			this.attribute_key = tempAttributeKey;
			this.value = tempValue;
		}
	}

	public class Response {
		public Integer status;
		public List<Data> data;
		public List<EMP_ResponseError.Error> error;
		public Response(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'status' {
								status = parser.getIntegerValue();
							}
							when 'data' {
								data = arrayOfData(parser);
							}
							when 'error' {
								error = EMP_ResponseError.arrayOfError(parser);
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'Response consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	public class Data {
		public String oms_order_id;
		public Data(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'oms_order_id' {
								oms_order_id = parser.getText();
							}
							when else {
								System.debug(
									LoggingLevel.WARN,
									'Data consuming unrecognized property: ' + text
								);
							}
						}
					}
				}
			}
		}
	}

	private static List<Data> arrayOfData(System.JSONParser p) {
		List<Data> res = new List<Data>();
		if (p.getCurrentToken() == null) {
			p.nextToken();
		}
		while (p.nextToken() != System.JSONToken.END_ARRAY) {
			res.add(new Data(p));
		}
		return res;
	}

	public static Response parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new Response(parser);
	}
}