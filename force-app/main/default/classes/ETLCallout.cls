/**
 * Created by bojan.zunko on 11/05/2020.
 */

public with sharing class ETLCallout {
    public static final String HANDLER_NAME = 'Digital Commerce ETL';
    public static void doCallout(String publicationID) {
        List<CCETLSettings__c> etlSettings = [SELECT ID,Client_ID__c FROM CCETLSettings__c];

        if(etlSettings.size() == 0 || String.isBlank(etlSettings[0].Client_ID__c)) {
            throw new CalloutException('Please define CC ETL custom settings to define the Client ID');
        }

        String CLIENT_ID = etlSettings[0].Client_ID__c;

        //Prepare inner structure for easier JSON
        ETLPublicationObserver.PublicationPayload payload = new ETLPublicationObserver.PublicationPayload();

        List<csb2c__E_Commerce_Publication__c> catalogeInProcess = [select id,(select id,csb2c__Catalogue__c,csb2c__Catalogue__r.Name, csb2c__Catalogue__r.cc_etl_custom_field_schema__c from csb2c__Publication_Catalogue_Associations__r) from csb2c__E_Commerce_Publication__c where id =:publicationID];
        payload.publicationId = publicationID;
        payload.elasticCatalogId = catalogeInProcess[0].csb2c__Publication_Catalogue_Associations__r[0].csb2c__Catalogue__c;
        payload.elasticCatalogName = catalogeInProcess[0].csb2c__Publication_Catalogue_Associations__r[0].csb2c__Catalogue__r.Name;
        payload.prgs = new List<ETLPublicationObserver.PRG>();
        if (String.isNotBlank(catalogeInProcess[0].csb2c__Publication_Catalogue_Associations__r[0].csb2c__Catalogue__r.cc_etl_custom_field_schema__c)) {
            payload.customFieldsSchema = (Map<String,Object>)JSON.deserializeUntyped(catalogeInProcess[0].csb2c__Publication_Catalogue_Associations__r[0].csb2c__Catalogue__r.cc_etl_custom_field_schema__c);
        }
        payload.clientId = CLIENT_ID;


        List<cspmb__Pricing_Rule_Group__c> prgs = CustomPRGFilter.getPRGsForPublication(ID.valueOf(publicationID));

        for(cspmb__Pricing_Rule_Group__c iter : prgs) {
            ETLPublicationObserver.PRG creatingPRG = new ETLPublicationObserver.PRG();
            creatingPRG.id = iter.cspmb__pricing_rule_group_code__c;
            creatingPRG.name = iter.Name;
            creatingPRG.currencyCode = 'EUR';
            payload.prgs.add(creatingPRG);
        }
        
		if (!Test.isRunningTest())
        	csam.ObjectGraphCalloutHandler.queueJsonPayloadMessageFromId(HANDLER_NAME, ID.valueOf(publicationID), JSON.serialize(payload));
    }

}