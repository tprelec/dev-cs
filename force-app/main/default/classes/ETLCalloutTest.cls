@isTest
public class ETLCalloutTest {

    private static testMethod void testExecute() {
        

        CCETLSettings__c ssettings = new CCETLSettings__c();
        ssettings.Client_ID__c = 'xxx';
        insert ssettings;

        /*csb2c__B2C_Setting__mdt b2cSettingMetadata = new csb2c__B2C_Setting__mdt();
        b2cSettingMetadata.MasterLabel = 'PS_DefaultPRGCodes';
        b2cSettingMetadata.csb2c__text_value__c = 'Group-001';
        insert b2cSettingMetadata;*/

        cspmb__Catalogue__c catalogue = new cspmb__Catalogue__c();
        catalogue.Name = 'test_catalogue';
        insert catalogue;

        csb2c__E_Commerce_Publication__c publicationObj = new csb2c__E_Commerce_Publication__c();
        insert publicationObj;


        csb2c__Publication_Catalogue_Association__c publicationAssociation = new csb2c__Publication_Catalogue_Association__c();
        publicationAssociation.csb2c__Catalogue__c = catalogue.Id;
        publicationAssociation.csb2c__Publication__c = publicationObj.Id;
        insert publicationAssociation;

        cspmb__Pricing_Rule_Group__c prg = new cspmb__Pricing_Rule_Group__c();
        prg.cspmb__pricing_rule_group_code__c = 'Group-001';
        insert prg;


        /*String body = '{"connectionTypes": [{ "type": "New", "quantity": 50 }, { "type": "Port-in", "quantity": 50 }], "charges":[{"source":"recurring","salesPrice":34,"listPrice":34,"quantity":1,"name":"recurring","description":"Override charge value forrecurring","chargeType":"recurring","isProductLevelPricing":true}],"discounts":[{"type":"PER","amount":5,"chargeType":"recurring","discountPrice":"sales","version":"2-0-0","recordType":"single","discountCharge":"recurring","source":"promo-test;promotion"}],"customData":{},"OE":[],"CS_QuantityStrategy":"Multiplier","csQuantity":1}';*/

        Test.startTest();

        ETLCallout.doCallout(publicationObj.Id);

        Test.stopTest();
    }
}