/**
 * Created by bojan.zunko on 11/05/2020.
 * Calls ETL service to complete publication process using HTTP callout.
 * For now it will be sending directly to ETL HTTP endpoint - to be changed later to Dispatcher
 */

global with sharing class ETLPublicationObserver implements csam.ObserverApi.IObserver {

	public class CustomException extends Exception {}
    global void execute(csam.ObserverApi.Observable o, Object arg) {
        try {
            if (o == null)
                throw new CustomException('Bad code.');
            
            if (o instanceof csam.InboundMessageObservable) {
                csam.InboundMessageObservable observable = (csam.InboundMessageObservable) o;
                execute(observable.getMessages());
            }
        } catch (Exception e) {
            exceptionHandling(o, e);
        }
    }

	global void exceptionHandling(csam.ObserverApi.Observable o, Exception e) {
		Attachment errorOut = new Attachment();
		errorOut.Name = 'Error during observer';
		errorOut.Body = Blob.valueOf(e.getMessage());

		if (o != null) {
			csam.InboundMessageObservable observable = (csam.InboundMessageObservable) o;
			errorOut.ParentId = observable.getMessages()[0].Id;
			insert errorOut;
		}
	}
    
    global void execute(List<csam__Incoming_Message__c> messages) {

        for (csam__Incoming_Message__c incomingMessage : messages) {

            if (incomingMessage.csam__Final_Chunk__c && incomingMessage.csam__Outgoing_Message__c != null &&
                    incomingMessage.csam__Outgoing_Message__r.csam__ObjectGraph_Callout_Handler__c != null &&
                    incomingMessage.csam__Outgoing_Message__r.csam__ObjectGraph_Callout_Handler__r.Name == 'Ecommerce Publication v3 Sync' &&
                    !incomingMessage.csam__Incoming_URL_Path__c.endsWith('error')) {


                List<csam__Outgoing_Message_Record__c> omrInProcess = [SELECT csam__Object_Record_Id__c from csam__Outgoing_Message_Record__c where csam__Outgoing_Message__c = :incomingMessage.csam__Outgoing_Message__c];
                ETLCallout.doCallout(omrInProcess[0].csam__Object_Record_Id__c);
            }
        }
    }

    global class PublicationPayload {
        public String publicationId;
        public String clientId;
        public String elasticCatalogId;
        public String elasticCatalogName;
        public List<ETLPublicationObserver.PRG> prgs;
        public Map<String,Object> customFieldsSchema;
    }

    global class PRG {
        public String id;
        public String name;
        public String currencyCode;
    }
}