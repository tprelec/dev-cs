/**
 * Description:    W-001473 Einstein discovery service to get all kinds of predictions back from the discovery stories
 * Date            2019-02  Marcel Vreuls
 */
public class EinsteinDiscoveryService {
  
    /**
     * Get the prediction given the custom setting and the trigger scope
     * As the eventual call "ed_insights.TriggerHandler.insertUpdateHandle" seems to be taking
     * the trigger objects to retrieve the predictions on, there is no need to pass any records
     * here..
     */
    public static void getPrediction(String sObjectType) {
        // Predictions cannot be performed in future annotations
        if (System.isFuture()) {
            return;
        }
        // Only run it once. Each prediction must be handled in this statement or it won't be executed
        if(ed_insights.CheckRecursive.runOnce()) {
            Set<String> configNameSet = getConfigNameSet(sObjectType);
            // Run all predictions:
            if (Test.isRunningTest()) return;
            for (String s : configNameSet) {
                ed_insights.TriggerHandler.insertUpdateHandle(s);
            }
        }
    }
    
    /**
     * Given the passed sobject name, retrieve the appropriate records from the custom setting "Einstein Discovery - Write Back"
     */
    private static Set<String> getConfigNameSet(String sObjectName) {
        Set<String> configNameSet = new Set<String>();
        Map<String, ed_insights__SDDPredictionConfig__c> predMap = ed_insights__SDDPredictionConfig__c.getAll();
        for (ed_insights__SDDPredictionConfig__c pred : predMap.values()) {
            if (pred.ed_insights__object_api_name__c.equalsIgnoreCase(sObjectName)) {
                configNameSet.add(pred.Name);
            }
        }
        return configNameSet;
    }
}