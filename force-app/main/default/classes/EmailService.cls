public with sharing class EmailService {
	private static Boolean isDeliverabilityActive = null;
	public static Boolean checkDeliverability() {
		if (isDeliverabilityActive == null) {
			try {
				Messaging.reserveSingleEmailCapacity(0);
				isDeliverabilityActive = true;
			} catch (System.NoAccessException e) {
				isDeliverabilityActive = false;
			}
		}
		return isDeliverabilityActive;
	}

	@InvocableMethod(
		label='Send Email'
		description='Sends email with option to attach files.'
		category='Email'
	)
	public static void sendEmail(List<EmailRequest> requests) {
		for (EmailRequest request : requests) {
			sendEmail(
				request.recipientId,
				request.relatedToId,
				request.toAddresses,
				request.ccAddresses,
				request.bccAddresses,
				request.templateId,
				request.attachmentIds,
				request.orgWideEmailId
			);
		}
	}

	public static void sendEmail(
		Id recipientId,
		Id relatedToId,
		List<String> toAddresses,
		List<String> ccAddresses,
		List<String> bccAddresses,
		Id templateId,
		List<Id> attachmentIds,
		Id orgWideEmailId
	) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setTemplateId(templateId);
		if (recipientId != null) {
			mail.setTargetObjectId(recipientId);
		}
		if (relatedToId != null) {
			mail.setWhatId(relatedToId);
		}
		if (toAddresses != null) {
			mail.setToAddresses(toAddresses);
		}
		if (ccAddresses != null) {
			mail.setCcAddresses(ccAddresses);
		}
		if (bccAddresses != null) {
			mail.setBccAddresses(bccAddresses);
		}
		if (attachmentIds != null) {
			mail.setEntityAttachments(attachmentIds);
		}
		if (orgWideEmailId != null) {
			mail.setOrgWideEmailAddressId(orgWideEmailId);
		}
		if (checkDeliverability()) {
			List<Messaging.SendEmailResult> result = Messaging.sendEmail(
				new List<Messaging.SingleEmailMessage>{ mail }
			);
			if (!result[0].isSuccess()) {
				throw new EmailException(result[0].getErrors()[0].getMessage());
			}
		}
	}

	public class EmailRequest {
		@InvocableVariable(label='Recipient Id')
		public Id recipientId;

		@InvocableVariable(label='Related Record Id')
		public Id relatedToId;

		@InvocableVariable(label='To Addresses')
		public List<String> toAddresses;

		@InvocableVariable(label='CC Addresses')
		public List<String> ccAddresses;

		@InvocableVariable(label='BCC Addresses')
		public List<String> bccAddresses;

		@InvocableVariable(label='Email Template Id')
		public Id templateId;

		@InvocableVariable(label='Attachment Ids')
		public List<Id> attachmentIds;

		@InvocableVariable(label='Org Wide Email Address Id')
		public Id orgWideEmailId;
	}

	public class EmailException extends Exception {
	}
}