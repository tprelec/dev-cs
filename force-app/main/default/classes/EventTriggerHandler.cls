/**
 * @description         This is the trigger handler for the Event object
 */
public with sharing class EventTriggerHandler extends TriggerHandler {


	/**
	 * @description			This handles the before insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeInsert(){
		List<Event> newEvents = (List<Event>) this.newList;	

		updatePublicCalendar(newEvents);

	}

    /**
     * @description         This method updates the EventId on the parent record, if appropriate
     * @author              Guy Clairbois    
     */
    private void updatePublicCalendar(List<Event> newEvents){
		for (Event e : newEvents){
			 if(e.Public_Calendar__c != null) {
			 	e.OwnerId = Public_Calendar__c.getInstance(e.Public_Calendar__c).PublicCalendarId__c;
			 	e.IsVisibleInSelfService = true;
			}
		}
    }

}