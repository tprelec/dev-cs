/**
 * @description			This class is responsible for handling an exception which must stop
 * 						current processing, but needs to make sure the administrators are aware of the exception
 *						and that more relevant information is included in the exception email.
 * @author				Guy Clairbois
 */
public class ExceptionHandler {
	
	private static List<String> SYSTEM_ADMIN_EMAIL_ADDRESSES = new List<String>();
	
	static {
		List<ExceptionEmailAddress__c> emailAddresses = new List<ExceptionEmailAddress__c>();
			
			//if(Test.isRunningTest()){
			//	emailAddresses.add(new ExceptionEmailAddress__c(EmailAddress__c = 'test@test.com'));
			//} else {
				emailAddresses = ExceptionEmailAddress__c.getAll().values();
			//}
		
			for(ExceptionEmailAddress__c emailAddress : emailAddresses){
				SYSTEM_ADMIN_EMAIL_ADDRESSES.add(emailAddress.EmailAddress__c);
			}
	}
	
	private static set<Id> userIds{
		get{
			if(userIds == null){
				Set<String> uniqueEmails = new Set<String>(); // create this set to prevent double emails being sent
				userIds = new Set<Id>();
				// make sure we use actual users in the system, so that we don't suffer from the max email governor limit
				for(User u :  [Select Id, Email, isActive, IsPortalEnabled From User Where email in :SYSTEM_ADMIN_EMAIL_ADDRESSES]){
					if(u.IsActive && !u.IsPortalEnabled) {
						if(!uniqueEmails.contains(u.Email)){
							uniqueEmails.add(u.Email);
							userIds.add(u.Id);
						}
					}
				}
				return userIds;				
			} else {
				return userIds;
			}
		}
		private set;
	}		
	
	private ExceptionHandler(){
		// This class is not to be instantiated, it's pupose is to only
		// inform system administrators of critical errors
	}

	
	public static void handleException(Exception ex){
		notifySystemAdministratorOfException(null, ex);
	}
	
	
	public static void handleException(String customMessage, Exception ex){
		notifySystemAdministratorOfException(customMessage, ex);
	}
	
	
	public static void handleException(String errorMsg){
		notifySystemAdministratorOfException('Vodafone Exception', errorMsg);
	}
	
	
	private static void notifySystemAdministratorOfException(String customMessage, Exception ex){
		// Build the basic part of the exception message
		String subject = 'Vodafone Exception: ' + ex.getMessage();
		String body = ex.getMessage() + '\n'
					+ ex.getCause() + '\n'
					+ ex.getStackTraceString() + '\n\n'
					+ 'Username: ' + UserInfo.getUserName() + '\n'
					+ 'User Type: ' + UserInfo.getUserType() + '\n'
					+ 'Exception Type: ' + ex.getTypeName() + '\n'
					+ 'User ID: ' + UserInfo.getUserId() + '\n'; 
		
		// Add additional user information
		User currentUser = [SELECT FirstName, LastName FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
			
		body += 'Full Name: ' + currentUser.firstName + ' ' + currentUser.lastName + '\n';
		
		// Output additional information from the DMLException
			if(ex instanceof DMLException){
				body += '\n\rDMLException output:\n\r';
				DMLException dex = (DMLException) ex;
					
					for(Integer i=0; i < dex.getNumDml(); i++){
						body += 'Row ' + i + '\n';
						body += 'Message: ' + dex.getDmlMessage(i).escapeHtml4() + '\n';
						body += 'DmlFieldNames: ' + dex.getDmlFieldNames(i) + '\n';
						body += 'DmlId: ' + dex.getDmlId(i) + '\n';
						body += 'DmlIndex: ' + dex.getDmlIndex(i) + '\n';
							
							if(i == 10){
								body += '...';
							}
					}
			}
		
		// Output the page parameters and headers if we are in a page context
		if(ApexPages.currentPage() != null){
			body += '\n\rPage parameters:\n\r';
			Map<String, String> params = ApexPages.currentPage().getParameters();
			body += JSON.serialize( params );
			body += '\n\r\n\rPage headers:\n\r';
			body += JSON.serialize( ApexPages.currentPage().getHeaders() );
			body += '\n\r\n\r';
		}
		
		// Output the custom message if one has been specified
		if(customMessage != null){
			body += 'Custom exception message: ' + customMessage;
		}
					
		notifySystemAdministratorOfException(subject, body);
	}
	
	
	private static void notifySystemAdministratorOfException(String subject, String body){
		
		if(SYSTEM_ADMIN_EMAIL_ADDRESSES.isEmpty()) return;
			
		if(userIds.isEmpty()) return;
		
		for(Id uId : userIds){
			// put a check as to not break the max 10 email invocations max
			if(Limits.getEmailInvocations() < Limits.getLimitEmailInvocations()){
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setTargetObjectId(uId);
				mail.setSaveAsActivity(false);
				mail.setSubject(subject);
				mail.setPlainTextBody(body);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			} else {
                // We assume that sufficient admins will have received the mail..
			}
   	  	}				
	}

}