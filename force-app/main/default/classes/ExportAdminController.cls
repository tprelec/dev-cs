global with sharing class ExportAdminController {
	public ExportAdminController() {
		scheduleStarted = false;
	}

    public static String selectedTab {
        get;
        set;
    }


	public Boolean scheduleStarted{get;set;}
	
	public pageReference startExportSchedule(){
		scheduleStarted = true;
		ExportBatchSchedule__c b = ExportBatchSchedule__c.getOrgDefaults();
		
		// kill the previous job if it's still running
		if(b != null)
			if(b.scheduled_id__c != null)
				if(b.scheduled_id__c.length() > 1)
					try{
        				System.abortJob(b.scheduled_id__c);
					} catch (Exception e){
						// do nothing
					}
			
        DateTime n = datetime.now().addSeconds(30);
        String cron = '';
 
        cron += n.second();
        cron += ' ' + n.minute();
        cron += ' ' + n.hour();
        cron += ' ' + n.day();
        cron += ' ' + n.month();
        cron += ' ' + '?';
        cron += ' ' + n.year();
 
 		// kick off the first job of the chain (account export)
        b.scheduled_id__c = System.schedule('Accounts Export '+system.now(), cron, new ScheduleAccountExportBatch());
 
        update b; 			
        return null;		
	}
	
	public List<Account> getFailedAccounts(){
		if(selectedTab=='accounts'){
			List<Account> accts = [Select 
										Id, 
										Name,
										LastModifiedDate, 
										BOP_Export_Datetime__c, 
										BOP_Export_Errormessage__c 
									From 
										Account
									Where
										BOP_Export_Errormessage__c != null 
										AND
										BOP_Export_Errormessage__c != 'Valid BAN Number is required for exporting an Account'
									Order By LastModifiedDate DESC 
									LIMIT 1000
									];
			return accts;	
			} else return null;
	}
	
	public List<Ban__c> getFailedBans(){
		if(selectedTab=='bans'){
			List<Ban__c> bans = [Select 
										Id, 
										Name,
										LastModifiedDate, 
										BOP_Export_Datetime__c, 
										BOP_Export_Errormessage__c 
									From 
										Ban__c
									Where
										BOP_Export_Errormessage__c != null 
										AND
										BOP_Export_Errormessage__c != 'Valid BAN Number is required for exporting an Account'
									Order By LastModifiedDate DESC 
									LIMIT 1000
									];
			return bans;	
			} else return null;
	}

	public List<Contact> getFailedContacts(){
		if(selectedTab=='contacts'){
			List<Contact> contacts = [Select 
										Id, 
										Name, 
										LastModifiedDate,
										BOP_Export_Datetime__c, 
										BOP_Export_Errormessage__c 
									From 
										Contact
									Where
										BOP_Export_Errormessage__c != null 
										AND
										BOP_Export_Errormessage__c != 'Email is required for triggering the export to BOP'
									Order By LastModifiedDate DESC
									LIMIT 1000 
									];
			return contacts;
		} else return null;

	}
	
	public List<User> getFailedUsers(){
		if(selectedTab=='users'){

			List<User> users = [Select 
										Id, 
										Name, 
										LastModifiedDate,
										BOP_Export_Datetime__c, 
										BOP_Export_Errormessage__c 
									From 
										User
									Where
										BOP_Export_Errormessage__c != null 
									Order By LastModifiedDate DESC
									LIMIT 1000 
									];
			return users;	
		} else return null;			
	}
	
	public List<Site__c> getFailedSites(){
		if(selectedTab=='sites'){
			List<Site__c> sites = [Select 
										Id, 
										Name, 
										LastModifiedDate,
										BOP_Export_Datetime__c, 
										BOP_Export_Errormessage__c 
									From 
										Site__c
									Where
										BOP_Export_Errormessage__c != null 
									Order By LastModifiedDate DESC
									LIMIT 1000 
									];
			return sites;	
		} else return null;						
	}
	
	public List<Order__c> getFailedOrders(){
		if(selectedTab=='orders'){
			List<Order__c> orders = [Select 
										Id, 
										Name, 
										LastModifiedDate,
										BOP_Export_Datetime__c, 
										BOP_Export_Errormessage__c 
									From 
										Order__c
									Where
										BOP_Export_Errormessage__c != null 
									Order By LastModifiedDate DESC
									LIMIT 1000 
									];
			return orders;	
		} else return null;									
	}				

	public List<SelectOption> getTabSelectOptions(){
		
		// retrieve all sites
		List<SelectOption> tabSelectOptions = new List<SelectOption>();
		
		tabSelectOptions.add(new SelectOption('-','-'));
       	tabSelectOptions.add(new SelectOption('accounts','accounts'));
       	tabSelectOptions.add(new SelectOption('bans','bans'));
       	tabSelectOptions.add(new SelectOption('contacts','contacts'));
       	tabSelectOptions.add(new SelectOption('users','users'));
       	tabSelectOptions.add(new SelectOption('sites','sites'));
       	tabSelectOptions.add(new SelectOption('orders','orders'));
        	
		return tabSelectOptions;
        			
	}		
}