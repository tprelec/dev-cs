public with sharing class ExportUtils {
	public static void startNextJob(String jobType) {

		ExportBatchSchedule__c b = ExportBatchSchedule__c.getOrgDefaults();

		// kill the previous job if it's still running
		killPreviousJob(b);

		// check if the manual 'kill switch' hasn't been activated
		if (b.Halt_schedule__c) {
			b.Halt_schedule__c = false;
			update b;
		} else {
			// if not, then schedule the next job
			DateTime n = datetime.now().addSeconds(60);
			String cron = '';

			cron += n.second();
			cron += ' ' + n.minute();
			cron += ' ' + n.hour();
			cron += ' ' + n.day();
			cron += ' ' + n.month();
			cron += ' ' + '?';
			cron += ' ' + n.year();

			if (jobType == 'Accounts') {
				b.scheduled_id__c = System.schedule('Accounts Export '+system.now(), cron, new ScheduleAccountExportBatch());
			} else if (jobType == 'Bans') {
				b.scheduled_id__c = System.schedule('Bans Export '+system.now(), cron, new ScheduleBanExportBatch());
			} else if(jobType == 'Sites') {
				b.scheduled_id__c = System.schedule('Sites Export '+system.now(), cron, new ScheduleSiteExportBatch());
			} else if(jobType == 'Contacts') {
				b.scheduled_id__c = System.schedule('Contacts Export '+system.now(), cron, new ScheduleContactExportBatch());
			} else if(jobType == 'ContactRoles') {
				b.scheduled_id__c = System.schedule('ContactRoles Export '+system.now(), cron, new ScheduleContactRoleExportBatch());
			} else if(jobType == 'Users') {
				b.scheduled_id__c = System.schedule('Users Export '+system.now(), cron, new ScheduleUserExportBatch());
			} else if(jobType == 'OlbicoUpdate') {
				b.scheduled_id__c = System.schedule('Olbico Update '+system.now(), cron, new ScheduleAccountUpdatetBatch());
			}
			update b;
		}
	}

    private static void killPreviousJob(ExportBatchSchedule__c b) {
        if (b != null && b.scheduled_id__c != null && b.scheduled_id__c.length() > 1) {
            try {
                System.abortJob(b.scheduled_id__c);
            } catch (Exception e) {
                // do nothing
            }
        }
    }
}