global class FilterCataloguePlugin extends csbb.PluginManager.BasePlugin {
    
    global override Object invoke (Object basketId) {
        system.debug('*****returnList: ');
        return this.getApprovedIds();
        
    }
    
    public List<Id> getApprovedIds () {
        List<List<SObject>> searchList = [FIND 'Sample*' IN NAME FIELDS RETURNING cscfga__Product_Category__c (Id), cscfga__Configuration_Offer__c (Id)];
        List<Id> returnList = new List<Id>();
        returnList.addAll((new Map<Id, sObject>(searchList[0])).keyset());
        returnList.addAll((new Map<Id, sObject>(searchList[1])).keyset());
        
        system.debug('*****returnList: ' + JSON.serializePretty(returnList));
        
        return returnList;
        
    }
    
}