public without sharing class ForecastQuotaManager {
    
    public RevenueForecast dummyQuota {get;set;}
    public Map<String,quotaWrapper> keyToQuota {get;set;}
    public List<quotaWrapper> quotas {get;set;}
    public String fiscalYear {get;set;} 
    final private Integer fiscalYearStartMonth = 4; // fiscal year starts in april

    // dummyAccount is misused to generate user lookup and currency fields in VisualForce
    public Account dummyAccount {get;set;}


    public ForecastQuotaManager() {
        dummyQuota = new RevenueForecast();
        dummyAccount = new Account();
        keyToQuota = new Map<String,QuotaWrapper>();

        fiscalYear = String.valueOf(System.today().year());

    }

    public pageReference load(){
        //dummyAccount.Current_fixed_costs_monthly__c = 0;
        //dummyAccount.Current_mobile_costs_monthly__c = 0;
        updateDummyQuota();
        if(dummyAccount.OwnerId != null){
            keyToQuota = new Map<String,QuotaWrapper>();
            
            for(RevenueForecast rf : [Select Id, ProductFamily, StartDate, Quota, Commit From RevenueForecast Where OwnerId = :dummyQuota.OwnerId]){
                String key = String.valueOf(rf.StartDate)+rf.ProductFamily;
                if(keyToQuota.containsKey(key)){
                    keyToQuota.get(key).revenue = rf;
                    //keyToQuota.get(key).dqa.Current_fixed_costs_monthly__c = rf.Quota;

                } else {
                    quotaWrapper qw = new quotaWrapper(dummyQuota,rf.startDate);
                    qw.revenue = rf;
                    //qw.dqa.Current_fixed_costs_monthly__c = rf.Quota;
                    
                    qw.productFamily = rf.productFamily;
                    keyToQuota.put(key,qw);
                }
            }
            for(QuantityForecast qf : [Select Id, ProductFamily, StartDate, Quota, Commit From QuantityForecast Where OwnerId = :dummyQuota.OwnerId]){
                String key = String.valueOf(qf.StartDate)+qf.ProductFamily;
                if(keyToQuota.containsKey(key)){
                    keyToQuota.get(key).quantity = qf;
                    //keyToQuota.get(key).dqa.Current_mobile_costs_monthly__c = qf.Quota;

                } else {
                    quotaWrapper qw = new quotaWrapper(dummyQuota,qf.startDate);
                    qw.quantity = qf;
					//qw.dqa.Current_mobile_costs_monthly__c = qf.Quota;

                    qw.productFamily = qf.productFamily;
                    keyToQuota.put(key,qw);
                }
            }           
        }
        
        quotas = new List<quotaWrapper>();
        // now put the selected months in the final list
        if(dummyQuota.StartDate != null && dummyQuota.OwnerId != null){
            for(Integer i=0;i<12;i++){
                Date startDate = dummyQuota.StartDate.addMonths(i).toStartOfMonth();
                String key = String.valueOf(startDate)+dummyQuota.ProductFamily;

                if(keyToQuota.containsKey(key)){
                    quotas.add(keyToQuota.get(key));
        			//dummyAccount.Current_fixed_costs_monthly__c += keyToQuota.get(key).revenue.Quota==null?0:keyToQuota.get(key).revenue.Quota;
        			//dummyAccount.Current_mobile_costs_monthly__c += keyToQuota.get(key).quantity.Quota==null?0:keyToQuota.get(key).quantity.Quota;

                } else {
                    quotas.add(new quotaWrapper(dummyQuota,startDate));
                }
            }
        }
        return null;
    }

    public pageReference updateQuotas(){
/*        List<RevenueForecast> rfToUpdate = new List<RevenueForecast>();
        List<QuantityForecast> qfToUpdate = new List<QuantityForecast>();
        Try{
            for(quotaWrapper qw : quotas){
                if(qw.dqa.Current_fixed_costs_monthly__c != null ){
                    qw.revenue.Quota = qw.dqa.Current_fixed_costs_monthly__c;
                    rfToUpdate.add(qw.revenue);
                }
                if(qw.dqa.Current_mobile_costs_monthly__c != null){
                    qw.quantity.quota = qw.dqa.Current_mobile_costs_monthly__c;
                    qfToUpdate.add(qw.quantity);
                }
            }
        } catch (TypeException te){
            Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,'Wrong input data: ' +te.getMessage()));
            return null;
        }
        
        try{
            // these do not always exist upfront, so do an upsert
            system.debug(rfToUpdate);
            upsert rfToUpdate;
            upsert qfToUpdate;
        } catch (dmlException de){
            Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,'Error updating values: ' +de));
            return null;            
        }
*/
        // reload to make sure that the totals update
        return load();
    }


    public class quotaWrapper{
        public Integer year{get;set;}
        public Integer month {get;set;}
        public Date startDate {get;set;}
        public String productFamily{get;set;}
        public RevenueForecast revenue {get;set;}
        public QuantityForecast quantity {get;set;}
        public Account dqa {get;set;} //dummyQuotaAccount, for displaying/capturing currency fields correctly on the screen

        public quotaWrapper(RevenueForecast theDummyQuota, Date startDate){
            this.startDate = startDate;
            this.month = startDate.Month();
            this.year = startDate.year();
            this.productFamily = theDummyQuota.ProductFamily;
            this.revenue = new RevenueForecast();
            this.revenue.OwnerId = theDummyQuota.ownerId;
            this.revenue.StartDate = startDate;
            this.revenue.ProductFamily = theDummyQuota.ProductFamily;
            this.quantity = new QuantityForecast();
            this.quantity.OwnerId = theDummyQuota.ownerId;
            this.quantity.StartDate = startDate;
            this.quantity.ProductFamily = theDummyQuota.ProductFamily;
            this.dqa = new Account();

        }
    }

    //builds a picklist of Product Families
    public String productFamilySelected {get;set;}
    
    public List<selectOption> getProductFamilies() {
        List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options

        options.add(new SelectOption('', '--None--')); 

        Schema.DescribeFieldResult prodFamily = Product2.Family.getDescribe();
        for (Schema.PicklistEntry a : Product2.Family.getDescribe().getPicklistValues()){ 
            options.add(new SelectOption(a.getLabel(), a.getValue())); 
        }

        return options; //return the picklist options
    }   


    public void updateDummyQuota(){
        // move data from the accessible fields to the dummyQuota object
        dummyQuota.ownerId = dummyAccount.OwnerId;
        Date d = Date.newinstance(Integer.valueOf(fiscalYear), fiscalYearStartMonth, 1);//dummyAccount.Framework_Agreement_date__c;
        dummyQuota.StartDate = d; 
        dummyQuota.ProductFamily = productFamilySelected;
    }

    public String revenueExcelInput{get;set;}
    public pageReference processRevenueExcelValues(){
        
        List<String> remaining = new List<String>();

        if(revenueExcelInput.length() > 0){
	        // remove euro signs and thousand separators
	        revenueExcelInput = revenueExcelInput.replace('€ ','');
	        revenueExcelInput = revenueExcelInput.replace('€','');
	        revenueExcelInput = revenueExcelInput.replace('.','');        

	        if(revenueExcelInput.contains(' ')){
	        	remaining = revenueExcelInput.split(' ');
	        }
	        system.debug(remaining);
	        try{
		        for(quotaWrapper qw : quotas){
		            if(!remaining.isEmpty()){
		            	// user has nl format (comma as decimal separator), so turn that around
		            	String formattedDecimal = remaining[0].replace(',','.');
		            	system.debug(formattedDecimal);



			        	//qw.dqa.Current_fixed_costs_monthly__c = decimal.valueOf(formattedDecimal);
		                remaining.remove(0);
		            }        	

		        }
	        } catch (TypeException te){
	            Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,'Wrong input data: ' +te.getMessage()));
	            return null;
	        }
	        revenueExcelInput = '';
	    }
	    return null;
    }

    public String quantityExcelInput{get;set;}
    public pageReference processQuantityExcelValues(){
        List<String> remaining = new List<String>();
        
        if(quantityExcelInput.length() > 0){
	        // remove euro signs and thousand separators
	        quantityExcelInput = quantityExcelInput.replace('€ ','');
	        quantityExcelInput = quantityExcelInput.replace('€','');
	        quantityExcelInput = quantityExcelInput.replace('.','');        

	        if(quantityExcelInput.contains(' ')){
	        	remaining = quantityExcelInput.split(' ');
	        }
	        system.debug(remaining);
	        try{
		        for(quotaWrapper qw : quotas){
		            if(!remaining.isEmpty()){
		            	// user has nl format (comma as decimal separator), so turn that around
		            	String formattedDecimal = remaining[0].replace(',','.');
		            	system.debug(formattedDecimal);

			        	//qw.dqa.Current_mobile_costs_monthly__c = decimal.valueOf(formattedDecimal);
		                remaining.remove(0);
		            }        	

		        }
	        } catch (TypeException te){
	            Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,'Wrong input data: ' +te.getMessage()));
	            return null;
	        }
	        quantityExcelInput = '';
	    }
	    return null;
    }    
}