public class FutureWaiterController
{
    
    public Boolean statusKpi {get; set;}
    public Boolean statusSnapshot {get; set;}
    public Integer numberOfMillisecondsToWait {get; set;}
    public String basketId {get; set;}
    public cscfga__Product_Basket__c basket;
    
    public FutureWaiterController() {
        Future_Optimisation__c optimisationSettings = Future_Optimisation__c.getInstance('Standard Optimisation');
        numberOfMillisecondsToWait = Integer.valueOf(optimisationSettings.Refresh_number_of_seconds__c) * 1000;
        
        basketId = System.currentPageReference().getParameters().get('basketId');
        basket = [SELECT Benchmark_Fixed__c, Benchmark_Mobile__c FROM cscfga__Product_Basket__c WHERE id = :basketId];
        statusKpi = basket.Benchmark_Fixed__c;
        statusSnapshot = basket.Benchmark_Mobile__c;
    }
    
    public PageReference checkFuture()
    {
        if(basketId != null && basketId != '') {
            if(statusKpi == false && statusSnapshot == false) {
                PageReference pageRef = new PageReference('/apex/csbb__basketbuilderapp?Id=' + basketId);
                pageRef.setRedirect(true);
                return pageRef;
            }
        
        
            if(basketId != null && basketId != '') {
                PageReference pageRef = new PageReference('/apex/c__FutureWaiterPage?basketId=' + basketId);
                pageRef.setRedirect(true);
                return pageRef;
            }
        }
        
        return null;
    }
}