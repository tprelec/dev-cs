global with sharing class GenerateDocPreExecHandler implements csclmcb.IPreExecutionHandler {
    
    public csclmcb.Result run (Id agreementId) {
        
        //If Agreement requires Approval but is not 'Document Approved' yet.
        
        csclm__Agreement__c a = [select Id, csclm__Opportunity__c, csclm__Approval_Type__c, csclm__Approval_Status__c 
                                 from csclm__Agreement__c 
                                 where Id = :agreementId];
        /*
        if (a.csclm__Approval_Type__c != 'No Approval' && a.csclm__Approval_Status__c != 'Document Approved') {
            return csclmcb.Result.failure('The Agreement has not gained Approval yet. A Document cannot be generated.');
        }
        else {
            return csclmcb.Result.success(a);
        }*/
        
        return csclmcb.Result.success(a);
    }
}