/*
 *      Description:    this class contains the logic to generate a set of test data for a new sandbox environent
 *                      DO NOT RUN THIS CLASS ON PRODUCTION! It will result in garbage data
 *      Author:         Guy Clairbois
 */

public with sharing class GenerateTestDataSet {
    

    
    public static void generateTestDataSet() {
        // MISC MAPPINGS

        // Field sync mappings
        // system.debug(JSON.serialize([Select Source_Field_Name__c,Target_Field_Name__c,Insert_Only__c,Object_Mapping__c From Field_Sync_Mapping__c]));
        String fieldsyncmappingData = generateTestDataSet.nameToResource.get('testdata_fieldsyncmappings').Body.toString();
        List<Field_Sync_Mapping__c> mappingsToUpsert = new List<Field_Sync_Mapping__c>();
        // make clones to remove id's
        for(Field_Sync_Mapping__c u : (List<Field_Sync_Mapping__c>)JSON.deserialize(fieldsyncmappingData,List<Field_Sync_Mapping__c>.class)){
            Field_Sync_Mapping__c u2 = u.clone(false,true);
            mappingsToUpsert.add(u2);
        }
        upsert mappingsToUpsert;            


        // Preferred suppliers
        // system.debug(JSON.serialize([Select Order__c,Technology_Type__c,UniqueId__c,Vendor__c From Preferred_Supplier__c]));
        String psData = generateTestDataSet.nameToResource.get('testdata_preferredsuppliers').Body.toString();
        List<Preferred_Supplier__c> psToUpsert = new List<Preferred_Supplier__c>();
        // make clones to remove id's
        for(Preferred_Supplier__c u : (List<Preferred_Supplier__c>)JSON.deserialize(psData,List<Preferred_Supplier__c>.class)){
            Preferred_Supplier__c u2 = u.clone(false,true);
            psToUpsert.add(u2);
        }
        upsert psToUpsert;      

        // SLA
        //system.debug(JSON.serialize([Select Name__c,Active__c,BOP_Code__c,Number_of_days__c,Object__c,Reaction_time__c,Reactive_Proactive__c,Repair_time__c,Replace_by__c,SLA_type__c From SLA__c]));
        String slaData = generateTestDataSet.nameToResource.get('testdata_slas').Body.toString();
        List<SLA__c> slaToUpsert = new List<SLA__c>();
        // make clones to remove id's
        for(SLA__c u : (List<SLA__c>)JSON.deserialize(slaData,List<SLA__c>.class)){
            SLA__c u2 = u.clone(false,true);
            slaToUpsert.add(u2);
        }
        upsert slaToUpsert; 


        // Postalcoderegion mapping (only the first 100 as this is too much data)
        // system.debug(JSON.serialize([Select Postcode_Number__c,Name,Channel__c,Dealer_Code__c,Dealer_Information__c,MajorAMSoHo__c,Postal_Code_Unique__c,ZSP__c From Postal_Code_Assignment__c LIMIT 100]));
        String pcaData = generateTestDataSet.nameToResource.get('testdata_postalcodeassignments').Body.toString();
        List<Postal_Code_Assignment__c> pcaToUpsert = new List<Postal_Code_Assignment__c>();
        // make clones to remove id's
        for(Postal_Code_Assignment__c u : (List<Postal_Code_Assignment__c>)JSON.deserialize(pcaData,List<Postal_Code_Assignment__c>.class)){
            Postal_Code_Assignment__c u2 = u.clone(false,true);
            u2.Dealer_Information__c = null;
            u2.ZSP__c = null;
            u2.MajorAMSoHo__c    = null;
            pcaToUpsert.add(u2);
        }
        upsert pcaToUpsert;

        // update custom settings to prevent unwanted exports to production environments
        List<External_Webservice_Config__c> ewconfigs = [Select Id, Name, URL__c From External_Webservice_Config__c];

        for(External_Webservice_Config__c ewc : ewconfigs){
            // prevent this from running more than once
            // BOP link
            ewc.URL__c.replace('https://esb.','https://TBD.');
            // SIAS link
            ewc.URL__c.replace('https://besrv.vodafone','https://TBD.vodafone');
            // D&B link
            ewc.URL__c.replace('https://webservices.','https://TBD.');
        }
        update ewconfigs;

        // PRODUCTS
        // create family tags
        // this is the query used for generating the json string that was pasted into the txt file familtytags.txt:
        // system.debug(JSON.serialize([Select Id,Name,Family_Tag_Name2__c From VF_Family_Tag__c ]));
        String familytagData = generateTestDataSet.nameToResource.get('testdata_familytags').Body.toString();
        List<VF_Family_Tag__c> tagsToUpsert = new List<VF_Family_Tag__c>();
        // make clones to remove id's
        for(VF_Family_Tag__c u : (List<VF_Family_Tag__c>)JSON.deserialize(familytagData,List<VF_Family_Tag__c>.class)){
            VF_Family_Tag__c u2 = u.clone(false,true);
            tagsToUpsert.add(u2);
        }
        upsert tagsToUpsert Family_Tag_Name2__c;    
      

        // create vf_products
        // needs to be done manually, as the list is continuously updated on prod.
        // just load all vf_products into excel and insert them into the sandbox. The triggerhanlder will take care of linking vf_products to product2 records


        // PARTNERACCOUNTS/CUSTOMERS/SITES/CONTACTS

        // create/check recordtypes
        // customer account
        Id customerRecordTypeId = [Select Id From RecordType Where SObjectType='Account' and DeveloperName = 'VF_Account'].Id;
        Id customerContactRecordTypeId = [Select Id From RecordType Where SObjectType='Contact' and DeveloperName = 'General'].Id;
        
        // partner account
        Id partnerRecordTypeId = [Select Id From RecordType Where SObjectType='Account' and DeveloperName = 'Dealer_Accounts'].Id;
        
        // create/check profiles -> should be there. Use GeneralUtils map.
        
        // create/check roles -> should be there. Use GeneralUtils map.

        // create/check Vodafone Account
        Account vodafoneAccount = new Account(BAN_Number__c = '318837537', BOPCode__c = 'TNF', Name='Vodafone Libertel B.v.', KVK_Number__c='04052264'); 
        upsert vodafoneAccount BOPCode__c;


        // create/check partner account
        // fetch a business partner manager user
        Id bpmUserId = [Select Id From User Where Profile.Name = 'VF Business Partner Manager' AND UserRoleId != null AND IsActive = true LIMIT 1].Id;
        // this is the query used for generating the json string that was pasted into the txt file bpaccounts.txt:
        // JSON.serialize([SELECT Name,IsPartner,OwnerId,Phone,RecordTypeId,Type,BigMachines__Partner_Organization__c,Channel__c,Dealer_code__c,Paid_dealer__c,Total_Licenses__c,VIP_Partner__c,Visiting_City__c,Visiting_Country__c,Visiting_Housenumber1__c,Visiting_Housenumber_Suffix__c,Visiting_Postal_Code__c,Visiting_street__c FROM Account Where Name = 'Test Partner - BP']) 
        String bpaccountData = generateTestDataSet.nameToResource.get('testdata_bpaccounts').Body.toString();
        List<Account> partnerAccounts = new List<Account>();
        // make clones to remove id's
        for(Account u : (List<Account>)JSON.deserialize(bpaccountData,List<Account>.class)){
            Account u2 = u.clone(false,true);
            u2.OwnerId = bpmUserId;
            u2.recordTypeId = partnerRecordTypeId;
            partnerAccounts.add(u2);
        }
        insert partnerAccounts;    
        // actually make them partneraccounts
        for(Account a : partnerAccounts){
            a.IsPartner = true;
        }     
        update partnerAccounts;
        
        // create/check partner contacts
        // this is the query used for generating the json string that was pasted into the txt file partneraccounts.txt:
        // JSON.serialize([Select LastName,AccountId,Email,FirstName,MobilePhone FROM Contact Where Account.Name = 'Test Partner - BP']);   
        String bpcontactData = generateTestDataSet.nameToResource.get('testdata_bpcontacts').Body.toString();
        List<Contact> partnerContacts = new List<Contact>();
        // make clones to remove id's
        for(Contact u : (List<Contact>)JSON.deserialize(bpcontactData,List<Contact>.class)){
            Contact u2 = u.clone(false,true);
            // default  to  first partner account (could be changed later)
            u2.AccountId = partnerAccounts[0].Id;
            partnerContacts.add(u2);
        }
        insert partnerContacts;     
        Set<Id> partnerContactIds = new Set<Id>();
        for(Contact c : partnerContacts){
            partnerContactIds.add(c.Id);
        }
        
        // create internal and partner users (queueable)
        System.enqueueJob(new GenerateTestDataSetQueueable(partnerContactIds));        
         
        // Dealer Information (attached to create dealer users)
        Dealer_Information__c di = new Dealer_Information__c();
        di.Dealer_Code__c = '800999';
        di.Name = partnerAccounts[0].Name;
        di.Chain__c = partnerAccounts[0].Name;
        di.Contact__c = partnerContacts[0].Id;
        di.Parent_Dealer_Code__c  = di.Dealer_Code__c;
        di.Status__c  = 'Active';
        di.Verified__c = true;
        insert di;


        //  create customer accounts
        // this is the query used for generating the json string that was pasted into the txt file custaccounts.txt:
        // JSON.serialize([Select Name,AccountNumber,AnnualRevenue,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,Description,Fax,Industry,IsPartner,NumberOfEmployees,Phone,RecordTypeId,Sic,Type,Website,Account_Manager__c,Account_Owner_IsActive__c,Account_Owner_Manager__c,Active_Ban_Count__c,Active_ONE_ONX__c,BAN_Number__c,BAN_status__c,Bankruptcy_Indicator__c,BIK_Code__c,Date_of_Establishment__c,Dealer_code__c,DUNS_Number__c,Frame_Work_Agreement__c,Framework_agreement_date__c,INT_UniqueAccount_ID__c,isOwnerChanged__c,ITW_Indicator__c,KVK_number__c,Legal_Structure__c,Legal_Structure_Code__c,Location_Status__c,Managed_by_D_B__c,NPS_Score__c,NumberOfEmployeesConcern__c,Previous_Fixed_Owner__c,Previous_Owner__c,Risk_Change_Date__c,SBI_Description__c,SBI_Main_Description__c,Screen_quantity__c,Server_quantity__c,Site_Update__c,Soho_Vertical__c,Trigger_Olbico_Update__c,Type_of_Change__c,Unify_Account_SubType__c,Unify_Account_Type__c,Unify_Ref_Id__c,VAT_Number__c,Version_FWA__c,VGE__c,VIP_Partner__c,Visiting_City__c,Visiting_Country__c,Visiting_Housenumber1__c,Visiting_Housenumber_Suffix__c,Visiting_Postal_Code__c,Visiting_street__c From Account Where Name in ('TOSCA TA BSS - Customer 2 VGE','TOSCA TA BSS - Customer 1 SOHO','TOSCA TA BSS - Customer 7 Enterprise','TOSCA TA BSS - Customer 5 Business','DE NIEUWE SCHAAR','KIWI ONLINE','De Grote Kikker B.v.')])    
        String accountData = generateTestDataSet.nameToResource.get('testdata_custaccounts').Body.toString();
        List<Account> custAccounts = new List<Account>();
        // make clones to remove id's
        for(Account u : (List<Account>)JSON.deserialize(accountData,List<Account>.class)){
            Account u2 = u.clone(false,true);
            u2.recordTypeId = customerRecordTypeId;
            u2.Logo_URL__c = 'testdataset'; // add a marker to refer to these accounts later on
            custAccounts.add(u2);
        }
        upsert custAccounts KvK_Number__c;
        
        // create a Map for referring accounts by KvK number
        Map<String,Id> kvkToAcctId = new Map<String,Id>();
        //for(Account a : custAccounts){
        for(Account a : [Select Id,KvK_Number__c From Account Where Logo_URL__c = 'testdataset']){
            kvkToAcctId.put(a.KvK_Number__c,a.Id);
        }           
        
        // create customer contacts
        // this is the query used for generating the json string that was pasted into the txt file custcontacts.txt:
        // JSON.serialize([Select Name,AccountNumber,AnnualRevenue,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,Description,Fax,Industry,IsPartner,NumberOfEmployees,Phone,RecordTypeId,Sic,Type,Website,Account_Manager__c,Account_Owner_IsActive__c,Account_Owner_Manager__c,Active_Ban_Count__c,Active_ONE_ONX__c,BAN_Number__c,BAN_status__c,Bankruptcy_Indicator__c,BIK_Code__c,Date_of_Establishment__c,Dealer_code__c,DUNS_Number__c,Frame_Work_Agreement__c,Framework_agreement_date__c,INT_UniqueAccount_ID__c,isOwnerChanged__c,ITW_Indicator__c,KVK_number__c,Legal_Structure__c,Legal_Structure_Code__c,Location_Status__c,Managed_by_D_B__c,NPS_Score__c,NumberOfEmployeesConcern__c,Previous_Fixed_Owner__c,Previous_Owner__c,Risk_Change_Date__c,SBI_Description__c,SBI_Main_Description__c,Screen_quantity__c,Server_quantity__c,Site_Update__c,Soho_Vertical__c,Trigger_Olbico_Update__c,Type_of_Change__c,Unify_Account_SubType__c,Unify_Account_Type__c,Unify_Ref_Id__c,VAT_Number__c,Version_FWA__c,VGE__c,VIP_Partner__c,Visiting_City__c,Visiting_Country__c,Visiting_Housenumber1__c,Visiting_Housenumber_Suffix__c,Visiting_Postal_Code__c,Visiting_street__c From Account Where Name in ('TOSCA TA BSS - Customer 2 VGE','TOSCA TA BSS - Customer 1 SOHO','TOSCA TA BSS - Customer 7 Enterprise','TOSCA TA BSS - Customer 5 Business','DE NIEUWE SCHAAR','KIWI ONLINE','De Grote Kikker B.v.')])    
        String contactData = generateTestDataSet.nameToResource.get('testdata_custcontacts').Body.toString();
        List<Contact> custContacts = new List<Contact>();
        // make clones to remove id's
        for(Contact u : (List<Contact>)JSON.deserialize(contactData,List<Contact>.class)){
            Contact u2 = u.clone(false,true);
            // misusing the Associated_Competitor_Asset__c field to create the link to the correct account
            u2.accountId = kvkToAcctId.get(u.Associated_Competitor_Asset__c);
            u2.recordTypeId = customerContactRecordTypeId;
            u2.OwnerId = userinfo.getUserId();
            custContacts.add(u2);
        }
        upsert custContacts Email;
        
        // create customer sites
        // this is the query used for generating the json string that was pasted into the txt file custsites.txt:
        // JSON.serialize([Select Name,AccountNumber,AnnualRevenue,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,Description,Fax,Industry,IsPartner,NumberOfEmployees,Phone,RecordTypeId,Sic,Type,Website,Account_Manager__c,Account_Owner_IsActive__c,Account_Owner_Manager__c,Active_Ban_Count__c,Active_ONE_ONX__c,BAN_Number__c,BAN_status__c,Bankruptcy_Indicator__c,BIK_Code__c,Date_of_Establishment__c,Dealer_code__c,DUNS_Number__c,Frame_Work_Agreement__c,Framework_agreement_date__c,INT_UniqueAccount_ID__c,isOwnerChanged__c,ITW_Indicator__c,KVK_number__c,Legal_Structure__c,Legal_Structure_Code__c,Location_Status__c,Managed_by_D_B__c,NPS_Score__c,NumberOfEmployeesConcern__c,Previous_Fixed_Owner__c,Previous_Owner__c,Risk_Change_Date__c,SBI_Description__c,SBI_Main_Description__c,Screen_quantity__c,Server_quantity__c,Site_Update__c,Soho_Vertical__c,Trigger_Olbico_Update__c,Type_of_Change__c,Unify_Account_SubType__c,Unify_Account_Type__c,Unify_Ref_Id__c,VAT_Number__c,Version_FWA__c,VGE__c,VIP_Partner__c,Visiting_City__c,Visiting_Country__c,Visiting_Housenumber1__c,Visiting_Housenumber_Suffix__c,Visiting_Postal_Code__c,Visiting_street__c From Account Where Name in ('TOSCA TA BSS - Customer 2 VGE','TOSCA TA BSS - Customer 1 SOHO','TOSCA TA BSS - Customer 7 Enterprise','TOSCA TA BSS - Customer 5 Business','DE NIEUWE SCHAAR','KIWI ONLINE','De Grote Kikker B.v.')])    
        String siteData = generateTestDataSet.nameToResource.get('testdata_custsites').Body.toString();
        List<Site__c> custSites = new List<Site__c>();
        // make clones to remove id's
        for(Site__c u : (List<Site__c>)JSON.deserialize(siteData,List<Site__c>.class)){
            Site__c u2 = u.clone(false,true);
            // misusing the kvk field to create the link to the correct account
            u2.site_Account__c = kvkToAcctId.get(u.KvK_Number__c);
            custSites.add(u2);
        }
        Schema.SObjectField theExternalId = Site__c.Fields.KvK_Number__c;
        Database.UpsertResult[] results = Database.upsert(custSites,theExternalId,false);
        
        // create postal check results
        // prepare default set
        // this is the query used for generating the json string that was pasted into the txt file custsites.txt:
        // system.debug(JSON.serialize([Select Id,Access_Site_ID__c,Access_Active__c,Access_Max_Down_Speed__c,Access_Max_Up_Speed__c,Access_Priority__c,Access_Region__c,Access_Result_Check__c,Access_Vendor__c,Accessclass__c,Existing_Infra__c,Technology_Type__c From Site_Postal_Check__c Where Access_Site_ID__c =  'a065E000000uRHZ'])); 
        String pcData = generateTestDataSet.nameToResource.get('testdata_custsitepostalchecks').Body.toString();
        List<Site_Postal_Check__c> custPCs = new List<Site_Postal_Check__c>();
        // make clones to remove id's
        for(Site_Postal_Check__c u : (List<Site_Postal_Check__c>)JSON.deserialize(pcData,List<Site_Postal_Check__c>.class)){
            Site_Postal_Check__c u2 = u.clone(false,true);
            custPCs.add(u2);
        }

        // for each site, create the full set of postalcheckresults
        List<Site_Postal_Check__c> pcsToInsert = new List<Site_Postal_Check__c>();              
        for(Site__c anySite : [Select Id From Site__c Where Site_Account__r.Logo_URL__c = 'testdataset']){
            // insert a set of postalcodecheckresults
            for(Site_Postal_Check__c pc : custPCs){
                Site_Postal_Check__c pc2 = pc.clone();
                pc2.Access_Site_ID__c = anySite.Id;
                pcsToInsert.add(pc2);
            }
        }
        Database.insert(pcsToInsert,false); 

        // create BANs
        List<String> custTypeWithSubType = new List<String>{'CA','GU','BW'};
        Integer counter = 0;        
        List<BAN__c> bansToInsert = new List<BAN__c>();
        for(Account a : custAccounts){
            BAN__c aBan = new BAN__c();
            aBan.Account__c = a.Id;
            Integer bannr = 543210000 + counter;
            ///aBan.Name = '543210' + string.valueOf(Math.mod(Math.round(Math.random()*1000),999)).leftPad(3, '0');
            aBan.Name = string.valueOf(bannr);
            aBan.BAN_Name__c = a.Name;
            aBan.BAN_Status__c = 'Active';
            Integer cidnr = 12345000 + counter;
            aBan.Corporate_Id__c = string.valueOf(cidnr);
            aBan.Unify_Customer_Type__c = custTypeWithSubType[math.mod(counter,3)].subString(0,1);
            aBan.Unify_Customer_SubType__c = custTypeWithSubType[math.mod(counter,3)].substring(1,2); 
            aBan.Unify_Ref_Id__c = aBan.Name;
            
            counter++;  
                                
            bansToInsert.add(aBan);
        }
        insert bansToInsert;
        
        // create FAs
        // prepare a map of financial contacts
        Map<Id,Id> accountIdToContactId = new Map<Id,Id>();
        for(Contact c : custContacts){
            accountIdToContactId.put(c.AccountId,c.Id);
        }       
        
        List<Financial_Account__c> faToInsert = new List<Financial_Account__c>();
        for(BAN__c a : bansToInsert){
            Financial_Account__c aFA = new Financial_Account__c();
            aFA.BAN__c = a.Id;
            if(accountIdToContactId.containsKey(a.Account__c)){
                aFA.Financial_Contact__c = accountIdToContactId.get(a.Account__c);
            }
            // bank accout name is not used from here, but we store it in order to be able to use in on BAR generation
            aFA.Bank_Account_Name__c = a.Name;
            //aFA.Payment_method__c = 'Invoice';
            aFA.Status__c = 'Open';
            aFA.Unify_Ref_Id__c = a.Name;
            
            faToInsert.add(aFA);
        }
        insert faToInsert;      
        
        // create BARs
        List<Billing_Arrangement__c> barToInsert = new List<Billing_Arrangement__c>();
        for(Financial_Account__c a : faToInsert){
            Billing_Arrangement__c aBAR = new Billing_Arrangement__c();
            aBAR.Financial_Account__c = a.Id;
            aBAR.Billing_Arrangement_Alias__c = a.Bank_Account_Name__c;
            aBAR.Bank_Account_Name__c = a.Bank_Account_Name__c;
            aBAR.Bill_Format__c = 'E';
            aBAR.Bill_Production_Indicator__c = 'Y';
            aBAR.Billing_City__c = StringUtils.randomString(15);
            aBAR.Billing_Country__c = 'NL';
            aBAR.Billing_Housenumber__c = String.valueOf(Math.mod(Math.round(Math.random()*1000),99));
            //aBAR.Billing_Housenumber_Suffix__c    
            aBAR.Billing_Postal_Code__c = '1235AB' ;    
            aBAR.Billing_Street__c = StringUtils.randomString(12);
            aBAR.Payment_method__c = 'Invoice';
            aBAR.Status__c = 'Open';
            aBAR.Unify_Ref_Id__c = a.Name;
            
            barToInsert.add(aBAR);
        }
        insert barToInsert;             
        
    }

    public static Map<String,StaticResource> nameToResource{
        get{
            if(nameToResource == null){        
                // a map of static resources containing the data
                nameToResource = new Map<String,StaticResource>();
                for(StaticResource sr : [SELECT Id, Name, Body FROM StaticResource WHERE Name like 'testdata%']){
                    nameToResource.put(sr.Name,sr);
                }
            }
            return nameToResource;
        }
        private set;
    }    
    
 
}