/**
 * @description         This class contains some items in test data generation that need to be queued (e.g. user creation)
 * @author              Guy Clairbois
 */

global class GenerateTestDataSetQueueable implements Queueable {
    
    private Set<Id> partnerContactIds;
    public GenerateTestDataSetQueueable(Set<Id> pcIds) {
        partnerContactIds = pcIds;
    }

    global void execute(QueueableContext context) {
        // create users
        // this is the query used for generating the json string that was pasted into the txt file users.txt:
        // JSON.serialize([Select Alias,CommunityNickname,Email,EmailEncodingKey,LanguageLocaleKey,LastName,LocaleSidKey,ProfileId,TimeZoneSidKey,Username,CompanyName,FirstName,IsActive,ManagerId,MobilePhone,Phone,UserRoleId,BigMachines__Time_Zone__c,Primary_Partner__c,SalesChannel__c,Ziggo__c,Name,User_License__c  From User Where UserName in ('test.bp@vfexample.com.uat','test.cm@vfexample.com.uat','test.cccommittee@vfexample.com.uat','test.sagindirect@vfexample.com.uat','test.sagdirect@vfexample.com.uat','test.creditassessment@vfexample.com.uat','test.bpsolutionsales@vfexample.com.uat','test.sc@vfexample.com.uat','test.bpmanager@vfexample.com.uat','test.salesmanager@vfexample.com.uat','test.accountmanager@vfexample.com.uat','gssgmscpsg_vfnlsias@amdocs.com.uat','ashish.redekar@amdocs.com.uat','ashish.beniwal@amdocs.com.uat','bo.vries@vodafone.com.uat')];  
        String userData = GenerateTestDataSet.nameToResource.get('testdata_users').Body.toString();
        List<User> users = new List<User>();
        // make clones to remove id's
        for(User u : (List<User>)JSON.deserialize(userData,List<User>.class)){
            User u2 = u.clone(false,true);
            // add the environment suffix (otherwise we'll have duplicate usernames)
            u2.CommunityNickname = u.CommunityNickname+UserInfo.getUserName().substringAfterLast('.');
            u2.username = u.username+UserInfo.getUserName().substringAfterLast('.');
            u2.ManagerId = null;
            users.add(u2);
        }
        system.debug(users);
        Database.insert(users,false);       
    
    
        // create/check partner users
        List<Contact> partnerContacts = [Select Id, Email, LastName, FirstName From Contact Where Id in :partnerContactIds];
        
        Id ppId = [Select Id From Profile Where Name = 'VF Partner Portal User'].Id;
        
        List<User> bpUsers = new List<User>();
        for(Contact c : partnerContacts){
            User u = new User();
            u.ProfileId = ppId;
            u.UserName = c.Email+StringUtils.randomString(4)+UserInfo.getUserName().substringAfterLast('.');
            u.Email=c.Email;
            u.LastName = c.LastName;
            u.FirstName = c.FirstName;
            u.ContactId = c.Id;
            u.Alias = c.FirstName+system.now().millisecond();
            u.TimeZoneSidKey = 'Europe/Amsterdam';
            u.LocaleSidKey = 'nl_NL';
            u.EmailEncodingKey = 'ISO-8859-1';
            u.LanguageLocaleKey = 'en_US';
            bpUsers.add(u);
        }
        insert bpUsers;
    

    }
    
}