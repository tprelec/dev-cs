@RestResource(urlMapping='/getbillingarrangements/*')
/**
* @description: Retrieves a list of available BANs within the context of the Billing Customer.
* @author: Jurgen van Westreenen
*/
global without sharing class GetBillingArrangementsService {    
    @HttpPost
    global static void returnBillingArrangements() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        JSONGenerator generator = JSON.createGenerator(false);
        try {     
            // Retrieve Billing Customer Id from request
            Map<String, Object> reqMap = (Map<String, Object>)JSON.deserializeUntyped(req.requestbody.tostring());
            String billingCustomerId = (String)reqMap.get('billingCustomerId');       
            system.debug('billingCustomerId: ' + billingCustomerId);            
            // Start building response JSON
            if (String.isNotBlank(billingCustomerId)) {            
                // Retrieve Billing Arrangements        
                List<Billing_Arrangement__c> billingArrangements = [SELECT Id, Status__c, Bill_Production_Indicator__c, Bill_Format__c, Payment_method__c,
                                                                    Bank_Account_Name__c, Bank_Account_Number__c, Billing_Street__c, Billing_Housenumber__c,
                                                                    Billing_Housenumber_Suffix__c, Billing_Postal_Code__c, Billing_City__c, Billing_Country__c,
                                                                    Billing_Arrangement_Alias__c, Unify_Ref_Id__c
                                                                    FROM Billing_Arrangement__c
                                                                    WHERE Financial_Account__r.BAN__r.Ban_Number__c = :billingCustomerId];
                system.debug(billingArrangements);
                if (billingArrangements.size() > 0) {
                    // Write Billing Arrangement details to response
                    generator.writeStartObject();
                    generator.writeStringField('status', 'OK');        
                    generator.writeFieldName('billingArrangements');
                    generator.writeStartArray();                      
                    for (Billing_Arrangement__c ba : billingArrangements) {
                        generator.writeStartObject();  
                        generator.writeStringField('billingArrangementId', ba.Unify_Ref_Id__c != null ? ba.Unify_Ref_Id__c : '');
                        generator.writeStringField('status', ba.Status__c != null ? ba.Status__c : '');
                        generator.writeStringField('billProductionIndicator', ba.Bill_Production_Indicator__c != null ? ba.Bill_Production_Indicator__c : '');
                        generator.writeStringField('billFormat', ba.Bill_Format__c != null ? ba.Bill_Format__c : '');
                        generator.writeStringField('paymentMethod', ba.Payment_method__c != null ? ba.Payment_method__c : '');
                        generator.writeStringField('bankAccountName', ba.Bank_Account_Name__c != null ? ba.Bank_Account_Name__c : '');
                        generator.writeStringField('bankAccountNumber', ba.Bank_Account_Number__c != null ? ba.Bank_Account_Number__c : '');
                        generator.writeStringField('billingStreet', ba.Billing_Street__c != null ? ba.Billing_Street__c : '');
                        generator.writeStringField('bilingHousenumber', ba.Billing_Housenumber__c != null ? ba.Billing_Housenumber__c : '');
                        generator.writeStringField('bilingHousenumberSuffix', ba.Billing_Housenumber_Suffix__c != null ? ba.Billing_Housenumber_Suffix__c : '');
                        generator.writeStringField('billingPostalcode', ba.Billing_Postal_Code__c != null ? ba.Billing_Postal_Code__c : '');
                        generator.writeStringField('billingCity', ba.Billing_City__c != null ? ba.Billing_City__c : '');
                        generator.writeStringField('billingCountry', ba.Billing_Country__c != null ? ba.Billing_Country__c : '');
                        generator.writeStringField('baAlias', ba.Billing_Arrangement_Alias__c != null ? ba.Billing_Arrangement_Alias__c : '');
                        generator.writeEndObject(); 
                    }
                    generator.writeEndArray();
                    // No errors
                    generator.writeFieldName('errors');
                    generator.writeStartArray();
                    generator.writeEndArray();        
                    generator.writeEndObject();            
                    system.debug('### Response body: ' + generator.getAsString());               
                }
                else {
                    // No Billing Arrangements found
                    generator.writeStartObject();
                    generator.writeStringField('status', 'FAILED');        
                    generator.writeFieldName('billingArrangements');
                    generator.writeStartArray();  
                    generator.writeEndArray();                
                    // Errors
                    generator.writeFieldName('errors');
                    generator.writeStartArray();
                    generator.writeStartObject();       
                    generator.writeStringField('errorCode', 'SFEC-0002');
                    generator.writeStringField('errorMessage', 'There were no records that could be updated or found in Salesforce.');
                    generator.writeEndObject();    
                    generator.writeEndArray();        
                }
            }
            else {
                // No Billing Customer Id supplied
                generator.writeStartObject();
                generator.writeStringField('status', 'FAILED');        
                generator.writeFieldName('billingArrangements');
                generator.writeStartArray();  
                generator.writeEndArray();            
                // Errors
                generator.writeFieldName('errors');
                generator.writeStartArray();
                generator.writeStartObject();       
                generator.writeStringField('errorCode', 'SFEC-0001');
                generator.writeStringField('errorMessage', 'Missing mandatory Input field: billingCustomerId');
                generator.writeEndObject();    
                generator.writeEndArray();        
            }
        }
        catch(Exception e) {
            // Any other occuring exceptions
            generator.writeStartObject();
            generator.writeStringField('status', 'FAILED');        
            generator.writeFieldName('billingArrangements');
            generator.writeStartArray();  
            generator.writeEndArray();            
            // Errors
            generator.writeFieldName('errors');
            generator.writeStartArray();
            generator.writeStartObject();       
            generator.writeStringField('errorCode', 'SFEC-9999');
            generator.writeStringField('errorMessage', 'An unhandled exception occured: ' + e.getMessage());
            generator.writeEndObject();    
            generator.writeEndArray();  
        }
        finally {
            // Populate response
            res.addHeader('Content-Type', 'text/json');
            res.statusCode = 200;
            res.responseBody = Blob.valueOf(generator.getAsString());
        }
    }
}