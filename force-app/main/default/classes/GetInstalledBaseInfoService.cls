@RestResource(urlMapping='/getinstalledbaseinfo/*')
/**
 * @description: Retrieves the details of Installed Base Items.
 * @author: Jurgen van Westreenen
 */
global without sharing class GetInstalledBaseInfoService {
	@HttpPost
	global static void returnInstalledBaseItems() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		List<Customer_Asset__c> caToReturn = new List<Customer_Asset__c>();
		List<Error> caErrors = new List<Error>();
		String respBody = '';
		try {
			GetInstalledBaseInfo body = (GetInstalledBaseInfo) JSON.deserializeStrict(
				req.requestbody.tostring(),
				GetInstalledBaseInfo.class
			);
			List<InstalledBaseRow> ibRows = body.installedBaseRows;
			Set<String> ibSet = new Set<String>();
			for (InstalledBaseRow ibRow : ibRows) {
				ibSet.add(ibRow.installedBaseId);
			}
			List<Customer_Asset__c> customerAssets = [
				SELECT
					Id,
					Installed_Base_Id__c,
					Site__c,
					Article_Code__c,
					Charge_Description__c,
					Site__r.BOP_Site_Name__c,
					Site__r.Site_House_Number__c,
					Site__r.Site_House_Number_Suffix__c,
					Site__r.Site_Postal_Code__c,
					Price__c,
					Discount__c,
					Contract_Period__c,
					Cost_Center__c,
					PO_Number__c,
					Billing_Arrangement__c,
					Quantity__c,
					Installation_Status__c,
					Billing_Status__c,
					Product__r.Billing_Type__c,
					Billing_Arrangement__r.Financial_Account__r.BAN__r.Ban_Number__c
				FROM Customer_Asset__c
				WHERE Installed_Base_Id__c IN :ibSet
			];
			Map<String, Customer_Asset__c> caMap = new Map<String, Customer_Asset__c>();
			for (Customer_Asset__c ca : customerAssets) {
				caMap.put(ca.Installed_Base_Id__c, ca);
			}
			for (InstalledBaseRow ibr : ibRows) {
				Customer_Asset__c caItem = caMap.get(ibr.installedBaseId); // Could be null
				Boolean noSiteMatch =
					String.isNotBlank(ibr.siteId) && ibr.siteId != caItem?.Site__c;
				Boolean noAddrMatch =
					(String.isNotBlank(ibr.postalCode) &&
					ibr.postalCode != caItem?.Site__r.Site_Postal_Code__c) ||
					(String.isNotBlank(ibr.houseNumber) &&
					ibr.houseNumber != String.valueOf(caItem?.Site__r.Site_House_Number__c)) ||
					(String.isNotBlank(ibr.houseNumberSuffix) &&
					ibr.houseNumberSuffix != caItem?.Site__r.Site_House_Number_Suffix__c);
				Boolean pendingOrder =
					(caItem?.Installation_Status__c != 'Active' &&
					caItem?.Installation_Status__c != null) ||
					(caItem?.Billing_Status__c != 'Active' &&
					caItem?.Billing_Status__c != null);
				if (!caMap.containsKey(ibr.installedBaseId)) {
					caErrors.add(
						new Error(
							'SFEC-0002',
							'No records were found in Salesforce with installedBaseId: ' +
							ibr.installedBaseId
						)
					);
				} else if (noSiteMatch) {
					caErrors.add(
						new Error(
							'SFEC-0010',
							'Supplied site (' +
							ibr.siteId +
							') does not match with the value found in Salesforce: ' +
							caItem?.Site__c
						)
					);
				} else if (noAddrMatch) {
					caErrors.add(
						new Error(
							'SFEC-0010',
							'Supplied site (' +
							ibr.postalCode +
							' ' +
							ibr.houseNumber +
							' ' +
							ibr.houseNumberSuffix +
							') does not match with the value found in Salesforce: ' +
							caItem?.Site__r.Site_Postal_Code__c +
							' ' +
							String.valueOf(caItem?.Site__r.Site_House_Number__c) +
							' ' +
							caItem?.Site__r.Site_House_Number_Suffix__c
						)
					);
				} else if (pendingOrder) {
					caErrors.add(
						new Error(
							'SFEC-0011',
							'The requested installedBaseId (' +
							ibr.installedBaseId +
							') is in a pending order'
						)
					);
				} else {
					caToReturn.add(caItem);
				}
			}
			respBody = createResponse(caToReturn, caErrors);
		} catch (Exception e) {
			caErrors.add(
				new Error('SFEC-9999', 'An unhandled exception occured: ' + e.getMessage())
			);
			respBody = createResponse(caToReturn, caErrors);
		} finally {
			// Populate response
			res.addHeader('Content-Type', 'text/json');
			res.statusCode = 200;
			res.responseBody = Blob.valueOf(respBody);
		}
	}

	private static String createResponse(List<Customer_Asset__c> caToReturn, List<Error> caErrors) {
		String respStatus = caToReturn.isEmpty()
			? 'FAILED'
			: (caErrors.isEmpty() ? 'OK' : 'PARTIALLY FAILED');
		// Build JSON response body
		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeStringField('status', respStatus);
		// First the installedBaseItems
		generator.writeFieldName('installedBaseItems');
		generator.writeStartArray();
		for (Customer_Asset__c ca : caToReturn) {
			generator.writeStartObject();
			generator.writeStringField('installedBaseId', nullCheck(ca.Installed_Base_Id__c));
			generator.writeStringField('bopArticleCode', nullCheck(ca.Article_Code__c));
			generator.writeStringField('chargeDescription', nullCheck(ca.Charge_Description__c));
			generator.writeStringField('location', nullCheck(ca.Site__r?.BOP_Site_Name__c));
			generator.writeStringField('price', nullCheck(ca.Price__c));
			generator.writeStringField('discount', nullCheck(ca.Discount__c));
			generator.writeStringField('contractPeriod', nullCheck(ca.Contract_Period__c));
			generator.writeStringField('costCenter', nullCheck(ca.Cost_Center__c));
			generator.writeStringField('purchaseOrderNumber', nullCheck(ca.PO_Number__c));
			generator.writeStringField(
				'billingArrangementId',
				nullCheck(ca.Billing_Arrangement__c)
			);
			generator.writeStringField('quantity', nullCheck(ca.Quantity__c));
			generator.writeStringField('billingType', nullCheck(ca.Product__r?.Billing_Type__c));
			generator.writeStringField(
				'billingCustomerID',
				nullCheck(ca.Billing_Arrangement__r?.Financial_Account__r?.BAN__r?.Ban_Number__c)
			);
			generator.writeEndObject();
		}
		generator.writeEndArray();
		// Followed by the errors (if applicable)
		generator.writeFieldName('errors');
		generator.writeStartArray();
		for (Error caErr : caErrors) {
			generator.writeStartObject();
			generator.writeStringField('errorCode', caErr.errCode);
			generator.writeStringField('errorMessage', caErr.errMessage);
			generator.writeEndObject();
		}
		generator.writeEndArray();
		generator.writeEndObject();

		return generator.getAsString();
	}

	private static String nullCheck(Object inp) {
		if (inp != null) {
			return String.valueOf(inp);
		}
		return '';
	}

	@TestVisible
	class GetInstalledBaseInfo {
		List<InstalledBaseRow> installedBaseRows;
	}

	@TestVisible
	class InstalledBaseRow {
		@TestVisible
		String installedBaseId;
		@TestVisible
		String siteId;
		@TestVisible
		Boolean specialBilling;
		@TestVisible
		String houseNumber;
		@TestVisible
		String houseNumberSuffix;
		@TestVisible
		String postalCode;
	}

	@TestVisible
	class Error {
		@TestVisible
		String errCode;
		@TestVisible
		String errMessage;
		@TestVisible
		Error(String errCode, String errMessage) {
			this.errCode = errCode;
			this.errMessage = errMessage;
		}
	}
}