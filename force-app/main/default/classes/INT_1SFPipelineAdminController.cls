public with sharing class INT_1SFPipelineAdminController {


public List<Opportunity> unSharedOpps;
public List<OpportunityLineItem> unSharedOppProducts;

public Boolean directSharing {get;set;}

public String OpportunityID{get;set;}
public String OpportunityProductID{get;set;}

public String individualMessage {get;set;}
public String bulkIDList {get;set;}
public String bulkOutputTextMessage{get;set;}

public Integer OpportunityCount{
	get {
		Integer oppCount=0;
		if (!unSharedOpps.IsEmpty()){
			oppCount = unsharedOpps.size();
		}	
		return oppCount;
	}
	set;
}

public Integer OpportunityProductCount{
	get {
		Integer oppCount=0;
		if (!unSharedOppProducts.IsEmpty()){
			oppCount = unSharedOppProducts.size();
		}	
		return oppCount;
	}
	set;
}


public String LastRefreshDateTime{
	get {
		return DateTime.now().format('dd/MM/yyyy HH:mm:ss a');		
	}
	set;
}

// Map to store the PartnerNetworkRecordConnection Status for Logical Join to Opportunity
public Map<Id,PartnerNetworkRecordConnection> unSharedOppStatusMap {
	get{
		if (unSharedOppStatusMap==null){
			unSharedOppStatusMap = new Map<Id,PartnerNetworkRecordConnection>();
			unSharedOppStatus = [select Id, Status, LocalRecordId from PartnerNetworkRecordConnection Where Status != 'Sent' and LocalRecordId in 
			(Select Id from Opportunity Where INT_IsReadyForSharingWith__c = '1SF')];				
		
			for (PartnerNetworkRecordConnection pnrc : unSharedOppStatus){
				if (!unSharedOppStatusMap.containsKey(pnrc.LocalRecordId)){
					system.debug('PNRC ID==' + pnrc.LocalRecordId);
					unSharedOppStatusMap.put(pnrc.LocalRecordId,pnrc);
				}
			}
			
			for (Opportunity opp : UnSharedOpps){
				if (!unSharedOppStatusMap.containsKey(opp.Id)){
					unSharedOppStatusMap.put(opp.Id, new PartnerNetworkRecordConnection());
				}
			}
			
		}	
		return unSharedOppStatusMap;	
	}
	set{
		unSharedOppStatusMap=value;
	}
}


// Map to store the PartnerNetworkRecordConnection Status for Logical Join to OpportunityProduct
public Map<Id,PartnerNetworkRecordConnection> unSharedOppProdStatusMap {
	get{
		if (unSharedOppProdStatusMap==null){
			unSharedOppProdStatusMap = new Map<Id,PartnerNetworkRecordConnection>();
			unSharedOppProdStatus = [select Id, Status, LocalRecordId from PartnerNetworkRecordConnection Where Status != 'Sent' and LocalRecordId in 
			(Select Id from OpportunityLineItem Where INT_IsReadyForSharingWith__c = '1SF')];				
		
			for (PartnerNetworkRecordConnection pnrc : unSharedOppProdStatus){
				if (!unSharedOppProdStatusMap.containsKey(pnrc.LocalRecordId)){
					system.debug('PNRC ID==' + pnrc.LocalRecordId);
					unSharedOppProdStatusMap.put(pnrc.LocalRecordId,pnrc);
				}
				
			}
			
			for (OpportunityLineItem oli : UnSharedOppProducts){
				if (!unsharedOppProdStatusMap.containsKey(oli.Id)){
					unsharedOppProdStatusMap.put(oli.Id, new PartnerNetworkRecordConnection());
				}
			}
			
			
		}	
		return unSharedOppProdStatusMap;	
	}
	set{
		unSharedOppProdStatusMap=value;
	}
}

public List<PartnerNetworkRecordConnection> unSharedOppStatus;
public List<PartnerNetworkRecordConnection> unSharedOppProdStatus;

// Constructor
public INT_1SFPipelineAdminController() {
	
}



public List<Opportunity> getUnSharedOpps(){
	if (unSharedOpps==null){
		unsharedOpps = [select Id,Name,CloseDate,LastModifiedDate,Amount, LastModifiedBy.Alias,StageName,Owner.Alias,INT_IsReadyForSharingWith__c From Opportunity Where INT_IsReadyForSharingWith__c = '1SF' and 
						Id Not In (Select LocalRecordId from PartnerNetworkRecordConnection where Status = 'Sent') limit 50];
		
	} 
	return unSharedOpps;
}

public List<OpportunityLineItem> getUnSharedOppProducts(){
	if (unsharedOppProducts==null){
		unSharedOppProducts = [select Id,Product2.Name,LastModifiedDate, Opportunity.Amount,LastModifiedBy.Alias,Opportunity.CloseDate,Opportunity.Name,Opportunity.StageName,Opportunity.Owner.Alias,INT_IsReadyForSharingWith__c 
								From OpportunityLineItem Where INT_IsReadyForSharingWith__c = '1SF' and Opportunity.INT_IsReadyForSharingWith__c = '1SF' and 
							    Id Not In (Select LocalRecordId from PartnerNetworkRecordConnection where Status = 'Sent') limit 50];
	}
	return unSharedOppProducts;	
}



public PageReference ShareOpportunities(){
	if (!unsharedOpps.IsEmpty()){
		if (!this.directSharing){
			Database.update(unSharedOpps,false);
			unSharedOpps=null;
		}
		else {
			// Call the sharing logic directly not through update triggers
			INT_SF2SF_Handler sf2sfHandler = new INT_SF2SF_Handler();
			try {
				sf2sfHandler.ShareOpportunities(this.unsharedOpps);
			} catch (Exception exSharing){
				ApexPages.Message sharingMessage = new ApexPages.Message(ApexPages.Severity.ERROR,'Error sharing Opportunities - ' + exSharing);
				ApexPages.addMessage(sharingMessage);
			}
		}
	}
	return ApexPages.currentPage();
}  


public PageReference ShareOpportunityProducts(){
	if (!unSharedOppProducts.IsEmpty()){
		if (!this.directSharing){
			Database.update(unSharedOppProducts,false);
			unSharedOppProducts=null;
		}
		else {
			// Call the sharing logic directly not through update triggers
			INT_SF2SF_Handler sf2sfHandler = new INT_SF2SF_Handler();
			try {
				sf2sfHandler.ShareOpportunityProducts(this.unSharedOppProducts);
			} catch (Exception exSharing){
				ApexPages.Message sharingMessage = new ApexPages.Message(ApexPages.Severity.ERROR,'Error sharing Opportunity Products - ' + exSharing);
				ApexPages.addMessage(sharingMessage);
			}
		}
	}
	return ApexPages.currentPage();
}  

// Sometimes sharing can become stuck in status of Invite (SFDC outage etc)
// This method is used to reset these records so they can be shared again.
public PageReference ClearOutOppInvites(){
	List<PartnerNetworkRecordConnection> pnrcList = [Select Id,Status From PartnerNetworkRecordConnection where Status='Invite' and LocalRecordId in (select Id from Opportunity Where INT_IsReadyForSharingWith__c = '1SF') Limit 50];
	if (!pnrcList.IsEmpty()){
		Database.delete(pnrcList,false);
		unSharedOppProducts=null;
	}
	return ApexPages.currentPage();	
	
}

public PageReference ClearOutOppProductInvites(){
	List<PartnerNetworkRecordConnection> pnrcList = [Select Id,Status From PartnerNetworkRecordConnection where Status='Invite' and LocalRecordId in (select Id from OpportunityLineItem Where INT_IsReadyForSharingWith__c = '1SF') Limit 50];
	if (!pnrcList.IsEmpty()){
		Database.delete(pnrcList,false);
		unSharedOppProducts=null;
	}
	return ApexPages.currentPage();	
	
}



public PageReference RefreshPage(){
	this.directSharing=false;
	unSharedOppProdStatusMap=null;
	unSharedOppStatusMap=null;
	unSharedOpps=null;
	unSharedOppProducts=null;
	
	return ApexPages.currentPage();	
	
}


public PageReference shareOpp(){
	List<Opportunity> oppList = [select Id,Name,CloseDate,LastModifiedDate,Amount, LastModifiedBy.Alias,StageName,Owner.Alias,INT_IsReadyForSharingWith__c From Opportunity Where INT_IsReadyForSharingWith__c = '1SF' and id = :OpportunityID];
	if (!oppList.isEmpty()){
		if (!this.directSharing){
			Database.update(oppList,false);
			unSharedOppProducts=null;
		}
		else {
			// Call the sharing logic directly not through update triggers
			INT_SF2SF_Handler sf2sfHandler = new INT_SF2SF_Handler();
			try {
				sf2sfHandler.ShareOpportunities(oppList);
			} catch (Exception exSharing){
				ApexPages.Message sharingMessage = new ApexPages.Message(ApexPages.Severity.ERROR,'Error sharing Opportunity Products - ' + exSharing);
				ApexPages.addMessage(sharingMessage);
			}
		}
		individualMessage='Attempted sharing of record ' + OpportunityId;
		OpportunityId='';
	}	
	return ApexPages.currentPage();
	
}

public PageReference shareOppProduct(){
	List<OpportunityLineItem> oppProdList = [select Id,Product2.Name,LastModifiedDate, Opportunity.Amount,LastModifiedBy.Alias,Opportunity.CloseDate,Opportunity.Name,Opportunity.StageName,Opportunity.Owner.Alias,INT_IsReadyForSharingWith__c 
								From OpportunityLineItem Where INT_IsReadyForSharingWith__c = '1SF' and id = :OpportunityProductID];
	if (!oppProdList.isEmpty()){
		if (!this.directSharing){
			Database.update(oppProdList,false);
			unSharedOppProducts=null;
		}
		else {
			// Call the sharing logic directly not through update triggers
			INT_SF2SF_Handler sf2sfHandler = new INT_SF2SF_Handler();
			try {
				sf2sfHandler.ShareOpportunityProducts(oppProdList);
			} catch (Exception exSharing){
				ApexPages.Message sharingMessage = new ApexPages.Message(ApexPages.Severity.ERROR,'Error sharing Opportunity Products - ' + exSharing);
				ApexPages.addMessage(sharingMessage);
			}
		}
		individualMessage='Attempted sharing of record ' + OpportunityProductId;
		OpportunityProductId='';
		
	}	
	return ApexPages.currentPage();	
}

public PageReference ShareBulkIds(){
	List<SObject> sObjectList = new List<sObject>();
	String[] IDStringArray;
	bulkOutputTextMessage = '';
	if (bulkIDList!=null && bulkIDList!=''){
		IDStringArray = bulkIDList.split('\r\n');
		system.debug('IDStringArray==' + IDStringArray);
	}
	for (Integer i=0;i<IDStringArray.size();i++){
		Id idToUpdate;
		String idString;
		try {
			idString = IDStringArray[i];
			idToUpdate = IDStringArray[i];
			SObject o1 = idToUpdate.getSObjectType().newSObject(idToUpdate);
			system.debug('Sobject==' + o1);
    		sObjectList.add(o1);
		}
		catch (Exception ex){
			bulkOutputTextMessage = bulkOutputTextMessage + '<<Exception (' + ex + ') - skipping - ' + idString + '>>';
		}		
		
	}
	
	if (!sObjectList.isEmpty()){
		bulkOutputTextMessage = bulkOutputTextMessage + ' *** Attempted sharing of : ' + sObjectList.size() + ' records ***';
		Database.update(sObjectList,false);
		unSharedOpps=null;
		unSharedOppProducts=null;
		bulkIDList='';
	}

 

	return ApexPages.currentPage();	
}




public PageReference viewSharingStatus(){
	List<PartnerNetworkRecordConnection> pnrcList = [Select Id,Status,StartDate,LocalRecordId from PartnerNetworkRecordConnection Where LocalRecordId = :OpportunityId or LocalRecordId =:OpportunityProductId];
	if (!pnrcList.isEmpty()){
		individualMessage = 'Requested Status = ' + pnrcList[0].status;
		OpportunityProductId='';
		OpportunityId='';
	}
	return ApexPages.currentPage();	
}


}