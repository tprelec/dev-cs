/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
*/

@isTest()
private class INT_1SFPipelineAdminControllerTest {

	static List<Opportunity> opportunityTestList = new List<Opportunity>();
	static List<OpportunityLineItem> opportunityProductTestList = new List<OpportunityLineItem>();
    static final String OPP_OPEN = 'Qualified';

    static testMethod void testAdminPage() {
    	
    	createTestData();
    	
    	INT_1SFPipelineAdminController pipeController = new INT_1SFPipelineAdminController();
    	
    	pipeController.RefreshPage();
 		pipeController.directSharing=false;
 		pipeController.OpportunityID = opportunityTestList[0].Id;
 		pipeController.OpportunityProductID =  opportunityProductTestList[0].Id;
		
		pipeController.shareOpp();
		pipeController.shareOppProduct();

 		pipeController.ClearOutOppInvites();


 		List<Opportunity> unsharedOpps = pipeController.getUnSharedOpps();
 		List<OpportunityLineItem> unsharedOppProds = pipeController.getUnSharedOppProducts();

 		pipeController.unSharedOpps = opportunityTestList;
 		pipeController.unSharedOppProducts = opportunityProductTestList;

 		system.debug(pipeController.unSharedOppStatusMap);
 		system.debug(pipeController.unSharedOppProdStatusMap);
 		system.debug(pipeController.LastRefreshDateTime);     
 		Integer unsharedOppCount = pipeController.OpportunityCount;
 		Integer unsharedOppProdCount = pipeController.OpportunityProductCount;
 		
 		pipeController.ShareOpportunities();
 		pipeController.ShareOpportunityProducts();

 		pipeController.directSharing=true;
 		     
 		pipeController.unSharedOpps = opportunityTestList;
 		pipeController.unSharedOppProducts = opportunityProductTestList;

 		pipeController.ShareOpportunities();
 		pipeController.ShareOpportunityProducts();
 		
 		pipeController.ClearOutOppInvites();
 		pipeController.ClearOutOppProductInvites();
 		pipeController.viewSharingStatus();

		pipeController.shareOpp();
		pipeController.shareOppProduct();

 		pipeController.bulkIDList = 'zzzzz' + '\r\n' + opportunityProductTestList[0].Id + '\r\n' + opportunityTestList[0].Id;
 		pipeController.ShareBulkIds();



    }
    
    static void createTestData(){

        OrderType__c ot = TestUtils.createOrderType();

    	Account testAC1 = new Account(Name='Test Account1');insert testAC1;
        Account testAC2 = new Account(Name='Test Account2');insert testAC2;
        
		Opportunity g1 = new Opportunity (AccountId = testAC1.Id,
                        Name = 'test',
                        Description = 'test',
                        StageName = 'Identification',
                        //Duration_months__c = 12,
                        //Decision_Date__c = System.today(),
                        CloseDate =  System.today().addDays(1),
                        //Execution_Start_Date__c = System.today(),
                        INT_IsReadyForSharingWith__c = '1SF'
                        //Execution_Completed_By__c= System.today().addDays(2)
                         );
        		

		        Opportunity o1 = new Opportunity (Name = 'Opp1', 
		                                        AccountId = testAC1.Id, 
		                                        StageName = OPP_OPEN,
		                                        CloseDate = System.today().addDays(1),
		                                        INT_IsReadyForSharingWith__c = '1SF');
		        Opportunity o2 = new Opportunity (Name = 'Opp2', 
		                                        AccountId = testAC1.Id, 
		                                        StageName = OPP_OPEN,
		                                        CloseDate = System.today().addDays(1), 
		                                        INT_IsReadyForSharingWith__c = '1SF');
		        Opportunity o3 = new Opportunity (Name = 'Opp3', 
		                                        AccountId = testAC1.Id, 
		                                        StageName = OPP_OPEN,  
		                                        CloseDate = System.today().addDays(1), 
		                                        INT_IsReadyForSharingWith__c = '1SF');
		        
				opportunityTestList.add(g1);
				opportunityTestList.add(o1);
				opportunityTestList.add(o2);
				opportunityTestList.add(o3);

		        insert opportunityTestList;
		        
				Product2 testProd = new Product2(Name='Test Product',INT_IsReadyForSharingWith__c = '1SF',Product_Line__c='fVodafone',Ordertype__c=ot.id);
				insert testProd;

				Pricebook2 standardPriceBook;

				/*try {
					standardPriceBook = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
					if (!standardPriceBook.isActive) {
		            	standardPriceBook.isActive = true;
		            	update standardPriceBook;
		        	}
				}
				catch (Exception ex){
					system.debug('standardPriceBook exception ==' + ex);
				}*/
				Id standardPriceBookId = Test.getStandardPricebookId();
				
				/*
        		Pricebook2 standardPriceBook = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
        
		        if (!standardPriceBook.isActive) {
		            standardPriceBook.isActive = true;
		            update standardPriceBook;
		        }
				*/

		        Pricebook2 testPriceBook = new Pricebook2(IsActive=true,Name='Test PB'); insert testPriceBook;
		
		        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId,IsActive=true,Product2Id=testProd.Id,UnitPrice=1);insert pbe;
		        
		        List<Product2> productList = new List<Product2>();
		        productList.add(testProd);
		
		        OpportunityLineItem oppProduct = new OpportunityLineItem();
		        oppProduct.PricebookEntryId = pbe.Id;
		        oppProduct.OpportunityId = o1.Id;
		        oppProduct.Quantity= 100;
		        oppProduct.TotalPrice = 11111;
		        oppProduct.Product_Arpu_Value__c=0;
		        //oppProduct.Connections_Resign__c = 10.32;
		        oppProduct.INT_IsReadyForSharingWith__c = '1SF';
		       // oppProduct.Product_Arpu_Value__c= 10.0;
		       oppProduct.CLC__c = 'Acq';
		        
		        opportunityProductTestList.add(oppProduct);
		        insert opportunityProductTestList;		        
    	
    } 
    
    
    
    
}