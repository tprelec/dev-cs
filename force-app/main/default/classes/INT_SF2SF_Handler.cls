public class INT_SF2SF_Handler {

    private String connectionKey='';

    // Default constructor
    public INT_SF2SF_Handler() {
    
    }
    
    public static boolean HasExecutedSharedOpportunities = false;
    public static boolean HasExecutedSharedOpportunityProducts=false;
    public static boolean ChangeOfCurrencyWithProducts = false;
    
    
    public static boolean HasExecutedSharedProducts = false;
    
    
    public void ForceOppProductUpdateForSharing(List<Opportunity> oppList,Map<Id,Opportunity> oppOldMap){
    	
    	List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
    	
    	List<Id> oppIdList = new List<Id>();
    	
    	for (Opportunity oppItem : oppList){
    		Opportunity oldOpp = oppOldMap.get(oppItem.Id);
    		// Has the Opportunity Changed ?
   			oppIdList.add(oppItem.Id);		
    	}
		
		oppLineItems = [Select Id From OpportunityLineItem where OpportunityId in : oppIdList];
		
		try {
			if (!oppLineItems.isEmpty()){
                ShareOpportunityProducts(oppLineItems);
			}
		}   
		catch (Exception ex){
			system.debug('Failed to Update OppProducts--' + ex);
		} 	
    	
    }
    
    
    
    public void ShareAccountOpportunities(List<Account> newAccounts, Map<Id, Account> mapOldAccounts) {
    
    	connectionKey = '1SF';
        system.debug('SF2SF Connection Key == ' + connectionKey);
     	INT_SF2SF_Connection_Config__c config = INT_SF2SF_Utilities.getConnectionConfigByValue(connectionKey);
     	
     	// Only run logic if its enabled
     	if (!config.Account_Opportunity_Sharing_Enabled__c){
     		return;
     	}
    
    
    	//Set of Account Ids to process
    	Set<Id> accountIds = new Set<Id>();
    	
    	for(Account newAccount: newAccounts) {
    		if (newAccount.INT_UniqueAccount_ID__c !=null && mapOldAccounts.get(newAccount.Id).INT_UniqueAccount_ID__c != newAccount.INT_UniqueAccount_ID__c) {
    			accountIds.add(newAccount.Id);  
    		}	
    	}
    	
    	if (accountIds.size()> 0 ) {
	    	List<Opportunity> listOppsToUpdate = new List<Opportunity>();
	    	List<OpportunityLineItem> listOppProductsToUpdate = new List<OpportunityLineItem>();
	    	 
	    	//Get Opportunity and Opp Products based on Account Ids
	    	List<Opportunity> listOpportunities = new List<Opportunity>([Select Id, (Select id, OpportunityId From OpportunityLineItems) From Opportunity where accountid in :accountIds limit 10000]);  
	    	
	    	for (Opportunity opp:listOpportunities ) {
	    			listOppsToUpdate.add(opp);
	    			if (opp.OpportunityLineItems != null && opp.OpportunityLineItems.size() > 0) {
	    				listOppProductsToUpdate.addAll(opp.OpportunityLineItems);
	    			}
	    	}
			
			//update Opportunities - invoke the 1SF Sharing Workflows
			update listOppsToUpdate;
			
			//update Opportunity products - invoke the 1SF Sharing Workflows
			update listOppProductsToUpdate;	    	
    	}
    	
    }
    
    public void ShareOpportunities(List<Opportunity> newOpportunities) {
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity o : newOpportunities){
            oppIds.add(o.Id);
        }
        ShareOpportunitiesOffline(oppIds);
    }


    @future
    public static void ShareOpportunitiesOffline(Set<Id> oppIds){
        if(!system.isBatch() && !system.isScheduled()) ShareOpportunitiesOnline(oppIds);
    }
    public static void ShareOpportunitiesOnline(Set<Id> oppIds){            
        String connectionKey = '';
        
        //List<Opportunity> newOpportunities = (List<Opportunity>) JSON.deserialize(newOpportunitiesJSONString, List<Opportunity>.class);
        List<Opportunity> newOpportunities = [Select Id, INT_IsReadyForSharingWith__c From Opportunity Where Id in: oppIds];
        
        if (!newOpportunities.isEmpty() && newOpportunities[0].INT_IsReadyForSharingWith__c!=null /*&& !system.isBatch() && !system.isScheduled()*/){
            connectionKey = newOpportunities[0].INT_IsReadyForSharingWith__c;
            system.debug('SF2SF Connection Key == ' + connectionKey);
        }
        else {
            // Error not ready for sharing!!
            return;
        }
        
        Set<Id> recordIDs = new Set<ID>();
        for (Opportunity  newOpportunity  : newOpportunities) {
            recordIDs.add(newOpportunity.ID);   
        }
        
        
        List<PartnerNetworkRecordConnection> OpportunityConnections =  new  List<PartnerNetworkRecordConnection>();
        
        System.debug('----------> ConnectionKey  '  + connectionKey);
        INT_SF2SF_Connection_Config__c config = INT_SF2SF_Utilities.getConnectionConfigByValue(connectionKey);
        Boolean IsSharingEnabled = false;
        String relatedRecords = '';
        Integer retryDelaySeconds = 0;
        
        
        
        System.debug('--------> ' + config.Opportunity_Sharing_Enabled__c);
        System.debug('--------> config ' + config);
        
        if (config != null) {
            IsSharingEnabled=config.Opportunity_Sharing_Enabled__c;
            relatedRecords = config.Opportunity_Related_Records__c;
            retryDelaySeconds = Integer.valueOf(config.Retry_Delay__c);
            if(UserInfo.getName().equalsIgnoreCase(config.Excluded_User_Names__c)){
            	// Don't run for this user
            	return;
            }
        }
        
        System.debug('=========> ' + IsSharingEnabled);
        if (!IsSharingEnabled){
            // Exit Method, Sharing Not Enabled
            return;
        }       
        
        
        ID connectionID = INT_SF2SF_Utilities.getConnection(connectionKey).Id;

        List<PartnerNetworkRecordConnection> partnerNetworkRecList = [Select LocalRecordId,Status, StartDate, PartnerRecordId, ParentRecordId,  ConnectionId From PartnerNetworkRecordConnection Where (status = 'Sent' or status = 'Invite') And LocalRecordId In: recordIDs and ConnectionId = :connectionID];
        List<PartnerNetworkRecordConnection> pnrcDelList = new List<PartnerNetworkRecordConnection>();

        Map<Id,PartnerNetworkRecordConnection> partnerNetworkRec = new Map<Id,PartnerNetworkRecordConnection>();

        
        for (PartnerNetworkRecordConnection pnrc : partnerNetworkRecList){
			if (pnrc.Status=='Invite' && pnrc.StartDate!=null && pnrc.StartDate.addSeconds(retryDelaySeconds) < DateTime.now()){
				pnrcDelList.add(pnrc);
			}
			else
			if (!partnerNetworkRec.containsKey(pnrc.LocalRecordId)){
                partnerNetworkRec.put(pnrc.LocalRecordId,pnrc);
            }
        }
        
		if (!pnrcDelList.IsEmpty()){
			try {
				system.debug('#### pnrcDelList ' + pnrcDelList);	
				delete pnrcDelList;
			}
			catch (Exception ex){
				system.debug('#### pnrcDelList Deletion exception' + ex);
			}
 		}

        for (Opportunity  oppItem: newOpportunities) {
            
            if (oppItem.INT_IsReadyForSharingWith__c != null && oppItem.INT_IsReadyForSharingWith__c != '' && !partnerNetworkRec.containsKey(oppItem.Id)) {
                
                PartnerNetworkRecordConnection newConnection =
                    new PartnerNetworkRecordConnection(
                        //ParentRecordId = oppItem.AccountId,
                        ConnectionId = connectionID,
                        RelatedRecords = relatedRecords,
                        LocalRecordId = oppItem.Id,
                        SendClosedTasks = false,
                        SendOpenTasks = false,
                        SendEmails = false);
                
                System.debug('INT Sharing Opportunity -- ' + newConnection);
                OpportunityConnections.add(newConnection);
            }
        }
        
        if (OpportunityConnections.size() > 0 ) {
            try {
                system.debug(LoggingLevel.ERROR, '### Before inserting partner record...');
                database.insert(OpportunityConnections);
                //insert OpportunityConnections;
                system.debug(LoggingLevel.ERROR, '### After inserting partner record!');
            }
            catch (Exception ex){
                system.debug('Exception Sharing Opportunity Raised -- ' + ex);
            }
        }
    }
   

    public void ShareOpportunityProducts(List<OpportunityLineItem> newOpportunityProducts){
        Set<Id> oppProdIds = new Set<Id>();
        for(OpportunityLineItem oli : newOpportunityProducts){
            oppProdIds.add(oli.Id);
        }
        ShareOpportunityProductsOffline(oppProdIds);
    }


    @future
    public static void ShareOpportunityProductsOffline(Set<Id> newOpportunityProductIds){
        if(!system.isBatch() && !system.isScheduled()) ShareOpportunitiesOnline(newOpportunityProductIds);
    }
    public static void ShareOpportunityProductsOnline(Set<Id> newOpportunityProductIds){              
        String connectionKey = '';

        //List<OpportunityLineItem> newOpportunityProducts = (List<OpportunityLineItem>) JSON.deserialize(newOpportunityProductsJSONString, List<OpportunityLineItem>.class);
        List<OpportunityLineItem> newOpportunityProducts = [Select Id, INT_IsReadyForSharingWith__c, OpportunityId From OpportunityLineItem Where Id in :newOpportunityProductIds];

        System.debug(Logginglevel.ERROR,'### newOpportunityProducts==' + newOpportunityProducts);
        
        if (!newOpportunityProducts.isEmpty() && newOpportunityProducts[0].INT_IsReadyForSharingWith__c!=null /*&& !system.isBatch() && !system.isScheduled()*/){
            connectionKey = newOpportunityProducts[0].INT_IsReadyForSharingWith__c;
            System.debug(Logginglevel.ERROR,'SF2SF Connection Key For Opp Products == ' + connectionKey);
        }
        else {
            // Error not ready for sharing!!
            System.debug(Logginglevel.ERROR,'### Received Opp Products == ' + newOpportunityProducts);
            return;
        }
        
        List<PartnerNetworkRecordConnection> opportunityLineItemConnections =  new  List<PartnerNetworkRecordConnection>();
        
        Set<Id> recordIDs = new Set<ID>();
        for (OpportunityLineItem  newOpportunityLineItem  : newOpportunityProducts) {
            recordIDs.add(newOpportunityLineItem.ID);   
        }
        
        Map<Id, Id> parentOppIds = new Map<Id, Id>();
        
        INT_SF2SF_Connection_Config__c config = INT_SF2SF_Utilities.getConnectionConfigByValue(connectionKey);
        
        if(UserInfo.getName().equalsIgnoreCase(config.Excluded_User_Names__c)){
        // Don't run for this user
			return;
        }
        
		ID connectionID = INT_SF2SF_Utilities.getConnection(connectionKey).Id;        
        
        List<PartnerNetworkRecordConnection> partnerNetworkRecList = [Select LocalRecordId,Status, StartDate, PartnerRecordId, ParentRecordId,  ConnectionId From PartnerNetworkRecordConnection Where (status = 'Sent' or status = 'Invite') And LocalRecordId In: recordIDs and ConnectionId = :connectionID];
        
        Map<Id,PartnerNetworkRecordConnection> partnerNetworkRec = new Map<Id,PartnerNetworkRecordConnection>();
        
        List<PartnerNetworkRecordConnection> pnrcDelList = new List<PartnerNetworkRecordConnection>();
        
        for (PartnerNetworkRecordConnection pnrc : partnerNetworkRecList){
			if (pnrc.Status=='Invite'){
				pnrcDelList.add(pnrc);
			}
			else
			if (!partnerNetworkRec.containsKey(pnrc.LocalRecordId)){
                partnerNetworkRec.put(pnrc.LocalRecordId,pnrc);
            }
        }
        
		if (!pnrcDelList.IsEmpty()){
			try {
				system.debug('#### pnrcDelList ' + pnrcDelList);	
				delete pnrcDelList;
			}
			catch (Exception ex){
				system.debug('#### pnrcDelList Deletion exception' + ex);
			}
			
 		}



        OpportunityLineItem errOLI = new OpportunityLineItem();
        
        for (OpportunityLineItem  oli: newOpportunityProducts) {
            
            System.debug(Logginglevel.ERROR,'### oli.INT_IsReadyForSharingWith__c: ' + oli.INT_IsReadyForSharingWith__c);
           
            if (oli.INT_IsReadyForSharingWith__c != null && oli.INT_IsReadyForSharingWith__c != '' && !partnerNetworkRec.containsKey(oli.Id)) {
                
                System.debug(Logginglevel.ERROR,'### INSIDE IF');
                PartnerNetworkRecordConnection newConnection =
                new PartnerNetworkRecordConnection(
                    ParentRecordId = oli.OpportunityId,
                    ConnectionId = connectionID,
                    LocalRecordId = oli.Id,
                    SendClosedTasks = false,
                    SendOpenTasks = false,
                    SendEmails = false);
                
                errOLI=oli;
                
                //oli.addError('INT Sharing Opportunity Line Item -- ' + newConnection);
                System.debug(Logginglevel.ERROR,'INT Sharing Opportunity Line Item -- ' + newConnection);
                opportunityLineItemConnections.add(newConnection);
            }
        }
       
        System.debug(Logginglevel.ERROR,'### BEFORE IF');
        if (opportunityLineItemConnections.size() > 0 ) {
            try {
                System.debug('### INSIDE IF 2');
                insert opportunityLineItemConnections;
            }
            catch (Exception ex){
               //errOLI.addError('INT Exception adding OpportunityLineItemConnection [connection,exception] --> [' + opportunityLineItemConnections + ',' + ex + ']');
               System.debug(Logginglevel.ERROR,'INT Exception adding OpportunityLineItemConnection [connection,exception] --> [' + opportunityLineItemConnections + ',' + ex + ']');
            }
        }

    }
    
    /*
    
    public void ShareAccounts(List<Account> newAccounts){
        
        List<PartnerNetworkRecordConnection> AccountConnections =  new  List<PartnerNetworkRecordConnection>();
        INT_SF2SF_ConnectionList__c config = INT_SF2SF_Utilities.getConnectionConfigByValue('1SF');
        Boolean IsSharingEnabled = false;
        String relatedRecords = '';
        
        
        ID connectionID;
        
        try {
            connectionID = INT_SF2SF_Utilities.getConnection('1SF').Id;         
        }
        catch (Exception ex){
            system.debug('Exception.... ' + ex);
        }

        
        if (config!=null){
            IsSharingEnabled=config.IsAccountSharingEnabled__c; 
            relatedRecords = config.Account_Related_Records__c;
        }

        if (!IsSharingEnabled){
            // Exit Method, Sharing Not Enabled
            return;
        }
        
        for (Account  account: newAccounts) {
            system.debug('account.INT_IsReadyForSharingWith__c==' + account.INT_IsReadyForSharingWith__c);
            if (account.INT_IsReadyForSharingWith__c != null && account.INT_IsReadyForSharingWith__c != '' && account.ConnectionSentId == null) {
                PartnerNetworkRecordConnection newConnection =
                new PartnerNetworkRecordConnection(
                    ConnectionId = connectionID,
                    RelatedRecords = relatedRecords, //'Opportunity,Contact,Competitor_Asset__c,VF_Contract__c',
                    LocalRecordId = Account.Id,
                    SendClosedTasks = true,
                    SendOpenTasks = false,
                    SendEmails = false);
                
                
                System.debug('INT Sharing Account -- ' + newConnection);
                AccountConnections.add(newConnection);
            }
        }
        
        
        if (AccountConnections.size() > 0 ) {
            try {
                database.insert(AccountConnections);
            }
            catch (Exception ex){
                system.debug('INT Exception sharing account --> ' + ex);
            }
        }
    
    }
    
    
    public void ShareContacts(List<Contact> newContacts){
        
        List<PartnerNetworkRecordConnection> contactConnections =  new  List<PartnerNetworkRecordConnection>();
        

        INT_SF2SF_ConnectionList__c config = INT_SF2SF_Utilities.getConnectionConfigByValue('1SF');
        Boolean IsSharingEnabled = false;
        String relatedRecords = '';
        
        if (config!=null){
            IsSharingEnabled=config.Opportunity_Sharing_Enabled__c; 
        }
        

        if (!IsSharingEnabled){
            // Exit Method, Sharing Not Enabled
            return;
        }       




        Set<Id> recordIDs = new Set<ID>();
        
        for (Contact newContact : newContacts) {
            recordIDs.add(newContact.ID);   
        }
        
        List<Contact> fullContactList = new List<Contact>([SELECT Id, ConnectionSentId, ConnectionReceivedId, Account.INT_IsReadyForSharingWith__c, AccountId FROM Contact WHERE Id In :recordIDs]);
        
        ID connectionID = INT_SF2SF_Utilities.getConnection('1SF').Id;
        
        
        for (Contact  contactItem: fullContactList) {
            
            if (contactItem.Account.INT_IsReadyForSharingWith__c != null && contactItem.Account.INT_IsReadyForSharingWith__c != '' && contactItem.ConnectionSentId == null && contactItem.ConnectionReceivedId==null) {
                
                
                PartnerNetworkRecordConnection newConnection =
                new PartnerNetworkRecordConnection(
                    ParentRecordId = contactItem.AccountId,
                    ConnectionId = connectionID,
                    LocalRecordId = contactItem.Id,
                    SendClosedTasks = true,
                    SendOpenTasks = false,
                    SendEmails = false);
                
                
                System.debug('INT Sharing Contact -- ' + newConnection);
                contactConnections.add(newConnection);
            }
        }
        
        
        if (contactConnections.size() > 0 ) {
            try {
                database.insert(contactConnections);
            }
            catch (Exception ex){
                system.debug('Exception Sharing Contact Raised -- ' + ex)   ;
            }
        }
    
    }
    
    
    */
    
    
    public void ShareProducts(List<Product2> newProducts){
        
        List<PartnerNetworkRecordConnection> productConnections =  new  List<PartnerNetworkRecordConnection>();
        

        
        if (!newProducts.isEmpty() && newProducts[0].INT_IsReadyForSharingWith__c!=null && !system.isBatch() && !system.isScheduled()){
            connectionKey = newProducts[0].INT_IsReadyForSharingWith__c;
            system.debug('SF2SF Connection Key == ' + connectionKey);
        }
        else {
            // Error not ready for sharing!!
            return;
        }
        
        Set<Id> recordIDs = new Set<ID>();
        for (Product2  newProduct  : newProducts) {
            recordIDs.add(newProduct.ID);   
        }
        
        
        INT_SF2SF_Connection_Config__c config = INT_SF2SF_Utilities.getConnectionConfigByValue(connectionKey);

        if(UserInfo.getName().equalsIgnoreCase(config.Excluded_User_Names__c)){
        // Don't run for this user
			return;
        }

        ID connectionID = INT_SF2SF_Utilities.getConnection(connectionKey).Id;

        List<PartnerNetworkRecordConnection> partnerNetworkRecList = [Select LocalRecordId,Status, StartDate, PartnerRecordId, ParentRecordId,  ConnectionId From PartnerNetworkRecordConnection Where status = 'Sent' And LocalRecordId In: recordIDs And ConnectionId = :connectionID];
        
        Map<Id,PartnerNetworkRecordConnection> partnerNetworkRec = new Map<Id,PartnerNetworkRecordConnection>();
        
        for (PartnerNetworkRecordConnection pnrc : partnerNetworkRecList){
            if (!partnerNetworkRec.containsKey(pnrc.LocalRecordId)){
                partnerNetworkRec.put(pnrc.LocalRecordId,pnrc);
            }
        }
        
        Boolean IsSharingEnabled = false;
        String relatedRecords = '';
        
        for (Product2  product: newProducts) {
            
            if (!partnerNetworkRec.containsKey(product.Id)) {
                
                
                PartnerNetworkRecordConnection newConnection =
                new PartnerNetworkRecordConnection(
                    ConnectionId = connectionID,
                    LocalRecordId = product.Id,
                    SendClosedTasks = false,
                    SendOpenTasks = false,
                    SendEmails = false);
                
                
                System.debug('INT Sharing Product -- ' + newConnection);
                productConnections.add(newConnection);
            }
        }
   
        
        if (productConnections.size() > 0 ) {
            try {
                database.insert(productConnections);
            }
            catch (Exception ex){
                system.debug('Exception Sharing Product Raised -- ' + ex)   ;
            }
        }
        
    
    }
    
    /*
    
    
    public void ShareTasks(List<Task> newTasks){
        
        List<PartnerNetworkRecordConnection> taskConnections =  new  List<PartnerNetworkRecordConnection>();
        
        Set<Id> parentIDsSet = new Set<Id>();
        
        ID parentID = null;
        
        // Loop and Fetch all Parent IDs for Querying PartnerNetworkConnectionList.
        for (Task  taskItem: newTasks) {
            if (taskItem.WhatId!=null){
                parentID=taskItem.WhatId;
            }
            else {
                parentID=taskItem.WhoId;
            }
            parentIDsSet.add(parentID);
            
        }
        
        ID connectionID = INT_SF2SF_Utilities.getConnection('1SF').Id;

        List<PartnerNetworkRecordConnection> parentConnectionsList = new List<PartnerNetworkRecordConnection>([Select Status, LocalRecordId, Id From PartnerNetworkRecordConnection Where LocalRecordId in : parentIDsSet and (Status='Sent' OR Status='Received')]);
        
        // Only Try and Share if Parent Object Has Already been shared
        Map<Id,PartnerNetworkRecordConnection> parentConnectionMap = new Map<Id,PartnerNetworkRecordConnection>();
        
        for (PartnerNetworkRecordConnection connectionItem : parentConnectionsList){
            if(!parentConnectionMap.containsKey(connectionItem.LocalRecordId)){
                parentConnectionMap.put(connectionItem.LocalRecordId,connectionItem);
            }
        }
        
        for (Task  taskItem: newTasks) {
                
            if ((taskItem.Status == 'Completed' && taskItem.ConnectionSentId == null && parentConnectionMap.containsKey(parentID)) || Test.isRunningTest()) {
                
                if (taskItem.WhatId!=null){
                    parentID=taskItem.WhatId;
                }
                else {
                    parentID=taskItem.WhoId;
                }
                
                PartnerNetworkRecordConnection newConnection =
                new PartnerNetworkRecordConnection(
                    ParentRecordId = parentID,
                    ConnectionId = connectionID,
                    LocalRecordId = taskItem.Id,
                    SendEmails = false);
                
                
                System.debug('INT Sharing Tasks -- ' + newConnection);
                taskConnections.add(newConnection);
            }
        }
        
        
        if (taskConnections.size() > 0 ) {
            try {
                database.insert(taskConnections);
            }
            catch (Exception ex){
                system.debug('Exception Sharing Tasks Raised -- ' + ex) ;
            }
        }
        
    
    }
    
    public void ShareAttachments(List<Attachment> newAttachments){
        
        List<PartnerNetworkRecordConnection> attachmentConnections =  new  List<PartnerNetworkRecordConnection>();
        
        Set<Id> recordIDs = new Set<ID>();
        Set<Id> parentIDsSet = new Set<Id>();
        
        for (Attachment  newAttachment  : newAttachments) {
            recordIDs.add(newAttachment.ID);
            parentIDsSet.add(newAttachment.parentId);   
        }

        List<PartnerNetworkRecordConnection> parentConnectionsList = new List<PartnerNetworkRecordConnection>([Select Status, LocalRecordId, Id From PartnerNetworkRecordConnection Where LocalRecordId in : parentIDsSet and (Status='Sent' OR Status='Received')]);
        
        // Only Try and Share if Parent Object Has Already been shared
        Map<Id,PartnerNetworkRecordConnection> parentConnectionMap = new Map<Id,PartnerNetworkRecordConnection>();
        
        for (PartnerNetworkRecordConnection connectionItem : parentConnectionsList){
            if(!parentConnectionMap.containsKey(connectionItem.LocalRecordId)){
                parentConnectionMap.put(connectionItem.LocalRecordId,connectionItem);
            }
        }
        
        
        List<Attachment> fullAttachmentList = new List<Attachment>([Select ParentId, Name, Id, ConnectionSentId, ConnectionReceivedId From Attachment Where ID In :recordIDs]);

        ID connectionID = INT_SF2SF_Utilities.getConnection('1SF').Id;      
        
        for (Attachment  attachment: fullAttachmentList) {
            
            if (attachment.ConnectionSentId == null && parentConnectionMap.containsKey(attachment.parentID) || Test.isRunningTest()) {
                
                PartnerNetworkRecordConnection newConnection =
                new PartnerNetworkRecordConnection(
                    ParentRecordId = attachment.ParentId,
                    ConnectionId = connectionID,
                    LocalRecordId = attachment.Id,
                    SendClosedTasks = false,
                    SendOpenTasks = false,
                    SendEmails = false);
                
                
                System.debug('INT Sharing Attachments -- ' + newConnection);
                attachmentConnections.add(newConnection);
            }
        }
        
        
        if (attachmentConnections.size() > 0 ) {
            try {
                database.insert(attachmentConnections);
            }
            catch (Exception ex){
                system.debug('Exception Sharing Attachments Raised -- ' + ex)   ;
            }
        }
    
    }*/
}