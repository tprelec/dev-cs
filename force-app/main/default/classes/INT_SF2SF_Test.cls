@isTest
private class INT_SF2SF_Test {
    
    static final String OPP_OPEN = 'Qualified';
    
    static testMethod void test_INT_SF2SF() {
        Account a1 = new Account (Name = 'Client1');
        insert a1;
        
        Opportunity g1 = new Opportunity (AccountId = a1.Id,
                        Name = 'test',
                        Description = 'test',
                        StageName = 'Identification',
                        //Duration_months__c = 12,
                        //Decision_Date__c = System.today(),
                        CloseDate =  System.today().addDays(1),
                        //Execution_Start_Date__c = System.today(),
                        INT_IsReadyForSharingWith__c = '1SF'
                        //Execution_Completed_By__c= System.today().addDays(2)
                         );
        insert g1;

        Opportunity o1 = new Opportunity (Name = 'Opp1', 
                                        AccountId = a1.Id, 
                                        StageName = OPP_OPEN,
                                        CloseDate = System.today().addDays(1),
                                        INT_IsReadyForSharingWith__c = '1SF');
        Opportunity o2 = new Opportunity (Name = 'Opp2', 
                                        AccountId = a1.Id, 
                                        StageName = OPP_OPEN, 
                                        CloseDate = System.today().addDays(1), 
                                        INT_IsReadyForSharingWith__c = '1SF');
        Opportunity o3 = new Opportunity (Name = 'Opp3', 
                                        AccountId = a1.Id, 
                                        StageName = OPP_OPEN,  
                                        CloseDate = System.today().addDays(1), 
                                        INT_IsReadyForSharingWith__c = '1SF');
        Opportunity[] ops = new Opportunity[]{ o1,o2,o3};
        insert ops;
        
    }
    
    /*private static void createBaseData(){
        INT_SF2SF_Connection_Config__c config = new INT_SF2SF_Connection_Config__c();
        config.Name = '1SFTest';
        config.SF2SF_Connection_Name__c = '1SF';
        config.Opportunity_Sharing_Enabled__c = true;
        config.Contact_Sharing_Enabled__c = true;
        config.Account_Sharing_Enabled__c = true;
        insert config;
        
    } */



    private static INT_SF2SF_Handler sf2sf{
        get{
            if (sf2sf==null){
                //createBaseData();
                sf2sf = new INT_SF2SF_Handler();
                
            }
            return sf2sf;
        }
        
        set;}
    


   @isTest()
   static void ShareAccounts() {
        
        //createBaseData();
        //system.debug('sf2sf==' + sf2sf);
        OrderType__c ot = TestUtils.createOrderType();
        
        Account testAC1 = new Account(Name='Test Account1');insert testAC1;
        Account testAC2 = new Account(Name='Test Account2');insert testAC2;
        
        Contact testContact1 = new Contact(FirstName='Test',LastName='User',Email='Test@Email.com', phone= '+44 777777777', AccountId=testAC1.Id);insert testContact1;
        Contact testContact2 = new Contact(FirstName='Test',LastName='User2',Email='Test2@Email.com', phone= '+44 777777777', AccountId=testAC2.Id);insert testContact2;
                
        Opportunity testOpp1 = new Opportunity(Name='Test Opp 1',AccountId=testAC1.Id,StageName='Closing',INT_IsReadyForSharingWith__c = '1SF',CloseDate=Date.Today());insert testOpp1;
        Opportunity testOpp2 = new Opportunity(Name='Test Opp 2',AccountId=testAC2.Id,StageName='Closing', CloseDate=Date.Today());insert testOpp2;
        
        Product2 testProd = new Product2(Name='Test Product',INT_IsReadyForSharingWith__c = '1SF',Product_Line__c='fVodafone',Ordertype__c=ot.id);
        insert testProd;
        
        /*Pricebook2 standardPriceBook = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
        
        if (!standardPriceBook.isActive) {
            standardPriceBook.isActive = true;
            update standardPriceBook;
        }*/
        Id standardPriceBookId = Test.getStandardPricebookId();

        Pricebook2 testPriceBook = new Pricebook2(IsActive=true,Name='Test PB'); insert testPriceBook;

        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId,IsActive=true,Product2Id=testProd.Id,UnitPrice=1);insert pbe;
        
        //Competitor_Asset__c testCompetitorAsset1 = new Competitor_Asset__c(Account__c = testAC1.Id,Contract_Expiration_date__c=Date.Today(),Invoice_value_per_month__c=100,PBX_type__c = testPBX.Id,Site__c = testSite1.Id); insert testCompetitorAsset1;
        //Competitor_Asset__c testCompetitorAsset2 = new Competitor_Asset__c(Account__c = testAC2.Id,Contract_Expiration_date__c=Date.Today(),Invoice_value_per_month__c=100); insert testCompetitorAsset2;
        
        
        
        List<Product2> productList = new List<Product2>();
        productList.add(testProd);

        OpportunityLineItem oppProduct = new OpportunityLineItem();
        oppProduct.PricebookEntryId = pbe.Id;
        oppProduct.OpportunityId = testOpp1.Id;
        oppProduct.Quantity= 100;
        oppProduct.TotalPrice = 11111;
        //oppProduct.Connections_Resign__c = 10.32;
        oppProduct.INT_IsReadyForSharingWith__c = '1SF';
        oppProduct.Product_Arpu_Value__c= 10.0;
        oppProduct.CLC__c = 'Acq';
        insert oppProduct;
        
        /*                      
        VF_Contract__c testContract = new VF_Contract__c();
        testContract.Account__c = testAC1.Id;
        testContract.Opportunity__c = testOpp1.Id;
        insert testContract;
        
        Contract_Attachment__c testContractAttachment = new Contract_Attachment__c();
        testContractAttachment.Contract_VF__c = testContract.Id;
        testContractAttachment.Attachment_Type__c = 'New Attach';
        insert testContractAttachment;
        */
        
        Task testTask1 = new Task();
        testTask1.WhatId = testAC1.Id;
        testTask1.ActivityDate = Date.Today();
        testTask1.Status = 'Completed';
        insert testTask1;
        
        Task testTask2 = new Task();
        testTask2.WhoId = testAC1.CreatedById;
        testTask2.ActivityDate = Date.Today();
        testTask2.Status = 'Completed';
        insert testTask2;

        Attachment testAttachment = new Attachment();
        testAttachment.Body = Blob.valueof('1234');
        testAttachment.Name = 'Test Attach';
        testAttachment.ParentId = testAC1.Id;
        insert testAttachment;
        
        /*
        List<VF_Contract__c> contractList = new List<VF_Contract__c>();
        contractList.add(testContract);
        
        List<Contract_Attachment__c> contractAttachmentList = new List<Contract_Attachment__c>();
        contractAttachmentList.add(testContractAttachment);
        */
        
        List<Attachment> attachmentList = new List<Attachment>();
        attachmentList.add(testAttachment);

        List<Task> taskList = new List<Task>();
        taskList.add(testTask1);
        taskList.add(testTask2);

        List<OpportunityLineItem> oppProdList = new List<OpportunityLineItem>();
        oppProdList.add(oppProduct);

        List<Account> accountList = new List<Account>();
        accountList.add(testAC1);
        accountList.add(testAC2);
        
        List<Opportunity> opportunityList = new List<Opportunity>();
        opportunityList.add(testOpp1);
        opportunityList.add(testOpp2);
        
        List<Contact> contactList = new List<Contact>();
        contactList.add(testContact1);
        contactList.add(testContact2);
        
        /*
        List<Competitor_Asset__c> competitorAssetList = new List<Competitor_Asset__c>();
        competitorAssetList.add(testCompetitorAsset1);
        competitorAssetList.add(testCompetitorAsset2);
        
        
        List<Site__c> siteList = new List<Site__c>();
        siteList.add(testSite1);
        
        List<PBX_Type__c> pbxList = new List<PBX_Type__c>();
        pbxLIst.add(testPBX);
        
        sf2sf.SharePBXType(pbxList);
        sf2sf.ShareSites(siteList);
        */
        
        sf2sf.ShareProducts(productList);
        

        sf2sf.ShareOpportunities(opportunityList);
        
        sf2sf.ShareOpportunityProducts(oppProdList);
        
        
    }
}