public with sharing class INT_SF2SF_Utilities {

	@TestVisible private static Map<String,INT_SF2SF_Connection_Config__c> ConnectionConfigMap {
		get { 
		
			if (ConnectionConfigMap == null){
				ConnectionConfigMap = new Map<String, INT_SF2SF_Connection_Config__c>();
				for (INT_SF2SF_Connection_Config__c item : connectionNameList) {
					if (!ConnectionConfigMap.containsKey(item.Name)) {
						ConnectionConfigMap.put(item.Name,item);
					}
				}
			}
			return ConnectionConfigMap;
		}
		set;
	}
	
	public static INT_SF2SF_Connection_Config__c getConnectionConfigByValue(String keyValue) {
		INT_SF2SF_Connection_Config__c returnItem = new INT_SF2SF_Connection_Config__c();
	
		if (ConnectionConfigMap.containsKey(keyValue)) {
			returnItem = ConnectionConfigMap.get(keyValue);
		}
		return returnItem;
	}


	public static List<INT_SF2SF_Connection_Config__c> connectionNameList {
		get {
			if (connectionNameList == null) {
				connectionNameList = New List<INT_SF2SF_Connection_Config__c>(
					[Select SF2SF_Connection_Name__c, Name , Retry_Delay__c, 
					 	Opportunity_Related_Records__c, Opportunity_Sharing_Enabled__c,
						Excluded_User_Names__c,Account_Opportunity_Sharing_Enabled__c,
						OpportunityTriggerHandlerUpdatesDisabled__c 
					From INT_SF2SF_Connection_Config__c]);
			}
			return connectionNameList;
		}
		set;
	}

	@TestVisible private static Map<String,String> connectionNameMap {
		get {
			if (connectionNameMap == null) {
				connectionNameMap = new Map<String,String>();
				for (INT_SF2SF_Connection_Config__c item : connectionNameList) {
					if (!connectionNameMap.containsKey(item.Name)){
						connectionNameMap.put(item.Name,item.SF2SF_Connection_Name__c);
					}
				}
			}
			return connectionNameMap;	
		}
		set;
	}

	public static String ReturnActualConnectionName(String ConnectionNameValue) {
		// Populate Connection Name Map
		if (connectionNameMap.containsKey(ConnectionNameValue)) {
			return connectionNameMap.get(ConnectionNameValue);
		}
		else {
			return 'ConnectionNameNotFound';
		}
	}

	@TestVisible private static Map<String, PartnerNetworkConnection> connectionMap {
		get {
			if (connectionMap == null) {
				List<PartnerNetworkConnection> partnerNetConList = [Select id,connectionName
																	from PartnerNetworkConnection
																	where connectionStatus = 'Accepted' And AccountId = null];
				connectionMap = new Map<String,PartnerNetworkConnection>();
				for (PartnerNetworkConnection item : partnerNetConList) {
					if (!connectionMap.containsKey(item.connectionName)) {
						connectionMap.put(item.connectionName,item);
					}
				} 
			}
			return connectionMap;
		}
		set;
	}

	// Returns the actual connection Object from a friendly name
	public static PartnerNetworkConnection getConnection(String ConnectionName) {
		
		PartnerNetworkConnection returnConn = new PartnerNetworkConnection();
		String actualConnectionName = ReturnActualConnectionName(ConnectionName);

		if (connectionMap.containsKey(actualConnectionName)) {
			returnConn =  connectionMap.get(actualConnectionName);
		}
		return returnConn;
	}
}