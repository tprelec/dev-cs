/**
 * @description     This class shows a Customer assets on InstalledBaseTableLwc.
 * @author          11-11-2020: Ashok Patel
 */
@SuppressWarnings('PMD.StdCyclomaticComplexity')
public with sharing class InstalledBaseTableLwcController {
	private static final string STATUS = 'Active';
	private static final string COMPLETED = 'Completed';
	private static final string RC = 'RC';
	private static final string OC = 'OC';
	private static final integer RECORDSLIMIT = 5000;

	/*************
   @description : This method returns customer assets (Installation_Status__c and Billing_Status__c is active) with limit 
   cacheable=false - to get latest list of records                                 
   ***************/
	@AuraEnabled(cacheable=false)
	public static List<Customer_Asset__c> getCustomerAssets(
		String opportunityId,
		Integer limitSize,
		Integer offset
	) {
		List<String> banIds = getBanIds(opportunityId);

		Set<string> customerAssetsId = getCustomerAssetsId(opportunityId);

		List<Customer_Asset__c> customerAssetList = [
			SELECT
				Id,
				Name,
				Installed_Base_Id__c,
				RC_OC__c,
				BOP_Managed_Item_ID__c,
				IsActive__c,
				Charge_Description__c,
				Price__c,
				Quantity__c,
				Discount__c,
				Contract_Period__c,
				Article_Code__c,
				BOP_Article_Description__c,
				Cost_Center__c,
				PO_Number__c,
				Contract_End_Date__c,
				Product__c,
				Product__r.VF_Product__c,
				Site__r.name,
				External_Account__c
			FROM Customer_Asset__c
			WHERE
				Id NOT IN :customerAssetsId
				AND ((Installation_Status__c = :STATUS
				AND Billing_Status__c = :STATUS)
				OR (Installation_Status__c = :STATUS
				AND Billing_Status__c = NULL)
				OR (Installation_Status__c = NULL
				AND Billing_Status__c = :STATUS))
				AND External_Account__c IN :banIds
			WITH SECURITY_ENFORCED
			ORDER BY CreatedDate
			LIMIT :limitSize
			OFFSET :offset
		];

		// system.debug('************Data' + customerAssetList);
		// system.debug('************Data' + customerAssetList.size());
		return customerAssetList;
	}

	/*************
   @description : This method returns customer assets with dynamic conditions 
                                     
   ***************/
	@AuraEnabled(cacheable=true)
	@SuppressWarnings('PMD')
	public static List<Customer_Asset__c> searchCustomerAssetsByFields(
		String searchChargeDesc,
		String searchsite,
		String searchPurchaseOrder,
		String searchAllocationCode,
		Integer searchContrtPeriod,
		String opportunityId
	) {
		List<String> banIds = getBanIds(opportunityId);
		Set<string> customerAssetsId = getCustomerAssetsId(opportunityId);

		String query = 'SELECT Id,Name,Installed_Base_Id__c,RC_OC__c,IsActive__c,BOP_Managed_Item_ID__c,Charge_Description__c,Price__c,Quantity__c,Discount__c,Contract_Period__c,Article_Code__c,BOP_Article_Description__c,Cost_Center__c,PO_Number__c,Contract_End_Date__c,Product__c,Product__r.VF_Product__c,Site__r.name,External_Account__c FROM Customer_Asset__c where ( Id NOT IN : customerAssetsId and ((Installation_Status__c =: STATUS and Billing_Status__c =: STATUS) OR (Installation_Status__c =: STATUS and Billing_Status__c = null) OR (Installation_Status__c = null and Billing_Status__c =:STATUS) )  and External_Account__c IN :banIds )';

		if (string.isnotblank(searchChargeDesc)) {
			String key = '%' + searchChargeDesc + '%';
			query += ' AND (Charge_Description__c LIKE :key)';
		}
		if (string.isnotblank(searchsite)) {
			String sitekey = '%' + searchsite + '%';
			query += ' AND (Site__r.name LIKE :sitekey)';
		}

		if (string.isnotblank(searchPurchaseOrder)) {
			String pokey = '%' + searchPurchaseOrder + '%';
			query += ' AND (PO_Number__c LIKE :pokey)';
		}
		if (string.isnotblank(searchAllocationCode)) {
			String ackey = '%' + searchAllocationCode + '%';
			query += ' AND (Cost_Center__c LIKE :ackey)';
		}
		if (SearchContrtPeriod != null && SearchContrtPeriod != 0) {
			query += ' AND (Contract_Period__c =: SearchContrtPeriod)';
		}

		return Database.query(query);
	}

	/*************
   @description : This method create and returns OpportunityLineItem 
                                     
   ***************/
	@AuraEnabled
	@SuppressWarnings('PMD')
	public static List<OpportunityLineItem> createProduct(
		List<String> customerAssetsId,
		String opportunityId,
		List<String> productsId
	) {
		List<OpportunityLineItem> oliToCreate = new List<OpportunityLineItem>();
		// List<String> oliIds = new List<String>();

		List<Customer_Asset__c> customerAssetLIst = [
			SELECT
				Id,
				Name,
				Installed_Base_Id__c,
				RC_OC__c,
				Charge_Description__c,
				Price__c,
				Quantity__c,
				Discount__c,
				Contract_Period__c,
				Article_Code__c,
				BOP_Article_Description__c,
				Cost_Center__c,
				PO_Number__c,
				Contract_End_Date__c,
				Billing_Arrangement__r.name,
				Product__c,
				Product__r.VF_Product__c,
				Site__r.name,
				Site__c
			FROM Customer_Asset__c
			WHERE Id IN :customerAssetsId
			WITH SECURITY_ENFORCED
		];

		// create a map of vfProductId->CLC->pbeId
		Map<Id, Map<String, Id>> vfProductIdToCLCToPbeId = new Map<Id, Map<String, Id>>();
		// create a map of pbeId to family
		// Map<Id, String> pbeIdToQuantityType = new Map<Id, String>();
		for (PriceBookEntry pbe : [
			SELECT
				Id,
				Product2.CLC__c,
				Product2.VF_Product__c,
				Product2.Family,
				Product2.Quantity_Type__c,
				Product2.Product_Line__c
			FROM PriceBookEntry
			WHERE Product2.VF_Product__c IN :productsId
		]) {
			if (!vfProductIdToCLCToPbeId.containsKey(pbe.Product2.VF_Product__c)) {
				Map<String, Id> clcToPbeId = new Map<String, Id>{ pbe.Product2.CLC__c => pbe.Id };
				vfProductIdToCLCToPbeId.put(pbe.Product2.VF_Product__c, clcToPbeId);
			} else {
				vfProductIdToCLCToPbeId.get(pbe.Product2.VF_Product__c)
					.put(pbe.Product2.CLC__c, pbe.Id);
			}
		}

		if (!customerAssetLIst.isEmpty() && !String.isBlank(OpportunityId)) {
			for (Customer_Asset__c cusAssetsObj : customerAssetLIst) {
				OpportunityLineItem oli = new OpportunityLineItem();
				oli.OpportunityId = OpportunityId;
				oli.Customer_Asset__c = cusAssetsObj.Id;
				//  oli.RC_OC__c = cusAssetsObj.RC_OC__c;
				if (vfProductIdToCLCToPbeId.get(cusAssetsObj.Product__r.VF_Product__c) != null) {
					oli.PriceBookEntryId = vfProductIdToCLCToPbeId.get(
							cusAssetsObj.Product__r.VF_Product__c
						)
						.get(null);
				}

				oli.CLC__c = 'Churn';
				oli.Activity_Type__c = 'Cease';
				oli.Location__c = cusAssetsObj.Site__c;
				oli.Deal_Type__c = '';
				oli.Group__c = '';
				// oli.Model_Number__c ='';
				oli.Quantity = cusAssetsObj.Quantity__c;
				oli.Gross_List_Price__c = cusAssetsObj.Price__c;
				oli.DiscountNew__c = cusAssetsObj.Discount__c;
				oli.Duration__c = cusAssetsObj.Contract_Period__c;
				//oli.LG_ContractTerm__c ='';
				oli.PO_Number__c = cusAssetsObj.PO_Number__c;
				oli.Cost_Center__c = cusAssetsObj.Cost_Center__c;
				oli.Billing__c = true;
				oli.Product_Arpu_Value__c =
					(oli.Gross_List_Price__c == null ? 0 : oli.Gross_List_Price__c) *
					((100 - (oli.DiscountNew__c == null ? 0 : oli.DiscountNew__c)) / 100.0);
				oli.TotalPrice =
					(oli.Quantity == null ? 0 : oli.Quantity) *
					(oli.Gross_List_Price__c == null ? 0 : oli.Gross_List_Price__c) *
					(oli.Duration__c == null ? 0 : oli.Duration__c) *
					((100 - (oli.DiscountNew__c == null ? 0 : oli.DiscountNew__c)) / 100.0);
				oli.recalculateFormulas();
				oliToCreate.add(oli);
			}

			insert oliToCreate;
		}

		return oliToCreate;
	}

	/**********
    @parameter opportunityId
    return set of customer assets Id
    *******/
	public static Set<String> getCustomerAssetsId(string opportunityId) {
		Set<String> customerAssetsId = new Set<String>();
		List<OpportunityLineItem> opportunityLineItemList = [
			SELECT id, name, OpportunityId, Customer_Asset__c
			FROM OpportunityLineItem
			WHERE OpportunityId = :opportunityId AND Customer_Asset__c != NULL
			WITH SECURITY_ENFORCED
		];
		if (!opportunityLineItemList.isEmpty()) {
			for (OpportunityLineItem oppLineItem : opportunityLineItemList) {
				customerAssetsId.add(oppLineItem.Customer_Asset__c);
			}
		}

		List<Contracted_Products__c> contractedProductsList = [
			SELECT id, name, VF_Contract__c, VF_Contract__r.Opportunity__c, Customer_Asset__c
			FROM Contracted_Products__c
			WHERE VF_Contract__r.Opportunity__c = :opportunityId AND Customer_Asset__c != NULL
			WITH SECURITY_ENFORCED
		];
		if (!contractedProductsList.isEmpty()) {
			for (Contracted_Products__c cpObj : contractedProductsList) {
				customerAssetsId.add(cpObj.Customer_Asset__c);
			}
		}
		return customerAssetsId;
	}

	/**********
    @parameter opportunityId
    return  List of ban__c Id
    *******/
	@SuppressWarnings('PMD.AvoidDeeplyNestedIfStmts')
	public static List<string> getBanIds(string opportunityId) {
		List<string> banIdList = new List<string>();

		//string banId = '';

		List<Opportunity> opptyList = [
			SELECT id, name, BAN__c, BAN__r.ExternalAccount__c
			FROM Opportunity
			WHERE id = :opportunityId AND BAN__c != NULL
			WITH SECURITY_ENFORCED
		];

		if (!opptyList.isEmpty()) {
			/* if (opptyList[0].BAN__r.ExternalAccount__c != null) {
                List<Ban__C> banList = [
                    SELECT Id, ExternalAccount__c
                    FROM BAN__c
                    WHERE
                        ExternalAccount__c = :opptyList[0]
                            .BAN__r.ExternalAccount__c WITH SECURITY_ENFORCED
                ];
                if (!banList.isEmpty()) {
                    for (Ban__C banObj : banList) {
                        banIdList.add(banObj.Id);
                    }
                }
            } else if (opptyList[0].BAN__c != null) {
                banIdList.add(opptyList[0].BAN__c);
            }*/
			for (Opportunity oppty : opptyList) {
				if (oppty.BAN__r.ExternalAccount__c != null) {
					banIdList.add(oppty.BAN__r.ExternalAccount__c);
				}
			}
		}

		return banIdList;
	}

	/*************
   @description : This method create and returns Contracted_Products__c 
                                     
   ***************/
	@AuraEnabled
	@SuppressWarnings('PMD.ApexCRUDViolation')
	// public static List<Contracted_Products__c> createContractedProduct(List<String>customerAssetsId,String OpportunityId,String vfContractId,String orderId){
	public static List<Contracted_Products__c> createContractedProduct(
		List<String> customerAssetsId,
		String opportunityId,
		String orderId
	) {
		Order__c orderObj = [
			SELECT id, name, VF_Contract__c, VF_Contract__r.Opportunity__c
			FROM Order__c
			WHERE id = :orderId
			WITH SECURITY_ENFORCED
		];

		String vfContractId = orderObj.VF_Contract__c;
		List<Contracted_Products__c> contractProdToCreate = new List<Contracted_Products__c>();

		List<Customer_Asset__c> customerAssetLIst = [
			SELECT
				Id,
				Name,
				Installed_Base_Id__c,
				RC_OC__c,
				Charge_Description__c,
				Price__c,
				Quantity__c,
				Discount__c,
				Contract_Period__c,
				Article_Code__c,
				BOP_Article_Description__c,
				Cost_Center__c,
				PO_Number__c,
				Contract_End_Date__c,
				Product__c,
				Product__r.VF_Product__c,
				Site__r.name,
				Site__c,
				Product__r.BAP_SAP__c,
				Product__r.ProductCode,
				Product__r.Family,
				Product__r.Multiplier_Vodacom__c
			FROM Customer_Asset__c
			WHERE Id IN :customerAssetsId
			WITH SECURITY_ENFORCED
		];

		// Get any other contracted product on the order
		Contracted_Products__c otherCP = [
			SELECT proposition__c, family_condition__c
			FROM Contracted_Products__c
			WHERE Order__c = :orderId
			WITH SECURITY_ENFORCED
			LIMIT 1
		];

		if (
			!customerAssetLIst.isEmpty() &&
			!String.isBlank(vfContractId) &&
			!String.isBlank(orderId)
		) {
			for (Customer_Asset__c cusAssetsObj : customerAssetLIst) {
				Contracted_Products__c newCP = new Contracted_Products__c();
				newCP.VF_Contract__c = vfContractId;
				newCP.Order__c = orderId;
				newCP.CLC__c = 'Churn';
				newCP.Activity_Type__c = 'Cease';
				newCP.Product__c = cusAssetsObj.Product__c;
				newCP.List_price__c = cusAssetsObj.Product__r.BAP_SAP__c;
				newCP.ProductCode__c = cusAssetsObj.Product__r.ProductCode;
				newCP.Site__c = cusAssetsObj.Site__c;
				newCP.Quantity__c = cusAssetsObj.Quantity__c;
				newCP.Fixed_Mobile__c = 'Fixed';
				newCP.Proposition_Type__c = 'FIXED';
				newCP.proposition__c = otherCP.proposition__c;
				newCP.family_condition__c = otherCP.family_condition__c;
				newCP.Multiplier_Vodacom__c = cusAssetsObj.Product__r.Multiplier_Vodacom__c;
				newCP.Product_Family__c = cusAssetsObj.Product__r.Family;
				newCP.Customer_Asset__c = cusAssetsObj.Id;
				newCP.Discount__c = cusAssetsObj.Discount__c;
				newCP.Duration__c = cusAssetsObj.Contract_Period__c;
				newCP.PO_Number__c = cusAssetsObj.PO_Number__c;
				contractProdToCreate.add(newCP);
			}

			insert contractProdToCreate;
		}

		return contractProdToCreate;
	}

	@AuraEnabled
	public static Order__c orderDetails(Id recordId) {
		Order__c orderObj = [
			SELECT id, name, VF_Contract__c, VF_Contract__r.Opportunity__c
			FROM Order__c
			WHERE id = :recordId
			WITH SECURITY_ENFORCED
		];

		return orderObj;
	}
}