public with sharing class LC_NewCase_Controller {

    public static Map<Id, String> recordtypemap;

    @AuraEnabled
    public static List<LC_NewCase_Controller.RecordTypeRadio> fetchRecordTypeValues(String objectName){
        List<LC_NewCase_Controller.RecordTypeRadio> returnList = new List<LC_NewCase_Controller.RecordTypeRadio>();
        List<Schema.RecordTypeInfo> recordtypes = Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfos();
        recordtypemap = new Map<Id, String>();
        for(RecordTypeInfo rt : recordtypes){
            if(LC_NewCase_Controller.isRecordTypeAvailable(rt)) {
                if (rt.getName() != 'Master' && rt.getName().trim() != '') {
                    LC_NewCase_Controller.RecordTypeRadio rtRadio = new LC_NewCase_Controller.RecordTypeRadio();
                    rtRadio.label = rt.getName();
                    rtRadio.value = rt.getRecordTypeId();
                    returnList.add(rtRadio);
                }
            }
        }
        return returnList;
    }

    public static Boolean isRecordTypeAvailable(RecordTypeInfo info){
        return info.isMaster() && !info.isDefaultRecordTypeMapping() ? false : info.isAvailable();
    }

    @AuraEnabled
    public static Opportunity fetchOpportunityDetails(String oppId) {
        Opportunity opp;
        if (!String.isEmpty(oppId)) {
            opp = [Select AccountId, Contract_VF__c, Primary_Basket__c, Primary_Quote__c From Opportunity Where Id =: oppId];
        }
        return opp;
    }

    @AuraEnabled
    public static Credit_Note__c fetchCreditNoteDetails(String cnId) {
        Credit_Note__c cn;
        if (!String.isEmpty(cnId)) {
            cn = [Select Account__c From Credit_Note__c Where Id =: cnId];
        }
        return cn;
    }
    
    @AuraEnabled
    public static HBO__c fetchHBODetails(String HBOId) {
        HBO__c hbo;
        if (!String.isEmpty(HBOId)) {
            hbo = [Select hbo_account__c From HBO__c Where Id =: HBOId];
        }
        return hbo;
    }

    @AuraEnabled
    public static Order__c fetchOrderDetails(String orderId) {
        Order__c order;
        if (!String.isEmpty(orderId)) {
            order = [Select VF_Contract__c, Account__c, VF_Contract__r.Opportunity__r.Id From Order__c Where Id =: orderId];
        }
        return order;
    }

    @AuraEnabled
    public static VF_Contract__c fetchContractVFDetails(String contractId) {
        VF_Contract__c contract;
        if (!String.isEmpty(contractId)) {
            contract = [Select Account__c, Opportunity__c  From VF_Contract__c Where Id =: contractId];
        }
        return contract;
    }

    @AuraEnabled
    public static cscfga__Product_Basket__c basket(String basketId) {
        cscfga__Product_Basket__c basket;
        if (!String.isEmpty(basketId)) {
            basket = [Select cscfga__Opportunity__c, csbb__Account__c  From cscfga__Product_Basket__c Where Id =: basketId];
        }
        return basket;
    }

    @AuraEnabled
    public static BigMachines__Quote__c fetchVFQuoteDetails(String vfQuoteId) {
        BigMachines__Quote__c vfQuote;
        if (!String.isEmpty(vfQuoteId)) {
            vfQuote = [Select BigMachines__Opportunity__c, BigMachines__Account__c  From BigMachines__Quote__c Where Id =: vfQuoteId];
        }
        return vfQuote;
    }




    public class RecordTypeRadio {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
    }
}