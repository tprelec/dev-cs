@IsTest
public class LG_AddressResponseTest {
    
	@IsTest
	public static void test1() {
        LG_AddressResponse ar = new LG_AddressResponse();
        
        LG_AddressResponse.geographicAreas ga = new LG_AddressResponse.geographicAreas();
        ga.type = 'gaType';
        ga.code = 'gaCode';
        ga.availabilityGroups = 'gaAvailabilityGroups';
        ga.areaType = 'gaAreaType';
        ga.activationCode = 'gaActivationCode';
        ga.setupId = 'gaSetupId';
        ga.region = 'gaRregion';
        ga.availabilityDescription = 'gaAvailabilityDescription';
        ga.isPartnerNetwork = 'gaIsPartnerNetwork';
        ga.networkId = 'gaNetworkId';
        ga.hubId = 'gaHubId';
        ga.nodeId = 'gaNodeId';
        
        LG_AddressResponse.addView aw = new LG_AddressResponse.addView();
        aw.additionalInfo = 'Additional Info';
        aw.addressId = 'Address Id';
        aw.attenstion = 'Attenstion';
        aw.city = 'City';
        aw.CountryName = 'Country Name';
        aw.district = 'District';
        aw.entrance = 'Entrance';
        aw.firstName = 'First Name';
        aw.footprint = 'Footprint';
        aw.geographicAreas = new List<LG_AddressResponse.geographicAreas>{ ga };
        aw.houseName = 'House Name';
        aw.houseNumber = 'House Number';
        aw.lastName = 'Last Name';
        aw.poBoxNr = 'Po Box Nr';
        aw.poBoxType = 'Po Box Type';
        aw.postcode = 'Postcode';
        aw.properties = new List<Object>();
        aw.region = 'Region';
        aw.stateOrProvince = 'State Or Province';
        aw.streetNrFirst = 'Street Nr First';
        aw.streetName = 'Street Name';
        aw.streetNrFirstSuffix = 'Street Nr First Suffix';
        aw.streetNrLastSuffix = 'Street Nr Last Suffix';
        aw.title = 'Title';
        aw.type = 'Type';
        
        ar.addView = new List<LG_AddressResponse.addView>{ aw };
        
		system.assertEquals('Type', aw.get('type'));
        system.assertEquals('Address Id', aw.get('addressId'));
        system.assertEquals('Street Name', aw.get('streetName'));
        system.assertEquals('Street Nr First Suffix', aw.get('streetNrFirstSuffix'));
        system.assertEquals('Street Nr First', aw.get('streetNrFirst'));
        system.assertEquals('City', aw.get('city'));
        system.assertEquals('Postcode', aw.get('postcode'));
        system.assertEquals('State Or Province', aw.get('stateOrProvince'));
        system.assertEquals('Country Name', aw.get('CountryName'));
        system.assertEquals(null, aw.get('somethingElse'));
        
        LG_AddressResponse.OptionalsJson oj = new LG_AddressResponse.OptionalsJson(ar.addView[0]);
        
        system.assertEquals('City', oj.city);
        system.assertEquals('Postcode', oj.postcode);
        system.assertEquals('Street Name', oj.street);
        
        cscrm__Address__c address = new cscrm__Address__c(
            cscrm__City__c = 'City2',
            cscrm__Zip_Postal_Code__c = 'ZIP',
            LG_HouseNumberExtension__c = 'ext.',
            LG_HouseNumber__c = '13',
            cscrm__Street__c = 'Street',
            LG_AddressID__c = 'AddressId'
        );
        
        LG_AddressResponse.OptionalsJson oj2 = new LG_AddressResponse.OptionalsJson(address);
        
        system.assertEquals('City2', oj2.city);
        system.assertEquals('ZIP', oj2.postcode);
        system.assertEquals('13', oj2.houseNumber);
    }
}