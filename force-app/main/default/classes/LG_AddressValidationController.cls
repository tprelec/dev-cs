public with sharing class LG_AddressValidationController {

    String objectType;
    Id recordID;
    public String recordCountry;
    public String returnURL {get; set;}
    public Map<String, String> searchField {get; set;}
    public Map<Integer, String> searchFieldSeq {get; set;}
    public List<String> searchFieldS {get; set;}
    settingInfo settingData = new settingInfo();
    public LG_AddressResponse addressResponse {get; set;}
    public String selectedAddressID {get; set;}
    public ZG_EndpointResolver.mapAndDisplay mapAndDisplaySetting {get; set;}
    String[] tableDisplayFields;
    Integer loopCount;
    public List<Add_ToBeDel__c> addressSuggestion {get; set;}
    public ID selectedAddress {get; set;}
    public sObject validationRecord {get; set;}
    public Boolean invalidSetting {get; set;}
    public Boolean outsideAddressValidationpage {get; set;}
    public LG_MACDConfigurationController macdConfController {get; set;}

    private Id opportunityId { get; set; }

    /*
        Constructor when this class is set as the extension of a Visualforce page
    */
    public LG_AddressValidationController(ApexPages.StandardController controller) {

        recordID = ApexPages.currentPage().getParameters().get('id');
        recordCountry = ApexPages.currentPage().getParameters().get('country');

        /**
         * Check if cscrm__Account__c validation is requested from Opportunity object
         *
         * @author Petar Miletic
         * @story SFDT-1136
         * @since  13/06/2016
        */
        if (checkForOpportunityOverride()) {
            return;
        }

        try {
            objectType = findObjectNameFromRecordIdPrefix(recordID);
            retrieveSearchSettings(objectType, recordCountry);
        } catch (Exception ex) {
            invalidSetting = true;

            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Invalid Salesforce ID: ' + recordID + ', ' + ex.getMessage()));
            return;
        }

        searchField = readFieldSet(settingData.fieldSet, objectType);
        retrieveCurrentData(searchField.keySet() , objectType, recordID);
        sequenceSearch();
        findAddress();
    }

    /*
        Constructor when this class is set as the primary controller of a Visualforce page
    */
    public LG_AddressValidationController() {

        recordID = ApexPages.currentPage().getParameters().get('id');
        recordCountry = ApexPages.currentPage().getParameters().get('country');
        returnURL = ApexPages.currentPage().getParameters().get('returnURL');

        /**
         * Check if cscrm__Account__c validation is requested from Opportunity object
         *
         * @author Petar Miletic
         * @story SFDT-1136
         * @since  13/06/2016
        */
        if (checkForOpportunityOverride()) {
            return;
        }

        boolean isFromMacd = false;

        //if used as a component outside the AddressValidation page where id is an accountID
        if (String.isBlank(recordId)) {
            isFromMacd = true;
            recordID = ApexPages.currentPage().getParameters().get('accountId');

            if (String.isNotBlank(recordId) && String.isBlank(recordCountry)) {
                Account acc = [SELECT Id, LG_VisitCountry__c FROM Account WHERE Id = :recordId];
                recordCountry = acc.LG_VisitCountry__c;

                if (String.isBlank(recordCountry)) {
                    recordCountry = 'Netherlands';
                }
            }
        }

        try {
            objectType = findObjectNameFromRecordIdPrefix(recordID);
            retrieveSearchSettings(objectType, recordCountry);
        } catch (Exception ex) {
            invalidSetting = true;

            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Invalid Salesforce ID: ' + recordID + ', ' + ex.getMessage()));
            return;
        }

        searchField = readFieldSet(settingData.fieldSet, objectType);
        retrieveCurrentData(searchField.keySet() , objectType, recordID);
        sequenceSearch();

        if (!isFromMacd) {
            findAddress();
        }
    }

    public PageReference cancel() {
        return new pagereference(returnURL);
    }

    /**
     * Address validation premises: when an Opportunity is synced I want to see what Opportunities
     * still have a premise that has a non-validated premise and
     * I want to be able to validate this premise
     *
     * @author Petar Miletic
     * @story SFDT-1136
     * @since  13/06/2016
    */
    private Boolean checkForOpportunityOverride() {

        Boolean allValidated = false;

        // Only do one SOQL to retrieve data and avoid subsequent calls
        if (this.opportunityId == null && ApexPages.currentPage().getParameters().containsKey('oppOverride')) {

            this.opportunityId = ApexPages.currentPage().getParameters().get('oppOverride');

            AggregateResult[] premises = [SELECT LG_Address__c, cscfga__Product_Basket__r.LG_MarketSegment__c MarketSegment
                                          FROM cscfga__Product_Configuration__c
                                          WHERE cscfga__Product_Basket__r.cscfga__Opportunity__c = :this.opportunityId AND LG_Address__c != null
                                                  AND cscfga__Product_Basket__r.csordtelcoa__Synchronised_with_Opportunity__c = true AND LG_Address__r.LG_AddressID__c = null
                                                          GROUP BY LG_Address__c, cscfga__Product_Basket__r.LG_MarketSegment__c LIMIT 1];

            // If premise exists override recordId with cscrm__Address__c object and contiune from there
            if (!premises.isEmpty()) {

                String marketSegment = (String)premises.get(0).get('MarketSegment');

                // Validate only SoHo Opportunities, skip all others
                if (MarketSegment != 'SoHo') {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'It is not required to validate the premise for this Opportunity'));
                    allValidated = true;
                } else {
                    recordID = (Id)premises.get(0).get('LG_Address__c');
                }
            } else {
                // Notify user about current state
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Premise for this Opportunity has already been validated'));
                allValidated = true;
            }
        }

        return allValidated;
    }

    void sequenceSearch() {
        searchFieldS = new List<String>();
        searchFieldS = searchFieldSeq.values();
    }

    void retrieveSearchSettings(String objectName, String country) {

        //loop not needed
        String strUrlUTF8; String displayMapJson;
        LG_AddressValidationSetup__mdt tempVar = [Select LG_FieldSetName__c, LG_DisplayConfiguration__c from LG_AddressValidationSetup__mdt where LG_Country__c = :country AND LG_Object__c = :objectName limit 1];

        String StaticResourceName;
        if (tempVar != null) {
            settingData.validSettingFound = true;
            settingData.fieldSet = tempVar.LG_FieldSetName__c;
            StaticResourceName = tempVar.LG_DisplayConfiguration__c;
        }

        StaticResource displayMapBlob = [Select body, name from StaticResource where Name = :StaticResourceName];

        strUrlUTF8 = displayMapBlob.body.toString();
        System.debug('String strUrlUTF8: [' + strUrlUTF8 + ']');
        displayMapJson = EncodingUtil.urlDecode(strUrlUTF8, 'UTF-8');
        System.debug('JSON Start' + displayMapJson + 'JSON Stop');

        if (this.opportunityId != null && objectName == 'Opportunity') {
            mapAndDisplaySetting = (ZG_EndpointResolver.mapAndDisplay)JSON.deserialize(displayMapJson, cscrm__Address__c.class);
        } else {
            mapAndDisplaySetting = (ZG_EndpointResolver.mapAndDisplay)JSON.deserialize(displayMapJson, ZG_EndpointResolver.mapAndDisplay.class);
        }

        System.assert(settingData.validSettingFound && settingData.fieldSet != '', 'No valid configuration found!');
    }

    void retrieveCurrentData(Set<String> searchField, String objectName, String recordID) {

        String query = 'SELECT ';
        loopCount = 1;
        for (String loopVar : searchField) {
            loopCount++;
            query += loopVar;
            if (loopCount <= searchField.size()) {
                query += ',';
            }
        }
        query += ' FROM ' + objectType + ' WHERE ID=' + '\'' + recordID + '\'';

        validationRecord = (sObject)Database.query(query);
        System.debug(' The Query' + validationRecord + ' The Query');
    }


    /* To be reused or Deleted*/
    public void findAddress () {
        
        ApexPages.getMessages().clear();
        if ( LG_EnvironmentVariables__c.getOrgDefaults().LG_AddressSearchLocal__c == true || Test.isRunningTest() ) {
            findAddressLocal();
        } else if (validationRecord.get('LG_VisitCountry__c') != null && validationRecord.get('LG_VisitPostalCode__c') != null && validationRecord.get('LG_VisitHouseNumber__c') != null) {
            findAddressPeal();
        }
    }

    public void findAddressPeal() {

        // String requestURL = 'callout:LG_OracleAccessGateway/peal/api/b2b/addresses?cty=NL&chl=B2B_CATALYST_NL&';
        ZG_EndpointResolver resolver = new ZG_EndpointResolver();

        String requestURL = resolver.getAddressCheckEndpoint(mapAndDisplaySetting, validationRecord);
        Integer loopCount = mapAndDisplaySetting.fieldMap.size();
        
        /*Boolean paramAdded = false;

        for (ZG_EndpointResolver.fieldMap loopVar : mapAndDisplaySetting.fieldMap) {
            //paramAdded =false;
            if (loopVar.searchFilter && validationRecord.get(loopVar.sfFieldName) != null) {
                requestURL += loopVar.webServiceParam;
                requestURL += ('=' + ((String)validationRecord.get(loopVar.sfFieldName)).replaceAll( '\\s+', ''));
                paramAdded = true;
            }
            loopCount++;
            if (mapAndDisplaySetting.fieldMap.size() > loopCount && paramAdded) {
                requestURL += '&';
                paramAdded = false;
            }
        }*/

        System.debug('URL FORMED' + requestURL);
        System.debug('URL FORMED 1' + mapAndDisplaySetting.fieldMap);

        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http httpObj = new Http();

        req.setMethod('GET');
        req.setEndpoint(requestURL);
        req.setTimeout(30000);

        try {
            res = httpObj.send(req);
        } catch (System.CalloutException e) {
            System.debug('Call Out Exception :' + e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Address validation service is not available. Please contact your administrator.'));
        } catch (Exception e) {
            System.debug('Exception :' + e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Address validation service is not available. Please contact your administrator.'));
        }

        System.debug('Response is \'' + res.getbody() + '\'');

        if (res.getStatusCode() == 200 && res.getStatus() == 'OK') {
            addressResponse = new LG_AddressResponse();
            addressResponse.addView = new List<LG_AddressResponse.addView>();

            try {
                addressResponse.addView = (List<LG_AddressResponse.addView>)JSON.deserialize(res.getBody(), List<LG_AddressResponse.addView>.class);
            } catch (System.JSONException ex) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Something went wrong. Please contact your administrator.'));
            }
        } else if (res.getStatusCode() == 100) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, 'No Address Found!'));
        } else if (res.getStatus() != null) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, res.getStatus()));
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'Something went wrong. Please contact your administrator.'));
        }
    }

    public void findAddressLocal () {

        if (objectType == 'cscrm__Address__c') {
            addressSuggestion = createLocalAddressQuery('cscrm__Country__c', 'cscrm__Zip_Postal_Code__c', 'LG_HouseNumber__c', 'LG_HouseNumberExtension__c');
        } else {
            addressSuggestion = createLocalAddressQuery('LG_VisitCountry__c', 'LG_VisitPostalCode__c', 'LG_VisitHouseNumber__c', 'LG_VisitHouseNumberExtension__c');
        }

        addressResponse = new LG_AddressResponse();
        addressResponse.moreAddressesAvailable = false;
        loopCount = 0;

        addressResponse.addView = new List<LG_addressResponse.addView>();

        if (addressSuggestion != null) {

            for (Add_ToBeDel__c loopVar : addressSuggestion) {
                addressResponse.addView.add(new LG_addressResponse.addView());
                addressResponse.addView[loopCount].addressID = loopVar.Address_ID__c;
                addressResponse.addView[loopCount].city = loopVar.City__c;
                addressResponse.addView[loopCount].streetNrFirstSuffix = loopVar.Extension__c;
                addressResponse.addView[loopCount].CountryName = loopVar.Country__c;
                addressResponse.addView[loopCount].streetNrFirst = loopVar.House_Number__c;
                addressResponse.addView[loopCount].postcode = loopVar.Post_Code__c ;
                addressResponse.addView[loopCount].streetName = loopVar.Street__c;
                loopCount++;
            }
        }
    }

    private List<Add_ToBeDel__c> createLocalAddressQuery(String country, String postalCode, String houseNumber, String houseNumberExtension) {

        String addressQuery = 'SELECT Name, Address_ID__c, House_Number__c, Extension__c, City__c, Country__c, Post_Code__c, Street__c FROM Add_ToBeDel__c ';

        List<Add_ToBeDel__c> retval = new List<Add_ToBeDel__c>();

        if (validationRecord.get(country) != null && validationRecord.get(postalCode) != null) {

            addressQuery = addressQuery + 'WHERE Country__c = \'' + validationRecord.get(country) + '\' AND Post_Code__c LIKE \'' + validationRecord.get(postalCode) + '\'';

            if (validationRecord.get(houseNumber) != null) {
                addressQuery = addressQuery  + ' AND House_Number__C LIKE \'' + validationRecord.get(houseNumber) + '\'';
            }

            if (validationRecord.get(houseNumberExtension) != null) {
                addressQuery = addressQuery  + ' AND Extension__c LIKE \'' + validationRecord.get(houseNumberExtension) + '\'';
            }

            System.debug('findAddressLocal: ' + addressQuery);
            retval = (List<Add_ToBeDel__c>)Database.Query(addressQuery);
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please fill data in Country & Post code');
        }

        return retval;
    }

    /* To be reused or Deleted*/
    public pagereference setAddress() {

        LG_addressResponse.addView selectAddress = new LG_addressResponse.addView();

        for (LG_addressResponse.addView loopVar : addressResponse.addView) {
            if (loopVar.addressID == selectedAddressID) {
                selectAddress = loopVar;
            }
        }

        if (outsideAddressValidationpage) {

            if (macdConfController.validateAddress) {
                update new cscrm__Address__c(Id = macdConfController.validateSiteId, LG_AddressID__c = selectAddress.addressId);
            } else {
                macdConfController.moveSite = new LG_AddressResponse.OptionalsJson(selectAddress);
            }

            return null;

        } else {

            for (ZG_EndpointResolver.fieldMap loopVar : mapAndDisplaySetting.fieldMap) {
                validationRecord.put(loopVar.sfFieldName , selectAddress.get(loopVar.jsonField));
            }

            try {
                update validationRecord;
            } catch (exception ex) {

                String errorMessage = ex.getMessage();
                Integer occurence;
                if (!ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
                    /*occurence = errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,') + 34;
                    errorMessage = errorMessage.mid(occurence, errorMessage.length());
                    occurence = errorMessage.lastIndexOf(':');
                    errorMessage = errorMessage.mid(0, occurence);*/
                    
                    errorMessage = ex.getMessage();
                    
                    System.debug('errorMessage' + errorMessage);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
                }   
                
                return null;
            }

            return (new pagereference(returnURL));
        }
    }

    /*
     * Description : This method processes Record ID and fetched the object Name from the Schema object
     * Param : Record ID
     * Return : API name of the object
    */
    public static String findObjectNameFromRecordIdPrefix(Id objId) {
        return objId.getSObjectType().getDescribe().getName();
    }

    /*
     * Descripion : This method process fieldset data and returns a map of Field Label and Field API name
     * Param : FieldSet API Name & Object Name
     * Return : Map of Field Label & Field API name
    */
    public Map<String, String> readFieldSet(String fieldSetName, String ObjectName) {

        System.assert(ObjectName != '', 'This page can be used with refernce to Salesforce Objects');
        System.assert(fieldSetName != '', 'No Search layout defined for this object');

        searchField = new Map<String, String>();

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        searchFieldSeq = new Map<Integer, String>();

        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        loopCount = 0;

        for (Schema.FieldSetMember loopVar : fieldSetObj.getFields()) {
            searchFieldSeq.put(loopCount, loopVar.getFieldPath());
            searchField.put(loopVar.getFieldPath(), loopVar.getLabel());
            loopCount++;
        }

        return searchField;
    }

    /*
        Description : This sclass captures from settings from custom metadata type valid for current object and country.
    */
    class settingInfo {
        public boolean validSettingFound;
        public String fieldSet;
        public settingInfo () {
            validSettingFound = false;
            fieldSet = '';
        }
    }

    /*
        Description : This class captures settings for mapping of Salesforce to JSON fields and also the the display setting on the address search layout.
    */
    /*class mapAndDisplay {
        public boolean isActive;
        public List<fieldMap> fieldMap {get; set;}
    }

    class fieldMap {
        public String sfFieldName {get; set;}
        public String jsonField {get; set;}
        public Boolean resultVisible {get; set;}
        public String displayLabel {get; set;}
        public Boolean searchFilter {get; set;}
        public Boolean requiredParam {get; set;}
        public String webServiceParam {get; set;}
    }*/
}