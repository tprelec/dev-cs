public class LG_AttachmentTriggerHandler 
{
	public static void AfterUpdateHandle(List<Attachment> lstAttachmentNew, List<Attachment> lstAttachmentOld)
    {
    	
    }
    
    public static void AfterInsertHandle(List<Attachment> lstAttachmentNew)
    {
    	SendEmailQuote(lstAttachmentNew);
    }
    
    
    private static list< CSCAP__Click_Approve_Setting__c> lstCAS = null;
    
    private static Id GetSiteApprovalSettingId()
    {
    	Id CASId=null;
    	
    	if (lstCAS==null)
    	{
    		lstCAS = [select Id, Name from CSCAP__Click_Approve_Setting__c c
				where Name like '%Opportunit%' limit 1];
    	}
    	
    	if (lstCAS.size()>0)
    	{
    		CASId = lstCAS[0].Id;
    	}
    	
    	return CASId;
    }
    
    private static void SendEmailQuote(List<Attachment> lstAttachmentNew)
    {
    	System.debug('CS TEST ****In send email');
    	if (GetSiteApprovalSettingId()!=null)//so there is a setting created
    	{
	    	map<Id,set<Id>> mapOppIdsetAttachmentId = new map<Id,set<Id>>();
	    	
			for (Attachment tmpAttachment : lstAttachmentNew)
			{
				if (string.valueOf(tmpAttachment.ParentId).startswith('006'))
				{
					if (mapOppIdsetAttachmentId.containsKey(tmpAttachment.ParentId))
					{
						set<Id> setAttachmentId = mapOppIdsetAttachmentId.get(tmpAttachment.ParentId);
						setAttachmentId.add(tmpAttachment.Id);
					}
					else
					{
						set<Id> setAttachmentId = new set<Id>();
						setAttachmentId.add(tmpAttachment.Id);
						mapOppIdsetAttachmentId.put(tmpAttachment.ParentId,setAttachmentId);
					}
				}
			}
			
			
			if (mapOppIdsetAttachmentId.size()>0)
			{
				CSCAP.API_1.MultipleSendApprovalRequestRecord tmpMultipleSendApprovalRequestRecord;
				list<CSCAP.API_1.MultipleSendApprovalRequestRecord> lstMultipleSendApprovalRequestRecord = new list<CSCAP.API_1.MultipleSendApprovalRequestRecord>();
				
				list<Opportunity> lstOpportunity = [select Id, LG_AutomatedQuoteDelivery__c, StageName, 
					(select ContactId, OpportunityId, Role From OpportunityContactRoles) 
					from Opportunity
					where LG_AutomatedQuoteDelivery__c='Quote Requested'
					and Id in : mapOppIdsetAttachmentId.keyset()];	
					
				// Custom settings that contains info about potential flyers
				List<FlyerInfo__c> finfo = [SELECT IncludeFlyer__c, DocumentIds__c FROM FlyerInfo__c];
				
				for (Opportunity tmpOpportunity : lstOpportunity)
				{
					Id recepientId=null;
					
					
					for (OpportunityContactRole tmpOpportunityContactRole : tmpOpportunity.OpportunityContactRoles)
					{
						if (tmpOpportunityContactRole.Role=='Business User')
						{
							recepientId=tmpOpportunityContactRole.ContactId;
							break;
						}
						else if (tmpOpportunityContactRole.Role=='Administrative Contact')
						{
							recepientId=tmpOpportunityContactRole.ContactId;
							break;
						}
						else
						{
							recepientId=tmpOpportunityContactRole.ContactId;
							break;						
						}
					}
					
					
					set<Id> setAttachmentId = mapOppIdsetAttachmentId.get(tmpOpportunity.Id);

					Set<Id> documentIds = new Set<Id>();

					if(!finfo.isEmpty()) {
						if(finfo[0].IncludeFlyer__c) {
							// DocumentIds__c contains comma separated Ids of flyer that are stored as Documents
							for(String docId : finfo[0].DocumentIds__c.split(',')) {
								if(docId.startsWith('015')) {
									documentIds.add(docId);
								}
							}
						}
					}
					
					if (recepientId!=null)
					{

						tmpMultipleSendApprovalRequestRecord = new CSCAP.API_1.MultipleSendApprovalRequestRecord();
						tmpMultipleSendApprovalRequestRecord.approvalObjectId=tmpOpportunity.Id;
						tmpMultipleSendApprovalRequestRecord.recipientContactId=recepientId;
						tmpMultipleSendApprovalRequestRecord.clickApproveSettingId=GetSiteApprovalSettingId();
						tmpMultipleSendApprovalRequestRecord.attachmentIds = setAttachmentId;
						tmpMultipleSendApprovalRequestRecord.documentIds= documentIds.isEmpty() ? null : documentIds;
						tmpMultipleSendApprovalRequestRecord.cc=null;
						tmpMultipleSendApprovalRequestRecord.bcc=null;
						
						system.debug('***tmpMultipleSendApprovalRequestRecord=' + tmpMultipleSendApprovalRequestRecord);
						
						lstMultipleSendApprovalRequestRecord.add(tmpMultipleSendApprovalRequestRecord);
						
					}
					
					tmpOpportunity.LG_AutomatedQuoteDelivery__c='Quote Sent';
					//tmpOpportunity.StageName='Quotation Delivered';
				}
				
				system.debug('***lstOpportunity=' + lstOpportunity);
				system.debug('***lstMultipleSendApprovalRequestRecord=' + lstMultipleSendApprovalRequestRecord);
				
				update lstOpportunity;
				
				
				if (lstMultipleSendApprovalRequestRecord.size() > 0 && !Test.isRunningTest())
					CSCAP.API_1.MultipleSendApprovalRequest(lstMultipleSendApprovalRequestRecord);
					
					
			}
    	}
    }
    
    
    public static void BeforeUpdateHandle(List<Attachment> lstAttachmentNew, List<Attachment> lstAttachmentOld)
    {
    	
    }
    
    public static void BeforeInsertHandle(List<Attachment> lstAttachmentNew)
    {

    }
}