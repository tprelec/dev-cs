@isTest
private class LG_AttachmentTriggerHandlerTest {
    
    private static testmethod void testSetAddressAndInstallationWishDate(){
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];

        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        simpleUser.LG_SalesChannel__c = 'D2D';
        insert simpleUser;

        System.runAs (simpleUser) {
        Test.startTest();

            CSCAP__Click_Approve_Setting__c csa = new CSCAP__Click_Approve_Setting__c();
            csa.name = 'Opportunity';
            insert csa;
            
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;
	        
	        Opportunity testOpp = CS_DataTest.createZiggoOpportunity(testAccount, 'Test Opp', simpleUser.Id);
    	    testOpp.LG_AutomatedQuoteDelivery__c = 'Quote Requested';
    	    insert testOpp;
            
            Blob bodyBlob = Blob.valueOf('this is a test');
            
            Attachment att = new Attachment();
            att.Name = 'Test Attachment';
    		att.body = bodyBlob;
    		att.ContentType = 'pdf';
    		att.IsPrivate = false;
    		att.ParentId = testOpp.Id;
    		// insert att;
    		
    		List<Attachment> attList= new List<Attachment>();
    		attList.add(att);
    		
    		Contact testContact = LG_GeneralTest.CreateContact(testAccount,'Test', 'Test', 'Ms', '', '', 'test@test.com', 'Administrative Contact', null, 'Utrecht', 'Netherlands', '3572RE', 'Lestraat');
            upsert testContact;
            
            OpportunityContactRole oppContactRole = LG_GeneralTest.CreateOpportunityContactRole(testAccount, testOpp, testContact, 'Administrative Contact', true, true);
            oppContactRole.ContactId = testContact.Id;
            upsert oppContactRole;
            
            LG_AttachmentTriggerHandler.BeforeInsertHandle(attList);
            LG_AttachmentTriggerHandler.BeforeUpdateHandle(attList, attList);
            LG_AttachmentTriggerHandler.AfterUpdateHandle(attList, attList);

    		insert attList;

        Test.stopTest();
        }
    }
}