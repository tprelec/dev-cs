public Class LG_CheckRecursive {

  private static boolean runPBInsert = true;
  public static boolean runPBOnceInsert() {
    if (runPBInsert ) {
      runPBInsert = false;
      return true;
    } else {
      return runPBInsert ;
    }
  }
  
  private static boolean runPBUpdate = true;
  public static boolean runPBOnceUpdate() {
    if (runPBUpdate ) {
      runPBUpdate = false;
      return true;
    } else {
      return runPBUpdate ;
    }
  }
    
  private static boolean runPBDelete = true;
  public static boolean runPBOnceDelete() {
    if (runPBDelete ) {
      runPBDelete = false;
      return true;
    } else {
      return runPBDelete ;
    }
  }
  
  private static boolean runSEPA = true;
  public static boolean runSEPAOnce() {
    if (runSEPA) {
      runSEPA = false;
      return true;
    } else {
      return runSEPA ;
    }
  }
  
  private static boolean countInvalidatedPremises = true;
  public static boolean countInvalidatedPremisesOnce() {
    if (countInvalidatedPremises) {
      countInvalidatedPremises = false;
      return true;
    } else {
      return countInvalidatedPremises ;
    }
  }
  
  private static boolean runPEAL = true;
  public static boolean runPEALOnce() {
    if (runPEAL) {
      runPEAL = false;
      return true;
    } else {
      return runPEAL ;
    }
  }
}