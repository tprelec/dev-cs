public class LG_CongaQuotesController {

    //public HTTPResponse response{get;set;}
    public String abc{get;set;}
    private String oppId;
    private Opportunity opp;
    public String url {get;set;}
    public String confirmationUrl {get;set;}
    private String dateStr ;   
    private String sessionId='';
    private User userDetails;
    public String myMsg{get;set;}
    private String congaResponse = '';
    private String congaResponseBody='';
    private String congaResponseBodySC='';
    public String selectedvalue{get;set;}
    public String url1{get;set;}
    public List<SelectOption> cTemps{get;set;}
    //Start CATGOV-330
    public List<SelectOption> quoteOptions{get;set;}
    public String selectedQuoteOption{get;set;}
    public List<Attachment> attachment{get;set;}
    public Attachment attachmentDetails;
    private string attachmentFileName;
    //End CATGOV-330
    public Boolean flagSuccess {get;set;}
    public Boolean flagSendConfirmation{get;set;}
    public Boolean flagError {get;set;}
    public Attachment congaAttachedSf{get;set;}
    public List<APXTConga4__Conga_Template__c> congaTemps{get;set;}

    public LG_CongaQuotesController () {   
        if(Apexpages.currentPage().getParameters().get('id') != null) {

            Id userId = userinfo.getUserId();

            flagSuccess = false;
            flagError = false;
            flagSendConfirmation = false;

            userDetails = [SELECT Id, Profile.Name, UserRole.Name FROM User where Id=:userId ];

            system.debug('Profile Name: ' + userDetails.Profile.Name);
            system.debug('Role Name: ' + userDetails.UserRole.Name);
            
            DateTime todayDate = Date.Today() ;
            dateStr =  todayDate.format('dd.MM.yyyy') ;

            oppId = Apexpages.currentPage().getParameters().get('id');
            sessionId = UserInfo.getSessionId();
            
            String query = 'Select Id ,LG_PrimaryContact__c,OwnerId, Account.LG_AccountNumber__c, LG_SignedQuoteAvailable__c, LG_SignedQuoteAttachmentId__c, csordtelcoa__Change_Type__c  from Opportunity Where Id=\'' + oppId + '\'';
            opp = Database.query(query);

            String TemGroup = 'Quote_Templates';
            String congaQuery = 'Select id,APXTConga4__Name__c from APXTConga4__Conga_Template__c where APXTConga4__Template_Group__c=: TemGroup';
            congaTemps= Database.query(congaQuery);

            cTemps = new List<SelectOption>();

            for(APXTConga4__Conga_Template__c c: congaTemps)
                cTemps.add(new SelectOption(c.id,c.APXTConga4__Name__c )); 

            //Start CATGOV-330
            //attachment = [SELECT Id, Description, ParentId FROM Attachment where Description='Signed Quote' and ParentId = :oppId];
            quoteOptions = new List<SelectOption>();

            if(opp.LG_SignedQuoteAvailable__c == 'true'){
                quoteOptions.add(new SelectOption('Send new confirmation (without signed quote)','Send new confirmation (without signed quote)')); 
                quoteOptions.add(new SelectOption('Send signed quote','Send signed quote')); 
            } else {
                quoteOptions.add(new SelectOption('Send new confirmation (without signed quote)','Send new confirmation (without signed quote)'));
            }
            //End CATGOV-330              
        }
    }
     
   public void callConga() {

       LG_EnvironmentVariables__c CS = LG_EnvironmentVariables__c.getInstance();
       System.debug(' inside call out '); 

        url = 'https://composer.congamerge.com/composer8/index.html?SessionId=' + UserInfo.getSessionId() + '&ServerURL=' + cs.LG_SalesforceBaseURL__c+'/services/Soap/u/29.0/' + UserInfo.getOrganizationId() +'&id='+opp.Id +         
                '&QueryId='+cs.LG_CongaQueryID1__c+','+
                cs.LG_CongaQueryID2__c+','+
                cs.LG_CongaQueryID3__c+','+
                cs.LG_CongaQueryID4__c+','+
                cs.LG_CongaQueryID5__c+','+
                cs.LG_CongaQueryID6__c+','+ 
                cs.LG_CongaQueryID7__c+','+ 
                cs.LG_CongaQueryID8__c+','+
                '[Query10]'+cs.LG_CongaQueryID10__c+','+
                '[Query11]'+cs.LG_CongaQueryID11__c+','+
                '[Query12]'+cs.LG_CongaQueryID12__c+','+      
                '[Query13]'+cs.LG_CongaQueryID13__c+','+
                '[Query14]'+cs.LG_CongaQueryID14__c+','+
                '[Query15]'+cs.LG_CongaQueryID15__c+','+
                '[Query16]'+cs.LG_CongaQueryID16__c+','+
                '[Query17]'+cs.LG_CongaQueryID17__c+','+
                '[Query18]'+cs.LG_CongaQueryID18__c+
                '&TemplateId='+selectedvalue+
                '&SelectTemplates=0'+
                '&DefaultPDF=1'+
                '&OFN=Uw_zakelijke_offerte_van_Ziggo'+dateStr+
                '&FP0=1'+
                '&DS2=1'+
                '&DS3=1&DS7=3'+
                '&SC0=1'+
                '&SC1=Attachments'
                    +'&APIMode=1'; 
               
        try
        {
            System.debug('url: ' + url);   
            System.debug(UserInfo.getOrganizationId().substring(0, 15) + ' ' +  UserInfo.getSessionId().substring(15));  
                            
            HttpRequest req;
            req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('GET');
            req.setTimeout(120000);
            
            Http objHttp = new Http();
            HTTPResponse res = objHttp.send(req);
            congaResponseBody = res.getBody();
            String responseText = res.getBody();

            System.debug('*** congaResponseBody: ' + congaResponseBody);
            System.debug(res .getStatus() + ':' + res .getStatusCode());
            System.debug('congaResponseBody: ' + congaResponseBody);

            congaAttachedSf=[Select id,name from Attachment where id=:congaResponseBody];           

            if(congaResponseBody.equalsIgnoreCase(congaAttachedSf.id))
            {                   
                flagSuccess=True;
                myMsg='Sucessfully Quote Created and attached to Opportunity with Attachment ID: '+congaAttachedSf.id;
            }
            else
            {
                flagError=true;
                myMsg='Conga Composer Response: '+congaResponseBody; 
            }   
        }
        catch(Exception e)
        {
            flagError=true;
            myMsg='Unable to Perform Operation -Exception :'+ e.getMessage() ;
        }            
    }

    /*
    */
    private boolean senConfirmationValidation(Opportunity in_opp){
        list<Contact> lst_contact = new list<Contact> ();
        list<cscfga__Product_Basket__c> lst_prodBasket = new list<cscfga__Product_Basket__c> ();

        if(opp==null && in_opp!=null)
            opp = in_opp;

        lst_contact = [SELECT Id, Email FROM Contact WHERE Id=:opp.LG_PrimaryContact__c AND Email!=null];
        if(lst_contact.isEmpty()){
            flagError=true;
            myMsg='Please check if primary contact is added and has a valid email address' ;
            return false;
        }

        lst_prodBasket = [SELECT Id, csordtelcoa__Synchronised_with_Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c=:opp.Id AND csordtelcoa__Synchronised_with_Opportunity__c=TRUE];
        if(lst_prodBasket.isEmpty()){
            flagError=true;
            myMsg='No product basket synchronized' ;
            return false;
        }

        if(opp.csordtelcoa__Change_Type__c!=null && opp.csordtelcoa__Change_Type__c!='Migrate'){
            flagError=true;
            myMsg='Can only be send for new or migrate order' ;
            return false;

        }

        return true;
    }

    public void callCongaConfirmationMail() {

        LG_EnvironmentVariables__c CS = LG_EnvironmentVariables__c.getInstance();
        List<String> attachmentsIds;

        System.debug(' inside call out '); 
        System.debug('***** selected option for confirmation: ' + selectedQuoteOption);
        system.debug('*** attachment: ' +  attachmentDetails);
        
        if (String.isEmpty(selectedQuoteOption)) {
            flagError = true;
            myMsg = 'Please select an option before clicking Send button.';
            return;
        }

        flagSendConfirmation = true;

        //Start CATGOV-330 (only if condition)
        if(selectedQuoteOption == 'Send new confirmation (without signed quote)') {

            if(!senConfirmationValidation(opp)) return;

            //End CATGOV-330
            confirmationUrl = 'https://composer.congamerge.com/composer8/index.html?SessionId=' + UserInfo.getSessionId() + '&ServerURL=' + cs.LG_SalesforceBaseURL__c+'/services/Soap/u/29.0/' + UserInfo.getOrganizationId() +'&id='+opp.Id +         
                '&QueryId='+cs.LG_CongaQueryID1__c+','+
                cs.LG_CongaQueryID2__c+','+
                cs.LG_CongaQueryID3__c+','+
                cs.LG_CongaQueryID4__c+','+
                cs.LG_CongaQueryID5__c+','+
                cs.LG_CongaQueryID6__c+','+ 
                cs.LG_CongaQueryID7__c+','+ 
                cs.LG_CongaQueryID8__c+','+ 
                '[Query10]'+cs.LG_CongaQueryID10__c+','+
                cs.LG_CongaQueryID11__c+','+ 
                cs.LG_CongaQueryID12__c+','+ 
                cs.LG_CongaQueryID13__c+','+ 
                cs.LG_CongaQueryID14__c+','+ 
                cs.LG_CongaQueryID15__c+','+
                '[Query16]'+cs.LG_CongaQueryID16__c+','+
                '[Query17]'+cs.LG_CongaQueryID17__c+','+
                '[Query18]'+cs.LG_CongaQueryID18__c+
                '&CongaEmailTemplateId='+cs.LG_OrderConfirmationEmailTemplateId__c+
                //'&TemplateId=aBB3N0000004C9g'+
                '&TemplateId='+cs.LG_NoSignedConfirmationEmailTemplateId__c+
                '&EmailToID='+opp.LG_PrimaryContact__c+ 
                '&EmailTemplateAttachments=1'+ 
                '&DefaultPDF=1'+ 
                '&OFN=Bevestiging_van_uw_bestelling_bij_Ziggo'+dateStr+ 
                '&DS7=12'+ 
                '&AC0=1'+ 
                '&SC0=1'+ 
                '&SC1=Attachments'+ 
                '&Ac1=Order_Confirmation_Email_Sent'+
                '&LGWhoId='+opp.LG_PrimaryContact__c+
                '&Qmode=SendEmail'+
                '&APIMode=12';
            
            } else { 

            //Start CATGOV-330                           
            confirmationUrl = 'https://composer.congamerge.com/composer8/index.html?SessionId=' + UserInfo.getSessionId() + '&ServerURL=' + cs.LG_SalesforceBaseURL__c+'/services/Soap/u/29.0/' + UserInfo.getOrganizationId() +'&id='+opp.Id +         
            '&QueryId='+cs.LG_CongaQueryID1__c+','+
                cs.LG_CongaQueryID2__c+','+
                cs.LG_CongaQueryID3__c+','+
                cs.LG_CongaQueryID4__c+','+
                cs.LG_CongaQueryID5__c+','+
                cs.LG_CongaQueryID6__c+','+ 
                cs.LG_CongaQueryID7__c+','+ 
                cs.LG_CongaQueryID8__c+','+ 
                '[Query10]'+cs.LG_CongaQueryID10__c+','+
                cs.LG_CongaQueryID11__c+','+ 
                cs.LG_CongaQueryID12__c+','+ 
                cs.LG_CongaQueryID13__c+','+ 
                cs.LG_CongaQueryID14__c+','+ 
                cs.LG_CongaQueryID15__c+','+
                '[Query16]'+cs.LG_CongaQueryID16__c+','+
                '[Query17]'+cs.LG_CongaQueryID17__c+','+
                '[Query18]'+cs.LG_CongaQueryID18__c+
                '&CongaEmailTemplateId='+cs.LG_SignedConfirmationEmailTemplateId__c+
                '&AttachmentId='+opp.LG_SignedQuoteAttachmentId__c+ 
                '&EmailToID='+opp.LG_PrimaryContact__c+ 
                '&EmailTemplateAttachments=1'+
                '&AC0=1'+ 
                '&Ac1=Order_Confirmation_Email_Sent_with_signed_quote'+
                '&LGWhoId='+opp.LG_PrimaryContact__c+
                +'&Qmode=SendEmail&APIMode=12';       
            }

        //End CATGOV-330            
        System.debug('Curl: ' + confirmationUrl);

        try
        {                  
            HttpRequest req;
            req = new HttpRequest();
            req.setEndpoint(confirmationUrl);
            req.setMethod('GET');
            req.setTimeout(120000);
            
            Http objHttp = new Http();
            HTTPResponse res = objHttp.send(req);

            congaResponseBodySC = res.getBody();
            System.debug('Response body: ' + res.getBody());

            List<Attachment> congaAttachedSfSendConfirmation;
            
            if(res.getBody() != 'done') {
                attachmentsIds = congaResponseBodySC.split(',');
                congaAttachedSfSendConfirmation = [SELECT Id, Name FROM Attachment WHERE Id IN :attachmentsIds];
            }
        
            //Attachment congaAttachedSfSendConfirmation=[Select id,name from Attachment where id=:congaResponseBodySC ];  
            if(congaResponseBodySC == 'done' || !congaAttachedSfSendConfirmation.isEmpty())
            {                   
                flagSuccess = True;
                myMsg = 'Confirmation Mail is Successfully Sent';
            }
            else
            {
                flagError = true;
                myMsg = 'Conga Composer Response: ' + congaResponseBody; 
            }   
        }
        catch(Exception e)
        {
            flagError = true;
            myMsg = 'Unable to Perform Operation -Exception: ' + e.getMessage();
        }
                        
    }

    
    public String getCongaResponseBody () {
        return congaResponseBody;
    }

    public String getSessionId () {
        return sessionId;
    }

    public id getOppId () {
        return oppid;
    }
}