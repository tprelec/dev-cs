@isTest
private class LG_CongaQuotesControllerTest{
  
    @testSetup
    static void setup() {

        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;

        Account acc = new Account(
            name = 'accName',
            KVK_number__c = '12345677',
            LG_Footprint__c = 'Ziggo'
        ); 
        insert acc;

        Opportunity opp = LG_GeneralTest.CreateOpportunity(acc);
        
    }

    @isTest
    static void callCongaTest() {   

        Opportunity oppor=[Select id from Opportunity LIMIT 1];

        PageReference pageRef = Page.LG_CongaQuote;
        Test.setCurrentPage(pageRef);

        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppor);
        ApexPages.currentPage().getParameters().put('id',oppor.id);
        
        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorConga());
        LG_CongaQuotesController ac=new LG_CongaQuotesController();         
        ac.callConga();

        Test.stopTest();

        //System.assertEquals(UserInfo.getSessionId(), ac.getSessionId());
        System.assertEquals(oppor.id, ac.getOppId());
        
    }
    
    @isTest
    static void callCongaConfirmationMailTest() {
        
        Opportunity oppor = [Select id from Opportunity LIMIT 1];

        PageReference pageRef = Page.LG_CongaQuote;
        Test.setCurrentPage(pageRef);

        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppor);
        ApexPages.currentPage().getParameters().put('id', oppor.id);

        Test.startTest();

            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorConga());

            LG_CongaQuotesController ac = new LG_CongaQuotesController();
            ac.confirmationUrl = 'test@test.com';         
            ac.attachment = null;
            ac.attachmentDetails = null;
            ac.callCongaConfirmationMail(); 

        Test.stopTest();

        //System.assertEquals(UserInfo.getSessionId(), ac.getSessionId());
        System.assertEquals(oppor.id, ac.getOppId());
        
    }
    
    @isTest
    static void callCongaConfirmationMailSignedQuoteTest() {
        
        Account acc= new Account(name='accName', KVK_number__c ='12345678',LG_Footprint__c = 'Ziggo'); 
        insert acc;

        Opportunity opp1 = LG_GeneralTest.CreateOpportunity(acc);
        opp1.LG_SignedQuoteAvailable__c = 'true';
        update opp1;

        Opportunity oppor = [Select id from Opportunity where id = :opp1.id];

        PageReference pageRef = Page.LG_CongaQuote;
        Test.setCurrentPage(pageRef);

        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppor);
        ApexPages.currentPage().getParameters().put('id',oppor.id);

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorConga());

        LG_CongaQuotesController ac = new LG_CongaQuotesController();   
        ac.selectedQuoteOption = 'Send signed quote';
        ac.callCongaConfirmationMail(); 

        Test.stopTest();

        //System.assertEquals(UserInfo.getSessionId(), ac.getSessionId());
        System.assertEquals(oppor.id, ac.getOppId());
        
    }
    
    @isTest
    static void callCongaConfirmationWithoutSignedQuoteTest() {
        
        Account acc= new Account(name='accName', KVK_number__c ='12345678',LG_Footprint__c = 'Ziggo'); 
        insert acc;

        Contact tmpContact = LG_GeneralTest.CreateContact(acc, 'Johan', 'De Vries', 'Mr.', '06-55932220', '', 'johan.devries@test.com', 'Business User', Date.newinstance(1960, 2, 17), 'MONNICKENDAM', 'Netherlands', '1141 AZ', 'HAVEN 14');
        
        Opportunity opp1=LG_GeneralTest.CreateOpportunity(acc);
        opp1.LG_SignedQuoteAvailable__c = 'true';
        opp1.LG_PrimaryContact__c = tmpContact.Id;
        opp1.csordtelcoa__Change_Type__c = 'Add';
        update opp1;

        Opportunity oppor=[Select id from Opportunity where id = :opp1.id];
        PageReference pageRef = Page.LG_CongaQuote;
        Test.setCurrentPage(pageRef);

        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppor);
        ApexPages.currentPage().getParameters().put('id',oppor.id);

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorConga());

        LG_CongaQuotesController ac=new LG_CongaQuotesController();  
        ac.selectedQuoteOption = 'Send new confirmation (without signed quote)';
        ac.callCongaConfirmationMail();
        
        String body = ac.getCongaResponseBody();
        String session = ac.getSessionId();

        Test.stopTest();

        //System.assertEquals(UserInfo.getSessionId(), ac.getSessionId());
        System.assertEquals(oppor.id, ac.getOppId());
        
    }
    
    @isTest
    static void callCongaConfirmationWithoutSignedQuoteTestNoMail() {
        
        Account acc= new Account(name='accName', KVK_number__c ='12345678',LG_Footprint__c = 'Ziggo'); 
        insert acc;

        Contact tmpContact = LG_GeneralTest.CreateContact(acc, 'Johan', 'De Vries', 'Mr.', '06-55932220', '', '', 'Business User', Date.newinstance(1960, 2, 17), 'MONNICKENDAM', 'Netherlands', '1141 AZ', 'HAVEN 14');
        Opportunity opp1=LG_GeneralTest.CreateOpportunity(acc);
        opp1.LG_SignedQuoteAvailable__c = 'true';
        opp1.LG_PrimaryContact__c = tmpContact.Id;
        opp1.csordtelcoa__Change_Type__c = 'Add';
        update opp1;

        Opportunity oppor=[Select id from Opportunity where id = :opp1.id];
        
        PageReference pageRef = Page.LG_CongaQuote;
        Test.setCurrentPage(pageRef);

        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppor);
        ApexPages.currentPage().getParameters().put('id',oppor.id);

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorConga());

        LG_CongaQuotesController ac=new LG_CongaQuotesController();  
        ac.selectedQuoteOption = 'Send new confirmation (without signed quote)';
        ac.abc = 'abc';
        ac.url1 = 'url1';
        ac.callCongaConfirmationMail();

        String body = ac.getCongaResponseBody();
        String session = ac.getSessionId();

        Test.stopTest();

        //System.assertEquals(UserInfo.getSessionId(), ac.getSessionId());
        System.assertEquals(oppor.id, ac.getOppId());
        
    }
    
}