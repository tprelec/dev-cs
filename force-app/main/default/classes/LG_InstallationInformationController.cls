/**
* Used as a controller for LG_InstallationInformation VF page.
*
* @author Tomislav Blazek
* @ticket SFDT-221
* @since  11/02/2016
*/
public with sharing class LG_InstallationInformationController {

    private static final String REDIRECT_URL = '/lightning/o/Contact/new?defaultFieldValues=AccountId=';
    private static final String CONTACT_PREFIX = Contact.getSObjectType().getDescribe().getKeyPrefix();

    public cscfga__Product_Basket__c basket {get; set;}
    public List<cscrm__Address__c> premises {get; set;}
    public Map<Id, cscrm__Site__c> premiseIdToSite {get; set;}
    public Map<Id, Integer> premiseMinLeadTime {get; set;}
    public Map<Id, Date> premiseMinInstallDate {get; set;}
    public Map<Id, String> premiseWishDate {get; set;}
    public Map<Id, String> premisePlannedDate {get; set;}
    public String premiseIdToPlannedDates {get; set;}
    public String premiseIdToWishDates {get; set;}
    public Map<Id, String> premiseMarketSegment {get; set;}
    public cscrm__Address__c dummyForTechContact {get; set;}
    private Id basketId;
    private Id returnId;
    private Id newContactId;
    private Id opportunityId;
    //BU-18 Start
    private boolean overStappenFlag=false;
    Integer OverstappenLeadTime=30;

    //BU-18 End
    private Map<Id, List<cscfga__Product_Configuration__c>> premiseToProdConfs {get; set;}
    private Map<Id, Id> prodConfToPremise {get; set;}
    private Map<Id, Id> prodConfToSite {get; set;}
    private LG_InstallationInformationSettings__c insInfoSettings;
    private List<cscrm__Site__c> sites {get; set;}
    //BU-18 Start
    public Map<String,String> overstappenToConfig=new Map<String,String>();

    //fetches the installation information fields - fields dependent on the profile/custom settings
    public List<Schema.FieldSetMember> getInstallationInfoFields()
    {
        Schema.FieldSet infoFieldSet = Schema.SObjectType.cscrm__Site__c.fieldSets.getMap().get(insInfoSettings.LG_SiteFieldSetName__c);
        return infoFieldSet != null ? infoFieldSet.getFields() : new List<Schema.FieldSetMember>();
    }


    public String getNewContactUrl() {
        // Business wants this to be working on lightning
        return REDIRECT_URL + basket.csbb__Account__r.Id;
    }

    public PageReference createNewContact() {
        PageReference newContactPage = new PageReference('/' + CONTACT_PREFIX + '/e');
        String accFieledReference = LG_Util.getLookupFieldReferenceId(newContactPage,
                                                                      Account.getSObjectType().getDescribe().getKeyPrefix());
        String pageURL = '/apex/LG_InstallationInformation?basketId=';
        newContactPage.getParameters().put('saveURL', pageURL + basketId);
        newContactPage.getParameters().put(accFieledReference, basket.csbb__Account__r.Name);
        newContactPage.getParameters().put(accFieledReference + '_lkid', basket.csbb__Account__r.Id);
        newContactPage.getParameters().put('retURL', pageURL + basketId);
        return newContactPage;
    }


    public LG_InstallationInformationController() {
        basketId = ApexPages.currentPage().getParameters().get('basketId');
        returnId = ApexPages.currentPage().getParameters().get('returnId');
        newContactId = ApexPages.currentPage().getParameters().get('newid');
        insInfoSettings = LG_InstallationInformationSettings__c.getInstance(UserInfo.getUserId());

        premises = new List<cscrm__Address__c>();
        sites = new List<cscrm__Site__c>();
        premiseIdToSite = new Map<Id, cscrm__Site__c>();

        if (String.isBlank(returnId))
        {
            returnId = basketId;
        }


        if (basketId != null)
        {
            basket = [SELECT Id, csordtelcoa__Change_Type__c, Name, cscfga__Opportunity__c,
                      csbb__Account__r.Name, csbb__Account__r.Id
                      FROM cscfga__Product_Basket__c
                      WHERE Id = :basketId];
            opportunityId = basket.cscfga__Opportunity__c;
            //BU-18 Start
            List<cscfga__Product_Configuration__c> prodconfigIds= [Select Id from cscfga__Product_Configuration__c where
                                                                   cscfga__Product_Basket__c = :basketId];
            List<cscfga__Attribute__c> attr=    [SELECT Name,cscfga__Display_Value__c,cscfga__Product_Configuration__c FROM cscfga__Attribute__c where Name='Overstappen'AND
                                                 cscfga__Product_Configuration__c IN: prodconfigIds];


            if(!attr.isEmpty() && attr.size()>0)
                for (cscfga__Attribute__c a: attr)
            {

                overstappenToConfig.put(a.cscfga__Product_Configuration__c,a.cscfga__Display_Value__c);
            }






            //fetch the product configuration - premises
            premises = [SELECT Id, LG_FullAddressDetails__c, cscrm__Account__c,
                        LG_SharedOfficeBuilding__c, LG_TechnicalContact__c,
                        (SELECT Id, LG_InstallationClass__c, LG_MarketSegment__c,
                         LG_InstallationLeadTime__c, LG_InstallationNeeded__c,
                         LG_InstallationWishDate__c, LG_InstallationPlannedDate__c,
                         LG_Site__c
                         FROM Product_Configurations_LG__r
                         WHERE cscfga__Product_Basket__r.Id = :basketId
                         AND LG_InstallationNeeded__c = true)
                        FROM cscrm__Address__c
                        WHERE Id IN (SELECT LG_Address__c
                                     FROM cscfga__Product_Configuration__c
                                     WHERE cscfga__Product_Basket__r.Id = :basketId
                                     AND LG_InstallationNeeded__c = true
                                     AND LG_Address__c != null)
                        ORDER BY LG_FullAddressDetails__c];

            if (!premises.isEmpty())
            {
                dummyForTechContact = new cscrm__Address__c(Id = premises[0].Id, LG_TechnicalContact__c = null,
                                                            cscrm__Account__c = premises[0].cscrm__Account__c);


                if (String.isNotBlank(newContactId))
                {
                    dummyForTechContact.LG_TechnicalContact__c = newContactId;
                }
            }


            premiseMinLeadTime = new Map<Id, Integer>();
            premiseMinInstallDate = new Map<Id, Date>();
            premiseWishDate = new Map<Id, String>();
            premisePlannedDate = new Map<Id, String>();
            premiseToProdConfs = new Map<Id, List<cscfga__Product_Configuration__c >>();
            prodConfToPremise = new Map<Id, Id>();
            prodConfToSite = new Map<Id, Id>();
            premiseMarketSegment = new Map<Id, String>();


            for(cscrm__Address__c premise : premises)
            {
                overstappenflag=false;
                Map<String, Integer> classToLeadTime = new Map<String, Integer>();

                for(cscfga__Product_Configuration__c prodConf : premise.Product_Configurations_LG__r)


                {

                    //if there is no lead time, prod configuration is  not relevant
                    if (prodConf.LG_InstallationLeadTime__c == null)
                    {
                        continue;
                    }

                    String installClass = prodConf.LG_InstallationClass__c != null ? prodConf.LG_InstallationClass__c : '';
                    //BU-18- start Add 30 days lead time for overstappen config
                    if((overstappenToConfig.containsKey(prodConf.id)) && (overstappenToConfig.get(prodConf.id)=='Yes'))
                    {
                        prodConf.LG_InstallationLeadTime__c=OverstappenLeadTime;
                        overStappenFlag=true;
                    }

                    if (classToLeadTime.containsKey(installClass))
                    {
                        //we want to catch the maximum Lead time in the same Installation class
                        if (prodConf.LG_InstallationLeadTime__c > classToLeadTime.get(installClass))
                        {

                            classToLeadTime.put(installClass, Integer.valueOf(prodConf.LG_InstallationLeadTime__c));


                        }
                    }
                    else
                    {

                        classToLeadTime.put(installClass, Integer.valueOf(prodConf.LG_InstallationLeadTime__c));


                    }
                }


                Integer minLeadTime = 9999;

                String minInstallClass = '';


                for(String installationClass : classToLeadTime.keySet())
                {
                    Integer installLeadTime = classToLeadTime.get(installationClass);


                    //We want to get the minimum lead time amongst all (max) lead time of all classes
                    if ((installLeadTime < minLeadTime) && (overStappenFlag==false))
                    {
                        minLeadTime = installLeadTime;
                        minInstallClass = installationClass;

                    }
                    //BU-18 start- To take the overstappen wish date with 30 days lead time if overstappen product is available in basket
                    else{
                        if(installLeadTime==OverstappenLeadTime){
                            minLeadTime = installLeadTime;
                            minInstallClass = installationClass;

                                                    }


                    }
                }




                minLeadTime = minLeadTime != 9999 ? minLeadTime : 0;

                premiseMinLeadTime.put(premise.Id, minLeadTime);
                premiseToProdConfs.put(premise.Id, new List<cscfga__Product_Configuration__c>());
                String marketSegment = '';


                for(cscfga__Product_Configuration__c prodConf : premise.Product_Configurations_LG__r)
                {
                    String installClass = prodConf.LG_InstallationClass__c != null ? prodConf.LG_InstallationClass__c : '';

                    //group prod confs of the same class
                    if (minInstallClass.equals(installClass))
                    {
                        premiseToProdConfs.get(premise.Id).add(prodConf);
                        prodConfToPremise.put(prodConf.Id, premise.Id);
                        prodConfToSite.put(prodConf.Id, prodConf.LG_Site__c);
                        premiseWishDate.put(premise.Id, getFormattedDate(prodConf.LG_InstallationWishDate__c));
                        premisePlannedDate.put(premise.Id, getFormattedDate(prodConf.LG_InstallationPlannedDate__c));
                        marketSegment = prodConf.LG_MarketSegment__c;

                    }
                }

                premiseMinInstallDate.put(premise.Id, getMinInstallDate(minLeadTime));
                premiseMarketSegment.put(premise.Id, marketSegment != null ? marketSegment : '');
            }

            prepareSitesList();
        }
    }

    //prepares the list of sites (a map) per address for the product configurations
    //if the product configurations have an existing site record related to it, the Site
    //record would be required, otherwise, creates a new site object
    private void prepareSitesList()
    {
        String sitesQuery = 'SELECT ';
        for(Schema.FieldSetMember f : this.getInstallationInfoFields())
        {
            sitesQuery += f.getFieldPath() + ', ';
        }

        Set<Id> siteIds = new Set<Id>();
        for(Id siteId : prodConfToSite.values())
        {
            siteIds.add(siteId);
        }

        sitesQuery += 'Id, cscrm__Installation_Address__c FROM cscrm__Site__c WHERE Id IN :siteIds';

        //fetch the product configuration - sites
        sites = Database.query(sitesQuery);

        for(Id premiseId : premiseToProdConfs.keySet())
        {
            for(cscrm__Site__c site : sites)
            {
                if (site.cscrm__Installation_Address__c == premiseId)
                {
                    premiseIdToSite.put(premiseId, site);
                }
            }
            if (!premiseIdToSite.containsKey(premiseId))
            {
                premiseIdToSite.put(premiseId, new cscrm__Site__c(cscrm__Installation_Address__c = premiseId));
            }
        }
    }

    // formats the date in the dd-mm-yyyy format
    private String getFormattedDate(Date value)
    {
        String formattedDate = '';

        if (value != null)
        {
            formattedDate = value.day() + '-' + value.month() + '-' + value.year();
        }

        return formattedDate;
    }

    //Calculates the minimum install date: today() + min lead time (working days).
    private Date getMinInstallDate(Integer minLeadTime)
    {
        Date minInstallDate = Date.today();

        //first check if today is a weekend, if so
        //set the minInstallDate first to Monday...
        DateTime dtMinDate = (DateTime) minInstallDate;
        String dayOfWeek = dtMinDate.format('EEE');

        //CATGOV-505, Overstappen logic is added to change min wish date from 30 working days to 30 calendar days
        if(overStappenFlag==false)
        {
            if  (dayOfWeek.equals('Sat'))
            {
                minInstallDate = minInstallDate.addDays(2);
            }
            else if (dayOfWeek.equals('Sun'))
            {
                minInstallDate = minInstallDate.addDays(1);
            }
        }


        //Now that we are sure that start ('today') is
        //a working day - Mon-Fri, add the min lead time
        //and don't take Sat and Sun in calculation
        for (Integer i = 0; i < minLeadTime; i++)
        {
            minInstallDate = minInstallDate.addDays(1);

            //check if date now falls on weekend (Sat)
            //if so, add 2 days to make it Monday
            dtMinDate = (DateTime) minInstallDate;
            dayOfWeek = dtMinDate.format('EEE');
            //CATGOV-505 , Overstappen logic is added to change minimum wish date from 30 working days to 30 calendar days
            if  (overStappenFlag==false && dayOfWeek.equals('Sat'))
            {
                minInstallDate = minInstallDate.addDays(2);
            }
        }

        return minInstallDate;
    }

    /**
* Saves/Updates the Address/Premise records with the Installation Information data

*
* @author Tomislav Blazek
* @ticket SFDT-221
* @since  11/02/2016
*/
    public PageReference save()
    {
        update premises;

        saveSiteInstallationData();

        saveWishPlannedDates();

        system.debug('@@@@dummyForTechContact: ' + dummyForTechContact);

        if(dummyForTechContact != null) {
            createContactRoles(dummyForTechContact.LG_TechnicalContact__c, opportunityId);
        }

        return redirectToReturnId();
    }

    // Create opportunity contact roles based on the Role of selected technical contact
    private void createContactRoles(Id contactId, Id oppId) {

        List<Contact> contacts = [SELECT Id, LG_Role__c FROM Contact WHERE Id = :contactId];

        if(!contacts.isEmpty() && String.isNotEmpty(contacts[0].LG_Role__c)) {

            List<OpportunityContactRole> contactRoles = new List<OpportunityContactRole>();
            for(String role: contacts[0].LG_Role__c.split(';')) {
                String trimmedRole = role.trim();
                if(String.isNotEmpty(trimmedRole)) {
                    contactRoles.add(new OpportunityContactRole(
                        ContactId = contactId,
                        Role = trimmedRole,
                        OpportunityId = oppId
                    ));
                }
            }
            if(!contactRoles.isEmpty()) {
                delete [SELECT Id FROM OpportunityContactRole WHERE OpportunityId = :oppId];
                insert contactRoles;
            }
        }
    }

    //saves the installation data entered in the installation information fields (from the profile
    //based fieldsets)
    //upserts the Sites records and relates them to the Product configuration (and its atttribute Site Id)
    //if needed.
    private void saveSiteInstallationData()
    {
        Set<Id> premiseIds = new Set<Id>();

        for(cscrm__Site__c site : premiseIdToSite.values())
        {
            if (site.Id == null)
            {
                premiseIds.add(site.cscrm__Installation_Address__c);
            }
        }

        if (!premiseIds.isEmpty())
        {
            List<cscrm__Site__c> existingSitesToCheck = [SELECT Id, cscrm__Installation_Address__c,

                                                         cscrm__Suite_Room__c
                                                         FROM cscrm__Site__c
                                                         WHERE cscrm__Installation_Address__c IN :premiseIds];

            for(cscrm__Site__c site : premiseIdToSite.values())
            {
                //if sites id is null, that means it previously wasn't linked to the address on this page
                //if the existing - queried site - has the same installation address and the room number (roomnumber used
                //as part of an 'id' for matching) that we have to link the Id of the existing record to the Site object, so
                //it could be properly upserted
                if (site.Id == null)
                {
                    for(cscrm__Site__c existingSite : existingSitesToCheck)
                    {
                        if (existingSite.cscrm__Installation_Address__c == site.cscrm__Installation_Address__c
                            && existingSite.cscrm__Suite_Room__c == site.cscrm__Suite_Room__c)
                        {
                            site.Id = existingSite.Id;
                            break;
                        }
                    }
                }
            }
        }

        upsert premiseIdToSite.values();


        Set<cscfga__Product_Configuration__c> prodConfsSet = new Set<cscfga__Product_Configuration__c>();


        for(Id premiseId : premiseToProdConfs.keySet())
        {
            prodConfsSet.addAll(premiseToProdConfs.get(premiseId));
        }


        List<cscfga__Product_Configuration__c> prodConfs = [SELECT Id, LG_InstallationWishDate__c, LG_InstallationPlannedDate__c,





                                                            LG_Address__c, LG_Site__c,
                                                            (SELECT Name, cscfga__Value__c, cscfga__Display_Value__c
                                                             FROM cscfga__Attributes__r
                                                             WHERE Name IN ('Site Id')
                                                            )
                                                            FROM cscfga__Product_Configuration__c
                                                            WHERE Id IN :prodConfsSet];

        List<cscfga__Attribute__c> attributesToUpdate = new List<cscfga__Attribute__c>();
        List<cscfga__Product_Configuration__c> prodConfsToUpdate = new List<cscfga__Product_Configuration__c>();

        for(cscfga__Product_Configuration__c prodConf : prodConfs)
        {
            cscrm__Site__c relatedSite = premiseIdToSite.get(prodConf.LG_Address__c);
            if (prodConf.LG_Site__c != relatedSite.Id)
            {
                prodConf.LG_Site__c = relatedSite.Id;
                prodConfsToUpdate.add(prodConf);
            }

            for(cscfga__Attribute__c att : prodConf.cscfga__Attributes__r)
            {
                boolean updateAtt = false;
                if (att.Name.equals('Site Id') && att.cscfga__Value__c != relatedSite.Id)
                {
                    att.cscfga__Value__c = relatedSite.Id;
                    att.cscfga__Display_Value__c = relatedSite.Id;
                    updateAtt = true;
                }
                if (updateAtt)
                {
                    attributesToUpdate.add(att);
                }
            }
        }

        if (!attributesToUpdate.isEmpty())
        {
            update attributesToUpdate;
        }

        if (!prodConfsToUpdate.isEmpty())
        {
            update prodConfsToUpdate;
        }
    }

    //saves the wish date and planned dates on the product configurations of
    //the same class (and the same premise) and saves the same dates on the related
    //product configuration attributes
    private void saveWishPlannedDates()
    {

        Set<Id> premiseIds = new Set<Id>();
        Map<Id, Date> premiseIdToWishDate = new Map<Id, Date>();
        Map<Id, Date> premiseIdToPlannedDate = new Map<Id, Date>();


        System.debug('***** premiseIdToWishDates **** : '+ premiseIdToWishDates);
        for (String premiseDate : premiseIdToWishDates.split('#'))
        {
            System.debug('***** premiseDate **** : '+ premiseDate);
            if (String.isNotBlank(premiseDate))
            {
                Id premiseId = premiseDate.split('~')[0];
                Date wishDate = null;

                if (premiseDate.split('~').size() > 1)
                {
                    String wishDateStr = premiseDate.split('~')[1];
                    //date is in dd-mm-yyyy format so we should create a new instance properly formatted
                    wishDate = Date.newInstance(Integer.valueOf(wishDateStr.split('-')[2]),
                                                Integer.valueOf(wishDateStr.split('-')[1]),
                                                Integer.valueOf(wishDateStr.split('-')[0]));
                }

                //take the premise Id from the string
                premiseIds.add(premiseId);
                premiseIdToWishDate.put(premiseId, wishDate);
            }
        }


        for (String premiseDate : premiseIdToPlannedDates.split('#'))
        {
            if (String.isNotBlank(premiseDate))
            {
                Id premiseId = premiseDate.split('~')[0];
                Date plannedDate = null;

                if (premiseDate.split('~').size() > 1)
                {
                    String plannedDateStr = premiseDate.split('~')[1];
                    //date is in dd-mm-yyyy format so we should create a new instance properly formatted
                    plannedDate = Date.newInstance(Integer.valueOf(plannedDateStr.split('-')[2]),
                                                   Integer.valueOf(plannedDateStr.split('-')[1]),
                                                   Integer.valueOf(plannedDateStr.split('-')[0]));
                }

                //take the premise Id from the string
                premiseIds.add(premiseId);
                premiseIdToPlannedDate.put(premiseId, plannedDate);
            }
        }


        if (!premiseIds.isEmpty())
        {
            Set<cscfga__Product_Configuration__c> prodConfsSet = new Set<cscfga__Product_Configuration__c>();


            System.debug('****** premiseToProdConfs *** ' + premiseToProdConfs);
            for(Id premiseId : premiseIds)
            {
                if (premiseToProdConfs.containsKey(premiseId))
                {
                    prodConfsSet.addAll(premiseToProdConfs.get(premiseId));
                }
            }


            List<cscfga__Product_Configuration__c> prodConfs = [SELECT Id, LG_InstallationWishDate__c, LG_InstallationPlannedDate__c,
                                                                LG_Address__c, csordtelcoa__Replaced_Subscription__c,


                                                                (SELECT Name, cscfga__Value__c, cscfga__Display_Value__c
                                                                 FROM cscfga__Attributes__r
                                                                 WHERE Name IN ('Installation Planned Date', 'Installation Wish Date')

                                                                )
                                                                FROM cscfga__Product_Configuration__c
                                                                WHERE Id IN :prodConfsSet];

            List<cscfga__Attribute__c> attributesToUpdate = new List<cscfga__Attribute__c>();
            List<cscfga__Product_Configuration__c> prodConfsToUpdate = new List<cscfga__Product_Configuration__c>();


            for(cscfga__Product_Configuration__c prodConf : prodConfs)
            {
                Id premiseId = prodConfToPremise.get(prodConf.Id);

                Date wishDate = premiseIdToWishDate.get(premiseId);
                Date plannedDate = premiseIdToPlannedDate.get(premiseId);

                if (prodConf.LG_InstallationWishDate__c != wishDate || prodConf.LG_InstallationPlannedDate__c != plannedDate)
                {
                    if (prodConf.LG_InstallationWishDate__c != wishDate)
                    {
                        prodConf.LG_InstallationWishDate__c = wishDate;
                    }
                    if (prodConf.LG_InstallationPlannedDate__c != plannedDate)
                    {
                        prodConf.LG_InstallationPlannedDate__c = plannedDate;
                    }
                    prodConfsToUpdate.add(prodConf);
                }

                for(cscfga__Attribute__c att : prodConf.cscfga__Attributes__r)
                {
                    boolean updateAtt = false;
                    String wishDateValue = wishDate != null ? wishDate.year() + '-' + wishDate.month() + '-' + wishDate.day()

                        : null;
                    String plannedDateValue = plannedDate != null ? plannedDate.year() + '-' + plannedDate.month() + '-' + plannedDate.day()

                        : null;

                    if (att.Name.equals('Installation Wish Date') && att.cscfga__Value__c != wishDateValue)
                    {
                        att.cscfga__Value__c = wishDateValue;
                        att.cscfga__Display_Value__c = wishDate != null ? getFormattedDate(wishDate) : null;
                        updateAtt = true;
                    }
                    if (att.Name.equals('Installation Planned Date') && att.cscfga__Value__c != plannedDateValue)
                    {
                        att.cscfga__Value__c = plannedDateValue;
                        att.cscfga__Display_Value__c = plannedDate != null ? getFormattedDate(plannedDate) : null;
                        updateAtt = true;
                    }
                    if (updateAtt)
                    {
                        attributesToUpdate.add(att);
                    }
                }
            }


            if (!attributesToUpdate.isEmpty())
            {
                update attributesToUpdate;
            }


            if (!prodConfsToUpdate.isEmpty())
            {
                update prodConfsToUpdate;

                if(basket.csordtelcoa__Change_Type__c == 'Migrate'
                   || basket.csordtelcoa__Change_Type__c == 'Move'
                   || basket.csordtelcoa__Change_Type__c == 'Terminate')
                {
                    saveWishDateOnJunctionObject(prodConfsToUpdate, basket.csordtelcoa__Change_Type__c);
                }
            }
        }
    }

    /**
* Saves the wish date on the subscription_macdBasket junction object
* for the related records (based on the subscription address and the product configuration
* address)
*
* @param  List<cscfga__Product_Configuration__c> prodConfsToUpdate
* @param  String changeType - MACD Scenario change type
* @author Tomislav Blazek
* @ticket SFDT-220
* @since  17/3/2015
*/
    private void saveWishDateOnJunctionObject(List<cscfga__Product_Configuration__c> prodConfsToUpdate, String changeType)
    {
        Map<Id, csordtelcoa__Subscr_MACDProductBasket_Association__c> subBasketJuncsToUpdate
            = new Map<Id, csordtelcoa__Subscr_MACDProductBasket_Association__c>();


        List<csordtelcoa__Subscr_MACDProductBasket_Association__c> subBasketJuncs = [SELECT csordtelcoa__Subscription__r.LG_Address__c,
                                                                                     LG_DeactivationWishDate__c, csordtelcoa__Subscription__c
                                                                                     FROM csordtelcoa__Subscr_MACDProductBasket_Association__c
                                                                                     WHERE csordtelcoa__Product_Basket__c = :basketId];

        for(csordtelcoa__Subscr_MACDProductBasket_Association__c junction : subBasketJuncs)
        {
            for(cscfga__Product_Configuration__c prodConf : prodConfsToUpdate)
            {
                if ((changeType.equals('Migrate') && prodConf.LG_Address__c == junction.csordtelcoa__Subscription__r.LG_Address__c)
                    || (changeType.equals('Move') && prodConf.csordtelcoa__Replaced_Subscription__c == junction.csordtelcoa__Subscription__c)
                    || (changeType.equals('Terminate') && prodConf.csordtelcoa__Replaced_Subscription__c == junction.csordtelcoa__Subscription__c))
                {
                    junction.LG_DeactivationWishDate__c = prodConf.LG_InstallationWishDate__c;
                    subBasketJuncsToUpdate.put(junction.Id, junction);
                }
            }
        }

        if (!subBasketJuncsToUpdate.isEmpty())
        {
            update subBasketJuncsToUpdate.values();
        }
    }

    //back to return Id
    public PageReference redirectToReturnId() {
        PageReference newocp = new PageReference('/' + returnId);

        return newocp;
    }
}