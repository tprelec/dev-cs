@isTest
private class LG_MACDConfigurationControllerTest {

	private static String vfBaseUrl = 'vforce.url';
	private static String sfdcBaseUrl = 'sfdc.url';

	@testsetup
	private static void setupTestData()
	{
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		LG_EnvironmentVariables__c envVariables = new LG_EnvironmentVariables__c();
		envVariables.LG_SalesforceBaseURL__c = sfdcBaseUrl;
		envVariables.LG_VisualForceBaseURL__c = vfBaseUrl;
		envVariables.LG_CloudSenceAnywhereIconID__c = 'csaID';
		envVariables.LG_ServiceAvailabilityIconID__c = 'saIconId';
		insert envVariables;

		List<LG_RfsCheckVariables__c> rfsVariables = new List<LG_RfsCheckVariables__c>();
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'AnalogueTv', LG_Value__c = 'Catv'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DigitalTv', LG_Value__c = 'DMM'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownBasic', LG_Value__c = '130'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownFp200', LG_Value__c = '300'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownFp500', LG_Value__c = '500'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Fp200', LG_Value__c = 'UPC Fiber Power Internet'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Fp500', LG_Value__c = 'UPC_superfast'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Internet', LG_Value__c = 'UPC Internet'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Mobile', LG_Value__c = 'mobile'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'MobileInternet', LG_Value__c = 'mobile_internet'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Qos', LG_Value__c = 'QoS'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Telephony', LG_Value__c = 'telephony'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpBasic', LG_Value__c = '30'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpFp200', LG_Value__c = '40'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpFp500', LG_Value__c = '40'));
        //This variable added as part of CATGOV-1052 Start.
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'ZakelijkInternetGiga' , LG_Value__c = 'Ziggo_Giga'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'GigaDownFp1000' , LG_Value__c = '1000'));
	   //CATGOV-1052 End.
		insert rfsVariables;

		csbb__Callout_Service__c httpService = new csbb__Callout_Service__c();
		httpService.Name = 'RfsCheckCallout';
		httpService.csbb__End_Point__c = 'testendpoint';
		insert httpService;

		List<cscrm__Address__c> addresses = new List<cscrm__Address__c>();
		cscrm__Address__c addressOne = new cscrm__Address__c(Name='AddressOne', cscrm__Zip_Postal_Code__c = '10000', LG_HouseNumber__c = '34', LG_AddressID__c = '123456');
		cscrm__Address__c addressTwo = new cscrm__Address__c(Name='AddressTwo');
		addresses.add(addressOne);
		addresses.add(addressTwo);
		insert addresses;

		csordtelcoa__Change_Types__c changeTypechange = new csordtelcoa__Change_Types__c();
		changeTypechange.csordtelcoa__Sort_Order__c = 0;
		changeTypechange.Name = 'Change';
		insert changeTypechange;

		csordtelcoa__Change_Types__c changeTypeMove = new csordtelcoa__Change_Types__c();
		changeTypeMove.csordtelcoa__Sort_Order__c = 1;
		changeTypeMove.Name = 'Move';
		insert changeTypeMove;

		csordtelcoa__Change_Types__c changeTypeTerm = new csordtelcoa__Change_Types__c();
		changeTypeTerm.csordtelcoa__Sort_Order__c = 2;
		changeTypeTerm.Name = 'Terminate';
		insert changeTypeTerm;

		Account account = LG_GeneralTest.CreateAccount('Account', '12345678', 'Ziggo', true);

		csord__Order_Request__c coreq = new csord__Order_Request__c(csord__Module_Name__c = 'Test', csord__Module_Version__c = '1.0');
		insert coreq;

		csord__Subscription__c sub = new csord__Subscription__c(csord__Identification__c = 'TestIdent', csord__Order_Request__c = coreq.Id, Name = 'Product1',
																csord__Account__c = account.Id, LG_Address__c = addressOne.Id, csordtelcoa__Closed_Replaced__c = false);
		csord__Subscription__c sub2 = new csord__Subscription__c(csord__Identification__c = 'TestIdent2', csord__Order_Request__c = coreq.Id, Name = 'Product2',
																csord__Account__c = account.Id, LG_Address__c = addressOne.Id, csordtelcoa__Closed_Replaced__c = false);
		insert sub;
		insert sub2;

		Opportunity opp = LG_GeneralTest.CreateOpportunity(account, true);

		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('BAsket58', account, null, opp, false);
		basket.csbb__Account__c = account.Id;
		insert basket;

		cscfga__Product_Category__c prodCatTerm = LG_GeneralTest.createProductCategory('Termination', true);

		cscfga__Product_Definition__c prodDefTerm = LG_GeneralTest.createProductDefinition('Termination', false);
		prodDefTerm.cscfga__Product_Category__c = prodCatTerm.Id;
		insert prodDefTerm;

		noTriggers.Flag__c = false;
		upsert noTriggers;
	}

	private static testmethod void testGetPageController()
	{
		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];
		csord__Subscription__c sub = [SELECT Id, csord__Identification__c, LG_Address__c
										FROM csord__Subscription__c WHERE csord__Identification__c = 'TestIdent'];

		PageReference pageRef = Page.LG_MACDConfiguration;
		pageRef.getParameters().put('accountId', account.Id);
		pageRef.getParameters().put('subscriptionId', sub.Id);
		Test.setCurrentPageReference(pageRef);

		Test.startTest();

			LG_MACDConfigurationController controller = new LG_MACDConfigurationController();

		Test.stopTest();

		System.assertEquals(controller, controller.getPageController(), 'Method should return the controller instance');
	}

	private static testmethod void testGetSiteCaption()
	{
		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];
		csord__Subscription__c sub = [SELECT Id, csord__Identification__c, LG_Address__c
										FROM csord__Subscription__c WHERE csord__Identification__c = 'TestIdent'];

		String addressJson = '{"selectedAddressDisplay":null,"city":"Amstelveen","postCode":"1183NW","houseNumberExt":"AE","houseNumber":"26","street":"Bankrashof","addressId":"12360"}';
		LG_AddressResponse.OptionalsJson moveSiteObject = (LG_AddressResponse.OptionalsJson) JSON.deserialize(addressJson, LG_AddressResponse.OptionalsJson.class);

		PageReference pageRef = Page.LG_MACDConfiguration;
		pageRef.getParameters().put('accountId', account.Id);
		pageRef.getParameters().put('subscriptionId', sub.Id);
		Test.setCurrentPageReference(pageRef);

		Test.startTest();

			LG_MACDConfigurationController controller = new LG_MACDConfigurationController();
			controller.moveSite = moveSiteObject;

		Test.stopTest();

		System.assertEquals('Bankrashof 26 AE, 1183NW Amstelveen', controller.moveSiteCaption, 'Bankrashof 26 AE, 1183NW Amstelveen');
		System.assertEquals('{"street":"Bankrashof","selectedAddressDisplay":"Bankrashof 26 AE, 1183NW Amstelveen","postCode":"1183NW","houseNumberExt":"AE","houseNumber":"26","city":"Amstelveen","addressId":"12360"}',
								controller.moveSiteJson, 'moveSiteJson should be '
								+ '{"street":"Bankrashof","selectedAddressDisplay":"Bankrashof 26 AE, 1183NW Amstelveen","postCode":"1183NW","houseNumberExt":"AE","houseNumber":"26","city":"Amstelveen","addressId":"12360"}');
	}

	private static testmethod void testRedirectToReturnId()
	{
		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];
		csord__Subscription__c sub = [SELECT Id, csord__Identification__c, LG_Address__c
										FROM csord__Subscription__c WHERE csord__Identification__c = 'TestIdent'];

		PageReference pageRef = Page.LG_MACDConfiguration;
		pageRef.getParameters().put('accountId', account.Id);
		pageRef.getParameters().put('subscriptionId', sub.Id);
		Test.setCurrentPageReference(pageRef);

		Test.startTest();

			LG_MACDConfigurationController controller = new LG_MACDConfigurationController();
			pageRef = controller.redirectToReturnId();

			System.assertEquals( sfdcBaseUrl+ '/' + sub.Id, pageRef.getUrl(),
								'Url should be ' + sfdcBaseUrl + '/' + sub.Id);

			pageRef = Page.LG_MACDConfiguration;
			pageRef.getParameters().put('accountId', account.Id);
			Test.setCurrentPageReference(pageRef);

			controller = new LG_MACDConfigurationController();
			pageRef = controller.redirectToReturnId();

		Test.stopTest();

		System.assertEquals( sfdcBaseUrl+ '/' + account.Id, pageRef.getUrl(),
							'Url should be ' + sfdcBaseUrl + '/' + account.Id);
	}

	private static testmethod void testRfsCheck()
	{
		cscrm__Address__c address = [SELECT Id FROM cscrm__Address__c
										WHERE Name = 'AddressOne'];

		Test.setMock(HttpCalloutMock.class, new LG_MACDConfRfsCheckMock(200, 'OK', '{result:ok}'));

		Test.startTest();
			LG_MACDConfigurationController.HttpResponseRemoting responseObj = LG_MACDConfigurationController.rfsCheck(address.Id, false);

		Test.stopTest();

		System.assertNotEquals(null, responseObj, 'Response object should not be null');
		System.assertEquals(200, responseObj.statusCode, 'Status code should be 200');
	}

	private static testmethod void testRfsCheckMove()
	{
		String addressJson = '{"selectedAddressDisplay":null,"city":"Amstelveen","postCode":"1183NW","houseNumberExt":"AE","houseNumber":"26","street":"Bankrashof","addressId":"12360"}';

		Test.setMock(HttpCalloutMock.class, new LG_MACDConfRfsCheckMock(200, 'OK', '{result:ok}'));

		Test.startTest();
			LG_MACDConfigurationController.HttpResponseRemoting responseObj = LG_MACDConfigurationController.rfsCheck(addressJson, true);

		Test.stopTest();

		System.assertNotEquals(null, responseObj, 'Response object should not be null');
		System.assertEquals(200, responseObj.statusCode, 'Status code should be 200');
	}

	private static testmethod void testCreateMacdAndProductBasket()
	{
		cscrm__Address__c address = [SELECT Id FROM cscrm__Address__c
										WHERE Name = 'AddressOne'];
		csord__Subscription__c subscription = [SELECT Id, csord__Identification__c, LG_Address__c
										FROM csord__Subscription__c WHERE csord__Identification__c = 'TestIdent'];

		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];

		LG_MACDConfigurationController.SitesObj siteObj = new LG_MACDConfigurationController.SitesObj();
		siteObj.sites = new LG_MACDConfigurationController.Site[]{};
		LG_MACDConfigurationController.Site site = new LG_MACDConfigurationController.Site();
		site.id = address.Id;
		site.rfsResponse = LG_RfsCheckUtility.buildRfsResponse();
		LG_MACDConfigurationController.Sub sub = new LG_MACDConfigurationController.Sub();
		sub.id = subscription.Id;
		site.subs = new LG_MACDConfigurationController.Sub[]{};
		site.subs.add(sub);
		siteObj.sites.add(site);

		Test.startTest();
			String macdOppId = LG_MACDConfigurationController.createMacdOppAndBasket(JSON.serialize(siteObj), 'Change', account.Id);
		Test.stopTest();

		//Only checking for MACD Opportunity as Basket is not created in the test context
		List<Opportunity> opps = [SELECT Id, csordtelcoa__Change_Type__c FROM Opportunity where csordtelcoa__Change_Type__c = 'Change'];
		System.assertEquals(1, opps.size(), 'MACD Opportunity should be created');
		System.assertEquals(opps[0].Id, macdOppId, 'MACD Opportunity Id should be ' + macdOppId);
		System.assertEquals('Change', opps.get(0).csordtelcoa__Change_Type__c, 'MACD Opportunity should be of a Change Type');
	}

	private static testmethod void testUpdateProductConfigurationAttributes()
	{
		cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Name = 'BAsket58'];
		cscrm__Address__c address = [SELECT Id FROM cscrm__Address__c
										WHERE Name = 'AddressOne'];
		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];

		cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('ProdDef', true);
		cscfga__Product_Configuration__c prodConf = LG_GeneralTest.createProductConfiguration('ProdConf', 3, basket, prodDef, true);
		cscfga__Attribute_Definition__c attDef = LG_GeneralTest.createAttributeDefinition('AttDef', prodDef, 'User Input', 'String',
			null, null, null, true);

		LG_GeneralTest.createAttribute('Premise Id', attDef, false, null, prodConf, false, address.Id, true);
		LG_GeneralTest.createAttribute('Product Family', attDef, false, null, prodConf, false, 'Internet', true);
		LG_GeneralTest.createAttribute('QoS', attDef, false, null, prodConf, false, '', true);
		LG_GeneralTest.createAttribute('Upload', attDef, false, null, prodConf, false, '', true);
		LG_GeneralTest.createAttribute('Download', attDef, false, null, prodConf, false, '', true);
		LG_GeneralTest.createAttribute('Installation Wish Date', attDef, false, null, prodConf, false, '', true);
        /* INC000001886457 */
        LG_GeneralTest.createAttribute('Discount', attDef, false, null, prodConf, false, '', true);
        /* INC000001886457 */
        /* PBI000000220041 */
        LG_GeneralTest.createAttribute('Order Number', attDef, false, null, prodConf, false, '', true);
        /* PBI000000220041 */

		Map<String, String> siteToRfsCommonResponse = new Map<String, String>();
		siteToRfsCommonResponse.put(address.Id, '{"zipCode":"2596AP","street":"JOZEF ISRAELSLAAN","houseNumberExt":"","houseNumber":"4","city":"S-GRAVENHAGE","availability":[{"technology":"HFC","supportingData":[],"limits":[{"up":"30","qos":"Standard","down":"199"}],"capability":"Data"},{"technology":"HFC","supportingData":[],"limits":[{"up":"40","qos":"Standard","down":"200"}],"capability":"Data"},{"technology":"Digital","supportingData":[],"limits":[{"up":"","qos":"Standard","down":""}],"capability":"TV"},{"technology":"Analogue","supportingData":[],"limits":[{"up":"","qos":"Standard","down":""}],"capability":"TV"},{"technology":"Digital","supportingData":[],"limits":[],"capability":"Voice"}]}');

		Test.startTest();
			LG_MACDConfigurationController.updateProductConfigurationAttributes(basket.Id, siteToRfsCommonResponse, 'Change', account.Id);
		Test.stopTest();

		List<cscfga__Attribute__c> attributes = [SELECT Name, cscfga__Value__c, cscfga__Product_Configuration__c
													FROM cscfga__Attribute__c
													WHERE Name IN ('Upload', 'Download', 'Product Family', 'Premise Id', 'QoS')
													AND cscfga__Product_Configuration__c = :prodConf.Id];

		System.assertEquals(5, attributes.size(), 'Five attributes expected');
		for(cscfga__Attribute__c att : attributes)
		{
			if (att.Name.equals('Upload'))
			{
				System.assertEquals('40', att.cscfga__Value__c, '40 should be max upload speed');
			}
			if (att.Name.equals('Download'))
			{
				System.assertEquals('200', att.cscfga__Value__c, '200 should be max download speed');
			}
			if (att.Name.equals('QoS'))
			{
				System.assertEquals('Standard', att.cscfga__Value__c, 'QoS should be Standard');
			}
			if (att.Name.equals('Installation Wish Date'))
			{
				System.assertEquals(null, att.cscfga__Value__c, 'Installation Wish Date should be null');
			}
		}
	}

	private static testmethod void testUpdateProductConfigurationAttributesMove()
	{
		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];

		String addressJson = '{"selectedAddressDisplay":null,"city":"Amstelveen","postCode":"1183NW","houseNumberExt":"AE","houseNumber":"26","street":"Bankrashof","addressId":"12360"}';

		LG_AddressResponse.OptionalsJson moveAddress = (LG_AddressResponse.OptionalsJson)
														JSON.deserialize(addressJson, LG_AddressResponse.OptionalsJson.class);

		Map<String, Object> addressIdMap = (Map<String, Object>) JSON.deserializeUntyped(
											AddressCheck.setAddress(moveAddress.street, moveAddress.houseNumber, moveAddress.houseNumberExt,
													moveAddress.postCode, moveAddress.city, account.Id,
													moveAddress.addressId, 'Netherlands'));

		Id addressId = (Id) addressIdMap.get('SalesforceAddressId');

		cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Name = 'BAsket58'];
		cscrm__Address__c address = [SELECT Id FROM cscrm__Address__c
										WHERE Name = 'AddressOne'];

		cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('ProdDef', true);
		cscfga__Product_Configuration__c prodConf = LG_GeneralTest.createProductConfiguration('ProdConf', 3, basket, prodDef, true);
		cscfga__Attribute_Definition__c attDef = LG_GeneralTest.createAttributeDefinition('AttDef', prodDef, 'User Input', 'String',
			null, null, null, true);

		LG_GeneralTest.createAttribute('Premise Id', attDef, false, null, prodConf, false, address.Id, true);
		LG_GeneralTest.createAttribute('Product Family', attDef, false, null, prodConf, false, 'Internet', true);
		LG_GeneralTest.createAttribute('QoS', attDef, false, null, prodConf, false, '', true);
		LG_GeneralTest.createAttribute('Upload', attDef, false, null, prodConf, false, '', true);
		LG_GeneralTest.createAttribute('Download', attDef, false, null, prodConf, false, '', true);

		Map<String, String> siteToRfsCommonResponse = new Map<String, String>();
		siteToRfsCommonResponse.put(addressJson, '{"zipCode":"2596AP","street":"JOZEF ISRAELSLAAN","houseNumberExt":"","houseNumber":"4","city":"S-GRAVENHAGE","availability":[{"technology":"HFC","supportingData":[],"limits":[{"up":"30","qos":"Standard","down":"199"}],"capability":"Data"},{"technology":"HFC","supportingData":[],"limits":[{"up":"40","qos":"Standard","down":"200"}],"capability":"Data"},{"technology":"Digital","supportingData":[],"limits":[{"up":"","qos":"Standard","down":""}],"capability":"TV"},{"technology":"Analogue","supportingData":[],"limits":[{"up":"","qos":"Standard","down":""}],"capability":"TV"},{"technology":"Digital","supportingData":[],"limits":[],"capability":"Voice"}]}');

		Test.startTest();
			LG_MACDConfigurationController.updateProductConfigurationAttributes(basket.Id, siteToRfsCommonResponse, 'Move', account.Id);
		Test.stopTest();

		List<cscfga__Attribute__c> attributes = [SELECT Name, cscfga__Value__c, cscfga__Product_Configuration__c
													FROM cscfga__Attribute__c
													WHERE Name IN ('Upload', 'Download', 'Product Family', 'Premise Id', 'QoS')
													AND cscfga__Product_Configuration__c = :prodConf.Id];

		System.assertEquals(5, attributes.size(), 'Five attributes expected');
		for(cscfga__Attribute__c att : attributes)
		{
			if (att.Name.equals('Upload'))
			{
				System.assertEquals('40', att.cscfga__Value__c, '40 should be max upload speed');
			}
			if (att.Name.equals('Download'))
			{
				System.assertEquals('200', att.cscfga__Value__c, '200 should be max download speed');
			}
			if (att.Name.equals('QoS'))
			{
				System.assertEquals('Standard', att.cscfga__Value__c, 'QoS should be Standard');
			}
			if (att.Name.equals('Premise Id'))
			{
				System.assertEquals(addressId, att.cscfga__Value__c, 'Premise Id should be ' + addressId);
			}
		}

		Boolean validPCs = true;
		List<cscfga__Product_Configuration__c> pcs = [SELECT Id, Name, cscfga__Key__c FROM cscfga__Product_Configuration__c];

		for (cscfga__Product_Configuration__c p :pcs) {

		    if (!LG_util.IsValidConfiguratorId(p.cscfga__Key__c)) {
		        validPCs = false;
		    }
		}

        /**
         * MACD - ​Getting Error "Insert Failed" on clicking Finish button in Edit
         * Product Configuration page once after adding extra
         * telephone number for existing customer with Telefonie Plus...
         *
         * @author Petar Miletic
         * @story SFDT-1279
         * @since 18/07/2016
        */
        System.assertEquals(true, validPCs, 'Invalid Product Configuration Key');
	}

	private static testmethod void testCreateMigrateMacdBasket()
	{
		/* Commented because the this is failing and MacD functionality is not implemented completely as part of droop 1 */
		/*
		cscrm__Address__c address = [SELECT Id, LG_FullAddressDetails__c FROM cscrm__Address__c
										WHERE Name = 'AddressOne'];
		List<csord__Subscription__c> subs = [SELECT Id, csord__Account__c, Name,
                                                        LG_Address__r.Id, LG_Address__r.LG_FullAddressDetails__c
                                                        FROM csord__Subscription__c
                                                        WHERE csord__Identification__c = 'TestIdent' OR csord__Identification__c = 'TestIdent2'
                                                        ORDER BY LG_Address__r.LG_FullAddressDetails__c];

		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];

		Opportunity opp = [SELECT Id FROM Opportunity WHERE AccountId = :account.Id];

		Test.startTest();
			Id macdBasketId = LG_MACDConfigurationController.createMigrateMacdBasket(opp.Id, subs);
		Test.stopTest();

		cscfga__Product_Basket__c basket = [SELECT Id, csordtelcoa__Change_Type__c, csbb__Account__c,
											LG_SelectedProducts__c, cscfga__Opportunity__c
											FROM cscfga__Product_Basket__c WHERE Id = :macdBasketId];

		System.assertEquals(opp.Id, basket.cscfga__Opportunity__c, 'Opportunity should be set on the MACD basket');
		System.assertEquals('Migrate', basket.csordtelcoa__Change_Type__c, 'Change Type should be Migrate');
		System.assertEquals(account.Id, basket.csbb__Account__c, 'Account should be set on basket');
		System.assertEquals(true, basket.LG_SelectedProducts__c.contains(address.LG_FullAddressDetails__c)
		 							&& basket.LG_SelectedProducts__c.contains('- Product1')
									&& basket.LG_SelectedProducts__c.contains('- Product2'), 'Selected Product field should contain ');
		*/
	}

	private static testmethod void testCreateMacdAndProductBasketTerminate()
	{
		/* Commented because the this is failing and MacD functionality is not implemented completely as part of droop 1 */
		/*
		cscrm__Address__c address = [SELECT Id FROM cscrm__Address__c
										WHERE Name = 'AddressOne'];
		csord__Subscription__c subscription = [SELECT Id, csord__Identification__c, LG_Address__c
										FROM csord__Subscription__c WHERE csord__Identification__c = 'TestIdent'];

		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];

		LG_MACDConfigurationController.SitesObj siteObj = new LG_MACDConfigurationController.SitesObj();
		siteObj.sites = new LG_MACDConfigurationController.Site[]{};
		LG_MACDConfigurationController.Site site = new LG_MACDConfigurationController.Site();
		site.id = address.Id;
		site.rfsResponse = LG_RfsCheckUtility.buildRfsResponse();
		LG_MACDConfigurationController.Sub sub = new LG_MACDConfigurationController.Sub();
		sub.id = subscription.Id;
		site.subs = new LG_MACDConfigurationController.Sub[]{};
		site.subs.add(sub);
		siteObj.sites.add(site);

		Test.startTest();
			String macdOppId = LG_MACDConfigurationController.createMacdOppAndBasket(JSON.serialize(siteObj), 'Terminate', account.Id);
		Test.stopTest();

		List<Opportunity> opps = [SELECT Id, csordtelcoa__Change_Type__c FROM Opportunity where csordtelcoa__Change_Type__c = 'Terminate'];
		System.assertEquals(1, opps.size(), 'MACD Opportunity should be created');
		System.assertEquals(opps[0].Id, macdOppId, 'MACD Opportunity Id should be ' + macdOppId);
		System.assertEquals('Terminate', opps.get(0).csordtelcoa__Change_Type__c, 'MACD Opportunity should be of a Terminate Type');

		*/
	}
}