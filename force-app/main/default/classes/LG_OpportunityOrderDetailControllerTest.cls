@isTest
public class LG_OpportunityOrderDetailControllerTest {

    @testsetup
	private static void setupTestData()
	{
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

        // Generate Account
        Account acc = LG_GeneralTest.CreateAccount('AccountSFDT', '12345678', 'Ziggo', true);
        
        // Generate Opportunity
        Opportunity opp = LG_GeneralTest.CreateOpportunity(acc, true);

        // Generate Address (Premise)
        cscrm__Address__c premise = LG_GeneralTest.crateAddress('TestAddress', 'street', 'city', '5', '73', '2014GC', 'Netherlands', acc, true);

        // Generate Product Basket
        cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('BAsket58', acc, null, opp, false);
        basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
        insert basket;

        // Generate Product Definition
        cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('ProdDef58', true);

        // Generate Product Configuration
        cscfga__Product_Configuration__c prodConfiguration = LG_GeneralTest.createProductConfiguration('ProdConf58', 3, basket, prodDef, false);
        prodConfiguration.LG_PortIn__c = true;
        prodConfiguration.LG_MaximumQuantity__c = 20;
        prodConfiguration.LG_Address__c = premise.Id;
        prodConfiguration.cscfga__Product_Basket__c = basket.Id;
        prodConfiguration.cscfga__Product_Family__c = 'Termination';
        prodConfiguration.LG_InstallationWishDate__c = Date.newInstance(2016, 12, 31);
        insert prodConfiguration;

		noTriggers.Flag__c = false;
		upsert noTriggers;
	}

    @IsTest
    public static void getProductDisplayDataTest() {
        
        List<LG_OpportunityOrderDetailController.ProductInfo> results = new List<LG_OpportunityOrderDetailController.ProductInfo>();
        
        Opportunity opp = [SELECT Id, Name FROM Opportunity LIMIT 1];
        
        Test.startTest();
        
        // Instantiate Controller
        LG_OpportunityOrderDetailController controller = new LG_OpportunityOrderDetailController();
        controller.relatedTo = opp;

        results = controller.pcs;

        Test.stopTest();
        
        System.assertEquals(1, results.size(), 'Invalid data');
        System.assertEquals(Date.newInstance(2016, 12, 31).format(), results[0].displayDate, 'Invalid data');
    }
}