@isTest
private class LG_OpportunityTriggerHandlerTest {
	private static Date someDate = Date.newInstance(1960, 2, 17);

	@testsetup
	private static void setupTestData() {
		Framework__c f = Framework__c.getOrgDefaults();
		f.Framework_Sequence_Number__c = 1;
		upsert f;

		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = false;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		Account account = LG_GeneralTest.createAccount('Account', '12345678', 'Ziggo', true);
		Opportunity opp = LG_GeneralTest.createOpportunity(account, false);
		opp.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName('Opportunity', 'SOHO_SMALL');
		insert opp;
		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket(
			'Basket',
			account,
			null,
			opp,
			false
		);
		basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
		basket.csordtelcoa__Change_Type__c = 'Migrate';
		insert basket;

		cscfga__Product_Category__c prodCategory = LG_GeneralTest.createProductCategory(
			'Test Category',
			true
		);

		cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition(
			'Phone Numbers',
			false
		);

		prodDef.cscfga__Product_Category__c = prodCategory.Id;
		insert prodDef;

		cscrm__Address__c address = new cscrm__Address__c(cscrm__Street__c = 'TestStreet');
		insert address;

		csord__Order_Request__c coreq = new csord__Order_Request__c(
			csord__Module_Name__c = 'Test',
			csord__Module_Version__c = '1.0'
		);
		insert coreq;

		csord__Subscription__c sub = new csord__Subscription__c(
			csord__Identification__c = 'TestIdent',
			csord__Order_Request__c = coreq.Id
		);
		insert sub;

		csord__Subscription__c sub2 = new csord__Subscription__c(
			csord__Identification__c = 'TestIdent2',
			csord__Order_Request__c = coreq.Id
		);
		insert sub2;

		csord__Service__c service = new csord__Service__c(
			csord__Identification__c = 'TestService',
			csord__Subscription__c = sub.Id,
			csord__Order_Request__c = coreq.Id
		);
		service.LG_Address__c = address.Id;
		insert service;

		csord__Service__c service2 = new csord__Service__c(
			csord__Identification__c = 'TestService2',
			csord__Subscription__c = sub2.Id,
			csord__Order_Request__c = coreq.Id
		);
		service2.LG_Address__c = address.Id;
		insert service2;

		insert new csordtelcoa__Subscr_MACDProductBasket_Association__c(
			csordtelcoa__Subscription__c = sub.Id,
			csordtelcoa__Product_Basket__c = basket.Id,
			LG_DeactivationWishDate__c = someDate
		);

		insert new csordtelcoa__Subscription_MACDOpportunity_Association__c(
			csordtelcoa__Subscription__c = sub.Id,
			csordtelcoa__Opportunity__c = opp.Id
		);
		insert new csordtelcoa__Subscription_MACDOpportunity_Association__c(
			csordtelcoa__Subscription__c = sub2.Id,
			csordtelcoa__Opportunity__c = opp.Id
		);

		cscfga__Product_Configuration__c pc = LG_GeneralTest.createProductConfiguration(
			'TestConf',
			12,
			basket,
			prodDef,
			false
		);
		pc.csordtelcoa__Replaced_Subscription__c = sub.Id;
		insert pc;

		csordtelcoa__Orders_Subscriptions_Options__c osOptions = new csordtelcoa__Orders_Subscriptions_Options__c();
		osOptions.LG_ServiceRequestDeactivateStatus__c = 'Service Termination Requested';
		osOptions.LG_SubscriptionRequestDeactivateStatus__c = 'Subscription Termination Requested';
		insert osOptions;

		LG_TerminationSpecificVariables__c termSpecifics = new LG_TerminationSpecificVariables__c();
		termSpecifics.LG_PenaltyFeeCalculationClass__c = 'ZG_PenaltyFeeCalculation';
		insert termSpecifics;

		noTriggers.Flag__c = false;
		upsert noTriggers;
	}

	private static testMethod void testDeactivateSubscriptionServices() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];
		Opportunity opp = [
			SELECT Id, csordtelcoa__Change_Type__c
			FROM Opportunity
			WHERE AccountId = :account.Id
		];
		opp.csordtelcoa__Change_Type__c = 'Migrate';
		update opp;
		noTriggers.Flag__c = false;
		upsert noTriggers;

		opp.StageName = 'Ready for Order';
		List<Opportunity> opps = new List<Opportunity>();
		opps.add(opp);
		Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
		oldMap.put(opp.Id, new Opportunity(StageName = ''));

		Test.startTest();
		LG_OpportunityTriggerHandler.deactivateSubscriptionServices(opps, oldMap);
		Test.stopTest();

		csord__Subscription__c sub = [
			SELECT csord__Status__c
			FROM csord__Subscription__c
			WHERE csord__Identification__c = 'TestIdent'
		];
		System.assertEquals(
			LG_Util.getSubscriptionRequestDeactivateStatus(),
			sub.csord__Status__c,
			'Status should be ' + LG_Util.getSubscriptionRequestDeactivateStatus()
		);

		csord__Service__c service = [
			SELECT csord__Status__c, LG_DeactivationWishDate__c
			FROM csord__Service__c
			WHERE csord__Identification__c = 'TestService'
		];
		System.assertEquals(
			LG_Util.getServiceRequestDeactivateStatus(),
			service.csord__Status__c,
			'Status should be ' + LG_Util.getServiceRequestDeactivateStatus()
		);
		System.assertEquals(
			someDate,
			service.LG_DeactivationWishDate__c,
			'Date should be ' + someDate
		);
	}

	//CATGOV=921 start
	static CSCAP__Click_Approve_Setting__c createClickApproveSetting() {
		//Folder tmpFolder = [select Id from Folder where Type='Email Template' limit 1];
		RecordType sitesRecordType = [
			SELECT Id
			FROM RecordType
			WHERE Name = 'Sites Approval'
			LIMIT 1
		];

		String htmlBody = '<table height="400" width="550" cellpadding="5" border="0" cellspacing="5" >';
		htmlBody += '<tr height="400" valign="top" >';
		htmlBody += '<td style=" color:#000000; font-size:12pt; background-color:#FFFFFF; font-family:arial; bLabel:main; bEditID:r3st1;" tEditID="c1r1" locked="0" aEditID="c1r1" >';
		htmlBody += '<![CDATA[test]]></td>';
		htmlBody += '</tr>';
		htmlBody += '</table>';

		EmailTemplate tmpEmailNameEmailTemplate = new EmailTemplate();
		tmpEmailNameEmailTemplate.Name = 'Ziggo Zakelijk Quote Template1';
		tmpEmailNameEmailTemplate.DeveloperName = 'ZiggoZakelijkQuoteTemplate1';
		tmpEmailNameEmailTemplate.Subject = 'Offerte Ziggo Zakelijk1';
		tmpEmailNameEmailTemplate.Body = 'Test Body';
		tmpEmailNameEmailTemplate.HtmlValue = htmlBody;
		tmpEmailNameEmailTemplate.TemplateType = 'text';
		tmpEmailNameEmailTemplate.FolderId = UserInfo.getUserId();
		//tmpEmailNameEmailTemplate.BrandTemplateId=tmpBrandTemplate.Id;
		insert tmpEmailNameEmailTemplate;

		CSCAP__Click_Approve_Setting__c cas = new CSCAP__Click_Approve_Setting__c();
		cas.Name = '15th_day_email1';
		cas.RecordTypeId = sitesRecordType.Id;
		cas.CSCAP__Ack_Template_Invalid_Approval__c = '';
		cas.CSCAP__Acknowledgement_Template_Approvals__c = '';
		cas.CSCAP__Acknowledgement_Template_Rejection__c = '';
		cas.CSCAP__Approve_Email_New__c = '';
		cas.CSCAP__Approve_Email_Message__c = '';
		cas.CSCAP__Associated_Email_Template_Name__c = 'Ziggo_15th_day_email_template1';
		cas.CSCAP__Send_Approval_Acknowledgment__c = false;
		cas.CSCAP__Sites_Approval_Header__c = null;
		cas.CSCAP__Sites_Approval_Title__c = 'Maak een opdracht van uw offerte';
		cas.CSCAP__Status__c = 'Active';
		cas.CSCAP__Update_Opportunity_On_Response__c = true;

		insert CAS;

		return CAS;
	}
	/* This test-method used to cover's the sendReminderEmail() of LG_OpportunityTriggerHandler Class.
	 * Covered Class : LG_OpportunityTriggerHandler
	 * Covered method : sendReminderEmail()
	 * @author Usha L
	 * @ticket CATGOV-921
	 * @since  28/5/2019
	 */
	private static testMethod void sendReminderEmailTest() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;
		User usr = [SELECT id FROM User WHERE Id = :UserInfo.getUserId()];
		System.runAs(usr) {
			createClickApproveSetting();
		}

		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];

		Contact con = new Contact();
		con.firstname = 'test';
		con.lastname = 'data';
		con.account = account;
		insert con;

		Opportunity opp = LG_GeneralTest.CreateOpportunity(account, false);
		insert opp;

		opp.toSendReminder__c = true;
		opp.StageName = 'Quotation Delivered';
		update opp;

		OpportunityContactRole contRole = new OpportunityContactRole();
		contRole.OpportunityId = opp.id;
		contRole.ContactId = con.id;
		contRole.Role = 'Administrative Contact';
		contRole.IsPrimary = true;
		insert contRole;

		CSCAP__Customer_Approval__c customerApproval = new CSCAP__Customer_Approval__c();
		customerApproval.CSCAP__Opportunity__c = opp.id;
		insert customerApproval;

		CSCAP__Click_Approve_Setting__c cas = new CSCAP__Click_Approve_Setting__c();
		cas.Name = '15th_day_email';
		cas.CSCAP__Send_Approval_Docs_With_Email_Request__c = true;
		insert cas;

		Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
		oldMap.put(opp.Id, new Opportunity(toSendReminder__c = false));
		List<Opportunity> opps = new List<Opportunity>();
		opps.add(opp);
		Test.StartTest();
		LG_OpportunityTriggerHandler.GetSiteApprovalSettingId();
		LG_OpportunityTriggerHandler.sendReminderEmail(opps, oldMap);
		Test.StopTest();
	}

	private static testMethod void testDeactivateSubscriptionServicesForPartialTermination() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];
		Opportunity opp = [
			SELECT Id, csordtelcoa__Change_Type__c
			FROM Opportunity
			WHERE AccountId = :account.Id
		];
		cscfga__Product_Basket__c basket = [
			SELECT Id, csordtelcoa__Change_Type__c
			FROM cscfga__Product_Basket__c
			LIMIT 1
		];
		basket.csordtelcoa__Change_Type__c = 'Change';
		update basket;
		opp.csordtelcoa__Change_Type__c = 'Change';
		update opp;

		noTriggers.Flag__c = false;
		upsert noTriggers;

		opp.StageName = 'Ready for Order';
		List<Opportunity> opps = new List<Opportunity>();
		opps.add(opp);

		Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
		oldMap.put(opp.Id, new Opportunity(StageName = ''));

		Test.startTest();
		LG_OpportunityTriggerHandler.deactivateSubscriptionServicesForPartialTermination(
			opps,
			oldMap
		);
		Test.stopTest();

		csord__Subscription__c sub = [
			SELECT csord__Status__c, csordtelcoa__Change_Type__c
			FROM csord__Subscription__c
			WHERE csord__Identification__c = 'TestIdent2'
		];
		System.assertEquals(
			LG_Util.getSubscriptionRequestDeactivateStatus(),
			sub.csord__Status__c,
			'Status should be ' + LG_Util.getSubscriptionRequestDeactivateStatus()
		);
		System.assertEquals(
			'Change',
			sub.csordtelcoa__Change_Type__c,
			'Change type should be Change'
		);

		csord__Service__c service = [
			SELECT csord__Status__c, LG_DeactivationWishDate__c
			FROM csord__Service__c
			WHERE csord__Identification__c = 'TestService2'
		];
		System.assertEquals(
			LG_Util.getServiceRequestDeactivateStatus(),
			service.csord__Status__c,
			'Status should be ' + LG_Util.getServiceRequestDeactivateStatus()
		);
	}

	private static testMethod void testRecalculatePenaltyFees() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		No_Validation__c noValidations = No_Validation__c.getInstance(UserInfo.getUserId());
		noValidations.Flag__c = true;
		noValidations.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noValidations;
		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];
		Opportunity opp = [
			SELECT Id, csordtelcoa__Change_Type__c
			FROM Opportunity
			WHERE AccountId = :account.Id
		];
		cscfga__Product_Basket__c basket = [
			SELECT Id, csordtelcoa__Change_Type__c
			FROM cscfga__Product_Basket__c
			LIMIT 1
		];
		opp.csordtelcoa__Change_Type__c = 'Terminate';
		opp.LG_CalculatedPenaltyFee__c = 12000;
		opp.LG_FinalPenalty__c = 6000;
		update opp;

		cscfga__Product_Category__c category = LG_GeneralTest.createProductCategory('Test', true);

		// Prepare Product Configurations with Total Price
		cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition(
			'Penalty Fee',
			false
		);
		prodDef.cscfga__Product_Category__c = category.Id;

		insert prodDef;

		cscfga__Attribute_Definition__c calcDef = LG_GeneralTest.createAttributeDefinition(
			'Calculated Price',
			prodDef,
			'User Input',
			'String',
			null,
			null,
			null,
			true
		);
		cscfga__Attribute_Definition__c overDef = LG_GeneralTest.createAttributeDefinition(
			'Overridden Price',
			prodDef,
			'User Input',
			'String',
			null,
			null,
			null,
			true
		);

		cscfga__Product_Configuration__c pc = LG_GeneralTest.createProductConfiguration(
			'ProdConf58',
			3,
			basket,
			prodDef,
			false
		);
		pc.cscfga__Total_Price__c = 3000;
		insert pc;

		cscfga__Attribute__c calcAt = LG_GeneralTest.createAttribute(
			'Calculated Price',
			calcDef,
			false,
			0,
			pc,
			false,
			'',
			false
		);
		calcAt.cscfga__Price__c = 3000;
		insert calcAt;
		cscfga__Attribute__c overAt = LG_GeneralTest.createAttribute(
			'Overridden Price',
			overDef,
			false,
			0,
			pc,
			false,
			'',
			false
		);
		overAt.cscfga__Price__c = 3000;
		insert overAt;

		cscfga__Product_Configuration__c pc2 = LG_GeneralTest.createProductConfiguration(
			'ProdConf58',
			3,
			basket,
			prodDef,
			false
		);
		pc2.cscfga__Total_Price__c = 9000;
		insert pc2;

		cscfga__Attribute__c calcAt2 = LG_GeneralTest.createAttribute(
			'Calculated Price',
			calcDef,
			false,
			0,
			pc2,
			false,
			'',
			false
		);
		calcAt2.cscfga__Price__c = 9000;
		insert calcAt2;
		cscfga__Attribute__c overAt2 = LG_GeneralTest.createAttribute(
			'Overridden Price',
			overDef,
			false,
			0,
			pc2,
			false,
			'',
			false
		);
		overAt2.cscfga__Price__c = 9000;
		insert overAt2;

		noTriggers.Flag__c = false;
		upsert noTriggers;

		opp.StageName = 'Ready for Order';
		List<Opportunity> opps = new List<Opportunity>();
		opps.add(opp);

		Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
		oldMap.put(
			opp.Id,
			new Opportunity(
				LG_CalculatedPenaltyFee__c = 12000,
				LG_FinalPenalty__c = 12000,
				csordtelcoa__Change_Type__c = 'Terminate'
			)
		);

		Test.startTest();
		LG_OpportunityTriggerHandler.recalculatePenaltyFees(opps, oldMap);
		Test.stopTest();

		List<cscfga__Product_Configuration__c> pcs = [
			SELECT
				Id,
				Name,
				cscfga__Product_Basket__c,
				cscfga__Total_Price__c,
				(
					SELECT Name, cscfga__Price__c
					FROM cscfga__Attributes__r
					WHERE Name IN ('Calculated Price', 'Overridden Price')
				)
			FROM cscfga__Product_Configuration__c
			WHERE
				cscfga__Product_Basket__c = :basket.Id
				AND cscfga__Product_Definition__r.Name = 'Penalty Fee'
		];

		Decimal sumOfPcs = 0;
		Decimal sumOfAtts = 0;

		for (cscfga__Product_Configuration__c prodConf : pcs) {
			sumOfPcs += prodConf.cscfga__Total_Price__c;

			for (cscfga__Attribute__c att : prodConf.cscfga__Attributes__r) {
				if (att.Name == 'Overridden Price') {
					sumOfAtts += att.cscfga__Price__c;
				}
			}
		}

		System.assertEquals(6000, sumOfPcs, 'New Overridden Price should be 6000');
		System.assertEquals(6000, sumOfAtts, 'New Overridden Price should be 6000');
	}

	private static testMethod void terminationTest() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		LG_OrderWebserviceVariables__c ow = new LG_OrderWebserviceVariables__c();
		ow.name = 'SubsStatusforTerminationInProgress';
		ow.LG_Value__c = 'BillingAccount';
		insert ow;

		Account account = [SELECT Id FROM Account WHERE Name = 'Account'];
		Opportunity opp = [
			SELECT Id, csordtelcoa__Change_Type__c
			FROM Opportunity
			WHERE AccountId = :account.Id
		];
		cscfga__Product_Basket__c basket = [
			SELECT Id, csordtelcoa__Change_Type__c
			FROM cscfga__Product_Basket__c
			LIMIT 1
		];
		basket.csordtelcoa__Change_Type__c = 'Terminate';
		update basket;
		opp.csordtelcoa__Change_Type__c = 'Terminate';
		update opp;

		noTriggers.Flag__c = false;
		upsert noTriggers;

		opp.StageName = 'Related Back Office order- Cancelled';
		List<Opportunity> opps = new List<Opportunity>();
		opps.add(opp);

		Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
		oldMap.put(opp.Id, new Opportunity(StageName = ''));

		Test.startTest();
		LG_OpportunityTriggerHandler.orderSubsServiceCancellation(opps);
		Test.stopTest();
	}

	private static testMethod void countInvalidatedPremisesTest() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		LG_OrderWebserviceVariables__c ow = new LG_OrderWebserviceVariables__c();
		ow.name = 'SubsStatusforTerminationInProgress';
		ow.LG_Value__c = 'BillingAccount';
		insert ow;

		Account account = [SELECT Id FROM Account LIMIT 1];
		Opportunity opp = [
			SELECT Id, csordtelcoa__Change_Type__c
			FROM Opportunity
			WHERE AccountId = :account.Id
		];
		cscfga__Product_Basket__c basket = [
			SELECT Id, csordtelcoa__Change_Type__c
			FROM cscfga__Product_Basket__c
			LIMIT 1
		];
		basket.csordtelcoa__Change_Type__c = 'Terminate';
		update basket;
		opp.csordtelcoa__Change_Type__c = 'Terminate';
		update opp;

		noTriggers.Flag__c = false;
		upsert noTriggers;

		opp.StageName = 'Related Back Office order- Cancelled';
		List<Opportunity> opps = new List<Opportunity>();
		opps.add(opp);

		Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
		oldMap.put(opp.Id, new Opportunity(StageName = ''));

		Test.startTest();
		LG_OpportunityTriggerHandler.countInvalidatedPremises(oldMap);
		Test.stopTest();
	}
}