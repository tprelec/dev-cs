global class LG_OrderToOPTWebService {
	global class Response {
		webService String error { get; set; }
		webService String status { get; set; }
		webService List<OrderData> orderData { get; set; }
	}

	global class OrderData {
		webService LG_Account accountDetail { get; set; }
		webService LG_OrderDetail orderDetail { get; set; }
		webService LG_Opportunity opportunityDetail { get; set; }
	}

	global class LG_Account {
		webService String Name { get; set; }
		webService String CustomAccountNumber { get; set; }
		webService String Phone { get; set; }
		webService String Fax { get; set; }
		webService String Website { get; set; }
		webService String EmailAdministrativeContact { get; set; }
		webService String ChamberOfCommerceNumber { get; set; }
		webService String ExternalAccountID { get; set; }
		webService String VisitCountry { get; set; }
		webService String VisitPostalCode { get; set; }
		webService String VisitHouseNumber { get; set; }
		webService String VisitStreet { get; set; }
		webService String VisitHouseNumberExtension { get; set; }
		webService String VisitCity { get; set; }
		webService String PostalCountry { get; set; }
		webService String PostalPostalCode { get; set; }
		webService String PostalHouseNumber { get; set; }
		webService String PostalStreet { get; set; }
		webService String PostalHouseNumberExtension { get; set; }
		webService String PostalCity { get; set; }
		webService List<LG_Contact> contactList { get; set; }
		webService List<LG_BillingAccount> billingAccountList { get; set; }
	}

	global class LG_Contact {
		webService String Name { get; set; }
		webService String FirstName { get; set; }
		webService String MiddleName { get; set; }
		webService String LastName { get; set; }
		webService String Salutation { get; set; }
		webService String Phone { get; set; }
		webService String MobilePhone { get; set; }
		webService String Fax { get; set; }
		webService String Email { get; set; }
		webService String TechnicalContact { get; set; }
		webService String CommercialContact { get; set; }
		webService String FinancialContact { get; set; }
		webService Date Birthdate { get; set; }
		webService String ExternalContactID { get; set; }
		webService String CustomContactNumber { get; set; }
	}

	global class LG_BillingAccount {
		webService String BankAccountHolder { get; set; }
		webService String ExternalID { get; set; }
		webService String CustomerExternalID { get; set; }
		webService String PaymentType { get; set; }
		webService String PaymentTerms { get; set; }
		webService String BillingAccountNumber { get; set; }
		webService String CustomerReference { get; set; }
		webService String BillingEmailAddress { get; set; }
		webService String BillingChannel { get; set; }
		webService String BankAccountNumberIBAN { get; set; }
		webService String FormatType { get; set; }
		webService String Country { get; set; }
		webService String Street { get; set; }
		webService String HouseNumber { get; set; }
		webService String HouseNumberExtension { get; set; }
		webService String Postcode { get; set; }
		webService String City { get; set; }
	}

	global class LG_OrderDetail {
		webService String OrderNumber { get; set; }
		webService String CustomAccountNumber { get; set; }
		webService String ExternalAccountID { get; set; }
		webService String OpportunityName { get; set; }
		webService String BillingAccountPaymentType { get; set; }
		webService Date PreferredInstallationDate { get; set; }
		webService String ContractTerm { get; set; }
		webService String OpportunityOwner { get; set; }
		webService String CommercialContact { get; set; }
		webService String TechnicalContact { get; set; }
		webService String ChangeTypeCode { get; set; }
		webService String Remark { get; set; }
		webService List<LG_OrderLine> orderLineList { get; set; }
	}

	global class LG_OrderLine {
		webService String BillingAccountExternalId { get; set; }
		webService String BillingAccountNumber { get; set; }
		webService String SubscriptionName { get; set; }
		webService String ServiceName { get; set; }
		webService String ExternalPriceName { get; set; }
		webService String PremiseName { get; set; }
		webService String PremiseRoomNumber { get; set; }
		webService String PremiseStreet { get; set; }
		webService String PremiseHouseNumber { get; set; }
		webService String PremiseHouseNumberExtension { get; set; }
		webService String PremiseZIPPostalCode { get; set; }
		webService String PremiseCity { get; set; }
		webService String PremiseFootprint { get; set; }
		webService String SubscriptionPublicId { get; set; }
		webService String ServicePublicId { get; set; }
		webService String SubscriptionExternalId { get; set; }
		webService String ServiceExternalId { get; set; }
		webService String contractstatus { get; set; }
		webService String TerminationReason { get; set; }
		webService String ContractcardCode { get; set; }
		webService Date TerminationDate { get; set; }
		webService String FinalPenaltyFee { get; set; }
		webService List<LG_AddOns> addOnList { get; set; }
		webService List<LG_PhoneNumbers> phoneNumberList { get; set; }
		webService List<LG_Discounts> discountList { get; set; }
	}

	global class LG_AddOns {
		webService String ExternalSubscriptionName { get; set; }
		webService String ExternalServiceName { get; set; }
		webService String Quantity { get; set; }
		webService String ServiceLineItemExternalDescription { get; set; }
		webService String ServiceLineItemPriceName { get; set; }
		webService String ServiceNumber { get; set; }
		webService String ServiceExternalID { get; set; }
		webService String Yes_No { get; set; }
		webService List<LG_Discounts> addOnDiscountList { get; set; }
	}

	global class LG_Discounts {
		webService String DiscountName { get; set; }
	}

	global class LG_PhoneNumbers {
		webService String AreaCodes { get; set; }
		webService String Phonenumbers { get; set; }
		webService String TypeOfPhoneRange { get; set; }
		webService String DonorOperatorCode { get; set; }
		webService String PortingName { get; set; }
		webService String PortingInitials { get; set; }
		webService String PortingPrefix { get; set; }
		webService String PortingStreet { get; set; }
		webService String PortingHouseNumber { get; set; }
		webService String PortingHouseNumberAddition { get; set; }
		webService String PortingZipcode { get; set; }
		webService String PortingCity { get; set; }
		webService Date PortingWishDate { get; set; }
		webService String CallerID { get; set; }
		webService String CallBarringLevel { get; set; }
		webService String BNumberShielding { get; set; }
		webService String InDirectory { get; set; }
		webService String DirectoryListingName { get; set; }
	}

	global class LG_Opportunity {
		webService String Id { get; set; }
		webService String Type { get; set; }
		webService String AccountSegment { get; set; }
		webService String FootPrint { get; set; }
		webService String Description { get; set; }
		webService String Name { get; set; }
		webService String AccountId { get; set; }
		webService String AccountName { get; set; }
		webService String AccountPhone { get; set; }
		webService String AccountChamberOfCommerce { get; set; }
		webService Date PreferredInstallationDate { get; set; }
		webService Boolean SharedOfficeBuilding { get; set; }
		webService String COAXConnectionLocation { get; set; }
		webService String ContractTermMonths { get; set; }
		webService String PaymentType { get; set; }
		webService String PartnerAccountId { get; set; }
		webService String PartnerAccountName { get; set; }
		webService String NEWSalesChannel { get; set; }
		webService String BillingStreet { get; set; }
		webService String BillingHouseNumber { get; set; }
		webService String BillingHouseNumberExtension { get; set; }
		webService String BillingPostalCode { get; set; }
		webService String BillingCity { get; set; }
		webService String BillingCountry { get; set; }
		webService String BankAccountName { get; set; }
		webService String BankNumber { get; set; }
		webService String CustomerReference { get; set; }
		webService String InstallationStreet { get; set; }
		webService String InstallationHouseNumber { get; set; }
		webService String InstallationHouseNumberExtension { get; set; }
		webService String InstallationPostalCode { get; set; }
		webService String InstallationCity { get; set; }
		webService String InstallationCountry { get; set; }
		webService String PostalStreet { get; set; }
		webService String PostalHouseNumber { get; set; }
		webService String PostalHouseNumberExtension { get; set; }
		webService String PostalCode { get; set; }
		webService String PostalCity { get; set; }
		webService String PostalCountry { get; set; }
		webService List<LG_OpportunityLineItem> opportunityLineItemList { get; set; }
		webService List<LG_OpportunityContactRole> opportunityContactRoleList { get; set; }
		webService List<LG_ProductDetail> productDetailList { get; set; }
		webService List<LG_PortingNumbersForSoho> portingNumberList { get; set; }
		webService List<LG_OperatorSwitch> operatorswitchList { get; set; }

		public LG_Opportunity(
			Opportunity opp,
			csconta__Billing_Account__c billingAcc,
			cscfga__Product_Configuration__c prodConfig,
			OrderEntryData jsonData,
			Site__c st
		) {
			this.Id = opp.Id;
			this.Type = opp.Type;
			this.Description = opp.Description;
			this.Name = opp.Name;
			this.AccountId = opp.Account.LG_CustomAccountNumber__c;
			this.PartnerAccountId = opp.PartnerAccountId;
			this.PartnerAccountName = opp.PartnerAccount.Name;
			this.NEWSalesChannel = opp.LG_NEWSalesChannel__c;
			this.AccountName = opp.Account.Name;
			this.AccountSegment = opp.Account.SF1_Segment__c;
			this.AccountPhone = opp.Account.Phone;
			this.AccountChamberOfCommerce = opp.Account.KVK_number__c;
			this.PostalStreet = opp.Account.LG_PostalStreet__c;
			this.PostalHouseNumber = opp.Account.LG_PostalHouseNumber__c;
			this.PostalHouseNumberExtension = opp.Account.LG_PostalHouseNumberExtension__c;
			this.PostalCode = opp.Account.LG_PostalPostalCode__c;
			this.PostalCity = opp.Account.LG_PostalCity__c;
			this.PostalCountry = opp.Account.LG_PostalCountry__c;

			if (billingAcc != null) {
				this.BillingStreet = billingAcc.csconta__Street__c;
				this.BillingHouseNumber = billingAcc.LG_HouseNumber__c;
				this.BillingHouseNumberExtension = billingAcc.LG_HouseNumberExtension__c;
				this.BillingPostalCode = billingAcc.csconta__Postcode__c;
				this.BillingCity = billingAcc.csconta__City__c;
				this.BillingCountry = billingAcc.csconta__Country__c;
				this.BankAccountName = billingAcc.LG_BankAccountHolder__c;
				this.BankNumber = billingAcc.LG_BankAccountNumberIBAN__c;
				this.CustomerReference = billingAcc.LG_CustomerReference__c;
				this.PaymentType = billingAcc.LG_PaymentType__c;
			}

			if (prodConfig != null) {
				this.PreferredInstallationDate = prodConfig.LG_InstallationWishDate__c;
				this.ContractTermMonths = String.valueOf(prodConfig.cscfga__Contract_Term__c);
				this.InstallationStreet = prodConfig.LG_Address__r.cscrm__Street__c;
				this.InstallationHouseNumber = prodConfig.LG_Address__r.LG_HouseNumber__c;
				this.InstallationHouseNumberExtension = prodConfig.LG_Address__r.LG_HouseNumberExtension__c;
				this.InstallationPostalCode = prodConfig.LG_Address__r.cscrm__Zip_Postal_Code__c;
				this.InstallationCity = prodConfig.LG_Address__r.cscrm__City__c;
				this.InstallationCountry = prodConfig.LG_Address__r.cscrm__Country__c;
				this.SharedOfficeBuilding = prodConfig.LG_Address__r.LG_SharedOfficeBuilding__c;
				this.COAXConnectionLocation = prodConfig.LG_Address__r.LG_COAXConnectionLocation__c;
				this.FootPrint = prodConfig.LG_Address__r.LG_Footprint__c;
			} else if (jsonData != null && st != null) {
				this.PreferredInstallationDate = jsonData.installation.preferredDate1.selectedDate;
				this.ContractTermMonths = String.valueOf(jsonData.contractTerm);
				this.InstallationStreet = st.Site_Street__c;
				this.InstallationHouseNumber = String.valueOf(st.Site_House_Number__c);
				this.InstallationHouseNumberExtension = st.Site_House_Number_Suffix__c;
				this.InstallationPostalCode = st.Site_Postal_Code__c;
				this.InstallationCity = st.Site_City__c;
				this.InstallationCountry = st.Country__c;
				this.SharedOfficeBuilding = false;
				this.FootPrint = jsonData.siteCheck.footprint;
				this.COAXConnectionLocation =
					jsonData.installation.preferredDate1.selectedDate +
					' ' +
					jsonData.installation.preferredDate1.dayPeriod +
					'\n' +
					jsonData.installation.preferredDate2.selectedDate +
					' ' +
					jsonData.installation.preferredDate2.dayPeriod +
					'\n' +
					jsonData.installation.preferredDate3.selectedDate +
					' ' +
					jsonData.installation.preferredDate3.dayPeriod +
					'\n' +
					(jsonData.installation.assignTechnician ? 'Yes' : 'No') +
					'\n' +
					jsonData.notes;
			}

			if (st != null) {
				this.PostalStreet = st.Site_Street__c;
				this.PostalHouseNumber = String.valueOf(st.Site_House_Number__c);
				this.PostalHouseNumberExtension = st.Site_House_Number_Suffix__c;
				this.PostalCode = st.Site_Postal_Code__c;
				this.PostalCity = st.Site_City__c;
				this.PostalCountry = st.Country__c;
			}

			this.addContactRoles(opp.OpportunityContactRoles);
			this.addProductDetails(opp.Product_Details__r);
			this.addPortingNumbers(opp.Porting_Numbers__r);
		}

		public void addContactRoles(List<OpportunityContactRole> oppContactRoles) {
			if (oppContactRoles == null) {
				return;
			}
			this.opportunityContactRoleList = new List<LG_OpportunityContactRole>();
			for (OpportunityContactRole ocr : oppContactRoles) {
				this.opportunityContactRoleList.add(new LG_OpportunityContactRole(ocr));
			}
		}

		public void addProductDetails(List<LG_ProductDetail__c> prodDetails) {
			if (prodDetails == null) {
				return;
			}
			this.productDetailList = new List<LG_ProductDetail>();
			for (LG_ProductDetail__c pd : prodDetails) {
				this.productDetailList.add(new LG_ProductDetail(pd));
			}
		}

		public void addPortingNumbers(List<LG_PortingNumber__c> portingNumbers) {
			if (portingNumbers == null) {
				return;
			}
			this.portingNumberList = new List<LG_PortingNumbersForSoho>();
			for (LG_PortingNumber__c pn : portingNumbers) {
				this.portingNumberList.add(new LG_PortingNumbersForSoho(pn));
			}
		}

		public void addLineItems(List<OpportunityLineItem> oppLineItems) {
			if (oppLineItems == null) {
				return;
			}
			this.opportunityLineItemList = new List<LG_OpportunityLineItem>();
			for (OpportunityLineItem oli : oppLineItems) {
				this.opportunityLineItemList.add(new LG_OpportunityLineItem(oli));
			}
		}

		public void addOperatorSwitch(
			List<cscfga__Attribute__c> operatorSwitchAttrs,
			OrderEntryData jsonData
		) {
			LG_OperatorSwitch os = new LG_OperatorSwitch();

			if (jsonData != null) {
				os.Overstappen = jsonData.operatorSwitch.requested;
				os.CurrentProvider = jsonData.operatorSwitch.currentProvider;
				os.ContractId = jsonData.operatorSwitch.currentContractNumber;
				os.AcceptTerminationFee = jsonData.operatorSwitch.potentialFeeAccepted;
			} else {
				for (cscfga__Attribute__c at : operatorSwitchAttrs) {
					if (at.Name == 'Overstappen' && at.cscfga__Display_Value__c != null) {
						os.Overstappen = at.cscfga__Display_Value__c == 'Yes';
					}
					if (
						at.Name == 'Current Internet Provider' &&
						(at.cscfga__Display_Value__c != null &&
						at.cscfga__Display_Value__c != '--None--')
					) {
						os.CurrentProvider = at.cscfga__Display_Value__c;
					}
					if (at.Name == 'ContractId' && at.cscfga__Display_Value__c != null) {
						os.ContractId = at.cscfga__Display_Value__c;
					}
					if (
						at.Name == 'Accept Termination Fee' &&
						at.cscfga__Display_Value__c != null
					) {
						os.AcceptTerminationFee = at.cscfga__Display_Value__c == 'Yes';
					}
				}
			}
			this.operatorswitchList = new List<LG_OperatorSwitch>{ os };
		}
	}

	global class LG_OpportunityLineItem {
		webService String PricebookEntryName { get; set; }
		webService String TotalPrice { get; set; }
		webService String OneOffPrice { get; set; }
		webService String Type { get; set; }
		webService Boolean MainOLI { get; set; }

		public LG_OpportunityLineItem(OpportunityLineItem oli) {
			this.PricebookEntryName = oli.ProductName__c;
			this.TotalPrice = String.valueOf(oli.TotalPrice);
			this.OneOffPrice = String.valueOf(oli.LG_OneOffPrice__c);
			if (oli.LG_Type__c == 'One Off') {
				this.Type = 'One Time';
			} else {
				this.Type = oli.LG_Type__c;
			}
			this.MainOLI = oli.LG_MainOLI__c;
		}
	}

	global class LG_OpportunityContactRole {
		webService String Role { get; set; }
		webService String Name { get; set; }
		webService String Salutation { get; set; }
		webService String LastName { get; set; }
		webService String Phone { get; set; }
		webService String MobilePhone { get; set; }
		webService String Fax { get; set; }
		webService String Email { get; set; }
		webService Date Birthdate { get; set; }

		public LG_OpportunityContactRole(OpportunityContactRole ocr) {
			this.Role = ocr.Role;
			this.Name = ocr.Contact.Name;
			this.Salutation = ocr.Contact.Salutation;
			this.LastName = ocr.Contact.LastName;
			this.Phone = ocr.Contact.Phone;
			this.MobilePhone = ocr.Contact.MobilePhone;
			this.Fax = ocr.Contact.Fax;
			this.Email = ocr.Contact.Email;
			this.Birthdate = ocr.Contact.Birthdate;
		}
	}

	global class LG_OperatorSwitch {
		webService Boolean Overstappen { get; set; }
		webService String CurrentProvider { get; set; }
		webService String ContractId { get; set; }
		webService Boolean AcceptTerminationFee { get; set; }
	}

	global class LG_ProductDetail {
		webService String Name { get; set; }
		webService String Product { get; set; }
		webService String Value { get; set; }

		public LG_ProductDetail(LG_ProductDetail__c prodDetail) {
			this.Name = prodDetail.LG_Name__c;
			this.Product = prodDetail.LG_Product__c;
			this.Value = prodDetail.LG_Value__c;
		}
	}

	global class LG_PortingNumbersForSoho {
		webService Boolean PortingIn { get; set; }
		webService Date PortingDate { get; set; }
		webService String PhoneNumber { get; set; }
		webService String Operator { get; set; }
		webService String CustomerName { get; set; }
		webService String InDirectory { get; set; }

		public LG_PortingNumbersForSoho(LG_PortingNumber__c portingNumber) {
			if (portingNumber.LG_PhoneNumber__c != null) {
				this.PortingIn = true;
			}
			this.PortingDate = portingNumber.LG_PortingDate__c;
			this.PhoneNumber = portingNumber.LG_PhoneNumber__c;
			this.Operator = portingNumber.LG_Operator__c;
			this.CustomerName = portingNumber.LG_CustomerName__c;
			this.InDirectory = portingNumber.LG_InDirectory__c;
		}
	}

	/**
	 * Gets the order data from SFDC and send it to external systems.
	 * @param  numberOfDays Last number of days for which orders will be retreived
	 * @param  typeOfOrders Type of orders that will be retreived.
	 */
	webService static Response getOrders(Integer numberOfDays, String typeOfOrders) {
		Response resp = new Response();
		resp.error = '';
		resp.status = 'Success';
		resp.orderData = new List<OrderData>();
		String orderType = typeOfOrders.toLowerCase();
		try {
			if (orderType == 'soho' || orderType == 'all') {
				// Get Opportunity Line Items & Opportunities
				Map<Id, List<OpportunityLineItem>> olisByOppId = getOpportunityLineItems(
					numberOfDays
				);
				Map<Id, Opportunity> opps = getOpportunities(olisByOppId.keySet());
				Map<Id, Attachment> oppIdsJsonAttsMap = getJsonData(opps.keySet());
				Map<Id, Site__c> oppIdsSitesMap = getSites(oppIdsJsonAttsMap.values());

				// Get Product Configurations
				Map<Id, cscfga__Product_Configuration__c> prodConfigs = getProductConfigs(
					opps.keySet()
				);

				Map<Id, cscfga__Product_Configuration__c> prodConfigsByOppId = new Map<Id, cscfga__Product_Configuration__c>();
				for (cscfga__Product_Configuration__c prodConfig : prodConfigs.values()) {
					prodConfigsByOppId.put(
						prodConfig.cscfga__Product_Basket__r.cscfga__Opportunity__c,
						prodConfig
					);
				}

				// Get Billing Accounts
				Map<Id, csconta__Billing_Account__c> billingAccountsByConfigId = getBillingAccounts(
					prodConfigs.keySet()
				);

				// Get Overstappen Service
				Map<Id, List<cscfga__Attribute__c>> overstappenAttrsByConfigId = getOverstappenAttributes(
					prodConfigs.keySet()
				);

				// Build Order Data for each Opportunity
				for (Opportunity opp : opps.values()) {
					OrderData order = new OrderData();

					cscfga__Product_Configuration__c prodConfig = prodConfigsByOppId.get(opp.Id);
					csconta__Billing_Account__c billingAcc = billingAccountsByConfigId.get(
						prodConfig?.Id
					);
					List<cscfga__Attribute__c> overstappenAttrs = overstappenAttrsByConfigId.get(
						prodConfig?.Id
					);

					OrderEntryData jsonData;
					if (
						oppIdsJsonAttsMap.keySet().size() > 0 &&
						oppIdsJsonAttsMap.containsKey(opp.Id)
					) {
						jsonData = (OrderEntryData) JSON.deserialize(
							oppIdsJsonAttsMap.get(opp.Id).Body.toString(),
							OrderEntryData.class
						);
						jsonData.init();
					}

					Site__c st;
					if (oppIdsSitesMap != null && oppIdsSitesMap.containsKey(opp.Id)) {
						st = oppIdsSitesMap.get(opp.Id);
					}

					order.opportunityDetail = new LG_Opportunity(
						opp,
						billingAcc,
						prodConfig,
						jsonData,
						st
					);
					order.opportunityDetail.addLineItems(olisByOppId.get(opp.Id));
					order.opportunityDetail.addOperatorSwitch(overstappenAttrs, jsonData);
					resp.orderData.add(order);
				}
			}
		} catch (Exception e) {
			resp.status = 'Error';
			String errorMessage = e.getMessage();
			errorMessage = errorMessage + '\nStack Trace:-- ' + e.getStackTraceString();
			resp.error = String.valueof(errorMessage);
		}
		return resp;
	}

	/**
	 * Gets Map of Opportunity Line Items grouped by Opportunity ID.
	 * @param  numberOfDays Last number of days for which Opportunity and Products should be retreived
	 */
	private static Map<Id, List<OpportunityLineItem>> getOpportunityLineItems(
		Integer numberOfDays
	) {
		Map<String, LG_OrderWebserviceVariables__c> settings = LG_OrderWebserviceVariables__c.getAll();
		String oppStageName = settings.get('Opportunity_Status_for_SoHo_Products').LG_Value__c;
		String segmentName = settings.get('Segment_OpportunityLineItem_SoHo').LG_Value__c;

		String query = 'SELECT PricebookEntry.Name, TotalPrice, LG_OneOffPrice__c, LG_Type__c, LG_MainOLI__c, OpportunityId, Opportunity.StageName, LG_Segment__c, ProductName__c ';
		query += 'FROM OpportunityLineItem WHERE Opportunity.StageName = :oppStageName AND LG_Segment__c = :segmentName ';
		query += 'AND Opportunity.CreatedDate = LAST_N_DAYS: ' + String.valueOf(numberOfDays);

		List<OpportunityLineItem> olis = Database.query(query);
		Map<Id, List<OpportunityLineItem>> olisByOppId = GeneralUtils.groupByIDField(
			olis,
			'OpportunityId'
		);
		return olisByOppId;
	}

	/**
	 * Gets Map of Opportunities based on Opportunity IDs
	 * Gets related Opportunity Contact Roles, Product Details and Porting Numbers
	 * @param  oppIds Opportunity IDs.
	 */
	private static Map<Id, Opportunity> getOpportunities(Set<Id> oppIds) {
		return new Map<Id, Opportunity>(
			[
				SELECT
					Id,
					Type,
					Account.LG_Segment__c,
					Account.LG_CustomAccountNumber__c,
					Account.LG_Footprint__c,
					Description,
					Name,
					AccountId,
					Account.Name,
					Account.KVK_number__c,
					LG_PreferredInstallationDate__c,
					LG_PreferredInstallationTime__c,
					Account.Phone,
					PartnerAccountId,
					PartnerAccount.Name,
					LG_NEWSalesChannel__c,
					Account.SF1_Segment__c,
					LG_PostalCountry__c,
					Account.LG_PostalStreet__c,
					Account.LG_PostalHouseNumber__c,
					Account.LG_PostalHouseNumberExtension__c,
					Account.LG_PostalPostalCode__c,
					Account.LG_PostalCity__c,
					Account.LG_PostalCountry__c,
					(
						SELECT
							Role,
							OpportunityId,
							Contact.Salutation,
							Contact.Name,
							Contact.LastName,
							Contact.Phone,
							Contact.MobilePhone,
							Contact.Fax,
							Contact.Email,
							Contact.Birthdate
						FROM OpportunityContactRoles
					),
					(
						SELECT LG_Name__c, LG_Product__c, LG_Value__c, LG_Opportunity__c
						FROM Product_Details__r
					),
					(
						SELECT
							LG_PortingDate__c,
							LG_PhoneNumber__c,
							LG_Operator__c,
							LG_CustomerName__c,
							LG_InDirectory__c,
							LG_Opportunity__c
						FROM Porting_Numbers__r
					)
				FROM Opportunity
				WHERE Id IN :oppIds
			]
		);
	}

	private static Map<Id, Attachment> getJsonData(Set<Id> oppIds) {
		List<Attachment> oppJsonAtts = [
			SELECT Id, Body, ParentId
			FROM Attachment
			WHERE ParentId IN :oppIds AND Name = 'OrderEntryData.json'
		];

		Map<Id, Attachment> oppIdsJsonAttsMap = new Map<Id, Attachment>();
		for (Attachment att : oppJsonAtts) {
			oppIdsJsonAttsMap.put(att.ParentId, att);
		}
		return oppIdsJsonAttsMap;
	}

	private static Map<Id, Site__c> getSites(List<Attachment> oppAtts) {
		Map<Id, Id> siteIdsOppIdsMap = new Map<Id, Id>();
		for (Attachment att : oppAtts) {
			OrderEntryData jsonData = new OrderEntryData();
			if (oppAtts != null) {
				jsonData = (OrderEntryData) JSON.deserialize(
					att.Body.toString(),
					OrderEntryData.class
				);
				jsonData.init();
				siteIdsOppIdsMap.put(jsonData.siteId, att.ParentId);
			}
		}

		Set<Id> siteIds = siteIdsOppIdsMap.keySet();
		Map<Id, Site__c> sitesMap = new Map<Id, Site__c>(
			[
				SELECT
					Id,
					Site_Street__c,
					Site_House_Number__c,
					Site_House_Number_Suffix__c,
					Site_Postal_Code__c,
					Site_City__c,
					Country__c
				FROM Site__c
				WHERE Id IN :siteIds
			]
		);

		Map<Id, Site__c> oppIdsSitesMap = new Map<Id, Site__c>();
		for (Id siteId : siteIdsOppIdsMap.keySet()) {
			oppIdsSitesMap.put(siteIdsOppIdsMap.get(siteId), sitesMap.get(siteId));
		}
		return oppIdsSitesMap;
	}

	/**
	 * Gets Map of Product Configurations based on Opportunity IDs
	 * @param  oppIds Opportunity IDs
	 */
	private static Map<Id, cscfga__Product_Configuration__c> getProductConfigs(Set<Id> oppIds) {
		return new Map<Id, cscfga__Product_Configuration__c>(
			[
				SELECT
					Id,
					cscfga__Product_Basket__c,
					cscfga__Product_Basket__r.csordtelcoa__Synchronised_with_Opportunity__c,
					LG_Address__c,
					LG_InstallationWishDate__c,
					cscfga__Contract_Term__c,
					cscfga__Parent_Configuration__c,
					cscfga__Product_Basket__r.cscfga__Opportunity__c,
					LG_Address__r.LG_Footprint__c,
					LG_Address__r.LG_SharedOfficeBuilding__c,
					LG_Address__r.LG_COAXConnectionLocation__c,
					LG_Address__r.cscrm__Street__c,
					LG_Address__r.cscrm__City__c,
					LG_Address__r.LG_HouseNumber__c,
					LG_Address__r.LG_HouseNumberExtension__c,
					LG_Address__r.cscrm__Zip_Postal_Code__c,
					LG_Address__r.cscrm__Country__c
				FROM cscfga__Product_Configuration__c
				WHERE
					cscfga__Product_Basket__r.cscfga__Opportunity__c IN :oppIds
					AND cscfga__Product_Basket__r.csordtelcoa__Synchronised_with_Opportunity__c = TRUE
					AND cscfga__Parent_Configuration__c = NULL
			]
		);
	}

	/**
	 * Gets Billing Accounts based on Product Configuration IDs
	 * @param  productConfigIds Product Configuration IDs
	 */
	private static Map<Id, csconta__Billing_Account__c> getBillingAccounts(
		Set<Id> productConfigIds
	) {
		String billingAccAttrName = LG_OrderWebserviceVariables__c.getAll()
			.get('AttributeFieldName_For_BillingAccount')
			.LG_Value__c;

		List<cscfga__Attribute_Field__c> attrFields = [
			SELECT
				Name,
				cscfga__Attribute__r.cscfga__Product_Configuration__c,
				cscfga__Attribute__r.cscfga__Is_Line_Item__c,
				cscfga__Value__c
			FROM cscfga__Attribute_Field__c
			WHERE
				Name = :billingAccAttrName
				AND cscfga__Attribute__r.cscfga__Is_Line_Item__c = TRUE
				AND cscfga__Attribute__r.cscfga__Product_Configuration__c IN :productConfigIds
		];
		Set<Id> billingAccountIds = new Set<Id>();
		Map<Id, Id> billingAccountIdsByConfigIds = new Map<Id, Id>();
		for (cscfga__Attribute_Field__c attrField : attrFields) {
			billingAccountIdsByConfigIds.put(
				attrField.cscfga__Attribute__r.cscfga__Product_Configuration__c,
				attrField.cscfga__Value__c
			);
			billingAccountIds.add(attrField.cscfga__Value__c);
		}
		Map<Id, csconta__Billing_Account__c> billingAccountsMap = new Map<Id, csconta__Billing_Account__c>(
			[
				SELECT
					Id,
					csconta__Street__c,
					csconta__City__c,
					csconta__Postcode__c,
					csconta__Country__c,
					LG_HouseNumber__c,
					LG_HouseNumberExtension__c,
					LG_BankAccountHolder__c,
					LG_BankAccountNumberIBAN__c,
					LG_CustomerReference__c,
					LG_PaymentType__c
				FROM csconta__Billing_Account__c
				WHERE Id IN :billingAccountIds
			]
		);
		Map<Id, csconta__Billing_Account__c> billingAccountsByConfig = new Map<Id, csconta__Billing_Account__c>();
		for (Id configId : billingAccountIdsByConfigIds.keySet()) {
			billingAccountsByConfig.put(
				configId,
				billingAccountsMap.get(billingAccountIdsByConfigIds.get(configId))
			);
		}
		return billingAccountsByConfig;
	}

	/**
	 * Gets Overstappen Service Attributes based on Product Configuration IDs
	 * @param  productConfigIds  Product Configuration IDs
	 */
	private static Map<Id, List<cscfga__Attribute__c>> getOverstappenAttributes(
		Set<Id> productConfigIds
	) {
		String attrNamesCSV = LG_OrderWebserviceVariables__c.getAll()
			.get('Attribute_Names_Overstappen')
			.LG_Value__c;
		Set<String> attrNames = new Set<String>();
		for (String attrName : attrNamesCSV.split(',')) {
			attrNames.add(attrName);
		}
		List<cscfga__Attribute__c> attrs = [
			SELECT Name, cscfga__Product_Configuration__c, cscfga__Display_Value__c
			FROM cscfga__Attribute__c
			WHERE Name IN :attrNames AND cscfga__Product_Configuration__c IN :productConfigIds
			ORDER BY Name DESC
		];
		return GeneralUtils.groupByIDField(attrs, 'cscfga__Product_Configuration__c');
	}
}