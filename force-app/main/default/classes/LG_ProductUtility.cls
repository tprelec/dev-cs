public class LG_ProductUtility {
    
    private static OrderType__c orderType = [SELECT id FROM OrderType__c WHERE Name = 'Ziggo'];
    
    public static List<CS_OLI_Product2_Map> returnOliProduct2Mappings() {
        List<CS_OLI_Product2_Map> oliProduct2Map = new List<CS_OLI_Product2_Map>();
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Erotiek', 'ProductRecordP_Erotiek'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Film 1', 'ProductRecordP_Film_1'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Film_1_Sport_1', 'ProductRecordP_Film_1_Sport_1'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Fox_Sports_Eredivisie', 'ProductRecordP_Fox_Sports_Eredivisie'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Fox_Sports_Ere_Int', 'ProductRecordP_Fox_Sports_Ere_Int'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Fox_Sports_International', 'ProductRecordP_Fox_Sports_International'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Gay_Lifestyle', 'ProductRecordP_Gay_Lifestyle'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_HBO', 'ProductRecordP_HBO'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Hindi', 'ProductRecordP_Hindi'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Sport_1', 'ProductRecordP_Sport_1'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Turks', 'ProductRecordP_Turks'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Ziggo_Movies_Series', 'ProductRecordP_Ziggo_Movies_Series'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Ziggo_Movies_Series_XL', 'ProductRecordP_Ziggo_Movies_Series_XL'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'P_Ziggo_Sport_Totaal_Ere', 'ProductRecordP_Ziggo_Sport_Totaal_Ere'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Zakelijk Internet', 'Total Basic Price', 'ProductRecord'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Additional hardware', 'Recurring Total Price', 'ProductRecord'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Additional Product', 'Total Recurring Price', 'ProductRecord'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Additional Product', 'Total One Off Price', 'ProductRecord'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('ZZP Telephony', 'Extra Line Telephony Option Price', 'ProductRecordTelephonyOptions'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('ZZP Telephony', 'Telephone Option Price', 'ProductRecordTelephony'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('ZZP Telephony', 'TV Price', 'ProductRecordTVOption'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'TV Price', 'ProductRecordTVOption'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('ZZP Telephony', 'Extra Telephone Line', 'ProductRecordTelephonyExtraPrice'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Zakelijk Internet', 'Internetkabel Price', 'ProductRecordInternetkabelPrice'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Zakelijk Internet', 'Stopcontact Price', 'ProductRecordStopcontactPrice'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Zakelijk Internet', 'Discount', ''));
        oliProduct2Map.add(new CS_OLI_Product2_Map('TV', 'Media Box Price', 'ProductRecordMediaBox'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Zakelijk Internet', 'Advance Security Item', 'ProductRecordAdvanceSecItem'));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Zakelijk Internet', 'Closing Deal Discount', ''));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Zakelijk Internet', 'Additional Discount', ''));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Zakelijk Internet', 'Advance Security Discount', ''));
        oliProduct2Map.add(new CS_OLI_Product2_Map('Zakelijk Internet', 'Security PIN', 'ProductRecordSecurityPIN'));
        return oliProduct2Map;
    }

    public static void CreateOLIs(Set<string> setProductBasketId) {
        /*
        --select all attributes which are line items
        --check if combinations of Product Family and Line Item Description exist in Product2
        --for the ones which do exist - get Product2.Id
        --for the others - insert and get Product2.Id

        --select standard price book

        --check if there is an existing combination of PriceBook and Product2 in PriceBookEntry
        --if it doesn't exist - create and get PriceBookEntry.Id
        --if it does exist - get PriceBookEntry.Id

        -- enter all PriceBookEntries into OLIs
        */

        Map<string, string> mapProductBasketIdPriceBookId = new LG_AssignnPriceBookToProductBasketImpl().AssignPriceBook(setProductBasketId);


        List<cscfga__Attribute__c> lstAttribute = [SELECT Id, cscfga__is_active__c, cscfga__Is_Line_Item__c, cscfga__Line_Item_Description__c, cscfga__Line_Item_Sequence__c,
                                                       cscfga__Price__c, cscfga__List_Price__c, cscfga__Product_Configuration__c, cscfga__Product_Configuration__r.cscfga__Product_Basket__c,
                                                       cscfga__Product_Configuration__r.cscfga__Product_Family__c, Name , cscfga__Recurring__c, cscfga__Attribute_Definition__r.cscfga__Line_Item_Sequence__c,
                                                       cscfga__Attribute_Definition__r.LG_OLIType__c, cscfga__Product_Configuration__r.cscfga__Contract_Term__c
                                                       FROM cscfga__Attribute__c
                                                       WHERE cscfga__Is_Line_Item__c = true AND cscfga__is_active__c = true AND
                                                       cscfga__Product_Configuration__r.cscfga__Product_Basket__c in : mapProductBasketIdPriceBookId.keyset()];

        System.debug('****lstAttribute=' + lstAttribute);
        
        List<CS_OLI_Product2_Map> oliProduct2Map = returnOliProduct2Mappings();
        
        Set<String> attributeNames = new Set<String>();
        
        for(CS_OLI_Product2_Map mapElement : oliProduct2Map) {
            if(mapElement.productAttributeName != '' && !attributeNames.contains(mapElement.productAttributeName)) {
                attributeNames.add(mapElement.productAttributeName);
            }
        }
        
        List<cscfga__Attribute__c> additionalListAttribute = [SELECT id, cscfga__Value__c, cscfga__Line_Item_Description__c, cscfga__Product_Configuration__r.cscfga__Contract_Term__c,
                                                                cscfga__Product_Configuration__c, cscfga__Product_Configuration__r.cscfga__Product_Family__c, cscfga__Recurring__c,
                                                                Name, cscfga__Product_Configuration__r.cscfga__Product_Basket__c, cscfga__Is_Line_Item__c, cscfga__List_Price__c,
                                                                cscfga__Attribute_Definition__c, cscfga__Attribute_Definition__r.LG_OLIType__c, cscfga__Price__c
                                                                FROM cscfga__Attribute__c WHERE Name in :attributeNames AND cscfga__is_active__c = true AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c in : mapProductBasketIdPriceBookId.keyset()]; //AND cscfga__Is_Line_Item__c = true 
        
        List<Id> listOfProduct2Ids = new List<Id>();
        
        for(cscfga__Attribute__c attribute : additionalListAttribute) {
            listOfProduct2Ids.add(attribute.cscfga__Value__c);
        }
        
        
        List<Product2> additionalProducts2 = [SELECT id, Description, Name FROM Product2 WHERE id IN :listOfProduct2Ids];
        
        System.debug('****additionalProducts2=' + additionalProducts2);

        System.debug('****additionalListAttribute=' + additionalListAttribute);

        //this is the map where the keys are: ProductFamily and ProductName (LineItemDescription)
        Map<string, Map<string, Product>> mapProductFamilymapProduct = CreateProducts2(lstAttribute, additionalListAttribute, additionalProducts2, oliProduct2Map);
        System.debug('****mapProductFamilymapProduct=' + mapProductFamilymapProduct);

        //this is the map where the keys are: PriceBookId and Product2Id
        Map<string, Map<string, PBEntry>> mapPriceBookIdmapPBEntry = MakePriceBookPBEntriesMap(mapProductBasketIdPriceBookId, lstAttribute, mapProductFamilymapProduct);
        System.debug('****MakePriceBookPBEntriesMap mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);

        //this function is void because it just modifies mapPriceBookIdmapPBEntry, so no need for return
        CreatePriceBookEntries(mapPriceBookIdmapPBEntry);
        System.debug('****CreatePriceBookEntries mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);

        //this function takes all structures created before and generates OLIs
        CreateOLIs(mapProductBasketIdPriceBookId, lstAttribute, mapProductFamilymapProduct, mapPriceBookIdmapPBEntry);
    }

    private static void CreateOLIs(Map<string, string> mapProductBasketIdPriceBookId, List<cscfga__Attribute__c> lstAttribute, Map<string, Map<string, Product>> mapProductFamilymapProduct, Map<string, Map<string, PBEntry>> mapPriceBookIdmapPBEntry) {

        List<OpportunityLineItem> lstOLI = new List<OpportunityLineItem>();

        Set<Id> productConfigurationIds = new Set<Id>();
        Set<Id> attributeItemsIds = new Set<Id>();
        Set<string> setProductBasketId = new Set<string>();
        Set<string> segment = new Set<string> { 'small', 'soho' };
        Set<string> productDetail = new Set<string> { 'ProductDetail' };

        for (cscfga__Attribute__c tmpAttribute : lstAttribute) {
            setProductBasketId.add(tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c);
            productConfigurationIds.add(tmpAttribute.cscfga__Product_Configuration__c);
        }

        // Get all Line Item Id's
        for (cscfga__Attribute__c a : lstAttribute) {

            if (a.cscfga__Is_Line_Item__c == true) {
                attributeItemsIds.add(a.Id);
            }
        }

        Set<string> attributeNames = new Set<string> { 'Segment', 'ProductDetail', 'Quantity', 'OLIType', 'OneOffPrice3rdParty' };
        List<cscfga__Attribute_Field__c> attributeFields = [SELECT Id, Name, cscfga__Attribute__c, cscfga__Value__c FROM cscfga__Attribute_Field__c WHERE Name IN :attributeNames AND cscfga__Attribute__c IN :attributeItemsIds];

        Map<Id, cscfga__Product_Basket__c> mapProductBasket = new Map<Id, cscfga__Product_Basket__c>([SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id in : setProductBasketId]);

        Map<Id, cscfga__Product_Configuration__c> productConfigurations = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, LG_Address__c, cscfga__Root_Configuration__c, cscfga__Root_Configuration__r.LG_Address__c FROM cscfga__Product_Configuration__c WHERE Id IN :productConfigurationIds]);

        System.debug('****mapProductBasket: ' + mapProductBasket);

        for (cscfga__Attribute__c tmpAttribute : lstAttribute) {

            Boolean MainOLI = false;
            string Type = null;

            if (tmpAttribute.cscfga__Attribute_Definition__r.LG_OLIType__c == 'Main OLI') {
                MainOLI = true;                
            } else if ((tmpAttribute.cscfga__Attribute_Definition__r.LG_OLIType__c == 'Promotional Discount') || 
                (tmpAttribute.cscfga__Attribute_Definition__r.LG_OLIType__c == 'Promotional Action') || 
                (tmpAttribute.cscfga__Attribute_Definition__r.LG_OLIType__c == 'Closing Deal') ||
                (tmpAttribute.cscfga__Attribute_Definition__r.LG_OLIType__c == '3rd Party Charge')) {
                    Type = tmpAttribute.cscfga__Attribute_Definition__r.LG_OLIType__c;
            }

            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            System.debug('****tmpProductFamily=' + tmpProductFamily);
            
            string tmpLineItemDescription = LG_ProductUtility.RemoveAmpresand(tmpAttribute.cscfga__Line_Item_Description__c);
            System.debug('****tmpLineItemDescription=' + tmpLineItemDescription);

            string tmpProductBasketId = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c;
            System.debug('****tmpProductBasketId=' + tmpProductBasketId);

            //string tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
            // ND - 17.12.
            String tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
            if(tmpPriceBookId == null && tmpProductBasketId != null) {
                tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId.substring(0, 15));
            }
            System.debug('****tmpPriceBookId=' + tmpPriceBookId);

            double tmpUnitPrice = tmpAttribute.cscfga__Price__c;
            System.debug('****tmpUnitPrice=' + tmpUnitPrice);

            Map<string, Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
            System.debug('****mapProduct=' + mapProduct);

            Product tmpProduct = mapProduct.get(tmpLineItemDescription);
            System.debug('****tmpProduct=' + tmpProduct);

            string tmpProduct2Id = tmpProduct.Prod2.Id;
            System.debug('****tmpProduct2Id=' + tmpProduct2Id);

            Map<string, PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPriceBookId);
            System.debug('****mapPBEntry=' + mapPBEntry);

            PBEntry tmpPBEntry = mapPBEntry.get(tmpProduct2Id);
            System.debug('****tmpPBEntry=' + tmpPBEntry);

            string tmpPriceBookEntryId = tmpPBEntry.PBE.Id;
            System.debug('****tmpPriceBookEntryId=' + tmpPriceBookEntryId);

            string tmpOpportunityId = mapProductBasket.get(tmpProductBasketId).cscfga__Opportunity__c;
            System.debug('****tmpOpportunityId=' + tmpOpportunityId);

            OpportunityLineItem tmpOpportunityLineItem = new OpportunityLineItem();

            tmpOpportunityLineItem.cscfga__Attribute__c = tmpAttribute.Id;
            tmpOpportunityLineItem.OpportunityId = tmpOpportunityId;
            tmpOpportunityLineItem.PricebookEntryId = tmpPriceBookEntryId;
            tmpOpportunityLineItem.LG_MainOLI__c = MainOLI;
            tmpOpportunityLineItem.Quantity = 1;
            tmpOpportunityLineItem.Product_Bundle_Name__c = tmpLineItemDescription;
            
            /**
             * Offerte sent for approval show contract term 1 month instead of 2 years
             *
             * @author Petar Miletic
             * @ticket SFDT-1131
             * @since  03/06/2016
            */
            if (tmpAttribute.cscfga__Product_Configuration__r.cscfga__Contract_Term__c != null) {
                tmpOpportunityLineItem.LG_ContractTerm__c = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Contract_Term__c;
            }

            double UnitPrice;

            if (tmpAttribute.cscfga__Price__c == null) {
                UnitPrice = 0;
            } else {
                UnitPrice = tmpAttribute.cscfga__Price__c;
            }
            
            if (tmpAttribute.cscfga__Recurring__c) {

                tmpOpportunityLineItem.UnitPrice = UnitPrice;
                tmpOpportunityLineItem.LG_OneOffPrice__c = 0; //this is implementation specific

                if (Type == null) {
                    tmpOpportunityLineItem.LG_Type__c = 'Recurring'; //this is implementation specific
                } else {
                    tmpOpportunityLineItem.LG_Type__c = Type;
                }

            } else {

                tmpOpportunityLineItem.UnitPrice = 0;
                tmpOpportunityLineItem.LG_OneOffPrice__c = UnitPrice; //this is implementation specific

                if (Type == null) {
                    tmpOpportunityLineItem.LG_Type__c = 'One Off'; //this is implementation specific
                } else {
                    tmpOpportunityLineItem.LG_Type__c = Type;
                }
            }

            // If attribute is line item copy segment value from Attribute_Definition to OpportunityLineItem (Opportunity Product Detail) and ticket SFDT-608 - Implement mapping
            if (tmpAttribute.cscfga__Is_Line_Item__c == true) {

                // Set Sequence__c
                if (tmpAttribute.cscfga__Attribute_Definition__c != null && tmpAttribute.cscfga__Attribute_Definition__r.cscfga__Line_Item_Sequence__c != null) {
                    tmpOpportunityLineItem.Sequence__c = tmpAttribute.cscfga__Attribute_Definition__r.cscfga__Line_Item_Sequence__c;
                } else {
                    tmpOpportunityLineItem.Sequence__c = tmpAttribute.cscfga__Line_Item_Sequence__c;
                }

                // Set LG_Address__c from Product Configuration
                if (tmpAttribute.cscfga__Product_Configuration__c != null && productConfigurations.keySet().contains(tmpAttribute.cscfga__Product_Configuration__c)) {

                    cscfga__Product_Configuration__c pc = productConfigurations.get(tmpAttribute.cscfga__Product_Configuration__c);

                    if (pc.cscfga__Root_Configuration__c != null && pc.cscfga__Root_Configuration__r.LG_Address__c != null) {

                        tmpOpportunityLineItem.LG_Address__c = pc.cscfga__Root_Configuration__r.LG_Address__c;
                    } else if (pc.LG_Address__c != null) {
                        tmpOpportunityLineItem.LG_Address__c = pc.LG_Address__c;
                    }
                }

                // Set Segment, quantity and product details
                for (cscfga__Attribute_Field__c a : attributeFields) {

                    if (tmpAttribute.Id == a.cscfga__Attribute__c && String.isNotBlank(a.cscfga__Value__c) && segment.contains(a.cscfga__Value__c.tolowercase())) {

                        tmpOpportunityLineItem.LG_Segment__c = a.cscfga__Value__c;
                    }

                    if (tmpAttribute.Id == a.cscfga__Attribute__c && String.isNotBlank(a.cscfga__Value__c) && productDetail.contains(a.Name)) {

                        tmpOpportunityLineItem.LG_ProductDetail__c = a.cscfga__Value__c;
                    }
                    
                    if (tmpAttribute.Id == a.cscfga__Attribute__c && String.isNotBlank(a.cscfga__Value__c) && a.Name == 'Quantity') {

                        tmpOpportunityLineItem.Quantity = decimal.valueOf(a.cscfga__Value__c);
                        
                        // SFDT-692 - Total Recurring price is displayed wrongly in the opportunity overview
                        if (tmpAttribute.cscfga__Recurring__c && tmpOpportunityLineItem.Quantity != 0) {
                            tmpOpportunityLineItem.UnitPrice = tmpOpportunityLineItem.UnitPrice / tmpOpportunityLineItem.Quantity;
                        }
                    }
                    
                    if (tmpAttribute.Id == a.cscfga__Attribute__c && String.isNotBlank(a.cscfga__Value__c) && a.Name == 'OLIType') {

                        tmpOpportunityLineItem.LG_OLIType__c = a.cscfga__Value__c;
                    }
                    
                    System.debug('****attrField=' + a.cscfga__Value__c + ' -- ' + a.Name);

                    // SFDT-1231 - Installation costs charged by 3rd party
                    if (tmpAttribute.Id == a.cscfga__Attribute__c && String.isNotBlank(a.cscfga__Value__c) && a.Name == 'OneOffPrice3rdParty') {

                        System.debug('****attrField3rdParty=' + a.cscfga__Value__c);

                        tmpOpportunityLineItem.LG_OneOffPrice3rdParty__c = decimal.valueOf(a.cscfga__Value__c);
                    }
                }
            }

            lstOLI.add(tmpOpportunityLineItem);
        }

        System.debug('****beforelstOLI=' + lstOLI);

        if (lstOLI.size() > 0) {
            insert lstOLI;
        }

        System.debug('****lstOLI=' + lstOLI);
    }


    private static Map<string, Map<string, PBEntry>> MakePriceBookPBEntriesMap(Map<string, string> mapProductBasketIdPriceBookId, List<cscfga__Attribute__c> lstAttribute, Map<string, Map<string, Product>> mapProductFamilymapProduct) {

        List<PBEntry> lstPBEntry = new List<PBEntry>();
        Map<string, Map<string, PBEntry>> mapPriceBookIdmapPBEntry = new Map<string, Map<string, PBEntry>>();

        System.debug('****mapProductBasketIdPriceBookId=' + mapProductBasketIdPriceBookId);

        for (cscfga__Attribute__c tmpAttribute : lstAttribute) {

            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            System.debug('****tmpProductFamily=' + tmpProductFamily);

            string tmpLineItemDescription = LG_ProductUtility.RemoveAmpresand(tmpAttribute.cscfga__Line_Item_Description__c);
            System.debug('****tmpLineItemDescription=' + tmpLineItemDescription);

            string tmpProductBasketId = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c;
            System.debug('****tmpProductBasketId=' + tmpProductBasketId);

            if (mapProductFamilymapProduct.containsKey(tmpProductFamily)) {
                Map<string, Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);

                if (mapProduct.containsKey(tmpLineItemDescription)) {
                    Product tmpProduct = mapProduct.get(tmpLineItemDescription);
                    System.debug('****tmpProduct=' + tmpProduct);

                    string tmpProduct2Id = tmpProduct.Prod2.Id;
                    System.debug('****tmpProduct2Id=' + tmpProduct2Id);

                    // ND - 17.12.
                    String tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
                    if(tmpPriceBookId == null && tmpProductBasketId != null) {
                        tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId.substring(0, 15));
                    }

                    //string tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
                    System.debug('****tmpPriceBookId=' + tmpPriceBookId);

                    PBEntry tmpPBEntry = new PBEntry();
                    tmpPBEntry.PriceBookId = tmpPriceBookId;
                    tmpPBEntry.Product2Id = tmpProduct2Id;

                    if (tmpAttribute.cscfga__Price__c == null) {
                        tmpPBEntry.UnitPrice = 0;
                    } else {
                        tmpPBEntry.UnitPrice = tmpAttribute.cscfga__Price__c;
                    }

                    tmpPBEntry.Name = tmpLineItemDescription;

                    if (mapPriceBookIdmapPBEntry.containsKey(tmpPriceBookId)) {

                        Map<string, PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPriceBookId);

                        if (!mapPBEntry.containsKey(tmpProduct2Id)) {
                            mapPBEntry.put(tmpProduct2Id, tmpPBEntry);
                        }

                    } else {

                        Map<string, PBEntry> mapPBEntry = new Map<string, PBEntry>();
                        mapPBEntry.put(tmpProduct2Id, tmpPBEntry);
                        mapPriceBookIdmapPBEntry.put(tmpPriceBookId, mapPBEntry);
                    }
                }
            }
        }

        System.debug('****mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        return mapPriceBookIdmapPBEntry;
    }

    private static void CreatePriceBookEntries(Map<string, Map<string, PBEntry>> mapPriceBookIdmapPBEntry) {

        System.debug('***mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);

        Set<string> setProduct2Id = new Set<string>();

        List<PricebookEntry> lstPricebookEntryInsert = new List<PricebookEntry>();

        for (Map<string, PBEntry> mapPBEntry : mapPriceBookIdmapPBEntry.values()) {

            for (PBEntry tmpPBEntry : mapPBEntry.values()) {
                setProduct2Id.add(tmpPBEntry.Product2Id);
            }
        }

        System.debug('***setProduct2Id=' + setProduct2Id);

        if (setProduct2Id.size() > 0) {

            Map<Id, PricebookEntry> mapPricebookEntry = new Map<Id, PricebookEntry> ([SELECT Id, IsActive, Name, Pricebook2Id, Product2Id, UnitPrice FROM PricebookEntry WHERE Product2Id in : setProduct2Id]);

            System.debug('***mapPricebookEntry=' + mapPricebookEntry);

            for (PricebookEntry tmpPricebookEntry : mapPricebookEntry.values()) {

                string tmpPricebook2Id = tmpPricebookEntry.Pricebook2Id;
                string tmpProduct2Id = tmpPricebookEntry.Product2Id;

                System.debug('***tmpPricebook2Id=' + tmpPricebook2Id);
                System.debug('***tmpProduct2Id=' + tmpProduct2Id);

                if (mapPriceBookIdmapPBEntry.containsKey(tmpPricebook2Id)) {

                    Map<string, PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPricebook2Id);

                    if (mapPBEntry.containsKey(tmpProduct2Id)) {
                        PBEntry tmpPBEntry = mapPBEntry.get(tmpProduct2Id);
                        tmpPBEntry.PBEntryId = tmpPricebookEntry.Id;
                    }
                }
            }

            for (Map<string, PBEntry> mapPBEntry : mapPriceBookIdmapPBEntry.values()) {

                for (PBEntry tmpPBEntry : mapPBEntry.values()) {

                    System.debug('***tmpPBEntry=' + tmpPBEntry);

                    if ((tmpPBEntry.PBEntryId == '') || (tmpPBEntry.PBEntryId == null)) {
                        PricebookEntry tmpPricebookEntry = new PricebookEntry();
                        tmpPricebookEntry.IsActive = true;
                        //tmpPricebookEntry.Name=tmpPBEntry.Name;
                        tmpPricebookEntry.Pricebook2Id = tmpPBEntry.PriceBookId;
                        tmpPricebookEntry.Product2Id = tmpPBEntry.Product2Id;
                        tmpPricebookEntry.UnitPrice = tmpPBEntry.UnitPrice;

                        tmpPBEntry.PBE = tmpPricebookEntry;

                        lstPricebookEntryInsert.add(tmpPricebookEntry);

                    } else {

                        PricebookEntry tmpPricebookEntry = mapPricebookEntry.get(tmpPBEntry.PBEntryId);
                        tmpPBEntry.PBE = tmpPricebookEntry;
                    }
                }
            }
        }

        if (lstPricebookEntryInsert.size() > 0) {
            insert lstPricebookEntryInsert;
        }
    }

    private static string RemoveAmpresand(string pLineItemDescription) {
        //example: Connect ZZP Internet Start &amp; Play Start

        if (pLineItemDescription != null) {

            if (pLineItemDescription.contains('&amp;')) {
                pLineItemDescription = pLineItemDescription.replace('&amp;', '&');
            }
        } else {
            pLineItemDescription = '';
        }

        return pLineItemDescription;
    }

    private static Map<string, Map<string, Product>> CreateProducts2(List<cscfga__Attribute__c> lstAttribute, List<cscfga__Attribute__c> additionalListAttributes, List<Product2> additionalProducts2, List<CS_OLI_Product2_Map> oliProduct2Map) {

        System.debug('***lstAttribute=' + lstAttribute);

        Map<string, Map<string, Product>> mapProductFamilymapProduct = new Map<string, Map<string, Product>>();

        for (cscfga__Attribute__c tmpAttribute : lstAttribute) {

            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            string tmpLineItemDescription = LG_ProductUtility.RemoveAmpresand(tmpAttribute.cscfga__Line_Item_Description__c);

            Product2 product2Found = null;
            
            Set<cscfga__Attribute__c> listOfConfigurationAttributes = new Set<cscfga__Attribute__c>();
            
            for(cscfga__Attribute__c tmpAdditionalAttribute : additionalListAttributes) {
                if(tmpAdditionalAttribute.cscfga__Product_Configuration__c == tmpAttribute.cscfga__Product_Configuration__c) {
                    listOfConfigurationAttributes.add(tmpAdditionalAttribute);
                }
            }
            
            // TODO needs to be optimized
            for(CS_OLI_Product2_Map elementMap : oliProduct2Map) {
                if(elementMap.lineItemAttributeName == tmpAttribute.Name) {
                    for(cscfga__Attribute__c tmpAdditionalAttribute : listOfConfigurationAttributes) {
                        if(tmpAdditionalAttribute.Name == elementMap.productAttributeName) {
                            for(Product2 p2 : additionalProducts2) {
                                if(p2.Id == tmpAdditionalAttribute.cscfga__Value__c) {
                                    product2Found = p2;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            

            if (mapProductFamilymapProduct.containsKey(tmpProductFamily)) {

                System.debug('***contains tmpProductFamily=' + tmpProductFamily);
                Map<string, Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                System.debug('***mapProduct=' + mapProduct);

                if (!mapProduct.containsKey(tmpLineItemDescription)) {

                    System.debug('***does not contain tmpLineItemDescription=' + tmpLineItemDescription);
                    Product tmpProduct = new Product();
                    tmpProduct.ProductFamily = tmpProductFamily;
                    tmpProduct.LiniItemDescription = tmpLineItemDescription;
                    if(product2Found != null) {
                        tmpProduct.Prod2 = product2Found;
                        tmpProduct.Product2Id = product2Found.Id;
                    }
                    mapProduct.put(tmpLineItemDescription, tmpProduct);
                }

            } else {

                System.debug('***does not contain tmpProductFamily=' + tmpProductFamily);
                Map<string, Product> mapProduct = new Map<string, Product>();
                Product tmpProduct = new Product();
                tmpProduct.ProductFamily = tmpProductFamily;
                tmpProduct.LiniItemDescription = tmpLineItemDescription;
                if(product2Found != null) {
                    tmpProduct.Prod2 = product2Found;
                    tmpProduct.Product2Id = product2Found.Id;
                }
                mapProduct.put(tmpLineItemDescription, tmpProduct);
                mapProductFamilymapProduct.put(tmpProductFamily, mapProduct);
            }
        }

        System.debug('***mapProductFamilymapProduct=' + mapProductFamilymapProduct);

        if (mapProductFamilymapProduct.size() > 0) {

            //Commented out to check Production P1
            Map<Id, Product2> mapProduct2 = new Map<Id, Product2>(); //Map maintained so that minimal code change to be done
            /*Map<Id, Product2> mapProduct2 = new Map<Id, Product2>([SELECT Family, Id, IsActive, Name FROM Product2 WHERE IsActive = true and Family in : mapProductFamilymapProduct.keySet()]);

            System.debug('Query: select Family, Id, IsActive, Name from Product2 where IsActive=true and Family in ' + mapProductFamilymapProduct.keySet());

            for (Product2 tmpProduct2 : mapProduct2.values()) {

                if (mapProductFamilymapProduct.containsKey(tmpProduct2.Family)) {
                    Map<string, Product> mapProduct = mapProductFamilymapProduct.get(tmpProduct2.Family);

                    if (mapProduct.containsKey(tmpProduct2.Name)) {
                        Product tmpProduct = mapProduct.get(tmpProduct2.Name);
                        tmpProduct.Product2Id = tmpProduct2.Id;
                    }
                }
            }
*/
            List<Product2> lstProduct2Insert = new List<Product2>();

            for (string tmpProductFamily : mapProductFamilymapProduct.keySet()) {

                System.debug('****tmpProductFamily=' + tmpProductFamily);

                Map<string, Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);

                System.debug('****mapProduct=' + mapProduct);
                
                
                for (Product tmpProduct : mapProduct.values()) {
                    
                    System.debug('****mapProduct: ' + mapProduct);

                    System.debug('****tmpProduct=' + tmpProduct);

                    if ((tmpProduct.Product2Id == '') || (tmpProduct.Product2Id == null)) {
                        Product2 tmpProduct2 = new Product2();
                        tmpProduct2.Family = tmpProductFamily;
                        tmpProduct2.Name = tmpProduct.LiniItemDescription;
                        
                        if(tmpProduct.Prod2 == null) {
                            tmpProduct.Prod2 = tmpProduct2;  
                        }
                        
                        // ND - added 17.12.2019
                        tmpProduct2.OrderType__c = orderType.Id;
                        tmpProduct2.Product_Line__c = 'fZiggo Only';

                        lstProduct2Insert.add(tmpProduct2);

                    } else {

                        Product2 tmpProduct2 = mapProduct2.get(tmpProduct.Product2Id);
                        
                        if(tmpProduct.Prod2 == null) {
                            tmpProduct.Prod2 = tmpProduct2;  
                        }
                    }
                }
            }

            System.debug('****before lstProduct2Insert=' + lstProduct2Insert);

            if (lstProduct2Insert.size() > 0) {
                insert lstProduct2Insert;
            }

            System.debug('****lstProduct2Insert=' + lstProduct2Insert);
        }

        System.debug('****mapProductFamilymapProduct=' + mapProductFamilymapProduct);

        return mapProductFamilymapProduct;
    }

    /*
     * Deprecated
    */
    public static void DeleteHardOLIs(Set<string> setProductBasketId) {

        List<cscfga__Product_Basket__c> lstPB = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id in : setProductBasketId];

        Set<Id> setOpportunityId = new Set<Id>();

        for (cscfga__Product_Basket__c tmpPB : lstPB) {
            setOpportunityId.add(tmpPB.cscfga__Opportunity__c);
        }

        if (setOpportunityId.size() > 0) {

            List<OpportunityLineItem> lstOLI = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId in : setOpportunityId];

            if (lstOLI.size() > 0) {
                delete lstOLI;
            }
        }
    }
    
    /*
     * Deletes Opportunity Line Items for unsynced baskets
    */
    public static void DeleteHardOLIs(Set<string> setProductBasketId, List<cscfga__Product_Basket__c> lstNewPB) {

        Set<Id> setOpportunityId = new Set<Id>();
        
        for (cscfga__Product_Basket__c pb :lstNewPB) {
            if (setProductBasketId.contains(pb.Id)) {
                setOpportunityId.add(pb.cscfga__Opportunity__c);    
            }
        }

        if (setOpportunityId.size() > 0) {

            List<OpportunityLineItem> lstOLI = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId in : setOpportunityId];

            if (lstOLI.size() > 0) {
                delete lstOLI;
            }
        }
    }
    
    private class CS_OLI_Product2_Map {
        public String productDefinition {get; set;}
        public String lineItemAttributeName {get; set;}
        public String productAttributeName {get; set;}
        
        public CS_OLI_Product2_Map(String productDefinition, String lineItemAttributeName, String productAttributeName) {
            this.productDefinition = productDefinition;
            this.lineItemAttributeName = lineItemAttributeName;
            this.productAttributeName = productAttributeName;
        }
    }

    private class Product {
        public string ProductFamily {get; set;}
        public string LiniItemDescription {get; set;}
        public string Product2Id {get; set;}
        public Product2 Prod2 {get; set;}
    }

    private class PBEntry {
        public string PBEntryId {get; set;}
        public string PriceBookId {get; set;}
        public string Product2Id {get; set;}
        public double UnitPrice {get; set;}
        public string Name {get; set;}
        public PriceBookEntry PBE {get; set;}
    }
}