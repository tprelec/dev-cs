@isTest
private class LG_ProductUtilityTest {
    
    @testsetup
	private static void setupTestData()
	{
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;
		
		OrderType__c newOrderType = CS_DataTest.createOrderType();
		newOrderType.Name = 'Ziggo';
		insert newOrderType;
		
		Account account = LG_GeneralTest.CreateAccount('Account', '12345678', 'Ziggo', true);
		
		Opportunity opp = LG_GeneralTest.CreateOpportunity(account, true);
		
		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', account, null, opp, false);
		
		basket.LG_CreatedFrom__c = 'Tablet';
		insert basket;
		
		cscfga__Product_Category__c prodCategory = LG_GeneralTest.createProductCategory('Test Category', true);
		
		cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('Phone Numbers', false);	
		
		prodDef.cscfga__Product_Category__c = prodCategory.Id;
		insert prodDef;

		Opportunity opp2 = LG_GeneralTest.CreateOpportunity(account, true);
		
		cscfga__Product_Basket__c basket2 = LG_GeneralTest.createProductBasket('Basket2', account, null, opp2, false);
		
		basket2.LG_CreatedFrom__c = 'NotTablet';
		insert basket2;

		cscfga__Product_Configuration__c prodConf = LG_GeneralTest.createProductConfiguration('Phone Numbers 1', 3, basket, prodDef, true);
		cscfga__Product_Configuration__c prodConf2 = LG_GeneralTest.createProductConfiguration('Phone Numbers 2', 3, basket2, prodDef, true);
		
		cscfga__Attribute_Definition__c attDef = LG_GeneralTest.createAttributeDefinition('Phone Numbers', prodDef, 'Text Display', 'String',
			null, null, null, true);
			
		String publicJson = '{"phoneNumbers": [{"phoneNumber":"005065832","publiclyListed":"true","listingName":"Tomsi ei sBpoz kau "},{"phoneNumber":"005065833",'
								+ '"publiclyListed":"false","listingName":""},{"phoneNumber":"005065834","publiclyListed":"false","listingName":""},{"phoneNumber":"005065835",'
								+ '"publiclyListed":"true","listingName":"TBTo ao os op"},{"phoneNumber":"005065836","publiclyListed":"false","listingName":""},{"phoneNumber"'
								+ ':"005065837","publiclyListed":"false","listingName":""},{"phoneNumber":"005065838","publiclyListed":"false","listingName":""},{"phoneNumber"'
								+ ':"005065839","publiclyListed":"false","listingName":""},{"phoneNumber":"005065840","publiclyListed":"false","listingName":""},{"phoneNumber":'
								+ '"005065841","publiclyListed":"false","listingName":""}]}';
					
		LG_GeneralTest.createAttribute('PublicListingJson', attDef, false, null, prodConf, false, publicJson, true);
		LG_GeneralTest.createAttribute('Number of lines', attDef, false, null, prodConf, false, '10', true);
		LG_GeneralTest.createAttribute('Phonenumbers', attDef, false, null, prodConf, false, '03012345678', true);
		
		List<LG_PortingNumber__c> numbersToInsert = new List<LG_PortingNumber__c>();
		
		LG_PortingNumber__c portingNumber = new LG_PortingNumber__c();
		portingNumber.LG_PhoneNumber__c = '012349870';
		portingNumber.LG_InDirectory__c = 'true';
		portingNumber.LG_DirectoryListingName__c = 'Public HR';
		portingNumber.LG_Type__c = 'SB Telephony';
		portingNumber.LG_ProductConfiguration__c = prodConf.Id;
		numbersToInsert.add(portingNumber);
		
		LG_PortingNumber__c portingNumber2 = new LG_PortingNumber__c();
		portingNumber2.LG_PhoneNumber__c = '012349875';
		portingNumber2.LG_InDirectory__c = 'true';
		portingNumber2.LG_DirectoryListingName__c = 'Public Helpdesk';
		portingNumber2.LG_Type__c = 'SB Telephony';
		portingNumber2.LG_ProductConfiguration__c = prodConf.Id;
		numbersToInsert.add(portingNumber2);
		
		insert numbersToInsert;
		
		noTriggers.Flag__c = false;
		upsert noTriggers;
	}

	private static testMethod void test() {
	    Set<Id> baskedIDs = new Map<Id, cscfga__Product_Basket__c>([SELECT Id FROM cscfga__Product_Basket__c WHERE Name = 'Basket']).keySet();
	    
	    Set<String> basketStrings = (Set<String>)JSON.deserialize(JSON.serialize(baskedIDs), Set<String>.class);
	    
        LG_ProductUtility.CreateOLIs(basketStrings);
        
        LG_ProductUtility.DeleteHardOLIs(basketStrings);
        
        LG_ProductUtility.DeleteHardOLIs(basketStrings, new List<cscfga__Product_Basket__c>());
	}

}