@isTest
private class LG_RfsCheckUtilityTest {
	
	@testsetup
	private static void setupTestData()
	{
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;
		
		List<LG_RfsCheckVariables__c> rfsVariables = new List<LG_RfsCheckVariables__c>();
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'AnalogueTv', LG_Value__c = 'Catv'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DigitalTv', LG_Value__c = 'DMM'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownBasic', LG_Value__c = '130'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownFp200', LG_Value__c = '300'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownFp500', LG_Value__c = '500'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Fp200', LG_Value__c = 'UPC Fiber Power Internet'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Fp500', LG_Value__c = 'UPC_superfast'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Internet', LG_Value__c = 'UPC Internet'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Mobile', LG_Value__c = 'mobile'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'MobileInternet', LG_Value__c = 'mobile_internet'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Qos', LG_Value__c = 'QoS'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Telephony', LG_Value__c = 'telephony'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpBasic', LG_Value__c = '30'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpFp200', LG_Value__c = '40'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpFp500', LG_Value__c = '40'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'ZakelijkInternetGiga' , LG_Value__c = 'Ziggo_Giga'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'GigaDownFp1000' , LG_Value__c = '1000'));
		insert rfsVariables;
		
		noTriggers.Flag__c = false;
		upsert noTriggers;
	}
	
	private static testmethod void testGetCommonRfsJsonFormatAllAvailable()
	{
		String rfsCommon = LG_RfsCheckUtility.getCommonRfsJsonFormat(LG_RfsCheckUtility.buildRfsResponse(true, true, true, true, true, true));
		System.assertEquals(true, rfsCommon.contains('"down":"' + LG_RfsCheckVariables__c.getAll().get('DownFp500').LG_Value__c + '"'));
		System.assertEquals(true, rfsCommon.contains('"qos":"Voice"'));
		System.assertEquals(true, rfsCommon.contains('"capability":"TV"'));
		System.assertEquals(true, rfsCommon.contains('"technology":"Mobile"'));
		System.assertEquals(true, rfsCommon.contains('"addressId":"11532544"'));
		System.assertEquals(true, rfsCommon.contains('"country":"Netherlands"'));
	}
	
	private static testmethod void testGetCommonRfsJsonFormatFp500NotAvailable()
	{
		String rfsCommon = LG_RfsCheckUtility.getCommonRfsJsonFormat(LG_RfsCheckUtility.buildRfsResponse(true, true, false, true, true, true));

		System.assertEquals(false, rfsCommon.contains('"down":"' + LG_RfsCheckVariables__c.getAll().get('DownFp500').LG_Value__c + '"'));
		System.assertEquals(true, rfsCommon.contains('"down":"' + LG_RfsCheckVariables__c.getAll().get('DownFp200').LG_Value__c + '"'));
		System.assertEquals(true, rfsCommon.contains('"qos":"Voice"'));
		System.assertEquals(true, rfsCommon.contains('"capability":"TV"'));
		System.assertEquals(true, rfsCommon.contains('"technology":"Mobile"'));
		System.assertEquals(true, rfsCommon.contains('"addressId":"11532544"'));
		System.assertEquals(true, rfsCommon.contains('"country":"Netherlands"'));
	}
	
	private static testmethod void testGetCommonRfsJsonFormatFp200NotAvailable()
	{
		String rfsCommon = LG_RfsCheckUtility.getCommonRfsJsonFormat(LG_RfsCheckUtility.buildRfsResponse(true, false, false, true, true, true));

		System.assertEquals(false, rfsCommon.contains('"down":"' + LG_RfsCheckVariables__c.getAll().get('DownFp500').LG_Value__c + '"'));
		System.assertEquals(false, rfsCommon.contains('"down":"' + LG_RfsCheckVariables__c.getAll().get('DownFp200').LG_Value__c + '"'));
		System.assertEquals(true, rfsCommon.contains('"down":"' + LG_RfsCheckVariables__c.getAll().get('DownBasic').LG_Value__c + '"'));
		System.assertEquals(true, rfsCommon.contains('"qos":"Voice"'));
		System.assertEquals(true, rfsCommon.contains('"capability":"TV"'));
		System.assertEquals(true, rfsCommon.contains('"technology":"Mobile"'));
		System.assertEquals(true, rfsCommon.contains('"addressId":"11532544"'));
		System.assertEquals(true, rfsCommon.contains('"country":"Netherlands"'));	
	}
	
	private static testmethod void testGetProductFamilyLimitsForInternet()
	{
		String rfsCommon = LG_RfsCheckUtility.getCommonRfsJsonFormat(LG_RfsCheckUtility.buildRfsResponse(true, true, true, true, true, true));
		
		Map<String, String> limitsValues = LG_RfsCheckUtility.getProductFamilyLimits(rfsCommon, 'Internet');
		
		System.assertEquals(LG_RfsCheckVariables__c.getAll().get('DownFp500').LG_Value__c, limitsValues.get('down'), 'Max Download should be 500');
		System.assertEquals(LG_RfsCheckVariables__c.getAll().get('UpFp500').LG_Value__c, limitsValues.get('up'), 'Max Upload should be 40');
		System.assertEquals('Voice', limitsValues.get('qos'), 'QoS should be Voice');
	}
}