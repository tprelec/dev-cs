/*******************************************************************************************************************************************
* File Name     :  LG_SalesforceOneLeadActionsController
* Description   :  This is the controller that drives the SF1 actions on lead object

* @author       :   Shreyas
* Modification Log
===================================================================================================
* Ver.    Date          Author              Modification
---------------------------------------------------------------------------------------------------
* 1.0     9th-Aug-16    Shreyas             Created the class for release R1.5

**Last modified by Manu -- CRQ000000530333,CRQ000000584139 

********************************************************************************************************************************************/

public class LG_SalesforceOneLeadActionsController{

    public static Lead leadRecord {get; set;}
    public boolean showError {get; set;}
    public static string leadId {get; set;}
    public static string callResult;
    public Lead leadRecord_NonStatic {get; set;}
    public date lead_PreferredContactDate {get; set;}
    public string lead_PreferredContactTime {get; set;}
    public string lead_FollowUpDescription {get; set;}
    
    // Lead Statuses
    public static Final string leadStatus_Qualified = 'Qualified';
    public static Final string leadStatus_NotAvailable = 'Not Available';
    public static Final string leadStatus_FutureInterest = 'Future Interest';
    public static Final string leadStatus_NotReached = 'Not Reached';
    public static Final string leadStatus_Disqualified = 'Disqualified';
    public static Final string leadStatus_FollowUp = 'Follow Up';
    
    
    public static string status_Temp = '';
    public static string reason_Temp = '';
    
    public LG_SalesforceOneLeadActionsController (ApexPages.standardcontroller stdcontroller){
        
        showError = false;
        leadId = '';
        leadId = ApexPages.currentPage().getParameters().get('Id');
        system.debug('LeadId: ' + leadId);
        leadRecord = new Lead();
        leadRecord = [Select Id, Status, OwnerId, LG_ReasonDisqualified__c, LG_PreferredContactDate__c, LG_PreferredContactTime__c, LG_FollowUpDescription__c 
                      from Lead where Id =: leadId];           
        
        leadRecord_NonStatic = new Lead();
        leadRecord_NonStatic = [Select Id, Status, OwnerId, LG_ReasonDisqualified__c, LG_PreferredContactDate__c, LG_PreferredContactTime__c, LG_FollowUpDescription__c 
                                from Lead where Id =: leadId]; 
                      
        callResult = '';  
                   
        lead_PreferredContactDate = leadRecord.LG_PreferredContactDate__c;
        lead_PreferredContactTime = leadRecord.LG_PreferredContactTime__c;
        lead_FollowUpDescription = leadRecord.LG_FollowUpDescription__c;

        
     }
    
    /*
        Name: acceptLead
        Purpose: updates lead owner to the logged in user
        Argument: leadRecordId
        Return type: NA
    */
    @RemoteAction
    public static string acceptLead (string leadRecordId){
        
        string result = '';
        try{
            Lead leadRecordToUpdate = new Lead();
            leadRecordToUpdate = [Select Id, OwnerId from Lead where Id =: leadRecordId]; 
            leadRecordToUpdate.OwnerId = userInfo.getUserId();
            update leadRecordToUpdate;
            result = 'Success';
            return result;
        }
        catch(exception e){
            result = e.getMessage();
            return result;
        }
    }
    
    
    /*
        Name: updateLeadStatusToQualified
        Purpose: updates lead status to "Qualified"
        Argument: leadRecordId
        Return type: NA
    */
    @RemoteAction
    public static string updateLeadStatusToQualified(string leadRecordId){
        
        string result = '';
        try{
        
            Lead leadRecordToUpdate = new Lead();
            leadRecordToUpdate = [Select Id, OwnerId, Status, LG_ReasonDisqualified__c from Lead where Id =: leadRecordId]; 
        
            //leadRecordToUpdate.Status = leadStatus_Qualified;
            leadRecordToUpdate.LG_ReasonDisqualified__c = null;
            leadRecordToUpdate.OwnerId = userInfo.getUserId();
            update leadRecordToUpdate;
            
            callResult = 'Qualified';
            logACallAfterLeadAction(callResult, leadRecordId);
            result = 'Success';
            return result;
        }
        catch(exception e){
            result = e.getMessage();
            return result;
        }
        

    }
    
    /*
        Name: updateLeadStatusToDisqualified_OnLoadAction
        Purpose: updates lead status to "Disqualified"
        Argument: NA
        Return type: pageReference
    */
    public static pageReference updateLeadStatusToDisqualified_OnLoadAction(){
        
        leadRecord.Status = leadStatus_Disqualified;
        return null;
    }
    
    /*
        Name: updateLeadStatusToDisqualified
        Purpose: updates lead status to "Disqualified"
        Argument: leadRecordId
        Return type: NA
    */
    public pageReference updateLeadStatusToDisqualified(){
        return null;
    }
    

    
    
    @RemoteAction
    public static string updateLeadStatusToDisqualified_Remote(string leadRecordId, string status, string reason){
        
        string result = '';
        try{
            Lead leadRecordToUpdate = new Lead();
            leadRecordToUpdate = [Select Id, Status, LG_ReasonDisqualified__c from Lead where Id =: leadRecordId];
            leadRecordToUpdate.Status = status;
            leadRecordToUpdate.LG_ReasonDisqualified__c = reason;
  
            if(status == 'Disqualified' && String.isBlank(reason)){
                throw new LG_Exception('blank_reason');
            }
            update leadRecordToUpdate;
            
            callResult = 'Not interested';
            logACallAfterLeadAction(callResult, leadRecordId);
            
            result = 'Success';
            return result;
        }
        catch(exception e){
            result = e.getMessage();
            return result;
        }
    }

    
    
    /*
        Name: updateLeadStatusToFollowUp_OnLoadAction
        Purpose: updates lead status to "Follow Up"
        Argument: NA
        Return type: pageReference
    */
    public static pageReference updateLeadStatusToFollowUp_OnLoadAction(){
        leadRecord.Status = leadStatus_FollowUp;
        return null;
    }
    
    /*
        Name: updateLeadStatusToFollowUp
        Purpose: updates lead status to "Follow-Up"
        Argument: leadRecordId
        Return type: NA
    */
    public pageReference updateLeadStatusToFollowUp(){
        return null;
    }
    
    @RemoteAction
    public static string updateLeadStatusToFollowUp_Remote(string leadRecordId, string status, string followUpDate, string followUpTime, string description){
      
      string result = '';
      try{
          Lead leadRecordToUpdate = new Lead();
          leadRecordToUpdate = [Select Id, Status, LG_ReasonDisqualified__c, LG_PreferredContactDate__c, LG_PreferredContactTime__c, LG_FollowUpDescription__c 
                                  from Lead where Id =: leadRecordId];
          leadRecordToUpdate.Status = status;
          leadRecordToUpdate.LG_PreferredContactDate__c = Date.valueOf(followUpDate);
          leadRecordToUpdate.LG_PreferredContactTime__c = followUpTime;
          leadRecordToUpdate.LG_FollowUpDescription__c  = description;
          leadRecordToUpdate.LG_ReasonDisqualified__c = null;
		  //CRQ000000584139
          leadRecordToUpdate.OwnerId = userInfo.getUserId();
		  //CRQ000000584139
          update leadRecordToUpdate;
          
          callResult = 'Follow Up';
          logACallAfterLeadAction(callResult, leadRecordId);
            
          result = 'Success';
          return result;
      }
      catch(exception e){
            result = e.getMessage();
            return result;
        }
    }
    
    
    
    
    /*
        Name: updateLeadStatusToNotAvailabale
        Purpose: updates lead status to "Not available"
        Argument: leadRecordId
        Return type: NA
    */
    @RemoteAction
    public static string updateLeadStatusToNotAvailable(string leadRecordId){
        
        string result = '';
        try{
            Lead leadRecordToUpdate = new Lead();
            leadRecordToUpdate = [Select Id, Status, LG_ReasonDisqualified__c from Lead where Id =: leadRecordId]; 
            
            leadRecordToUpdate.Status = leadStatus_NotAvailable;
            leadRecordToUpdate.LG_ReasonDisqualified__c = null;
            update leadRecordToUpdate;
            
			//CRQ000000584139
            callResult = 'Not available';
			//CRQ000000584139
            logACallAfterLeadAction(callResult, leadRecordId);
            result = 'Success';
            return result;
        }
        catch(exception e){
            result = e.getMessage();
            return result;
        }
        
    }
    
    /*
        Name: updateLeadStatusToFutureInterest
        Purpose: updates lead status to "Future Interest"
        Argument: leadRecordId
        Return type: NA
    */
    @RemoteAction
    public static string updateLeadStatusToFutureInterest(string leadRecordId){
        
        string result = '';
        try{
            Lead leadRecordToUpdate = new Lead();
            leadRecordToUpdate = [Select Id, Status, LG_ReasonDisqualified__c from Lead where Id =: leadRecordId];
            
            leadRecordToUpdate.Status = leadStatus_FutureInterest;
            leadRecordToUpdate.LG_ReasonDisqualified__c = null;
            update leadRecordToUpdate;
            
            callResult = 'Future Interest';
            logACallAfterLeadAction(callResult,leadRecordId);
            result = 'Success';
            return result;
        }
        catch(exception e){
            result = e.getMessage();
            return result;
        }
        
    }
    
    /*
        Name: updateLeadStatusToNotReached
        Purpose: updates lead status to "Not Reached"
        Argument: leadRecordId
        Return type: NA
    */
    @RemoteAction
    public static string updateLeadStatusToNotReached(string leadRecordId){
    
        string result = '';
        try{
            Lead leadRecordToUpdate = new Lead();
            leadRecordToUpdate = [Select Id, Status, LG_ReasonDisqualified__c from Lead where Id =: leadRecordId];
            
            leadRecordToUpdate.Status = leadStatus_NotReached;
            leadRecordToUpdate.LG_ReasonDisqualified__c = null;
            update leadRecordToUpdate;
            
            callResult = 'Not Reached';
            logACallAfterLeadAction(callResult, leadRecordId);
            result = 'Success';
            return result;
        }
        catch(exception e){
            result = e.getMessage();
            return result;
        }
    }
        
    
    /*
        Name: logACallAfterLeadAction
        Purpose: logs a call after performing the lead actions
        Argument: None
        Return type: pagereference
    */
    public static void logACallAfterLeadAction(string callResultValue, string leadRecordId){
        
        Task t = new Task();
        t.Subject = 'D2D Visit';
        t.ActivityDate = System.today();
        t.Type = 'Result of D2D Visit';
        t.LG_Result__c = callResultValue;
        t.Status = 'Completed';
        t.WhoId = leadRecordId;
        t.OwnerId = userinfo.getUserId();

        insert t;
    }


    

}