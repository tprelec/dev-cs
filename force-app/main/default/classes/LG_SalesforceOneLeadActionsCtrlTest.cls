/*******************************************************************************************************************************************
* File Name     :  LG_SalesforceOneLeadActionsCtrlTest
* Description   :  Test class for LG_SalesforceOneLeadActionsController

* @author       :   Shreyas
* Modification Log
===================================================================================================
* Ver.    Date          Author              Modification
---------------------------------------------------------------------------------------------------
* 1.0     9th-Aug-16    Shreyas             Created the class for release R1.5
********************************************************************************************************************************************/

@isTest
private class LG_SalesforceOneLeadActionsCtrlTest {

     /*
        Name: TestMethod1_SalesforceOneActions
        Purpose: Test method for testing the logic for salesforce1 lead actions
        Argument: none
        Return type: none
     */
    static testmethod void TestMethod1_SalesforceOneActions() {


        Lead l = new Lead();
        l.Company = 'Test Company';
        l.LastName = 'Test Last Name';
        l.Email = 'test@test.com';
        l.MobilePhone = '12345678';
        l.Status = 'Not Contacted';
        l.LG_VisitCountry__c = 'Netherlands';
        insert l;

        ApexPages.StandardController stdController = new ApexPages.StandardController(l);
        LG_SalesforceOneLeadActionsController obj1;

        //coverage for the vf page: LG_UpdateLeadStatustoQualified
        PageReference pageRef_Qualified = Page.LG_UpdateLeadStatustoQualified;
        Test.setCurrentPage(pageRef_Qualified);
        ApexPages.currentPage().getParameters().put('Id', l.Id);
        string leadId = l.Id;
        obj1 = new LG_SalesforceOneLeadActionsController(stdController);
        LG_SalesforceOneLeadActionsController.updateLeadStatusToQualified(leadId);

        //coverage for the vf page: LG_UpdateLeadStatustoDisqualified
        PageReference pageRef_Disqualified = Page.LG_UpdateLeadStatustoDisqualified;
        Test.setCurrentPage(pageRef_Disqualified);
        ApexPages.currentPage().getParameters().put('Id', l.Id);
        obj1 = new LG_SalesforceOneLeadActionsController(stdController);
        LG_SalesforceOneLeadActionsController.updateLeadStatusToDisqualified_OnLoadAction();
        LG_SalesforceOneLeadActionsController.updateLeadStatusToDisqualified_Remote(l.Id, 'Disqualified', 'Other');
        obj1.updateLeadStatusToDisqualified();


        //coverage for the vf page: LG_AcceptLead
        PageReference pageRef_Accept = Page.LG_AcceptLead;
        Test.setCurrentPage(pageRef_Accept);
        ApexPages.currentPage().getParameters().put('Id', l.Id);
        obj1 = new LG_SalesforceOneLeadActionsController(stdController);
        //obj1.acceptLead(l.Id);
        LG_SalesforceOneLeadActionsController.acceptLead(leadId);

        //coverage for the vf page: LG_UpdateLeadStatustoContactedFollowUp
        PageReference pageRef_FollowUp = Page.LG_UpdateLeadStatustoFollowUp;
        Test.setCurrentPage(pageRef_FollowUp);
        ApexPages.currentPage().getParameters().put('Id', l.Id);
        obj1 = new LG_SalesforceOneLeadActionsController(stdController);
        LG_SalesforceOneLeadActionsController.updateLeadStatusToFollowUp_OnLoadAction();
        LG_SalesforceOneLeadActionsController.updateLeadStatusToFollowUp_Remote(l.Id, 'Follow Up', '30-10-2016', 'test', 'test');
        obj1.updateLeadStatusToFollowUp();

        Test.startTest();
        //coverage for the vf page: LG_UpdateLeadStatustoNotAvailable
        PageReference pageRef_NotAvailable = Page.LG_UpdateLeadStatustoNotAvailable;
        Test.setCurrentPage(pageRef_NotAvailable);
        ApexPages.currentPage().getParameters().put('Id', l.Id);
        obj1 = new LG_SalesforceOneLeadActionsController(stdController);
        //obj1.updateLeadStatusToNotAvailabale(l.Id);
        LG_SalesforceOneLeadActionsController.updateLeadStatusToNotAvailable(leadId);

        //coverage for the vf page: LG_UpdateLeadStatustoFutureInterest
        PageReference pageRef_FutureInterest = Page.LG_UpdateLeadStatustoFutureInterest;
        Test.setCurrentPage(pageRef_FutureInterest);
        ApexPages.currentPage().getParameters().put('Id', l.Id);
        obj1 = new LG_SalesforceOneLeadActionsController(stdController);
        //obj1.updateLeadStatusToFutureInterest(l.Id);
        LG_SalesforceOneLeadActionsController.updateLeadStatusToFutureInterest(leadId);

        //coverage for the vf page: LG_UpdateLeadStatustoNotReached
        PageReference pageRef_NotReached = Page.LG_UpdateLeadStatustoNotReached;
        Test.setCurrentPage(pageRef_NotReached);
        ApexPages.currentPage().getParameters().put('Id', l.Id);
        obj1 = new LG_SalesforceOneLeadActionsController(stdController);
        //obj1.updateLeadStatusToNotReached(l.Id);
        LG_SalesforceOneLeadActionsController.updateLeadStatusToNotReached(leadId);
        Test.stopTest();
    }

}