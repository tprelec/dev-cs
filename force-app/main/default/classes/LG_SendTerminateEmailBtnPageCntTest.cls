@isTest
private class LG_SendTerminateEmailBtnPageCntTest {
	
	private static testmethod void testSendEmailMethod()
	{
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;
		
		Account account = LG_GeneralTest.CreateAccount('Account', '12345678', 'Ziggo', true);
				
		Opportunity opp = LG_GeneralTest.CreateOpportunity(account, true);
                        
        Contact con=LG_GeneralTest.CreateContact(account,'Test Fname', 'Test Lname', 'Mr.', '62626262', '62626262', 'a@b.com', 'Administrative Contact', system.today().addDays(-700),'', '', '','',true);
        
        OpportunityContactRole OCR=LG_GeneralTest.CreateOpportunityContactRole(account, opp,con, '', true, true);
		        // 
		EmailTemplate terminateTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'LG_ZiggoZakelijkTerminationTemplate']; 
		
		noTriggers.Flag__c = false;
		upsert noTriggers;
		
		Test.startTest();
		
			ApexPages.StandardController oppController = new ApexPages.StandardController(opp);
			LG_SendTerminateEmailBtnPageController controller = new LG_SendTerminateEmailBtnPageController(oppController);	
			controller.sendEmailRedirect();
		Test.stopTest();
		
		System.assertEquals(true, controller.shouldRedirect, 'Should redirect should be set to true');
		System.assertEquals('/_ui/core/email/author/EmailAuthor?retURL=' + opp.Id + '&template_id=' + terminateTemplate.Id + '&p3_lkid=' + opp.Id + '&p2_lkid=null', controller.redirectUrl, 'Redirect url should be set');
	}
}