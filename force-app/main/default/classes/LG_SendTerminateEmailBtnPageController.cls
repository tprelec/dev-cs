public with sharing class LG_SendTerminateEmailBtnPageController {

	private final Opportunity opportunity;
	private final Id terminateTemplateId;
	public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}

    // The extension constructor initializes the private member
    // variable Opportunity by using the getRecord method from the standard
    // controller.
    public LG_SendTerminateEmailBtnPageController(ApexPages.StandardController stdController)
	{
        this.opportunity = (Opportunity) stdController.getRecord();
		EmailTemplate terminateTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'LG_ZiggoZakelijkTerminationTemplate'];
		terminateTemplateId = terminateTemplate.Id;
		shouldRedirect = false;
    }

	//back to return Id
    public PageReference sendEmailRedirect() 
	{
        Id conId=null;
        Opportunity Opp=[Select id, (SELECT OpportunityId, IsPrimary, ContactId FROM OpportunityContactRoles WHERE Role = 'Administrative Contact' AND IsPrimary=true  limit 1) from Opportunity where id=:opportunity.Id];        
        system.debug('Opp is:'+Opp+'OppConRoles:'+opp.OpportunityContactRoles);
        if(Opp!=null && Opp.OpportunityContactRoles.Size()>0){
            conId=Opp.OpportunityContactRoles[0].contactId;            
        }
        system.debug('conId is:'+conId);
        shouldRedirect = true;
        redirectUrl = '/_ui/core/email/author/EmailAuthor?retURL=' + opportunity.Id + '&template_id=' + terminateTemplateId + '&p3_lkid=' + opportunity.Id + '&p2_lkid='+ conId;
        return null;
    }
}