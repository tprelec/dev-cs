/**
* Used as a controller for LG_SeparateInvoices VF page.
*
* @author Tomislav Blazek
* @ticket SFDT-58
* @since  13/01/2016
*/
public with sharing class LG_SeparateInvoicesController {

    private static final String REDIRECT_URL = '/lightning/o/csconta__Billing_Account__c/new?defaultFieldValues=csconta__Account__c=';
    private static final String BILLING_ACCOUNT_PREFIX = csconta__Billing_Account__c.sObjectType.getDescribe().getKeyPrefix();

    private Id basketId;
    private Account account;
    public List<LIWrap> liwrappers {get;set;}

    public String getNewBillingAccountUrl() {
        // Business wants this to be working on lightning
        return REDIRECT_URL + account.Id;
    }

    public PageReference createNewBillAccount() {

        PageReference createNewBillPage = new PageReference('/' + BILLING_ACCOUNT_PREFIX + '/e');

        String accFieledReference = LG_Util.getLookupFieldReferenceId(createNewBillPage,
                                        Account.getSObjectType().getDescribe().getKeyPrefix());
        String pageURL = '/apex/LG_SeparateInvoices?basketId=';
        createNewBillPage.getParameters().put('saveURL', pageURL+ basketId);
        createNewBillPage.getParameters().put(accFieledReference, account.Name);
        createNewBillPage.getParameters().put(accFieledReference + '_lkid', account.Id);
        createNewBillPage.getParameters().put('retURL', pageURL + basketId);

        return createNewBillPage;
    }

    public LG_SeparateInvoicesController() {
        basketId = ApexPages.currentPage().getParameters().get('basketId');

        if (basketId != null)
        {
            cscfga__Product_Basket__c basket = [SELECT Id, csbb__Account__r.Name, csbb__Account__r.Id
                                                FROM cscfga__Product_Basket__c
                                                WHERE Id = :basketId];

            account = basket.csbb__Account__r;


            getLI();
        }
    }

    //Populate the select list with the Billing Account associated to the Customer account of the opportunity
    public List<SelectOption> getItems() {

        List<SelectOption> options = new List<SelectOption>();

        List<csconta__Billing_Account__c> billingAccounts = [SELECT Id, LG_BillingAccountIdentifier__c
                                                                FROM csconta__Billing_Account__c
                                                                WHERE csconta__Account__c = :account.Id];

        options.add(new SelectOption('', 'Select'));

        for(csconta__Billing_Account__c billingAcc : billingAccounts)
        {
            options.add(new SelectOption(billingAcc.Id, billingAcc.LG_BillingAccountIdentifier__c));
        }

        return options;
    }

    /**
     * Sets the Billing Account (selected in the Dropdown) on the individual line items.
     *
     * @author Tomislav Blazek
     * @ticket SFDT-58
     * @since  2016-01-12
     */
    public void save()
    {
        List<cscfga__Attribute_Field__c> fieldsToUpsert = new List<cscfga__Attribute_Field__c>();
        //Added as part of CATGOV-302
        cscfga__Product_Basket__c basket = [SELECT Id, LG_Selected_Billing_Account__c , csbb__Account__r.Name, csbb__Account__r.Id
                                                FROM cscfga__Product_Basket__c
                                                WHERE Id = :basketId];
        for(LIWrap wp : liwrappers)
        {
            Id fieldId = wp.billingAccountField != null ? wp.billingAccountField.Id : null;
            String selectedBillAccId = String.isBlank(wp.selValBill) ? null : wp.selValBill;

            fieldsToUpsert.add(new cscfga__Attribute_Field__c(Id = fieldId,
                                                            cscfga__Attribute__c = wp.att.Id,
                                                            Name = 'BillingAccount',
                                                           cscfga__Value__c = selectedBillAccId));
            //Added as part of CATGOV-302
            basket.LG_Selected_Billing_Account__c = selectedBillAccId;
        }
        system.debug('value :****** ' + fieldsToUpsert);
        upsert fieldsToUpsert;

        //Added as part of CATGOV-302
        update basket;
    }

    /**
     * Sets the Billing Account (selected in the Dropdown) on the individual line items and redirects
     *
     * @author Tomislav Blazek
     * @ticket SFDT-58
     * @since  2016-01-12
     */
    public PageReference saveWithRedirect()
    {
        save();

        return redirectToBasket();
    }

    //retreive the line items (need to have a wrapper class so each line get their own billing account)
    void getLI() {

        liwrappers = new List<LIWrap>();

        for(cscfga__Attribute__c attribute : [SELECT Id, Name, cscfga__Line_Item_Description__c, cscfga__Price__c, cscfga__Recurring__c,
                                                cscfga__Product_Configuration__r.LG_Address__r.cscrm__Address_Details__c,
                                                (SELECT Id, Name, cscfga__Value__c FROM cscfga__Attribute_Fields__r WHERE Name = 'BillingAccount')
                                                FROM cscfga__Attribute__c
                                                WHERE cscfga__Is_Line_Item__c = true
                                                AND cscfga__Product_Configuration__c IN
                                                (SELECT Id FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basketId)
                                                ORDER BY cscfga__Product_Configuration__r.LG_Address__r.cscrm__Address_Details__c,
                                                            cscfga__Line_Item_Sequence__c])
        {
            liwrappers.add(new LIWrap(attribute));
        }
    }

    public class LIWrap {

        public cscfga__Attribute__c att {get;set;}
        public String selValBill {get;set;}
        private cscfga__Attribute_Field__c billingAccountField {get; set;}
        public Boolean selected {get;set;}

        public LIWrap(cscfga__Attribute__c att) {
            this.att = att;
            this.selected = false;

            for(cscfga__Attribute_Field__c field : att.cscfga__Attribute_Fields__r)
            {
                this.selValBill = field.cscfga__Value__c;
                this.billingAccountField = field;
            }
        }
    }

    //back to basket
    public PageReference redirectToBasket() {
        PageReference newocp = new PageReference('/' + basketId);

        return newocp;
    }
}