@isTest
private class LG_SeparateInvoicesControllerTest {

    private static String vfBaseUrl = 'vforce.url';
    private static String sfdcBaseUrl = 'sfdc.url';

    @testsetup
    private static void setupTestData()
    {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;

        LG_EnvironmentVariables__c envVariables = new LG_EnvironmentVariables__c();
        envVariables.LG_SalesforceBaseURL__c = sfdcBaseUrl;
        envVariables.LG_VisualForceBaseURL__c = vfBaseUrl;
        envVariables.LG_CloudSenceAnywhereIconID__c = 'csaID';
        envVariables.LG_ServiceAvailabilityIconID__c = 'saIconId';
        insert envVariables;

        Account account = LG_GeneralTest.CreateAccount('AccountSFDT-58', '12345678', 'Ziggo', true);

        LG_GeneralTest.createBillingAccount('BillingAccount', account.Id, true, true);

        Opportunity opp = LG_GeneralTest.CreateOpportunity(account, true);

        cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('BAsket58', account, null, opp, false);
        basket.csbb__Account__c = account.Id;
        insert basket;

        cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('ProdDef58', true);

        cscfga__Product_Configuration__c prodConf = LG_GeneralTest.createProductConfiguration('ProdConf58', 3, basket, prodDef, true);

        cscfga__Attribute_Definition__c attDef = LG_GeneralTest.createAttributeDefinition('AttDef58', prodDef, 'Lookup', 'String',
            null, null, null, true);

        Double price = 5.15;

        LG_GeneralTest.createAttribute('Att58-1', attDef, true, price, prodConf, true, '58', true);
        LG_GeneralTest.createAttribute('Att58-2', attDef, true, price, prodConf, true, '58', true);

        noTriggers.Flag__c = false;
        upsert noTriggers;
    }

    private static testmethod void testRedirectToBasket()
    {
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Name = 'BAsket58'];
        PageReference pageRef = Page.LG_SeparateInvoices;
        pageRef.getParameters().put('basketId',basket.Id);
        Test.setCurrentPageReference(pageRef);

        Test.startTest();

            LG_SeparateInvoicesController controller = new LG_SeparateInvoicesController();
            PageReference redirectPagerefence = controller.redirectToBasket();

        Test.stopTest();

        System.assertEquals('/' + basket.Id, redirectPagerefence.getUrl());
    }

    private static testmethod void testCreateNewBillAccount()
    {
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Name = 'BAsket58'];
        Account account = [SELECT Id, Name FROM Account WHERE Name = 'AccountSFDT-58'];
        PageReference pageRef = Page.LG_SeparateInvoices;
        pageRef.getParameters().put('basketId',basket.Id);
        Test.setCurrentPageReference(pageRef);

        Test.startTest();

            LG_SeparateInvoicesController controller = new LG_SeparateInvoicesController();
            pageRef = controller.createNewBillAccount();
            String redirectUrl = controller.getNewBillingAccountUrl();

        Test.stopTest();

        System.assertEquals('/'+csconta__Billing_Account__c.sObjectType.getDescribe().getKeyPrefix() + '/e?'
                                + 'retURL=%2Fapex%2FLG_SeparateInvoices%3FbasketId%3D' + basket.Id
                                + '&saveURL=%2Fapex%2FLG_SeparateInvoices%3FbasketId%3D' + basket.Id
                                +'&testId='+ account.Name +'&testId_lkid='+account.Id, pageRef.getUrl(),
                            'Url should be ' + csconta__Billing_Account__c.sObjectType.getDescribe().getKeyPrefix() + '/e?'
                                                + '&saveURL=/apex/LG_SeparateInvoices?basketId=' + basket.Id
                                                    +'&testId='+account.Name + '&testId_lkid='+account.Id
                                                    + '&retURL=/apex/LG_SeparateInvoices?basketId=' + basket.Id);
        System.assertNotEquals('', redirectUrl);
    }

    private static testmethod void testSave()
    {
        Account account = [SELECT Id FROM Account WHERE Name = 'AccountSFDT-58'];

        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Name = 'BAsket58'];
        Opportunity opp = [SELECT Id FROM Opportunity WHERE AccountID = :account.Id];

        csconta__Billing_Account__c billingAccount = [SELECT Id FROM csconta__Billing_Account__c WHERE csconta__Financial_Account_Number__c = 'BillingAccount'];

        PageReference pageRef = Page.LG_SeparateInvoices;
        pageRef.getParameters().put('oppId',opp.Id);
        pageRef.getParameters().put('basketId',basket.Id);
        Test.setCurrentPageReference(pageRef);

        Test.startTest();

            LG_SeparateInvoicesController controller = new LG_SeparateInvoicesController();
            List<SelectOption> options = controller.getItems();
            System.debug(options);
            System.debug(controller.liwrappers);
            for (LG_SeparateInvoicesController.LIWrap liwrap : controller.liwrappers)
            {
                liwrap.selValBill = billingAccount.Id;
            }
            controller.saveWithRedirect();

        Test.stopTest();

        System.assertEquals(2, options.size(), 'List should contain two options - Select and BillingAccount');

        for(cscfga__Attribute__c attribute : [SELECT Id, Name,
                                                (SELECT Id, Name, cscfga__Value__c FROM cscfga__Attribute_Fields__r WHERE Name = 'BillingAccount')
                                                FROM cscfga__Attribute__c
                                                WHERE cscfga__Is_Line_Item__c = true
                                                AND cscfga__Product_Configuration__c IN
                                                (SELECT Id FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basket.Id)])
        {
            for (cscfga__Attribute_Field__c attField : attribute.cscfga__Attribute_Fields__r)
            {
                if (attField.Name.equals('BillingAccount'))
                {
                    System.assertEquals(billingAccount.Id, attField.cscfga__Value__c, 'BillingAccount Id:'
                                        + billingAccount.Id + ' should be set on Line Items');
                }
            }
        }
    }
}