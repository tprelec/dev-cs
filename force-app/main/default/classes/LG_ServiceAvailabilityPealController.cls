/*
This controller executes HTTP callouts to PEAL
in order to get information about the availability of different services for a given address.
If no information are available for the given address, it presents the user suggestions
for house number extensions. If there is an information available for the desired address, 
another callout is made to fetch the availability information.
1) Address validate call to fetch addressId / or list of extensions
2) Address details call (Rfs check)
*/
public without sharing class LG_ServiceAvailabilityPealController {
    
    // Service availability information, contains service name and boolean flag
    public class ServiceAvailability {
			
		public final String serviceName { get; private set; }
			
		public final Boolean available { get; private set; }
			
		public ServiceAvailability(String serviceName, Boolean available) {
			this.serviceName = serviceName;
			this.available = available;
		}
	
	}
    
    // Encapsulates a suggestion for house number extension returned by REST service
    public class HouseNumberExtensionSuggestion {
        
        public final String suggestion { get; private set; }
        
        // ECMAScript escaped string to be used in JavaScript
        public String getEscapedSuggestion() {
            return suggestion.escapeEcmaScript();
        }
        
        public HouseNumberExtensionSuggestion(String suggestion) {
            this.suggestion = suggestion;
        }
        
    }
	
	// Postal code set by attribute
	public String postalCodeAttribute { get; set; }
	
	// House number set by attribute
	public String houseNumberAttribute { get; set; }
	
	// Extension code set by attribute
	public String extensionAttribute { get; set; }
	
	// Postal code set by input field
	public String postalCode {
		get {
			if (postalCode == null) {
				postalCode = ((postalCodeAttribute != null) ? postalCodeAttribute : '');
			}
			return postalCode;
		}
		set;
	}
	
	// House number set by input field
	public String houseNumber {
		get {
			if (houseNumber == null) {
				houseNumber = ((houseNumberAttribute != null) ? houseNumberAttribute : '');
			}
			return houseNumber;
		}
		set;
	}
	
	// Extension number set by input field
	public String extension {
		get {
			if (extension == null) {
				extension = ((extensionAttribute != null) ? extensionAttribute : '');
			}
			return extension;
		}
		set;
	}
    
    // Suggestions for house number extension returned by REST service
    public List<HouseNumberExtensionSuggestion> extensionSuggestions {
        get {
            if (extensionSuggestions == null) {
                extensionSuggestions = new List<HouseNumberExtensionSuggestion>();
            }
            return extensionSuggestions;
        }
        private set;
    }
    
    // Count of available suggestions for house number extension
    public Integer extensionSuggestionCount {
        get {
            return extensionSuggestions.size();
        }
    }
	
	public String errorMessage { get; private set; }
	
	public List<ServiceAvailability> serviceAvailabilityInformation {
		get {
			if (serviceAvailabilityInformation == null) {
				serviceAvailabilityInformation = new List<ServiceAvailability>();
			}
			return serviceAvailabilityInformation;
		}
		private set;
	}
	
	// Invoke external web service and update availability information
    public void updateServiceAvailability() {
        
        serviceAvailabilityInformation = null;
        extensionSuggestions = null;
		errorMessage = null;
		
		String normalizedPostalCode = postalCode.replaceAll('(\\s+)', '');
        String normalizedHouseNumber = houseNumber.trim();
        
        // If test is running don't do the callout
        if (Test.isRunningTest()) {
            
            HttpRequest request = new HttpRequest();
            // HttpResponse response = new HttpResponse();
            // response.setBody(LG_AddressResponse.buildAddressResponse(false));
            processAddressesResponse(request);
            
            errorMessage = '';
            return;
        }
        
        LG_EndpointResolver resolver = LG_EndpointResolver.getInstance();
        
		if ((normalizedPostalCode.length() > 0) && (normalizedHouseNumber.length() > 0)) {
		    
			HttpRequest request = new HttpRequest();
            request.setMethod('GET');
            request.setTimeout(5000);

			request.setEndpoint(resolver.getAddressCheckEndpoint(normalizedHouseNumber, null, normalizedPostalCode));
			processAddressesResponse(request);
			
		} else {
		    
			errorMessage = Label.LG_FillOutRequired;
		}
	}
	
	
	// Continuation request label
	@TestVisible
	private String requestLabel;
    
    private void processAddressesResponse(HttpRequest request)
    {
        HttpResponse response = new HttpResponse();
        Integer statusCode = null;
        
        Http h = new Http();
        
        if (Test.isRunningTest() || LG_Util.getSandboxInstanceName().equals('dev'))
        {
            statusCode = 200;
            //mimick the response - if houseFlatExt included, mimick response of only one address, else return two addresses
            // response.setBody(LG_AddressResponse.buildAddressResponse(request.getEndpoint().contains('houseFlatExt')));
            
            response.setBody(LG_AddressResponse.buildAddressResponse(false));
        }
        else
        {
            try
            {
                response = h.send(request);
                statusCode = response.getStatusCode();
            }
            catch (CalloutException ce)
            {
                statusCode = 0;
            }
        }
        
        System.debug('Status Code: ' + statusCode);
        if (statusCode == 200) 
        {
            LG_AddressResponse addressResponse = new LG_AddressResponse();
            addressResponse.addView = new List<LG_AddressResponse.addView>();
            try{
                addressResponse.addView = (List<LG_AddressResponse.addView>)JSON.deserialize(response.getBody(), List<LG_AddressResponse.addView>.class);
                
                //more than 1 address returned - update extension suggestions()
                if (addressResponse.addView != null && addressResponse.addView.size() > 1)
                {
                    errorMessage = Label.LG_NoServiceAvailabilityInformation;
                    extensionSuggestions = updateExtensionSuggestions(addressResponse.addView);
                }
                //exact address details returned, call the RFS check
                else if (addressResponse.addView != null && addressResponse.addView.size() == 1)
                {
                    if (!Test.isRunningTest()) {
                        
                        serviceAvailabilityInformation = rfsCheck(addressResponse.addView[0].addressId);
                    }
                }
                //something failed
                else
                {
                    errorMessage = Label.LG_UnknownApexError;
                }
                
            } catch(System.JSONException ex) {
                errorMessage = Label.LG_UnknownApexError;
            }
        } else if (statusCode == 404) {
            errorMessage = Label.LG_NoServiceAvailabilityInformation;
        } else if (statusCode == 2000) {
            errorMessage = Label.LG_CalloutTimedOut;
        } else {
            errorMessage = Label.LG_UnknownApexError;
        }
        requestLabel = null;
    }
    
    @TestVisible
    private List<ServiceAvailability> rfsCheck(String addressId)
    {
        HttpResponse response = new HttpResponse();
        Integer statusCode = null;
        
        if (Test.isRunningTest() || LG_Util.getSandboxInstanceName().equals('dev'))
        {
            //some mocking based on house numbers available in dev
            Boolean dtv = houseNumber == '20' && extension == '001';
            Boolean fp500 = houseNumber == '22';
            Boolean fp200 = extension == '1';
            response.setBody(LG_RfsCheckUtility.buildRfsResponse(true, fp200, fp500, true, dtv, true));
            statusCode = 200;
        }
        else
        {
            Http h = new Http();
            
            LG_EndpointResolver resolver = LG_EndpointResolver.getInstance();
            
            HttpRequest req = new HttpRequest();
            //req.setEndpoint('callout:LG_OracleAccessGateway/peal/api/b2b/addresses/' + addressId + '/details?cty=NL&chl=B2B_CATALYST_NL');
            req.setEndpoint(resolver.getAvailabilityCheckEndpoint(addressId));
            req.setMethod('GET');
            req.setTimeout(5000);

            try
            {
                response = h.send(req);
                statusCode = response.getStatusCode();
            }
            catch (CalloutException ce)
            {
                statusCode = 0;
            }
        }
        
        System.debug('Status Code: ' + statusCode);
        if (statusCode == 200) 
        {
            try {
                return parseServiceAvailabilityResponse(response.getBody());
            } catch(Exception ex) {
                errorMessage = Label.LG_UnknownApexError;
            }
        } else if (statusCode == 404) {
            errorMessage = Label.LG_NoServiceAvailabilityInformation;
        } else if (statusCode == 2000) {
            errorMessage = Label.LG_CalloutTimedOut;
        } else {
            errorMessage = Label.LG_UnknownApexError;
        }
        
        return null;
    }
	
	// Parse JSON response containing service availability data
	private List<ServiceAvailability> parseServiceAvailabilityResponse(String responseBody)
    {
		List<ServiceAvailability> availabilityInformation = new List<ServiceAvailability>();
        String rfsCommonJson = LG_RfsCheckUtility.getCommonRfsJsonFormat(responseBody);
        
        Boolean internetAvailable = false;
        Boolean fp200Available = false;
        Boolean fp500Available = false;
        Boolean sopAvailable = false;
        Boolean telephonyAvailable = false;
        
        System.debug('rfsCommonJson: ' + rfsCommonJson);
        LG_RfsCheckUtility.RfsCommon rfsCommon = (LG_RfsCheckUtility.RfsCommon) JSON.deserialize(rfsCommonJson, LG_RfsCheckUtility.RfsCommon.class);
        for (LG_RfsCheckUtility.common_Availability avail : rfsCommon.availability)
        {
            if (avail.capability.equals('Data') && avail.technology.equals('HFC'))
            {
                internetAvailable = true;
                
                for (LG_RfsCheckUtility.Limits lim : avail.limits)
                {
                    if (LG_RfsCheckVariables__c.getAll().get('DownFp500').LG_Value__c.equals(lim.down))
                    {
                        fp500Available = true;
                        fp200Available = true;
                    }
                    else if (LG_RfsCheckVariables__c.getAll().get('DownFp200').LG_Value__c.equals(lim.down))
                    {
                        fp500Available = false;
                        fp200Available = true;
                    }
                }
            }
            else if (avail.capability.equals('TV') && avail.technology.equals('Analogue'))
            {
                sopAvailable = true;
            }
            else if (avail.capability.equals('Voice') && avail.technology.equals('Digital'))
            {
                telephonyAvailable = true;
            }
        }
        
        availabilityInformation.add(new ServiceAvailability('internet', internetAvailable));
        availabilityInformation.add(new ServiceAvailability('fp200', fp200Available));
        availabilityInformation.add(new ServiceAvailability('fp500', fp500Available));
        availabilityInformation.add(new ServiceAvailability('SOP', sopAvailable));
        availabilityInformation.add(new ServiceAvailability('telephony', telephonyAvailable));
            
		return availabilityInformation;
	}
    
    //Fetch the suggestions for house number extension from the address list
    private List<HouseNumberExtensionSuggestion> updateExtensionSuggestions(List<LG_AddressResponse.addView> addresses) 
    {
        List<HouseNumberExtensionSuggestion> suggestions = new List<HouseNumberExtensionSuggestion>();
        
        for(LG_AddressResponse.addView address : addresses)
        {
            suggestions.add(new HouseNumberExtensionSuggestion(address.streetNrFirstSuffix));
        }
        return suggestions;
    }

}