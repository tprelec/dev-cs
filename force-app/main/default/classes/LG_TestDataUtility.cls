@isTest
public class LG_TestDataUtility{
    public static List<LG_PostalCode__c> createPostalCodes(){
     List<LG_PostalCode__c> postalCodeList=new List<LG_PostalCode__c>
        {
                new LG_PostalCode__c(Name='9999XL9',LG_HouseNumber__c=9,LG_City__c='STITSWERD',LG_PostalCode__c='9999 XL',LG_StreetName__c='Akkemaweg',LG_Country__c='Netherlands'),
                new LG_PostalCode__c(Name='9999XK9',LG_HouseNumber__c=9,LG_City__c='STITSWERD',LG_PostalCode__c='9999 XK',LG_StreetName__c='Stiel',LG_Country__c='Netherlands'),
                new LG_PostalCode__c(Name='9997PR6',LG_HouseNumber__c=6,LG_City__c='ZANDEWEER',LG_PostalCode__c='9997 PR',LG_StreetName__c='Domies Laand',LG_Country__c='Netherlands'),
                new LG_PostalCode__c(Name='9998XE6',LG_HouseNumber__c=6,LG_City__c='ROTTUM GN',LG_PostalCode__c='9998 XE',LG_StreetName__c='Knolweg',LG_Country__c='Netherlands')
        };
     return postalCodeList;
    }
    
    public static Account CreateAccount(string AccountName,string ChamberofCommerceNumber, string FootPrint,string VisitHouseNumber,string VisitPostalCode,String PostalHouseNumber,String PostalCodeNumber)
    {
        Account tmpAccount = new Account(Name = AccountName);
        tmpAccount.KVK_number__c=ChamberofCommerceNumber;
        tmpAccount.LG_Footprint__c=FootPrint;
        tmpAccount.LG_VisitHouseNumber__c=VisitHouseNumber;
        tmpAccount.LG_VisitPostalCode__c=VisitPostalCode;
        tmpAccount.LG_PostalHouseNumber__c=PostalHouseNumber;
        tmpAccount.LG_PostalPostalCode__c=PostalCodeNumber;
        
        return tmpAccount;
    }

    public static csconta__Billing_Account__c createBillingAccount(String billingAccountName, Id accountId,string HouseNumber,string PostalCode)
    {
        csconta__Billing_Account__c billAcc = new csconta__Billing_Account__c(LG_BillingAccountName__c= billingAccountName,
                                                         csconta__Account__c = accountId, csconta__Billing_Channel__c = 'Paper bill',
                                LG_PaymentType__c = 'Bank Transfer',LG_HouseNumber__c=HouseNumber, csconta__Postcode__c=PostalCode);
        return billAcc;
    }
    
    public static cscrm__Address__c createAddress(string addrName,string houseNumber,string zip,Account acc) {
        
        cscrm__Address__c premise = new cscrm__Address__c();
        
        premise.Name = addrName;
        premise.LG_HouseNumber__c = houseNumber;
        premise.cscrm__Zip_Postal_Code__c = zip;
        premise.cscrm__Account__c = acc.Id;
      
        return premise;
    } 
    
    public static Lead CreateLead(string Company, string FirstName, string LastName, 
         string LeadStatus,string VisitHouseNumber,string VisitPostalCode)
    { 
        Lead tmpLead=new Lead();
        tmpLead.Company=Company;
        tmpLead.FirstName=FirstName;
        tmpLead.LastName=LastName;
        tmpLead.Status=LeadStatus;
        /*tmpLead.LG_VisitHouseNumber__c=VisitHouseNumber;*/
        /*tmpLead.LG_VisitPostalCode__c=VisitPostalCode;*/

        return tmpLead;
    }
    }