@isTest
private class LG_UtilTest {

    private static String vfBaseUrl = 'vforce.url';
    private static String sfdcBaseUrl = 'sfdc.url';

    @testsetup
    private static void setupTestData() {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;

        LG_EnvironmentVariables__c envVariables = new LG_EnvironmentVariables__c();
        envVariables.LG_SalesforceBaseURL__c = sfdcBaseUrl;
        envVariables.LG_VisualForceBaseURL__c = vfBaseUrl;
        envVariables.LG_CloudSenceAnywhereIconID__c = 'csaID';
        envVariables.LG_ServiceAvailabilityIconID__c = 'saIconId';
        envVariables.LG_SandboxInstance__c = 'dev';
        insert envVariables;

        csordtelcoa__Orders_Subscriptions_Options__c osOptions = new csordtelcoa__Orders_Subscriptions_Options__c();
        osOptions.csordtelcoa__Statuses_Not_Allowing_Change__c = 'Change,Move,Terminate';
        osOptions.LG_ServiceRequestDeactivateStatus__c = 'Ready for Deactivation';
        osOptions.LG_SubscriptionRequestDeactivateStatus__c = 'Ready for Deactivation';
        osOptions.csordtelcoa__Subscription_Closed_Replaced_State__c = 'Inactive';
        insert osOptions;

        LG_MACDSpecificVariables__c macdSpecific = new LG_MACDSpecificVariables__c();
        macdSpecific.LG_SalesChannelsAllowedForTerminate__c = 'Test Sales Channel,Test2 Sales';
        insert macdSpecific;

        List<cscrm__Address__c> addresses = new List<cscrm__Address__c>();
        cscrm__Address__c addressOne = new cscrm__Address__c(Name='AddressOne', LG_AddressID__c = 'Address1', LG_Footprint__c = 'TestFootprint');
        cscrm__Address__c addressTwo = new cscrm__Address__c(Name='AddressTwo', LG_AddressID__c = 'Address2', LG_Footprint__c = 'Ziggo');
        addresses.add(addressOne);
        addresses.add(addressTwo);
        insert addresses;

        noTriggers.Flag__c = false;
        upsert noTriggers;
    }

    private static testmethod void testGetVisualForceBaseURl() {
        System.assertEquals(vfBaseUrl, LG_Util.getVisualForceBaseUrl(),
                            'VisualForce url base url should be ' + vfBaseUrl);
    }

    private static testmethod void testGetSalesforceBaseURl() {
        System.assertEquals(sfdcBaseUrl, LG_Util.getSalesforceBaseUrl(),
                            'Salesforce url base url should be ' + sfdcBaseUrl);
    }

    private static testmethod void testGetOSStatusesNotAllowingChange() {
        List<String> statusesNotAllowingChange = LG_Util.getOSStatusesNotAllowingChange();
        System.assertEquals(3, statusesNotAllowingChange.size(), 'Three statuses should be in the Custom settings list');
    }

    private static testmethod void testGetSubscriptionRequestDeactivateStatus() {
        System.assertEquals('Ready for Deactivation', LG_Util.getSubscriptionRequestDeactivateStatus(), 'Status should be Ready for Deactivation');
    }

    private static testmethod void testGetServiceRequestDeactivateStatus() {
        System.assertEquals('Ready for Deactivation', LG_Util.getServiceRequestDeactivateStatus(), 'Status should be Ready for Deactivation');
    }

    private static testmethod void testGetSubscriptionClosedReplacedStatus() {
        System.assertEquals('Inactive', LG_Util.getSubscriptionClosedReplacedStatus(), 'Status should be Inactive');
    }

    private static testmethod void testGetLookupFieldReferenceId() {
        Test.startTest();
        String referenceId = LG_Util.getLookupFieldReferenceId(new PageReference('/' + Contact.getSObjectType().getDescribe().getKeyPrefix()),
                             Account.getSObjectType().getDescribe().getKeyPrefix());
        Test.stopTest();

        System.assertEquals('testId', referenceId, 'reference Id should be testId');
    }

    private static testmethod void testGetSandboxInstanceName() {
        System.assertEquals('dev', LG_Util.getSandboxInstanceName(), 'Instance should be dev');
    }

    @IsTest
    private static void ValidateIDsTest() {
        Boolean BothIDs = false;
        Boolean SFID = false;
        Boolean GUID = false;

        Test.startTest();

        BothIDs = (LG_Util.IsValidId('00Q8E00000259dU') && LG_Util.IsValidId('gasfhdhd-xsw2-4r32-8e2h-df232454fs1a'));
        SFID = LG_Util.IsValidSFId('00Q8E00000259dU');
        GUID = LG_Util.IsValidConfiguratorId('gasfhdhd-xsw2-4r32-ye2h-df232454fs1a');

        Test.stopTest();

        System.assertEquals(true, BothIDs, 'Invalid conversion');
        System.assertEquals(true, SFID, 'Invalid conversion');
        System.assertEquals(true, GUID, 'Invalid conversion');
    }

    /**
     * Billing account is not automatically assigned to the product
     * basket, when order is uploaded from D2D app
     *
     * @author Petar Miletic
     * @ticket SFDT-1182, SFDT-1341
     * @since  17/06/2016
    */
    @IsTest
    private static void resolveAndUpsertBillingAccountDuplicatesTest() {

        Account acc = LG_GeneralTest.CreateAccount('AccountSFDT', '12345678', 'Ziggo', true);

        // Create Billing Account with IBAN field populated
        csconta__Billing_Account__c ba = LG_GeneralTest.createBillingAccount('SFDT-59 Billb', acc.Id, true, false);
        ba.LG_BankAccountNumberIBAN__c = 'NL91ABNA0417164300';
        insert ba;

        // Create list of Billing Accounts
        List<csconta__Billing_Account__c> billingAccounts = new List<csconta__Billing_Account__c>();

        for (Integer i = 0; i < 3; i++) {

            if (i == 0) {

                // This one should not be matched because Account is different
                Account acc2 = LG_GeneralTest.CreateAccount('AccountSFDT', '87654321', 'Ziggo', true);
                billingAccounts.add(LG_GeneralTest.createBillingAccount('SFDT-59 Bill' + i, acc2.Id, true, false));
                continue;
            }

            billingAccounts.add(LG_GeneralTest.createBillingAccount('SFDT-59 Bill' + i, acc.Id, true, false));
        }

        billingAccounts[2].LG_BankAccountNumberIBAN__c = 'NL91ABNA0417164300';

        Test.startTest();

        LG_Util.resolveAndUpsertBillingAccountDuplicates(billingAccounts);

        Test.stopTest();

        List<csconta__Billing_Account__c> billingAccountsUpdated =  [SELECT Id, Name FROM csconta__Billing_Account__c];

        // There must be 3 records. 2 inserted and 1 updated (matched by IBAN)
        System.assertEquals(3, billingAccountsUpdated.size(), 'Invalid Billing Account data');
    }

    private static testmethod void testUpdatePremiseFootprint()
    {
        cscrm__Address__c address = [SELECT Id, LG_Footprint__c FROM cscrm__Address__c WHERE LG_AddressID__c = 'Address1'];
        System.assertNotEquals('Ziggo', address.LG_Footprint__c, 'Address footprint should not be Ziggo');

        Test.startTest();
            LG_Util.updatePremiseFootprint('Address1', 'Ziggo');
        Test.stopTest();

        address = [SELECT Id, LG_Footprint__c FROM cscrm__Address__c WHERE LG_AddressID__c = 'Address1'];
        System.assertEquals('Ziggo', address.LG_Footprint__c, 'Address footprint should be Ziggo');
    }

    private static testmethod void testResolveSiteDuplicatesAndUpsert()
    {

        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        // Commented on 5.11.2019. after test failing with: "System.DmlException: Upsert failed. First exception on row 0; first error: DUPLICATE_VALUE, duplicate value found: SetupOwnerId duplicates value on record with id: 00D3N0000008aJ3: []"
        // noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;

        List<cscrm__Address__c> addresses = new List<cscrm__Address__c>();
        cscrm__Address__c addressOne = new cscrm__Address__c(Name='AddressOne', LG_AddressID__c = 'Address1', LG_Footprint__c = 'TestFootprint');
        addresses.add(addressOne);
        insert addresses;

        Account account = LG_GeneralTest.CreateAccount('AccountSFDT', '12345678', 'Ziggo', true);

        Opportunity opp = LG_GeneralTest.CreateOpportunity(account, true);

        cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('PC Test Basket', account, null, opp, false);
        basket.LG_SuiteRoom__c = '204A';
        basket.LG_CreatedFrom__c = 'Tablet';
        insert basket;

        cscrm__Site__c site = new cscrm__Site__c();
        site.cscrm__Suite_Room__c = '204A';
        site.cscrm__Installation_Address__c = addressOne.Id;
        insert site;

        noTriggers.Flag__c = false;
        upsert noTriggers;

        Test.startTest();
            cscrm__Site__c siteToUpsert = LG_Util.createNewSite(basket, addressOne.Id);
            List<cscrm__Site__c> sites = new List<cscrm__Site__c>();
            sites.add(siteToUpsert);

            LG_Util.resolveSiteDuplicatesAndUpsert(sites);
        Test.stopTest();

        System.assertEquals(site.Id, sites[0].Id, 'Site Ids should be the same');
    }

    private static testmethod void testGetCurrentUserSalesChannel()
    {
        User currentUser = [SELECT Id, LG_SalesChannel__c
                            FROM User WHERE Id = :UserInfo.getUserId()];

        Test.startTest();
            String currentUserSalesChannel = LG_Util.getCurrentUserSalesChannel();
        Test.stopTest();

        System.assertEquals(currentUser.LG_SalesChannel__c, currentUserSalesChannel, 'Sales channel should be the same');
    }

    private static testmethod void testGetSalesChannelsForTerminate()
    {
        Test.startTest();
            List<String> salesChannels = LG_Util.getSalesChannelsForTerminate();
        Test.stopTest();
        Set<String> salesChannelsSet = new Set<String>(salesChannels);

        System.assertEquals(true, salesChannelsSet.contains('Test Sales Channel'), 'Sales channel list should contain Test Sales Channel');
        System.assertEquals(true, salesChannelsSet.contains('Test2 Sales'), 'Sales channel list should contain Test2 Sales');
    }

    /*
     * Create Task test
     *
     * @author Petar Miletic
     * @ticket SFDT-902, SFDT-1261
    */
    @IsTest
    private static void createTaskTest() {

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        Account acc = LG_GeneralTest.CreateAccount('AccountSFDT', '12345678', 'Ziggo', true);
        Opportunity opp = LG_GeneralTest.CreateOpportunity(acc, false);
        opp.ownerId = thisUser.Id;
        insert opp;

        Test.startTest();

        LG_Util.createTask('Task Creation Test', 'Task: Other', null, opp.ownerId, Date.today(), 'Completed', 'Normal', opp.Id, true);

        Test.stopTest();

        List<Task> tasks = [SELECT Id, Subject FROM Task WHERE Subject = 'Task Creation Test' AND WhatId = :opp.Id];

        System.assertEquals(1, tasks.size(), 'Invalid data, Task is misssing');
    }

    @Istest
    private static void generateRandomNumberStringTest() {

        String str = '';

        Test.startTest();

        str = LG_Util.generateRandomNumberString(5);

        Test.stopTest();

        System.assertEquals(5, str.length(), 'Invalid data');
        System.assertEquals(true, Pattern.matches('\\d+', str), 'Invalid data');
    }

    @IsTest
    private static void createNewPremiseTest() {

        cscrm__Address__c address = null;

        Framework__c setting = new Framework__c();
        setting.Framework_Sequence_Number__c=0;
        insert setting;

        Account acc = LG_GeneralTest.CreateAccount('AccountSFDT', '12345678', 'Ziggo', true);
        Opportunity opp = LG_GeneralTest.CreateOpportunity(acc, true);

        cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', acc, null, opp, false);

        basket.LG_InstallationCity__c = 'Agram';
        basket.LG_InstallationCountry__c = 'Netherlands';
        basket.LG_InstallationHouseNumber__c = '10';
        basket.LG_InstallationStreet__c = 'Test Street';
        basket.LG_InstallationPostalCode__c = 'NX201456';
        basket.LG_InstallationHouseNumberExtension__c = 'A';
        basket.csbb__Account__c = acc.Id;
        basket.LG_SharedOfficeBuilding__c = false;
        basket.LG_COAXConnectionLocation__c = 'Test2';

        insert basket;

        Test.startTest();

        address = LG_Util.createNewPremise(basket);

        Test.stopTest();

        System.assertEquals('Agram', address.cscrm__City__c, 'Invalid data');
        System.assertEquals('Netherlands', address.cscrm__Country__c, 'Invalid data');
        System.assertEquals('10', address.LG_HouseNumber__c, 'Invalid data');
        System.assertEquals('Test Street', address.cscrm__Street__c, 'Invalid data');
        System.assertEquals('NX201456', address.cscrm__Zip_Postal_Code__c, 'Invalid data');
        System.assertEquals('A', address.LG_HouseNumberExtension__c, 'Invalid data');
        System.assertEquals(acc.Id, address.cscrm__Account__c, 'Invalid data');
        System.assertEquals(false, address.LG_SharedOfficeBuilding__c, 'Invalid data');
        System.assertEquals('Test2', address.LG_COAXConnectionLocation__c, 'Invalid data');
    }

    @IsTest
    private static void createNewPremiseByOpportunityTest() {

        cscrm__Address__c address = null;

        Framework__c setting = new Framework__c();
        setting.Framework_Sequence_Number__c=0;
        insert setting;

        Account acc = LG_GeneralTest.CreateAccount('AccountSFDT', '12345678', 'Ziggo', true);
        Opportunity opp = LG_GeneralTest.CreateOpportunity(acc, true);

        cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', acc, null, opp, false);

        basket.LG_InstallationCity__c = 'Agram';
        basket.LG_InstallationCountry__c = 'Netherlands';
        basket.LG_InstallationHouseNumber__c = '10';
        basket.LG_InstallationStreet__c = 'Test Street';
        basket.LG_InstallationPostalCode__c = 'NX201456';
        basket.LG_InstallationHouseNumberExtension__c = 'A';
        basket.csbb__Account__c = opp.AccountID;
        basket.LG_SharedOfficeBuilding__c = false;
        basket.LG_COAXConnectionLocation__c = 'Test2';

        insert basket;

        Test.startTest();

        address = LG_Util.createNewPremise(basket, opp);

        Test.stopTest();

        System.assertEquals('Agram', address.cscrm__City__c, 'Invalid data');
        System.assertEquals('Netherlands', address.cscrm__Country__c, 'Invalid data');
        System.assertEquals('10', address.LG_HouseNumber__c, 'Invalid data');
        System.assertEquals('Test Street', address.cscrm__Street__c, 'Invalid data');
        System.assertEquals('NX201456', address.cscrm__Zip_Postal_Code__c, 'Invalid data');
        System.assertEquals('A', address.LG_HouseNumberExtension__c, 'Invalid data');
        System.assertEquals(opp.AccountID, address.cscrm__Account__c, 'Invalid data');
        System.assertEquals(false, address.LG_SharedOfficeBuilding__c, 'Invalid data');
        System.assertEquals('Test2', address.LG_COAXConnectionLocation__c, 'Invalid data');
    }

    @IsTest
    private static void GetCountryCodeFromCountryNameTest() {

        String countryCodeNL = '';
        String countryCodeDE = '';
        String countryCodeAT = '';
        String countryCodePL = '';
        String countryCodeISO = '';

        Test.startTest();

        countryCodeNL = LG_Util.GetCountryCodeFromCountryName('Netherlands');
        countryCodeDE = LG_Util.GetCountryCodeFromCountryName('Germany');
        countryCodeAT = LG_Util.GetCountryCodeFromCountryName('Auatria');
        countryCodePL = LG_Util.GetCountryCodeFromCountryName('Poland');
        countryCodeISO = LG_Util.GetCountryCodeFromCountryName('Test');

        Test.stopTest();

        System.assertEquals('NL', countryCodeNL, 'Invalid data');
        System.assertEquals('DE', countryCodeDE, 'Invalid data');
        System.assertEquals('AT', countryCodeAT, 'Invalid data');
        System.assertEquals('PL', countryCodePL, 'Invalid data');
        System.assertEquals('ISO', countryCodeISO, 'Invalid data');
    }

    @IsTest
    private static void compareTwoTriggerMaps() {

        cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('Phone Numbers', true);

        cscfga__Attribute_Definition__c a1 = LG_GeneralTest.createAttributeDefinition('Test definition 01', prodDef, 'Display Value', 'Decimal', '', 'Main OLI', '', false);
        cscfga__Attribute_Definition__c a2 = LG_GeneralTest.createAttributeDefinition('Test definition 02', prodDef, 'Display Value', 'Decimal', '', 'Main OLI', '', false);
        cscfga__Attribute_Definition__c a3 = LG_GeneralTest.createAttributeDefinition('Test definition 03', prodDef, 'Display Value', 'Decimal', '', 'Main OLI', '', false);
        cscfga__Attribute_Definition__c a4 = LG_GeneralTest.createAttributeDefinition('Test definition 04', prodDef, 'Display Value', 'Decimal', '', 'Main OLI', '', false);
        cscfga__Attribute_Definition__c a5 = LG_GeneralTest.createAttributeDefinition('Test definition 05', prodDef, 'Display Value', 'Decimal', '', 'Main OLI', '', false);

        a1.LG_Commitment__c = 'Yes';
        a2.LG_Commitment__c = 'Yes';
        a3.LG_Commitment__c = 'No';
        a4.LG_Commitment__c = 'No';
        a5.LG_Commitment__c = 'No';

        List<cscfga__Attribute_Definition__c> attributeDefinitions = new List<cscfga__Attribute_Definition__c> { a1, a2, a3, a4, a5 };

        insert attributeDefinitions;

        LG_GeneralTest.createAttributeFieldDefinition('Commitment', 'Yes', a1, true);
        LG_GeneralTest.createAttributeFieldDefinition('Commitment', 'Yes', a2, true);

        // Create old new Map
        Map<Id, cscfga__Attribute_Definition__c> oldMap = new Map<Id, cscfga__Attribute_Definition__c>([SELECT Id, Name, LG_Commitment__c FROM cscfga__Attribute_Definition__c]);

        Map<Id, cscfga__Attribute_Definition__c> newMap = new Map<Id, cscfga__Attribute_Definition__c>();

        for (cscfga__Attribute_Definition__c o :oldMap.values()) {

            newMap.put(o.Id, new cscfga__Attribute_Definition__c( Id = o.Id, Name = o.Name, LG_Commitment__c = 'Yes'));
        }

        Set<Id> retval = new Set<Id>();

        Test.startTest();

        retval = LG_Util.compareTwoTriggerMaps('LG_Commitment__c', oldMap, newMap);

        Test.stopTest();

        System.assertEquals(3, retval.size(), 'Invalid data');
    }

    private static testmethod void testGetFormattedDate()
    {
        System.assertEquals('20-3-2016', LG_Util.getFormattedDate(date.newinstance(2016, 3, 20)), 'Date string should be 20-3-2016');
    }

    private static testmethod void getAdminEmailAddressTest() {
        Test.startTest();
        List<String> adminEmailAddresses = LG_Util.getAdminEmailAddresses();
        System.debug('Email: ' + adminEmailAddresses);
        Test.stopTest();
    }

    private static testmethod void getVisualForceBaseUrlTest() {
        Test.startTest();
        String baseUrl = LG_Util.getVisualForceBaseUrl();
        System.debug('Email: ' + baseUrl);
        Test.stopTest();
    }

    private static testmethod void getSalesForceBaseUrlTest() {
        Test.startTest();
        String baseUrl = LG_Util.getSalesforceBaseUrl();
        System.debug('Email: ' + baseUrl);
        Test.stopTest();
    }

    private static testmethod void getSubscriptionRequestDeactivateStatusTest() {
        Test.startTest();
        String subscriptionRequestDeactivateStatus = LG_Util.getSubscriptionRequestDeactivateStatus();
        System.debug('subscriptionRequestDeactivateStatus: ' + subscriptionRequestDeactivateStatus);
        System.assertEquals('Ready for Deactivation', subscriptionRequestDeactivateStatus);
        Test.stopTest();
    }

    private static testmethod void getSubscriptionClosedReplacedStatusTest() {
        Test.startTest();
        String subscriptionClosedReplacedStatus = LG_Util.getSubscriptionClosedReplacedStatus();
        System.debug('subscriptionClosedReplacedStatus: ' + subscriptionClosedReplacedStatus);
        System.assertEquals('Inactive', subscriptionClosedReplacedStatus);
        Test.stopTest();
    }

    private static testmethod void getRecordTypebyDevNameTest() {
        Test.startTest();
        String recordTypebyDevName = LG_Util.getRecordTypebyDevName('Test');
        System.debug('recordTypebyDevName: ' + recordTypebyDevName);
        System.assertEquals(null, recordTypebyDevName);
        Test.stopTest();
    }

    private static testmethod void generateRandomCharacterStringTest() {
        Test.startTest();
        String randomCharacterString = LG_Util.generateRandomCharacterString(10);
        System.debug('randomCharacterString: ' + randomCharacterString);
        System.assertNotEquals('', randomCharacterString);
        Test.stopTest();
    }

    private static testmethod void generateGUIDTest() {
        Test.startTest();
        String guid = LG_Util.generateGUID();
        System.debug('guid: ' + guid);
        System.assertNotEquals('', guid);
        Test.stopTest();
    }

    private static testmethod void trimAllTest() {
        Test.startTest();
        String trimmedString = LG_Util.trimAll(' Test ');
        System.debug('trimmedString: ' + trimmedString);
        String expectedTrimmedString = 'Test';
        System.assertEquals(expectedTrimmedString, trimmedString);
        Test.stopTest();
    }

    private static testmethod void createInstallationLeadDateForPhoneNumberTest() {
        Test.startTest();
        Date installationLeadDateForPhoneNumber = LG_Util.createInstallationLeadDateForPhoneNumber(0);
        System.debug('installationLeadDateForPhoneNumber: ' + installationLeadDateForPhoneNumber);
        Test.stopTest();
    }

    private static testmethod void sendAdminEmailTest() {
        Test.startTest();
        LG_Util.SendAdminEmail(new LG_Exception(), 'Test Place');
        Test.stopTest();
    }

    private static testmethod void checkNullPaddedTest() {
        Test.startTest();
        String paddedString = LG_Util.checkNullPadded('Input', 'Prefix', 'Sufix');
        System.debug('paddedString: ' + paddedString);
        String expectedPaddedString = 'PrefixInputSufix';
        System.assertEquals(expectedPaddedString, paddedString);
        Test.stopTest();
    }

    private static testmethod void checkIdNullTest() {
        Test.startTest();
        String checkIdString = LG_Util.checkIdNull(null);
        System.debug('checkIdString: ' + checkIdString);
        System.assertEquals('', checkIdString);
        Test.stopTest();
    }

    private static testmethod void getFormattedAddressTest() {
        Test.startTest();
        String formattedAddressString = LG_Util.getFormattedAddress('Street', 'HouseNumber', 'HouseNumberExt', 'PostCode', 'City');
        System.debug('formattedAddressString: ' + formattedAddressString);
        String formattedExpected = 'Street HouseNumber HouseNumberExt, PostCode City';
        System.assertEquals(formattedExpected, formattedAddressString);
        Test.stopTest();
    }

    private static testmethod void createNewBillingAccountTest() {

        Framework__c frameworkSetting = new Framework__c();
	    frameworkSetting.Framework_Sequence_Number__c = 2;
        insert frameworkSetting;

        Test.startTest();
        List<cscrm__Address__c> addresses = new List<cscrm__Address__c>();
        cscrm__Address__c addressOne = new cscrm__Address__c(Name='AddressOne', LG_AddressID__c = 'Address1', LG_Footprint__c = 'TestFootprint');
        addresses.add(addressOne);
        insert addresses;

        Account account = LG_GeneralTest.CreateAccount('AccountSFDT', '12345678', 'Ziggo', true);

        Opportunity opp = LG_GeneralTest.CreateOpportunity(account, true);

        cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('PC Test Basket', account, null, opp, false);
        basket.LG_SuiteRoom__c = '204A';
        basket.LG_CreatedFrom__c = 'Tablet';
        insert basket;
        csconta__Billing_Account__c billingAccount = LG_Util.createNewBillingAccount(basket, opp);
        System.debug('billingAccount: ' + billingAccount); //csconta__Account__c
        System.assertEquals(account.Id, billingAccount.csconta__Account__c);
        Test.stopTest();
    }

    private static testmethod void resolveAndInsertPremiseDuplicatesTest() {
        Test.startTest();
        List<cscrm__Address__c> addresses = new List<cscrm__Address__c>();
        cscrm__Address__c address = new cscrm__Address__c();
        addresses.add(address);
        LG_Util.resolveAndInsertPremiseDuplicates(addresses);
        Test.stopTest();
    }

    private static testmethod void resolveAndUpsertPremiseDuplicatesTest() {
        Test.startTest();
        List<cscrm__Address__c> addresses = new List<cscrm__Address__c>();
        cscrm__Address__c address = new cscrm__Address__c();
        addresses.add(address);
        LG_Util.resolveAndUpsertPremiseDuplicates(addresses);
        Test.stopTest();
    }

    private static testmethod void getKeySetByPropertyNameTest() {
        Test.startTest();
        List<sObject> objList = new List<sObject>();
        Set<Id> keySetByPropertyName = LG_Util.getKeySetByPropertyName('fieldname', objList);
        System.debug('keySetByPropertyName: ' + keySetByPropertyName);
        System.assert(true, keySetByPropertyName.isEmpty());
        Test.stopTest();
    }

    private static testmethod void validateUpdateFieldsTest() {
        Test.startTest();

        Boolean replaceDifferent = LG_Util.validateUpdateFields('Test', 'Test1');
        System.assert(true, replaceDifferent);
        System.debug('replaceDifferent: ' + replaceDifferent);

        Test.stopTest();
    }

    // Coommented out due to missing LG_Account trigger logic
/*
    private static testmethod void fetchPostalCodeAddressesTest() {
        List<LG_PostalCode__c> postCodes=LG_TestDataUtility.createPostalCodes();
        insert postCodes;
        Account customerAccount=LG_TestDataUtility.CreateAccount('Test customer Account', '00001235','Ziggo','9','9999 XK','9','9999 XK');
        insert customerAccount;

        Account accFetched = new Account();
        accFetched = [Select Id,LG_PostalStreet__c,LG_PostalCountry__c,LG_PostalCity__c,LG_VisitCity__c,LG_VisitCountry__c,LG_VisitStreet__c from Account where Id =: customerAccount.id];
        System.assertEquals('STITSWERD',accFetched.LG_PostalCity__c);
    }
*/
    }