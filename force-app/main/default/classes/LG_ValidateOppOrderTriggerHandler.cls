/**
* Used for Opportunity validation
* 
* @author Petar Miletic
* @ticket  & SFDT-271
* @since  14/03/2016
*/
public with sharing class LG_ValidateOppOrderTriggerHandler {

    
    public static void BeforeUpdateHandle(Map<Id, Opportunity> mapNew, Map<Id, Opportunity> mapOld) {
        
        ValidateOpportunity(mapNew, mapOld);
        ValidateAdminContact(mapNew, mapOld);
    }
    
    private static void ValidateAdminContact(Map<ID,Opportunity> mapNew, Map<ID, Opportunity> mapOld) {
        
        Map<Id, Opportunity> relevantOpportunities = new Map<Id, Opportunity>();
        for (Opportunity opp : mapNew.values()) {           
            if (opp.StageName == 'Ready for Order' && mapOld.get(opp.Id).StageName != 'Ready for Order') {
                relevantOpportunities.put(opp.Id, opp);
            }
        }
        
        Map<ID, OpportunityContactRole> mapOppConRole = new Map<ID, OpportunityContactRole>();
        for(OpportunityContactRole loopVar : [Select OpportunityID, ContactID, Contact.Salutation,Contact.FirstName, Contact.Phone, Contact.MobilePhone, Contact.Email, Role from OpportunityContactRole where OpportunityID in : relevantOpportunities.keySet() and role = :Label.LG_AdministrativeContact]) {
            mapOppConRole.put(loopVar.OpportunityID, loopVar);
        }
        
        for(ID loopVar : relevantOpportunities.keySet()) {
            if(!mapOppConRole.keySet().contains(loopVar)) {
                relevantOpportunities.get(loopVar).addError(Label.LG_AdminContactMissing , false);
                break;
            } else if(mapOppConRole.get(loopVar).Contact.Salutation == null || mapOppConRole.get(loopVar).Contact.FirstName == null || mapOppConRole.get(loopVar).Contact.Email == null || (mapOppConRole.get(loopVar).Contact.Phone == null && mapOppConRole.get(loopVar).Contact.MobilePhone == null) ) {
                relevantOpportunities.get(loopVar).addError(Label.LG_AdminContactIncomplete , false);
                break;
            }
        }
    }

    private static void ValidateOpportunity(Map<Id, Opportunity> mapNew, Map<Id, Opportunity> mapOld) {
        
        String errorTemplate = '<p class="errorLine">•&nbsp;{0}</p>';
        

        // Group Contacts per Opportunity Id
        Map<Id, ValidationHelper> opportunityContacts = new Map<Id, ValidationHelper>();
        Map<Id, Opportunity> relevantOpportunities = new Map<Id, Opportunity>();
        Set<Id> noTechicalContact = new Set<Id>();
        
        Set<Id> accountIds = new Set<Id>();
    
        // Create collection of relevant Opportunituies
        for (Opportunity opp : mapNew.values()) {
            
            if (opp.StageName == 'Ready for Order' && mapOld.get(opp.Id).StageName != 'Ready for Order') {
                relevantOpportunities.put(opp.Id, opp);
                accountIds.add(opp.AccountId);
            }
        }

        if (relevantOpportunities.size() > 0) {
        
            // Get all attributes for Billing Account validation
            List<cscfga__Attribute_Field__c> attributeFields = [SELECT Id, Name, cscfga__Attribute__c, cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Basket__c, cscfga__Value__c,
                                                                    cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c
                                                                    FROM cscfga__Attribute_Field__c 
                                                                    WHERE Name = 'BillingAccount' AND 
                                                                    cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c IN :relevantOpportunities.keySet()
                                                                    AND cscfga__Attribute__r.cscfga__Is_Line_Item__c = true
                                                                    AND cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csordtelcoa__Synchronised_with_Opportunity__c = true];
                                                                    // AND cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c = NULL 


            Set<Id> technicalContactIDs = new Set<Id>();
            
            Map<Id, cscfga__Product_Configuration__c> pcs = new Map<Id, cscfga__Product_Configuration__c>([SELECT 
                                                                                                            Id, 
                                                                                                            Name, 
                                                                                                            cscfga__Product_Basket__c, 
                                                                                                            LG_Address__r.Id, 
                                                                                                            cscfga__Product_Basket__r.cscfga__Opportunity__c,
                                                                                                            cscfga__Product_Basket__r.cscfga__Opportunity__r.csordtelcoa__Change_Type__c,
                                                                                                            cscfga__Product_Basket__r.csordtelcoa__Synchronised_with_Opportunity__c, 
                                                                                                            LG_Address__r.LG_TechnicalContact__c, 
                                                                                                            LG_InstallationWishDate__c, 
                                                                                                            LG_InstallationPlannedDate__c,
                                                                                                            LG_InstallationNeeded__c,
                                                                                                            //CRQ000000759452
                                                                                                            cscfga__Product_Family__c,
                                                                                                            (SELECT Id, Name, cscfga__Value__c FROM cscfga__Attributes__r where name in ('Installation Lead Time','Porting Wish Date'))
                                                                                                            //CRQ000000759452
                                                                                                            FROM cscfga__Product_Configuration__c 
                                                                                                            WHERE cscfga__Product_Basket__r.cscfga__Opportunity__c IN :relevantOpportunities.keySet() AND
                                                                                                            cscfga__Product_Basket__r.csordtelcoa__Synchronised_with_Opportunity__c = true]);

            // One Opportunities had one ValidatiorHelper class. If one of PC is invalid then the entire Opportunity is invalid
            // For multiple Product Configurations update validator values by calling CompareAndUpdate method
            for (cscfga__Product_Configuration__c pc :pcs.values()) {
                
                ValidationHelper temp;
                
                // Check if opportunity exists. If so append data to already existing ID set
                if (opportunityContacts.containsKey(pc.cscfga__Product_Basket__r.cscfga__Opportunity__c)) {
                    
                    temp = opportunityContacts.get(pc.cscfga__Product_Basket__r.cscfga__Opportunity__c);
                    temp.CompareAndUpdate(pc);
                }
                else {
                    
                    temp = new ValidationHelper(pc);
                    opportunityContacts.put(pc.cscfga__Product_Basket__r.cscfga__Opportunity__c, temp);
                }
                
                technicalContactIDs.addAll(temp.GetTechnicalContactIDs());
            }

            Map<Id, Contact> contacts = new Map<Id, Contact>([SELECT Id, Name, Phone, MobilePhone, Email, FirstName, LastName, Salutation, cscrm__Address__c, LG_Role__c FROM Contact WHERE Id IN :technicalContactIDs]);

            for (Opportunity o :relevantOpportunities.values()) {
                
                ValidationHelper validator = opportunityContacts.get(o.Id);
                
                if (validator == null) {
                    o.addError(String.format(errorTemplate, new List<string> { 'Product Configurations are missing' }), false);
                    continue;
                }
                
                // Check if Opportunity is synced
                if (!validator.ValidateSync()) {
                    o.addError(Label.LG_SynchronizedProductBasketNeeded);
                    continue;
                }
                
                if (!CheckBillingAccount(o.Id, attributeFields)) {
                    o.addError(String.format(errorTemplate, new List<string> { 'Billing Account has not been assigned to the Line Items' }), false);
                    continue;
                }

                string errorMessage = '';

                // SFDT-648 - Not needed
                // if (!validator.ValidateInstallationPlannedDate()) {
                //    errorMessage += String.format(errorTemplate, new List<string> { 'Some Product Configurations have invalid installation date' });
                //}
                
                if (!validator.ValidateInstallationWishDate()) {
                    errorMessage += String.format(errorTemplate, new List<string> { 'Some Product Configurations are missing the preferred wish date' });
                }
                
                //Start - CATGOV-578
                if (!validator.CheckInstallationWishDate()) {
                    errorMessage += String.format(errorTemplate, new List<string> { 'Invalid wish date' });
                }
                system.debug('++++ errormessage : '+ errorMessage);
                //End - CATGOV-578
                
                // Check if technical contact exists
                if (!validator.ValidateTechnicalContacts()) {
                    errorMessage += String.format(errorTemplate, new List<string> { 'Some Premisses have undefined Technical Contact' });
                }
                //CRQ000000759452
                if(!validator.ValidatePortingWishDate()){
                    errorMessage += String.format(errorTemplate, new List<string> { 'Some phone numbers have incorrect porting wish date' });
                }
                //CRQ000000759452
                if (errorMessage.length() > 0) {
                    o.addError(errorMessage, false);
                    continue;
                }
                
                // For each contact that belongs to current opportunity
                for (ID contactId :validator.GetTechnicalContactIDs()) {
                    
                    Contact c = contacts.get(contactId);

                    if (c == null) {
                        continue;
                    }

                    if (c.MobilePhone == null && c.Phone == null) {
                        errorMessage += String.format(errorTemplate, new List<string> { String.format(Label.LG_MissingContactInformation, new List<String> {Schema.sObjectType.Contact.fields.MobilePhone.getLabel() + ' or ' + Schema.sObjectType.Contact.fields.Phone.getLabel(), 'Technical Contact'})});
                    }

                    if (c.Email == null) {
                        errorMessage += String.format(errorTemplate, new List<string> { String.format(Label.LG_MissingContactInformation, new List<String> {Schema.sObjectType.Contact.fields.Email.getLabel(), 'Technical Contact'})});
                    }
                    
                    if (c.FirstName == null) {
                        errorMessage += String.format(errorTemplate, new List<string> { String.format(Label.LG_MissingContactInformation, new List<String> {Schema.sObjectType.Contact.fields.FirstName.getLabel(), 'Technical Contact'})});
                    }
                    
                    // Last Name is required, no need for checking
                    
                    if (c.Salutation == null) {
                        errorMessage += String.format(errorTemplate, new List<string> { String.format(Label.LG_MissingContactInformation, new List<String> {Schema.sObjectType.Contact.fields.Salutation.getLabel(), 'Technical Contact'})});
                    }

                    if (errorMessage.length() > 0) {
                        o.addError(errorMessage, false);
                        break;
                    }   
                }
            }
        }
    }
    
    /**
    * Check Billing Account for Opportunity Sync Basket
    * 
    * @author Petar Miletic
    * @ticket SFDT-958
    * @since 18/05/2016
    */
    private static Boolean CheckBillingAccount(Id opportunityId, List<cscfga__Attribute_Field__c> attributeFields) {
        
        Boolean valExist = true;
        Boolean oppExist = false;

        for (cscfga__Attribute_Field__c a :attributeFields) {
            
            if (a.cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c == opportunityId) {
                
                oppExist = true;

                if (String.isBlank(a.cscfga__Value__c)) {
                    valExist = false;
                }
            }
        }
        
        // Both conditions need to verify in order to validate
        return oppExist && valExist;
    }
    
    // Helper class for Opportunity validation
    // One ValidatiorHelper coresponds to one Opportunity
    //
    // ValidateHeper is used for every Product Configuration, on first iteration use 
    // Constructor and for each subsequent one use CompareAndUpdate to test and set values
    private class ValidationHelper {

        // Private properties
        private Set<Id> TechnicalContactIDs = new Set<Id>();
        private Boolean AreTechnicalContactsValid { get; set; }
        private Boolean IsOpportunitySynced { get; set; }
        //CRQ000000759452
        private Map<cscfga__Product_Configuration__c,Map<String,cscfga__Attribute__c>> phoneNumber = new Map<cscfga__Product_Configuration__c,Map<String,cscfga__Attribute__c>>();
        //CRQ000000759452
        // SFDT-648 - Not needed
        //private Boolean AreInstallationPlannedDateSet { get; set; }
        
        private Boolean AreInstallationWishDateSet { get; set; }
        //Start - CATGOV-578
        private Boolean IsInstallationWishDateValid {get; set; }
        private Date minInstallDate { get; set; }
        //End - CATGOV-578
        
        //Start CATGOV-649
        private Date selectedWishDate { get; set; }
        //End CATGOV-649
        public Set<Id> GetTechnicalContactIDs() {
            return this.TechnicalContactIDs;
        }
        
        public Boolean ValidateTechnicalContacts() {
            return this.AreTechnicalContactsValid;
        }
        
        public Boolean ValidateSync() {
            return this.IsOpportunitySynced;
        }
               
        //private Boolean ValidateInstallationPlannedDate() {
        //    return this.AreInstallationPlannedDateSet;
        //}

        private Boolean ValidateInstallationWishDate() {
            return this.AreInstallationWishDateSet;
        }
        //CRQ000000759452
        private Boolean ValidatePortingWishDate(){
            Boolean isValid = true;
            system.debug('+++ phoneNumber : '+phoneNumber);
            if(this.phoneNumber!=null){
                for(cscfga__Product_Configuration__c pc : this.phoneNumber.keySet()){
                    system.debug('++++selectedWishDate : ' +  selectedWishDate);
                    Date installationLeadDate = LG_Util.createInstallationLeadDateForPhoneNumber(Integer.valueOf(phoneNumber.get(pc).get('Installation Lead Time').cscfga__Value__c));
                    Date portingWishDate = Date.valueOf(phoneNumber.get(pc).get('Porting Wish Date').cscfga__Value__c);
                    //OR Condition below is added as part of CATGOV-649
                    system.debug('++++portingWishDate : ' +  portingWishDate);
                    if(portingWishDate < installationLeadDate || portingWishDate < selectedWishDate.addDays(1)){
                        isValid = false;
                    }
                    if(!isValid)
                        break;
                }
            }
            return isValid;
        }
        //CRQ000000759452
        //Start - CATGOV-578
        private Boolean CheckInstallationWishDate() {
            return this.IsInstallationWishDateValid ;
        }        
        
        public Date GetMinInstallDate() {
            return this.minInstallDate;
        }
        
        //Calculates the minimum install date: today() + min lead time (working days).
        private void setMinInstallDate(cscfga__Product_Configuration__c pc)
        {
            if (pc.cscfga__Product_Basket__r.cscfga__Opportunity__r.csordtelcoa__Change_Type__c == 'Terminate') {
                this.minInstallDate = Date.today();
            } else {
                Boolean overStappenFlag = false;
                List<cscfga__Attribute__c> attr = [SELECT Name,cscfga__Display_Value__c,cscfga__Product_Configuration__c FROM cscfga__Attribute__c where Name='Overstappen'AND cscfga__Display_Value__c='Yes' AND cscfga__Product_Configuration__c =:pc.Id];
                system.debug('attr'+attr);             
                overStappenFlag = (!attr.isEmpty() && attr.size()>0) ? true : false;
                Integer minLeadTime = overStappenFlag == true ? 30 : 10;
                this.minInstallDate = Date.today();
                //first check if today is a weekend, if so
                //set the minInstallDate first to Monday...
                DateTime dtMinDate = (DateTime) this.minInstallDate;
                String dayOfWeek = dtMinDate.format('EEE');
                
                 //CATGOV-505, Overstappen logic is added to change min wish date from 30 working days to 30 calendar days
                if(overStappenFlag==false)
                {
                    if  (dayOfWeek.equals('Sat'))
                  {
                      this.minInstallDate = this.minInstallDate.addDays(2);
                  }
                  else if (dayOfWeek.equals('Sun'))
                  {
                      this.minInstallDate = this.minInstallDate.addDays(1);
                  }
                }
                
                //Now that we are sure that start ('today') is
                //a working day - Mon-Fri, add the min lead time
                //and don't take Sat and Sun in calculation
                for (Integer i = 0; i < minLeadTime; i++)
                {
                    this.minInstallDate = this.minInstallDate.addDays(1);
                    
                    //check if date now falls on weekend (Sat)
                    //if so, add 2 days to make it Monday
                    dtMinDate = (DateTime) this.minInstallDate;
                    dayOfWeek = dtMinDate.format('EEE');
                    
                    //CATGOV-505 , Overstappen logic is added to change minimum wish date from 30 working days to 30 calendar days
                    if  (overStappenFlag==false && dayOfWeek.equals('Sat'))
                    {
                        this.minInstallDate = this.minInstallDate.addDays(2);
                    }
                }
            }
            
        }
        //End - CATGOV-578
        
        // Constructor
        public ValidationHelper(cscfga__Product_Configuration__c pc) {
            
            this.AreTechnicalContactsValid = true;
            this.IsOpportunitySynced = false;
            //this.AreInstallationPlannedDateSet = true;
            this.AreInstallationWishDateSet = true;
            
            //Start - CATGOV-578
            this.IsInstallationWishDateValid = true;
            //End - CATGOV-578
            SetUpData(pc);
        }
        
        // Compare and update current data (when looping trough Product Configuration List)
        public void CompareAndUpdate(cscfga__Product_Configuration__c pc) {
            
            SetUpData(pc);
        }
        
        private void SetUpData(cscfga__Product_Configuration__c pc) {
            
            // Mark Opportunity as synced
            if (pc.cscfga__Product_Basket__r.csordtelcoa__Synchronised_with_Opportunity__c == true) {
                IsOpportunitySynced = true;
            }
            
            /*
             * If Address exists and Installation is needed populate
             * Technical Contact and Installation Wish Date
             * If not required field can be omitted
            */
            if (pc.LG_Address__c != null && pc.LG_InstallationNeeded__c) {
                
                if (pc.LG_Address__r.LG_TechnicalContact__c != null) {
                    this.TechnicalContactIDs.add(pc.LG_Address__r.LG_TechnicalContact__c);
                }
                else if (pc.LG_Address__r.LG_TechnicalContact__c == null) {
                    this.AreTechnicalContactsValid = false;
                }
                
                //if (pc.LG_InstallationNeeded__c && pc.LG_InstallationPlannedDate__c == null) {
                //    this.AreInstallationPlannedDateSet = false;
                //}
                
                //if (pc.LG_InstallationNeeded__c && pc.LG_InstallationWishDate__c == null) {
                if (pc.LG_InstallationWishDate__c == null) {
                    this.AreInstallationWishDateSet = false;
                }
                
                //Start - CATGOV-578
                setMinInstallDate(pc);
                system.debug('++++product installation wish date: ' +  pc.LG_InstallationWishDate__c);
                system.debug('++++earliest installation wish date: ' +  GetMinInstallDate());
                //Start CATGOV-649
                this.selectedWishDate = pc.LG_InstallationWishDate__c;
                //End CATGOV-649
                if (pc.LG_InstallationWishDate__c < Date.today() || pc.LG_InstallationWishDate__c < GetMinInstallDate()) {
                    this.IsInstallationWishDateValid = false;
                }
                //End - CATGOV-578
            }
            //CRQ000000759452
            if(pc.cscfga__Product_Family__c == 'Phone Numbers'){
                Boolean portIn = false;
                system.debug('++++product config attributes: ' +  pc.cscfga__Attributes__r);
                for(cscfga__Attribute__c a : pc.cscfga__Attributes__r){
                    if(a.name == 'Porting Wish Date' && (a.cscfga__Value__c != null || !String.isBlank(a.cscfga__Value__c))){
                        portIn = true;
                    }
                }
                if(portIn){
                    
                    Map<String, cscfga__Attribute__c> portinDetails = new Map<String, cscfga__Attribute__c>();
                    for(cscfga__Attribute__c a : pc.cscfga__Attributes__r){
                    if(a.name == 'Porting Wish Date'){
                        portinDetails.put('Porting Wish Date',a);
                    }
                    else if(a.name == 'Installation Lead Time'){
                        portinDetails.put('Installation Lead Time',a);
                    }
                }
                phoneNumber.put(pc,portinDetails);
                }
            }            
            //CRQ000000759452
        }
    }   
    
    
    
}