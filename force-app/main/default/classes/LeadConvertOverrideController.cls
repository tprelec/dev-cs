/**
 * @description			This class handles custom logic for lead conversion and different processes for internal/external sales
 * @author				Guy Clairbois
 */
public with sharing class LeadConvertOverrideController {

	private Lead theLead {
		get; set;
	} 
	private Boolean startedFromUI {
		get; set;
	}
	public LeadConvertOverrideController(ApexPages.StandardController controller) {
		if(!Test.isRunningTest()){
			controller.addFields(new List<String>{'Name','Company','BAN_Number__c','KVK_Number__c','Description','Visiting_PostalCode__c','Visiting_HouseNumber1__c'});
		}
		this.theLead = (Lead) controller.getRecord();
		this.startedFromUI = false;
		System.Debug('LEAD:'+this.theLead);
	}
	public PageReference customConvertLeadFromUI() {
		this.startedFromUI = true;
		if(System.Userinfo.getUserType() == 'PowerPartner'){
			return this.customConvertLead();
		} else {
			// normal conversion page
			PageReference pr = new PageReference('/lead/leadconvert.jsp?retURL=%2F'+theLead.Id+'&id='+theLead.Id+'&nooverride=1');
			return pr;
		}
	}
	
	/* reject status selection dropdown controllers */
	
	public String rejectStatusSelected {get;set;}

    public List<SelectOption> getRejectStatuses() {
        List<SelectOption> options = new List<SelectOption>();
        
        SelectOption so = new SelectOption('Followed up, long term interest','Followed up, long term interest');
	    options.add(so);
        SelectOption so2 = new SelectOption('No lead but service request','No lead but service request');
	    options.add(so2);	        	

        system.debug(options);
        return options;
    }

	
	public PageReference rejectLeadFromUI() {
		theLead.status = rejectStatusSelected;
		
		update theLead;
		return New PageReference('/'+theLead.Id);
	}	
	
	
	public PageReference cancel(){
		return New PageReference('/'+theLead.Id);
	}
	
	public PageReference customConvertLead() {
	
		// update Lead status
		//theLead.Status = 'Followed up, positive result';
		//theLead.recordTypeId = GeneralUtils.recordTypeMap.get('Lead').get('Converted_Lead');
		//theLead.IsConverted = true; THIS IS NOT POSSIBLE.
		theLead.Workflow_Cancel__c = true;
		try{
			update theLead;
		} catch (Exception e){
			return null;
		}
		
		// forward user to custom account search screen, prefilling some fields
		AccountCustomSearchController acsc = new AccountCustomSearchController();

		acsc.leadId = theLead.Id;
 
		PageReference pr = Page.AccountCustomSearch;
		if(theLead.BAN_Number__c != null) pr.getParameters().put('bannr',theLead.BAN_Number__c);
		if(theLead.KVK_Number__c != null) pr.getParameters().put('kvknr',theLead.KVK_Number__c);
		if(theLead.Name != null) pr.getParameters().put('name',theLead.Company);
		if(theLead.Visiting_PostalCode__c != null) pr.getParameters().put('zip',theLead.Visiting_PostalCode__c);
		if(theLead.Visiting_HouseNumber1__c != null) pr.getParameters().put('hnr',String.valueof(theLead.Visiting_HouseNumber1__c));
		
		//if(theLead.Description != null) pr.getParameters().put('descr',theLead.Description);
		pr.getParameters().put('leadId',theLead.Id);
		return pr;

	}	
	
}