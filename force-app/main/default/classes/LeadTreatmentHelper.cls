public with sharing class LeadTreatmentHelper {
	public LeadTreatmentHelper() {
		
	}

	public static List<SelectOption> GetStatusSelectOptions(){
		List<SelectOption> statuses = new List<SelectOption>();

		Schema.DescribeFieldResult fieldResult = Lead.Status.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for(Schema.PicklistEntry f : ple)
		{
			statuses.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
			return statuses;
	}

	public static List<SelectOption> GetTreatmentSelectOptions(Lead lead){
		List<SelectOption> treatments = new List<SelectOption>();


		if(lead != null){
			if(lead.Chosen_Treatment__c==null)treatments.add(new SelectOption('empty',Label.LABEL_Not_specified));
		
			//only add "Do not follow up" for non-unica campaign leads
			if(lead.Treatment_02__c!=null && Lead.LeadSource != 'CVM Campaign') treatments.add(new SelectOption('Do not follow up',Label.Label_CVM));

			if(lead.Treatment_01__c!=null)treatments.add(new SelectOption(lead.Treatment_01__c,lead.Treatment_01__c));
			if(lead.Treatment_02__c!=null)treatments.add(new SelectOption(lead.Treatment_02__c,lead.Treatment_02__c));
			if(lead.Treatment_03__c!=null)treatments.add(new SelectOption(lead.Treatment_03__c,lead.Treatment_03__c));
			if(lead.Treatment_04__c!=null)treatments.add(new SelectOption(lead.Treatment_04__c,lead.Treatment_04__c));
			if(lead.Treatment_05__c!=null)treatments.add(new SelectOption(lead.Treatment_05__c,lead.Treatment_05__c));
			if(lead.Treatment_06__c!=null)treatments.add(new SelectOption(lead.Treatment_06__c,lead.Treatment_06__c));
			if(lead.Treatment_07__c!=null)treatments.add(new SelectOption(lead.Treatment_07__c,lead.Treatment_07__c));
			if(lead.Treatment_08__c!=null)treatments.add(new SelectOption(lead.Treatment_08__c,lead.Treatment_08__c));
//			if(lead.Treatment_09__c!=null)treatments.add(new SelectOption(lead.Treatment_09__c,lead.Treatment_09__c));
//			if(lead.Treatment_10__c!=null)treatments.add(new SelectOption(lead.Treatment_10__c,lead.Treatment_10__c));
		}
		
		return treatments;
	}

	public static List<String> GetTreatmentsString(Lead lead, String selectedTreatment){
		List<String> treatmentsString = new List<String>();

		if(Lead != null){
			//only add "Do not follow up" for non-unica campaign leads
			if(lead.Treatment_02__c!=null && Lead.LeadSource != 'CVM Campaign') treatmentsString.add(Label.Label_CVM);

			system.debug('***SP*** selectedTreatment: ' + selectedTreatment); 
			system.debug('***SP*** Label.Label_CVM: ' + Label.Label_CVM);

			if(selectedTreatment == null || selectedTreatment == '' || selectedTreatment == 'empty'){
				//first filter, all available ones
				if(lead.Treatment_01__c!=null) treatmentsString.add(lead.Treatment_01__c);
				if(lead.Treatment_02__c!=null) treatmentsString.add(lead.Treatment_02__c);
				if(lead.Treatment_03__c!=null) treatmentsString.add(lead.Treatment_03__c);
				if(lead.Treatment_04__c!=null) treatmentsString.add(lead.Treatment_04__c);
				if(lead.Treatment_05__c!=null) treatmentsString.add(lead.Treatment_05__c);
				if(lead.Treatment_06__c!=null) treatmentsString.add(lead.Treatment_06__c);
				if(lead.Treatment_07__c!=null) treatmentsString.add(lead.Treatment_07__c);
				if(lead.Treatment_08__c!=null) treatmentsString.add(lead.Treatment_08__c);
//				if(lead.Treatment_09__c!=null) treatmentsString.add(lead.Treatment_09__c);
//				if(lead.Treatment_10__c!=null) treatmentsString.add(lead.Treatment_10__c);
			}
			else
			{
				if(lead.Treatment_01__c!=null && selectedTreatment == lead.Treatment_01__c) treatmentsString.add(lead.Treatment_01__c);
				if(lead.Treatment_02__c!=null && selectedTreatment == lead.Treatment_02__c) treatmentsString.add(lead.Treatment_02__c);
				if(lead.Treatment_03__c!=null && selectedTreatment == lead.Treatment_03__c) treatmentsString.add(lead.Treatment_03__c);
				if(lead.Treatment_04__c!=null && selectedTreatment == lead.Treatment_04__c) treatmentsString.add(lead.Treatment_04__c);
				if(lead.Treatment_05__c!=null && selectedTreatment == lead.Treatment_05__c) treatmentsString.add(lead.Treatment_05__c);
				if(lead.Treatment_06__c!=null && selectedTreatment == lead.Treatment_06__c) treatmentsString.add(lead.Treatment_06__c);
				if(lead.Treatment_07__c!=null && selectedTreatment == lead.Treatment_07__c) treatmentsString.add(lead.Treatment_07__c);
				if(lead.Treatment_08__c!=null && selectedTreatment == lead.Treatment_08__c) treatmentsString.add(lead.Treatment_08__c);
//				if(lead.Treatment_09__c!=null && selectedTreatment == lead.Treatment_09__c) treatmentsString.add(lead.Treatment_09__c);
//				if(lead.Treatment_10__c!=null && selectedTreatment == lead.Treatment_10__c) treatmentsString.add(lead.Treatment_10__c);
					
			}
		}
		
		return treatmentsString;
	}


}