public with sharing class LeadTriggerHandler extends TriggerHandler {
	private static final String ZIPCODE_FIELDNAME = Lead.ein_pc4__c.getDescribe().getName();
	private static final String KVKNUMB_FIELDNAME = Lead.KVK_number__c.getDescribe().getName();

	private List<Lead> oldLeads;
	private List<Lead> newLeads;
	private Map<Id, Lead> oldLeadMap;
	private Map<Id, Lead> newLeadMap;

	private void init() {
		oldLeads = (List<Lead>) this.oldLeads;
		newLeads = (List<Lead>) this.newList;
		oldLeadMap = (Map<Id, Lead>) this.oldMap;
		newLeadMap = (Map<Id, Lead>) this.newMap;
	}

	public override void beforeInsert() {
		init();
		runPartnerAssignment();
		cleanUpPostalCode();
		cleanUpPhone();
		populateZiggoLeadLookups();
		populateZiggoSizoLookup();
	}

	public override void afterInsert() {
		init();
		syncAccountQueueUserWithLeadOwner();
	}

	public override void beforeUpdate() {
		init();
		runPartnerAssignment();
		fillOpportunityLookup();
		cleanUpPhone();
		populateZiggoLeadLookups();
		populateZiggoSizoLookup();
	}

	public override void afterUpdate() {
		init();

		syncAccountQueueUserWithLeadOwner();
		populateQueueUserOnLeadConversion();
		runPartnerManagerApproval();
		updateSharingRules();
		updateAccountLeadHotnessSummary();
		assignOwnershipOfConvertedAccounts();
	}

	/**
	 * Returns list of converted leads
	 */
	private List<Lead> getConvertedLeads() {
		List<Lead> convertedLeads = new List<Lead>();
		for (Lead l : newLeads) {
			if (l.IsConverted && !oldLeadMap.get(l.Id).IsConverted) {
				convertedLeads.add(l);
			}
		}
		return convertedLeads;
	}

	/**
	 * Builds a map of account ids and ultimate parent account ids from converted leads
	 */
	private Map<Id, Id> getAccountParentsMap(List<Lead> leads) {
		Map<Id, Id> parentsMap = new Map<Id, Id>();
		for (Lead l : leads) {
			if (l.Ultimate_Parent_Account__c != null) {
				parentsMap.put(l.ConvertedAccountId, l.Ultimate_Parent_Account__c);
			}
		}
		return parentsMap;
	}

	/**
	 * Checks if dealer code matches
	 */
	private Dealer_Information__c checkDealer(
		Dealer_Information__c existingDealerInfo,
		Dealer_Information__c newDealerInfo,
		String dealerCode
	) {
		if (existingDealerInfo != null) {
			return existingDealerInfo;
		} else if (newDealerInfo != null && newDealerInfo.Dealer_Code__c == dealerCode) {
			return newDealerInfo;
		}
		return null;
	}

	/**
	 * Assigns account owner once Lead is converted
	 */
	public void assignOwnershipOfConvertedAccounts() {
		List<Lead> convertedLeads = getConvertedLeads();
		if (convertedLeads.isEmpty()) {
			return;
		}
		Set<Id> owners = GeneralUtils.getIDSetFromList(convertedLeads, 'OwnerId');
		Map<Id, Id> parentsMap = getAccountParentsMap(convertedLeads);

		Dealer_Information__c cmDealer;
		Dealer_Information__c cvmDealer;
		Map<Id, Dealer_Information__c> userDealerInfo = new Map<Id, Dealer_Information__c>();

		for (Dealer_Information__c d : [
			SELECT Id, Contact__r.UserId__c, Dealer_Code__c, ContactUserId__c
			FROM Dealer_Information__c
			WHERE
				Dealer_Code__c = :Label.Dealer_Code_CM_Park_Owner
				OR Dealer_Code__c = :Label.Dealer_Code_CVM_Owner
				OR Contact__r.UserId__c IN :owners
		]) {
			cmDealer = checkDealer(cmDealer, d, Label.Dealer_Code_CM_Park_Owner);
			cvmDealer = checkDealer(cvmDealer, d, Label.Dealer_Code_CVM_Owner);
			userDealerInfo.put(d.Contact__r.UserId__c, d);
		}

		List<Account> accountsToUpdate = new List<Account>();
		Map<Id, Account> parentAccounts = AccountTriggerHandler.getUltimateParents(
			new Set<Id>(parentsMap.values())
		);
		for (Lead l : convertedLeads) {
			Account a = new Account(Id = l.ConvertedAccountId);
			if (l.Ultimate_Parent_Account__c != null) {
				AccountTriggerHandler.setAccountResponsible(
					a,
					parentAccounts.get(l.Ultimate_Parent_Account__c)
				);
			} else if (l.VGE__c) {
				a.OwnerId = l.OwnerId;

				// dealer code
				if (userDealerInfo.containsKey(l.OwnerId)) {
					a.Mobile_Dealer__c = userDealerInfo.get(l.OwnerId).Id;
				}
			}
			setCmOwnership(l, a, cmDealer);
			setCvmOwnership(l, a, cvmDealer);

			accountsToUpdate.add(a);
		}

		if (!accountsToUpdate.isEmpty()) {
			SharingUtils.updateRecordsWithoutSharing(accountsToUpdate);
		}
	}

	private void setCmOwnership(Lead l, Account a, Dealer_Information__c cmDealer) {
		if (
			(l.No_of_Employees_Concern__c != null && l.No_of_Employees_Concern__c >= 50) ||
			(l.No_of_Employees_Concern__c == null &&
			l.NumberOfEmployees != null &&
			l.NumberOfEmployees >= 50)
		) {
			// cm ownership
			AccountTriggerHandler.setAccountResponsible(a, cmDealer);
		}
	}

	private void setCvmOwnership(Lead l, Account a, Dealer_Information__c cvmDealer) {
		if (
			(l.No_of_Employees_Concern__c != null && l.No_of_Employees_Concern__c < 50) ||
			(l.NumberOfEmployees != null &&
			l.NumberOfEmployees < 50)
		) {
			// cvm ownership
			AccountTriggerHandler.setAccountResponsible(a, cvmDealer);
		}
	}

	/**
	 * Populates the necessary lookups:
	 * - Demographics based on zipcode (digits only) - Could exist
	 * - Province (zipcode match - digits only) - Must exist
	 */
	private void populateZiggoSizoLookup() {
		if (GeneralUtils.filterChangedRecords(newLeads, oldLeadMap, KVKNUMB_FIELDNAME).isEmpty()) {
			return;
		}

		Map<String, List<Lead>> kvkLeadMap = getKVKLeadMap(newLeads);
		Set<String> kvkSet = kvkLeadMap.keySet();
		Map<String, Id> kvkInformationMap = sizoInformationMap(kvkSet);

		//Sizo
		for (string kvkno : kvkSet) {
			List<lead> zLeadSet1 = kvkLeadMap.get(kvkno);

			for (lead zLead1 : zLeadSet1) {
				zLead1.ein_sizo_data__c = kvkInformationMap.containsKey(kvkno)
					? kvkInformationMap.get(kvkno)
					: null;
			}
		}
	}

	/**
	 * Populates the necessary lookups:
	 * - Demographics based on zipcode (digits only) - Could exist
	 * - Province (zipcode match - digits only) - Must exist
	 */
	private void populateZiggoLeadLookups() {
		if (GeneralUtils.filterChangedRecords(newLeads, oldLeadMap, ZIPCODE_FIELDNAME).isEmpty()) {
			return;
		}

		Map<String, List<Lead>> zipcodeLeadMap = getZipcodeLeadMap(newLeads);
		Set<String> zipcodeSet = zipcodeLeadMap.keySet();

		Map<String, Id> zipcodeDemographicMap = getZipcodeDemographicsMap(zipcodeSet);
		Map<String, Id> zipcodeInformationMap = zipcodeInformationMap(zipcodeSet);

		for (String zipcode : zipcodeSet) {
			List<lead> zLeadSet = zipcodeLeadMap.get(zipcode);

			for (lead zLead : zLeadSet) {
				zLead.Ein_Demographic__c = zipcodeDemographicMap.containsKey(zipcode)
					? zipcodeDemographicMap.get(zipcode)
					: null;
				zLead.ein_Zipcode_Info__c = zipcodeInformationMap.containsKey(zipcode)
					? zipcodeInformationMap.get(zipcode)
					: null;
			}
		}
	}

	private Map<String, Id> getZipcodeDemographicsMap(Set<String> zipcodeSet) {
		Map<String, Id> zipcodeDemographicMap = new Map<String, Id>();

		for (Demographic__c d : [
			SELECT Id, ein_pc4__c
			FROM Demographic__c
			WHERE ein_pc4__c IN :zipcodeSet
		]) {
			zipcodeDemographicMap.put(d.ein_pc4__c, d.Id);
		}

		return zipcodeDemographicMap;
	}

	private Map<String, Id> zipcodeInformationMap(Set<String> zipcodeSet) {
		return ZipcodeService.zipcodeInformationMap(zipcodeSet);
	}

	private Map<String, Id> sizoInformationMap(Set<String> zipcodeSet) {
		return ZipcodeService.sizoInformationMap(zipcodeSet);
	}

	private Map<String, List<Lead>> getZipcodeLeadMap(List<Lead> ziggoLeadList) {
		return ZipcodeService.getZipcodeSObjectMap(ziggoLeadList, ZIPCODE_FIELDNAME);
	}

	private Map<String, List<Lead>> getKVKLeadMap(List<Lead> ziggoLeadList) {
		return ZipcodeService.getSizoSObjectMap(ziggoLeadList, KVKNUMB_FIELDNAME);
	}

	/**
	 * Cleans up phone to match NL phone number standard
	 */
	private void cleanUpPhone() {
		List<String> fieldsToCheck = new List<String>{
			'Phone',
			'Fax',
			'MobilePhone',
			'Direct_Phone_Number__c'
		};

		for (Lead mLead : newLeads) {
			for (String field : fieldsToCheck) {
				String fieldValue = (String) mLead.get(field);
				if (fieldValue != null) {
					mLead.put(field, StringUtils.cleanNLPhoneNumberInternal(fieldValue));
				}
			}
		}
	}

	/**
	 * Handles the approval and assignment of Partner Leads
	 */
	private void runPartnerAssignment() {
		Map<Id, Account> prefPartnerToPartnerAccount = new Map<Id, Account>();
		Map<Id, Id> prefPartnerToNewOwnerId = new Map<Id, Id>();

		for (Lead l : newLeads) {
			if (l.Preferred_Partner__c == null) {
				continue;
			}

			// only for new leads or leads with updated owner
			if (
				GeneralUtils.isRecordFieldChanged(
					l,
					oldLeadMap,
					new List<String>{ 'OwnerId', 'Preferred_Partner__c' },
					false
				)
			) {
				prefPartnerToPartnerAccount.put(l.Preferred_Partner__c, null);
			}

			// if it was approved just now, add it to both maps
			if (
				l.Approved_by_Partner_Manager__c &&
				GeneralUtils.isRecordFieldChanged(l, oldLeadMap, 'Approved_by_Partner_Manager__c')
			) {
				prefPartnerToNewOwnerId.put(l.Preferred_Partner__c, null);
				prefPartnerToPartnerAccount.put(l.Preferred_Partner__c, null);
			}
		}

		prefPartnerToPartnerAccount.putAll(
			getPartnerToPartnerAccMap(prefPartnerToPartnerAccount.keySet())
		);
		prefPartnerToNewOwnerId.putAll(
			getMainPartnerUsers(prefPartnerToNewOwnerId.keySet(), prefPartnerToPartnerAccount)
		);
		setPartnerApprovers(prefPartnerToPartnerAccount, prefPartnerToNewOwnerId);
	}

	/**
	 * Gets Partner Accounts needed for Partner assignment
	 */
	private Map<Id, Account> getPartnerToPartnerAccMap(Set<Id> accIds) {
		Map<Id, Account> prefPartnerToPartnerAccount = new Map<Id, Account>();
		if (!accIds.isEmpty()) {
			for (Account a : [
				SELECT Id, OwnerId, Dealer_Code__c
				FROM Account
				WHERE Id IN :accIds
			]) {
				prefPartnerToPartnerAccount.put(a.Id, a);
			}
		}
		return prefPartnerToPartnerAccount;
	}

	/**
	 * Gets Map of Partner Account IDs and related Main User IDs based on Dealer Code
	 */
	private Map<Id, Id> getMainPartnerUsers(Set<Id> partnerIds, Map<Id, Account> partnerMap) {
		Map<Id, Id> partnerToMainUser = new Map<Id, Id>();

		if (partnerIds.isEmpty()) {
			return partnerToMainUser;
		}

		for (User u : [
			SELECT Id, AccountId, DealerCode__c
			FROM User
			WHERE AccountId IN :partnerIds
		]) {
			// assign the lead to the user with the matching dealercode (main partner user)
			if (u.DealerCode__c == null || !partnerMap.containsKey(u.AccountId)) {
				continue;
			}
			String partnerDealerCode = formatDealerCode(partnerMap.get(u.AccountId).Dealer_Code__c);
			String userDealerCode = formatDealerCode(u.Dealercode__c);

			if (userDealerCode == partnerDealerCode) {
				partnerToMainUser.put(u.AccountId, u.Id);
			}
		}
		return partnerToMainUser;
	}

	/**
	 * Formats dealer code so it can be used for comparison on Account and User
	 */
	private String formatDealerCode(String unformattedDealerCode) {
		String dealerCode = unformattedDealerCode;
		if (dealerCode.startsWith('00')) {
			dealerCode = dealerCode.subString(2, dealerCode.length());
		}
		return dealerCode;
	}

	/**
	 * Sets partner approvers based on preferred partner
	 */
	private void setPartnerApprovers(
		Map<Id, Account> prefPartnerToPartnerAccount,
		Map<Id, Id> prefPartnerToNewOwnerId
	) {
		for (Lead l : newLeads) {
			// set partner manager for approval
			if (
				prefPartnerToPartnerAccount.containsKey(l.Preferred_Partner__c) &&
				prefPartnerToPartnerAccount.get(l.Preferred_Partner__c) != null
			) {
				l.Partner_Manager__c = prefPartnerToPartnerAccount.get(l.Preferred_Partner__c)
					.OwnerId;
			}

			if (
				l.Approved_by_Partner_Manager__c &&
				(oldLeadMap == null ||
				GeneralUtils.isRecordFieldChanged(l, oldLeadMap, 'Approved_by_Partner_Manager__c'))
			) {
				// if approved, set owner to partner user (this is the last step in the partner lead approval process)
				if (
					prefPartnerToNewOwnerId.containsKey(l.Preferred_Partner__c) &&
					prefPartnerToNewOwnerId.get(l.Preferred_Partner__c) != null
				) {
					l.OwnerId = prefPartnerToNewOwnerId.get(l.Preferred_Partner__c);
				} else {
					l.addError(
						'No user (new Lead Owner) found for partner account ' +
						l.Preferred_Partner__c
					);
				}
			}
		}
	}

	/**
	 *  Cleans Up Postal Code, Splits it into separate fields
	 */
	private void cleanUpPostalCode() {
		Pattern postalCodePattern = Pattern.compile('\\d{4}[A-Za-z]{2}');
		for (Lead l : newLeads) {
			if (l.Visiting_postalcode_merged_input__c != null) {
				// Remove whitespaces from Postalcode
				if (l.Visiting_postalcode_merged_input__c.length() > 6) {
					l.Visiting_postalcode_merged_input__c = l.Visiting_postalcode_merged_input__c.deleteWhitespace();
				}
				// Split Postcode
				if (l.Visiting_postalcode_merged_input__c.length() == 6) {
					Matcher regexMatcher = postalCodePattern.matcher(
						l.Visiting_postalcode_merged_input__c
					);
					if (regexMatcher.matches()) {
						l.Postcode_char__c = l.Visiting_postalcode_merged_input__c.right(2);
						l.Postcode_number__c = l.Visiting_postalcode_merged_input__c.left(4);
					}
				}
			}
		}
	}

	/**
	 * Triggers Partner Manager Approval Process
	 */
	private void runPartnerManagerApproval() {
		List<Approval.ProcessSubmitRequest> reqList = new List<Approval.ProcessSubmitRequest>();

		for (Lead l : newLeads) {
			if (
				l.OwnerId == GeneralUtils.partnerQueueId &&
				l.OwnerId != oldLeadMap.get(l.Id).OwnerId &&
				l.Preferred_Partner__c != null &&
				!l.Approved_by_Partner_Manager__c &&
				l.Partner_Manager__c != null &&
				l.Manager_approval_Status__c == null
			) {
				// start approval process
				Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
				req1.setObjectId(l.id);
				reqList.add(req1);
			}
		}

		if (!reqList.isEmpty()) {
			Approval.process(reqList);
		}
	}

	/**
	 * Populates opportunity lookup upon conversion
	 */
	private void fillOpportunityLookup() {
		for (Lead l : newLeads) {
			if (
				l.ConvertedOpportunityId != null &&
				oldLeadMap.get(l.Id).ConvertedOpportunityId == null
			) {
				l.Opportunity__c = l.ConvertedOpportunityId;
			}
		}
	}

	/**
	 * Updates sharing rules on Lead if Owner is changed
	 */
	private void updateSharingRules() {
		Set<Id> leadOwnerIds = GeneralUtils.getIDSetFromList(
			GeneralUtils.filterChangedRecords(newLeads, oldLeadMap, 'OwnerId'),
			'OwnerId'
		);

		if (leadOwnerIds.isEmpty()) {
			return;
		}

		// create sharing for the other dealercode contacts (only 1 per parent is necessary, the others will
		// get it automatically because they are all partner superusers)
		Map<Id, Id> parentPartnerUserIds = GeneralUtils.getParentPartnerUserIdsFromUserIds(
			leadOwnerIds
		);

		List<LeadShare> leadSharingRulesToInsert = new List<LeadShare>();
		if (!parentPartnerUserIds.isEmpty()) {
			for (Lead myLead : newLeads) {
				Id parentPartnerUserLeadOwner = getParentPartnerUserLeadOwner(
					parentPartnerUserIds,
					myLead.OwnerId
				);

				// Make sure the share isn't going to the owner (not possible and will throw an error)
				if (
					parentPartnerUserLeadOwner == null ||
					parentPartnerUserLeadOwner == myLead.OwnerId
				) {
					continue;
				}

				leadSharingRulesToInsert.add(
					new LeadShare(
						LeadId = myLead.Id,
						LeadAccessLevel = 'Read',
						UserOrGroupId = parentPartnerUserLeadOwner
					)
				);
			}
		}

		if (!leadSharingRulesToInsert.isEmpty()) {
			SharingUtils.insertRecordsWithoutSharing(leadSharingRulesToInsert);
		}
	}

	private Id getParentPartnerUserLeadOwner(Map<Id, Id> parentPartnerUserIds, Id ownerId) {
		return parentPartnerUserIds.containsKey(ownerId) ? parentPartnerUserIds.get(ownerId) : null;
	}

	private void updateAccountLeadHotnessSummary() {
		Map<Id, Account> accountsToUpdateMap = new Map<Id, Account>();

		//find all accounts that are related to leads being changed
		for (Lead myLead : newLeads) {
			if (myLead.Account_name__c != null) {
				accountsToUpdateMap.put(myLead.Account_name__c, null);
			}
		}

		if (accountsToUpdateMap.isEmpty()) {
			return;
		}

		Set<Id> accIds = accountsToUpdateMap.keySet();

		accountsToUpdateMap = new Map<Id, Account>(
			[
				SELECT Id, Name, Account_Lead_Hotness_Summary__c
				FROM Account
				WHERE Id IN :accountsToUpdateMap.keyset()
			]
		);

		//we are using fieldset to define which fields are we using for data models
		List<Schema.FieldSetMember> allDataModelFields = SObjectType.Lead.FieldSets.Silverpop_Lead_Score_Fields.getFields();

		//find all leads for all those accounts and put them in a map
		String sqlQuery = 'SELECT Id, Account_name__c ';

		for (FieldSetMember membr : allDataModelFields) {
			sqlQuery += ', ' + membr.getFieldPath();
		}

		sqlQuery += ' FROM Lead WHERE Account_name__c in : accIds';

		List<Lead> allLeads = Database.query(String.escapeSingleQuotes(sqlQuery));
		Map<Id, List<Lead>> leadsByAccountId = new Map<Id, List<Lead>>();

		for (Lead myLead : allLeads) {
			Id accId = myLead.Account_name__c;
			List<Lead> leadList = leadsByAccountId.get(accId);

			if (leadList == null) {
				leadList = new List<Lead>();
				leadsByAccountId.put(accId, leadList);
			}

			leadList.add(myLead);
		}

		setAccLeadHotnessSummary(leadsByAccountId, allDataModelFields, accountsToUpdateMap);

		if (!accountsToUpdateMap.isEmpty()) {
			SharingUtils.updateObjectsWithoutSharingStatic(accountsToUpdateMap.values());
		}
	}

	private void setAccLeadHotnessSummary(
		Map<Id, List<Lead>> leadsByAccountId,
		List<Schema.FieldSetMember> allDataModelFields,
		Map<Id, Account> accountsToUpdateMap
	) {
		//get the values that we are checking from the custom settings
		Silverpop_Lead_Settings__c mySettings = Silverpop_Lead_Settings__c.getInstance();

		// for each account, go through it's leads and see if there is any lead that has any of the
		// scoring fields with value that is needed to trigger the account hotness (as per custom settings)
		for (Id accId : leadsByAccountId.keySet()) {
			List<Lead> leadsForAccount = leadsByAccountId.get(accId);

			String hotnessLabel = '';

			for (Lead myLead : leadsForAccount) {
				for (FieldSetMember membr : allDataModelFields) {
					String foundHotness = (String) myLead.get(membr.getFieldPath());

					if (foundHotness == mySettings.SilverPop_Level_1_Lead_Hotness_Label__c) {
						hotnessLabel = mySettings.SilverPop_Level_1_Lead_Hotness_Label__c;
					} else if (foundHotness == mySettings.SilverPop_Level_2_Lead_Hotness_Label__c) {
						hotnessLabel = mySettings.SilverPop_Level_2_Lead_Hotness_Label__c;
					} else if (foundHotness == mySettings.SilverPop_Level_3_Lead_Hotness_Label__c) {
						hotnessLabel = mySettings.SilverPop_Level_3_Lead_Hotness_Label__c;
					} else if (foundHotness == mySettings.SilverPop_Level_4_Lead_Hotness_Label__c) {
						hotnessLabel = mySettings.SilverPop_Level_4_Lead_Hotness_Label__c;
					}
				}
			}

			Account acc = accountsToUpdateMap.get(accId);
			if (acc == null) {
				continue;
			}
			acc.Account_Lead_Hotness_Summary__c = hotnessLabel;
		}
	}

	private void syncAccountQueueUserWithLeadOwner() {
		Map<String, Id> kvkNumberToLeadOwner = getKvkNumberToLeadOwnerMap();
		Map<Id, List<Account>> leadOwnerToAccounts = getLeadOwnerToAccountsMap(
			kvkNumberToLeadOwner
		);
		if (!leadOwnerToAccounts.isEmpty()) {
			assingQueueUserToAccounts(leadOwnerToAccounts);
		}
	}

	private Map<String, Id> getKvkNumberToLeadOwnerMap() {
		Map<String, Id> result = new Map<String, Id>();

		Id csaDetailsRecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName(
			'Lead',
			'CSADetails'
		);

		for (Lead ld : newLeadMap.values()) {
			if (
				csaDetailsRecordTypeId == ld.RecordTypeId &&
				ld.KVK_Number__c != null &&
				ld.OwnerId != null &&
				(oldLeadMap == null ||
				(oldLeadMap != null &&
				oldLeadMap.containsKey(ld.Id) &&
				oldLeadMap.get(ld.Id).OwnerId != ld.OwnerId))
			) {
				result.put(ld.KVK_Number__c, ld.OwnerId);
			}
		}
		return result;
	}

	private Map<Id, List<Account>> getLeadOwnerToAccountsMap(Map<String, Id> kvkNumberToLeadOwner) {
		Map<Id, List<Account>> result = new Map<Id, List<Account>>();

		if (kvkNumberToLeadOwner.isEmpty()) {
			return result;
		}

		String query =
			'SELECT Id, KVK_Number__c, Queue_User_Assigned__c, Queue_User_Assigned__r.OwnerId ' +
			'FROM Account WHERE KVK_Number__c IN ' +
			GeneralUtils.getEscapedSOQLList(kvkNumberToLeadOwner.keySet());

		List<Account> accs = (List<Account>) SharingUtils.queryRecordsWithoutSharing(query);

		for (Account acc : accs) {
			Id leadOwnerId = kvkNumberToLeadOwner.containsKey(acc.KVK_Number__c)
				? kvkNumberToLeadOwner.get(acc.KVK_Number__c)
				: null;

			if (leadOwnerId != null) {
				if (result.containsKey(leadOwnerId)) {
					result.get(leadOwnerId).add(acc);
				} else {
					result.put(leadOwnerId, new List<Account>{ acc });
				}
			}
		}

		return result;
	}

	private void populateQueueUserOnLeadConversion() {
		Map<Id, Id> accountIdToLeadOwner = new Map<Id, Id>();

		Id csaDetailsRecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName(
			'Lead',
			'CSADetails'
		);

		for (Lead l : newLeadMap.values()) {
			if (
				csaDetailsRecordTypeId == l.RecordTypeId &&
				l.IsConverted &&
				oldLeadMap.containsKey(l.Id) &&
				!oldLeadMap.get(l.Id).IsConverted
			) {
				accountIdToLeadOwner.put(l.ConvertedAccountId, l.OwnerId);
			}
		}

		Map<Id, List<Account>> leadOwnerToAccounts = getLeadOwnerToAccountsMap(
			accountIdToLeadOwner
		);
		if (!leadOwnerToAccounts.isEmpty()) {
			assingQueueUserToAccounts(leadOwnerToAccounts);
		}
	}

	private Map<Id, List<Account>> getLeadOwnerToAccountsMap(Map<Id, Id> accountIdToLeadOwner) {
		Map<Id, List<Account>> result = new Map<Id, List<Account>>();

		if (accountIdToLeadOwner.isEmpty()) {
			return result;
		}

		String query =
			'SELECT Id, Queue_User_Assigned__c, Queue_User_Assigned__r.OwnerId ' +
			'FROM Account WHERE Id IN ' +
			GeneralUtils.getEscapedSOQLList(accountIdToLeadOwner.keySet());

		List<Account> accs = (List<Account>) SharingUtils.queryRecordsWithoutSharing(query);

		for (Account acc : accs) {
			Id accLeadOwnerId = accountIdToLeadOwner.containsKey(acc.Id)
				? accountIdToLeadOwner.get(acc.Id)
				: null;

			if (
				accLeadOwnerId != null &&
				(acc.Queue_User_Assigned__c == null ||
				accLeadOwnerId != acc.Queue_User_Assigned__r.OwnerId)
			) {
				if (result.containsKey(accountIdToLeadOwner.get(acc.Id))) {
					result.get(accLeadOwnerId).add(acc);
				} else {
					result.put(accLeadOwnerId, new List<Account>{ acc });
				}
			}
		}

		return result;
	}

	private void assingQueueUserToAccounts(Map<Id, List<Account>> ownerToAccountsMap) {
		Map<Id, Id> mapQueueUserByOwners = new Map<Id, Id>();

		String query =
			'SELECT Id, OwnerId FROM Queue_User_Sharing__c ' +
			'WHERE OwnerId IN ' +
			GeneralUtils.getEscapedSOQLList(ownerToAccountsMap.keySet());

		List<Queue_User_Sharing__c> qus = (List<Queue_User_Sharing__c>) SharingUtils.queryRecordsWithoutSharing(
			query
		);

		for (Queue_User_Sharing__c qu : qus) {
			mapQueueUserByOwners.put(qu.OwnerId, qu.Id);
		}

		List<Queue_User_Sharing__c> lstQueueUserToInsert = new List<Queue_User_Sharing__c>();

		for (Id leadOwnerId : ownerToAccountsMap.keySet()) {
			if (!mapQueueUserByOwners.containsKey(leadOwnerId)) {
				lstQueueUserToInsert.add(new Queue_User_Sharing__c(OwnerId = leadOwnerId));
			}
		}

		if (!lstQueueUserToInsert.isEmpty()) {
			SharingUtils.insertRecordsWithoutSharing(lstQueueUserToInsert);
		}

		for (Queue_User_Sharing__c qu : lstQueueUserToInsert) {
			mapQueueUserByOwners.put(qu.OwnerId, qu.Id);
		}

		List<Account> listAccountToUpdate = filterOutAccs(ownerToAccountsMap, mapQueueUserByOwners);

		if (!listAccountToUpdate.isEmpty()) {
			SharingUtils.updateRecordsWithoutSharing(listAccountToUpdate);
		}
	}

	private List<Account> filterOutAccs(
		Map<Id, List<Account>> ownerToAccountsMap,
		Map<Id, Id> mapQueueUserByOwners
	) {
		List<Account> listAccountToUpdate = new List<Account>();
		for (Id ownerLeadId : ownerToAccountsMap.keySet()) {
			for (Account acc : ownerToAccountsMap.get(ownerLeadId)) {
				if (mapQueueUserByOwners.containsKey(ownerLeadId)) {
					acc.Queue_User_Assigned__c = mapQueueUserByOwners.get(ownerLeadId);
					listAccountToUpdate.add(acc);
				}
			}
		}
		return listAccountToUpdate;
	}
}