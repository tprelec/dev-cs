global with sharing class MLEPriceAggregatorLookupImpl extends cscfga.ALookupSearch {

    private static final String LINKED_ID_FIELD = 'MLE Linked Id';
    private static final String DEFINITION_ID_FIELD = 'MLE Product Definition Id';
    private static final String REFRESH_INDICATIOR_FIELD = 'MLE Refresh Aggregate Indicator';

    global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
        String basketId = searchFields.get(LINKED_ID_FIELD);
        Decimal totalPrice = 0;
        Integer numOfConfigs = 0;

        if (!String.isBlank(basketId)) {
            String definitionId = searchFields.get(DEFINITION_ID_FIELD);
            List<AggregateResult> result = [
                select
                    sum(cscfga__total_price__c) total,
                    count(id) numOfConfigs
                from cscfga__Product_Configuration__c
                where cscfga__product_basket__c = :basketId
                    and cscfga__configuration_offer__c = null
                    and cscfga__root_configuration__c = null
                    and cscfga__product_definition__c = :definitionId
            ];

            if (!result.isEmpty() && result[0].get('total') != null) {
                totalPrice = (Decimal) result[0].get('total');
                numOfConfigs = (Integer) result[0].get('numOfConfigs');
            }
        }

        return new List<cscfga__Product_Configuration__c> {
            new cscfga__Product_Configuration__c(
                cscfga__total_price__c = totalPrice,
                cscfga__quantity__c = numOfConfigs
            )
        };
    }

    global override String getRequiredAttributes() {
        // we need refresh indicator in order to properly calculate value
        return '["' + LINKED_ID_FIELD + '","' + REFRESH_INDICATIOR_FIELD +  + '","' + DEFINITION_ID_FIELD + '"]';
    }
}