public with sharing class MassLeadTreatmentSelectionController {

	private final Lead lead;
	public Boolean treatmentIsEditable{get;private set;}
	public Boolean treatmentChanged {get;set;}
	public String error {get;set;}
	public Unica_Campaign__c fakeUC {get;set;}
	public List<Lead> filteredLeads {get;set;}
	public List<LeadWrapper> filteredLeadsW {get;set;}

	public MassLeadTreatmentSelectionController(ApexPages.StandardController stdController) {

		fakeUC = new Unica_Campaign__c();
		filteredLeads = new List<Lead>();
		filteredLeadsW = new List<LeadWrapper>();

		Id LeadId = Apexpages.currentpage().getparameters().get('LeadId');
		system.debug('************* LeadId:'+ LeadId); 

		if(LeadId != null){

			fakeUC.Lead__C = LeadId;
			
			this.lead = [select Id,
								Name, 
								LeadSource, 
								Status, 
								NumberOfEmployees, 
								Company,
								Treatment_01__c, 
								Treatment_02__c, 
								Treatment_03__c, 
								Treatment_04__c, 
								Treatment_05__c, 
								Treatment_06__c, 
								Treatment_07__c,
								Treatment_08__c,
								Chosen_Treatment__c,
								Createddate,
								Campaign__c,
								Sel_Deadline_Review__c
							from Lead
							where Id = : leadId];
			
			refreshFilters();
		}

		treatmentChanged = false;
		error = null;
		if(treatments.size() == 1){
			treatmentIsEditable = false;
		} else {
			treatmentIsEditable = true;
		}
		if(lead != null && lead.Chosen_Treatment__c != null) selectedTreatment = lead.Chosen_Treatment__c;
	}

	public String selectedTreatment {get;set;}
	public String selectedStatus {get;set;}

	public List<SelectOption> treatments {
		get{
			return LeadTreatMentHelper.GetTreatmentSelectOptions(lead);
		}
		private set;
	}

	public List<SelectOption> statuses {
		get{
			return LeadTreatMentHelper.GetStatusSelectOptions();
		}
		private set;
	}

	public pageReference changeTreatment(){
		
		treatmentChanged = true;
		refreshFilters();
		treatmentChanged = true;
		return null;
	}

	public PageReference updateTreatments(){
		
		Savepoint sp = Database.setSavepoint();

		for(LeadWrapper oneLeadW : filteredLeadsW){

			if(oneLeadW.selected){
				oneLeadW.Lead.Chosen_Treatment__c = selectedTreatment;	
			}
			
		}

		try{
			update filteredLeads;
			treatmentChanged = false;
		} catch (Exception e){
			error = 'You can\'t change the treatment after the review period has ended';
			Database.rollback(sp);
		}
		
		return null;
	}

	public PageReference updateStatuses(){
		
		Savepoint sp = Database.setSavepoint();

		for(LeadWrapper oneLeadW : filteredLeadsW){

			if(oneLeadW.selected){
				oneLeadW.Lead.Status = selectedStatus;	
			}
			
		}

		try{
			update filteredLeads;
		} catch (Exception e){
			error = 'You can\'t change the treatment after the review period has ended';
			Database.rollback(sp);
		}
		
		return null;
	}

	public void getSelected()
    {
        filteredLeads.clear();

        for(LeadWrapper leadW : filteredLeadsW){

        	if(leadW.selected == true){
        		filteredLeads.add(leadW.lead);		
        	}
        }

    }

    public pagereference reloadWithNewParameter() { 
    	String newPageUrl = '/apex/MassLeadTreatmentSelection?LeadId='+ fakeUC.Lead__C;
		PageReference newPage = new PageReference(newPageUrl);
		newPage.setRedirect(true);
		return newPage;
	}

    public void refreshFilters(){

    if(lead != null){
	    	List<String> treatmentsToFilter = LeadTreatmentHelper.GetTreatmentsString(lead, selectedTreatment);
	    	//takes the selected lead and filters the lead list to ever
	    	filteredLeads = [select Id,
									Name, 
									LeadSource, 
									Status, 
									NumberOfEmployees, 
									Company,
									Treatment_01__c, 
									Treatment_02__c, 
									Treatment_03__c, 
									Treatment_04__c, 
									Treatment_05__c, 
									Treatment_06__c, 
									Treatment_07__c,
									Treatment_08__c,
									Chosen_Treatment__c,
									Createddate,
									Campaign__c,
									Sel_Deadline_Review__c
							from Lead
							where 	(	Sel_Deadline_Review__c >= TODAY or
										Sel_Deadline_Review__c = null) and
									Chosen_Treatment__c = null and
									(	Treatment_01__c in :treatmentsToFilter
										or Treatment_01__c in :treatmentsToFilter
										or Treatment_02__c in :treatmentsToFilter
										or Treatment_03__c in :treatmentsToFilter
										or Treatment_04__c in :treatmentsToFilter
										or Treatment_05__c in :treatmentsToFilter
										or Treatment_06__c in :treatmentsToFilter
										or Treatment_07__c in :treatmentsToFilter
										or Treatment_08__c in :treatmentsToFilter
										)
							order by Name
							];

			filteredLeadsW = wrapLeads(filteredLeads);
		} 
    }


        
   public static List<LeadWrapper> wrapLeads(List<Lead> leadList){
    	
    	List<LeadWrapper> wrappedLeads = new List<LeadWrapper>();
    	
    	for(Lead lead : leadList){
    		wrappedLeads.add(new LeadWrapper(lead));
    	}

    	return wrappedLeads;
    }

    public class LeadWrapper
    {
        public Lead lead{get; set;}
        public Boolean selected {get; set;}
        public LeadWrapper(Lead l)
        {
            lead = l;
            selected = false;
        }

    }
}