public with sharing class MavenDocumentsService {
	public static mmdoc__Document_Template__c getDocumentTemplate(String docTemplateName) {
		mmdoc__Document_Template__c docTemplate = [
			SELECT Id, mmdoc__Document_Solution__c
			FROM mmdoc__Document_Template__c
			WHERE Name = :docTemplateName
		];
		return docTemplate;
	}

	public static mmdoc__Document_Request__c createDocRequest(
		Id recordId,
		mmdoc__Document_Template__c template,
		String saveAs,
		String format,
		String processingType,
		Boolean doInsert
	) {
		mmdoc__Document_Request__c docRequest = new mmdoc__Document_Request__c(
			mmdoc__Document_Solution__c = template.mmdoc__Document_Solution__c,
			mmdoc__Document_Template__c = template.Id,
			mmdoc__Status__c = 'New',
			mmdoc__Record_Id__c = recordId,
			mmdoc__Document_Format__c = format,
			mmdoc__Save_As__c = saveAs,
			mmdoc__Processing_Type__c = processingType
		);
		if (doInsert) {
			insert docRequest;
		}
		return docRequest;
	}

	public static mmdoc__Document_Request__c getDocumentRequest(Id docRequestId) {
		mmdoc__Document_Request__c docRequest = [
			SELECT Id, mmdoc__Status__c, mmdoc__File_Id__c
			FROM mmdoc__Document_Request__c
			WHERE Id = :docRequestId
		];
		return docRequest;
	}
}