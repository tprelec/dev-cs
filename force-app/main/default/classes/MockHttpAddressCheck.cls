@isTest
public class MockHttpAddressCheck implements HttpCalloutMock {
	public HTTPResponse respond(HTTPRequest req) {
		System.assertEquals(
			'callout:AddressCheck/zipcode/1111XX/housenumber/11',
			req.getEndpoint()
		);
		System.assertEquals('GET', req.getMethod());

		// Create a fake response
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody('{"example":"test"}');
		res.setStatusCode(200);
		return res;
	}
}