@isTest
global class MockResponseGeneratorforConnectionsOE {
    
    Public Id pcId ;
    
    public MockResponseGeneratorforConnectionsOE(Id pcid){
        this.pcID = pcid;
    }
    
    global Map<Id,List<cssmgnt.ProductProcessingUtility.Component>> createConnectionsTestOE() {
        Map<Id,List<cssmgnt.ProductProcessingUtility.Component>> oeMap = new Map<Id,List<cssmgnt.ProductProcessingUtility.Component>>();
        List<cssmgnt.ProductProcessingUtility.Component> complist = new List<cssmgnt.ProductProcessingUtility.Component>();
        cssmgnt.ProductProcessingUtility.Component comp = new cssmgnt.ProductProcessingUtility.Component();
        List<cssmgnt.ProductProcessingUtility.Configuration> configlist =  new List<cssmgnt.ProductProcessingUtility.Configuration>();
        
        Map<String,String> attrmap = new Map<String,String>();
        attrmap.put('ConnectionType','New');
        attrmap.put('Quantity','1');
        attrmap.put('guid','1234');
        
        List<cssmgnt.ProductProcessingUtility.Attribute> attrlst = new List<cssmgnt.ProductProcessingUtility.Attribute>();
        
        for(String attrname : attrmap.keySet()){
            cssmgnt.ProductProcessingUtility.Attribute attr = new cssmgnt.ProductProcessingUtility.Attribute();
            
            attr.name = attrname;
            attr.value = attrmap.get(attrname);
            attr.displayValue = attrmap.get(attrname);
            attrlst.add(attr);
        }
        
        cssmgnt.ProductProcessingUtility.Configuration config = new cssmgnt.ProductProcessingUtility.Configuration();
        config.attributes = attrlst;
        config.guid = '1234';
        config.ConfigurationName = 'test 0';
        configlist.add(config);
        
        Map<String,String> attrmap2 = new Map<String,String>();
        attrmap2.put('ConnectionType','Porting');
        attrmap2.put('Quantity','1');
        attrmap2.put('guid','12345');
        
        for(String attrname : attrmap2.keySet()){
            cssmgnt.ProductProcessingUtility.Attribute attr1 = new cssmgnt.ProductProcessingUtility.Attribute();
            
            attr1.name = attrname;
            attr1.value = attrmap2.get(attrname);
            attr1.displayValue = attrmap2.get(attrname);
            attrlst.add(attr1);
        }
        
        cssmgnt.ProductProcessingUtility.Configuration config1 = new cssmgnt.ProductProcessingUtility.Configuration();
        config1.attributes = attrlst;
        config1.guid = '7890';
        config1.ConfigurationName = 'Second';
        configlist.add(config1);
        
        comp.configurations = configlist;
        comp.id = null; //Association ID
        oeMap.put(this.pcId, new List<cssmgnt.ProductProcessingUtility.Component>{comp}); //pc id
        
        return oeMap;
    }
}