global with sharing class MyObserver implements csbb.ObserverApi.IObserver {
    global MyObserver () {}
    global void execute (csbb.ObserverApi.Observable o, Object arg){
    System.debug('AN---In observer-start');
        csbb.ProductConfigurationObservable observable = (csbb.ProductConfigurationObservable)o;
        String context = observable.getContext();
        csbb__Product_Configuration_Request__c pcr = observable.getProductConfigurationRequest();
        cscfga__Product_Configuration__c pc = observable.getProductConfiguration();
        System.debug(context);System.debug(pcr.id);
        System.debug(pc.id);
         System.debug('AN---In observer-end');
}
}