public without sharing class NetProfitCTNGenerator {
	//The test coverage from this class comes from TestCreateMobileInformationController

	private static final Set<String> CTN_VOICE_PRODUCT_FAMILIES = new Set<String>{
		'Acq Voice',
		'Ret Voice',
		'Mig Voice',
		'Voice',
		'HHBD',
		'Acq HHBD',
		'Ret HHBD'
	};

	private static final Set<String> CTN_DATA_PRODUCT_FAMILIES = new Set<String>{
		'MBB',
		'Acq MBB',
		'Mig MBB',
		'Ret MBB'
	};

	private static final String PRODUCT_GROUP_PRICEPLAN = 'Priceplan';
	private static final String PRODUCT_GROUP_ADDONS = 'Addons';
	private static final String PRODUCT_GROUP_CONNECTIONCOST = 'ConnectionCosts';

	private static final Set<String> ALLOWED_PRODUCT_GROUPS = new Set<String>{
		PRODUCT_GROUP_ADDONS,
		PRODUCT_GROUP_PRICEPLAN,
		PRODUCT_GROUP_CONNECTIONCOST
	};

	private static final Set<String> ALLOWED_AANSLUITKOSTEN_PRICE_PLANS = new Set<String>{
		'Aansluitkosten'
	};

	private static final Set<String> ALLOWED_ADDON_CONTRACTED_PRODUCTS = new Set<String>{
		'COMPANYLEVELSERVICES'
	};

	private static final Map<String, String> DEAL_TYPE_TO_CTN_ACTION = new Map<String, String>{
		'New' => 'New',
		'Retention' => 'Retention',
		'Porting' => 'Porting',
		'New and Porting' => 'Migration',
		'Migration' => 'Migration',
		'Acquisition' => 'New'
	};

	private static final Map<String, String> DEAL_TYPE_TO_CTN_STATUS = new Map<String, String>{
		'New' => 'Future',
		'Retention' => 'Future',
		'Porting' => 'Future',
		'New and Porting' => 'Future',
		'Migration' => 'Active',
		'Acquisition' => 'Future'
	};

	private static final String CONNECTION_TYPE_DATA = 'Data';
	private static final String CONNECTION_TYPE_VOICE = 'Voice';
	private static final String CONNECTION_TYPE_VOICE_DATA = 'Voice/Data';
    private Map<Id, List<OpportunityLineItem>> lineItemsByOpportunityId;

	private static Integer voiceNumberCounter = 0;
	private static Integer dataNumberCounter = 0;
	public List<OpportunityLineItem> lineItems;
	public Id npInfoId { get; set; }

	public NetProfitCTNGenerator(String oppId) {
		this.lineItems = [
			SELECT
				Id,
				Duration__c,
				PricebookEntry.Product2.BAP_SAP__c,
				vMAFDiscountExtraPromo__c,
				Allowed_SAC_Per_CTN__c,
				PricebookEntry.Product2.Product_Group__c,
				PricebookEntry.Product2.Quantity_type__c,
				DiscountNew__c,
				PricebookEntry.Product2.Family,
				Model_Number__c,
				Deal_Type__c,
				Description,
				Quantity,
				Gross_List_Price__c,
				PricebookEntry.Product2.Name,
				PricebookEntry.Product2.Gemini_Code__c,
				Proposition_Type__c,
				OpportunityId,
				Price_Plan_Class__c
			FROM OpportunityLineItem
			WHERE OpportunityId = :oppId AND Fixed_Mobile__c = 'Mobile'
		];
		lineItemsByOpportunityId = GeneralUtils.groupByIDField(lineItems, 'OpportunityId');
	}

	private Map<Id, Map<Decimal, Map<String, Map<String, Map<String, List<OpportunityLineItem>>>>>> groupOppLineItems() {
		Map<Id, Map<Decimal, Map<String, Map<String, Map<String, List<OpportunityLineItem>>>>>> lineItemsByOpportunity = new Map<Id, Map<Decimal, Map<String, Map<String, Map<String, List<OpportunityLineItem>>>>>>();
		for (OpportunityLineItem lineItem : lineItems) {
			// Contracted Product Group must be in the allowed set
			if (
				!ALLOWED_PRODUCT_GROUPS.contains(lineItem.PricebookEntry.Product2.Product_Group__c)
			) {
				continue;
			}
			// Get Product Group & Connection Type
			String productGroup = getProductGroup(lineItem);
			String connectionType = getConnectionType(lineItem);

			if (!lineItemsByOpportunity.containsKey(lineItem.OpportunityId)) {
				lineItemsByOpportunity.put(
					lineItem.OpportunityId,
					new Map<Decimal, Map<String, Map<String, Map<String, List<OpportunityLineItem>>>>>()
				);
			}

            Map<Decimal, Map<String, Map<String, Map<String, List<OpportunityLineItem>>>>> lineItemsByModelNumber = lineItemsByOpportunity.get(
				lineItem.OpportunityId
			);
			if (!lineItemsByModelNumber.containsKey(lineItem.Model_Number__c)) {
				lineItemsByModelNumber.put(
					lineItem.Model_Number__c,
					new Map<String, Map<String, Map<String, List<OpportunityLineItem>>>>()
				);
			}

			// Contracted Products by Connection Type
			Map<String, Map<String, Map<String, List<OpportunityLineItem>>>> lineItemsByConnectionType = lineItemsByModelNumber.get(
				lineItem.Model_Number__c
			);
			if (!lineItemsByConnectionType.containsKey(connectionType)) {
				lineItemsByConnectionType.put(
					connectionType,
					new Map<String, Map<String, List<OpportunityLineItem>>>()
				);
			}
			// Contracted Products by Deal Type
			Map<String, Map<String, List<OpportunityLineItem>>> lineItemsByDealType = lineItemsByConnectionType.get(
				connectionType
			);
			if (!lineItemsByDealType.containsKey(lineItem.Deal_Type__c)) {
				lineItemsByDealType.put(
					lineItem.Deal_Type__c,
					new Map<String, List<OpportunityLineItem>>()
				);
			}

			// Contracted Products by Product Group
			Map<String, List<OpportunityLineItem>> lineItemsByProductGroup = lineItemsByDealType.get(
				lineItem.Deal_Type__c
			);
			if (!lineItemsByProductGroup.containsKey(productGroup)) {
				lineItemsByProductGroup.put(productGroup, new List<OpportunityLineItem>());
			}
			lineItemsByProductGroup.get(productGroup).add(lineItem);
		}
		return lineItemsByOpportunity;
	}

	public List<NetProfit_CTN__c> generateCTNInfo() {
		//buildNPIsByOrderMap();
		Map<Id, Map<Decimal, Map<String, Map<String, Map<String, List<OpportunityLineItem>>>>>> lineItempsByOpportunity = groupOppLineItems();
		// Go through all groups in the map, creating ctn lineitems and assigning ctn numbers

		List<NetProfit_CTN__c> ctnsToInsert = new List<NetProfit_CTN__c>();
        for (Id oppId : lineItemsByOpportunityId.keySet()) {
            if (oppId != null) {
                Map<Decimal, Map<String, Map<String, Map<String, List<OpportunityLineItem>>>>> lineItemsByModelNumber = lineItempsByOpportunity.get(oppId);
                for (Decimal modelNumber : lineItemsByModelNumber.keySet()) {
                    Map<String, Map<String, Map<String, List<OpportunityLineItem>>>> lineItemsByConnectionType = lineItemsByModelNumber.get(
                        modelNumber
                    );
                    for (String connectionType : lineItemsByConnectionType.keySet()) {
                        Map<String, Map<String, List<OpportunityLineItem>>> lineItemsByDealType = lineItemsByConnectionType.get(
                            connectionType
                        );
                        for (String dealType : lineItemsByDealType.keySet()) {
                            Map<String, List<OpportunityLineItem>> lineItemsByProductGroup = lineItemsByDealType.get(
                                dealType
                            );

                            Boolean companyLevelServicesFound = checkCompanyLevelServices(
                                lineItemsByProductGroup
                            );

                            Integer quantity = getQuantity(
                                lineItemsByProductGroup,
                                companyLevelServicesFound
                            );

                            for (Integer i = 0; i < quantity; i++) {
                                // Assign a dummy CTN phone number to each item in the new set of CTNs. Each item should get the same number.
                                String dummyCtnNumber = getDummyCtnNumber(connectionType, modelNumber);
                                for (String productGroup : lineItemsByProductGroup.keySet()) {
                                    // Each group (exccept addons) should have only 1 entry.
                                    for (
                                        OpportunityLineItem lineItem : lineItemsByProductGroup.get(
                                            productGroup
                                        )
                                    ) {
                                        if (
                                            ALLOWED_ADDON_CONTRACTED_PRODUCTS.contains(
                                                lineItem.Proposition_Type__c
                                            )
                                        ) {
                                            dummyCtnNumber = '';
                                        }
                                        // If no errors, create a new CTN Information record for each item in the group.
                                        ctnsToInsert.add(newCTN(lineItem, dummyCtnNumber, i));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
		return ctnsToInsert;
	}

	private NetProfit_CTN__c newCTN(
		OpportunityLineItem lineItem,
		String dummyCTNNumber,
		Integer i
	) {
		NetProfit_CTN__c netProfitCtn = new NetProfit_CTN__c();
		netProfitCtn.NetProfit_Information__c = npInfoId;
		netProfitCtn.OpportunityLineItem__c = lineItem.Id;
		netProfitCtn.Duration__c = lineItem.Duration__c;
		netProfitCtn.MAF__c = lineItem.Gross_List_Price__c;
		netProfitCtn.Discount__c = lineItem.DiscountNew__c;
		netProfitCtn.Commission_Total__c = lineItem.Allowed_SAC_Per_CTN__c;
		netProfitCtn.Quote_Profile__c = String.valueOf(lineItem.Model_Number__c);
		netProfitCtn.Product_Group__c = lineItem.PricebookEntry.Product2.Product_Group__c;
		netProfitCtn.Product_Quantity_type__c = lineItem.PricebookEntry.Product2.Quantity_type__c;
		netProfitCtn.CTN_Status__c = DEAL_TYPE_TO_CTN_STATUS.get(lineItem.Deal_Type__c);
		netProfitCtn.Action__c = DEAL_TYPE_TO_CTN_ACTION.get(lineItem.Deal_Type__c);
		netProfitCtn.Price_Plan_Class__c = getConnectionTypeBasedOnProduct(lineItem);
		netProfitCtn.CTN_Number__c = dummyCTNNumber;
		netProfitCtn.Price_Plan_Description__c = lineItem.Description == null
			? lineItem.PricebookEntry.Product2.Name
			: lineItem.Description;
		netProfitCtn.Price_Plan_Code__c = lineItem.PricebookEntry.Product2.Gemini_Code__c;
		netProfitCtn.Generated_Counter__c = String.valueOf(i);
		return netProfitCtn;
	}

	private Integer getQuantity(
		Map<String, List<OpportunityLineItem>> lineItemsByProductGroup,
		Boolean companyLevelServicesFound
	) {
		Integer quantity = 1;
		if (
			!companyLevelServicesFound &&
			lineItemsByProductGroup.containsKey(PRODUCT_GROUP_PRICEPLAN)
		) {
			quantity = (Integer) lineItemsByProductGroup.get(PRODUCT_GROUP_PRICEPLAN)[0].Quantity;
		}
		return quantity;
	}

	private Boolean checkCompanyLevelServices(
		Map<String, List<OpportunityLineItem>> lineItemsByProductGroup
	) {
		Boolean companyLevelServicesFound = false;
		for (String productGroup : lineItemsByProductGroup.keySet()) {
			List<OpportunityLineItem> tmpList = lineItemsByProductGroup.get(productGroup);
			for (OpportunityLineItem lineItem : tmpList) {
				if (ALLOWED_ADDON_CONTRACTED_PRODUCTS.contains(lineItem.Proposition_Type__c)) {
					companyLevelServicesFound = true;
				}
			}
		}
		return companyLevelServicesFound;
	}

	private String getProductGroup(OpportunityLineItem lineItem) {
		String productGroup = lineItem.PricebookEntry.Product2.Product_Group__c;
		// Update the connection cost productgroup (only in-memory)
		// We are not updating the CP data but the product2 data
		for (String aansluitkostenPriceplan : ALLOWED_AANSLUITKOSTEN_PRICE_PLANS) {
			if (lineItem.PricebookEntry.Product2.Name.contains(aansluitkostenPriceplan)) {
				productGroup = PRODUCT_GROUP_CONNECTIONCOST;
				lineItem.PricebookEntry.Product2.Product_Group__c = PRODUCT_GROUP_CONNECTIONCOST;
			}
		}
		return productGroup;
	}

	private String getConnectionTypeBasedOnProduct(OpportunityLineItem lineItem) {
		String connType;
		if (isVoiceProduct(lineItem)) {
			connType = CONNECTION_TYPE_VOICE;
		} else if (isDataProduct(lineItem)) {
			connType = CONNECTION_TYPE_DATA;
		}
		return connType;
	}

	private String getConnectionType(OpportunityLineItem lineItem) {
		String connType;
		if (lineItem.Price_Plan_Class__c == CONNECTION_TYPE_VOICE_DATA) {
			for (OpportunityLineItem allLineItem : lineItemsByOpportunityId.get(lineItem.OpportunityId)) {
				if (
					allLineItem.Model_Number__c == lineItem.Model_Number__c &&
					allLineItem.PricebookEntry.Product2.Product_Group__c == PRODUCT_GROUP_PRICEPLAN
				) {
					connType = getConnectionTypeBasedOnProduct(allLineItem);
					if (connType != null) {
						break;
					}
				}
			}
		} else {
			connType = getConnectionTypeBasedOnProduct(lineItem);
		}
		return connType;
	}

	/**
	 * Checks if Contracted Product is Void Product
	 * @param  conProd Contracted Product
	 * @return         True if it is Voice Product, false otherwise
	 */
	private Boolean isVoiceProduct(OpportunityLineItem lineItem) {
		return lineItem != null &&
			CTN_VOICE_PRODUCT_FAMILIES.contains(lineItem.PricebookEntry.Product2.Family);
	}

	/**
	 * Checks if Contracted Product is Data Product
	 * @param  conProd Contracted Product
	 * @return         True if it is Data Product, false otherwise
	 */
	private Boolean isDataProduct(OpportunityLineItem lineItem) {
		return lineItem != null &&
			CTN_DATA_PRODUCT_FAMILIES.contains(lineItem.PricebookEntry.Product2.Family);
	}

	private String getDummyCtnNumber(String connectionType, Decimal modelNumber) {
		String dummyCtnNumber = '';
		if (connectionType == CONNECTION_TYPE_VOICE) {
			voiceNumberCounter += 1;
			dummyCtnNumber =
				'316XXX' +
				modelNumber +
				String.ValueOf(voiceNumberCounter).leftPad(4, '0');
		} else if (connectionType == CONNECTION_TYPE_DATA) {
			dataNumberCounter += 1;
			dummyCtnNumber =
				'3197XX' +
				modelNumber +
				String.ValueOf(dataNumberCounter).leftPad(4, '0');
		}
		return dummyCtnNumber;
	}
}