public with sharing class NetProfitCTNTriggerHandler extends TriggerHandler {
	private static final Set<String> CTN_ACTIONS = new Set<String>{ 'New', 'Porting', 'Retention' };

	List<NetProfit_CTN__c> newNPCs;
	List<NetProfit_CTN__c> oldNPCs;
	Map<Id, NetProfit_CTN__c> oldNPCsMap;

	private void init() {
		newNPCs = (List<NetProfit_CTN__c>) this.newList;
		oldNPCs = (List<NetProfit_CTN__c>) this.oldList;
		oldNPCsMap = (Map<Id, NetProfit_CTN__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		matchRetentionCtnsAndAssets();
	}

	public override void afterInsert() {
		init();
		updateSummaryFieldsOnCTNInformation(newNPCs);
		updateSummaryFieldsOnOrder();
	}

	public override void beforeUpdate() {
		init();
		matchRetentionCtnsAndAssets();
	}

	public override void afterUpdate() {
		init();
		updateSummaryFieldsOnCTNInformation(newNPCs);
		updateSummaryFieldsOnOrder();
		updateSummaryFieldOnOpportunity();
	}

	public override void beforeDelete() {
		init();
		updateSummaryFieldsOnCTNInformation(oldNPCs);
		updateSummaryFieldsOnOrder();
	}

	private void updateSummaryFieldsOnOrder() {
		Set<Id> orderIds = new Set<Id>();
		List<NetProfit_CTN__c> npcs = newNPCs == null ? oldNPCs : newNPCs;
		Map<Id, NetProfit_CTN__c> oldMap = newNPCs == null ? null : oldNPCsMap;
		for (NetProfit_CTN__c npc : npcs) {
			if (
				CTN_ACTIONS.contains(npc.Action__c) &&
				!String.isBlank(npc.Order__c) &&
				(GeneralUtils.isRecordFieldChanged(npc, oldMap, 'CTN_Status__c') ||
				GeneralUtils.isRecordFieldChanged(npc, oldMap, 'Order__c'))
			) {
				orderIds.add(npc.Order__c);
			}
		}
		List<Order__c> ordersToUpdate = [
			SELECT
				Id,
				(
					SELECT Id, CTN_Status__c, Action__c, Error_on_Connect_GLP__c
					FROM Mobile_Order_CTNs__r
					WHERE Action__c IN :CTN_ACTIONS AND Product_Group__c = 'Priceplan'
				)
			FROM Order__c
			WHERE Id IN :orderIds
		];

		for (Order__c tempOrder : ordersToUpdate) {
			List<NetProfit_CTN__c> ctns = tempOrder.Mobile_Order_CTNs__r;

			tempOrder.action_New_CTNs__c = countCTNs(ctns, 'Action__c', new Set<String>{ 'New' });
			tempOrder.action_Porting_CTNs__c = countCTNs(
				ctns,
				'Action__c',
				new Set<String>{ 'Porting' }
			);
			tempOrder.action_Retention_CTNs__c = countCTNs(
				ctns,
				'Action__c',
				new Set<String>{ 'Retention' }
			);
			tempOrder.new_CTNs_with_Future_Status__c = countCTNs(
				ctns,
				'CTN_Status__c',
				new Set<String>{ 'Future' }
			);
			tempOrder.new_CTNs_To_Order__c = countCTNs(
				ctns,
				'CTN_Status__c',
				new Set<String>{ 'MSISDN Assigned' }
			);
			tempOrder.new_CTNs_with_activated_SIM__c = countCTNs(
				ctns,
				'CTN_Status__c',
				new Set<String>{ 'SIM Activation Order ID received' }
			);
			tempOrder.CTNs_with_executed_Retention__c = countCTNs(
				ctns,
				'CTN_Status__c',
				new Set<String>{ 'Retention Order ID Received' }
			);
			tempOrder.total_CTNs_with_Order_Summary_Pending__c = countCTNs(
				ctns,
				'CTN_Status__c',
				new Set<String>{ 'Order Summary pending' }
			);
			tempOrder.new_CTNs_with_completed_Order__c = countCTNs(
				ctns,
				'CTN_Status__c',
				new Set<String>{ 'Order Summary closed' }
			);
			tempOrder.new_CTNs_with_Updated_Settings__c = countCTNs(
				ctns,
				'CTN_Status__c',
				new Set<String>{ 'Update Product Settings confirmed' }
			);
			tempOrder.new_CTNs_with_Failed_Status__c = countCTNs(
				ctns,
				'CTN_Status__c',
				new Set<String>{
					'SIM Activation failed',
					'Update Product Settings failed',
					'Order Summary failed',
					'Retention Failed'
				}
			);
			tempOrder.CTNs_with_no_GLP_Connection__c = countCTNsCheckbox(
				ctns,
				'Error_on_Connect_GLP__c'
			);

			tempOrder.total_CTNs_to_process__c =
				tempOrder.action_New_CTNs__c +
				tempOrder.action_Porting_CTNs__c +
				tempOrder.action_Retention_CTNs__c;
			tempOrder.total_CTNs_processed__c =
				tempOrder.new_CTNs_with_completed_Order__c +
				tempOrder.new_CTNs_with_Failed_Status__c;
			tempOrder.total_CTNs_remaining_to_process__c =
				tempOrder.total_CTNs_to_process__c - tempOrder.total_CTNs_processed__c;
		}
		update ordersToUpdate;
	}

	private Integer countCTNs(List<NetProfit_CTN__c> ctns, String field, Set<String> values) {
		Integer result = 0;
		for (NetProfit_CTN__c ctn : ctns) {
			String fieldValue = (String) ctn.get(field);
			if (values.contains(fieldValue)) {
				result++;
			}
		}
		return result;
	}

	private Integer countCTNsCheckbox(List<NetProfit_CTN__c> ctns, String field) {
		Integer result = 0;
		for (NetProfit_CTN__c ctn : ctns) {
			Boolean fieldMarked = (Boolean) ctn.get(field);
			if (fieldMarked) {
				result++;
			}
		}
		return result;
	}

	private void updateSummaryFieldsOnCTNInformation(List<NetProfit_CTN__c> npcs) {
		// gather netprofit infos on which something change
		Set<Id> npiIds = new Set<Id>();

		for (NetProfit_CTN__c npc : npcs) {
			npiIds.add(npc.NetProfit_Information__c);
		}

		Map<Id, Map<String, Set<String>>> npiIdToProfileToDescriptions = new Map<Id, Map<String, Set<String>>>();

		AggregateResult[] ctns = [
			SELECT
				NetProfit_Information__c npiId,
				Quote_Profile__c profile,
				Price_Plan_Description__c description
			FROM NetProfit_CTN__c
			WHERE NetProfit_Information__c IN :npiIds
			GROUP BY NetProfit_Information__c, Quote_Profile__c, Price_Plan_Description__c
			ORDER BY Quote_Profile__c ASC
		];

		// recalculate for these npi's by doing an aggregate query
		for (AggregateResult ar : ctns) {
			Id npiId = (Id) ar.get('npiId');
			String profile = (String) ar.get('profile');
			String description = (String) ar.get('description');

			if (npiIdToProfileToDescriptions.containsKey(npiId)) {
				Map<String, Set<String>> profiles = npiIdToProfileToDescriptions.get(npiId);

				if (profiles.containsKey(profile)) {
					profiles.get(profile).add(description);
				} else {
					profiles.put(profile, new Set<String>{ description });
				}
			} else {
				Map<String, Set<String>> tempMap = new Map<String, Set<String>>{
					profile => new Set<String>{ description }
				};
				npiIdToProfileToDescriptions.put(npiId, tempMap);
			}
		}

		List<NetProfit_Information__c> npiToUpdate = new List<NetProfit_Information__c>();

		for (Id npiId : npiIdToProfileToDescriptions.keySet()) {
			String finalText = '';
			Map<String, Set<String>> profiles = npiIdToProfileToDescriptions.get(npiId);
			for (String profile : profiles.keySet()) {
				finalText += profile + ' - ';
				for (String description : profiles.get(profile)) {
					finalText += description + '; ';
				}
				finalText += '\n';
			}
			npiToUpdate.add(
				new NetProfit_Information__c(Id = npiId, Quote_Profiles_Details__c = finalText)
			);
		}
		update npiToUpdate;
	}

	private void matchRetentionCtnsAndAssets() {
		Set<String> retentionCTNNumbers = new Set<String>();
		List<NetProfit_CTN__c> retentionCTNs = new List<NetProfit_CTN__c>();
		// Check which CTNs should be retained
		for (NetProfit_CTN__c ctn : newNPCs) {
			if (
				isRetainableCTN(ctn) &&
				GeneralUtils.isRecordFieldChanged(ctn, oldNPCsMap, 'CTN_Number__c')
			) {
				ctn.Assigned_Product_Id__c = null;
				retentionCTNNumbers.add(ctn.CTN_Number__c);
				retentionCTNs.add(ctn);
			}
		}
		if (retentionCTNNumbers.isEmpty()) {
			return;
		}
		// Get matching Assets and copy Assigned Product ID
		Map<String, VF_Asset__c> assetsByCTN = getMatchingCTNAssets(retentionCTNNumbers);
		for (NetProfit_CTN__c ctn : retentionCTNs) {
			VF_Asset__c asset = assetsByCTN.get(ctn.CTN_Number__c);
			if (asset != null) {
				ctn.Assigned_Product_Id__c = asset.Assigned_Product_Id__c;
			}
		}
	}

	private Boolean isRetainableCTN(NetProfit_CTN__c ctn) {
		return ctn.Action__c == 'Retention' &&
			ctn.CTN_Number__c != null &&
			ctn.Product_Group__c == 'Priceplan';
	}

	private Map<String, VF_Asset__c> getMatchingCTNAssets(Set<String> ctnNumbers) {
		List<String> ctnNumbersEscaped = new List<String>();
		for (String ctnNumber : ctnNumbers) {
			ctnNumbersEscaped.add('\'' + ctnNumber + '\'');
		}
		String queryString =
			'SELECT Id, CTN__c, CTN_Status__c, Assigned_Product_Id__c, Contract_End_Date__c ' +
			'FROM VF_Asset__c ' +
			'WHERE CTN_Status__c = \'Active\' AND CTN__c IN (' +
			String.join(ctnNumbersEscaped, ',') +
			') AND Assigned_Product_Id__c != null ' +
			'ORDER BY Contract_End_Date__c';
		List<VF_Asset__c> assets = (List<VF_Asset__c>) SharingUtils.queryRecordsWithoutSharing(
			queryString
		);
		Map<String, VF_Asset__c> assetsByCTN = new Map<String, VF_Asset__c>();
		for (VF_Asset__c asset : assets) {
			assetsByCTN.put(asset.CTN__c, asset);
		}
		return assetsByCTN;
	}

	private void updateSummaryFieldOnOpportunity() {
		Map<Id, NetProfit_CTN__c> updatedCtns = new Map<Id, NetProfit_CTN__c>(newNPCs);
		//Map<Id, NetProfit_CTN__c> allCtns = new Map<Id, NetProfit_CTN__c>();
		Set<Id> oppIds = new Set<Id>();

		for (NetProfit_CTN__c ctn : [
			SELECT Id, NetProfit_Information__r.Opportunity__c
			FROM NetProfit_CTN__c
			WHERE Id IN :updatedCtns.keySet() AND NetProfit_Information__r.Status__c != ''
		]) {
			if (ctn.NetProfit_Information__r.Opportunity__c != null) {
				oppIds.add(ctn.NetProfit_Information__r.Opportunity__c);
			}
		}
		if (!oppIds.isEmpty()) {
			List<NetProfit_CTN__c> allCtns = [
				SELECT Id, NetProfit_Information__r.Opportunity__c
				FROM NetProfit_CTN__c
				WHERE NetProfit_Information__r.Opportunity__c IN :oppIds
			];

			Map<Id, Opportunity> oppsById = new Map<Id, Opportunity>(
				[
					SELECT Id, NetProfit_Complete__c
					FROM Opportunity
					WHERE Id IN :oppIds AND StageName NOT IN ('Closed Won', 'Closed, Lost')
				]
			);
			TriggerHandler.preventRecursiveTrigger('NetProfitCTNTriggerHandler', null, 0);

			List<Database.SaveResult> saveResults = Database.update(allCtns, false);

			for (Integer i = 0; i < saveResults.size(); i++) {
				if (!saveResults[i].isSuccess()) {
					if (oppIds.contains(allCtns.get(i).NetProfit_Information__r.Opportunity__c)) {
						oppIds.remove(allCtns.get(i).NetProfit_Information__r.Opportunity__c);
					}
				}
			}

			for (Id oppId : oppIds) {
				if (oppsById.containsKey(oppId)) {
					oppsById.get(oppId).NetProfit_Complete__c = true;
				}
			}
			update oppsById.values();
		}
	}
}