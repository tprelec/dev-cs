global class NewApprovalsEmailController {

    global Id entryId { get; set; }
    global appro__Email_Activity__c emailActivity {
        get{
            System.debug(emailActivity);
            if ( emailActivity == null) {

                this.emailActivity = [
                    SELECT
                            appro__Approval_Request__r.appro__RID__c,
                            appro__Approval_Request__r.Product_Basket_lookup__r.Department__c,
                            appro__Approval_Request__r.Product_Basket_lookup__r.DirectIndirect__c,
                            appro__Approval_Request__r.Product_Basket_lookup__r.OwnerId,
                            appro__Approval_Request__r.Product_Basket_lookup__r.cscfga__Opportunity__c,
                            appro__Approval_Request__r.Product_Basket_lookup__r.cscfga__Opportunity__r.Name,
                            appro__Approval_Request__r.Product_Basket_lookup__r.Number_of_CTN__c,
                            appro__Approval_Request__r.Product_Basket_lookup__r.Number_of_SIP__c,
                            appro__Approver_User__r.Name, appro__Approval__c,
                            appro__Approval__r.appro__Approval_Step__r.Name,
                            appro__Approval_Request__r.Owner.Name,
                            appro__Approval_Request__r.appro__Record_Name__c,
                            appro__Approval_Request__r.appro__Record_Object_Label__c,
                            appro__Approval_Request__r.appro__Submitted_Message__c,
                            internal_record_URL__c,
                            appro__Approval__r.appro__Approval_Step__r.appro__Approval_Reason__c,
                                (
                                    SELECT
                                            appro__Approval__c,
                                            appro__Approval__r.appro__Approval_Step__r.Name,
                                            appro__Approval__r.appro__Approval_Step__r.appro__Approval_Reason__c
                                    FROM appro__Email_Activity_Items__r
                                )
                    FROM appro__Email_Activity__c
                    WHERE Id = : entryId
                    LIMIT 1
                ];
                return emailActivity;
            } else {
                return emailActivity;
            }
        }
        set;
    }

    global String ctnOrSip {
        get {
            if (ctnOrSip == null) {
                ctnOrSip = '';
                //if (emailActivity.appro__Approval_Request__r.Product_Basket_lookup__r.Number_of_CTN__c > 0) {
                    ctnOrSip = '<tr><td>Number of CTNs:</td><td>'+String.valueOf(emailActivity.appro__Approval_Request__r.Product_Basket_lookup__r.Number_of_CTN__c)+'</td></tr>';
                //}
                //if (emailActivity.appro__Approval_Request__r.Product_Basket_lookup__r.Number_of_SIP__c > 0) {
                    ctnOrSip += '<tr><td>Number of SIP channels:</td><td>'+String.valueOf(emailActivity.appro__Approval_Request__r.Product_Basket_lookup__r.Number_of_SIP__c)+'</td></tr>';
                //}
            }
            return ctnOrSip;
        }
        private set;
    }

    global String ownerName {
        get {
            if (ownerName == null) {
                try{
                    Id ownerId = emailActivity.appro__Approval_Request__r.Product_Basket_lookup__r.OwnerId;
                    User usr = [Select Id, Name From User WHere Id =: ownerId];
                    ownerName = usr.Name;
                } catch(Exception e) {}
            }
            return ownerName;
        }
        private set;
    }

    global String teamName{
        get {
            if (teamName == null) {
                teamName = '';
                if(emailActivity != null) {
                    try{
                        if (emailActivity.appro__Approval_Request__r.Product_Basket_lookup__r.DirectIndirect__c == 'Direct') {
                            Id ownerId = emailActivity.appro__Approval_Request__r.Product_Basket_lookup__r.OwnerId;
                            User usr = [Select Id, UserRole.Name From User WHere Id =: ownerId];
                            teamName = usr.UserRole.Name;
                        } else {
                            Id ownerId = emailActivity.appro__Approval_Request__r.Product_Basket_lookup__r.OwnerId;
                            User usr = [Select Id, ContactId From User WHere Id =: ownerId];
                            if (usr.ContactId != null) {
                                Contact con = [Select Account.Name From Contact Where Id = :usr.ContactId];
                                teamName = con.Account.Name;
                            }
                        }
                    } catch(Exception e) {}


                }
            }
            return teamName;
        }
        private set;
    }

    global String urlRecord{
        get{
            //return Url.getSalesforceBaseUrl().toExternalForm() + '/';
            return 'https://vodafoneziggo.my.salesforce.com/';
        }
    }

    global List<appro__Email_Activity_Item__c> approvalList {
        get{
            List<appro__Email_Activity_Item__c> approvalList = new List<appro__Email_Activity_Item__c>();
            if (!emailActivity.appro__Email_Activity_Items__r.isEmpty()) {
                return emailActivity.appro__Email_Activity_Items__r;
            } else {
                approvalList.add(new appro__Email_Activity_Item__c(appro__Approval__c = emailActivity.appro__Approval__c));
                return approvalList;
            }
        }
    }

}