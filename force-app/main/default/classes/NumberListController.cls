@SuppressWarnings('PMD.Prone-EmptyStatementBlock')
//To supress the warning on the wrapper constructor
public without sharing class NumberListController {
	public Opportunity theOpp { get; set; }
	public List<NetProfit_CTN__c> ctns { get; set; }
	public List<OpportunityLineItem> oliHardwareMobile {
		get {
			List<OpportunityLineItem> returnOLI = new List<OpportunityLineItem>();
			if (ApexPages.currentPage().getParameters().get('Id') != null) {
				returnOLI = [
					SELECT
						Id,
						Model_Number__c,
						ProductName__c,
						Quantity,
						Gross_List_Price__c,
						DiscountNew__c,
						Net_Unit_Price__c,
						Product_Family__c
					FROM OpportunityLineItem
					WHERE
						OpportunityId = :ApexPages.currentPage().getParameters().get('Id')
						AND Product_Family__c = 'Mobile Hardware'
				];
			}
			return returnOLI;
		}
		set;
	}
	public String language { get; set; }
	public Boolean renderPorting { get; set; }
	public String modelNumber {
		get {
			return this.language.equalsIgnoreCase('Dutch')
				? System.Label.modelNumber_NL
				: System.Label.modelNumber_ENG;
		}
		set;
	}
	public String productName {
		get {
			return this.language.equalsIgnoreCase('Dutch')
				? System.Label.productName_NL
				: System.Label.productName_ENG;
		}
		set;
	}
	public String quantity {
		get {
			return this.language.equalsIgnoreCase('Dutch')
				? System.Label.quantity_NL
				: System.Label.quantity_ENG;
		}
		set;
	}
	public String grossListPrice {
		get {
			return this.language.equalsIgnoreCase('Dutch')
				? System.Label.grossListPrice_NL
				: System.Label.grossListPrice_ENG;
		}
		set;
	}
	public String discountNew {
		get {
			return this.language.equalsIgnoreCase('Dutch')
				? System.Label.discountNew_NL
				: System.Label.discountNew_ENG;
		}
		set;
	}
	public String netUnitPrice {
		get {
			return this.language.equalsIgnoreCase('Dutch')
				? System.Label.netUnitPrice_NL
				: System.Label.netUnitPrice_ENG;
		}
		set;
	}

	public NumberListController() {
		String oppId = ApexPages.currentPage().getParameters().get('Id');
		this.theOpp = [
			SELECT
				Account.Name,
				Owner.Name,
				Main_Contact_Person__r.Name,
				Account.Authorized_to_sign_1st__r.Name,
				Owner.Dealercode__c,
				BAN__r.Ban_Number__c
			FROM Opportunity
			WHERE Id = :oppId
		];
		this.ctns = [
			SELECT
				Id,
				NetProfit_Information__c,
				Quote_Profile__c,
				Price_Plan_Description__c,
				Discount__c,
				MAF__c,
				Product_Quantity_type__c,
				Duration__c,
				Action__c,
				CTN_Number__c,
				Simcard_number_VF__c,
				Sharing__c,
				Data_Limit__c,
				dataBlock__c,
				Contract_Number_Current_Provider__c,
				Current_Provider_name__c,
				Service_Provider_Name__c,
				Billing_Arrangement__c,
				Contract_Enddate__c,
				Gross_List_Price__c,
				Net_Unit_Price__c
			FROM NetProfit_CTN__c
			WHERE NetProfit_Information__r.Opportunity__c = :oppId
		];
		this.language = [
			SELECT Id, Contract_Language__c
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c = :oppId AND Primary__c = TRUE
			LIMIT 1
		]
		.Contract_Language__c;

		this.renderPorting = false;
		for (NetProfit_CTN__c ctn : this.ctns) {
			if (ctn.Action__c == 'Porting') {
				this.renderPorting = true;
			}
			if (ctn.CTN_Number__c != null && ctn.CTN_Number__c.containsIgnoreCase('x')) {
				ctn.CTN_Number__c = '';
			}
		}
	}
}