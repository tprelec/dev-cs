public with sharing class NumberPortingRowWrapper {
	//wrapper class for numberporting rows
    public Numberporting_row__c portingRow {get;set;}
    public Integer id {get; set;}
    public Boolean toDelete {get;set;}
	public Boolean selected {get; set;}    
    
    public NumberportingRowWrapper(Integer rowId, Boolean inEdit, Numberporting_row__c p,Boolean isDeleted) {
        this.portingRow = p;
        this.id = rowId;
        this.toDelete = false;
        this.selected = false;
    }
}