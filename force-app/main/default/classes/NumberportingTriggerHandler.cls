/**
 * @description			This is the trigger handler for the Numberporting sObject.
 * @author				Guy Clairbois
 */
public with sharing class NumberportingTriggerHandler extends TriggerHandler {

	/**
	 * @description			This handles the before insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeInsert(){
	}

    
	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterInsert(){
		List<Numberporting__c> newnumberportings = (List<Numberporting__c>) this.newList;	
		
		triggerAccountExport(newnumberportings);
	}

	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterUpdate(){
		List<Numberporting__c> newnumberportings = (List<Numberporting__c>) this.newList;	
		List<Numberporting__c> oldnumberportings = (List<Numberporting__c>) this.oldList;
		Map<Id,Numberporting__c> oldnumberportingsMap = (Map<Id,Numberporting__c>) this.oldMap;
		
		// also after update, in case an account is added later on to an existing numberporting
		triggerAccountExport(newnumberportings);
	}	
	
	/**
	 * @description			This method exports the Numberporting's account to BOP, if applicable
	 * @author				Guy Clairbois
	 */
	private void triggerAccountExport(List<Numberporting__c> numberportings){
	// OBSOLETE
	/*	Set<Id> acctIds = new Set<Id>();
		for(Numberporting__c np : numberportings){
			acctIds.add(np.Customer__c);
		}

		Set<Id> acctIdsForExport = new Set<Id>();
		// check if account has BOPCode. If so, no need to re-sync. If not, creat the account in ECS
		for(Account a : [Select 
							Id, 
							Name, 
							BOPCode__c 
						From 
							Account 
						Where 
							Id in :acctIds 
						AND (BOPCode__c = null 
						OR BOPCode__c = '')
		]){
			acctIdsForExport.add(a.Id);
		}
		
		if(!acctIdsForExport.isEmpty()){
			AccountExport.exportAccountsOffline(acctIdsForExport);
		}			
	*/	
	}
	
}