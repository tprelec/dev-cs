public class OlbicoCdpController {

    private String token;
    private String language;
    private String accountId;
    private String recordType;
    
    public OlbicoCdpController (ApexPages.StandardController controller) {
        FillAccountId();
        FillLanguage();
        FillRecordType();
        LogIn();
        
     
    }
    
    private void FillRecordType()
    {
       recordType = ApexPages.currentPage().getParameters().get('RecordType');
    }
    
    private void FillAccountId()
    {
        Id accountIdShort = ApexPages.currentPage().getParameters().get('accid');

        if (accountIdShort != null)
        {
            Account account = [SELECT Id, Name FROM Account WHERE Id = :accountIdShort ];
            accountId= account.Id;
        }
        else
        {
            accountId = ApexPages.currentPage().getParameters().get('selectedAccount');
        }        
    }
    
    private void FillLanguage()
    {
        String locale = UserInfo.getLanguage();
        String[] localeParts = locale.split('_');
        language = localeParts[0];
    }
    
    private void LogIn() {
        token = '';
        try {
            
            OlbicoCdpClientServiceProxy.BasicHttpBinding_ICrmClientService1 loginService = new OlbicoCdpClientServiceProxy.BasicHttpBinding_ICrmClientService1();

            /*token = loginService.CrmClientLogIn('VodafoneTestDev1','55afgH8Gp3PRmDw');*/
            
            
            String username = OlbicoSettings__c.getOrgDefaults().CdpUserName__c;
            String password = OlbicoSettings__c.getOrgDefaults().CdpPassword__c;
     
            
            token = loginService.CrmClientLogIn(username,password);
            
            if (token != '') {
                System.debug('Login finished, user is logged in');
            }
            else {
                System.debug('Login finished, user is NOT logged in');
            }
        }
        catch(Exception e) {
            System.debug('Login ended in an exception:');
            System.debug(e.getMessage());
        }
    }
    
    public string getAccountId() {
        return accountId;
    }
    
    public string getRecordType() {
        return recordType;
    }
    
    public string getLanguage() {
        return language;
    }
    
    public string getToken() {
        return token;
    }
    
    public string getParams() {
        //return the current querystring as a string
        String queryString = '';
        for (String key: ApexPages.currentPage().getParameters().keyset() ) {
            String value = ApexPages.currentPage().getParameters().get(key);
            
            queryString += '&' + key + '=' + EncodingUtil.urlEncode(value, 'UTF-8');
        }
        
        return queryString;

    }
}