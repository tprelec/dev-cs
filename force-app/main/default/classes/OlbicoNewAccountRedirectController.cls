public with sharing class OlbicoNewAccountRedirectController {

    public OlbicoNewAccountRedirectController (ApexPages.StandardController controller) {
    }
    
    public PageReference Redirect() {
 
        Id profileId = userinfo.getProfileId();
        
        ApexPage apexPage = [SELECT Id FROM ApexPage WHERE Name = 'OlbicoCdpNewAccount'];
        Id apexPageId = apexPage.Id;
        
        Integer accessCount = [
            SELECT COUNT()
            FROM SetupEntityAccess
            WHERE ParentId IN (SELECT Id FROM PermissionSet WHERE IsOwnedByProfile = true AND ProfileId = :profileId)
            AND SetupEntityId = :apexPageId];
           
        //System.Debug('accessCount: ' + accessCount);
        
        PageReference newPage;
        
        if (accessCount > 0)
        {
            newPage = Page.OlbicoCdpNewAccount.setRedirect(True);
        }
        else
        {
            newPage = new PageReference('/001/e?retURL=%2F001%2Fo');
            newPage.getParameters().put('nooverride','1');
        }
        
        return newPage;
    }

}