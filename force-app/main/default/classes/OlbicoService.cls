public class OlbicoService extends ECSSOAPBaseWebService implements IWebService<String, String>{

	public final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	public final Schema.SObjectType accountSchema = schemaMap.get('Account');
	public final map<String, Schema.SObjectField> accountFieldMap = accountSchema.getDescribe().fields.getMap();

	private String response = '';
	private String kvk;
	private String accId;
	private Account accSend;
	private Boolean batch = false;
	private Id accRecordTypeId = GeneralUtils.recordTypeMap.get('Account').get('VF_Account');

	public OlbicoService() {
		if(!Test.isRunningTest()){
			setWebServiceConfig( WebServiceConfigLocator.getConfig('OlbicoSOAP') );
		}
		else setWebServiceConfig( WebServiceConfigLocator.createConfig());
	}

	public void setRequest(String request){
		this.kvk = request;
		this.response = '';
		this.accSend = null;
	}

	public String getResponse(){
		return response;
	}

	public String getAccId(){
		return accId;
	}

	public Account getAcc(){
		return accSend;
	}

	public void setBatch(){
		this.batch = true;
	}

	public void makeRequest(String requestType){

		if(webserviceConfig == null) throw new ExWebServiceCalloutException('Missing configuration');
		if(kvk == null) throw new ExWebServiceCalloutException('A request has not been set');

		OlbicoSOAP.CustomerServiceSoap service = new OlbicoSOAP.CustomerServiceSoap();
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		OlbicoSOAP.SearchAndMatch_element checkRequest = buildCheckUsingRequest();
		//System.debug('D&Bcheck: ' + JSON.serialize(checkRequest));

		OlbicoSOAP.ArrayOfDataAndPercentages checkResult = new OlbicoSOAP.ArrayOfDataAndPercentages();
		
		try {
			if (Test.isRunningTest()) {

			} else {
				system.debug(checkRequest.types);
				system.debug(checkRequest.values);
				checkResult = service.SearchAndMatch(checkRequest.types, checkRequest.values, checkRequest.customerKey);
				parseResponse(checkResult);
			}
		} catch(Exception ex){
			response = ex.getStackTraceString();
			//throw new ExWebServiceCalloutException('There was an error when attempting to retrieve the Account from D&B. Please contact your administrator.');
		}
		
	}

	private OlbicoSOAP.SearchAndMatch_element buildCheckUsingRequest(){

		OlbicoSOAP.SearchAndMatch_element createRequest = new OlbicoSOAP.SearchAndMatch_element();

		OlbicoSOAP.ArrayOfString type = new OlbicoSOAP.ArrayOfString();
		type.string_x = new List<String>{'Local_id1','Free_field1'};
		createRequest.types = type;

		OlbicoSOAP.ArrayOfString value = new OlbicoSOAP.ArrayOfString();
		value.string_x = new List<String>{kvk,'Hoofdzaak'};
		createRequest.values = value;

		createRequest.customerKey = webServiceConfig.getPassword();
		return createRequest;
	}

	private void parseResponse(OlbicoSOAP.ArrayOfDataAndPercentages checkResult){

		if(checkResult == null) return;
		Map<String, Account> accMap = new Map<String, Account>();
		Map<Contact, String> conMap = new Map<Contact, String>();
		List<Contact> insertList = new List<Contact>();
		String kvk;

		for(OlbicoSOAP.DataAndPercentages dAndP : checkResult.DataAndPercentages){
			if(dAndP.Success == true && dAndP.ColumnNames.string_x != null){
				Map<String, DB_Mapping__c> mapping = DB_Mapping__c.getAll();
				Account acc = new Account();
				acc.RecordTypeId = accRecordTypeId;
				Contact con = new Contact();
				Integer count = 0;

				system.debug(dAndP.ResultRows.Row[0]);
				system.debug(dAndP.ResultRows.Row[0].values);

				for(String column : dAndP.ColumnNames.string_x){

					if(mapping.containsKey(column)){
						if(column == 'KVK_8'){
							kvk = column;
						}

						system.debug(column);
						system.debug(mapping);
						system.debug(mapping.get(column).Account_Field__c);

						system.debug(dAndP.ResultRows.Row[0].Values.string_x[count]);

						if(mapping.get(column).Account_Field__c != null && dAndP.ResultRows.Row[0].Values.string_x[count] != ''){
							String accountFieldName = mapping.get(column).Account_Field__c;

							if(accountFieldName != null && !accountFieldMap.containsKey(accountFieldName)){
								// error, field doesn't exist in SFDC
								response = 'Mapping error: Account field '+accountFieldName+' does not exist in Salesforce. Please contact your administrator.';
								return;
							}	

							Object obj = dAndP.ResultRows.Row[0].Values.string_x[count];
							if(accountFieldMap.get(accountFieldName).getDescribe().getType().name() == 'CURRENCY' ||
							   accountFieldMap.get(accountFieldName).getDescribe().getType().name() == 'DOUBLE'){
								obj = Decimal.valueOf(dAndP.ResultRows.Row[0].Values.string_x[count]);
							} else if(accountFieldMap.get(accountFieldName).getDescribe().getType().name() == 'INTEGER'){
								obj = Integer.valueOf(dAndP.ResultRows.Row[0].Values.string_x[count]);
							} else if(accountFieldMap.get(accountFieldName).getDescribe().getType().name() == 'BOOLEAN'){
								obj = Boolean.valueOf(dAndP.ResultRows.Row[0].Values.string_x[count]);
							} else if(accountFieldMap.get(accountFieldName).getDescribe().getType().name() == 'DATE'){
								obj = Date.valueOf(dAndP.ResultRows.Row[0].Values.string_x[count]);
							}
							acc.put(accountFieldName, obj);
						} else if(mapping.get(column).Contact_Field__c != null && dAndP.ResultRows.Row[0].Values.string_x[count] != ''){
							con.put(mapping.get(column).Contact_Field__c, dAndP.ResultRows.Row[0].Values.string_x[count]);
						}
					} // no problem if the field is not in the mapping. It can be ignored then.
					count++;
				}
				accMap.put(kvk, acc);
				conMap.put(con, kvk);
				accSend = acc;

			} else {
				//System.debug(dAndP.ErrorMessages);
				response = 'The KVK number is not known in the database. Please contact your administrator.';
			}
		}

		if(batch == false){
			//Insert accounts
			try{
				insert accMap.values();
			} catch(Exception ex){
				response = 'There was an error when trying to create the new Account. Please contact your administrator.';
				//throw new ExWebServiceCalloutException('There was an error when trying to create the new Account from D&B. Please contact your administrator.');
			}

			for(Account acc : accMap.values()){
				accId = acc.Id;
			}

			for(Contact con : conMap.keySet()){
				if(con.LastName != null){
					con.AccountId = accMap.get(conMap.get(con)).Id;
					insertList.add(con);
				}
			}

			//Insert contacts
			try{
				insert insertList;
			} catch(Exception ex){
				response = 'There was an error when trying to create the new Contact. Please contact your administrator.';
				//throw new ExWebServiceCalloutException('There was an error when trying to create the new Contact from D&B. Please contact your administrator.');
			}
		}
		system.debug(response);
	}
}