/**
 * @description           This class is responsible for calling, parsing the Olbico JSON Webservice
 * @author                Marcel Vreuls
 * @History               Okt 2017: W-000269: inital creation for
 *                        Jan 2018: W-000286: added json interface foor BAG
 *
 * @Defaults              Requires the creation and update of the Olbico Custom settings
 */

public with sharing class OlbicoServiceJSON {

    private Account accSend;
    private string  kvk;
    private string zipcode;
    private String accId;
    private String response = '';
    public string errorMessage {get;set;}
    private Boolean batch = false;
    public final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    public final Schema.SObjectType accountSchema = schemaMap.get('Account');
    public final map<String, Schema.SObjectField> accountFieldMap = accountSchema.getDescribe().fields.getMap();
    private Id accRecordTypeId = GeneralUtils.recordTypeMap.get('Account').get('VF_Account');
    private List<SelectOption> addresInformation {get; set;}
    public Boolean addressFound {get;set;}

    public OlbicoServiceJSON() {
        //Olbico_Json_BAGSearch__c: Search BAG address: 797f9acc-4c34-46aa-bc0b-e0d41bc5b4fe
        //Search D&B 1 record : ef62f95b-30fc-4520-9be1-8af222e57ef6
        //Search D&B           :    32c4eded-41e1-4b1b-8908-f6ced256d666
    }

    public List<SelectOption> AdresInfo() {
        return addresInformation;
    }

    public String getResponse() {
        return response;
    }

    public void setBatch() {
        this.batch = true;
    }

    /**
    * Returns the ID from a recently created account for Partnerith the Olbico API data
    */
    public String getAccId() {
        return accId;
    }

    /**
    * Returns the filled accountobject with the Olbico API data
    */
    public Account getAcc() {
        return accSend;
    }


    /**
    * Sets the request parameters for the makeRequest method.
    */
    public  void setRequestRestJson(String kvk) {
        this.accSend = null; //Make sure object is empty
        this.kvk = kvk; //Set query parameter
    }


    /**
    * Actually calls the webservice and maps its with the BAG mapping table in custom settings
    */
    public void makeReqestRestJson(String requestType) {

        Account acc = new Account();
        Contact con = new Contact();
        acc.RecordTypeId = accRecordTypeId;
        String responseFromNet;

        String endpointURL  = OlbicoSettings__c.getOrgDefaults().Olbico_JSon_Endpoint__c; //'https://api.dqbox.com/api/search';
        String sUserName    = OlbicoSettings__c.getOrgDefaults().Olbico_Json_Username__c;
        String sPassword    = OlbicoSettings__c.getOrgDefaults().Olbico_Json_Password__c;
        String sCaseKey     = OlbicoSettings__c.getOrgDefaults().Olbico_Json_BAGSearch__c;

        Map<String, Account> accMap = new Map<String, Account>();
        Map<Contact, String> conMap = new Map<Contact, String>();
        List<Contact> insertList = new List<Contact>();

        HttpRequest reqData = new HttpRequest();
        Http http = new Http();

        reqData.setHeader('Content-Type','application/json');
        reqData.setHeader('Connection','keep-alive');
        reqData.setHeader('Content-Length','0');
        Blob headerValue = Blob.valueOf(sUserName + ':' + sPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        reqData.setHeader('Authorization', authorizationHeader);
        reqData.setTimeout(20000);
        reqData.setEndpoint(endpointURL);
        reqData.setBody('{"CaseKey":"' + sCaseKey + '","Record":{"Local_id1":[" ' + this.kvk +'"],"Free_field1":["Hoofdzaak"]}}}');
        reqData.setMethod('POST');

        try {
            String column;
            HTTPResponse res = http.send(reqData);
            responseFromNet = res.getBody();
            System.debug(responseFromNet);
            Map<String, BAG_Mapping__c> mapping = BAG_Mapping__c.getAll();

            JSONParser parser = JSON.createParser(responseFromNet);
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                    column = parser.getText();

                    if (mapping.containsKey(column)) {
                        parser.nextValue();
                        if (column == 'kvk_8') {
                            kvk = column;
                        }

                        String accountFieldName = mapping.get(column).Account_Field__c;
                        if (accountFieldName != null) {
                            String jValue = parser.getText();
                            if (String.isNotBlank(jValue) && jValue != 'null') {
                                Object obj = jValue;
                                String fieldType = accountFieldMap.get(accountFieldName).getDescribe().getType().name();
                                if (fieldType == 'CURRENCY' || fieldType == 'DOUBLE') {
                                    obj = Decimal.valueOf(jValue);
                                } else if (fieldType == 'INTEGER' || fieldType == 'NUMBER') {
                                    obj = Integer.valueOf(jValue);
                                } else if (fieldType == 'BOOLEAN') {
                                    obj = Boolean.valueOf(jValue);
                                } else if (fieldType == 'DATE') {
                                    obj = Date.valueOf(jValue);
                                }
                                acc.put(accountFieldName, obj);
                            }
                        } else if (mapping.get(column).Contact_Field__c != null) {
                            con.put(mapping.get(column).Contact_Field__c, parser.getText());
                        }
                    }
                }
            }
            accMap.put(kvk, acc);
            conMap.put(con, kvk);
            acc.Trigger_Olbico_Update__c = false;
            this.accSend = acc;
        } catch (Exception exp) {
            response = exp.getStackTraceString();
            System.debug('exception '+exp);
        }
            
        if (batch == false) {
            //Insert accounts
            try {
                insert accMap.values();
            } catch(Exception ex) {
                System.debug('@@@@Account exception ' + ex.getStackTraceString());
                response = 'There was an error when trying to create the new Account. Please contact your administrator.';
            }

            for (Account acc1 : accMap.values()) {
                accId = acc1.Id;
            }

            for (Contact con1 : conMap.keySet()) {
                if (con1.LastName != null) {
                    con1.AccountId = accMap.get(conMap.get(con1)).Id;
                    insertList.add(con);
                }
            }

            //Insert contacts
            try {
                insert insertList;
            } catch(Exception ex) {
                System.debug('@@@@Contact exception ' + ex.getStackTraceString());
                response = 'There was an error when trying to create the new Contact. Please contact your administrator.';
                //throw new ExWebServiceCalloutException('There was an error when trying to create the new Contact from D&B. Please contact your administrator.');
            }
        }
    }


    /**
    * Sets the request parameters for the makeRequest method.
    */
    public void setRequestRestJsonStreet(String zipCode) {
        this.accSend = null; //Make sure object is empty
        this.zipcode = zipCode; //Set query parameter
    }

/**
    * Actually calls the webservice and maps its with the BAG mapping table in custom settings
    */
    public  void makeRequestRestJsonStreet(String requestType) {

        String column = '';
        String responseFromNet;
        Integer i = 0;
        String endpointURL     = OlbicoSettings__c.getOrgDefaults().Olbico_JSon_Endpoint__c; //'https://api.dqbox.com/api/search';
        String sUserName    = OlbicoSettings__c.getOrgDefaults().Olbico_Json_Username__c; //'usr_vod2325sitf';
        String sPassword    = OlbicoSettings__c.getOrgDefaults().Olbico_Json_Password__c; //'eee6yLb4LwA9PbPh';
        String sCaseKey     = OlbicoSettings__c.getOrgDefaults().Olbico_Json_BAG_Streets__c; //'ef62f95b-30fc-4520-9be1-8af222e57ef6';

        HttpRequest reqData = new HttpRequest();
        Http http = new Http();

        reqData.setHeader('Content-Type','application/json');
        reqData.setHeader('Connection','keep-alive');
        reqData.setHeader('Content-Length','0');

        Blob headerValue = Blob.valueOf(sUserName + ':' + sPassword);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        reqData.setHeader('Authorization', authorizationHeader);
        reqData.setTimeout(20000);
        reqData.setEndpoint(endpointURL);


        if (this.ZipCode.length() > 6) {
            String zip = this.ZipCode.substring(0,6);
            String housenumber = this.Zipcode.substring(6,this.ZipCode.length());
            reqData.setBody('{"CaseKey":"' + sCaseKey + '","Record":{"Adress1b":["'+ housenumber +'"],"Zipcode1":[" ' + zip +'"]},"Paging": {"Page": 1,"PageSize": 100}}');
        } else {
            reqData.setBody('{"CaseKey":"' + sCaseKey + '","Record":{"Zipcode1":[" ' + this.ZipCode +'"]},"Paging": {"Page": 1,"PageSize": 100}}');
        }

        reqData.setMethod('POST');

        addresInformation = new List<SelectOption>();

         try {
            HTTPResponse res = http.send(reqData);
            responseFromNet = res.getBody();
            system.debug('makeRequestRestJsonStreet: ' + responseFromNet);

            JSONParser parser = JSON.createParser(responseFromNet);

            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME )) {
                    if (parser.getText() == 'straatnaam') {
                        parser.nextToken();
                        column = parser.getText();
                        parser.nextToken();
                    }
                    if (parser.getText() == 'huisnummer') {
                        parser.nextToken();
                        column = column + ' | ' + parser.getText();
                        parser.nextToken();
                    }
                    if (parser.getText() == 'huisletter') {
                        parser.nextToken();
                        column = column + ' | ' + parser.getText();
                        parser.nextToken();
                    }

                    if (parser.getText() == 'huisnummertoevoeging') {
                        parser.nextToken();
                        column = column + ' | ' + parser.getText();
                        parser.nextToken();
                    }
                    if (parser.getText() == 'postcode') {
                        parser.nextToken();
                        column = column + ' | ' + parser.getText();
                        parser.nextToken();
                    }
                    if (parser.getText() == 'woonplaatsnaam') {
                        parser.nextToken();
                        column = column + ' | ' + parser.getText();
                        parser.nextToken();
                    }
                    i++;
                }

                if (column != '') {
                    addresInformation.add(new SelectOption(column, column));
                }
                column = '';
            }

            if (addresInformation.size() > 0) {
                doSort(addresInformation, FieldToSort.Value);
                addressFound = true;
            } else {
                addressFound = false;
                if (responseFromNet.contains('Too Many Requests')) {
                    errorMessage = 'Please try again in 1 minute';
                }
            }

        } catch (Exception exp) {
            response = exp.getStackTraceString();
            System.debug('exception '+exp);
         }

         if (batch == false) {
            //Insert accounts
            try {
                //insert accMap.values();
            } catch(Exception ex) {
                response = 'There was an error when trying to create the new Account. Please contact your administrator.';
                //throw new ExWebServiceCalloutException('There was an error when trying to create the new Account from D&B. Please contact your administrator.');
            }
        }
    }


    //Used to select on what to sort
    public enum FieldToSort {
        Label, Value
    }

     //Sort options list
    public static void doSort(List<SelectOption> opts, FieldToSort sortField) {
        Map<String, SelectOption> mapping = new Map<String, SelectOption>();
        Integer suffix = 1;
        for (SelectOption opt : opts) {
            if (sortField == FieldToSort.Label) {
                mapping.put((opt.getLabel() + suffix++), opt);
            } else {
                mapping.put((opt.getValue() + suffix++), opt);
            }
        }

        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();
        opts.clear();

        for (String key : sortKeys) {
            opts.add(mapping.get(key));
        }
    }
}