/*
 *    @Description:    Used for controlling the opportunity attachments management page. Requires WITHOUT SHARING as other ways to edit OpportunityAttachments will be disabled
 *    @Author:         Guy Clairbois, Marcel Vreuls
 */
public without sharing class OpportunityAttachmentManagerController {

  /*
   *    Variables
   */
  public Id opportunityId {get;set;}
  public Opportunity opportunity {get;set;}
  public List<AttachmentWrapper> opportunityAttachments {get;set;}
  public List<AttachmentWrapper> newOpportunityAttachments {get;set;}
  public string SelectedoaId { get; set; }
  public Boolean signedSLA {get;set;}
  public Boolean signedContract {get;set;}
  public Boolean errorFound {get;set;}
  public Boolean hasEditRightsOnOpportunity {get;set;}

  // the number of new attachments to add to the list when the user clicks 'Add More'
  public static final Integer NUM_ATTACHMENTS_TO_ADD=3;

  /*
   *    Controller
   */

  public OpportunityAttachmentManagerController() {
    opportunityId = Apexpages.currentPage().getParameters().get('opportunityId');
    errorFound = false;
    refreshPage=false;
    LoadData();

  }

  public OpportunityAttachmentManagerController(ApexPages.StandardController controller) {
    // No idea why but this is not getting the ID in DEV, but does in UAT and PRD
    opportunityId = controller.getId();
    if (opportunityId==null) {
      opportunityId = Apexpages.currentPage().getParameters().get('opportunityId');      
    }
    System.Debug('GJN opportunityId:'+opportunityId);

    errorFound = false;
    refreshPage=false;
    LoadData();
    addMore();
  }  


  private void LoadData(){
    opportunityAttachments = new List<AttachmentWrapper>();      // This is what gets shown on the page
    newOpportunityAttachments = new List<AttachmentWrapper>();

    // get opportunity
    List<Opportunity> opportunities = [Select 
            Id, 
            Name,
            AccountId,
            StageName
          from 
            Opportunity 
          Where 
            Id = :opportunityId];
    if(!opportunities.isEmpty()){
      opportunity = opportunities[0];
    } else {
      ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.ERROR,'Opportunity not found'));
      errorFound = true;
      return;
    }
    if(opportunity.StageName == 'Closed Won'){
      ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.ERROR,'Opportunity has stage closedwon. Attachments are now managed in the VF_Contract'));
      errorFound = true;
      return;      
    }

    // check opportunity access
    UserRecordAccess ua = [SELECT 
                  RecordId, 
                  HasEditAccess 
                FROM 
                  UserRecordAccess
                   WHERE
                     UserId = :System.UserInfo.getUserId()
                     AND RecordId = :opportunity.Id];
       hasEditRightsOnOpportunity = ua.HasEditAccess;

       // get opportunity attachments
    Map<Id,AttachmentWrapper> tempMap = new Map<Id,AttachmentWrapper>();
    List<Opportunity_Attachment__c> tempList = new List<Opportunity_Attachment__c>();   // use list to maintain sorted order
    Map<Id,AttachmentWrapper> fileMap = new Map<Id,AttachmentWrapper>();        // Maps contentDocumentID to wrapper
    Map<id,Opportunity_Attachment__c> oaMap = new Map<Id,Opportunity_Attachment__c>();
    Set<ID> contentSet = new Set<ID>();
    for(Opportunity_Attachment__c oa :[Select 
                  Id, 
                  Name,
                  Attachment_Type__c, 
                  Description_Optional__c,
                  CreatedById,
                  CreatedDate,
                  Signed_Contract__c,
                  ContentDocumentId__c
                From 
                  Opportunity_Attachment__c 
                Where 
                  Opportunity__c = :opportunityId
                Order By Attachment_Type__c
                ])
    {
      if(!GeneralUtils.currentUserIsPartnerUser() || !getHiddenContractTypes().contains(oa.Attachment_Type__c)){
        tempList.add(oa);
        tempMap.put(oa.Id,new AttachmentWrapper(oa,null));
        oaMap.put(oa.id,oa);        
      }
    }


    // 1 - Get the ContentDocumentLink Records for the opportunity attachments
    Set<ID> oaSet = oaMap.keySet();
    if (oaSet.size()>0) {
      for (ContentDocumentLink link : [select ContentDocumentId, LinkedEntityId from ContentDocumentLink where LinkedEntityId in:oaSet]) {
        // 2 - Use results to Populate a Map (fileMap) where key is contentDocumentID and result is new AttachmentWrapper
        Opportunity_Attachment__c oa = oaMap.get(link.LinkedEntityId);
        fileMap.put(link.ContentDocumentId,new AttachmentWrapper(oa,null));
        contentSet.add(link.ContentDocumentId);      
        if (oa.ContentDocumentID__c==null) oa.ContentDocumentID__c=link.ContentDocumentId;
      }

      // 3 - Get the contentversion records for the contentDocuments
      // Get the contentversion record for each opportunity attachment
      for (contentversion file : [select id, ContentDocumentId, pathOnClient from contentversion where contentdocumentid in:contentSet and isLatest=true]) {
        // 4 - Use the fileMap to get the wrapper and populate with the contentversion      
        fileMap.get(file.ContentDocumentId).file = file;
      }

      if(!GeneralUtils.currentUserIsPartnerUser()){
        for(FeedItem fi : [Select
                    Id,
                    ParentId,
                    ContentFileName,
                    RelatedRecordId
                  from
                    FeedItem
                  Where
                    ParentId in :tempMap.keySet()]){
          tempMap.get(fi.ParentId).feed = fi;
        }
      }

      for(Opportunity_Attachment__c oa : tempList){
        // now go through list to maintain sort order
        opportunityAttachments.add(fileMap.get(oa.ContentDocumentID__c));  
      }
      
    }

    // if there are no attachments yet, show 5 empty ones to prevent the user from having to click
    if(tempList.isEmpty()) addMore();
  }


  public class AttachmentWrapper {
    public Opportunity_Attachment__c oa {get;set;}
    public FeedItem feed {get;set;}
    public String type {get;set;}
    public ContentVersion file {get;set;}

    public AttachmentWrapper(Opportunity_Attachment__c oa, ContentVersion file){
      this.oa = oa;
      if(file!= null){
        this.file = file;
      }
      this.type = oa.Attachment_Type__c;
    }    

    public FeedItem getHiddenFeedItem(){
      if(file != null) {
        //Adding a Content post
          FeedItem post = new FeedItem();
        post.ParentId = oa.Id;
          post.Body = file.pathOnClient;
          post.ContentData = file.VersionData;
          post.ContentFileName = file.pathOnClient;
          post.Visibility = 'InternalUsers';
          return post;
      }
      return null;

    }
  }

  /*
   *    Page References
   */

  public pageReference addMore(){
    for (Integer idx=0; idx<NUM_ATTACHMENTS_TO_ADD; idx++){

      newOpportunityAttachments.add(new AttachmentWrapper(new Opportunity_Attachment__c(Opportunity__c = opportunityId),new ContentVersion()));
    }
    return ApexPages.currentPage();
  }

  public pageReference backToOpportunity(){
    return new PageReference('/'+opportunityId);
  }

  // for the embedded page, this is the link to the actual attachmentmanager
  public pageReference manageAttachments(){
    return new PageReference('/apex/VF_OpportunityAttachmentManager?opportunityId='+OpportunityId);
  }
  public pageReference nothing(){
    refreshPage=true;
    return null;
  }
  public Boolean refreshPage {get; set;}


  // Note: There is two way linking happening here. The native object ContentDocumentLink links to the opportunity_attachment__c
  // and the opportunity_attachment__c links to ContentDocument. This second link is populated in ContentDocumentLinkTriggerHandler
  public void saveAttachments(){
    
    List<Opportunity_Attachment__c> opportunityAttachmentsToInsert = new List<Opportunity_Attachment__c>();
    List<Opportunity_Attachment__c> opportunityAttachmentsToUpdate = new List<Opportunity_Attachment__c>();
    List<ContentDocumentLink> contentDocumentLinkToInsert = new List<ContentDocumentLink>();
    List<ContentVersion> contentVersionToInsert = new List<ContentVersion>();
    List<ContentVersion> contentVersionList = new List<ContentVersion>();
    List<FeedItem> feedItemsToInsert = new List<FeedItem>();
    for(AttachmentWrapper aw : newOpportunityAttachments){
      if(aw.oa != null && aw.oa.Attachment_Type__c != null) opportunityAttachmentsToInsert.add(aw.oa);
      
    }
    Savepoint sp = Database.setSavepoint();
    try{
      insert opportunityAttachmentsToInsert;

      Integer counter = 0;
      for(Opportunity_Attachment__c oa : opportunityAttachmentsToInsert){

        if(oa.Id != null){
          system.debug(newOpportunityAttachments[counter]);
          if(newOpportunityAttachments[counter].file != null){

            // Hijack this field to store the Opportunity_Attachment__c ID so we can link it back later on
            newOpportunityAttachments[counter].file.ReasonForChange = oa.Id;

            // for certain attachment types, don't save as attachment but as (hidden) feed item so that partners cannot see it
            if(!getHiddenContractTypes().contains(oa.Attachment_Type__c)){
              contentVersionToInsert.add(newOpportunityAttachments[counter].file);
            } else {
              feedItemsToInsert.add(newOpportunityAttachments[counter].getHiddenFeedItem());
            }
          }
        }
        counter++;
      }
      insert feedItemsToInsert;
      
      // Insert the files (ContentVersion)
          insert contentVersionToInsert;  

      // Create the links between the content document and the opportunity attachment
      // and store the content document id on the opportunity attachment
      contentVersionList = [SELECT ContentDocumentId, ReasonForChange FROM ContentVersion WHERE Id = :contentVersionToInsert];
      for (ContentVersion cv:contentVersionList) {
        ContentDocumentLink cdl = new ContentDocumentLink();        
        cdl.ContentDocumentId = cv.ContentDocumentId;
        cdl.LinkedEntityId = cv.ReasonForChange;
        cdl.ShareType = 'V';    
        cdl.Visibility  = 'Allusers'; //W-001438   
        contentDocumentLinkToInsert.add(cdl);                
      }  
      insert contentDocumentLinkToInsert;          

      // process any attachment type updates
      for(AttachmentWrapper aw : opportunityAttachments){
        if (aw.oa!=null) {
          if(aw.type != aw.oa.Attachment_Type__c){
            opportunityAttachmentsToUpdate.add(aw.oa);
          }
        }
      }
      update opportunityAttachmentsToUpdate;

      // reset datasets
      newOpportunityAttachments.clear();
      opportunityAttachments = null;


    } catch (dmlException de){
      Database.rollback(sp);
      Apexpages.addMessages(de);
    }


        //refresh the data
       LoadData();
    newOpportunityAttachments.clear();    
  }  


  public void DeleteOpportunityAttachment(){
      // if for any reason we are missing the reference 
        if (SelectedoaId == null) {
           return;
        }
     
    Delete New Opportunity_Attachment__c(Id=SelectedoaId);
      
     
        //refresh the data
        LoadData();
   }    

   public List<SelectOption> signedContractOptions {
       get{
         if(signedContractOptions == null){
           signedContractOptions = new List<SelectOption>();
           signedContractOptions.add(new SelectOption('','none'));

           // first gather all opportunity_attachments for the particular customer
           Set<Id> oaIds = new Set<Id>();
           for(Opportunity_Attachment__c oa : [Select 
                             Id 
                           From 
                             Opportunity_Attachment__c 
                           Where 
                             Attachment_Type__c = 'Contract (Signed)'
                           AND
                             Opportunity__r.AccountId = :opportunity.AccountId]){
             oaIds.add(oa.Id);
           }

           for(Attachment a : [Select Id, ParentId, Name From Attachment Where ParentId in :oaIds]){
             signedContractOptions.add(new SelectOption(a.ParentId,a.Name));
           }
         }
         return signedContractOptions;
       }
       private set;
   }


   private Set<String> getHiddenContractTypes(){
       Set<String> returnSet = new Set<String>();

       for(Attachment_Setting__mdt atts : [select DeveloperName, Label, Attachment_types_hidden_from_partners__c from Attachment_Setting__mdt Where Label = 'default']){
         for(String s : atts.Attachment_types_hidden_from_partners__c.split(';')){
           returnSet.add(s);
         }
       }

       return returnSet;
   }

}