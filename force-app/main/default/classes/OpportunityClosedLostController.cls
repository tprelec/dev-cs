public with sharing class OpportunityClosedLostController {

	public OpportunityClosedLostController(ApexPages.StandardController controller) {
		if(!Test.isRunningTest()){
			controller.addFields(New List<String> {'OwnerId','IsClosed'});
		}
		theOpp = (Opportunity) controller.getRecord();

		if(theOpp.IsClosed){
            Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,'Opportunity is already closed.'));
            errorFound = true;
		}
	}

	public Opportunity theOpp {get;set;}
	public Boolean errorFound {get;set;}

	public pageReference confirmClosedLost(){
		Savepoint sp = Database.setSavepoint();
		try{
			// set the opportunity to closedwon
			theOpp.StageName = 'Closed Lost';
			update theOpp;

			// load the opportunity page
			PageReference oppPage = new PageReference('/'+theOpp.Id);
			oppPage.setRedirect(true);
			return oppPage;		

		} catch (Exception e){
			database.rollback(sp);
            Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,System.Label.LABEL_Error_DML +e));
			return null;
		}
	}

	public pageReference cancelClosedLost(){
		PageReference oppPage = new PageReference('/'+theOpp.Id);
		oppPage.setRedirect(true);
		return oppPage;		
	}

}