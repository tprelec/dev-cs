@SuppressWarnings('pmd')
public with sharing class OpportunityClosedWonController {
	private final static Set<String> MOBILE_PORTFOLIOS = new Set<String>{
		'Voice',
		'HHBD',
		'MBB',
		'M2M',
		'Enterprise Services',
		'Mobile Hardware',
		'Hardware',
		'Business Managed Service'
	};
	private final static Set<String> ZIGGO_PRODUCT_LINES = new Set<String>{
		'fZiggo Only',
		'fZiggo Xsell'
	};

	public Boolean errorFound { get; set; }
	public Opportunity theOpp { get; set; }

	public Boolean showBooleans { get; set; }
	public Boolean lightningAlreadyClosed { get; private set; }
	public Boolean showClosedWonWithRedirect { get; private set; }
	@testVisible
	public Boolean showClosedWonWithEmail { get; private set; }
	public Boolean showMixedProductWarning { get; private set; }
	public Boolean showClosingBySAGMessage { get; private set; }
	public Boolean showMissingTypeInfo { get; private set; }
	public Boolean nonMobileNonStandardFound { get; private set; }
	public Date minClosedDate { get; private set; }
	public String closeDateErrorLabel { get; private set; }

	// Reusable query so that it can be used by other code e.g. opportunity trigger
	// This is the Opportunity query for order validations on closed/won
	public static List<Opportunity> fetchOppList(Set<Id> oppids) {
		String query = 'SELECT Id';
		// Get the fields from OrderValidationField__c
		for (OrderValidationField__c ovf : OrderValidationField__c.getAll().values()) {
			if (ovf.object__c == 'Opportunity') {
				query += ',' + ovf.Field__c;
			}
		}
		query += ' FROM Opportunity WHERE Id = :oppIds';
		return Database.query(String.escapeSingleQuotes(query));
	}

	public OpportunityClosedWonController(ApexPages.StandardController controller) {
		Id oppId = controller.getId();
		if (oppId == null) {
			oppId = ApexPages.currentPage().getParameters().get('opportunityId');
		}
		theOpp = fetchOppList(new Set<Id>{ oppId })[0];
		showBooleans = ApexPages.currentPage().getParameters().get('showBooleans') == '1';
		// Validate
		validateOpportunity();
		// Close Winning an app, can only put date to today
		theOpp.CloseDate = Date.today();
		// Determine status of the opp and set booleans for screen rendering
		setBooleans();
	}
	@SuppressWarnings('CyclomaticComplexity')
	private void validateOpportunity() {
		errorFound = false;
		Boolean isBOP = checkBop(theOpp.Id);
		if (isBOP) {
			if (theOpp.BAN__c != null) {
				Boolean inValidBan = checkValidBan(theOpp.AccountId, theOpp.BAN__c);

				if (inValidBan) {
					// lightningAlreadyClosed = true;
					ApexPages.addMessage(
						new ApexPages.Message(ApexPages.Severity.ERROR, Label.ERROR_BAN)
					);
					errorFound = true;
				}
			}
		}

		// Check if opp is already closed
		if (theOpp.IsClosed || ApexPages.currentPage().getParameters().get('isClosed') == '1') {
			lightningAlreadyClosed = true;
			ApexPages.addMessage(
				new ApexPages.Message(ApexPages.Severity.ERROR, Label.ERROR_Closed_Opportunity)
			);
			errorFound = true;
		}
		// Check if non-sag user and closing by sag status
		if (
			theOpp.StageName == 'Closing By SAG' &&
			!Special_Authorizations__c.getInstance().Close_NonStandard_Opportunities__c
		) {
			ApexPages.addMessage(
				new ApexPages.Message(ApexPages.Severity.ERROR, Label.ERROR_Closing_By_SAG)
			);
			errorFound = true;
		}
		// Check if at least 1 site exists under the account (for non-bm opps)
		// Partners don't add fixed products so either fixed products (direct) or ispartner
		if ((theOpp.Hidden_Fixed_Products__c > 0 || oppOwnedByPartner) && accountSites.isEmpty()) {
			ApexPages.addMessage(
				new ApexPages.Message(
					ApexPages.Severity.ERROR,
					Label.ERROR_No_sites_exist_for_fixed_order
				)
			);
			errorFound = true;
		}
		// Run the 'advanced validations' (Order_Validation__c checks) as a pre-check
		String errors = OrderValidation.checkCompleteOpportunities(
			new List<Opportunity>{ theOpp },
			new Set<Id>{ theOpp.Id }
		);
		OpportunityTriggerHandler.checkedOppIds.add(theOpp.Id);
		if (errors != '') {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errors));
			errorFound = true;
		}
	}

	private List<Site__c> accountSites {
		get {
			if (accountSites == null) {
				accountSites = [SELECT Id FROM Site__c WHERE Site_Account__c = :theOpp.AccountId];
			}
			return accountSites;
		}
		private set;
	}

	public void loadContacts() {
		contacts = new List<ContactWrapper>();
		for (
			Contact c : [
				SELECT Id, Name, Title, Email, Received_Welcome_Email__c
				FROM Contact
				WHERE AccountId = :theOpp.accountId
				ORDER BY Authorized_to_sign__c DESC, LastName ASC
				LIMIT 500 // hardcoded limit here since Vodafone Account has many contacts
			]
		) {
			ContactWrapper cw = new ContactWrapper();
			cw.theContact = c;
			cw.selected = false;
			contacts.add(cw);
		}
	}

	public Boolean oppOwnedByPartner {
		get {
			return GeneralUtils.isPartnerUser(theOpp.Owner);
		}
		private set;
	}

	// Returns true if there are 1 or more Ziggo products on the Opportunity
	// When true, the Ziggo_Order_Reference__c field is enabled on the Closed/Won page and becomes mandatory
	public Boolean oppHasZiggoProduct {
		get {
			return (theOpp.Ziggo_Only_Products__c > 0 || theOpp.Ziggo_XSell_Products__c > 0);
		}
		private set;
	}

	private List<OpportunityLineItem> products {
		get {
			if (products == null) {
				products = [
					SELECT
						Id,
						Manually_Added_Product__c,
						PricebookEntry.ProductCode,
						PricebookEntry.Name,
						Quantity,
						Duration__c,
						UnitPrice,
						TotalPrice,
						Portfolio__c
					FROM OpportunityLineItem
					WHERE OpportunityId = :theOpp.Id
				];
			}
			return products;
		}
		set;
	}

	public List<OpportunityLineItem> manualProducts {
		get {
			if (manualProducts == null) {
				manualProducts = new List<OpportunityLineItem>();
				if (!theOpp.Confirm_Mixed_Opportunity__c) {
					for (OpportunityLineItem oli : products) {
						if (oli.Manually_Added_Product__c) {
							manualProducts.add(oli);
						}
					}
				}
			}
			return manualProducts;
		}
		private set;
	}

	private Boolean hasManualProducts {
		get {
			return !manualProducts.isEmpty();
		}
		set;
	}

	private Boolean hasNonManualProducts {
		get {
			return products.size() > 0 && products.size() > manualProducts.size();
		}
		private set;
	}

	public Boolean customerReferenceOptional {
		get {
			return theOpp.Account.GT_Fixed__c;
		}
		private set;
	}

	private Boolean isMacOpportunity {
		get {
			return theOpp.RecordType.Name == 'MAC';
		}
		private set;
	}

	private Boolean isOrganicGrowthOpportunity {
		get {
			return theOpp.Organic_Growth__c;
		}
		private set;
	}

	private Boolean hasEmailSigner {
		get {
			return (theOpp.Owner.Manager.Manager.Signature_Document_Id__c == null &&
				theOpp.Owner.Manager.Signature_Document_Id__c == null) ||
				theOpp.Vodafone_Products__c == 0;
		}
		private set;
	}

	private User oppOwner {
		get {
			if (oppOwner == null) {
				oppOwner = [SELECT Id FROM User WHERE Id = :theOpp.OwnerId];
			}
			return oppOwner;
		}
		private set;
	}

	private User currentUser {
		get {
			if (currentUser == null) {
				currentUser = [
					SELECT Id, Name, UserRole.Name
					FROM User
					WHERE Id = :UserInfo.getUserId()
				];
			}
			return currentUser;
		}
		private set;
	}

	// Determine the status of the opp in order to determine which screen(s) to show
	private void setBooleans() {
		showMixedProductWarning = false;
		showMissingTypeInfo = false;
		showClosedWonWithRedirect = false;
		showClosedWonWithEmail = false;
		// Check if user is not partner. shoud not go off if parther user
		if (
			oppOwner.Id != null &&
			!GeneralUtils.isPartnerUser(theOpp.Owner) &&
			theOpp.BAN__c != null &&
			(theOpp.BAN__r.Unify_Customer_Type__c == null ||
			theOpp.BAN__r.Unify_Customer_SubType__c == null ||
			theOpp.Account.Unify_Account_Type__c == null ||
			theOpp.Account.Unify_Account_SubType__c == null)
		) {
			showMissingTypeInfo = true;
			return;
		}
		// Set the min closed date based on custom settings start
		minClosedDate = System.today();
		Integer numberOfDays = 150000; //some big number, this is more than 421 years
		// If it's organic growth opp, keep number of days huge so it never triggers, else get settings and do as normal
		if (
			!isOrganicGrowthOpportunity &&
			Sales_Settings__c.getInstance().Number_of_Days_for_Closed_Won_Date__c != null
		) {
			numberOfDays = Integer.valueOf(
				Sales_Settings__c.getInstance().Number_of_Days_for_Closed_Won_Date__c
			);
		}
		// Set the min closed date based on custom settings end
		minClosedDate = minClosedDate.addDays(0 - numberOfDays);
		closeDateErrorLabel = String.format(
			System.Label.ERROR_Close_Date_Not_Allowed,
			new List<String>{ String.valueOf(numberOfDays) }
		);

		// Check if opp contains manually added lines and/or bm (cpq) lines. If mixed opp, the user needs to confirm that first
		if (hasManualProducts && hasNonManualProducts && !theOpp.Confirm_Mixed_Opportunity__c) {
			showMixedProductWarning = true;
			return;
		}

		// Determine if this is a non-standard non-mobile non-ziggo config
		checkMobileNonStandard();

		// For non standard we show the closing by sag message in three situations
		// 1 - The user is not authorised to close non standard opps
		// 2 - Its a MAC or Default and has no Ziggo products (it needs to become MAC)
		// 3 - It has Ziggo products and is not in the stagename of Closing By SAG (it needs go to that stage)
		showClosingBySAGMessage =
			nonMobileNonStandardFound &&
			(!Special_Authorizations__c.getInstance().Close_NonStandard_Opportunities__c ||
			(!isMacOpportunity && !oppHasZiggoProduct) ||
			(oppHasZiggoProduct && theOpp.StageName != 'Closing By SAG'));
		if (showClosingBySAGMessage) {
			return;
		}

		// If no errors are found, determine which screen to show next
		if (oppOwnedByPartner || hasEmailSigner) {
			showClosedWonWithRedirect = true;
		} else {
			// Determine the welcome email signer. If there isn't any, skip the email screen
			showClosedWonWithEmail = true;
			loadContacts();
			// Dummy Contact is used for capturing the date on which the mail should be sent out
			dummyContact = new Contact(Welcome_Email_Date__c = System.today());
			checkNoneSelected();
			showQuickNewContact = false;
		}
	}

	private void checkMobileNonStandard() {
		nonMobileNonStandardFound = false;
		if (!theOpp.No_contract_required__c) {
			if (products.isEmpty()) {
				// Opps without lineitems but with project comments will become MAC opps (closing by SAG)
				nonMobileNonStandardFound =
					theOpp.Project_Comments__c != null &&
					theOpp.Project_Comments__c != '';
			} else {
				for (OpportunityLineItem oli : products) {
					if (
						oli.Manually_Added_Product__c &&
						!MOBILE_PORTFOLIOS.contains(oli.Portfolio__c)
					) {
						nonMobileNonStandardFound = true;
						break;
					}
				}
			}
		}
	}

	public PageReference closeAndRedirect(Boolean isSAGClosing) {
		if (
			theOpp.Solution_Sales__c == null &&
			theOpp.Department__c.contains('Indirect') &&
			theOpp.Deal_Type__c != 'Cancellation'
		) {
			ApexPages.AddMessage(
				new ApexPages.Message(
					ApexPages.Severity.ERROR,
					'Please specify the solution sales contact'
				)
			);
			return null;
		}

		Savepoint sp = Database.setSavepoint();
		try {
			if (theOpp.StageName == 'Closing By SAG') {
				if (
					currentUser.UserRole.Name.contains('Sales Assistance Group') ||
					currentUser.UserRole.Name.contains('MLE')
				) {
					theOpp.StageName = 'Closed Won';
				} else {
					ApexPages.AddMessage(
						new ApexPages.Message(
							ApexPages.Severity.ERROR,
							'Only Inside Sales can close this opportunity'
						)
					);
					return null;
				}
			} else {
				if (currentUser.UserRole.Name.contains('MLE')) {
					theOpp.StageName = isSAGClosing ? 'Closing By SAG' : 'Closed Won';
				} else {
					calculateOpportunityStage();
				}
			}
			if (isSAGClosing) {
				theOpp.Organic_Growth__c = false;
			}
			update theOpp;
			return redirect(isSAGClosing);
		} catch (Exception e) {
			Database.rollback(sp);
			ApexPages.AddMessage(
				new ApexPages.Message(
					ApexPages.Severity.ERROR,
					System.Label.LABEL_Error_DML + e.getMessage(),
					''
				)
			);
			return null;
		}
	}

	private void calculateOpportunityStage() {
		if (hasManualProducts) {
			if (oppHasZiggoProduct) {
				theOpp.StageName = 'Closing By SAG';
			}
			if (nonMobileNonStandardFound && !oppHasZiggoProduct) {
				theOpp.StageName = 'Closing By SAG';
				theOpp.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('MAC');
			}
			if (!nonMobileNonStandardFound && !oppHasZiggoProduct) {
				theOpp.StageName = 'Closed Won';
			}
		} else {
			theOpp.StageName = 'Closed Won';
		}
		if (theOpp.No_contract_required__c && oppHasZiggoProduct) {
			theOpp.StageName = 'Closed Won';
		}
		if (
			theOpp.Project_Comments__c != null &&
			theOpp.Special_project_type__c != null &&
			theOpp.Department__c.contains('Indirect') &&
			theOpp.BigMachines__Line_Items__c == 0
		) {
			theOpp.StageName = 'Closing By SAG';
			theOpp.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('MAC');
		}
	}

	private PageReference redirect(Boolean isSAGClosing) {
		PageReference pageRef;
		// Handle opps for which no contract is required
		if (theOpp.No_contract_required__c) {
			if (PP_LightningController.isLightningCommunity()) {
				pageRef = ApexPages.currentPage();
				pageRef.getParameters().put('opportunityId', theOpp.Id);
				pageRef.getParameters().put('isClosed', '1');
				pageRef.setRedirect(true);
			} else {
				pageRef = new PageReference('/' + theOpp.Id);
				pageRef.setRedirect(true);
			}
			return pageRef;
		}
		// Validate Contract
		VF_Contract__c[] vfcs;
		if (theOpp.StageName != 'Closing By SAG') {
			vfcs = [
				SELECT Id
				FROM VF_Contract__c
				WHERE Opportunity__c = :theOpp.Id
				ORDER BY CreatedDate DESC
			];
			if (vfcs.isEmpty()) {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.Severity.ERROR,
						'Opportunity was Closed but Contract cannot be found. You might not be authorized to view it. Please contact your administrator'
					)
				);
				return null;
			}
		}
		// Email
		handleContactsAndEmail();
		// Redirect
		if (PP_LightningController.isLightningCommunity()) {
			if (theOpp.StageName != 'Closing By SAG' && vfcs != null) {
				pageRef = new PageReference('/apex/OpportunityContract?contractId=' + vfcs[0].Id);
				pageRef.getParameters().put('opportunityId', theOpp.Id);
			} else {
				pageRef = ApexPages.currentPage();
				pageRef.getParameters().put('opportunityId', theOpp.Id);
				pageRef.getParameters().put('isClosed', '1');
			}
		} else {
			if (theOpp.StageName != 'Closing By SAG' && vfcs != null) {
				pageRef = new PageReference('/apex/OpportunityContract?contractId=' + vfcs[0].Id);
			} else {
				pageRef = new PageReference('/' + theOpp.Id);
			}
		}
		pageRef.setRedirect(true);
		return pageRef;
	}

	private void handleContactsAndEmail() {
		if (!showClosedWonWithEmail) {
			return;
		}
		// Update the fields on contact
		List<Contact> contactsToUpdate = new List<Contact>();
		for (ContactWrapper cw : contacts) {
			if (cw.selected) {
				// determine the welcome email signer
				if (theOpp.Owner.Manager.Manager.Signature_Document_Id__c != null) {
					cw.theContact.Welcome_Email_Signer__c = theOpp.Owner.Manager.ManagerId;
				} else if (theOpp.Owner.Manager.Signature_Document_Id__c != null) {
					cw.theContact.Welcome_Email_Signer__c = theOpp.Owner.ManagerId;
				}
				cw.theContact.Welcome_Email_Date__c = dummyContact.Welcome_Email_Date__c;
				cw.theContact.Received_Welcome_Email__c = !(dummyContact.Welcome_email_Date__c >
				System.today());
				contactsToUpdate.add(cw.theContact);
			}
		}
		update contactsToUpdate;

		if (dummyContact.Welcome_email_date__c == System.today()) {
			// Send the welcome emails immediately
			sendWelcomeEmail(contacts, ccToCurrentUser, theOpp.Id);
		}
	}

	public PageReference confirmClosingBySAG() {
		return closeAndRedirect(true);
	}

	public PageReference confirmClosedWon() {
		if (showClosedWonWithEmail && dummyContact.Welcome_Email_Date__c < System.today()) {
			ApexPages.AddMessage(
				new ApexPages.Message(
					ApexPages.Severity.ERROR,
					'You cannot select a date before today'
				)
			);
			return null;
		}
		return closeAndRedirect(false);
	}

	public PageReference confirmMixedOpportunity() {
		if (PP_LightningController.isLightningCommunity()) {
			sendMessageToParentWindowToCloseFrame();
			return null;
		} else {
			update theOpp;
			setBooleans();
			return null;
		}
	}

	public PageReference confirmMissingType() {
		update theOpp;
		SharingUtils.updateRecordsWithoutSharing(theOpp.Account);
		SharingUtils.updateRecordsWithoutSharing(theOpp.Ban__r);
		// Requery to get the correct latest status
		theOpp = fetchOppList(new Set<Id>{ theOpp.Id })[0];
		setBooleans();
		return null;
	}

	public PageReference cancelClosedWon() {
		if (PP_LightningController.isLightningCommunity()) {
			sendMessageToParentWindowToCloseFrame();
			return null;
		} else {
			PageReference oppPage = new PageReference('/' + theOpp.Id);
			oppPage.setRedirect(true);
			return oppPage;
		}
	}

	public Contact quickNewContact { get; set; }
	public Boolean showQuickNewContact { get; private set; }

	public PageReference createNewContact() {
		quickNewContact = new Contact(AccountId = theOpp.AccountId);
		showQuickNewContact = true;
		return null;
	}

	public PageReference saveQuickNewContact() {
		try {
			insert quickNewContact;
			// requery to get the correct Name
			quickNewContact = [
				SELECT Id, Name, Title, Email, Received_Welcome_Email__c
				FROM Contact
				WHERE Id = :quickNewContact.Id
			];
			// put in wrapper and add to contact list
			ContactWrapper cw = new ContactWrapper();
			cw.theContact = quickNewContact;
			cw.selected = false;
			contacts.add(cw);

			showQuickNewContact = false;
		} catch (dmlException de) {
			ApexPages.addMessages(de);
		}
		return null;
	}

	public PageReference backToForm() {
		showQuickNewContact = false;
		return null;
	}

	public Id templateId {
		get {
			if (templateId == null) {
				EmailTemplate template = [
					SELECT Id
					FROM EmailTemplate
					WHERE DeveloperName = 'Welcome_Email_EBU'
				];
				templateId = template.Id;
			}
			return templateId;
		}
		private set;
	}

	public String getBaseUrl() {
		return System.URL.getSalesforceBaseUrl().toExternalForm();
	}

	public String getContentUrl() {
		return System.URL.getSalesforceBaseUrl()
			.toExternalForm()
			.replace('.visualforce.com', '.documentforce.com');
	}

	public void sendWelcomeEmail(
		List<ContactWrapper> contacts,
		Boolean ccToCurrentUser,
		Id opportunityId
	) {
		List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
		for (ContactWrapper cw : contacts) {
			if (cw.selected) {
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setwhatid(opportunityId);
				String[] ccAddresses = new List<String>{};
				if (ccToCurrentUser) {
					ccAddresses.add(GeneralUtils.currentUser.Email);
				}
				mail.setTargetObjectId(cw.theContact.Id);
				mail.setCCAddresses(ccAddresses);
				mail.setReplyTo(GeneralUtils.currentUser.Email);
				mail.setSenderDisplayName(GeneralUtils.currentUser.Name);
				mail.setUseSignature(false);
				mail.setSaveAsActivity(true);
				mail.setTemplateID(templateId);
				messages.add(mail);
			}
		}
		Messaging.sendEmail(messages);
	}

	public Boolean ccToCurrentUser { get; set; }
	public List<ContactWrapper> contacts { get; set; }
	public Contact dummyContact { get; set; }

	public class ContactWrapper {
		public Boolean selected { get; set; }
		public Contact theContact { get; set; }
	}

	public Boolean noneSelected { get; private set; }

	public pageReference checkNoneSelected() {
		for (ContactWrapper cw : contacts) {
			if (cw.selected) {
				noneSelected = false;
				return null;
			}
		}
		noneSelected = true;
		return null;
	}

	public String scriptToRun { get; set; }

	public void sendMessageToParentWindowToCloseFrame() {
		scriptToRun =
			'<script>' +
			'var dirtyFrame = new DirtyFrame( PARENT_WINDOW_ORIGIN );' +
			'dirtyFrame.sendClosePageMessage( PAGE_NAME );' +
			'</script>';
	}

	private Boolean checkBop(string opptyId) {
		Boolean isBop = false;

		List<OpportunityLineItem> opptyLineItemList = [
			SELECT Id, OrderType__c, OrderType__R.Id, OrderType__r.ExportSystem__c, OpportunityId
			FROM OpportunityLineItem
			WHERE OpportunityId = :opptyId
		];

		if (!opptyLineItemList.isEmpty()) {
			for (OpportunityLineItem oli : opptyLineItemList) {
				if (oli.OrderType__c != null && oli.OrderType__r.ExportSystem__c == 'BOP') {
					isBop = true;
				}
			}
		}
		return isBop;
	}

	private Boolean checkValidBan(string accountId, string banId) {
		Boolean isPair = false;
		Set<String> excludedStatuses = new Set<String>{ 'Closed' };
		map<String, String> noOfPair = new Map<String, String>();
		List<Ban__c> visibleBanList = [
			SELECT
				Id,
				Name,
				Ban_Number__c,
				BAN_Status__c,
				Account__c,
				BOPCode__c,
				Has_Fixed__c,
				ExternalAccount__c,
				ExternalAccount__r.External_Account_Id__c,
				ExternalAccount__r.Related_External_Account__c,
				ExternalAccount__r.Related_External_Account__r.External_Account_Id__c
			FROM Ban__c
			WHERE Account__c = :accountId AND BAN_Status__c NOT IN :excludedStatuses
		];

		if (!visibleBanList.isEmpty()) {
			for (ban__c b : visibleBanList) {
				{
					if (
						(b.ExternalAccount__c != null &&
						b.ExternalAccount__r.External_Account_Id__c != null) &&
						(b.ExternalAccount__r.Related_External_Account__c != null &&
						b.ExternalAccount__r.Related_External_Account__r.External_Account_Id__c !=
						null)
					) {
						noOfPair.put(
							b.ExternalAccount__r.External_Account_Id__c,
							b.ExternalAccount__r.Related_External_Account__r.External_Account_Id__c
						);
					}
				}
			}
		}

		ban__c ban = [
			SELECT Id, ExternalAccount__c, ExternalAccount__r.External_Account_Id__c
			FROM ban__c
			WHERE Id = :banId
			LIMIT 1
		];

		if (
			!noOfPair.isEmpty() &&
			ban.ExternalAccount__c != null &&
			ban.ExternalAccount__r.External_Account_Id__c != null &&
			!noOfPair.containsKey(ban.ExternalAccount__r.External_Account_Id__c)
		) {
			isPair = true;
		}

		return isPair;
	}
}