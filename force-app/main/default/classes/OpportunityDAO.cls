public inherited sharing class OpportunityDAO {
	/**
	 * Retrieve the Opportunity details based on Set of Opportunity Ids
	 */
	public static Map<Id, Opportunity> getOpportunityDetailsBasedOnId(
		Set<String> fieldSet,
		Set<Id> opportunityIdSet
	) {
		String query = String.format(
			'SELECT {0} FROM Opportunity WHERE Id IN: opportunityIdSet WITH SECURITY_ENFORCED',
			new List<String>{ String.join(new List<String>(fieldSet), ',') }
		);
		return new Map<Id, Opportunity>(
			(List<Opportunity>) Database.query(query)
		);
	}
}