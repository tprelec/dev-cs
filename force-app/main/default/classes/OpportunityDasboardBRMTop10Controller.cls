public with sharing class OpportunityDasboardBRMTop10Controller {
	
	public OpportunityDasboardBRMTop10Controller() {
	}

	public Decimal theTop10Total {get;set;}
	public String clc {get;set;}
	public String stageType {get;set;}
    public String dataTypeSelected {get;set;}
	public String accountManagerSelected {get;set;}
	public String fiscalQuarterSelected {get;set;}

	public String verticalSelected {get;set;}
	public String fiscalYearSelected {get;set;}
	public String specialistSegmentSelected {get;set;}
    public Set<String> specialistRoles {
        get{
            if(specialistRoles == null){
                specialistRoles = GeneralUtils.segmentToRoles.get(specialistSegmentSelected);
            }
            return specialistRoles;
        }
        set;
    }
	public List<String> visibleRoleNames {get;set;}


    private String filterChecker {get;set;}


    transient List<OpportunityDashboardBRMTop10EntryClass> currenttop10;
    public List<OpportunityDashboardBRMTop10EntryClass> getCurrenttop10(){

        // only rerun the query if the filters have changed
        String newFilterChecker = fiscalYearSelected+fiscalQuarterSelected+dataTypeSelected+verticalSelected+specialistSegmentSelected+accountManagerSelected+visibleRoleNames;

        if(filterChecker == null || filterChecker != newFilterChecker){
            
            filterChecker = newFilterChecker;
            currenttop10 = new List<OpportunityDashboardBRMTop10EntryClass>();
            system.debug(fiscalYearSelected);
            system.debug(dataTypeSelected);
        	if(fiscalYearSelected != null && dataTypeSelected != null ){
        		
                Id currentUserId = UserInfo.getUserId();
                Integer fiscalYear = Integer.valueOf(fiscalYearSelected);
                Integer fiscalQuarter = fiscalQuarterSelected==null?0:Integer.valueOf(fiscalQuarterSelected);

                String oppLiQuery = 'SELECT Opportunity.Account.Name AccountName, Opportunity.RoleCreateOrChangeOwner__c ownerrole, Opportunity.AccountId AccountId, ';
                if(dataTypeSelected=='CTN' || dataTypeSelected=='License') {
                    oppLiQuery += 'SUM(Quantity) SumAmount'; 
                } else if(dataTypeSelected=='MRR'){
                    oppLiQuery += 'SUM(MRR1__c) SumAmount';
                } else {
                    oppLiQuery += 'SUM(TotalPrice) SumAmount';
                }

                oppLiQuery += ' FROM OpportunityLineItem oli WHERE ';
                oppLiQuery += ' CLC__c =:clc ';
                if(specialistSegmentSelected == null){
                    oppLiQuery += ' AND (Opportunity.RoleCreateOrChangeOwner__c in :visibleRoleNames OR Opportunity.Owner.Id = :currentUserId) ';
                } else if (specialistSegmentSelected == 'Cloud Productivity Solution Sales'){
                    oppLiQuery += ' AND Opportunity.Hidden_Microsoft_Solution_Specialist__c != null ';
                } else if (specialistSegmentSelected == 'Enterprise Services Solution Sales'){
                    oppLiQuery += ' AND Opportunity.Hidden_Ent_Services_Solution_Specialist__c != null ';         
                } else if (specialistSegmentSelected == 'Partner Sales'){
                    oppLiQuery += ' AND OpportunityId in (Select OpportunityId From OpportunityPartner Where IsPrimary = true) ';         
                } else {
                    oppLiQuery += ' AND Opportunity.RoleCreateOrChangeSolutionSales__c IN :specialistRoles ';
                } 

                oppLiQuery += ' AND FISCAL_YEAR(Opportunity.CloseDate) = :fiscalYear ';
                if(fiscalQuarterSelected != '0' && fiscalQuarterSelected != null){
                    oppLiQuery += ' AND FISCAL_QUARTER(Opportunity.CloseDate) = :fiscalQuarter ';
                }
                if(verticalSelected != 'all' && verticalSelected != null){
                    oppLiQuery += ' AND Opportunity.RoleCreateOrChangeOwner__c LIKE \'%'+verticalSelected+'%\' '; 
                }
                if(accountManagerSelected != 'all' && accountManagerSelected != null){
                    oppLiQuery += ' AND Opportunity.OwnerId = \'' + accountManagerSelected + '\' ';
                }        
                oppLiQuery += ' AND Opportunity.StageName IN :typeToStages ';

                oppLiQuery += ' GROUP BY Opportunity.AccountId, Opportunity.Account.Name,Opportunity.RoleCreateOrChangeOwner__c ';
                if(dataTypeSelected=='CTN' || dataTypeSelected=='License') {
                    oppLiQuery += ' ORDER BY SUM(Quantity) DESC ';
                } else if(dataTypeSelected=='MRR'){
                    oppLiQuery += ' ORDER BY SUM(MRR1__c) DESC ';
                } else {
                    oppLiQuery += ' ORDER BY SUM(TotalPrice) DESC ';
                }        
                
                oppLiQuery += ' LIMIT 10';

                system.debug(oppLiQuery);

                for (AggregateResult ar : Database.query(oppLiQuery)){
                    OpportunityDashboardBRMTop10EntryClass te = new OpportunityDashboardBRMTop10EntryClass();
                    te.customer = (String)ar.get('AccountName');
                    //te.vertical = OpportunityDashboardUtils.roleToVertical(opp.RoleCreateOrChangeOwner__c);
                    String role = (String)ar.get('ownerrole');
                    if(GeneralUtils.roleToSegment.get(role)==null){
                        if(role.endsWith('Partner User')){
                            te.vertical = 'Business Partners';
                        } else {
                            te.vertical = 'Other';
                        }
                    } else {
                        te.vertical = GeneralUtils.roleToSegment.get(role);
                    }             
                    te.accId = (Id)ar.get('AccountId');
                    te.amount = (Decimal)ar.get('SumAmount');
                    //te.oppId = opp.Id;
                    currenttop10.add(te);
                }

            	theTop10Total = 0;
        		for(OpportunityDashboardBRMTop10EntryClass top10Entry : currenttop10){
        			theTop10Total += top10Entry.amount;
        		}
            }
        }
    	return currenttop10;

    }	

    public Set<String> typeToStages{
    	get{
	    	if(stageType=='Funnel'){
	    		return new Set<String>{'Prospect','Qualify'};
	    	} else {
	    		return new Set<String>{stageType};
	    	}
	    }
	    set;
    }

  
}