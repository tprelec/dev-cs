public with sharing class OpportunityDasboardBRMTop10PanelCont {
	
	public OpportunityDasboardBRMTop10PanelCont() {
	}

	public Decimal theTop10Total {get;set;}
	public String clc {get;set;}
	public String stageType {get;set;}
    public String dataTypeSelected {get;set;}
	public String accountManagerSelected {get;set;}
	public String fiscalQuarterSelected {get;set;}
	public String verticalSelected {get;set;}
	public String fiscalYearSelected {get;set;}
	public String specialistSegmentSelected {get;set;}
    public Set<String> specialistRoles {
        get{
            if(specialistRoles == null){
                specialistRoles = GeneralUtils.segmentToRoles.get(specialistSegmentSelected);
            }
            return specialistRoles;
        }
        set;
    }
	public List<String> visibleRoleNames {get;set;}

    public String currentFiscalYear {
        get{
            if(currentFiscalYear==null) currentFiscalYear = [SELECT FiscalYearSettings.Name FROM Period WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY].FiscalYearSettings.Name;       
            return currentFiscalYear;           
        }
        set;
    }

    public List<SelectOption> getFiscalYearOptions(){
        List<SelectOption> fiscalYears = new List<SelectOption>();
        String previousFiscalYear = String.valueOf(Integer.valueOf(currentFiscalYear)-1);
        fiscalYears.add(New SelectOption(previousFiscalYear,previousFiscalYear));
        fiscalYears.add(New SelectOption(currentFiscalYear,currentFiscalYear));
        String nextFiscalYear = String.valueOf(Integer.valueOf(currentFiscalYear)+1);
        fiscalYears.add(New SelectOption(nextFiscalYear,nextFiscalYear));

        return fiscalYears;
    }

    public List<SelectOption> getVerticalOptions() {
        List<SelectOption> returnList = new List<SelectOption>();
        returnList.add(new SelectOption('all','-All-'));

        Set<String> verticals = new Set<String>();
        if(visibleRoleNames != null){
            for(String s : visibleRoleNames){
                s = OpportunityDashboardUtils.roleToVertical(s);
                verticals.add(s);
            }
        }
        for(String s : verticals){
            returnList.add(new SelectOption(s,s));
        }
        return returnList;

    }       

    public List<SelectOption> getAccountManagerOptions() {
        List<SelectOption> returnList = new List<SelectOption>();
        returnList.add(new SelectOption('all','-All-'));
        if(visibleRoleNames != null){
            for(User u : [Select Id, Name From User Where UserRole.Name in :visibleRoleNames order by Name]){
                returnList.add(new SelectOption(u.Id,u.Name));
            }
        }
        return returnList;

    }    
}