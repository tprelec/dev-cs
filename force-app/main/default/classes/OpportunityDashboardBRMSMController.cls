public without sharing class OpportunityDashboardBRMSMController {

	public Set<Id> visibleRoleIds {get;set;}
	public List<String> visibleRoleNames {get;set;}
	public Integer fiscalYearInteger {get;set;}
	public Boolean variablesInitialized {get;set;}
    public Boolean showSecondaryTabs{get;set;}
    public void activateSecondaryTabs(){
        showSecondaryTabs = true;
    }

    // these are used in dynamic query syntax so need to be explicitly declared locally
    public List<String> solSalesProductFamilies = OpportunityDashboardUtils.solSalesProductFamilies;
    public List<String> ctnProductFamilies = OpportunityDashboardUtils.ctnProductFamilies;
    public List<String> enterpriseServiceLicenseProductSubFamilies = OpportunityDashboardUtils.enterpriseServiceLicenseProductSubFamilies;

	public String currentFiscalYear {
		get{
			if(currentFiscalYear==null) currentFiscalYear = [SELECT FiscalYearSettings.Name FROM Period WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY].FiscalYearSettings.Name;
			return currentFiscalYear;
		}
		set;
	}
	public String fiscalYearSelected {
		get{
			// initialize with current fiscal year
			if(fiscalYearSelected==null) fiscalYearSelected = currentFiscalYear;
			return fiscalYearSelected;
		}
		set;
	}
	public String dataTypeSelected {
		get{
			// initialize with TCV
			if(dataTypeSelected==null) dataTypeSelected = 'MRR';
			return dataTypeSelected;
		}
		set;
	}
	public Set<Id> visibleSalesManagerRoleIds {
		get{
			if(visibleSalesManagerRoleIds == null){
				// get current user's manager
				Id currentUserManager = [Select Id, ParentRoleId From UserRole Where Id = :UserInfo.getUserRoleId()].ParentRoleId;
				// if the current user's role has no parent, use the current user's role (probably admin)
				if(currentUserManager == null){
					currentUserManager = UserInfo.getUserRoleId();
				}

				// get all heads of roles
				//Set<Id> headOfRoleIds = GeneralUtils.getAllHeadsOfRoleIds();

				// get all sales manager roles
				/*Set<Id> salesManagerRoleIds = new Set<Id>();
				for(UserRole smRole : [Select Id From UserRole Where ParentRoleId in :headOfRoleIds]){
					salesManagerRoleIds.add(smRole.Id);
				}*/
                Set<Id> salesManagerRoleIds = GeneralUtils.getAllSalesManagerRoleIds();

				system.debug(currentUserManager);
				//system.debug(headOfRoleIds);
				system.debug(salesManagerRoleIds);

				// determine which roles the current user's manager can see
				Set<Id> visibleRoleIdsParent = GeneralUtils.getAllSubRoleIds(new Set<Id>{currentUserManager});
				// and add the manager role itself
				visibleRoleIdsParent.add(currentUserManager);
				system.debug(visibleRoleIdsParent);

				visibleRoleIdsParent.retainAll(salesManagerRoleIds);
				visibleSalesManagerRoleIds = visibleRoleIdsParent;
				system.debug(visibleSalesManagerRoleIds);

			}
			return visibleSalesManagerRoleIds;
		}
		set;
	}
	public String runAsUserRoleIdSelected {
		get{
			// initialize with current user's role
			if(runAsUserRoleIdSelected==null) {

				// if the current use is a salesmgr, default to that view. Else default to the first salesmanager
				if(visibleSalesManagerRoleIds.contains(UserInfo.getUserRoleId())){
					runAsUserRoleIdSelected = UserInfo.getUserRoleId();
				} else {
					for(Id urId : visibleSalesManagerRoleIds){
						runAsUserRoleIdSelected = urId;
						break;
					}
				}
			}
			return runAsUserRoleIdSelected;
		}
		set;
	}

	public OpportunityDashboardBRMSMController() {

        //showSecondaryTabs = false;
        dashboardTypeSelected = 'salesmanager';

        // get stored values from cookie in order to continue with the view that the user used last
        Cookie brmSettings = ApexPages.currentPage().getCookies().get('brmSettingsSM');
        if (brmSettings == null) {
            brmSettings = new Cookie('brmSettingsSM', OpportunityDashboardUtils.getBrmSettingCookieString(dashboardTypeSelected, runAsUserRoleIdSelected, fiscalYearSelected, dataTypeSelected), null, -1, true, 'Strict');
        } else {
            String[] cookieData = OpportunityDashboardUtils.getBrmSettingCookie(brmSettings.getValue());
            system.debug(cookieData);
            try{
                if(cookieData.size() == 4 && cookieData[0] == dashboardTypeSelected) {
                    system.debug(cookieData);
                    if(cookieData[1] != null && cookieData[1] != 'null') runAsUserRoleIdSelected = cookieData[1];
                    if(cookieData[2] != null && cookieData[2] != 'null') fiscalYearSelected = cookieData[2];
                    if(cookieData[3] != null && cookieData[3] != 'null') dataTypeSelected = cookieData[3];
                }
            } catch (Exception e){
                // if this fails for any reason, re-initiate the cookie
                brmSettings = new Cookie('brmSettingsDirector', OpportunityDashboardUtils.getBrmSettingCookieString(dashboardTypeSelected, runAsUserRoleIdSelected, fiscalYearSelected, dataTypeSelected), null, -1, true, 'Strict');
            }

        }

		loadDatapoints();
	}


	public void initializeVariables(){
		visibleRoleIds = GeneralUtils.getAllSubRoleIds(new Set<Id>{runAsUserRoleIdSelected});
		visibleRoleIds.add(runAsUserRoleIdSelected);
		visibleRoleNames = new List<String>(GeneralUtils.getAllSubRoleNames(new Set<Id>{runAsUserRoleIdSelected}));
		visibleRoleNames.add(GeneralUtils.RoleIdToRoleName.get(runAsUserRoleIdSelected));
		fiscalYearInteger = Integer.valueOf(fiscalYearSelected);

		variablesInitialized = true;
	}

	public pageReference loadDatapoints(){
        showSecondaryTabs = false;

        initializeVariables();
        // reset the json strings so they will be requeried
        mainAcqDatapointsJSON = null;
        mainRetDatapointsJSON = null;
        solSalesDatapointsJSON = null;
        itwDatapointsJSON = null;
        msDatapointsJSON = null;

		system.debug(runAsUserRoleIdSelected);
		system.debug(visibleRoleNames);

        // update the cookie with the latest selection
        String cookieString = OpportunityDashboardUtils.getBrmSettingCookieString(dashboardTypeSelected,runAsUserRoleIdSelected,fiscalYearSelected,dataTypeSelected);
        Cookie brmSettings = new Cookie('brmSettingsSM', cookieString, null, -1, true, 'Strict');
        ApexPages.currentPage().setCookies(new Cookie[]{brmSettings});

		return null;
	}

	public String basicQueryFields{
		get{
			//if(basicQueryFields == null){
				basicQueryFields = 'SELECT CLC__c,';
				//basicQueryFields += 'Product_Family__c,';
                basicQueryFields += 'PricebookEntry.Product2.Taxonomy__r.Product_Family__c,';
				basicQueryFields += 'Opportunity.StageName,';
				//basicQueryFields += 'Opportunity.Segment__c,';
				basicQueryFields += 'FISCAL_YEAR(Opportunity.CloseDate) fy,';
				basicQueryFields += 'FISCAL_QUARTER(Opportunity.CloseDate) fq,';
				//basicQueryFields += 'SUM(TotalPrice) SumTotalPrice,';
				//basicQueryFields += 'SUM(Quantity) SumQuantity';

                if(dataTypeSelected=='CTN' || dataTypeSelected=='License') {
                    basicQueryFields += 'SUM(Quantity) SumAmount';//SumQuantity,';
                } else if(dataTypeSelected=='MRR'){
                    basicQueryFields += 'SUM(MRR1__c) SumAmount';//SumMRR,';
                } else {
                    basicQueryFields += 'SUM(TotalPrice) SumAmount';//SumTotalPrice,';
                }

			//}
			return basicQueryFields;
		}
		set;
	}

	public String basicQueryFilters{
		get{
			basicQueryFilters = ' FROM OpportunityLineItem WHERE ';
			//basicQueryFilters += 'Opportunity.StageName != \'Closed Lost\' AND ';
			basicQueryFilters += ' FISCAL_YEAR(Opportunity.CloseDate) =  :fiscalYearInteger';
            //basicQueryFilters += ' AND Opportunity.Account.VGE__c =  false ';
			basicQueryFilters += ' AND Opportunity.RoleCreateOrChangeOwner__c in :visibleRoleNames';
            if (dataTypeSelected == 'CTN'){
                basicQueryFilters += ' AND Product_Family__c IN :ctnProductFamilies'; // TAXONOMY HAZARD !!
            } else if (dataTypeSelected == 'License') {
                basicQueryFilters += ' AND PricebookEntry.Product2.Model__c IN :enterpriseServiceLicenseProductSubFamilies'; // TAXONOMY HAZARD !!
            }
			return basicQueryFilters;
		}
		set;
	}

	public String basicQueryGroupBy{
		get{
			if(basicQueryGroupBy == null){
				basicQueryGroupBy = ' GROUP BY CLC__c,';
				//basicQueryGroupBy += 'Product_Family__c,';
                basicQueryGroupBy += 'PricebookEntry.Product2.Taxonomy__r.Product_Family__c,';
				basicQueryGroupBy += 'Opportunity.StageName,';
				//basicQueryGroupBy += 'Opportunity.Segment__c,';
				basicQueryGroupBy += 'FISCAL_YEAR(Opportunity.CloseDate),';
				basicQueryGroupBy += 'FISCAL_QUARTER(Opportunity.CloseDate)';

			}
			return basicQueryGroupBy;
		}
		set;
	}

    public String mainAcqDatapointsSOQLFilter{
        get {
            // add filters to public string, so they can be reused in drilldown
            mainAcqDatapointsSOQLFilter = basicQueryFilters;
            //mainAcqDatapointsSOQLFilter += ' AND Opportunity.Owner.UserType != \'PowerPartner\'';
            mainAcqDatapointsSOQLFilter += ' AND (CLC__c = \'Acq\'  OR CLC__c = null)';
            return mainAcqDatapointsSOQLFilter;
        }
        set;
    }
    public String mainAcqDatapointsJSON {
        get{
            //if(mainAcqDatapointsJSON == null){
                if(!variablesInitialized) initializeVariables();

                // load opportunity lineitems for Main tab
                String mainAcqQuery = basicQueryFields;
                mainAcqQuery += ',Opportunity.Owner.Name AccountManager';
                // add filters
                mainAcqQuery += mainAcqDatapointsSOQLFilter;
                // add groupby
                mainAcqQuery += basicQueryGroupBy;
                mainAcqQuery += ',Opportunity.Owner.Name';
                system.debug(mainAcqQuery);
                //List<AggregateResult> mainAcqars = Database.query(mainAcqQuery);
                //mainAcqDatapointsJSON = JSON.serialize(generateDatapointList(Database.query(mainAcqQuery),'Acquisition Sales'));
            //}
            //return mainAcqDatapointsJSON;

            return JSON.serialize(generateDatapointList(mainAcqQuery,'Acquisition Sales'));
        }
        set;
    }

    public String mainRetDatapointsSOQLFilter{
        get {
            // add filters to public string, so they can be reused in drilldown
            mainRetDatapointsSOQLFilter = basicQueryFilters;
            //mainRetDatapointsSOQLFilter += ' AND Opportunity.Owner.UserType != \'PowerPartner\'';
            mainRetDatapointsSOQLFilter += ' AND CLC__c = \'Ret\' ';
            return mainRetDatapointsSOQLFilter;
        }
        set;
    }
    public String mainRetDatapointsJSON {
        get{
            //if(mainRetDatapointsJSON == null){
                if(!variablesInitialized) initializeVariables();

                // load opportunity lineitems for Main tab
                String mainRetQuery = basicQueryFields;
                mainRetQuery += ',Opportunity.Owner.Name AccountManager';
                // add filters
                mainRetQuery += mainRetDatapointsSOQLFilter;
                // add groupby
                mainRetQuery += basicQueryGroupBy;
                mainRetQuery += ',Opportunity.Owner.Name';
                system.debug(mainRetQuery);
                //List<AggregateResult> mainRetars = Database.query(mainRetQuery);
                mainRetDatapointsJSON = JSON.serialize(generateDatapointList(mainRetQuery,'Retention Sales'));
            //}
            return mainRetDatapointsJSON;
        }
        set;
    }


    public String solSalesDatapointsSOQLFilter{
        get {
            // add filters to public string, so they can be reused in drilldown
            solSalesDatapointsSOQLFilter = basicQueryFilters;
            //solSalesDatapointsSOQLFilter += ' AND Opportunity.Owner.UserType != \'PowerPartner\'';
            solSalesDatapointsSOQLFilter += ' AND (CLC__c = \'Acq\' OR CLC__c = null)';
            solSalesDatapointsSOQLFilter += ' AND Opportunity.Solution_Sales__c != null';
            solSalesDatapointsSOQLFilter += ' AND Product_Family__c in :solSalesProductFamilies'; // TAXONOMY HAZARD
            return solSalesDatapointsSOQLFilter;
        }
        set;
    }
    public String solSalesDatapointsJSON {
        get{
            //if(solSalesDatapointsJSON == null){
                if(!variablesInitialized) initializeVariables();

                // load opportunity lineitems for Main tab
                String solSalesQuery = basicQueryFields;
                solSalesQuery += ',Opportunity.Solution_Sales__r.Name AccountManager';
                // add filters
                solSalesQuery += solSalesDatapointsSOQLFilter;
                // add groupby
                solSalesQuery += basicQueryGroupBy;
                solSalesQuery += ',Opportunity.Solution_Sales__r.Name';
                system.debug(solSalesQuery);
                //List<AggregateResult> solSalesars = Database.query(solSalesQuery);

            //}
            return JSON.serialize(generateDatapointList(solSalesQuery,'Solution Sales'));
        }
        set;
    }

    public String itwDatapointsSOQLFilter{
        get {
            // add filters to public string, so they can be reused in drilldown
            itwDatapointsSOQLFilter = basicQueryFilters;
            //itwDatapointsSOQLFilter += ' AND Opportunity.Owner.UserType != \'PowerPartner\'';
            itwDatapointsSOQLFilter += ' AND (CLC__c = \'Acq\' OR CLC__c = null)';
            itwDatapointsSOQLFilter += ' AND Opportunity.Hidden_Inside_Sales_Specialist__c != null';
            return itwDatapointsSOQLFilter;
        }
        set;
    }
    public String itwDatapointsJSON {
        get{
            //if(itwDatapointsJSON == null){
                if(!variablesInitialized) initializeVariables();

                // load opportunity lineitems for Main tab
                String itwQuery = basicQueryFields;
                itwQuery += ',Opportunity.Hidden_Inside_Sales_Specialist__r.Name AccountManager';
                // add filters
                itwQuery += itwDatapointsSOQLFilter;
                // add groupby
                itwQuery += basicQueryGroupBy;
                itwQuery += ',Opportunity.Hidden_Inside_Sales_Specialist__r.Name';
                system.debug(itwQuery);
                //List<AggregateResult> itwars = Database.query(itwQuery);
                itwDatapointsJSON = JSON.serialize(generateDatapointList(itwQuery,'Inside Sales'));
            //}
            return itwDatapointsJSON;
        }
        set;
    }


    public String msDatapointsSOQLFilter{
        get {
            // add filters to public string, so they can be reused in drilldown
            msDatapointsSOQLFilter = basicQueryFilters;
            //msDatapointsSOQLFilter += ' AND Opportunity.Owner.UserType != \'PowerPartner\'';
            msDatapointsSOQLFilter += ' AND (CLC__c = \'Acq\' OR CLC__c = null)';
            msDatapointsSOQLFilter += ' AND Opportunity.Hidden_Microsoft_Solution_Specialist__c != null';
            // only display microsoft sols sales
            msDatapointsSOQLFilter += ' AND Product_Family__c = \'Acq Microsoft Solutions\' ';            // TAXONOMY HAZARD
            return msDatapointsSOQLFilter;
        }
        set;
    }
    public String msDatapointsJSON {
        get{
            //if(msDatapointsJSON == null){
                if(!variablesInitialized) initializeVariables();

                // load opportunity lineitems for Main tab
                String msQuery = basicQueryFields;
                msQuery += ',Opportunity.Hidden_Microsoft_Solution_Specialist__r.Name AccountManager';
                // add filters
                msQuery += msDatapointsSOQLFilter;
                // add groupby
                msQuery += basicQueryGroupBy;
                msQuery += ',Opportunity.Hidden_Microsoft_Solution_Specialist__r.Name';
                system.debug(msQuery);
                //List<AggregateResult> msars = Database.query(msQuery);
                msDatapointsJSON = JSON.serialize(generateDatapointList(msQuery,'Microsoft'));
            //}
            return msDatapointsJSON;
        }
        set;
    }

    public String miteDatapointsSOQLFilter{
        get {
            // add filters to public string, so they can be reused in drilldown
            miteDatapointsSOQLFilter = basicQueryFilters;
            //miteDatapointsSOQLFilter += ' AND Opportunity.Owner.UserType != \'PowerPartner\'';
            miteDatapointsSOQLFilter += ' AND (CLC__c = \'Acq\' OR CLC__c = null)';
            //miteDatapointsSOQLFilter += ' AND Opportunity.Hidden_Ent_Services_Solution_Specialist__c != null';
            // only display microsoft sols sales
            miteDatapointsSOQLFilter += ' AND Product_Family__c = \'Acq Enterprise Services\' ';  // TAXONOMY HAZARD
            return miteDatapointsSOQLFilter;
        }
        set;
    }
    public String miteDatapointsJSON {
        get{
            //if(miteDatapointsJSON == null){
                if(!variablesInitialized) initializeVariables();

                // load opportunity lineitems for Main tab
                String miteQuery = basicQueryFields;
                miteQuery += ',PricebookEntry.Product2.Model__c SubFamily ';
                miteQuery += ',Opportunity.Hidden_Ent_Services_Solution_Specialist__r.Name AccountManager';

                // add filters
                miteQuery += miteDatapointsSOQLFilter;
                // add groupby
                miteQuery += basicQueryGroupBy;
                miteQuery += ',PricebookEntry.Product2.Model__c ';
                miteQuery += ',Opportunity.Hidden_Ent_Services_Solution_Specialist__r.Name';

                system.debug(miteQuery);
                //List<AggregateResult> mitears = Database.query(miteQuery);
                miteDatapointsJSON = JSON.serialize(generateDatapointList(miteQuery,'Mite Segment'));
            //}
            return miteDatapointsJSON;
        }
        set;
    }


	public List<Datapoint> generateDatapointList(String query, String targetType){
		List<Datapoint> datapoints = new List<Datapoint>();
		transient Map<String,Map<String,Map<String,Map<String,Map<String,Datapoint>>>>> datapointMap = new Map<String,Map<String,Map<String,Map<String,Map<String,Datapoint>>>>>();

        if(Test.isRunningTest()) query += ' LIMIT 10 ';

		// load opportunityProducts
		for(AggregateResult ar : Database.query(query)){
			Datapoint dp = new Datapoint();
			//dp.fy = (Integer) ar.get('fy');
			//dp.fq = (Integer) ar.get('fq');
            dp.Period = 'Q'+ (Integer) ar.get('fq') + ' FY' + (Integer) ar.get('fy');
			//dp.accountManager = (String)ar.get('AccountManager');
        	String acctMgr = (String)ar.get('AccountManager');

            if(targetType == 'Mite Segment'){
                if(acctMgr == 'null' || acctMgr == null || acctMgr == ''){
                    dp.accountManager = 'Other';
                } else {
                    dp.accountManager = 'Mite';
                }
            } else {
                dp.accountManager = acctMgr;
            }

			//dp.segment = (String)ar.get('Segment__c');
			dp.productFamily = (String)ar.get('Product_Family__c');
            //dp.productFamily = dp.productFamily.replace('Acq ','').replace('Ret ','');

            if(targetType == 'Mite Segment' || targetType == 'Mite') dp.productFamily += ' - '+(String)ar.get('SubFamily');

			//if(dataTypeSelected=='CTN' || dataTypeSelected=='License') dp.amount = (Decimal)ar.get('SumQuantity');
			//else dp.amount = (Decimal)ar.get('SumTotalPrice');
            dp.amount = (Decimal)ar.get('SumAmount');

			//dp.sType = (String)ar.get('StageName');
            dp.Type = (String)ar.get('StageName');


			addToDatapointMapping(
					//dp.fq+''+dp.fy,
                    dp.Period,
					dp.accountManager,
					dp.productFamily,
					dp.segment,
					//dp.sType,
                    dp.Type,
					dp,
					datapointMap
			);
		}

        system.debug(Limits.getHeapSize()); // 646K

		// load targets
		for(AggregateResult ar : [SELECT
									User__r.Name accountManager,
									Product_Family__c,
									//Segment__c,
                                    FISCAL_QUARTER(Period_startdate__c) fq,
                                    FISCAL_YEAR(Period_startdate__c) fy,
                                    //Fiscal_Quarter__c,
                                    //Fiscal_Year__c,
									SUM(Revenue__c) sumRevenue
								From
									Sales_Target__c
								Where
									FISCAL_YEAR(Period_startdate__c) = :Integer.valueOf(fiscalYearSelected)
								AND(
									UserRoleName__c in :visibleRoleNames
								//OR
									//User__c = :UserInfo.getUserId()
								)
				                AND
				                  User__c != null
								AND
									Segment__c = :targetType
								AND
									Data_type__c = :dataTypeSelected
								GROUP BY
									User__r.Name,
									Product_Family__c,
									//Segment__c,
                                    FISCAL_QUARTER(Period_startdate__c),
                                    FISCAL_YEAR(Period_startdate__c)
								])
		{

			Datapoint dp = new Datapoint();
			//dp.fy = Integer.valueOf((String) ar.get('Fiscal_Year__c'));
			//dp.fq = Integer.valueOf((String) ar.get('Fiscal_Quarter__c'));
            dp.Period = 'Q'+ Integer.valueOf( ar.get('fq')) + ' FY' +  Integer.valueOf( ar.get('fy'));
			dp.accountManager = (String)ar.get('accountManager');
			//dp.segment = (String)ar.get('Segment__c');
			dp.productFamily = (String)ar.get('Product_Family__c');
            //dp.productFamily = dp.productFamily.replace('Acq ','').replace('Ret ','');
			//dp.clc = 'Acq';
			dp.amount = (Decimal)ar.get('sumRevenue');
			//dp.sType = 'Target';
            dp.Type = 'Target';


			addToDatapointMapping(
					//dp.fq+''+dp.fy,
                    dp.Period,
					dp.accountManager,
					dp.productFamily,
					dp.segment,
					'Target',
					dp,
					datapointMap
			);


		}
        system.debug(Limits.getHeapSize()); // 1115K
		//system.debug(datapoints);
		//system.debug(datapointMap);

		// make sure all types exist in the map, so that we always have all columns in the pivot
		Set<String> allTypes = new Set<String>{'Commit','Closed Won','Target','Gap','Offer','Funnel','Closed Lost','Coverage'};
		for(String p : datapointMap.keySet()){
			for(String v : datapointMap.get(p).keySet()){
				for(String pf : datapointMap.get(p).get(v).keySet()){
					for(String s : datapointMap.get(p).get(v).get(pf).keySet()){

						Datapoint dp = new Datapoint();
						//dp.fq = Integer.valueOf(p.subString(0,1));
						//dp.fy = Integer.valueOf(p.substring(1,5));
                        dp.Period = p;
						dp.Amount = 0;
						dp.AccountManager = v;
						dp.ProductFamily = pf;
						dp.Segment = s;

						for(String theType : allTypes){
							if(!datapointMap.get(p).get(v).get(pf).get(s).containsKey(theType)) {
								Datapoint dpCopy = dp.clone();
								//dpCopy.sType = theType;
								dpCopy.Type = theType;
								datapointMap.get(p).get(v).get(pf).get(s).put(theType,dpCopy);
							}
						}
					}
				}
			}
		}

        system.debug(Limits.getHeapSize()); // 2674K //2478

		for(String p : datapointMap.keySet()){
			for(String v : datapointMap.get(p).keySet()){
				for(String pf : datapointMap.get(p).get(v).keySet()){
					for(String s : datapointMap.get(p).get(v).get(pf).keySet()){
						for(String t : datapointMap.get(p).get(v).get(pf).get(s).keySet()){
							datapoints.add(datapointMap.get(p).get(v).get(pf).get(s).get(t));
                            datapointMap.get(p).get(v).get(pf).get(s).remove(t);
							//system.debug(datapoints);
						}
					}
				}
			}
		}

        system.debug(Limits.getHeapSize()); // 2815K //2423   //6510

		return datapoints;
	}

	public void addToDatapointMapping(String period, String accountManager, String productFamily, String segment, String type, Datapoint dp,Map<String,Map<String,Map<String,Map<String,Map<String,Datapoint>>>>> datapointMap){

		if(datapointMap.containsKey(period)){
			Map<String,Map<String,Map<String,Map<String,Datapoint>>>> tempMap4 = datapointMap.get(period);
			if(tempMap4.containsKey(accountManager)){
				Map<String,Map<String,Map<String,Datapoint>>> tempMap3 = tempMap4.get(accountManager);
				if(tempMap3.containsKey(productFamily)){
					Map<String,Map<String,Datapoint>> tempMap2 = tempMap3.get(productFamily);
					if(tempMap2.containsKey(segment)){
						Map<String,Datapoint> tempMap = tempMap2.get(segment);
						tempMap = addTypeToMap(type,dp,tempMap);
						tempMap2.put(segment,tempMap);
					} else {
						Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>());
						tempMap2.put(segment,tempMap);
					}
					tempMap3.put(productFamily,tempMap2);
				} else {
					//Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>{type=>dp});
					Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>());
					Map<String,Map<String,Datapoint>> tempMap2 = new Map<String,Map<String,Datapoint>>{segment=>tempMap};
					tempMap3.put(productFamily,tempMap2);
				}
				tempMap4.put(accountManager,tempMap3);
			} else {
				Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>());
				//Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>{type=>dp});
				Map<String,Map<String,Datapoint>> tempMap2 = new Map<String,Map<String,Datapoint>>{segment=>tempMap};
				Map<String,Map<String,Map<String,Datapoint>>> tempMap3 = new Map<String,Map<String,Map<String,Datapoint>>>{productFamily=>tempMap2};
				tempMap4.put(accountManager,tempMap3);
			}
			datapointMap.put(period,tempMap4);
		} else {
			Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>());
			//Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>{type=>dp});
			Map<String,Map<String,Datapoint>> tempMap2 = new Map<String,Map<String,Datapoint>>{segment=>tempMap};
			Map<String,Map<String,Map<String,Datapoint>>> tempMap3 = new Map<String,Map<String,Map<String,Datapoint>>>{productFamily=>tempMap2};
			Map<String,Map<String,Map<String,Map<String,Datapoint>>>> tempMap4 = new Map<String,Map<String,Map<String,Map<String,Datapoint>>>>{accountManager=>tempMap3};
			datapointMap.put(period,tempMap4);
		}

	}

	public Map<String,Datapoint> addTypeToMap(String type,Datapoint dp,Map<String,Datapoint> theMap){
		// logic for assigning types to BRM categories
		if(type == 'Prospect') {
			theMap = addTypeAmountToMap('Funnel',dp,theMap);
			theMap = addTypeAmountToMap('Coverage',dp,theMap);
		} else if(type == 'Qualify') {
			theMap = addTypeAmountToMap('Funnel',dp,theMap);
			theMap = addTypeAmountToMap('Coverage',dp,theMap);
		} else if(type == 'Offer') {
			theMap = addTypeAmountToMap('Offer',dp,theMap);
			theMap = addTypeAmountToMap('Coverage',dp,theMap);
		} else if(type == 'Closing' || type =='Closing by SAG') {
			theMap = addTypeAmountToMap('Commit',dp,theMap);
			theMap = addTypeAmountToMap('Gap',dp,theMap);
			theMap = addTypeAmountToMap('Coverage',dp,theMap);
		} else if(type == 'Closed Won') {
			theMap = addTypeAmountToMap('Commit',dp,theMap);
			theMap = addTypeAmountToMap('Closed Won',dp,theMap);
			theMap = addTypeAmountToMap('Gap',dp,theMap);
			theMap = addTypeAmountToMap('Coverage',dp,theMap);
		} else if(type == 'Target') {
			theMap = addTypeAmountToMap('Target',dp,theMap);
			theMap = addTypeAmountToMap('TargetGap',dp,theMap);
        } else if(type == 'Closed Lost') {
            theMap = addTypeAmountToMap('Closed Lost',dp,theMap);
        }
		return theMap;
	}

	public Map<String,Datapoint> addTypeAmountToMap(String type,Datapoint dpSource,Map<String,Datapoint> theMap){
		Datapoint dp = dpSource.clone();
		if(type=='TargetGap'){
			type='Gap';
			// for gap, the negative value needs to be taken
			dp.Amount = -1 * dp.Amount;
		}
		dp.Type = type;
		if(theMap.containsKey(type)){
			// if exact type already exists, only add to the amount
			theMap.get(type).amount = theMap.get(type).amount + dp.amount;
		} else {
			theMap.put(type,dp);
		}
		return theMap;

	}

	public class Datapoint {
		public Decimal Amount {get;set;}
		//private Integer fy;
		//private Integer fq;
		public String AccountManager {get;set;} // ownerrole? health / I&S / public
		public String Segment {get;set;} // department op opp
		public String ProductFamily {get;set;}
		//public String teamRoles {get;set;}
		public String Period{get;set;}

		public String Type {get;set;}
		//public String sType {get;set;}

	}


	public List<SelectOption> getFiscalYearOptions(){
		List<SelectOption> fiscalYears = new List<SelectOption>();
		String previousFiscalYear = String.valueOf(Integer.valueOf(currentFiscalYear)-1);
		fiscalYears.add(New SelectOption(previousFiscalYear,previousFiscalYear));
		fiscalYears.add(New SelectOption(currentFiscalYear,currentFiscalYear));
		String nextFiscalYear = String.valueOf(Integer.valueOf(currentFiscalYear)+1);
		fiscalYears.add(New SelectOption(nextFiscalYear,nextFiscalYear));

		return fiscalYears;
	}

	/*
	 *	Description: this method fetches the current user's role and it's sibling roles to enable switching
	 */
	public List<SelectOption> getRunAsUserOptions(){
		Map<String,SelectOption> runAsUsersMap = new Map<String,SelectOption>();
        List<SelectOption> returnList = new List<SelectOption>();

        // fetch all values that should be in the list
        for(Id roleId : visibleSalesManagerRoleIds){
			runAsUsersMap.put(GeneralUtils.RoleIdToRoleName.get(roleId),New SelectOption(roleId,GeneralUtils.RoleIdToRoleName.get(roleId)));
		}

        // sort the picklist in custom roles order
        for(String s : OpportunityDashboardUtils.customRoleOrder){
            if(runAsUsersMap.containsKey(s)){
                returnList.add(runAsUsersMap.get(s));
                runAsUsersMap.remove(s);
            }
        }
        // add any remaining roles at the end (sorted alphabetically)
        List<SelectOption> remainingList = runAsUsersMap.values();
        remainingList.sort();
        returnList.addAll(remainingList);

		return returnList;
	}


    public List<SelectOption> getDataTypeOptions(){
        List<SelectOption> dataTypes = new List<SelectOption>();

        dataTypes.add(New SelectOption('MRR','MRR'));
		dataTypes.add(New SelectOption('TCV','TCV'));
        dataTypes.add(New SelectOption('CTN','CTN'));
        dataTypes.add(New SelectOption('License','License'));

        return dataTypes;
    }

	public String fiscalQuarterSelected {get;set;}

	public String verticalSelected{get;set;}
	public List<SelectOption> getVerticalOptions() {
		List<SelectOption> returnList = new List<SelectOption>();
		returnList.add(new SelectOption('all','-All-'));

		Set<String> verticals = new Set<String>();
		for(String s : visibleRoleNames){
			s = OpportunityDashboardUtils.roleToVertical(s);
			verticals.add(s);
		}
		for(String s : verticals){
			returnList.add(new SelectOption(s,s));
		}
		return returnList;

	}
	public String accountManagerSelected{get;set;}
	public List<SelectOption> getAccountManagerOptions() {
		List<SelectOption> returnList = new List<SelectOption>();
		returnList.add(new SelectOption('all','-All-'));
		for(User u : [Select Id, Name From User Where UserRoleId in :visibleRoleIds]){
			returnList.add(new SelectOption(u.Id,u.Name));
		}
		return returnList;

	}

	public String getRunasUserLabel(){
		system.debug(runAsUserRoleIdSelected);
		UserRole[] usrRoles = [Select Name From UserRole Where Id = :runAsUserRoleIdSelected];
		if(!usrRoles.isEmpty()){
			return (usrRoles[0].Name).replace(' Sales Manager','').replace('Head of ','').replace(' Sales','').replace('Director ','');
		} else {
			return 'Main';
		}
	}

    public String dashboardTypeSelected{get;set;}
    public List<SelectOption> getDashboardTypeOptions(){
        return OpportunityDashboardUtils.getDashboardTypeOptions();
    }
    public pageReference loadDashboard(){
        return OpportunityDashboardUtils.dashboardVFPage(dashboardTypeSelected);
    }

}