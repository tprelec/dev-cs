public with sharing class OpportunityDashboardBRMTop10EntryClass {
	public AggregateResult data {get;set;}
	public String customer {get;set;}
	public Id accId {get;set;}
	//public String productFamily {get;set;}
	public String vertical {get;set;}
	public String opportunityName {get;set;}
	public Decimal tcv {get;set;}
	public Decimal amount {get;set;}
	public Id oppId {get;set;}

}