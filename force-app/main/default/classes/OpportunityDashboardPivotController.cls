public with sharing class OpportunityDashboardPivotController {
	public OpportunityDashboardPivotController() {
		
	}

	public String customSegmentOrder{
		get{
			if(customSegmentOrder == null){
				customSegmentOrder = '"';
				for(String s : OpportunityDashboardUtils.customSegmentOrder){
					customSegmentOrder += s;
					customSegmentOrder += '","';
				}
				customSegmentOrder = customSegmentOrder.subString(0,customSegmentOrder.length() - 2);
			}
			return customSegmentOrder;
		}
		set;
	}
		

}