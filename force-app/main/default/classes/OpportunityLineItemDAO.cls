public inherited sharing class OpportunityLineItemDAO {
    /**
     * Retrieve the Opportunity details based on Set of Opportunity Ids
     */
    public static Map<Id, List<OpportunityLineItem>> getOpportunityDetailsBasedOnId(
        Set<String> fieldSet,
        Set<Id> opportunityIdSet
    ) {
        return getOpportunityDetailsBasedOnId(fieldSet, opportunityIdSet, null);
    }
    /**
     * Retrieve the Opportunity details based on Set of Opportunity Ids
     */
    public static Map<Id, List<OpportunityLineItem>> getOpportunityDetailsBasedOnId(
        Set<String> fieldSet,
        Set<Id> opportunityIdSet,
        QueryService.QueryProperties qp
    ) {
        Map<Id, List<OpportunityLineItem>> opptyOpptyLIMap = new Map<Id, List<OpportunityLineItem>>();
        // Make sure OpptyId is in the fieldSet:
        fieldSet.add('OpportunityId');
        String additionalQueryOptions = '';

        if (qp != null && String.isNotBlank(qp.additionalQueryOptions)) {
            additionalQueryOptions = qp.additionalQueryOptions;
            fieldSet.addAll(qp.fieldsToAdd);
        }

        String query = String.format(
            'SELECT {0} FROM OpportunityLineItem WHERE OpportunityId IN: opportunityIdSet WITH SECURITY_ENFORCED',
            new List<String>{ String.join(new List<String>(fieldSet), ',') }
        );

        query += additionalQueryOptions;

       // System.debug('query: ' + query);
        @SuppressWarnings(
            'PMD.ApexSOQLInjection'
        ) // No need to escape any user input as this is in a controlled manner. No user input is used here
        List<OpportunityLineItem> oliList = (List<OpportunityLineItem>) Database.query(
            query
        );
        if (oliList.size() > 0) {
            for (OpportunityLineItem oli : oliList) {
                Id opptyId = oli.OpportunityId;
                if (!opptyOpptyLIMap.containsKey(opptyId)) {
                    opptyOpptyLIMap.put(
                        opptyId,
                        new List<OpportunityLineItem>()
                    );
                }
                opptyOpptyLIMap.get(opptyId).add(oli);
            }
        }
        return opptyOpptyLIMap;
    }
}