public without sharing class OpportunityLineItemTriggerHandler extends TriggerHandler {
	private List<OpportunityLineItem> newOppLineItems;
	private Map<Id, OpportunityLineItem> oldOppLineItemsMap;

	private void init() {
		newOppLineItems = (List<OpportunityLineItem>) this.newList;
		oldOppLineItemsMap = (Map<Id, OpportunityLineItem>) this.oldMap;
	}

	public override void beforeInsert() {
		init();

		if (isVodafoneRecord(newOppLineItems)) {
			setClc();
			copySiteToSiteList();
			blockClosedOppsEditing();
			setOrderType();
			calculateFields();
		}
	}

	public override void beforeUpdate() {
		init();

		if (isVodafoneRecord(newOppLineItems)) {
			setClc();
			copySiteToSiteList();
			blockClosedOppsEditing();
			setOrderType();
			calculateFields();
		}
	}

	public override void beforeDelete() {
		init();

		if (isVodafoneRecord(oldOppLineItemsMap.values())) {
			blockClosedOppsEditing();
		}
	}

	public override void afterInsert() {
		init();

		if (isVodafoneRecord(newOppLineItems)) {
			updateForecastedActivationDate();
			setOppDealType(newOppLineItems, null);
			handleFrameworkAgreementLogicByProposition();
		}
	}

	public override void afterUpdate() {
		init();

		if (isVodafoneRecord(newOppLineItems)) {
			updateForecastedActivationDate();
			setOppDealType(newOppLineItems, oldOppLineItemsMap);
		}
	}

	public override void afterDelete() {
		init();

		if (isVodafoneRecord(oldOppLineItemsMap.values())) {
			setOppDealType(oldOppLineItemsMap.values(), null);
		}
	}

	private Boolean isVodafoneRecord(List<OpportunityLineItem> oppLineItems) {
		return CS_Brand_Resolver.isVodafoneRecord(oppLineItems);
	}

	private void calculateFields() {
		for (OpportunityLineItem oli : newOppLineItems) {
			calculateGrossRevenue(oli);
			calculateDiscountPerProduct(oli);
			calculateContributionMargin(oli);
			calculateContributionMarginNet(oli);
			calculateContributionMarginGross(oli);
			calculateNetRevenue(oli);

			// Proposition_Type__c = derived from Proposition__c via Orderform_Propositions__mdt
			setPropositionType(oli);

			// Model_Number__c = derived from Model_Number_Identifier__c (CloudSense)
			setModelNumber(oli);
		}
	}

	private void calculateGrossRevenue(OpportunityLineItem oli) {
		if (oli.Quantity != null && oli.Duration__c != null && oli.Gross_List_Price__c != null) {
			oli.Gross_Revenues__c = oli.Quantity * oli.Duration__c * oli.Gross_List_Price__c;
		}
	}

	private void calculateDiscountPerProduct(OpportunityLineItem oli) {
		if (oli.DiscountNew__c != null && oli.Gross_Revenues__c != null) {
			oli.DiscountPerProduct__c = (oli.DiscountNew__c / 100) * oli.Gross_Revenues__c;
		}
	}

	private void calculateContributionMargin(OpportunityLineItem oli) {
		if (
			oli.Gross_Revenues__c != null &&
			(oli.DiscountPerProduct__c != null ||
			oli.Cost__c != null)
		) {
			Decimal tmpDec = 0;
			if (oli.DiscountPerProduct__c > 0) {
				tmpDec += oli.DiscountPerProduct__c;
			}
			if (oli.Cost__c > 0) {
				tmpDec += oli.Cost__c;
			}
			oli.ContributionMargin__c = oli.Gross_Revenues__c - tmpDec;
		}
	}

	private void calculateContributionMarginNet(OpportunityLineItem oli) {
		if (oli.ContributionMargin__c != null && oli.Net_Revenue__c != null) {
			if (oli.Net_Revenue__c > 0 && oli.ContributionMargin__c > 0) {
				oli.Contribution_Margin__c = (oli.ContributionMargin__c / oli.Net_Revenue__c) * 100;
			} else {
				oli.Contribution_Margin__c = 0;
			}
		}
	}

	private void calculateContributionMarginGross(OpportunityLineItem oli) {
		if (
			oli.ContributionMargin__c != null &&
			oli.Gross_Revenues__c != null &&
			oli.ContributionMargin__c > 0 &&
			oli.Gross_Revenues__c > 0
		) {
			oli.Contribution_Margin_Gross__c =
				(oli.ContributionMargin__c / oli.Gross_Revenues__c) * 100;
		}
	}

	private void calculateNetRevenue(OpportunityLineItem oli) {
		if (oli.Quantity != null && oli.Duration__c != null && oli.Net_Unit_Price__c != null) {
			oli.Net_Revenue__c = (oli.Quantity * oli.Duration__c) * oli.Net_Unit_Price__c;
		}
	}

	private void setPropositionType(OpportunityLineItem oli) {
		if (oli.Proposition_Type__c == null && oli.Proposition__c != null) {
			//Proposition__c may consist of a ';' separated list of Propositions. If that is the case, these propositions will always have the same type.
			String proposition = getDelimitedPropositions(oli.Proposition__c)[0];

			if (propositionMap.containsKey(proposition)) {
				oli.Proposition_Type__c = propositionMap.get(proposition).Proposition_Type__c;
			}
		}
	}

	private void setModelNumber(OpportunityLineItem oli) {
		if (oli.Model_Number_Identifier__c != null && oli.Model_Number__c == null) {
			if (!oppIdToModelNumberMap.containsKey(oli.OpportunityId)) {
				oppIdToModelNumberMap.put(oli.OpportunityId, new Map<String, Decimal>());
			}

			if (
				oppIdToModelNumberMap.get(oli.OpportunityId)
					.containsKey(oli.Model_Number_Identifier__c)
			) {
				oli.Model_Number__c = oppIdToModelNumberMap.get(oli.OpportunityId)
					.get(oli.Model_Number_Identifier__c);
			} else {
				oli.Model_Number__c = oppIdToModelNumberMap.get(oli.OpportunityId).size() + 1;

				oppIdToModelNumberMap.get(oli.OpportunityId)
					.put(oli.Model_Number_Identifier__c, oli.Model_Number__c);
			}
		}
	}

	public static Map<Id, Map<String, Decimal>> oppIdToModelNumberMap {
		get {
			if (oppIdToModelNumberMap == null) {
				oppIdToModelNumberMap = new Map<Id, Map<String, Decimal>>();
			}
			return oppIdToModelNumberMap;
		}
		set;
	}

	public static Map<String, Orderform_Proposition__mdt> propositionMap {
		get {
			if (propositionMap == null) {
				propositionMap = new Map<String, Orderform_Proposition__mdt>();

				for (Orderform_Proposition__mdt op : [
					SELECT MasterLabel, Proposition_Type__c, Framework_Agreement_Type__c
					FROM Orderform_Proposition__mdt
				]) {
					propositionMap.put(op.MasterLabel, op);
				}
			}
			return propositionMap;
		}
		set;
	}

	private void setClc() {
		Map<Id, String> productIdToClc = new Map<Id, String>();

		// collect all missing clc's
		for (OpportunityLineItem oli : newOppLineItems) {
			// if new oli or existing oli with changed product, update the clc
			if (oli.CLC__c == null) {
				productIdToClc.put(oli.PriceBookEntryId, null);
			}
		}
		// fetch the actual clc values
		if (!productIdToClc.isEmpty()) {
			for (PriceBookEntry pbe : [
				SELECT Id, Product2.CLC__c
				FROM PriceBookEntry
				WHERE Id IN :productIdToClc.keySet() AND Product2.CLC__c != NULL
			]) {
				productIdToClc.put(pbe.Id, pbe.Product2.CLC__c);
			}

			// paste the clc values to the olis, or if none is filled in and none is found, return an error
			for (OpportunityLineItem oli : newOppLineItems) {
				// if new oli update the clc
				if (oli.CLC__c == null) {
					if (
						productIdToCLC.containsKey(oli.PriceBookEntryId) &&
						productIdToCLC.get(oli.PriceBookEntryId) != null
					) {
						oli.CLC__c = productIdToCLC.get(oli.PriceBookEntryId);
					} else {
						oli.CLC__c.addError('Please specify a CLC value for this line item.');
					}
				}
			}
		}
	}

	/*
	 *	Description: This method makes sure that site__c and site_list__c are kept in sync. site__c is the user-facing
	 *				 field used for MAC opportunities. site_list__c is used for generating contracted_products
	 */
	private void copySiteToSiteList() {
		for (OpportunityLineItem oli : newOppLineItems) {
			if (oli.Location__c != null && oli.Location__c != oli.Site_List__c) {
				oli.Site_List__c = oli.Location__c;
			}
		}
	}

	private void blockClosedOppsEditing() {
		Set<Id> oppIdsToCheck = new Set<Id>();
		Set<Id> oppIdsToBlock = new Set<Id>();

		List<String> relevantFields = new List<String>{
			'PricebookEntryId',
			'Quantity',
			'Product_Arpu_Value__c',
			'Duration__c',
			'ServiceDate',
			'Product_Family__c',
			'Group__c',
			'Gross_List_Price__c',
			'DiscountNew__c',
			'Cost_Center__c',
			'Billing__c'
		};

		if (oldOppLineItemsMap == null) {
			// check for insert
			for (OpportunityLineItem oli : newOppLineItems) {
				oppIdsToCheck.add(oli.OpportunityId);
			}
		} else if (newOppLineItems == null) {
			// check for delete
			for (OpportunityLineItem oli : oldOppLineItemsMap.values()) {
				oppIdsToCheck.add(oli.OpportunityId);
			}
		} else {
			// check for update
			for (OpportunityLineItem oli : newOppLineItems) {
				if (
					GeneralUtils.isRecordFieldChanged(
						oli,
						oldOppLineItemsMap,
						relevantFields,
						false
					)
				) {
					oppIdsToCheck.add(oli.OpportunityId);
				}
			}
		}

		if (!oppIdsToCheck.isEmpty()) {
			addErrorForBlockedOlis(oppIdsToCheck, oppIdsToBlock);
		}
	}

	private void addErrorForBlockedOlis(Set<Id> oppIdsToCheck, Set<Id> oppIdsToBlock) {
		for (Opportunity opp : [
			SELECT Id, StageName
			FROM Opportunity
			WHERE Id IN :oppIdsToCheck AND (StageName = 'Closed Won' OR StageName = 'Closed Lost')
		]) {
			// change is not allowed
			oppIdsToBlock.add(opp.Id);
		}

		if (!oppIdsToBlock.isEmpty()) {
			List<OpportunityLineItem> relevantList = new List<OpportunityLineItem>();

			if (newOppLineItems != null) {
				relevantList = newOppLineItems;
			} else {
				relevantList = oldOppLineItemsMap.values();
			}

			for (OpportunityLineItem oli : relevantList) {
				if (
					oppIdsToBlock.contains(oli.OpportunityId) &&
					!Special_Authorizations__c.getInstance().Edit_Closed_Opportunities__c
				) {
					oli.addError(
						'No edits are allowed to Opportunities with status \'Closed Won\' or \'Closed Lost\'. If you need to correct anything, contact your Sales Manager.'
					);
				}
			}
		}
	}

	private void updateForecastedActivationDate() {
		// create a map containing the earliest servicedate for each opportunity in the batch
		Map<Id, Date> oppIdToDate = new Map<Id, Date>();

		for (OpportunityLineItem oli : newOppLineItems) {
			if (
				oldOppLineItemsMap == null ||
				oli.ServiceDate != oldOppLineItemsMap.get(oli.Id).ServiceDate
			) {
				if (
					!oppIdToDate.containsKey(oli.OpportunityId) ||
					oli.ServiceDate < oppIdToDate.get(oli.OpportunityId)
				) {
					oppIdToDate.put(oli.OpportunityId, oli.ServiceDate);
				}
			}
		}

		if (!oppIdToDate.isEmpty()) {
			List<Opportunity> oppsToUpdate = new List<Opportunity>();

			for (Opportunity opp : [
				SELECT Id, Opportunity_Activation_Date_Forecast__c
				FROM Opportunity
				WHERE Id IN :oppIdToDate.keySet()
			]) {
				if (
					opp.Opportunity_Activation_Date_Forecast__c == null ||
					oppIdToDate.get(opp.Id) < opp.Opportunity_Activation_Date_Forecast__c
				) {
					opp.Opportunity_Activation_Date_Forecast__c = oppIdToDate.get(opp.Id);
					oppsToUpdate.add(opp);
				}
			}
			update oppsToUpdate;
		}
	}

	/*
	 *	Description: calculates the order type by looking up the product2 object. Except for MAC opps. Those always have ordertype MAC.
	 */
	private void setOrderType() {
		List<OpportunityLineItem> oppsToCalc = new List<OpportunityLineItem>();
		Set<Id> oppIds = new Set<Id>();
		Set<ID> prodIdSet = new Set<ID>();

		for (OpportunityLineItem oli : newOppLineItems) {
			if (
				oldOppLineItemsMap == null ||
				GeneralUtils.isRecordFieldChanged(oli, oldOppLineItemsMap, 'Product2Id')
			) {
				oppsToCalc.add(oli);
				oppIds.add(oli.OpportunityId);
				prodIdSet.add(oli.Product2Id);
			}
		}

		if (!prodIdSet.isEmpty()) {
			Id macId = [SELECT Id FROM OrderType__c WHERE Name = 'MAC'].Id;
			Set<Id> macOppIds = new Set<Id>();

			for (Opportunity opp : [
				SELECT Id, Record_Type_is_MAC__c
				FROM Opportunity
				WHERE Id IN :oppIds AND Record_Type_is_MAC__c = TRUE
			]) {
				macOppIds.add(opp.Id);
			}

			Map<Id, Product2> productMap = new Map<Id, Product2>(
				[SELECT Id, OrderType__c FROM Product2 WHERE Id IN :prodIdSet]
			);

			for (OpportunityLineItem oli : oppsToCalc) {
				if (macOppIds.contains(oli.OpportunityId)) {
					oli.OrderType__c = macId;
				} else {
					oli.OrderType__c = productMap.get(oli.Product2Id).OrderType__c;
				}
			}
		}
	}

	/*
	 *	Description: calculates the deal type on opp by looking at clc's. Except for MAC opps. Those require a manual update of the dealtype.
	 */
	private void setOppDealType(
		List<OpportunityLineItem> newOppLineItems,
		Map<Id, OpportunityLineItem> oldOppLineItemsMap
	) {
		Set<Id> oppIds = new Set<Id>();

		for (OpportunityLineItem oli : newOppLineItems) {
			if (
				(oldOppLineItemsMap == null && oli.CLC__c != null) ||
				GeneralUtils.isRecordFieldChanged(oli, oldOppLineItemsMap, 'CLC__c')
			) {
				oppIds.add(oli.OpportunityId);
			}
		}

		if (!oppIds.isEmpty()) {
			List<Opportunity> oppsToUpdate = new List<Opportunity>();
			Map<Id, String> oppIdToDealType = new Map<Id, String>();
			Map<Id, String> oppIdToOldDealType = new Map<Id, String>();

			for (AggregateResult ar : [
				SELECT OpportunityId oppId, CLC__c clc, Opportunity.Deal_Type__c dealType
				FROM OpportunityLineitem
				WHERE
					CLC__c != NULL
					AND OpportunityId IN :oppIds
					AND Opportunity.Record_Type_is_MAC__c = FALSE
				GROUP BY OpportunityId, CLC__c, Opportunity.Deal_Type__c
				ORDER BY CLC__c
			]) {
				String clc = String.valueOf(ar.get('clc'));
				String oppId = String.valueOf(ar.get('oppId'));
				String oppDealType = String.valueOf(ar.get('dealType'));

				if (!oppIdToDealType.containsKey(oppId)) {
					oppIdToDealType.put(oppId, '');
				}

				String currentDealType = getCurrentDealType(oppIdToDealType, oppId, clc);
				oppIdToDealType.put(oppId, currentDealType);
				oppIdToOldDealType.put(oppId, oppDealType);
			}

			addOppsToUpdate(oppIdToDealType, oppIdToOldDealType, oppsToUpdate);
			update oppsToUpdate;
		}
	}

	private String getCurrentDealType(Map<Id, String> oppIdToDealType, String oppId, String clc) {
		String currentDealType = oppIdToDealType.get(oppId);

		switch on clc {
			when 'Acq' {
				currentDealType += 'Acquisition + ';
			}
			when 'Ret' {
				currentDealType += 'Retention + ';
			}
			when 'Mig' {
				currentDealType += 'Migration + ';
			}
			when 'Churn' {
				currentDealType += 'Cancellation + ';
			}
		}

		return currentDealType;
	}

	private void addOppsToUpdate(
		Map<Id, String> oppIdToDealType,
		Map<Id, String> oppIdToOldDealType,
		List<Opportunity> oppsToUpdate
	) {
		for (Id oppId : oppIdToDealType.keySet()) {
			String dealType = oppIdToDealType.get(oppId);
			dealType = dealType.subString(0, dealType.length() - 3);

			if (dealType != oppIdToOldDealType.get(oppId)) {
				oppsToUpdate.add(new Opportunity(Id = oppId, Deal_Type__c = dealType));
			}
		}
	}

	private void handleFrameworkAgreementLogicByProposition() {
		Map<Id, Opportunity> oliOppIdToOppMap = populateOliOppIdToOppMap();
		Set<Id> accountIdsWithNewFWA = new Set<Id>();
		Set<Id> accountIdsWithNewVZFWA = new Set<Id>();

		for (OpportunityLineItem oli : newOppLineItems) {
			if (String.isBlank(oli.Proposition__c)) {
				continue;
			}

			List<String> propositions = getDelimitedPropositions(oli.Proposition__c);

			for (String proposition : propositions) {
				if (!propositionMap.keySet().contains(proposition)) {
					throw new OpportunityLineItemTriggerHandlerException(
						'The Opportunity Line Item Proposition \'' +
						proposition +
						' \' from the not-delimited Proposition__c \'' +
						oli.Proposition__c +
						'\' is not included in the Orderform_Proposition__mdt Custom Metadata'
					);
				}
				if (oliOppIdToOppMap.containsKey(oli.OpportunityId)) {
					Id oliAccountId = oliOppIdToOppMap.get(oli.OpportunityId).AccountId;

					if (
						propositionMap.get(proposition).Framework_Agreement_Type__c ==
						'VodafoneZiggo'
					) {
						accountIdsWithNewVZFWA.add(oliAccountId);
					} else {
						accountIdsWithNewFWA.add(oliAccountId);
					}
				}
			}
		}

		Boolean noAccount = accountIdsWithNewFWA.isEmpty() && accountIdsWithNewVZFWA.isEmpty();

		if (!noAccount) {
			AccountService.getInstance()
				.generateFrameworkAgreementIdAndInvokeFrameworkAgreementCreation(
					accountIdsWithNewFWA,
					accountIdsWithNewVZFWA
				);
		}
	}

	private Map<Id, Opportunity> populateOliOppIdToOppMap() {
		Set<Id> oliOppIds = GeneralUtils.getIDSetFromList(newOppLineItems, 'OpportunityId');

		Map<Id, Opportunity> oliOppIdToOppMap = new Map<Id, Opportunity>(
			[
				SELECT AccountId
				FROM Opportunity
				WHERE
					Id IN :oliOppIds
					AND (Opportunity.Owner.UserType != 'PowerPartner'
					OR Opportunity.Direct_Indirect__c = 'Direct')
			]
		);
		return oliOppIdToOppMap;
	}

	@testVisible
	private static List<String> getDelimitedPropositions(String inputPropositionString) {
		if (inputPropositionString.contains(';')) {
			List<String> retPropositions = inputPropositionString.split(';');

			for (Integer i = 0; i < retPropositions.size(); i++) {
				retPropositions[i] = retPropositions[i].trim();
			}
			return retPropositions;
		}
		return new List<String>{ inputPropositionString };
	}

	public class OpportunityLineItemTriggerHandlerException extends Exception {
	}
}