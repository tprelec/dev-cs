/**
 * @description         This is the class that contains logic for triggering workflow on update lineitem for MRR field
 * @author              Marcel Vreuls
 *                      to run paste this: 
 *                      Id batchJobId = Database.executeBatch(new OpportunityLineitemBatchUpdate(), 100);
 * @history             Marcel Vreuls W-000022 : changed dataparameter to alle records changed from yesterday on
 */
global class OpportunityLineitemBatchUpdate implements Database.Batchable <sObject>, Database.AllowsCallouts, Database.Stateful{
 

   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('Select Id, CloseDate from Opportunity WHERE CloseDate < 2018-01-01');
    } 
 
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){

        processRecords(scope);
        
    }
 
    private void processRecords(List<Opportunity> oppertunitiesToProcess){

        List<OpportunityLineItem> OLIlist2  = new List<OpportunityLineItem>();

        //1 Select all oppertunity met closeddate voor 01-01-2018
        // Opportunity.CloseDate > DATE(2017,12,31)

        //2 loop through the lineitems van de oppertunities
        // Select Id, MRR1__c, MRR__c From OpportunityLineItem Where LastModifiedDate <= TODAY]
       //system.debug('vreuls size :' + scope.size());
        try{
            for(Opportunity otu : oppertunitiesToProcess){
                 system.debug('vreuls OppID:' + otu.Id);   
                List<OpportunityLineItem> listOLI = getOLIs(otu.Id);
                for(OpportunityLineItem li : listOLI)
                {
                  li.MRR1__c  = li.MRR__C;
                  OLIlist2.Add(li);   
                  system.debug('vreuls OppLineId:' + otu.Id);              
                } 
            }
             
       
        }
        catch (Exception e){
            //logs.add(new Log__c( Name__c = '...',  Stacktrace__c = e.getStackTraceString()));
            throw e;
        }
        Database.update(OLIlist2);  
    }


    private List<OpportunityLineItem> getOLIs(Id Id) {

            List<OpportunityLineItem> OLIlist2 = [Select Id, MRR1__c, MRR__c FROM OpportunityLineItem WHERE OpportunityId =: Id];
            return OLIlist2;
    }

    global void finish(Database.BatchableContext BC){
        //Get the batch job for reference in the email.
        AsyncApexJob a = [SELECT
                            Status,
                            NumberOfErrors,
                            TotalJobItems
                          FROM
                            AsyncApexJob
                          WHERE
                            Id =: BC.getJobId()];

        // Send an email to the running user notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        String[] toAddresses = new String[] {UserInfo.getUserEmail()};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Oppertunity LineItems geupdate: ' + a.Status);
        mail.setPlainTextBody('The Oppertunity LineItems Update job processed ' + a.TotalJobItems +
                              ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}