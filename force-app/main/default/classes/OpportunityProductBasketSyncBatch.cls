global class OpportunityProductBasketSyncBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {

	global Database.QueryLocator start(Database.BatchableContext BC) {

		// Retrieve any primary product baskets that have been updated after their last sync
   		
		return Database.getQueryLocator('Select Id from cscfga__Product_Basket__c Where Pending_Autosync__c = true ');

	}

	// This is the execute to handle a schedule request (just enables the job to be scheduled)
	global void execute(SchedulableContext sc) {
        OpportunityProductBasketSyncBatch controller = new OpportunityProductBasketSyncBatch();
        database.executebatch(controller,1);   
	}

	// This is the execute for the scope of the batch (ie does the processing)
   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		// Note that we will want to limit the batch size to 1 so that only one basket is processed in each execute   			
		CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(scope[0].Id);
        
	}
	
	global void finish(Database.BatchableContext BC) {

	}


}