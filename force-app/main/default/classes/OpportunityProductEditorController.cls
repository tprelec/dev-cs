global with sharing class OpportunityProductEditorController {

    public static final Set<String> ziggoFamilies = new Set<String>{'Ziggo XSell','Ziggo Only'};
    public static final Set<String> ziggoProductLines = new Set<String>{'fZiggo Only','fZiggo Xsell'};
    public static final Set<String> ziggoOnlyFamilies = new Set<String>{'Ziggo Only'};
    public static final Integer Max_No_Sites = Integer.valueof(Order_Form_Settings__c.getInstance().Max_no_of_sites_in_list__c);
    public static Opportunity theOpp {get;set;}
    public static List<SiteWrapper> siteList {get;set;}

    public static Boolean hasVFProducts {get;set;}
    public static Boolean hasZiggoProducts {get;set;}

  public OpportunityProductEditorController() {
    hasZiggoProducts = false;
    hasVFProducts = false;

    initializeOpp();
        
    // load site dropdown
    initializeSiteList(theOpp.accountId,null);

  }

  public static void initializeOpp(){
    theOpp = [select Id, Name, Department__c, Pricebook2Id, PriceBook2.Name, AccountId, Deal_Type__c, RecordType.Name from Opportunity where Id = :ApexPages.currentPage().getParameters().get('Id') limit 1];
  }

  public static void initializeSiteList(Id accountId, Set<Id> siteIds){
    siteList = new List<SiteWrapper>();
    system.debug(accountId);
    if(siteIds == null) siteIds = new Set<Id>();
    for(Site__c s : [SELECT Id, Name, Site_City__c, Site_Street__c,Site_House_Number__c, Site_House_Number_Suffix__c, Site_Postal_Code__c
              FROM Site__c 
              WHERE 
                (Site_Account__c != null AND Site_Account__c = :accountId )
                OR
                (Id in :siteIds )
              ORDER BY Site_City__c, Site_Street__c, Site_House_Number__c,Site_House_Number_Suffix__c
              LIMIT : Max_No_Sites]){
      SiteWrapper sw = new SiteWrapper();
      sw.theSite=s;
      sw.selected=false;
      siteList.add(sw);
    }    
  }

  public class SiteWrapper {
    public Site__c theSite {get;set;}
    public Boolean selected {get;set;}
    public String siteLabel {
      get{
        return theSite.Site_City__c + ', ' + theSite.Site_Street__c + ', ' + theSite.Site_House_Number__c + (theSite.Site_House_Number_Suffix__c==null?'':' '+theSite.Site_House_Number_Suffix__c) + ', ' + theSite.Site_Postal_Code__c;
      }
      set;
    }
  }

  public String getshoppingCart() {

    return JSON.serialize(queryShoppingCart());

  }

  private List<ShoppingCartItem> queryShoppingCart(){

    List<shoppingCartItem> cart = new List<shoppingCartItem>();

    for(OpportunityLineItem oli : [SELECT Id, Quantity, UnitPrice, Description, CLC__c, PriceBookEntryId, PriceBookEntry.Name, PriceBookEntry.IsActive, 
                         PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.PriceBook2Id, Group__c, Product_Arpu_Value__c, 
                         DiscountNew__c, Gross_List_Price__c, PriceBookEntry.Product2.CLC__c,Discount,PriceBookEntry.Product2.ProductCode,
                         PriceBookEntry.Product2.Family, PriceBookEntry.Product2.VF_Product__c, PricebookEntry.Product2.Quantity_type__c,
                         Duration__c, Location__c, OpportunityId, Cost_Center__c, Billing__c, Fixed_Mobile__c, Manually_added_product__c, Location__r.Name,
                         Product_Line__c, Model_Number__c, Deal_Type__c, LG_ContractTerm__c
                    FROM 
                      opportunityLineItem 
                    WHERE 
                      OpportunityId=:theOpp.Id
                    ORDER BY
                      Location__r.Site_City__c ASC, 
                      Location__r.Site_Street__c ASC,
                      Group__c ASC
                    ])
    {
      shoppingCartItem sci = new shoppingCartItem();
      sci.id = oli.Id;
      sci.productId = oli.PriceBookEntry.Product2.VF_Product__c;
      sci.productLabel = vfProdToLabel(sci.productId);
      
      if(oli.CLC__c != null){
        sci.clc = oli.CLC__c;
      } else {
        sci.clc = oli.PriceBookEntry.Product2.CLC__c;
      }
      
      sci.dealtype = oli.Deal_Type__c;
      sci.site = oli.Location__c;
      sci.siteMulti.add(oli.Location__c);
      sci.siteLabel = siteIdToLabel.get(oli.Location__c);
      sci.grp = oli.Group__c;
      sci.modelgrp = String.valueOf(oli.Model_Number__c);
      sci.quantity = oli.Quantity;
      sci.contractterm = oli.LG_ContractTerm__c==null?0:oli.LG_ContractTerm__c;
      sci.qType = oli.PriceBookEntry.Product2.Quantity_Type__c==null?'Each':oli.PriceBookEntry.Product2.Quantity_Type__c;

      // convert 'sales' fields to 'mac' fields if appropriate
      if(oli.DiscountNew__c == null) {
        if(oli.Discount > 0)
          oli.DiscountNew__c = oli.Discount;
        else
          oli.DiscountNew__c = 0;
      }

      if(oli.Product_Arpu_Value__c > 0 && oli.Gross_List_Price__c == null) oli.Gross_List_Price__c = oli.Product_Arpu_Value__c / ((100-oli.DiscountNew__c)/100);
      sci.listprice = oli.Gross_List_Price__c;
      
      
      sci.discount = oli.DiscountNew__c;
      sci.duration = oli.Duration__c;
      sci.costCenter = oli.Cost_Center__c;
      sci.billing = oli.Billing__c;

      // Calculate the one off and monthly costs for each row
      // These will be used in a hidden column to calculate the sub totals for each site
      //if (sci.qType != 'Monthly') {
      if (sci.duration == 1) {
        sci.oneOff = ((100-oli.DiscountNew__c)/100) * oli.Quantity * oli.Gross_List_Price__c;
        sci.monthly = 0;
      } else {
        sci.monthly = ((100-oli.DiscountNew__c)/100) * oli.Quantity * oli.Gross_List_Price__c;
        sci.oneOff = 0;
      }

      cart.add(sci);

      //if(ziggoFamilies.contains(oli.PriceBookEntry.Product2.Family))
      if(ziggoProductLines.contains(oli.Product_Line__c))
        hasZiggoProducts = true;
      else
        hasVFProducts = true;
    }  
    return cart;  
  }


  global Class ShoppingCartItem {
    global String id;
    global String productId;
    global String productLabel;
    global String clc;
    global String dealtype;
    global String site;
    global List<String> siteMulti{
      get{
        if(siteMulti == null) siteMulti = new List<String>();
        return siteMulti;
      }
      set;
    }
    global String siteLabel;
    global String grp;
    global String modelgrp;
    global Decimal quantity;
    global String qType;
    global Decimal listprice;
    global Decimal contractterm;
    global Decimal discount;
    global Decimal duration;
    global String costCenter;
    global Boolean billing;
    global Decimal oneOff;
    global Decimal monthly;

  }

  global Class FieldError{
    global String name;
    global String status;
  }

  global Class ErrorItem {
    global List<FieldError> fieldErrors;
    global List<String> data;
  }

  global Class CatalogueItem {
    global String id;
    //global String productCode;
    global String label;
    //global String clc;
    //global Decimal price;
    //global String family;
    //global String qType;
  }

  @RemoteAction
  global static shoppingCartItem[] createProduct(String jsonString, Id oppId) {
  
    system.debug(jsonString);
    jsonString = preprocessJSONString(jsonString);
    Map<String,ShoppingCartItem> createMap = (Map<String,ShoppingCartItem>)JSON.deserialize(jsonString, Map<String,ShoppingCartItem>.class);
    system.debug(createMap);

    List<OpportunityLineItem> oliToCreate = new List<OpportunityLineItem>();
    List<shoppingCartItem> returnList = new List<shoppingCartItem>();

    // collect site id's (we need them for the labels) and product id's (we need them for the clc's)
    Set<Id> siteIds = new Set<Id>();
    Set<Id> pbeIds = new Set<Id>();

    for(ShoppingCartItem theSci : createMap.values()){
      if(theSci.site != null && theSci.site != '') {
        siteIds.add(theSci.site);
      }
      if(theSci.siteMulti != null) {
        for(String siteId : theSci.siteMulti){
          siteIds.add(siteId);
        }
      }      
      if(theSci.productId != null) pbeIds.add(theSci.productId);
    }

    // create a map of vfProductId->CLC->pbeId
    Map<Id,Map<String,Id>> vfProductIdToCLCToPbeId = new Map<Id,Map<String,Id>>();
    // create a map of pbeId to family
    Map<Id,String> pbeIdToQuantityType = new Map<Id,String>();
    for(PriceBookEntry pbe : [Select Id, Product2.CLC__c, Product2.VF_Product__c,Product2.Family,Product2.Quantity_Type__c,Product2.Product_Line__c From PriceBookEntry Where Product2.VF_Product__c in :pbeIds]){
      //if(pbe.Product2.CLC__c == null) pbe.Product2.CLC__c = 'Acq'; // this is a dummy value for non-split-up articles?
      if(!vfProductIdToCLCToPbeId.containsKey(pbe.Product2.VF_Product__c)){
        Map<String,Id> clcToPbeId = new Map<String,Id>{pbe.Product2.CLC__c=>pbe.Id};
        vfProductIdToCLCToPbeId.put(pbe.Product2.VF_Product__c,clcToPbeId);
      } else {
        vfProductIdToCLCToPbeId.get(pbe.Product2.VF_Product__c).put(pbe.Product2.CLC__c,pbe.Id);
      }
      pbeIdToQuantityType.put(pbe.Product2.VF_Product__c,pbe.Product2.Quantity_Type__c);

      // apply the Z/VF filter after a product of that type has been added
      //if(ziggoFamilies.contains(pbe.Product2.Family))
      if(ziggoProductLines.contains(pbe.Product2.Product_Line__c))
        hasZiggoProducts = true;
      else
        hasVFProducts = true;      
    }
    
    initializeSiteList(null,siteIds);

    Map<Integer,Map<Id,Integer>> oldGroupToNewSiteToNewGroup = new Map<Integer,Map<Id,Integer>>();
    Integer maxGroupUsed = 0;

    // determine maxGroupUsed 
    for(OpportunityLineItem oli : [Select Group__c From OpportunityLineItem Where OpportunityId = :oppId AND Group__c <> null]){
      if(Integer.valueOf(oli.Group__c) > maxGroupUsed){
        maxGroupUsed = Integer.valueOf(oli.Group__c);
      }      
    }

    // a create is always for 1 site/set of sites. So we fetch the sites from the 1st record in order to allow looping through the sites
    Set<String> sitesToCreate = new Set<String>();
    sitesToCreate.addAll(createMap.values()[0].siteMulti);
    // in case no site is filled, put a dummy value in there so that at least 1 row is created (with an empty site)
    if(sitesToCreate.size() == 0) sitesToCreate.add('');

    // loop through all the sites selected
    for(String siteId : sitesToCreate){

      for(String lineItemId : createMap.keySet()){
        ShoppingCartItem initSci = createMap.get(lineItemId);
        // create a clone because the initial sci might be reused in case of multiple sites
        ShoppingCartItem sci = initSci.clone();
        // create the actual opportunityLineItems
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = oppId;

        // fetch the correct pbeId by first defaulting to the product2 with empty CLC. If that doesn't exist, take the one with the corresponding CLC
        if(vfProductIdToCLCToPbeId.get(sci.productId).containsKey(null)) {
          oli.PriceBookEntryId = vfProductIdToCLCToPbeId.get(sci.productId).get(null);
        } else {
          oli.PriceBookEntryId = vfProductIdToCLCToPbeId.get(sci.productId).get(sci.clc);
        }
        oli.CLC__c = sci.clc;

        // and reset the productLabel and the quantity type
        sci.productLabel = vfProdToLabel(sci.productId);
        sci.qType = pbeIdToQuantityType.get(sci.productId)==null?'Each':pbeIdToQuantityType.get(sci.productId);

        // Calculate the one off and monthly costs for each row
        // These will be used in a hidden column to calculate the sub totals for each site
        //if (sci.qType != 'Monthly') {
        if (sci.duration == 1) {          
          sci.oneOff = ((100-sci.discount)/100) * sci.quantity * sci.listprice;
          sci.monthly = 0;
        } else {
          sci.monthly = ((100-sci.discount)/100) * sci.quantity * sci.listprice;
          sci.oneOff = 0;
        }
          
        if(siteId != ''){
          sci.siteLabel = siteIdToLabel.get(siteId);
          sci.site = siteId;
          sci.siteMulti.clear();
          sci.siteMulti.add(siteId);
          oli.Location__c = siteId;
          system.debug(sci.siteLabel);
        }

        if(initSci.grp != null && initSci.grp != ''){
          if(!oldGroupToNewSiteToNewGroup.containsKey(Integer.valueOf(initSci.grp))){
            oldGroupToNewSiteToNewGroup.put(Integer.valueOf(initSci.grp),new Map<Id,Integer>{siteId=>maxGroupUsed+1});
            sci.grp = String.valueOf(maxGroupUsed+1);
            maxGroupUsed++;            
          } else {
            if(!oldGroupToNewSiteToNewGroup.get(Integer.valueOf(initSci.grp)).containsKey(siteId)){
              oldGroupToNewSiteToNewGroup.get(Integer.valueOf(initSci.grp)).put(siteId,maxGroupUsed+1);
              sci.grp = String.valueOf(maxGroupUsed+1);
              maxGroupUsed++;                
            } else {
              sci.grp = String.valueOf(oldGroupToNewSiteToNewGroup.get(Integer.valueOf(initSci.grp)).get(siteId));
            }
          }
        }
        
        oli.Deal_Type__c = sci.dealtype;
        oli.Group__c = sci.grp;
        oli.Model_Number__c = sci.modelgrp==''?null:Integer.valueOf(sci.modelgrp);
        oli.Quantity = sci.quantity;
        oli.Gross_List_Price__c = sci.listprice;
        oli.DiscountNew__c = sci.discount;
        oli.Duration__c = sci.duration;
        oli.LG_ContractTerm__c = sci.contractterm==0?null:sci.contractterm;
        oli.Cost_Center__c = sci.costCenter;
        oli.Billing__c = sci.billing;
        // set arpu
        oli.Product_Arpu_Value__c = oli.Gross_List_Price__c * ((100 - oli.DiscountNew__c) / 100.0);      

        oli.TotalPrice = oli.Quantity * (oli.Gross_List_Price__c==null?0:oli.Gross_List_Price__c) * oli.Duration__c * ((100 - (oli.DiscountNew__c==null?0:oli.DiscountNew__c)) / 100.0);

        system.debug(oli);
        oli.recalculateFormulas();
        oliToCreate.add(oli);    
        returnList.add(sci);
      }
    }
    insert oliToCreate;
    // update return result with the new id's
    for(Integer i=0;i<oliToCreate.size();i++){
      returnList[i].Id = oliToCreate[i].Id;
    }
    return returnList;
  }

  @RemoteAction
  global static shoppingCartItem[] updateProduct(String jsonString, Id oppId) {
  
    jsonString = preprocessJSONString(jsonString);
    system.debug(jsonString);
    Map<Id,ShoppingCartItem> updateMap = (Map<Id,ShoppingCartItem>)JSON.deserialize(jsonString, Map<Id,ShoppingCartItem>.class);
    system.debug(updateMap);

    Map<Id,OpportunityLineItem> oliToUpdate = new Map<Id,OpportunityLineItem>();
    Map<Id,OpportunityLineItem> oliToInsert = new Map<Id,OpportunityLineItem>();
    List<OpportunityLineItem> oliToDelete = new List<OpportunityLineItem>();
    List<ShoppingCartItem> returnList = new List<ShoppingCartItem>();

    // collect product id's (we need them for the pbeids)
    Set<Id> pbeIds = new Set<Id>();
    for(ShoppingCartItem theSci : updateMap.values()){
      if(theSci.productId != null) pbeIds.add(theSci.productId);
    }

    // create a map of vfProductId->CLC->pbeId
    Map<Id,Map<String,Id>> vfProductIdToCLCToPbeId = new Map<Id,Map<String,Id>>();
    // create a map of pbeId to family
    Map<Id,String> pbeIdToQuantityType = new Map<Id,String>();    
    for(PriceBookEntry pbe : [Select Id, Product2.CLC__c, Product2.VF_Product__c,Product2.Quantity_Type__c From PriceBookEntry Where Product2.VF_Product__c in :pbeIds]){
      //if(pbe.Product2.CLC__c == null) pbe.Product2.CLC__c = 'Acq'; // this is a dummy value for non-split-up articles?
      if(!vfProductIdToCLCToPbeId.containsKey(pbe.Product2.VF_Product__c)){
        Map<String,Id> clcToPbeId = new Map<String,Id>{pbe.Product2.CLC__c=>pbe.Id};
        vfProductIdToCLCToPbeId.put(pbe.Product2.VF_Product__c,clcToPbeId);
      } else {
        vfProductIdToCLCToPbeId.get(pbe.Product2.VF_Product__c).put(pbe.Product2.CLC__c,pbe.Id);
      }
      pbeIdToQuantityType.put(pbe.Product2.VF_Product__c,pbe.Product2.Quantity_Type__c);
    }
    system.debug(pbeIdToQuantityType);

    Id accountId = [select AccountId From Opportunity Where Id = :oppId].AccountId;

    initializeSiteList(accountId,null);

    // check if any product or clc changed. In that case, we need to replace the entire lineitem
    // query current values
    Map<Id,opportunityLineItem> oliIdToPBEMap = new Map<Id,opportunityLineItem>([SELECT Id, 
                      PriceBookEntryId, 
                      PriceBookEntry.Product2.CLC__c,
                      PriceBookEntry.Product2.VF_Product__c
                    FROM 
                      opportunityLineItem 
                    WHERE 
                      Id in :updateMap.keySet()
                    ]);
    system.debug(oliIdToPBEMap);
    for(String lineItemId : updateMap.keySet()){
      ShoppingCartItem sci = updateMap.get(lineItemId);
      OpportunityLineItem oli;

      // special handling for changed product or changed clc!
      if(sci.productId != oliIdToPBEMap.get(lineItemId).PriceBookEntry.Product2.VF_Product__c){
        // new lineitem needed with new product and (possibly) new clc
        oli = new OpportunityLineItem();
        oli.OpportunityId = oppId;

        // fetch the correct pbeId by first defaulting to the product2 with empty CLC. If that doesn't exist, take the one with the corresponding CLC
        if(vfProductIdToCLCToPbeId.get(sci.productId).containsKey(null)) {
          oli.PriceBookEntryId = vfProductIdToCLCToPbeId.get(sci.productId).get(null);
        } else {
          oli.PriceBookEntryId = vfProductIdToCLCToPbeId.get(sci.productId).get(sci.clc);
        }
      
      } else if (oliIdToPBEMap.get(lineItemId).PriceBookEntry.Product2.CLC__c != null && sci.clc != oliIdToPBEMap.get(lineItemId).PriceBookEntry.Product2.CLC__c){
        // as long as not all pbe's have been moved to clc-less Product2, also a CLC change can result in a different PBEId. 
        oli = new OpportunityLineItem();
        oli.OpportunityId = oppId;
        
        // fetch the correct pbeId by first defaulting to the product2 with empty CLC. If that doesn't exist, take the one with the corresponding CLC
        if(vfProductIdToCLCToPbeId.get(sci.productId).containsKey(null)) {
          oli.PriceBookEntryId = vfProductIdToCLCToPbeId.get(sci.productId).get(null);
        } else {
          oli.PriceBookEntryId = vfProductIdToCLCToPbeId.get(sci.productId).get(sci.clc);
        }        

        system.debug(sci);
        system.debug(vfProductIdToCLCToPbeId);
      } else {
        // update the actual opportunityLineItems
        oli = new OpportunityLineItem(Id=lineItemId);        
        sci.id = lineItemId;
      }

      // and reset the productLabel and the quantity type
      system.debug(sci.productId);
      sci.productLabel = vfProdToLabel(sci.productId);
      sci.qType = pbeIdToQuantityType.get(sci.productId)==null?'Each':pbeIdToQuantityType.get(sci.productId);

      // Calculate the one off and monthly costs for each row
      // These will be used in a hidden column to calculate the sub totals for each site
      //if (sci.qType != 'Monthly') {
      if (sci.duration == 1) {        
        sci.oneOff = ((100-sci.discount)/100) * sci.quantity * sci.listprice;
        sci.monthly = 0;
      } else {
        sci.monthly = ((100-sci.discount)/100) * sci.quantity * sci.listprice;
        sci.oneOff = 0;
      }

      if(sci.site != null && sci.site != '') {
        system.debug(siteIdToLabel);
        system.debug(sci.site);
        sci.siteLabel = siteIdToLabel.get(sci.site);
        sci.siteMulti.clear();
        sci.siteMulti.add(sci.site);
        oli.Location__c = sci.site;
      }
      oli.CLC__c = sci.clc;
      oli.Deal_Type__c = sci.dealtype;
      oli.Group__c = sci.grp;
      oli.Model_Number__c = sci.modelgrp==''?null:Integer.valueOf(sci.modelgrp);
      oli.Quantity = sci.quantity;
      oli.Gross_List_Price__c = sci.listprice;
      oli.DiscountNew__c = sci.discount;
      oli.Duration__c = sci.duration;
      oli.LG_ContractTerm__c = sci.contractterm==0?null:sci.contractterm;
      oli.Cost_Center__c = sci.costCenter;
      oli.Billing__c = sci.billing;
      // set arpu
      oli.Product_Arpu_Value__c = oli.Gross_List_Price__c * ((100 - oli.DiscountNew__c) / 100.0);            

      oli.TotalPrice = oli.Quantity * (oli.Gross_List_Price__c==null?0:oli.Gross_List_Price__c) * oli.Duration__c * ((100 - (oli.DiscountNew__c==null?0:oli.DiscountNew__c)) / 100.0);

      system.debug(oli);
      oli.recalculateFormulas();
      if(oli.Id != null){
        oliToUpdate.put(oli.Id,oli);    
      } else {
        oliToInsert.put(lineItemId,oli);
        oliToDelete.add(new OpportunityLineItem(Id=lineItemId));
      }
      updateMap.put(lineItemId,sci);
    }
    Savepoint sp = database.setSavepoint();
    try{
      delete oliToDelete;

      insert oliToInsert.values();
      // process new id's for insert
        for(Id lineItemId : oliToInsert.keySet()){
          // repace item's id in returnlist with new id
          updateMap.get(lineItemId).id = oliToInsert.get(lineItemId).Id;
        }

      update oliToUpdate.values();

      returnList = updateMap.values();
      return returnList;
    } catch (dmlException de){
      Database.rollback(sp);
      // not using the standard datatables error pattern here because the page is expecting a list of shoppingCartItems. Something to improve later.
      throw new ExInvalidConfigurationException('Error updating products. First error: ' +de.getDmlMessage(0));

      return returnList;
    }
    
    }

    @RemoteAction
    global static shoppingCartItem[] deleteProduct(String jsonString) {
      jsonString = preprocessJSONString(jsonString);
    List<shoppingCartItem> returnList = new List<shoppingCartItem>();
    system.debug(jsonString);
    Map<Id,ShoppingCartItem> deleteMap = (Map<Id,ShoppingCartItem>)JSON.deserialize(jsonString, Map<Id,ShoppingCartItem>.class);
    system.debug(deleteMap);


    delete [select Id From OpportunityLineItem Where Id in :deleteMap.keySet()];
    return returnList;
  }


    // This is the query to return the list of selectable products (for the New button and inline editing) 
  @RemoteAction
    global static String getFilteredJsonProductPicklist(String queryFilterString, Id oppId) {
    // fetch the opp (to find out if it has VF and/or Z products)
    Opportunity opp = [Select owner.UserRoleId, Account.Wholesale__c, Account.GT_Fixed__c, Account.IWR__c, Department__c, Owner.Ziggo__c, Ziggo_Only_Products__c, Ziggo_XSell_Products__c, Vodafone_Products__c From Opportunity Where Id = :oppId];
    system.debug('Vreuls QSFS: ' + queryFilterString);
    //system.debug('Vreuls : ' + plc);
    //system.debug('Vreuls Read: ')+ ApexPages.currentPage().getParameters().get('clc');

    // replace spaces by wildcards
    queryFilterString = queryFilterString.replaceAll(' ','%');

    // We dynamically build a query string 
    String qString = 'select Id, Active__c, Name, Brand__c, Product_Line__c,'; //Product2.Family,
    qString += 'ProductCode__c, BAP_SAP__c, Quantity_type__c, Description__c ';
    // only fetch the ACQ pbe's now. Change that if necessary while saving.
    qString += 'from VF_Product__c where Active__c=true ';

    if(opp.Vodafone_Products__c > 0){
      qString += ' AND Product_Line__c NOT IN :ziggoProductLines ';
    } else if(opp.Ziggo_Only_Products__c > 0 || opp.Ziggo_XSell_Products__c > 0){    
      qString += ' AND Product_Line__c IN :ziggoProductLines ';      
    }

    // IWR filter
    // The account being sold to must be marked as eligable for IWR
    if (!opp.Account.IWR__c) {
      qString += ' AND Product_Line__c!=\'IWR Mobile\' ';
    }

    // GT Fixed filter
    // The account being sold to must be marked as eligable for GT Fixed
    if (!opp.Account.GT_Fixed__c) {
      qString += ' AND Product_Line__c!=\'GT Fixed\' ';
    }

    // Wholesale filter on Account 
    // The account being sold to must be marked as eligable for Wholesale 
    if (!opp.Account.Wholesale__c) { 
      qString += ' AND Product_Line__c!=\'Wholesale\' '; 
    }     

    // Wholesale filter on Opp owner 
    // The opportunity owner should have role ‘Commercial Industries Carriers & SP’ 
    if(GeneralUtils.RoleIdToRoleName.get(opp.owner.UserRoleId) != Label.Wholesale_Role_Name){
      qString += ' AND Product_Line__c!=\'Wholesale\' '; 
    }     

    // Consumer Products filter
    // The opportunity owner must be 'soho (retail)' to make consumer products available
    if(opp.Department__c != 'SoHo'){
      qString += ' AND Product_Line__c!=\'Consumer\' ';
    }

    // text filter(s)
    qString += ' AND (';
    qString += ' Name LIKE \'%'+queryFilterString+'%\' OR ';
    qString += ' ProductCode__c LIKE \'%'+queryFilterString+'%\' OR ';
    qString = qString.subString(0,qString.length() - 3);
    
    qString += ' ) LIMIT 20 ';
    
    system.debug('qString:' +qString);     

    system.debug(String.valueOf(Limits.getHeapSize()));

    List<CatalogueItem> pList = new List<CatalogueItem>();
    for(VF_Product__c pbe : database.query(qString)){
      CatalogueItem item = new CatalogueItem();
      item.id = String.valueOf(pbe.Id);
      item.label = vfprodToLabel(pbe);
      pList.add(item);
    }

    return JSON.serialize(pList);
  }  


  @RemoteAction
    global static String getProductInfo(String vfProductId,String previousProductId) {
    system.debug(vfProductId);
    VF_Product__c vfp  = [Select BAP_SAP__c,Quantity_Type__c From VF_Product__c Where Id = :vfProductId];

    system.debug(vfp);
    if(previousProductId != null){
      VF_Product__c prevProd  = [Select BAP_SAP__c,Quantity_Type__c From VF_Product__c Where Id = :previousProductId];
      if(prevProd.Quantity_Type__c == vfp.Quantity_type__c) vfp.Quantity_Type__c = null;
    }

    return JSON.serialize(vfp);
  }

  public static String preprocessJSONString(String jsonString){
    jsonString = jsonString.replace('\"billing\":\"\"','\"billing\":false');
    return jsonString;
  }

  public static String vfProdToLabel(VF_Product__c prod){
    String prodName = prod.Name;
    if(prod.Brand__c != null && !prod.Name.contains(prod.Brand__c)) prodName = prod.Brand__c + ' ' + prod.Name;
    //W-311: added product line to dropdown, so it is searchable/filterable for the end-users.
    String theLabel =  prod.Product_Line__c + ' - ' + prod.ProductCode__c + ' - ' + prodName + (prod.Quantity_type__c==null?'':' - '+prod.Quantity_type__c); 
    return theLabel;
  }

  public static Map<Id,String> prodIdToLabel {
    get{
      if(prodIdToLabel == null){
        prodIdToLabel = new Map<Id,String>();
      }
      return prodIdToLabel;
    }
    set;
  }

  public static String vfProdToLabel(Id pbeId){
    system.debug(pbeId);
    if(prodIdToLabel.containsKey(pbeId)){
      return(prodIdToLabel.get(pbeId));
    } else {
      VF_Product__c prod = [Select Id, Brand__c, ProductCode__c, Name, Quantity_type__c From VF_Product__c Where Id = :pbeId];
      String prodName = prod.Name;
      if(prod.Brand__c != null && !prod.Name.contains(prod.Brand__c)) prodName = prod.Brand__c + ' ' + prod.Name;
      String theLabel = prod.ProductCode__c + ' - ' + prodName + (prod.Quantity_type__c==null?'':' - '+prod.Quantity_type__c); 
      prodIdToLabel.put(pbeId,theLabel);
      return theLabel;
    }
  }  


  public PageReference cancel(){
    return new PageReference('/'+ApexPages.currentPage().getParameters().get('Id'));
  }  

  // get PickList Values for Location Picklist
  public String sitesJSON {
    get{
      if(sitesJSON == null){
        sitesJSON = '[';
        for(SiteWrapper s: siteList) {
          sitesJSON += '{label:"'+s.siteLabel.escapeJava()+'", value: "'+s.theSite.Id+'" },';
        }
        if(sitesJSON.length() > 1) sitesJSON = sitesJSON.subString(0,sitesJSON.length()-1);
        sitesJSON += ']';

      }
      return sitesJSON;
    }
    set;
  }

  public static Map<Id,String> siteIdToLabel{
    get{
      if(siteIdToLabel == null){
        siteIdToLabel = new Map<Id,String>();
        if(siteList == null) initializeSiteList(theOpp.accountId,null);
        for(SiteWrapper s : siteList){
          siteIdToLabel.put(s.theSite.Id,s.siteLabel);
        }
      }
      return siteIdToLabel;
    }
    set;
  }
}