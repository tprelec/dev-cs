/**
 * @description			This is the class that contains logic for triggering 1SF sync for opps
 * @author				Guy Clairbois
 *						to run paste this: 
 *						Id batchJobId = Database.executeBatch(new OpportunitySharingBatch1SF(), 100);
 */
global class OpportunitySharingBatch1SF implements Database.Batchable <sObject>, Database.AllowsCallouts{
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('Select Id From Opportunity Where LastModifiedDate > 2017-01-01T00:00:00Z');
    } 
 
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){
    	Set<Id> oppIds = new Set<Id>();
		for(Opportunity o : scope){
            oppIds.add(o.Id);
        }
        INT_SF2SF_Handler.ShareOpportunitiesOnline(oppIds);    	
    	
    }
 
    global void finish(Database.BatchableContext BC){


    }
}