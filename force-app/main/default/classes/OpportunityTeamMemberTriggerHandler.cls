public without sharing class OpportunityTeamMemberTriggerHandler extends TriggerHandler {
	private List<OpportunityTeamMember> newOtms;
	private Map<Id, OpportunityTeamMember> oldOtmsMap;

	private Map<Id, Id> oppIdToSolutionSpecMap;
	private Map<Id, Id> oppIdToInsideSalesSpecMap;
	private Map<Id, Id> oppIdToMicrosoftSpecMap;
	private Map<Id, Id> oppIdToEntServicesSpecMap;

	private void init() {
		newOtms = (List<OpportunityTeamMember>) this.newList;
		oldOtmsMap = (Map<Id, OpportunityTeamMember>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		blockClosedOppsEditing();
	}

	public override void beforeUpdate() {
		init();
		blockClosedOppsEditing();
	}

	public override void beforeDelete() {
		init();
		blockClosedOppsEditing();
		updateContractSharingRules();
	}

	public override void afterInsert() {
		init();
		updateContractSharingRules();
		updateOpportunitySpecialists();
	}

	public override void afterUpdate() {
		init();
		updateContractSharingRules();
		updateOpportunitySpecialists();
	}

	private void blockClosedOppsEditing() {
		Set<Id> oppIdsToCheck = new Set<Id>();
		Set<Id> oppIdsToBlock = new Set<Id>();

		if (oldOtmsMap == null) {
			// check for insert
			for (OpportunityTeamMember otm : newOtms) {
				oppIdsToCheck.add(otm.OpportunityId);
			}
		} else if (newOtms == null) {
			// check for delete
			for (OpportunityTeamMember otm : oldOtmsMap.values()) {
				oppIdsToCheck.add(otm.OpportunityId);
			}
		} else {
			// check for update
			for (OpportunityTeamMember otm : newOtms) {
				if (otm.UserId != oldOtmsMap.get(otm.Id).UserId) {
					oppIdsToCheck.add(otm.OpportunityId);
				}
			}
		}

		if (!oppIdsToCheck.isEmpty()) {
			addErrorForBlockedOtms(oppIdsToCheck, oppIdsToBlock);
		}
	}

	private void addErrorForBlockedOtms(Set<Id> oppIdsToCheck, Set<Id> oppIdsToBlock) {
		for (Opportunity opp : [
			SELECT Id, StageName
			FROM Opportunity
			WHERE Id IN :oppIdsToCheck AND (StageName = 'Closed Won' OR StageName = 'Closed Lost')
		]) {
			oppIdsToBlock.add(opp.Id);
		}

		if (!oppIdsToBlock.isEmpty()) {
			List<OpportunityTeamMember> relevantList = new List<OpportunityTeamMember>();

			if (newOtms != null) {
				relevantList = newOtms;
			} else {
				relevantList = oldOtmsMap.values();
			}

			for (OpportunityTeamMember otm : relevantList) {
				if (
					oppIdsToBlock.contains(otm.OpportunityId) &&
					!Special_Authorizations__c.getInstance().Edit_Closed_Opportunities__c
				) {
					otm.addError(
						'No edits are allowed to Opportunities with status \'Closed Won\' or \'Closed Lost\'. If you need to correct anything, contact your Sales Manager.'
					);
				}
			}
		}
	}

	private void updateOpportunitySpecialists() {
		List<Opportunity> oppsToUpdate = new List<Opportunity>();
		Set<Id> oppIds = new Set<Id>();

		oppIdToSolutionSpecMap = new Map<Id, Id>();
		oppIdToInsideSalesSpecMap = new Map<Id, Id>();
		oppIdToMicrosoftSpecMap = new Map<Id, Id>();
		oppIdToEntServicesSpecMap = new Map<Id, Id>();

		for (OpportunityTeamMember otm : newOtms) {
			if (oldOtmsMap == null || otm.TeamMemberRole != oldOtmsMap.get(otm.Id).TeamMemberRole) {
				switch on otm.TeamMemberRole {
					when 'Solution Specialist' {
						oppIdToSolutionSpecMap.put(otm.OpportunityId, otm.UserId);
						oppIds.add(otm.OpportunityId);
					}
					when 'ITW Sales Rep' {
						oppIdToInsideSalesSpecMap.put(otm.OpportunityId, otm.UserId);
						oppIds.add(otm.OpportunityId);
					}
					when 'Cloud Productivity Consultant' {
						oppIdToMicrosoftSpecMap.put(otm.OpportunityId, otm.UserId);
						oppIds.add(otm.OpportunityId);
					}
					when 'Solution Specialist Enterprise Services' {
						oppIdToEntServicesSpecMap.put(otm.OpportunityId, otm.UserId);
						oppIds.add(otm.OpportunityId);
					}
				}
			}
		}

		for (Opportunity opp : [
			SELECT
				Id,
				Solution_Sales__c,
				Hidden_Inside_Sales_Specialist__c,
				Hidden_Microsoft_Solution_Specialist__c,
				Hidden_Ent_Services_Solution_Specialist__c
			FROM Opportunity
			WHERE Id IN :oppIds
		]) {
			if (populateSpecMapsAndShouldUpdateOpp(opp)) {
				oppsToUpdate.add(opp);
			}
		}

		update oppsToUpdate;
	}

	private Boolean populateSpecMapsAndShouldUpdateOpp(Opportunity opp) {
		Boolean updateOpp = false;

		if (
			oppIdToSolutionSpecMap.containsKey(opp.Id) &&
			opp.Solution_Sales__c != oppIdToSolutionSpecMap.get(opp.Id)
		) {
			opp.Solution_Sales__c = oppIdToSolutionSpecMap.get(opp.Id);
			updateOpp = true;
		}

		if (
			oppIdToInsideSalesSpecMap.containsKey(opp.Id) &&
			opp.Hidden_Inside_Sales_Specialist__c != oppIdToInsideSalesSpecMap.get(opp.Id)
		) {
			opp.Hidden_Inside_Sales_Specialist__c = oppIdToInsideSalesSpecMap.get(opp.Id);
			updateOpp = true;
		}

		if (
			oppIdToMicrosoftSpecMap.containsKey(opp.Id) &&
			opp.Hidden_Microsoft_Solution_Specialist__c != oppIdToMicrosoftSpecMap.get(opp.Id)
		) {
			opp.Hidden_Microsoft_Solution_Specialist__c = oppIdToMicrosoftSpecMap.get(opp.Id);
			updateOpp = true;
		}

		if (
			oppIdToEntServicesSpecMap.containsKey(opp.Id) &&
			opp.Hidden_Ent_Services_Solution_Specialist__c != oppIdToEntServicesSpecMap.get(opp.Id)
		) {
			opp.Hidden_Ent_Services_Solution_Specialist__c = oppIdToEntServicesSpecMap.get(opp.Id);
			updateOpp = true;
		}

		return updateOpp;
	}

	private void updateContractSharingRules() {
		Map<Id, Set<Id>> oldOppIdsToUserIdsMap = new Map<Id, Set<Id>>();
		Set<Id> newOtmIds = new Set<Id>();

		if (oldOtmsMap == null) {
			// check for insert
			for (OpportunityTeamMember otm : newOtms) {
				newOtmIds.add(otm.Id);
				populateOldOppIdsToUserIdsMap(otm, oldOppIdsToUserIdsMap);
			}
		} else if (newOtms == null) {
			// check for delete - delete existing sharing only
			for (OpportunityTeamMember otm : oldOtmsMap.values()) {
				populateOldOppIdsToUserIdsMap(otm, oldOppIdsToUserIdsMap);
			}
		} else {
			// check for update
			for (OpportunityTeamMember otm : newOtms) {
				if (otm.OpportunityAccessLevel != oldOtmsMap.get(otm.Id).OpportunityAccessLevel) {
					newOtmIds.add(otm.Id);
					populateOldOppIdsToUserIdsMap(otm, oldOppIdsToUserIdsMap);
				}
			}
		}

		// creating has to happen offline, as to properly capture the SharingLevel setting
		updateContractSharingRules(newOtmIds, JSON.serialize(oldOppIdsToUserIdsMap));
	}

	private void populateOldOppIdsToUserIdsMap(
		OpportunityTeamMember otm,
		Map<Id, Set<Id>> oldOppIdsToUserIdsMap
	) {
		if (oldOppIdsToUserIdsMap.containsKey(otm.OpportunityId)) {
			oldOppIdsToUserIdsMap.get(otm.OpportunityId).add(otm.UserId);
		} else {
			oldOppIdsToUserIdsMap.put(otm.OpportunityId, new Set<Id>{ otm.UserId });
		}
	}

	@future
	private static void updateContractSharingRules(
		Set<Id> newOtmIds,
		String jsonOldOppIdsToUserIds
	) {
		Map<Id, Set<Id>> oldOppIdsToUserIdsMap = (Map<Id, Set<Id>>) JSON.deserialize(
			jsonOldOppIdsToUserIds,
			Map<Id, Set<Id>>.class
		);
		List<VF_Contract__Share> contractSharingRulesToDelete = new List<VF_Contract__Share>();
		List<VF_Contract__Share> contractSharingRulesToInsert = new List<VF_Contract__Share>();
		Map<Id, List<OpportunityTeamMember>> newOppIdsToMemberMap = new Map<Id, List<OpportunityTeamMember>>();

		if (!newOtmIds.isEmpty()) {
			// insert or update.
			// renew teammember data by querying (OpportunityAccess is not correctly displayed)
			populateNewOppIdsToMemberMap(newOtmIds, newOppIdsToMemberMap);
		}

		if (!oldOppIdsToUserIdsMap.isEmpty()) {
			// delete the sharing rules from the old opportunity team members
			// also check if any team sharing exists for the new opportunity team members and remove those (else the insert will fail later on)
			getContractSharingRulesToDelete(oldOppIdsToUserIdsMap, contractSharingRulesToDelete);
		}

		if (!newOppIdsToMemberMap.isEmpty()) {
			// create new sharing rules for all contracts for all members
			createNewContractSharingRules(newOppIdsToMemberMap, contractSharingRulesToInsert);
		}

		try {
			delete contractSharingRulesToDelete;
		} catch (DmlException ex) {
			// not a problem if deletion fails. Happens sometimes if many updates happen at the same time.
			// also no need to rollback
			System.debug(LoggingLevel.INFO, 'Error while creating Contract sharing rules: ' + ex);
		}

		Savepoint sp = Database.setSavepoint();

		try {
			// JvW 22-11-2019: Changed insert to Database.insert to avoid DUPLICATE_VALUE
			// insert contractSharingRulestoInsert;
			Database.insert(contractSharingRulestoInsert, false);
		} catch (DmlException ex) {
			Database.rollback(sp);
			throw new ExInvalidStateException('Error while creating Contract sharing rules: ' + ex);
		}
	}

	private static void populateNewOppIdsToMemberMap(
		Set<Id> newOtmIds,
		Map<Id, List<OpportunityTeamMember>> newOppIdsToMemberMap
	) {
		List<OpportunityTeamMember> newOtmsWithNoneLevel = [
			SELECT Id, UserId, OpportunityId, OpportunityAccessLevel, TeamMemberRole
			FROM OpportunityTeamMember
			WHERE Id IN :newOtmIds AND OpportunityAccessLevel != 'None'
		]; // prevent the 'empty' owner rule from being copied

		for (OpportunityTeamMember otm : newOtmsWithNoneLevel) {
			if (newOppIdsToMemberMap.containsKey(otm.OpportunityId)) {
				newOppIdsToMemberMap.get(otm.OpportunityId).add(otm);
			} else {
				newOppIdsToMemberMap.put(otm.OpportunityId, new List<OpportunityTeamMember>{ otm });
			}
		}
	}

	private static void getContractSharingRulesToDelete(
		Map<Id, Set<Id>> oldOppIdsToUserIdsMap,
		List<VF_Contract__Share> contractSharingRulesToDelete
	) {
		for (VF_Contract__Share vcs : [
			SELECT Id, UserOrGroupId, parentId, Parent.Opportunity__c
			FROM VF_Contract__Share
			WHERE
				Parent.Opportunity__c IN :oldOppIdsToUserIdsMap.keySet()
				AND RowCause = :Schema.VF_Contract__Share.RowCause.Opportunity_Team__c
		]) {
			if (oldOppIdsToUserIdsMap.containsKey(vcs.Parent.Opportunity__c)) {
				if (
					oldOppIdsToUserIdsMap.get(vcs.Parent.Opportunity__c).contains(vcs.UserOrGroupId)
				) {
					contractSharingRulesToDelete.add(vcs);
				}
			}
		}
	}

	private static void createNewContractSharingRules(
		Map<Id, List<OpportunityTeamMember>> newOppIdsToMemberMap,
		List<VF_Contract__Share> contractSharingRulesToInsert
	) {
		for (VF_Contract__c contract : [
			SELECT Id, Opportunity__c
			FROM VF_Contract__c
			WHERE Opportunity__c IN :newOppIdsToMemberMap.keySet()
		]) {
			Set<Id> usersHandled = new Set<Id>();

			for (OpportunityTeamMember otm : newOppIdsToMemberMap.get(contract.Opportunity__c)) {
				// prevent creating 2 sharings for the same user withint 1 contract as it will crash the process
				if (!usersHandled.contains(otm.UserId)) {
					usersHandled.add(otm.UserId);

					VF_Contract__Share vcs = new VF_Contract__Share(
						ParentId = contract.Id,
						UserOrGroupId = otm.UserId,
						RowCause = Schema.VF_Contract__Share.RowCause.Opportunity_Team__c
					);

					if (otm.OpportunityAccessLevel == 'All') {
						vcs.AccessLevel = 'Edit';
					} else {
						vcs.AccessLevel = otm.OpportunityAccessLevel;
					}

					contractSharingRulestoInsert.add(vcs);
				}
			}
		}
	}
}