public with sharing class OpportunityUpdateNextStepsController {

    public OpportunityUpdateNextStepsController(ApexPages.StandardController controller) {
    	if(!Test.isRunningTest()){
    		//controller.addFields(New List<String>{'NextStep', 'Next_Steps_History__c'});
            controller.addFields(New List<String>{'NextStep'});
    	}
    	thisOpp = (Opportunity)controller.getRecord();
    	this.controller = controller;
    	
    	nextStepPrefix = '['+System.now().format('yyyy-MM-dd')+' '+System.Userinfo.getName()+'] ';
    	
		if (thisOpp.NextStep != null){
			oldNextStepText = thisOpp.NextStep;
		} else {
			oldNextStepText = '';
		}    	
    	   	
    }
    
    private ApexPages.Standardcontroller controller {get;set;}
    private final Opportunity thisOpp;
  
    
    public Integer nextStepLength{
    	get{
    		if(nextStepLength == null){
    			nextStepLength = 255 - nextStepPrefix.length();
    		}
    		return nextStepLength;
    	}
    	set;
    }
    
    public Integer charsRemaining {
    	get{
    		if(nextStepText == null)
    			return nextStepLength;
    		else
    			return nextStepLength - nextStepText.length();
    	}
    	set;
    }
    
    public void nothing(){
    	
    }
    
    public String nextStepPrefix{get;set;}
    public String nextStepText {get;set;}
    public String oldNextStepText {get;set;
    }
    public String[] nextStepsHistory {        
    	get{
    		if(nextStepsHistory == null){
    			nextStepsHistory = new String[]{};
/*    			if(thisOpp.Next_Steps_History__c != null){
	    			for(String s : thisOpp.Next_Steps_History__c.split('\n')){
	    				nextStepsHistory.add(s);	
	    			}
    			}
*/    		}
    		return nextStepsHistory;
    	}
    	set;
    }
    
    public PageReference updateOpp(){
    	system.debug(thisOpp.NextStep);
    	thisOpp.NextStep = nextStepPrefix + nextStepText;
    	system.debug(thisOpp.NextStep);
    	system.debug(nextStepPrefix);
    	system.debug(nextStepText);
    	system.debug(oldNextStepText);
//    	thisOpp.Next_Steps_History__c = oldNextStepText + (oldNextStepText==''?'':'\n') + (thisOpp.Next_Steps_History__c==null?'':thisOpp.Next_Steps_History__c);
    	update thisOpp;
    	PageReference p = new PageReference('/'+thisOpp.Id);
    	p.setRedirect(false);
    	return p;
    }

}