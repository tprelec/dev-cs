/**
 * 	@description	This class contains utils for Opportunites
 *	@Author			Gerhard Newman
 */
 global without sharing class OpportunityUtils {

 	// returns the profile name of user.
 	// Used in New Quote button on Opportunity page
 	// Needed to do without sharing to get community user access
 	// formula field not suitable as it required to be on page layout
    webService static String getUserProfile(ID userID){
    	User u = [Select Profile.Name from User where ID=:userID];
    	String profile = u.Profile.Name;
    	Return profile;
    }
}