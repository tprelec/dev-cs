@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class OrchExecuteCtnRetentionExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
	private Map<Id, String> mapOrderId;
	private Map<Id, List<String>> orderIdToCTNs;
	private Map<Id, EMP_TIBCO_ValidateOrderFEBulk.Response> calloutResultsMap = new Map<Id, EMP_TIBCO_ValidateOrderFEBulk.Response>();

	public Boolean performCallouts(List<SObject> data) {
		Boolean calloutsPerformed = false;

		try {
			List<CSPOFA__Orchestration_Step__c> steps = (List<CSPOFA__Orchestration_Step__c>) data;
			Set<Id> resultIds = GeneralUtils.getIDSetFromList(
				steps,
				'CSPOFA__Orchestration_Process__c'
			);
			OrchUtils.getProcessDetails(resultIds);
			mapOrderId = OrchUtils.mapOrderId;
			orderIdToCTNs = new Map<Id, List<String>>();

			for (CSPOFA__Orchestration_Step__c step : steps) {
				Id processId = step.CSPOFA__Orchestration_Process__c;
				orderIdToCTNs.put(mapOrderId.get(processId), new List<String>());
			}

			List<NetProfit_CTN__c> ctns = [
				SELECT Id, Order__c, CTN_Status__c
				FROM NetProfit_CTN__c
				WHERE
					Order__c IN :orderIdToCTNs.keySet()
					AND Action__c = 'Retention'
					AND CTN_Status__c = 'MSISDN Assigned'
				FOR UPDATE
			];

			for (NetProfit_CTN__c ctn : ctns) {
				orderIdToCTNs.get(ctn.Order__c).add(ctn.Id);
			}

			for (CSPOFA__Orchestration_Step__c step : steps) {
				Id processId = step.CSPOFA__Orchestration_Process__c;
				String orderId = mapOrderId.get(processId);

				List<String> stringCtnIds = orderIdToCTNs.get(orderId);

				EMP_TIBCO_ValidateOrderFEBulk.Response response = getEmptyOkResponse();

				if (!stringCtnIds.isEmpty()) {
					Set<Id> ctnIds = new Set<Id>((List<Id>) stringCtnIds);
					response = EMP_TIBCO_Integration.executeRetention(ctnIds);
				}

				calloutResultsMap.put(step.Id, response);
				calloutsPerformed = true;
			}
		} catch (Exception e) {
			System.debug(
				LoggingLevel.DEBUG,
				'Orchestartor step error (Execute retention for CTNs): ' +
				getFormateddErrorMessage(e)
			);
		}
		return calloutsPerformed;
	}

	private EMP_TIBCO_ValidateOrderFEBulk.Response getEmptyOkResponse() {
		String emptyJsonResponse = '{"status":200,"data":[{}],"error":[]}';
		EMP_TIBCO_ValidateOrderFEBulk.Response res = (EMP_TIBCO_ValidateOrderFEBulk.Response) JSON.deserialize(
			emptyJsonResponse,
			EMP_TIBCO_ValidateOrderFEBulk.Response.class
		);
		res.httpRequest = 'REQ';
		res.httpResponse = 'RES';
		return res;
	}

	public List<sObject> process(List<SObject> data) {
		List<sObject> results = new List<sObject>();
		List<NetProfit_CTN__c> ctnToUpdate = new List<NetProfit_CTN__c>();
		List<CSPOFA__Orchestration_Step__c> steps = (List<CSPOFA__Orchestration_Step__c>) data;
		for (CSPOFA__Orchestration_Step__c step : steps) {
			if (!calloutResultsMap.containsKey(step.Id)) {
				step = OrchUtils.setStepRecord(
					step,
					true,
					'Error occurred: Callout results not received.'
				);
				results.add(step);
				continue;
			}
			EMP_TIBCO_ValidateOrderFEBulk.Response calloutResult = calloutResultsMap.get(step.Id);
			step.Request__c = calloutResult.httpRequest;
			step.Response__c = calloutResult.httpResponse;
			if (
				calloutResult == null ||
				(calloutResult.error != null && !calloutResult.error.isEmpty())
			) {
				step = OrchUtils.setStepRecord(
					step,
					true,
					'Error occurred: ' + calloutResult.getErrorMessages()
				);
				results.add(step);
				continue;
			}
			step = OrchUtils.setStepRecord(
				step,
				false,
				'Execute retention for CTNs step completed'
			);
			String orderId = mapOrderId.get(step.CSPOFA__Orchestration_Process__c);
			for (Id ctnId : orderIdToCTNs.get(orderId)) {
				ctnToUpdate.add(
					new NetProfit_CTN__c(Id = ctnId, CTN_Status__c = 'Retention Requested')
				);
			}
			results.add(step);
		}
		return updateRecords(steps, results, ctnToUpdate);
	}

	@TestVisible
	private List<SObject> updateRecords(
		List<CSPOFA__Orchestration_Step__c> steps,
		List<SObject> results,
		List<NetProfit_CTN__c> recordsToUpdate
	) {
		try {
			Map<Id, String> updateFailedRecordsMap = new Map<Id, String>();
			List<Database.SaveResult> updateResults = Database.update(recordsToUpdate, false);
			for (Integer i = 0; i < updateResults.size(); i++) {
				Database.SaveResult updateResult = updateResults.get(i);
				if (!updateResult.isSuccess()) {
					updateFailedRecordsMap.put(
						recordsToUpdate.get(i).Id,
						updateResult.getErrors().get(0).getMessage()
					);
				}
			}
			if (updateFailedRecordsMap.isEmpty()) {
				return results;
			}
			for (CSPOFA__Orchestration_Step__c step : steps) {
				String orderId = mapOrderId.get(step.CSPOFA__Orchestration_Process__c);
				for (Id ctnId : orderIdToCTNs.get(orderId)) {
					if (updateFailedRecordsMap.containsKey(ctnId)) {
						String error =
							'Error occurred: CTN update failed with error: ' +
							updateFailedRecordsMap.get(ctnId);
						results.add(OrchUtils.setStepRecord(step, true, error));
					}
				}
			}
		} catch (Exception e) {
			for (CSPOFA__Orchestration_Step__c step : steps) {
				String errorInfo =
					'Error occurred (exception thrown): ' + getFormateddErrorMessage(e);
				results.add(OrchUtils.setStepRecord(step, true, errorInfo.abbreviate(255)));
			}
		}
		return results;
	}

	private String getFormateddErrorMessage(Exception e) {
		return e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString();
	}
}