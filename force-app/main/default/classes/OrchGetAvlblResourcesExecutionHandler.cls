@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class OrchGetAvlblResourcesExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
	private Map<Id, List<OrchCalloutsWrapperCls.calloutData>> calloutResultsMap;
	private Map<Id, String> mapOrderId;
	private Map<Id, String> mapbillingCustomer;
	private Map<Id, String> mapsalesJourney;
	private static Map<String, List<String>> mapTypeListMSISDNs = new Map<String, List<String>>();

	public Boolean performCallouts(List<SObject> data) {
		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;
		calloutResultsMap = new Map<Id, List<OrchCalloutsWrapperCls.calloutData>>();
		Boolean calloutsPerformed = false;
		try {
			Set<Id> resultIds = new Set<Id>();
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				resultIds.add(step.CSPOFA__Orchestration_Process__c);
			}
			OrchUtils.getProcessDetails(resultIds);
			mapOrderId = OrchUtils.mapOrderId;
			mapbillingCustomer = OrchUtils.mapbillingCustomer;
			mapsalesJourney = OrchUtils.mapsalesJourney;

			for (CSPOFA__Orchestration_Step__c step : stepList) {
				Id processId = step.CSPOFA__Orchestration_Process__c;
				calloutResultsMap.put(
					step.Id,
					EMP_BSLintegration.getResourceTypeForCTNs(
						mapOrderId.get(processId),
						mapbillingCustomer.get(processId),
						mapsalesJourney.get(processId)
					)
				);
				calloutsPerformed = true;
			}
		} catch (exception e) {
			System.debug(LoggingLevel.DEBUG, e.getMessage());
		}
		return calloutsPerformed;
	}

	public List<sObject> process(List<sObject> data) {
		List<sObject> result = new List<sObject>();
		List<NetProfit_CTN__c> ctnToUpdate = new List<NetProfit_CTN__c>();
		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;
		Map<Id, List<NetProfit_CTN__c>> mapOIdLstCTN = getCTNsPerOrderId(mapOrderId);

		for (CSPOFA__Orchestration_Step__c step : stepList) {
			if (calloutResultsMap.containsKey(step.Id)) {
				List<OrchCalloutsWrapperCls.calloutData> calloutResult = calloutResultsMap.get(
					step.Id
				);
				if (calloutResult != null && calloutResult[0].success) {
					try {
						Id processId = step.CSPOFA__Orchestration_Process__c;
						Id orderId = (Id) (mapOrderId.get(processId));
						getMSISDNs(calloutResult);
						List<NetProfit_CTN__c> lstCTNQuery = mapOIdLstCTN.get(orderId);
						Map<String, List<NetProfit_CTN__c>> mapNewNumberListCTN = getCTNsByAction(
							lstCTNQuery,
							'New'
						);
						Map<String, List<NetProfit_CTN__c>> mapPortingNumberListCTN = getCTNsByAction(
							lstCTNQuery,
							'Porting'
						);
						Map<String, List<NetProfit_CTN__c>> mapRetentionNumberListCTN = getCTNsByAction(
							lstCTNQuery,
							'Retention'
						);
						ctnToUpdate.addAll(processNewCTNs(mapNewNumberListCTN));
						ctnToUpdate.addAll(processExistingCTNs(mapPortingNumberListCTN));
						ctnToUpdate.addAll(processExistingCTNs(mapRetentionNumberListCTN));
						String strMessage = calloutResult[0]
								.errorMessage.equalsIgnoreCase('Callout not Needed')
							? calloutResult[0].errorMessage
							: 'Get available resources step completed';
						step = OrchUtils.setStepRecord(step, false, strMessage);
					} catch (Exception e) {
						step = OrchUtils.setStepRecord(
							step,
							true,
							'Error occurred: 1 ' +
							e +
							' - ' +
							e.getLineNumber() +
							' - ' +
							e.getStackTraceString()
						);
					}
				} else {
					step = OrchUtils.setStepRecord(
						step,
						true,
						'Error occurred: 2 ' + (String) calloutResult[0].errorMessage
					);
				}
				step = setConcatenateRequestResponse(calloutResult, step);
			} else {
				step = OrchUtils.setStepRecord(
					step,
					true,
					'Error occurred: Callout results not received.'
				);
			}
			result.add(step);
		}
		result = OrchUtils.tryUpdateRelatedRecord(stepList, result, ctnToUpdate);
		return result;
	}

	//method as enhancement to not overwrite the previous request and reponse when a new batch of CTNs is requested
	//and to make easier the debugging
	private static CSPOFA__Orchestration_Step__c setConcatenateRequestResponse(
		List<OrchCalloutsWrapperCls.calloutData> calloutResult,
		CSPOFA__Orchestration_Step__c step
	) {
		//get if step already contains an earlier request and concatenate to new batch of CTNs requested
		String strOldRequest = String.isNotEmpty(step.Request__c)
			? step.Request__c + ' -new-req-> '
			: '';
		step.Request__c = (calloutResult?.get(0)?.strReq != null
				? strOldRequest + calloutResult?.get(0).strReq
				: '')
			.removeEnd(' -new-req-> ')
			.abbreviate(131000);
		//get if step already contains an earlier response and concatenate to new batch of CTNs requested
		String strOldResponse = String.isNotEmpty(step.Response__c)
			? step.Response__c + ' -new-res-> '
			: '';
		step.Response__c = (calloutResult?.get(0)?.strRes != null
				? strOldResponse + calloutResult?.get(0)?.strRes
				: '')
			.removeEnd(' -new-res-> ')
			.abbreviate(131000);
		return step;
	}

	private static Map<Id, List<NetProfit_CTN__c>> getCTNsPerOrderId(Map<Id, String> mapOrderId) {
		List<NetProfit_CTN__c> lstCTNQuery = [
			SELECT
				Id,
				Order__c,
				CTN_Status__c,
				Action__c,
				Price_Plan_Class__c,
				Product_Group__c,
				CTN_Number__c
			FROM NetProfit_CTN__c
			WHERE
				Order__c IN :mapOrderId.values()
				AND CTN_Status__c = 'Future'
				AND (CTN_Number__c != ''
				OR CTN_Number__c != NULL)
		];
		return (Map<Id, List<NetProfit_CTN__c>>) GeneralUtils.groupByIDField(
			lstCTNQuery,
			'Order__c'
		);
	}

	private static void getMSISDNs(List<OrchCalloutsWrapperCls.calloutData> calloutResult) {
		if (calloutResult[0].errorMessage != 'Callout not Needed') {
			for (OrchCalloutsWrapperCls.calloutData objCD : calloutResult) {
				String strKey = objCD.infoToCOM.get('requestType');
				List<String> listmsisdns = mapTypeListMSISDNs.get(strKey);
				if (listmsisdns == null) {
					listmsisdns = new List<String>();
					mapTypeListMSISDNs.put(objCD.infoToCOM.get('requestType'), listmsisdns);
				}
				listmsisdns.add(objCD.infoToCOM.get('numberCTN'));
			}
		}
	}

	private static Map<String, List<NetProfit_CTN__c>> getCTNsByAction(
		List<NetProfit_CTN__c> lstCTNQuery,
		String action
	) {
		Map<String, List<NetProfit_CTN__c>> mapReturn = new Map<String, List<NetProfit_CTN__c>>();
		for (NetProfit_CTN__c ctn : lstCTNQuery) {
			if (ctn.Action__c.equalsIgnoreCase(action)) {
				if (!mapReturn.containsKey(ctn.CTN_Number__c)) {
					mapReturn.put(ctn.CTN_Number__c, new List<NetProfit_CTN__c>());
				}
				mapReturn.get(ctn.CTN_Number__c).add(ctn);
			}
		}
		return mapReturn;
	}

	private static List<NetProfit_CTN__c> processNewCTNs(
		Map<String, List<NetProfit_CTN__c>> mapNewNumberListCTN
	) {
		List<NetProfit_CTN__c> retlstCTN = new List<NetProfit_CTN__c>();
		for (String ctnXXXX : mapNewNumberListCTN.keySet()) {
			List<String> listMsisdnsData = getListFromMap('Data');
			List<String> listMsisdnsVoice = getListFromMap('Voice');
			for (NetProfit_CTN__c ctn : mapNewNumberListCTN.get(ctnXXXX)) {
				if (
					ctn.Price_Plan_Class__c.equalsIgnoreCase('Data') && !listMsisdnsData.isEmpty()
				) {
					ctn.CTN_Number__c = listMsisdnsData.get(0);
					ctn.CTN_Status__c = 'MSISDN Assigned';
					listMsisdnsData.remove(0);
				} else if (
					ctn.Price_Plan_Class__c.equalsIgnoreCase('Voice') && !listMsisdnsVoice.isEmpty()
				) {
					ctn.CTN_Number__c = listMsisdnsVoice.get(0);
					ctn.CTN_Status__c = 'MSISDN Assigned';
					listMsisdnsVoice.remove(0);
				}
				retlstCTN.add(ctn);
			}
		}
		return retlstCTN;
	}

	private static List<String> getListFromMap(String strResourceAsked) {
		return mapTypeListMSISDNs.containsKey(strResourceAsked)
			? mapTypeListMSISDNs.get(strResourceAsked)
			: new List<String>();
	}

	private static List<NetProfit_CTN__c> processExistingCTNs(
		Map<String, List<NetProfit_CTN__c>> mapExistingnNumberListCTN
	) {
		List<NetProfit_CTN__c> retlstCTN = new List<NetProfit_CTN__c>();
		for (String ctnPor : mapExistingnNumberListCTN.keySet()) {
			for (NetProfit_CTN__c ctn : mapExistingnNumberListCTN.get(ctnPor)) {
				if (ctn.CTN_Status__c.equalsIgnoreCase('Future')) {
					if (!ctn.CTN_Number__c.containsIgnoreCase('XX')) {
						ctn.CTN_Status__c = 'MSISDN Assigned';
					} else {
						ctn.CTN_Status__c = 'MSISDN Not Assigned';
					}
					retlstCTN.add(ctn);
				}
			}
		}
		return retlstCTN;
	}
}