//This is the custom controller class for the CS Orchestrator custom step 'Get Default Billing Customer'.
global without sharing class  OrchGetDBCExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable{
    private Map<Id,List<OrchCalloutsWrapperCls.calloutData>> calloutResultsMap = new Map<Id,List<OrchCalloutsWrapperCls.calloutData>>(); 
    private Map<Id,String> mapOrderId;
    private Map<Id,String> mapbillingCustomer;
    private Map<Id,String> mapcontractId;
    
    public Boolean performCallouts(List<SObject> data) {
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        Boolean calloutsPerformed = false;
        try{
            Set<Id> resultIds = new Set<Id>();
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                resultIds.add( step.CSPOFA__Orchestration_Process__c );
            }
            OrchUtils.getProcessDetails(resultIds);
            mapOrderId = OrchUtils.mapOrderId;
            mapbillingCustomer = OrchUtils.mapbillingCustomer;
            mapcontractId = OrchUtils.mapcontractId;
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                Id processId = step.CSPOFA__Orchestration_Process__c;
                String orderId = mapOrderId.get( processId );
                String billingCustomerId = mapbillingCustomer.get( processId );			
                calloutResultsMap.put(step.Id,EMP_BSLintegration.getDefaultBillingcustomer(orderId, billingCustomerId));      
                calloutsPerformed = true;            
            }
        } catch (exception e) {
            system.debug(e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString());
        }
        return calloutsPerformed;
    }

    public List<sObject> process(List<sObject> data) {
        List<sObject> result = new List<sObject>();
        List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        for (CSPOFA__Orchestration_Step__c step : stepList) {
            Boolean success = false;
            if(calloutResultsMap.containsKey(step.Id)) {
                List<OrchCalloutsWrapperCls.calloutData> calloutResult = calloutResultsMap.get(step.Id);
                if(calloutResult != null && (Boolean) calloutResult[0].success == true ) {
                    success = true;
                }
                if(success) { 
                    step = OrchUtils.setStepRecord( step , false , 'Default Billing Customer fetching step completed' );
                    String contractId = mapcontractId.get( step.CSPOFA__Orchestration_Process__c );
                    contractsToUpdate.add(new VF_Contract__c(
                        Id=contractId,
                        Default_Billing_Customer__c=(String) calloutResult[0].infoToCOM.get('defaultBillingCustomerId'))); 
                } else {   
                    step = OrchUtils.setStepRecord( step , true , 'Error occurred: '+(String) calloutResult[0].errorMessage );         	
                }
                step.Request__c = calloutResult[0].strReq;
                step.Response__c = calloutResult[0].strRes;
            } else {  
                step = OrchUtils.setStepRecord( step , true , 'Error occurred: Callout results not received.' );          	                
            }
            result.add(step);
        }
        result = tryUpdateRelatedRecord( stepList , result, contractsToUpdate , mapcontractId );
        return result;
    }

    private static List<sObject> tryUpdateRelatedRecord(List<CSPOFA__Orchestration_Step__c> stepList, List<sObject> result, List<VF_Contract__c> contractsToUpdate , Map<Id,String> mapcontractId) {
        try{
            Map<Id,String> failedContractUpdateMap = new Map<Id,String>();
            List<Database.SaveResult> updateResults = Database.update(contractsToUpdate, false);
            for(Integer i=0;i<updateResults.size();i++) {
                if (!updateResults.get(i).isSuccess()) { 
                    failedContractUpdateMap.put(contractsToUpdate.get(i).Id,updateResults.get(i).getErrors().get(0).getMessage()); 
                }
            }
            if(!failedContractUpdateMap.isEmpty()) {
                for (CSPOFA__Orchestration_Step__c step : stepList) {
                    String contractId = mapcontractId.get( step.CSPOFA__Orchestration_Process__c );
                    if(failedContractUpdateMap.containsKey(contractId)) {
                        result.add(OrchUtils.setStepRecord( step , true , 'Error occurred: Contract update failed with error: '+failedContractUpdateMap.get(contractId)));
                    }
                }
            }
        } catch (Exception e) {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                result.add(OrchUtils.setStepRecord( step , true , ('Error occurred: '+e.getMessage() + ' on line '+ e.getLineNumber() + e.getStackTraceString()).abbreviate(255)));
            }
        }
        return result;
    }
}