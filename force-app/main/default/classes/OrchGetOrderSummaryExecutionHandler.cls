//This is the custom controller class for the CS Orchestrator custom step 'Get Order Summary'.
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class OrchGetOrderSummaryExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
	private Map<Id, List<OrchCalloutsWrapperCls.calloutData>> calloutResultsMap = new Map<Id, List<OrchCalloutsWrapperCls.calloutData>>();
	private String calloutError;
	private Map<Id, List<String>> orderIdToCTNs;
	private Map<Id, String> mapOrderId;

	public Boolean performCallouts(List<SObject> data) {
		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;
		calloutResultsMap = new Map<Id, List<OrchCalloutsWrapperCls.calloutData>>();
		Boolean calloutsPerformed = false;
		try {
			Set<Id> resultIds = new Set<Id>();
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				resultIds.add(step.CSPOFA__Orchestration_Process__c);
			}
			OrchUtils.getProcessDetails(resultIds);
			mapOrderId = OrchUtils.mapOrderId;
			orderIdToCTNs = new Map<Id, List<String>>();
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				Id processId = step.CSPOFA__Orchestration_Process__c;
				String orderId = mapOrderId.get(processId);
				orderIdToCTNs.put(orderId, new List<String>());
			}
			EMP_Automatic_Order_Flow__c csEMP = EMP_Automatic_Order_Flow__c.getOrgDefaults();
			Datetime datefilter = system.now()
				.addMinutes(-1 * (csEMP.Get_Order_Summary_Time_Check__c.intValue()));
			for (NetProfit_CTN__c ctn : [
				SELECT Id, Order__c
				FROM NetProfit_CTN__c
				WHERE
					Order__c IN :orderIdToCTNs.keySet()
					AND ((CTN_Status__c = 'SIM Activation Order ID received'
					AND SIM_Activation_Timestamp__c < :datefilter)
					OR (CTN_Status__c = 'Update Product Settings confirmed'
					AND Update_Settings_Confirmed_Timestamp__c < :datefilter)
					OR (CTN_Status__c = 'Order Summary pending'
					AND (SIM_Activation_Timestamp__c < :datefilter
					OR Retention_Confirmed_Timestamp__c < :datefilter))
					OR (CTN_Status__c = 'Retention Order ID Received'
					AND Retention_Confirmed_Timestamp__c < :datefilter))
			]) {
				orderIdToCTNs.get(ctn.Order__c).add(ctn.Id);
			}
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				Id processId = step.CSPOFA__Orchestration_Process__c;
				String orderId = mapOrderId.get(processId);
				calloutResultsMap.put(
					step.Id,
					EMP_BSLintegration.getOrderSummaryBulk(orderIdToCTNs.get(orderId))
				);
				calloutsPerformed = true;
			}
		} catch (exception e) {
			System.debug(
				LoggingLevel.DEBUG,
				(e.getMessage() +
					' on line ' +
					e.getLineNumber() +
					e.getStackTraceString())
					.abbreviate(255)
			);
		}
		return calloutsPerformed;
	}

	public List<sObject> process(List<sObject> data) {
		List<sObject> result = new List<sObject>();
		List<NetProfit_CTN__c> ctnToUpdate = new List<NetProfit_CTN__c>();
		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;
		for (CSPOFA__Orchestration_Step__c step : stepList) {
			if (calloutResultsMap.containsKey(step.Id)) {
				List<OrchCalloutsWrapperCls.calloutData> calloutResult = calloutResultsMap.get(
					step.Id
				);
				if (calloutResult != null && calloutResult[0].success) {
					String strMessage = calloutResult[0]
							.errorMessage.equalsIgnoreCase('Callout not Needed')
						? calloutResult[0].errorMessage
						: 'Get order summary fetching step completed';
					step = OrchUtils.setStepRecord(step, false, strMessage);
					String orderId = mapOrderId.get(step.CSPOFA__Orchestration_Process__c);
					ctnToUpdate = setCTNrecords(strMessage, orderId, ctnToUpdate);
				} else {
					step = OrchUtils.setStepRecord(
						step,
						true,
						'Error occurred: ' + (String) calloutResult[0].errorMessage
					);
				}
				step.Request__c = calloutResult != null &&
					!calloutResult.isEmpty() &&
					calloutResult[0].strReq != null
					? calloutResult[0].strReq.abbreviate(131000)
					: '';
				step.Response__c = calloutResult != null &&
					!calloutResult.isEmpty() &&
					calloutResult[0].strRes != null
					? calloutResult[0].strRes.abbreviate(131000)
					: '';
			} else {
				step = OrchUtils.setStepRecord(
					step,
					true,
					'Error occurred: Callout results not received.'
				);
			}
			result.add(step);
		}
		result = orchUtils.tryUpdateRelatedRecord(stepList, result, ctnToUpdate);
		return result;
	}

	private List<NetProfit_CTN__c> setCTNrecords(
		String strMessage,
		String orderId,
		List<NetProfit_CTN__c> ctnToUpdate
	) {
		if (strMessage.equalsIgnoreCase('Get order summary fetching step completed')) {
			for (Id ctnId : orderIdToCTNs.get(orderId)) {
				NetProfit_CTN__c ctn = new NetProfit_CTN__c(
					Id = ctnId,
					CTN_Status__c = 'Order Summary pending'
				);
				ctnToUpdate.add(ctn);
			}
		}
		return ctnToUpdate;
	}
}