//This is the custom controller class for the CS Orchestrator custom step 'Update Product Settings'.
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class OrchUpdProductSettingsExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
	private Map<Id, List<OrchCalloutsWrapperCls.calloutData>> calloutResultsMap = new Map<Id, List<OrchCalloutsWrapperCls.calloutData>>();
	private Map<Id, String> mapOrderId;
	private Map<Id, List<String>> orderIdToCTNs = new Map<Id, List<String>>();

	public Boolean performCallouts(List<SObject> data) {
		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;
		calloutResultsMap = new Map<Id, List<OrchCalloutsWrapperCls.calloutData>>();
		Boolean calloutsPerformed = false;
		try {
			Set<Id> resultIds = new Set<Id>();
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				resultIds.add(step.CSPOFA__Orchestration_Process__c);
			}
			OrchUtils.getProcessDetails(resultIds);
			mapOrderId = OrchUtils.mapOrderId;
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				Id processId = step.CSPOFA__Orchestration_Process__c;
				String orderId = mapOrderId.get(processId);
				orderIdToCTNs.put(orderId, new List<String>());
			}

			for (NetProfit_CTN__c ctn : [
				SELECT Id, Order__c
				FROM NetProfit_CTN__c
				WHERE Order__c IN :orderIdToCTNs.keySet() AND CTN_Status__c = 'Future'
			]) {
				orderIdToCTNs.get(ctn.Order__c).add(ctn.Id);
			}

			for (CSPOFA__Orchestration_Step__c step : stepList) {
				Id processId = step.CSPOFA__Orchestration_Process__c;
				String orderId = mapOrderId.get(processId);
				calloutResultsMap.put(
					step.Id,
					EMP_BSLintegration.updateProductSettingsBulk(orderId)
				);
				calloutsPerformed = true;
			}
		} catch (exception e) {
			system.debug(
				LoggingLevel.DEBUG,
				(e.getMessage() +
					' on line ' +
					e.getLineNumber() +
					e.getStackTraceString())
					.abbreviate(255)
			);
		}
		return calloutsPerformed;
	}

	public List<sObject> process(List<sObject> data) {
		List<sObject> result = new List<sObject>();
		List<NetProfit_CTN__c> ctnToUpdate = new List<NetProfit_CTN__c>();
		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;
		for (CSPOFA__Orchestration_Step__c step : stepList) {
			if (calloutResultsMap.containsKey(step.Id)) {
				List<OrchCalloutsWrapperCls.calloutData> calloutResult = calloutResultsMap.get(
					step.Id
				);
				if (calloutResult != null && (Boolean) calloutResult[0].success == true) {
					// simulate positive result
					String strMessage = calloutResult[0]
							.errorMessage.equalsIgnoreCase('Callout not Needed')
						? calloutResult[0].errorMessage
						: 'Update Product Setting in bulk step completed';
					step = OrchUtils.setStepRecord(step, false, strMessage);
					String orderId = mapOrderId.get(step.CSPOFA__Orchestration_Process__c);
					ctnToUpdate = setCTNrecords(strMessage, orderId, ctnToUpdate);
				} else {
					step = OrchUtils.setStepRecord(
						step,
						true,
						'Error occurred: ' + (String) calloutResult[0].errorMessage
					);
				}
				step.Request__c = calloutResult != null &&
					!calloutResult.isEmpty() &&
					calloutResult[0].strReq != null
					? calloutResult[0].strReq.abbreviate(131000)
					: '';
				step.Response__c = calloutResult != null &&
					!calloutResult.isEmpty() &&
					calloutResult[0].strRes != null
					? calloutResult[0].strRes.abbreviate(131000)
					: '';
			} else {
				step = OrchUtils.setStepRecord(
					step,
					true,
					'Error occurred: Callout results not received.'
				);
			}
			result.add(step);
		}
		result = orchUtils.tryUpdateRelatedRecord(stepList, result, ctnToUpdate);
		return result;
	}

	private List<NetProfit_CTN__c> setCTNrecords(
		String strMessage,
		String orderId,
		List<NetProfit_CTN__c> ctnToUpdate
	) {
		if (strMessage.equalsIgnoreCase('Update Product Setting in bulk step completed')) {
			for (Id ctnId : orderIdToCTNs.get(orderId)) {
				NetProfit_CTN__c ctn = new NetProfit_CTN__c(
					Id = ctnId,
					CTN_Status__c = 'Update Product Settings confirmed'
				);
				ctnToUpdate.add(ctn);
			}
		}
		return ctnToUpdate;
	}
}