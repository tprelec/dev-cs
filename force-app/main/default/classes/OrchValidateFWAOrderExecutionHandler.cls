//This is the custom controller class for the CS Orchestrator custom step 'Validate FWA Order'.
global without sharing class OrchValidateFWAOrderExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
    private Map<Id,List<OrchCalloutsWrapperCls.calloutData>> calloutResultsMap = new Map<Id,List<OrchCalloutsWrapperCls.calloutData>>(); 
    private String calloutError;
    private Map<Id,String> mapcontractId;
    
    public Boolean performCallouts(List<SObject> data) {
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        calloutResultsMap = new Map<Id,List<OrchCalloutsWrapperCls.calloutData>>();
        Boolean calloutsPerformed = false;
        
        try{
            Set<Id> resultIds = new Set<Id>();
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                resultIds.add( step.CSPOFA__Orchestration_Process__c );
            }
            OrchUtils.getProcessDetails(resultIds);
            mapcontractId = OrchUtils.mapcontractId;
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                String contractId = mapcontractId.get( step.CSPOFA__Orchestration_Process__c );			
                calloutResultsMap.put(step.Id,EMP_BSLintegration.createFramework(contractId)); 
                calloutsPerformed = true;
            }
        } catch (exception e) {
            system.debug((e.getMessage() + ' on line '+ e.getLineNumber() + e.getStackTraceString()).abbreviate(255));
        }
        return calloutsPerformed;
    }
    
    public List<sObject> process(List<sObject> data) {
        List<sObject> result = new List<sObject>();
        List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        for (CSPOFA__Orchestration_Step__c step : stepList) {
            if(calloutResultsMap.containsKey(step.Id)) {
                List<OrchCalloutsWrapperCls.calloutData> calloutResult = calloutResultsMap.get(step.Id);
                if(calloutResult != null && (Boolean) calloutResult[0].success == true) { 
                    step = OrchUtils.setStepRecord( step , false , 'Validate Create Framework step completed' );
                    String contractId = mapcontractId.get( step.CSPOFA__Orchestration_Process__c );
                    contractsToUpdate.add(new VF_Contract__c(
                        Id=contractId,
                        Unify_Order_Id__c=(String) calloutResult[0].infoToCOM.get('orderId'),
                        Unify_Framework_Id__c= (String) calloutResult[0].infoToCOM.get('FrameworkId')));
                } else {
                    step = OrchUtils.setStepRecord( step , true , 'Error occurred: '+(String) calloutResult[0].errorMessage );          	
                }
                step.Request__c = calloutResult != null && !calloutResult.isEmpty() && calloutResult[0].strReq != null? calloutResult[0].strReq.abbreviate(131000) : '' ;
                step.Response__c = calloutResult != null && !calloutResult.isEmpty() && calloutResult[0].strRes != null? calloutResult[0].strRes.abbreviate(131000) : '' ;
            } else {  
                step = OrchUtils.setStepRecord( step , true , 'Error occurred: Callout results not received.' );           	                
            }
            result.add(step);
    	}
        result = tryUpdateRelatedRecord( stepList , result, contractsToUpdate , mapcontractId );
        return result;
    }

    private static List<sObject> tryUpdateRelatedRecord(List<CSPOFA__Orchestration_Step__c> stepList, List<sObject> result, List<VF_Contract__c> contractsToUpdate , Map<Id,String> mapcontractId) {
        try{
            Map<Id,String> failedContractUpdateMap = new Map<Id,String>();
            List<Database.SaveResult> updateResults = Database.update(contractsToUpdate, false);
            for(Integer i=0;i<updateResults.size();i++) {
				if (!updateResults.get(i).isSuccess()) {
                    Database.Error error = updateResults.get(i).getErrors().get(0);
                    failedContractUpdateMap.put(contractsToUpdate.get(i).Id,error.getMessage());
                 }
            }
            if(!failedContractUpdateMap.isEmpty()) {
                for (CSPOFA__Orchestration_Step__c step : stepList) {
                    String contractId = mapcontractId.get( step.CSPOFA__Orchestration_Process__c );
                    if(failedContractUpdateMap.containsKey(contractId)) {
                        result.add(OrchUtils.setStepRecord( step , true , 'Error occurred: VF Contract update failed with error: ' + failedContractUpdateMap.get(contractId)));
                    }
                }
            }
        } catch (Exception e) {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                result.add(OrchUtils.setStepRecord( step , true , ('Error occurred: ' + e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString()).abbreviate(255)));
            }
        }
        return result;
    }
}