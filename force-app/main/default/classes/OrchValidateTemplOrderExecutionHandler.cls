//This is the custom controller class for the CS Orchestrator custom step 'Validate Template Order'.
global without sharing class OrchValidateTemplOrderExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
    private Map<Id,List<OrchCalloutsWrapperCls.calloutData>> calloutResultsMap = new Map<Id,List<OrchCalloutsWrapperCls.calloutData>>(); 
    private Map<Id,String> mapcontractedProductId;
    private Map<Id,String> mapcpFrameworkId;
    
    public Boolean performCallouts(List<SObject> data) {
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        calloutResultsMap = new Map<Id,List<OrchCalloutsWrapperCls.calloutData>>();
        Boolean calloutsPerformed = false;
        String contractedProductId;
        String frameworkId;
        try{
            Set<Id> resultIds = new Set<Id>();
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                resultIds.add( step.CSPOFA__Orchestration_Process__c );
            }
            OrchUtils.getProcessDetails(resultIds);
            mapcontractedProductId = OrchUtils.mapcontractedProductId;
            mapcpFrameworkId = OrchUtils.mapcpFrameworkId;
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                contractedProductId = mapcontractedProductId.get( step.CSPOFA__Orchestration_Process__c );
                frameworkId = mapcpFrameworkId.get( step.CSPOFA__Orchestration_Process__c );
                calloutResultsMap.put(step.Id,EMP_BSLintegration.createTemplate(contractedProductId,frameworkId));       
                calloutsPerformed = true;          
            }
        } catch (exception e) {
            system.debug(('cpId:' + contractedProductId + 'cpframeworkId:' + frameworkId + ' ' + e.getMessage() + ' on line '+ e.getLineNumber() + e.getStackTraceString()).abbreviate(255));
        }
        return calloutsPerformed;
    }

    public List<sObject> process(List<sObject> data) {
        List<sObject> result = new List<sObject>();
        List<Contracted_Products__c> contractedProductsToUpdate = new List<Contracted_Products__c>();
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        for (CSPOFA__Orchestration_Step__c step : stepList) {
            if(calloutResultsMap.containsKey(step.Id)) {
                List<OrchCalloutsWrapperCls.calloutData> calloutResult = calloutResultsMap.get(step.Id);
                if(calloutResult != null && (Boolean) calloutResult[0].success == true) { 
                    step = OrchUtils.setStepRecord( step , false , 'Validate Create Template step completed' );
                    String contractedProductId = mapcontractedProductId.get( step.CSPOFA__Orchestration_Process__c );
                    contractedProductsToUpdate.add(new Contracted_Products__c(
                        Id=contractedProductId,
                        Unify_Template_Id__c=(String) calloutResult[0].infoToCOM.get('TemplateId'),
                        Unify_Order_Id__c=(String) calloutResult[0].infoToCOM.get('orderID'))); 
                } else { 
                    step = OrchUtils.setStepRecord( step , true , 'Error occurred: '+(String) calloutResult[0].errorMessage );         	
                }
                step.Request__c = calloutResult != null && !calloutResult.isEmpty() && calloutResult[0].strReq != null ? calloutResult[0].strReq : '';
                step.Response__c = calloutResult != null && !calloutResult.isEmpty() && calloutResult[0].strRes != null ? calloutResult[0].strRes.abbreviate(131000) : '';
            } else {           	                
                step = OrchUtils.setStepRecord( step , true , 'Error occurred: Callout results not received.' );         	
            }
            result.add(step);
    	}
        result = tryUpdateRelatedRecord( stepList , result, contractedProductsToUpdate , mapcontractedProductId );
        return result;
    }
    
    private static List<sObject> tryUpdateRelatedRecord(List<CSPOFA__Orchestration_Step__c> stepList, List<sObject> result, List<Contracted_Products__c> contractedProductsToUpdate , Map<Id,String> mapcontractedProductId) {
        try{
            Map<Id,String> failedContractUpdateMap = new Map<Id,String>();
            List<Database.SaveResult> updateResults = Database.update(contractedProductsToUpdate, false);
            for(Integer i=0;i<updateResults.size();i++) {
				if (!updateResults.get(i).isSuccess()) {
                    Database.Error error = updateResults.get(i).getErrors().get(0);
                    failedContractUpdateMap.put(contractedProductsToUpdate.get(i).Id,error.getMessage());
                 }
            }
            if(!failedContractUpdateMap.isEmpty()) {
                for (CSPOFA__Orchestration_Step__c step : stepList) {
                    String contractedProductId = mapcontractedProductId.get( step.CSPOFA__Orchestration_Process__c );
                    if(failedContractUpdateMap.containsKey(contractedProductId)) {
                        step = OrchUtils.setStepRecord( step , true , 'Error occurred: Contracted Product update failed with error: '+failedContractUpdateMap.get(contractedProductId) );
                    }
                }
            }
        } catch (Exception e) {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step = OrchUtils.setStepRecord( step , true , ('Error occurred: ' + e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString()).abbreviate(255) );
            }
        }
        return result;
    }
}