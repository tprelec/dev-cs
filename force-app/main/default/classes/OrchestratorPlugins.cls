/**
 * @description Required for assigning a custom calendar to work with Orchestrator
 * needs to have the global modifier in order to work properly because of the different namespaces.
 */
global class OrchestratorPlugins {
    global class OperationalLevelAgreementActivityProvider implements CSPOFA.OlaActivityOperations {
        private String calendarId;
        public OperationalLevelAgreementActivityProvider() {
        }
        public void setIdentifier(String identifier) {
            calendarId = identifier;
        }
        public Long getTimeAmount(Datetime startTime, Datetime endTime) {
            CSSX.Calendar_Funcs_API_V1.SearchContext sc = new CSSX.Calendar_Funcs_API_V1.SearchContext(calendarId);
            sc.startDateTime = startTime;
            sc.endDateTime = endTime;
            Long milis = CSSX.Calendar_Funcs_API_V1.getDuration(sc, CSSX.Calendar_Funcs_API_V1.SearchType.MILLISECOND);
            return milis;
        }
        public Datetime getNextTimepoint(Datetime startTime, Long timeAmount) {
            CSSX.Calendar_Funcs_API_V1.SearchContext sc = new CSSX.Calendar_Funcs_API_V1.SearchContext(calendarId);
            sc.startDateTime = startTime;
            Datetime newTimePoint = CSSX.Calendar_Funcs_API_V1.forward(sc, CSSX.Calendar_Funcs_API_V1.SearchType.MILLISECOND, timeAmount);
            return newTimePoint;
        }
        public String getIdentifierType() {
            return 'CSSX__Calendar__c';
        }
    }
    global class OperationalLevelAgreementActivityProviderFactory implements CSPOFA.PluginFactory {
        public Object create() {
            return new OperationalLevelAgreementActivityProvider();
        }
    }
}