public with sharing class OrderEntryController {
	private static Set<Id> productIds;
	private static Set<Id> addonIds;
	private static List<String> promotionNames;

	// Get the initial data from JSON or initialize a new JSON
	@AuraEnabled
	public static OrderEntryData getOrderEntryData(String oppId) {
		OrderEntryData data = new OrderEntryData();

		List<Attachment> att = [
			SELECT Id, Body
			FROM Attachment
			WHERE ParentId = :oppId AND Name = 'OrderEntryData.json'
		];

		if (!att.isEmpty()) {
			data = (OrderEntryData) JSON.deserialize(att[0].Body.toString(), OrderEntryData.class);
			data.init();
		} else {
			Opportunity opp = [
				SELECT Id, AccountId, ContactId
				FROM Opportunity
				WHERE Id = :oppId
				WITH SECURITY_ENFORCED
			];

			data.opportunityId = opp.Id;
			data.accountId = opp.AccountId;
			data.init();

			insertAttachment(JSON.serializePretty(data), oppId);
		}
		return data;
	}

	@future
	static void insertAttachment(String body, String oppId) {
		Attachment att = new Attachment(
			Name = 'OrderEntryData.json',
			Body = Blob.valueOf(body),
			ParentId = oppId
		);
		insert att;
	}

	@AuraEnabled
	public static Boolean updateState(Id oppId, String state) {
		try {
			List<Attachment> att = [
				SELECT Id, Body
				FROM Attachment
				WHERE ParentId = :oppId AND Name = 'OrderEntryData.json'
			];

			att[0].Body = Blob.valueOf(state);

			update att;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@AuraEnabled(cacheable=true)
	public static List<OE_Product__c> getProducts(List<String> productTypes) {
		String query = 'SELECT Name, Sort_Order__c, Type__c FROM OE_Product__c';
		if (productTypes != null && productTypes.size() != 0) {
			query += ' WHERE Type__c IN :productTypes';
		}
		query += ' ORDER BY Sort_Order__c';

		List<OE_Product__c> products = Database.query(query);

		return products;
	}

	@AuraEnabled
	public static List<OE_Product_Add_On__c> getAddons(Id parentProduct) {
		List<OE_Product_Add_On__c> addons = [
			SELECT
				Id,
				Product__c,
				Sort_Order__c,
				Automatic_Add_On__c,
				Recurring_Price_Override__c,
				One_Off_Price_Override__c,
				Add_On__r.Type__c,
				Add_On__r.Product_Code__c,
				Add_On__r.Name,
				Add_On__r.Max_Quantity__c
			FROM OE_Product_Add_On__c
			WHERE Product__c = :parentProduct
			ORDER BY Sort_Order__c
		];
		return addons;
	}

	@AuraEnabled
	public static cspmb__Add_On_Price_Item__c getAddonPrice(String code) {
		if (code == null) {
			throw new AuraHandledException('No code selected!');
		}

		List<cspmb__Add_On_Price_Item__c> prices = [
			SELECT
				Id,
				Name,
				cspmb__One_Off_Charge_External_Id__c,
				cspmb__Recurring_Charge_External_Id__c,
				cspmb__One_Off_Charge__c,
				cspmb__Recurring_Charge__c,
				cspmb__Product_Definition_Name__c,
				Product_Name__c
			FROM cspmb__Add_On_Price_Item__c
			WHERE
				cspmb__Is_Active__c = TRUE
				AND (cspmb__One_Off_Charge_External_Id__c = :code
				OR cspmb__Recurring_Charge_External_Id__c = :code)
		];

		if (prices.size() > 0) {
			return prices[0];
		}
		return null;
	}

	@AuraEnabled
	public static OE_Product_Bundle__c getDefaultBundle(Id mainProductId) {
		List<OE_Product_Bundle__c> bundles = [
			SELECT
				Name,
				Main_Product__c,
				Internet_Product__c,
				Telephony_Product__c,
				TV_Product__c,
				Bundle_Product_Code__c,
				Default__c
			FROM OE_Product_Bundle__c
			WHERE Main_Product__c = :mainProductId AND Default__c = TRUE
		];

		if (bundles.size() > 0) {
			return bundles[0];
		}
		return null;
	}

	@AuraEnabled
	public static OrderEntryData.OrderEntryBundle getBundle(
		Id mainId,
		Id internetId,
		Id telephonyId,
		Id tvId
	) {
		// now hardcoded to 1 but can be changed in the future to something else
		Integer quantity = 1;

		List<OE_Product_Bundle__c> bundles = [
			SELECT Name, Bundle_Product_Code__c, Type__c, Promo_Description__c
			FROM OE_Product_Bundle__c
			WHERE
				Main_Product__c = :mainId
				AND Internet_Product__c = :internetId
				AND Telephony_Product__c = :telephonyId
				AND TV_Product__c = :tvId
		];

		if (bundles.size() > 0) {
			String productCode = bundles[0].Bundle_Product_Code__c;
			List<cspmb__Price_Item__c> prices = [
				SELECT
					Id,
					cspmb__One_Off_Charge__c,
					cspmb__Recurring_Charge__c,
					cspmb__Product_Definition_Name__c
				FROM cspmb__Price_Item__c
				WHERE
					cspmb__Is_Active__c = TRUE
					AND (cspmb__One_Off_Charge_External_Id__c = :productCode
					OR cspmb__Recurring_Charge_External_Id__c = :productCode)
			];

			if (prices.size() > 0) {
				OrderEntryData.OrderEntryBundle b = new OrderEntryData.OrderEntryBundle();
				b.code = productCode;
				b.id = bundles[0].Id;
				b.name = bundles[0].Name;
				b.quantity = quantity;
				b.oneOff = prices[0].cspmb__One_Off_Charge__c;
				b.recurring = prices[0].cspmb__Recurring_Charge__c;
				b.totalOneOff = prices[0].cspmb__One_Off_Charge__c != null
					? prices[0].cspmb__One_Off_Charge__c * quantity
					: null;
				b.totalRecurring = prices[0].cspmb__Recurring_Charge__c != null
					? prices[0].cspmb__Recurring_Charge__c * quantity
					: null;
				b.type = bundles[0].Type__c;
				b.bundleName = prices[0].cspmb__Product_Definition_Name__c;
				b.promoDescription = bundles[0].Promo_Description__c;
				return b;
			}

			return null;
		}

		return null;
	}

	@AuraEnabled(cacheable=true)
	public static String getHeaderInfo(String oppId) {
		Opportunity opp = [SELECT Id, ContactId, Account.Name FROM Opportunity WHERE Id = :oppId];
		List<OpportunityContactRole> oppCont = [
			SELECT Id, Contact.Name
			FROM OpportunityContactRole
			WHERE opportunityId = :oppId AND IsPrimary = TRUE
		];

		String header = oppCont.isEmpty() ? ' ' : oppCont[0].Contact.Name;
		return opp.Account.Name + '  •  ' + header;
	}

	@AuraEnabled(cacheable=true)
	public static Account getAccountData(Id accId) {
		return [SELECT Id, Name, KVK_number__c FROM Account WHERE Id = :accId];
	}

	@AuraEnabled(cacheable=true)
	public static List<Contact> getContactData(Id accId) {
		return [
			SELECT Id, FirstName, LastName, MobilePhone, Phone, Email
			FROM Contact
			WHERE AccountId = :accId
		];
	}

	@AuraEnabled(cacheable=true)
	public static Contact getPrimaryContact(Id contId) {
		return [
			SELECT Id, FirstName, LastName, MobilePhone, Phone, Email
			FROM Contact
			WHERE Id = :contId
		];
	}

	@AuraEnabled
	public static Contact saveContact(Contact newContact, Id oppId) {
		Boolean createContactRole = true;
		// Save newly created contact
		Contact cnt = newContact;
		upsert cnt;

		// Set Primary contact on opportunity
		Opportunity opp = [SELECT Id, LG_PrimaryContact__c FROM Opportunity WHERE Id = :oppId];
		opp.LG_PrimaryContact__c = cnt.Id;
		update opp;

		// Get old primary contact(s) role and un-set the flag isPrimary
		List<OpportunityContactRole> oldPrimCnt = [
			SELECT Id, IsPrimary, OpportunityId, ContactId
			FROM OpportunityContactRole
			WHERE opportunityId = :oppId
		];
		for (OpportunityContactRole ocr : oldPrimCnt) {
			if (ocr.IsPrimary = true && ocr.ContactId != cnt.Id) {
				ocr.IsPrimary = false;
			} else if (ocr.ContactId == cnt.Id) {
				ocr.IsPrimary = true;
				createContactRole = false;
			}
		}

		update oldPrimCnt;

		// Set new contact as primary contact on opportunity
		if (createContactRole) {
			insert new OpportunityContactRole(
				ContactId = cnt.Id,
				Role = 'Administrative Contact',
				IsPrimary = true,
				opportunityId = oppId
			);
		}

		return cnt;
	}

	@AuraEnabled(cacheable=true)
	public static List<Site__c> getSites(Id accId) {
		return [
			SELECT
				Id,
				Site_Street__c,
				Site_Postal_Code__c,
				Site_House_Number__c,
				Site_City__c,
				Site_House_Number_Suffix__c
			FROM Site__c
			WHERE Site_Account__c = :accId
		];
	}

	@AuraEnabled
	public static Site__c getSite(Id siteId) {
		return [
			SELECT
				Id,
				Site_Street__c,
				Site_Postal_Code__c,
				Site_House_Number__c,
				Site_City__c,
				Site_House_Number_Suffix__c
			FROM Site__c
			WHERE Id = :siteId
		];
	}

	@AuraEnabled
	public static Map<String, String> oblicoSearch(String zip) {
		Map<String, String> addressInformation = new Map<String, String>();
		if (
			String.isNotBlank(zip) && Pattern.matches('^[0-9]{4}[a-z,A-Z]{2} ?[0-9,a-z,A-Z]*$', zip)
		) {
			OlbicoServiceJSON jService = new OlbicoServiceJSON();
			jService.setRequestRestJsonStreet(zip);
			jService.makeRequestRestJsonStreet(zip);

			for (SelectOption so : jService.AdresInfo()) {
				addressInformation.put(so.getValue(), so.getLabel());
			}
		}
		return addressInformation;
	}

	@AuraEnabled
	public static Boolean updateSite(Site__c updatedSite, Id opportunityId) {
		List<String> fields = new List<String>{
			'Site_Street__c',
			'Site_Postal_Code__c',
			'Site_House_Number__c',
			'Site_City__c',
			'Site_House_Number_Suffix__c'
		};
		Boolean updateSite = false;
		Site__c site = getSite(updatedSite.Id);

		for (String field : fields) {
			if (site.get(field) != updatedSite.get(field)) {
				site.put(field, updatedSite.get(field));
				updateSite = true;
			}
		}

		if (updateSite) {
			update site;
		}

		updateOpportunityAddress(site, opportunityId);

		return true;
	}

	public static void updateOpportunityAddress(Site__c site, Id opportunityId) {
		Opportunity opp = [SELECT Id FROM Opportunity WHERE Id = :opportunityId];

		opp.LG_InstallationCity__c = site.Site_City__c;
		opp.LG_InstallationHouseNumber__c = String.valueOf(site.Site_House_Number__c);
		opp.LG_InstallationHouseNumberExtension__c = site.Site_House_Number_Suffix__c;
		opp.LG_InstallationPostalCode__c = site.Site_Postal_Code__c;
		opp.LG_InstallationStreet__c = site.Site_Street__c;

		opp.Installation_Adres__c =
			site.Site_Street__c +
			' ' +
			String.valueOf(site.Site_House_Number__c) +
			' ' +
			(String.isBlank(site.Site_House_Number_Suffix__c)
				? ''
				: site.Site_House_Number_Suffix__c + ', ') +
			site.Site_Postal_Code__c +
			' ' +
			site.Site_City__c;

		update opp;
	}

	@AuraEnabled
	public static Site__c saveSite(String selectedAddress, Id accId) {
		Account acc = [SELECT Id, KVK_number__c FROM Account WHERE Id = :accId];

		List<String> address = selectedAddress.split('\\|');
		String street = address[0];
		Integer houseNr = Integer.valueof(address[1].Trim());
		String houseNrLetter = address[2];
		String houseNrLetterAddition = address[3];
		String postcode = address[4];
		String cityAddition = address.size() > 6 ? ' - ' + address[6] : '';
		String city = address[5] + cityAddition;

		Site__c newSite = new Site__c();
		newSite.Site_Account__c = acc.Id;
		newSite.KVK_Number__c = acc.KVK_number__c;
		newSite.Site_Street__c = street;
		newSite.Site_House_Number__c = houseNr;
		newSite.Site_House_Number_Suffix__c = houseNrLetter + ' ' + houseNrLetterAddition;
		newSite.Site_Postal_Code__c = postcode;
		newSite.Site_City__c = city;
		newSite.Olbico__c = true;
		newSite.Name = street;

		insert newSite;
		return newSite;
	}

	@AuraEnabled
	public static String siteCheck(String postCode, Integer houseNumber) {
		Map<String, String> params = new Map<String, String>();
		params.put(AddressCheckService.POSTAL_CODE, postCode.trim());
		params.put(AddressCheckService.HOUSE_NUMBER, String.valueOf(houseNumber).trim());

		String result = AddressCheckService.callAddressCheckService(params);

		return result;
	}

	@AuraEnabled
	public static void saveProducts(Id opportunityId) {
		OrderEntryData data = getOrderEntryData(opportunityId);
		OrderEntryProductBuilder builder = new OrderEntryProductBuilder(data);
		builder.build();
	}

	/**
	 * Update porting numbers for telephony products
	 */
	@AuraEnabled
	public static void savePortingNumbers(Id opportunityId) {
		OrderEntryData data = getOrderEntryData(opportunityId);
		Map<String, OrderEntryData.Telephony> telephony = data.telephony;

		List<LG_PortingNumber__c> current = [
			SELECT Id
			FROM LG_PortingNumber__c
			WHERE LG_Opportunity__c = :opportunityId
		];

		if (current.size() > 0) {
			delete current;
		}

		if (telephony != null && telephony.values().size() > 0) {
			List<LG_PortingNumber__c> nums = new List<LG_PortingNumber__c>();
			for (OrderEntryData.Telephony tel : telephony.values()) {
				if (tel.portingEnabled != null && tel.portingEnabled && tel.portingNumber != null) {
					LG_PortingNumber__c pn = new LG_PortingNumber__c();
					pn.LG_Opportunity__c = opportunityId;
					pn.LG_PhoneNumber__c = tel.portingNumber;
					pn.LG_Type__c = 'SOHO Telephony';
					nums.add(pn);
				}
			}
			if (nums.size() > 0) {
				insert nums;
			}
		}
	}

	/**
	 * Remove addons with quantity === 0
	 * And group by ID and sum quantites
	 */
	private static List<OrderEntryData.OrderEntryAddon> filterEmptyAddons(
		List<OrderEntryData.OrderEntryAddon> addons
	) {
		Map<Id, OrderEntryData.OrderEntryAddon> uniqueAddons = new Map<Id, OrderEntryData.OrderEntryAddon>();
		for (OrderEntryData.OrderEntryAddon addon : addons) {
			if (uniqueAddons.get(addon.id) != null) {
				uniqueAddons.get(addon.id).quantity =
					uniqueAddons.get(addon.id).quantity + addon.quantity;
			} else {
				uniqueAddons.put(addon.id, addon);
			}
		}

		List<OrderEntryData.OrderEntryAddon> cleanAddons = new List<OrderEntryData.OrderEntryAddon>();
		for (OrderEntryData.OrderEntryAddon addon : uniqueAddons.values()) {
			if (addon.quantity > 0) {
				cleanAddons.add(addon);
			}
		}
		return cleanAddons;
	}

	/**
	 * Inner class that creates/updates/removes OpportunityLineItems for selected products and addons
	 */
	private with sharing class OrderEntryProductBuilder {
		private final String soho = 'SoHo';
		private final String recurring = 'Recurring';
		private final String oneOff = 'One Off';
		private final String contractLabel = ' maand contract';

		Opportunity opp { get; set; }
		OrderEntryData data { get; set; }
		OrderEntryData.OrderEntryBundle bundle { get; set; }
		List<OrderEntryData.OrderEntryAddon> addons { get; set; }
		List<OpportunityLineItem> oldOlis { get; set; }
		Map<String, OpportunityLineItem> olisMap { get; set; }
		List<OpportunityLineItem> newOlis { get; set; }
		Map<Id, OpportunityLineItem> updateOlis { get; set; }
		List<OpportunityLineItem> removeOlis { get; set; }
		List<LG_ProductDetail__c> newLgProductDetails { get; set; }
		List<LG_ProductDetail__c> removeLgProductDetails { get; set; }
		Integer contractTerm { get; set; }
		Map<String, PricebookEntry> pricebookEntriesMap { get; set; }

		public OrderEntryProductBuilder(OrderEntryData data) {
			this.data = data;
			this.bundle = data.bundle;
			this.addons = filterEmptyAddons(data.addons);
			this.contractTerm = Integer.valueOf(data.contractTerm);

			this.getOpportunity();
			this.setOldOlis();
			this.setOldOlisMap();
			this.setPricebookEntries();
		}

		/**
		 * Get the opportunity
		 */
		private void getOpportunity() {
			this.opp = [SELECT Id FROM Opportunity WHERE Id = :this.data.opportunityId];
		}

		/**
		 * Set list of already created OLIs on selected opportunity
		 */
		private void setOldOlis() {
			Id opportunityId = this.data.opportunityId;

			this.oldOlis = [
				SELECT Id, Quantity, UnitPrice, Product2.ProductCode, Product2Id
				FROM OpportunityLineItem
				WHERE OpportunityId = :opportunityId
			];
		}

		/**
		 * Remap list of old OLIs to ProductCode->OLI
		 */
		private void setOldOlisMap() {
			this.olisMap = new Map<String, OpportunityLineItem>();

			for (OpportunityLineItem oli : this.oldOlis) {
				this.olisMap.put(oli.Product2.ProductCode, oli);
			}
		}

		/**
		 * Set pricebook entries for selected bundles and addons by product codes
		 */
		private void setPricebookEntries() {
			this.pricebookEntriesMap = new Map<String, PricebookEntry>();
			List<String> productCodes = new List<String>();

			productCodes.add(this.bundle.code);

			for (OrderEntryData.OrderEntryAddon addon : this.addons) {
				productCodes.add(addon.code);
			}

			List<PricebookEntry> entries = [
				SELECT Id, Product2.ProductCode, Product2Id
				FROM PricebookEntry
				WHERE Product2.ProductCode IN :productCodes AND IsActive = TRUE
			];

			for (PricebookEntry entry : entries) {
				this.pricebookEntriesMap.put(entry.Product2.ProductCode, entry);
			}
		}

		/**
		 * Build OLIs for selected bundles
		 */
		private void buildBundleOlis() {
			if (this.olisMap.get(this.bundle.code) != null) {
				OpportunityLineItem oli = this.olisMap.get(this.bundle.code);
				oli.Quantity = this.bundle.quantity;
				this.updateOlis.put(oli.Id, oli);
			} else {
				OpportunityLineItem oli = new OpportunityLineItem();
				oli.OpportunityId = this.data.opportunityId;
				oli.Quantity = this.bundle.quantity;
				oli.UnitPrice = this.bundle.recurring;
				oli.LG_MainOLI__c = true;
				oli.LG_OneOffPrice__c = this.bundle.oneOff;
				oli.LG_ProductDetail__c = String.valueOf(this.contractTerm * 12) + contractLabel;
				oli.LG_Segment__c = soho;
				oli.LG_Type__c = (this.bundle.oneOff != null &&
					this.bundle.oneOff != 0)
					? oneOff
					: recurring;
				oli.Location__c = this.data.siteId;
				oli.LG_ContractTerm__c = this.contractTerm * 12;
				oli.PricebookEntryId = this.pricebookEntriesMap.get(this.bundle.code)?.Id;
				oli.Product_Bundle_Name__c = this.bundle.bundleName;
				this.newOlis.add(oli);
			}
		}

		/**
		 * Build OLIs for selected addons
		 */
		private void buildAddonOlis() {
			for (OrderEntryData.OrderEntryAddon addon : this.addons) {
				if (this.olisMap.get(addon.code) != null) {
					OpportunityLineItem oli = this.olisMap.get(addon.code);
					oli.Quantity = addon.quantity;
					this.updateOlis.put(oli.Id, oli);
				} else {
					OpportunityLineItem oli = new OpportunityLineItem();
					oli.OpportunityId = this.data.opportunityId;
					oli.Quantity = addon.quantity;
					oli.UnitPrice = addon.recurring;
					oli.LG_MainOLI__c = false;
					oli.LG_OneOffPrice__c = addon.oneOff;
					oli.LG_ProductDetail__c =
						String.valueOf(this.contractTerm * 12) + contractLabel;
					oli.LG_Segment__c = soho;
					oli.LG_Type__c = (addon.oneOff != null &&
						addon.oneOff != 0)
						? oneOff
						: recurring;
					oli.Location__c = this.data.siteId;
					oli.LG_ContractTerm__c = this.contractTerm * 12;
					if (this.pricebookEntriesMap.get(addon.code)?.Id == null) {
						throw new AuraException('Product ' + addon.code + ' is inactive.');
					} else {
						oli.PricebookEntryId = this.pricebookEntriesMap.get(addon.code)?.Id;
					}

					this.newOlis.add(oli);
				}
			}
		}

		/**
		 * Select OLIs that should be removed
		 */
		private void setForRemoval() {
			this.removeOlis = new List<OpportunityLineItem>();

			for (OpportunityLineItem oli : this.oldOlis) {
				Boolean remove = true;
				if (this.updateOlis.values().size() > 0) {
					for (OpportunityLineItem uoli : this.updateOlis.values()) {
						if (uoli.Product2.ProductCode == oli.Product2.ProductCode) {
							remove = false;
						}
					}
				}
				if (remove) {
					this.removeOlis.add(oli);
				}
			}
		}

		/**
		 * Build Product Detail records for selected products and addons
		 */
		private void buildProductDetails() {
			// Delete old records first
			this.removeLgProductDetails = [
				SELECT Id
				FROM LG_ProductDetail__c
				WHERE LG_Opportunity__c = :this.data.opportunityId
			];

			Set<Id> productAndAddonIds = new Set<Id>();

			for (OrderEntryData.OrderEntryProduct product : this.data.products) {
				productAndAddonIds.add(product.id);
			}
			for (OrderEntryData.OrderEntryAddon addon : this.addons) {
				productAndAddonIds.add(addon.id);
			}

			List<Order_Entry_Product_Detail__c> orderEntryProductDetails = [
				SELECT Name, Category__c, Type__c, Value__c
				FROM Order_Entry_Product_Detail__c
				WHERE Product__c IN :productAndAddonIds OR Add_On__c IN :productAndAddonIds
			];

			for (Order_Entry_Product_Detail__c oepd : orderEntryProductDetails) {
				LG_ProductDetail__c lgpd = new LG_ProductDetail__c(
					LG_Opportunity__c = this.data.opportunityId,
					LG_Name__c = oepd.Name,
					LG_Product__c = oepd.Category__c,
					LG_Type__c = oepd.Type__c,
					LG_Value__c = oepd.Value__c
				);
				this.newLgProductDetails.add(lgpd);
			}
		}

		/**
		 * Build and process everything
		 */
		public void build() {
			this.newOlis = new List<OpportunityLineItem>();
			this.updateOlis = new Map<Id, OpportunityLineItem>();
			this.newLgProductDetails = new List<LG_ProductDetail__c>();
			this.removeLgProductDetails = new List<LG_ProductDetail__c>();

			this.buildBundleOlis();
			this.buildAddonOlis();
			this.setForRemoval();
			this.buildProductDetails();

			if (this.updateOlis.values().size() > 0) {
				update this.updateOlis.values();
			}

			if (this.newOlis.size() > 0) {
				insert this.newOlis;
			}

			if (this.removeOlis.size() > 0) {
				delete this.removeOlis;
			}

			if (this.removeLgProductDetails.size() > 0) {
				delete this.removeLgProductDetails;
			}

			if (this.newLgProductDetails.size() > 0) {
				insert this.newLgProductDetails;
			}

			if (this.opp != null) {
				this.opp.LG_ContractTermMonths__c = this.contractTerm * 12;
				update this.opp;
			}
		}
	}

	@AuraEnabled
	public static List<OE_Promotion__c> getBundlePromotions(
		Id opportunityId,
		Boolean b2cChecked,
		List<OrderEntryData.OrderEntryProduct> products,
		List<OrderEntryData.OrderEntryAddon> addons
	) {
		OrderEntryData data = getOrderEntryData(opportunityId);
		String customerType = b2cChecked != null && b2cChecked ? 'Existing' : 'New';
		productIds = new Set<Id>();
		addonIds = new Set<Id>();
		getProductsAndAddons(products, addons);

		List<OE_Promotion__c> promotions = [
			SELECT
				Id,
				Promotion_Name__c,
				Contract_Term__c,
				Customer_Type__c,
				Connection_Type__c,
				Off_net_Type__c,
				(
					SELECT
						Discount__r.Name,
						Discount__r.Type__c,
						Discount__r.Value__c,
						Discount__r.Duration__c,
						Discount__r.Level__c,
						Discount__r.Product__c,
						Discount__r.Add_On__c
					FROM Discounts__r
				)
			FROM OE_Promotion__c
			WHERE
				Active__c = TRUE
				AND Type__c = 'Standard'
				AND Customer_Type__c INCLUDES (:customerType)
				AND Contract_Term__c INCLUDES (:data.contractTerm)
				AND Connection_Type__c = :data.addressCheckResult
				AND Off_net_Type__c = :data.offNetType
			ORDER BY Promotion_Name__c
		];
		return filterAvailablePromotions(promotions);
	}

	@AuraEnabled
	public static List<OE_Promotion__c> getSpecialPromotions(
		Id opportunityId,
		List<OrderEntryData.OrderEntryProduct> products,
		List<OrderEntryData.OrderEntryAddon> addons
	) {
		productIds = new Set<Id>();
		addonIds = new Set<Id>();
		getProductsAndAddons(products, addons);

		List<OE_Promotion__c> promotions = [
			SELECT
				Id,
				Promotion_Name__c,
				Contract_Term__c,
				Customer_Type__c,
				Connection_Type__c,
				Off_net_Type__c,
				(
					SELECT
						Discount__r.Name,
						Discount__r.Type__c,
						Discount__r.Value__c,
						Discount__r.Duration__c,
						Discount__r.Level__c,
						Discount__r.Product__c,
						Discount__r.Add_On__c
					FROM Discounts__r
				)
			FROM OE_Promotion__c
			WHERE Active__c = TRUE AND Type__c = 'Special'
			ORDER BY Promotion_Name__c
		];
		return filterAvailablePromotions(promotions);
	}

	private static List<OE_Promotion__c> filterAvailablePromotions(
		List<OE_Promotion__c> promotions
	) {
		List<OE_Promotion__c> availablePromotions = new List<OE_Promotion__c>();

		for (OE_Promotion__c promo : promotions) {
			Boolean isPromotionAvailable = true;

			for (OE_Promotion_Discount__c discount : promo.Discounts__r) {
				if (
					discount.Discount__r.Product__c != null &&
					!productIds.contains(discount.Discount__r.Product__c)
				) {
					isPromotionAvailable = false;
					break;
				} else if (
					discount.Discount__r.Add_On__c != null &&
					!addonIds.contains(discount.Discount__r.Add_On__c)
				) {
					isPromotionAvailable = false;
					break;
				}
			}
			if (isPromotionAvailable) {
				availablePromotions.add(promo);
			}
		}
		return availablePromotions;
	}

	private static void getProductsAndAddons(
		List<OrderEntryData.OrderEntryProduct> products,
		List<OrderEntryData.OrderEntryAddon> addons
	) {
		if (products != null) {
			for (OrderEntryData.OrderEntryProduct product : products) {
				productIds.add(product.id);
			}
		}

		if (addons != null) {
			List<OrderEntryData.OrderEntryAddon> cleanAddons = filterEmptyAddons(addons);

			for (OrderEntryData.OrderEntryAddon addon : cleanAddons) {
				addonIds.add(addon.id);
			}
		}
	}

	@AuraEnabled
	public static Integer getCountryIbanLength(String countryCode) {
		LG_IBAN__c ibanCountrySpecificConfiguration = LG_IBAN__c.getValues(countryCode);
		if (ibanCountrySpecificConfiguration != null) {
			return Integer.valueOf(ibanCountrySpecificConfiguration.LG_Length__c);
		}
		return null;
	}

	@AuraEnabled
	public static String getBankAccountHolder(Id contactId) {
		return [SELECT Name FROM Contact WHERE Id = :contactId].Name;
	}

	@AuraEnabled
	public static void createDiscountRelatedRecords(Id opportunityId) {
		OrderEntryData data = getOrderEntryData(opportunityId);
		promotionNames = new List<String>();

		if (data.promos != null) {
			for (OrderEntryData.Promotion promotion : data.promos) {
				promotionNames.add(promotion.name);
			}
		}

		Id ziggoOrderTypeId = [SELECT Id FROM OrderType__c WHERE Name = 'Ziggo'][0].Id;

		Map<String, Product2> productsMap = new Map<String, Product2>();
		Map<Id, PricebookEntry> productIdsPricebookEntries = new Map<Id, PricebookEntry>();

		removeUnavailablePromotionsOlis(opportunityId, ziggoOrderTypeId);
		findOrCreateProducts(data, ziggoOrderTypeId, productsMap);
		findOrCreatePricebookEntries(productsMap, productIdsPricebookEntries);
		createOpportunityProducts(data, productsMap, productIdsPricebookEntries);
		createBillingAccountUpdateOpp(opportunityId, data);
	}

	private static void removeUnavailablePromotionsOlis(Id opportunityId, Id ziggoOrderTypeId) {
		List<OpportunityLineItem> removeOldOlis = [
			SELECT Id
			FROM OpportunityLineItem
			WHERE
				OpportunityId = :opportunityId
				AND LG_Type__c IN ('Promotional Action', 'Closing Deal')
				AND Product2.Name NOT IN :promotionNames
				AND Product2.OrderType__c = :ziggoOrderTypeId
				AND Product2.Product_Line__c = 'fZiggo Only'
				AND PriceBookEntry.UnitPrice = 0
		];

		if (removeOldOlis.size() > 0) {
			delete removeOldOlis;
		}
	}

	private static void findOrCreateProducts(
		OrderEntryData data,
		Id ziggoOrderTypeId,
		Map<String, Product2> productsMap
	) {
		List<Product2> products = [
			SELECT Id, Name, (SELECT Id FROM PricebookEntries)
			FROM Product2
			WHERE Name IN :promotionNames
		];

		for (Product2 product : products) {
			if (!productsMap.containsKey(product.Name)) {
				productsMap.put(product.Name, product);
			}
		}

		List<Product2> newProducts = new List<Product2>();

		for (OrderEntryData.Promotion promotion : data.promos) {
			Product2 product;

			if (productsMap.containsKey(promotion.name)) {
				product = productsMap.get(promotion.name);
			} else {
				product = new Product2(
					Name = promotion.name,
					Family = 'Zakelijk Internet',
					OrderType__c = ziggoOrderTypeId,
					Product_Line__c = 'fZiggo Only'
				);
				newProducts.add(product);
			}
		}
		if (newProducts.size() > 0) {
			insert newProducts;
		}
	}

	private static void findOrCreatePricebookEntries(
		Map<String, Product2> productsMap,
		Map<Id, PricebookEntry> productIdsPricebookEntries
	) {
		// This SOQL is needed again so we can get newly inserted products
		List<Product2> products = [
			SELECT Id, Name, (SELECT Id FROM PricebookEntries)
			FROM Product2
			WHERE Name IN :promotionNames
		];

		for (Product2 product : products) {
			if (!productsMap.containsKey(product.Name)) {
				productsMap.put(product.Name, product);
			}
		}

		Id standardPricebookId = [SELECT Id FROM Pricebook2 WHERE Name = 'Standard Price Book'][0]
		.Id;
		List<PricebookEntry> newPricebookEntries = new List<PricebookEntry>();

		for (Product2 product : productsMap.values()) {
			PricebookEntry pricebookEntry;

			if (product.PricebookEntries.size() > 0) {
				pricebookEntry = product.PricebookEntries[0];
			} else {
				pricebookEntry = new PricebookEntry(
					Pricebook2Id = standardPricebookId,
					Product2Id = product.Id,
					IsActive = true,
					UnitPrice = 0
				);
				newPricebookEntries.add(pricebookEntry);
			}
			productIdsPricebookEntries.put(product.Id, pricebookEntry);
		}
		if (newPricebookEntries.size() > 0) {
			insert newPricebookEntries;
		}
	}

	private static void createOpportunityProducts(
		OrderEntryData data,
		Map<String, Product2> productsMap,
		Map<Id, PricebookEntry> productIdsPricebookEntries
	) {
		List<OpportunityLineItem> existingOlis = [
			SELECT Product_Bundle_Name__c
			FROM OpportunityLineItem
			WHERE OpportunityId = :data.opportunityId
		];

		Set<String> existingOliNames = new Set<String>();
		for (OpportunityLineItem oli : existingOlis) {
			existingOliNames.add(oli.Product_Bundle_Name__c);
		}

		List<OpportunityLineItem> newOlis = new List<OpportunityLineItem>();

		for (OrderEntryData.Promotion promotion : data.promos) {
			if (!existingOliNames.contains(promotion.name)) {
				Product2 product = productsMap.get(promotion.name);

				OpportunityLineItem oli = new OpportunityLineItem(
					PricebookEntryId = productIdsPricebookEntries.get(product.Id).Id,
					OpportunityId = data.opportunityId,
					LG_ContractTerm__c = Decimal.valueOf(data.contractTerm),
					LG_MainOLI__c = false,
					LG_OneOffPrice__c = 0,
					LG_Segment__c = 'SoHo',
					LG_Type__c = promotion.type == 'Standard'
						? 'Promotional Action'
						: 'Closing Deal',
					Product_Bundle_Name__c = promotion.name,
					Quantity = 1,
					UnitPrice = 0
				);
				newOlis.add(oli);
			}
		}
		if (newOlis.size() > 0) {
			insert newOlis;
		}
	}

	private static void createBillingAccountUpdateOpp(Id opportunityId, OrderEntryData data) {
		List<csconta__Billing_Account__c> targetBillingAccounts = [
			SELECT Id
			FROM csconta__Billing_Account__c
			WHERE
				csconta__Account__c = :data.accountId
				AND LG_BankAccountHolder__c = :data.payment.bankAccountHolder
				AND LG_PaymentType__c = :data.payment.paymentType
				AND LG_BankAccountNumberIBAN__c = :data.payment.iban
				AND csconta__Billing_Channel__c = :data.payment.billingChannel
		];

		Site__c site = [
			SELECT
				Site_Street__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Site_Postal_Code__c,
				Site_City__c
			FROM Site__c
			WHERE Id = :data.siteId
		];

		Contact mainContact = [SELECT Name, Email FROM Contact WHERE Id = :data.primaryContactId];
		String bankAccHolder = String.isNotBlank(data.payment.bankAccountHolder)
			? data.payment.bankAccountHolder
			: mainContact.Name;

		if (targetBillingAccounts.isEmpty()) {
			csconta__Billing_Account__c billingAccount = new csconta__Billing_Account__c(
				csconta__Account__c = data.accountId,
				LG_PaymentType__c = data.payment.paymentType,
				LG_BankAccountNumberIBAN__c = data.payment.iban,
				csconta__Billing_Channel__c = data.payment.billingChannel,
				csconta__City__c = site.Site_City__c,
				csconta__Postcode__c = site.Site_Postal_Code__c,
				csconta__Street__c = site.Site_Street__c,
				LG_HouseNumber__c = String.valueOf(site.Site_House_Number__c),
				LG_HouseNumberExtension__c = site.Site_House_Number_Suffix__c,
				LG_BillingEmailAddress__c = mainContact.Email,
				LG_BankAccountHolder__c = bankAccHolder
			);
			insert billingAccount;
		}

		updateOpportunityFields(site, bankAccHolder, data);
	}

	private static void updateOpportunityFields(
		Site__c site,
		String bankAccHolder,
		OrderEntryData data
	) {
		Opportunity opp = [SELECT Id FROM Opportunity WHERE Id = :data.opportunityId];
		opp.LG_BillingCity__c = site.Site_City__c;
		opp.LG_BillingHouseNumber__c = String.valueOf(site.Site_House_Number__c);
		opp.LG_BillingHouseNumberExtension__c = site.Site_House_Number_Suffix__c;
		opp.LG_BillingPostalCode__c = site.Site_Postal_Code__c;
		opp.LG_BillingStreet__c = site.Site_Street__c;
		opp.LG_BankNumber__c = data.payment.iban;
		opp.LG_BankAccountName__c = bankAccHolder;
		opp.Internet_Customer__c = data.b2cInternetCustomer;

		if (opp != null) {
			update opp;
		}
	}

	@AuraEnabled
	public static mmdoc__Document_Request__c generateQuote(Id opportunityId) {
		mmdoc__Document_Template__c docTemplate = MavenDocumentsService.getDocumentTemplate(
			'Ziggo Quote'
		);
		return createDocumentRequest(opportunityId, docTemplate);
	}

	@AuraEnabled
	public static mmdoc__Document_Request__c generateConfirmation(Id opportunityId) {
		mmdoc__Document_Template__c docTemplate = MavenDocumentsService.getDocumentTemplate(
			'Ziggo Order Confirmation'
		);
		return createDocumentRequest(opportunityId, docTemplate);
	}

	private static mmdoc__Document_Request__c createDocumentRequest(
		Id recordId,
		mmdoc__Document_Template__c docTemplate
	) {
		return MavenDocumentsService.createDocRequest(
			recordId,
			docTemplate,
			'Attachment',
			'PDF',
			'Queue',
			true
		);
	}

	@AuraEnabled
	public static mmdoc__Document_Request__c getDocumentRequest(Id docRequestId) {
		return MavenDocumentsService.getDocumentRequest(docRequestId);
	}

	@AuraEnabled
	public static Opportunity saveOpportunityDescription(Id opportunityId, String notes) {
		Opportunity opp = [SELECT Id, Description FROM Opportunity WHERE Id = :opportunityId];
		opp.Description = notes;
		update opp;
		return opp;
	}

	@AuraEnabled
	public static Boolean updateQuoteAttachment(Id fileId, String attachmentBodyBlob) {
		if (String.valueOf(fileId.getSobjectType()) == 'Attachment') {
			Attachment attachment = [SELECT Id, ParentId, Body FROM Attachment WHERE Id = :fileId];
			attachment.Body = EncodingUtil.base64Decode(attachmentBodyBlob);
			update attachment;
		} else {
			ContentVersion contentVersion = [
				SELECT Id, VersionData, ContentDocumentId, Title, PathOnClient
				FROM ContentVersion
				WHERE Id = :fileId
			];
			insert new ContentVersion(
				ContentDocumentId = contentVersion.ContentDocumentId,
				Title = contentVersion.Title,
				VersionData = EncodingUtil.Base64Decode(attachmentBodyBlob),
				PathOnClient = contentVersion.PathOnClient,
				ContentLocation = 'S'
			);
		}

		return true;
	}

	@AuraEnabled
	public static void updateOpportunitySingedQuote(Id oppId, Id attachmentId, Boolean singed) {
		Opportunity opp = [
			SELECT Id, LG_SignedQuoteAvailable__c, LG_SignedQuoteAttachmentId__c
			FROM Opportunity
			WHERE Id = :oppId
		];

		if (opp != null) {
			opp.LG_SignedQuoteAttachmentId__c = (attachmentId != null)
				? attachmentId
				: opp.LG_SignedQuoteAttachmentId__c;
			opp.LG_SignedQuoteAvailable__c = String.valueOf(singed);

			update opp;
		}
	}

	@AuraEnabled
	public static Opportunity getOpportunity(Id oppId, List<String> fields) {
		String query = 'SELECT ';
		query += String.join(fields, ',');
		query += ' FROM Opportunity WHERE Id =: oppId';

		List<Opportunity> opportunityList = Database.query(query);

		if (!opportunityList.isEmpty()) {
			return opportunityList[0];
		}
		return null;
	}

	@AuraEnabled
	public static void updateOpportunityAutomatedQuoteDelivery(Id oppId, String value) {
		try {
			Opportunity opp = [
				SELECT Id, LG_AutomatedQuoteDelivery__c
				FROM Opportunity
				WHERE Id = :oppId
			];
			opp.LG_AutomatedQuoteDelivery__c = value;
			update opp;
		} catch (Exception ex) {
			throw new AuraException(ex.getMessage());
		}
	}

	@AuraEnabled
	public static void updateOpportunityStage(Id oppId, String value) {
		try {
			// Update with the flag
			Opportunity opp = [SELECT Id, StageName FROM Opportunity WHERE Id = :oppId];
			opp.StageName = value;
			opp.Allow_Stage_Change__c = true;
			update opp;

			// Clear the flag
			opp.Allow_Stage_Change__c = false;
			update opp;
		} catch (Exception ex) {
			throw new AuraException(ex.getMessage());
		}
	}

	@AuraEnabled
	public static Opportunity cloneOrder(Id oppId, String oppName) {
		OrderEntryData data = new OrderEntryData();

		Opportunity opp = [
			SELECT Id, Name, AccountId, RecordTypeId
			FROM Opportunity
			WHERE Id = :oppId
		];

		Opportunity newOpp = new Opportunity();
		newOpp.AccountId = opp.AccountId;
		newOpp.Name = oppName;
		newOpp.RecordTypeId = opp.RecordTypeId;
		newOpp.CloseDate = Date.today();
		newOpp.StageName = 'Awareness of interest';
		insert newOpp;

		List<Attachment> attList = [
			SELECT Id, Body
			FROM Attachment
			WHERE ParentId = :opp.Id AND Name = 'OrderEntryData.json'
		];

		if (!attList.isEmpty()) {
			data = (OrderEntryData) JSON.deserialize(
				attList[0].Body.toString(),
				OrderEntryData.class
			);
			data.opportunityId = newOpp.Id;
			data.lastStep = 'Select Product';
			data.init();

			Attachment att = new Attachment(
				Name = 'OrderEntryData.json',
				Body = Blob.valueOf(JSON.serializePretty(data)),
				ParentId = newOpp.Id
			);
			insert att;
			return newOpp;
		}
		return null;
	}
}