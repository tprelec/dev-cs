public with sharing class OrderEntryRedirectController {
	@AuraEnabled
	public static Boolean isCommunity() {
		return Site.getName() != null;
	}
}