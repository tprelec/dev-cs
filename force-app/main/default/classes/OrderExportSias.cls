public with sharing class OrderExportSias {

    public static void sendOrderNotifications(Set<Id> orderIds){
        for(Id orderId : orderIds){
            sendOrderNotification(orderId);
        }
    }

    @future(callout=true)
    public static void sendOrderNotificationsOffline(Set<Id> orderIds){
        sendOrderNotifications(orderIds);
    }

    public static void sendOrderNotification(Id orderId){
        // run the BOP order export, but replace the url with a different one

        //List<Order__c> createOrders = new List<Order__c>();
        Order__c createOrder;
        List<Order__c> ordersToUpdate = new List<Order__c>();
        
        for(Order__c o : OrderUtils.getOrderDataByContractId(null,new Set<Id>{orderId})){
            // will only return 1..
            createOrder = o;
        }

        Map<Id,OrderWrapper> orderIdToOrderWrapper = OrderExport.buildOrderIdToOrderWrapper(new Set<Id>{orderId});
        //OrderExport.buildRelatedData(new Set<Id>{orderId});
        
        if(createOrder != null){        
            ECSOrderService createService = new ECSOrderService();

            // override the config with the SIAS config instead of BOP
            if(!Test.isRunningTest()){
                createService.setWebServiceConfig( WebServiceConfigLocator.getConfig('AmdocsSiasOrder') );
            }
            else createService.setWebServiceConfig( WebServiceConfigLocator.createConfig());

            List<ECSOrderService.request> requests = new List<ECSOrderService.request>();
            Map<String,Id> orderNameToId = new Map<String,Id>();
     
            //for(Order__c o : createOrders){

                ECSOrderService.request req = OrderExport.orderToRequest(createOrder,orderIdToOrderWrapper);
                requests.add(req);
                orderNameToId.put(createOrder.BOP_Export_Order_Id__c,createOrder.Id);
            //}

            createService.setRequest(requests);
            

            Boolean success = false;
            try{
                if(createOrder.propositions__c.contains('Legacy')){
                    createService.makeRequest('legacy');
                } else {
                    createService.makeRequest('create');
                }
                success = true;
            } catch (ExWebServiceCalloutException ce){
                system.debug(ce);
                system.debug(ce.getMessage());
                
                // TODO: process errors
                /*
                    // cleanup the errormsg and put it in the order
                    o.BOP_export_Errormessage__c = ce.get255charMessage();
                    o.Status__c = 'Refused';
                    o.Record_Locked__c = false;
                    OrdersToUpdate.add(o);  
                */
            }
        
            if(success){
                for (ECSOrderService.response response : createService.getResponse()){
                    system.debug(response);
                    if(response.referenceId != null){
                        Order__c o = new Order__c(Id = orderNameToId.get(response.referenceId));
                    // TODO: process success
                    /*
                        // indicator for success is the orderId
                        if(response.orderId != null && response.orderId != '0'){
                            // success
                            // add info to order
                            o.BOP_Order_Id__c = response.orderId;                           
                            o.Sales_Order_Id__c = response.salesOrderId;
                            o.BOP_export_datetime__c = system.now();
                            o.BOP_export_Errormessage__c = null;
                            o.Status__c = 'Accepted';
                        } else {
                            // failure
                            // add errormessage to order and unlock order
                            o.BOP_export_Errormessage__c = (response.errorCode+' - '+response.errorMessage).left(255);
                            o.Status__c = 'Refused';
                            o.Record_Locked__c = false;
                        }
                        OrdersToUpdate.add(o);
                    */  
                    } else {
                        throw new ExMissingDataException('No Order Reference Id returned by BOP');
                    }
                }
            }
        }

/*      try{
            update ordersToUpdate;
        } catch (DMLException e){
            ExceptionHandler.handleException(e);
        }
*/
    }

}