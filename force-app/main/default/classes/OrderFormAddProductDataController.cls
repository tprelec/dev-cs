// Written by Gerhard Newman
// See Case 10844 for design
public with sharing class OrderFormAddProductDataController {

    public String contractId{get; private set;}
    public String orderId{get; private set;}
	private Boolean errorFound{get;set;}
    public OrderWrapper theOrderWrapper {get;set;}
	public Order__c theOrder {get;set;}    
	public List<selectOption> products {get;set;}
    public Contracted_Products__c cp {get;set;}
    public static final Set<String> ziggoFamilies = new Set<String>{'Ziggo XSell','Ziggo Only'};
    public static final Set<String> ziggoProductLines = new Set<String>{'fZiggo Only','fZiggo Xsell'};
    public static final Set<String> ziggoOnlyFamilies = new Set<String>{'Ziggo Only'};     
	public List<selectOption> sites {get;set;}
	public List<ID> selectedSites {get;set;}
	public ID product {get;set;}
	public ID accountId {get;set;}
   
	public OrderFormAddProductDataController() {

    	contractId = ApexPages.currentPage().getParameters().get('contractId');
    	orderId = ApexPages.currentPage().getParameters().get('orderId');
    	errorFound = false;
	    	
    	if(contractId == null){
    		ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'No ContractId provided.'));
    		errorFound = true;
    	}
    	
    	// only continue if no errors were found earlier on
    	if(!errorFound){
            theOrderWrapper = OrderUtils.retrieveOrderDetails(orderId,null);	    	
    	}

    	cp = new Contracted_Products__c();
    	cp.VF_Contract__c=contractId;
    	cp.Order__c=orderID;

    	// Get contract data
    	VF_Contract__c contract = [select Deal_Type__c, 
    								Account__c,
    								Opportunity__r.owner.Ziggo__c, 
    								Opportunity__r.Vodafone_Products__c,
    								Opportunity__r.Ziggo_Only_Products__c,
    								Opportunity__r.Ziggo_XSell_Products__c
    								from VF_Contract__c where id=:contractId];

    	accountId = contract.Account__c;

    	// Default CLC value
    	if (contract.Deal_Type__c=='Acquisition') {
    		cp.CLC__c='ACQ';
    	} else if (contract.Deal_Type__c=='Retention') {
			cp.CLC__c='RET';
		}

		// Get available products
		String qString = 'select Id, Name, Brand__c, ';
		qString += 'ProductCode__c, BAP_SAP__c, Quantity_type__c, Description__c ';
		qString += 'from VF_Product__c where Active__c=true and Allowed_as_new_CP__c=true';

		// Business Partner Filter
		if (Special_Authorizations__c.getInstance().Add_Products_to_Orders__c=='BP Only') {	
			qString += ' AND Allowed_as_Business_Partner__c=true ';
		}
		
		// JV filter
		//if(contract.Opportunity__r.Owner.Ziggo__c){
			// Ziggo user
			// filter out Vodafone-only products
			//qString += ' AND Family__c IN :ziggoFamilies ';
		//} else {
			// Vodafone user
			if(contract.Opportunity__r.Vodafone_Products__c > 0){
				// filter out any additional Z products
				//qString += ' AND Family__c NOT IN :ziggoFamilies ';
				qString += ' AND Product_Line__c NOT IN :ziggoProductLines ';				
			} else if(contract.Opportunity__r.Ziggo_Only_Products__c > 0 || contract.Opportunity__r.Ziggo_XSell_Products__c > 0){
				// filter out any additional VF products
				//qString += ' AND Family__c IN :ziggoFamilies AND Family__c NOT IN :ziggoOnlyFamilies ';			
				qString += ' AND Product_Line__c IN :ziggoProductLines ';								
			} else {
				// filter out just the Ziggo-only products
				// GC commented out on 01-07-2017 because all users are now equal
				//qString += ' AND Family__c NOT IN :ziggoOnlyFamilies ';				
			}
		//}
		
		// Restrict to numberporting and zero priced products
		qString += ' AND VF_Product__c.Id IN (SELECT Product2.VF_Product__c FROM Product2 where Role__c=\'Numberporting\' or BAP_SAP__c=0)';

		// Sort alpha
 		qString += ' order by name';

		products = new List<selectOption>(); 
		for(VF_Product__c pbe : database.query(qString)){				
			products.add(new selectOption(pbe.id, pbe.Name));
		}

		// Site list should be restricted to sites that are already on the order
		List<Contracted_Products__c> contractedProductSites = [select site__c from Contracted_Products__c where order__c=:orderID];
		Set<ID> siteSet = new Set<ID>();
		for (Contracted_Products__c cps:contractedProductSites) {
			siteSet.add(cps.site__c);
		}

		// load site dropdown
		sites = new List<selectOption>(); 		
		for(Site__c s : [SELECT Id, Name, Site_City__c, Site_Street__c,Site_House_Number__c, Site_House_Number_Suffix__c
							FROM Site__c 
							WHERE 
								(Site_Account__c != null AND Site_Account__c = :contract.Account__c AND id in:siteSet)
							ORDER BY Site_City__c, Site_Street__c, Site_House_Number__c,Site_House_Number_Suffix__c]){
			sites.add(new selectOption(s.id,s.Site_City__c + ', ' + s.Site_Street__c + ', ' + s.Site_House_Number__c + (s.Site_House_Number_Suffix__c==null?'':' '+s.Site_House_Number_Suffix__c) + ' (' + s.Name + ')'));
		}		
	}

	public PageReference create(){
		List<Contracted_Products__c> cpToInsert = new List<Contracted_Products__c>();

		// Validate that at least 1 site has been selected
		if (selectedSites.size()==0) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR,System.Label.ERROR_Select_Site));
            return null;                			
		}

		// Validate quantity (not sure why this is not on object validation rule)
		if (cp.Quantity__c==null) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR,System.Label.ERROR_Quantity));
            return null;			
		}

		// Validate duration (not sure why this is not on object validation rule)
		if (cp.Duration__c==null) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR,System.Label.ERROR_Duration));
            return null;			
		}

		// Get the product
		PriceBookEntry pbe = [Select Id, Product2.Family, Product2.Multiplier_Vodacom__c, Product2.Role__c, Product2Id, Product2.ProductCode, Product2.BAP_SAP__c, Product2.CLC__c, Product2.VF_Product__c, Product2.Quantity_Type__c From PriceBookEntry 
								Where Product2.VF_Product__c = :product 
								and Product2.CLC__c = :cp.CLC__c];

		// Get any other contracted product on the order
		Contracted_Products__c otherCP = [select proposition__c, family_condition__c from Contracted_Products__c where Order__c =:cp.Order__c limit 1];

		// Populate the contracted product for each site
		for (ID site:selectedSites){
			Contracted_Products__c newCP = new Contracted_Products__c();
    		newCP.VF_Contract__c=cp.VF_Contract__c;
    		newCP.Order__c=cp.Order__c;
    		newCP.CLC__c=cp.CLC__c;
			newCP.Product__c=pbe.Product2Id;
			newCP.List_price__c=pbe.Product2.BAP_SAP__c;
			newCP.Net_Unit_Price__c=pbe.Product2.BAP_SAP__c;
			newCP.ARPU_Value__c=pbe.Product2.BAP_SAP__c;
			newCP.Discount__c=0;
			newCP.ProductCode__c=pbe.Product2.ProductCode;
			newCP.Site__c=site;	
			newCP.Quantity__c=cp.Quantity__c;
			newCP.Duration__c=cp.Duration__c;		
			newCP.Fixed_Mobile__c='Fixed';
			newCP.Proposition_Type__c='FIXED';
			newCP.proposition__c=otherCP.proposition__c;
			newCP.family_condition__c=otherCP.family_condition__c;
			newCP.Multiplier_Vodacom__c=pbe.Product2.Multiplier_Vodacom__c;
			newCP.UnitPrice__c = pbe.Product2.BAP_SAP__c * cp.Duration__c;
			newCP.Gross_List_Price__c = pbe.Product2.BAP_SAP__c;
			newCP.Subtotal__c = pbe.Product2.Multiplier_Vodacom__c * pbe.Product2.BAP_SAP__c * cp.Duration__c * cp.Quantity__c;
			newCP.Product_Family__c=pbe.Product2.Family;		
			cpToInsert.add(newCP);		
		}

		// Insert the new contracted product
		try {
			insert cpToInsert;
    	} catch (DmlException ex) {
	        ApexPages.addMessages(ex);
	        return null;  	        
	    }

	    // Check if the new product is number porting
	    // If it is generate a number porting row
	    if (pbe.product2.Role__c=='Numberporting') {
			// Pass in the new porting product(s)
			List<Contracted_Products__c> numberportingCPs = [Select 
			                                            Id,
			                                            Deal_Type__c,
			                                            Duration__c,
			                                            Name,
			                                            Fixed_Mobile__c,
			                                            Interface_type__c,
			                                            Product__r.Name,
			                                            Product__r.Product_Group__c,
			                                            Product__r.ProductCode,
			                                            Product__r.Role__c,
			                                            Product__r.SLA__c,
			                                            Product__r.Family_Tag__r.Name,
			                                            Product_Family__c,
			                                            Proposition__c,
			                                            Proposition_Type__c,
			                                            Quantity__c,
			                                            Order__c,
			                                            Proposition_Component__c,
			                                            Site__c,
			                                            Site__r.PBX__c,
			                                            Unify_Product_Family__c,
			                                            Unify_Parent_Component__c,
			                                            VF_Contract__c,
			                                            VF_Contract__r.Opportunity__r.RecordTypeId
			                                        From
			                                            Contracted_Products__c
			                                        Where id in:cpToInsert];

			// Generate the numberporting row                
			List<Numberporting_Row__c> numberportingsToInsert = OrderFormController.createNumberportings(numberportingCPs,AccountId);

			// This will link to an existing number port, or create a new one if one does not exist
			for(Numberporting_Row__c npr : numberportingsToInsert){
			    if (npr.Numberporting__c == null) {
	                try {
	                    Numberporting__c np = OrderUtils.createPorting(theOrder);
	                    PhonebookRegistrationController prc = new PhonebookRegistrationController();
	                    prc.createNewPhonebookRegistration(np.Id,AccountId,0);
			        	npr.Numberporting__c = np.Id;
                        theOrder.PhonebookregistrationReady__c = false;   			        	
	                } catch (Exception e){
	                    ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR,System.Label.ERROR_OrderForm_Cannot_Create_PhonebReg+' '+e.getMessage()));
	                    return null;                
	                }                                
			    }   
			}
			try {
				insert numberportingsToInsert;
	    	} catch (DmlException ex) {
		        ApexPages.addMessages(ex);
		        return null;
		    }
            theOrder.NumberportingReady__c = false;

	    } // End number porting

	    // Update the proposition (this is only looking at fixed because only fixed orders can add products)
        Set<String> fixedPropositions = new Set<String>();
        if(theOrder.Propositions__c=='Legacy'){
            fixedPropositions.add('Legacy');        	
        } else {
			List<Contracted_Products__c> allOrderCPs = [Select Product__r.Role__c,
			                                            Proposition__c,
			                                            Fixed_Mobile__c
			                                        From
			                                            Contracted_Products__c
			                                        Where Order__c=:cp.Order__c];
			for (Contracted_Products__c aCP:allOrderCPs) {
		        if(aCP.Product__r.Role__c == 'ERS') {
		            fixedPropositions.add('ERS');
		        } else if(aCP.Product__r.Role__c == 'Numberporting') {
		            fixedPropositions.add('Numberporting');
		        } else if(aCP.Proposition__c != null) {
		            if(aCP.Fixed_Mobile__c == 'Fixed') {
		                fixedPropositions.addAll(aCP.Proposition__c.split('\\+'));
		            }		
				}
			}
		}
        String propString = '';
        for(String s : fixedPropositions) propString += s+';';
        theOrder.Propositions__c = propString;  
       	update theOrder;

        // Create a new product ready for entry and refresh page with success message
    	cp = new Contracted_Products__c(VF_Contract__c=cp.VF_Contract__c,Order__c=cp.Order__c,CLC__c=cp.CLC__c);
		ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record Created Successfully.'));    	
		return null;

	}	

}