/**
 * @description         This is the controller class for the Billing Data Order Form. It collects all required data and shows it to the user
 * @author              Guy Clairbois
 */
public without sharing class OrderFormBillingDataController {

    public Order__c order {get;set;}
    
    public Ban__c ban {get;set;}
    public Financial_Account__c fa {get;set;}
    public Billing_Arrangement__c bar {get;set;}
    public String contractId{get; private set;}
    public String orderId{get; private set;}
    public Boolean locked {get;set;}

    public Boolean errorFound {get;set;}
    //public OrderFormCache__c currentCache {get;set;}
    
    /**
     *      Controllers
     */

    public OrderFormBillingDataController getApexController() {
        // used for passing the controller between form components
        return this;
    }
    
    public OrderFormBillingDataController() {
        contractId = ApexPages.currentPage().getParameters().get('contractId');
        orderId = ApexPages.currentPage().getParameters().get('orderId');
        
        if(contractId == null){
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'No ContractId provided.'));
            errorFound = true;
        }
        
        //currentCache = OrderFormCache__c.getInstance(orderId);
        
        // fetch the contract details if the order is based on a contract
        if(contractId != null){
            errorFound = retrieveBillingData();
        }

       
        if (ban == null) ban = new Ban__c();
        if (fa == null) fa = new Financial_Account__c();
        if (bar == null) bar = new Billing_Arrangement__c();

        locked = OrderUtils.getLocked(order);
        
    }

    public Account theAccount{
        get{
            if(theAccount == null){
                theAccount = [Select Id, 
                            Billing_City__c, 
                            Billing_Housenumber__c, 
                            Billing_Housenumber_Suffix__c, 
                            Billing_Postal_Code__c, 
                            Billing_Street__c, 
                            Visiting_street__c,
                            Visiting_Housenumber1__c,
                            Visiting_Housenumber_Suffix__c ,
                            Visiting_Postal_Code__c,     
                            Visiting_City__c,
                            Visiting_Country__c,                            
                            Name 
                        From 
                            Account 
                        Where Id = :order.Account__c];   
            }
            return theAccount;
        }
        set;
    }

    /**
     * @description         Retrieve the order's billing data
     */
    private Boolean retrieveBillingData(){
        Boolean error = false;
        order = OrderUtils.getOrderDataByContractId(null,new Set<Id>{Id.valueOf(orderId)})[0];

        banIdSelected = order.VF_Contract__r.Opportunity__r.Ban__r.Name;
        changeBAN();

        // if a bar is already connected to the order, load the bar and associated fa
        if(order.Billing_Arrangement__c != null){
            faIdSelected = Order.Billing_Arrangement__r.Financial_Account__c;
            changeFA();

            barIdSelected = Order.Billing_Arrangement__c;
            changeBAR();
        } else {
            faIdSelected = 'empty';
            barIdSelected = 'empty';
        }
        
        return error;
    }
    
    public pageReference changeBAN(){
        system.debug(banIdSelected);
        if(banIdSelected != null && banIdSelected != 'Request New Unify BAN' && banIdSelected != 'empty'){

            // Gerhard: Variant code to use OrderValidationField__c as I don't want to add duplicate fields into the object
            // but also don't want to run the full billing_arrangement__c query here
            String query = 'Select Id';
            // Get the fields from OrderValidationField__c
            for(OrderValidationField__c ovf : OrderValidationField__c.getAll().values()){
                if (ovf.object__c=='Billing_Arrangement__c') {
                    // We want all fields in the config that hang off Ban__r.
                    String FAfield = ovf.Field__c.substringAfter('Ban__r.');
                    if (FAfield!='') {
                        query+=','+FAfield;
                    }
                }
            }
            query+=' From Ban__c Where Name = :banIdSelected';
            ban = Database.query(query);

        } else if(banIdSelected == 'Request New Unify BAN') {
            ban = new Ban__c();
        }
        if(ban != null){
            // default ban name if empty
            if(ban.BAN_Name__c == null) ban.BAN_Name__c = theAccount.Name;
            // default ban values for partners
            if(order.Direct_Indirect__c == 'Indirect'){
                if(ban.Unify_Customer_Type__c == null) ban.Unify_Customer_Type__c = 'B'; // Business
                if(ban.Unify_Customer_SubType__c == null) ban.Unify_Customer_SubType__c = 'BU'; // Zakelijk
            }
        }
        faIdSelected = 'empty';
        barIdSelected = 'empty';
        return null;
    }

    public pageReference changeFA(){
        system.debug(faIdSelected);
        if(faIdSelected != 'new' && faIdSelected != 'empty'){

            // Gerhard: Variant code to use OrderValidationField__c as I don't want to add duplicate fields into the object
            // but also don't want to run the full billing_arrangement__c query here
            String query = 'Select Id';
            // Get the fields from OrderValidationField__c
            for(OrderValidationField__c ovf : OrderValidationField__c.getAll().values()){
                if (ovf.object__c=='Billing_Arrangement__c') {
                    // We want all fields in the config that hang off financial_account__r
                    String FAfield = ovf.Field__c.substringAfter('Financial_Account__r.');
                    if (FAfield!='') {
                        query+=','+FAfield;
                    }
                }
            }
            query+=' From Financial_Account__c Where Id = :faIdSelected';
            fa = Database.query(query);
                    
        } else {
            fa = new Financial_Account__c();   
        }

        // reset bar selection
        barIdSelected = 'empty';
        bar = new Billing_Arrangement__c();
        return null;
    }

    public pageReference changeBAR(){
        system.debug(barIdSelected);
        if(barIdSelected != 'new' && barIdSelected != 'empty'){

            // Gerhard: Variant code to use OrderValidationField__c as I don't want to add duplicate fields into the object
            // but also don't want to run the full billing_arrangement__c query here
            String query = 'Select Id';
            // Get the fields from OrderValidationField__c
            for(OrderValidationField__c ovf : OrderValidationField__c.getAll().values()){
                if (ovf.object__c=='Billing_Arrangement__c') {
                    // We want all fields in the config that hang off Billing_Arrangement__r
                    String FAfield = ovf.Field__c.substringAfter('Billing_Arrangement__r.');
                    if (FAfield!='') {
                        query+=','+FAfield;
                    }
                }
            }
            query+=' From Billing_Arrangement__c Where Id = :barIdSelected';
            bar = Database.query(query);

        } else {
            bar = new Billing_Arrangement__c();
            // set default values
            if(theAccount.Billing_Postal_Code__c != null){
                bar.Billing_Street__c = theAccount.Billing_Street__c;
                bar.Billing_Housenumber__c = String.valueOf(theAccount.Billing_Housenumber__c);
                bar.Billing_Housenumber_Suffix__c = theAccount.Billing_Housenumber_Suffix__c;
                bar.Billing_Postal_Code__c = theAccount.Billing_Postal_Code__c;
                bar.Billing_City__c = theAccount.Billing_City__c;
                bar.Billing_Country__c = 'NL'; // default to NL as country is not captured by D&B  
            } else {
                // if billing address is not filled on account, default to visiting address
                bar.Billing_Street__c = theAccount.Visiting_Street__c;
                bar.Billing_Housenumber__c = String.valueOf(theAccount.Visiting_Housenumber1__c);
                bar.Billing_Housenumber_Suffix__c = theAccount.Visiting_Housenumber_Suffix__c;
                bar.Billing_Postal_Code__c = theAccount.Visiting_Postal_Code__c;
                bar.Billing_City__c = theAccount.Visiting_City__c;
                bar.Billing_Country__c = theAccount.Visiting_Country__c;
            }

            bar.Billing_Arrangement_Alias__c = StringUtils.truncate(theAccount.Name,50);
            bar.Bill_Format__c = 'EB';
            bar.Bill_Production_Indicator__c = 'Y';

            bar.Bank_Account_Name__c = theAccount.Name;
            bar.Payment_method__c = 'Invoice';            
        }

        return null;
    }

    public pageReference refresh(){
        retrieveBillingData();
        
        // if the billing details are complete, update the order 
        if(OrderValidation.checkCompleteBillingFlag(ban,fa,bar,order))
        {
            system.debug(order);         
            if(order.billingReady__c == false){
                order.billingReady__c = true;
                SharingUtils.updateRecordsWithoutSharing(order);
            }
        } else {
            if(order.billingReady__c == true){
                order.billingReady__c = false;
                SharingUtils.updateRecordsWithoutSharing(order);
            }
        }

        // recheck if this order is now clean 
        OrderUtils.updateOrderStatus(order,true);        
        
        return null;    
    }

    public pageReference saveBilling(){

        Savepoint sp = database.setSavepoint();
        try{

            // update/insert ban, financial account and BAR
            // if new ban, insert it
            if(banIdSelected == 'Request New Unify BAN'){
                //ban = new Ban__c();
                ban.Account__c = order.Account__c;
                ban.Name = 'Requested New Unify BAN '+system.now();
                ban.BAN_Status__c = 'Requested';
            }
            if(banIdSelected != 'empty'){
                upsert ban;
                banIdSelected = ban.Name;
            }

            // if new ban or ban was changed, update it on the opportunity
            if(banIdSelected != order.VF_Contract__r.Opportunity__r.Ban__r.Name){
                Opportunity o;
                if(banIdSelected != 'empty'){
                    //Ban__c lookupBan = new Ban__c(BAN_Number__c=banIdSelected);
                    o = new Opportunity(Id=order.VF_Contract__r.Opportunity__c,Ban__c=ban.Id);//Ban__r=lookupBan);
                } else {
                    o = new Opportunity(Id=order.VF_Contract__r.Opportunity__c,Ban__c=null);
                }
                update o;
            }

            // if fa is selected but bar is not, generate an error (fa is linked to order via the bar)
            if(faIdSelected != 'empty' && barIdSelected == 'empty'){
                ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'You need to select both an FA and a BAR in order to save the FA data.'));
                return null;
            } else {

                //upsert the fa
                if(banIdSelected != null && faIdSelected != 'empty'){
                    // only set ban on insert
                    if(fa.Id == null) fa.ban__c = ban.Id;

                    //fa.Financial_Contact__c = faContactIdSelected;
                    upsert fa;
                }

                //upsert the bar
                if(fa != null && fa.Id != null && barIdSelected != 'empty'){
                    // only set fa on insert
                    if(bar.Id == null) bar.Financial_Account__c = fa.Id;
                    upsert bar;

                    // update the order
                    if(bar.Id != order.Billing_Arrangement__c){
                        order.Billing_Arrangement__c = bar.Id;
                        update order;
                    }
                }
            }
        } catch (dmlException e){
            ApexPages.addMessages(e);
            Database.rollback(sp);
            return null;
        }
        // refetch the data and update order status
        refresh();

        return null;
    }

    public String banIdSelected {get;set;}
    
    public transient List<SelectOption> banSelectOptions {
        get{
            Boolean fixed = false;
            Boolean onenet = false;
            for(OpportunityLineItem oli : [Select Id, Fixed_Mobile__c, Proposition__c, Opportunity.Hidden_Fixed_Products__c From OpportunityLineItem Where OpportunityId = :order.VF_Contract__r.Opportunity__c]){
                if(oli.Proposition__c != null && oli.Proposition__c.contains('One Net')){
                    onenet = true;
                } else if (oli.Fixed_Mobile__c == 'Fixed' || oli.opportunity.Hidden_Fixed_Products__c > 0){
                // any order under an opportunity that has fixed products should follow the 'fixed' rules. Even if the order is mobile.
                    fixed = true;
                }
            } 
            //List<Ban__c> bans = [Select Id, Name, BAN_Status__c From Ban__c Where BAN_Status__c!='Closed' AND Account__c = :order.Account__c limit 975] ; //limit op1000  geeft 1002 resultaten :-( go figure)
            system.debug(order.VF_Contract__r.Opportunity__r.Ban__c);
            BanManagerData theBmData = new BanManagerData(new Set<Id>{order.Account__c},null,fixed,onenet,false,true);

            return theBmData.bans;

        }
        private set;
    }   

    public String faIdSelected {get;set;}
    
    public List<SelectOption> faSelectOptions {
        get{
            faSelectOptions = new List<SelectOption>();
            faSelectOptions.add(new SelectOption('empty',Label.LABEL_Not_specified));
            if(banIdSelected != null){
                for(Financial_Account__c fa : [Select Id, Name From Financial_Account__c Where Ban__r.Name = :banIdSelected]){
                    // add to options
                    //if(b.BAN_Status__c == 'Closed'){
                        //fas.add(new SelectOption(fa.Id,fa.Name+(b.BOPCode__c==null?'':' ('+b.BOPCode__c+')'+' (closed)'),false));
                        faSelectOptions.add(new SelectOption(fa.Id, fa.Name  ));
                    //} 
                }
                faSelectOptions.add(new SelectOption('new','Create new in Unify'));
            }
            return faSelectOptions;
        }
        private set;
    }    


    public String barIdSelected {get;set;}
    
    public List<SelectOption> barSelectOptions {
        get{
            barSelectOptions = new List<SelectOption>();
            barSelectOptions.add(new SelectOption('empty',Label.LABEL_Not_specified));
            
            if(faIdSelected != null){
                for(Billing_Arrangement__c bar : [Select Id, Name, Payment_method__c,Bill_Format__c,Unify_Ref_Id__c From Billing_Arrangement__c Where Financial_Account__c = :faIdSelected]){
                    // add to options
                    //if(b.BAN_Status__c == 'Closed'){
                        //fas.add(new SelectOption(fa.Id,fa.Name+(b.BOPCode__c==null?'':' ('+b.BOPCode__c+')'+' (closed)'),false));
                        barSelectOptions.add(new SelectOption(bar.Id, bar.Name + ' ' + bar.Payment_method__c + (bar.Unify_Ref_Id__c==null?'':' ' + bar.Unify_Ref_Id__c)) );
                    //} 
                }
                barSelectOptions.add(new SelectOption('new','Create new in Unify'));
            }
            return barSelectOptions;
        }
        private set;
    }    

    public String faContactIdSelected {
        get{
            //if(faContactIdSelected == null){
                if(fa != null && fa.Financial_Contact__c != null)
                    faContactIdSelected = fa.Financial_Contact__c;
            //}
            return faContactIdSelected;
        }
        set;
    }

    public List<SelectOption> faContactSelectOptions {
        get{
            faContactSelectOptions = new List<SelectOption>();
            
            for(Contact c : [Select Id, Name From Contact Where AccountId = :order.Account__c]){
                faContactSelectOptions.add(new SelectOption(c.Id,c.Name));
            }
            return faContactSelectOptions;
        }
        private set;
    }    


}