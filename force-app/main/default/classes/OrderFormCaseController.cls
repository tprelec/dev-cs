public with sharing class OrderFormCaseController {

    String orderId;
    String contractId;
    public Case theCase {get;set;}
    public Blob document {get; set;}
    public String fileName {get; set;}

    public OrderFormCaseController() {
        orderId = apexpages.currentpage().getparameters().get('orderId');
        contractId = apexpages.currentpage().getparameters().get('contractId');
        theCase = new Case(
                        Contract_VF__c = contractId,
                        Order__c = orderId );
    }

    public Pagereference insertCaseRedirectToOrderForm(){
        try {
            insert theCase;
            if(document != null){
                Attachment attach = new Attachment(Body= document, ParentID = theCase.id, Name= filename);
                insert attach;
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }

        Pagereference orderForm = Page.OrderFormPreload;
        orderForm.getParameters().put('contractId',contractId);
        orderForm.getParameters().put('orderId',orderId);
        return orderForm;
    }

    public Pagereference insertCaseRedirectToCase(){
        try {
            insert theCase;
            if(document != null){
                Attachment attach = new Attachment(Body= document, ParentID = theCase.id, Name= filename);
                insert attach;
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }

        Pagereference theCasePage = new PageReference('/' + theCase.Id);
        return theCasePage;
    }
}