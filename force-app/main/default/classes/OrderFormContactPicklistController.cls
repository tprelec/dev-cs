public without sharing class OrderFormContactPicklistController {

	public static Order__c currentOrder {get;set;}
	private static Boolean isPartner {get;set;}
	public  static Boolean showPartner {get;set;}
	public static String partnerAcctId {
		get{
			if(partnerAcctId == null && isPartner != false){
        		VF_Contract__c contract = [Select Id, Opportunity__r.Owner.UserType, Opportunity__r.Owner.Contact.AccountId From VF_Contract__c Where Id = :currentOrder.VF_Contract__c];

        		if(contract != null && GeneralUtils.IsPartnerUser(contract.Opportunity__r.Owner)){
        			partnerAcctId = contract.Opportunity__r.Owner.Contact.AccountId; 
        			
        		} else {
        			isPartner = false;
        		}
			}
			return partnerAcctId;
		}
		set;

	}

    public List<SelectOption> getOrderContacts() {
    	system.debug('Vreuls: OrderFormContactPicklistController' + showPartner);
    	//showPartnerContacts=true;
        return OrderUtils.getOrderContacts(currentOrder.Account__c,partnerAcctId,showPartner);
    }
}