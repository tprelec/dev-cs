/**
 * @description         This is the controller class for the Customer Data Order Form. It collects all required data and shows it to the user
 * @author              Guy Clairbois
 */
public with sharing class OrderFormCustomerDataController {

    public VF_Contract__c theContract {get;set;}
    public String contractId{get; private set;}
    public String orderId{get; private set;}
    public Boolean customerLocked {get;set;}
    public Boolean locked {get;set;}
    public Boolean errorFound {get;set;}
    public Boolean showPartnerContacts {get;set;}
    //public OrderFormCache__c currentCache {get;set;}
    public OrderWrapper theOrderWrapper {get;set;}
    public String ZiggoOrderReference {get;set;}

    /**
     *      Controllers
     */

    public OrderFormCustomerDataController getApexController() {
        // used for passing the controller between form components
        return this;
    }
    
    public OrderFormCustomerDataController() {
        contractId = ApexPages.currentPage().getParameters().get('contractId');
        orderId = ApexPages.currentPage().getParameters().get('orderId');
        
        if(contractId == null){
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'No ContractId provided.'));
            errorFound = true;
        }
        
        //currentCache = OrderFormCache__c.getInstance(orderId);
        system.debug(orderId);
        system.debug(contractId);
        
        // fetch the contract details if the order is based on a contract
        if(contractId != null){
            errorFound = retrieveContract();
            if(orderId != null)
                theOrderWrapper = OrderUtils.retrieveOrderDetails(orderId,null);
            else
                theOrderWrapper = OrderUtils.retrieveOrderDetails(null,contractId);
                
            retrieveCustomerContacts(theContract.Account__c);            
        }
        
        locked = OrderUtils.getLocked(theOrderWrapper.theOrder); 
        customerLocked = locked;  
        // if customer is already synced with Unify, lock down all fields
        if(theContract.Account__r.Unify_Ref_Id__c != null) customerLocked = true;     

        // Store the current Ziggo Order Reference so we can check if it changes later
        ZiggoOrderReference = theContract.Opportunity__r.Ziggo_Order_Reference__c;
       
    }

    /**
     * @description         Retrieve the contract that the orders should be created/updated for
     */
    private Boolean retrieveContract(){
        Boolean error = false;
        theContract = OrderUtils.getContractRecordById(contractId);
        return error;
    }
    
    public pageReference refresh(){
        retrieveContract();

        // retrieve existing customer contacts
        retrieveCustomerContacts(theContract.Account__c);     
        List<Contact_Role__c> customerContactsToCheck = new List<Contact_Role__c>();
        if(theOrderWrapper.customerMainContact != null) customerContactsToCheck.add(theOrderWrapper.customerMainContact);
        if(theOrderWrapper.customerMaintenanceContact != null) customerContactsToCheck.add(theOrderWrapper.customerMaintenanceContact);
        if(theOrderWrapper.customerIncidentContact != null) customerContactsToCheck.add(theOrderWrapper.customerIncidentContact);
        if(theOrderWrapper.customerChooserContact != null) customerContactsToCheck.add(theOrderWrapper.customerChooserContact);


        // set default ban values for partners
        if(theOrderWrapper.theOrder.Direct_Indirect__c=='Indirect'){
            if(theContract.Account__r.Unify_Account_Type__c == null) theContract.Account__r.Unify_Account_Type__c = 'B'; // Business
            if(theContract.Account__r.Unify_Account_SubType__c == null) theContract.Account__r.Unify_Account_SubType__c = 'SBD'; // SME-Business Dealers
            SharingUtils.updateRecordsWithoutSharing(theContract.Account__r);
        } 

        // if the customer/contract details are complete, update the cache 
        if(OrderValidation.checkCompleteCustomerFlag(theContract,customerContactsToCheck,theOrderWrapper.theOrder)){
            //system.debug(currentCache);           
            if(theOrderWrapper.theOrder.customerReady__c == false){
                theOrderWrapper.theOrder.customerReady__c = true;
                SharingUtils.updateRecordsWithoutSharing(theOrderWrapper.theOrder);
            }
        } else {
            if(theOrderWrapper.theOrder.customerReady__c == true){
                theOrderWrapper.theOrder.customerReady__c = false;
                SharingUtils.updateRecordsWithoutSharing(theOrderWrapper.theOrder);
            }
        }


        // recheck if this order is now clean 
        OrderUtils.updateOrderStatus(theOrderWrapper.theOrder,true);        
        
        return null;    
    }

    public pageReference saveCustomer(){

        Savepoint sp = database.setSavepoint();
        try{

            // update opportunity - needed to save Ziggo Order Reference
            // But first check if the Ziggo Order Reference has changed - otherwise the opportunity update will fail due to validation rules
            if (theContract.Opportunity__r.Ziggo_Order_Reference__c!=ZiggoOrderReference) {            
                SharingUtils.updateRecordsWithoutSharing(theContract.Opportunity__r);       
                ZiggoOrderReference = theContract.Opportunity__r.Ziggo_Order_Reference__c;
            }

            // update customer
            SharingUtils.updateRecordsWithoutSharing(theContract.Account__r);
            update theContract;


            // update customer contacts
            List<Contact_Role__c> contactRolesToUpsert = new List<Contact_Role__c>();
            system.debug(mainContactSelected);
            system.debug(theOrderWrapper.customerMainContact);            
            if(mainContactSelected != null){
                if(theOrderWrapper.customerMainContact == null || mainContactSelected != theOrderWrapper.customerMainContact.Contact__c){
                    contactRolesToUpsert.add(createContactRole('main',mainContactSelected));
                }
                // update the main contact on the order to be able to check the order completeness
                //theOrderWrapper.theOrder.Customer_Main_Contact__c = mainContactSelected;                
            } else {
                // else deactivate the current contactrole
                if(theOrderWrapper.customerMainContact != null){
                    theOrderWrapper.customerMainContact.active__c = false;
                    contactRolesToUpsert.add(theOrderWrapper.customerMainContact);
                    //theOrderWrapper.theOrder.Customer_Main_Contact__c = null;
                }
            }
            if(maintenanceContactSelected != null){
                if(theOrderWrapper.customerMaintenanceContact == null || maintenanceContactSelected != theOrderWrapper.customerMaintenanceContact.Contact__c){
                    contactRolesToUpsert.add(createContactRole('maintenance',maintenanceContactSelected));
                }
            } else {
                // else deactivate the current contactrole
                if(theOrderWrapper.customerMaintenanceContact != null){
                    theOrderWrapper.customerMaintenanceContact.active__c = false;
                    contactRolesToUpsert.add(theOrderWrapper.customerMaintenanceContact);
                }
            }
            if(incidentContactSelected != null){
                if(theOrderWrapper.customerIncidentContact == null || incidentContactSelected != theOrderWrapper.customerIncidentContact.Contact__c){
                    contactRolesToUpsert.add(createContactRole('incident',incidentContactSelected));
                }
            }    else {
                // else deactivate the current contactrole
                if(theOrderWrapper.customerIncidentContact != null){
                    theOrderWrapper.customerIncidentContact.active__c = false;
                    contactRolesToUpsert.add(theOrderWrapper.customerIncidentContact);
                }
            }
            if(chooserContactSelected != null){
                if(theOrderWrapper.customerChooserContact == null || chooserContactSelected != theOrderWrapper.customerChooserContact.Contact__c){
                    contactRolesToUpsert.add(createContactRole('chooser',chooserContactSelected));
                }
            } else {
                // else deactivate the current contactrole
                if(theOrderWrapper.customerChooserContact != null){
                    theOrderWrapper.customerChooserContact.active__c = false;
                    contactRolesToUpsert.add(theOrderWrapper.customerChooserContact);
                }
            }           

            if(!contactRolesToUpsert.isEmpty()){
                // now check if any contact exports need to be triggered
                Set<Id> contactIds = new Set<Id>();
                for(Contact_Role__c cr : contactRolesToUpsert){
                    if(cr.Active__c) contactIds.add(cr.Contact__c);
                }
                Set<Id> contactIdsForInsert = new Set<Id>();
                for(Contact c : [Select Id From Contact Where BOP_Export_Datetime__c = null AND Id in :contactIds]){
                    contactIdsForInsert.add(c.Id);
                }
                List<Contact> contactsToUpdate = new List<Contact>();
                if(!contactIdsForInsert.isEmpty())
                    contactsToUpdate = ContactExport.exportContacts(contactIdsForInsert,'create');

                Set<Id> contactIdsForUpdate = new Set<Id>();
                for(Contact c : [Select Id From Contact Where BOP_Export_Datetime__c != null AND BOP_Export_Errormessage__c != null AND Id in :contactIds]){
                    contactIdsForUpdate.add(c.Id);
                }
                if(!contactIdsForUpdate.isEmpty())
                    contactsToUpdate = ContactExport.exportContacts(contactIdsForUpdate,'update');

                SharingUtils.updateRecordsWithoutSharing(contactsToUpdate);

                // upsert because existing role might be reused            
                SharingUtils.upsertRecordsWithoutSharing(contactRolesToUpsert);     
            }


        } catch (dmlException e){
            ApexPages.addMessages(e);
            Database.rollback(sp);
            system.debug(e);
            return null;
        }
        // refetch the data and update order status
        refresh();

        return null;
    }    

    /*
     *      Description:    generic method to create a new contact role. First check if inactive role can be reused
     */
    public Contact_Role__c createContactRole(String type,String contactId){
        // first check if inactive role can be reused
        Contact_Role__c[] crs = [Select 
                                    Id, 
                                    Contact__c, 
                                    Account__c, 
                                    Type__c 
                                From 
                                    Contact_Role__c 
                                Where 
                                    Account__c = :theContract.Account__c 
                                AND
                                    Contact__c = :contactId 
                                AND 
                                    Type__c = :type
                                AND
                                    Active__c = false];
        Contact_Role__c cr;
        if(!crs.isEmpty()){
            cr = crs[0];
        } else {
            // if no inactive can be reused, create new
            cr = new Contact_Role__c();
            cr.Type__c = type;
            cr.Contact__c = contactId;
            cr.Account__c = theContract.Account__c;
        }
        // set to active
        cr.Active__c = true;
        return cr;
    }    

    public void retrieveCustomerContacts(Id accountId){

        
        for(Contact_Role__c cr : OrderUtils.getContactRolesByAccountId(new Set<Id>{accountId}).get(accountId)){
            
            // we can assume that each customer contact role exists only once.
            if(cr.Type__c == 'main'){
                theOrderWrapper.customerMainContact = cr;   
                mainContactSelected = cr.Contact__c;    
            } else if(cr.Type__c == 'maintenance'){
                theOrderWrapper.customerMaintenanceContact = cr;        
                maintenanceContactSelected = cr.Contact__c;
            } else if(cr.Type__c == 'incident'){
                theOrderWrapper.customerIncidentContact = cr;       
                incidentContactSelected = cr.Contact__c;
            } else if(cr.Type__c == 'chooser'){
                theOrderWrapper.customerChooserContact = cr;        
                chooserContactSelected = cr.Contact__c;
            } 
        }
        system.debug(theOrderWrapper.customerMainContact);
        system.debug(theOrderWrapper.customerMaintenanceContact);

    }

    public String mainContactSelected {get;set;}
    public String maintenanceContactSelected {get;set;}
    public String incidentContactSelected {get;set;}
    public String chooserContactSelected {get;set;}

}