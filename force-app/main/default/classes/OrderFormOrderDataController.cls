/**
 * @description			This is the controller class for the Order Form. It collects all required data and shows it to the user
 * @author				Guy Clairbois
 */
public with sharing class OrderFormOrderDataController {

	/**
	 *		Variables
	 */

    //public VF_Contract__c contract {get;set;}
	public OrderWrapper theOrderWrapper {get;set;}
	public List<Contracted_Products__c> additionalArticles {get;set;}    
    public String contractId{get; private set;}
    public String orderId{get; private set;}
	private Boolean errorFound{get;set;}
    //public OrderFormCache__c currentCache {get;set;}
    public Boolean locked {get;set;}
    public static Boolean orderSubmitted {get;set;}
    public Contact quickNewContact {get;set;}
    public Boolean showQuickNewContact {get;set;}
	public String newRemarkLine {get;set;}
	public String newRemarkLinePrefix {get;set;}

	public String newRejectionRemarkLine {get;set;}
	public String newRejectionRemarkLinePrefix {get;set;}

	public Boolean disableAddingRejectionRemarks {get;set;}

	public PageReference orderPage{get;set;}


	/**
	 *		Controllers
	 */

 	public OrderFormOrderDataController getApexController() {
 		// used for passing the controller between form components
        return this;
    }
    
    public OrderFormOrderDataController() {

    	//begin temp stuff - Stjepan?

    		orderPage = page.OrderFormOrderDetails;
    		orderPage.getParameters().put('contractId',contractId);
    		orderPage.getParameters().put('orderId',orderId);
    		orderPage.setRedirect(true);

    	//end temp stuff	
    	disableAddingRejectionRemarks = !Special_Authorizations__c.getInstance().Submit_Orders__c;

    	contractId = ApexPages.currentPage().getParameters().get('contractId');
    	orderId = ApexPages.currentPage().getParameters().get('orderId');

		//currentCache = OrderFormCache__c.getInstance(orderId);
	    	
    	errorFound = false;
    	showQuickNewContact = false;
    	newRemarkLinePrefix = '['+System.now().format('yyyy-MM-dd')+' '+System.Userinfo.getName()+'] ';
    	newRejectionRemarkLinePrefix = '['+System.now().format('yyyy-MM-dd')+' '+System.Userinfo.getName()+'] ';
	    	
    	if(orderId == null ){
    		ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'No Order Id provided.'));
    		errorFound = true;
    	}
	    	
    	// only continue if no errors were found earlier on
    	if(!errorFound){
    		// retrieve existing orders
			retrieveOrder();

			// retrieve additional articles
			additionalArticles = OrderUtils.retrieveOrderIdToAdditionalArticles(New Set<Id>{orderId}).get(orderId);
    	}
    	locked = OrderUtils.getLocked(theOrderWrapper.theOrder);
    	system.debug(theOrderWrapper);
    	system.debug(theOrderWrapper.theOrder.VF_Contract__r.Opportunity__r.Ban__r.BAN_Number__c);
				
    }


	/**
	 *		Page References
	 */


	public pageReference saveOrder(){
		// if record is locked, prevent updating
		if(theOrderWrapper.theOrder.Record_Locked__c){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_Order_Locked));
			return null;
		}		
		try{
			// update order record
			update theOrderWrapper.theOrder;
			// update contract record
			update theOrderWrapper.theOrder.VF_Contract__r;
			// reload/recheck to correctly load the red/green flag
			updateOrderStatus();			
		} catch (dmlException e){
			ApexPages.addMessages(e);
		}
		return null;
	}

	public void updateOrderStatus(){
		retrieveOrder();

       // if the order/contact details are complete, update the cache
       if(OrderValidation.checkCompleteOrderFlag(theOrderWrapper.theOrder)){
    		system.debug(theOrderWrapper.theOrder);        	
			if(theOrderWrapper.theOrder.OrderReady__c == false){
				theOrderWrapper.theOrder.OrderReady__c = true;
				SharingUtils.updateRecordsWithoutSharing(theOrderWrapper.theOrder);
			}
        } else {
			if(theOrderWrapper.theOrder.OrderReady__c == true){
				theOrderWrapper.theOrder.OrderReady__c = false;
				SharingUtils.updateRecordsWithoutSharing(theOrderWrapper.theOrder);
			}
        }			
		
		// recheck if this order is now clean 
		OrderUtils.updateOrderStatus(theOrderWrapper.theOrder,true);			
	}

	public pageReference addLineToRemarks(){
		system.debug(newRemarkLine);
		if(newRemarkLine != null){

			theOrderWrapper.theOrder.remarks__c = newRemarkLinePrefix + newRemarkLine + '\n' + (theOrderWrapper.theOrder.remarks__c==null?'':theOrderWrapper.theOrder.remarks__c);
			update theOrderWrapper.theOrder;
			newRemarkLine = null;
		}
		return null;
	}

		public pageReference addLineToRejectionRemarks(){
		system.debug(newRejectionRemarkLine);
		if(newRejectionRemarkLine != null){

			theOrderWrapper.theOrder.Rejection_Remarks__c = newRejectionRemarkLinePrefix + newRejectionRemarkLine + '\n' + (theOrderWrapper.theOrder.Rejection_Remarks__c==null?'':theOrderWrapper.theOrder.Rejection_Remarks__c);
			update theOrderWrapper.theOrder;
			newRejectionRemarkLine = null;
		}
		return null;
	}



    /**
     * @description			Retrieve any already existing orders
     */    
    public void retrieveOrder(){
        
        List<Order__c> orders = OrderUtils.getOrderDataByContractId(null,new Set<Id>{Id.valueOf(orderId)});
        
    	if(theOrderWrapper == null) theOrderWrapper = new OrderWrapper();
    	theOrderWrapper.theOrder = orders[0];

    }
    

  
 	/*		
 	 *		Description: Functionality for loading the selection lists for Order contacts (from both customer contacts and partner contacts).
 	 */ 	
 	 
	/* Contact Selection dropdowns */
	


	public String projectContactSelected {
		get{
			if(theOrderWrapper != null){
				return theOrderWrapper.theOrder.Project_Contact__c;
			} else {
				return null;
			}			
		}
		set{
			if(value != ''){
				projectContactSelected = value;
				theOrderWrapper.theOrder.Project_Contact__c = projectContactSelected;
			}
		}
	}
	public String ccContactSelected {
		get{
			if(theOrderWrapper != null){
				return theOrderWrapper.theOrder.CC_Contact__c;
			} else {
				return null;
			}			
		}
		set{
			if(value != '' ){
				ccContactSelected = value;
				theOrderWrapper.theOrder.CC_Contact__c = ccContactSelected;
			}
		}
	}	
	public String infraContactSelected {
		get{
			if(theOrderWrapper != null){
				return theOrderWrapper.theOrder.Infra_Contact__c;
			} else {
				return null;
			}			
		}
		set{
			if(value != ''){
				infraContactSelected = value;
				theOrderWrapper.theOrder.Infra_Contact__c = infraContactSelected;
			}
		}
	}

	public pageReference claimOrder(){
		OrderAccept.claimOrders(new List<Order__c> {theOrderWrapper.theOrder});
		return null;
	}

	public pageReference acceptOrder(){
		OrderAccept.acceptOrders(new List<Order__c> {theOrderWrapper.theOrder});
		// this could lead to a green flag. Re-fetch cache because it will be updated.
		//currentCache = OrderFormCache__c.getInstance(orderId);

		updateOrderStatus();
		return null;
	}

	public pageReference rejectOrder(){
		OrderReject.rejectOrders(new List<Order__c> {theOrderWrapper.theOrder});
		return null;
	}

    public pageReference createQuickNewContact() {
    	quickNewContact = new Contact(AccountId = theOrderWrapper.theOrder.Account__c);
    	showQuickNewContact = true;
    	return null;
    }

    public pageReference backToOrderForm(){
    	showQuickNewContact = false;
    	return null;

    }

    public pageReference saveQuickNewContact(){
    	try{
    		insert quickNewContact;
    		showQuickNewContact = false;

    		PageReference orderPage = page.OrderFormOrderDetails;
    		orderPage.getParameters().put('contractId',contractId);
    		orderPage.getParameters().put('orderId',orderId);
    		orderPage.setRedirect(true);
    		return orderPage;
    	} catch (dmlException de){
    		ApexPages.addMessages(de);
   		
    	}
    	return null;
    }



}