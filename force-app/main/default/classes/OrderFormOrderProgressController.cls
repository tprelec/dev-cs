/**
 * @description         This is the controller class for the OrderProgress part of the Order Form. It collects all required data and shows it to the user
 * @author              Guy Clairbois
 */
public with sharing class OrderFormOrderProgressController {

    public Order__c order {get;set;}
    public List<Order_Response__c> responses {get;set;}
    public String contractId{get; private set;}
    public String orderId{get; private set;}
    public Boolean errorFound {get;set;}
    
    /**
     *      Controllers
     */
    public OrderFormOrderProgressController getApexController() {
        // used for passing the controller between form components
        return this;
    }
    
    public OrderFormOrderProgressController() {
        orderId = ApexPages.currentPage().getParameters().get('orderId');

        if (orderId == null) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'No OrderId provided.'));
            errorFound = true;
        }

        // fetch the contract details if the order is based on a contract
        if (orderId != null) {
            errorFound = retrieveOrder();
            errorFound = retrieveOrderResponses();

            // add order validations
            if (orderValidation()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, validationErrors));
            }
        }
        
    }

    /**
     * @description         Retrieve the order data
     */
    public Boolean retrieveOrder() {
        Boolean error = false;
        order = OrderUtils.getOrderDataByContractId(null, new Set<Id>{orderId})[0];
        return error;
    }

    /**
     * @description         Refresh the order data
     */
    public PageReference refreshOrder() {
        Boolean b = retrieveOrder();
        return null;
    }


    /**
     * @description         Retrieve the orders responses
     */
    private Boolean retrieveOrderResponses() {
        Boolean error = false;
        responses = [
            SELECT Id, 
                    CreatedDate,
                    Type__c, 
                    Unify_Error_Number__c, 
                    Unify_Error_Description__c 
            FROM Order_Response__c 
            WHERE Order__c = :orderId
            ORDER BY CreatedDate DESC
        ];
        return error;
    }    
    
    /**
     * @description         Do order validation
     */
    public String validationErrors { get; set; }
    public Boolean orderValidation() {
        Boolean error = false;
        validationErrors = OrderValidation.checkCompleteValidationBeforeBOP(order);
        error = String.isNotBlank(validationErrors);
        return error;
    }

    public pageReference refresh() {
        retrieveOrder();
        retrieveOrderResponses();
        if (orderValidation()) {
            order.BOP_export_Errormessage__c = validationErrors;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, validationErrors));
        } else if (order.Status__c == 'Validation') { // to do: check
            update new Order__c(Id = orderId, Status__c = 'Submitted');
            errorFound = retrieveOrder();
        }
        return null;    
    }

    public Integer getProgressValue() {
        if (order.Export_System_CustomerData__c == 'BOP' && order.Export__c == 'BOP') {
            if (order.Sales_Signoff_Datetime__c == null) {
                return 0;
            } else if (order.SAG_Signoff_Datetime__c == null) {
                return 33;
            } else if (order.BOP_export_datetime__c == null) {
                return 67;
            } else {
                return 100;
            }
        } else {
            if (order.Sales_Signoff_Datetime__c == null) {
                return 0;
            } else if (order.SAG_Signoff_Datetime__c == null) {
                return 25;
            } else if (order.Unify_Customer_export_datetime__c == null) {
                return 50;
            } else if (order.Unify_Order_export_datetime__c == null && order.BOP_export_datetime__c == null) {
                return 75;                
            } else {
                return 100;
            }
        }
    }

}