/**
 * @description			This is the controller class for the Phonebook Registration details on the Order Form. 
 * @author				Guy Clairbois
 */
public with sharing class OrderFormPhonebookRegDataController {

	/**
	 *		Variables
	 */

    public String contractId{get; private set;}
    public String orderId{get; private set;}
    public List<PhonebookRegistrationWrapper> phonebookRegistrations {get;set;}
	private Boolean errorFound{get;set;}
    public OrderWrapper theOrderWrapper {get;set;}
	public Numberporting__c numberporting {get;set;}


	public Boolean locked {get;set;}


	/**
	 *		Controllers
	 */

 	public OrderFormPhonebookRegDataController getApexController() {
 		// used for passing the controller between form components
        return this;
    }
    
    public OrderFormPhonebookRegDataController() {
    	contractId = ApexPages.currentPage().getParameters().get('contractId');
    	orderId = ApexPages.currentPage().getParameters().get('orderId');
    	errorFound = false;
	    	
    	if(contractId == null){
    		ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'No ContractId provided.'));
    		errorFound = true;
    	}
	    
    	
    	// only continue if no errors were found earlier on
    	if(!errorFound){
            theOrderWrapper = OrderUtils.retrieveOrderDetails(orderId,null);

	    	// fetch any existing phonebookregs
	    	phonebookRegistrations = new List<PhonebookRegistrationWrapper>();
		    
		    retrievePhonebookRegistrations();
			try {
				numberporting = [Select Phonebook_Registration_Option__c From Numberporting__c Where Order__c = :theOrderWrapper.theOrder.Id];		
			} catch(Exception ex){
				// number porting data is missing for some reason so let's create it
				numberporting = new Numberporting__c();
				numberporting.Name = 'Numberporting for '+theOrderWrapper.theOrder.Name; 
				numberporting.Customer__c = theOrderWrapper.theOrder.account__c;
				numberporting.Order__c = theOrderWrapper.theOrder.id;
			}    	
    	}
    	locked = OrderUtils.getLocked(theOrderWrapper.theOrder);

    }

    /**
     * @description			Retrieve any already existing phonebookregs
     */    
    private void retrievePhonebookRegistrations(){
	    	
	    Map<Id,List<PhonebookRegistrationWrapper>> orderIdToPhonebookRegistrations = OrderUtils.retrieveOrderIdToPhonebookRegistrations(new Set<Id>{orderId});
    	if(orderIdToPhonebookRegistrations.containsKey(orderId))
    		phonebookRegistrations = orderIdToPhonebookRegistrations.get(orderId); 

    }  
    
	/**
	 *		Page References
	 */
    

	public pageReference savePhonebookRegistrations(){ 	
		// if record is locked, prevent updating
		if(theOrderWrapper.theOrder.Record_Locked__c){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_Order_Locked));
			return null;
		}		
		system.debug(phonebookRegistrations);
        //Loop through the phonebook registrations list and create a new storable list, also fill the reference to
        //the numberporting for each phonebook registration
    	List<Phonebook_registration__c> registrations = new List<Phonebook_registration__c>();
    	for (PhonebookRegistrationWrapper pbr : phonebookRegistrations) {
    		
    		if (pbr.phonebookReg.Numberporting__c == null) {
    			try{
    				Numberporting__c np = OrderUtils.createPorting(theOrderWrapper.theOrder);
    				pbr.phonebookReg.Numberporting__c = np.Id;
    			} catch (dmlException ex){
    				ApexPages.addMessages(ex);
    				return null;
    			}
    		}
    		if(pbr.phonebookReg.Zipcode__c != null) pbr.phonebookReg.Zipcode__c = pbr.phonebookReg.Zipcode__c.toUpperCase(); 
    		
    		registrations.add(pbr.phonebookReg);
    	}

		// savepoint only needed here. No prob if numberporting record is created..	
		Savepoint sp = Database.setSavepoint();    	
    	// store the phonebookregistrations to the database
		try{
    		upsert registrations;

    		if (numberporting.id==null) {
    			insert numberporting;
    		} else {
    			update numberporting;    			
    		}

			// reload/recheck to correctly load the red/green flag
			updatePhonebookRegistrationStatus();
			  		
		} catch(DmlException ex){
			Database.rollback(sp);
			ApexPages.addMessages(ex);    
		}    
    	return null; 		
 	}
 
 	public void updatePhonebookRegistrationStatus(){
		// reload to correctly load the red/green flag
		retrievePhonebookRegistrations();
		
		Boolean allPhonebookRegistationsReady = true;
		
		for(PhonebookRegistrationWrapper pbr : phonebookRegistrations){
			if(OrderValidation.checkObject(pbr.phonebookreg,theOrderWrapper.theOrder) != ''){
				allPhonebookRegistationsReady = false;
				break;
			}
   		}

		if(theOrderWrapper.theOrder.PhonebookregistrationReady__c != allPhonebookRegistationsReady){
			theOrderWrapper.theOrder.PhonebookregistrationReady__c = allPhonebookRegistationsReady;			
			SharingUtils.updateRecordsWithoutSharing(theOrderWrapper.theOrder);
		}
		
		// recheck if this order is now clean 
		OrderUtils.updateOrderStatus(theOrderWrapper.theOrder,true);			

 	}
 
}