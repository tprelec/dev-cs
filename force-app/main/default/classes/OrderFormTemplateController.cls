public without sharing class OrderFormTemplateController {
	public String ORDER_STATUS_PROCESSED_MANUALLY {
		get {
			return Constants.ORDER_STATUS_PROCESSED_MANUALLY;
		}
		private set;
	}
	public String contractId { get; private set; }
	public String orderId { get; private set; }
	public Integer unorderedItems {
		get {
			unorderedItems = 0;
			for (Contracted_Products__c cp : [
				SELECT Id
				FROM Contracted_Products__c
				WHERE VF_Contract__c = :contractId AND Order__c = NULL
			]) {
				unorderedItems++;
			}
			return unorderedItems;
		}
		private set;
	}
	public Boolean showCreateNewOrder { get; private set; }
	public Boolean skipOnlineSubmit { get; private set; }
	public Order__c theOrder {
		get {
			// Note that this is always newly queried. So should only be used if you absolutely need fresh data
			return [
				SELECT
					Id,
					Record_Locked__c,
					Name,
					Status__c,
					Mobile_Fixed__c,
					VF_Contract__r.Opportunity__r.Escalation__c,
					Propositions__c,
					Ready_for_Inside_Sales__c,
					O2C_Order__c,
					Ordertype__r.Support_needed__c,
					EMP_Automated_Mobile_order__c,
					Export_system_Customerdata__c,
					Remark__c,
					Remark_Information__c,
					Account__c
				FROM Order__c
				WHERE Id = :orderId
			];
		}
		set;
	}
	public List<Order__c> orderCaches {
		get {
			// Generate the list of orders to select from
			orderCaches = [
				SELECT
					Id,
					Name,
					BOP_Order_Id__c,
					Sales_Order_Id__c,
					BOP_Export_Datetime__c,
					VF_Contract__c,
					CreatedDate,
					CreatedById,
					Export__c,
					Export_system_Customerdata__c,
					Number_of_items__c,
					Propositions__c,
					Status__c,
					Ready_for_Inside_Sales_Datetime__c,
					OrderType__r.Support_Needed__c
				FROM Order__c
				WHERE VF_Contract__c = :contractId
				ORDER BY Name ASC
			];
			return orderCaches;
		}
		set;
	}

	public Boolean proceed { get; set; }
	public Boolean showValidationMessage { get; set; }

	public List<ContractedProductWrapper> cpWrappers { get; set; }

	public OrderFormTemplateController() {
		Boolean errorFound = false;
		this.proceed = true;
		this.showValidationMessage = false;
		if (contractId == null) {
			contractId = ApexPages.currentPage().getParameters().get('contractId');
		}
		if (orderId == null) {
			orderId = ApexPages.currentPage().getParameters().get('orderId');
		}
		if (orderId == null) {
			ApexPages.addMessage(
				new ApexPages.Message(ApexPages.severity.ERROR, 'No OrderId provided.')
			);
			errorFound = true;
		}
		// Only continue if no errors were found earlier on
		if (!errorFound) {
			// preset the current orderform as selected
			if (orderFormSelected == null && orderId != null) {
				orderFormSelected = orderId;
			}
		}
		skipOnlineSubmit = false;
	}

	public void cancel() {
	}

	public PageReference backTo() {
		Id opportunityId = [
			SELECT Opportunity__c
			FROM VF_Contract__c
			WHERE Id = :contractId
			LIMIT 1
		]
		.Opportunity__c;
		PageReference pr;
		if (contractId != null) {
			pr = new PageReference('/' + opportunityId);
		} else {
			pr = new PageReference('/home/home.jsp');
		}
		return pr;
	}

	public PageReference submitOrder() {
		Order__c o = theOrder;

		// prevent double submitting in 1 transaction
		if (OrderFormOrderDataController.orderSubmitted != true) {
			if (o.Status__c == 'Clean') {
				OrderFormOrderDataController.orderSubmitted = true;
				if (this.proceed && !o.Remark__c && pricePlanClassValidation(o)) {
					// set flag to prompt user for input
					this.proceed = false;
					this.showValidationMessage = true;
					return null;
				} else {
					this.showValidationMessage = false;
				}
				// trigger online order export (to improve feedback to end user)
				if (!skipOnlineSubmit) {
					if (o.Export_system_Customerdata__c == 'SIAS') {
						// kick off sias customer export
						// order export (sias or bop) will be automatically triggered from there
						CustomerExportSias.sendCustomerNotification(orderId);
					} else {
						// if customer export is not sias (so bop), order export should also be bop
						if (o.Propositions__c != 'Legacy') {
							if (!OrderExport.ordersInExport.contains(orderId)) {
								OrderExport.ordersInExport.add(orderId);
								OrderExport.exportOrders(new Set<Id>{ orderId }, 'create');
							}
						} else {
							if (!OrderExport.ordersInExport.contains(orderId)) {
								OrderExport.ordersInExport.add(orderId);
								OrderExport.exportOrders(new Set<Id>{ orderId }, 'legacy');
							}
						}
					}
				} else {
					// offline order export
					o.Status__c = 'Submitted'; // no need to set to 'submitted' as it already will be accepted or refused..
					update o;
				}
				// generate onenet provisioning xml if applicable
				if (
					OrderWrapper.getHasOneNetFromPropositions(o.Propositions__c) &&
					OrderWrapper.getHasFixedVoiceFromPropositions(o.Propositions__c)
				) {
					generateProvisioningXML();
				}
				// refresh to get current status
				o = [SELECT Id, Status__c FROM Order__c WHERE Id = :orderId];
				o.SAG_Signoff_Datetime__c = System.now();
				if (o.Status__c == 'Accepted') {
					o.Record_Locked__c = true;
				}
				update o;
				OrderFormOrderDataController.orderSubmitted = false;
			} else {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.severity.ERROR,
						'Only orders with status Clean can be submitted.'
					)
				);
				return null;
			}
		}
		Pagereference orderPage = Page.OrderFormOrderProgress;
		orderPage.getParameters().put('contractId', contractId);
		orderPage.getParameters().put('orderId', orderId);
		return orderPage.setRedirect(true);
	}

	@testVisible
	private Boolean pricePlanClassValidation(Order__c order) {
		if (!order.EMP_Automated_Mobile_order__c) {
			return false;
		}
		List<Contracted_Products__c> contractedProdustList = [
			SELECT Id, Deal_Type__c, Order__c, Price_Plan_Class__c
			FROM Contracted_Products__c
			WHERE
				Order__c = :order.Id
				AND Deal_Type__c = 'Retention'
				AND Price_Plan_Class__c = 'Data'
		];
		Boolean hasDataProducts = !contractedProdustList.isEmpty();
		List<VF_Asset__c> assetList = [
			SELECT Id, Account__c, Priceplan_Class__c
			FROM VF_Asset__c
			WHERE
				Account__c = :order.Account__c
				AND Priceplan_Class__c = 'Data Only'
				AND CTN_Status__c = 'Active'
		];
		Boolean hasDataOnlyAssets = !assetList.isEmpty();
		return hasDataOnlyAssets && !hasDataProducts;
	}

	public void procedeWithManualProvisioning() {
		Order__c order = [
			SELECT Id, Remark__c, Remark_Information__c, Remarks__c
			FROM Order__c
			WHERE Id = :orderId
		];
		order.Remark__c = true;
		order.Remark_Information__c = System.Label.EMP_Proceed_With_Automated_Provisioning;
		update order;
		this.proceed = true;
		this.showValidationMessage = false;
	}

	public PageReference acceptOrder() {
		String errors = OrderAccept.acceptOrdersError(
			new List<Order__c>{ new Order__c(Id = orderId, Status__c = theOrder.Status__c) }
		);
		if (errors == '') {
			return ApexPages.currentPage().setRedirect(true);
		} else {
			return null;
		}
	}

	public PageReference goToOrderProgress() {
		Pagereference orderPage = Page.OrderFormOrderProgress;
		orderPage.getParameters().put('contractId', contractId);
		orderPage.getParameters().put('orderId', orderId);
		return orderPage.setRedirect(true);
	}

	public String assignToInsideSalesLabel {
		get {
			if (System.Userinfo.getUserType() != 'PowerPartner') {
				return Label.LABEL_Sales_Assistance;
			} else {
				return 'Vodafone';
			}
		}
		set;
	}

	public PageReference assignToInsideSales() {
		Order__c o = new Order__c(
			Id = orderId,
			Ready_for_Inside_Sales__c = true,
			Status__c = 'Assigned to Inside Sales',
			Ready_for_Inside_Sales_Datetime__c = System.now(),
			Sales_Signoff_Datetime__c = System.now()
		);

		// If the order is mobile, simulate acceptance by SAG (this is needed to check if it is clean and auto-process)
		if (
			theOrder.Mobile_Fixed__c == 'Mobile' ||
			theOrder.Ordertype__r.Support_needed__c == 'No'
		) {
			o.Status__c = 'Accepted by Inside Sales';
		}
		update o;

		// Reload the order and all of it's statuses using the Inside Sales requirements
		OrderUtils.updateOrderStatus(
			OrderUtils.getOrderDataByContractId(null, new Set<Id>{ Id.valueOf(orderId) }),
			null
		);

		// If the order is clean and doesn't need sag updates, submit it automatically (e.g. mobile orders)
		if (
			(theOrder.Mobile_Fixed__c == 'Mobile' ||
			theOrder.Ordertype__r.Support_needed__c == 'No') && theOrder.Status__c == 'Clean'
		) {
			skipOnlineSubmit = true;
			return submitOrder();
		}
		return ApexPages.currentPage().setRedirect(true);
	}

	public PageReference generateProvisioningXML() {
		OneNetXMLGenerate.createAndSaveProvisioningXML(orderId);
		ApexPages.addMessage(
			new ApexPages.Message(
				ApexPages.severity.INFO,
				'Provisioning XML generated and added to Contract Attachments'
			)
		);
		return null;
	}

	public PageReference createNewOrder() {
		showCreateNewOrder = true;
		cpWrappers = new List<ContractedProductWrapper>();
		for (Contracted_Products__c cp : [
			SELECT
				Id,
				Duration__c,
				Name,
				Interface_type__c,
				Line_Description__c,
				Product__r.Name,
				Product__r.Product_Group__c,
				Product__r.ProductCode,
				Product__r.Role__c,
				Proposition__c,
				Quantity__c,
				Site__c,
				Site__r.PBX__c,
				VF_Contract__c
			FROM Contracted_Products__c
			WHERE VF_Contract__c = :contractId AND Order__c = NULL
		]) {
			ContractedProductWrapper cpw = new ContractedProductWrapper();
			cpw.cp = cp;
			cpw.selected = false;
			cpWrappers.add(cpw);
		}
		return null;
	}

	public PageReference confirmAddToOrder() {
		if (orderSelected == null) {
			ApexPages.addMessage(
				new ApexPages.Message(
					ApexPages.severity.INFO,
					'You have to make an Order selection!'
				)
			);
			return null;
		} else if (orderSelected == 'New') {
			return confirmCreateNewOrder();
		} else {
			return confirmAddToExistingOrder();
		}
	}

	public PageReference confirmCreateNewOrder() {
		Integer numberSelected = 0;
		Set<String> propositions = new Set<String>();
		String propositionsString = '';
		Boolean errorFound = false;
		for (ContractedProductWrapper cpw : cpWrappers) {
			if (cpw.selected) {
				numberSelected++;
				// make sure that for orders with numberporting only, the proposition is Numberporting-only
				if (cpw.cp.Proposition__c != null && cpw.cp.Product__r.Role__c == 'Numberporting') {
					propositions.add('Numberporting');
				} else if (cpw.cp.Proposition__c != null) {
					propositions.add(cpw.cp.Proposition__c);
				} else {
					propositions.add('Legacy');
				}
			}
		}
		for (String s : propositions) {
			propositionsString += s + ';';
		}
		if (numberSelected == 0) {
			ApexPages.addMessage(
				new ApexPages.Message(ApexPages.severity.ERROR, 'No products selected.')
			);
			errorFound = true;
			return null;
		} else {
			VF_Contract__c contract = [
				SELECT Id, Account__c, Opportunity__r.Main_Contact_Person__c
				FROM VF_Contract__c
				WHERE Id = :contractId
			];
			Order__c newOrder = OrderFormController.createNewOrder(
				numberSelected,
				propositions,
				contract
			);
			Savepoint sp = Database.setSavepoint();
			try {
				insert newOrder;
				List<Contracted_Products__c> cpToUpdate = new List<Contracted_Products__c>();
				for (ContractedProductWrapper cpw : cpWrappers) {
					if (cpw.selected) {
						cpw.cp.Order__c = newOrder.Id;
						cpToUpdate.add(cpw.cp);
					}
				}
				update cpToUpdate;
			} catch (Exception e) {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.severity.ERROR,
						System.Label.ERROR_OrderForm_Cannot_Create_Numberportings +
						' ' +
						e.getMessage()
					)
				);
				errorFound = true;
				Database.rollback(sp);
				return null;
			}
			// Add numberportings to the order
			try {
				List<NumberPortingRowWrapper> portingRows = new List<NumberportingRowWrapper>();
				OrderFormController.addDefaultNumberportings(theOrder, portingRows);
			} catch (Exception e) {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.severity.ERROR,
						System.Label.ERROR_OrderForm_Cannot_Create_Numberportings +
						' ' +
						e.getMessage()
					)
				);
				errorFound = true;
				Database.rollback(sp);
				return null;
			}
		}
		showCreateNewOrder = false;
		return null;
	}

	public PageReference confirmAddToExistingOrder() {
		Integer numberSelected = 0;
		Boolean errorFound = false;
		for (ContractedProductWrapper cpw : cpWrappers) {
			if (cpw.selected) {
				numberSelected++;
			}
		}
		if (numberSelected == 0) {
			ApexPages.addMessage(
				new ApexPages.Message(ApexPages.severity.ERROR, 'No products selected.')
			);
			errorFound = true;
			return null;
		} else {
			Savepoint sp = Database.setSavepoint();
			List<Contracted_Products__c> newPortings = new List<Contracted_Products__c>();
			Order__c theOrder = [
				SELECT
					Id,
					Name,
					VF_Contract__c,
					Account__c,
					Number_of_items__c,
					Propositions__c,
					Mobile_Fixed__c
				FROM Order__c
				WHERE Id = :orderSelected
			];
			try {
				List<Contracted_Products__c> cpToUpdate = new List<Contracted_Products__c>();
				for (ContractedProductWrapper cpw : cpWrappers) {
					if (cpw.selected) {
						cpw.cp.Order__c = orderSelected;
						cpToUpdate.add(cpw.cp);
						newPortings.add(cpw.cp);
					}
				}
				update cpToUpdate;

				theOrder.Number_of_items__c = theOrder.Number_of_items__c != null
					? theOrder.Number_of_items__c + cpToUpdate.size()
					: cpToUpdate.size();
				update theOrder;
			} catch (Exception e) {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.severity.ERROR,
						System.Label.ERROR_OrderForm_Cannot_Create_Numberportings +
						' ' +
						e.getMessage()
					)
				);
				errorFound = true;
				Database.rollback(sp);
				return null;
			}
			// Add numberportings to the order
			try {
				List<Numberporting_Row__c> numberportingsToInsert = OrderformController.createNumberportings(
					newPortings,
					theOrder.Account__c
				);
				for (Numberporting_Row__c npr : numberportingsToInsert) {
					if (npr.Numberporting__c == null) {
						Numberporting__c np = OrderUtils.createPorting(theOrder);
						npr.Numberporting__c = np.Id;
					}
				}
				insert numberportingsToInsert;
			} catch (Exception e) {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.severity.ERROR,
						System.Label.ERROR_OrderForm_Cannot_Create_Numberportings +
						' ' +
						e.getMessage()
					)
				);
				errorFound = true;
				Database.rollback(sp);
				return null;
			}
		}
		// Reload the order and all of it's statuses including the new items
		OrderUtils.updateOrderStatus(
			OrderUtils.getOrderDataByContractId(null, new Set<Id>{ Id.valueOf(orderId) }),
			null
		);
		showCreateNewOrder = false;
		// Reload the entire page in order to show the additions correctly
		PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
		pageRef.getParameters().clear();
		pageRef.getParameters().put('contractId', contractId);
		pageRef.getParameters().put('orderId', orderId);
		pageRef = pageRef.setRedirect(true);
		return pageRef;
	}

	public PageReference cancelCreateNewOrder() {
		showCreateNewOrder = false;
		return null;
	}

	public class ContractedProductWrapper {
		public Contracted_Products__c cp { get; set; }
		public Boolean selected { get; set; }
	}

	public Boolean getShowSubmitButton() {
		// Only show button for inside sales and for administrators
		if (Special_Authorizations__c.getInstance().Submit_Orders__c) {
			return true;
		}
		return false;
	}

	public Boolean getShowAddProductButton() {
		if (
			!OrderUtils.getLocked(theOrder) &&
			Special_Authorizations__c.getInstance().Add_Products_to_Orders__c != null &&
			theOrder.Mobile_Fixed__c == 'Fixed'
		) {
			return true;
		} else {
			return false;
		}
	}

	public PageReference addProduct() {
		Pagereference orderPage = Page.OrderFormAddProduct;
		orderPage.getParameters().put('contractId', contractId);
		orderPage.getParameters().put('orderId', orderId);
		return orderPage.setRedirect(true);
	}

	public PageReference logCase() {
		Pagereference casePage = Page.OrderFormLogCase;
		casePage.getParameters().put('contractId', contractId);
		casePage.getParameters().put('orderId', orderId);
		return casePage.setRedirect(true);
	}

	public String getAssignToLabel() {
		if (
			(theOrder.Mobile_Fixed__c == 'Mobile' ||
			theOrder.Ordertype__r.Support_needed__c == 'No') &&
			!theOrder.VF_Contract__r.Opportunity__r.Escalation__c
		) {
			return Label.Label_Submit_Order;
		} else {
			return Label.LABEL_Assign_To + ' ' + assignToInsideSalesLabel;
		}
	}

	public String orderformSelected {
		get;
		set {
			orderformSelected = value;
		}
	}

	public PageReference changeOrderForm() {
		PageReference newPage = page.OrderFormCustomerDetails;
		newPage.getParameters().put('orderId', orderformSelected);
		newPage.getParameters().put('contractId', contractId);
		newPage.setRedirect(true);
		return newPage;
	}

	public String orderSelected { get; set; }

	public List<SelectOption> getAvailableOrders() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('New', 'Create New Order'));
		for (Order__c orderCache : orderCaches) {
			// only orders that have not been sent to BOP can get extra items.
			if (orderCache.BOP_Order_Id__c == null) {
				options.add(
					new SelectOption(
						orderCache.Id,
						'Add to ' +
						orderCache.Name +
						' - ' +
						orderCache.Propositions__c
					)
				);
			}
		}
		return options;
	}

	public void setOrderToProcessedManually() {
		try {
			update new Order__c(Id = orderId, Status__c = ORDER_STATUS_PROCESSED_MANUALLY);
		} catch (Exception e) {
			ApexPages.addMessage(
				new ApexPages.Message(
					ApexPages.severity.ERROR,
					System.Label.ERROR_OrderForm_Cannot_Update_Order_Status +
					'<br/>' +
					e.getMessage()
				)
			);
		}
		ApexPages.addMessage(
			new ApexPages.Message(
				ApexPages.severity.CONFIRM,
				System.Label.Orderform_Set_Status_To_Processed_Manually_Success
			)
		);
	}
}