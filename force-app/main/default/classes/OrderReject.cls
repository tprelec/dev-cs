public with sharing class OrderReject {
    
    public List<Order__c> selectedOrders {get;set;}

    public OrderReject(ApexPages.StandardSetController controller) {
        controller.addFields(new LIST<String> {'Status__c'});
        selectedOrders = (List<Order__c>)controller.getSelected();
    }

    public pageReference rejectOrders(){
        return rejectOrders(selectedOrders);
    }

    public static pageReference rejectOrders(List<Order__c> orders){
        for(Order__c order : orders){
            if(order.Status__c == 'Assigned to Inside Sales'){
                order.Status__c = 'In progress';
                order.Ready_for_Inside_Sales_Datetime__c = null;
                order.Ready_for_Inside_Sales__c = false;
                order.Rejected_by_SAG__c = true;

            } else {
                ApexPages.AddMessage(New ApexPages.Message(ApexPages.Severity.ERROR,System.Label.ERROR_OrderForm_Status_Must_Be_Assigned_Rejection));
                return null;
            }
        }
        try{
            update orders;
        } catch (dmlException de){
            ApexPages.AddMessages(de);
            return null;
        }

        pageReference pr = new PageReference('/'+Order__c.SObjectType.getDescribe().getKeyPrefix());
        pr.setRedirect(true);
        return pr;
    }

    public pageReference backToList(){
        pageReference pr = new PageReference('/'+Order__c.SObjectType.getDescribe().getKeyPrefix());
        pr.setRedirect(true);
        return pr;
    }
}