/**
 * @description         This is the trigger handler for the Order_Response__c sObject. 
 * @author              Guy Clairbois
 */
public with sharing class OrderResponseTriggerHandler extends TriggerHandler {
    
    private static set<String> siasBlockingOrderResponseErrorcodes = new Set<String>{'SIAS008','SIAS101','SIAS034','SIAS035'};
    private static Map<Id, Order__c> orderMap = new Map<Id, Order__c>();

    /**
     * @description         This handles the before insert trigger event.
     */
    public override void AfterInsert(){
        List<Order_Response__c> newResponses = (List<Order_Response__c>) this.newList;
        Map<Id,Order_Response__c> newResponsesMap = (Map<Id,Order_Response__c>) this.newMap;

        processUpdates(newResponses);

    }
    

    /**
    * @description         This handles the before update trigger event.
    */
    public override void BeforeUpdate(){
        List<Order_Response__c> newResponses = (List<Order_Response__c>) this.newList;

    }


    /**
     * @description         This handles the before update trigger event.
     */
    public override void AfterUpdate(){
        List<Order_Response__c> newResponses = (List<Order_Response__c>) this.newList;   
        List<Order_Response__c> oldResponses = (List<Order_Response__c>) this.oldList;
        Map<Id,Order_Response__c> newResponsesMap = (Map<Id,Order_Response__c>) this.newMap;        
        Map<Id,Order_Response__c> oldResponsesMap = (Map<Id,Order_Response__c>) this.oldMap;
 
        doReprocessing(newResponses,oldResponsesMap);
    }


    /**
     * @description         This handles the before delete trigger event.
     */
    public override void BeforeDelete(){
        List<Order_Response__c> newResponses = (List<Order_Response__c>) this.newList;   
        List<Order_Response__c> oldResponses = (List<Order_Response__c>) this.oldList;
        Map<Id,Order_Response__c> oldResponsesMap = (Map<Id,Order_Response__c>) this.oldMap;
 

    }   

    private void processUpdates(List<Order_Response__c> newResponses){
        retrieveOrders(newResponses);
        updateUnifyContactIds(newResponses);
        updateUnifySiteIds(newResponses);
        updateUnifyCustomerIds(newResponses);
        updateBOPCustomerIds(newResponses);
        updateOrderStatus(newResponses);        
    }

    /**
     * @description         This method handles all order responses that lead to order updates.
     */
    private void updateOrderStatus(List<Order_Response__c> newResponses) {

        List<Order__c> ordersToUpdate = new List<Order__c>();
        Map<Id, Id> orderIdToResponseId = new Map<Id, Id>();
        for (Order_Response__c response : newResponses) {
            if (response.Type__c == 'StartOrder' && orderMap.get(response.Order__c).Status__c != 'Accepted') {
                if (response.Unify_Error_Number__c == 'SIAS000') {
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Unify_Customer_Export_Datetime__c = System.now();
                    o.Unify_Customer_Export_Errormessage__c = '';
                    // re-submit order (for order notification)
                    o.Status__c = 'Validation'; // locked
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c, response.Id);
                } else {
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Unify_Customer_Export_Errormessage__c = ('StartOrder failed: '+ response.Unify_Error_Number__c + '-' + response.Unify_Error_Description__c).abbreviate(255);
                    // reset order status and make order editable (to repair any errors); but only when not yet Accepted!
                    if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
                        o.Status__c = 'Clean';
                    }
                    o.Record_Locked__c = false;
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c,response.Id);
                }
            } else if (response.Type__c == 'BopOrderCreation') {
                if (response.Unify_Error_Number__c == '0') {
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.BOP_export_datetime__c = system.now();
                    o.BOP_export_Errormessage__c = '';
                    o.BOP_Order_Id__c = response.BOP_Order_Id__c;
                    ordersToUpdate.add(o);    
                    orderIdToResponseId.put(response.Order__c,response.Id);
                } else if (response.Unify_Error_Number__c == 'SIAS102') {
                    // this is the response for a mobile order
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Status__c = 'Accepted';
                    // put the description in so the end user knows that a ticket was created
                    o.Unify_Order_Errormessage__c = (response.Unify_Error_Description__c).abbreviate(255);
                    o.Unify_Order_Export_Datetime__c = system.now();                    
                    ordersToUpdate.add(o);    
                    orderIdToResponseId.put(response.Order__c,response.Id);                    
                } else {
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.BOP_export_Errormessage__c = 'BopOrderCreation failed: '+ response.Unify_Error_Number__c + '-' + response.Unify_Error_Description__c;
                    // reset order status and make order editable (to repair any errors)
                    o.Status__c = 'Refused';
                    o.Record_Locked__c = false;
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c,response.Id);
                }
            } else if(response.Type__c == 'UnifyOrderCreation' || response.Type__c == 'Order'){
                if(response.Type__c == 'UnifyOrderCreation' && response.Unify_Error_Number__c == 'SIAS000'){
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Status__c = 'Accepted';
                    o.Unify_Order_Errormessage__c = '';
                    o.Unify_Order_Export_Datetime__c = system.now();
                    ordersToUpdate.add(o);    
                    orderIdToResponseId.put(response.Order__c,response.Id);
                } else if(siasBlockingOrderResponseErrorcodes.contains(response.Unify_Error_Number__c)){
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Unify_Order_Errormessage__c = ('UnifyOrderCreation failed: '+ response.Unify_Error_Number__c + '-' + response.Unify_Error_Description__c).abbreviate(255);
                    // order is processed manually and should be blocked in SFDC
                    o.Status__c = 'Processed Manually in Unify';
                    o.Record_Locked__c = true;
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c,response.Id);                   
                } else {
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Unify_Order_Errormessage__c = 'UnifyOrderCreation failed: '+ response.Unify_Error_Number__c + '-' + response.Unify_Error_Description__c;
                    // reset order status and make order editable (to repair any errors)
                    o.Status__c = 'Refused';
                    o.Record_Locked__c = false;
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c,response.Id);
                }
            } else if (response.Type__c == 'SendMappingObject') {
                if (response.Unify_Error_Number__c == 'SIAS000') {
                    // we noticed we sometimes receive SendMappingObject without sales_order_id. Prevent this from overwriting an already present Sales_Order_Id
                    if (response.BOP_Sales_Order_Id__c != null) { 
                        Order__c o = new Order__c(Id = response.Order__c);
                        o.Sales_Order_Id__c = response.BOP_Sales_Order_Id__c;
                        o.Status__c = 'Accepted';
                        ordersToUpdate.add(o);    
                        orderIdToResponseId.put(response.Order__c,response.Id);
                    }
                } else {
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Unify_Order_Errormessage__c = ('SendMappingObject failed: '+ response.Unify_Error_Number__c + '-' + response.Unify_Error_Description__c + '. No action required. Order will be processed manually in Unify.').abbreviate(255);
                    // order status doesn't need to be reset, as order will be processed manually.
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c,response.Id);
                }                
            }          

        }
        if (!ordersToUpdate.isEmpty()) {
            updateOrders(ordersToUpdate,orderIdToResponseId);
        }
    }

    /*
     *  Description:    Utility method to support updating orders, and if it fails updating the order response result
     */
    private void updateOrders(List<Order__c> ordersToUpdate, Map<Id,Id> orderIdToResponseId){
        if(!ordersToUpdate.isEmpty()){
            List<Order_Response__c> responseToUpdate = new List<Order_Response__c>();

            Database.saveResult[] results = SharingUtils.databaseUpdateRecordsWithoutSharing(ordersToUpdate); 

            for (Integer i = 0; i < ordersToUpdate.size(); i++) {
                Database.SaveResult s = results[i];
                Order_Response__c orToUpdate = new Order_Response__c(Id = orderIdToResponseId.get(ordersToUpdate[i].Id));
                if (!s.isSuccess()) {
                    String errors = 'Error(s) occurred: ';
                    for (Database.Error err : s.getErrors()){
                        errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
                    }
                    orToUpdate.Processing_result__c = errors.abbreviate(255);
                    responseToUpdate.add(orToUpdate);                   

                } 

            }
            SharingUtils.databaseUpdateRecordsWithoutSharing(responseToUpdate); 
        }        
    }

    /**
     * @description         This method handles all order responses that lead to updates of Unify Contact Ids.
     */
    private void updateUnifyContactIds(List<Order_Response__c> newResponses){

        List<Contact> contactsToUpdate = new List<Contact>();
        Map<Id,Id> contactIdToResponseId = new Map<Id,Id>();
        List<Order__c> ordersToUpdate = new List<Order__c>();
        Map<Id,Id> orderIdToResponseId = new Map<Id,Id>();        
        for(Order_Response__c response : newResponses){
            if(response.Type__c == 'UnifyContact' || response.Type__c == 'BOPContact'){
                // Always process the Unify_Contact_Id__c if it has been returned 
                if(response.Type__c == 'UnifyContact'){
                    if(response.SFDC_Contact_Id__c != null && response.Unify_Contact_Id__c!=null){
                        Contact c = new Contact(Id = response.SFDC_Contact_Id__c);
                        c.Unify_Ref_Id__c = response.Unify_Contact_Id__c;
                        contactsToUpdate.add(c);
                        contactIdToResponseId.put(response.SFDC_Contact_Id__c,response.Id);
                    }     
                } else {
					if(response.Unify_Error_Number__c == 'SIAS000'){                	    
                        // succesful BOP contact creation. Put datetime stamp on Contact (to support later direct updates)
                        Contact c = new Contact(Id = response.SFDC_Contact_Id__c);
                        c.BOP_Export_Datetime__c = response.CreatedDate;
                        contactsToUpdate.add(c);
                        contactIdToResponseId.put(response.SFDC_Contact_Id__c,response.Id);                        
                    }
                }
                if(response.Unify_Error_Number__c != 'SIAS000'){
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Unify_Customer_Export_Errormessage__c = ('UnifyContact failed: '+ response.Unify_Error_Number__c + '-' + response.Unify_Error_Description__c).abbreviate(255);
                    // reset order status and make order editable (to repair any errors); but only when not yet Accepted!
                    if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
                        o.Status__c = 'Clean';
                    }
                    o.Record_Locked__c = false;
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c,response.Id);
                }                
            }
        }
        if(!contactsToUpdate.isEmpty()){
            List<Order_Response__c> responseToUpdate = new List<Order_Response__c>();

            Database.saveResult[] results = SharingUtils.databaseUpdateRecordsWithoutSharing(contactsToUpdate); 

            for (Integer i = 0; i < contactsToUpdate.size(); i++) {
                Database.SaveResult s = results[i];
                Order_Response__c orToUpdate = new Order_Response__c(Id = contactIdToResponseId.get(contactsToUpdate[i].Id));
                if (!s.isSuccess()) {
                    String errors = 'Error(s) occurred: ';
                    for (Database.Error err : s.getErrors()){
                        errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
                    }
                    orToUpdate.Processing_result__c = errors.abbreviate(255);
                } else {
                    orToUpdate.Processing_result__c = 'Processed';
                    
                } 
                responseToUpdate.add(orToUpdate);
            }
            SharingUtils.databaseUpdateRecordsWithoutSharing(responseToUpdate); 
        }
        if(!ordersToUpdate.isEmpty()){
            updateOrders(ordersToUpdate,orderIdToResponseId);
        }        
    }

    /**
     * @description         This method handles all order responses that lead to updates of Unify Site Ids.
     */
    private void updateUnifySiteIds(List<Order_Response__c> newResponses){
        List<Site__c> sitesToUpdate = new List<Site__c>();
        Map<Id,Id> siteIdToResponseId = new Map<Id,Id>();
        List<Order__c> ordersToUpdate = new List<Order__c>();
        Map<Id,Id> orderIdToResponseId = new Map<Id,Id>();        
        for(Order_Response__c response : newResponses){
            if(response.Type__c == 'UnifySite' || response.Type__c == 'BOPLocation' || response.Type__c == 'UnifySiteAPID'){
                if(response.SFDC_Site_Id__c != null){
                    Site__c c = new Site__c(Id = response.SFDC_Site_Id__c);
                    if (response.Unify_Site_Id__c!=null) c.Unify_Ref_Id__c = response.Unify_Site_Id__c;
                    if (response.Unify_Site_AP_Id__c!=null) c.Assigned_Product_Id__c = response.Unify_Site_AP_Id__c;
                    if (response.BOP_Location_Id__c!=null) c.BOP_Location_Id__c = response.BOP_Location_Id__c;
                    if (response.Type__c == 'BOPLocation') c.BOP_Export_datetime__c = response.createddate;
                    sitesToUpdate.add(c);
                    siteIdToResponseId.put(response.SFDC_Site_Id__c,response.Id);
                }                
                if(response.Unify_Error_Number__c != 'SIAS000'){
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Unify_Customer_Export_Errormessage__c = ('UnifySite failed: '+ response.Unify_Error_Number__c + '-' + response.Unify_Error_Description__c).abbreviate(255);
                    // reset order status and make order editable (to repair any errors); but only when not yet Accepted!
                    if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
                        o.Status__c = 'Clean';
                    }
                    o.Record_Locked__c = false;
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c,response.Id);
                }  
            }
        }
        if(!sitesToUpdate.isEmpty()){
            List<Order_Response__c> responseToUpdate = new List<Order_Response__c>();
            Database.saveResult[] results = SharingUtils.databaseUpdateRecordsWithoutSharing(sitesToUpdate); 

            for (Integer i = 0; i < sitesToUpdate.size(); i++) {
                Database.SaveResult s = results[i];
                Order_Response__c orToUpdate = new Order_Response__c(Id = siteIdToResponseId.get(sitesToUpdate[i].Id));
                if (!s.isSuccess()) {
                    String errors = 'Error(s) occurred: ';
                    for (Database.Error err : s.getErrors()){
                        errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
                    }
                    orToUpdate.Processing_result__c = errors.abbreviate(255);
                } else {
                    orToUpdate.Processing_result__c = 'Processed';
                    
                } 
                responseToUpdate.add(orToUpdate);
            }
            SharingUtils.databaseUpdateRecordsWithoutSharing(responseToUpdate); 

        }
        if(!ordersToUpdate.isEmpty()){
            updateOrders(ordersToUpdate,orderIdToResponseId);
        }        
    }

    /**
     * @description         This method handles all order responses that lead to updates of Unify Account/Customer Ids.
     */
    private void updateUnifyCustomerIds(List<Order_Response__c> newResponses){
        List<Order__c> ordersToUpdate = new List<Order__c>();
        Map<Id,Id> orderIdToResponseId = new Map<Id,Id>();        
        List<Account> accountsToUpdate = new List<Account>();
        List<Ban__c> bansToUpdate = new List<Ban__c>();
        List<Financial_Account__c> fasToUpdate = new List<Financial_Account__c>();
        List<Billing_Arrangement__c> barsToUpdate = new List<Billing_Arrangement__c>();
        Map<Id,Order_Response__c> responsesToUpdate = new Map<Id,Order_Response__c>();
        Map<Id,Id> accountIdToResponseId = new Map<Id,Id>();
        Map<Id,Id> banIdToResponseId = new Map<Id,Id>();
        Map<Id,Id> faIdToResponseId = new Map<Id,Id>();
        Map<Id,Id> barIdToResponseId = new Map<Id,Id>();
        Set<Id> responseIds = new Set<Id>();
        for(Order_Response__c response : newResponses){
            if(response.Type__c == 'UnifyAccount'){
                responseIds.add(response.Id);
                // When there is an error always update the order with the failure
                if(response.Unify_Error_Number__c != 'SIAS000'){
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Unify_Customer_Export_Errormessage__c = ('UnifyAccount failed: '+ response.Unify_Error_Number__c + '-' + response.Unify_Error_Description__c).abbreviate(255);
                    // reset order status and make order editable (to repair any errors); but only when not yet Accepted!
                    if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
                        o.Status__c = 'Clean';
                    }
                    o.Record_Locked__c = false;
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c,response.Id);
                }                      
            }
        }
        if(!responseIds.isEmpty()){
            // requery responses to get full info on fa, bar, ban, account
            for(Order_Response__c response : [Select Id, 
                                            Order__r.Billing_Arrangement__c, 
                                            Order__r.Billing_Arrangement__r.Financial_Account__c,
                                            Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__c,
                                            Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Account__c,
                                            Unify_Account_Id__c,
                                            Unify_Billing_Customer_Id__c,
                                            BAN_Corporate_Id__c,
                                            Unify_Financial_Account_Id__c,
                                            Unify_Billing_Arrangement_Id__c
                                        From
                                            Order_Response__c
                                        Where 
                                            Id in :responseIds]){

                // Process any ID's returned from Unify (it does not matter if there was an overall error or not)
                if (response.Unify_Account_Id__c!=null) {
                    accountsToUpdate.add(new Account(
                                            Id = response.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Account__c, 
                                            Unify_Ref_Id__c = response.Unify_Account_Id__c));
                    accountIdToResponseId.put(response.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Account__c,response.Id);
                }

                if (response.Unify_Billing_Customer_Id__c != null) {
                    bansToUpdate.add(
                        new Ban__c( 
                            Id = response.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__c, 
                            Name = response.Unify_Billing_Customer_Id__c, 
                            BAN_Status__c = 'Opened',
                            Unify_Ref_Id__c = response.Unify_Billing_Customer_Id__c,
                            Corporate_Id__c = response.BAN_Corporate_Id__c
                        )
                    );
                    banIdToResponseId.put(response.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__c, response.Id);
                }

                if(response.Unify_Financial_Account_Id__c != null){
                    fasToUpdate.add(new Financial_Account__c(
                                            Id = response.Order__r.Billing_Arrangement__r.Financial_Account__c,
                                            Unify_Ref_Id__c = response.Unify_Financial_Account_Id__c));
                    faIdToResponseId.put(response.Order__r.Billing_Arrangement__r.Financial_Account__c,response.Id);
                }

                if(response.Unify_Billing_Arrangement_Id__c != null){
                    barsToUpdate.add(new Billing_Arrangement__c(
                                            Id = response.Order__r.Billing_Arrangement__c,
                                            Unify_Ref_Id__c = response.Unify_Billing_Arrangement_Id__c));
                    barIdToResponseId.put(response.Order__r.Billing_Arrangement__c,response.Id);
                }

                response.Processing_result__c = '';
                responsesToUpdate.put(response.Id,response);

            }

            
            if(!accountsToUpdate.isEmpty()){
                // try updating all objects
                Database.saveResult[] accountResults = SharingUtils.databaseUpdateRecordsWithoutSharing(accountsToUpdate); 

                for (Integer i = 0; i < accountsToUpdate.size(); i++) {
                    Database.SaveResult s = accountResults[i];
                    Order_Response__c orToUpdate = responsesToUpdate.get(accountIdToResponseId.get(accountsToUpdate[i].Id));
                    if (!s.isSuccess()) {
                        String errors = orToUpdate.Processing_result__c + 'Error(s) occurred while updating Account: ';
                        for (Database.Error err : s.getErrors()){
                            errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
                        }
                        orToUpdate.Processing_result__c = errors.abbreviate(255);
                    } 
                    responsesToUpdate.put(orToUpdate.Id,orToUpdate);
                }
            }

            if (!bansToUpdate.isEmpty()) {
                Database.saveResult[] banResults = SharingUtils.databaseUpdateRecordsWithoutSharing(bansToUpdate); 
                for (Integer i = 0; i < bansToUpdate.size(); i++) {
                    Database.SaveResult s = banResults[i];
                    Order_Response__c orToUpdate = responsesToUpdate.get(banIdToResponseId.get(bansToUpdate[i].Id));
                    if (!s.isSuccess()) {
                        String errors = orToUpdate.Processing_result__c + 'Error(s) occurred while updating Ban: ';
                        for (Database.Error err : s.getErrors()){
                            errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
                        }
                        orToUpdate.Processing_result__c = errors.abbreviate(255);
                    } 
                    responsesToUpdate.put(orToUpdate.Id,orToUpdate);
                }
            }

            if (!fasToUpdate.isEmpty()) {
                Database.saveResult[] faResults = SharingUtils.databaseUpdateRecordsWithoutSharing(fasToUpdate); 
                for (Integer i = 0; i < fasToUpdate.size(); i++) {
                    Database.SaveResult s = faResults[i];
                    Order_Response__c orToUpdate = responsesToUpdate.get(faIdToResponseId.get(fasToUpdate[i].Id));
                    if (!s.isSuccess()) {
                        String errors = orToUpdate.Processing_result__c + 'Error(s) occurred while updating Financial Account: ';
                        for (Database.Error err : s.getErrors()){
                            errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
                        }
                        orToUpdate.Processing_result__c = errors.abbreviate(255);
                    } 
                    responsesToUpdate.put(orToUpdate.Id,orToUpdate);
                }
            }

            if (!barsToUpdate.isEmpty()) {
                Database.saveResult[] barResults = SharingUtils.databaseUpdateRecordsWithoutSharing(barsToUpdate); 
                for (Integer i = 0; i < barsToUpdate.size(); i++) {
                    Database.SaveResult s = barResults[i];
                    Order_Response__c orToUpdate = responsesToUpdate.get(barIdToResponseId.get(barsToUpdate[i].Id));
                    if (!s.isSuccess()) {
                        String errors = orToUpdate.Processing_result__c + 'Error(s) occurred while updating Billing Arrangement: ';
                        for (Database.Error err : s.getErrors()){
                            errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
                        }
                        orToUpdate.Processing_result__c = errors.abbreviate(255);
                    } 
                    responsesToUpdate.put(orToUpdate.Id,orToUpdate);
                }         
            }              

            if (!responsesToUpdate.isEmpty()) {
                // lastly, put ok for all responses that have no errors
                for(Id responseId : responsesToUpdate.keySet()) {
                    if(responsesToUpdate.get(responseId).Processing_result__c == ''){
                        Order_Response__c response = responsesToUpdate.get(responseId);
                        response.Processing_result__c = 'Processed';
                        responsesToUpdate.put(responseId,response);
                    }
                }
                SharingUtils.databaseUpdateRecordsWithoutSharing(responsesToUpdate.values()); 
            }

        }

        if(!ordersToUpdate.isEmpty()){
            updateOrders(ordersToUpdate,orderIdToResponseId);
        }        
    }

    /**
     * @description         This method handles all order responses that lead to updates of BOP Customer Ids.
     * JvW 26-07-2019: Replaced all references to BAN with Account references
     */
    private void updateBOPCustomerIds(List<Order_Response__c> newResponses){
        List<Order__c> ordersToUpdate = new List<Order__c>();
        Map<Id,Id> orderIdToResponseId = new Map<Id,Id>();
        List<Account> accntsToUpdate = new List<Account>();
        Map<Id,Order_Response__c> responsesToUpdate = new Map<Id,Order_Response__c>();
        Map<Id,Id> accIdToResponseId = new Map<Id,Id>();
        Set<Id> responseIds = new Set<Id>();
        for(Order_Response__c response : newResponses){
            if(response.Type__c == 'BOPCompany'){
                responseIds.add(response.Id);                
                if(response.Unify_Error_Number__c != 'SIAS000'){
                    Order__c o = new Order__c(Id = response.Order__c);
                    o.Unify_Customer_Export_Errormessage__c = ('BOPCompany failed: '+ response.Unify_Error_Number__c + '-' + response.Unify_Error_Description__c).abbreviate(255);
                    // reset order status and make order editable (to repair any errors); but only when not yet Accepted!
                    if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
                        o.Status__c = 'Clean';
                    }
                    o.Record_Locked__c = false;
                    ordersToUpdate.add(o);
                    orderIdToResponseId.put(response.Order__c,response.Id);
                }     
            }
        }
        if(!responseIds.isEmpty()){
            // requery responses to get full info on fa, bar, ban, account
            for(Order_Response__c response : [Select Id, 
                                            Order__r.Account__c,
                                            BAN_BopCode__c,
                                            CreatedDate
                                        From
                                            Order_Response__c
                                        Where 
                                            Id in :responseIds]){

                if (response.BAN_BopCode__c!=null) {
                    accntsToUpdate.add(new Account( 
                                            Id = response.Order__r.Account__c, 
                                            BOPCode__c = response.BAN_BopCode__c,
                    						BOP_Export_Datetime__c = response.CreatedDate));
                    accIdToResponseId.put(response.Order__r.Account__c,response.Id);
                    response.Processing_result__c = '';
                    responsesToUpdate.put(response.Id,response);
                }
            }

            
            if(!accntsToUpdate.isEmpty()){
                // try updating all objects
                Database.saveResult[] accResults = SharingUtils.databaseUpdateRecordsWithoutSharing(accntsToUpdate); 
                for (Integer i = 0; i < accntsToUpdate.size(); i++) {
                    Database.SaveResult s = accResults[i];
                    Order_Response__c orToUpdate = responsesToUpdate.get(accIdToResponseId.get(accntsToUpdate[i].Id));
                    if (!s.isSuccess()) {
                        String errors = orToUpdate.Processing_result__c + 'Error(s) occurred while updating Account: ';
                        for (Database.Error err : s.getErrors()){
                            errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
                        }
                        orToUpdate.Processing_result__c = errors.abbreviate(255);
                    } 
                    responsesToUpdate.put(orToUpdate.Id,orToUpdate);
                }

                // lastly, put ok for all responses that have no errors
                for(Id responseId : responsesToUpdate.keySet()) {
                    if(responsesToUpdate.get(responseId).Processing_result__c == ''){
                        Order_Response__c response = responsesToUpdate.get(responseId);
                        response.Processing_result__c = 'Processed';
                        responsesToUpdate.put(responseId,response);
                    }
                }
                SharingUtils.databaseUpdateRecordsWithoutSharing(responsesToUpdate.values()); 
            }
        }
        if(!ordersToUpdate.isEmpty()){
            updateOrders(ordersToUpdate,orderIdToResponseId);
        }        
    }

    /**
     * @description         This method retrieves all orders corresponding to the order responses.
     */
    private void retrieveOrders(List<Order_Response__c> newResponses){
        Set<Id> orderIds = new Set<Id>(); 
        for (Order_Response__c resp : newResponses) {
            orderIds.add(resp.Order__c);
        }
        // Just to make sure the map is empty
        orderMap.clear();
        if (!orderIds.isEmpty()) {
            orderMap.putAll([SELECT Id, Status__c FROM Order__c WHERE Id IN :orderIds]);
        }
    }    

    /**
     * @description         This method allows for reprocessing of an order respons by removing the value from the Processing result field.
     */
    private void doReprocessing(List<Order_Response__c> newResponses,Map<Id,Order_Response__c> oldResponsesMap){

        List<Order_Response__c> orToReprocess = new List<Order_Response__c>();

        for(Order_Response__c o : newResponses){
            if(o.Processing_result__c == null && oldResponsesMap.get(o.Id).Processing_result__c != null){
                orToReprocess.add(o);
            }
        }

        processUpdates(orToReprocess);
    }

}