public with sharing class OrderService {

    public static String getOpportunityNumberWithOrderId(String orderId){
        Order__c theOrder = [SELECT Id, VF_Contract__r.Opportunity__r.Opportunity_Number__c FROM Order__c WHERE Name =: orderId];

        return theOrder.VF_Contract__r.Opportunity__r.Opportunity_Number__c;
    }
}