public with sharing class OrderSiasButtonController
{
    private ApexPages.StandardController standardController;
     private Id recordId;
 
    public OrderSiasButtonController(ApexPages.StandardController standardController){
        this.standardController = standardController;
        recordId = standardController.getId();
    }
 
    public PageReference sendCustomerNotification(){
        CustomerExportSias.sendCustomerNotification(recordId);
        return null;
    }
    
    public PageReference sendOrderNotification(){
        OrderExportSias.sendOrderNotification(recordId);        
        return null;
    }    
    
    public PageReference returnToOrder(){
        return new PageReference('/'+recordId);
    }
}