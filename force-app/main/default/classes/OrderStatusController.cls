public with sharing class OrderStatusController {
	public Order__c order {get;set;}
    public List<Order__c> selectedOrders {get;set;}

	public OrderStatusController(ApexPages.StandardController controller) {
		Id orderId = controller.getId();
		order = [Select Id, Order_Export_Sequence_Number__c, Status__c, Name, Export__c From Order__c Where Id = :orderId];
	}

    public OrderStatusController(ApexPages.StandardSetController setcontroller) {
        setcontroller.addFields(new LIST<String> {'Status__c'});
        selectedOrders = (List<Order__c>)setcontroller.getSelected();
    }

	public PageReference confirmReset(){
		if(order.Status__c == 'Accepted' && order.Export__c == 'BOP'){
			Order__c o = new Order__c(Id=order.Id);
			o.Status__c = 'Clean';
			if(order.Order_Export_Sequence_Number__c == null){
				o.Order_Export_Sequence_Number__c = '2';
			} else {
				o.Order_Export_Sequence_Number__c = String.valueOf(Integer.valueOf(order.Order_Export_Sequence_Number__c) + 1);
			}
			o.BOP_Order_Id__c = null;
			o.Sales_Order_Id__c = null;
			o.BOP_export_datetime__c = null;
			o.BOP_export_Errormessage__c = null;
			o.Record_Locked__c = false;

			update o;

			return new PageReference('/'+order.Id);

		} else {
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'Only non-SIAS orders with status Accepted can be reset. If you want to reset SIAS orders contact SFDC support, as this needs to be done manually.'));
			return null;
		}
	}


    public pageReference processedManuallyOrders(){
        return processedManuallyOrders(selectedOrders);
    }

    public static pageReference processedManuallyOrders(List<Order__c> orders){
        for(Order__c order : orders){
            if(order.Status__c != 'Accepted'){
                order.Status__c = 'Processed Manually';
            } else {
                ApexPages.AddMessage(New ApexPages.Message(ApexPages.Severity.ERROR,'Orders that were already accepted by BOP cannot be set to Processed Manually'));
                return null;
            }
        }
        try{
            update orders;
        } catch (dmlException de){
            ApexPages.AddMessages(de);
            return null;
        }

        pageReference pr = new PageReference('/'+Order__c.SObjectType.getDescribe().getKeyPrefix());
        pr.setRedirect(true);
        return pr;
    }	

    public pageReference backToList(){
        pageReference pr = new PageReference('/'+Order__c.SObjectType.getDescribe().getKeyPrefix());
        pr.setRedirect(true);
        return pr;
    }

	public PageReference cancel(){

		return new PageReference('/'+order.Id);
	}	
}