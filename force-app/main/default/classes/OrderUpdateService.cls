/**
 * @description			This class contains webservices methods for updating Orders from external systems
 * @author				Guy Clairbois
 */
global class OrderUpdateService {
	
    // method to enable external system to update an Order by referenceId (Order name)
    webService static orderUpdateResult[] updateOrders(orderUpdate[] ordersToUpdate) {
    	List<orderUpdateResult> updateResult = new List<orderUpdateResult>();
    	List<Order__c> sfOrdersToUpdate = new List<Order__c>();
    	Map<String,Order__c> refIdToOrder = new Map<String,Order__c>();

        // referenceId coming from BOP is the BOP_Export_Order_Id
        // first collect those in order to create an order query
        for(orderUpdate ou : ordersToUpdate){
            refIdToOrder.put(ou.referenceId,null);
        }        

        for(Order__c o : [Select Id, BOP_Export_Order_Id__c From Order__c Where BOP_Export_Order_Id__c in :refIdToOrder.keySet()]){
            refIdToOrder.put(o.BOP_Export_Order_Id__c,o);
        }

    	for(orderUpdate ou : ordersToUpdate){
    		if(refIdToOrder.containsKey(ou.referenceId)){
        		Order__c o = refIdToOrder.get(ou.referenceId);
                o.BOP_Order_Status__c = ou.orderStatus;
        		o.BOP_Project_Number__c = ou.projectNumber;
        		sfOrdersToUpdate.add(o);
            }
    	}
    	
    	Integer counter = 0;
    	for(Database.Saveresult sr : Database.update(sfOrdersToUpdate,false)){
    		orderUpdateResult our = new orderUpdateResult();
    		if(sr.isSuccess()){
    			our.errorCode = 0;
    			our.errorMessage = '';
    			our.referenceId = sr.getId();
    			our.orderId = ordersToUpdate[counter].orderId;
    		} else {
    			our.errorCode = 1;
    			our.errorMessage = sr.getErrors()[0].getMessage();
    			our.referenceId = ordersToUpdate[counter].referenceId;
    			our.orderId = ordersToUpdate[counter].orderId;
    		}
    		updateResult.add(our);
    		counter++;
    	}
    	return updateResult;
    }
    
    // method to enable external system to update an Order by referenceId (Order name)
    webService static orderLineItemUpdateResult[] updateLineItemOrders(orderLineItemUpdate[] ordersLineItemToUpdate) {
    	List<orderLineItemUpdateResult> updateLineItemResult = new List<orderLineItemUpdateResult>();
    	List<Order_update__c> sfLineItemToInsert = new List<Order_update__c>();

        Map<String,Id> orderNameToId = new Map<String,Id>();
        for(orderLineItemUpdate oliu : ordersLineItemToUpdate){
            String refId;
            if(oliu.referenceId.length() != 15 && oliu.referenceId.length() != 18){
                orderNameToId.put(oliu.referenceId,null);
            }
        }
        // query order id's if names are provided
        if(!orderNameToId.isEmpty()){
            for(Order__c o : [Select Id,Name From Order__c Where Name in :orderNameToId.keySet()]){
                orderNameToId.put(o.Name,o.Id);
            }
        }
    	
    	for(orderLineItemUpdate oliu : ordersLineItemToUpdate){
    		String refId;
    		if(oliu.referenceId.length() != 15 && oliu.referenceId.length() != 18){
                if(orderNameToId.containsKey(oliu.referenceId)){
                    refId = orderNameToId.get(oliu.referenceId);    
                }
    			
    		} else {
    			refId = oliu.referenceId;
    		}

    		Order_update__c ou = new Order_update__c(Order__c = refId);
    		ou.Field_name__c = oliu.fieldName;
    		ou.Old_value__c = oliu.oldValue;
    		ou.New_value__c = oliu.newValue;
    		sfLineItemToInsert.add(ou);
    	}
    	
    	Integer counter = 0;
    	for(Database.Saveresult sr : Database.insert(sfLineItemToInsert,false)){
    		orderLineItemUpdateResult our = new orderLineItemUpdateResult();
    		if(sr.isSuccess()){
    			our.errorCode = 0;
    			our.errorMessage = '';
    			our.referenceId = ordersLineItemToUpdate[counter].referenceId;
    			our.orderId = ordersLineItemToUpdate[counter].orderId;
    		} else {
    			our.errorCode = 1;
    			our.errorMessage = sr.getErrors()[0].getMessage();
    			our.referenceId = ordersLineItemToUpdate[counter].referenceId;
    			our.orderId = ordersLineItemToUpdate[counter].orderId;
    		}
    		updateLineItemResult.add(our);
    		counter++;
    	}
    	return updateLineItemResult;
    }
	
	// helper class for providing order details
    global class orderUpdate {
        webservice String referenceId;
        webservice String orderId;
        webservice String projectNumber;
        webservice String projectManagerName;
        webservice String projectManagerEmail;
        webservice String projectManagerPhone;
        webservice String orderStatus; 		
	}

	// helper class for returning order update results	
	global class orderUpdateResult {
        webservice String referenceId;
        webservice String orderId;
        webservice Integer errorCode;
        webservice String errorMessage;		
	}
	
	// helper class for providing order line item details
    global class orderLineItemUpdate {
        webservice String referenceId;        
        webservice String fieldName;
        webservice String oldValue;
        webservice String newValue;
        webservice String orderId;       	
	}
	
	// helper class for returning order update results	
	global class orderLineItemUpdateResult {
        webservice String referenceId;
        webservice String orderId;
        webservice Integer errorCode;
        webservice String errorMessage;		
	}
}