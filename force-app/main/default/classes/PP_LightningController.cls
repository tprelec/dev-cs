/**
 * This class is used as controller extension in Visualforce Pages
 * that need to be adjusted for Lightning Experience or Lightning Community.
 *
 * @test PP_LightningControllerTest
 */
public with sharing class PP_LightningController
{
    public PP_LightningController(ApexPages.StandardController stdController) {}

    public PP_LightningController(ApexPages.StandardSetController stdController) {}

    public PP_LightningController(TrainingRegistrationController controller) {}

    public PP_LightningController(PartnerInfo controller) {}

    public PP_LightningController(OrderFormCustomerDataController controller) {}

    public PP_LightningController(OpportunityAttachmentManagerController controller) {}

    public PP_LightningController(OrderFormTemplateController controller) {}

    public PP_LightningController(OrderFormAddProductDataController controller) {}

    public PP_LightningController(OrderFormBillingDataController controller) {}

    public PP_LightningController(OrderFormCTNDetailsController controller) {}

    public PP_LightningController(OrderFormDeliveryDataController controller) {}

    public PP_LightningController(OrderFormNumberPortingDataController controller) {}

    public PP_LightningController(OrderFormOrderDataController controller) {}

    public PP_LightningController(OrderFormOrderProgressController controller) {}


    public PP_LightningController(OrderFormPhonebookRegDataController controller) {}


    /**
     * Return true if User is in Lightning Experience.
     */
    public Boolean getIsLEX()
    {
        return isLEX();
    }


    public static Boolean isLEX()
    {
        String currentTheme = UserInfo.getUiThemeDisplayed() == null ? '' : UserInfo.getUiThemeDisplayed();
        return currentTheme.equals('Theme4d');
    }


    /**
     * Return true if User is in Lightning Community.
     */
    public Boolean getIsLightningCommunity()
    {
        return isLightningCommunity();
    }


    public static Boolean isLightningCommunity()
    {
        String siteName = Site.getName() == null ? '' : Site.getName();
        return TestUtils.isPartnerCommunity ? true : siteName.equals(Label.LABEL_Lightning_Partner_CommunityName);
    }
}