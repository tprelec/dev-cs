@IsTest
private class PP_SearchAccountControllerTest
{
    @IsTest
    static void createAccountSharing_test()
    {
        Account partnerAccount = TestUtils.createPartnerAccount();
        Contact con = TestUtils.portalContact(partnerAccount);
        User portalUser = TestUtils.createPortalUser(partnerAccount);
        //System.debug(':::ut = ' + portalUser.UserType);
        update con; // magic: System.AsyncException: Future method cannot be called from a future or batch method: AccountExport.exportAccountsOffline(Set<Id>)...

        portalUser = PP_TestUtils.getUser(portalUser.Id);
        //System.assertEquals('PowerPartner', portalUser.UserType);

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);

        System.runAs(portalUser)
        {
            PP_SearchAccountController.createAccountSharing(acct.Id);
        }
    }


    @IsTest
    static void searchAccountServer_test()
    {
        Account partnerAccount = TestUtils.createPartnerAccount();
        Contact con = TestUtils.portalContact(partnerAccount);
        User portalUser = TestUtils.createPortalUser(partnerAccount);
        update con; // magic: System.AsyncException: Future method cannot be called from a future or batch method: AccountExport.exportAccountsOffline(Set<Id>)...

        User owner = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '12345678';
        acct.BAN_Number__c = '123456789';
        insert acct;

        PP_SearchAccountController.SearchAccountParams params =
                new PP_SearchAccountController.SearchAccountParams();

        params.accountName = acct.Name;
        params.banNumber = '123456789';
        params.searchOffset = 0;

        System.runAs(portalUser)
        {
            PP_SearchAccountController.SearchAccountResult result =
                    PP_SearchAccountController.searchAccountsServer( JSON.serialize(params) );
            System.assertEquals(1, result.searchResults.size());
            System.assertEquals(acct.Id, result.searchResults.get(0).account.Id);
        }


        params.accountName = acct.Name;
        params.kvkNumber = '12345678';
        params.zipCode = '1234AB';

        System.runAs(portalUser)
        {
            PP_SearchAccountController.SearchAccountResult result =
                    PP_SearchAccountController.searchAccountsServer( JSON.serialize(params) );
            System.assertEquals(1, result.searchResults.size());
            System.assertEquals(acct.Id, result.searchResults.get(0).account.Id);
        }

    }


    @IsTest
    static void searchAccountServer_createAccount_test()
    {
        Account partnerAccount = TestUtils.createPartnerAccount();
        Contact con = TestUtils.portalContact(partnerAccount);
        User portalUser = TestUtils.createPortalUser(partnerAccount);
        update con; // magic: System.AsyncException: Future method cannot be called from a future or batch method: AccountExport.exportAccountsOffline(Set<Id>)...

        //User owner = TestUtils.createAdministrator();
        //Account acct = TestUtils.createAccount(owner);

        PP_SearchAccountController.SearchAccountParams params =
                new PP_SearchAccountController.SearchAccountParams();

        params.accountName = 'new Account';
        params.kvkNumber = '12345678';

        System.runAs(portalUser)
        {
            PP_SearchAccountController.SearchAccountResult result =
                    PP_SearchAccountController.searchAccountsServer( JSON.serialize(params) );
            //System.debug(':::result = ' + result);
            //System.assertEquals(1, result.searchResults.size());
            //System.assertEquals(acct.Id, result.searchResults.get(0).account.Id);
        }
    }


    @IsTest
    static void searchAccount_invalidParams_test()
    {
        Account partnerAccount = TestUtils.createPartnerAccount();
        Contact con = TestUtils.portalContact(partnerAccount);
        User portalUser = TestUtils.createPortalUser(partnerAccount);
        update con; // magic: System.AsyncException: Future method cannot be called from a future or batch method: AccountExport.exportAccountsOffline(Set<Id>)...

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);

        PP_SearchAccountController.SearchAccountParams params =
                new PP_SearchAccountController.SearchAccountParams();
        params.accountName = '1';

        System.runAs(portalUser)
        {
            PP_SearchAccountController.SearchAccountResult result =
                    PP_SearchAccountController.searchAccountsServer( JSON.serialize(params) );
            System.assertEquals(false, result.success);
        }


        params.accountName = '123';
        params.zipCode = 'invalidzip';
        params.banNumber = 'invalidBan';
        params.kvkNumber = 'invalidKvkNumber';

        System.runAs(portalUser)
        {
            PP_SearchAccountController.SearchAccountResult result =
                    PP_SearchAccountController.searchAccountsServer( JSON.serialize(params) );
            System.assertEquals(false, result.success);
        }
    }
}