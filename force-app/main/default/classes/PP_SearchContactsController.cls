public with sharing class PP_SearchContactsController
{
    private static final Integer QUERY_LIMIT = 15;


    @AuraEnabled
    public static List<Contact> getContacts(String accountId, String offsetString)
    {
        Integer offset = 0;
        try
        {
            offset = Integer.valueOf(offsetString);
        }
        catch (Exception exc)
        {
            System.debug(LoggingLevel.ERROR, exc);
            offset = 0;
        }

        List<Contact> contacts = [
                SELECT Id, FirstName, LastName, Name, Title, Email, Phone, MobilePhone,
                       Email_or_Mobile_Phone_Missing__c, Authorized_to_Sign_1st__c
                FROM Contact
                WHERE AccountId = :accountId
                ORDER BY LastName
                LIMIT :QUERY_LIMIT
                OFFSET :offset
        ];
        return contacts;
    }


    @AuraEnabled
    public static Contact getContact(String contactId)
    {
        Contact contact = [
                SELECT Id, FirstName, LastName, Name, Title, Email, Phone, MobilePhone,
                       Email_or_Mobile_Phone_Missing__c, Authorized_to_Sign_1st__c
                FROM Contact
                WHERE Id = :contactId
        ];
        return contact;
    }


    @AuraEnabled
    public static AuraActionResult authorizeToSignFirst(String accountId, String contactId)
    {
        try
        {
            //1. Check that contact belongs to account
            List<Contact> contacts = [
                    SELECT Id, AccountId
                    FROM Contact
                    WHERE Id = :contactId AND AccountId = :accountId
            ];

            if (contacts.size() != 1)
            {
                return new AuraActionResult(false,
                        new AuraMessage('Unexpected error', AuraMessageSeverity.ERROR));
            }

            //2. Update Account
            update new Account(
                    Id = contacts.get(0).AccountId,
                    Authorized_to_sign_1st__c = contacts.get(0).Id
            );

            return new AuraActionResult(true);
        }
        catch (Exception exc)
        {
            return new AuraActionResult(false,
                    new AuraMessage(exc.getMessage(), AuraMessageSeverity.ERROR));
        }
    }


    /*
    @AuraEnabled
    public static AuraActionResult deleteContact(String contactId)
    {
        try
        {
            if (!Schema.sObjectType.Contact.isDeletable()) {
                return new AuraActionResult(false,
                        new AuraMessage('You don\'t have rights to delete this record', AuraMessageSeverity.ERROR));
            }

            delete new Contact(Id = contactId);
        }
        catch( Exception exc )
        {
            return new AuraActionResult(false,
                    new AuraMessage(exc.getMessage(), AuraMessageSeverity.ERROR));
        }

        return new AuraActionResult(true,
                new AuraMessage('Contact has been deleted', AuraMessageSeverity.INFO));
    }
    */
}