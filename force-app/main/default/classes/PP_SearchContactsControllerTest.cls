@IsTest
private class PP_SearchContactsControllerTest
{
    @TestSetup
    static void setup()
    {
        User portalUser = PP_TestUtils.createPortalUser();

        User owner = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;
        Account acct = TestUtils.createAccount(owner);
        acct.Name = 'CustomerTest';
        insert acct;

        List<Contact> contacts = new List<Contact> {
                new Contact(
                        AccountId = acct.Id,
                        LastName = 'Last',
                        FirstName = 'First'
                ),
                new Contact(
                        AccountId = acct.Id,
                        LastName = 'Last2',
                        FirstName = 'First2'
                )
        };
        insert contacts;

        BanManagerData.createAccountSharing(acct.Id, portalUser.Id);
    }


    @IsTest
    static void getContact_test()
    {
        User portalUser = PP_TestUtils.getPortalUser();
        Contact cont = [ SELECT Id, LastName FROM Contact WHERE LastName = 'Last' ];

        System.runAs(portalUser)
        {
            Contact result = PP_SearchContactsController.getContact(cont.Id);
            System.assertEquals(cont.LastName, result.LastName);
        }
    }


    @IsTest
    static void getContacts_test()
    {
        User portalUser = PP_TestUtils.getPortalUser();
        Account acct = [ SELECT Id FROM Account WHERE Name = 'CustomerTest' ];

        System.runAs(portalUser)
        {
            Test.startTest();
            List<Contact> result = PP_SearchContactsController.getContacts(acct.Id, 'abracadabra');
            System.assertEquals(2, result.size());

            result = PP_SearchContactsController.getContacts(acct.Id, '1');
            System.assertEquals(1, result.size());
            Test.stopTest();
        }
    }


    @IsTest
    static void authorizeToSignFirst_test()
    {
        User portalUser = PP_TestUtils.getPortalUser();
        Account acct = [ SELECT Id, Authorized_to_sign_1st__c FROM Account WHERE Name = 'CustomerTest' ];
        System.assertEquals(null, acct.Authorized_to_sign_1st__c, 'Authorized_to_sign_1st__c should not be set yet');

        System.runAs(portalUser)
        {
            Test.startTest();
            List<Contact> contacts = PP_SearchContactsController.getContacts(acct.Id, '0');
            AuraActionResult result =
                    PP_SearchContactsController.authorizeToSignFirst(acct.Id, contacts.get(0).Id);
            System.assertEquals(true, result.success);

            acct = [ SELECT Id, Authorized_to_sign_1st__c FROM Account WHERE Name = 'CustomerTest' ];
            System.assertEquals(contacts.get(0).Id, acct.Authorized_to_sign_1st__c,
                    'Authorized_to_sign_1st__c should be updated');


            result = PP_SearchContactsController.authorizeToSignFirst('invalid id', contacts.get(0).Id);
            System.assertEquals(false, result.success);
            Test.stopTest();
        }
    }
}