/**
 * TODO: consider sharing
 */
public without sharing class PP_SearchSitesController
{
    /*
    @AuraEnabled
    public static List<Site__c> getAccountSites(String accountId)
    {
        List<Site__c> sites = [
                SELECT Id, Name, Site_Account__c, Location_Type__c,
                       Site_Street__c, Site_City__c, Site_House_Number__c, Site_House_Number_Suffix__c,
                       Site_Employees__c, Site_Postal_Code__c
                FROM Site__c
                WHERE Site_Account__c = :accountId
        ];

        return sites;
    }
    */


    @AuraEnabled
    public static Boolean checkAccountSitesExist(String accountId)
    {
        List<AggregateResult> aggrResults = [
                SELECT COUNT(Id) COUNTER
                FROM Site__c
                WHERE Site_Account__c = :accountId
        ];

        return (Integer)aggrResults.get(0).get('COUNTER') > 0;
    }
}