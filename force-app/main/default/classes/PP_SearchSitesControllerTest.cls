@IsTest
private class PP_SearchSitesControllerTest
{
    @IsTest
    static void checkAccountSitesExist_test()
    {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        TestUtils.createSite(acct);

        System.assertEquals(true, PP_SearchSitesController.checkAccountSitesExist(acct.Id));
    }
}