@IsTest
public class PP_TestUtils
{
    public static User getUser(Id userId)
    {
        return [
                SELECT Id, FirstName, LastName, Alias, Email, Username, CommunityNickname,
                       TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId,
                       LanguageLocaleKey, ContactId, Dealercode__c, UserRoleId, UserType
                  FROM User
                 WHERE Id = :userId
        ];
    }


    public static User getPortalUser()
    {
        return [
                SELECT Id, FirstName, LastName, Alias, Email, Username, CommunityNickname,
                        TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId,
                        LanguageLocaleKey, ContactId, Dealercode__c, UserRoleId, UserType
                FROM User
                WHERE Profile.Name = 'VF Partner Portal User' AND LastName = 'PortalUser'
        ];
    }


    public static User createPortalUser()
    {
        Account partnerAccount = TestUtils.createPartnerAccount();
        Contact con = TestUtils.portalContact(partnerAccount);
        User portalUser = TestUtils.createPortalUser(partnerAccount);

        // magic to fix "System.AsyncException: Future method cannot be called from a future or batch method: AccountExport.exportAccountsOffline(Set<Id>)..."
        update con;

        // In order to access UserType
        return getPortalUser();
    }
}