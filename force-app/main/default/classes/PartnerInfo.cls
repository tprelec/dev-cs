/*  
  @description         Query information that is accessible on the partner home VF page
  @author              Sander Stulemeijer
  @editor              Guy Clairbois
  
Note Sander 17-2-2014:
When no query result on any, VF page crashes. Should be prevented.

*/
public without sharing class PartnerInfo{

    //declare variables to return query results
    Account AccPartner;
    User uPartner;
    User uOPAM;
    User uSolSales;
    User uChanMgr;
    User uHotLine;

    public PartnerInfo() {
     
      //query the account ID the partner user is related to
      uPartner = [Select Id, AccountId, Name, SmallPhotoURL,UserType From User Where Id = :UserInfo.getUserId()];
      if(!GeneralUtils.isPartnerUser(uPartner)){
        ApexPages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR, uPartner.Name+LABEL.ERROR_not_a_partner_user));
        return;
      }
      
      //query the partner account information related to the logged in user  
      List<Account> AccPartners = [select Id, 
                            Name, 
                            Visiting_street__c, 
                            Visiting_Housenumber1__c, 
                            Visiting_Housenumber_suffix__c, 
                            Visiting_Postal_Code__c, 
                            Visiting_City__c, 
                            Dealer_Code__c, 
                            Partner_Accreditation_Status__c, 
                            Logo_URL__c 
                          from 
                            Account 
                          where 
                            id = :uPartner.AccountId];
      if(!AccPartners.isEmpty()){
        AccPartner = AccPartners[0];
      }
      //query the user ID of the solution specialist related to the partner account of the logged in user             
      List<AccountTeamMember> ATMSolSalesList =  [select 
                                            UserId 
                                          from 
                                            AccountTeamMember 
                                          where 
                                            TeamMemberRole = 'Solution Specialist' 
                                            AND AccountId = :uPartner.AccountId 
                                          LIMIT 1];
      if(ATMSolSalesList.isEmpty()){
        ApexPages.addMessage(New Apexpages.Message(ApexPages.severity.WARNING,LABEL.ERROR_no_solution_specialist+AccPartner.Name));
      } else {
        System.debug('ATMSolSales User Id retrieved: ' + ATMSolSalesList[0].UserId);
         
        //query the solution sales user information
        //system.debug([select Id, FirstName, LastName, Phone, MobilePhone, Email, SmallPhotoURL from User Where Email != null]);
        uSolSales= [select Id, 
                          FirstName, 
                          LastName, 
                          Phone, 
                          MobilePhone, 
                          Email, 
                          SmallPhotoURL 
                        from 
                          User
                        where id = :ATMSolSalesList[0].UserId];
      } 
      //query the user ID of the OPAM related to the partner account of the logged in user             
      List<AccountTeamMember> ATMOPAMList =  [select 
                                            UserId 
                                          from 
                                            AccountTeamMember 
                                          where 
                                            TeamMemberRole = 'Operational Partner Manager' 
                                            AND AccountId = :uPartner.AccountId 
                                          LIMIT 1];
      if(ATMOPAMList.isEmpty()){
        // no problem if there is no OPAM. Not every partner has one.
        //ApexPages.addMessage(New Apexpages.Message(ApexPages.severity.WARNING,'No OPAM found for '+AccPartner.Name));
      } else {
        System.debug('ATMOPAMList User Id retrieved: ' + ATMOPAMList[0].UserId);
         
        //query the solution sales user information
        uOPAM= [select Id, 
                          FirstName, 
                          LastName, 
                          Phone, 
                          MobilePhone, 
                          Email, 
                          SmallPhotoURL 
                        from 
                          User
                        where id = :ATMOPAMList[0].UserId];
                           
      }               

      //query the user ID of the channel manager related to the partner account of the logged in user             
      List<AccountTeamMember> ATMChanMgrList =  [select 
                                        UserId 
                                      from 
                                        AccountTeamMember 
                                      where 
                                        TeamMemberRole = 'Channel Manager' 
                                        AND AccountId = :uPartner.AccountId 
                                      LIMIT 1];
      if(ATMChanMgrList.isEmpty()) {
        ApexPages.addMessage(New Apexpages.Message(ApexPages.severity.WARNING,LABEL.ERROR_no_channel_manager+AccPartner.Name));
      } else {
        //query the channel manager user information
        uChanMgr = [select 
                      FirstName, 
                      LastName, 
                      Phone, 
                      MobilePhone, 
                      Email, 
                      SmallPhotoURL 
                    from 
                      User
                    where id = :ATMChanMgrList[0].UserId];
      }            
      //TEMP query the support user information
      List<User> uHotLineList = [select 
                    FirstName, 
                    LastName, 
                    Phone, 
                    MobilePhone, 
                    Email, 
                    SmallPhotoURL 
                  from 
                    User
                  where Hotline__c = true LIMIT 1];  
      system.debug(uHotLineList);                       
      if(!uHotLineList.isEmpty()){
        uHotLine = uHotLineList[0];
      } else {
        ApexPages.addMessage(New Apexpages.Message(ApexPages.severity.WARNING,LABEL.ERROR_no_hotline+AccPartner.Name));
      }
       
    }
          
    //execute queries and return values in variables
    public User getUserThis() {
          return uPartner;
    }     
    
    public User getUserSolSales() {
          return uSolSales;
    }
    
    public Account getThispartner() {
          return AccPartner;
    }
          
    public User getUserChanMgr() {
          return uChanMgr;
    }

    public User getUserOPAM() {
          return uOPAM;
    }    
    
        public User getuHotLine() {
          return uHotLine;
    }

      
}