public with sharing class PdfViewerController {
	@AuraEnabled
	public static PdfFile getContent(Id fileId) {
		String objectName = fileId.getSObjectType().getDescribe().getName();
		PdfFile result;
		if (objectName == 'Attachment') {
			result = new PdfFile([SELECT Id, Name, Body FROM Attachment WHERE Id = :fileId]);
		} else if (objectName == 'ContentVersion') {
			result = new PdfFile([SELECT Id, Title, VersionData FROM ContentVersion WHERE Id = :fileId]);
		}
		return result;
	}

	public class PdfFile {   
		@AuraEnabled
		public String name { get; set; }
		@AuraEnabled
		public String data { get; set; }

		public PdfFile(Attachment att) {
			this(att.Name, att.Body);
		}

		public PdfFile(ContentVersion cv) {
			this(cv.Title, cv.VersionData);
		}

		public PdfFile(String name, Blob content) {
			this.name = name;
			this.data = EncodingUtil.base64Encode(content);
		}
	}
}