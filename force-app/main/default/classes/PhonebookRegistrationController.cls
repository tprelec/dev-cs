public with sharing class PhonebookRegistrationController {

    public List<PhonebookRegistrationWrapper> phonebookRegistrations{get;set;}
	//public OrderFormCache__c currentCache {get;set;}
	public Order__c theOrder {get;set;}
	public Numberporting__c numberporting {get;set;}

	public boolean getshowUpdatePhonebook(){
		return (numberporting.Phonebook_Registration_Option__c=='Update the phonebook registration' || numberporting.Phonebook_Registration_Option__c==null);
	}

	public Pagereference addPhonebookRegistration(){
		//Add a new phonebook registration to the phonebook registration list
		Id numberPortingId = null;
		if(!phonebookRegistrations.isEmpty()) {
			numberportingId = phonebookRegistrations[0].phonebookReg.Numberporting__c;
		} else {
			numberportingId = [Select Id From Numberporting__c Where Order__c = :theOrder.Id].Id;			
		}
		phonebookRegistrations.add(createNewPhonebookRegistration(numberportingId,theOrder.Account__c,phonebookRegistrations.size()));
		return null;
	}
	
	public PhonebookRegistrationWrapper createNewPhonebookRegistration(Id numberportingId, Id theAccountId, Integer index){
		Account theAccount = getTheAccount(theAccountId);
		Phonebook_Registration__c pbr = new Phonebook_Registration__c();
		pbr.Numberporting__c = numberportingId;
		// default address to customer's visiting address
		if(theAccount != null){
			pbr.Name__c = theAccount.Name;
			pbr.Address__c = theAccount.Visiting_Street__c+' '+String.valueOf(theAccount.Visiting_Housenumber1__c)+ (theAccount.Visiting_Housenumber_Suffix__c == null ? '' : theAccount.Visiting_Housenumber_Suffix__c);
			pbr.Zipcode__c = theAccount.Visiting_Postal_Code__c==null?null:theAccount.Visiting_Postal_Code__c.replace(' ','');
			pbr.City__c = theAccount.Visiting_City__c;
			insert pbr;
		}		
		
		PhonebookRegistrationWrapper pbrw = new PhonebookRegistrationWrapper(index, true, pbr,false);
		
		return pbrw;
	}
	
	public pageReference delPhonebookRegistration(){
		Phonebook_Registration__c pbr = phonebookRegistrations[integer.valueof(ApexPages.currentPage().getParameters().get('toDelete'))].phonebookReg;
		if(pbr.Id != null) new WithoutSharingMethods().deleteObject(pbr);
		phonebookRegistrations.remove(integer.valueof(ApexPages.currentPage().getParameters().get('toDelete')));		

		// check if this changed anything in the order readiness
		Boolean allPhonebookRegsReady = true;

		for(PhonebookRegistrationWrapper pbrw : phonebookRegistrations){
			if(OrderValidation.checkObject(pbrw.phonebookReg,theOrder) != ''){
				allPhonebookRegsReady = false;
				break;
			}
		}
		
		if(theOrder.PhonebookRegistrationReady__c != allPhonebookRegsReady){
			theOrder.PhonebookRegistrationReady__c = allPhonebookRegsReady;			
			update theOrder;
		}
		
		// reset row id's in order to prevent missing indices later on..
		for(Integer i=0;i<phonebookRegistrations.size();i++){
			phonebookRegistrations[i].Id = i;
		}

		// recheck if this order is now clean 
		OrderUtils.updateOrderStatus(theOrder,true);	

		return  null;
	}
	
	
	public string getHTMLFormatPhoneHelptext(){
		return Phonebook_Registration__c.Phone__c.getDescribe().getInlineHelpText().replaceAll('\r\n', '<br/>');
	}	

	public Account getTheAccount(Id theAccountId) {
		Account theAccount = new Account();
		if(theAccountId != null){
			theAccount = [Select
							Name,
							Visiting_Street__c,
							Visiting_Housenumber1__c,
							Visiting_Housenumber_Suffix__c,
							Visiting_Postal_Code__c,
							Visiting_City__c
						From
							Account
						Where
							Id = :theAccountId];
		}
		return theAccount;			
	}

	/**
	 * @description			A without sharing util class to do the updates/inserts/deletes without interfering with authorizations 
	 * @author				Guy Clairbois
	 */	
    public without sharing class WithoutSharingMethods{
        public void deleteObject(sObject objectToDelete){
            delete objectToDelete;
        }        
    }

}