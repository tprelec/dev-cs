public with sharing class PhonebookRegistrationWrapper {
	//wrapper class for phonebook registrations
    
    public Phonebook_registration__c phonebookReg {get; set;}
    public Integer id {get; set;}
    
    public PhonebookRegistrationWrapper(Integer rowId, Boolean inEdit, Phonebook_registration__c p,Boolean isDeleted) {
        this.phonebookReg = p;
        this.id = rowId;
    }
}