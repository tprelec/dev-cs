/**
 * @description Picklist service class to offer functionality around picklists
 */
// TODO create test class
public inherited sharing class PicklistService {
	/**
	 * Picklist options:
	 */
	public class PicklistOption {
		@AuraEnabled
		public String value,
			label;

		public PicklistOption(String label, String value) {
			this.label = label;
			this.value = value;
		}
	}

	/**
	 * @description Dynamically return the Picklist options (label/value combination) based on provided input:
	 */
	public static List<PicklistOption> getPicklistOptions(
		String sObjectType,
		String fieldName
	) {
		try {
			sObjectType = String.escapeSingleQuotes(sObjectType);
			fieldName = String.escapeSingleQuotes(fieldName);

			List<PicklistOption> picklistOptionsList = new List<PicklistOption>();

			DescribeSObjectResult objResult = Schema.getGlobalDescribe()
				.get(sObjectType)
				.getDescribe();
			// Get the field dynamically
			List<Schema.PicklistEntry> plList = objResult.fields.getMap()
				.get(fieldName)
				.getDescribe()
				.getPicklistValues();

			for (Schema.PicklistEntry pe : plList) {
				picklistOptionsList.add(
					new PicklistOption(pe.getLabel(), pe.getValue())
				);
			}
			return picklistOptionsList;
		} catch (Exception e) {
			throw new PicklistServiceException(e);
		}
	}

	public class PicklistServiceException extends Exception {
	}
}