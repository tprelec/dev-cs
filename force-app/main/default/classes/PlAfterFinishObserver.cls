global with sharing class PlAfterFinishObserver implements cspl.PLObserver {
    global PlAfterFinishObserver() {
         
    }
    
    global void execute(String data, String basketId) {
        /*
        List<Object> dataList = (List<Object>) JSON.deserializeUntyped(data);
        
        cscfga__Product_Basket__c basket = [SELECT Id, Name, cscfga__Basket_Status__c, KPI_Sales_Margin_Net_revenue__c, KPI_EBITDA_Net_revenue__c, KPI_Free_cash_flow_Net_revenue__c,
                                            KPI_Net_Incremental_Billed_Revenue__c, KPI_NPV__c, 
                                            KPI_Payback_Period_Months__c FROM cscfga__Product_Basket__c WHERE Id= :basketId LIMIT 1];
                                            
        if(basket.cscfga__Basket_Status__c != 'Approved' && basket.cscfga__Basket_Status__c != 'Contract created') {
            for (Object obj : dataList) {
                Map<String, Object> objMap = (Map<String, Object>)obj;
                String selectedValue = (String) objMap.get('SelectedValue');
                String rowName = (String) objMap.get('Name');
                System.debug(objMap);
                if (selectedValue == 'Total') {
                    if (rowName == 'KPI Sales Margin / Net revenue') {
                        Decimal total = changeStringToDecimal((String) objMap.get('Total'));
                        basket.KPI_Sales_Margin_Net_revenue__c = total;
                    }
                    if (rowName == 'KPI EBITDA / Net revenue') {
                        Decimal total = changeStringToDecimal((String) objMap.get('Total'));
                        basket.KPI_EBITDA_Net_revenue__c = total;
                    }
                    if (rowName == 'KPI Free cash flow / Net revenue') {
                        Decimal total = changeStringToDecimal((String) objMap.get('Total'));
                        basket.KPI_Free_cash_flow_Net_revenue__c = total;
                    }
                    if (rowName == 'KPI Net Incremental Billed Revenue') {
                        Decimal total = changeStringToDecimal((String) objMap.get('Total'));
                        basket.KPI_Net_Incremental_Billed_Revenue__c = total;
                    }
                    if (rowName == 'KPI NPV') {
                        Decimal total = changeStringToDecimal((String) objMap.get('Total'));
                        basket.KPI_NPV__c = total;
                    }
                    if (rowName == 'KPI Payback Period(Months)') {
                        Decimal total = changeStringToDecimal((String) objMap.get('Total'));
                        basket.KPI_Payback_Period_Months__c = total;
                    }
                } else {
                    break;
                }
            }
        
            update basket;
        }
        */
    }
    
    public Decimal changeStringToDecimal(String totalString) {
        if (totalString.substring(totalString.length() - 3, totalString.length() - 2) == ',') {
            totalString = totalString.replace(',', '.');
        }
        Decimal total = Decimal.valueOf(totalString);
        return total;
    }
}