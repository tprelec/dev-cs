/**
 * @description         This is the trigger handler for the NetProfit Information sObject.
 * @author              Stjepan Pavuna
 */
public with sharing class PostalCodeAssignmentTriggerHandler extends TriggerHandler {

    /**
     * @description         This handles the before insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeInsert(){
        //List<Postal_Code_Assignment__c> newAssignments = (List<Postal_Code_Assignment__c>) this.newList;
        
    }

    
    /**
     * @description         This handles the after insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterInsert(){
        List<Postal_Code_Assignment__c> newAssignments = (List<Postal_Code_Assignment__c>) this.newList;

        updateDealerInfoDetails(newAssignments,null);

    }

    /**
     * @description         This handles the after insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeUpdate(){
        /*List<Postal_Code_Assignment__c> newAssignments = (List<Postal_Code_Assignment__c>) this.newList;    
        List<Postal_Code_Assignment__c> oldAssignments = (List<Postal_Code_Assignment__c>) this.oldList;
        Map<Id,Postal_Code_Assignment__c> oldAssignmentsMap = (Map<Id,Postal_Code_Assignment__c>) this.oldMap;*/

    }
    
    /**
     * @description         This handles the after update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void afterUpdate(){
        List<Postal_Code_Assignment__c> newAssignments = (List<Postal_Code_Assignment__c>) this.newList;    
        List<Postal_Code_Assignment__c> oldAssignments = (List<Postal_Code_Assignment__c>) this.oldList;
        Map<Id,Postal_Code_Assignment__c> oldAssignmentsMap = (Map<Id,Postal_Code_Assignment__c>) this.oldMap;

        updateDealerInfoDetails(newAssignments,oldAssignmentsMap);

    }


    private void updateDealerInfoDetails(List<Postal_Code_Assignment__c> newAssignments, Map<Id,Postal_Code_Assignment__c> oldAssignmentsMap){

		List<Id> assignmentsToUpdate = new List<Id>();
		system.debug('***SP*** 1');

        for(Postal_Code_Assignment__c assignment : newAssignments){
        	if(oldAssignmentsMap == null){
        		//inserts
        		system.debug('***SP*** 2');
        		if(assignment.Dealer_Information__c != null){
        			system.debug('***SP*** 3');
	        		assignmentsToUpdate.add(assignment.id);
	        	}
        	}
        	else if(newAssignments == null){
        		//deletes
        		//nevermind, record is gone
        		system.debug('***SP*** 4');
        	}
        	else{
        		//updates
        		system.debug('***SP*** 5');
        		if(assignment.Dealer_Information__c != oldAssignmentsMap.get(assignment.id).Dealer_Information__c){
        			system.debug('***SP*** 6');
        			assignmentsToUpdate.add(assignment.id);
        		}
        	}
        	
        }

        PostalCodeAssignmentHelper.updatePostalCodeAssignments(assignmentsToUpdate);
    }
}