/**
 * @description         This class is responsible for creating site_postal_check records for a number of different input scenarios
 * @author              Guy Clairbois
 */
public with sharing class PostalCodeCheckController {
    public Id siteId { get; set; }
    public static String error { get; set; }
    public Boolean showActionStatus { get; set; }
    public Site__c s { get; set; }

    public static final Set<String> activeAccessTypes {
        get {
            if (activeAccessTypes == null) {
                activeAccessTypes = new Set<String>();
                for (Access_Check_Result__mdt acr : [
                    SELECT Id, DeveloperName, Make_Active__c
                    FROM Access_Check_Result__mdt
                    WHERE Make_Active__c = TRUE
                ]) {
                    activeAccessTypes.add((acr.DeveloperName.toLowerCase()));
                }
            }
            return activeAccessTypes;
        }
        private set;
    }
    /**
     *  method to do a postalcodecheck based on a single siteid in the page parameters
     */
    public pageReference postalCodeCheckForSite() {
        siteId = ApexPages.currentPage().getParameters().get('SiteId');
        String requestType = ApexPages.currentPage().getParameters().get('requestType');

        fetchSite();

        showActionStatus = false;
        if (s.Postal_Code_Check_Status__c == 'Requested') {
            Apexpages.addMessage(
                new Apexpages.Message(
                    ApexPages.severity.ERROR,
                    'A postalcode check was already requested. Note that for each new site, the first postalcode check is requested automatically.'
                )
            );
            showActionStatus = true;
            return null;
        }

        if (
            requestType == 'fiber' &&
            s.Blocked_checks__c != null &&
            s.Blocked_checks__c.contains('fiber')
        ) {
            Apexpages.addMessage(
                new Apexpages.Message(
                    ApexPages.severity.ERROR,
                    'Since a check was done on ' +
                    s.Last_fiber_check__c +
                    ', fiber is currently blocked'
                )
            );
            showActionStatus = true;
            return null;
        }
        if (
            requestType == 'dsl' &&
            s.Blocked_checks__c != null &&
            s.Blocked_checks__c.contains('dsl')
        ) {
            Apexpages.addMessage(
                new Apexpages.Message(
                    ApexPages.severity.ERROR,
                    'Since a check was done on ' +
                    s.Last_dsl_check__c +
                    ', dsl is currently blocked'
                )
            );
            showActionStatus = true;
            return null;
        }

        if (s.Site_Postal_Code__c == null || s.Site_House_Number__c == null) {
            Apexpages.addMessage(
                new Apexpages.Message(
                    ApexPages.severity.ERROR,
                    'Postal Code and House Number are required for doing a postalcode check.'
                )
            );
            showActionStatus = true;
            return null;
        }

        doPostalCodeCheckForSites(new List<Site__c>{ s }, requestType);

        if (error != null) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, error));
            return null;
        }
        return new PageReference('/' + siteId);
    }

    public void fetchSite() {
        s = [
            SELECT
                Id,
                Blocked_checks__c,
                Last_fiber_check__c,
                Last_dsl_check__c,
                Last_dsl_Canvas_check_c__c,
                Last_fiber_Canvas_check_c__c,
                Postal_Code_Check_Status__c,
                Site_Street__c,
                Site_Postal_Code__c,
                Site_House_Number__c,
                Site_House_Number_Suffix__c,
                Canvas_Postal_Code__c,
                Canvas_House_Number__c,
                Canvas_House_Number_Suffix__c
            FROM Site__c
            WHERE Id = :siteId
            LIMIT 1
        ];
    }

    /**
     *  method to do an offline postalcodecheck based on a set of siteIds
     */
    @future(callout=true)
    public static void doOfflinePostalCodeCheckForSites(Set<Id> siteIds, String requestType) {
        doOnlinePostalCodeCheckForSites(siteIds, requestType);
    }

    public static void doOnlinePostalCodeCheckForSites(Set<Id> siteIds, String requestType) {
        List<Site__c> sites = [
            SELECT
                Id,
                Blocked_checks__c,
                Last_fiber_check__c,
                Last_dsl_check__c,
                Site_Street__c,
                Site_Postal_Code__c,
                Site_House_Number__c,
                Site_House_Number_Suffix__c,
                Canvas_Postal_Code__c,
                Canvas_House_Number__c,
                Canvas_House_Number_Suffix__c
            FROM Site__c
            WHERE Id IN :siteIds
        ];
        List<Site_Postal_Check__c> spcs = doPostalCheck(sites, requestType);
        if (!spcs.isEmpty()) {
            List<Site_Postal_Check__c> spcsOld = deactivateExistingChecks(sites, requestType);

            // This seems odd, but it needed to get latest values (this is sometimes running in future and needs to get current state as other future jobs can change the checks)
            Map<Id, Site__c> currentSiteMap = new Map<Id, Site__c>(
                [
                    SELECT
                        Last_fiber_check__c,
                        Last_dsl_check__c,
                        Last_fiber_Canvas_check_c__c,
                        Last_dsl_Canvas_check_c__c
                    FROM site__c
                    WHERE ID IN :siteIds
                ]
            );

            for (Site__c s : sites) {
                if (requestType != 'dsl') {
                    s.Last_fiber_check__c = System.now();
                    if (currentSiteMap.get(s.id).Last_dsl_check__c != null) {
                        s.Postal_Code_Check_Status__c = 'Completed';
                    }
                }
                if (requestType != 'fiber') {
                    s.Last_dsl_check__c = System.now();
                    if (currentSiteMap.get(s.id).Last_fiber_check__c != null) {
                        s.Postal_Code_Check_Status__c = 'Completed';
                    }
                }
                if (requestType != 'CanvasD') {
                    s.Last_dsl_Canvas_check_c__c = System.now();
                    if (currentSiteMap.get(s.id).Last_fiber_Canvas_check_c__c != null) {
                        s.Postal_Code_Check_Status__c = 'Completed';
                    }
                }
                if (requestType != 'CanvasF') {
                    s.Last_fiber_Canvas_check_c__c = System.now();
                    if (currentSiteMap.get(s.id).Last_dsl_Canvas_check_c__c != null) {
                        s.Postal_Code_Check_Status__c = 'Completed';
                    }
                }
            }

            Savepoint sp = Database.setSavepoint();
            try {
                update sites;
                update spcsOld;
                insert spcs;
            } catch (Exception e) {
                Database.rollback(sp);
            }
        }
    }

    public static void doBatchCheck(Map<String, Set<Id>> requesttypeToSites) {
        List<Site_Postal_Check__c> spcs = new List<Site_Postal_Check__c>();
        List<Site_Postal_Check__c> spcsOld = new List<Site_Postal_Check__c>();

        // process dsl
        List<Site__c> dslSites = [
            SELECT
                Id,
                Blocked_checks__c,
                Last_dsl_check__c,
                Site_Street__c,
                Site_Postal_Code__c,
                Site_House_Number__c,
                Site_House_Number_Suffix__c,
                Canvas_Postal_Code__c,
                Canvas_House_Number__c,
                Canvas_House_Number_Suffix__c
            FROM Site__c
            WHERE Id IN :requesttypeToSites.get('dsl')
        ];

        if (!dslSites.isEmpty()) {
            List<Site_Postal_Check__c> dslChecks = doPostalCheck(dslSites, 'dsl');
            if (!dslChecks.isEmpty()) {
                spcs.addAll(dslChecks);
                spcsOld.addAll(deactivateExistingChecks(dslSites, 'dsl'));
                for (Site__c s : dslSites) {
                    s.Last_dsl_check__c = System.now();
                    s.Postal_Code_Check_Status__c = 'Completed';
                }
            }
        }

        List<Site__c> fiberSites = [
            SELECT
                Id,
                Blocked_checks__c,
                Last_fiber_check__c,
                Site_Street__c,
                Site_Postal_Code__c,
                Site_House_Number__c,
                Site_House_Number_Suffix__c,
                Canvas_Postal_Code__c,
                Canvas_House_Number__c,
                Canvas_House_Number_Suffix__c
            FROM Site__c
            WHERE Id IN :requesttypeToSites.get('fiber')
        ];

        if (!fiberSites.isEmpty()) {
            List<Site_Postal_Check__c> fiberChecks = doPostalCheck(fiberSites, 'fiber');
            if (!fiberChecks.isEmpty()) {
                spcs.addAll(fiberChecks);
                spcsOld.addAll(deactivateExistingChecks(fiberSites, 'fiber'));
                for (Site__c s : fiberSites) {
                    s.Last_fiber_check__c = System.now();
                    s.Postal_Code_Check_Status__c = 'Completed';
                }
            }
        }

        Savepoint sp = Database.setSavepoint();
        try {
            update spcsOld;
            insert spcs;
            update dslSites;
            update fiberSites;
        } catch (Exception e) {
            Database.rollback(sp);
        }
    }

    /**
     *  method to do a postalcodecheck based on a list of sites
     */
    public static void doPostalCodeCheckForSites(List<Site__c> sites, String requestType) {
        List<Site_Postal_Check__c> spcs = doPostalCheck(sites, requestType);
        if (!spcs.isEmpty()) {
            List<Site_Postal_Check__c> spcsOld = deactivateExistingChecks(sites, requestType);
            for (Site__c s : sites) {
                if (
                    (requestType != 'dsl' && s.Last_fiber_check__c != null) ||
                    (requestType != 'CanvasD' &&
                    s.Last_fiber_Canvas_check_c__c != null)
                ) {
                    s.addError('Fiber check is currently blocked.');
                }
                if (
                    (requestType != 'fiber' && s.Last_dsl_check__c != null) ||
                    (requestType != 'CanvasF' &&
                    s.Last_dsl_Canvas_check_c__c != null)
                ) {
                    s.addError('DSL check is currently blocked.');
                }

                if (requestType == 'dsl') {
                    s.Last_dsl_check__c = System.now();
                }
                if (requestType == 'CanvasD') {
                    s.Last_dsl_Canvas_check_c__c = System.now();
                }
                if (requestType == 'fiber') {
                    s.Last_fiber_check__c = System.now();
                }
                if (requestType == 'CanvasF') {
                    s.Last_fiber_Canvas_check_c__c = System.now();
                }
                s.Postal_Code_Check_Status__c = 'Completed';
            }
            Savepoint sp = Database.setSavepoint();
            try {
                update sites;
                update spcsOld;
                for (Site_Postal_Check__c spc : spcs) {
                    system.debug(spc);
                }
                insert spcs;
            } catch (Exception e) {
                Database.rollback(sp);
                throw new ExWebServiceCalloutException(
                    'Error while saving postalcodecheck results: ' + e
                );
            }
        }
    }

    /**
     *  first deactivate existing checks, based on list of sites
     */
    private static List<Site_Postal_Check__c> deactivateExistingChecks(
        List<Site__c> sites,
        String requestType
    ) {
        List<Site_Postal_Check__c> postalChecksToUpdate = new List<Site_Postal_Check__c>();
        Set<Id> siteIds = new Set<Id>();
        for (Site__c s : sites) {
            siteIds.add(s.Id);
        }
        String queryString = 'SELECT Id, Access_Active__c ';
        queryString += 'FROM Site_Postal_Check__c ';
        queryString += 'WHERE Access_Active__c = true ';
        queryString += 'AND Access_Site_ID__c in :siteIds ';
        queryString += 'AND Manual__c != true ';
        if (requestType == 'fiber') {
            queryString += 'AND Accessclass__c in (\'Fiber\',\'sdslbis\',\'hfc\') ';
            queryString += 'AND (NOT Access_Site_ID__r.Blocked_checks__c LIKE \'fiber\') ';
        } else if (requestType == 'dsl') {
            queryString += 'AND Accessclass__c not in (\'Fiber\',\'sdslbis\',\'hfc\') ';
            queryString += 'AND (NOT Access_Site_ID__r.Blocked_checks__c LIKE \'dsl\') ';
        } else if (requestType == 'CanvasD') {
            queryString += 'AND Accessclass__c not in (\'Fiber\',\'sdslbis\',\'hfc\') ';
            queryString += 'AND (NOT Access_Site_ID__r.Blocked_checks__c LIKE \'dsl\') ';
        } else if (requestType == 'CanvasF') {
            queryString += 'AND Accessclass__c in (\'Fiber\',\'sdslbis\',\'hfc\') ';
            queryString += 'AND (NOT Access_Site_ID__r.Blocked_checks__c LIKE \'fiber\') ';
        } else {
            // in case no requesttype is given, overwrite both if any of the 2 is outdated
            queryString += 'AND (Access_Site_ID__r.Blocked_checks__c != \'\') ';
        }
        system.debug(querystring);
        for (Site_Postal_Check__c spc : Database.query(queryString)) {
            spc.Access_Active__c = false;
            postalChecksToUpdate.add(spc);
        }
        return postalChecksToUpdate;
    }

    /**
     *  general call to service, based on list of sites
     */
    public static Map<Id, Set<String>> siteTechTypeUniqueMap = new Map<Id, Set<String>>();
    public static List<Site_Postal_Check__c> doPostalCheck(
        List<Site__c> sites,
        String requestType
    ) {
        List<Site_Postal_Check__c> postalChecksToReturn = new List<Site_Postal_Check__c>();
        Map<String, Set<Id>> postalCodeToSites = new Map<String, Set<Id>>();
        ECSPostalCodeCheckService service = new ECSPostalCodeCheckService();
        ECSPostalCodeCheckService.request[] theRequests = new List<ECSPostalCodeCheckService.Request>{};
        Map<String, List<Preferred_Supplier__c>> techTypeToPrefVendor = getPrefVendorMap();

        system.debug(requestType);

        for (Site__c s : sites) {
            system.debug(s.Blocked_checks__c);
            // skip the check if it is currently blocked
            if (requestType == 'fiber') {
                if (s.Blocked_checks__c != null && s.Blocked_checks__c.contains('fiber')) {
                    continue;
                }
            } else if (requestType == 'CanvasF') {
                if (s.Blocked_checks__c != null && s.Blocked_checks__c.contains('CanvasF')) {
                    continue;
                }
            } else if (requestType == 'dsl') {
                if (s.Blocked_checks__c != null && s.Blocked_checks__c.contains('dsl')) {
                    continue;
                }
            } else if (requestType == 'CanvasD') {
                if (s.Blocked_checks__c != null && s.Blocked_checks__c.contains('CanvasD')) {
                    continue;
                }
            } else {
                if (s.Blocked_checks__c != null) {
                    continue;
                }
            }

            if (s.Site_Postal_Code__c == null) {
                throw new ExWebServiceCalloutException(
                    'Postal code is required for postalcode check.'
                );
            }
            if (s.Site_House_Number__c == null) {
                throw new ExWebServiceCalloutException(
                    'Housenumber is required for postalcode check.'
                );
            }

            //check if canvas postalcode and housnumber are empty.
            //if not, use canvas data. otherwise the old logic
            ECSPostalCodeCheckService.request theRequest = new ECSPostalCodeCheckService.Request();
            if (s.Canvas_Postal_Code__c == null && s.Canvas_House_Number__c == null) {
                if (s.Site_Postal_Code__c == null) {
                    throw new ExWebServiceCalloutException(
                        'Postal code is required for postalcode check.'
                    );
                }
                if (s.Site_House_Number__c == null) {
                    throw new ExWebServiceCalloutException(
                        'Housenumber is required for postalcode check.'
                    );
                }
                theRequest.postalCode = s.Site_Postal_Code__c;
                theRequest.houseNumber = Integer.valueOf(s.Site_House_Number__c);
                theRequest.houseNumberExtension = s.Site_House_Number_Suffix__c;
            } else {
                if (s.Canvas_Postal_Code__c == null) {
                    throw new ExWebServiceCalloutException(
                        'Canvas Postal code is required for postalcode check.'
                    );
                }
                if (s.Canvas_House_Number__c == null) {
                    throw new ExWebServiceCalloutException(
                        'Canvas Housenumber is required for postalcode check.'
                    );
                }
                theRequest.postalCode = s.Canvas_Postal_Code__c;
                theRequest.houseNumber = Integer.valueOf(s.Canvas_House_Number__c);
                theRequest.houseNumberExtension = s.Canvas_House_Number_Suffix__c;
            }

            theRequests.add(theRequest);

            // create map for retrieving the site id by postalcode later on in the response
            // first map on canvas, if null then default site. could also the other way arround
            String key =
                s.Canvas_Postal_Code__c +
                String.valueOf(s.Canvas_House_Number__c) +
                (s.Canvas_House_Number_Suffix__c == null ? '' : s.Canvas_House_Number_Suffix__c);
            if (s.Canvas_Postal_Code__c == null && s.Canvas_House_Number__c == null) {
                key =
                    s.Site_Postal_Code__c +
                    String.valueOf(s.Site_House_Number__c) +
                    (s.Site_House_Number_Suffix__c == null ? '' : s.Site_House_Number_Suffix__c);
            }

            Set<Id> tempSet = new Set<Id>();
            if (postalCodeToSites.containsKey(key)) {
                tempSet = postalCodeToSites.get(key);
            }
            tempSet.add(s.Id);
            postalCodeToSites.put(key, tempSet);
            
            if (!siteTechTypeUniqueMap.containsKey(s.Id)) {
                siteTechTypeUniqueMap.put(s.Id, new Set<String>());
            }
        }

        if (!theRequests.isEmpty()) {
            system.debug('theRequest:' + theRequests);
            service.setRequest(theRequests);

            try {
                service.makeRequest(requestType);
            } catch (ExWebServiceCalloutException e) {
                error = e.getMessage();
            }

            if (requestType == 'fiber' || requestType == 'CanvasF') {
                try {
                    service.makeRequest('sdslbis');
                } catch (ExWebServiceCalloutException e) {
                    error = e.getMessage();
                }

                try {
                    service.makeRequest('coax');
                } catch (ExWebServiceCalloutException e) {
                    error = e.getMessage();
                }
            }

            ECSPostalCodeCheckService.Response[] theResponses = service.getResponse();
            // handle responses
            for (ECSPostalCodeCheckService.Response theResponse : theResponses) {
                // first identify which site thise response is for
                String key =
                    theResponse.postalCode +
                    String.valueOf(theResponse.houseNumber) +
                    (theResponse.houseNumberExtension == null
                        ? ''
                        : theResponse.houseNumberExtension);
                if (postalCodeToSites.containsKey(key)) {
                    for (Id siteId : postalCodeToSites.get(key)) {
                        Set<String> technologyTypeUniqueList = siteTechTypeUniqueMap.get(siteId);
                        
                        // create postal check records for each of the lines
                        Map<String, List<Site_Postal_Check__c>> techTypeToSPC = new Map<String, List<Site_Postal_Check__c>>();
                        for (ECSPostalCodeCheckService.lineOption lo : theResponse.lineOptions) {
                            Site_Postal_Check__c spc = new Site_Postal_Check__c();
                            spc.Access_Site_ID__c = siteId;
                            spc.Access_Result_Check__c = lo.serviceable;
                            spc.Access_Max_Down_Speed__c = lo.maxDownload;
                            spc.Access_Max_Up_Speed__c = lo.maxUpload;
                            spc.Technology_Type__c = lo.technologytype;
                            spc.Access_Vendor__c = GeneralUtils.portfolioVendorMap.get(
                                lo.portfolio
                            );
                            spc.Accessclass__c = lo.accessclass;
                            spc.Access_Region__c = lo.classification;

                            postalChecksToReturn.add(spc);
                        }
                        // create a map by techtype+vendor as a preparation for determining preferred items
                        // + techonology unique list for W-002029
                        for (Site_Postal_Check__c spc : postalChecksToReturn) {
                            // split techtype into 2 parts...
                            if (spc.Technology_Type__c != null) {
                                String techType = spc.Technology_Type__c.split('_')[0];
                                technologyTypeUniqueList.add(techType);
                                key = techType.toLowerCase() + spc.Access_Vendor__c;
                                if (techTypeToSPC.containsKey(key)) {
                                    techTypeToSPC.get(key).add(spc);
                                } else {
                                    techTypeToSPC.put(key, new List<Site_Postal_Check__c>{ spc });
                                }
                            }
                        }
                        system.debug(techTypeToSPC);
                        system.debug(techTypeToPrefVendor);
                        // only activate lineoptions that are preferred suppliers
                        for (String techType : techTypeToPrefVendor.keySet()) {
                            // for each techtype/vendor combi, go through the list and see if it's a preferred one. If so, activate it and add the Order #.
                            if (techTypeToPrefVendor.containsKey(techType)) {
                                Boolean techHasOnnetLevel0 = false;
                                for (
                                    Preferred_Supplier__c vendor : techTypeToPrefVendor.get(
                                        techType
                                    )
                                ) {
                                    String matchString = techType.toLowerCase() + vendor.Vendor__c;
                                    if (techTypeToSPC.containsKey(matchString)) {
                                        if (vendor.Order__c == 0) {
                                            for (
                                                Site_Postal_Check__c spc : techTypeToSPC.get(
                                                    matchString
                                                )
                                            ) {
                                                if (
                                                    activeAccessTypes.contains(
                                                        spc.Access_Result_Check__c.toLowerCase()
                                                    )
                                                ) {
                                                    // onnet level 0 provider found
                                                    techHasOnnetLevel0 = true;
                                                    spc.Access_Active__c = true;
                                                    spc.Access_Priority__c = vendor.Order__c;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                for (
                                    Preferred_Supplier__c vendor : techTypeToPrefVendor.get(
                                        techType
                                    )
                                ) {
                                    String matchString = techType.toLowerCase() + vendor.Vendor__c;
                                    if (techTypeToSPC.containsKey(matchString)) {
                                        for (
                                            Site_Postal_Check__c spc : techTypeToSPC.get(
                                                matchString
                                            )
                                        ) {
                                            spc.Access_Priority__c = vendor.Order__c;
                                            system.debug(activeAccessTypes);
                                            system.debug(spc.Access_Result_Check__c);
                                            if (
                                                !techHasOnnetLevel0 &&
                                                activeAccessTypes.contains(
                                                    spc.Access_Result_Check__c.toLowerCase()
                                                )
                                            ) {
                                                //if there's no active level 0 then make the others active
                                                spc.Access_Active__c = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //if both VDSL and VVDSL are active, deactivate the VDSL
                        //if both BVDSL and BVVDSL are active, deactivate the BVDSL
                        //if FttH is available, deactivate the xDSL
                        Set<String> dsls = new Set<String>{ 'ADSL', 'VDSL', 'VVDSL', 'BVDSL', 'BVVDSL', 'EthernetOverCopper', 'EthernetOverCopperSingle' };
                        for (Site_Postal_Check__c spc : postalChecksToReturn) {
                            if (spc.Technology_Type__c == null) {
                                continue;
                            }

                            String techType = spc.Technology_Type__c.split('_')[0];
                            InfrastructureMapping__c infra = InfrastructureMapping__c.getValues(
                                spc.Technology_Type__c
                            );
                            if (
                                (techType == 'VDSL2' &&
                                technologyTypeUniqueList.contains('VVDSL2')) ||
                                (techType == 'BVDSL2' &&
                                technologyTypeUniqueList.contains('BVVDSL2')) ||
                                (infra != null && 
                                techType.contains('SDSL') && 
                                dsls.contains(infra.AccessInfrastructure__c)) ||                                
                                (infra != null &&
                                infra.AccessInfrastructure__c == 'ADSL' &&
                                GeneralUtils.setsIntersect(
                                    technologyTypeUniqueList,
                                    new Set<String>{ 'VVDSL2', 'VDSL2' }
                                )) ||
                                (infra != null &&
                                dsls.contains(infra.AccessInfrastructure__c) &&
                                GeneralUtils.setsIntersect(
                                    technologyTypeUniqueList,
                                    new Set<String>{'EoF', 'GoF'}
                                ))
                            ) {
                                spc.Access_Active__c = false;
                            }
                        }

                        // lastly add any error that occurred as reference records
                        for (
                            ECSPostalCodeCheckService.lineOptionError loe : theResponse.errorMessages
                        ) {
                            Site_Postal_Check__c spc = new Site_Postal_Check__c();
                            spc.Access_Site_ID__c = siteId;
                            spc.Access_Result_Check__c = loe.errorMessage.abbreviate(254);
                            spc.Access_Max_Down_Speed__c = 0;
                            spc.Access_Max_Up_Speed__c = 0;
                            spc.Technology_Type__c = null;
                            spc.Access_Vendor__c = GeneralUtils.portfolioVendorMap.get(
                                loe.portfolio
                            );
                            spc.Accessclass__c = null;
                            postalChecksToReturn.add(spc);
                        }
                    }
                }
            }
        }
        return postalChecksToReturn;
    }

    private static Map<String, List<Preferred_Supplier__c>> getPrefVendorMap() {
        Map<String, List<Preferred_Supplier__c>> prefVendorMap = new Map<String, List<Preferred_Supplier__c>>();
        for (Preferred_Supplier__c ps : [
            SELECT Id, Vendor__c, Technology_Type__c, Order__c
            FROM Preferred_Supplier__c
            ORDER BY Order__c ASC
        ]) {
            if (prefVendorMap.containsKey(ps.Technology_Type__c)) {
                prefVendorMap.get(ps.Technology_Type__c).add(ps);
            } else {
                prefVendorMap.put(ps.Technology_Type__c, new List<Preferred_Supplier__c>{ ps });
            }
        }
        return prefVendorMap;
    }

    public pageReference backToSite() {
        return new PageReference('/' + siteId);
    }
}