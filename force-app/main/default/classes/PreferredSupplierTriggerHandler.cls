public without sharing class PreferredSupplierTriggerHandler extends TriggerHandler {

    /**
     * @description         This handles the before insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeInsert(){
        List<Preferred_Supplier__c> newPreferredSupplier = (List<Preferred_Supplier__c>) this.newList;

    }

    
    /**
     * @description         This handles the after insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterInsert(){
        List<Preferred_Supplier__c> newPreferredSupplier = (List<Preferred_Supplier__c>) this.newList;   

        setReferenceOnPostalcodechecks(newPreferredSupplier);
    }


    /**
     * @description         This handles the before update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeUpdate(){
        List<Preferred_Supplier__c> newPreferredSupplier = (List<Preferred_Supplier__c>) this.newList;   
        List<Preferred_Supplier__c> oldPreferredSupplier = (List<Preferred_Supplier__c>) this.oldList;
        Map<Id,Preferred_Supplier__c> oldPreferredSupplierMap = (Map<Id,Preferred_Supplier__c>) this.oldMap;

    }
    

    /**
     * @description         This handles the after update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterUpdate(){
        List<Preferred_Supplier__c> newPreferredSupplier = (List<Preferred_Supplier__c>) this.newList;   
        List<Preferred_Supplier__c> oldPreferredSupplier = (List<Preferred_Supplier__c>) this.oldList;
        Map<Id,Preferred_Supplier__c> oldPreferredSupplierMap = (Map<Id,Preferred_Supplier__c>) this.oldMap;

    }   


    private void setReferenceOnPostalcodechecks(List<Preferred_Supplier__c> newPreferredSupplier){
    	// create a mapping
    	Map<String,Map<String,Id>> vendorToTechTypeToPSId = new Map<String,Map<String,Id>>();
    	for(Preferred_Supplier__c pf : newPreferredSupplier){
    		if(!vendorToTechTypeToPSId.containsKey(pf.Vendor__c)){
    			vendorToTechTypeToPSId.put(pf.Vendor__c,new Map<String,Id>());
    		}
    		if(!vendorToTechTypeToPSId.get(pf.Vendor__c).containsKey(pf.Technology_Type__c)){
    			vendorToTechTypeToPSId.get(pf.Vendor__c).put(pf.Technology_Type__c,pf.Id);
    		}
    	}
    	List<Site_Postal_Check__c> pcChecksToUpdate = new List<Site_Postal_Check__c>();
    	// loop through the checks and copy the pf reference
    	for(Site_Postal_Check__c spc : [Select Id, Preferred_Supplier_Reference__c,Access_Vendor__c,Technology_Type__c 
    									From Site_Postal_Check__c 
    									Where Preferred_Supplier_Reference__c = null
                                        AND CreatedDate = LAST_MONTH
    									AND Access_Vendor__c in :vendorToTechTypeToPSId.keySet()]){
    		if(vendorToTechTypeToPSId.containsKey(spc.Access_Vendor__c)){
    			if(vendorToTechTypeToPSId.get(spc.Access_Vendor__c).containsKey(spc.Technology_Type__c)){
    				spc.Preferred_Supplier_Reference__c = vendorToTechTypeToPSId.get(spc.Access_Vendor__c).get(spc.Technology_Type__c);
    				pcChecksToUpdate.add(spc);
    			}
    		}
    	}    
    	// update the checks. This is not in a batch as we don't expect too many checks without a preferredsupplier
    	update pcChecksToUpdate;	
    }
}