global class PriceItemBatch implements Database.Batchable<sObject> {

    String query = 'SELECT Id'
                   + ', Name'
                   + ', Config_nr__c, Category__c, Regio__c'
                   + ', BOP_article_nr__c, Type__c, ConfigMenuOption__c, Description__c, Vendor__c, Region__c, Carrier__c, Codec__c, Endpoint__c'
                   + ', Channels__c, OConnet_Gross__c, OCoffnet_Gross__c, RC_Gross__c, CPE__c, OtherComponents__c, AccessLine__c, VoiceCircuit__c'
                   + ', DataCircuit__c, DataOverbooking__c, OtherHardware__c, Unit__c, Proposition__c, ProductFamily__c, AccessPortfolio__c'
                   + ', WirelessBackup__c, InstallationCode__c, From_To1__c, Amount1__c, From_To2__c, Amount2__c, From_To3__c, Amount3__c'
                   + ', From_To4__c, Amount4__c, From_To5__c, Amount5__c, ContractPeriodFrom__c, ContractPeriodTo__c, ProcessedDateTime__c'
                   + ', ValidFromDate__c, ValidToDate__c, Status__c, Errors__c'
                   + ' FROM Price_Item_Import__c WHERE Status__c = \'New\' ORDER BY CreatedDate';


    /************************************************************
    *  constructor
    ************************************************************/
    global PriceItemBatch() {
    }

    /****************************************************************
    *  start(Database.BatchableContext BC)
    *****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(this.query);
    }

    /*********************************************************************
    *  execute(Database.BatchableContext BC, List scope)
    **********************************************************************/
    global void execute(Database.BatchableContext BC, List<Price_Item_Import__c> scope) {

        List<cspmb__Price_Item__c> insertPiiList = new List<cspmb__Price_Item__c>();
        for(Price_Item_Import__c pii : scope) {
            pii.Errors__c = '';
            pii.Status__c = 'Pending';

            List<cspmb__Price_Item__c> piiList = PriceItemImportHandler.createCommercialProduct(pii);
            insertPiiList.addAll(piiList);

        }
        try {
            Set<Id> successSet = new Set<Id>();
            Map<Id, List<String>> failedMap = new Map<Id, List<String>>();
            if (insertPiiList.size() > 0) {
                List<Database.SaveResult> saveResult = Database.insert(insertPiiList, false);
                // Iterate through each returned result
                for(Integer i=0;i<saveResult.size();i++){
                    if(!saveResult[i].isSuccess()){
                        List<String> errorList;
                        if (failedMap.containsKey(insertPiiList[i].Price_Item_Import__c)) {
                            errorList = failedMap.get(insertPiiList[i].Price_Item_Import__c);
                        } else {
                            errorList = new List<String>();
                        }
                        for(Database.Error err : saveResult[i].getErrors()) {
                            errorList.add(err.getMessage());
                        }
                        failedMap.put(insertPiiList[i].Price_Item_Import__c, errorList);
                    }
                    else {
                        // Operation Success
                        successSet.add(insertPiiList[i].Price_Item_Import__c);
                    }
                }
                for (Price_Item_Import__c pii : scope) {
                    if (failedMap.containsKey(pii.Id)) {
                        pii.Status__c = 'Failed';
                        List<String> errorList = failedMap.get(pii.Id);
                        String errorMsg = '';
                        for (String error : errorList) {
                            errorMsg += error + '\n';
                        }
                        pii.Errors__c = errorMsg;
                    }
                    if (successSet.contains(pii.Id)) {
                        pii.Status__c = 'Processed';
                    }
                }
                update scope;
            }
        } catch(Exception e) {
            system.debug('##e.getMessage: '+e.getMessage());
        }

    }

    /****************************************************
    *  finish(Database.BatchableContext BC)
    *****************************************************/
    global void finish(Database.BatchableContext BC) {

    }
}