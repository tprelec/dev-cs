public with sharing class PriceItemImportHandler extends TriggerHandler {

    public override void BeforeInsert() {
        List<Price_Item_Import__c> newPIiiList= (List<Price_Item_Import__c>) this.newList;
        adjustDates(newPIiiList);
    }

    public override void AfterInsert() {
        PriceItemBatch b = New PriceItemBatch();
        database.executeBatch(b,200);
    }

    public override void BeforeUpdate() {}

    public override void AfterUpdate() {}

    /**
     * Fixes the ValidFrom and ValidTo Dates
     *
     * @param newPIiiList
     */
    private void adjustDates(List<Price_Item_Import__c> newPIiiList) {
        for (Price_Item_Import__c pii : newPIiiList) {
            if (!String.isEmpty(pii.ValidFrom__c)) {
                Date transformedDate = transformDate(pii.ValidFrom__c);
                system.debug('##transformedDate: '+transformedDate);
                if (transformedDate == null) {
                    pii.Errors__c = 'ValidFrom__c field contains no transformable date\n';
                } else {
                    pii.ValidFromDate__c = transformedDate;
                }
                system.debug('##pii.ValidFromDate__c: '+pii.ValidFromDate__c);
            }
            if (!String.isEmpty(pii.ValidTo__c)) {
                Date transformedDate = transformDate(pii.ValidTo__c);
                if (transformedDate == null) {
                    pii.Errors__c += 'ValidTo__c field contains no transformable date\n';
                } else {
                    pii.ValidToDate__c = transformedDate;
                }

            }
        }
    }

    private Date transformDate(String dateString) {
        Date returnDate;
        system.debug('##dateString: '+dateString);
        if (dateString.length() == 10) {
            try {
                returnDate = Date.newInstance(
                        Integer.valueOf(dateString.right(4)),
                        Integer.valueOf(dateString.mid(3, 2)),
                        Integer.valueOf(dateString.left(2))
                );
            } catch(Exception e) {}
        }
        /*List<String> tmpValidFromList = dateString.split('.');
        system.debug('##tmpValidFromList: '+tmpValidFromList);
        /* Check if list contains 3 items (day/month/year), if not; error = true */
        /*if (tmpValidFromList.size() == 3) {
            try {
                returnDate = Date.newInstance(
                        Integer.valueOf(tmpValidFromList[2]),
                        Integer.valueOf(tmpValidFromList[1]),
                        Integer.valueOf(tmpValidFromList[0])
                );
            } catch(Exception e) {}
        }*/
        return returnDate;
    }

    public static List<cspmb__Price_Item__c> createCommercialProduct(Price_Item_Import__c pii) {
        List<cspmb__Price_Item__c> returnList = new List<cspmb__Price_Item__c>();

        /**
         * Determine ranges
         */
        Map<Integer, Integer> rangeMap = new Map<Integer, Integer>();
        rangeMap.putAll(PriceItemImportHandler.getRangeValues(new List<String> {pii.From_To1__c, pii.From_To2__c, pii.From_To3__c, pii.From_To4__c, pii.From_To5__c}));
        system.debug('##rangeMap: '+rangeMap);

        /**
         * Loop Through the ranges and begin creating Price Items
         */
        Integer rangeCounter = 1;
        for (Integer fromRange : rangeMap.keySet()) {
            Integer toRange = rangeMap.get(fromRange);

            /** Create Price Item */
            cspmb__Price_Item__c pi = new cspmb__Price_Item__c();

            /** Name */
            pi.Name = pii.Description__c;

            /** Valid Dates */
            pi.cspmb__Effective_Start_Date__c = pii.ValidFromDate__c;
            pi.cspmb__Effective_End_Date__c = pii.ValidToDate__c;

            /** Add Duration / Range */
            pi.Min_Duration__c = fromRange;
            pi.Max_Duration__c = toRange;

            /** OneFixed Technology Type */
            pi.OneFixed_Technology_Type__c = pii.Endpoint__c;

            /** Recurring Charge Code */
            if (!String.isEmpty(pii.BOP_article_nr__c)) {
                pi.cspmb__Recurring_Charge_External_Id__c = pii.BOP_article_nr__c;
            }
            pi.cspmb__Recurring_Charge__c = pii.RC_Gross__c;

            /** One Off code */
            if (String.isEmpty(pii.InstallationCode__c)) {
                pi.cspmb__One_Off_Charge_Code__c = pii.InstallationCode__c;
            }

            /** Available bandwidth */
            if (!String.isEmpty(pii.AccessLine__c)) {
                String tmpValue = pii.AccessLine__c.toLowerCase();
                Integer downSpeed;
                Integer upSpeed;
                if (tmpValue.contains('kb')) {
                    List<String> tmpSpeed = tmpValue.split('kb');
                    try {
                        if (tmpSpeed.size() == 1) {
                            Integer speedInt = Integer.valueOf(tmpSpeed[0]);
                            downSpeed = speedInt;
                            upSpeed = downSpeed;
                        }
                    } catch(Exception e) {}
                } else if (tmpValue.contains('mb')) {
                    List<String> tmpSpeed = tmpValue.split('mb');
                    try {
                        if (tmpSpeed.size() == 1) {
                            Integer speedInt = Integer.valueOf(tmpSpeed[0]);
                            downSpeed = speedInt * 1024;
                            upSpeed = downSpeed;
                        }
                    } catch(Exception e) {}
                } else  if (tmpValue.contains('gb')) {
                    List<String> tmpSpeed = tmpValue.split('gb');
                    try {
                        if (tmpSpeed.size() == 1) {
                            Integer speedInt = Integer.valueOf(tmpSpeed[0]);
                            downSpeed = speedInt * 1024 * 1024;
                            upSpeed = downSpeed;
                        }
                    } catch(Exception e) {}
                } else  if (tmpValue.contains('/')) {
                    // First is down. second is up
                    List<String> splitKb = tmpValue.split('/');
                    downSpeed = Integer.valueOf(splitKb[0]);
                    upSpeed = Integer.valueOf(splitKb[1]);
                }
                if (downSpeed != null || upSpeed != null) {
                    pi.Available_bandwidth_up__c = upSpeed;
                    pi.Available_bandwidth_down__c = downSpeed;
                }
            }


            /** Amount */
            if (!String.isEmpty(pii.InstallationCode__c)) {
                if (rangeCounter == 1) {
                    if (pii.Amount1__c != null) {
                        pi.cspmb__One_Off_Charge__c = pii.Amount1__c;
                    }
                } else if (rangeCounter == 2) {
                    if (pii.Amount2__c != null) {
                        pi.cspmb__One_Off_Charge__c = pii.Amount2__c;
                    }
                } else if (rangeCounter == 3) {
                    if (pii.Amount3__c != null) {
                        pi.cspmb__One_Off_Charge__c = pii.Amount3__c;
                    }
                } else if (rangeCounter == 4) {
                    if (pii.Amount4__c != null) {
                        pi.cspmb__One_Off_Charge__c = pii.Amount4__c;
                    }
                } else if (rangeCounter == 5) {
                    if (pii.Amount5__c != null) {
                        pi.cspmb__One_Off_Charge__c = pii.Amount5__c;
                    }
                }
            }

            pi.Commision_allowed__c = false;

            /** */
            pi.cspmb__Price_Item_Code__c = pii.Config_nr__c;


            /** */
            pi.Vendor__c = pii.Vendor__c;


            pi.Access_Type_Multi__c  = pii.Carrier__c;

            pi.OneFixed_Codec__c  = pii.Codec__c;

            pi.OneFixed_SIP_Channels__c = pii.Channels__c;

            if (!String.isEmpty(pii.Proposition__c)) {
                pi.Bundle_Services__c = pii.Proposition__c;
            }

            if (pii.WirelessBackup__c) {
                if (!String.isEmpty(pi.Bundle_Services__c)) {
                    pi.Bundle_Services__c += ';';
                }
                pi.Bundle_Services__c += '4G';
                pi.Wireless_compatible__c  = true;
            }

            pi.Category__c = pii.Category__c;

            // 
            if (!String.isEmpty(pii.InstallationCode__c)) {
                pi.cspmb__One_Off_Charge_Code__c = pii.InstallationCode__c;
                pi.cspmb__One_Off_Charge_External_Id__c = pii.InstallationCode__c;
            }

            pi.Price_Item_Import__c = pii.Id;

            pi.Result_check__c = 'ONNET';

            /** Add Price Item to the returning list */
            system.debug('##returnList: '+returnList);
            returnList.add(pi);

            rangeCounter += 1;
        }

        List<cspmb__Price_Item__c> tmpList = new List<cspmb__Price_Item__c>();

        /** Step 2 */
        for (cspmb__Price_Item__c pi : returnList) {
            /** Add Offnet clone */
            cspmb__Price_Item__c piCloneOffnet = pi.clone(false, true);
            piCloneOffnet.Result_check__c = 'OFFNET';
            tmpList.add(piCloneOffnet);

            /** Nearnet logic */
            if (pi.Access_Type_Multi__c == 'EthernetOverFiber') {
                if (pi.Vendor__c == 'ZIGGO') {
                    cspmb__Price_Item__c piCloneNearnet100 = pi.clone(false, true);
                    piCloneNearnet100.Result_check__c = 'NEARNET_UPTO100M';
                    tmpList.add(piCloneNearnet100);

                    cspmb__Price_Item__c piCloneNearnet250 = pi.clone(false, true);
                    piCloneNearnet250.Result_check__c = 'NEARNET_UPTO250M';
                    tmpList.add(piCloneNearnet250);
                } else {
                    cspmb__Price_Item__c piCloneNearnet = pi.clone(false, true);
                    piCloneNearnet.Result_check__c = 'NEARNET';
                    tmpList.add(piCloneNearnet);
                }

            }
        }
        if (tmpList.size() > 0) {
            returnList.addAll(tmpList);
        }

        return returnList;
    }


    // Group Association Method
    //
    // Loop door CP's
    //
    //
    /*public static void createGroupAssociationRecords(List<cspmb__Price_Item__c> bundleRecords) {
     *  List<Product_Group_Association__c> assRecList = new List<Product_Group_Association__c>();
        List<cspmb__Price_Item__c> allNotBundles = [Select Id FROM cspmb__Price_Item__c Where  Active = true];
        for (cspmb__Price_Item__c pi: bundleRecords) {
            // Access
            // if (Category == 'Access')
                    if (Access_Type_Multi__c != EthernetOverFiber) {
                    	pi.Available_bandwidth_down__c <= singleRecord.Available_bandwidth_down__c
						pi.Available_bandwidth_up__c <= singleRecord.Available_bandwidth_up__c
                    }

                //if (pi.Carrier__c == singleRecord.Carrier__c
                //&& pi.Vendor__c == singleRecord.Vendor__c
                //&& (pi.Region__c == singleRecord.Region__c || Region__c == '*')
                //&& pi.Result_check__c == singleRecord.Result_check__c
                //&& pi.Min_Duration__c >= singleRecord.Min_Duration__c
                //&& pi.Max_Duration__c < singleRecord.Max_Duration__c
                //
                //

			Product_Group_Association__c groupAssRec = new Product_Group_Association__c();
			groupAssRec.Primary_Price_Item__c = pi.Id;
			groupAssRec.Secondary_Price_Item__c = singleRecord.Id;
			groupAssRec.Default_Quantity__c = '';
			assRecList.add(groupAssRec);
            insert assRecList;
			//
        }

    }*/


    /**
     * Split the ranges in the FromTo fields
     *
     * @param rangeList List<String>
     *
     * @return Map<Integer, Integer>
     */
    public static Map<Integer, Integer> getRangeValues(List<String> rangeList) {
        Map<Integer, Integer> returnMap = new Map<Integer, Integer>();
        for (String range : rangeList) {
            if (!String.isEmpty(range)) {
                List<String> tmprangeList = range.split('-');
                try {
                    if (tmprangeList.size() == 2) {
                        returnMap.put(Integer.valueOf(tmprangeList[0]), Integer.valueOf(tmprangeList[1]));
                    }
                } catch (Exception e) {}
            }
        }
        return returnMap;
    }
}