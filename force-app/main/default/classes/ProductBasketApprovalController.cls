public with sharing class ProductBasketApprovalController {

	Id basketId {get;set;}

	public ProductBasketApprovalController(ApexPages.StandardController controller) {
		basketId = ApexPages.Currentpage().getParameters().get('Id');
	}

	public pageReference BackToBasket(){
		return new PageReference('/'+basketId);
	}
}