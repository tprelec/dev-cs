/*
 *    @Description:    Used for controlling the product basket attachments management page. Requires WITHOUT SHARING as other ways to edit BasketAttachments will be disabled
 *    @Author:         Guy Clairbois
 */
public without sharing class ProductBasketAttachmentManagerController {

    /*
     *    Variables
     */
    public Id basketId {get;set;}
    public cscfga__Product_Basket__c basket {get;set;}
    public List<AttachmentWrapper> basketAttachments {get;set;}
    public List<AttachmentWrapper> newBasketAttachments {get;set;}
    public string SelectedBaId { get; set; }
    public Boolean errorFound {get;set;}
    public Boolean hasEditRightsOnBasket {get;set;}
    
    // the number of new attachments to add to the list when the user clicks 'Add More'
    public static final Integer NUM_ATTACHMENTS_TO_ADD=5;

    /*
     *    Controller
     */

    public ProductBasketAttachmentManagerController () {
        basketId = Apexpages.currentPage().getParameters().get('basketId');
        errorFound = false;
        if(basketAttachments==null){
            LoadData();
            addMore();
        }
        
    }

    public ProductBasketAttachmentManagerController(ApexPages.StandardController controller) {
        BasketId = controller.getId();
        errorFound = false;
        if(basketAttachments==null){
            LoadData();
            addMore();
        }
    }  

    private void LoadData(){
        BasketAttachments = new List<AttachmentWrapper>();
        newBasketAttachments = new List<AttachmentWrapper>();
        
        // get Basket
        List<cscfga__Product_Basket__c> Baskets = [Select 
                                                   Id, 
                                                   Name,
                                                   csbb__Account__c
                                                   from 
                                                   cscfga__Product_Basket__c 
                                                   Where 
                                                   Id = :BasketId];
        if(!Baskets.isEmpty()){
            Basket = Baskets[0];
        } else {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.ERROR,'Basket not found'));
            errorFound = true;
            return;
        }

        // check Basket access
        UserRecordAccess ua = [SELECT 
                               RecordId, 
                               HasEditAccess 
                               FROM 
                               UserRecordAccess
                               WHERE
                               UserId = :System.UserInfo.getUserId()
                               AND RecordId = :Basket.Id];
        hasEditRightsOnBasket = ua.HasEditAccess;

        // get Basket attachments
        Map<Id,AttachmentWrapper> fileMap = new Map<Id,AttachmentWrapper>();        // Maps contentDocumentID to wrapper
        Set<ID> contentSet = new Set<ID>();
        Map<Id,Product_Basket_Attachment__c> pbaMap = new Map<Id,Product_Basket_Attachment__c>();
        List<Product_Basket_Attachment__c> pbaList = new List<Product_Basket_Attachment__c>(); // use list to maintain sorted order
        for(Product_Basket_Attachment__c pba :[Select 
                                              Id, 
                                              Name,
                                              Attachment_Type__c, 
                                              ContentDocumentID__c,
                                              Description_Optional__c,
                                              CreatedById,
                                              CreatedDate
                                              From 
                                              Product_Basket_Attachment__c 
                                              Where 
                                              Product_Basket__c = :BasketId
                                              Order By Attachment_Type__c
                                             ])
        {
            pbaList.add(pba);
            pbaMap.put(pba.Id,pba);
        }

        // 1 - Get the ContentDocumentLink Records for the basket attachments
        Set<ID> pbaSet = pbaMap.keySet();
        if (pbaSet.size()>0) {
            for (ContentDocumentLink link : [select ContentDocumentId, LinkedEntityId from ContentDocumentLink where LinkedEntityId in :pbaSet]) {
                // 2 - Use results to Populate a Map (fileMap) where key is contentDocumentID and result is new AttachmentWrapper
                Product_Basket_Attachment__c pba = pbaMap.get(link.LinkedEntityId);
                fileMap.put(link.ContentDocumentId,new AttachmentWrapper(pba,null));
                contentSet.add(link.ContentDocumentId);      
                if (pba.ContentDocumentID__c==null) pba.ContentDocumentID__c=link.ContentDocumentId;
            }
            
            // 3 - Get the contentversion records for the contentDocuments
            // Get the contentversion record for each opportunity attachment
            for (contentversion file : [select id, ContentDocumentId, pathOnClient from contentversion where contentdocumentid in:contentSet and isLatest=true]) {
                // 4 - Use the fileMap to get the wrapper and populate with the contentversion      
                fileMap.get(file.ContentDocumentId).file = file;
            }
            
            
            for(Product_Basket_Attachment__c pba : pbaList){
                // now go through list to maintain sort order
                BasketAttachments.add(fileMap.get(pba.ContentDocumentID__c));
            }
            
        }
    }

    public class AttachmentWrapper {
        public Product_Basket_Attachment__c pba {get;set;}
        public ContentVersion file {get;set;}
        public String type {get;set;}
        
        public AttachmentWrapper(Product_Basket_Attachment__c pba, ContentVersion file){
            this.pba = pba;
            if(file!= null){
                this.file = file;
            }        
        }
    }

    /*
     *    Page References
     */
    
    public pageReference addMore(){
        for (Integer idx=0; idx<NUM_ATTACHMENTS_TO_ADD; idx++){
            
            newBasketAttachments.add(new AttachmentWrapper(new Product_Basket_Attachment__c(Product_Basket__c = BasketId),new ContentVersion()));
        }
        return null;
    }
    
    public pageReference backToBasket(){
        return new PageReference('/'+BasketId);
    }

    // for the embedded page, this is the link to the actual attachmentmanager
    public pageReference manageAttachments(){
        return new PageReference('/apex/ProductBasketAttachmentManager?BasketId='+BasketId);
    }

    
    // Note: There is two way linking happening here. The native object ContentDocumentLink links to the basket_attachment__c
    // and the basket_attachment__c links to ContentDocument. This second link is populated in ContentDocumentLinkTriggerHandler
    public void saveAttachments(){
    
        List<Product_Basket_Attachment__c> basketAttachmentsToInsert = new List<Product_Basket_Attachment__c>();
        List<Product_Basket_Attachment__c> basketAttachmentsToUpdate = new List<Product_Basket_Attachment__c>();
        List<ContentDocumentLink> contentDocumentLinkToInsert = new List<ContentDocumentLink>();
        List<ContentVersion> contentVersionToInsert = new List<ContentVersion>();
        List<ContentVersion> contentVersionList = new List<ContentVersion>();
        for(AttachmentWrapper aw : newBasketAttachments){
            if(aw.pba != null && aw.pba.Attachment_Type__c != null) basketAttachmentsToInsert.add(aw.pba);
          
        }
        Savepoint sp = Database.setSavepoint();
        try{
            insert basketAttachmentsToInsert;
            
            Integer counter = 0;
            for(Product_Basket_Attachment__c pba : basketAttachmentsToInsert){
                
                if(pba.Id != null){
                    system.debug(newBasketAttachments[counter]);
                    if(newBasketAttachments[counter].file != null){
                        
                        // Hijack this field to store the Basket Attachment Id so we can link it back later on
                        newBasketAttachments[counter].file.ReasonForChange = pba.Id;
                        
                        contentVersionToInsert.add(newBasketAttachments[counter].file);
                    }
                }
                counter++;
            }
            
            // Insert the files (ContentVersion)
            insert contentVersionToInsert;  
            
            // Create the links between the content document and the basket attachment
            // and store the content document id on the basket attachment
            contentVersionList = [SELECT ContentDocumentId, ReasonForChange FROM ContentVersion WHERE Id = :contentVersionToInsert];
            for (ContentVersion cv:contentVersionList) {
                ContentDocumentLink cdl = new ContentDocumentLink();        
                cdl.ContentDocumentId = cv.ContentDocumentId;
                cdl.LinkedEntityId = cv.ReasonForChange;
                cdl.ShareType = 'V';    
                cdl.Visibility  = 'Allusers'; 
                contentDocumentLinkToInsert.add(cdl);                
            }  
            insert contentDocumentLinkToInsert;          
            
            // process any attachment type updates
            for(AttachmentWrapper aw : basketAttachments){
                if (aw.pba!=null) {
                    if(aw.type != aw.pba.Attachment_Type__c){
                        basketAttachmentsToUpdate.add(aw.pba);
                    }
                }
            }
            update basketAttachmentsToUpdate;
            
            // reset datasets
            newBasketAttachments.clear();
            basketAttachments = null;
            
            
        } catch (dmlException de){
            Database.rollback(sp);
            Apexpages.addMessages(de);
            system.debug(de);
            system.debug(de.getLineNumber());
        }
          
          
        //refresh the data
        LoadData();
        newBasketAttachments.clear();    
    }       
      

    public void DeleteBasketAttachment(){
        // if for any reason we are missing the reference 
        if (SelectedBaId== null) {
           return;
        }
     
        Delete New Product_Basket_Attachment__c(Id=SelectedBaId);
      
     
        //refresh the data
        LoadData();
    }    

}