public class ProductBasketOverrideController {

    @AuraEnabled
    public static String checkUserAccess(String parentId) {
        String returnString = '';
        Boolean oracleQuoteExist = false;
        try {
            /**
             * Create Basket
             */
            Opportunity opp = [Select Id, OwnerId From Opportunity Where Id =: parentId];

            /**
            * Check to see if any Oracle Quotes Exist
            */
            List<BigMachines__Quote__c> qList = [Select Id From BigMachines__Quote__c Where BigMachines__Opportunity__c =: parentId];
            if (qList.size() > 0) {
                oracleQuoteExist = true;
            }

            if (!oracleQuoteExist) {
                String profileName = OpportunityUtils.getUserProfile(opp.OwnerId);
                List<New_Quote_Profile__mdt> quoteProfileList = [Select profile__c from New_Quote_Profile__mdt where profile__c = :profileName];
                if (quoteProfileList.size() == 1) {
                    cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
                    basket.cscfga__Opportunity__c = parentId;
                    insert basket;
                    returnString = basket.Id;
                }
            }
        } catch(Exception e) {}

        if (oracleQuoteExist) {
            returnString = 'oracle';
        }

        return returnString;
    }
}