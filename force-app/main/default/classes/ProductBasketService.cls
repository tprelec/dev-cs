/**
 * @description Service class for operations on Product Basket object
 */

public with sharing class ProductBasketService {

    /**
     * @description Queries detail fields for a given Product Basket record. Used by the RemoteActionProvider class.
     *
     * @param productBasketId ID of the product basket record to query the details for
     *
     * @return Product Basket record with the given ID and all necessary fields queried
     */
    public cscfga__Product_Basket__c queryProductBasketDetails(Id productBasketId) {
        return [
                SELECT Id,
                        Name,
                        cscfga__Opportunity__r.Direct_Indirect__c
                FROM cscfga__Product_Basket__c
                WHERE Id = :productBasketId
        ];
    }

}