global class ProductBasketUploadedEventHandler extends csac.RestApiEventHandler {

    public override csac.RestApiResponse handleEvent(Object payload) {
        System.debug(payload);
        if (payload == null) {
            return new csac.RestApiResponse('Failure!', 'Payload object is null.');
        }
        String basketId = '';
        if (payload instanceof Map<String, String>) {
            Map<String, String> basekt = (Map<String, String>)payload;
            basketId = basekt.get('Id');
        }
        if (String.isNotBlank(basketId)) {
            Id bsId = (Id)basketId.removeEnd('?');
            basketId = basketId.removeEnd('?');
            createOlisAndProductDetails(basketId);
            return new csac.RestApiResponse('Success!', 'After-basket-upload job is done.');
        } else {
            return new csac.RestApiResponse('Failure!', 'Basket ID is empty, impossible.');
        }
    }

    @future
    public static void createOlisAndProductDetails(Id basketId) {
        DoorToDoorBasketService d2dBasketSvc = new DoorToDoorBasketService(basketId);
        d2dBasketSvc.convertLeadAndSyncBasket();
    }

}