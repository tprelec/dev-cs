@isTest
private class ProductBasketUploadedEventHandlerTest {
	@testSetup
	private static void makeData() {
		Account account = LG_GeneralTest.createAccount('Account', '12345678', 'Ziggo', true);
		Opportunity opp = LG_GeneralTest.createOpportunity(account, true);

		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket(
			'Basket Test Handler',
			account,
			null,
			opp,
			false
		);
		basket.LG_CreatedFrom__c = 'Tablet';
		insert basket;

		cscfga__Product_Category__c prodCategory = LG_GeneralTest.createProductCategory(
			'Test Category',
			true
		);

		cscfga__Product_Definition__c prodDefinitionTv = LG_GeneralTest.createProductDefinition(
			'TV',
			false
		);
		prodDefinitionTv.cscfga__Product_Category__c = prodCategory.Id;
		insert prodDefinitionTv;

		cscfga__Product_Definition__c prodDefinitionMkbInternet = LG_GeneralTest.createProductDefinition(
			'Zakelijk Internet Pro',
			false
		);
		prodDefinitionMkbInternet.cscfga__Product_Category__c = prodCategory.Id;
		insert prodDefinitionMkbInternet;

		cscfga__Product_Configuration__c prodConfigurationTv = LG_GeneralTest.createProductConfiguration(
			'TV Configuration',
			3,
			basket,
			prodDefinitionTv,
			false
		);
		prodConfigurationTv.cscfga__Product_Family__c = 'TV';
		prodConfigurationTv.LG_MarketSegment__c = 'SoHo';
		insert prodConfigurationTv;

		cscfga__Product_Configuration__c prodConfigurationMkbInternet = LG_GeneralTest.createProductConfiguration(
			'MKB Configuration',
			3,
			basket,
			prodDefinitionMkbInternet,
			false
		);
		prodConfigurationMkbInternet.cscfga__Product_Family__c = 'Zakelijk Internet Pro';
		prodConfigurationMkbInternet.LG_MarketSegment__c = 'Small';
		insert prodConfigurationMkbInternet;

		TestUtils.autoCommit = false;
		OrderType__c orderType = TestUtils.createOrderType();
		orderType.Name = 'Ziggo';
		insert orderType;
	}

	@isTest
	private static void testBasketUploadPositive() {
		cscfga__Product_Basket__c basket = getBasket();
		Map<String, String> basketPayload = new Map<String, String>();
		basketPayload.put('Id', basket.Id);

		Test.startTest();
		ProductBasketUploadedEventHandler handler = new ProductBasketUploadedEventHandler();
		handler.handleEvent(basketPayload);
		Test.stopTest();

		basket = getBasket();
		System.assertEquals(
			true,
			basket.csbb__Synchronised_With_Opportunity__c,
			'Basket should be synchronized to Opportunity'
		);
	}

	@isTest
	private static void testBasketUploadNegative() {
		cscfga__Product_Basket__c basket = getBasket();
		Map<String, String> basketPayload = new Map<String, String>();

		Test.startTest();
		ProductBasketUploadedEventHandler handler = new ProductBasketUploadedEventHandler();
		handler.handleEvent(basketPayload);
		Test.stopTest();

		basket = getBasket();
		System.assertEquals(
			false,
			basket.csbb__Synchronised_With_Opportunity__c,
			'Basket shouldn\'t be synchronized to Opportunity'
		);
	}

	private static cscfga__Product_Basket__c getBasket() {
		return [
			SELECT Id, csbb__Synchronised_With_Opportunity__c, Error_Message__c
			FROM cscfga__Product_Basket__c
		];
	}
}