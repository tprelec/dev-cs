global class ProductDefinitionClearCacheController {
 
    private final cscfga__Product_Definition__c obj;
     
    public ProductDefinitionClearCacheController(ApexPages.StandardController stdController) {
        this.obj = (cscfga__Product_Definition__c)stdController.getRecord();
    }
 
 
    @RemoteAction
    global static String clearCache() {
     
        String retval = 'ok';
     
        try {
 
            cssmgnt__SM_Options__c processingOptions = cssmgnt__SM_Options__c.getInstance(UserInfo.getUserId());
            HttpRequest req = new HttpRequest();
            req.setHeader('content-type', cssmgnt.Constants.HEROKU_CONTENT_TYPE);
            req.setHeader('Organization', UserInfo.getOrganizationId().substring(0, 15));
            req.setTimeout(30000);
            req.setMethod('POST');
            req.setEndpoint(processingOptions.cssmgnt__endpoint__c + '/flushCache');
            Map<String, Object> requestMap = new Map<String, Object>();
            String body = JSON.serialize(requestMap);
            String payloadHash = cssmgnt.ConfiguratorHerokuServiceSecurity.calculateRsaPayloadHash(body, cssmgnt.Constants.HEROKU_CONTENT_TYPE);
            req.setBody(body);
            req.setHeader(
                        'Authorization',
                        cssmgnt.ConfiguratorHerokuServiceSecurity.generateRsaAuthorizationHeader(
                            processingOptions.cssmgnt__certificate_name__c,
                            UserInfo.getOrganizationId().substring(0, 15),
                            cssmgnt.Constants.HTTP_POST,
                            '/flushCache',
                            payloadHash
                        )
                    );
            Http http = new Http();
            HTTPResponse res = http.send(req);
            System.debug(res.getBody());
             
        } catch (Exception ex) {
            retval = ex.getMessage();
        }
         
        return retval;
    }
}