/**
 * Created by miaaugustinovic on 12/04/2021.
 */
@isTest
public with sharing class ProductDefinitionClearCacheTest {

    @isTest
    public static void clearCache() {

        Test.startTest();

        String retval = ProductDefinitionClearCacheController.clearCache();

        Test.stopTest();

        System.assertNotEquals(null, retval, 'Invalid data');
    }
}