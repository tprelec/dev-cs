/**
 * @description         This is the trigger handler for the Product2 sObject. 
 * @author              Guy Clairbois
 */
public with sharing class ProductTriggerHandler extends TriggerHandler {

	/**
    * @description         This handles the before insert trigger event.
    */
    public override void BeforeInsert(){
        List<Product2> newProducts = (List<Product2>) this.newList;        
        
    }

    /**
     * @description         This handles the after insert trigger event.
     */
    public override void AfterInsert(){
        List<Product2> newProducts = (List<Product2>) this.newList;
        Map<Id,Product2> newProductsMap = (Map<Id,Product2>) this.newMap;

        createHistory(newProducts,null);
        createPriceBookEntry(newProductsMap);
    }
    

    /**
	* @description         This handles the before update trigger event.
	*/
	public override void BeforeUpdate(){
		List<Product2> newProducts = (List<Product2>) this.newList;

		fillThresholdGroup(newProducts);
	}


    /**
     * @description         This handles the after update trigger event.
     */
    public override void AfterUpdate(){
        List<Product2> newProducts = (List<Product2>) this.newList;   
        List<Product2> oldProducts = (List<Product2>) this.oldList;
        Map<Id,Product2> newProductsMap = (Map<Id,Product2>) this.newMap;        
        Map<Id,Product2> oldProductsMap = (Map<Id,Product2>) this.oldMap;
 
        createHistory(newProducts,oldProductsMap);
        createPriceBookEntry(newProductsMap);
    }


    /**
     * @description         This handles the before delete trigger event.
     */
    public override void BeforeDelete(){
        List<Product2> newProducts = (List<Product2>) this.newList;   
        List<Product2> oldProducts = (List<Product2>) this.oldList;
        Map<Id,Product2> oldProductsMap = (Map<Id,Product2>) this.oldMap;
 
        checkReferences(oldProductsMap);
        deleteHistory(oldProductsMap);
    }

    private void createHistory(List<Product2> newProducts, Map<Id,Product2> oldProductsMap){
    	List<Product_History__c> prodHistoryToInsert = new List<Product_History__c>();
    	Map<String, Schema.SObjectField> productFieldMap = Schema.SObjectType.Product2.fields.getMap();

    	for(Product2 prod : newProducts){

    		if(oldProductsMap == null){
    			// insert -> create new history
    			Product_History__c ph = createPH(prod);
    			ph.Field_name__c = 'Created.'; // required field..
    			prodHistoryToInsert.add(ph);
    		} else {
    			// loop through all fields to see if they changed
				for(String field: productFieldMap.keyset()){
					if(productFieldMap.get(field).getDescribe().isUpdateable()){
						if(prod.get(field) != oldProductsMap.get(prod.Id).get(field)){
							Product_History__c ph = createPH(prod);
							ph.Field_name__c = productFieldMap.get(field).getDescribe().getLabel();
							ph.Old_Value__c = String.valueOf(oldProductsMap.get(prod.Id).get(field));
							ph.New_Value__c = String.valueOf(prod.get(field));
                            ph.Old_Value__c = (ph.Old_Value__c==null?null:StringUtils.truncate(ph.Old_Value__c,255));
                            ph.New_Value__c = (ph.New_Value__c==null?null:StringUtils.truncate(ph.New_Value__c,255));
							prodHistoryToInsert.add(ph);
						}				
					}
				}
    		}
    	}
    	insert prodHistoryToInsert;
    }

    private void deleteHistory(Map<Id,Product2> oldProductsMap){
    	Delete [Select Id From Product_History__c Where Product__c in :oldProductsMap.keySet()];
    }    

    private Product_History__c createPH(Product2 prod){
		Product_History__c ph = new Product_History__c();
		ph.Product__c = prod.Id;
		ph.User__c = UserInfo.getUserId();
		ph.Date__c = prod.LastModifiedDate;    	

		return ph;
    }

    /**
     * @description     Prevent deletion if there are referenced BMQuoteProducts or ContractedProducts
     */
    private void checkReferences(Map<Id,Product2> oldProductsMap){
        Set<Id> prodIdsToBlock = new Set<Id>();
        for(BigMachines__Quote_Product__c qp : [Select Id,BigMachines__Product__c From BigMachines__Quote_Product__c Where BigMachines__Product__c in :oldProductsMap.keySet()]){
            prodIdsToBlock.add(qp.BigMachines__Product__c);
        }
        for(Contracted_Products__c cp : [Select Id,Product__c From Contracted_Products__c Where Product__c in :oldProductsMap.keySet()]){
            prodIdsToBlock.add(cp.Product__c);
        }        
        for(Product2 p : oldProductsMap.values()){
            if(prodIdsToBlock.contains(p.Id)){
                p.addError('Product is referenced in a BigMachines Quote or in a Contracted Product so cannot be deleted. Consider deactivating the product instead.');
            }
        }
        
    }    


    /**
     * @description         Automatically create/update a pricebookentry based on the Product2 BAP_SAP__c field.
     */
    private void createPriceBookEntry(Map<Id,Product2> newProductsMap){
        Id pbId;
        if(Test.isRunningTest()){
            pbId = Test.getStandardPricebookId();
        } else {
            pbId = [Select id from Pricebook2 where isStandard=true limit 1].Id;   
        }
        List<PricebookEntry> pbeToUpsert = new List<PricebookEntry>();

        // collect all existing pricebookentries
        Map<Id,PricebookEntry> prodIdToPricebookEntry = new Map<Id,Pricebookentry>();
        for(PricebookEntry pe : [Select Id, Product2Id, UnitPrice From PricebookEntry Where Product2Id in :newProductsMap.keySet()]){
            prodIdToPricebookEntry.put(pe.Product2Id,pe);
        }

        for(Product2 p : newProductsMap.values()){
            
            if(prodIdToPricebookEntry.containsKey(p.Id)){
                // if price is different, update
                if(p.BAP_SAP__c != prodIdToPricebookEntry.get(p.Id).UnitPrice){
                	if(p.BAP_SAP__c != null){
                		prodIdToPricebookEntry.get(p.Id).UnitPrice = p.BAP_SAP__c;
                	} else {
                		prodIdToPricebookEntry.get(p.Id).UnitPrice = 0.0;
                	}                    
                    pbeToUpsert.add(prodIdToPricebookEntry.get(p.Id));
                }

            } else {
                if(p.BAP_SAP__c != null){
                    // if no entry exists, create one
                    PricebookEntry priceBookEntry = new PricebookEntry( IsActive = true, Product2Id = p.id, UnitPrice = p.BAP_SAP__c, Pricebook2Id = pbId);        
                    pbeToUpsert.add(priceBookEntry);
                }
            }         

        }
        upsert pbeToUpsert;
    }

	/**
	 * @description		Fills the Thresholdgroup field if it is empty.
	 */
	private void fillThresholdGroup(List<Product2> newProducts){
		for(Product2 product : newProducts){
			if(product.BigMachines__Part_Number__c != null && product.ThresholdGroup__c == null){
				if(product.BigMachines__Part_Number__c.contains('_')){
					product.ThresholdGroup__c = product.BigMachines__Part_Number__c.substring(0, (product.BigMachines__Part_Number__c.lastIndexOf('_')));
				} else {
					product.ThresholdGroup__c = product.BigMachines__Part_Number__c;
				}
			}
		}
	}
}