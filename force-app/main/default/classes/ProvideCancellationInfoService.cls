@RestResource(urlMapping='/providecancellationinfo/*')
/**
 * @description: Cancels the installed base items in Salesforce for rollback purposes
 * @author: Jurgen van Westreenen
 */
global without sharing class ProvideCancellationInfoService {
	public static final String BILLING_STATUS_NEW = 'New';
	public static final String BILLING_STATUS_FAILED = 'Failed';
	public static final String DELIVERY_STATUS_OPEN = 'OPEN';
	public static final String DELIVERY_STATUS_CLOSED = 'CLOSED';

	private static List<Error> returnErrors = new List<Error>();
	// private static List<Customer_Asset__c> parentCustAssets = new List<Customer_Asset__c>();
	private static Map<String, CancellationResult> ordIdMap = new Map<String, CancellationResult>();

	@HttpPost
	global static void cancelOrderLineItems() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;

		ProvideCancellationInfo body = (ProvideCancellationInfo) JSON.deserializeStrict(
			req.requestbody.tostring(),
			ProvideCancellationInfo.class
		);

		String transactionId = body.transactionID;
		List<Order_Billing_Transaction__c> ordBillTrans = [
			SELECT Id, Callback_Message__c, Transaction_Id__c
			FROM Order_Billing_Transaction__c
			WHERE Transaction_Id__c = :transactionId
			LIMIT 1
		];
		if (ordBillTrans.size() > 0) {
			ordBillTrans[0].Callback_Message__c = req.requestbody.tostring();
			update ordBillTrans;
		} else {
			Error cpErr = new Error();
			cpErr.errorCode = 'SFEC-9999';
			cpErr.errorMessage = 'Record not found with transactionID: ' + transactionId;
			returnErrors.add(cpErr);
		}

		for (CancellationResult canRes : body.cancellationResults) {
			ordIdMap.put(canRes.orderId, canRes);
		}
		Set<String> orderIds = ordIdMap.keySet();

		List<Contracted_Products__c> contProds = [
			SELECT
				Id,
				Billing_Order_Id__c,
				Error_Info__c,
				Customer_Asset__c,
				Customer_Asset__r.Installed_Base_Id__c
			FROM Contracted_Products__c
			WHERE Billing_Order_Id__c IN :orderIds
		];

		updateContProducts(contProds, orderIds);

		// updateCustomerAssets(parentCustAssets);

		for (String refId : orderIds) {
			Error cpErr = new Error();
			cpErr.errorCode = 'SFEC-9999';
			cpErr.errorMessage = 'Record not found with orderId: ' + refId;
			returnErrors.add(cpErr);
		}
		Response resp = new Response();
		resp.status = returnErrors.size() > 0 ? 'FAILED' : 'OK';
		resp.errors = returnErrors;
		res.addHeader('Content-Type', 'text/json');
		res.statusCode = 200;
		res.responseBody = Blob.valueOf(JSON.serializePretty(resp));
	}

	private static void updateContProducts(
		List<Contracted_Products__c> cpList,
		Set<String> orderIds
	) {
		for (Contracted_Products__c contProd : cpList) {
			CancellationResult canResult = ordIdMap.get(contProd.Billing_Order_Id__c);
			// Remove matched ref id's from list
			orderIds.remove(contProd.Billing_Order_Id__c);
			if (canResult.result == 'OK') {
				contProd.Billing_Status__c = BILLING_STATUS_NEW;
				contProd.Delivery_Status__c = DELIVERY_STATUS_OPEN;
				contProd.Credit__c = false;
			} else {
				for (Error err : canResult.errors) {
					contProd.Error_Info__c += err.errorMessage;
					contProd.Billing_Status__c = BILLING_STATUS_FAILED;
				}
			}
		}
		Database.SaveResult[] srProductList = Database.update(cpList, false);
		for (Integer i = 0; i < srProductList.size(); i++) {
			if (!srProductList[i].isSuccess()) {
				for (Database.Error err : srProductList[i].getErrors()) {
					Error cpErr = new Error();
					cpErr.errorCode = 'SFEC-9999';
					cpErr.errorMessage =
						'Failed to update record with orderId: ' +
						cpList[i].Billing_Order_Id__c +
						'; ' +
						err.getMessage();
					returnErrors.add(cpErr);
				}
				// } else {
				// 	parentCustAssets.add(cpList[i].Customer_Asset__r);
			}
		}
	}
	/* 
	private static void updateCustomerAssets(List<Customer_Asset__c> caList) {
		for (Customer_Asset__c ca : caList) {
			ca.Latest_Activation_Date__c = null;
			ca.Cost_Center__c = null;
			ca.PO_Number__c = null;
			ca.Quantity__c = null;
		}
		Database.SaveResult[] srAssetList = Database.update(caList, false);
		for (Integer i = 0; i < srAssetList.size(); i++) {
			if (!srAssetList[i].isSuccess()) {
				for (Database.Error err : srAssetList[i].getErrors()) {
					Error cpErr = new Error();
					cpErr.errorCode = 'SFEC-9999';
					cpErr.errorMessage =
						'Failed to update record with Installed Base Id: ' +
						caList[i].Installed_Base_Id__c +
						'; ' +
						err.getMessage();
					returnErrors.add(cpErr);
				}
			}
		}
	}
     */
	global class ProvideCancellationInfo {
		public String transactionID;
		public List<CancellationResult> cancellationResults;
	}
	global class CancellationResult {
		public String orderId;
		public String result;
		public List<Error> errors;
	}
	global class Error {
		public String errorCode;
		public String errorMessage;
	}
	global class Response {
		public String status;
		public List<Error> errors;
	}
}