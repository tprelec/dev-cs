@RestResource(urlMapping='/providecreditinfo/*')
/**
 * @description: Updates the Status of the Credit Lines
 * @author: Jurgen van Westreenen
 */
global without sharing class ProvideCreditInfoService {
	private static List<Error> returnErrors = new List<Error>();

	@HttpPost
	global static void updateCreditLineItems() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;

		// Deserialize JSON
		ProvideCreditInfo body = (ProvideCreditInfo) JSON.deserializeStrict(
			req.requestbody.tostring(),
			ProvideCreditInfo.class
		);

		String transactionId = body.transactionID;
		List<Credit_Note__c> credNotes = [
			SELECT Id, Transaction_Id__c, (SELECT Id, Name, Error_Info__c FROM Credit_Lines__r)
			FROM Credit_Note__c
			WHERE Transaction_Id__c = :transactionId
			LIMIT 1
		];
		if (credNotes.size() == 0) {
			Error cpErr = new Error();
			cpErr.errorCode = 'SFEC-9999';
			cpErr.errorMessage = 'Record not found with transactionID: ' + transactionId;
			returnErrors.add(cpErr);
		} else {
			processRequest(body.creditLineResults, credNotes[0].Credit_Lines__r);
		}

		// Build response
		Response resp = new Response();
		resp.status = returnErrors.size() > 0 ? 'FAILED' : 'OK';
		resp.errors = returnErrors;
		res.addHeader('Content-Type', 'text/json');
		res.statusCode = 200;
		res.responseBody = Blob.valueOf(JSON.serializePretty(resp));
	}

	private static void processRequest(
		List<CreditLineResult> clResults,
		List<Credit_Line__c> clItems
	) {
		Map<String, Credit_Line__c> clMap = new Map<String, Credit_Line__c>();
		List<Credit_Line__c> clToUpdate = new List<Credit_Line__c>();
		for (Credit_Line__c cl : clItems) {
			clMap.put(cl.Name, cl);
		}
		for (CreditLineResult clr : clResults) {
			if (clMap.containsKey(clr.sfdcCreditId)) {
				Credit_Line__c credLine = clMap.get(clr.sfdcCreditId);
				if (clr.result == 'OK') {
					credLine.Status__c = 'Billing Processed';
					credLine.Unify_Credit_Id__c = clr.creditId;
				} else {
					for (Error err : clr.errors) {
						credLine.Error_Info__c = String.isNotBlank(credLine.Error_Info__c)
							? credLine.Error_Info__c + '\n' + err.errorMessage
							: err.errorMessage;
					}
					credLine.Status__c = 'Billing Error';
				}
				clToUpdate.add(credLine);
			} else {
				Error cpErr = new Error();
				cpErr.errorCode = 'SFEC-9999';
				cpErr.errorMessage = 'Record not found with sfdcCreditId: ' + clr.sfdcCreditId;
				returnErrors.add(cpErr);
			}
		}
		updCreditLines(clToUpdate);
	}

	private static void updCreditLines(List<Credit_Line__c> clToUpdate) {
		Database.SaveResult[] credLineList = Database.update(clToUpdate, false);
		for (Integer i = 0; i < credLineList.size(); i++) {
			if (!credLineList[i].isSuccess()) {
				// Operation failed, so get all errors
				for (Database.Error err : credLineList[i].getErrors()) {
					Error cpErr = new Error();
					cpErr.errorCode = 'SFEC-9999';
					cpErr.errorMessage =
						'Failed to update record with sfdcCreditId: ' +
						clToUpdate[i].Id +
						'; ' +
						err.getMessage();
					returnErrors.add(cpErr);
				}
			}
		}
	}

	@TestVisible
	class ProvideCreditInfo {
		@TestVisible
		String transactionID;
		@TestVisible
		List<CreditLineResult> creditLineResults;
	}
	@TestVisible
	class CreditLineResult {
		@TestVisible
		String sfdcCreditId;
		@TestVisible
		String creditId;
		@TestVisible
		String result;
		@TestVisible
		List<Error> errors;
	}
	@TestVisible
	class Error {
		@TestVisible
		String errorCode;
		@TestVisible
		String errorMessage;
	}
	@TestVisible
	class Response {
		@TestVisible
		String status;
		@TestVisible
		List<Error> errors;
	}
}