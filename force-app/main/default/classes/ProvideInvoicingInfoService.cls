@RestResource(urlMapping='/provideinvoicinginfo/*')
/**
 * @description: Updates the installed base items in Salesforce with a reference to Invoicing
 * @author: Jurgen van Westreenen
 */
global without sharing class ProvideInvoicingInfoService {
	public static final String BILLING_STATUS_PROCESSED = 'Processed';
	public static final String BILLING_STATUS_FAILED = 'Failed';
	public static final String BILLING_STATUS_ACTIVE = 'Active';
	public static final String BILLING_STATUS_COMPLETED = 'Completed';
	public static final String DELIVERY_STATUS_OPEN = 'OPEN';
	public static final String DELIVERY_STATUS_CLOSED = 'CLOSED';
	public static final String DELIVERY_STATUS_ERROR = 'BILLING_ERROR';

	private static List<Error> returnErrors = new List<Error>();
	private static List<Customer_Asset__c> parentCustAssets = new List<Customer_Asset__c>();
	private static Map<String, OrderingResult> extRefMap = new Map<String, OrderingResult>();

	@HttpPost
	global static void updateOrderLineItems() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;

		ProvideInvoicingInfo body = (ProvideInvoicingInfo) JSON.deserializeStrict(
			req.requestbody.tostring(),
			ProvideInvoicingInfo.class
		);

		String transactionId = body.transactionID;
		List<Order_Billing_Transaction__c> ordBillTrans = [
			SELECT Id, Callback_Message__c, Transaction_Id__c
			FROM Order_Billing_Transaction__c
			WHERE Transaction_Id__c = :transactionId
			LIMIT 1
		];
		if (ordBillTrans.size() > 0) {
			ordBillTrans[0].Callback_Message__c = req.requestbody.tostring();
			update ordBillTrans;
		} else {
			Error cpErr = new Error();
			cpErr.errorCode = 'SFEC-9999';
			cpErr.errorMessage = 'Record not found with transactionID: ' + transactionId;
			returnErrors.add(cpErr);
		}

		for (OrderingResult ordRes : body.orderingResults) {
			extRefMap.put(ordRes.externalReferenceId, ordRes);
		}
		Set<String> extRefIds = extRefMap.keySet();
		// Retrieve Contracted Products and corresponding (parent) Customer Assets
		List<Contracted_Products__c> contProds = [
			SELECT
				Id,
				Customer_Asset__r.Assigned_Product_Id__c,
				Is_Recurring__c,
				Customer_Asset__r.Billing_Status__c,
				Billing_Status__c,
				Error_Info__c
			FROM Contracted_Products__c
			WHERE Id IN :extRefIds
		];
		// ...or the Customer Assets if no corresponding Contracted Product exists
		List<Customer_Asset__c> custAssets = [
			SELECT Id, Assigned_Product_Id__c, Error_Info__c
			FROM Customer_Asset__c
			WHERE Id IN :extRefIds
		];

		// Update the Contracted Products
		updateContProducts(contProds, extRefIds);

		// Update the corresponding (parent) Customer Assets
		updateCustomerAssets(parentCustAssets);

		// Update the stand-alone Customer Assets
		for (Customer_Asset__c custAsset : custAssets) {
			OrderingResult ordResult = extRefMap.get(custAsset.Id);
			// Remove matched ref id's from list
			extRefIds.remove(custAsset.Id);
			if (ordResult.result == 'OK') {
				custAsset.Assigned_Product_Id__c = ordResult.apId;
			} else {
				for (Error err : ordResult.errors) {
					custAsset.Error_Info__c += err.errorMessage;
				}
			}
		}
		updateCustomerAssets(custAssets);

		for (String refId : extRefIds) {
			Error cpErr = new Error();
			cpErr.errorCode = 'SFEC-9999';
			cpErr.errorMessage = 'Record not found with externalReferenceId: ' + refId;
			returnErrors.add(cpErr);
		}
		Response resp = new Response();
		resp.status = returnErrors.size() > 0 ? 'FAILED' : 'OK';
		resp.errors = returnErrors;
		res.addHeader('Content-Type', 'text/json');
		res.statusCode = 200;
		res.responseBody = Blob.valueOf(JSON.serializePretty(resp));
	}

	private static void updateContProducts(
		List<Contracted_Products__c> cpList,
		Set<String> extRefIds
	) {
		for (Contracted_Products__c contProd : cpList) {
			OrderingResult ordResult = extRefMap.get(contProd.Id);
			// Remove matched ref id's from list
			extRefIds.remove(contProd.Id);
			if (ordResult.result == 'OK') {
				contProd.Customer_Asset__r.Assigned_Product_Id__c = ordResult.apId;
				contProd.Customer_Asset__r.Billing_Status__c = contProd.Is_Recurring__c
					? BILLING_STATUS_ACTIVE
					: BILLING_STATUS_COMPLETED;
				contProd.Billing_Status__c = BILLING_STATUS_PROCESSED;
				contProd.Billing_Order_Id__c = ordResult.orderId;
			} else {
				for (Error err : ordResult.errors) {
					contProd.Error_Info__c += err.errorMessage;
					contProd.Billing_Status__c = BILLING_STATUS_FAILED;
				}
			}
		}
		Database.SaveResult[] srProductList = Database.update(cpList, false);
		for (Integer i = 0; i < srProductList.size(); i++) {
			if (!srProductList[i].isSuccess()) {
				for (Database.Error err : srProductList[i].getErrors()) {
					Error cpErr = new Error();
					cpErr.errorCode = 'SFEC-9999';
					cpErr.errorMessage =
						'Failed to update record with externalReferenceId: ' +
						cpList[i].Id +
						'; ' +
						err.getMessage();
					returnErrors.add(cpErr);
				}
			} else {
				parentCustAssets.add(cpList[i].Customer_Asset__r);
			}
		}
	}

	private static void updateCustomerAssets(List<Customer_Asset__c> caList) {
		Database.SaveResult[] srAssetList = Database.update(caList, false);
		for (Integer i = 0; i < srAssetList.size(); i++) {
			if (!srAssetList[i].isSuccess()) {
				for (Database.Error err : srAssetList[i].getErrors()) {
					Error cpErr = new Error();
					cpErr.errorCode = 'SFEC-9999';
					cpErr.errorMessage =
						'Failed to update record with externalReferenceId: ' +
						caList[i].Id +
						'; ' +
						err.getMessage();
					returnErrors.add(cpErr);
				}
			}
		}
	}
	global class ProvideInvoicingInfo {
		public String transactionID;
		public List<OrderingResult> orderingResults;
	}
	global class OrderingResult {
		public String apId;
		public String externalReferenceId;
		public String installedBaseId;
		public String orderId;
		public String result;
		public List<Error> errors;
	}
	global class Error {
		public String errorCode;
		public String errorMessage;
	}
	global class Response {
		public String status;
		public List<Error> errors;
	}
}