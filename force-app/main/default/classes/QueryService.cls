public inherited sharing class QueryService {
	/**
	 * Object to support additional query logic including sorting, offset and limits in a soql query.
	 * In essense, based on the input it provides the additional query string to concatenate to the query itself
	 */
	public class QueryProperties {
		public String additionalQueryOptions = ' ';
		public Set<String> fieldsToAdd = new Set<String>(); // Add all fields in the sorting section

		public QueryProperties(Map<String, String> sortingMap) {
			this(null, null, sortingMap);
		}

		public QueryProperties(
			Integer offset,
			Integer queryLimit,
			Map<String, String> sortingMap
		) {
			if (sortingMap != null && !sortingMap.isEmpty()) {
				additionalQueryOptions += ' ORDER BY ';
				Integer i = 0; // Keep track of map size to skip comma
				Integer mapSize = sortingMap.size();
				for (String sortingKey : sortingMap.keySet()) {
					fieldsToAdd.add(sortingKey);
					additionalQueryOptions +=
						sortingKey +
						' ' +
						sortingMap.get(sortingKey);
					if (i++ < mapSize - 1) {
						additionalQueryOptions += ', ';
					}
				}
			}
			if (queryLimit != null) {
				additionalQueryOptions += ' limit ' + queryLimit;
			}
			if (offset != null) {
				additionalQueryOptions += ' offset ' + offset;
			}
		}
	}
}