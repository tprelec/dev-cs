public class QueueableInsertObject implements Queueable {

	public List<List<sObject>> objectsToInsert;

	public QueueableInsertObject (List<List<sObject>> objectsToInsert) {
	   this.objectsToInsert = objectsToInsert;
	}

	public void execute(QueueableContext context) {
	    system.debug(objectsToInsert);
	    system.debug(objectsToInsert.size());
	    
	    insert objectsToInsert[0];
	    objectsToInsert.remove(0);
	    // schedule a new job for the rest
	    if(!objectsToInsert.isEmpty()){
	    	System.enqueueJob(new QueueableInsertObject(objectsToInsert));		      
	    }
	}
}