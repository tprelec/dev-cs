public class QueueablePostalcodeCheck implements Queueable, Database.AllowsCallouts {

	public List<Site__c> sitesToCheck;
	public Integer batchSize = 2;

	public QueueablePostalcodeCheck (List<Site__c> sitesToCheck) {
	   this.sitesToCheck = sitesToCheck;
	}

	public void execute(QueueableContext context) {

		system.debug(sitesToCheck);
		system.debug(sitesToCheck.size());
		system.debug(batchSize);

		// determine how many checks to do in 1 transaction (not too much since each check is an individual callout and BOP needs quite some time to respond)
		List<Site__c> sitesToCheckInThisBatch = new List<Site__c>();
		for(Integer i=0;i<batchSize;i++){
			system.debug(i);
			if(sitesToCheck.size() > i){
				sitesToCheckInThisBatch.add(sitesToCheck[i]);
			}
		}
		// remove them from sitesToCheck
		for(Integer i=0;i<batchSize;i++){
			system.debug(i);
			if(!sitesToCheck.isEmpty()){
				sitesToCheck.remove(0);
			}
		}		
		system.debug(sitesToCheckInThisBatch);
		system.debug(sitesToCheck);

		// do pccheck here
		Map<String,Set<Id>> requesttypeToSites = new Map<String,Set<Id>>();
		requesttypeToSites.put('dsl',new Set<Id>());
		requesttypeToSites.put('fiber',new Set<Id>());

		for(Site__c s : sitesToCheckInThisBatch){
		  if(s.Postal_Code_Check_Status__c == 'DSL check requested'){
		    //PostalCodeCheckController.doOnlinePostalCodeCheckForSites(new Set<Id>{s.Id},'dsl');   
		    requesttypeToSites.get('dsl').add(s.Id);
		    //return;
		  } else if(s.Postal_Code_Check_Status__c == 'Fiber check requested'){
		    //PostalCodeCheckController.doOnlinePostalCodeCheckForSites(new Set<Id>{s.Id},'fiber');   
		    requesttypeToSites.get('fiber').add(s.Id);
		    //return;
		  } else if(s.Postal_Code_Check_Status__c == 'Combined check requested'){
		  	// do both checks separately in order to make sure that sdslbis is also done
		    //PostalCodeCheckController.doOnlinePostalCodeCheckForSites(new Set<Id>{s.Id},'dsl');    
		    //PostalCodeCheckController.doOnlinePostalCodeCheckForSites(new Set<Id>{s.Id},'fiber');   
		    requesttypeToSites.get('dsl').add(s.Id);
		    requesttypeToSites.get('fiber').add(s.Id);
		    //return;
		  }
		}
		PostalCodeCheckController.doBatchCheck(requesttypeToSites);
      

	    // schedule a new job for the rest
	    if(!sitesToCheck.isEmpty()){
	    	System.enqueueJob(new QueueablePostalcodeCheck(sitesToCheck));		      
	    }
	}
}