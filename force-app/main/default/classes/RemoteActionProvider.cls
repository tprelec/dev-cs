/**
* @description Handler for all Remote Action methods called from javascript resources
*/

global with sharing class RemoteActionProvider implements cssmgnt.RemoteActionDataProvider {

    @TestVisible
    private static ProductBasketService productBasketService {
        get {
            if (productBasketService == null) {
                productBasketService = new ProductBasketService();
            }
            return productBasketService;
        }
        set;
    }

    private static Map<String, Object> inputMap;
    private static Map<String, Object> outputMap;

    /**
     * @description Generic dispatcher for all remote methods called from the JS code. The particular method needs to be specified in the "method" parameter.
     *
     * @param inputMap Map of all input parameters, including "method" parameter and then all input parameters for the specific method
     *
     * @return outputMap, specific for each method
     */
    @RemoteAction
    global static Map<String, Object> getData(Map<String, Object> inputData) {
        inputMap = inputData;
        outputMap = new Map<String, Object>();

        String method = String.valueOf(inputMap.get('method'));

        switch on method {
            when 'productBasketDetails' {
                getProductBasketDetails();
            }
        }
        return outputMap;
    }

    /**
     * @description Retrieves additional fields for Product Basket based on given "productBasketId" parameter.
     */
    private static void getProductBasketDetails() {
        String productBasketId = String.valueOf(inputMap.get('productBasketId'));

        try {
            if (productBasketId == null) {
                outputMap.put('status', false);
            } else {
                cscfga__Product_Basket__c productBasket = productBasketService.queryProductBasketDetails(productBasketId);

                outputMap.put('status', true);
                outputMap.put('basketDetails', productBasket);
                outputMap.put('opportunityDirectIndirect', productBasket.cscfga__Opportunity__r.Direct_Indirect__c);
            }
        } catch (Exception ex) {
            outputMap.put('status', false);
            outputMap.put('message', ex.getMessage());
        }
    }

}