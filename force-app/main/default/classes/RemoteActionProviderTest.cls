/**
 * Created by miaaugustinovic on 09/04/2021.
 *
 * @description Test class for RemoteActionProvider class
 */
@IsTest
public with sharing class RemoteActionProviderTest {

    private static cscfga__Product_Basket__c productBasket;

    static {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs (simpleUser) {
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;

            No_Triggers__c notriggers = new No_Triggers__c();
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;

            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
            insert tmpOpp;

            productBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
            productBasket.MobileScenarios__c = '[OneMobile]';
            insert productBasket;

            for(cscfga__Product_Basket__c basket : [select cscfga__Opportunity__r.Direct_Indirect__c from cscfga__Product_Basket__c where Id = :productBasket.Id]) {
                system.debug('**** basket : ' + basket.cscfga__Opportunity__r.Direct_Indirect__c );
                productBasket = basket;
                update productBasket;
            }
        }
    }

    private static testMethod void testGetProductBasketDetails_positive() {
        Map<String, Object> inputMap = new Map<String, Object>{
                'method' => 'productBasketDetails',
                'productBasketId' => productBasket.Id
        };

        Test.startTest();
        Map<String, Object> result = RemoteActionProvider.getData(inputMap);
        Test.stopTest();

        System.assertEquals(productBasket.cscfga__Opportunity__r.Direct_Indirect__c, String.valueOf(result.get('opportunityDirectIndirect')), 'Invalid Opp. Direct/Indirect value returned.');
    }

    private static testMethod void testGetProductBasketDetails_negative() {
        Map<String, Object> inputMap = new Map<String, Object>{
                'method' => 'productBasketDetails',
                'productBasketId' => null
        };

        Test.startTest();
        Map<String, Object> result = RemoteActionProvider.getData(inputMap);
        Test.stopTest();

        system.assertEquals(false,result.get('status'), 'Status is not updated properly');
    }
}