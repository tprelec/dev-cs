public with sharing class RetentionOpportunityController {
    private final Opportunity opportunity;
    public String contractId{get; private set;}
    
    public RetentionOpportunityController(ApexPages.StandardController stdController) {
        this.opportunity = (Opportunity)stdController.getRecord();
        contractId = Apexpages.currentPage().getParameters().get('vf_contract_id');
        VF_Contract__c contract = [select id, name, Account__c, Net_Revenues__c, Account__r.OwnerId from VF_Contract__c where Id=: contractId];
        opportunity.Retention_Contract_VF__c = contract.id;
        opportunity.AccountId = contract.Account__c;
        opportunity.OwnerId = contract.Account__r.OwnerId;
        opportunity.Amount = contract.Net_Revenues__c;
        opportunity.Type = 'Existing Business';
    }
    
    public pageReference resetProbability() {
        List<OpportunityStage> OppStage = [Select DefaultProbability From OpportunityStage where MasterLabel =: opportunity.StageName];
        opportunity.Probability = OppStage.get(0).DefaultProbability;
        return null;
    }
}