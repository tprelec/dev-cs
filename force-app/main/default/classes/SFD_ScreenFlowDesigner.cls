/**
 * Build UI + process (OMB-125 Complex change)
 *
 * @author Petar Miletic
 * @since  21/02/2017
*/
global class SFD_ScreenFlowDesigner {

    @RemoteAction
    global static SFD_ScreenFlowDesigner.Model getInit() {

        SFD_ScreenFlowDesigner.Model model = new SFD_ScreenFlowDesigner.Model();
        return model;
    }

    /*
     * Load Screen Flows
    */
    @RemoteAction
    global static List<cscfga__Screen_Flow__c> getScreenFlows() {

        // return [SELECT Id, Name, cscfga__Predicate__c FROM cscfga__Screen_Flow__c ORDER BY Name ASC];
        return [SELECT Id, Name, cscfga__Predicate__c, (SELECT Id, cscfga__Product_Definition__c FROM cscfga__Screen_Flow_Product_Assocations__r ORDER BY CreatedDate DESC LIMIT 1) FROM cscfga__Screen_Flow__c ORDER BY Name ASC];
    }

    /*
     * Load Configuration Screens using Screen Flow Id
    */
    @RemoteAction
    global static List<cscfga__Configuration_Screen__c> getConfigurationScreens(Id screenFlowId) {

        return [SELECT Id, Name, cscfga__Label__c, cscfga__product_definition__c, cscfga__Reference__c, cscfga__Show_Configuration_and_Product_Name__c, cscfga__Show_Product_Configuration_header__c, cscfga__Type__c, cscfga__Index__c FROM cscfga__Configuration_Screen__c WHERE cscfga__screen_flow__c = :screenFlowId ORDER BY Name ASC];
    }

    /*
     * Load Screen Sections using Configuration Id
    */
    @RemoteAction
    global static List<cscfga__Screen_Section__c> getScreenSections(Id configurationId) {

        return [SELECT Id, Name, cscfga__Label__c, cscfga__Configuration_Screen__c FROM cscfga__Screen_Section__c WHERE cscfga__configuration_screen__c = :configurationId ORDER BY cscfga__index__c ASC];
    }

    /*
     * Get Product Definitions
    */
    @RemoteAction
    global static List<cscfga__Product_Definition__c> getProductDefinitions() {

        return [SELECT Id, Name FROM cscfga__Product_Definition__c WHERE cscfga__IsArchived__c = false ORDER BY Name ASC];
    }

    /*
     * Load Attributes for Product Configuration Id
    */   
    @RemoteAction
    global static List<Item> getAttributes(Id productDefinitionId) {

        List<cscfga__Attribute_Definition__c> attributes = [SELECT Id, Name, cscfga__Product_Definition__c, cscfga__Product_Definition__r.Name FROM cscfga__Attribute_Definition__c WHERE cscfga__Product_Definition__c = :productDefinitionId  ORDER BY Name ASC];

        List<Item> items = new List<Item>();

        for (cscfga__Attribute_Definition__c attr :attributes) {

            Item itm = new Item();

            itm.Id = attr.Id;
            itm.Name = attr.Name;
            itm.Column = 0;
            itm.ItemDefinition = attr;
            itm.locked = false;

            items.add(itm);
        }

        return items;
    }

    /*
     * Load Screen Sections data (Id, Name, Type, list of lists containing attribute associations)
    */
    @RemoteAction
    global static List<SFD_ScreenFlowDesigner.ScreenSection> getScreenSectionsData(Id configurationId) {

        Set<Id> productDefinitionIds = new Set<Id>();

		List<Id> attributes;
        
        List<SFD_ScreenFlowDesigner.ScreenSection> screenSections = new List<SFD_ScreenFlowDesigner.ScreenSection>();

        List<cscfga__Screen_Section__c> sections = SFD_ScreenFlowDesigner.getScreenSections(configurationId);
        
        List<cscfga__Attribute_Screen_Section_Association__c> associations = [SELECT Id,
                                                                                    cscfga__Row_Sequence__c,
                                                                                    cscfga__column_sequence__c, 
                                                                                    cscfga__Screen_Section__c, 
                                                                                    cscfga__Attribute_Definition__c,
                                                                                    cscfga__Attribute_Definition__r.Name,
                                                                                    cscfga__Attribute_Definition__r.cscfga__Product_Definition__c,
                                                                                    cscfga__Attribute_Definition__r.cscfga__Product_Definition__r.Name,
                                                                                    cscfga__attribute_locked__c
                                                                                FROM cscfga__Attribute_Screen_Section_Association__c
                                                                                WHERE cscfga__Screen_Section__r.cscfga__Configuration_Screen__c = :configurationId
                                                                                ORDER BY cscfga__row_sequence__c ASC];

        for (cscfga__Screen_Section__c obj :sections) {
            
            attributes = new List<Id>();
            
            SFD_ScreenFlowDesigner.ScreenSection section = new SFD_ScreenFlowDesigner.ScreenSection();
            section.Id = obj.Id;
            section.Name = obj.Name;
            section.columns = new List<List<Item>>();

            List<Item> left = new List<Item>();
            List<Item> right = new List<Item>();

            for (cscfga__Attribute_Screen_Section_Association__c aobj :associations) {

                if (aobj.cscfga__Attribute_Definition__r.cscfga__Product_Definition__c != null) {
                    productDefinitionIds.add(aobj.cscfga__Attribute_Definition__r.cscfga__Product_Definition__c);    
                }

                if (aobj.cscfga__Screen_Section__c == obj.Id) {

                    Item attr = new Item();
					
                    attr.Id = aobj.cscfga__Attribute_Definition__c;
                    attr.Name = aobj.cscfga__Attribute_Definition__r.Name;
                    
                    attr.Column = aobj.cscfga__column_sequence__c == null ? 0 : aobj.cscfga__column_sequence__c;
                    attr.ItemDefinition = aobj.cscfga__Attribute_Definition__r;

                    attr.AssociationId = aobj.Id;
                    attr.ScreenSectionId = aobj.cscfga__screen_section__c;
                    
                    attr.locked = aobj.cscfga__attribute_locked__c;

                    if (aobj.cscfga__column_sequence__c > 0) {
                        right.add(attr);
                    } else {
                        left.add(attr);
                    }
                    
                    // Flattened list of used ID's 
                    attributes.add(attr.Id);
                }
            }

			section.attributes = attributes;        	
			section.productDefinitionIds = productDefinitionIds;
            section.columns.add(left);
            section.columns.add(right);

            screenSections.add(section);
        }

        return screenSections;
    }
 
    /*
     * Get Item data and metadata
    */
    @RemoteAction
    global static MetadataData getModelMetadata(String objectType, String recordId) {

        Id objectId = Id.valueOf(recordId);

        return new MetadataData(objectType, objectId);
    }

    @RemoteAction
    global static Id saveModel(String objectType, String obj) {

        try {
            
            if (objectType == 'cscfga__Screen_Flow__c' || objectType.contains(':')) {
                
                List<String> parts = objectType.split(':');
                
                cscfga__Screen_Flow__c o = (cscfga__Screen_Flow__c)JSON.deserialize(obj, cscfga__Screen_Flow__c.class);
                upsert o;
                
                if (parts.size() == 2) {
                    
                    String productDefinition = parts[1];

                    List<cscfga__Screen_Flow_Product_Association__c> associations = [SELECT Id, cscfga__Product_Definition__c 
                                                                                        FROM cscfga__Screen_Flow_Product_Association__c 
                                                                                        WHERE cscfga__Screen_Flow__c = :o.Id];
                    
                    delete associations;

                    if (SFD_Util.IsValidId(productDefinition)) {
                        
                        cscfga__Screen_Flow_Product_Association__c association = new cscfga__Screen_Flow_Product_Association__c();
                        association.cscfga__Screen_Flow__c = o.Id;
                        association.cscfga__Product_Definition__c = productDefinition;
                        
                        insert association;
                    }
                }

                return o.Id;
            } 
            else if (objectType == 'cscfga__Configuration_Screen__c') {
                cscfga__Configuration_Screen__c o = (cscfga__Configuration_Screen__c)JSON.deserialize(obj, cscfga__Configuration_Screen__c.class);
    
                upsert o;
                
                return o.Id;
            } 
            else {
                cscfga__Screen_Section__c o = (cscfga__Screen_Section__c)JSON.deserialize(obj, cscfga__Screen_Section__c.class);
                
                upsert o;

                return o.Id;
            }
        }
        catch (Exception ex) {
			return ex.getMessage();            
        }
    }
    
    @RemoteAction
    global static String deleteItem(Id recordId) {

        try {
            
            String objectType = String.valueOf(recordId.getSobjectType());
            
            if (objectType == 'cscfga__Screen_Flow__c') {
                cscfga__Screen_Flow__c o = new cscfga__Screen_Flow__c( Id = recordId );

                List<cscfga__Configuration_Screen__c> cs = [SELECT Id FROM cscfga__Configuration_Screen__c WHERE cscfga__screen_flow__c = :recordId];
                List<cscfga__Screen_Section__c> ss = [SELECT Id FROM cscfga__Screen_Section__c WHERE cscfga__configuration_screen__r.cscfga__Screen_Flow__c = :recordId];
                List<cscfga__Attribute_Screen_Section_Association__c> ssa = [SELECT Id FROM cscfga__Attribute_Screen_Section_Association__c WHERE cscfga__Screen_Section__r.cscfga__Configuration_Screen__r.cscfga__Screen_Flow__c = :recordId];
                
                List<cscfga__Screen_Flow_Product_Association__c> associations = [SELECT Id, cscfga__Product_Definition__c FROM cscfga__Screen_Flow_Product_Association__c WHERE cscfga__Screen_Flow__c = :recordId];
                
                delete associations;
                delete ssa;
                delete ss;
                delete cs;
                
                delete o;
            }
            else if (objectType == 'cscfga__Configuration_Screen__c') {
                cscfga__Configuration_Screen__c o = new cscfga__Configuration_Screen__c( Id = recordId );
    
                List<cscfga__Screen_Section__c> ss = [SELECT Id FROM cscfga__Screen_Section__c WHERE cscfga__configuration_screen__r.cscfga__Screen_Flow__c = :recordId];
                List<cscfga__Attribute_Screen_Section_Association__c> ssa = [SELECT Id FROM cscfga__Attribute_Screen_Section_Association__c WHERE cscfga__Screen_Section__r.cscfga__Configuration_Screen__c = :recordId];
                
                delete ssa;
                delete ss;

                delete o;
            }
            else if (objectType == 'cscfga__Screen_Section__c') {
                cscfga__Screen_Section__c o = new cscfga__Screen_Section__c( Id = recordId );
    
                List<cscfga__Attribute_Screen_Section_Association__c> ssa = [SELECT Id FROM cscfga__Attribute_Screen_Section_Association__c WHERE cscfga__Screen_Section__c = :recordId];
                
                delete ssa;
    
                delete o;
            }
            else if (objectType == 'cscfga__Attribute_Screen_Section_Association__c') {
                cscfga__Attribute_Screen_Section_Association__c o = new cscfga__Attribute_Screen_Section_Association__c( Id = recordId );

                delete o;
            } 
    
            return 'Success!';         
        }
        catch (Exception ex) {
			return ex.getMessage();            
        }
    }
    
    /*
     * Saves Configuration layout (Sections and Attributes)
    */
    @RemoteAction
    global static SFD_ScreenFlowDesigner.SaveData saveConfiguration(Id recordId, String configuration, String items) {

        SFD_ScreenFlowDesigner.SaveData data = new SFD_ScreenFlowDesigner.SaveData();

        try {
            cscfga__Configuration_Screen__c obj = (cscfga__Configuration_Screen__c)JSON.deserialize(configuration, cscfga__Configuration_Screen__c.class);
            List<SFD_ScreenFlowDesigner.ScreenSection> objs = (List<SFD_ScreenFlowDesigner.ScreenSection>)JSON.deserialize(items, List<SFD_ScreenFlowDesigner.ScreenSection>.class);
            
            System.debug(objs);
                       
            List<cscfga__Attribute_Screen_Section_Association__c> associations = [SELECT Id,
                                                                                      cscfga__Row_Sequence__c,
                                                                                      cscfga__column_sequence__c, 
                                                                                      cscfga__Screen_Section__c, 
                                                                                      cscfga__Attribute_Definition__c,
                                                                                      cscfga__Attribute_Definition__r.Name,
                                                                                      cscfga__attribute_locked__c
                                                                                  FROM cscfga__Attribute_Screen_Section_Association__c
                                                                                  WHERE cscfga__Screen_Section__r.cscfga__Configuration_Screen__c = :obj.Id
                                                                                  ORDER BY cscfga__row_sequence__c ASC];
            
            List<cscfga__Screen_Section__c> sections = new List<cscfga__Screen_Section__c>();
            Set<cscfga__Attribute_Screen_Section_Association__c> newElements = new Set<cscfga__Attribute_Screen_Section_Association__c>();
            
            List<cscfga__Attribute_Screen_Section_Association__c> cleanupItems = new List<cscfga__Attribute_Screen_Section_Association__c>();

            Integer sectionIndex = 0;
            Integer row;

            for(SFD_ScreenFlowDesigner.ScreenSection sec :objs) {
                
                cscfga__Screen_Section__c section = new cscfga__Screen_Section__c();
                
                section.Id = sec.Id;
                section.cscfga__Index__c = sectionIndex;
                
                sections.add(section);
                
                for(List<Item> col :sec.columns) {

                    row = 0;
                    
                    for (Item itm :col) {
                     
                        for (cscfga__Attribute_Screen_Section_Association__c asociation :associations) {
                            
                            if (asociation.cscfga__Attribute_Definition__c == itm.Id) {
                                
                                if (asociation.cscfga__screen_section__c != itm.ScreenSectionId) {
                                    System.debug('Reparenting needed: ' + asociation);
                                    
                                    cscfga__Attribute_Screen_Section_Association__c newClone = asociation.clone(false, true, false, false);
                                    newClone.cscfga__Screen_Section__c = itm.ScreenSectionId;
                                	newClone.cscfga__column_sequence__c = itm.Column;
                                	newClone.cscfga__Row_Sequence__c = row * 5;
                                	newClone.cscfga__attribute_locked__c = itm.locked;

                                    cleanupItems.add(asociation);
                                    newElements.add(newClone);

                                    continue;
                                }

                                // asociation.cscfga__screen_section__c = itm.ScreenSectionId;
                                asociation.cscfga__column_sequence__c = itm.Column;
                                asociation.cscfga__Row_Sequence__c = row * 5;
                                asociation.cscfga__attribute_locked__c = itm.locked;
                                continue;
                            }
                        }

                        if (itm.AssociationId == null) {

                            cscfga__Attribute_Screen_Section_Association__c newAssociation = new cscfga__Attribute_Screen_Section_Association__c();

                            newAssociation.cscfga__Attribute_Definition__c = itm.Id;
                            newAssociation.cscfga__Screen_Section__c = itm.ScreenSectionId;
                            newAssociation.cscfga__column_sequence__c = itm.Column;
                            newAssociation.cscfga__Row_Sequence__c = row * 5;
                            newAssociation.cscfga__attribute_locked__c = itm.locked;

                            newElements.add(newAssociation);
                        }

                        row++;
                    }
                }
                
                sectionIndex++;
            }

            associations.addAll(newElements);

            upsert associations;
            update sections;
            
            delete cleanupItems;

            data.msg = 'Success!';
            data.data = SFD_ScreenFlowDesigner.getScreenSectionsData(recordId);

        } catch (Exception ex) {
            
            data.msg = ex.getMessage();
        }
        
        return data;
    }
    
    @RemoteAction
    global static String deleteSectionAssociations(Id screenSectionId) {
        
        try
        {
        	List<cscfga__Attribute_Screen_Section_Association__c> objs = [SELECT Id 
            	                                                          FROM cscfga__Attribute_Screen_Section_Association__c 
					                                                      WHERE cscfga__Screen_Section__c = :screenSectionId];
            
            delete objs;
            
            return 'Success!';
            
        } catch (Exception ex) {
			return ex.getMessage();            
        }
	}
	
	@RemoteAction
	global static String cloneScreenFlowTree(Id screenFlowId) {
	    
	    try {
	        
    	    String[] types = new String[]{ 'cscfga__Screen_Flow__c', 'cscfga__Configuration_Screen__c', 'cscfga__Screen_Section__c', 'cscfga__Attribute_Screen_Section_Association__c' };
    	    
    	    Map<String, Set<String>> objectFieldsMap = SFD_Util.getObjectFields(types);
    
            String sfFields = SFD_Util.getColumns(objectFieldsMap.get('cscfga__Screen_Flow__c'));
            String csFields = SFD_Util.getColumns(objectFieldsMap.get('cscfga__Configuration_Screen__c'));
            String ssFields = SFD_Util.getColumns(objectFieldsMap.get('cscfga__Screen_Section__c'));
            String aaFields = SFD_Util.getColumns(objectFieldsMap.get('cscfga__Attribute_Screen_Section_Association__c'));
    
            Map<Id, sObject> screenFlowMap = null;
            Map<Id, sObject> configurationScreenMap = null;
            Map<Id, sObject> screenSectionMap = null;
            Map<Id, sObject> sectionAttributeAssociations = null;
    
            // Handle Screen Flows	    
            List<cscfga__Screen_Flow__c> sf = Database.query('SELECT ' + sfFields + ' FROM cscfga__Screen_Flow__c WHERE Id = :screenFlowId');
    
            if (!sf.isEmpty()) {
    
                screenFlowMap = SFD_ScreenFlowDesigner.getClones(sf, null, null);
    
                for (sObject obj :screenFlowMap.values()) {
                    
                    obj.put('Name', obj.get('Name') + ' Clone');
                }
                
                insert screenFlowMap.values();
            }
            
            // Handle Configuration Screen
            List<cscfga__Configuration_Screen__c> cs = Database.query('SELECT ' + csFields + ' FROM cscfga__Configuration_Screen__c WHERE cscfga__screen_flow__c = :screenFlowId');
            
            if (!cs.isEmpty()) {
            
                configurationScreenMap = SFD_ScreenFlowDesigner.getClones(cs, 'cscfga__Screen_Flow__c', screenFlowMap);
                
                insert configurationScreenMap.values();
            }
            
            // Handle Screen Section
            List<cscfga__Screen_Section__c> ss = Database.query('SELECT ' + ssFields + ' FROM cscfga__Screen_Section__c WHERE cscfga__Configuration_Screen__r.cscfga__screen_flow__c = :screenFlowId');
            
            if (!ss.isEmpty()) {
            
                screenSectionMap = SFD_ScreenFlowDesigner.getClones(ss, 'cscfga__Configuration_Screen__c', configurationScreenMap);
                
                insert screenSectionMap.values();
            }
            
            // Handle Attribute Screen Section Association
            List<cscfga__Attribute_Screen_Section_Association__c> aa = Database.query('SELECT ' + aaFields + ' FROM cscfga__Attribute_Screen_Section_Association__c WHERE cscfga__Screen_Section__r.cscfga__Configuration_Screen__r.cscfga__screen_flow__c = :screenFlowId');
            
            if (!aa.isEmpty()) {
            
                sectionAttributeAssociations = SFD_ScreenFlowDesigner.getClones(aa, 'cscfga__Screen_Section__c', screenSectionMap);
                
                insert sectionAttributeAssociations.values();
            }
            
            return screenFlowMap.values()[0].Id;   

        } catch (Exception ex) {
			return ex.getMessage();            
        }
	}
	
	private static Map<Id, sObject> getClones(List<sObject> objs, string field, Map<Id, sObject> parents) {
	    
        Map<Id, sObject> retval = new Map<Id, sObject>();
        
        for (sObject obj :objs) {
            
            sObject cloned = obj.clone(false, true, false, false);
            
            if (String.isNotBlank(field)) {
                
                Id oldId = (Id)obj.get(field);

                cloned.put(field, parents.get(oldId).Id);
            }
            
            retval.put(obj.Id, cloned);
        }
        
        return retval;
	}

    global class Model {
        
        public List<cscfga__Screen_Flow__c> screenFlows { get; set; }
        public List<cscfga__Product_Definition__c> productDefinitions { get; set; }
        public List<cscfga__Predicate__c> predicates { get; set; }
        
        public Model() {
            this.screenFlows = [SELECT Id, Name, (SELECT Id, cscfga__Product_Definition__c FROM cscfga__Screen_Flow_Product_Assocations__r ORDER BY CreatedDate DESC LIMIT 1) FROM cscfga__Screen_Flow__c ORDER BY Name ASC];
            this.productDefinitions = [SELECT Id, Name FROM cscfga__Product_Definition__c WHERE cscfga__IsArchived__c = false ORDER BY Name ASC];
            this.predicates = [SELECT Id, Name, cscfga__Label__c FROM cscfga__Predicate__c ORDER BY Name ASC];
        }
    }

    /*
     * Encapsulates Screen Section data (to be used for layout)
    */
    global class ScreenSection
    {
        public Id Id { get; set; }

        public String Name { get; set; }

        public String type { get; set; }

        public List<List<Item>> columns { get; set; }
        
        List<Id> attributes { get; set; }
        
        Set<Id> productDefinitionIds { get; set; }

        public ScreenSection() {
            this.type = 'cscfga__Screen_Section__c';
        }
    }

    /*
     * Encapsulates Item data (Item == Attribute Definition + Additional data)
    */
    global class Item
    {
        public Id Id { get; set; }

        public String Name { get; set; }

        public String type { get; set; }

        public Decimal Column { get; set;}
        
        public Boolean locked { get; set; }

        public cscfga__Attribute_Definition__c ItemDefinition { get; set; }

        public Id AssociationId { get; set; }

        public Id ScreenSectionId { get; set; }

        public Item() {
            this.type = 'item';
        }
    }

    /*
     * Encapsulates model Metadata and Data
    */
    global class MetadataData {

        public SObject model { get; set; }
        public Id productDefinitionId { get; set; }
        public Integer multipleDefinitions { get; set; }
        // public List<cscfga__Screen_Flow_Product_Association__c> associatedDefinitions { get; set; }
        
        public MetadataData(String objectType, Id recordId) {

            loadData(objectType, recordId);
        }

        private void loadData(String objectType, Id recordId) {

            if (objectType == 'cscfga__Screen_Flow__c') {
                this.model = [SELECT Id, 
                                  Name, 
                                  cscfga__Predicate__c,
                                  cscfga__Template_Reference__c, 
                                  cscfga__configuration_locked__c 
                              FROM cscfga__Screen_Flow__c
                              WHERE Id = :recordId LIMIT 1];
                              
                List<cscfga__Screen_Flow_Product_Association__c> associations = [SELECT Id, cscfga__Product_Definition__c 
                                                                                    FROM cscfga__Screen_Flow_Product_Association__c 
                                                                                    WHERE cscfga__Screen_Flow__c = :recordId ORDER BY CreatedDate DESC];
                
                if (!associations.isEmpty()) {
                    
                    this.productDefinitionId = associations[0].cscfga__Product_Definition__c;
                }
                
                // this.associatedDefinitions = associations;
                this.multipleDefinitions = associations.size();
            } 
            else if (objectType == 'cscfga__Configuration_Screen__c') {
                this.model = [SELECT Id, 
                                    Name, 
                                    cscfga__Label__c, 
                                    cscfga__product_definition__c, 
                                    cscfga__Reference__c, 
                                    cscfga__Show_Configuration_and_Product_Name__c, 
                                    cscfga__Show_Product_Configuration_header__c, 
                                    cscfga__Type__c,
                                    cscfga__Index__c
                              FROM cscfga__Configuration_Screen__c 
                              WHERE Id = :recordId LIMIT 1];
            } 
            else if (objectType == 'cscfga__Screen_Section__c') {
                this.model = [SELECT Id, Name, cscfga__Label__c, cscfga__Configuration_Screen__c FROM cscfga__Screen_Section__c WHERE Id = :recordId LIMIT 1];
            }
        }
    }
    
    global class SaveData {
        
        public String msg { get; set; }
        
        public List<SFD_ScreenFlowDesigner.ScreenSection> data { get; set; }
    }
}