/**
 * Test for SFD_ScreenFlowDesigner
 *
 * @author Petar Miletic
 * @story PPA Strech Objective
 * @since 02-04/2017
*/
@IsTest
public class SFD_ScreenFlowDesignerTest {

	@testsetup
    private static void setupTestData()
    {
        cscfga__Screen_Flow__c sf = SFD_TestHelper.createScreenFlow('Test', 'Test template', true);
        cscfga__Configuration_Screen__c cs = SFD_TestHelper.createConfigurationScreen('Test Configuration', sf, true);
        cscfga__Screen_Section__c ss = SFD_TestHelper.createScreenSection('Test Section', cs, true);
        cscfga__Product_Definition__c pd = SFD_TestHelper.createProductDefinition('Test Product Definitions', 'Product Definition Description', true);
        pd.Product_Type__c = 'Fixed';
        cscfga__Attribute_Definition__c ad =  SFD_TestHelper.createAttributeDefinition('Attribute Definition', pd, true);
        cscfga__Attribute_Definition__c ad2 =  SFD_TestHelper.createAttributeDefinition('Attribute Definition Price', pd, true);
        
        SFD_TestHelper.createScreenSectonAssociation(ad, ss, true);
        SFD_TestHelper.createScreenSectonAssociation(ad2, ss, true);
	}
    
    @IsTest
    public static void getScreenFlows() {
        
		Test.startTest();
		
		List<cscfga__Screen_Flow__c> objs = SFD_ScreenFlowDesigner.getScreenFlows();

		Test.stopTest();
        
        System.assertEquals(1, objs.size(), 'Invalid data');
    }
    
    @IsTest
    public static void getConfigurationScreens() {

        cscfga__Screen_Flow__c sf = [SELECT Id FROM cscfga__Screen_Flow__c LIMIT 1];
        
        Test.startTest();
		
		List<cscfga__Configuration_Screen__c> objs = SFD_ScreenFlowDesigner.getConfigurationScreens(sf.Id);

		Test.stopTest();
        
        System.assertEquals(1, objs.size(), 'Invalid data');
    }
    
    @IsTest
    public static void getScreenSections() {
        
        cscfga__Configuration_Screen__c cs = [SELECT Id FROM cscfga__Configuration_Screen__c LIMIT 1];
        
        Test.startTest();
		
		List<cscfga__Screen_Section__c> objs = SFD_ScreenFlowDesigner.getScreenSections(cs.Id);

		Test.stopTest();
        
        System.assertEquals(1, objs.size(), 'Invalid data');
    }
    
    @IsTest
    public static void getProductDefinitions() {

		Test.startTest();
		
		List<cscfga__Product_Definition__c> objs = SFD_ScreenFlowDesigner.getProductDefinitions();

		Test.stopTest();
        
        System.assertEquals(1, objs.size(), 'Invalid data');
    }
    
    @IsTest
    public static void getAttributes() {
        
        cscfga__Product_Definition__c pd = [SELECT Id FROM cscfga__Product_Definition__c LIMIT 1];

		Test.startTest();
		
		List<SFD_ScreenFlowDesigner.Item> objs = SFD_ScreenFlowDesigner.getAttributes(pd.Id);

		Test.stopTest();
        
        System.assertEquals(2, objs.size(), 'Invalid data');
    }
    
    @IsTest
    public static void getScreenSectionsData() {

        cscfga__Configuration_Screen__c cs = [SELECT Id FROM cscfga__Configuration_Screen__c LIMIT 1];

		Test.startTest();
		
		List<SFD_ScreenFlowDesigner.ScreenSection> objs = SFD_ScreenFlowDesigner.getScreenSectionsData(cs.Id);

		Test.stopTest();
        
        System.assertEquals(1, objs.size(), 'Invalid data');
    }
    
    @IsTest
    public static void getModelMetadata() {

        cscfga__Screen_Flow__c sf = [SELECT Id FROM cscfga__Screen_Flow__c LIMIT 1];
        cscfga__Configuration_Screen__c cs = [SELECT Id FROM cscfga__Configuration_Screen__c LIMIT 1];
        cscfga__Screen_Section__c ss = [SELECT Id FROM cscfga__Screen_Section__c LIMIT 1];

		Test.startTest();
		
		SFD_ScreenFlowDesigner.MetadataData objsf = SFD_ScreenFlowDesigner.getModelMetadata('cscfga__Screen_Flow__c', sf.Id);
        SFD_ScreenFlowDesigner.MetadataData objcs = SFD_ScreenFlowDesigner.getModelMetadata('cscfga__Configuration_Screen__c', cs.Id);
        SFD_ScreenFlowDesigner.MetadataData objss = SFD_ScreenFlowDesigner.getModelMetadata('cscfga__Screen_Section__c', ss.Id);

		Test.stopTest();
        
        System.assertNotEquals(null, objsf, 'Invalid data');

        System.assertNotEquals(null, objcs, 'Invalid data');

        System.assertNotEquals(null, objss, 'Invalid data');
    }

    @IsTest
    public static void saveModelInsert() {
        
        cscfga__Screen_Flow__c sf = SFD_TestHelper.createScreenFlow('Test Insert', 'Test template', false);
        sf.cscfga__configuration_locked__c = true;
        
		Test.startTest();
		
		String retval = SFD_ScreenFlowDesigner.saveModel('cscfga__Screen_Flow__c', JSON.serialize(sf));

		Test.stopTest();
		
		System.assert(retval InstanceOf ID, 'Invalid data');
    }
    
    @IsTest
    public static void saveModelInsertUpdate() {
        
        cscfga__Configuration_Screen__c cs = [SELECT Id, Name FROM cscfga__Configuration_Screen__c LIMIT 1];
        cs.Name = 'Updated Configuration Screen';
        
		Test.startTest();
		
		String retval = SFD_ScreenFlowDesigner.saveModel('cscfga__Configuration_Screen__c', JSON.serialize(cs));

        cs = [SELECT Id, Name FROM cscfga__Configuration_Screen__c WHERE Id = :cs.Id LIMIT 1];
		
        cscfga__Screen_Section__c ss = SFD_TestHelper.createScreenSection('Test Section', cs, true);
        
        String retval2 = SFD_ScreenFlowDesigner.saveModel('cscfga__Screen_Section__c', JSON.serialize(ss));
        
        Test.stopTest();
        
		System.assert(retval InstanceOf ID, 'Invalid data');
        System.assertEquals('Updated Configuration Screen', cs.Name, 'Invalid data'); 
        System.assert(retval2 InstanceOf ID, 'Invalid data');
    }
    
    @IsTest
    public static void saveConfiguration() {

        cscfga__Configuration_Screen__c cs = [SELECT Id, Name FROM cscfga__Configuration_Screen__c LIMIT 1];
        cscfga__Product_Definition__c pd = [SELECT Id, Name FROM cscfga__Product_Definition__c LIMIT 1];
        
        List<SFD_ScreenFlowDesigner.ScreenSection> items = new List<SFD_ScreenFlowDesigner.ScreenSection>();
 
		cscfga__Screen_Section__c ss = SFD_TestHelper.createScreenSection('New Section', cs, true);
        
        cscfga__Attribute_Definition__c ad =  SFD_TestHelper.createAttributeDefinition('New Attribute Definition', pd, true);
        
        cscfga__Attribute_Screen_Section_Association__c assa = SFD_TestHelper.createScreenSectonAssociation(ad, ss, false);
        assa.cscfga__column_sequence__c = 0;
        insert assa;
        
        cscfga__Attribute_Screen_Section_Association__c aobj = [SELECT Id,
                                                                    cscfga__Row_Sequence__c,
                                                                    cscfga__column_sequence__c, 
                                                                    cscfga__Screen_Section__c, 
                                                                    cscfga__Attribute_Definition__c,
                                                                    cscfga__Attribute_Definition__r.Name
                                                                FROM cscfga__Attribute_Screen_Section_Association__c
                                                                WHERE Id = :assa.Id
                                                                ORDER BY cscfga__row_sequence__c ASC LIMIT 1];

        aobj.cscfga__column_sequence__c = 1;
        
        SFD_ScreenFlowDesigner.ScreenSection section = new SFD_ScreenFlowDesigner.ScreenSection();
        section.Id = ss.Id;
        section.Name = ss.Name;
        section.columns = new List<List<SFD_ScreenFlowDesigner.Item>>();
        
        List<SFD_ScreenFlowDesigner.Item> left = new List<SFD_ScreenFlowDesigner.Item>();
        List<SFD_ScreenFlowDesigner.Item> right = new List<SFD_ScreenFlowDesigner.Item>();
        
        SFD_ScreenFlowDesigner.Item attr = new SFD_ScreenFlowDesigner.Item();
        
        attr.Id = aobj.cscfga__Attribute_Definition__c;
        attr.Name = aobj.cscfga__Attribute_Definition__r.Name;
        attr.locked = true;
        
        attr.Column = aobj.cscfga__column_sequence__c;
        attr.ItemDefinition = aobj.cscfga__Attribute_Definition__r;
        
        attr.AssociationId = aobj.Id;
        attr.ScreenSectionId = aobj.cscfga__screen_section__c;
        
        if (aobj.cscfga__column_sequence__c > 0) {
            right.add(attr);
        } else {
            left.add(attr);
        }
        
        cscfga__Attribute_Definition__c ad2 =  SFD_TestHelper.createAttributeDefinition('New Attribute Definition', pd, true);
        SFD_ScreenFlowDesigner.Item attr2 = new SFD_ScreenFlowDesigner.Item();
        
        attr2.Id = ad2.Id;
        attr2.Name = 'New one';
        attr2.locked = true;
        
        attr2.Column = 0;
        attr2.ItemDefinition = ad2;
        
        attr2.ScreenSectionId = ss.Id;
        
        left.add(attr2);
        
        section.columns.add(left);
        section.columns.add(right);
        
        items.add(section);
        
        // End Prepare ScreenFlowDesigner.ScreenSection data
        
		Test.startTest();
        
		SFD_ScreenFlowDesigner.SaveData retval = SFD_ScreenFlowDesigner.saveConfiguration(cs.Id, JSON.serialize(cs), JSON.serialize(items));
        
        Test.stopTest();
        
        cscfga__Attribute_Screen_Section_Association__c assaNew = [SELECT Id, cscfga__column_sequence__c FROM cscfga__Attribute_Screen_Section_Association__c WHERE Id = :assa.Id];
        
        System.assertEquals('Success!', retval.msg, 'Invalid data');
        System.assertEquals(0, assa.cscfga__column_sequence__c, 'Invalid data');
        System.assertEquals(1, assaNew.cscfga__column_sequence__c, 'Invalid data');
    }
    
    @IsTest
    public static void cloneScreenFlowTree() {
        
        cscfga__Screen_Flow__c obj = [SELECT Id, Name, cscfga__Template_Reference__c FROM cscfga__Screen_Flow__c WHERE Name = 'Test' LIMIT 1];
        
        Test.startTest();
        
		String retval = SFD_ScreenFlowDesigner.cloneScreenFlowTree(obj.Id);
        
        Test.stopTest();
        
        System.assert(retval InstanceOf ID, 'Invalid data');
        
        obj = [SELECT Id, Name, cscfga__Template_Reference__c FROM cscfga__Screen_Flow__c WHERE Id = :retval LIMIT 1];
        
        System.assertEquals('Test Clone', obj.Name, 'Invalid data');
    }
    
    @IsTest
    public static void getInit() {
        
        Test.startTest();
        
        SFD_ScreenFlowDesigner.Model obj = SFD_ScreenFlowDesigner.getInit();
        
        Test.stopTest();
        
        System.assertEquals(1, obj.screenFlows.size(), 'Invali data');
        System.assertEquals('Test', obj.screenFlows[0].Name, 'Invali data');
    }
    
    @IsTest
    public static void deleteSectionAssociations() {
        
        cscfga__Screen_Section__c obj = [SELECT Id, Name FROM cscfga__Screen_Section__c WHERE Name = 'Test Section'];
        
        Test.startTest();
        
        String retval = SFD_ScreenFlowDesigner.deleteSectionAssociations(obj.Id);
        
        Test.stopTest();       
        
        System.assertEquals('Success!', retval, 'Invalid data');
    }
    
    @IsTest
    public static void deleteItemSF() {

        cscfga__Screen_Flow__c obj = [SELECT Id, Name FROM cscfga__Screen_Flow__c LIMIT 1];

        Test.startTest();
        
        String retval = SFD_ScreenFlowDesigner.deleteItem(obj.Id);
        
        Test.stopTest();       
        
        System.assertEquals('Success!', retval, 'Invalid data');
    }
    
    @IsTest
    public static void deleteItemCS() {

        cscfga__Configuration_Screen__c obj = [SELECT Id, Name FROM cscfga__Configuration_Screen__c LIMIT 1];

        Test.startTest();
        
        String retval = SFD_ScreenFlowDesigner.deleteItem(obj.Id);
        
        Test.stopTest();       
        
        System.assertEquals('Success!', retval, 'Invalid data');
    }
    
    @IsTest
    public static void deleteItemSS() {
        
        cscfga__Screen_Section__c obj = [SELECT Id, Name FROM cscfga__Screen_Section__c LIMIT 1];

        Test.startTest();
        
        String retval = SFD_ScreenFlowDesigner.deleteItem(obj.Id);
        
        Test.stopTest();       
        
        System.assertEquals('Success!', retval, 'Invalid data');
    }
    
    @IsTest
    public static void deleteItemSSA() {
        
        cscfga__Attribute_Screen_Section_Association__c obj = [SELECT Id, Name FROM cscfga__Attribute_Screen_Section_Association__c LIMIT 1];

        Test.startTest();
        
        String retval = SFD_ScreenFlowDesigner.deleteItem(obj.Id);
        
        Test.stopTest();       
        
        System.assertEquals('Success!', retval, 'Invalid data');
        
    }
}