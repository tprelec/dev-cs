/**
 * Test helper for SFD_ScreenFlowDesignerTest
 *
 * @author Petar Miletic
 * @story PPA Strech Objective
 * @since 02-04/2017
*/
@IsTest
public class SFD_TestHelper {
    
    public static cscfga__Screen_Flow__c createScreenFlow(String objectName, String templateReference, Boolean autoInsert) {
        
        cscfga__Screen_Flow__c obj = new cscfga__Screen_Flow__c();
        obj.Name = objectName;
        obj.cscfga__Template_Reference__c = templateReference;
        
        if (autoInsert) {
            
            insert obj;
        }
        
        return obj;
    }
    
    public static cscfga__Configuration_Screen__c createConfigurationScreen(String objectName, cscfga__Screen_Flow__c screenFlow, Boolean autoInsert) {
        
        cscfga__Configuration_Screen__c obj = new cscfga__Configuration_Screen__c();
        obj.Name = objectName;
        obj.cscfga__Screen_Flow__c = screenFlow.Id;
        
        if (autoInsert) {
            
            insert obj;
        }
        
        return obj;
    }
    
    public static cscfga__Screen_Section__c createScreenSection(String objectName, cscfga__Configuration_Screen__c configurationScreen, Boolean autoInsert) {
        
        cscfga__Screen_Section__c obj = new cscfga__Screen_Section__c();
        obj.Name = objectName;
        obj.cscfga__Configuration_Screen__c = configurationScreen.Id;
        
        if (autoInsert) {
            
            insert obj;
        }
        
        return obj;
    }
    
    public static cscfga__Product_Definition__c createProductDefinition(String objectName, String description, Boolean autoInsert) {
        
        cscfga__Product_Definition__c obj = new cscfga__Product_Definition__c();
        obj.Name = objectName;
        obj.cscfga__Description__c = description;
        
        if (autoInsert) {
            
            insert obj;
        }
        
        return obj;
    }
    
    public static cscfga__Attribute_Definition__c createAttributeDefinition(String objectName, cscfga__Product_Definition__c productDefinition, Boolean autoInsert) {
        
        cscfga__Attribute_Definition__c obj = new cscfga__Attribute_Definition__c();
        obj.Name = objectName;
        obj.cscfga__Product_Definition__c = productDefinition.Id;
        
        if (autoInsert) {
            
            insert obj;
        }
        
        return obj;
    }
    
    public static cscfga__Attribute_Screen_Section_Association__c createScreenSectonAssociation(cscfga__Attribute_Definition__c attributeDefinition, cscfga__Screen_Section__c screenSection, Boolean autoInsert) {
        
        cscfga__Attribute_Screen_Section_Association__c obj = new cscfga__Attribute_Screen_Section_Association__c();
        obj.cscfga__Attribute_Definition__c = attributeDefinition.Id;
        obj.cscfga__Screen_Section__c = screenSection.Id;

        if (autoInsert) {
            
            insert obj;
        }
        
        return obj;
    }
    
    
}