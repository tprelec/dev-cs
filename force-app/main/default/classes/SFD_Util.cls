public class SFD_Util {

    public static Map<String, Set<String>> getObjectFields(String[] types) {

        Map<String, Set<String>> retval = new Map<String, Set<String>>();
        
        // Make the describe call
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
                
        // For each returned result, get some info
        for(Schema.DescribeSobjectResult res : results) {

            Set<String> fieldNames = new Set<String>();

            for (SObjectField obj :res.fields.getMap().values()) {

                Schema.DescribeFieldResult result = obj.getDescribe();

                fieldNames.add(result.getName());
            }

			retval.put(res.getName(), fieldNames);
        }

        return retval;
    }
    
	public static String getColumns(Set<String> fields) {

	    List<String> columns = new List<String>(fields);

        return String.join(columns, ',');
	}
	
	/*
	 * Return empty string if string is null
	*/
	public static string checkNull(string input) {
		return String.isBlank(input) ? '' : input;
	}
	
	/*
	 * Standard GUID regex pattern, used by Configurator for temoporary IDs
	*/
	private static string GUID {
		get {
			return '([a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-4[a-zA-Z0-9]{3}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12})';
		}
	}
	/*IsValidId
	 * Validate standard GUID field
	*/
	private static Boolean ValidateGUID(string input) {

		input = checkNull(input);

		return Pattern.matches(GUID, input);
	}
	
	/*
	 * Validate SalesForce ID
	*/
	public static Boolean IsValidId(String strId) {

		return ((strId InstanceOf ID) || ValidateGUID(strId));
	}
}