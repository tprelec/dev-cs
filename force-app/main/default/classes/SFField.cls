public with sharing class SFField implements Comparable{
	@AuraEnabled
	public String value;
	@AuraEnabled
	public String label;
	public String name;
	public Integer sequence = 0;
	@AuraEnabled	
	public String fieldType;
	@AuraEnabled	
	public Boolean showInUI = true;
	
	
	public SFField(String name, String label, Integer sequence, Boolean showInUI) {
		this.value = name;
		this.label = label;
		if (showInUI != null) {
			this.showInUI = showInUI;
		}

		if (sequence != null) {
			this.sequence = sequence;
		}
	}
	
	public SFField(String name, String label) {
		this.value = name;
		this.label = label;
	}
	
	public SFField() {
	}
	
	public Integer compareTo(Object compareTo) {
		SFField compareToEmp = (SFField)compareTo;
		
		if (sequence == compareToEmp.sequence) return 0;
		if (sequence > compareToEmp.sequence) return 1;
		return -1;
	}

}