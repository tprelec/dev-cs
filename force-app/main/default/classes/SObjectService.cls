public inherited sharing class SObjectService {

    private static Map<sObjectType, Map<String, Schema.SObjectField>> objectTypeToFields = new Map<sObjectType, Map<String, Schema.SObjectField>>();

    public static String getObjectFieldsAsCSV(sObjectType objectType) {
        Map<String, Schema.SObjectField> fieldMap = getObjectFields(objectType);
        return String.join(new List<String>(fieldMap.keySet()), ',');
    }

    public static Map<String, Schema.SObjectField> getObjectFields(sObjectType objectType) {
        Map<String, Schema.SObjectField> fieldMap;
        if (objectTypeToFields.containsKey(objectType)) {
            return objectTypeToFields.get(objectType);
        }
        fieldMap = objectType.getDescribe().fields.getMap();
        objectTypeToFields.put(objectType, fieldMap);
        return fieldMap;
    }

}