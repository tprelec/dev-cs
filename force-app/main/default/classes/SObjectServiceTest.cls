@isTest
public with sharing class SObjectServiceTest {

    @isTest
    private static void testObjectFields() {
        Map<String, Schema.SObjectField> accountFields = SObjectService.getObjectFields(Account.getSObjectType());
        String accountFieldsCSV = SObjectService.getObjectFieldsAsCSV(Account.getSObjectType());
        System.assert(!accountFields.isEmpty(), 'Account should have some fields available.');
        System.assertEquals(accountFields.size(), accountFieldsCSV.split(',').size(), 'Number of Account fields should match.');
    }

}