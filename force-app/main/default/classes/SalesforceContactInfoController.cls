public with sharing class SalesforceContactInfoController {

	public void checkUserEmail(){
		// check if the current user's email address has changed
		// this has to be done here as this is a homepage component that is loaded for every user
		// and the APEX trigger on User cannot detect an Email address change on User (SFDC BUG!)
		//
		// Additionally check that the bop_reference__c field is in sync between user and contact
		User u = [Select Id, Email, BOP_reference__c from User Where Id = :UserInfo.getUserId()];

        Contact[] c = [Select Id, Email, BOP_reference__c From Contact Where UserId__c = :u.Id];
        if(!c.isEmpty()){
        	// Make sure the contact has the same email as the user email
        	if(c[0].Email != u.Email){
            	c[0].Email = u.Email;
				c[0].Hidden_allow_update_contact__c = system.now();	               	
        		SharingUtils.updateRecordsWithoutSharing(c[0]);
        	}
        	// Make sure the user has the same bop_reference__c as the contact
        	if (u.BOP_reference__c!=c[0].BOP_reference__c) {
        		u.BOP_reference__c=c[0].BOP_reference__c;
        		SharingUtils.updateRecordsWithoutSharing(u);        		
        	}
        }
	}

}