/**
 * @description         This class schedules the batch processing of Account export.
 * @author              Guy Clairbois
 */
global class ScheduleAccountExportBatch implements Schedulable {
    
    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){
        
		AccountExportBatch accountBatch = new AccountExportBatch();
		Id batchprocessId = Database.executeBatch(accountBatch,100);
    }
    
}