/**
 * @description         This class schedules the batch processing of Account export.
 * @author              Marcel Vreuls
 */
global class ScheduleAccountUpdatetBatch implements Schedulable {
    
    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){
        
		AccountOlbicoUpdateBatch accountBatch = new AccountOlbicoUpdateBatch();
		Id batchprocessId = Database.executeBatch(accountBatch,1);
    }
    
}