/**
 * @description         This class schedules the batch processing of Contact export.
 * @author              Guy Clairbois
 */
global class ScheduleContactExportBatch implements Schedulable {
    
    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){
        
		ContactExportBatch contactBatch = new ContactExportBatch();
		Id batchprocessId = Database.executeBatch(contactBatch,100);
    }
    
}