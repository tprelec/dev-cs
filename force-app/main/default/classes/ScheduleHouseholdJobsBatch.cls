/**
 * @description         This class schedules the batch processing of Household jobs.
 * @author              Guy Clairbois
 */
global class ScheduleHouseholdJobsBatch implements Schedulable {

    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){

    	if(Job_Management__c.getInstance().BPAccountSharingCleanup__c){
			BPAccountSharingCleanupBatch bpCleaningBatch = new BPAccountSharingCleanupBatch();
			Id batchprocessId = Database.executeBatch(bpCleaningBatch,100);
		}
	}
}