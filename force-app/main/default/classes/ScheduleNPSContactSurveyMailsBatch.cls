global class ScheduleNPSContactSurveyMailsBatch implements Database.Batchable<sObject>, Schedulable, Database.Stateful {

    global FINAL String strQuery;
    global FINAL Date normalDate = Date.today().addDays(14);
    global FINAL Date reminderDate = Date.today().addDays(7);

    global ScheduleNPSContactSurveyMailsBatch() {
        this.strQuery = getBatchQuery();
    }

    private String getBatchQuery() {
        //String strQuery = 'SELECT Id, Name, Owner.Name, OwnerId, Owner.Email FROM Account WHERE NPS_Survey_Date__c = NEXT_N_DAYS:14 AND NPS_Pause__c = false';
        String strQuery = 'SELECT Id, Name, Owner.Name, OwnerId, Owner.Email, NPS_Survey_Date__c FROM Account WHERE (NPS_Survey_Date__c =: normalDate AND NPS_Reminder__c = false AND NPS_Pause__c = false AND NPS_Status__c != \'Completed\') OR (NPS_Survey_Date__c =: reminderDate AND NPS_Reminder__c = true AND NPS_Pause__c = false AND NPS_Status__c != \'Completed\')';
        return strQuery;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scopeList) {
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();

        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'ebusalesforce.nl@vodafone.com'];
        EmailTemplate et = [Select Id, Name,HTMLValue,subject,Body from EmailTemplate where DeveloperName = 'NPS_Survey_Email'];
        EmailTemplate etReminder = [Select Id, Name,HTMLValue,subject,Body from EmailTemplate where DeveloperName = 'Reminder_NPS_Survey_Email'];

        System.debug(LoggingLevel.INFO, '== scopeList size ==' + scopeList.size());
        Map<Id, List<Account>> ownerAccountMap = new Map<Id, List<Account>>();
        List<Account> accList = (List<Account>) scopeList;
        for (Account acc : accList) {
            if (acc.NPS_Survey_Date__c == normalDate) {
                acc.NPS_Started__c = Date.today();
                acc.NPS_Status__c = 'Started';
            }

            if (ownerAccountMap.containsKey(acc.OwnerId)) {
                List<Account> accTmpList = ownerAccountMap.get(acc.OwnerId);
                accTmpList.add(acc);
                ownerAccountMap.put(acc.OwnerId, accTmpList);
            } else {
                ownerAccountMap.put(acc.OwnerId, new List<Account> {acc});
            }
        }

        for (Id ownerId : ownerAccountMap.keySet()) {
            String plainBody;
            String htmlBody;

            String accLinksPlain = '';
            String accLinksHtml = '';
            List<Account> accTmpList = ownerAccountMap.get(ownerId);
            Integer counter = 0;

            Boolean useNormalDate = true;
            for (Account acc : accTmpList) {
                if (acc.NPS_Survey_Date__c == reminderDate) {
                    useNormalDate = false;
                }
                if (accTmpList.size() > 1 && counter == 0) {
                    accLinksPlain += '\n';
                    accLinksHtml += '<br />';
                }
                accLinksPlain += acc.Name + '\n';
                accLinksHtml += '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+acc.Id+'">'+acc.Name+'</a><br />';
                counter += 1;
            }

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(ownerId);
            if (useNormalDate) {
                plainBody = et.Body;
                htmlBody = et.HTMLValue;
                mail.setTemplateId(et.id);
                mail.setSubject(et.subject);
            } else {
                plainBody = etReminder.Body;
                htmlBody = etReminder.HTMLValue;
                mail.setTemplateId(etReminder.id);
                mail.setSubject(etReminder.subject);
            }
            mail.setCcAddresses(new List<String> {'ebusalesforce@vodafoneziggo.com'});
            mail.setSaveAsActivity(false);
            mail.setTreatBodiesAsTemplate(false);

            if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }

            String surveyDate = '';
            surveyDate = String.valueOf(reminderDate.day());
            surveyDate += '-';
            surveyDate += String.valueOf(reminderDate.month());
            surveyDate += '-';
            surveyDate += String.valueOf(reminderDate.year());

            htmlBody = htmlBody.replace('{!Account.OwnerFullName}',accTmpList[0].Owner.Name);
            htmlBody = htmlBody.replace('{!Account.Link}',accLinksHtml);
            htmlBody = htmlBody.replace('{!Account.NPS_Survey_Date__c}',surveyDate);
            plainBody = plainBody.replace('{!Account.OwnerFullName}',accTmpList[0].Owner.Name);
            plainBody = plainBody.replace('{!Account.Link}',accLinksPlain);
            plainBody = plainBody.replace('{!Account.NPS_Survey_Date__c}',surveyDate);

            //mail.setHtmlBody(htmlBody);
            mail.plainTextBody = plainBody;
            emailList.add(mail);
        }
        if (emailList.size() > 0) {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(emailList);
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
            }
        }
        if (accList.size() > 0) {
            update accList;
        }
    }


    global void finish(Database.BatchableContext BC) {}

    global void execute(SchedulableContext sc) {
        ScheduleNPSContactSurveyMailsBatch snInstance = new ScheduleNPSContactSurveyMailsBatch();
        ID batchprocessid = Database.executeBatch(snInstance);
    }
}