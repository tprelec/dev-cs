/**
 * @description         This class schedules the batch processing of the SAP Hardware Automation.
 * @author              Ferdinand Bondt
 */
global class ScheduleSAPHardwareAutomation implements Schedulable {
 
    /**
     * @description         This method executes the batch job.
     */
    global void execute(SchedulableContext SBatch) {
    	
        SAPHardwareAutomation sha = new SAPHardwareAutomation();
        ID batchprocessid = Database.executeBatch(sha);
    }
}