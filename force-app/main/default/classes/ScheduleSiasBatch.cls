/**
 * @description         This class schedules the processing of recurring contracted products, create billing transaction and send to SIAS.
 * @author              Rahul Sharma
 */
public without sharing class ScheduleSiasBatch implements Schedulable {
	// variable for batch name
	private static final String SIAS_BATCH_NAME = 'SIAS Job';

	//Marked as readonly to get higher SOQL governor limits
	@ReadOnly
	public void execute(SchedulableContext sc) {
		callSiasBatch();
	}

	// aggregates the order and send to SIAS with a batch class
	@TestVisible
	private static void callSiasBatch() {
		// query all approved CPs
		Set<Id> setOrderIds = new Set<Id>();
		for (Contracted_Products__c cp : [
			SELECT
				Id,
				Row_Type__c,
				Order__c,
				Net_Unit_Price__c,
				Discount__c,
				Customer_Asset__r.Price__c,
				Customer_Asset__r.Discount__c
			FROM Contracted_Products__c
			WHERE
				Order__c != NULL
				AND Delivery_Status__c = :Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED
				AND Billing_Status__c = :Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW
				AND Customer_Asset__c != NULL
				AND Order__r.O2C_Order__c = TRUE
				AND Do_not_export__c = FALSE
				AND Billing__c = TRUE
		]) {
			if (
				!(cp.Row_Type__c == 'Price Change' &&
				cp.Net_Unit_Price__c == cp.Customer_Asset__r.Price__c &&
				cp.Discount__c == cp.Customer_Asset__r.Discount__c)
			) {
				Id orderId = cp.Order__c;
				setOrderIds.add(orderId);
			}
		}

		// As 'Database.executeBatch' is treated as DML due to which callout fails, run th batch's code in future context
		if (Test.isRunningTest()) {
			SiasBatch.executeCalloutAsFuture(setOrderIds);
		} else {
			// as callout limit is 100 per transaction, set batch size as 25 to be on safe side
			// send contracted products per order to SIAS for billing
			Database.executeBatch(new SiasBatch(setOrderIds), 25);
		}
	}

	public static void scheduleInFiveMinutes() {
		Integer nextFiveMinute = DateTime.now().minute();
		// in order to prevent scheduling conflicts add 1 minute
		nextFiveMinute++;

		// find the next 5th minute divisible by 5
		do {
			nextFiveMinute++;
		} while (Math.mod(nextFiveMinute, 5) != 0);

		if (nextFiveMinute >= 59) {
			nextFiveMinute = 0;
		}

		System.schedule(
			SIAS_BATCH_NAME,
			'0 ' +
			nextFiveMinute +
			' * * * ?',
			new scheduleSiasBatch()
		);
	}

	public static void abort() {
		for (CronTrigger cronTrigger : [
			SELECT Id
			FROM CronTrigger
			WHERE CronJobDetail.Name LIKE :SIAS_BATCH_NAME
		]) {
			System.abortJob(cronTrigger.Id);
		}
	}
}