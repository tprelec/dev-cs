/**
 * @description         This class schedules the batch processing of Site export.
 * @author              Guy Clairbois
 */
global class ScheduleSiteExportBatch implements Schedulable {
    
    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){
        
		SiteExportBatch siteBatch = new SiteExportBatch();
		Id batchprocessId = Database.executeBatch(siteBatch,10);
    }
    
}