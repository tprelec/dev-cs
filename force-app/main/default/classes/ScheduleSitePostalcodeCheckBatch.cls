/**
 * @description         This class schedules the batch processing of postalcode checks.
 * @author              Guy Clairbois
 */
global class ScheduleSitePostalcodeCheckBatch implements Schedulable {
    
    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){
        
		SitePostalcodeCheckBatch pcCheckBatch = new SitePostalcodeCheckBatch();
		Id batchprocessId = Database.executeBatch(pcCheckBatch,2);
    }
}