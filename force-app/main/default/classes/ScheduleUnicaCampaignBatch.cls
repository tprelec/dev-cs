/**
 * @description         This class schedules the batch processing of the Unica Campaign.
 * @author              Guy Clairbois
 */
global class ScheduleUnicaCampaignBatch implements Schedulable {
 
    /**
     * @description         This method executes the batch job.
     */
    global void execute(SchedulableContext SBatch) {
    	
        UnicaCampaignBatch ucb = new UnicaCampaignBatch();
        ID batchprocessid = Database.executeBatch(ucb);
    }
}