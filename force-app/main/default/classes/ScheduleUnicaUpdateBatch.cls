/**
 * @description         This class schedules the batch processing of the Unica Update.
 * @author              Stjepan Pavuna
 */
global class ScheduleUnicaUpdateBatch implements Schedulable {
 
    /**
     * @description         This method executes the batch job.
     */
    global void execute(SchedulableContext SBatch) {
        
        UnicaUpdateBatch uub = new UnicaUpdateBatch();
        ID batchprocessid = Database.executeBatch(uub);
    }
}