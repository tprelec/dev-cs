/**
 * @description         Used for scheduling the UpdateBillingStatusBatch
 * @author              Jurgen van Westreenen
 */
public without sharing class ScheduleUpdateBillingStatusBatch implements Schedulable {
	public void execute(SchedulableContext sc) {
		UpdateBillingStatusBatch ubsBatch = new UpdateBillingStatusBatch();
		Database.executeBatch(ubsBatch, 200);
	}
}