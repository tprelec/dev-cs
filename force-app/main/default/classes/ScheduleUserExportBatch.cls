/**
 * @description         This class schedules the batch processing of User export.
 * @author              Guy Clairbois
 */
global class ScheduleUserExportBatch implements Schedulable {
    
    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){
        
		UserExportBatch userBatch = new UserExportBatch();
		Id batchprocessId = Database.executeBatch(userBatch,100);
    }
    
}