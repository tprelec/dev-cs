/**
 * @description         This class schedules the batch processing of leadscores.
 * @author              Marcel Vreuls
* @History				      Juni 2019: Calling new version of API service 
 */
global class ScheduleZiggoLeadScoreUpdate implements Schedulable {
    
    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){
        
      ZiggoLeadScoreUpdateBatch leadBatch = new ZiggoLeadScoreUpdateBatch();
      Id batchprocessId = Database.executeBatch(leadBatch,5);
    }
    
}