public with sharing class SendOrderConfirmationCtrl {
	private static final String NEW_CONFIRMATION = 'Send new confirmation (without signed quote)';
	private static final String SIGNED_CONFIRMATION = 'Send signed quote';
	private static final String DOC_TEMPLATE_NAME = 'Ziggo Order Confirmation';
	private static final String EMAIL_TEMPLATE_SIGNED = 'Ziggo Order Confirmation - Signed';
	private static final String EMAIL_TEMPLATE_UNSIGNED = 'Ziggo Order Confirmation - Unsigned';

	@AuraEnabled
	public static void validateOpportunity(Id oppId) {
		Opportunity opp = getOpportunity(oppId);
		if (opp.LG_PrimaryContact__r?.Email == null) {
			throw new AuraHandledException(
				'Please check if primary contact is added and has a valid email address.'
			);
		}
		List<OpportunityLineItem> olis = [
			SELECT Id
			FROM OpportunityLineItem
			WHERE OpportunityId = :opp.Id
		];
		if (olis.isEmpty()) {
			throw new AuraHandledException(
				'No Opportunity Products. Please make sure you have selected products before sending confirmation.'
			);
		}
	}

	@AuraEnabled
	public static List<AuraSelectOption> getAvailableOptions(Id oppId) {
		Opportunity opp = getOpportunity(oppId);
		List<AuraSelectOption> options = new List<AuraSelectOption>();
		options.add(new AuraSelectOption(NEW_CONFIRMATION, NEW_CONFIRMATION));
		if (opp.LG_SignedQuoteAvailable__c == 'true') {
			options.add(new AuraSelectOption(SIGNED_CONFIRMATION, SIGNED_CONFIRMATION));
		}
		return options;
	}

	@AuraEnabled
	public static mmdoc__Document_Request__c createNewConfirmation(Id oppId) {
		mmdoc__Document_Template__c docTemplate = MavenDocumentsService.getDocumentTemplate(
			DOC_TEMPLATE_NAME
		);
		return MavenDocumentsService.createDocRequest(
			oppId,
			docTemplate,
			'Attachment',
			'PDF',
			'Queue',
			true
		);
	}

	@AuraEnabled
	public static mmdoc__Document_Request__c getDocumentRequest(Id requestId) {
		return MavenDocumentsService.getDocumentRequest(requestId);
	}

	@AuraEnabled
	public static void sendNewConfirmation(Id oppId, Id attachmentId) {
		sendConfirmation(oppId, attachmentId, getEmailTemplate(EMAIL_TEMPLATE_UNSIGNED).Id);
	}

	@AuraEnabled
	public static void sendSignedConfirmation(Id oppId) {
		Opportunity opp = getOpportunity(oppId);
		sendConfirmation(
			oppId,
			(Id) opp.LG_SignedQuoteAttachmentId__c,
			getEmailTemplate(EMAIL_TEMPLATE_SIGNED).Id
		);
	}

	private static void sendConfirmation(Id oppId, Id attachmentId, Id emailTemplateId) {
		Opportunity opp = getOpportunity(oppId);
		EmailService.sendEmail(
			opp.LG_PrimaryContact__c,
			opp.Id,
			null,
			null,
			null,
			emailTemplateId,
			new List<Id>{ attachmentId },
			null
		);
	}

	private static Opportunity getOpportunity(Id oppId) {
		return [
			SELECT
				Id,
				LG_SignedQuoteAvailable__c,
				LG_SignedQuoteAttachmentId__c,
				LG_PrimaryContact__c,
				LG_PrimaryContact__r.Email
			FROM Opportunity
			WHERE Id = :oppId
		];
	}

	private static EmailTemplate getEmailTemplate(String name) {
		return [SELECT Id FROM EmailTemplate WHERE Name = :name];
	}
}