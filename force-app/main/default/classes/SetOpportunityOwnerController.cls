/**
 * @description         Allows Admins and SAG to easily change the opportunity ownership to the General Mobile owner or Fixed Owner (W-000132)
 * @author              Gerhard Newman
 */
public without sharing class SetOpportunityOwnerController {

    private Opportunity opp {get;set;}
    private ApexPages.StandardController stdController;    
    public Account acc {get;set;}
    public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}

    public SetOpportunityOwnerController(ApexPages.StandardController stdController) {
        this.stdController = stdController;        
        opp = (Opportunity)stdController.getRecord();
        opp = [select accountid, Assigned_SAG_User__c, StageName from opportunity where id=:this.opp.id];
        acc = [select Fixed_Dealer__r.name, Fixed_Dealer__r.ContactUserId__c, Fixed_Dealer__r.Contact__r.UserActive__c, Mobile_Dealer__r.Contact__r.UserActive__c, Mobile_Dealer__r.name, Mobile_Dealer__r.ContactUserId__c from account where id=:this.opp.accountid];    
        shouldRedirect = false;  
    }

    // True if there is a general (mobile) owner 
    // and the current user is authorised
    // and the dealer user is active
    //
    // Additionally if the Opportunity is Closed Won or Closed Lost then the user
    // must have permission to edit closed opportunities
    public Boolean getEnableSetGeneralOwner() {
        if (acc.Mobile_Dealer__r.ContactUserId__c!=null && acc.Mobile_Dealer__r.Contact__r.UserActive__c) {
            if ((opp.StageName=='Closed Won' || opp.StageName=='Closed Lost') && !Special_Authorizations__c.getInstance().Edit_Closed_Opportunities__c) {
                return false;
            } else {
                return Special_Authorizations__c.getInstance().Set_Opportunity_Owner__c;   
            }
        } else {
            return false;
        }        
    }

    // True if there is a fixed owner and the current user is authorised
    // and the dealer user is active 
    // Additionally if the Opportunity is Closed Won or Closed Lost then the user
    // must have permission to edit closed opportunities       
    public Boolean getEnableSetFixedOwner() {
        if (acc.Fixed_Dealer__r.ContactUserId__c!=null && acc.Fixed_Dealer__r.Contact__r.UserActive__c) {
            if ((opp.StageName=='Closed Won' || opp.StageName=='Closed Lost') && !Special_Authorizations__c.getInstance().Edit_Closed_Opportunities__c) {
                return false;
            } else {            
                return Special_Authorizations__c.getInstance().Set_Opportunity_Owner__c;   
            }
        }  else {
            return false;
        }        
    }    

    // Sets the General Owner as the opportunity owner and refreshes the main page so that the new owner is shown
    public PageReference setGeneralOwner() {
        opp.OwnerId = acc.Mobile_Dealer__r.ContactUserId__c;     
        update opp;
        putSAGonOpportunityTeam();        
        shouldRedirect = true;
        redirectUrl = stdController.view().getUrl();
        return null;        
    }

    // Sets the Fixed Owner as the opportunity owner and refreshes the main page so that the new owner is shown
    public PageReference setFixedOwner() {
        opp.OwnerId = acc.Fixed_Dealer__r.ContactUserId__c;     
        update opp;
        putSAGonOpportunityTeam();           
        shouldRedirect = true;
        redirectUrl = stdController.view().getUrl();
        return null;        
    }

    public void putSAGonOpportunityTeam() {
        // Check the current users profile
        // If SAG we want to add them to the Opportunity Team
        User u = GeneralUtils.currentUser;
        if (u.profile.name=='VF Direct Inside Sales' || u.profile.name=='VF Indirect Inside Sales') {
            // Make sure they are not already the assigned sag user
            if (u.id!=opp.Assigned_SAG_User__c){
                OpportunityTeamMember otm = new OpportunityTeamMember();
                otm.OpportunityId = opp.Id;
                otm.TeamMemberRole = 'Sales Assistance Group';
                otm.UserId = u.id;
                // Strangely no error is thrown if they are already on the team
                // Presumably this is handled somewhere in the opportunityteamtriggerhandler
                insert otm; 
            }
        }
    }

}