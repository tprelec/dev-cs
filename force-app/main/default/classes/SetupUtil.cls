/**
 * Created by bojan.zunko on 28/07/2020.
 */

global with sharing class SetupUtil {

    global static final String OG_HANDLER_NAME = 'Digital Commerce ETL';
    global static final String INTEGRATION_ENDPOINT = 'DigitalCommerceETL';
    global static final String URL_PATH = '/dc-etl/v1/sync';
    global static final String STARTPOINT_OBJECT = 'csb2c__E_Commerce_Publication__c';
    global static final String TYPE_STRATEGY = 'Id,Name';
    global static final String DEFAULT_CALLOUT_HOST = 'cs-messaging-dispatcher-eu-dev.herokuapp.com';


    global static String createRequiredCalloutSettings(Boolean invokeDML) {

        List<SObject> dmlObject = new List<SObject>();

        dmlObject.addAll(createIntegrationEndpoint(invokeDML));
        dmlObject.addAll(createOGHandlerAndStrategy(invokeDML));

        return 'Result would be ' + dmlObject;
    }



    public static List<SObject> createOGHandlerAndStrategy(Boolean invokeDML) {

        List<SObject> returnList = new List<SObject>();
        List<csam__ObjectGraph_Callout_Handler__c> checkIfExists = [SELECT ID FROM csam__ObjectGraph_Callout_Handler__c WHERE Name = :OG_HANDLER_NAME];

        if (checkIfExists.size() == 0) {
            csam__ObjectGraph_Callout_Handler__c etlHandler = new csam__ObjectGraph_Callout_Handler__c();
            etlHandler.Name = OG_HANDLER_NAME;
            etlHandler.csam__Method__c = 'POST';
            etlHandler.csam__Integration_Endpoint_Name__c = INTEGRATION_ENDPOINT;
            etlHandler.csam__URL_Path__c = URL_PATH;
            etlHandler.csam__Startpoint_Type_Name__c = STARTPOINT_OBJECT;

            if (invokeDML) {
                insert etlHandler;
            }
            returnList.add(etlHandler);


            csam__ObjectGraph_Callout_Strategy__c etlStrategy = new csam__ObjectGraph_Callout_Strategy__c();
            etlStrategy.Name = STARTPOINT_OBJECT;
            etlStrategy.csam__Type_Strategy__c = TYPE_STRATEGY;
            etlStrategy.csam__ObjectGraph_Callout_Handler__c = etlHandler.Id;

            if (invokeDML) {
                insert etlStrategy;
            }
            returnList.add(etlStrategy);
        }
        return returnList;
    }

    public static List<SObject> createIntegrationEndpoint(Boolean invokeDML) {

        List<SObject> returnList = new List<SObject>();
        List<csam__Integration_Endpoint__c> checkIfExists = [SELECT ID FROM csam__Integration_Endpoint__c WHERE Name = :INTEGRATION_ENDPOINT];

        if (checkIfExists.size() == 0) {
            csam__Integration_Endpoint__c integrationEndpoint = new csam__Integration_Endpoint__c();
            integrationEndpoint.Name = INTEGRATION_ENDPOINT;
            integrationEndpoint.csam__Callout_Host__c = DEFAULT_CALLOUT_HOST;

            if(invokeDML) {
                insert integrationEndpoint;
            }
            returnList.add(integrationEndpoint);
        }
        return returnList;
    }
}