@isTest
public class SetupUtilTest {

    private static testMethod void testExecute() {

        Test.startTest();

        String returnString = SetupUtil.createRequiredCalloutSettings(true);

        System.assert(returnString.contains('Result would be'), 'Good return.');

        Test.stopTest();
    }
}