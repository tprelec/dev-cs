/**
 * @description         This scheduled class creates a sharing rule. Required in order to schedule this task as
 *                      calculating the rules takes processing time and only one rule on an object can be 
 *                      actively processed. ie This get's scheduled when something else is finished.
 *                      Note:   Future method could not be used as this needs to be scheduled
 *                              A schedulable class could not be used as they do not permit callouts
 * @author              Gerhard Newman
 */
global class SharingOwnerRuleSchedulable implements Schedulable, Database.AllowsCallouts, Database.Batchable<sObject> {
    String objectName;
    List<String> dshList;
    String accessLevel;
    String sessionId;
    String shareType;

    public void execute(SchedulableContext SC) {
        Database.executebatch(new SharingOwnerRuleSchedulable(ObjectName, DshList, AccessLevel, SessionId, ShareType));
    }

    // This is where the main logic is run.
    public Iterable<sObject> start(Database.Batchablecontext BC){
        DealerInfoTriggerHandler.createDealerSharingOwnerRuleHelper(objectName, dshList, accessLevel, sessionId, shareType);
        return new List<Account>(); // required to pass the start definition     
    }

    // Not required to do anything but here as required by batch definition
    public void execute(Database.BatchableContext BC, List<sObject> scope){  
        
    }

    // This is required to remove the job when it has finished
    public void finish(Database.BatchableContext info){
        List<CronTrigger> listCronTrigger  = [select Id from CronTrigger where State = 'Deleted' or State = 'Complete'];
        if(listCronTrigger.size() > 0)
        {
             for(Integer i = 0; i < listCronTrigger.size(); i++)
             {
                System.abortJob(listCronTrigger[i].Id);                      
             }
                          
        }         
    }

    //Constructor initialization and removal of old sharing rules
    global SharingOwnerRuleSchedulable(String inObjectName, List<String> inDshList, String inAccessLevel, String inSessionId, String inShareType) {
        objectName=inObjectName;
        dshList=inDshList;
        accessLevel=inAccessLevel;
        sessionId=inSessionId;
        shareType=inShareType;
    }

}