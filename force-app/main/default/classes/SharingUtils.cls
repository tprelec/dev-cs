public without sharing class SharingUtils {

    // GroupId - There are 3 options
    // 1) The dealer is not a parent dealer and has no parent dealer - group will be the Role of the dealer user
    // 2) The dealer has a parent dealer - group will be the PM's dealer group (all Partner Members) (this group is created in dealerInfoTriggerHandler.cls)
    // 3) The dealer is a parent - group will be the PM's dealer group.
    public Map<ID,ID> dealerToGroupMap = new Map<ID,ID>();

    public List<group> groupList = new List<group>();
    public List<Group> partnerGroups = new List<Group>();

    public static void rollUpNpsContacts(List<Contact> conList, Boolean isDelete) {
        Map<Id, Integer> accConCountMap = new Map<Id, Integer>();
        Map<Id, Id> deleteContactMap = new Map<Id, Id>();

        for (Contact con : conList) {
            if(con.AccountId == GeneralUtils.vodafoneAccount.Id) { continue; } // not needed for vodafone account
            if (isDelete) {
                deleteContactMap.put(con.Id, con.Id);
            }
            if (!accConCountMap.containsKey(con.AccountId)) {
                accConCountMap.put(con.AccountId, 0);
            }
        }
        if (accConCountMap.size() > 0) {
            List<Account> accToUpdate = new List<Account>();

            for (Account acc : [
                SELECT Id, Total_NPS_Contacts__c,
                (SELECT Id, AccountId, NPS_contact__c FROM Contacts WHERE NPS_contact__c = 'Yes')
                FROM Account
                WHERE Id IN : accConCountMap.keySet()
            ]) {
                for (Contact con : acc.Contacts) {
                    if (!deleteContactMap.containsKey(con.Id)) {
                        Integer countNps = accConCountMap.get(con.AccountId);
                        countNps += 1;
                        accConCountMap.put(con.AccountId, countNps);
                    }
                }
                if (acc.Total_NPS_Contacts__c != accConCountMap.get(acc.Id)) {
                	acc.Total_NPS_Contacts__c = accConCountMap.get(acc.Id);
                    accToUpdate.add(acc);
                }
            }
            update accToUpdate;
        }
    }

    public void dealerShareHelper(Set<ID> dealerSet) {
        List<String> parentCodeList = new List<String>();
        List<String> topLevelDealerName = new List<String>();
        Map<String,Dealer_Information__c> parentDealerMapByDealerCode = new Map<String,Dealer_Information__c>();
        Map<String,ID> groupNameMap = new Map<String,ID>();

        List<Dealer_Information__c> dealerList = [
            SELECT Name, HasParent__c, Parent_Dealer_Code__c, Dealer_Information__c.ContactUserId__c, Dealer_Code__c
            FROM Dealer_Information__c
            WHERE Id IN:dealerSet
        ];
        Set<ID> dealerUserSet = new Set<ID>();
        for (Dealer_Information__c di:dealerList) {
            dealerUserSet.add(di.ContactUserId__c);
            if (di.HasParent__c){
                parentCodeList.Add(di.Parent_Dealer_Code__c);
            } else {
                topLevelDealerName.add(groupName(di.name));
            }
        }

        if (!parentCodeList.isEmpty()) {
            List<Dealer_Information__c> parentDealerList = new List<Dealer_Information__c>([
                SELECT NAME, Dealer_Code__c
                FROM Dealer_Information__c
                WHERE Dealer_Code__c IN :parentCodeList AND Contact__c != null AND Is_Dealer_in_SFDC__c = true
            ]);
            for (Dealer_Information__c di : parentDealerList) {
                topLevelDealerName.add(groupName(di.name));
                parentDealerMapByDealerCode.put(di.Dealer_Code__c,di);
            }
        }

        if (!topLevelDealerName.isEmpty()) {
            partnerGroups = [SELECT name FROM group WHERE Name IN:topLevelDealerName];
            for (Group g:partnerGroups) {
                groupNameMap.put(g.name,g.id);
            }
        }

        Map<Id, User> userMap = new Map<Id, User>([SELECT userrole.DeveloperName FROM User WHERE Id in :dealerUserSet]);
        Set<String> developerNameSet = new Set<String>();
        for (Id userID:userMap.keySet()) {
            User u = userMap.get(userID);
            developerNameSet.add(u.userrole.DeveloperName);
        }
        groupList = [SELECT id, DeveloperName FROM group WHERE DeveloperName IN :developerNameSet AND type = 'RoleAndSubordinates'];
        Map<String, Id> developerNameToIDMap = new Map<String, Id>();
        for (group g : groupList) {
            developerNameToIDMap.put(g.DeveloperName,g.id);
        }
        for (Dealer_Information__c di : dealerList) {
            Id groupID;
            Dealer_Information__c parentDealer;
            String dealerID = di.id;
            if (di.HasParent__c) {
                parentDealer = parentDealerMapByDealerCode.get(di.Parent_Dealer_Code__c);
                if (parentDealer!=null) {
                    groupID = groupNameMap.get(groupName(parentDealer.name));
                }
            } else {
                groupID = groupNameMap.get(groupName(di.name));
            }
            if (groupID == null) {
                String developerName = userMap.get(di.ContactUserId__c).userrole.DeveloperName;
                groupID = developerNameToIDMap.get(developerName);
            }
            // For some reason some test cases end up with administrator user here and we need to handle it
            if (groupID == null) {
                groupID = di.ContactUserId__c; // Share directly to the user (logically not really needed but is needed to pass test)
            }
            dealerToGroupMap.put(dealerID,groupID);
        }
    }

    private static String groupName(String partnerName){
        return 'PMs:' + partnerName.left(36);
    }

    public static void createSharingForMobileDealerNonFuture(Set<Id> accountsIdSet, Map<Id, Id> mobileDealerIdMap, Map<Id, Id> oldAccIdToMobileDealerId, Boolean fromOpportunityChange) {
        Map<Id, Id> newAccountIdAccountMobileDealerIdMap = new Map<Id, Id>();
        Map<Id, Id> oldAccountIdAccountMobileDealerIdMap = new Map<Id, Id>();
        Map<Id, Id> dealerInformationToUserIdMap = new Map<Id,Id>();
        Set<Id> accIdSet = new Set<Id>();
        Map<Id,Id> roleExclusionMap= new Map<Id,Id>();
        Map<Id,Boolean> userPartnerMap = new Map<Id,Boolean>();

        List<Account> newAccounts = [SELECT Id, Mobile_Dealer__c FROM Account WHERE Id IN :accountsIdSet];

        /** There needs to be an exclusion, opp/contract owner.role == Commercial Industries Wholesale Account Manager can't be shared */
        List<UserRole> usrRoleList = [SELECT Id, Name FROM UserRole WHERE Name = 'Commercial Industries Wholesale Account Manager'];
        for (UserRole usrRole : usrRoleList) {
            roleExclusionMap.put(usrRole.Id, usrRole.Id);
        }

        /**
         * First, check if any mobile dealers changed. If not make a list for the new mobiledealers to have sharing rights
         * and a second list for the dealers where sharing rights needs to be removed
         */
        Boolean goForward = true;
        if (goForward) {
            List<Dealer_Information__c> diList = [SELECT Id, ContactUserId__c, Contact__c, Contact__r.Userid__c, Contact__r.Userid__r.Partner_User__c FROM Dealer_Information__c WHERE Id IN :mobileDealerIdMap.keySet()];
            for (Dealer_Information__c di : diList) {
                if (di.Id != null) {
                    if (di.Contact__c != null) {
                        if (di.Contact__r.Userid__c != null) {
                            dealerInformationToUserIdMap.put(di.Id, di.ContactUserId__c);
                            userPartnerMap.put(di.Contact__r.Userid__c, di.Contact__r.Userid__r.Partner_User__c);
                        }
                    }
                }
            }

            if (oldAccIdToMobileDealerId != null) {
                for (Id oldAcc : oldAccIdToMobileDealerId.keySet()) {
                    Id mobileDealerId = oldAccIdToMobileDealerId.get(oldAcc);
                    Id dealerInfoId = dealerInformationToUserIdMap.get(mobileDealerId);
                    oldAccountIdAccountMobileDealerIdMap.put(oldAcc, dealerInfoId);
                }
            }
            for (Account acc : newAccounts) {
                if (dealerInformationToUserIdMap.containsKey(acc.Mobile_Dealer__c)) {
                    newAccountIdAccountMobileDealerIdMap.put(acc.Id, dealerInformationToUserIdMap.get(acc.Mobile_Dealer__c));
                }
            }
            if (newAccountIdAccountMobileDealerIdMap != null) {
                List<OpportunityShare> insertOppShareList = new List<OpportunityShare>();
                List<OpportunityShare> updateOppShareList = new List<OpportunityShare>();
                List<OpportunityShare> deleteOppShareList = new List<OpportunityShare>();
                List<VF_Contract__Share> insertContractShareList = new List<VF_Contract__Share>();
                List<VF_Contract__Share> updateContractShareList = new List<VF_Contract__Share>();
                List<VF_Contract__Share> deleteContractShareList = new List<VF_Contract__Share>();

                Set<Id> oppIdSet = new Set<Id>();
                Set<Id> contractIdSet = new Set<Id>();
                Map<Id, Id> oppOwnerMap = new Map<Id, Id>();
                List<Opportunity> oppList = [
                    SELECT Id, Name, AccountId, OwnerId, Owner.Partner_User__c
                    FROM Opportunity
                    WHERE AccountId IN :newAccountIdAccountMobileDealerIdMap.keySet()
                    AND Owner.UserRoleId NOT IN :roleExclusionMap.keySet()
                ];
                for (Opportunity opp : oppList) {
                    oppOwnerMap.put(opp.Id, opp.OwnerId);
                    oppIdSet.add(opp.Id);
                }
                List<VF_Contract__c> contractList = [
                    SELECT Id, Opportunity__r.AccountId, Opportunity__r.Owner.Partner_User__c, Opportunity__c
                    FROM VF_Contract__c
                    WHERE Opportunity__c IN :oppIdSet
                    AND Opportunity__r.Owner.UserRoleId NOT IN :roleExclusionMap.keySet()];
                for (VF_Contract__c opp : contractList) {
                    contractIdSet.add(opp.Id);
                }

                List<OpportunityShare> currentOppShareList = [SELECT Id, OpportunityId, UserOrGroupId, OpportunityAccessLevel FROM OpportunityShare WHERE OpportunityId IN :oppIdSet AND RowCause = 'Manual'];
                List<VF_Contract__Share> currentContractShareList = [SELECT Id, ParentId, UserOrGroupId, AccessLevel FROM VF_Contract__Share WHERE ParentID IN :contractIdSet AND RowCause = 'Manual'];

                Map<Id, List<OpportunityShare>> oppIdToOppShareMap = new Map<Id, List<OpportunityShare>>();
                Map<Id, List<VF_Contract__Share>> contractIdToContractShareMap = new Map<Id, List<VF_Contract__Share>>();

                for (OpportunityShare oppShare : currentOppShareList) {
                    if (oppIdToOppShareMap.containsKey(oppShare.OpportunityId)) {
                        List<OpportunityShare> oppShareList = oppIdToOppShareMap.get(oppShare.OpportunityId);
                        oppShareList.add(oppShare);
                        oppIdToOppShareMap.put(oppShare.OpportunityId, oppShareList);
                    } else {
                        List<OpportunityShare> oppShareList = new List<OpportunityShare>();
                        oppShareList.add(oppShare);
                        oppIdToOppShareMap.put(oppShare.OpportunityId, oppShareList);
                    }
                }
                for (VF_Contract__Share contractShare : currentContractShareList) {
                    if (contractIdToContractShareMap.containsKey(contractShare.ParentId)) {
                        List<VF_Contract__Share> contractShareList = contractIdToContractShareMap.get(contractShare.ParentId);
                        contractShareList.add(contractShare);
                        contractIdToContractShareMap.put(contractShare.ParentId, contractShareList);
                    } else {
                        List<VF_Contract__Share> contractShareList = new List<VF_Contract__Share>();
                        contractShareList.add(contractShare);
                        contractIdToContractShareMap.put(contractShare.ParentId, contractShareList);
                    }
                }

                for (Opportunity opp : oppList) {
                    Id accOwnerId = newAccountIdAccountMobileDealerIdMap.get(opp.AccountId);
                    if (oldAccountIdAccountMobileDealerIdMap != null) {
                        if (oldAccountIdAccountMobileDealerIdMap.containsKey(opp.AccountId)) {
                            if (oppIdToOppShareMap.containsKey(opp.Id)) {
                                List<OpportunityShare> oppShareList = oppIdToOppShareMap.get(opp.Id);
                                Id oldAccOwnerId = oldAccountIdAccountMobileDealerIdMap.get(opp.AccountId);
                                if (oppShareList != null) {
                                    for (OpportunityShare oppShare : oppShareList) {
                                        if (oppShare.UserOrGroupId == oldAccOwnerId) {
                                            deleteOppShareList.add(oppShare);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (oppIdToOppShareMap.containsKey(opp.Id)) {
                        List<OpportunityShare> oppShareList = oppIdToOppShareMap.get(opp.Id);
                        for (OpportunityShare oppShare : oppShareList) {
                            if (userPartnerMap.containsKey(oppShare.UserOrGroupId)) {
                                if (userPartnerMap.get(oppShare.UserOrGroupId)) {
                                    deleteOppShareList.add(oppShare);
                                }
                            }
                        }
                    }

                    if (!opp.Owner.Partner_User__c) { /** Only Direct Opportunities are shared with the Account Owner */
                        Boolean sharingExists = false;
                        if (oppIdToOppShareMap.containsKey(opp.Id)) {
                            List<OpportunityShare> oppShareList = oppIdToOppShareMap.get(opp.Id);
                            for (OpportunityShare oppShare : oppShareList) {
                                if (oppShare.UserOrGroupId == accOwnerId) {
                                    sharingExists = true;
                                    if (oppShare.OpportunityAccessLevel != 'Read') {
                                        oppShare.OpportunityAccessLevel = 'Read';
                                        updateOppShareList.add(oppShare);
                                    }
                                }
                            }
                        }

                        if (!sharingExists) {
                            Boolean shareRecord = true;
                            if (oppOwnerMap.containsKey(opp.Id)) {
                                if (oppOwnerMap.get(opp.Id) == accOwnerId) {
                                    shareRecord = false;
                                }
                            }
                            if (shareRecord) {
                                OpportunityShare oppShare = new OpportunityShare();
                                oppShare.OpportunityId = opp.Id;
                                oppShare.UserOrGroupId = accOwnerId;
                                oppShare.OpportunityAccessLevel = 'Read';
                                insertOppShareList.add(oppShare);
                            }
                        }
                    } else {
                        if (oppIdToOppShareMap.containsKey(opp.Id)) {
                            List<OpportunityShare> oppShareList = oppIdToOppShareMap.get(opp.Id);
                            for (OpportunityShare oppShare : oppShareList) {
                                if (oppShare.UserOrGroupId == accOwnerId) {
                                    deleteOppShareList.add(oppShare);
                                }
                            }
                        }
                    }
                }

                for (VF_Contract__c contract : contractList) {
                    Id accOwnerId = newAccountIdAccountMobileDealerIdMap.get(contract.Opportunity__r.AccountId);
                    if (oldAccountIdAccountMobileDealerIdMap != null) {
                        if (oldAccountIdAccountMobileDealerIdMap.containsKey(contract.Opportunity__r.AccountId)) {
                            if (contractIdToContractShareMap.containsKey(contract.Id)) {
                                List<VF_Contract__Share> contractShareList = contractIdToContractShareMap.get(contract.Id);
                                Id oldAccOwnerId = oldAccountIdAccountMobileDealerIdMap.get(contract.Opportunity__r.AccountId);
                                if (contractShareList != null) {
                                    for (VF_Contract__Share contractShare : contractShareList) {
                                        if (contractShare.UserOrGroupId == oldAccOwnerId) {
                                            deleteContractShareList.add(contractShare);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (contractIdToContractShareMap.containsKey(contract.Id)) {
                        List<VF_Contract__Share> contractShareList = contractIdToContractShareMap.get(contract.Id);
                        for (VF_Contract__Share contractShare : contractShareList) {
                            if (userPartnerMap.containsKey(contractShare.UserOrGroupId)) {
                                if (userPartnerMap.get(contractShare.UserOrGroupId)) {
                                    deleteContractShareList.add(contractShare);
                                }
                            }
                        }
                    }

                    if (!contract.Opportunity__r.Owner.Partner_User__c) { /** Only Direct Opportunities/Contracts are shared with the Account Owner */
                        Boolean sharingExists = false;
                        if (contractIdToContractShareMap.containsKey(contract.Id)) {
                            List<VF_Contract__Share> contractShareList = contractIdToContractShareMap.get(contract.Id);
                            for (VF_Contract__Share contractShare : contractShareList) {
                                if (contractShare.UserOrGroupId == accOwnerId) {
                                    sharingExists = true;
                                    if (contractShare.AccessLevel != 'Read') {
                                        contractShare.AccessLevel = 'Read';
                                        updateContractShareList.add(contractShare);
                                    }
                                }
                            }
                        }

                        if (!sharingExists) {
                            Boolean shareRecord = true;
                            if (oppOwnerMap.containsKey(contract.Opportunity__c)) {
                                if (oppOwnerMap.get(contract.Opportunity__c) == accOwnerId) {
                                    shareRecord = false;
                                }
                            }
                            if (shareRecord) {
                                VF_Contract__Share contractShare = new VF_Contract__Share();
                                contractShare.ParentId = contract.Id;
                                contractShare.UserOrGroupId = accOwnerId;
                                contractShare.AccessLevel = 'Read';
                                insertContractShareList.add(contractShare);
                            }
                        }
                    } else {
                        if (contractIdToContractShareMap.containsKey(contract.Id)) {
                            List<VF_Contract__Share> contractShareList = contractIdToContractShareMap.get(contract.Id);
                            for (VF_Contract__Share contractShare : contractShareList) {
                                if (contractShare.UserOrGroupId == accOwnerId) {
                                    deleteContractShareList.add(contractShare);
                                }
                            }
                        }
                    }
                }

                try {
                    if (insertOppShareList.size() > 0 || deleteOppShareList.size() > 0 || updateOppShareList.size() > 0) {
                        WithoutSharingMethods wsm = new WithoutSharingMethods();
                        if (insertOppShareList.size() > 0) {
                            wsm.insertSharingRules(insertOppShareList);
                        }
                        if (deleteOppShareList.size() > 0) {
                            wsm.deleteObjects(deleteOppShareList);
                        }
                        if (updateOppShareList.size() > 0) {
                            wsm.updateObjects(updateOppShareList);
                        }
                    }
                    if (insertContractShareList.size() > 0 || deleteContractShareList.size() > 0 || updateContractShareList.size() > 0) {
                        WithoutSharingMethods wsm = new WithoutSharingMethods();
                        if (insertContractShareList.size() > 0) {
                            wsm.insertSharingRules(insertContractShareList);
                        }
                        if (deleteContractShareList.size() > 0) {
                            wsm.deleteObjects(deleteContractShareList);
                        }
                        if (updateContractShareList.size() > 0) {
                            wsm.updateObjects(updateContractShareList);
                        }
                    }
                } catch (Exception e) {}
            }
        }
    }

    @future
    public static void createSharingForMobileDealer(Set<Id> accountsIdSet, Map<Id, Id> mobileDealerIdMap, Map<Id, Id> oldAccIdToMobileDealerId, Boolean fromOpportunityChange) {
        SharingUtils.createSharingForMobileDealerNonFuture(accountsIdSet, mobileDealerIdMap, oldAccIdToMobileDealerId, fromOpportunityChange);
    }

    public static void createAccountSharing(Map<Id,Id> accountIdToOwnerId, Set<Id> partnerOwnerIds, Map<Id,Id> parentPartnerUserIds){
        List<AccountShare> accountSharingRulesToInsert = new List<AccountShare>();

        // if parentpartneruserids are not provided, load it here
        if(parentPartnerUserIds == null)
            parentPartnerUserIds = GeneralUtils.getParentPartnerUserIdsFromUserIds(partnerOwnerIds);

        // add account sharing rule if the Ban owner cannot view the account yet
        Map<Id,Id> ownerToGroupId = GeneralUtils.getPartnerUserRoleGroupIdFromUserId(partnerOwnerIds);
        Map<Id,Id> accountToGroupId = new Map<Id,Id>();
        for(Id acctId : accountIdToOwnerId.KeySet()){
            Id ownerId = accountIdToOwnerId.get(acctId);
            if(ownerToGroupId.containsKey(ownerId)){
                accountToGroupId.put(acctId,ownerToGroupId.get(ownerId));
            }
        }
        if(!accountToGroupId.isEmpty()){
            for(Id accountId : accountToGroupId.keySet()){
                AccountShare accShare = new AccountShare();
                accShare.AccountId = accountId;
                accShare.UserOrGroupId = accountToGroupId.get(accountId);
                accShare.CaseAccessLevel = 'None';
                accShare.OpportunityAccessLevel = 'None';
                accShare.AccountAccessLevel = 'Edit';
                accountSharingRulestoInsert.add(accShare);

                if(parentPartnerUserIds.containsKey(accountIdToOwnerId.get(accountId))){
                    AccountShare parentAccShare = new AccountShare();
                    parentAccShare.AccountId = accountId;
                    parentAccShare.UserOrGroupId = ownerToGroupId.get(parentPartnerUserIds.get(accountIdToOwnerId.get(accountId)));
                    parentAccShare.CaseAccessLevel = 'None';
                    parentAccShare.OpportunityAccessLevel = 'None';
                    parentAccShare.AccountAccessLevel = 'Edit';
                    accountSharingRulestoInsert.add(parentAccShare);
                }
            }
            WithoutSharingMethods wsm = new WithoutSharingMethods();
            wsm.insertSharingRules(accountSharingRulesToInsert);
        }
    }

    public without sharing class WithoutSharingMethods{
        public void insertSharingRules(List<sObject> sharingRulestoInsert){
            Database.insert(sharingRulestoInsert,false);
        }

        public void insertObjects(List<sObject> objectsTooInsert){
            insert objectsTooInsert;
        }
        public void updateObjects(List<sObject> objectsToUpdate){
            update objectstoUpdate;
        }
        public void upsertObjects(List<sObject> objectsToUpsert){
            upsert objectstoUpsert;
        }
        public void deleteObjects(List<sObject> objectsToDelete){
            delete objectsToDelete;
        }
        public List<sObject> queryObjects(String queryString){
            return Database.query(queryString);
        }
        public List<Database.saveResult> databaseUpdateObjects(List<sObject> objectsToUpdate) {
            return Database.update(objectsToUpdate,false);
        }
    }

    public static void insertSharingRulesWithoutSharingStatic(List<sObject> sharingRulestoInsert){
        Database.insert(sharingRulestoInsert,false);
    }

    public static void insertObjectsWithoutSharingStatic(List<sObject> objectsTooInsert){
        insert objectsTooInsert;
    }
    public static void updateObjectsWithoutSharingStatic(List<sObject> objectsToUpdate){
        update objectstoUpdate;
    }

    public static void insertRecordsWithoutSharing(sObject objecttoInsert){
        insertRecordsWithoutSharing(new List<sObject> {objecttoInsert});
    }
    public static void insertRecordsWithoutSharing(List<sObject> objectstoInsert){
        SharingUtils.WithoutSharingMethods su = new SharingUtils.WithoutSharingMethods();
        su.insertObjects(objectstoInsert);
    }

    public static void updateRecordsWithoutSharing(sObject objecttoUpdate){
        updateRecordsWithoutSharing(new List<sObject> {objecttoUpdate});
    }
    public static void updateRecordsWithoutSharing(List<sObject> objectsToUpdate){
        SharingUtils.WithoutSharingMethods su = new SharingUtils.WithoutSharingMethods();
        su.updateObjects(objectstoUpdate);
    }

    public static List<Database.saveResult> databaseUpdateRecordsWithoutSharing(List<sObject> objectsToUpdate) {
        SharingUtils.WithoutSharingMethods su = new SharingUtils.WithoutSharingMethods();
        return su.databaseUpdateObjects(objectstoUpdate);
    }

    public static void upsertRecordsWithoutSharing(sObject objecttoUpsert){
        upsertRecordsWithoutSharing(new List<sObject> {objecttoUpsert});
    }
    public static void upsertRecordsWithoutSharing(List<sObject> objectsToUpsert){
        SharingUtils.WithoutSharingMethods su = new SharingUtils.WithoutSharingMethods();
        su.upsertObjects(objectstoUpsert);
    }

    public static void deleteRecordsWithoutSharing(sObject objecttoDelete){
        deleteRecordsWithoutSharing(new List<sObject> {objecttoDelete});
    }

    public static void deleteRecordsWithoutSharing(List<sObject> objecttoDelete){
        SharingUtils.WithoutSharingMethods su = new SharingUtils.WithoutSharingMethods();
        su.deleteObjects(objecttoDelete);
    }

    public static List<sObject> queryRecordsWithoutSharing(String queryString){
        SharingUtils.WithoutSharingMethods su = new SharingUtils.WithoutSharingMethods();
        return su.queryObjects(queryString);
    }
}