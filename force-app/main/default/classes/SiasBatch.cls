/**
 * @description         This batch class is responsible for sending the order to SIAS.
 * @author              Rahul Sharma
 */
@SuppressWarnings('PMD')
public class SiasBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
	// properties
	public Set<Id> orderIds = new Set<Id>();

	// constructor accepts Set of order ids for processing
	public SiasBatch(Set<Id> orderIds) {
		this.orderIds = orderIds;
	}

	// start method
	public Database.QueryLocator start(Database.BatchableContext bc) {
		System.debug(LoggingLevel.INFO, '@@@@orderIds: ' + orderIds);
		return Database.getQueryLocator([SELECT Id, Name FROM Order__c WHERE Id IN :this.orderIds]);
	}

	// execute method
	public void execute(Database.BatchableContext bc, List<Order__c> orders) {
		System.debug(LoggingLevel.INFO, '@@@@orders: ' + orders);
		SendOrdersToSias(orders);
	}

	// generates the billing transaction and sends them to SIAS for processing
	@TestVisible
	private static void sendOrdersToSias(List<Order__c> orders) {
		List<Order_Billing_Transaction__c> billingTransactionsToInsert = new List<Order_Billing_Transaction__c>();
		List<Contracted_Products__c> contractedProductsToUpdate = new List<Contracted_Products__c>();

		Map<Id, List<Contracted_Products__c>> contractedProductsByOrderMap = getContractedProductsByOrderMap(
			orders
		);

		Map<Id, Contracted_Products__c> nonBillableCPsMap = new Map<Id, Contracted_Products__c>();
		Map<Id, Contracted_Products__c> specialBillingCpsMap = new Map<Id, Contracted_Products__c>();

		Set<Id> orderSet = new Set<Id>();

		// iterate over orders
		for (Order__c order : orders) {
			orderSet.add(order.Id);
			List<Id> contractedProductIds = new List<Id>();
			Boolean orderHasProducts =
				contractedProductsByOrderMap.containsKey(order.Id) &&
				!contractedProductsByOrderMap.get(order.Id).isEmpty();

			if (orderHasProducts) {
				List<Contracted_Products__c> relatedContractedProducts = contractedProductsByOrderMap.get(
					order.Id
				);

				System.debug(
					LoggingLevel.INFO,
					'@@@@relatedContractedProducts: ' + JSON.serialize(relatedContractedProducts)
				);

				// iterate over approved contracted products to collect the amount, CP and CA id's
				for (Contracted_Products__c contractedProduct : relatedContractedProducts) {
					if (
						(contractedProduct.Product__r?.Billing_Type__c ==
						Constants.PRODUCT2_BILLINGTYPE_SPECIAL) &&
						(contractedProduct.Order__r.Account__c != null &&
						contractedProduct.Order__r.Account__r.GT_Fixed__c)
					) {
						specialBillingCpsMap.put(contractedProduct.Id, contractedProduct);
					} else if (
						contractedProduct.Gross_List_Price__c > 0 && contractedProduct.Billing__c
					) {
						// collect contracted products to send to SIAS for billing
						contractedProductIds.add(contractedProduct.Id);
					} else {
						nonBillableCPsMap.put(contractedProduct.Id, contractedProduct);
					}
				}

				// send the orders to SIAS to notify about the Billing
				// done in loop as SIAS accepts this per order
				if (!contractedProductIds.isEmpty()) {
					String transactionId = 'TR-' + Datetime.now().getTime();

					// make callout to interface
					CreateBSSOrderRest.CollectionWrapper collectionWrapper = CreateBSSOrderRest.createBSSOrder(
						transactionId,
						contractedProductIds
					);
					// collect the records for processing
					billingTransactionsToInsert.add(collectionWrapper.billingTransactionToInsert);
					contractedProductsToUpdate.addAll(collectionWrapper.contractedProductsToUpdate);
				}

				System.debug(
					LoggingLevel.INFO,
					order.Id +
					'@@@@contractedProductIds: ' +
					JSON.serialize(contractedProductIds)
				);
			}
		}

		// notify BOP for the records having no billing
		sendNoBillingStatusToBop(orderSet, nonBillableCPsMap, specialBillingCpsMap);

		System.debug(
			LoggingLevel.INFO,
			'@@@@contractedProductsToUpdate: ' + JSON.serialize(contractedProductsToUpdate)
		);
		System.debug(
			LoggingLevel.INFO,
			'@@@@billingTransactionsToInsert: ' + JSON.serialize(billingTransactionsToInsert)
		);

		// update billing status
		updateBillingStatusOnCp(contractedProductsToUpdate);

		// create billing transaction for orders send to SIAS
		if (!billingTransactionsToInsert.isEmpty()) {
			insert billingTransactionsToInsert;
		}
	}

	private static void sendNoBillingStatusToBop(
		Set<Id> orderSet,
		Map<Id, Contracted_Products__c> nonBillableCPsMap,
		Map<Id, Contracted_Products__c> specialBillingCpsMap
	) {
		List<Contracted_Products__c> contractedProductsToUpdate = new List<Contracted_Products__c>();
		List<Customer_Asset__c> customerAssetsToUpdate = new List<Customer_Asset__c>();

		Map<Id, Contracted_Products__c> allNoBillingCps = new Map<Id, Contracted_Products__c>();
		allNoBillingCps.putAll(nonBillableCPsMap);
		allNoBillingCps.putAll(specialBillingCpsMap);

		// send no billing states to BOP delivery export service
		if (!orderSet.isEmpty() && !allNoBillingCps.isEmpty()) {
			DeliveryExport deliveryExportObject = new DeliveryExport(
				orderSet,
				allNoBillingCps.keySet(),
				'update',
				Constants.CONTRACTED_PRODUCT_BOP_STATUS_CLOSED
			);
			deliveryExportObject.exportDeliveries();
			// Id jobId = System.enqueueJob(deliveryExportObject);
		}

		// collect the records to update
		for (Id cpId : allNoBillingCps.keySet()) {
			Contracted_Products__c contractedProduct = allNoBillingCps.get(cpId);
			String cpBillingStatus = nonBillableCPsMap.containsKey(cpId)
				? Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NO_BILLING
				: Constants.CONTRACTED_PRODUCT_BILLING_STATUS_SPECIAL_BILLING;
			String caBillingStatus = nonBillableCPsMap.containsKey(cpId)
				? Constants.CUSTOMER_ASSET_BILLING_STATUS_NO_BILLING
				: Constants.CUSTOMER_ASSET_BILLING_STATUS_SPECIAL_BILLING;
			contractedProductsToUpdate.add(
				new Contracted_Products__c(Id = cpId, Billing_Status__c = cpBillingStatus)
			);

			if (String.isNotEmpty(contractedProduct.Customer_Asset__c)) {
				customerAssetsToUpdate.add(
					new Customer_Asset__c(
						Id = contractedProduct.Customer_Asset__c,
						Billing_Status__c = caBillingStatus
					)
				);
			}
		}

		System.debug(
			LoggingLevel.INFO,
			'@@@@contractedProductsToUpdate: ' + JSON.serialize(contractedProductsToUpdate)
		);
		System.debug(
			LoggingLevel.INFO,
			'@@@@customerAssetsToUpdate: ' + JSON.serialize(customerAssetsToUpdate)
		);

		if (!contractedProductsToUpdate.isEmpty()) {
			update contractedProductsToUpdate;
		}

		if (!customerAssetsToUpdate.isEmpty()) {
			update customerAssetsToUpdate;
		}
	}

	// prepares and returns map of order id with related contracted products
	private static Map<Id, List<Contracted_Products__c>> getContractedProductsByOrderMap(
		List<Order__c> orders
	) {
		Map<Id, List<Contracted_Products__c>> contractedProductsByOrderMap = new Map<Id, List<Contracted_Products__c>>();
		for (
			Contracted_Products__c cp : [
				SELECT
					Id,
					Customer_Asset__c,
					Gross_List_Price__c,
					Order__c,
					Is_Recurring__c,
					Product__c,
					Product__r.Billing_Type__c,
					Billing__c,
					Order__r.Account__c,
					Order__r.Account__r.GT_Fixed__c
				FROM Contracted_Products__c
				WHERE
					Order__c IN :orders
					AND Delivery_Status__c = :Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED
					AND Billing_Status__c = :Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW
					// CLC__c = :Constants.CONTRACTED_PRODUCT_CLC_ACQ AND - JvW 15-01-2020: Removed for W-002334
					AND Customer_Asset__c != NULL
					// Product__r.Billing_Type__c != :Constants.PRODUCT2_BILLINGTYPE_SPECIAL AND
					AND Order__r.O2C_Order__c = TRUE
					AND Do_not_export__c = FALSE
			]
		) {
			if (!contractedProductsByOrderMap.containsKey(cp.Order__c)) {
				contractedProductsByOrderMap.put(
					cp.Order__c,
					new List<Contracted_Products__c>{ cp }
				);
			} else {
				contractedProductsByOrderMap.get(cp.Order__c).add(cp);
			}
		}

		return contractedProductsByOrderMap;
	}

	private static void updateBillingStatusOnCp(List<Contracted_Products__c> contractedProducts) {
		// update the billing status for record sends to SIAS
		if (!contractedProducts.isEmpty()) {
			// requery the CPs to see if the billing status was not already updated by SIAS
			Map<Id, Contracted_Products__c> mapUpdatedProducts = new Map<Id, Contracted_Products__c>(
				[
					SELECT Id
					FROM Contracted_Products__c
					WHERE
						Id IN :contractedProducts
						AND Billing_Status__c IN (
							:Constants.CONTRACTED_PRODUCT_BILLING_STATUS_PROCESSED,
							:Constants.CONTRACTED_PRODUCT_BILLING_STATUS_FAILED
						)
				]
			);

			List<Contracted_Products__c> contractedProductsToUpdate = new List<Contracted_Products__c>();

			for (Contracted_Products__c cp : contractedProducts) {
				// collect the records which are not processed already by provide invoicing info
				// this is added as a fix because while salesforce make a callout in for loop for create BSS service
				//      check here prevents updating the records already updated by the callback from SIAS
				if (!mapUpdatedProducts.containsKey(cp.Id)) {
					contractedProductsToUpdate.add(cp);
				}
			}

			update contractedProductsToUpdate;
		}
	}

	// finish method
	public void finish(Database.BatchableContext bc) {
		// abort and schedule batch after 5 minutes
		ScheduleSiasBatch.abort();
		ScheduleSiasBatch.scheduleInFiveMinutes();
	}

	// calls the execute method in context of future
	// this method is intended to be called in context of test method
	@future(callout=true)
	public static void executeCalloutAsFuture(Set<Id> orderIds) {
		SiasBatch batchInstance = new SiasBatch(orderIds);
		// Get an iterator
		Database.QueryLocatorIterator iterator = batchInstance.start(null).iterator();

		List<Order__c> orders = new List<Order__c>();

		// Collect the first record
		if (iterator.hasNext()) {
			orders.add((Order__c) iterator.next());
		}

		// call execute method to make callout and process the record
		if (!orders.isEmpty()) {
			batchInstance.execute(null, orders);
		}
	}
}