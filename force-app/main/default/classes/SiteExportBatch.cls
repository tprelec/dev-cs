/**
 * @description         This is the class that contains logic for running the Site export in batches
 * @author              Guy Clairbois
 */
global class SiteExportBatch implements Database.Batchable <sObject>, Database.AllowsCallouts{
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,BOP_export_datetime__c,BOP_Export_Errormessage__c FROM Site__c Where BOP_Export_ErrorMessage__c != null');
    } 
 
    global void execute(Database.BatchableContext BC, List<Site__c> scope){
        // do Contact exporting here
        set<Id> createSiteIds = new Set<Id>();
        set<Id> updateSiteIds = new Set<Id>();
        // only process the first 100 records because any more will cause issues (10 accounts per call, 10 calls per transaction)
        Integer size = math.min(scope.size(),10); 

        for(Integer i=0;i<size;i++){
            // only process if 2 first characters are non-numeric (so not coming from BOP)
            if(!scope[i].BOP_Export_Errormessage__c.left(1).isNumeric()) {            
                if(scope[i].BOP_export_datetime__c == null)
                    createSiteIds.add(scope[i].Id);
                else
                    updateSiteIds.add(scope[i].Id);
            }
        }

        List<Site__c> siteExportResults = SiteExport.exportSites(createSiteIds,'create');
        siteExportResults.addAll(SiteExport.exportSites(updateSiteIds,'update'));
        Database.update(siteExportResults,false);       
        
        
    }
 
    global void finish(Database.BatchableContext BC){
        ExportUtils.startNextJob('Accounts');
 
    }
 
}