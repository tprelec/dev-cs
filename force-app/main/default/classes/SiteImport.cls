/**
 * @description			This is the class that contains logic for uploading Sites via a csv file
 * @author				Guy Clairbois
 */
public with sharing class SiteImport {
	//Site Name,Phone,Street,HouseNr,HouseNrSuffic,Postalcode,City.

    public string csvName {get;set;}  
    public Blob csvBody {get;set;}
	public Id accountId {get;set;}

	public SiteImport(){
		if (ApexPages.currentPage().getParameters().containsKey('AccountId')) {
			accountId = ApexPages.currentPage().getParameters().get('AccountId');
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'No account found.'));
		}
	}

	public pageReference cancel(){
		PageReference p = new PageReference('/'+accountId);
		p.setRedirect(false);
		return p;
	}

	public String separator {
		get{
			if(separator == null) separator = ',';
			return separator;
		}
		set;
	}
	public List<SelectOption> getSeparators() {

		List<SelectOption> separators = new List<SelectOption>();
		separators.add(new SelectOption(',',','));
		separators.add(new SelectOption(';',';'));
		return separators;
	}

	public void handleCSV(){

		List<Site__c> sitesToInsert = new List<Site__c>();
		List<List<String>> parsedCSV = new List<List<String>>();

		try {
			parsedCSV = StringUtils.parseCSV(csvBody.toString(),true,separator);
			for(list<string> line:parsedCSV){

				Site__c s = new Site__c();
				s.Site_Account__c = accountId;
				s.Name = line[0];
				s.Site_Phone__c = line[1];
				s.Site_Street__c = line[2];
				s.Site_House_Number__c = Integer.valueOf(line[3]);
				s.Site_House_Number_Suffix__c = line[4];
				s.Site_Postal_Code__c = line[5];
				s.Site_City__c = line[6];
				//s.Site_Country__c = siteRow.country;
				sitesToInsert.add(s);				    
			}
			//insert sitesToInsert;		
					

		} catch(Exception ex){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'CSV parsing error: '+ex));				
			//return null;
		}

    	try{
    		if(!sitesToInsert.isEmpty()){
    			Database.SaveResult[] insertList = Database.insert(sitesToInsert, false);
    			//Database.insert sitesToInsert;

    			Integer successcounter = 0;
    			Integer rowCounter = 0;
    			failedRows = new List<String>();

    			for (Database.SaveResult sr : insertList) {
				    if (sr.isSuccess()) {
				        // Operation was successful, so get the ID of the record that was processed
				        successcounter++;
				    }
				    else {
				    	String error = '';
				    	for(String field : parsedCSV[rowCounter]){
				    		error += field + separator;
				    	}
				        // Operation failed, so get all errors                
				        for(Database.Error err : sr.getErrors()) {
				        	error += err.getMessage();
				            //failedRows.add( + err.getStatusCode() + ': ' + err.getMessage());
				            System.debug(err.getStatusCode() + ': ' + err.getMessage());
				            
				        }
				        failedRows.add(error);
				    }
				    rowCounter++;
				}
				if(successcounter > 0){
    				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, successcounter+' sites were uploaded successfully!'));
    			}
    			if(rowCounter != successcounter){
    				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, rowCounter-successcounter+' sites failed! (see below)'));
    			}

    		} else {
    			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'No sites found in csv file!'));
    		}
    	}catch (DMLException ex){
    		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'There was an error when creating the sites: ' + ex));
    	}
            			
		//return null;	
	}

	public pageReference downloadExample(){
		PageReference pr = Page.SiteImportExample;
		return pr;

	}

	public List<String> failedRows {
		get;
		set;
	}
}