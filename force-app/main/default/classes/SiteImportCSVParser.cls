/**
 * @description			This class is responsible for parsing a Site Import CSV.
 * @author				Guy Clairbois
 */
public class SiteImportCSVParser extends CSVParser {
	
	private Integer maxNumRowsToProcess = 1000;
	
	
	/**
	 * @description			This class represents a single row in the CSV. Not all of the values
	 * 						will be populated depending on the CSV being processed.
	 */
	public class CSVRow extends CSVParser.CSVRow {
			//Site Name,Phone,Street,HouseNr,HouseNrSuffic,Postalcode,City,Country
		
		public String siteName { get; set; }
		public String phone { get; set; }
		public String street { get; set; }
		public Integer houseNr { get; set; }
		public String houseNrSuffix { get; set; }
		public String postalCode { get; set; }
		public String city { get; set; }
		public String country { get; set; }
		public String errors { get; set; }
		
		/**
		 * @description			This will setup the mappings between the properties in the child CSV row class
		 *						and the column names in the CSV. This will allow us to reverse the mapping between
		 *						them, so that we can eventually generate a CSV structure.
		 */
		public override void setupColumnPropertyMappings(){
			
			mapColumnToProperty('SITE NAME', siteName);
			mapColumnToProperty('PHONE', phone);
			mapColumnToProperty('STREET', street);
			mapColumnToProperty('HOUSENUMBER', houseNr);
			mapColumnToProperty('HOUSENUMBERSUFFIX', houseNrSuffix);
			mapColumnToProperty('POSTALCODE', postalCode);
			mapColumnToProperty('CITY', city);
			mapColumnToProperty('COUNTRY', city);
			
			// This is a new column in the CSV which was not prevent when reading
			mapColumnToProperty('ERRORS', errors);
		}
	}

	
	/**
	 * @description			The constructor is responsible for setting up the explicit column heading
	 *						groups acceptable to be processed.
	 */
	public SiteImportCSVParser(){
		// Define the explicit column header groups acceptable
		addExplicitColumnHeaderGroupWithName(
			new Set<String>{
				'SITE NAME',
				'PHONE',
				'STREET',
				'HOUSENUMBER',
				'HOUSENUMBERSUFFIX',
				'POSTALCODE',
				'CITY'
				//'COUNTRY'
			}, 
		'SITE_CSV');
	}
	
	
	/**
	 * @description			This will set the maximum number of rows that can be processed in the CSV.
	 *						If the CSV when read contains more than this amount of rows then a ParserException
	 *						will be thrown.
	 */
	public void setMaxNumRowsToProcess(Integer maxNumRowsToProcess){
		this.maxNumRowsToProcess = maxNumRowsToProcess;
	}
	
	
	/**
	 * @description			This method overrides the readFile method in the super class so
	 *						we can check to see if there is less than 200 rows in the CSV.
	 */
	public override void readFile(Blob file){
		setRemoveBlankRows(false);
		setStoreOriginalRawInput(true);

		// Request the file to be processed
		super.readFile(file);
		
		// Check to see if the correct amount of rows were loaded
			if(getNumberOfRows() > maxNumRowsToProcess){
				throw new ParseException('The CSV can only have up to ' + maxNumRowsToProcess + ' rows');
			}
	}
	
	
	/**
	 * @description			The super class is not aware of which class is responsible for containing the
	 *						data held on a CSV row, this method provides a way for the correct class to be 
	 *						instantiated.
	 * @return				A new instance of the XYZCSVRow class.
	 */
	protected override CSVParser.CSVRow createCSVRow(){
		return new CSVRow();
	}
	
	
	/**
	 * @description			This is called by the super class to set the value held in a row in the CSV
	 *						to a property held in the CSVRow class in an instance specific to a row.
	 * @param	csvRow		Instance of CSVRow specific to the row currently being processed.
	 * @param	columnName	The name of the column the value belongs to.
	 * @param	cellValue	The value which is currently being processed in the CSV.
	 */
	protected override void setRowCellValue(CSVParser.CSVRow csvRow, String columnName, String cellValue){
		CSVRow r = (CSVRow) csvRow;
			
			if(columnName == 'SITE NAME'){
				r.siteName = cellValue;
				
			} else if(columnName == 'PHONE'){
				r.phone = cellValue;
				
			}  else if(columnName == 'STREET'){
				r.street = cellValue;
				
			} else if(columnName == 'HOUSENUMBER'){
				r.houseNr = convertToInteger(cellValue);
				
			} else if(columnName == 'HOUSENUMBERSUFFIX'){
				r.houseNrSuffix = cellValue;
				
			} else if(columnName == 'POSTALCODE'){
				r.postalCode = cellValue;
				
			} else if(columnName == 'CITY'){
				r.city = cellValue;
				
			} else if(columnName == 'COUNTRY'){
				r.country = cellValue;
			}

	}
	
	
	/**
	 * @description			This overrides the generateCSV method found in the super class so that we can add
	 *						additional columns to the output of the CSV.
	 */
	public override String generateCSV(){
		addColumn('ERRORS');
		return super.generateCSV();
	}
	
}