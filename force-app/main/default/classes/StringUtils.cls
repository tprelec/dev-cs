/**
 * @description				This class contains generally used string modifiers.
 * @author					Guy Clairbois
 */
public with sharing class StringUtils {

	/**
	 * 	@description		create a 'clean' 10-digit phone number out of an input string
	 * 						Marcel Vreuls: wordt alleen gebruikt voor naar de BOP.
	 *	@author				Guy Clairbois
	 */
	public static String cleanNLPhoneNumber(String inputString) {
		system.debug(inputString);
		if(inputString == null) return null;
		inputString = inputString.deleteWhitespace();
		inputString = inputString.remove('-');

		inputString = inputString.replace('+31(0)','0');
		inputString = inputString.replace('31(0)','0');
		inputString = inputString.replace('+31','0');
		inputString = inputString.replace('(31)','0');
		inputString = inputString.replace('0031','0');
		inputString = inputString.replace('(','');
		inputString = inputString.replace(')','');
		inputString = inputString.replace('+','00');
		
		system.debug(inputString);
		return inputString;
	}
	/**
	 * 	@description		create a 'clean' phonenumber for SIAS
	 *	@author				Marcel Vreuls
	 */
	public static String cleanNLPhoneNumberInternal(String inputString) {
		
		system.debug(inputString);
		if(inputString == null) return null;
		
		//doen we gewoon altijd standaard. de rest halen we uit de custom fields
		inputString = inputString.deleteWhitespace(); 

		//custom setting 	
		 for(PhoneInvalidChar__c ovf : PhoneInvalidChar__c.getAll().values()){		 	
            inputString = inputString.remove(ovf.Invalid_Character__c);                
            }        

		return inputString;
	}


	/**
	 * 	@description		create a 'clean' 10-digit phone number out of an input string
	 *	@author				Guy Clairbois
	 */
	public static List<String> cleanHouseNumberSuffix(String housenumberString) {
		String houseNumber = '';
		String suffix = '';
		
		if(housenumberString != null){
			for(Integer i=0;i<housenumberString.length();i++){
				if(housenumberString.substring(i,i+1).isNumeric()){
					houseNumber += housenumberString.substring(i,i+1);
				} else {
					suffix += housenumberString.subString(i,housenumberString.length());
					break;
				}
			}
		}
		return new List<String>{houseNumber,suffix};
	}
	
	/**
	 * 	@description		truncate a string to a certain number of characters // OBSOLETE, THERE IS NOW THE ABBREVIATE METHOD ON STRING
	 *	@author				Guy Clairbois
	 */
	public static String truncate(String inputString, Integer numberOfCharacters) {
		if(inputString.length() > numberOfCharacters){
			inputString = inputString.subString(0,numberOfCharacters);
		}
		return inputString;
	}	
	
	public DateTime formattedDateTime { get; set; } //property that reads the datetime value from the component attribute tag
	public String customFormat {get;set;} // optional custom format to override standard format
	
	//returns the properly formatted datetime value
	public String getTimeZoneValue() {
		String userLocale = UserInfo.getLocale(); //grab the locale of the user
		system.debug(userLocale);
		String datetimeFormat = 'M/d/yyyy h:mm a'; //variable for the datetime format defaulted to the US format
		if(customFormat == null){
			if (mappedValues.containsKey(userLocale)) { //if the map contains the correct datetime format
				datetimeFormat = mappedValues.get(userLocale); //grab the datetime format for the locale
			}
		} else {
			datetimeFormat = customFormat;
		}
		if(formattedDateTime != null){
			return formattedDateTime.format(datetimeFormat); //create a string with the proper format
		} else {
			return null;
		}
	}

	/**
	 * 	@description		check if a string is a valid ban (Gemini '3'+8numbers, Unify '5'+8numbers, BOP-Unify ('8'+8numbers))
	 *	@author				Guy Clairbois
	 */
	public static Boolean checkBan(String ban){
		if(ban != null && Pattern.matches('^[358][0-9]{8}', ban)){
			return true;
		} else {
			return false;
		}
		
	}
	
	//populate a map with locale values and corresponding datetime formats
	private static Map<String, String> mappedValues {
		get{
			if(mappedValues == null){
				mappedValues = new Map<String, String>(); //holds the locale to timedate formats
				mappedValues.put('ar', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('ar_AE', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('ar_BH', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('ar_JO', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('ar_KW', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('ar_LB', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('ar_SA', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('bg_BG', 'yyyy-M-d H:mm');
				mappedValues.put('ca', 'dd/MM/yyyy HH:mm');
				mappedValues.put('ca_ES', 'dd/MM/yyyy HH:mm');
				mappedValues.put('ca_ES_EURO', 'dd/MM/yyyy HH:mm');
				mappedValues.put('cs', 'd.M.yyyy H:mm');
				mappedValues.put('cs_CZ', 'd.M.yyyy H:mm');
				mappedValues.put('da', 'dd-MM-yyyy HH:mm');
				mappedValues.put('da_DK', 'dd-MM-yyyy HH:mm');
				mappedValues.put('de', 'dd.MM.yyyy HH:mm');
				mappedValues.put('de_AT', 'dd.MM.yyyy HH:mm');
				mappedValues.put('de_AT_EURO', 'dd.MM.yyyy HH:mm');
				mappedValues.put('de_CH', 'dd.MM.yyyy HH:mm');
				mappedValues.put('de_DE', 'dd.MM.yyyy HH:mm');
				mappedValues.put('de_DE_EURO', 'dd.MM.yyyy HH:mm');
				mappedValues.put('de_LU', 'dd.MM.yyyy HH:mm');
				mappedValues.put('de_LU_EURO', 'dd.MM.yyyy HH:mm');
				mappedValues.put('el_GR', 'd/M/yyyy h:mm a');
				mappedValues.put('en_AU', 'd/MM/yyyy HH:mm');
				mappedValues.put('en_B', 'M/d/yyyy h:mm a');
				mappedValues.put('en_BM', 'M/d/yyyy h:mm a');
				mappedValues.put('en_CA', 'dd/MM/yyyy h:mm a');
				mappedValues.put('en_GB', 'dd/MM/yyyy HH:mm');
				mappedValues.put('en_GH', 'M/d/yyyy h:mm a');
				mappedValues.put('en_ID', 'M/d/yyyy h:mm a');
				mappedValues.put('en_IE', 'dd/MM/yyyy HH:mm');
				mappedValues.put('en_IE_EURO', 'dd/MM/yyyy HH:mm');
				mappedValues.put('en_NZ', 'd/MM/yyyy HH:mm');
				mappedValues.put('en_SG', 'M/d/yyyy h:mm a');
				mappedValues.put('en_US', 'M/d/yyyy h:mm a');
				mappedValues.put('en_ZA', 'yyyy/MM/dd hh:mm a');
				mappedValues.put('es', 'd/MM/yyyy H:mm');
				mappedValues.put('es_AR', 'dd/MM/yyyy HH:mm');
				mappedValues.put('es_BO', 'dd-MM-yyyy hh:mm a');
				mappedValues.put('es_CL', 'dd-MM-yyyy hh:mm a');
				mappedValues.put('es_CO', 'd/MM/yyyy hh:mm a');
				mappedValues.put('es_CR', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('es_EC', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('es_ES', 'd/MM/yyyy H:mm');
				mappedValues.put('es_ES_EURO', 'd/MM/yyyy H:mm');
				mappedValues.put('es_GT', 'd/MM/yyyy hh:mm a');
				mappedValues.put('es_HN', 'MM-dd-yyyy hh:mm a');
				mappedValues.put('es_MX', 'd/MM/yyyy hh:mm a');
				mappedValues.put('es_PE', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('es_PR', 'MM-dd-yyyy hh:mm a');
				mappedValues.put('es_PY', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('es_SV', 'MM-dd-yyyy hh:mm a');
				mappedValues.put('es_UY', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('es_VE', 'dd/MM/yyyy hh:mm a');
				mappedValues.put('et_EE', 'd.MM.yyyy H:mm');
				mappedValues.put('fi', 'd.M.yyyy H:mm');
				mappedValues.put('fi_FI', 'd.M.yyyy H:mm');
				mappedValues.put('fi_FI_EURO', 'd.M.yyyy H:mm');
				mappedValues.put('fr', 'dd/MM/yyyy HH:mm');
				mappedValues.put('fr_BE', 'd/MM/yyyy H:mm');
				mappedValues.put('fr_CA', 'yyyy-MM-dd HH:mm');
				mappedValues.put('fr_CH', 'dd.MM.yyyy HH:mm');
				mappedValues.put('fr_FR', 'dd/MM/yyyy HH:mm');
				mappedValues.put('fr_FR_EURO', 'dd/MM/yyyy HH:mm');
				mappedValues.put('fr_LU', 'dd/MM/yyyy HH:mm');
				mappedValues.put('fr_MC', 'dd/MM/yyyy HH:mm');
				mappedValues.put('hr_HR', 'yyyy.MM.dd HH:mm');
				mappedValues.put('hu', 'yyyy.MM.dd. H:mm');
				mappedValues.put('hy_AM', 'M/d/yyyy h:mm a');
				mappedValues.put('is_IS', 'd.M.yyyy HH:mm');
				mappedValues.put('it', 'dd/MM/yyyy H.mm');
				mappedValues.put('it_CH', 'dd.MM.yyyy HH:mm');
				mappedValues.put('it_IT', 'dd/MM/yyyy H.mm');
				mappedValues.put('iw', 'HH:mm dd/MM/yyyy');
				mappedValues.put('iw_IL', 'HH:mm dd/MM/yyyy');
				mappedValues.put('ja', 'yyyy/MM/dd H:mm');
				mappedValues.put('ja_JP', 'yyyy/MM/dd H:mm');
				mappedValues.put('kk_KZ', 'M/d/yyyy h:mm a');
				mappedValues.put('km_KH', 'M/d/yyyy h:mm a');
				mappedValues.put('ko', 'yyyy. M. d a h:mm');
				mappedValues.put('ko_KR', 'yyyy. M. d a h:mm');
				mappedValues.put('lt_LT', 'yyyy.M.d HH.mm');
				mappedValues.put('lv_LV', 'yyyy.d.M HH:mm');
				mappedValues.put('ms_MY', 'dd/MM/yyyy h:mm a');
				mappedValues.put('nl', 'd-M-yyyy H:mm');
				mappedValues.put('nl_BE', 'd/MM/yyyy H:mm');
				mappedValues.put('nl_NL', 'd-M-yyyy H:mm');
				mappedValues.put('nl_SR', 'd-M-yyyy H:mm');
				mappedValues.put('no', 'dd.MM.yyyy HH:mm');
				mappedValues.put('no_NO', 'dd.MM.yyyy HH:mm');
				mappedValues.put('pl', 'yyyy-MM-dd HH:mm');
				mappedValues.put('pt', 'dd-MM-yyyy H:mm');
				mappedValues.put('pt_AO', 'dd-MM-yyyy H:mm');
				mappedValues.put('pt_BR', 'dd/MM/yyyy HH:mm');
				mappedValues.put('pt_PT', 'dd-MM-yyyy H:mm');
				mappedValues.put('ro_RO', 'dd.MM.yyyy HH:mm');
				mappedValues.put('ru', 'dd.MM.yyyy H:mm');
				mappedValues.put('sk_SK', 'd.M.yyyy H:mm');
				mappedValues.put('sl_SI', 'd.M.y H:mm');
				mappedValues.put('sv', 'yyyy-MM-dd HH:mm');
				mappedValues.put('sv_SE', 'yyyy-MM-dd HH:mm');
				mappedValues.put('th', 'M/d/yyyy h:mm a');
				mappedValues.put('th_TH', 'd/M/yyyy, H:mm ?.');
				mappedValues.put('tr', 'dd.MM.yyyy HH:mm');
				mappedValues.put('ur_PK', 'M/d/yyyy h:mm a');
				mappedValues.put('vi_VN', 'HH:mm dd/MM/yyyy');
				mappedValues.put('zh', 'yyyy-M-d ah:mm');
				mappedValues.put('zh_CN', 'yyyy-M-d ah:mm');
				mappedValues.put('zh_HK', 'yyyy-M-d ah:mm');
				mappedValues.put('zh_TW', 'yyyy/M/d a h:mm');
				system.debug(mappedValues);


			}
			system.debug(mappedValues);
			return mappedValues; //return the map
		}
		private set;
	}	

	public static list<list<String>> parseCSV(String contents,Boolean skipHeaders,String separator) {
	    list<list<String>> allFields = new List<List<String>>();

	    // replace instances where a double quote begins a field containing a comma
	    // in this case you get a double quote followed by a doubled double quote
	    // do this for beginning and end of a field
	    contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
	    // now replace all remaining double quotes - we do this so that we can reconstruct
	    // fields with commas inside assuming they begin and end with a double quote
	    contents = contents.replaceAll('""','DBLQT');
	    // replace all windows carriage returns by normal newlines
	    contents =contents.replace('\r\n','\n');

	    // we are not attempting to handle fields with a newline inside of them
	    // so, split on newline to get the spreadsheet rows
	    list<String> lines=new list<string>();
	    try {
	        lines = contents.split('\n');
	    } catch (System.ListException e) {
	        System.debug('Limits exceeded?  '+e.getMessage());
	    }
	    integer num=0;
	    for(string line :lines) {
	        // check for blank CSV lines (only commas)
	        if(line.replaceAll(',','').trim().length()==0) 
	            break;
	        list<String> fields=line.split(separator,-1);    // needs -1 parameter to make sure also empty columns at the end are included
	        list<String> cleanFields=new list<String>();
	        string compositeField;
	        boolean makeCompositeField=false;
	        for(string field : fields) {
	            if(field.startsWith('"') && field.endsWith('"')) {
	                cleanFields.add(field.replaceAll('DBLQT','"').removeStart('"').removeEnd('"'));
	            } else if(field.startsWith('"')) {
	                makeCompositeField = true;
	                compositeField = field;
	            } else if(field.endsWith('"')) {
	                compositeField += separator + field;
	                cleanFields.add(compositeField.replaceAll('DBLQT','"').removeStart('"').removeEnd('"'));
	                makeCompositeField = false;
	            } else if(makeCompositeField) {
	                compositeField +=  separator + field;
	            } else{
	                cleanFields.add(field.replaceAll('DBLQT','"').removeStart('"').removeEnd('"'));
	            }
	        }  
	        allFields.add(cleanFields);
	    }
	    if(skipHeaders) 
	        allFields.remove(0);
	    return allFields;       
	}

	public static String delimitStrings(List<String> inputStrings, String delimiter){

		String retVal = '';

		if(inputStrings == null) 
			return '';

		for(String inputString : inputStrings){
			
			if (inputString == null)
				inputString = '';

			retVal += inputString + delimiter;
		}

		retVal = retVal.substring(0, retVal.length()-1);

		return retVal;
	}
	
	public static List<String> deDelimitStrings(String inputStrings, String delimiter){

		List<String> retVal = new List<String>();

		if(inputStrings == null || inputStrings == '')
			return new List<String>();
		retVal = inputStrings.split(delimiter);

		return retVal;
	}

	public static Set<String> deDelimitStringsToSet(String inputStrings, String delimiter){

		Set<String> retVal = new Set<String>();

		if(inputStrings == null || inputStrings == ''){
			return new Set<String>();
		} else {
			for(String s : inputStrings.split(delimiter)){
				retVal.add(s);
			}
		}

		return retVal;
	}

	public static String randomString(Integer length){
        Blob blobKey = crypto.generateAesKey(192);
		String key = EncodingUtil.base64encode(blobKey);
		return key.substring(0,length);		
	}

    public static String translateLabel(String labelName, String language){
            Pagereference r = Page.LabelTranslator;
            r.getParameters().put('label_lang', language);
            r.getParameters().put('label', labelName);  
            String labelValue = r.getContent().toString();
            return labelValue;
    }	
}