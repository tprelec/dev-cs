/**
 * @description			Syncs Account, Contact, Site, User and BAN to BOP
 * 						See Work W-000192
 * @author				Gerhard Newman
 */
public without sharing class SyncToBOP {

	// Process starts with an Account ID and syncs the other objects based on this account
	public static void SyncToBOP(ID accID) {
		syncAccount(accID);
		syncBan(accID);
		syncContact(accID);
		syncSite(accID);
	}


	private static void syncAccount(ID accountID) {
		// Note: Accounts don't export unless they are partner accounts
		Set<ID> accountSet = new Set<ID>();
		accountSet.add(accountID);
        AccountExport.exportAccountsOffline(accountSet);
    }


    private static void syncContact(ID accountID) {
        // Note: Contacts don't export without an email address
        //		 Contacts don't export unless the Account or BAN has a BOP code
        //
        // Retrieve all the contacts for this account (but not interal users)
        List<Contact> contactList = [select id, BOP_export_datetime__c 
        							from contact 
        							where accountid=:accountID
        							and email!=null
        							and recordTypeId!=:GeneralUtils.recordTypeMap.get('Contact').get('Internal_Users')];
		Set<Id> updatedContacts = new Set<Id>();
		Set<Id> newContacts = new Set<Id>();

		// Determine if the contact has been exported before or not
		for(Contact c : contactList){
			if(c.BOP_export_datetime__c == null){
				newContacts.add(c.Id);	
			} else {
				updatedContacts.add(c.Id);
			}
		}
		if(!newContacts.isEmpty()){
			ContactExport.exportNewContactsOffline(newContacts);
		}
		if(!updatedContacts.isEmpty()){
			ContactExport.exportUpdatedContactsOffline(updatedContacts);
		} 	
    }


    private static void syncBan(ID accountID) {
    	// Only sync BAN's that already have a BOP code
    	// BOP codes are populated at the time of customer-creation for any new order that flows to SIAS. SIAS returns the bopcode to SFDC.

    	// Retrieve all the Bans for this account
    	// GC 2019-09-30 no longer possible after BonP
    	/*List<Ban__c> banList = [select id, BOPCode__c, BOP_export_Errormessage__c
    							from Ban__c
    							where Account__c=:accountID
    							and BOPCode__c!=null];

        Set<Id> banIdsForExport = new Set<Id>();

        for(Ban__c b : banList){
            banIdsForExport.add(b.Id);
        }

        if(!banIdsForExport.isEmpty()){
            AccountExport.exportBansOffline(banIdsForExport);
        } */    
    }


    private static void syncSite(ID accountID) {
 		Set<Id> siteIDsForUpdate = new Set<Id>();
		Set<Id> newSiteIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();

		// Retrieve all the Sites for this account
    	List<Site__c> siteList = [select id, BOP_export_datetime__c, Site_Account__c
    							from Site__c
    							where Site_Account__c=:accountID];		


		for(Site__c s : siteList){
			accountIds.add(s.Site_Account__c);	
		}

		// check if the Site's account or one of the BANs has a BOPCode (otherwise do not export)
		Set<Id> accountIdsWithBopCode = new Set<Id>();
		for(Ban__c b : [Select BOPCode__c, Account__r.BOPCode__c From Ban__c Where Account__c in :accountIds AND (BOPCode__c != null OR Account__r.BOPCode__c != null)]){
			accountIdsWithBopCode.add(b.Account__c);
		}

		// For all the sites that have an account or ban with a bop code add to a set for processing
		for(Site__c s : siteList){
			if (accountIdsWithBopCode.contains(s.Site_Account__c)) {
				if(s.BOP_export_datetime__c == null){
					newSiteIds.add(s.Id);	
				} else {
					siteIDsForUpdate.add(s.Id);
				}
			}
		}
		if(!newSiteIds.isEmpty()) {	
			SiteExport.exportNewSitesOffline(newSiteIds);						
		}

		if(!siteIDsForUpdate.isEmpty()){
			SiteExport.exportUpdatedSitesOffline(siteIDsForUpdate);											
		}
    }

}