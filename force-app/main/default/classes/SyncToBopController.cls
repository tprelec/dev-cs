public with sharing class SyncToBopController {

	private final Account acc;

    public SyncToBopController(ApexPages.StandardController stdController) {
        this.acc = (Account)stdController.getRecord();
    }

    // Determines if the button should be enabled for the current user
    public Boolean getEnableSyncToBOP() {
        return Special_Authorizations__c.getInstance().Sync_To_BOP__c;
    }

    public void sync() {
    	SyncToBOP.syncToBOP(acc.id);
    }

}