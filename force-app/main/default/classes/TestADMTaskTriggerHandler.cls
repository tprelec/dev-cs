@isTest
private class TestADMTaskTriggerHandler {
	@testSetup
	static void makeData() {
		agf__ADM_Scrum_Team__c team = TestUtils.createScrumTeams(1, true)[0];
		agf__ADM_Product_Tag__c productTag = TestUtils.createProductTags(1, true, team.Id)[0];
		TestUtils.createWorkItems(1, true, TestUtils.createAdministrator().Id, productTag.Id);
		TestUtils.createThemes(1, true, GeneralUtils.currentUser.Id);
	}

	@isTest
	static void testThemeAssignment() {
		User u = TestUtils.createAdministrator();
		TestUtils.createThemes(1, true, u.Id);

		agf__ADM_Work__c work = TestUtils.getWork()[0];

		Test.startTest();

		agf__ADM_Task__c tsk = new agf__ADM_Task__c(
			agf__Assigned_To__c = u.Id,
			agf__Work__c = work.Id,
			agf__Subject__c = 'Test'
		);
		insert tsk;

		System.assertEquals(
			1,
			getThemeAssignmentCount(),
			'Theme Assignment Count should be 1 (Theme is assigned).'
		);

		tsk.agf__Subject__c = 'Test2';
		tsk.agf__Assigned_To__c = GeneralUtils.currentUser.Id;
		update tsk;

		System.assertEquals(
			1,
			getThemeAssignmentCount(),
			'Theme Assignment Count should be 1 (Theme is assigned).'
		);

		delete tsk;

		System.assertEquals(
			0,
			getThemeAssignmentCount(),
			'Theme Assignment Count should be 0 (Theme is not assigned).'
		);

		Test.stopTest();
	}

	private static Integer getThemeAssignmentCount() {
		return Database.countQuery('SELECT COUNT() FROM agf__ADM_Theme_Assignment__c');
	}
}