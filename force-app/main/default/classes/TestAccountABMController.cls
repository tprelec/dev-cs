@isTest
private class TestAccountABMController {
	
	@isTest static void SetABMBooleanTrue() {
		
		// Implement test code
		User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        User u = TestUtils.createManager();
        
        test.startTest();
        acct.Account_Based_Marketing__C = true;
        update acct;
	}
	
	@isTest static void SetABMBooleanFalse() {
		
		// Implement test code
		User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
  
        User u = TestUtils.createManager();
        
        test.startTest();
        acct.Account_Based_Marketing__C = false;
        update acct;

        
	}
	
}