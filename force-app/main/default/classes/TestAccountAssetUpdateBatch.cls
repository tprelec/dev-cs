@isTest
private class TestAccountAssetUpdateBatch {
    
    @isTest
    static void AssetCount() {

        // create an account and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        User u = TestUtils.createManager();

        VF_Asset__c vfa = new VF_Asset__c(Account__c=acct.id);
        insert vfa;
        
        test.startTest();

        AccountAssetUpdateBatch controller = new AccountAssetUpdateBatch();
        Database.executeBatch(controller, 1);

        test.stopTest();
    }

    @isTest
    static void canSchedule() {

        AccountAssetUpdateBatch controller = new AccountAssetUpdateBatch();
        String start = '0 0 23 * * ?';
        system.schedule('Test AccountAssetUpdateBatch', start, controller);
 
    }
	
}