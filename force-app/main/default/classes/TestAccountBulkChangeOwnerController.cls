@isTest
private class TestAccountBulkChangeOwnerController {
    
    @isTest
    static void testChange() {

        // create an account and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact cont = TestUtils.createContact(acct);
        User u = TestUtils.createManager();
        
        test.startTest();
        cont.Userid__c = u.Id;
        update cont;

        Dealer_Information__c di = TestUtils.createDealerInformation(cont.Id);
        acct.Mobile_Dealer__c = di.Id;
        acct.Fixed_Dealer__c = di.Id;
        update acct;
        
        List<account> accounts = [select id from account];
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(accounts);
        AccountBulkChangeOwnerController abcoc = new AccountBulkChangeOwnerController(ssc);

        integer size = abcoc.getMySelectedSize();
        Boolean restricted = abcoc.getIsRestricted();
        // Expect the running user (admin for test) not to have access restricted 
        System.assertEquals(restricted,false);

        test.stopTest();
    }
}