@isTest
private class TestAccountChangeOwnerController {
	
	@isTest
    static void testClaim() {

        // create an account and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact cont = TestUtils.createContact(acct);
        User u = TestUtils.createManager();
        
        Test.startTest();
        cont.Userid__c = u.Id;
        update cont;

        Dealer_Information__c di = TestUtils.createDealerInformation(cont.Id);
        acct.Mobile_Dealer__c = di.Id;
        acct.Fixed_Dealer__c = di.Id;
        update acct;

    	ApexPages.StandardController controller = new ApexPages.Standardcontroller(acct);
    	AccountChangeOwnerController acoc = new AccountChangeOwnerController(controller);

    	acoc.typeOfChangeSelected = 'Claim';
    	acoc.ownershipLevelSelected = 'general';
    	
    	acoc.SaveAndSubmit();

    	acoc.acc.New_Dealer__c = di.Id;
    	acoc.SaveAndSubmit();

    	acoc.ownershipLevelSelected = 'fixed';
    	acoc.SaveAndSubmit();
        acoc.getIsLocked();

        di.Contact__c = null;
        update di;
    	acoc.acc.New_Dealer__c = di.Id;
    	acoc.SaveAndSubmit();
        
    	Test.stopTest();
	}

	@isTest
    static void testHandover() {

        // create an account and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact cont = TestUtils.createContact(acct);
        User u = TestUtils.createManager();

        Test.startTest();

        cont.Userid__c = u.Id;
        update cont;

        Dealer_Information__c di = TestUtils.createDealerInformation(cont.Id);
        acct.Mobile_Dealer__c = di.Id;
        acct.Fixed_Dealer__c = di.Id;
        update acct;
    	
    	ApexPages.StandardController controller = new ApexPages.Standardcontroller(acct);
    	AccountChangeOwnerController acoc = new AccountChangeOwnerController(controller);

    	acoc.typeOfChangeSelected = 'Hand over';
    	acoc.ownershipLevelSelected = 'general';
    	
    	acoc.SaveAndSubmit();

    	acoc.acc.New_Dealer__c = di.Id;
    	acoc.SaveAndSubmit();

    	acoc.ownershipLevelSelected = 'fixed';
    	acoc.SaveAndSubmit();

    	Test.stopTest();
	}	

	@isTest
    static void testBatch() {

        // create an account and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact cont = TestUtils.createContact(acct);
        User u = TestUtils.createManager();
        
        Test.startTest();
        cont.Userid__c = u.Id;
        update cont;

        Dealer_Information__c di = TestUtils.createDealerInformation(cont.Id);
        acct.Mobile_Dealer__c = di.Id;
        acct.Fixed_Dealer__c = di.Id;
        update acct;

        VF_Asset__c vfa1 = new VF_Asset__c();
        vfa1.Account__c = acct.Id;
        vfa1.Dealer_Code__c = di.Dealer_Code__c;
        vfa1.Productmatrix__c = 'VOICE'; // DATA M2M
        vfa1.CTN_Status__c = 'Active';
        vfa1.Contract_Start_Date__c = system.today().addMonths(-12);
        VF_Asset__c vfa2 = vfa1.clone(false,true);
        vfa2.Productmatrix__c = 'DATA';
        VF_Asset__c vfa3 = vfa1.clone(false,true);
        vfa3.Productmatrix__c = 'M2M';
        insert new List<VF_Asset__c>{vfa1,vfa2,vfa3};        

		AccountOwnershipBatch aoBatch = new AccountOwnershipBatch();
		Id batchprocessId = Database.executeBatch(aoBatch,3);	

    	Test.stopTest();
	}	
}