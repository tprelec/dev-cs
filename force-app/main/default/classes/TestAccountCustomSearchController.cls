/**
 * 	@description	This class contains unit tests for the AccountCustomSearch class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestAccountCustomSearchController {
	
	static testMethod void testNormalSearch(){
		Account a = new Account();
		a.Name = 'name';
		a.KvK_Number__c = '12345678';
		a.Visiting_Postal_Code__c = '1234';
		a.BillingPostalCode = '1234';
		insert a;
		Ban__c b = new Ban__c();
		b.Name = '312345678';
		b.Account__c = a.Id;
		insert b;
		
		AccountCustomSearchController acse = new AccountCustomSearchController(); 
		
		acse.searchBanNumber = '312345678';
		acse.searchHousenumber = '52';
		acse.searchKvkNumber ='12345678';
		acse.searchName = 'name';
		acse.searchZipcode = '1234';
		
		acse.doSearch();
		
		acse.createAccount();
		// (no real account is created, only the screen is loaded. So nothing to check here)
		
		PageReference searchForm = page.AccountCustomSearch;
    	Test.setCurrentPage(searchForm);
		ApexPages.currentPage().getParameters().put('accId',a.Id);
		
		AccountCustomSearchController.SearchResult sr = acse.searchResults[0];
		sr.createOpportunity();
		// (no real opportunity is created, only the screen is loaded. So nothing to check here)
		
		System.assertEquals(true,true);
 	}	
 	
	static testMethod void testErroneousSearch(){
		Account a = new Account();
		a.Name = 'name';
		a.Visiting_Postal_Code__c = '1234';
		a.BillingPostalCode = '1234';
		insert a;
		Ban__c b = new Ban__c();
		b.Name = '312345678';
		b.Account__c = a.Id;
		insert b;		
		
		AccountCustomSearchController acse = new AccountCustomSearchController(); 
		
		// no name
		acse.searchBanNumber = '312345678';
		acse.searchHousenumber = '52';
		acse.searchKvkNumber ='12345678';
		acse.searchName = '';
		acse.searchZipcode = '1234';
		acse.doSearch();

		// only name
		acse.searchBanNumber = '';
		acse.searchHousenumber = '';
		acse.searchKvkNumber ='';
		acse.searchName = 'name';
		acse.searchZipcode = '';
		acse.doSearch();

		// wrong zipcode
		acse.searchBanNumber = '';
		acse.searchHousenumber = '';
		acse.searchKvkNumber ='';
		acse.searchName = 'name';
		acse.searchZipcode = '1';
		acse.doSearch();

		// housenr with no zipcode
		acse.searchBanNumber = '';
		acse.searchHousenumber = '1';
		acse.searchKvkNumber ='';
		acse.searchName = 'name';
		acse.searchZipcode = '';
		acse.doSearch();
		
		// wrong ban
		acse.searchBanNumber = '123';
		acse.searchHousenumber = '';
		acse.searchKvkNumber ='';
		acse.searchName = 'name';
		acse.searchZipcode = '';
		acse.doSearch();		

		// wrong kvk
		acse.searchBanNumber = '';
		acse.searchHousenumber = '';
		acse.searchKvkNumber ='1234567890';
		acse.searchName = 'name';
		acse.searchZipcode = '';
		acse.doSearch();		

		System.assertEquals(true,true);
 	} 	
}