/**
 * @description		This is the test class for the (complete) account export
 * @author        	Guy Clairbois
 */
@isTest
private class TestAccountExport {

	@isTest
    static void testCreateAccount() {
        Test.startTest();
		// use mockservice for testing
		Test.setMock(WebServiceMock.class, new TestUtilMockServices());        
        // in order to trigger the account export, create a contract
        TestUtils.createCompleteContract();
        Test.stopTest();
    }

	@isTest
    static void testUpdateAccount() {
        
        // in order to trigger the account export, create a contract
        TestUtils.createCompleteContract();

        List<VF_Contract__c> contracts = new List<VF_Contract__c>([
        	SELECT Id, Name, Account__c FROM VF_Contract__c WHERE Opportunity__r.Name = 'Test Opportunity'
        ]);
        Account a = new Account(Id=contracts[0].Account__c);
        
        Test.startTest();
		// use mockservice for testing
		Test.setMock(WebServiceMock.class, new TestUtilMockServices());        
        a.Phone = '0543543543';
        update a;
        Test.stopTest();
    }
}