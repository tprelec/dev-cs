/**
 * @description		This is the test class for ECSSOAPCompany class. Because of a bug in mockservices, we have to cover this specifically
 * @author        	Guy Clairbois
 */
@isTest
private class TestAccountExportSOAPData {

    static testMethod void dataTest() {
    	ECSSOAPCompany c = new ECSSOAPCompany();
    	
    	ECSSOAPCompany.authenticationHeader_element ah = new ECSSOAPCompany.authenticationHeader_element();
    	ECSSOAPCompany.billType bt = new ECSSOAPCompany.billType();
    	ECSSOAPCompany.companyRefType crt = new ECSSOAPCompany.companyRefType();
    	ECSSOAPCompany.companyResponseType crst = new ECSSOAPCompany.companyResponseType();
    	ECSSOAPCompany.companySoap cs = new ECSSOAPCompany.companySoap();
    	ECSSOAPCompany.createCustomerRequest_element cre = new ECSSOAPCompany.createCustomerRequest_element();
    	ECSSOAPCompany.createResellerRequest_element crr = new ECSSOAPCompany.createResellerRequest_element();
        ECSSOAPCompany.customerBaseType cbt = new ECSSOAPCompany.customerBaseType();         
    	ECSSOAPCompany.customerCreateType cct = new ECSSOAPCompany.customerCreateType();
    	ECSSOAPCompany.customerResponse_element crse = new ECSSOAPCompany.customerResponse_element();
    	ECSSOAPCompany.customerUpdateType cut = new ECSSOAPCompany.customerUpdateType();
        ECSSOAPCompany.resellerBaseType pbt = new ECSSOAPCompany.resellerBaseType();         
 		ECSSOAPCompany.resellerCreateType rct = new ECSSOAPCompany.resellerCreateType();
 		ECSSOAPCompany.resellerResponse_element rre = new ECSSOAPCompany.resellerResponse_element();
 		ECSSOAPCompany.resellerUpdateType rut = new ECSSOAPCompany.resellerUpdateType();    	
    	ECSSOAPCompany.updateCustomerRequest_element ucre = new ECSSOAPCompany.updateCustomerRequest_element();
    	ECSSOAPCompany.updateResellerRequest_element urre = new ECSSOAPCompany.updateResellerRequest_element();
 		 
      	System.assert(true, 'dummy assertion');
    }
    
}