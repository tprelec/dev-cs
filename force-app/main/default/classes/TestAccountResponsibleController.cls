@isTest
public class TestAccountResponsibleController {

    public static testMethod void TestARController_testCase() {
        // claim account
        Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
        Contact currContact = new Contact(LastName = 'CurrentUser', UserId__c = GeneralUtils.currentUser.Id, AccountId = acc.Id);
        insert currContact;
        TestUtils.createDealerInformation(currContact.Id, '888888');
        
        AccountResponsibleController ctrl = new AccountResponsibleController();
        AccountResponsibleController.fetchAll(acc.Id);
        AccountResponsibleController.claim(acc.Id, 'comment', null);
        
    }

    public static testMethod void TestARController_testCase2() {
        // record locked
        Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
        AccountResponsibleController.fetchAll(acc.Id);

        Contact currContact = new Contact(LastName = 'CurrentUser', UserId__c = GeneralUtils.currentUser.Id, AccountId = acc.Id);
        insert currContact;
        Dealer_Information__c currDealer = TestUtils.createDealerInformation(currContact.Id, '888888');
        acc.Claimed__c = true;
        acc.User__c = GeneralUtils.currentUser.Id;
        acc.New_Dealer__c = currDealer.Id;
        update acc;
        AccountResponsibleController.claim(acc.Id, 'comment', currDealer.Id);
        System.assertEquals('comment', [SELECT Id, Approval_Request_Comment__c FROM Account WHERE Id = :acc.Id].Approval_Request_Comment__c);
    }

    public static testMethod void TestARController_testCase3() {
        // no dealer info error
        User manager = TestUtils.createManager();
        Account acc = TestUtils.createAccount(manager);
        Contact c = new Contact(LastName = 'DealerLastName', AccountId = acc.Id);
        insert c;
        Dealer_Information__c dealer = TestUtils.createDealerInformation(c.Id, Label.Dealer_Code_CVM_Owner);
        AccountResponsibleController.fetchAll(acc.Id);
        
        // no validation error
        Contact currContact = new Contact(LastName = 'CurrentUser', UserId__c = GeneralUtils.currentUser.Id, AccountId = acc.Id);
        insert currContact;
        Dealer_Information__c currDealer = TestUtils.createDealerInformation(currContact.Id, '888888');
        AccountResponsibleController.fetchAll(acc.Id);
        AccountResponsibleController.claim(acc.Id, 'comment', null);
        System.assertEquals('comment', [SELECT Id, Approval_Request_Comment__c FROM Account WHERE Id = :acc.Id].Approval_Request_Comment__c);
    }

}