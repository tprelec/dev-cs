/**
 * 	@description	This class contains unit tests for the AccountRevenueTriggerHandler class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestAccountRevenueTriggerHandler {
	
	static testMethod void testCreateUpdateAccountRevenueByBAN(){
	    User owner = TestUtils.createAdministrator();
	    Account acct = TestUtils.createAccount(owner);
	    Test.startTest();
	    
		Account_Revenue__c ar = new Account_Revenue__c();
		// make sure BAN is equal to created account, in order to trigger the trigger
		ar.AR_Billing_Account_Num__c = '399999999';
		ar.AR_Year_Month_Num__c = String.valueOf(system.today().year())+'01';
		ar.Account__c = acct.id;
		insert ar;
		Test.stopTest();
		
		ar = [Select Id, Account__c, OwnerId From Account_Revenue__c Where Id = :ar.Id LIMIT 1];
		System.assert(ar.Account__c == acct.Id, 'AccountRevenue should be linked to correct account.');
      	System.assert(ar.OwnerId == acct.OwnerId, 'AccountRevenue owner should be equal to account owner');
 	}	

	static testMethod void testCreateUpdateAccountRevenueByBOP(){
	    User owner = TestUtils.createAdministrator();
	    Account acct = TestUtils.createAccount(owner);
	    Test.startTest();
	    
		Account_Revenue__c ar = new Account_Revenue__c();
		// make sure BAN is equal to created account, in order to trigger the trigger
		ar.BOP_Code__c = 'TST';
		ar.AR_Year_Month_Num__c = String.valueOf(system.today().year())+'01';
		ar.Account__c = acct.id;
		insert ar;
		Test.stopTest();
		
		ar = [Select Id, Account__c, OwnerId From Account_Revenue__c Where Id = :ar.Id LIMIT 1];
		System.assert(ar.Account__c == acct.Id, 'AccountRevenue should be linked to correct account.');
      	System.assert(ar.OwnerId == acct.OwnerId, 'AccountRevenue owner should be equal to account owner');
 	} 	
}