@isTest
private class TestAccountTriggerHandler {

    private static final String OTHER_DEALER_CODE = '888888';

    @testSetup
    static void makeData() {
        TestUtils.autoCommit = false;
        // Create Dealer Account and Contact
        Account dealerAcc = TestUtils.createAccount(null);
        dealerAcc.Total_Licenses__c = 50;
        insert dealerAcc;
        // Create Contact
        Contact dealerContact = TestUtils.createContact(dealerAcc);
        dealerContact.Userid__c = UserInfo.getUserId();
        insert dealerContact;
        // Create CVM, CM and Other Dealer Info
        Dealer_Information__c cvmDealer = TestUtils.createDealerInformation(dealerContact.Id);
        cvmDealer.Name = 'DealerInfoCVM';
        cvmDealer.Dealer_Code__c = Label.Dealer_Code_CVM_Owner;
        Dealer_Information__c cmDealer = TestUtils.createDealerInformation(dealerContact.Id);
        cmDealer.Name = 'DealerInfoCM';
        cmDealer.Dealer_Code__c = Label.Dealer_Code_CM_Park_Owner;
        Dealer_Information__c otherDealer = TestUtils.createDealerInformation(dealerContact.Id);
        otherDealer.Name = 'OtherDealer';
        otherDealer.Dealer_Code__c = OTHER_DEALER_CODE;
        otherDealer.Default__c = true;
        insert new List<Dealer_Information__c>{cvmDealer, cmDealer, otherDealer};
        TestUtils.autoCommit = true;
        // Create Mapping SOHO Vertical
        TestUtils.createMappingSOHOVertical(true);
        // Create Order Type
        TestUtils.createOrderType();
        // Create Partner Account
        Account partnerAcc = TestUtils.createPartnerAccount();

        // Create Users
        System.runAs(GeneralUtils.currentUser) {
            User partner = TestUtils.createPortalUser(partnerAcc);
            User admin = TestUtils.createAdministrator();
        }

    }

    private static Dealer_Information__c mobileDealer {
        get {
            if (mobileDealer == null) {
                mobileDealer = [SELECT Id FROM Dealer_Information__c WHERE Dealer_Code__c = :OTHER_DEALER_CODE];
            }
            return mobileDealer;
        }
        private set;
    }

    private static User partnerUser {
        get {
            if (partnerUser == null) {
                partnerUser = [SELECT Id, ContactId FROM User WHERE CreatedDate = TODAY AND LastName = 'PortalUser'];
            }
            return partnerUser;
        }
        private set;
    }

    private static User adminUser {
        get {
            if (adminUser == null) {
                adminUser = [SELECT Id FROM User WHERE CreatedDate = TODAY AND Alias = 'TestUser'];
            }
            return adminUser;
        }
        private set;
    }

    private static Map<Id, Account> getAccounts() {
        return new Map<Id, Account>([
            SELECT
                Id,
                OwnerId,
                VGE__c,
                Mobile_Dealer__c,
                Mobile_Dealer__r.Dealer_Code__c,
                Visiting_Postal_Code__c,
                Visiting_Street__c,
                Visiting_Housenumber1__c,
                Visiting_Housenumber_Suffix__c,
                Visiting_City__c,
                LG_VisitPostalCode__c,
                LG_VisitStreet__c,
                LG_VisitHouseNumber__c,
                LG_VisitHouseNumberExtension__c,
                Soho_Vertical__c,
                LG_ResponsiblePartner__c,
                Fixed_Dealer__c
            FROM
                Account
        ]);
    }

    @isTest
    static void testAssignAccountOwnership() {
        Test.startTest();

        TestUtils.autoCommit = false;

        // Create Parent Account
        Account parentAcc = TestUtils.createAccount(null);
        parentAcc.DUNS_Number__c = '758493759';
        parentAcc.VGE__c = true;
        parentAcc.Mobile_Dealer__c = mobileDealer.Id;
        insert parentAcc;

        // Create Account with Ultimate Parent
        Account accWithParent = TestUtils.createAccount(null);
        accWithParent.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName('Account', 'VF_Account');
        accWithParent.Ultimate_Parent_Account__c = parentAcc.Id;

        // Create Account with Parent DUNS
        Account accWithParentDUNS = TestUtils.createAccount(null);
        accWithParentDUNS.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName('Account', 'VF_Account');
        accWithParentDUNS.Parent_DUNS_Number__c = '758493759';

        // Create Account with NumberOfEmployeesConcern__c > 50
        Account accWithEmployees = TestUtils.createAccount(null);
        accWithEmployees.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName('Account', 'VF_Account');
        accWithEmployees.NumberOfEmployeesConcern__c = 100;

        // Create Other Account
        Account accOther = TestUtils.createAccount(null);
        accOther.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName('Account', 'VF_Account');

        insert new List<Account>{accWithParent, accWithParentDUNS, accWithEmployees, accOther};

        Test.stopTest();

        // Verify Results

        Map<Id, Account> accountsMap = getAccounts();

        accWithParent = accountsMap.get(accWithParent.Id);
        System.assertEquals(UserInfo.getUserId(), accWithParent.OwnerId);
        System.assertEquals(mobileDealer.Id, accWithParent.Mobile_Dealer__c);
        System.assert(accWithParent.VGE__c);

        accWithParentDUNS = accountsMap.get(accWithParentDUNS.Id);
        System.assertEquals(UserInfo.getUserId(), accWithParentDUNS.OwnerId);
        System.assertEquals(mobileDealer.Id, accWithParentDUNS.Mobile_Dealer__c);
        System.assert(accWithParentDUNS.VGE__c);

        accWithEmployees = accountsMap.get(accWithEmployees.Id);
        System.assertEquals(UserInfo.getUserId(), accWithEmployees.OwnerId);

        accOther = accountsMap.get(accOther.Id);
        System.assertEquals(UserInfo.getUserId(), accOther.OwnerId);
    }


    @isTest
    static void testCorrectVisitingAddress() {
        Test.startTest();
        TestUtils.autoCommit = false;

        // Create Account
        Account acc = TestUtils.createAccount(null);
        acc.Visiting_Postal_Code__c = '12345 ';
        acc.Visiting_Street__c = ' Street ';
        acc.Visiting_Housenumber1__c = 123;
        acc.Visiting_Housenumber_Suffix__c = 'b';
        acc.Visiting_City__c = 'City';
        insert acc;

        Test.stopTest();

        acc = getAccounts().get(acc.Id);
        System.assertEquals('12345', acc.Visiting_Postal_Code__c);
        System.assertEquals('12345', acc.LG_VisitPostalCode__c);
        System.assertEquals('Street', acc.Visiting_Street__c);
        System.assertEquals('Street', acc.LG_VisitStreet__c);
        System.assertEquals('123', acc.LG_VisitHouseNumber__c);
        System.assertEquals('b', acc.LG_VisitHouseNumberExtension__c);
        System.assertEquals('City', acc.Visiting_City__c);
    }

    @isTest
    static void testSetSohoVertical() {
        Test.startTest();
        TestUtils.autoCommit = false;

        // Create Accounts
        Account acc1 = TestUtils.createAccount(null);
        acc1.BIK_Code__c = '10';
        Account acc2 = TestUtils.createAccount(null);
        acc2.BIK_Code__c = 'XX';
        insert new List<Account>{acc1, acc2};

        Test.stopTest();

        Map<Id, Account> accountsMap = getAccounts();
        acc1 = accountsMap.get(acc1.Id);
        System.assertEquals('Industrie', acc1.Soho_Vertical__c);
        acc2 = accountsMap.get(acc2.Id);
        System.assertEquals('Overig', acc2.Soho_Vertical__c);
    }

    @isTest
    static void testPopulatePartnerAccount() {
        Queue_User_Sharing__c qus = new Queue_User_Sharing__c(
            OwnerId = partnerUser.Id
        );
        insert qus;

        Test.startTest();
        TestUtils.autoCommit = false;

        // Create Account
        Account acc = TestUtils.createAccount(null);
        acc.Queue_User_Assigned__c = qus.Id;
        insert acc;

        Test.stopTest();

        acc = getAccounts().get(acc.Id);
        System.assert(acc.LG_ResponsiblePartner__c != null);
    }

    @isTest
    static void testCreateMainSite() {
        TriggerSettings__c config = TriggerSettings__c.getOrgDefaults();
        config.createMainSite_Enabled__c = true;
        upsert config;

        Test.startTest();
        TestUtils.autoCommit = false;

        // Create Account
        Account acc = TestUtils.createAccount(null);
        acc.Visiting_street__c = 'Street';
        acc.Visiting_Postal_Code__c = '1111AA';
        acc.Visiting_City__c = 'Amsterdam';
        acc.Visiting_Housenumber1__c = 1;

        insert acc;

        Test.stopTest();

        List<Site__c> sites = [SELECT Id FROM Site__c];

        System.assert(!sites.isEmpty(), 'Account main site is not created');
    }

    @isTest
    static void testFixedOwnerSharing() {

        // Create Account and BAN
        Account acc = TestUtils.createAccount(null);
        BAN__c ban = TestUtils.createBan(acc);

        Test.startTest();

        acc.Fixed_Dealer__c = mobileDealer.Id;
        update acc;

        List<AccountShare> accShares = [SELECT Id FROM AccountShare WHERE AccountId = :acc.Id];
        System.assert(!accShares.isEmpty());
        List<Ban__Share> banShares = [SELECT Id FROM Ban__Share WHERE parentId = :ban.Id];
        System.assert(!banShares.isEmpty());

        acc.Fixed_Dealer__c = null;
        update acc;

        Test.stopTest();
    }

    @isTest
    static void testTriggerAccountExport() {
        // Create Account
        TestUtils.autoCommit = false;
        Account acc = getAccounts().values()[0];
        acc.Frame_Work_Agreement__c = 'VF-0001';
        update acc;
        BAN__c ban = TestUtils.createBan(acc);
        ban.BOPCode__c = 'VDF';
        ban.Dealer_Code__c = '123456';
        insert ban;

        Test.startTest();

        Opportunity opp = TestUtils.createOpportunityWithBan(acc, Test.getStandardPricebookId(), ban, GeneralUtils.currentUser);
        insert opp;
        VF_Contract__c cont = TestUtils.createVFContract(acc, opp);
        insert cont;

        // Create Order
        Order__c o = TestUtils.createOrder(cont);
        o.Export_System_Customerdata__c = 'BOP';
        o.OrderType__c = [SELECT Id FROM OrderType__c LIMIT 1].Id;
        o.Account__c = acc.Id;
        insert o;

        acc.Name = 'updated name';
        update acc;

        Test.stopTest();

        System.assert(!AccountExport.accountsInExport.isEmpty(), 'Account Export has not started');
    }

    @isTest
    static void testCheckNPSSurveyAccount() {
        Test.startTest();

        // Create Account
        Account acc = TestUtils.createAccount(null);
        acc.NPS_Pulse_Survey__c = true;
        update acc;

        Test.stopTest();

        List<Medallia_Sync_Records__c> syncs = [SELECT Id FROM Medallia_Sync_Records__c];
        System.assert(!syncs.isEmpty());
    }

    @isTest
    static void testAccountClaim() {
        // Create Account, Opportunity, Task, Lead
        Account acc = getAccounts().values()[0];
        Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
        TestUtils.autoCommit = false;

        Contact c = TestUtils.createContact(acc);
        Task t = new Task(
            WhatId = acc.Id,
            WhoId = c.Id,
            Subject = 'Test'
        );
        insert t;

        Test.startTest();
        Ban__c b = TestUtils.createBAN(acc);
        Lead l = TestUtils.createLead();
        l.Account_name__c = acc.Id;
        insert l;
        acc.Claimed__c = true;
        acc.User__c = adminUser.Id;
        acc.New_Dealer__c = mobileDealer.Id;
        update acc;
        Test.stopTest();

        acc = [SELECT Id, Claimed__c FROM Account WHERE Id = :acc.Id];
        System.assert(!acc.Claimed__c, 'Account is not claimed');

    }

    @isTest
    static void testDealerParentUpdate() {
        TestUtils.autoCommit = false;
        Account partner2 = TestUtils.createPartnerAccount();
        partner2.Dealer_code__c = '999997';
        partner2.Name = 'Dealer ABD';
        partner2.BOPCode__c = 'ABD';
        partner2.BigMachines__Partner_Organization__c = 'sTn';
        insert partner2;

        Integer memberNo = [SELECT Id FROM GroupMember WHERE Group.Name = 'PMs:PASU Dealer'].size();

        Test.startTest();
        Account partnerAcc = [SELECT Id, Commercial_Terms_Group__c FROM Account WHERE Dealer_code__c = '999998'];
        partnerAcc.VIP_Partner__c = true;
        update partnerAcc;
        partner2.Channel__c = 'Distri';
        partner2.ParentId = partnerAcc.Id;
        update partner2;
        System.assertEquals(
            [SELECT Commercial_Terms_Group__c FROM Account WHERE Id = :partner2.Id].Commercial_Terms_Group__c,
            partnerAcc.Commercial_Terms_Group__c
        );

        partner2.VIP_Partner__c = false;
        update partner2;
        System.assertEquals(
            [SELECT VIP_Partner__c FROM Account WHERE Id = :partner2.Id].VIP_Partner__c,
            partnerAcc.VIP_Partner__c
        );

        partner2.ParentId = null;
        update partner2;
        Test.stopTest();

        System.assertEquals([SELECT Id FROM GroupMember WHERE Group.Name = 'PMs:PASU Dealer'].size(), memberNo);
    }
    
}