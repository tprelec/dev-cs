@isTest
public class TestAddressCheckService {
	@isTest
	static void testCallAddressCheckService() {
		Test.setMock(HttpCalloutMock.class, new MockHttpAddressCheck());

		Map<String, String> params = new Map<String, String>();
		params.put(AddressCheckService.POSTAL_CODE, '1111XX');
		params.put(AddressCheckService.HOUSE_NUMBER, '11');

		String result = AddressCheckService.callAddressCheckService(params);

		System.assertEquals(result, '{"example":"test"}', 'Wrong result.');
	}
}