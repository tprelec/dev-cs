@isTest
private class TestApprovalRequestTriggerHandler {
    @TestSetup
    static void makeData() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c = opp.id;
        basket.Name = 'New Basket ' + system.now();
        basket.OwnerId = owner.Id;
        insert basket;
    }

    @isTest
    static void testUpdateOwner() {
        Test.startTest();
        cscfga__Product_Basket__c basketFetch = getBasket();

        appro__Approval_Request__c testRequest = new appro__Approval_Request__c();
        testRequest.appro__RID__c = basketFetch.Id;
        testRequest.appro__Record_Object_Label__c = 'Product Basket';
        insert testRequest;

        appro__Approval_Request__c testRequestFetch = getRequest(testRequest.Id);
        Test.stopTest();

        System.assertEquals(
            basketFetch.OwnerId,
            testRequestFetch.OwnerId,
            'Owner IDs should match'
        );
    }

    @isTest
    static void testUpdateBasketLookup() {
        Test.startTest();
        cscfga__Product_Basket__c basket = getBasket();

        appro__Approval_Request__c testRequest = new appro__Approval_Request__c();
        testRequest.appro__RID__c = basket.Id;
        testRequest.appro__Record_Object_Label__c = 'Product Basket';
        testRequest.Product_Basket_lookup__c = null;
        insert testRequest;

        appro__Approval_Request__c testRequestFetch = getRequest(testRequest.Id);
        Test.stopTest();

        System.assertEquals(
            basket.Id,
            testRequestFetch.Product_Basket_lookup__c,
            'Basket ID lookup should match'
        );
    }

    static appro__Approval_Request__c getRequest(Id requestId) {
        appro__Approval_Request__c testRequestFetch = [
            SELECT Id, Product_Basket_lookup__c, OwnerId
            FROM appro__Approval_Request__c
            WHERE Id = :requestId
        ];

        return testRequestFetch;
    }

    static cscfga__Product_Basket__c getBasket() {
        cscfga__Product_Basket__c basketFetch = [
            SELECT Id, OwnerId
            FROM cscfga__Product_Basket__c
            LIMIT 1
        ];

        return basketFetch;
    }
}