@IsTest
public with sharing class TestAttachmentTriggerHandler {
	@TestSetup
	static void makeData() {
		TestUtils.autoCommit = false;
		User u = TestUtils.createAdministrator();
		u.Email = 'ziggotestuser@vodafone.com';
		u.LG_SalesChannel__c = 'D2D';
		insert u;

		System.runAs(u) {
			No_Triggers__c noTriggers = new No_Triggers__c(SetupOwnerId = u.Id, Flag__c = true);
			insert noTriggers;
			TestUtils.autoCommit = true;
			TestUtils.createMavenDocumentsRecords();
			TestUtils.createClickApproveSettings('Opportunity');
		}
	}

	@IsTest
	private static void testSendQuoteApproval() {
		User u = getUser();
		System.runAs(u) {
			Test.startTest();

			Account acc = TestUtils.createAccount(u);
			Contact con = TestUtils.createContact(acc);

			TestUtils.autoCommit = false;

			Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
			opp.RecordTypeId = GeneralUtils.getRecordTypeIdByName('Opportunity', 'Ziggo');
			opp.LG_AutomatedQuoteDelivery__c = 'Quote Requested';
			opp.OwnerId = u.Id;
			insert opp;

			OpportunityContactRole ocr = new OpportunityContactRole(
				OpportunityId = opp.Id,
				ContactId = con.Id,
				Role = 'Administrative Contact'
			);
			insert ocr;

			Attachment att = new Attachment(
				Name = 'Test Attachment',
				body = Blob.valueOf('this is a test'),
				ContentType = 'pdf',
				IsPrivate = false,
				ParentId = opp.Id
			);
			insert att;

			Test.stopTest();

			opp = [SELECT Id, LG_AutomatedQuoteDelivery__c FROM Opportunity WHERE Id = :opp.Id];
			System.assertEquals(
				'Quote Sent',
				opp.LG_AutomatedQuoteDelivery__c,
				'Quote should be sent.'
			);
		}
	}

	private static User getUser() {
		return [SELECT Id FROM User WHERE Email = 'ziggotestuser@vodafone.com'];
	}
}