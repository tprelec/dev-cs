@isTest
public with sharing class TestBanManagerData {
	@isTest
	public static void testCreateBanManagerData() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		BAN__c ban = TestUtils.createBan(acc);
		BanManagerData bmd = new BanManagerData(
			new Set<Id>{ acc.Id },
			ban.Id,
			true,
			true,
			true,
			true
		);
		List<SelectOption> options;

		Test.startTest();
		options = bmd.bans;
		Test.stopTest();

		System.assert(!options.isEmpty(), 'Test');
	}

	@isTest
	public static void testInsertBan() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		BAN__c ban = TestUtils.createBan(acc);

		BanManagerData bmd = new BanManagerData(
			new Set<Id>{ acc.Id },
			ban.Id,
			false,
			false,
			false,
			false
		);
		bmd.newBan = '599999999';
		BAN__c newBan;

		Test.startTest();
		newBan = bmd.insertNewBan(acc.Id);
		Test.stopTest();

		System.assert(newBan.name == '599999999', 'Test');
	}

	@isTest
	public static void testGetBanRequestNew() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		BAN__c ban = TestUtils.createBan(acc);

		BanManagerData bmd = new BanManagerData(
			new Set<Id>{ acc.Id },
			ban.Id,
			false,
			false,
			false,
			false
		);
		bmd.newBan = ban.Name;
		bmd.banSelected = 'Request New Unify BAN';
		BAN__c newBan;
		Test.startTest();
		newBan = bmd.getBan(acc.Id);
		Test.stopTest();

		System.assert(newBan.name.contains('Requested New Unify BAN'), 'Test');
	}

	@isTest
	public static void testGetBanNew() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		//BAN__c ban = TestUtils.createBan(acc);

		BanManagerData bmd = new BanManagerData(
			new Set<Id>{ acc.Id },
			null,
			false,
			false,
			false,
			false
		);
		bmd.newBan = '599999999';
		bmd.banSelected = 'New';
		BAN__c newBan;

		Test.startTest();
		newBan = bmd.getBan(acc.Id);
		Test.stopTest();

		System.assert(newBan.name == '599999999', 'Test');
	}

	@isTest
	public static void testGetBanRequestException() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		BAN__c ban = TestUtils.createBan(acc);

		BanManagerData bmd = new BanManagerData(
			new Set<Id>{ acc.Id },
			ban.Id,
			false,
			false,
			false,
			false
		);
		bmd.newBan = ban.Name;
		bmd.banSelected = 'new';
		String result;
		Test.startTest();
		try {
			bmd.getBan(acc.Id);
		} catch (Exception e) {
			result = e.getMessage();
		}

		Test.stopTest();

		System.assert(
			result.contains('BAN already exists for this account. Please select from the list.'),
			'Test'
		);
	}

	@isTest
	public static void testGetBanPartner() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		Account partnerAcc = TestUtils.createPartnerAccount();
		User portalUser = TestUtils.createPortalUser(partnerAcc);

		BanManagerData bmd = new BanManagerData(
			new Set<Id>{ acc.Id },
			null,
			false,
			false,
			false,
			false
		);
		bmd.newBan = '599999999';
		BAN__c newBan;

		System.runAs(portalUser) {
			newBan = bmd.getBan(acc.Id);
		}
		System.assertEquals(acc.Id, acc.Id, 'Test');
	}

	@isTest
	public static void testSharingInsert() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		BAN__c ban = TestUtils.createBan(acc);
		Account partnerAcc = TestUtils.createPartnerAccount();
		User portalUser = TestUtils.createPortalUser(partnerAcc);

		Test.startTest();
		BanManagerData.createAccountSharing(acc.Id, portalUser.Id);
		BanManagerData.createBanSharing(ban, UserInfo.getUserId());
		Test.stopTest();

		List<AccountShare> accShares = [SELECT Id FROM AccountShare];
		LIST<Ban__Share> banShares = [SELECT Id FROM Ban__Share];
		System.assert(!accShares.isEmpty() && !banShares.isEmpty(), 'Test');
	}

	@isTest
	public static void testCreateBanManagerData1() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);

		External_Account__c externalAccountObj = new External_Account__c();
		externalAccountObj.Account__c = acc.Id;
		externalAccountObj.External_Account_Id__c = '178';
		externalAccountObj.External_Source__c = 'BOP';
		insert externalAccountObj;
		External_Account__c externalAccount = new External_Account__c();
		externalAccount.Account__c = acc.Id;
		externalAccount.External_Account_Id__c = '678';
		externalAccount.External_Source__c = 'Unify';
		externalAccount.Related_External_Account__c = externalAccountObj.Id;
		insert externalAccount;

		BAN__c ban = new BAN__c();
		ban.Account__c = acc.Id;
		ban.Unify_Customer_Type__c = 'C';
		ban.Unify_Customer_SubType__c = 'A';
		ban.Name = '388888889';
		ban.BAN_Status__c = 'Opened';
		ban.ExternalAccount__c = externalAccount.Id;
		insert ban;
		List<Ban__c> banList = new List<Ban__c>(
			[
				SELECT
					Id,
					Name,
					Ban_Number__c,
					BAN_Status__c,
					Account__c,
					BOPCode__c,
					Has_Fixed__c,
					ExternalAccount__c,
					ExternalAccount__r.External_Account_Id__c,
					ExternalAccount__r.Related_External_Account__c,
					ExternalAccount__r.Related_External_Account__r.External_Account_Id__c
				FROM Ban__c
				WHERE Id = :ban.Id
			]
		);
		// banList.add(ban);
		BanManagerData bmd = new BanManagerData(
			new Set<Id>{ acc.Id },
			ban.Id,
			true,
			true,
			true,
			true
		);
		bmd.visibleBanList = banList;
		List<SelectOption> options;

		Test.startTest();
		options = bmd.bans;
		Test.stopTest();

		System.assert(!options.isEmpty(), 'Test');
	}
	@isTest
	public static void testCreateBanManagerData2() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);

		External_Account__c externalAccountObj = new External_Account__c();
		externalAccountObj.Account__c = acc.Id;
		externalAccountObj.External_Account_Id__c = '178';
		externalAccountObj.External_Source__c = 'BOP';
		insert externalAccountObj;
		External_Account__c externalAccount = new External_Account__c();
		externalAccount.Account__c = acc.Id;
		externalAccount.External_Account_Id__c = '678';
		externalAccount.External_Source__c = 'Unify';
		// externalAccount.Related_External_Account__c=externalAccountObj.Id;
		insert externalAccount;

		BAN__c ban = new BAN__c();
		ban.Account__c = acc.Id;
		ban.Unify_Customer_Type__c = 'C';
		ban.Unify_Customer_SubType__c = 'A';
		ban.Name = '388888889';
		ban.BAN_Status__c = 'Opened';
		ban.ExternalAccount__c = externalAccount.Id;
		insert ban;
		List<Ban__c> banList = new List<Ban__c>(
			[
				SELECT
					Id,
					Name,
					Ban_Number__c,
					BAN_Status__c,
					Account__c,
					BOPCode__c,
					Has_Fixed__c,
					ExternalAccount__c,
					ExternalAccount__r.External_Account_Id__c,
					ExternalAccount__r.Related_External_Account__c,
					ExternalAccount__r.Related_External_Account__r.External_Account_Id__c
				FROM Ban__c
				WHERE Id = :ban.Id
			]
		);
		// banList.add(ban);
		BanManagerData bmd = new BanManagerData(
			new Set<Id>{ acc.Id },
			ban.Id,
			true,
			true,
			true,
			true
		);
		bmd.visibleBanList = banList;
		List<SelectOption> options;

		Test.startTest();
		options = bmd.bans;
		Test.stopTest();

		System.assert(!options.isEmpty(), 'Test');
	}
	@isTest
	public static void testCreateBanManagerData3() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);

		External_Account__c externalAccountObj = new External_Account__c();
		externalAccountObj.Account__c = acc.Id;
		externalAccountObj.External_Account_Id__c = '178';
		externalAccountObj.External_Source__c = 'BOP';
		insert externalAccountObj;
		External_Account__c externalAccount = new External_Account__c();
		externalAccount.Account__c = acc.Id;
		externalAccount.External_Account_Id__c = '678';
		externalAccount.External_Source__c = 'Unify';
		// externalAccount.Related_External_Account__c=externalAccountObj.Id;
		insert externalAccount;

		BAN__c ban = new BAN__c();
		ban.Account__c = acc.Id;
		ban.Unify_Customer_Type__c = 'C';
		ban.Unify_Customer_SubType__c = 'A';
		ban.Name = '388888889';
		ban.BAN_Status__c = 'Closed';
		ban.ExternalAccount__c = externalAccount.Id;
		insert ban;
		List<Ban__c> banList = new List<Ban__c>(
			[
				SELECT
					Id,
					Name,
					Ban_Number__c,
					BAN_Status__c,
					Account__c,
					BOPCode__c,
					Has_Fixed__c,
					ExternalAccount__c,
					ExternalAccount__r.External_Account_Id__c,
					ExternalAccount__r.Related_External_Account__c,
					ExternalAccount__r.Related_External_Account__r.External_Account_Id__c
				FROM Ban__c
				WHERE Id = :ban.Id
			]
		);
		// banList.add(ban);
		BanManagerData bmd = new BanManagerData(
			new Set<Id>{ acc.Id },
			ban.Id,
			true,
			true,
			true,
			true
		);
		bmd.visibleBanList = banList;
		List<SelectOption> options;

		Test.startTest();
		options = bmd.bans;
		Test.stopTest();

		System.assert(!options.isEmpty(), 'Test');
	}
}