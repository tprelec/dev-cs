@IsTest
private class TestBanTriggerHandler {
    static final String ACCOUNT_NAME_1 = 'Test Account 1';
    static final String ACCOUNT_NAME_2 = 'Test Account 2';
    static final String DEALER_CODE = '990000';
    static final String PARENT_DEALER_CODE = '999999';

    @TestSetup
    static void makeData() {
        User owner = TestUtils.createAdministrator();

        TestUtils.autoCommit = false;

        System.runAs(owner) {
            // Create Dealer Date
            Dealer_Information__c dealerInfo = createDealerData(DEALER_CODE, PARENT_DEALER_CODE);
            Dealer_Information__c parentDealerInfo = createDealerData(
                PARENT_DEALER_CODE,
                PARENT_DEALER_CODE
            );

            // Create Accounts
            Account acc1 = TestUtils.createAccount(owner);
            acc1.Name = ACCOUNT_NAME_1;
            acc1.Fixed_Dealer__c = dealerInfo.Id;

            Account acc2 = TestUtils.createAccount(owner);
            acc2.Name = ACCOUNT_NAME_2;
            acc2.Fixed_Dealer__c = parentDealerInfo.Id;

            List<Account> accs = new List<Account>{ acc1, acc2 };
            insert accs;
        }
    }

    static Dealer_Information__c createDealerData(String dealerCode, String parentDealerCode) {
        String email = dealerCode + '@vfztest.com';

        Account dealerAcc = TestUtils.createPartnerAccount();
        dealerAcc.Name = dealerCode;
        dealerAcc.Dealer_code__c = dealerCode;
        dealerAcc.BOPCode__c = dealerCode.left(3);
        insert dealerAcc;

        Contact dealerCon = TestUtils.createContact(dealerAcc);
        dealerCon.Email = email;
        insert dealerCon;

        User dealerUser = TestUtils.createPortalUser(dealerAcc);
        dealerUser.Username = email;
        dealerUser.CommunityNickname = 'TestDealer' + dealerCode;
        dealerUser.Dealercode__c = dealerCode;
        dealerUser.ContactId = dealerCon.Id;
        insert dealerUser;

        dealerCon.Userid__c = dealerUser.Id;
        update dealerCon;

        Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(
            dealerCon.Id,
            dealerCode,
            parentDealerCode
        );
        insert dealerInfo;
        return dealerInfo;
    }

    @IsTest
    static void testAccountBanCount() {
        Account acc = getAccount();

        Test.startTest();
        Ban__c ban = TestUtils.createBan(acc);
        ban.BAN_Status__c = 'Opened';
        update ban;

        acc = getAccount();

        System.assertEquals(1, acc.Active_Ban_Count__c, 'Account BANs should be counted.');

        delete ban;
        Test.stopTest();

        acc = getAccount();

        System.assertEquals(0, acc.Active_Ban_Count__c, 'Account BANs should be counted.');
    }

    @IsTest
    static void testUpdateAccountToCustomer() {
        Account acc = getAccount();

        Test.startTest();
        Ban__c ban = TestUtils.createBan(acc);
        Test.stopTest();

        acc = getAccount();

        System.assertEquals(
            'Customer',
            acc.Type,
            'Account Type should be Customer if it has related BANs.'
        );
    }

    @IsTest
    static void testCreateSharingRules() {
        Account acc = getAccount();

        Test.startTest();
        TestUtils.autoCommit = false;
        Ban__c ban = TestUtils.createBan(acc);
        ban.OwnerId = getUser(DEALER_CODE).Id;
        insert ban;

        // Fixed Owner Share Check
        Group dealerGroup = getDealerGroup(DEALER_CODE);
        List<Ban__Share> banShares = getBanShares(ban, dealerGroup.Id);
        System.assert(banShares.size() > 0, 'BAN shares should be created for Fixed Owner.');

        // Hierarchy Share Check
        User parentDealer = getUser(PARENT_DEALER_CODE);
        banShares = getBanShares(ban, parentDealer.Id);
        System.assert(banShares.size() > 0, 'BAN shares should be created for Dealer Hierarchy.');
        Test.stopTest();
    }

    @IsTest
    static void testUpdateSharingRules() {
        Account acc = getAccount();

        Test.startTest();
        Ban__c ban = TestUtils.createBan(acc);
        Account acc2 = getAccount(ACCOUNT_NAME_2);
        ban.Account__c = acc2.Id;
        update ban;

        // There should be no Shares related to child Dealer
        Group dealerGroup = getDealerGroup(DEALER_CODE);
        List<Ban__Share> banShares = getBanShares(ban, dealerGroup.Id);
        System.assert(banShares.size() == 0, 'BAN shares should not exist for old Fixed Dealer.');

        // There should be Shares related to the parent dealer
        Group parentDealerGroup = getDealerGroup(PARENT_DEALER_CODE);
        banShares = getBanShares(ban, parentDealerGroup.Id);
        System.assert(banShares.size() > 0, 'BAN shares should be created for new Fixed Dealer.');
        Test.stopTest();
    }

    private static Account getAccount() {
        return getAccount(ACCOUNT_NAME_1);
    }

    private static Account getAccount(String name) {
        return [SELECT Id, Type, Active_Ban_Count__c FROM Account WHERE Name = :name];
    }

    private static User getUser(String dealerCode) {
        String email = dealerCode + '@vfztest.com';
        return [SELECT Id, UserRole.DeveloperName FROM User WHERE Username = :email];
    }

    private static Group getDealerGroup(String dealerCode) {
        User dealerUser = getUser(dealerCode);
        return [
            SELECT Id
            FROM Group
            WHERE
                DeveloperName = :dealerUser.UserRole.DeveloperName
                AND Type = 'RoleAndSubordinates'
        ];
    }

    private static Dealer_Information__c getDealerInfo(String dealerCode) {
        return [SELECT Id FROM Dealer_Information__c WHERE Dealer_Code__c = :dealerCode];
    }

    private static List<Ban__Share> getBanShares(Ban__c ban, Id userOrGroupId) {
        return [
            SELECT Id
            FROM Ban__Share
            WHERE
                ParentId = :ban.Id
                AND UserOrGroupId = :userOrGroupId
                AND RowCause = :Schema.Ban__share.RowCause.Dealer_Hierarchy__c
        ];
    }
}