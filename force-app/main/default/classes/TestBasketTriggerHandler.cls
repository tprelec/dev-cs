@isTest
private class TestBasketTriggerHandler {
	
	@isTest
    static void createFrameWorkID() {
   
        User owner = TestUtils.createAdministrator();

        Account acct = TestUtils.createAccount(owner);

        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );

        Framework__c f = Framework__c.getOrgDefaults();
        f.Framework_Sequence_Number__c = 1;
        upsert f;

        test.startTest();

    	cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
		basket.cscfga__Opportunity__c=opp.id;
		basket.Name = 'New Basket '+system.now();
		insert basket;
        
        test.stopTest();
    }
	
}