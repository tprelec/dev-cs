@isTest
private class TestBdsRestCreateOpportunity
{
    @isTest
    static void runValidateValuesMissingKvk() {
        Test.startTest();

        List<BdsResponseClasses.ValidationError> errList = BdsRestCreateOpportunity.validateValues('', '001', 'name@example.com', 'New', 'IPVPN','1');
        System.assertEquals(1, errList.size());
        System.assertEquals('kvk', errList[0].field);
        Test.stopTest();
    }


    @isTest
    static void runValidateValuesMissingDealercode() {
        Test.startTest();

        List<BdsResponseClasses.ValidationError> errList = BdsRestCreateOpportunity.validateValues('098765432', '', 'name@example.com', 'New', 'IPVPN','1');
        System.assertEquals(1, errList.size());
        System.assertEquals('dealercode', errList[0].field);
        Test.stopTest();
    }

    @isTest
    static void runValidateValuesMissingProjectcontact() {
        Test.startTest();

        List<BdsResponseClasses.ValidationError> errList = BdsRestCreateOpportunity.validateValues('098765432', '001', '', 'New', 'IPVPN','1');
        System.assertEquals(1, errList.size());
        System.assertEquals('projectcontact', errList[0].field);
        Test.stopTest();
    }

    @isTest
    static void runValidateValuesMissingDealtype() {
        Test.startTest();

        List<BdsResponseClasses.ValidationError> errList = BdsRestCreateOpportunity.validateValues('098765432', '001', 'name@example.com', '', 'IPVPN','1');
        System.assertEquals(1, errList.size());
        System.assertEquals('dealtype', errList[0].field);
        Test.stopTest();
    }

    @isTest
    static void runCreateOpp() {User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0623456789';
        conClass.mobilephone = '0623456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);
        Test.startTest();



        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN','1', '');
        Test.stopTest();
    }

    @isTest
    static void runCreateOppNoContact() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;

        Test.startTest();
        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0623456789';
        conClass.mobilephone = '0623456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('123', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN', '1', '');
        System.assertEquals(false, retClass.isSuccess);
        System.assertEquals('Unable to find contact with email-address: name@example.com', retClass.errorMessage);

        Test.stopTest();
    }

    @isTest
    static void runCreateOppMissingContactUser() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;

        Test.startTest();
        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0623456789';
        conClass.mobilephone = '0623456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];

        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN', '1', '');
        System.assertEquals(false, retClass.isSuccess);
        System.assertEquals('Dealer has no attached user', retClass.errorMessage);

        Test.stopTest();
    }

    @isTest
    static void runCreateOppMissingMissingDealer() {
        Test.startTest();
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0623456789';
        conClass.mobilephone = '0623456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN', '1', '');
        System.assertEquals(false, retClass.isSuccess);
        System.assertEquals('Unable to find dealer with dealercode: 001001', retClass.errorMessage);

        Test.stopTest();
    }
}