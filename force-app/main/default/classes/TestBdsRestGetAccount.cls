@isTest
private class TestBdsRestGetAccount {
    @isTest
    static void runGetAcc() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass = new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0623456789';
        conClass.mobilephone = '0623456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        Test.startTest();

        BdsRestGetAccount.doPost('01234567');

        Test.stopTest();
    }

    @isTest
    static void runGetAccNoKvk() {

        Test.startTest();

        BdsRestGetAccount.doPost('');

        Test.stopTest();
    }

    @isTest
    static void runGetAccInvalidKvk() {

        Test.startTest();

        BdsRestGetAccount.doPost('1');

        Test.stopTest();
    }
}