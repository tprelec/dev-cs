@isTest
public with sharing class TestBdsRestGetOpportunityId {

    @isTest
    public static void testDoGetPositive() {
        TestUtils.autocommit = true;
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

        opp.Ban__c = ban.Id;
        update opp;
        Order__c ord = TestUtils.createOrder(contr);

        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        String orderName = [SELECT Id, Name FROM Order__c WHERE Id =: ord.Id].Name;

        request.requestUri = URL.getSalesforceBaseUrl().toExternalForm() + '/apexrest/getopportunityid/' + orderName;

        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;

        Test.startTest();
            BdsRestGetOpportunityId.doGet();
        Test.stopTest();

        System.assertEquals('{"opportunityNumber":"' + [SELECT Id, Opportunity_Number__c FROM Opportunity WHERE Id =: opp.Id].Opportunity_Number__c + '"}',
                            res.responseBody.toString(),
                            'The API response should return the Opportunity_Number__c of the Opportunity created in the Testclass');
    }

    @isTest
    public static void testDoGetNegative() {
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();

        request.requestUri = URL.getSalesforceBaseUrl().toExternalForm() + '/apexrest/getopportunityid/' + 'WrongValue';

        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = res;

        Test.startTest();
            BdsRestGetOpportunityId.doGet();
        Test.stopTest();

        System.assert(res.responseBody.toString().contains('error') ,'The API response should return an error');
    }
}