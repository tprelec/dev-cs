@isTest
private class TestBdsRestUploadFiles {
    @isTest
    static void runAttachKvk() {

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0123456789';
        conClass.mobilephone = '0123456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        Test.startTest();
        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN','1', '');

        RestRequest req = new RestRequest();
        req.params.put('oppId', retClass.opportunityId);
        req.params.put('docType', 'kvk');
        req.params.put('docExt', 'pdf');
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        BdsRestUploadFiles.attachFile();
        Test.stopTest();
    }

    @isTest
    static void runAttachKvkDoc() {

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0123456789';
        conClass.mobilephone = '0123456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);


        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;
        
        Test.startTest();
        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN','1', '');

        RestRequest req = new RestRequest();
        req.params.put('oppId', retClass.opportunityId);
        req.params.put('docType', 'kvk');
        req.params.put('docExt', 'doc');
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        BdsRestUploadFiles.attachFile();
        Test.stopTest();
    }

    @isTest
    static void runAttachKvkDocx() {

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0123456789';
        conClass.mobilephone = '0123456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        Test.startTest();
        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN','1', '');

        RestRequest req = new RestRequest();
        req.params.put('oppId', retClass.opportunityId);
        req.params.put('docType', 'kvk');
        req.params.put('docExt', 'docx');
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        BdsRestUploadFiles.attachFile();
        Test.stopTest();
    }

    @isTest
    static void runAttachContractPdf() {

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0123456789';
        conClass.mobilephone = '0123456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        Test.startTest();
        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN','1', '');

        RestRequest req = new RestRequest();
        req.params.put('oppId', retClass.opportunityId);
        req.params.put('docType', 'contract');
        req.params.put('docExt', 'pdf');
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        BdsRestUploadFiles.attachFile();
        Test.stopTest();
    }

    @isTest
    static void runAttachContractDoc() {

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0123456789';
        conClass.mobilephone = '0123456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        Test.startTest();
        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN','1', '');

        RestRequest req = new RestRequest();
        req.params.put('oppId', retClass.opportunityId);
        req.params.put('docType', 'contract');
        req.params.put('docExt', 'doc');
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        BdsRestUploadFiles.attachFile();
        Test.stopTest();
    }

    @isTest
    static void runAttachContractDocx() {

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0123456789';
        conClass.mobilephone = '0123456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        Test.startTest();
        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN','1', '');

        RestRequest req = new RestRequest();
        req.params.put('oppId', retClass.opportunityId);
        req.params.put('docType', 'contract');
        req.params.put('docExt', 'docx');
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        BdsRestUploadFiles.attachFile();
        Test.stopTest();
    }
}