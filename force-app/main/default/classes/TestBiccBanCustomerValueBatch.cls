/**
 * @description			This is the test class that contains the tests for the BICC BAN Customer Value Batch
 * @author				Ferdinand Bondt
 */

@isTest
private class TestBiccBanCustomerValueBatch {

	static testMethod void myUnitTest() {

		//Data preparation
		TestUtils.autoCommit = false;
		
		map<String, String> mapping = new map<String, String>{'BAN__c' => 'Name'
															 /* ,'VALUE_TIER_7__c' => 'Customer_Value__c' REMOVING THIS AS THE FIELD IS REMOVED. SHOULD BE REMOVE THE ENTIRE CODE?*/
															};
		list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
		for (String field : mapping.keyset()) {
			fsmlist.add(TestUtils.createSync('BICC_BAN_CUST_VALUE__c -> Ban__c', field, mapping.get(field)));
		}
		insert fsmlist;

		list<Account> accountList = new list<Account>{
		new Account(Name = 'Test Account 00', KVK_number__c = '22312351', Visiting_Postal_Code__c = '1491 HV', Ban_Number__c = '312312311')};
		insert accountList;

		Account acc = [select Id from Account where KVK_number__c = '22312351'];

		list<Ban__c> banList = new list<Ban__c>{
		new Ban__c(Name = '312312311', Account__c = acc.Id)}; //Update
		insert banList;

		TestUtils.autoCommit = true;

		TestUtils.createBICCValue('412312311', 'A'); //Wrong BAN number
		TestUtils.createBICCValue('312312311', 'B'); //Match existing account
		TestUtils.createBICCValue('312312312', 'C'); //Ban number doesn't exist
		TestUtils.createBICCValue('312312311', 'B'); //Double entry

		//Test
		Test.startTest();

		PageReference pageRef = Page.BiccBanCustomerValueBatchExecute;
		Test.setCurrentPage(pageRef);
		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new list<BICC_BAN_CUST_VALUE__c>());
		BiccBanCustValBatchExecuteController controller = new BiccBanCustValBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		//Checks
		// NO LONGER APPLICABLE SINCE THE CUSTOMER VALUE FIELD WAS DELETED
		//System.assertEquals([select Customer_Value__c from Ban__c where Name =: '312312311' limit 1].Customer_Value__c, 'B'); //Succesful update
	}
}