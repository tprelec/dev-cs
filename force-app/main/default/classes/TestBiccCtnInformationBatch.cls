/**
 * @description         This is the test class that contains the tests for the BICC CTN Customer Value Batch
 * @author              Ferdinand Bondt
 */
@isTest
private class TestBiccCtnInformationBatch {

    static testMethod void myUnitTest() {
        
        //Data preparation
        TestUtils.autoCommit = false;

        map<String, String> mapping = new map<String, String>{'BAN__c' => 'BAN_Number__c',
                                                              'CTN__c' => 'CTN__c',
                                                              'ctn_status__c' => 'ctn_status__c',
                                                              'nof_used_devices__c' => 'Number_of_used_Devices__c',
                                                              'Retained_Contract_Dt__c' => 'Retention_Date__c',
                                                              'Agreement_Number__c' => 'Agreement_Number__c',
                                                              'Contract_Number__c' => 'Contract_Number__c'};
        list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
        for (String field : mapping.keyset()) {
            fsmlist.add(TestUtils.createSync('BICC_CTN_Information__c -> VF_Asset__c', field, mapping.get(field)));
        }
        insert fsmlist;

        Account acc = new Account(Name = 'Test Account', BAN_Number__c = '312312311'); //Match on Ban
        insert acc;

        VF_Asset__c updatevfa = new VF_Asset__c(Account__c = acc.Id, CTN__c = '12343', Ctn_status__c = 'Inactive', Dealer_Code__c = '00123456', Contract_Number__c = '100'); //Update asset
        system.debug('---------------- updatevfa = ' + updatevfa);
        insert updatevfa;
        VF_Asset__c updatevfa2 = new VF_Asset__c(Account__c = acc.Id, CTN__c = '12344', Ctn_status__c = 'Active', Dealer_Code__c = '00123456', Contract_Number__c = '200'); //Update asset
        system.debug('---------------- updatevfa2 = ' + updatevfa2);
        insert updatevfa2;
        
        TestUtils.autoCommit = true;
        
        TestUtils.createBiccCtn('312312311', '12341', 'Active', '100', '2011-02-03'); //Match on Ban
        TestUtils.createBiccCtn('412312316', '12355', 'Inactive', '100', '2011-02-03'); //No valid Ban
        TestUtils.createBiccCtn('312312312', '12342', 'Active', '100', '2011-02-03'); //No matching account
        TestUtils.createBiccCtn('312312311', '12343', 'Active', '100', '2011-02-03'); //Update asset
        
        //Test
        Test.startTest();
        updatevfa.Contract_Number__c = '101';
        system.debug('---------------- updatevfa = ' + updatevfa);
        update updatevfa;
        PageReference pageRef = Page.BiccCtnInformationBatchExecute;
        Test.setCurrentPage(pageRef);       
        Apexpages.StandardSetController sc = new Apexpages.standardSetController(new list<BICC_CTN_Information__c>());
        BiccCtnInformationBatchExecuteController controller = new BiccCtnInformationBatchExecuteController(sc);
        controller.callBatch();

        Test.stopTest();

        //Checks
        System.assertEquals([select Id from VF_Asset__c where Account__c =: acc.Id AND CTN__c = '12343'].size(), 1); //Succesful update
        System.assertEquals([select Ctn_status__c from VF_Asset__c where CTN__c =: '12343'].Ctn_status__c, 'Inactive'); //Succesful asset update
    }
}