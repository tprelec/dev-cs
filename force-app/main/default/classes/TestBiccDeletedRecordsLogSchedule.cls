@isTest
private class TestBiccDeletedRecordsLogSchedule {

   // CRON expression: put in any date.
   // Because this is a test, job executes
   // immediately after Test.stopTest().
   public static String CRON_EXP = '0 0 0 15 3 ? 2022';

   static testmethod void test() {

      // Set mock callout class 
      Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout());
      
      // create a org default Job_Management__c custom setting
      Job_Management__c jm = new Job_Management__c();
      jm.BI_Error_Log_Email__c = '123@example.com';
      insert jm;

      Test.startTest();

      // Schedule the test job
      String jobId = System.schedule('ScheduleApexClassTest',CRON_EXP, new BiccDeletedRecordsLogSchedule(UserInfo.getSessionId(),system.now().addDays(-5),system.now().addDays(-4)));
         
      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

      // Verify the expressions are the same
      System.assertEquals(CRON_EXP, ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

      // Verify the next time the job will run
      System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
      // Verify the scheduled job hasn't run yet.
      BICC_Deleted_Records_Log__c[] ml = [SELECT Id FROM BICC_Deleted_Records_Log__c WHERE Name != null];
      System.assertEquals(ml.size(),0);



      Test.stopTest();

      // Now that the scheduled job has executed after Test.stopTest(),
      // fetch the new deleted record that got added.
      ml = [SELECT Id, Object__c FROM BICC_Deleted_Records_Log__c WHERE CreatedDate = TODAY];
      system.debug(ml);
      // there is a bug here! Records deleted in the test are not visible, while some 'live' records (e.g. OpportunityLineItem) are..
      //System.assertEquals(1,ml.size());

   }
}