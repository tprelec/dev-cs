//@isTest(isParallel=true)
	@isTest
	private class TestBiccGenericBatch {
	@isTest
	static void testCTNOwnership() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(
			TestUtils.createSync(
				'BICC_CTN_Ownership__c -> VF_Asset__c',
				'Dealer_code__c',
				'Dealer_code__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync('BICC_CTN_Ownership__c -> VF_Asset__c', 'CTN__c', 'CTN__c')
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_CTN_Ownership__c -> VF_Asset__c',
				'Calculation_Date__c',
				'Calculation_Date__c'
			)
		);
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(admin);
		Ban__c ban = TestUtils.createBan(acc);
		VF_Asset__c asset = new VF_Asset__c();
		asset.Account__c = acc.id;
		asset.BAN__c = ban.ID;
		asset.CTN__c = '1234';
		insert asset;

		//Data preparation for load table

		list<BICC_CTN_Ownership__c> loadList = new List<BICC_CTN_Ownership__c>{
			new BICC_CTN_Ownership__c(
				Calculation_Date__c = '2017-05-05',
				CTN__c = '1234',
				Dealer_code__c = '999'
			),
			new BICC_CTN_Ownership__c(
				Calculation_Date__c = 'FAIL',
				CTN__c = '1234',
				Dealer_code__c = '999'
			),
			new BICC_CTN_Ownership__c(
				Calculation_Date__c = '2017-05-05',
				CTN__c = '666',
				Dealer_code__c = '999'
			),
			new BICC_CTN_Ownership__c(Calculation_Date__c = '2017-05-05', CTN__c = '666')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_CTN_Ownership__c');
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
		database.executebatch(controller);

		Test.stopTest();
	}

	@isTest
	static void testCTNInformation() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(
			TestUtils.createSync(
				'BICC_CTN_Information__c -> VF_Asset__c',
				'BAN__c',
				'BAN_Number__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync('BICC_CTN_Information__c -> VF_Asset__c', 'CTN__c', 'CTN__c')
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_CTN_Information__c -> VF_Asset__c',
				'ctn_status__c',
				'ctn_status__c'
			)
		);
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(admin);
		TestUtils.autoCommit = false;
		Ban__c ban = TestUtils.createBan(acc);
		ban.Name = '323456789';
		insert ban;
		VF_Asset__c asset = new VF_Asset__c();
		asset.Account__c = acc.id;
		asset.BAN__c = ban.ID;
		asset.CTN__c = '1234';
		insert asset;

		//Data preparation for load table

		list<BICC_CTN_Information__c> loadList = new List<BICC_CTN_Information__c>{
			new BICC_CTN_Information__c(
				BAN__c = '323456789',
				CTN__c = '1234',
				ctn_status__c = 'Active'
			),
			new BICC_CTN_Information__c(BAN__c = 'FAIL', CTN__c = '1234', ctn_status__c = 'Active'),
			new BICC_CTN_Information__c(
				BAN__c = '323456789',
				CTN__c = '666',
				ctn_status__c = 'Active'
			),
			new BICC_CTN_Information__c(BAN__c = '323456789', CTN__c = '666')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_CTN_Information__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testSites() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(
			TestUtils.createSync('BICC_Site__c -> Site__c', 'Unify_Site_Id__c', 'Unify_Ref_Id__c')
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Site__c -> Site__c',
				'Unify_Account_Id__c',
				'Unify_Account_Id__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Site__c -> Site__c',
				'Site_House_Number__c',
				'Site_House_Number__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Site__c -> Site__c',
				'Site_Postal_Code__c',
				'Site_Postal_Code__c'
			)
		);

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Site__c site = TestUtils.createSite(acc);
		site.Unify_Ref_Id__c = '55';
		site.Unify_Account_Id__c = '99';
		insert site;
        Site__c site2 = TestUtils.createSite(acc);
		site2.Unify_Account_Id__c = '99';
        site2.Site_Postal_Code__c = '1234AB';
        insert site2;

		//Data preparation for load table

		list<BICC_Site__c> loadList = new List<BICC_Site__c>{
			new BICC_Site__c(
				Unify_Site_Id__c = '55',
				Unify_Account_Id__c = '99',
				Site_House_Number__c = '88',
				Site_Postal_Code__c = '1234AB'
			),
			new BICC_Site__c(
				Unify_Site_Id__c = '44',
				Unify_Account_Id__c = '99',
				Site_House_Number__c = '88',
				Site_Postal_Code__c = '1234AB'
			),
			new BICC_Site__c(
				Unify_Site_Id__c = '55',
				Unify_Account_Id__c = '666',
				Site_House_Number__c = '88',
				Site_Postal_Code__c = '1234AB'
			),
			new BICC_Site__c(
				Unify_Site_Id__c = '88',
				Unify_Account_Id__c = '99',
				Site_Postal_Code__c = '1234AB'
			)
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Site__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testBillingArrangement() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Billing_Arrangment__c -> Billing_Arrangement__c',
				'Unify_Billing_Arrangement_Id__c',
				'Unify_Ref_Id__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Billing_Arrangment__c -> Billing_Arrangement__c',
				'Payment_Method__c',
				'Payment_Method__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Billing_Arrangment__c -> Billing_Arrangement__c',
				'Bank_Account_Name__c',
				'Bank_Account_Name__c'
			)
		);

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Contact con = TestUtils.createContact(acc);
		insert con;
		Ban__c ban = TestUtils.createBan(acc);
		ban.Name = '323456789';
		insert ban;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.id);
		insert fa;
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
		ba.Unify_Ref_Id__c = '55';
		insert ba;

		//Data preparation for load table

		list<BICC_Billing_Arrangment__c> loadList = new List<BICC_Billing_Arrangment__c>{
			new BICC_Billing_Arrangment__c(
				Unify_Billing_Arrangement_Id__c = '55',
				Payment_Method__c = 'Invoice',
				Bank_Account_Name__c = 'Slush Fund'
			),
			new BICC_Billing_Arrangment__c(
				Unify_Billing_Arrangement_Id__c = '44',
				Payment_Method__c = 'Invoice',
				Bank_Account_Name__c = 'Slush Fund'
			),
			new BICC_Billing_Arrangment__c(
				Unify_Billing_Arrangement_Id__c = '55',
				Payment_Method__c = 'Robbery',
				Bank_Account_Name__c = 'Slush Fund'
			),
			new BICC_Billing_Arrangment__c(
				Unify_Billing_Arrangement_Id__c = '55',
				Payment_Method__c = 'Invoice'
			)
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Billing_Arrangment__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testFinancialAccounts() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Financial_Account__c -> Financial_Account__c',
				'Status__c',
				'Status__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Financial_Account__c -> Financial_Account__c',
				'Unify_Financial_Account_Id__c',
				'Unify_Ref_Id__c'
			)
		);

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Contact con = TestUtils.createContact(acc);
		insert con;
		Ban__c ban = TestUtils.createBan(acc);
		ban.Name = '323456789';
		insert ban;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.id);
		fa.Unify_Ref_Id__c = '55';
		insert fa;

		//Data preparation for load table

		list<BICC_Financial_Account__c> loadList = new List<BICC_Financial_Account__c>{
			new BICC_Financial_Account__c(
				Unify_Financial_Account_Id__c = '55',
				Status__c = 'Open',
				Ban__c = '323456789',
				Financial_Contact__c = con.id
			),
			new BICC_Financial_Account__c(
				Unify_Financial_Account_Id__c = '44',
				Status__c = 'Open',
				Ban__c = '323456789',
				Financial_Contact__c = con.id
			),
			new BICC_Financial_Account__c(
				Unify_Financial_Account_Id__c = '55',
				Status__c = 'Missing',
				Ban__c = '323456789',
				Financial_Contact__c = con.id
			),
			new BICC_Financial_Account__c(
				Unify_Financial_Account_Id__c = '55',
				Status__c = 'Open',
				Ban__c = '555',
				Financial_Contact__c = con.id
			),
			new BICC_Financial_Account__c(
				Unify_Financial_Account_Id__c = '55',
				Status__c = 'Open',
				Ban__c = '323456789'
			)
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Financial_Account__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testBans() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_BAN_Information__c -> Ban__c', 'BAN__c', 'Name'));
		fsmlist.add(
			TestUtils.createSync(
				'BICC_BAN_Information__c -> Ban__c',
				'Unify_reference_account__c',
				'Unify_Ref_Id__c'
			)
		);

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;

		//Data preparation for load table

		list<BICC_BAN_Information__c> loadList = new List<BICC_BAN_Information__c>{
			new BICC_BAN_Information__c(Unify_reference_account__c = '99', Ban__c = '323456789'),
			new BICC_BAN_Information__c(Unify_reference_account__c = '44', Ban__c = '323456789'),
			new BICC_BAN_Information__c(Unify_reference_account__c = '99', Ban__c = '555')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_BAN_Information__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testContacts() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Email__c', 'Email'));
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Contact__c -> Contact',
				'Unify_Contact_Id__c',
				'Unify_Ref_Id__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync('BICC_Contact__c -> Contact', 'Unify_Account_Id__c', 'N/A')
		);
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Contact con = TestUtils.createContact(acc);
		con.Unify_Ref_Id__c = '123';
		insert con;
		TestUtils.autocommit = true;

		//Data preparation for load table

		list<BICC_Contact__c> loadList = new List<BICC_Contact__c>{
			new BICC_Contact__c(
				Email__c = 'gerhard276@hotmail.com',
				Unify_Contact_Id__c = '123',
				Unify_Account_Id__c = '99'
			),
			new BICC_Contact__c(
				Email__c = 'gerhard276@hotmail.com',
				Unify_Contact_Id__c = '123',
				Unify_Account_Id__c = '88'
			),
			new BICC_Contact__c(
				Email__c = 'geen@vodafone.com',
				Unify_Contact_Id__c = '123',
				Unify_Account_Id__c = '99'
			)
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Contact__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testContacts2() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Email__c', 'Email'));
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Contact__c -> Contact',
				'Unify_Contact_Id__c',
				'Unify_Ref_Id__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync('BICC_Contact__c -> Contact', 'Unify_Account_Id__c', 'N/A')
		);
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Contact con = TestUtils.createContact(acc);
		con.Unify_Ref_Id__c = '123';
		con.Email = 'email@vodafone.com';
		insert con;
		TestUtils.autocommit = true;
		/**
		Contact_Unify_Map__c cum = new Contact_Unify_Map__c(
			Contact__c = con.Id,
			Unify_Ref_Id__c = '1234'
		);
		insert cum;
		**/

		//Data preparation for load table

		list<BICC_Contact__c> loadList = new List<BICC_Contact__c>{
			new BICC_Contact__c(
				Email__c = 'email@vodafone.com',
				Unify_Contact_Id__c = '1234',
				Unify_Account_Id__c = '99'
			)
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Contact__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testAccount() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(
			TestUtils.createSync('BICC_Account__c -> Account', 'DUNS_Number__c', 'DUNS_Number__c')
		);
		fsmlist.add(
			TestUtils.createSync('BICC_Account__c -> Account', 'COC_number__c', 'KVK_number__c')
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Account__c -> Account',
				'Unify_Account_Id__c',
				'Unify_Ref_Id__c'
			)
		);
		fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'Phone__c', 'Phone'));

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		acc = TestUtils.createAccount(admin);
		acc.DUNS_Number__c = '88';
		insert acc;
		acc = TestUtils.createAccount(admin);
		acc.KVK_number__c = '12345678';
		insert acc;

		//Data preparation for load table

		list<BICC_Account__c> loadList = new List<BICC_Account__c>{
			new BICC_Account__c(
				DUNS_Number__c = '88',
				Phone__c = '0653957801',
				Unify_Account_Id__c = '199'
			),
			new BICC_Account__c(
				COC_number__c = '12345678',
				Phone__c = '0653957801',
				Unify_Account_Id__c = '299'
			),
			new BICC_Account__c(Unify_Account_Id__c = '99', Phone__c = '0653957801'),
			new BICC_Account__c(
				DUNS_Number__c = '1',
				Phone__c = '0653957801',
				Unify_Account_Id__c = '399'
			),
			new BICC_Account__c(
				COC_number__c = '2',
				Phone__c = '0653957801',
				Unify_Account_Id__c = '499'
			),
			new BICC_Account__c(Unify_Account_Id__c = '3', Phone__c = '0653957801')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Account__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testDealerInformation() {
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Dealer_Information__c -> Dealer_Information__c',
				'Dealer_Contact_Email__c',
				'Dealer_Contact_Email__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Dealer_Information__c -> Dealer_Information__c',
				'Parent_Dealer_Code__c',
				'Parent_Dealer_Code__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Dealer_Information__c -> Dealer_Information__c',
				'Status__c',
				'Status__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Dealer_Information__c -> Dealer_Information__c',
				'Sales_Channel__c',
				'Sales_Channel__c'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Dealer_Information__c -> Dealer_Information__c',
				'Name',
				'Name'
			)
		);
		fsmlist.add(
			TestUtils.createSync(
				'BICC_Dealer_Information__c -> Dealer_Information__c',
				'Dealer_Code__c',
				'Dealer_Code__c'
			)
		);

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(admin);
		TestUtils.autoCommit = false;
		Contact con = TestUtils.createContact(acc);
		con.Email = 'Testclass@test.com';
		insert con;

		list<BICC_Dealer_Information__c> loadList = new List<BICC_Dealer_Information__c>{
			new BICC_Dealer_Information__c(
				Dealer_Contact_Email__c = 'Testclass@test.com',
				Status__c = 1,
				Name = 'Dealer1',
				Dealer_Code__c = '123456',
				Sales_Channel__c = 'Large'
			),
			new BICC_Dealer_Information__c(
				Dealer_Contact_Email__c = 'wrongemail@test.com',
				Status__c = 1,
				Name = 'Dealer2',
				Dealer_Code__c = '654321',
				Sales_Channel__c = 'Large'
			)
		};

		insert loadList;

		Test.startTest();
		BiccGenericBatch batch = new BiccGenericBatch('BICC_Dealer_Information__c');
		database.executebatch(batch);
		Test.stopTest();

		List<Dealer_Information__c> dealerInformation = [
			SELECT Dealer_Code__c
			FROM Dealer_Information__c
		];
		System.assertEquals(
			'123456',
			dealerInformation[0].Dealer_Code__c,
			'1 Dealer Information record should be created with the correct code'
		);
	}

	}