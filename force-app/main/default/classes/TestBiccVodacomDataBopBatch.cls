/**
 * @description			This is the test class that contains the tests for the BICC Vodacom Data Bop Batch
 * @author				Ferdinand Bondt
 */
@isTest
private class TestBiccVodacomDataBopBatch {

	static testMethod void myUnitTest() {

		//Data preparation
		TestUtils.autoCommit = false;

		map<String, String> mapping = new map<String, String>{'Bob_Code__c' => 'BOP_Code__c',
															  'InYear_Base__c' => 'AR_InYear_Base__c',
															  'InYear_Charges__c' => 'AR_InYear_Charges__c',
															  'Year_Month_Num__c' => 'AR_Year_Month_Num__c'};
		list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
		for (String field : mapping.keyset()) {
			fsmlist.add(TestUtils.createSync('BICC_Vodacom_Data_BOP__c -> Account_Revenue__c', field, mapping.get(field)));
		}
		insert fsmlist;

		Account acc = new Account(Name = 'Test Account', BOPCode__c = 'AB1'); //Match on Bop
		insert acc;

		Ban__c ban = new Ban__c(Name = '312312311', Account__c = acc.Id, BOPCode__c = 'AB1');
		insert ban;

		TestUtils.autoCommit = true;

		TestUtils.createBiccVodacomBop('AB1', '150', '150', '010114'); //Succesfull insert, match on Bop
		TestUtils.createBiccVodacomBop('ab1', '200', '200', '010114'); //No valid Bop
		TestUtils.createBiccVodacomBop('AB1', '150', '150', null); //No year month num
		TestUtils.createBiccVodacomBop('AB2', '200', '200', '010115'); //No matching Account

		//Test
		Test.startTest();

		PageReference pageRef = Page.BiccVodacomDataBopBatchExecute;
		Test.setCurrentPage(pageRef);
		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new list<BICC_Vodacom_Data_BOP__c>());
		BiccVodacomDataBopBatchExecuteController controller = new BiccVodacomDataBopBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		//Checks
		System.assertEquals([select AR_InYear_Base__c from Account_Revenue__c where BOP_Code__c =: 'AB1' limit 1].AR_InYear_Base__c, 150); //Succesful update
	}
}