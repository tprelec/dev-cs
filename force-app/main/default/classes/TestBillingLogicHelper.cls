/**
 * Test class for BillingLogicHelper.cls
 *
 * @author: Rahul Sharma
 */
@IsTest
public class TestBillingLogicHelper {

    // verify customer asset creation on order status change (New -> Submitted -> Accepted)
    @IsTest
    static void verifyBillingLogic() {

        // create test data
        Order__c order = createOrderWithContractedProducts(4);

        // start testing
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, 
        new TestUtilMultiRequestMock.SingleRequestMock(
                200,
                'Complete',
                'Error message',
                null
            )
        );

        TriggerHandler.resetAlreadyModified();

        // submit the order
        TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

        // for each assets a customer asset must be created
        verifyCustomerAssetCreation(new List<Id> { order.Id }, 4);
        verifyMainAssetCreation(new List<Id> { order.Id }, 4);
        // verify the billing and delivery statuses of the CPs
        verifyStatusesOfContractedProducts(new List<Id> { order.Id }, 4);

        // for submitted orders, add 2 more contracted products
        addContractedProducts(order.Id, 100, 2);

        // for each assets a customer asset must be created
        verifyCustomerAssetCreation(new List<Id> { order.Id }, 6);
        verifyMainAssetCreation(new List<Id> { order.Id }, 6);
        // verify the billing and delivery statuses of the CPs
        verifyStatusesOfContractedProducts(new List<Id> { order.Id }, 6);

        TriggerHandler.resetAlreadyModified();

        // submit the order
        TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_ACCEPTED);

        // for submitted orders, add 2 more contracted products
        addContractedProducts(order.Id, 100, 2);

        // customer assets should be created for new products
        verifyCustomerAssetCreation(new List<Id> { order.Id }, 8);
        verifyMainAssetCreation(new List<Id> { order.Id }, 8);
        // contracted products must not have statuses and linked to CPs
        verifyStatusesOfContractedProducts(new List<Id> { order.Id }, 8);

        //System.assertEquals(Limits.getLimitQueries(), Limits.getQueries());
        Test.stopTest();
    }

    public static Order__c createOrderWithContractedProducts(Integer totalContractedProducts) {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
        
        TestUtils.createOrderValidationOrder();
        TestUtils.createOrderValidationContractedProducts();
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Site__c site = TestUtils.createSite(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
        OrderType__c ot = TestUtils.createOrderType();
        System.assertNotEquals(null, ot.Id);

        TestUtils.autoCommit = false;
        Product2 product = TestUtils.createProduct(new Map<String, Object> {
            'Billing_Type__c' => Constants.PRODUCT2_BILLINGTYPE_STANDARD
        });
        insert product;
        System.assertNotEquals(null, product.Id);

        // create an order with new status
        Order__c order = new Order__c(
            Status__c = 'New',
            Propositions__c = 'Legacy',
            OrderType__c = ot.Id,
            VF_Contract__c = contr.Id,
            Number_of_items__c = 100,
            O2C_Order__c = true
        );
        insert order;
        System.assertNotEquals(null, order.Id);

        List<Order__c> orders = [SELECT Id, Status__c FROM Order__c LIMIT 2];
        System.assertEquals(1, orders.size());

        // add contracted products
        createContractedProducts(product.Id, contr.Id, site.Id, order.Id, 100, totalContractedProducts);

        // verify number of contracted products as per the filters in order trigger
        verifyContractedProducts(order.Id, 4);

        // verify customer assets at this point
        verifyCustomerAssetCreation(new List<Id> { order.Id }, 4);

        return order;
    }

    public static List<Contracted_Products__c> addContractedProducts(Id orderId,
        Integer value,
        Integer totalRecords) {

        List<Contracted_Products__c> contractedProductsToReturn = new List<Contracted_Products__c>();
        // assuming products were added initially
        List<Contracted_Products__c> contractedProducts = [SELECT Id, Product__c,
            VF_Contract__c, Site__c FROM Contracted_Products__c WHERE Order__c = :orderId LIMIT 1];
        if(!contractedProducts.isEmpty()) {
            contractedProductsToReturn = createContractedProducts(
                contractedProducts[0].Product__c,
                contractedProducts[0].VF_Contract__c,
                contractedProducts[0].Site__c,
                orderId,
                value,
                totalRecords);
        }

        // System.assertEquals(null, JSON.serialize(cpss));

        return contractedProductsToReturn;
    }

    // create multiple contracted products
    private static List<Contracted_Products__c> createContractedProducts(Id productId,
        Id contractId,
        Id siteId,
        Id orderId,
        Integer value,
        Integer totalRecords) {
        List<Contracted_Products__c> contractedProducts = new List<Contracted_Products__c>();
        // add contracted products
        for(Integer count = 0; count < totalRecords; count++) {
            contractedProducts.add(new Contracted_Products__c (
                Quantity__c = 1,
                Arpu_Value__c = value,
                Duration__c = 1,
                Product__c = productId,
                VF_Contract__c = contractId,
                Site__c = siteId,
                Order__c = orderId,
                CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ,
                ProductCode__c = 'C106929'
            ));
        }
        insert contractedProducts;

        for(Contracted_Products__c cp: contractedProducts) {
            System.assertNotEquals(null, cp.Id);
        }
        return contractedProducts;
    }

    public static void verifyContractedProducts(Id orderId, Integer totalRecords) {
        System.assertEquals(totalRecords, [SELECT COUNT()
            FROM Contracted_Products__c WHERE
            Order__c = :orderId AND
            CLC__c = :Constants.CONTRACTED_PRODUCT_CLC_ACQ AND
//            Customer_Asset__c = null AND
            Product__r.Billing_Type__c != :Constants.PRODUCT2_BILLINGTYPE_SPECIAL]);
    }

    public static void verifyStatusesOfContractedProducts(List<Id> orderIds, Integer totalRecords) {
        System.assertEquals(totalRecords, [SELECT COUNT()
            FROM Contracted_Products__c WHERE
            Order__c IN :orderIds AND
            // Delivery_Status__c = :Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_OPEN AND
            Billing_Status__c = :Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW AND
            Customer_Asset__c != null]);
    }

    // verify the customer asset record creation
    public static void verifyCustomerAssetCreation(List<Id> orderIds, Integer totalRecords) {
        System.assertEquals(totalRecords, [SELECT COUNT() FROM Customer_Asset__c WHERE Order__c IN :orderIds AND Account__c != null AND Site__c != null]);
    }

    // verify the customer asset record creation with type as normal
    public static void verifyMainAssetCreation(List<Id> orderIds, Integer totalRecords) {
        System.assertEquals(totalRecords, [SELECT COUNT() FROM Customer_Asset__c WHERE
            Type__c = :Constants.CUSTOMER_ASSET_TYPE_NORMAL AND Order__c IN :orderIds AND Account__c != null AND Site__c != null]);
    }

}