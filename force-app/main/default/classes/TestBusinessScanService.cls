@IsTest
public class TestBusinessScanService {
	@IsTest
	static void testBusinessScanCreate() {
		StaticResource sr = [
			SELECT Id, Body
			FROM StaticResource
			WHERE Name = 'BusinessScanTest'
			LIMIT 1
		];
		String jsonData = sr.Body.toString();

		Test.startTest();
		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/business-scan';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(jsonData);
		RestContext.response = new RestResponse();

		RestContext.request = request;
		BusinessScanService.create();
		Test.stopTest();

		Business_Scan__c scan = [
			SELECT Id, Business_Name__c, Chamber_of_Commerce__c
			FROM Business_Scan__c
			LIMIT 1
		];

		List<Business_Scan_Q_A__c> questions = [
			SELECT Id
			FROM Business_Scan_Q_A__c
			WHERE Business_Scan__c = :scan.Id
		];

		System.assertEquals(200, RestContext.response.statusCode, 'Response code must be 200');
		System.assertEquals(
			'Vodafone Europe B.V.',
			scan.Business_Name__c,
			'Business name does not match'
		);
		System.assertEquals('27166573', scan.Chamber_of_Commerce__c, 'KVK number does not match');
		System.assertEquals(20, questions.size(), 'Questions count does not match');
	}

	@IsTest
	static void testBusinessScanFail() {
		String jsonData = '{}';

		Test.startTest();
		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/business-scan';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(jsonData);
		RestContext.response = new RestResponse();

		RestContext.request = request;
		BusinessScanService.create();
		Test.stopTest();

		System.assertEquals(400, RestContext.response.statusCode, 'Response code must be 400');
	}
}