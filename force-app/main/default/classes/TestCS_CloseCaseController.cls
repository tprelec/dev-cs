@IsTest
private class TestCS_CloseCaseController {

    @IsTest
    static void testBehaviorNoError() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(simpleUser, false);
            vfContract.Contract_Cleaning_Result__c = 'Clean';
            vfContract.Responsible_Quality_Officer__c = simpleUser.Id;
            insert vfContract;

            Case c1 = CS_DataTest.createCase('FQC', 'CS MF Final Quality Check', simpleUser, false);
            c1.Status = 'Open';
            c1.Responsible_Quality_Officer__c = simpleUser.Id;
            c1.Contract_VF__c = vfContract.Id;
            c1.FQC_Result__c = 'Passed';
            c1.FQC_Quality_Score__c = 5;
            insert c1;

            CS_CloseCaseController.closeCase(c1.Id);

            List<Case> casesAfter = new List<Case>([
                SELECT Id, Status 
                FROM Case 
                WHERE Id = :c1.Id
            ]);

            System.assertEquals('Closed', casesAfter[0].Status);
        }
    }

    @IsTest
    static void testBehavior_OrderCleanup() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(simpleUser, false);
            vfContract.Contract_Cleaning_Result__c = 'Clean';
            vfContract.Responsible_Quality_Officer__c = simpleUser.Id;
            insert vfContract;

            Case c1 = CS_DataTest.createCase('FQC', 'CS MF Order Cleaning', simpleUser, false);
            c1.Status = 'Open';
            c1.Responsible_Quality_Officer__c = simpleUser.Id;
            c1.Contract_VF__c = vfContract.Id;
            c1.FQC_Result__c = 'Passed';
            c1.FQC_Quality_Score__c = 5;
            insert c1;

            CS_CloseCaseController.closeCase(c1.Id);

            List<Case> casesAfter = new List<Case>([
                SELECT Id, Status, Contract_Cleaning_Result__c 
                FROM Case 
                WHERE Id = :c1.Id
            ]);

            System.assertEquals('Closed', casesAfter[0].Status);
            System.assertEquals(vfContract.Contract_Cleaning_Result__c, casesAfter[0].Contract_Cleaning_Result__c);
        }
    }
}