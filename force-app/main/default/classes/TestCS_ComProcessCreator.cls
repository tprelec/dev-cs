@IsTest
private class TestCS_ComProcessCreator {
    @IsTest
    static void testProcessCreatorBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            List<VF_Contract__c> objects = new List<VF_Contract__c>();

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Mobile Flow', '5', true);
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);
            CSPOFA__Orchestration_Step_Template__c step2Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 2', '2', true);

            Integer numberOfContracts = 2;
            objects = CS_DataTest.createVfContracts(numberOfContracts, simpleUser);

            CS_ComProcessCreator cpc = new CS_ComProcessCreator(objects);
            cpc.run();

            List<CSPOFA__Orchestration_Process__c> orchProcess = new List<CSPOFA__Orchestration_Process__c>([
                SELECT Id,
                    Name,
                    Contract_VF__c
                FROM CSPOFA__Orchestration_Process__c
                WHERE Contract_VF__c IN :objects
            ]);

            System.assertEquals(numberOfContracts, orchProcess.size());
        }
    }
}