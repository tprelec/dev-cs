@IsTest
private class TestCS_CustomStepSendEmail {
    @IsTest
    static void testOrchestrationCustomMail() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            List<SObject> objects = new List<SObject>();

            EmailTemplate et = CS_DataTest.createEmailTemplate('Test Email Template', 'Test_Email_Template', 'Subject', '<div>test</div>', 'body', true);

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);
            CSPOFA__Orchestration_Step_Template__c step2Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 2', '2', true);

            VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(simpleUser, false);
            vfContract.Implementation_Manager__c = simpleUser.Id;
            insert vfContract;

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.Contract_VF__c = vfContract.Id;
            insert testProcess;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);
            objects.add(step1);

            CSPOFA__Orchestration_Step__c step2 = CS_DataTest.createOrchStep(step2Template.Id, testProcess.Id, false);
            step2.CS_Email_Template__c = 'Test Email Template';
            insert step2;
            objects.add(step2);

            CS_CustomStepSendEmail csCustomSendEmail = new CS_CustomStepSendEmail();
            List<CSPOFA__Orchestration_Step__c> result = csCustomSendEmail.process(objects);

            for (CSPOFA__Orchestration_Step__c step : result) {
                System.assert(step.CSPOFA__Status__c != 'Error', 'Process step ended with exceptions' + step.CSPOFA__Message__c);
            }
        }
    }

    @IsTest
    static void CS_CustomStepSendEmailTestWithSolution(){
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            List<SObject> objects = new List<SObject>();

            EmailTemplate et = CS_DataTest.createEmailTemplate('Test Email Template', 'Test_Email_Template', 'Subject', '<div>test</div>', 'body', true);

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);
            CSPOFA__Orchestration_Step_Template__c step2Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 2', '2', true);

            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            insert ord;

            Contact testContact = CS_DataTest.createContact('Contact Name', 'String lastName', 'not so importan role', 'mail@address.com', tmpAcc.Id);
            insert testContact;

            Site__c testSite = CS_DataTest.createSite('LEIDEN, Breestraat 112',tmpAcc, '1111AA', 'Breestraat','LEIDEN', 112);
            insert testSite;

            csord__Solution__c solution = CS_DataTest.createSolution('Solution 1', ord.Id, false);
            solution.Technical_Contact__c = testContact.Id;
            solution.Installation_Start__c = Datetime.now();
            solution.Installation_End__c = Datetime.now();
            solution.Products__c = 'Product A, Product B';
            solution.Site__c = testSite.Id;
            insert solution;

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.Solution__c = solution.Id;
            insert testProcess;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);
            objects.add(step1);

            CSPOFA__Orchestration_Step__c step2 = CS_DataTest.createOrchStep(step2Template.Id, testProcess.Id, false);
            step2.CS_Email_Template__c = 'Test Email Template';
            insert step2;
            objects.add(step2);

            CS_CustomStepSendEmail csCustomSendEmail = new CS_CustomStepSendEmail();
            List<CSPOFA__Orchestration_Step__c> result = csCustomSendEmail.process(objects);

            for (CSPOFA__Orchestration_Step__c step : result) {
                System.assert(step.CSPOFA__Status__c != 'Error', 'Process step ended with exceptions' + step.CSPOFA__Message__c);
            }
        }
    }
}