@IsTest
private class TestCS_EDMServiceSpecification {
    @IsTest
    static void testCoverWrapperStructure() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();
        
        System.runAs (simpleUser) {
            CS_EDMServiceSpecification serSpec = new CS_EDMServiceSpecification();
            serSpec.status = 'Status';
            serSpec.guid = '789-654312';
            serSpec.startDate = '233.08.2020';
            serSpec.name = 'Name';
            serSpec.description = 'description';
            serSpec.version = '1';
            serSpec.replacedSpecification = '';
            serSpec.identifier = '456987123';
            serSpec.endDate = '25.01.3000';
            serSpec.code = 'xCode';
            serSpec.instanceId = 'id_56';
            serSpec.productConfigurationId = '45678';

            List<CS_EDMServiceSpecification.SimpleAttribute> simpleAttributes = new List<CS_EDMServiceSpecification.SimpleAttribute>();
            CS_EDMServiceSpecification.SimpleAttribute sa = new CS_EDMServiceSpecification.SimpleAttribute('sa name','sa value');
            simpleAttributes.add(sa);

            serSpec.simpleAttributes = simpleAttributes;
            serSpec.additionalSimpleAttributes = simpleAttributes;
            serSpec.serviceId = '456892';

            Map<String,List<CS_EDMServiceSpecification.ComplexAttribute>> complexAttributesMap = new Map<String,List<CS_EDMServiceSpecification.ComplexAttribute>>();
            List<CS_EDMServiceSpecification.ComplexAttribute> complexAttributes = new List<CS_EDMServiceSpecification.ComplexAttribute>();
            CS_EDMServiceSpecification.ComplexAttribute comAtt = new CS_EDMServiceSpecification.ComplexAttribute();
            comAtt.productConfigurationId = '456789';
            comAtt.simpleAttributes = simpleAttributes;
            complexAttributes.add(comAtt);
            complexAttributesMap.put('key_2', complexAttributes);
            serSpec.complexAttributes = complexAttributesMap;

            Test.startTest();
            System.assertEquals('id_56', serSpec.instanceId);
            Test.stopTest();
        }
    }
}