@IsTest
public class TestCS_MFStatusUpdateLeadTime {
    public static final String CASE_STATUS_CLOSED = 'Closed';

    @IsTest
    private static void updateLeadTimePositive() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();
        VF_Contract__c vfContract;
        Test.startTest();

        System.runAs (simpleUser) {
            vfContract = createContract(simpleUser);
            List<Case> casesToInsert = new List<Case>();

            Case productIntakeAndPrepCase = CS_DataTest.createCase('Project Intake and Prep Case','CS MF Project Intake and Preparation', simpleUser, false);
            productIntakeAndPrepCase.Contract_VF__c = vfContract.Id;
            productIntakeAndPrepCase.Case_Start_Date__c = Date.today().addDays(-2);
            insert productIntakeAndPrepCase;

            Case finalQualityCheckCase = CS_DataTest.createCase('FQC Case','CS MF Final Quality Check', simpleUser, false);
            finalQualityCheckCase.Contract_VF__c = vfContract.Id;
            insert finalQualityCheckCase;
            // List<Id> caseList = new List<Id>();
            // caseList.add(finalQualityCheckCase.Id);
            // CS_MFStatusUpdateLeadTime.calculateLeadTime(caseList);
        }
        Test.stopTest();

        Decimal days = [SELECT LeadTime__c FROM VF_Contract__c WHERE Id = :vfContract.Id][0].LeadTime__c;
        System.assertEquals(0, days);
    }

    private static VF_Contract__c createContract(User runningUser) {
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;

        Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', runningUser.id);
        testOpp.RoleCreateOrChangeOwner__c = 'VGE Global Account Manager';
        insert testOpp;

        VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(runningUser, false);
        vfContract.Opportunity__c = testOpp.Id;
        vfContract.Contract_Cleaning_Result__c = 'Clean';
        insert vfContract;
        return vfContract;
    }
}