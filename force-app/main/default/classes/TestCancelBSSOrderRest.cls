@IsTest
public class TestCancelBSSOrderRest {
	static void setMock(String url, String statusCode, Integer responseCode) {
		Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
		endpoint2TestResp.put(
			url,
			new TestUtilMultiRequestMock.SingleRequestMock(
				responseCode,
				'Complete',
				'{"status": "' +
				statusCode +
				'"}',
				null
			)
		);
		HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);

		Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);
	}

	@IsTest
	private static void validateCancellation() {
		List<Id> contProdIds = createOrderWithContractedProducts(
			2,
			Constants.PRODUCT2_BILLINGTYPE_STANDARD
		);

		External_WebService_Config__c config = new External_WebService_Config__c(
			Name = CancelBSSOrderRest.INTEGRATION_SETTING_NAME,
			URL__c = CancelBSSOrderRest.MOCK_URL,
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;

		setMock(config.URL__c, 'OK', 200);

		Test.startTest();

		System.enqueueJob(new CancelBSSOrderRest(contProdIds));

		Test.stopTest();

		System.assertEquals(
			1,
			[SELECT COUNT() FROM Order_Billing_Transaction__c],
			'No Billing Transaction found'
		);
	}

	@IsTest
	private static void cancellationError() {
		List<Id> contProdIds = createOrderWithContractedProducts(
			2,
			Constants.PRODUCT2_BILLINGTYPE_STANDARD
		);

		External_WebService_Config__c config = new External_WebService_Config__c(
			Name = CancelBSSOrderRest.INTEGRATION_SETTING_NAME,
			URL__c = CancelBSSOrderRest.MOCK_URL,
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;

		setMock(config.URL__c, 'FAILED', 400);

		Test.startTest();

		System.enqueueJob(new CancelBSSOrderRest(contProdIds));

		Test.stopTest();

		System.assertEquals(
			1,
			[SELECT COUNT() FROM Order_Billing_Transaction__c],
			'No Billing Transaction found'
		);
	}

	static List<Id> createOrderWithContractedProducts(
		Integer totalContractedProducts,
		String billingType
	) {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();

		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct(
			new Map<String, Object>{ 'Billing_Type__c' => billingType }
		);
		insert product;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;

		List<Contracted_Products__c> contractedProducts = new List<Contracted_Products__c>();
		for (Integer count = 0; count < totalContractedProducts; count++) {
			contractedProducts.add(
				new Contracted_Products__c(
					Quantity__c = 1,
					Arpu_Value__c = 100,
					Duration__c = 1,
					Product__c = product.Id,
					VF_Contract__c = contr.Id,
					Site__c = site.Id,
					Order__c = order.Id,
					CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ,
					Gross_List_Price__c = 50,
					ProductCode__c = 'C106929',
					Start_Invoicing_Date__c = System.today().toStartOfMonth(),
					Row_Type__c = Constants.CONTRACTED_PRODUCT_ROW_TYPE_REMOVE,
					Billing_Order_Id__c = '12345'
				)
			);
		}
		insert contractedProducts;

		List<Id> contractedProductIds = new List<Id>();
		for (Contracted_Products__c cp : contractedProducts) {
			contractedProductIds.add(cp.Id);
		}

		return contractedProductIds;
	}
}