@isTest
private class TestCaseTriggerHandler {
	private static Account acc;
	private static Contact con;
	private static Opportunity opp;
	private static VF_Contract__c vfc;
	private static agf__ADM_Scrum_Team__c team;
	private static Case c;

	private static void createTestData() {
		acc = TestUtils.createAccount(GeneralUtils.currentUser);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		con = TestUtils.createContact(acc);
		vfc = TestUtils.createVFContract(acc, opp);
		team = TestUtils.createScrumTeams(1, true)[0];
		TestUtils.createThemes(1, true, GeneralUtils.currentUser.Id);

		insert new agf__ADM_Impact__c(Name = 'Malfunctioning');
		insert new agf__ADM_Frequency__c(Name = 'Always');
		insert new agf__ADM_Build__c(Name = 'Production');
		insert new agf__ADM_Sprint__c(
			agf__Start_Date__c = Date.today(),
			agf__End_Date__c = Date.today().addDays(1),
			agf__Scrum_Team__c = team.Id
		);

		List<agf__ADM_Product_Tag__c> productTags = new List<agf__ADM_Product_Tag__c>();

		productTags.add(new agf__ADM_Product_Tag__c(Name = 'Undefined', agf__Team__c = team.Id));
		productTags.add(new agf__ADM_Product_Tag__c(Name = 'Dev SFDC', agf__Team__c = team.Id));

		insert productTags;

		TestUtils.createSync('agf__ADM_Work__c -> Case', 'agf__Subject__c', 'Subject');
		TestUtils.createSync('Case -> agf__ADM_Work__c', 'Subject', 'agf__Subject__c');
		TestUtils.createSync('Case -> agf__ADM_Work__c', 'ContactId', 'Case_Contact__c');
	}

	@isTest
	static void testFillAccount() {
		createTestData();

		Test.startTest();

		c = new Case(Opportunity__c = opp.Id);
		insert c;

		c.Contract_VF__c = vfc.Id;
		update c;

		Test.stopTest();

		Case resultCase = [SELECT Account__c FROM Case WHERE Id = :c.Id];
		System.assertEquals(
			acc.Id,
			resultCase.Account__c,
			'Case Account should be the created account.'
		);
	}

	@isTest
	static void testFillContact() {
		createTestData();
		con.Userid__c = UserInfo.getUserId();
		update con;

		Test.startTest();

		c = new Case();
		insert c;

		Test.stopTest();

		Case resultCase = [SELECT ContactId FROM Case WHERE Id = :c.Id];
		System.assertEquals(
			con.Id,
			resultCase.ContactId,
			'Case Contact should be the created contact.'
		);
	}

	@isTest
	static void testCheckOpenChildCasesOnInsert() {
		createTestData();

		Case parentCase = new Case(Open_subcases__c = false);
		insert parentCase;

		Test.startTest();

		c = new Case(ParentId = parentCase.Id);
		insert c;

		Test.stopTest();

		Case pc = [SELECT Open_subcases__c FROM Case WHERE Id = :parentCase.Id];
		System.assertEquals(
			true,
			pc.Open_subcases__c,
			'Parent Case Open_subcases__c should be true.'
		);
	}

	@isTest
	static void testCheckOpenChildCasesOnDelete() {
		createTestData();

		Case parentCase = new Case(Open_subcases__c = true);
		insert parentCase;

		Test.startTest();

		c = new Case(ParentId = parentCase.Id);
		insert c;

		delete c;

		Test.stopTest();

		Case pc = [SELECT Open_subcases__c FROM Case WHERE Id = :parentCase.Id];
		System.assertEquals(
			false,
			pc.Open_subcases__c,
			'Parent Case Open_subcases__c should be false.'
		);
	}

	@isTest
	static void testCheckOpenChildCasesOnUpdate() {
		createTestData();

		Case parentCase = new Case(Open_subcases__c = true);
		insert parentCase;

		Test.startTest();

		c = new Case(ParentId = parentCase.Id, Status = 'New');
		insert c;

		c.Status = 'Closed';
		update c;

		Test.stopTest();

		Case pc = [SELECT Open_subcases__c FROM Case WHERE Id = :parentCase.Id];
		System.assertEquals(
			false,
			pc.Open_subcases__c,
			'Parent Case Open_subcases__c should be false.'
		);
	}

	@isTest
	static void testCheckOpenChildCasesOnUpdateUnclosed() {
		createTestData();

		Case parentCase = new Case(Open_subcases__c = false);
		insert parentCase;

		Test.startTest();

		c = new Case(ParentId = parentCase.Id, Status = 'Closed');
		insert c;

		c.Status = 'New';
		update c;

		Test.stopTest();

		Case pc = [SELECT Open_subcases__c FROM Case WHERE Id = :parentCase.Id];
		System.assertEquals(
			true,
			pc.Open_subcases__c,
			'Parent Case Open_subcases__c should be true.'
		);
	}

	@isTest
	static void testSyncToAgileAcceleratorCreateUserStory() {
		createTestData();

		Test.startTest();

		c = new Case(Sprint_Case__c = true, Subject = 'Test', Internal_comments__c = 'Test');
		insert c;

		Test.stopTest();

		List<agf__ADM_Work__c> resultWork = [SELECT Id FROM agf__ADM_Work__c];
		System.assertEquals(1, resultWork.size(), 'User story should be created.');
	}

	@isTest
	static void testSyncToAgileAcceleratorCreateBug() {
		createTestData();

		Test.startTest();

		c = new Case(Blocking_Issue__c = true, Subject = 'Test', Internal_comments__c = 'Test');
		insert c;

		Test.stopTest();

		List<agf__ADM_Work__c> resultWork = [SELECT Id FROM agf__ADM_Work__c];
		System.assertEquals(1, resultWork.size(), 'Bug should be created.');

		List<agf__ADM_Task__c> resultTasks = [SELECT Id FROM agf__ADM_Task__c];
		System.assertEquals(1, resultTasks.size(), 'Task should be created.');
	}

	@isTest
	static void testMobileFlowTasks() {
		createTestData();

		c = new Case(OwnerId = UserInfo.getUserId());
		insert c;

		Task t = new Task(WhatId = c.Id, Status = 'New');
		insert t;

		Test.startTest();

		User admin = TestUtils.createAdministrator();

		c.OwnerId = admin.Id;
		c.Mobile_Flow_Tasks_Created__c = false;
		update c;

		c.OwnerId = UserInfo.getUserId();
		c.Mobile_Flow_Tasks_Created__c = true;
		update c;

		Test.stopTest();

		Task resultTask = [SELECT OwnerId FROM Task WHERE Id = :t.Id];
		System.assertEquals(
			UserInfo.getUserId(),
			resultTask.OwnerId,
			'Task owner should be the case owner.'
		);
	}

	@isTest
	static void testUpdateRoleCreatorField() {
		createTestData();
		opp.RoleCreateOrChangeOwner__c = 'Commercial Industries Account Manager';
		update opp;

		Test.startTest();

		c = new Case(Contract_VF__c = vfc.Id);
		insert c;

		Test.stopTest();

		Case resultCase = [SELECT RoleCreatorChangeOwnerCreated__c FROM Case WHERE Id = :c.Id];
		System.assertEquals(
			'Enterprise & Hospitality / Service & Industry',
			resultCase.RoleCreatorChangeOwnerCreated__c,
			'Case RoleCreatorChangeOwnerCreated__c should be Enterprise & Hospitality / Service & Industry.'
		);
	}

	@isTest
	static void testSetFinalQualityCheckScore() {
		createTestData();

		Id fqcRecTypeId = [
			SELECT Id
			FROM RecordType
			WHERE SobjectType = 'Case' AND DeveloperName = 'CS_MF_Final_Quality_Check'
		][0]
		.Id;

		c = new Case(
			Contract_VF__c = vfc.Id,
			RecordTypeId = fqcRecTypeId,
			FQC_Quality_Score__c = 5,
			Responsible_Quality_Officer__c = UserInfo.getUserId(),
			FQC_Result__c = 'Passed'
		);
		insert c;

		Test.startTest();

		c.Status = 'Closed';
		update c;

		Test.stopTest();

		VF_Contract__c resultContract = [
			SELECT FQC_Quality_Score__c
			FROM VF_Contract__c
			WHERE Id = :vfc.Id
		];
		System.assertEquals(
			5,
			resultContract.FQC_Quality_Score__c,
			'Conract FQC_Quality_Score__c should be 5.'
		);
	}
}