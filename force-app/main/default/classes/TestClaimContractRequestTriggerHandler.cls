@isTest
public class TestClaimContractRequestTriggerHandler {
    private static Account acc;
    private static Opportunity opp;
    private static User claimer;
    private static VF_Contract__c c;
    private static Claimed_Contract_Approval_Request__c ccReq;

    private static void createTestData(
        String firstApproval,
        String secondApproval,
        String thirdApproval
    ) {
        acc = TestUtils.createAccount(GeneralUtils.currentUser);
        opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
        claimer = TestUtils.createDealer();
        GeneralUtils.userMap.put(claimer.Id, claimer);

        ccReq = new Claimed_Contract_Approval_Request__c(
            OwnerId = claimer.Id,
            Account__c = acc.Id,
            Old_Owner__c = GeneralUtils.currentUser.Id,
            First_Approval__c = firstApproval,
            Second_Approval__c = secondApproval,
            Third_Approval_Status__c = thirdApproval,
            Approver__c = GeneralUtils.currentUser.Id,
            Secondary_Approver__c = 'Sales Manager',
            Second_Approver__c = GeneralUtils.currentUser.Id,
            Third_Approval__c = 'Commercial Management'
        );
        insert ccReq;

        c = TestUtils.createVFContract(acc, opp);
        c.Claimed_Contract_Approval_Request__c = ccReq.Id;
        update c;

        Contact cont = new Contact(LastName = 'Test', AccountId = acc.Id, UserId__c = claimer.Id);
        insert cont;
        Dealer_Information__c di = new Dealer_Information__c(
            Dealer_Code__c = '000000',
            Contact__c = cont.Id
        );
        insert di;
    }

    @isTest
    static void testFirstApproval() {
        createTestData('Pending', 'Not Started', 'Not Started');

        Test.startTest();

        ccReq.First_Approval__c = 'Approved';
        update ccReq;

        Test.stopTest();

        Claimed_Contract_Approval_Request__c resultCcReq = [
            SELECT First_Approval__c
            FROM Claimed_Contract_Approval_Request__c
            WHERE Id = :ccReq.Id
        ];

        System.assertEquals(
            'Approved',
            resultCcReq.First_Approval__c,
            'First Approval status should be Approved.'
        );
    }

    @isTest
    static void testSecondApproval() {
        createTestData('Rejected', 'Pending', 'Not Started');

        Test.startTest();

        ccReq.Second_Approval__c = 'Approved';
        update ccReq;

        Test.stopTest();

        Claimed_Contract_Approval_Request__c resultCcReq = [
            SELECT Second_Approval__c
            FROM Claimed_Contract_Approval_Request__c
            WHERE Id = :ccReq.Id
        ];

        System.assertEquals(
            'Approved',
            resultCcReq.Second_Approval__c,
            'Second Approval status is not Approved.'
        );
    }

    @isTest
    static void testThirdApproval() {
        createTestData('Rejected', 'Rejected', 'Pending');

        Test.startTest();

        ccReq.Third_Approval_Status__c = 'Approved';
        update ccReq;

        Test.stopTest();

        Claimed_Contract_Approval_Request__c resultCcReq = [
            SELECT Third_Approval_Status__c
            FROM Claimed_Contract_Approval_Request__c
            WHERE Id = :ccReq.Id
        ];

        System.assertEquals(
            'Approved',
            resultCcReq.Third_Approval_Status__c,
            'Third Approval status should be Approved.'
        );
    }

    @isTest
    static void testRecallRequest() {
        createTestData('Pending', 'Not Started', 'Not Started');

        Test.startTest();

        ccReq.First_Approval__c = 'Approved';
        ccReq.Recall_Approval_Request__c = true;
        update ccReq;

        Test.stopTest();

        VF_Contract__c resultContract = [
            SELECT Claimed_Contract_Approval_Request__c
            FROM VF_Contract__c
            WHERE Id = :c.Id
        ];

        System.assertEquals(
            null,
            resultContract.Claimed_Contract_Approval_Request__c,
            'Contract\'s Claimed Contract Approval Request should be null.'
        );
    }

    @isTest
    static void testHandleRecall() {
        createTestData('Pending', 'Pending', 'Pending');

        Test.startTest();

        ccReq.Recall_Approval_Request__c = true;
        update ccReq;

        Test.stopTest();

        Claimed_Contract_Approval_Request__c resultCcReq = [
            SELECT First_Approval__c, Second_Approval__c, Third_Approval_Status__c
            FROM Claimed_Contract_Approval_Request__c
            WHERE Id = :ccReq.Id
        ];

        System.assertEquals(
            'Recalled',
            resultCcReq.First_Approval__c,
            'First Approval status should be Recalled.'
        );

        System.assertEquals(
            'Recalled',
            resultCcReq.Second_Approval__c,
            'Second Approval status should be Recalled.'
        );

        System.assertEquals(
            'Recalled',
            resultCcReq.Third_Approval_Status__c,
            'Third Approval status should be Recalled.'
        );
    }
}