/**
 * @description		This is the test class for ECSSOAPUser class. Because of a bug in mockservices, we have to cover this specifically
 * @author        	Guy Clairbois
 */
@isTest
private class TestContactExportSOAPData {

    static testMethod void dataTest() {
    	ECSSOAPUser u = new ECSSOAPUser();
    	ECSSOAPUser.authenticationHeader_element ah = new ECSSOAPUser.authenticationHeader_element(); 
    	ECSSOAPUser.companyRefType crt = new ECSSOAPUser.companyRefType();
    	ECSSOAPUser.createUsersRequest_element cur = new ECSSOAPUser.createUsersRequest_element();
    	ECSSOAPUser.updateUsersRequest_element uur = new ECSSOAPUser.updateUsersRequest_element();
    	ECSSOAPUser.userBaseType ubt = new ECSSOAPUser.userBaseType();
    	ECSSOAPUser.userCreateType uct = new ECSSOAPUser.userCreateType();
    	ECSSOAPUser.userSoap us = new ECSSOAPUser.userSoap();
    	ECSSOAPUser.usersResponse_element ure = new ECSSOAPUser.usersResponse_element();
    	ECSSOAPUser.userResponseType urt = new ECSSOAPUser.userResponseType();
    	ECSSOAPUser.userUpdateType uut = new ECSSOAPUser.userUpdateType();
 	
      	System.assert(true, 'dummy assertion');
    }
    
}