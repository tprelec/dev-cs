@isTest
private class TestContactRoleTriggerhandler {
	
	@isTest static void test_createUpdateContactRole() {
	    User owner = TestUtils.createAdministrator();
	    Account acct = TestUtils.createAccount(owner);
		Contact cont = TestUtils.createContact(acct);
		cont.BOP_Export_Datetime__c  = system.now();
		update cont;
		
		Contact_Role__c cr = new Contact_Role__c();
		cr.Account__c = acct.Id;
		cr.Contact__c = cont.Id;
		cr.Type__c = 'Main';
		cr.Active__c = true;
		insert cr;

		system.assertNotEquals(null,cr.Id);

		cr.Active__c = false;
		update cr;

		cr.Active__c = true;
		update cr;

	}
	
	@isTest static void test_deleteContactRole() {
	    User owner = TestUtils.createAdministrator();
	    Account acct = TestUtils.createAccount(owner);
		Contact cont = TestUtils.createContact(acct);
		cont.BOP_Export_Datetime__c  = system.now();
		update cont;
				
		Contact_Role__c cr = new Contact_Role__c();
		cr.Account__c = acct.Id;
		cr.Contact__c = cont.Id;
		cr.Type__c = 'Main';
		insert cr;

		try{
			delete cr;
		} catch(Exception e){
			// this is ok
		}

		cr.Active__c = false;
		update cr;

		delete cr;

		
	}
	
}