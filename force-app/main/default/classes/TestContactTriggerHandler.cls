/**
 * 	@description	This class contains unit tests for the ContactTriggerHandler class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestContactTriggerHandler {
	
	static testMethod void testCreateContact(){
	    User owner = TestUtils.createAdministrator();
	    Account acct = TestUtils.createAccount(owner);
	    Ban__c ban = TestUtils.createBan(acct);
	    //ban.BOPCode__c = 'TST';
	    ban.BOP_export_datetime__c = system.now();
	    update ban;
		Contact cont = TestUtils.createContact(acct);
		
		
      	System.assert(true, 'dummy assertion');
 	}	

	static testMethod void testUpdateContact(){
	    User owner = TestUtils.createAdministrator();
	    Account acct = TestUtils.createAccount(owner);
	    Ban__c ban = TestUtils.createBan(acct);
	    //ban.BOPCode__c = 'TST';
	    ban.BOP_export_datetime__c = system.now();
	    update ban;	    
		Contact cont = TestUtils.createContact(acct);
		
		Test.startTest();

		cont.BOP_export_datetime__c = system.now();
		cont.Email = 'test@test.nl';
		update cont;


		Test.stopTest();
		
      	System.assert(true, 'dummy assertion');
 	}	 	
}