@isTest
public class TestContentDocumentLinkTriggerHandler {
    @isTest
    static void testCreateCDL() {
        TestUtils.createCompleteOpportunity();

        Opportunity_Attachment__c oppAtt = new Opportunity_Attachment__c();
        oppAtt.Attachment_Type__c = 'Contract (Signed)';
        oppAtt.Opportunity__c = TestUtils.theOpportunity.Id;
        insert oppAtt;

        ContentVersion cv = new ContentVersion();
        cv.Title = 'New file 2';
        cv.PathOnClient = 'newfile.txt';
        cv.VersionData = Blob.valueOf('Hi!!!!');
        cv.FirstPublishLocationId = oppAtt.Id;

        Test.startTest();

        insert cv;

        Test.stopTest();

        ContentDocumentLink resultCDL = [
            SELECT Visibility, ContentDocumentId
            FROM ContentDocumentLink
            WHERE LinkedEntityId = :oppAtt.Id
        ];

        System.assertEquals(
            'AllUsers',
            resultCDL.Visibility,
            'Content Document Link visibility should be set to AllUsers.'
        );

        Opportunity_Attachment__c resultOppAtt = [
            SELECT ContentDocumentId__c
            FROM Opportunity_Attachment__c
            WHERE Id = :oppAtt.Id
        ];

        System.assertEquals(
            resultCDL.ContentDocumentId,
            resultOppAtt.ContentDocumentId__c,
            'Opportunity Attachment ContentDocumentId should be equal to Content Document Id.'
        );
    }
}