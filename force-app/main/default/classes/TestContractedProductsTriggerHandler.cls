@isTest
public class TestContractedProductsTriggerHandler {
	private static Account acc;
	private static Opportunity opp;
	private static VF_Contract__c vfc;
	private static OrderType__c ot;
	private static Product2 prod;
	private static Site__c site;
	private static Competitor_Asset__c ca;
	private static VF_Family_Tag__c ft;
	private static PBX_Type__c pbxt;
	private static Order__c o;
	private static Contracted_Products__c cp;

	private static void createTestData() {
		acc = TestUtils.createAccount(GeneralUtils.currentUser);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		vfc = TestUtils.createVFContract(acc, opp);
		ot = TestUtils.createOrderType();
		prod = TestUtils.createProduct();
		site = TestUtils.createSite(acc);

		ft = new VF_Family_Tag__c(Name = 'PBX_Trunking');
		insert ft;

		pbxt = new PBX_Type__c(Vendor__c = 'Onenet', Product_Name__c = 'Virtual PBX');
		insert pbxt;

		Id pabxRecordTypeId = [
			SELECT Id
			FROM RecordType
			WHERE SobjectType = 'Competitor_Asset__c' AND DeveloperName = 'PABX'
		][0]
		.Id;

		ca = new Competitor_Asset__c(
			Account__c = acc.Id,
			Site__c = site.Id,
			PBX_type__c = pbxt.Id,
			RecordTypeId = pabxRecordTypeId
		);
		insert ca;

		o = new Order__c(
			Account__c = acc.Id,
			VF_Contract__c = vfc.Id,
			OrderType__c = ot.Id,
			Status__c = 'Accepted',
			O2C_Order__c = true
		);
		insert o;

		TestUtils.createOrderValidationContractedProducts();

		OrderValidationField__c ovfo = new OrderValidationField__c(
			Name = 'Sales_Order_Id__c',
			Object__c = 'Order__c',
			Field__c = 'Sales_Order_Id__c'
		);
		insert ovfo;
	}

	@isTest
	static void testSetClc() {
		createTestData();

		Test.startTest();

		cp = new Contracted_Products__c(VF_Contract__c = vfc.Id, Product__c = prod.Id);
		insert cp;

		Test.stopTest();

		Contracted_Products__c resultCp = [
			SELECT CLC__c
			FROM Contracted_Products__c
			WHERE Id = :cp.Id
		];

		System.assertEquals('Acq', resultCp.CLC__c, 'CLC__c should be Acq.');
	}

	@isTest
	static void testNoProductClc() {
		createTestData();

		Test.startTest();

		cp = new Contracted_Products__c(VF_Contract__c = vfc.Id);

		try {
			insert cp;
		} catch (Exception e) {
			Boolean expectedExceptionThrown = (e.getMessage()
					.contains(
						'FIELD_CUSTOM_VALIDATION_EXCEPTION, Please specify a CLC value for this lineitem.: [CLC__c]'
					))
				? true
				: false;
			System.AssertEquals(true, expectedExceptionThrown, e.getMessage());
		}

		Test.stopTest();
	}

	@isTest
	static void testCopyPbxFromSite() {
		createTestData();

		prod.Family_Tag__c = ft.Id;
		update prod;

		site.PBX__c = ca.Id;
		update site;

		Test.startTest();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Product__c = prod.Id,
			Site__c = site.Id,
			Proposition__c = 'One Net'
		);
		insert cp;

		Test.stopTest();

		Contracted_Products__c resultCp = [
			SELECT PBX__c
			FROM Contracted_Products__c
			WHERE Id = :cp.Id
		];

		System.assertEquals(ca.Id, resultCp.PBX__c, 'PBX__c should be Competitor Asset Id.');
	}

	@isTest
	static void testEnrichDataOnInsert() {
		createTestData();

		prod.BAP_SAP__c = 10;
		prod.Multiplier_Vodacom__c = 1;
		prod.Family = 'Acq Voice';
		update prod;

		Contracted_Products__c otherCp = new Contracted_Products__c(
			Proposition__c = 'One Net',
			Family_Condition__c = 'Test Family Condition',
			VF_Contract__c = vfc.Id,
			CLC__c = 'Acq'
		);
		insert otherCp;

		Test.startTest();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Product__c = prod.Id,
			Gross_List_Price__c = 100,
			Discount__c = 5,
			Quantity__c = 20,
			Duration__c = 5
		);
		insert cp;

		Test.stopTest();

		Contracted_Products__c resultCp = [
			SELECT
				List_price__c,
				Net_Unit_Price__c,
				ARPU_Value__c,
				Fixed_Mobile__c,
				Proposition_Type__c,
				Multiplier_Vodacom__c,
				UnitPrice__c,
				Subtotal__c,
				Product_Family__c,
				Proposition__c,
				Family_Condition__c
			FROM Contracted_Products__c
			WHERE Id = :cp.Id
		];

		System.assertEquals(10, resultCp.List_price__c, 'List_price__c should be 10.');
		System.assertEquals(95, resultCp.Net_Unit_Price__c, 'Net_Unit_Price__c should be 95.');
		System.assertEquals(95, resultCp.ARPU_Value__c, 'ARPU_Value__c should be 95.');
		System.assertEquals('Fixed', resultCp.Fixed_Mobile__c, 'Fixed_Mobile__c should be Fixed.');
		System.assertEquals(
			'FIXED',
			resultCp.Proposition_Type__c,
			'Proposition_Type__c should be FIXED.'
		);
		System.assertEquals(
			1,
			resultCp.Multiplier_Vodacom__c,
			'Multiplier_Vodacom__c should be 1.'
		);
		System.assertEquals(50, resultCp.UnitPrice__c, 'UnitPrice__c should be 50.');
		System.assertEquals(9500, resultCp.Subtotal__c, 'Subtotal__c should be 9500.');
		System.assertEquals(
			'Acq Voice',
			resultCp.Product_Family__c,
			'Product_Family__c should be Acq Voice.'
		);
		System.assertEquals(
			'One Net',
			resultCp.Proposition__c,
			'Proposition__c should be One Net.'
		);
		System.assertEquals(
			'Test Family Condition',
			resultCp.Family_Condition__c,
			'Family_Condition__c should be Test Family Condition.'
		);
	}

	@isTest
	static void testEnrichDataOnUpdate() {
		createTestData();

		prod.BAP_SAP__c = 8;
		prod.Multiplier_Vodacom__c = 0.5;
		prod.Family = 'Acq Voice';
		update prod;

		Product2 prod2 = TestUtils.createProduct();
		prod2.BAP_SAP__c = 10;
		prod2.Multiplier_Vodacom__c = 1;
		prod2.Family = 'Acq Voice';
		update prod2;

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Product__c = prod.Id,
			Gross_List_Price__c = 95,
			Discount__c = 4,
			Quantity__c = 10,
			Duration__c = 3
		);
		insert cp;

		Test.startTest();

		cp.Product__c = prod2.Id;
		cp.Gross_List_Price__c = 100;
		update cp;

		cp.Discount__c = 5;
		update cp;

		cp.Quantity__c = 20;
		update cp;

		cp.Duration__c = 5;
		update cp;

		Test.stopTest();

		Contracted_Products__c resultCp = [
			SELECT
				List_price__c,
				Net_Unit_Price__c,
				ARPU_Value__c,
				Multiplier_Vodacom__c,
				UnitPrice__c,
				Subtotal__c,
				Product_Family__c
			FROM Contracted_Products__c
			WHERE Id = :cp.Id
		];

		System.assertEquals(10, resultCp.List_price__c, 'List_price__c should be 10.');
		System.assertEquals(95, resultCp.Net_Unit_Price__c, 'Net_Unit_Price__c should be 95.');
		System.assertEquals(95, resultCp.ARPU_Value__c, 'ARPU_Value__c should be 95.');
		System.assertEquals(
			1,
			resultCp.Multiplier_Vodacom__c,
			'Multiplier_Vodacom__c should be 1.'
		);
		System.assertEquals(50, resultCp.UnitPrice__c, 'UnitPrice__c should be 50.');
		System.assertEquals(9500, resultCp.Subtotal__c, 'Subtotal__c should be 9500.');
		System.assertEquals(
			'Acq Voice',
			resultCp.Product_Family__c,
			'Product_Family__c should be Acq Voice.'
		);
	}

	@isTest
	static void testCalculateNumberOfSitesInContract() {
		createTestData();

		VF_Contract__c vfc2 = TestUtils.createVFContract(acc, opp);

		Test.startTest();

		cp = new Contracted_Products__c(VF_Contract__c = vfc.Id, CLC__c = 'Acq');
		insert cp;

		cp.VF_Contract__c = vfc2.Id;
		cp.Site__c = site.Id;
		update cp;

		Test.stopTest();

		VF_Contract__c resultVfc = [
			SELECT Number_of_sites_in_contract__c
			FROM VF_Contract__c
			WHERE Id = :vfc2.Id
		];

		System.assertEquals(
			1,
			resultVfc.Number_of_sites_in_contract__c,
			'Number_of_sites_in_contract__c should be 1.'
		);
	}

	@isTest
	static void testRollupFamilyConditions() {
		TriggerFlagsService.turnOff('ContractedProductsTriggerHandler', 'rollupFamilyConditions');

		createTestData();

		Test.startTest();

		cp = new Contracted_Products__c(VF_Contract__c = vfc.Id, Order__c = o.Id, CLC__c = 'Acq');
		insert cp;

		TriggerFlagsService.turnOn('ContractedProductsTriggerHandler', 'rollupFamilyConditions');

		cp.Family_Condition__c = 'Test Family Condition';
		update cp;

		Test.stopTest();

		VF_Contract__c resultVfc = [
			SELECT Family_Conditions__c
			FROM VF_Contract__c
			WHERE Id = :vfc.Id
		];

		Order__c resultOrder = [SELECT Family_Conditions__c FROM Order__c WHERE Id = :o.Id];

		System.assertEquals(
			'Test Family Condition;',
			resultVfc.Family_Conditions__c,
			'Family_Conditions__c should be Test Family Condition;.'
		);

		System.assertEquals(
			'Test Family Condition;',
			resultOrder.Family_Conditions__c,
			'Family_Conditions__c should be Test Family Condition;.'
		);
	}

	@isTest
	static void testTriggerChangePabx() {
		createTestData();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Delivery_Status__c = 'New',
			CLC__c = 'Acq'
		);
		insert cp;

		TriggerHandler.resetAlreadyModified();

		Test.startTest();

		cp.Delivery_Status__c = 'Approved';
		cp.PBX__c = ca.Id;
		cp.ProductCode__c = 'C123456';
		update cp;

		Test.stopTest();

		Contracted_Products__c resultCp = [
			SELECT Delivery_Status__c
			FROM Contracted_Products__c
			WHERE Id = :cp.Id
		];

		System.assertEquals(
			'Approved',
			resultCp.Delivery_Status__c,
			'Delivery_Status__c should be Approved.'
		);
	}

	@isTest
	static void testTriggerEcsDelivery() {
		TriggerFlagsService.turnOff('ContractedProductsTriggerHandler', 'triggerEcsDelivery');

		createTestData();

		Test.startTest();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Billing_Status__c = 'New',
			Delivery_Status__c = 'New',
			Order__c = o.Id,
			CLC__c = 'Acq'
		);
		insert cp;

		TriggerFlagsService.turnOn('ContractedProductsTriggerHandler', 'triggerEcsDelivery');

		o.Status__c = 'Accepted';
		update o;

		cp.ProductCode__c = 'C123456';
		update cp;

		cp.Billing_Status__c = 'Processed';
		cp.Delivery_Status__c = 'Approved';
		update cp;

		Test.stopTest();

		Order__c resultOrder = [SELECT BOP_Delivery_Status__c FROM Order__c WHERE Id = :o.Id];

		System.assertEquals(
			'Refused',
			resultOrder.BOP_Delivery_Status__c,
			'BOP_Delivery_Status__c should be Refused.'
		);
	}

	@isTest
	static void testUpdateOrderReadyFlag() {
		createTestData();

		Test.startTest();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Order__c = o.Id,
			Billing_Status__c = 'New',
			CLC__c = 'Acq'
		);
		insert cp;

		cp.Billing_Status__c = 'Processed';
		update cp;

		Test.stopTest();

		Order__c resultOrder = [SELECT ContractedproductsReady__c FROM Order__c WHERE Id = :o.Id];

		System.assertEquals(
			true,
			resultOrder.ContractedproductsReady__c,
			'ContractedproductsReady__c should be true.'
		);
	}

	@isTest
	static void testExistingCustAssetCheck() {
		createTestData();

		Customer_Asset__c custAsset = new Customer_Asset__c(
			Site__c = site.Id,
			Article_Code__c = 'abc',
			Price__c = 10,
			Discount__c = 10,
			Installation_Status__c = 'Active'
		);
		insert custAsset;

		Test.startTest();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Order__c = o.Id,
			Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW,
			CLC__c = 'Acq',
			Site__c = site.Id,
			ProductCode__c = 'abc',
			Gross_List_Price__c = 10,
			Discount__c = 10,
			Row_Type__c = Constants.CONTRACTED_PRODUCT_ROW_TYPE_REMOVE,
			Customer_Asset__c = custAsset.Id
		);
		insert cp;

		cp.Start_Invoicing_Date__c = System.today().addDays(-2);
		cp.Delivery_Status__c = 'Approved';
		update cp;

		Test.stopTest();

		Customer_Asset__c caResult = [
			SELECT Id, Billing_Status__c
			FROM Customer_Asset__c
			WHERE Id = :custAsset.Id
		];

		System.assertEquals(
			'Pending Change',
			caResult.Billing_Status__c,
			'Billing Status should be Pending Change.'
		);

		Contracted_Products__c cpResult = [
			SELECT Id, Credit__c
			FROM Contracted_Products__c
			WHERE Id = :cp.Id
		];

		System.assertEquals(true, cpResult.Credit__c, 'Credit flag should be TRUE.');
	}

	@isTest
	static void testOcChargeCreation() {
		createTestData();

		Customer_Asset__c custAsset = new Customer_Asset__c(
			Site__c = site.Id,
			Article_Code__c = 'abc',
			Price__c = 10,
			Discount__c = 10,
			Installation_Status__c = 'Active'
		);
		insert custAsset;

		Test.startTest();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Order__c = o.Id,
			Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW,
			CLC__c = 'Acq',
			Site__c = site.Id,
			ProductCode__c = 'abc',
			Gross_List_Price__c = 10,
			Discount__c = 10,
			Row_Type__c = Constants.CONTRACTED_PRODUCT_ROW_TYPE_ADD,
			Customer_Asset__c = custAsset.Id
		);
		insert cp;

		cp.Start_Invoicing_Date__c = System.today().addDays(-2);
		cp.Delivery_Status__c = 'Approved';
		update cp;

		Test.stopTest();

		Customer_Asset__c caResult = [
			SELECT Id, Billing_Status__c
			FROM Customer_Asset__c
			WHERE Id != :custAsset.Id
		];

		System.assertNotEquals(null, caResult, 'No OC Charge Found');
	}

	@isTest
	static void testupdateCustomerAssets() {
		createTestData();

		Customer_Asset__c custAsset = new Customer_Asset__c(
			Site__c = site.Id,
			Article_Code__c = 'abc',
			Price__c = 10,
			Discount__c = 10,
			Installation_Status__c = 'Active'
		);
		insert custAsset;

		Test.startTest();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Order__c = o.Id,
			Billing_Status__c = 'New',
			CLC__c = 'Acq',
			Site__c = site.Id,
			ProductCode__c = 'abc',
			Gross_List_Price__c = 10,
			Discount__c = 10,
			Row_Type__c = Constants.CONTRACTED_PRODUCT_ROW_TYPE_REMOVE,
			Customer_Asset__c = custAsset.Id
		);
		insert cp;

		custAsset.Billing_Status__c = Constants.CUSTOMER_ASSET_PENDING_CHANGE;
		update custAsset;

		cp.Start_Invoicing_Date__c = System.today().addDays(-2);
		cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_CANCELLED;
		update cp;

		Test.stopTest();

		Customer_Asset__c caResult = [
			SELECT Id, Billing_Status__c
			FROM Customer_Asset__c
			WHERE Id = :custAsset.Id
		];

		System.assertEquals(
			'Active',
			caResult.Billing_Status__c,
			'Billing Status should be Pending Change.'
		);
	}

	@isTest
	static void testupdateCustomerAssetsOne() {
		createTestData();

		Customer_Asset__c custAsset = new Customer_Asset__c(
			Site__c = site.Id,
			Article_Code__c = 'abc',
			Price__c = 10,
			Discount__c = 10,
			Installation_Status__c = 'Active'
		);
		insert custAsset;

		Test.startTest();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Order__c = o.Id,
			Billing_Status__c = 'New',
			CLC__c = 'Acq',
			Site__c = site.Id,
			ProductCode__c = 'abc',
			Gross_List_Price__c = 10,
			Discount__c = 10,
			Row_Type__c = Constants.CONTRACTED_PRODUCT_ROW_TYPE_REMOVE,
			Customer_Asset__c = custAsset.Id
		);
		insert cp;

		custAsset.Billing_Status__c = Constants.CUSTOMER_ASSET_PENDING_CHANGE;
		custAsset.Installation_Status__c = Constants.CUSTOMER_ASSET_PENDING_CHANGE;
		update custAsset;

		cp.Start_Invoicing_Date__c = System.today().addDays(-2);
		cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_CANCELLED;
		update cp;

		Test.stopTest();

		Customer_Asset__c caResult = [
			SELECT Id, Billing_Status__c, Installation_Status__c
			FROM Customer_Asset__c
			WHERE Id = :custAsset.Id
		];

		System.assertEquals(
			'Active',
			caResult.Billing_Status__c,
			'Billing Status should be Pending Change.'
		);
		System.assertEquals(
			'Active',
			caResult.Installation_Status__c,
			'Installation_Status__c should be Pending Change.'
		);
	}

	@isTest
	static void testupdateCustomerAssetsTwo() {
		createTestData();

		Customer_Asset__c custAsset = new Customer_Asset__c(
			Site__c = site.Id,
			Article_Code__c = 'abc',
			Price__c = 10,
			Discount__c = 10,
			Billing_Status__c = 'Active'
		);
		insert custAsset;

		Test.startTest();

		cp = new Contracted_Products__c(
			VF_Contract__c = vfc.Id,
			Order__c = o.Id,
			Billing_Status__c = 'New',
			CLC__c = 'Acq',
			Site__c = site.Id,
			ProductCode__c = 'abc',
			Gross_List_Price__c = 10,
			Discount__c = 10,
			Row_Type__c = Constants.CONTRACTED_PRODUCT_ROW_TYPE_REMOVE,
			Customer_Asset__c = custAsset.Id
		);
		insert cp;

		custAsset.Installation_Status__c = Constants.CUSTOMER_ASSET_PENDING_CHANGE;
		update custAsset;

		cp.Start_Invoicing_Date__c = System.today().addDays(-2);
		cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_CANCELLED;
		update cp;

		Test.stopTest();

		Customer_Asset__c caResult = [
			SELECT Id, Billing_Status__c, Installation_Status__c
			FROM Customer_Asset__c
			WHERE Id = :custAsset.Id
		];

		System.assertEquals(
			'Active',
			caResult.Installation_Status__c,
			'Installation_Status__c should be Pending Change.'
		);
	}
}