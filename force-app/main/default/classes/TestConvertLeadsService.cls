@IsTest
public with sharing class TestConvertLeadsService {
	@TestSetup
	static void makeData() {
		TestUtils.autoCommit = false;
		Lead l = TestUtils.createLead();
		l.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName('Lead', 'CSADetails');
		l.KVK_number__c = '12345678';
		l.Street__c = 'Leistraat';
		l.Visiting_Housenumber1__c = 12;
		l.Visiting_postalcode_merged_input__c = 'RE3572';
		l.Town__c = 'Utrecht';
		insert l;

		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.KVK_number__c = '87654321';
		insert acc;

		Contact con = TestUtils.createContact(acc);
		con.Email = 'test@test.com';
		insert con;
	}

	@IsTest
	static void testLeadConvert() {
		Lead l = getLead();

		Test.startTest();
		ConvertLeadsService cls = new ConvertLeadsService(new List<Lead>{ l });
		cls.convertLeads();

		Test.stopTest();

		l = getLead();
		System.assert(l.IsConverted, 'Lead should be converted');

		Account acc = getAccount(l.KVK_number__c);
		System.assertEquals(
			l.ConvertedAccountId,
			acc.Id,
			'Lead should be converted to the Account with same KVK Number'
		);
	}

	@IsTest
	static void testZiggoLeadConvert() {
		Lead l = getLead();
		l.KVK_number__c = '87654321';
		l.Email = 'test@test.com';
		update l;

		Test.startTest();
		ConvertLeadAction.convertLeads(new List<Lead>{ l });

		Test.stopTest();

		l = getLead();
		System.assert(l.IsConverted, 'Lead should be converted');

		Account acc = getAccount(l.KVK_number__c);
		System.assertEquals(
			l.ConvertedAccountId,
			acc.Id,
			'Lead should be converted to the Account with same KVK Number'
		);

		Contact con = getContact(acc.Id, l.Email);
		System.assertEquals(
			l.ConvertedContactId,
			con.Id,
			'Lead should be converted to the Account with same Email'
		);

		Site__c site = getSite(acc.Id, l.Visiting_postalcode_merged_input__c);
		System.assertEquals(
			l.Visiting_postalcode_merged_input__c,
			site.Site_Postal_Code__c,
			'Lead should be converted to the Site with same Postcode'
		);

		Opportunity opp = getOpportunity(l.ConvertedOpportunityId);
		System.assertEquals(opp.LG_PrimaryContact__c, con.Id, 'Opportunity should be created');
		System.assertEquals(
			getAttachment(l.ConvertedOpportunityId).Name,
			'OrderEntryData.json',
			'Attachment should be created'
		);

		OpportunityContactRole contactRole = getOpportunityContactRole(opp.Id);
		System.assertEquals(
			contactRole.Role,
			'Administrative Contact',
			'Opportunity Contact Role should exist and its role should be Administrative Contact'
		);

		Task task = getTask(opp.Id);
		System.assertEquals(
			task.Subject,
			'D2D Visit',
			'Task should be created and its subject should be D2D Visit'
		);
	}

	static Lead getLead() {
		return [
			SELECT
				Id,
				RecordTypeId,
				Email,
				KVK_Number__c,
				Street__c,
				Visiting_Housenumber1__c,
				House_Number_Suffix__c,
				Visiting_postalcode_merged_input__c,
				Town__c,
				Visiting_Country__c,
				LG_InternetCustomer__c,
				IsConverted,
				ConvertedAccountId,
				ConvertedContactId,
				ConvertedOpportunityId
			FROM Lead
			LIMIT 1
		];
	}

	static Account getAccount(String kvkNumber) {
		return [SELECT Id, KVK_Number__c FROM Account WHERE KVK_Number__c = :kvkNumber];
	}

	static Contact getContact(Id accountId, String email) {
		return [SELECT Id FROM Contact WHERE AccountId = :accountId AND Email = :email];
	}

	static Site__c getSite(Id accountId, String postcode) {
		return [
			SELECT Id, Site_Postal_Code__c
			FROM Site__c
			WHERE Site_Account__c = :accountId AND Site_Postal_Code__c = :postcode
		];
	}

	static Opportunity getOpportunity(Id oppId) {
		return [SELECT Id, LG_PrimaryContact__c FROM Opportunity WHERE Id = :oppId];
	}

	static Attachment getAttachment(Id oppId) {
		return [
			SELECT Id, ParentId, Name
			FROM Attachment
			WHERE ParentId = :oppId AND Name = 'OrderEntryData.json'
		];
	}

	static OpportunityContactRole getOpportunityContactRole(Id oppId) {
		return [
			SELECT Id, OpportunityId, ContactId, Role
			FROM OpportunityContactRole
			WHERE OpportunityId = :oppId
		];
	}

	static Task getTask(Id whatId) {
		return [SELECT Id, What.Id, Subject FROM Task WHERE What.Id = :whatId];
	}
}