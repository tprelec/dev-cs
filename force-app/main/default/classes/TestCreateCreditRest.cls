@IsTest
public class TestCreateCreditRest {
	private static void setupMock(Integer statusCode, String body) {
		Test.setMock(
			HttpCalloutMock.class,
			new TestUtilMultiRequestMock.SingleRequestMock(statusCode, 'Complete', body, null)
		);
	}

	@IsTest
	private static void validatePositiveStatusCode() {
		Credit_Note__c credNote = prepareTestData();

		setAuthenticationInSetting();

		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeStringField('status', 'OK');
		generator.writeEndObject();
		String resp = generator.getAsString();

		setupMock(200, resp);

		Test.startTest();

		credNote.Status__c = 'Advice';
		credNote.Approved__c = true;
		update credNote;

		Test.stopTest();

		Credit_Line__C credLine = [SELECT Id, Status__c FROM Credit_Line__c LIMIT 1];

		System.assertEquals(
			'Billing Requested',
			credLine.Status__c,
			'Incorrect Credit Line Status'
		);
	}

	@IsTest
	private static void validateNegativeStatusCode() {
		Credit_Note__c credNote = prepareTestData();

		setAuthenticationInSetting();

		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeFieldName('errorInfo');
		generator.writeStartObject();
		generator.writeStringField('errorCode', 'SIASERR-1500');
		generator.writeStringField('errorDescription', 'Error received from AR Create Credit');
		generator.writeStringField('targetSystemName', 'AR');
		generator.writeStringField('targetServiceName', 'createCredit');
		generator.writeEndObject();
		generator.writeEndObject();
		String resp = generator.getAsString();

		setupMock(400, resp);

		Test.startTest();

		credNote.Status__c = 'Advice';
		credNote.Approved__c = true;
		update credNote;

		Test.stopTest();

		Credit_Line__C credLine = [SELECT Id, Status__c FROM Credit_Line__c LIMIT 1];

		System.assertEquals('Billing Error', credLine.Status__c, 'Incorrect Credit Line Status');
	}

	private static Credit_Note__c prepareTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);

		TestUtils.createCompleteContract();
		TestUtils.createContact(TestUtils.theAccount);
		TestUtils.createFinancialAccount(TestUtils.theBan, TestUtils.theContact.Id);
		TestUtils.createBillingArrangement(TestUtils.theFA);
		Account partAcc = TestUtils.createPartnerAccount();
		insert partAcc;

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'OneMobile';
		ord.OrderType__c = TestUtils.theOrderType.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = TestUtils.theContract.Id;
		ord.O2C_Order__c = true;
		ord.Sales_Order_Id__c = '4444444';
		ord.Account__c = TestUtils.theAccount.Id;
		insert ord;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ;
		cp.VF_Contract__c = TestUtils.theContract.Id;
		cp.Billing_Status__c = 'New';
		cp.Product__c = TestUtils.theProduct.Id;
		cp.ProductCode__c = 'C106929';
		cp.Site__c = TestUtils.theSite.Id;
		cp.Credit__c = true;
		cp.Row_Type__c = 'Price Change';
		cp.Net_Unit_Price__c = 10;
		cp.Quantity__c = 1;
		cp.Start_Invoicing_Date__c = System.today();
		cp.Activation_Date__c = System.today().addDays(3);
		insert cp;

		Map<String, Schema.RecordTypeInfo> cnRecordTypes = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByDeveloperName();
		// Account partnerAccount = [SELECT Id FROM Account WHERE Name = 'testPartner123' LIMIT 1];
		// System.debug('partnerAccount: ' + partnerAccount);
		Credit_Note__c cn = new Credit_Note__c();
		cn.Order__c = ord.Id;
		cn.Credit_Note_Id__c = ord.Name + '-' + String.valueOf(System.today().month());
		cn.Account__c = ord.Account__c;
		cn.Partner__c = partAcc.Id;
		cn.RecordTypeId = cnRecordTypes.get('Credit_Note_Indirect').getRecordTypeId();
		cn.Subscription__c = 'Fixed';
		cn.Credit_Amount__c = 0;
		cn.Credit_Type__c = 'Contractual';
		insert cn;

		Customer_Asset__c ca = [SELECT Id FROM Customer_Asset__c LIMIT 1];
		ca.Billing_Arrangement__c = TestUtils.theBAR.Id;
		update ca;

		Credit_Line__c cl = new Credit_Line__c();
		cl.Description__c = 'Test';
		cl.Calculated_Amount__c = 10;
		cl.Customer_Asset__c = ca.Id;
		cl.Credit_Note__c = cn.Id;
		insert cl;

		Credit_Line__c cl2 = new Credit_Line__c();
		cl2.Description__c = 'Test2';
		cl2.Calculated_Amount__c = 10;
		cl2.Customer_Asset__c = ca.Id;
		cl2.Credit_Note__c = cn.Id;
		insert cl2;

		return cn;
	}

	private static void setAuthenticationInSetting() {
		External_WebService_Config__c config = new External_WebService_Config__c(
			Name = CreateCreditRest.INTEGRATION_SETTING_NAME,
			URL__c = OrderPABXRest.MOCK_URL,
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;
	}
}