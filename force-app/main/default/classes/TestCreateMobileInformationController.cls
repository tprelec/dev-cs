@isTest
public inherited sharing class TestCreateMobileInformationController {
	@TestSetup
	static void setupOppAndBasket() {
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ProductBasketTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
		basket.cscfga__Basket_Status__c = 'Contract created';
		update basket;

		Opportunity theOpp = [SELECT Id FROM Opportunity];
		List<PricebookEntry> entries = [SELECT Id, UnitPrice FROM PricebookEntry];

		TestUtils.autoCommit = false;
		OpportunityLineItem lineItem = TestUtils.createOpportunityLineItem(theOpp, entries[0]);
		lineItem.Proposition_Component__c = 'Mobile';
		insert lineItem;
	}

	@isTest
	public static void testBasketContractIsCreated() {
		Opportunity opp = [SELECT Id FROM Opportunity];

		Test.startTest();
		Boolean contractIsCreated = CreateMobileInformationController.basketContractIsCreated(
			opp.Id
		);
		Test.stopTest();

		System.assertEquals(
			true,
			contractIsCreated,
			'The Controller should validate that there is a Basket with status Contract created'
		);
	}

	@isTest
	static void testGetOpportunity() {
		Opportunity opp = [
			SELECT
				Account.Name,
				Owner.Name,
				Main_Contact_Person__r.Name,
				DS_Authorized_to_sign_1st__r.Name,
				Account.BAN_Number__c,
				Ban__r.Ban_Number__c
			FROM Opportunity
		];

		Test.startTest();
		Opportunity oppFound = CreateMobileInformationController.getOpportunity(opp.Id);
		Test.stopTest();

		System.assertEquals(opp, oppFound, 'The Controller should return the correct Opportunity');
	}

	@isTest
	public static void testInitComponent() {
		Opportunity opp = [SELECT Id FROM Opportunity];
		cscfga__Product_Basket__c basket = [
			SELECT Id, cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
		];
		basket.cscfga__Basket_Status__c = 'Approved';
		update basket;

		Test.startTest();
		List<NetProfit_CTN__c> initCtns = CreateMobileInformationController.getNetProfitCtn(
			opp.Id,
			'Quote_Profile__c',
			'asc'
		);
		Boolean npInfoInserted = CreateMobileInformationController.insertNetProfitCtn(
			initCtns,
			opp.Id
		);
		CreateMobileInformationController.getQuoteProfiles(opp.Id);
		Test.stopTest();

		System.assertEquals(
			3,
			initCtns.size(),
			'The controller should have created 3 NetProfit_CTN__c records. Records created : ' +
			initCtns.size()
		);
		System.assertEquals(
			true,
			npInfoInserted,
			'The controller should have inserted a NetProfit_Information__c record for us'
		);
	}

	@isTest
	public static void testSave() {
		Opportunity opp = [SELECT Id FROM Opportunity];
		cscfga__Product_Basket__c basket = [
			SELECT Id, cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
		];
		basket.cscfga__Basket_Status__c = 'Approved';
		update basket;
		List<NetProfit_CTN__c> initCtns = CreateMobileInformationController.getNetProfitCtn(
			opp.Id,
			'Quote_Profile__c',
			'asc'
		);
		CreateMobileInformationController.insertNetProfitCtn(initCtns, opp.Id);

		NetProfit_CTN__c toSave = initCtns[0].clone(true, true, true, true);
		toSave.CTN_Number__c = '31612345678';
		toSave.Simcard_number_VF__c = '123456789';
		toSave.Sharing__c = 'Yes';
		toSave.Data_Limit__c = '50';
		toSave.dataBlock__c = 'Yes';
		toSave.Contract_Number_Current_Provider__c = '12345678';
		toSave.Current_Provider_name__c = 'KPN';
		toSave.Service_Provider_Name__c = 'KPN';
		toSave.Billing_Arrangement__c = '12345678';
		toSave.Contract_Enddate__c = Date.today();

		Test.startTest();
		CreateMobileInformationController.save(
			initCtns,
			new List<NetProfit_CTN__c>{ toSave },
			opp.Id
		);
		Test.stopTest();

		NetProfit_CTN__c checkResult = [
			SELECT Id, CTN_Number__c
			FROM NetProfit_CTN__c
			WHERE Id = :initCtns[0].Id
		];
		System.assertEquals(
			'31612345678',
			checkResult.CTN_Number__c,
			'The controller should have saved the correct values to our NetProfit_CTN__c record'
		);
	}

	@isTest
	public static void testImportCsv() {
		Opportunity opp = [SELECT Id FROM Opportunity];
		cscfga__Product_Basket__c basket = [
			SELECT Id, cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
		];
		basket.cscfga__Basket_Status__c = 'Approved';
		update basket;

		NetProfit_Information__c npInfo = new NetProfit_Information__c(Opportunity__c = opp.Id);
		insert npInfo;

		NetProfit_CTN__c toSave = new NetProfit_CTN__c();
		toSave.NetProfit_Information__c = npInfo.Id;
		toSave.CTN_Number__c = '31612345678';
		toSave.Simcard_number_VF__c = '123456789';
		toSave.Sharing__c = 'Yes';
		toSave.Data_Limit__c = '50';
		toSave.dataBlock__c = 'Yes';
		toSave.Contract_Number_Current_Provider__c = '12345678';
		toSave.Current_Provider_name__c = 'KPN';
		toSave.Service_Provider_Name__c = 'KPN';
		toSave.Billing_Arrangement__c = '12345678';
		toSave.Contract_Enddate__c = Date.today();
		insert toSave;

		Test.startTest();
		List<NetProfit_CTN__c> insertedCtns = [SELECT Id FROM NetProfit_CTN__c];
		String rawCSVToImport =
			'"Id";"Model Number";"Product Name";"Discount %";"Duration";"BAP SAP";"Quantity Type";"Deal Type";"CTN";"Simcard Number VF";"Sharing";"Data Limit";"Data Blocking";"Contract Number Current Provider";"Current Provider Name";"Service Provider Name";"Billing Arrangement";"Porting Wishdate"\n"' +
			insertedCtns[0].Id +
			'";"";"Flex Mobile Voice";"0";"24";"12.5";"Monthly";"Retention";"31612345679";"";"";"";"";"";"";"";"";"2099-01-01"';

		Blob rawCSVBlob = Blob.valueOf(rawCSVToImport);
		String based64 = EncodingUtil.base64Encode(rawCSVBlob);
		CreateMobileInformationController.importCSV(based64, 'NetProfitCTN.csv', opp.Id, ';');
		Test.stopTest();

		List<NetProfit_CTN__c> result = [
			SELECT Id, CTN_Number__c
			FROM NetProfit_CTN__c
			WHERE NetProfit_Information__r.Opportunity__c = :opp.Id
		];
		System.assertEquals(
			'31612345679',
			result[0].CTN_Number__c,
			'The controller should updated the CTN_Number__c to the correct value'
		);
	}

	@isTest
	public static void testCreateNumberListPDF() {
		Opportunity opp = [SELECT Id FROM Opportunity];
		cscfga__Product_Basket__c basket = [
			SELECT Id, cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
		];
		basket.cscfga__Basket_Status__c = 'Approved';
		update basket;

		Contract_Generation_Settings__c contractGenSettings = new Contract_Generation_Settings__c();
		contractGenSettings.Document_template_name_direct__c = 'Direct Sales Template';
		contractGenSettings.Document_template_name_direct_2__c = 'Direct Sales Template 2';
		contractGenSettings.Document_template_name_BPA__c = 'BPA Template new';
		insert contractGenSettings;

		csclm__Document_Definition__c testDocumentDefinition = CS_DataTest.createDocumentDefinition();
		testDocumentDefinition.ExternalID__c = 'SomeId41';
		insert testDocumentDefinition;

		csclm__Document_Template__c docTemplate = CS_DataTest.createDocumentTemplate(
			'Direct Sales Template'
		);
		docTemplate.csclm__Document_Definition__c = testDocumentDefinition.Id;
		docTemplate.csclm__Valid__c = true;
		docTemplate.csclm__Active__c = true;
		docTemplate.ExternalID__c = 'SomeId4';
		insert docTemplate;
		system.debug('++++docTemplate: ' + docTemplate);

		csclm__Document_Template__c docTemplateBPA = CS_DataTest.createDocumentTemplate(
			'BPA Template new'
		);
		docTemplateBPA.csclm__Document_Definition__c = testDocumentDefinition.Id;
		docTemplateBPA.csclm__Valid__c = true;
		docTemplateBPA.csclm__Active__c = true;
		docTemplateBPA.ExternalID__c = 'SomeId5';
		insert docTemplateBPA;

		csclm__Document_Template__c docTemplate2 = CS_DataTest.createDocumentTemplate(
			'Direct Sales Template 2'
		);
		docTemplate2.csclm__Document_Definition__c = testDocumentDefinition.Id;
		docTemplate2.csclm__Valid__c = true;
		docTemplate2.csclm__Active__c = true;
		docTemplate2.ExternalID__c = 'SomeId42';
		insert docTemplate2;

		Test.startTest();
		System.assertEquals(
			true,
			CreateMobileInformationController.createNumberListPDF(
				opp.Id,
				new List<NetProfit_CTN__c>()
			),
			'pdf should be generated correctly'
		);
		Test.stopTest();
	}

	@isTest
	public static void testCreateNumberListPDFNoBasket() {
		Opportunity opp = [SELECT Id FROM Opportunity];
		delete [SELECT Id, cscfga__Basket_Status__c FROM cscfga__Product_Basket__c];

		Test.startTest();
		System.assertEquals(
			true,
			CreateMobileInformationController.createNumberListPDF(
				opp.Id,
				new List<NetProfit_CTN__c>()
			),
			'pdf should be generated correctly'
		);
		Test.stopTest();
	}

	@isTest
	public static void testconvertStringToDateWrongDates() {
		Test.startTest();
		System.assertEquals(
			null,
			CreateMobileInformationController.convertStringToDate(''),
			'should throw null when no date is added into the csv'
		);
		//simulate try catch for server on the lwc side which is invoking this method indirectly
		String dateInput = 'abcdefg';
		try {
			CreateMobileInformationController.convertStringToDate(dateInput);
		} catch (Exception lwcEx) {
			System.assertEquals(
				'Error processing date value: ' +
				dateInput +
				'. Please provide in DD-MM-YYYY or YYYY-MM-DD or YYYY/MM/DD format.',
				lwcEx.getMessage(),
				'An error should be thrown to the client in javascript'
			);
		}
		Test.stopTest();
	}
}