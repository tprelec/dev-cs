@isTest
private class TestCreditNoteBatch {
	@isTest
	static void testCreditNoteCreation() {
		Contracted_Products__c cp = prepareTestData('Price Change', false);
		cp.Delivery_Status__c = 'Approved';
		update cp;

		test.startTest();
		Database.executeBatch(new CreditNoteBatch(), 1);
		test.stopTest();

		List<Credit_Note__c> cnList = [
			SELECT Id, (SELECT Id FROM Credit_Lines__r)
			FROM Credit_Note__c
		];
		System.assert(cnList.size() > 0, 'No Credit Note found');

		List<Credit_Line__c> clList = cnList[0].Credit_Lines__r;
		System.assert(clList.size() > 0, 'No Credit Lines found');
	}

	@isTest
	static void testExistingCreditNote() {
		Contracted_Products__c cp = prepareTestData('Cease', true);
		cp.Delivery_Status__c = 'Approved';
		update cp;

		test.startTest();
		Database.executeBatch(new CreditNoteBatch(), 1);
		test.stopTest();

		List<Credit_Note__c> cnList = [
			SELECT Id, (SELECT Id FROM Credit_Lines__r)
			FROM Credit_Note__c
		];
		System.assert(cnList.size() > 0, 'No Credit Note found');

		List<Credit_Line__c> clList = cnList[0].Credit_Lines__r;
		System.assert(clList.size() > 0, 'No Credit Lines found');
	}

	static Contracted_Products__c prepareTestData(String rowType, Boolean createNote) {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		TestUtils.createCompleteContract();
        Account partAcc = TestUtils.createPartnerAccount();
		insert partAcc;

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'OneMobile';
		ord.OrderType__c = TestUtils.theOrderType.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = TestUtils.theContract.Id;
		ord.O2C_Order__c = true;
		ord.Sales_Order_Id__c = '4444444';
		ord.Account__c = TestUtils.theAccount.Id;
		insert ord;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ;
		cp.VF_Contract__c = TestUtils.theContract.Id;
		cp.Billing_Status__c = 'New';
		cp.Product__c = TestUtils.theProduct.Id;
		cp.ProductCode__c = 'C106929';
		cp.Site__c = TestUtils.theSite.Id;
		cp.Row_Type__c = rowType;
		cp.Net_Unit_Price__c = 10;
		cp.Quantity__c = 1;
		cp.Start_Invoicing_Date__c = System.today().addDays(-2);
		cp.Activation_Date__c = System.today().addDays(3);
		insert cp;

		List<Customer_Asset__c> caList = [SELECT Id FROM Customer_Asset__c];
		for (Customer_Asset__c ca : caList) {
			ca.Price__c = 20;
			ca.Quantity__c = 2;
		}
		update caList;

		if (createNote) {
			Map<String, Schema.RecordTypeInfo> cnRecordTypes = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByDeveloperName();
			Order__c o = [SELECT Id, Name, Account__c FROM Order__c];
			Credit_Note__c cn = new Credit_Note__c();
			cn.Order__c = o.Id;
			cn.Credit_Note_Id__c = o.Name + '-' + String.valueOf(System.today().month());
			cn.Account__c = o.Account__c;
            cn.Partner__c = partAcc.Id;
			cn.RecordTypeId = cnRecordTypes.get('Credit_Note_Indirect').getRecordTypeId();
			cn.Subscription__c = 'Fixed';
			cn.Credit_Amount__c = 0;
			insert cn;
		}

		return cp;
	}
}