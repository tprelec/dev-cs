@isTest
private class TestCreditNoteTriggerHandler {
	@TestSetup
	static void makeData() {
		User theAdmin = TestUtils.createAdministrator();
		User myAccountManager = TestUtils.createAccountManager();
		TestUtils.createManager();

		//to allow us to modify before insert
		TestUtils.autoCommit = false;

		//Create the Partner and Dealer accounts
		List<Account> testAccounts = new List<Account>();
		testAccounts.add(TestUtils.createAccount(theAdmin));
		Account partnerAccount = TestUtils.createPartnerAccount();
		testAccounts.add(partnerAccount);
		insert testAccounts;

		// Create Partner and Dealer Contacts
		List<Contact> testContacts = new List<Contact>();
		testContacts.add(TestUtils.createContact(testAccounts[0]));
		testContacts.add(TestUtils.portalContact(testAccounts[1]));
		testContacts[1].Userid__c = theAdmin.Id;
		testContacts.add(new Contact(LastName = 'EBU', Email = 'ebusalesforce@vodafoneziggo.com'));
		insert testContacts;

		// Create Dealer Information
		Dealer_Information__c testDealer = TestUtils.createDealerInformation(testContacts[1].Id);
		insert testDealer;

		//Create customer account
		Account customerAccount = TestUtils.createAccount(theAdmin, testDealer);
		insert customerAccount;
		customerAccount.Fixed_Credit_Approver__c = testDealer.Id;
		update customerAccount;

		BAN__c clientBAN = TestUtils.createBan(customerAccount);
		insert clientBAN;

		Opportunity theOpp = TestUtils.createOpportunityWithBan(
			customerAccount,
			Test.getStandardPricebookId(),
			clientBAN
		);
		insert theOpp;

		VF_Contract__c contractVF = TestUtils.createVFContract(customerAccount, theOpp);
		insert contractVF;
		contractVF.Dealer_Information__c = testDealer.Id;
		contractVF.No_of_CTN_Assets__c = 2;
		update contractVF;

		VF_Asset__c newAsset = new VF_Asset__c();
		newAsset.Account__c = customerAccount.Id;
		newAsset.Dealer_Code__c = testDealer.Dealer_Code__c;
		newAsset.Contract_Number__c = '1';
		newAsset.CTN_Status__c = Constants.VF_ASSET_CTN_STATUS_ACTIVE;
		newAsset.BAN_Number__c = '399999932';
		newAsset.Contract_VF__c = contractVF.Id;
		insert newAsset;

		CreditNote_Approvals__c newDirector = new CreditNote_Approvals__c();
		newDirector.Name = '(InDirect) Business Partners';
		newDirector.User__c = myAccountManager.Id;
		newDirector.Is_Director__c = true;
		insert newDirector;

		CreditNote_Approvals__c newSM = new CreditNote_Approvals__c();
		newSM.Name = 'CVM/CM';
		newSM.User__c = myAccountManager.Id;
		newSM.Is_Partner_Salesmanager__c = true;
		insert newSM;
	}

	private static VF_Contract__c getContracts() {
		VF_Contract__c contractVF = [
			SELECT
				Id,
				Name,
				Account__c,
				BAN__c,
				Contract_Number__c,
				No_of_CTN_Assets__c,
				Opportunity__r.BAN__r.Id,
				Dealer_Information__r.Is_ZSP_in_SFDC__c
			FROM VF_Contract__c
			LIMIT 1
		];
		return contractVF;
	}

	private static void updateDealerData(
		Account partnerAccount,
		Account customerAccount,
		VF_Contract__c contract
	) {
		User theAdmin = getAdministrator();
		Dealer_Information__c dealerInfo = getDealer();

		System.runAs(theAdmin) {
			TestUtils.autoCommit = true;
			User dealerUser = TestUtils.createPortalUser(partnerAccount);
			update new Contact(Id = dealerUser.ContactId, UserId__c = dealerUser.Id);
			TestUtils.autoCommit = false;
			customerAccount.Fixed_Credit_Approver__c = dealerInfo.Id;
			customerAccount.Mobile_Dealer__c = dealerInfo.Id;
			update customerAccount;
			partnerAccount.Mobile_Dealer__c = dealerInfo.Id;
			update partnerAccount;
			contract.Dealer_Information__c = dealerInfo.Id;
			update contract;
		}
	}

	private static User getAdministrator() {
		User u = [SELECT Id FROM User WHERE Alias = 'TestUser' LIMIT 1];
		return u;
	}

	private static User getAccountManager() {
		User u = [SELECT Id FROM User WHERE Alias = 'TActMgr' LIMIT 1];
		return u;
	}

	private static User getManager() {
		User u = [SELECT Id, ManagerId FROM User WHERE Alias = 'TestMgr' LIMIT 1];
		return u;
	}

	private static Account getPartnerAccount() {
		Account pa = [SELECT Id FROM Account WHERE Name = 'testPartner123' LIMIT 1];
		return pa;
	}

	private static Account getCustomerAccount() {
		Account ca = [SELECT Id, Status__c FROM Account WHERE Mobile_Dealer__c != NULL LIMIT 1];
		return ca;
	}

	private static User getPartnerPortalUser() {
		User pu = [SELECT Id FROM User WHERE LastName = 'PortalUser'];
		return pu;
	}

	private static Dealer_Information__c getDealer() {
		Dealer_Information__c d = [
			SELECT Id, Contact__r.UserId__c, Status__c
			FROM Dealer_Information__c
			LIMIT 1
		];
		return d;
	}

	private static void createEmailTemplate() {
		EmailTemplate validEmailTemplate = new EmailTemplate();
		validEmailTemplate.isActive = true;
		validEmailTemplate.Name = 'Contract_Reassign_Request';
		validEmailTemplate.DeveloperName = 'Contract_Reassign_Request';
		validEmailTemplate.TemplateType = 'text';
		validEmailTemplate.FolderId = UserInfo.getUserId();
		validEmailTemplate.Subject = 'Subject';

		insert validEmailTemplate;
	}

	private static void createVfContact() {
		Contact vfc = new Contact();
		vfc.Email = 'ebusalesforce@vodafoneziggo.com';
		vfc.FirstName = 'VF';
		vfc.Lastname = 'Contact';
		insert vfc;
	}

	@isTest
	static void testCreditNoteDirect() {
		VF_Contract__c contractVF = getContracts();

		Test.startTest();
		//Direct
		Credit_Note__c testCNote = new Credit_Note__c();
		testCNote.RecordTypeId = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByName()
			.get('Credit Note Direct')
			.getRecordTypeId();
		testCNote.Account__c = contractVF.Account__c;
		testCNote.Contract_VF__c = contractVF.Id;
		testCNote.Description__c = 'Creditnote';
		testCNote.Portfolio__c = 'Site_Connectivity';
		testCNote.Product_Family__c = 'Fixed_Connectivity';
		testCNote.Product_Category__c = 'Corporate Internet';
		insert testCNote;
		Test.stopTest();

		Credit_Note__c fetchedNote = [
			SELECT Id, Account_Owner__c
			FROM Credit_Note__c
			WHERE Id = :testCNote.Id
		];
		Dealer_Information__c dealer = getDealer();

		System.assertEquals(
			fetchedNote.Account_Owner__c,
			dealer.Contact__r.UserId__c,
			'Account owner User does not match'
		);
	}

	@isTest
	static void testCreditNoteIndirect() {
		Test.startTest();
		VF_Contract__c contractVF = getContracts();
		Account partnerAccount = getPartnerAccount();
		Account customerAccount = getCustomerAccount();

		Credit_Note__c testCNote = new Credit_Note__c();

		testCNote.RecordTypeId = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByName()
			.get('Credit Note Indirect')
			.getRecordTypeId();
		testCNote.Contract_VF__c = contractVF.Id;
		testCNote.Subscription__c = 'Mobile';
		testCNote.Account__c = customerAccount.Id;
		testCNote.Partner__c = partnerAccount.Id;
		testCNote.Description__c = 'Creditnote Indirect';
		testCNote.Portfolio__c = 'Site_Connectivity';
		testCNote.Product_Family__c = 'Fixed_Connectivity';
		testCNote.Product_Category__c = 'Corporate Internet';
		testCNote.Status__c = 'Draft';

		insert testCNote;
		Test.stopTest();

		Credit_Note__c cn = [
			SELECT Id, Director__c, Business_Partner_Manager__c, Sales_Manager__c
			FROM Credit_Note__c
			WHERE Id = :testCNote.Id
		];

		System.assertNotEquals(null, cn.Director__c, 'Director must be set');
		System.assertNotEquals(
			null,
			cn.Business_Partner_Manager__c,
			'Business partner manager must be set'
		);
	}

	/**
	 * Suppressed PMD warning because in this method we are testing email sending
	 * and email are turned off on sandboxes so no result is returned from the method
	 */
	@SuppressWarnings('PMD.ApexUnitTestClassShouldHaveAsserts')
	@isTest
	static void testInfoNotificationSend() {
		Test.startTest();
		VF_Contract__c contractVF = getContracts();
		Account partnerAccount = getPartnerAccount();
		Account customerAccount = getCustomerAccount();
		Dealer_Information__c di = getDealer();
		User accManager = getAccountManager();
		User manager = getManager();

		updateDealerData(partnerAccount, customerAccount, contractVF);

		manager.ManagerId = accManager.Id;
		update manager;

		di.Status__c = 'Expired';
		update di;

		Credit_Note__c testCNote = new Credit_Note__c();
		testCNote.RecordTypeId = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByName()
			.get('Credit Note fZiggo')
			.getRecordTypeId();
		testCNote.Contract_VF__c = contractVF.Id;
		testCNote.Subscription__c = 'Mobile';
		testCNote.Account__c = customerAccount.Id;
		testCNote.Partner__c = partnerAccount.Id;
		testCNote.Description__c = 'Creditnote Indirect';
		testCNote.Portfolio__c = 'Site_Connectivity';
		testCNote.Product_Family__c = 'Fixed_Connectivity';
		testCNote.Product_Category__c = 'Corporate Internet';
		testCNote.Related_Services__c = 'Ziggo Services';
		testCNote.Status__c = 'Draft';

		insert testCNote;
		Test.stopTest();
	}
}