/*
    Author: Juan Cardona
    Description: Tests class for CustomIterableUtils.cls
 */

@IsTest
private class TestCustomIterableUtils {

    @IsTest
    static void TestCustomIterableFunctionality() {
        Integer arrayLength = 0;
        Object referenceToNullAccount;
        List<Account> listAccounts = new List<Account>{new Account(Name = 'test_1'), new Account(Name = 'test_2')};

        Test.startTest();

        CustomIterableUtils iterator = new CustomIterableUtils(listAccounts);
        while (iterator.hasNext()) {
            iterator.next();
            arrayLength++;
        }
        referenceToNullAccount = iterator.next();

        Test.stopTest();

        System.assertEquals(2, arrayLength);
        System.assertEquals(null, referenceToNullAccount);
    }
}