@isTest
public with sharing class TestCustomerAssetTriggerHandler {

    @isTest
    public static void testCreateBSSOrder() {
        TestUtils.createCompleteContract();
        Order__c theOrder = TestUtils.createOrder(testUtils.theContract);

        External_WebService_Config__c config = new External_WebService_Config__c(
			Name = CreateBSSOrderRest.INTEGRATION_SETTING_NAME,
			URL__c = CreateBSSOrderRest.MOCK_URL,
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;
		System.assertNotEquals(null, config.Id, 'Config Id is NULL');

		setMock(config.URL__c, CreateBSSOrderRest.STATUS_OK, 200);
        Customer_Asset__c customerAsset = TestUtils.createCustomerAsset(theOrder, true);

        Test.startTest();
            insert customerAsset;
        Test.stopTest();

        List<Order_Billing_Transaction__c> obt = [SELECT Id FROM Order_Billing_Transaction__c];

        System.assertEquals(1, obt.size(),
                            'verify that an OrderBillingTransaction was made');
    }

	static void setMock(String url, String statusCode, Integer responseCode) {
		Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
		endpoint2TestResp.put(
			url,
			new TestUtilMultiRequestMock.SingleRequestMock(
				responseCode,
				'Complete',
				'{"status": "' +
				statusCode +
				'"}',
				null
			)
		);
		HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);

		Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);
	}
}