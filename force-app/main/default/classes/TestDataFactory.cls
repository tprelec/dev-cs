@isTest
public with sharing class TestDataFactory {

    @isTest
    public static void testPositive() {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('DealerInfoTriggerHandler', null, 0);

        Map<String, User> admins = TestUtils.createAdmins(3);

        System.runAs(admins.get('Admin1')){
            admins.get('Admin3').FirstName = 'Test';
            admins.get('Admin3').LastName = 'Account Manager';
            update admins.get('Admin3');
        }

        System.runAs(admins.get('Admin2')){
            TestUtils.autoCommit = false;
            Account Acc = TestUtils.createPartnerAccount();
            Acc.Type = 'Prospect';
            Acc.KVK_number__c = '12345678';
            insert Acc;

            TestUtils.autoCommit = false;
            Contact con = TestUtils.createContact(Acc);

            TestUtils.autoCommit = false;
            Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(con.Id);
            dealerInfo.Name = 'Test Account Manager';
            insert dealerInfo;

            Framework__c customSetting = new Framework__c(Name= 'TestSettings');
            customSetting.Framework_Sequence_Number__c = 2120;
            insert customSetting;

            TestUtils.createPBXTypes(10);
        }

        String error;

        Test.startTest();
            System.runAs(admins.get('Admin2')){
                try {
                    Datafactory.setupTestAccounts(1);
                } catch (Exception e) {
                    error = e.getMessage();
                }    
            }
        Test.stopTest();

        System.assertEquals(1, [select Id from Account where Name like '%ITE2%'].size(), 'One ITE2 Account should be created');
        System.assertEquals(1, [select Id from Contact where Email like '%ITE2.nl'].size(), 'One ITE2 Contact should be created');
        System.assertEquals(1, [select Id from Opportunity].size(), 'One Opportunity should be created');
        System.assertEquals(1, [select Id from cscfga__Product_Basket__c].size(), 'One Basket should be created');
        System.assertEquals(10, [select Id from Competitor_Asset__c].size(), '10 Competitor_Asset__c should be created');
        System.assertEquals(10, [select Id from Site__c].size(), '10 Site__c should be created');
        System.assertEquals(500, [select Id from Site_Postal_Check__c].size(), '500 Site_Postal_Check__c should be created');            
    }

    @isTest
    public static void testBulk(){
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('DealerInfoTriggerHandler', null, 0);

        Map<String, User> admins = TestUtils.createAdmins(3);

        System.runAs(admins.get('Admin1')){
            admins.get('Admin3').FirstName = 'Test';
            admins.get('Admin3').LastName = 'Account Manager';
            update admins.get('Admin3');
        }

        System.runAs(admins.get('Admin2')){
            TestUtils.autoCommit = false;
            List<Account> accounts = new List<Account>();

            for(Integer i = 0; i < 6; i++){
                Account Acc = TestUtils.createPartnerAccount();
                Acc.Type = 'Prospect' + i;
                Acc.KVK_number__c = '1234567' + i;
                Acc.BOPCode__c = Acc.BOPCode__c.removeEnd('P') + i;
                accounts.add(Acc);
            }
            insert accounts;


            TestUtils.autoCommit = false;
            Contact con = TestUtils.createContact(accounts[0]);

            TestUtils.autoCommit = false;
            Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(con.Id);
            dealerInfo.Name = 'Test Account Manager';
            insert dealerInfo;

            Framework__c customSetting = new Framework__c(Name= 'TestSettings');
            customSetting.Framework_Sequence_Number__c = 2120;
            insert customSetting;

            TestUtils.createPBXTypes(10);
        }

        String error;

        Test.startTest();
            System.runAs(admins.get('Admin2')){
                try {
                    Datafactory.setupTestAccounts(6);
                } catch (Exception e) {
                    error = e.getMessage();
                }    
            }
        Test.stopTest();

        System.assertEquals(6, [select Id from Account where Name like '%ITE2%'].size(), '6 ITE2 Account should be created');
        System.assertEquals(6, [select Id from Contact where Email like '%ITE2.nl'].size(), '6 ITE2 Contact should be created');
        System.assertEquals(6, [select Id from Opportunity].size(), '6 Opportunity should be created');
        System.assertEquals(6, [select Id from cscfga__Product_Basket__c].size(), '6 Basket should be created');
        System.assertEquals(60, [select Id from Competitor_Asset__c].size(), '60 Competitor_Asset__c should be created');
        System.assertEquals(60, [select Id from Site__c].size(), '60 Site__c should be created');
        System.assertEquals(3000, [select Id from Site_Postal_Check__c].size(), '3000 Site_Postal_Check__c should be created');
    }

    @isTest
    public static void testNoDealerInformation(){

        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('DealerInfoTriggerHandler', null, 0);

        Map<String, User> admins = TestUtils.createAdmins(3);

        System.runAs(admins.get('Admin1')){
            admins.get('Admin3').FirstName = 'Test';
            admins.get('Admin3').LastName = 'Account Manager';
            update admins.get('Admin3');
        }

        System.runAs(admins.get('Admin2')){
            TestUtils.autoCommit = false;
            Account Acc = TestUtils.createPartnerAccount();
            Acc.Type = 'Prospect';
            Acc.KVK_number__c = '12345678';
            insert Acc;

            TestUtils.autoCommit = false;
            Contact con = TestUtils.createContact(Acc);

            Framework__c customSetting = new Framework__c(Name= 'TestSettings');
            customSetting.Framework_Sequence_Number__c = 2120;
            insert customSetting;

            TestUtils.createPBXTypes(10);
        }

        String error;

        Test.startTest();
            System.runAs(admins.get('Admin2')){
                try {
                    Datafactory.setupTestAccounts(1);    
                } catch (Exception e) {
                    error = e.getMessage();
                }
                
            }
        Test.stopTest();

        System.assertEquals('Can \'t find a Dealer Information record with the name \'Test Account Manager\'. This is required to run this class so please verify that it exists and the name is correct.', 
                            error,
                            'A custom error should be thrown to notify the user the correct Dealer Information dosen\'t exist');
    }

    @isTest
    public static void testNoTestAccountManager(){
        
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('DealerInfoTriggerHandler', null, 0);

        Map<String, User> admins = TestUtils.createAdmins(3);
        List<User> testAccountManagers = [select Id, FirstName from User where Name = 'Test Account Manager'];

        if (!testAccountManagers.isEmpty()) {
            for (User u : testAccountManagers) {
                u.FirstName = null;
            }

            System.runAs(admins.get('Admin1')){
                update testAccountManagers;
            }            
        }

        System.runAs(admins.get('Admin2')){
            TestUtils.autoCommit = false;
            Account Acc = TestUtils.createPartnerAccount();
            Acc.Type = 'Prospect';
            Acc.KVK_number__c = '12345678';
            insert Acc;

            TestUtils.autoCommit = false;
            Contact con = TestUtils.createContact(Acc);

            TestUtils.autoCommit = false;
            Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(con.Id);
            dealerInfo.Name = 'Test Account Manager';
            insert dealerInfo;

            Framework__c customSetting = new Framework__c(Name= 'TestSettings');
            customSetting.Framework_Sequence_Number__c = 2120;
            insert customSetting;

            TestUtils.createPBXTypes(10);
        }

        String error;

        Test.startTest();
            System.runAs(admins.get('Admin2')){
                try {
                    Datafactory.setupTestAccounts(1);    
                } catch (Exception e) {
                    error = e.getMessage();
                }
                
            }
        Test.stopTest();

        System.assertEquals('Can \'t find a User with the name \'Test Account Manager\'. This is required to run this class so please verify that it exists and the name is correct.', 
                        error,
                        'A custom error should be thrown to notify the user Test Account Owner dosen \'t exist');      
    }

    @isTest
    public static void testNoAccounts(){
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('DealerInfoTriggerHandler', null, 0);

        Map<String, User> admins = TestUtils.createAdmins(3);

        System.runAs(admins.get('Admin1')){
            admins.get('Admin3').FirstName = 'Test';
            admins.get('Admin3').LastName = 'Account Manager';
            update admins.get('Admin3');
        }

        System.runAs(admins.get('Admin2')){

            TestUtils.autoCommit = false;
            Account Acc = TestUtils.createPartnerAccount();
            Contact con = TestUtils.createContact(Acc);

            TestUtils.autoCommit = false;
            Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(con.Id);
            dealerInfo.Name = 'Test Account Manager';
            insert dealerInfo;

            Framework__c customSetting = new Framework__c(Name= 'TestSettings');
            customSetting.Framework_Sequence_Number__c = 2120;
            insert customSetting;

            TestUtils.createPBXTypes(10);
        }

        String error;

        Test.startTest();
            System.runAs(admins.get('Admin2')){
                try {
                    Datafactory.setupTestAccounts(1);    
                } catch (Exception e) {
                    error = e.getMessage();
                }
                
            }
        Test.stopTest();

        System.assertEquals('No Accounts where found of the Type \'Prospect\' with a name that dosen\'t start with \'ITE2\' found. Please import some more Accounts that have a valid KVK as well', 
                        error,
                        'A custom error should be thrown to notify there are no viable accounts');                   
    }

    @isTest
    public static void testNotEnoughAccounts(){
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('DealerInfoTriggerHandler', null, 0);

        Map<String, User> admins = TestUtils.createAdmins(3);

        System.runAs(admins.get('Admin1')){
            admins.get('Admin3').FirstName = 'Test';
            admins.get('Admin3').LastName = 'Account Manager';
            update admins.get('Admin3');
        }

        System.runAs(admins.get('Admin2')){
            TestUtils.autoCommit = false;
            Account Acc = TestUtils.createPartnerAccount();
            Acc.Type = 'Prospect';
            Acc.KVK_number__c = '12345678';
            insert Acc;

            TestUtils.autoCommit = false;
            Contact con = TestUtils.createContact(Acc);

            TestUtils.autoCommit = false;
            Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(con.Id);
            dealerInfo.Name = 'Test Account Manager';
            insert dealerInfo;

            Framework__c customSetting = new Framework__c(Name= 'TestSettings');
            customSetting.Framework_Sequence_Number__c = 2120;
            insert customSetting;

            TestUtils.createPBXTypes(10);
        }

        String error;

        Test.startTest();
            System.runAs(admins.get('Admin2')){
                try {
                    Datafactory.setupTestAccounts(5);    
                } catch (Exception e) {
                    error = e.getMessage();
                }  
            }
        Test.stopTest();

        System.assertEquals('You requested 5 but only 1 are available. Either run the script for this number of Accounts or import new ones  of the Type \'Prospect\' with a name that dosen\'t start with \'ITE2\' and a valid KVK number.', 
                            error,
                            'A custom error should be thrown to notify the user there aren\'t enough Accounts');  
    }

    @isTest 
    public static void testDataFactoryWebServicePositive() {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('DealerInfoTriggerHandler', null, 0);

        Map<String, User> admins = TestUtils.createAdmins(3);

        System.runAs(admins.get('Admin1')){
            admins.get('Admin3').FirstName = 'Test';
            admins.get('Admin3').LastName = 'Account Manager';
            update admins.get('Admin3');
        }

        System.runAs(admins.get('Admin2')){
            TestUtils.autoCommit = false;
            Account Acc = TestUtils.createPartnerAccount();
            Acc.Type = 'Prospect';
            Acc.KVK_number__c = '12345678';
            insert Acc;

            TestUtils.autoCommit = false;
            Contact con = TestUtils.createContact(Acc);

            TestUtils.autoCommit = false;
            Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(con.Id);
            dealerInfo.Name = 'Test Account Manager';
            insert dealerInfo;

            Framework__c customSetting = new Framework__c(Name= 'TestSettings');
            customSetting.Framework_Sequence_Number__c = 2120;
            insert customSetting;

            TestUtils.createPBXTypes(10);
        }

        String result;
        Test.startTest();
            System.runAs(admins.get('Admin2')){
                RestRequest request = new RestRequest();
                request.requestUri = URL.getSalesforceBaseUrl().toExternalForm() + '/apexrest/DataFactory/';
                request.httpMethod = 'GET';
                RestContext.request = request;
                
                result = DataFactoryWebService.setupTestAccount().results.values()[0];
            }
        Test.stopTest();

        System.assertEquals(String.valueOf([select Id from Account limit 1].Id),
                            result,
                            'The WebService should return the Id of our TestAccount');            
    }

    @isTest
    public static void testDataFactoryWebServiceNegative(){

        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('DealerInfoTriggerHandler', null, 0);

        Map<String, User> admins = TestUtils.createAdmins(3);

        System.runAs(admins.get('Admin1')){
            admins.get('Admin3').FirstName = 'Test';
            admins.get('Admin3').LastName = 'Account Manager';
            update admins.get('Admin3');
        }

        System.runAs(admins.get('Admin2')){
            TestUtils.autoCommit = false;
            Account Acc = TestUtils.createPartnerAccount();
            Acc.Type = 'Prospect';
            Acc.KVK_number__c = '12345678';
            insert Acc;

            TestUtils.autoCommit = false;
            Contact con = TestUtils.createContact(Acc);

            Framework__c customSetting = new Framework__c(Name= 'TestSettings');
            customSetting.Framework_Sequence_Number__c = 2120;
            insert customSetting;

            TestUtils.createPBXTypes(10);
        }

        String result;
        Test.startTest();
            System.runAs(admins.get('Admin2')){
                RestRequest request = new RestRequest();
                request.requestUri = URL.getSalesforceBaseUrl().toExternalForm() + '/apexrest/DataFactory/';
                request.httpMethod = 'GET';
                RestContext.request = request;
                
                result = DataFactoryWebService.setupTestAccount().results.get('Error');
            }
        Test.stopTest();

        System.assertEquals('Can \'t find a Dealer Information record with the name \'Test Account Manager\'. This is required to run this class so please verify that it exists and the name is correct.', 
                            result,
                            'A custom error should be thrown to notify the user the correct Dealer Information dosen\'t exist');
    }
}