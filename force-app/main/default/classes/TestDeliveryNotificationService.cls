@IsTest
private class TestDeliveryNotificationService {
	@IsTest
	static void testUpdateOrderSuccess() {
		String orderId = prepareTestData();
		DeliveryNotificationService.ProjectManager projMgr = new DeliveryNotificationService.ProjectManager();
		projMgr.firstName = 'John';
		projMgr.lastName = 'Doe';
		projMgr.phone = '0031687654321';
		projMgr.email = 'john@doe.com';
		DeliveryNotificationService.DeliveryNotification delNot = new DeliveryNotificationService.DeliveryNotification();
		delNot.orderId = orderId;
		delNot.projectManager = projMgr;
		delNot.status = 'Completed';
		delNot.serviceReadyDate = '2020-9-7';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/deliverynotification/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(delNot));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		DeliveryNotificationService.updateOrders();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('OK', m.get('status'), 'Status not OK');
	}

	@IsTest
	static void testUpdateOrderSuccess1() {
		String orderId = prepareTestData();
		DeliveryNotificationService.ProjectManager projMgr = new DeliveryNotificationService.ProjectManager();
		projMgr.firstName = 'John';
		projMgr.lastName = 'Doe';
		projMgr.phone = '0031687654321';
		projMgr.email = 'john@doe.com';
		DeliveryNotificationService.DeliveryNotification delNot = new DeliveryNotificationService.DeliveryNotification();
		delNot.orderId = orderId;
		delNot.projectManager = projMgr;
		delNot.status = 'Cancelled';
		delNot.serviceReadyDate = '2020-9-7';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/deliverynotification/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(delNot));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		DeliveryNotificationService.updateOrders();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('OK', m.get('status'), 'Status not OK');
	}

	@IsTest
	static void testUpdateOrderSuccess2() {
		String orderId = prepareTestDataPart1();
		DeliveryNotificationService.ProjectManager projMgr = new DeliveryNotificationService.ProjectManager();
		projMgr.firstName = 'John';
		projMgr.lastName = 'Doe';
		projMgr.phone = '0031687654321';
		projMgr.email = 'john@doe.com';
		DeliveryNotificationService.DeliveryNotification delNot = new DeliveryNotificationService.DeliveryNotification();
		delNot.orderId = orderId;
		delNot.projectManager = projMgr;
		delNot.status = 'Cancelled';
		delNot.serviceReadyDate = '2020-9-7';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/deliverynotification/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(delNot));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		DeliveryNotificationService.updateOrders();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('OK', m.get('status'), 'Status not OK');
	}

	@IsTest
	static void testUpdateOrderError1() {
		DeliveryNotificationService.ProjectManager projMgr = new DeliveryNotificationService.ProjectManager();
		projMgr.firstName = 'John';
		projMgr.lastName = 'Doe';
		projMgr.phone = '0031687654321';
		projMgr.email = 'john@doe.com';
		DeliveryNotificationService.DeliveryNotification delNot = new DeliveryNotificationService.DeliveryNotification();
		delNot.orderId = '';
		delNot.projectManager = projMgr;
		delNot.status = 'Completed';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/deliverynotification/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(delNot));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		DeliveryNotificationService.updateOrders();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('FAILED', m.get('status'), 'Status not FAILED');

		List<Object> l = (List<Object>) m.get('errors');
		Map<String, Object> a = (Map<String, Object>) l[0];
		System.assertEquals('SFEC-0001', a.get('errorCode'), 'Expected SFEC-0001');
	}

	@IsTest
	static void testUpdateOrderError2() {
		DeliveryNotificationService.ProjectManager projMgr = new DeliveryNotificationService.ProjectManager();
		projMgr.firstName = 'John';
		projMgr.lastName = 'Doe';
		projMgr.phone = '0031687654321';
		projMgr.email = 'john@doe.com';
		DeliveryNotificationService.DeliveryNotification delNot = new DeliveryNotificationService.DeliveryNotification();
		delNot.orderId = 'foobar';
		delNot.projectManager = projMgr;
		delNot.status = 'Completed';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/deliverynotification/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(delNot));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		DeliveryNotificationService.updateOrders();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('FAILED', m.get('status'), 'Status not FAILED');

		List<Object> l = (List<Object>) m.get('errors');
		Map<String, Object> a = (Map<String, Object>) l[0];
		System.assertEquals('SFEC-0002', a.get('errorCode'), 'Expected SFEC-0002');
	}

	@IsTest
	static void testUpdateOrderError3() {
		String orderId = prepareTestData();
		DeliveryNotificationService.ProjectManager projMgr = new DeliveryNotificationService.ProjectManager();
		projMgr.firstName = 'John';
		projMgr.lastName = 'Doe';
		projMgr.phone = '0031687654321';
		projMgr.email = 'johndoe.com';
		DeliveryNotificationService.DeliveryNotification delNot = new DeliveryNotificationService.DeliveryNotification();
		delNot.orderId = orderId;
		delNot.projectManager = projMgr;
		delNot.status = 'Completed';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/deliverynotification/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(delNot));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		DeliveryNotificationService.updateOrders();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('FAILED', m.get('status'), 'Status not FAILED');

		List<Object> l = (List<Object>) m.get('errors');
		Map<String, Object> a = (Map<String, Object>) l[0];
		System.assertEquals('SFEC-9999', a.get('errorCode'), 'Expected SFEC-9999');
	}

	private static String prepareTestData() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		// Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct();
		insert product;

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = contr.Id;
		ord.O2C_Order__c = true;
		insert ord;

		Customer_Asset__c ca = new Customer_Asset__c();
		ca.Installation_Status__c = 'Pending Change';
		ca.Billing_Status__c = 'Pending Change';
		insert ca;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ;
		cp.VF_Contract__c = contr.Id;
		cp.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
		cp.Product__c = product.Id;
		cp.ProductCode__c = 'C106929';
		cp.Site__c = site.Id;
		cp.Credit__c = true;
		cp.Row_Type__c = Constants.CONTRACTED_PRODUCT_ROW_TYPE_PRICE_CHANGE;
		cp.Net_Unit_Price__c = 10;
		cp.Quantity__c = 1;
		cp.Start_Invoicing_Date__c = System.today();
		cp.Activation_Date__c = System.today().addDays(3);
		cp.Customer_Asset__c = ca.Id;
		insert cp;

		String ordId = ord.Id;
		Order__c newOrder = [SELECT BOP_Export_Order_Id__c FROM Order__c WHERE Id = :ordId];
		return newOrder.BOP_Export_Order_Id__c;
	}
	private static String prepareTestDataPart1() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		// Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct();
		insert product;

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = contr.Id;
		ord.O2C_Order__c = true;
		insert ord;

		Customer_Asset__c ca = new Customer_Asset__c();
		// ca.Installation_Status__c = 'Pending Change';
		ca.Billing_Status__c = 'Pending Change';
		insert ca;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ;
		cp.VF_Contract__c = contr.Id;
		cp.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
		cp.Product__c = product.Id;
		cp.ProductCode__c = 'C106929';
		cp.Site__c = site.Id;
		cp.Credit__c = true;
		cp.Row_Type__c = Constants.CONTRACTED_PRODUCT_ROW_TYPE_PRICE_CHANGE;
		cp.Net_Unit_Price__c = 10;
		cp.Quantity__c = 1;
		cp.Start_Invoicing_Date__c = System.today();
		cp.Activation_Date__c = System.today().addDays(3);
		cp.Customer_Asset__c = ca.Id;
		insert cp;

		String ordId = ord.Id;
		Order__c newOrder = [SELECT BOP_Export_Order_Id__c FROM Order__c WHERE Id = :ordId];
		return newOrder.BOP_Export_Order_Id__c;
	}
}