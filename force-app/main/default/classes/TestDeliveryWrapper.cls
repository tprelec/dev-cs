@isTest
private class TestDeliveryWrapper {
	
	@isTest static void test_method_one() {
		
	    User owner = TestUtils.createAdministrator();
	    Account acct = TestUtils.createAccount(owner);
		Site__c s = TestUtils.createSite(acct);

		DeliveryWrapper dw = new DeliveryWrapper();
		dw.site = s;
		dw.updateCpes();
		dw.updatePbxs();
		dw.updatePhones();
		dw.updateSite();
		dw.updateAdditionalLocationArticles();
		dw.getProposition();
		dw.getHasIPVPN();
		dw.getHasInternet();
		dw.getHasLegacy();
		dw.getSipCode();
		//dw.getBOPExportOrderId();
		dw.setOverflowPbx();
		dw.getHasMultipleCPEs();
		dw.getMaxSessionsPerCPE();
		dw.getLinksWithoutCarrier();
		dw.getHasOneFixed();
		dw.getHasERS();
		dw.getCustomerBOPCode();

		dw.updateLinks();
		dw.updateDelivery();
		dw.getOtherPbxsSelectOptions();
		dw.getClockSourceSelectOptions();
		
		PBX_Type__c pbx = new PBX_Type__c(Name = 'Onenet Virtual PBX', Vendor__c = 'Onenet', Product_Name__c = 'Virtual PBX');
		insert pbx;
		Competitor_Asset__c ca = new Competitor_Asset__c();
		ca = PBXSelectionWizardController.createAsset(s, ca, pbx.id);
		ca.Clock_Source__c = 'OTHER';
		ca.Clock_Source_Device__c = 'Test';
		dw.pbxs = new List<Competitor_Asset__c>();
		dw.pbxs.add(ca);
		dw.clockSourceOther = 'Test';
		dw.clockSourceSelected = 'Test';
		System.assertEquals(dw.clockSourceSelected, 'Test_');
		System.assertEquals(dw.clockSourceOther, '');

		Test.startTest();
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteContract();
		Test.stopTest();
		Contracted_Products__c cp = [
			SELECT Id, Is_RA_reference_phone_number__c, Is_RA_specs__c, 
				Inbound_Channel_Restriction__c, Outbound_Channel_Restriction__c, 
				Line_Identifier__c, Proposition_Component__c, Link_Type__c,
				Number_of_sessions__c, Codec__c, Interface_Type__c
			FROM Contracted_Products__c 
			WHERE VF_Contract__r.Opportunity__c = :TestUtils.theOpportunity.Id LIMIT 1
		];
		cp.Proposition_Component__c = 'IPVPNdata;IPVPNvoice;IPVPNvoip;QoS';
		DeliveryWrapper.ContractedProductWrapper cpw = new DeliveryWrapper.ContractedProductWrapper(cp);
		cpw.getCodec();
		cpw.getInterfaceType();
		cpw.getLinkType();
		cpw.getNumberOfSessions();
		cpw.getServices();
		cpw.getQos();

	}
	
}