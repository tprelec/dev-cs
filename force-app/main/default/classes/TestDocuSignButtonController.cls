@isTest
private class TestDocuSignButtonController {

    @isTest
    static void prepareOpportunityAndGetURL_base() {
        TestUtils.createCSProductBasket();

        Test.startTest();
        User accountManagerManager = TestUtils.createAccountManager();
        Opportunity opp = [SELECT Id, AccountId, Amount, Primary_Basket__c, OwnerId FROM Opportunity LIMIT 1];
        opp.Amount = 500001;
        User accountManager = new User(Id = opp.OwnerId, ManagerId = accountManagerManager.Id);
        update accountManager;
        opp.Primary_Basket__c = [SELECT Id, Sign_on_behalf_of_customer__c FROM cscfga__Product_Basket__c].Id;
        update new cscfga__Product_Basket__c(Id = opp.Primary_Basket__c,Sign_on_behalf_of_customer__c = 'both');
        update opp;
        Account acc = [SELECT Id FROM Account WHERE Id = :opp.AccountId];
        List<Contact> signers = getSignersAndPutOnAccount(acc);
        csclm__Agreement__c agreement = CS_DataTest.createAgreement('Test Agreement');
        agreement.csclm__Opportunity__c = opp.Id;
        agreement.recordTypeId = Schema.SObjectType.csclm__Agreement__c.getRecordTypeInfosByDeveloperName().get('Bespoke').getRecordTypeId();
        insert agreement;
        Attachment attachment = new Attachment(
            Name = 'I\'m testing attachments',
            Body = Blob.valueOf('Some body once told me the world is gonna roll me'),
            ParentId = agreement.Id
        );
        insert attachment;

        GenericMock mock = new GenericMock();
        mock.returns.put('attachDocumentsToSObjectForRegularAttachments', null);
        mock.returns.put('attachDocumentsToSObjectForContractConditions', null);
        DocumentService.instance = (DocumentService) Test.createStub(DocumentService.class, mock);

        // The DocuSignButtonController is dependent on a process builder on the Opportunity to
        // fill values on the Opportunity from the Opportunity.Owner.ManagerId, which is tested here.
        opp = [SELECT DS_Manager_Email__c, DS_Manager_Name__c FROM Opportunity WHERE Id = :opp.Id];
        System.assert(opp.DS_Manager_Name__c.contains(accountManagerManager.LastName));
        System.assertEquals(
            accountManagerManager.Email,
            opp.DS_Manager_Email__c,
            'A process builder on the Opportunity should fill the internal signer on the Opportunity ' +
            'From the Opportunity.Owner.ManagerId'
        );

        String result = DocuSignButtonController.prepareOpportunityAndGetURL(opp.Id);
        opp = [
            SELECT
                Id,
                Docusign_Attachment_Status__c,
                OwnerId,
                DS_Mail_Message_Digital_Signing__c,
                DS_Mail_Subject_Digital_Signing__c,
                DS_Authorized_Internal_Signer_1_Mail__c,
                Sales_Director_Name__c,
                DS_Authorized_to_sign_1st_Mail__c,
                DS_Authorized_to_sign_1st_Name__c,
                DS_Authorized_to_sign_2nd_Mail__c,
                DS_Authorized_to_sign_2nd_Name__c
            FROM Opportunity
            WHERE Id = :opp.Id
        ];
        signers = [SELECT Name, Email FROM Contact WHERE Id IN :signers ORDER BY Email];
        // The following assertions test the flow 'Docusign_Signer_Update_Autolaunched' that populates these values
        // on the Opp from the Account.
        System.assertEquals(signers[0].Name, opp.DS_Authorized_to_sign_1st_Name__c);
        System.assertEquals(signers[0].Email, opp.DS_Authorized_to_sign_1st_Mail__c);
        System.assertEquals(signers[1].Name, opp.DS_Authorized_to_sign_2nd_Name__c);
        System.assertEquals(signers[1].Email, opp.DS_Authorized_to_sign_2nd_Mail__c);

        System.assertEquals(opp.Id, mock.args.get('attachDocumentsToSObjectForRegularAttachments')[0]);
        List<CS_DocItem> docsInMethodCall = (List<CS_DocItem>) mock.args.get('attachDocumentsToSObjectForRegularAttachments')[1];
        System.assertEquals(attachment.Id, docsInMethodCall[0].docId);
        System.assertEquals(1, mock.callCount.get('attachDocumentsToSObjectForRegularAttachments'));
        System.assertEquals(1, mock.callCount.get('attachDocumentsToSObjectForContractConditions'));
        System.assertEquals(Constants.DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED, opp.Docusign_Attachment_Status__c);
        System.assert(new Pagereference(result).getParameters().get('CRL').contains(signers[0].Email));
        Test.stopTest();
    }

    @isTest
    static void prepareOpportunityAndGetURL_legalCase() {
        Account acc = TestUtils.createAccount(null);
        List<Contact> signers = getSignersAndPutOnAccount(acc);
        Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
        opp.Amount = 500001;
        update opp;
        TestUtils.createFullSprint();

        TestUtils.autoCommit = false;
        Contact internalSigner2 = TestUtils.createContact(acc);
        internalSigner2.Email = 'internalsigner2@test.com';
        insert internalSigner2;
        Case sourceCase = new Case(
            AccountId = opp.AccountId,
            Subject = 'Test',
            Description = 'Test',
            Opportunity__c = opp.Id,
            DS_Authorized_Internal_Signer_2__c = internalSigner2.Id,
            DS_Sign_on_behalf_of_customer__c = 'both',
            DS_Authorized_to_sign_1st__c = signers[0].Id,
            DS_Authorized_to_sign_2nd__c = signers[1].Id
        );
        sourceCase.RecordTypeId = Schema.SObjectType.Case
            .getRecordTypeInfosByDeveloperName()
            .get('Enterprise_Legal')
            .getRecordTypeId();
        insert sourceCase;
        TestUtils.autoCommit = true;

        // Dependency: A Process Builder on the Case populates the Case Id to the Opportunity
        // if the case is Record Type 'Enterprise_Legal' and it looks up to an Opportunity.
        //assert here: opp fields are populated. if not: custom message: flow <developername> not working
        System.assertEquals(
            sourceCase.Id,
            [SELECT Enterprise_Legal_Case__c FROM Opportunity WHERE Id = :opp.Id].Enterprise_Legal_Case__c,
            'The Case Process builder did not populate the Enterprise_Legal_Case__c on the Opportunity'
        );

        GenericMock mock = new GenericMock();
        mock.returns.put('attachDocumentsToSObjectFromSObject', null);
        mock.returns.put('getCaseAttachmentIdsByCaseId', new Set<Id>{'a142p00000Nqs6GAAR'}); // any valid Id will do
        DocumentService.instance = (DocumentService) Test.createStub(DocumentService.class, mock);

        Test.startTest();
        String result = DocuSignButtonController.prepareOpportunityAndGetURL(opp.Id);
        opp = [
            SELECT
                Id,
                Docusign_Attachment_Status__c,
                OwnerId,
                DS_Mail_Message_Digital_Signing__c,
                DS_Mail_Subject_Digital_Signing__c,
                DS_Authorized_Internal_Signer_1_Mail__c,
                Sales_Director_Name__c,
                DS_Authorized_Internal_Signer_2_Mail__c,
                DS_Authorized_to_sign_1st_Mail__c,
                DS_Authorized_to_sign_1st_Name__c,
                DS_Authorized_to_sign_2nd_Mail__c,
                DS_Authorized_to_sign_2nd_Name__c
            FROM Opportunity
            WHERE Id = :opp.Id
        ];
        signers = [SELECT Name, Email FROM Contact WHERE Id IN :signers ORDER BY Email];

        // The following assertions test the flow 'Docusign_Signer_Update_Autolaunched' that populates these values
        // on the Opp from the Account for legal cases.
        System.assertEquals(internalSigner2.Email, Opp.DS_Authorized_Internal_Signer_2_Mail__c);
        System.assertEquals(signers[0].Name, opp.DS_Authorized_to_sign_1st_Name__c);
        System.assertEquals(signers[0].Email, opp.DS_Authorized_to_sign_1st_Mail__c);
        System.assertEquals(signers[1].Name, opp.DS_Authorized_to_sign_2nd_Name__c);
        System.assertEquals(signers[1].Email, opp.DS_Authorized_to_sign_2nd_Mail__c);

        System.assertEquals(1, mock.callCount.get('getCaseAttachmentIdsByCaseId'));
        System.assertEquals(sourceCase.Id, mock.args.get('getCaseAttachmentIdsByCaseId')[0]);
        System.assertEquals(opp.Id, mock.args.get('attachDocumentsToSObjectFromSObject')[0]);
        System.assertEquals(new Set<Id>{'a142p00000Nqs6GAAR'}, mock.args.get('attachDocumentsToSObjectFromSObject')[1]);
        System.assertEquals(1, mock.callCount.get('attachDocumentsToSObjectFromSObject'));
        Test.stopTest();
    }

    @isTest
    static void prepareOpportunityAndGetURL_exception() {
        AuraHandledException expectedException;
        try {
        String result = DocuSignButtonController.prepareOpportunityAndGetURL(null);
        } catch (AuraHandledException caughtException) {
        expectedException = caughtException;
        }
        System.assert(expectedException != null);
    }

    @isTest
    static void getDocuSignPagereference() {
        Opportunity opp = TestUtils.createOpportunity(TestUtils.createAccount(null), Test.getStandardPricebookId());
        opp.Amount = 500001;
        update opp;
        opp = [
            SELECT
                Id,
                Docusign_Attachment_Status__c,
                OwnerId,
                DS_Mail_Message_Digital_Signing__c,
                DS_Mail_Subject_Digital_Signing__c,
                DS_Authorized_Internal_Signer_1_Mail__c,
                Sales_Director_Name__c,
                DS_Authorized_Internal_Signer_2_Mail__c,
                DS_Authorized_Internal_Signer_2_Name__c,
                DS_Authorized_to_sign_1st_Mail__c,
                DS_Authorized_to_sign_1st_Name__c,
                DS_Authorized_to_sign_2nd_Mail__c,
                DS_Authorized_to_sign_2nd_Name__c,
                DocuSign_On_Click_Option__c
            FROM Opportunity
            WHERE Id = :opp.Id
        ];
        opp.DS_Authorized_to_sign_1st_Mail__c = 'sign1@test.com';
        opp.DS_Authorized_to_sign_1st_Name__c = 'Sign1';
        opp.DS_Authorized_to_sign_2nd_Mail__c = 'sign2@test.com';
        opp.DS_Authorized_to_sign_2nd_Name__c = 'Sign2';
        DocuSignButtonController.opp = opp;

        Pagereference ref = DocuSignButtonController.getDocuSignPagereference();
        System.assert(ref.getUrl().contains('dsfs__DocuSign_CreateEnvelope'));
        System.assert(ref.getParameters().containsKey('CRL'));
        System.assert(ref.getParameters().get('CRL').contains(opp.DS_Authorized_to_sign_2nd_Mail__c));
    }

    @isTest
    static void validateEmptyParams() {
        Map<String, String> validMap = new Map<String, String>{'What goes in' => 'Must come out'};
        Map<String, String> inValidMap = new Map<String, String>{'What goes in' => ''};

        DocuSignButtonController.validateEmptyParams(validMap);

        DocuSignButtonController.DocuSignButtonControllerException expectedException;
        try {
            DocuSignButtonController.validateEmptyParams(inValidMap);
        } catch (DocusignButtonController.DocuSignButtonControllerException caughtException) {
            expectedException = caughtException;
        }
        System.assert(expectedException != null);
        System.assert(expectedException.getMessage().contains('What goes in'));
    }

    private class GenericMock implements System.StubProvider {
        private Map<String, Object> returns = new Map<String, Object>();
        private Map<String, Integer> callCount = new Map<String, Integer>();
        private Map<String, List<Object>> args = new Map<String, List<Object>>();

        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, List<Object> listOfArgs) {
            if (!callCount.containsKey(stubbedMethodName)) {
                callCount.put(stubbedMethodName, 0);
            }
            callCount.put(stubbedMethodName, callCount.get(stubbedMethodName) + 1);
            if (!args.containsKey(stubbedMethodName)) {
                args.put(stubbedMethodName, new List<Object>());
            }
            args.put(stubbedMethodName, listOfArgs);
            return returns.get(stubbedMethodName);
        }
    }

    private static List<Contact> getSignersAndPutOnAccount(Account acc) {
        TestUtils.autoCommit = false;
        List<Contact> signers = new List<Contact>{TestUtils.createContact(acc), TestUtils.createContact(acc)};
        signers[0].Email = 'signer1@test.com';
        signers[1].Email = 'signer2@test.com';
        insert signers;
        TestUtils.autoCommit = true;
        acc.Authorized_to_sign_1st__c = signers[0].Id;
        acc.Authorized_to_sign_2nd__c = signers[1].Id;
        update acc;
        return signers;
    }
}