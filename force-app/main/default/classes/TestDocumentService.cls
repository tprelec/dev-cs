@isTest
private class TestDocumentService {

    @testSetup
    static void makeData() {
        TestUtils.createCompleteOpportunity();
    }

    @isTest
    static void getInstance() {
        System.assertNotEquals(null, DocumentService.getInstance());
    }

    @isTest
    static void attachDocumentsToSObjectForRegularAttachments() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Attachment attachment = new Attachment(
            Name = 'I\'m testing attachments',
            Body = Blob.valueOf('Some body once told me the world is gonna roll me'),
            ParentId = opp.Id
        );
        insert attachment;
        CS_DocItem document = new CS_DocItem();
        document.name = 'Lame Name';
        document.docId = attachment.Id;

        Test.startTest();
        DocumentService.getInstance().attachDocumentsToSObjectForRegularAttachments(opp.Id, new List<CS_DocItem>{document});
        System.assertEquals(1, [SELECT Count() FROM ContentDocumentLink WHERE LinkedEntityId = :opp.Id]);
        Test.stopTest();
    }

    @isTest
    static void attachDocumentsToSObjectForContractConditions() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        ContentVersion contentVersion = createContentVersion();
        ContentVersion contentVersionForContentDocument = createContentVersion();
        Id contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionForContentDocument.Id].ContentDocumentId;

        List<CS_DocItem> documents = new List<CS_DocItem>();
        List<String> urls = new List<String>{
            'http://thisafakeurl.com/' + contentVersion.Id,
            'http://thisanotherfakeurl.com/' + contentDocumentId,
            'http://thisanotherfakeurl.com/AlgemeneVoorwaarden'
        };
        for (Integer i = 0; i <= 2; i++) {
            CS_DocItem document = new CS_DocItem();
            document.serviceDescriptionUrl = urls[i];
            documents.add(document);
        }
        documents[2].isGeneralCondition = true;

        Test.startTest();
        DocumentService.getInstance().attachDocumentsToSObjectForContractConditions(opp.Id, documents);
        System.assertEquals(2, [SELECT Count() FROM ContentDocumentLink WHERE LinkedEntityId = :opp.Id]);
        Test.stopTest();
    }

    @isTest
    static void getCaseAttachmentIdsByCaseId() {
        Opportunity opp = [SELECT AccountId FROM Opportunity LIMIT 1];
        Test.startTest();
        Case c = new Case(AccountId = opp.AccountId, Subject = 'Test', Description = 'Test');
        insert c;
        Id contractAttachmentContractDocumentRecordTypeId = Schema.SObjectType.Case_Attachments__c
            .getRecordTypeInfosByDeveloperName()
            .get('Contract_Document')
            .getRecordTypeId();
        Case_Attachments__c caseAttachment = new Case_Attachments__c(Case__c = c.Id, RecordTypeId = contractAttachmentContractDocumentRecordTypeId);
        insert caseAttachment;

        System.assertEquals(
            new Set<Id>{caseAttachment.Id},
            DocumentService.getInstance().getCaseAttachmentIdsByCaseId(c.Id)
        );
        Test.stopTest();
    }

    @isTest
    static void attachDocumentsToSObjectFromSObject() {
        Opportunity opp = [SELECT AccountId FROM Opportunity LIMIT 1];
        Test.startTest();
        Case sourceCase = new Case(AccountId = opp.AccountId, Subject = 'Test', Description = 'Test');
        insert sourceCase;

        ContentVersion contentVersionForContentDocument = createContentVersion();
        Id contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionForContentDocument.Id].ContentDocumentId;
       ContentDocumentLink contentDocumentLinkForCase = new ContentDocumentLink(
            ContentDocumentId = ContentDocumentId,
            LinkedEntityId = sourceCase.Id,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        insert contentDocumentLinkForCase;

        DocumentService.getInstance().attachDocumentsToSObjectFromSObject(opp.Id, new Set<Id>{sourceCase.Id});

        System.assertEquals(
            contentDocumentLinkForCase.ContentDocumentId,
            [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :opp.Id].ContentDocumentId
        );
        Test.stopTest();
    }

    //TODO: move this to TestUtils
    private static ContentVersion createContentVersion() {
        ContentVersion contentVersion = new ContentVersion(
            PathOnClient = 'Test.txt',
            Title = 'Testing',
            OwnerId = UserInfo.getUserId(),
            firstPublishLocationId = UserInfo.getUserId(),
            ContentLocation = 'S',
            Origin = 'C',
            VersionData = Blob.valueOf('Some body once told me the world is gonna roll me')
        );
        insert contentVersion;
        return contentVersion;
    }
}