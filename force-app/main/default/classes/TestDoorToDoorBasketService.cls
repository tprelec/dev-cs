@isTest
private class TestDoorToDoorBasketService {
	@testSetup
	private static void makeData() {
		List<Profile> profiles = [
			SELECT Id, Name
			FROM Profile
			WHERE Name = 'System Administrator'
			LIMIT 1
		];
		List<UserRole> roles = [SELECT Id FROM UserRole WHERE ParentRoleId = NULL];
		User u = CS_DataTest.createUser(profiles, roles);
		u.LG_SalesChannel__c = 'D2D';
		insert u;
		System.runAs(u) {
			Account acc = LG_GeneralTest.createAccount('Account', '12345678', 'Ziggo', true);
			LG_GeneralTest.createContact(
				acc,
				'Test',
				'Test',
				null,
				null,
				null,
				'Test@Test.com',
				null,
				null,
				null,
				null,
				null,
				null
			);

			TestUtils.autoCommit = false;

			Lead lead = TestUtils.createLead();
			lead.Company = 'CS TEST';
			lead.FirstName = 'Test';
			lead.LastName = 'Test';
			lead.Postcode_char__c = 'RE';
			lead.Postcode_number__c = '3572';
			lead.Street__c = 'Leistraat';
			lead.Visiting_Country__c = '3572';
			lead.Visiting_Housenumber1__c = 12;
			lead.Town__c = 'Utrecht';
			lead.Visiting_Country__c = 'Netherlands';
			lead.KVK_number__c = '87654321';
			lead.Email = 'test@test.com';
			insert lead;

			cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket(
				'Basket Test Handler',
				null,
				lead,
				null,
				false
			);
			basket.LG_CreatedFrom__c = 'Tablet';
			basket.LG_ResultofVisit__c = 'Quote Requested';
			basket.LG_BankAccountName__c = 'Test';
			basket.LG_InstallationCity__c = 'Utrecht';
			basket.LG_InstallationCountry__c = 'Netherlands';
			basket.LG_InstallationHouseNumber__c = '12';
			basket.LG_InstallationPostalCode__c = '3572RE';
			basket.LG_InstallationStreet__c = 'Leistraat';
			basket.LG_InstallationHouseNumberExtension__c = '';
			basket.LG_BillingCity__c = 'Utrecht';
			basket.LG_BillingCountry__c = 'Netherlands';
			basket.LG_BillingHouseNumber__c = '12';
			basket.LG_BillingStreet__c = 'Leistraat';
			basket.LG_BillingPostalCode__c = '3572RE';
			basket.LG_BillingHouseNumberExtension__c = '';
			basket.LG_AdminContactPicklist__c = lead.Id;
			basket.LG_AdminContactEmail__c = 'test@test.COM';
			basket.LG_AdminContactFirstName__c = 'Test';
			basket.LG_AdminContactLastName__c = 'Test';
			basket.LG_AdminContactSalutation__c = 'Ms.';
			basket.LG_TechContactSameAsBillingContact__c = true;
			basket.LG_TechnicalContactEmail__c = 'test3@test.com';
			basket.LG_TechnicalContactFirstName__c = 'Test2';
			basket.LG_TechnicalContactLastName__c = 'Test2';
			basket.LG_AdminContactSalutation__c = 'Ms.';
			basket.Lead__c = lead.Id;
			insert basket;

			cscfga__Product_Category__c prodCategory = LG_GeneralTest.createProductCategory(
				'Test Category',
				true
			);

			cscfga__Product_Definition__c prodDefinitionMkbInternet = LG_GeneralTest.createProductDefinition(
				'Zakelijk Internet Pro',
				false
			);
			prodDefinitionMkbInternet.cscfga__Product_Category__c = prodCategory.Id;
			insert prodDefinitionMkbInternet;

			cscfga__Attribute_Definition__c billingAccountAttDef = LG_GeneralTest.createAttributeDefinition(
				'BillingAccount',
				prodDefinitionMkbInternet,
				'User Input',
				'String',
				null,
				null,
				null,
				false
			);
			cscfga__Attribute_Definition__c jsonSOHOAttDef = LG_GeneralTest.createAttributeDefinition(
				'JSON Porting SOHO Telephony',
				prodDefinitionMkbInternet,
				'User Input',
				'String',
				null,
				null,
				'JSON Porting SOHO Telephony',
				false
			);
			cscfga__Attribute_Definition__c jsonMobileAttDef = LG_GeneralTest.createAttributeDefinition(
				'JSON Porting Mobile Telephony',
				prodDefinitionMkbInternet,
				'User Input',
				'String',
				null,
				null,
				'JSON Porting Mobile Telephony',
				false
			);
			cscfga__Attribute_Definition__c basketNumberAttDef = LG_GeneralTest.createAttributeDefinition(
				'Basket Number',
				prodDefinitionMkbInternet,
				'User Input',
				'String',
				null,
				null,
				null,
				false
			);
			cscfga__Attribute_Definition__c orderNumberAttDef = LG_GeneralTest.createAttributeDefinition(
				'Order Number',
				prodDefinitionMkbInternet,
				'User Input',
				'String',
				null,
				null,
				null,
				false
			);
			cscfga__Attribute_Definition__c premiseIdAttDef = LG_GeneralTest.createAttributeDefinition(
				'Premise Id',
				prodDefinitionMkbInternet,
				'User Input',
				'String',
				null,
				null,
				null,
				false
			);
			cscfga__Attribute_Definition__c premiseNumberAttDef = LG_GeneralTest.createAttributeDefinition(
				'Premise Number',
				prodDefinitionMkbInternet,
				'User Input',
				'String',
				null,
				null,
				null,
				false
			);
			cscfga__Attribute_Definition__c siteIdAttDef = LG_GeneralTest.createAttributeDefinition(
				'Site Id',
				prodDefinitionMkbInternet,
				'User Input',
				'String',
				null,
				null,
				null,
				false
			);

			List<cscfga__Attribute_Definition__c> attDefs = new List<cscfga__Attribute_Definition__c>{
				billingAccountAttDef,
				jsonSOHOAttDef,
				jsonMobileAttDef,
				basketNumberAttDef,
				orderNumberAttDef,
				premiseIdAttDef,
				premiseNumberAttDef,
				siteIdAttDef
			};
			insert attDefs;

			cscfga__Attribute_Field_Definition__c billingAccountAttFieldDef = LG_GeneralTest.createAttributeFieldDefinition(
				'BillingAccount',
				null,
				billingAccountAttDef,
				false
			);
			List<cscfga__Attribute_Field_Definition__c> attFieldDefs = new List<cscfga__Attribute_Field_Definition__c>{
				billingAccountAttFieldDef
			};
			insert attFieldDefs;

			cscfga__Product_Configuration__c prodConfigurationMkbInternet = LG_GeneralTest.createProductConfiguration(
				'MKB Configuration',
				3,
				basket,
				prodDefinitionMkbInternet,
				false
			);
			prodConfigurationMkbInternet.cscfga__Product_Family__c = 'Zakelijk Internet Pro';
			prodConfigurationMkbInternet.LG_MarketSegment__c = 'Small';
			insert prodConfigurationMkbInternet;

			cscfga__Attribute__c billingAccAtt = LG_GeneralTest.createAttribute(
				'BillingAccount',
				billingAccountAttDef,
				false,
				10,
				prodConfigurationMkbInternet,
				false,
				'10',
				false
			);
			cscfga__Attribute__c jsonSOHOAtt = LG_GeneralTest.createAttribute(
				'JSON Porting Mobile Telephony',
				jsonSOHOAttDef,
				false,
				null,
				prodConfigurationMkbInternet,
				false,
				null,
				false
			);
			cscfga__Attribute__c jsonMobileAtt = LG_GeneralTest.createAttribute(
				'JSON Porting Mobile Telephony',
				jsonMobileAttDef,
				false,
				null,
				prodConfigurationMkbInternet,
				false,
				null,
				false
			);
			cscfga__Attribute__c basketNumberAtt = LG_GeneralTest.createAttribute(
				'Basket Number',
				basketNumberAttDef,
				false,
				null,
				prodConfigurationMkbInternet,
				false,
				null,
				false
			);
			cscfga__Attribute__c orderNumberAtt = LG_GeneralTest.createAttribute(
				'Order Number',
				orderNumberAttDef,
				false,
				null,
				prodConfigurationMkbInternet,
				false,
				null,
				false
			);
			cscfga__Attribute__c premiseIdAtt = LG_GeneralTest.createAttribute(
				'Premise Id',
				premiseIdAttDef,
				false,
				null,
				prodConfigurationMkbInternet,
				false,
				null,
				false
			);
			cscfga__Attribute__c premiseNumberAtt = LG_GeneralTest.createAttribute(
				'Premise Number',
				premiseNumberAttDef,
				false,
				null,
				prodConfigurationMkbInternet,
				false,
				null,
				false
			);
			cscfga__Attribute__c siteIdAtt = LG_GeneralTest.createAttribute(
				'Site Id',
				siteIdAttDef,
				false,
				null,
				prodConfigurationMkbInternet,
				false,
				null,
				false
			);
			List<cscfga__Attribute__c> atts = new List<cscfga__Attribute__c>{
				billingAccAtt,
				jsonSOHOAtt,
				jsonMobileAtt,
				basketNumberAtt,
				orderNumberAtt,
				premiseIdAtt,
				premiseNumberAtt,
				siteIdAtt
			};
			insert atts;

			cscfga__Attribute_Field__c billingAccAttField = LG_GeneralTest.createAttributeField(
				'BillingAccount',
				billingAccAtt,
				null,
				false
			);
			List<cscfga__Attribute_Field__c> attFields = new List<cscfga__Attribute_Field__c>{
				billingAccAttField
			};
			insert attFields;

			OrderType__c orderTypeZiggo = TestUtils.createOrderType();
			orderTypeZiggo.Name = 'Ziggo';
			OrderType__c orderTypeMAC = TestUtils.createOrderType();
			orderTypeMAC.Name = 'MAC';
			insert new List<OrderType__c>{ orderTypeZiggo, orderTypeMAC };
		}
	}

	@isTest
	private static void testConversionNoAccount() {
		cscfga__Product_Basket__c basket = getBasket();
		Test.startTest();
		DoorToDoorBasketService d2dBasketSvc = new DoorToDoorBasketService(basket.Id);
		d2dBasketSvc.convertLeadAndSyncBasket();
		Test.stopTest();

		// Verify Results
		basket = getBasket();
		System.assertEquals(
			getLeadConvertStatus().MasterLabel,
			getLead().Status,
			'Lead should be converted'
		);
		System.assertEquals(
			true,
			basket.csbb__Synchronised_With_Opportunity__c,
			'Basket should be synchronized to Opportunity'
		);
		System.assertEquals(2, getAccounts().size(), 'Lead should be converted to the new Account');
		List<Opportunity> opps = getOpportunities();
		System.assertEquals(1, opps.size(), 'Opportunity should be created');
		System.assertEquals(
			'Awareness of interest',
			opps[0].StageName,
			'Invalid Opportunity Stage'
		);
		System.assertEquals(
			'Quote Requested',
			opps[0].LG_AutomatedQuoteDelivery__c,
			'Invalid Opportunity Quote Delivery'
		);
		System.assertEquals(1, getBillingAccounts().size(), 'Billing Account should be created');
		System.assertEquals(1, getSites().size(), 'Site should be created');
		System.assertEquals(1, getAddresses().size(), 'Address should be created');
		System.assertEquals(2, getContacts().size(), 'One new Contact should be created');
		System.assertEquals(1, getTasks().size(), 'Task should be created');
		System.assert(
			getOppContactRoles().size() > 0,
			'Opportunity Contact Roles should be created'
		);
	}

	@isTest
	private static void testConversionAccountFound() {
		cscfga__Product_Basket__c basket = getBasket();
		basket.LG_TechContactSameAsBillingContact__c = false;
		basket.LG_ResultofVisit__c = 'Customer Approved';
		update basket;
		Lead l = getLead();
		l.KVK_number__c = '12345678';
		update l;
		Test.startTest();
		DoorToDoorBasketService d2dBasketSvc = new DoorToDoorBasketService(basket.Id);
		d2dBasketSvc.convertLeadAndSyncBasket();
		Test.stopTest();

		// Verify Results
		basket = getBasket();
		System.assertEquals(null, basket.Error_Message__c, 'Basket Error Message should be empty');
		System.assertEquals(
			getLeadConvertStatus().MasterLabel,
			getLead().Status,
			'Lead should be converted'
		);
		System.assertEquals(
			true,
			basket.csbb__Synchronised_With_Opportunity__c,
			'Basket should be synchronized to Opportunity'
		);
		System.assertEquals(1, getAccounts().size(), 'Existing Account should be used');
		List<Opportunity> opps = getOpportunities();
		System.assertEquals(1, opps.size(), 'Opportunity should be created');
		System.assertEquals('Customer Approved', opps[0].StageName, 'Invalid Opportunity Stage');
		System.assertEquals(1, getBillingAccounts().size(), 'Billing Account should be created');
		System.assertEquals(1, getSites().size(), 'Site should be created');
		System.assertEquals(1, getAddresses().size(), 'Address should be created');
		System.assertEquals(2, getContacts().size(), 'Two Contacts should be created');
		System.assertEquals(1, getTasks().size(), 'Task should be created');
		System.assert(
			getOppContactRoles().size() > 0,
			'Opportunity Contact Roles should be created'
		);
	}

	@isTest
	private static void testNoLead() {
		cscfga__Product_Basket__c basket = getBasket();
		basket.LG_TechContactSameAsBillingContact__c = false;
		basket.LG_ResultofVisit__c = 'Customer Approved';
		basket.Lead__c = null;
		basket.csbb__Account__c = getAccounts()[0].Id;
		update basket;

		Test.startTest();
		DoorToDoorBasketService d2dBasketSvc = new DoorToDoorBasketService(basket.Id);
		d2dBasketSvc.convertLeadAndSyncBasket();
		Test.stopTest();

		// Verify Results
		basket = getBasket();
		System.assertEquals(null, basket.Error_Message__c, 'Basket Error Message should be empty');
		System.assertEquals(
			true,
			basket.csbb__Synchronised_With_Opportunity__c,
			'Basket should be synchronized to Opportunity'
		);
		List<Opportunity> opps = getOpportunities();
		System.assertEquals(1, opps.size(), 'Opportunity should be created');
		System.assertEquals('Customer Approved', opps[0].StageName, 'Invalid Opportunity Stage');
		System.assertEquals(1, getBillingAccounts().size(), 'Billing Account should be created');
		System.assertEquals(1, getSites().size(), 'Site should be created');
		System.assertEquals(1, getAddresses().size(), 'Address should be created');
		System.assertEquals(2, getContacts().size(), 'Two Contacts should be created');
		System.assertEquals(1, getTasks().size(), 'Task should be created');
		System.assert(
			getOppContactRoles().size() > 0,
			'Opportunity Contact Roles should be created'
		);
	}

	private static cscfga__Product_Basket__c getBasket() {
		return [
			SELECT Id, csbb__Synchronised_With_Opportunity__c, Error_Message__c
			FROM cscfga__Product_Basket__c
		];
	}

	private static Lead getLead() {
		return [SELECT Id, Status FROM Lead];
	}

	private static LeadStatus getLeadConvertStatus() {
		return [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1];
	}

	private static List<Account> getAccounts() {
		return [SELECT Id FROM Account];
	}

	private static List<Opportunity> getOpportunities() {
		return [SELECT Id, StageName, LG_AutomatedQuoteDelivery__c FROM Opportunity];
	}

	private static List<csconta__Billing_Account__c> getBillingAccounts() {
		return [SELECT Id FROM csconta__Billing_Account__c];
	}

	private static List<cscrm__Site__c> getSites() {
		return [SELECT Id FROM cscrm__Site__c];
	}

	private static List<cscrm__Address__c> getAddresses() {
		return [SELECT Id FROM cscrm__Address__c];
	}

	private static List<Contact> getContacts() {
		return [SELECT Id, Email, Name, Account.Name FROM Contact WHERE AccountId != NULL];
	}

	private static List<Task> getTasks() {
		return [SELECT Id FROM Task WHERE Subject = 'Visit'];
	}

	private static List<OpportunityContactRole> getOppContactRoles() {
		return [SELECT Id FROM OpportunityContactRole];
	}
}