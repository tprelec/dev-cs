@isTest
private class TestECSContactServie {
	
	@isTest static void test_method_one() {
		// Implement test code
		ECSContactService ecs = new ECSContactService();
		ECSContactService.Request theRequest = new ECSContactService.Request();
		theRequest.companyBanNumber = '123';
		theRequest.companyBopCode = 'ABC';
		theRequest.companyCorporateId = '123';
		theRequest.email = 'test@example.com';
		theRequest.houseNumber = '1';
		theRequest.houseNumberExtension = 'a';
		//theRequest.referenceId = '123';
		theRequest.service = 'test';
		theRequest.type = 'Create';
		theRequest.zipCode = '1235AB';
		List<ECSContactService.Request> theRequests = new List<ECSContactService.Request>();
		theRequests.add(theRequest);

		ecs.setRequest(theRequests);

		ecs.makeRequest('create');
		ecs.makeCreateRequest();
		ecs.makeDeleteRequest();
		List<ECSContactService.Response> theResponses = ecs.getResponse();
		ECSContactService.Response theResponse = new ECSContactService.Response();
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}