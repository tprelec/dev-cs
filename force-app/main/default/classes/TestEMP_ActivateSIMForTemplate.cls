@isTest
public class TestEMP_ActivateSIMForTemplate{

    @isTest
    public static void testResponseASFTBulk(){
        String allcontents = '{"status":200,"data":[{"orderID":"557475359A"}],"error":[{"message" : "The allocated number [, Simkaartnummer] for  resource is not available. Please allocate new number. sourceSystem  OMS","code" : "500"}]}';

        test.startTest();
            EMP_ActivateSIMForTemplate.Response res = EMP_ActivateSIMForTemplate.parse(allcontents);
            System.assertEquals(true, res != null, 'meaning the parse was done sucessfully');
        test.stopTest();
    }
    
    @isTest
    public static void testRequestASFTBulk(){
        List<EMP_ActivateSIMForTemplate.ActivateSimForTemplateRequest> activateSIMinBulkData = New List<EMP_ActivateSIMForTemplate.ActivateSimForTemplateRequest>();
        EMP_ActivateSIMForTemplate.CustomerInfo customerInfo = new EMP_ActivateSIMForTemplate.CustomerInfo('1237652');
        EMP_ActivateSIMForTemplate.PortInData portInData = new EMP_ActivateSIMForTemplate.PortInData('kpn', 'test', 'kpn', '07-09-2020', 'test', '31690453445', '0vxjksdf78347');
        EMP_ActivateSIMForTemplate.SelectPricePlanData selectPricePlanData = New EMP_ActivateSIMForTemplate.SelectPricePlanData(customerInfo, '00999999', '627534A', '6253428A', '8723645A', '31683764583', '234567865377572', portInData);

        List<EMP_ActivateSIMForTemplate.ComponentSetting> componentSettingsData = New List<EMP_ActivateSIMForTemplate.ComponentSetting>();
        componentSettingsData.add(New EMP_ActivateSIMForTemplate.ComponentSetting('APN1', '1'));
        componentSettingsData.add(New EMP_ActivateSIMForTemplate.ComponentSetting('APN2', '2'));
        componentSettingsData.add(New EMP_ActivateSIMForTemplate.ComponentSetting('APN3', '3'));
        componentSettingsData.add(New EMP_ActivateSIMForTemplate.ComponentSetting('APN4', '4'));
        componentSettingsData.add(New EMP_ActivateSIMForTemplate.ComponentSetting('Internet_Access', 'IA'));

        List<EMP_ActivateSIMForTemplate.ComponentSetting> componentSettingsSettings = New List<EMP_ActivateSIMForTemplate.ComponentSetting>();

        componentSettingsSettings.add(New EMP_ActivateSIMForTemplate.ComponentSetting('Outgoing_Service_Settings', 'tempCTN.outgoingService__c'));
        componentSettingsSettings.add(New EMP_ActivateSIMForTemplate.ComponentSetting('Third_Party_Content', 'tempCTN.thirdPartyContent__c'));
        componentSettingsSettings.add(New EMP_ActivateSIMForTemplate.ComponentSetting('Premium_Destinations', 'tempCTN.premiumDestinations__c'));
        componentSettingsSettings.add(New EMP_ActivateSIMForTemplate.ComponentSetting('SMS_Premium', 'tempCTN.SMSPremium__c'));

        List<EMP_ActivateSIMForTemplate.ComponentSetting> componentSettingsRoaming = New List<EMP_ActivateSIMForTemplate.ComponentSetting>();
        componentSettingsRoaming.add(New EMP_ActivateSIMForTemplate.ComponentSetting('Roaming_Spending_Limit', 'tempCTN.roamingspendingLimit__c'));

        List<EMP_ActivateSIMForTemplate.UpdateProductSettingsInput> updateProductSettingsInputList = new List<EMP_ActivateSIMForTemplate.UpdateProductSettingsInput>();

        updateProductSettingsInputList.add(New EMP_ActivateSIMForTemplate.UpdateProductSettingsInput('Data', componentSettingsData));
        updateProductSettingsInputList.add(New EMP_ActivateSIMForTemplate.UpdateProductSettingsInput('Settings', componentSettingsSettings));
        updateProductSettingsInputList.add(New EMP_ActivateSIMForTemplate.UpdateProductSettingsInput('Data_Roaming', componentSettingsRoaming));
        EMP_ActivateSIMForTemplate.Request singleActivateSIMRequest = new EMP_ActivateSIMForTemplate.Request(selectPricePlanData, updateProductSettingsInputList);
        List<EMP_ActivateSIMForTemplate.BslHttpHeader> requestHeaders = new List<EMP_ActivateSIMForTemplate.BslHttpHeader>();

        requestHeaders.add(new EMP_ActivateSIMForTemplate.BslHttpHeader('DealerCode', '00354673'));
        requestHeaders.add(new EMP_ActivateSIMForTemplate.BslHttpHeader('MessageID', string.valueOf(system.now())));
        //single request
        activateSIMinBulkData.add(New EMP_ActivateSIMForTemplate.ActivateSimForTemplateRequest(requestHeaders, '00765467276', 5, singleActivateSIMRequest));

        List<EMP_ActivateSIMForTemplate.SharedBslHttpHeader> sharedRequestHeaders = new List<EMP_ActivateSIMForTemplate.SharedBslHttpHeader>();
        sharedRequestHeaders.add(new EMP_ActivateSIMForTemplate.SharedBslHttpHeader('DealerCode', 'not used'));
        sharedRequestHeaders.add(new EMP_ActivateSIMForTemplate.SharedBslHttpHeader('MessageID', string.valueOf(system.now())));
        //request wrapper
        EMP_ActivateSIMForTemplate.ActivateSimForTemplateBulkRequest activateSIMinBulkRequest = New EMP_ActivateSIMForTemplate.ActivateSimForTemplateBulkRequest(sharedRequestHeaders, activateSIMinBulkData);
        
        test.startTest();
            EMP_ActivateSIMForTemplate.ActivateSimForTemplateBulkRequestTop activateSIMinBulkRequestComplete = New EMP_ActivateSIMForTemplate.ActivateSimForTemplateBulkRequestTop(activateSIMinBulkRequest);
            System.assertEquals(true, activateSIMinBulkRequestComplete != null, 'meaning the parse was done sucessfully');
        test.stopTest();
    }
}