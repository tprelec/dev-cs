@isTest
public class TestEMP_AddMobileProductController {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, owner);
	}

	@isTest
	public static void testGetContractNumber() {
		Opportunity objOppQ = [SELECT Id, OwnerId FROM Opportunity];
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_FWAres200');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');

		User u = [SELECT Id FROM User WHERE Id = :objOppQ.OwnerId];
		Account a = TestUtils.createAccount(u);
		a.Dealer_code__c = '00822033';
		Contact c = TestUtils.createContact(a);
		cscfga__Product_Basket__c pb = CS_DataTest.createProductBasket(objOppQ, '822033', a);
		insert pb;
		Dealer_Information__c di = TestUtils.createDealerInformation(c.Id, '822033');
		di.Sales_Channel__c = 'Indirect';
		upsert di;

		VF_Contract__c contr = TestUtils.createVFContract(a, objOppQ);
		contr.Contract_Number__c = '234234234';
		upsert contr;

		// Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		// Call the method that performs the callout
		Test.startTest();
		List<EMP_AddmobileProductController.WrapperFWATemp> res = EMP_AddmobileProductController.GetFWATempFromAPI(
			'999999',
			objoppQ.Id
		);
		System.assertEquals(true, !res.isEmpty(), 'Response should not be empty.');
		//assure the method entry directly cannot modify the JSON to add the contracts number due to basket number, contract name, prmary basket name, and primary quote code has dynamic values or auto-incremented
		System.assertEquals(
			contr.name,
			EMP_AddmobileProductController.getContractNumber(new Set<String>{ contr.name }, contr),
			'Validates that the method return the right value for contract Name'
		);
		System.assertEquals(
			'234234234',
			EMP_AddmobileProductController.getContractNumber(
				new Set<String>{ contr.Contract_Number__c },
				contr
			),
			'Validates that the method return the right value for contract Number'
		);
		System.assertEquals(
			null,
			EMP_AddmobileProductController.getContractNumber(new Set<String>{ pb.Name }, contr),
			'Validates that the method enter and evaluates remain formula or autoincremeted fields'
		);
		Test.stopTest();
	}

	@isTest
	public static void testCalloutWithStaticResources200() {
		Opportunity objOppQ = [SELECT Id, OwnerId FROM Opportunity];
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_FWAres200');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');

		User u = [SELECT Id FROM User WHERE Id = :objOppQ.OwnerId];
		Account a = TestUtils.createAccount(u);
		a.Dealer_code__c = '00822033';
		Contact c = TestUtils.createContact(a);
		cscfga__Product_Basket__c pb = CS_DataTest.createProductBasket(objOppQ, '822033', a);
		insert pb;
		Dealer_Information__c di = TestUtils.createDealerInformation(c.Id, '822033');
		di.Sales_Channel__c = 'SOHO';
		upsert di;

		VF_Contract__c contr = TestUtils.createVFContract(a, objOppQ);
		contr.Contract_Number__c = '234234234';
		upsert contr;

		// Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		// Call the method that performs the callout
		Test.startTest();
		List<EMP_AddmobileProductController.WrapperFWATemp> res = EMP_AddmobileProductController.GetFWATempFromAPI(
			'999999',
			objoppQ.Id
		);
		System.assertEquals(true, !res.isEmpty(), 'Response should not be empty.');
		Test.stopTest();
	}

	@isTest
	public static void testCalloutWithStaticResources400() {
		Opportunity objOppQ = [SELECT Id, OwnerId FROM Opportunity];
		// Use StaticResourceCalloutMock built-in class to
		// specify fake response and include response body
		// in a static resource.
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_GenericAPI400_Error');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');

		User u = [SELECT Id FROM User WHERE Id = :objOppQ.OwnerId];
		Account a = TestUtils.createAccount(u);
		a.Dealer_code__c = '00822033';
		Contact c = TestUtils.createContact(a);
		Dealer_Information__c di = TestUtils.createDealerInformation(c.Id, '822033');
		di.Sales_Channel__c = 'SOHO';
		upsert di;

		// Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		// Call the method that performs the callout
		Test.startTest();
		try {
			EMP_AddmobileProductController.getFWATempFromAPI('123456', objoppQ.Id);
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains(System.Label.EMP_ErrorAddMobileController),
				'Incorrect error caught.'
			);
		}
		Test.stopTest();
	}

	@isTest
	public static void testCalloutWithStaticResources200NoData() {
		Opportunity objOppQ = [SELECT Id, OwnerId FROM Opportunity];
		// Use StaticResourceCalloutMock built-in class to
		// specify fake response and include response body
		// in a static resource.
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_GenericAPI400_Error');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');

		User u = [SELECT Id FROM User WHERE Id = :objOppQ.OwnerId];
		Account a = TestUtils.createAccount(u);
		a.Dealer_code__c = '00822033';
		Contact c = TestUtils.createContact(a);
		Dealer_Information__c di = TestUtils.createDealerInformation(c.Id, '822033');
		di.Sales_Channel__c = 'SOHO';
		upsert di;

		// Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		// Call the method that performs the callout
		try {
			EMP_AddmobileProductController.GetFWATempFromAPI('123456', objoppQ.Id);
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains(System.Label.EMP_ErrorAddMobileController),
				'Incorrect error caught.'
			);
		}
		Test.stopTest();
	}

	@isTest
	public static void testCalloutWithStaticResources200NoOutput() {
		Opportunity objOppQ = [SELECT Id, OwnerId FROM Opportunity];
		// Use StaticResourceCalloutMock built-in class to
		// specify fake response and include response body
		// in a static resource.
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_FWA_NoOutput');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');

		User u = [SELECT Id FROM User WHERE Id = :objOppQ.OwnerId];
		Account a = TestUtils.createAccount(u);
		a.Dealer_code__c = '00822033';
		Contact c = TestUtils.createContact(a);
		Dealer_Information__c di = TestUtils.createDealerInformation(c.Id, '822033');
		di.Sales_Channel__c = 'SOHO';
		upsert di;

		// Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		// Call the method that performs the callout
		try {
			EMP_AddmobileProductController.GetFWATempFromAPI('123456', objoppQ.Id);
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage()
					.contains('No Framework Agreements and Templates were found for this customer'),
				'Incorrect error caught.'
			);
		}
		Test.stopTest();
	}

	@isTest
	public static void testOLICreation() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		OrderType__c ot = TestUtils.createOrderType();
		TestUtils.autoCommit = true;
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			null,
			owner
		);
		VF_Product__c vfp = new VF_Product__c(
			Name = 'tst 2',
			Family__c = 'MBB',
			Product_Line__c = 'fVodafone',
			ExternalID__c = 'VFP-02-1234568',
			AllowsGroupDataSharing__c = 'No',
			ProductCode__c = 'DUMMYADDMOBILEPRODUCT_DATA',
			OrderType__c = ot.Id
		);
		insert vfp;
		TestUtils.autoCommit = false;
		Product2 p = TestUtils.createProduct();
		p.AllowsGroupDataSharing__c = 'No';
		insert p;
		p.VF_Product__c = vfp.Id;
		p.productCode = '1231234';
		p.Family = 'MBB';
		update p;
		TestUtils.autoCommit = true;
		TestUtils.createPricebookEntry(Test.getStandardPricebookId(), p);

		String strOliJSON = '[{';
		strOliJSON += '"fwaId": "9876",';
		strOliJSON += '"templateId": "1234",';
		strOliJSON += '"quantity": "1",';
		strOliJSON += '"type": "New",';
		strOliJSON += '"resourceCat": "097",';
		strOliJSON += '"connectionType": "Data",';
		strOliJSON += '"baseplan": "BO_TEST" },';
		strOliJSON += '{"fwaId": "9876",';
		strOliJSON += '"templateId": "1234",';
		strOliJSON += '"quantity": "2",';
		strOliJSON += '"type": "Portin",';
		strOliJSON += '"resourceCat": "06",';
		strOliJSON += '"connectionType": "Data",';
		strOliJSON += '"baseplan": "BO_TEST" }]';
		Test.startTest();
		EMP_AddmobileProductController.createOLI(strOliJSON, opp.Id);
		List<OpportunityLineItem> lstAssert = [
			SELECT Id
			FROM OpportunityLineItem
			WHERE Opportunity.Id = :opp.Id
		];
		System.assertEquals(2, lstAssert.size(), 'Opportunity Line Items should be created');
		Test.stopTest();
	}

	@isTest
	public static void testOLICreation2() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		OrderType__c ot = TestUtils.createOrderType();
		TestUtils.autoCommit = true;
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			null,
			owner
		);
		VF_Product__c vfp = new VF_Product__c(
			Name = 'tst',
			Family__c = 'Voice',
			Product_Line__c = 'fVodafone',
			ExternalID__c = 'VFP-02-1234567',
			AllowsGroupDataSharing__c = 'No',
			ProductCode__c = 'DUMMYADDMOBILEPRODUCT_VOICE',
			OrderType__c = ot.Id
		);
		insert vfp;
		TestUtils.autoCommit = false;
		Product2 p = TestUtils.createProduct();
		p.AllowsGroupDataSharing__c = 'No';
		insert p;
		p.VF_Product__c = vfp.Id;
		p.productCode = '123123';
		p.Family = 'Voice';
		update p;
		TestUtils.autoCommit = true;
		TestUtils.createPricebookEntry(Test.getStandardPricebookId(), p);

		String strOliJSON = '[{';
		strOliJSON += '"fwaId": "9876",';
		strOliJSON += '"templateId": "1234",';
		strOliJSON += '"quantity": "1",';
		strOliJSON += '"type": "New",';
		strOliJSON += '"resourceCat": "097",';
		strOliJSON += '"connectionType": "Voice",';
		strOliJSON += '"baseplan": "BO_TEST" },';
		strOliJSON += '{"fwaId": "9876",';
		strOliJSON += '"templateId": "1234",';
		strOliJSON += '"quantity": "2",';
		strOliJSON += '"type": "Portin",';
		strOliJSON += '"resourceCat": "06",';
		strOliJSON += '"connectionType": "Voice",';
		strOliJSON += '"baseplan": "BO_TEST" }]';
		Test.startTest();
		EMP_AddmobileProductController.createOLI(strOliJSON, opp.Id);
		List<OpportunityLineItem> lstAssert = [
			SELECT Id
			FROM OpportunityLineItem
			WHERE Opportunity.Id = :opp.Id
		];
		System.assertEquals(2, lstAssert.size(), 'Opportunity Line Items should be created');
		Test.stopTest();
	}

	@isTest
	public static void testOLICreation3() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		OrderType__c ot = TestUtils.createOrderType();
		TestUtils.autoCommit = true;
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			null,
			owner
		);
		opp.Segment__c = 'SoHo';
		opp.Select_Journey__c = 'Add Mobile Product';
		opp.Type_of_Service__c = 'Mobile';
		update opp;
		VF_Product__c vfp = new VF_Product__c(
			Name = 'tst',
			Family__c = 'Voice',
			Product_Line__c = 'fVodafone',
			ExternalID__c = 'VFP-02-1234567',
			AllowsGroupDataSharing__c = 'No',
			ProductCode__c = 'DUMMYADDMOBILEPRODUCT_VOICE',
			OrderType__c = ot.Id
		);
		insert vfp;
		TestUtils.autoCommit = false;
		Product2 p = TestUtils.createProduct();
		p.AllowsGroupDataSharing__c = 'No';
		insert p;
		p.VF_Product__c = vfp.Id;
		p.productCode = '123123';
		p.Family = 'Voice';
		update p;
		TestUtils.autoCommit = true;
		TestUtils.createPricebookEntry(Test.getStandardPricebookId(), p);

		String strOliJSON = '[{';
		strOliJSON += '"fwaId": "9876",';
		strOliJSON += '"templateId": "1234",';
		strOliJSON += '"quantity": "1",';
		strOliJSON += '"type": "New",';
		strOliJSON += '"resourceCat": "097",';
		strOliJSON += '"connectionType": "Voice",';
		strOliJSON += '"baseplan": "BO_TEST" },';
		strOliJSON += '{"fwaId": "9876",';
		strOliJSON += '"templateId": "1234",';
		strOliJSON += '"quantity": "2",';
		strOliJSON += '"type": "Portin",';
		strOliJSON += '"resourceCat": "06",';
		strOliJSON += '"connectionType": "Voice",';
		strOliJSON += '"baseplan": "BO_TEST" }]';
		Test.startTest();
		EMP_AddmobileProductController.createOLI(strOliJSON, opp.Id);
		List<OpportunityLineItem> lstAssert = [
			SELECT Id
			FROM OpportunityLineItem
			WHERE Opportunity.Id = :opp.Id
		];
		System.assertEquals(2, lstAssert.size(), 'Opportunity Line Items should be created');
		Test.stopTest();
	}
}