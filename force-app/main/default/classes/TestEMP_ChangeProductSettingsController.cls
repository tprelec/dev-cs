@isTest
public class TestEMP_ChangeProductSettingsController {
    @isTest 
    public static void testAssetsRetrieval() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        VF_Asset__c vfAsset = TestUtils.createVFAsset(acct);
        TestUtils.autoCommit = true;
        vfAsset.Owner__c = UserInfo.getUserId();
        vfAsset.CTN__c = '31652944670';
        update vfAsset;
        Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), null, owner);

        test.startTest();
            List<VF_Asset__c> lstAssets = EMP_ChangeProductSettingsController.getVFAssetList(opp.Id, 10, 1 , null);
            System.assertEquals(1, lstAssets.Size());
        test.stopTest();
    }

    @isTest
    public static void testOLICreation(){
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        OrderType__c ot = TestUtils.createOrderType();
        TestUtils.autoCommit = true;
        Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), null, owner);
        VF_Product__c vfp = new VF_Product__c(Name = 'tst', Family__c = 'tst', Product_Line__c='fVodafone',ExternalID__c = 'VFP-02-1234567',AllowsGroupDataSharing__c= 'No', ProductCode__c = 'DUMMYUPDATEMOBILEPRODUCT', OrderType__c = ot.Id);
        insert vfp;
        TestUtils.autoCommit = false;
        Product2 p = TestUtils.createProduct();
        p.AllowsGroupDataSharing__c = 'No';
        insert p;
        p.VF_Product__c = vfp.Id;
        p.productCode = '123123';
        update p;
        TestUtils.autoCommit = true;
        PriceBookEntry pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), p);

        String strOliJSON = '[{';
        strOliJSON += '"sobjectType": "VF_Asset__c",';
        strOliJSON += '"Assigned_Product_Id__c": "9876",';
        strOliJSON += '"Priceplan_Description__c": "testing",';
        strOliJSON += '"CTN__c": "3168476584"}]';
        test.startTest();
            EMP_ChangeProductSettingsController.createOLI(strOliJSON, opp.Id);
            List<OpportunityLineItem> lstAssert = [SELECT Id from OpportunityLineItem where Opportunity.Id =: opp.Id ];
            System.assertEquals(1, lstAssert.Size());
        test.stopTest();
    }
}