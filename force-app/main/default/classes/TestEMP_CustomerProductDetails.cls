@isTest
class TestEMP_CustomerProductDetails {
	@isTest
	static void testGetErrorResponse() {
		EMP_CustomerProductDetails cpd = new EMP_CustomerProductDetails();
		EMP_CustomerProductDetails.Response response = cpd.getErrorResponse('test');

		System.assertEquals(400, response.status, 'Should be the custom hardcoded status code 400');
		System.assertEquals(1, response.error.size(), 'It should be just one error message');
		System.assert(
			response.getErrorMessages().contains('Message: test'),
			'It should contain the set error message'
		);
	}

	@isTest
	static void testGetSuccessResponse() {
		Test.setMock(HttpCalloutMock.class, new EMP_GetCustomerProductDetailsMock());

		EMP_CustomerProductDetails cpd = new EMP_CustomerProductDetails();
		EMP_CustomerProductDetails.Response response = cpd.getProductDetailsResponse(
			'1234567',
			'1234567'
		);

		System.assertNotEquals(null, response, 'Response should not be null');
		System.assertNotEquals(null, response.httpRequest, 'Must contain http request infos');
		System.assertNotEquals(null, response.httpResponse, 'Must contain http response body');
		System.assertEquals(200, response.status, 'Status code should 200');
	}

	@isTest
	static void testNoGplResponse() {
		EMP_CustomerProductDetails cpd = new EMP_CustomerProductDetails();
		String emptyJsonResponse = '{"status":200,"data":[{}],"error":[]}';

		EMP_CustomerProductDetails.Response response = (EMP_CustomerProductDetails.Response) JSON.deserialize(
			emptyJsonResponse,
			EMP_CustomerProductDetails.Response.class
		);

		EMP_CustomerProductDetails.Response checkedResponse = cpd.checkResponseForNoGlpError(
			response
		);
		System.assert(checkedResponse == response, 'Should return the same response');

		checkedResponse.error.add(new EMP_CustomerProductDetails.Error('BSL-12005', 'test'));
		checkedResponse = cpd.checkResponseForNoGlpError(checkedResponse);
		System.assert(checkedResponse.error.isEmpty(), 'Should return an empty but valid response');
	}

	@isTest
	static void usePropertiesForCoverage() {
		EMP_CustomerProductDetails.CustomerProductPricingReference cppf = new EMP_CustomerProductDetails.CustomerProductPricingReference();
		cppf.catalogPricingID = 1234567;

		EMP_CustomerProductDetails.CustomerProductDetails cpd = new EMP_CustomerProductDetails.CustomerProductDetails();
		cpd.offerName = 'offer name';
		cpd.catalogProductID = 1234567;
		cpd.catalogProductCod = 'product code';
		cpd.pricingReferenceData = new List<EMP_CustomerProductDetails.CustomerProductPricingReference>{
			cppf
		};
		cpd.childImplementedProducts = new List<EMP_CustomerProductDetails.CustomerProductDetails>{
			cpd
		};

		EMP_CustomerProductDetails.CustomerProductDetailsOutputData outputData = new EMP_CustomerProductDetails.CustomerProductDetailsOutputData();
		outputData.implementedProductData = new List<EMP_CustomerProductDetails.CustomerProductDetails>{
			cpd
		};

		EMP_CustomerProductDetails.CustomerProductDetailsData cpdData = new EMP_CustomerProductDetails.CustomerProductDetailsData();
		cpdData.getCustomerProductDetailsOutputData = outputData;

		EMP_CustomerProductDetails.Response response = new EMP_CustomerProductDetails.Response();
		response.status = 200;
		response.error = new List<EMP_CustomerProductDetails.Error>{
			new EMP_CustomerProductDetails.Error('code', 'message')
		};
		response.data = new List<EMP_CustomerProductDetails.CustomerProductDetailsData>();
		response.data.add(cpdData);

		System.assert(true, 'Nothing to assert');
	}
}