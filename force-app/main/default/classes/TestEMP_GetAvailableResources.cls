@isTest
public class TestEMP_GetAvailableResources{

    @isTest
    public static void testResponseGetAvailableBulk(){
        String allcontents = '{"status":200,"data":[{"resources":[{"number":"31652944824","type":"MSISDN","status":"AVAILABLE"},{"number":"31652944825","type":"MSISDN","status":"AVAILABLE"}],"paginationData":{"pageSize":"2","pageNumber":"1","paginationCursorInfoKey":"0338377176","numberOfRows":"836"}}],"error":[{"message" : "The allocated number [, Simkaartnummer] for  resource is not available. Please allocate new number. sourceSystem  OMS","code" : "500"}]}';

        test.startTest();
            EMP_GetAvailableResources.Response res = new EMP_GetAvailableResources.Response(allcontents);
            System.assertEquals(true, res != null, 'meaning the parse was done sucessfully');
        test.stopTest();
    }

    @isTest
    public static void testRequestGetAvailableBulk(){
        String allcontents = '{"pageSize":2,"pageNumber":1,"numberOfRows":2},"customerId":"800050352"}';
        EMP_GetAvailableResources.PaginationData bodyContent = new EMP_GetAvailableResources.PaginationData(5, 1, 'F', 5 );
        EMP_GetAvailableResources.Request requestBody = new EMP_GetAvailableResources.Request('7864525', bodyContent);
        JSONParser parser = JSON.createParser(allcontents);
        test.startTest();
            EMP_GetAvailableResources.PaginationData requestBody2 = new EMP_GetAvailableResources.PaginationData(parser);
            System.assertEquals(true, requestBody2 != null, 'meaning the parse was done sucessfully');
        test.stopTest();
    }
}