/**
 * Test class for the Einstein Discovery Service
 */
@IsTest
public class TestEinsteinDiscoveryService {
	
	private static final String OBJECT_API_NAME = Ziggo_Lead__c.sObjectType.getDescribe().getName();
	
	@IsTest
	private static void testDiscoveryPredictions() {
		Test.startTest();
			EinsteinDiscoveryService.getPrediction(OBJECT_API_NAME);
		Test.stopTest();
		
		// No assertion, no return value, no action in test mode
		
	}
	
	@TestSetup
	private static void prepareTest() {
		TestEinsteinDiscoveryUtil.discoveryTestSetup(OBJECT_API_NAME);
	}

}