/**
 * Util class to prepare the discovery test environment
 */

@IsTest
public class TestEinsteinDiscoveryUtil {
	
	private static final String COMMENT_FIELD = '',
        OUTCOME_FIELD = 'Name',
        PREDICTION_ID = '',
        PRESCRIPTION_FIELD = 'Name',
        CUSTOM_SETTING_NAME = 'p2b',
        ZIPCODE = '1000',
        PROVINCE = 'ZH';
	
	/**
	 * This sets up the test data for Einstein discovery
	 */
	public static void discoveryTestSetup(String sObjectName) {
		createCustomSettings(sObjectName);
	}
	
	/**
     * Create:
     * - ed_insights__SDDPredictionConfig__c
     */
    private static void createCustomSettings(String sObjectName) {
        ed_insights__SDDPredictionConfig__c e = new ed_insights__SDDPredictionConfig__c();
        e.ed_insights__commentary_field_api_name__c = COMMENT_FIELD;
        e.ed_insights__object_api_name__c = sObjectName;
        e.ed_insights__outcome_field_api_name__c = OUTCOME_FIELD;
        e.ed_insights__prediction_definition_id__c = PREDICTION_ID;
        e.ed_insights__prescription_field_api_name__c = PRESCRIPTION_FIELD;
        e.Name = CUSTOM_SETTING_NAME;
        insert e;
    }
    
}