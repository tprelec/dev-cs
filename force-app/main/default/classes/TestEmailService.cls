@IsTest
public with sharing class TestEmailService {
	@TestSetup
	static void makeData() {
		Account acc = TestUtils.createAccount(null);
		TestUtils.autoCommit = false;
		Contact con = TestUtils.createContact(acc);
		con.Email = 'test@test.com';
		insert con;
	}

	@IsTest
	private static void testSendEmailInvocable() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Contact con = [SELECT Id FROM Contact WHERE Email = 'test@test.com' LIMIT 1];
		Attachment att = new Attachment(
			Name = 'Test Attachment',
			body = Blob.valueOf('this is a test'),
			ContentType = 'pdf',
			IsPrivate = false,
			ParentId = acc.Id
		);
		insert att;
		EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];
		Test.startTest();
		EmailService.EmailRequest req = new EmailService.EmailRequest();
		req.recipientId = con.Id;
		req.relatedToId = acc.Id;
		req.toAddresses = new List<String>{ 'test1@test.com' };
		req.ccAddresses = new List<String>{ 'test2@test.com' };
		req.bccAddresses = new List<String>{ 'test3@test.com' };
		req.templateId = template.Id;
		req.attachmentIds = new List<Id>{ att.Id };
		EmailService.sendEmail(new List<EmailService.EmailRequest>{ req });
		Test.stopTest();
		if (EmailService.checkDeliverability()) {
			List<EmailMessage> emails = [SELECT Id FROM EmailMessage];
			System.assert(!emails.isEmpty(), 'Email should be sent');
		}
	}
}