/**
 * @description			This is the test class that contains the tests for the ExceptionHandler
 * @author				Guy Clairbois
 */
 
@isTest
private class TestExceptionHandler {
	static testMethod void testHandlingException(){
		// create a test user
		TestUtils.createAdministrator();
		
		// create test entry in custom settings
		ExceptionEmailAddress__c eea = new ExceptionEmailAddress__c();
		eea.Name = 'thetester';
		eea.EmailAddress__c = TestUtils.theAdministrator.Email;
		insert eea;
		
		List<String> stringList;
			
		test.StartTest();	
		try {
			stringList.clear();
			System.assert(false, 'Should have thrown null-pointer exception');
		} catch(NullPointerException ex){
			try {
				ExceptionHandler.handleException(ex);
				ExceptionHandler.handleException(ex.getMessage());
				ExceptionHandler.handleException('VF error',ex);
				System.assert(true);
 			} catch(Exception ex2){
				System.assert(false, 'No exceptions should have been thrown from the exception handler');
			}
		}
		test.StopTest();
	}
	
	static testMethod void testHandlingDMLException(){
		Account a = new Account();
			
		try {
			insert a;
			System.assert(false, 'Should have thrown dml exception');
		} catch(DMLException ex){
			try {
				ExceptionHandler.handleException(ex);
				System.assert(true);
 			} catch(Exception ex2){
				System.assert(false, 'No exceptions should have been thrown from the exception handler');
			}
		}
	}	
}