/**
 * @description         This is the test class for the ForecastQuotaManager. 
 * @author              Guy Clairbois
 */

@isTest
private class TestForecastQuotaManager {
	
	@isTest static void test_method_quantityAndRevenue() {
		ForecastQuotaManager fqm = new ForecastQuotaManager();
		fqm.dummyAccount.OwnerId = [Select Id From User Where IsActive = true AND ForecastEnabled = true Limit 1].Id;

		fqm.getProductFamilies(); // run this just to have it covered		
		fqm.ProductFamilySelected = 'Acq Voice';

		fqm.load();

		// set some values
		fqm.quantityExcelInput = '1 2';
		fqm.revenueExcelInput = '100 400';

		fqm.processRevenueExcelValues();
		fqm.processQuantityExcelValues();

		fqm.updateQuotas();

		// now verify
		//system.assertEquals(fqm.quotas[0].quantity.Quota,1);
		//system.assertEquals(fqm.quotas[1].revenue.Quota,400);
	}
	
	@isTest static void test_method_quantityOnly() {
		ForecastQuotaManager fqm = new ForecastQuotaManager();
		fqm.dummyAccount.OwnerId = [Select Id From User Where IsActive = true AND ForecastEnabled = true Limit 1].Id;
		fqm.ProductFamilySelected = 'Acq Voice';
		fqm.load();

		// set some values
		//fqm.quotas[0].dqa.Current_mobile_costs_monthly__c = 1;
		//fqm.quotas[1].dqa.Current_mobile_costs_monthly__c = 2;

		fqm.updateQuotas();

		// now verify
		//system.assertEquals(fqm.quotas[0].quantity.Quota,1);
		//system.assertEquals(fqm.quotas[1].quantity.Quota,2);
	}

	@isTest static void test_method_wrongInputs() {
		ForecastQuotaManager fqm = new ForecastQuotaManager();
		fqm.dummyAccount.OwnerId = [Select Id From User Where IsActive = true AND ForecastEnabled = true Limit 1].Id;
		fqm.ProductFamilySelected = 'Acq Voice';
		fqm.load();

		// set some values
		fqm.quantityExcelInput = '1,1,1';
		//fqm.quotas[1].quantityQuota = '';
		fqm.revenueExcelInput = '100 euro';
		//fqm.quotas[2].revenueQuota = '400';

		fqm.updateQuotas();

		// now verify
		system.assertEquals(fqm.quotas[0].quantity.Quota,null);
		system.assertEquals(fqm.quotas[1].revenue.Quota,null);
	}	
}