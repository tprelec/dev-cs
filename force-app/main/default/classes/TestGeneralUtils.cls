/**
 * 	@description	This class contains unit tests for the StringUtils class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestGeneralUtils {

    @isTest
    private static void testRecordTypeMap () {

        Map<String, Map<String, Id>> rtMap = GeneralUtils.recordTypeMap;
        System.assert(!rtmap.isEmpty());

        Map<String, Id> devnameToIdMap = rtMap.get('Account');
        System.assert(!devnameToIdMap.isEmpty());

        DescribeSObjectResult describeResult = SObjectType.Account;
        Set<String> devNames = describeResult.getRecordTypeInfosByDeveloperName().keySet();

        for (String devName : devnameToIdMap.keySet()) {
            System.assert(devNames.contains(devName));
        }
    }

    @isTest
    private static void testportfolioVendorMap () {
        TestUtils.createPortFolioVendorMappings();
        Map<String, String> pvMap = GeneralUtils.portfolioVendorMap;
        System.assertNotEquals(true, pvMap.isEmpty());
    }

    @isTest
    static void testUtilities () {
        User u = GeneralUtils.vodafoneUser;
        Account a = GeneralUtils.vodafoneAccount;
        Id pqId = GeneralUtils.partnerQueueId;
        Boolean ip = GeneralUtils.isProduction();
        System.assert(GeneralUtils.isProduction() != GeneralUtils.runningInASandbox(), 'Sandbox is not production');
        Boolean ipu = GeneralUtils.currentUserIsPartnerUser();
        Map<Id, User> um = GeneralUtils.UserMap;
        Map<Id, String> pitpn = GeneralUtils.ProfileIdToProfileName;
        Boolean arEqual = GeneralUtils.compareDealerCodes('0012345', '012345');

        // create partner Account + user
        Account partnerAccount = TestUtils.createPartnerAccount();
        User partnerUser = TestUtils.createPortalUser(partnerAccount);
        GeneralUtils.getPartnerUserRoleGroupIdFromUserId(partnerUser.Id);

        PBX_Type__c isdn2PBX = new PBX_Type__c(Vendor__c = 'ISDN PBX', Product_Name__c = 'Standard ISDN2');
        insert isdn2PBX;

        Id isdn2Id = GeneralUtils.defaultISDN2PBXType;
        PBX_Type__c isdn30PBX = new PBX_Type__c(Vendor__c = 'ISDN PBX', Product_Name__c = 'Standard ISDN30');
        insert isdn30PBX;

        Id isdn30Id = GeneralUtils.defaultISDN30PBXType;
        PBX_Type__c onenetPBX = new PBX_Type__c(Vendor__c = 'Onenet', Product_Name__c = 'Virtual PBX');
        insert onenetPBX;

        Id onenetId = GeneralUtils.defaultOnenetPBXType;
        u = [SELECT ID, SalesChannel__c, UserType FROM User WHERE ID = :u.ID];

        Map<String, Id> roleMap = GeneralUtils.roleNameToRoleId;
        Map<Id, Set<Id>> roleToSubRolesMap = GeneralUtils.roleIdToSubRoleIds;
        Set<Id> roleToSubRoleIds = GeneralUtils.getAllSubRoleIds(roleToSubRolesMap.keySet());
        Set<String> subroleNames = GeneralUtils.getAllSubRoleNames(roleToSubRolesMap.keySet());
        Set<Id> managerRoles = GeneralUtils.getAllSalesManagerRoleIds();
        Set<Id> allCommercialDirecorRoles = GeneralUtils.getAllCommercialDirectorRoleIds();
        Map<String, Set<String>> segmentToRoles = GeneralUtils.SegmentToRoles;
        Map<String, String> roleToMainSegment = GeneralUtils.RoleToMainSegment;
        Map<String, Set<String>> mainSegmentToRole = GeneralUtils.MainSegmentToRoles;
        Map<String, String> postalCodeToZSP = GeneralUtils.postalCodeToZSPMap;
        Boolean validId = GeneralUtils.isValidId(u.Id);

        Account acc = GeneralUtils.unifyOrphansAccount;
        acc.DUNS_Number__c = null;
        acc.KVK_Number__c = '32112332';
        acc.Unify_Ref_Id__c = '321321';
        insert acc;

        Map<Id, OrderType__c> orderTypeMap = GeneralUtils.orderTypeMap;
        Map<String, Id> orderTypeMapByName = GeneralUtils.orderTypeMapByName;
        Boolean intersacts = GeneralUtils.setsIntersect(new Set<String> { 'test', 'general' }, new Set<String> { 'utils', 'test' });
        Set<String> testSet = GeneralUtils.convertSetToLowerCase(new Set<String> { 'tEst' });
        Boolean access = GeneralUtils.checkVisualforcePageAccess('AccountChangeOwner', '');
        Boolean isSandbox = GeneralUtils.runningInASandbox();
    }

    @isTest
    private static void testGetSetFromList () {
        TestUtils.autoCommit = false;

        Account acc1 = TestUtils.createAccount(GeneralUtils.currentUser);
        acc1.Name = 'Account 1';
        acc1.Description = 'Test Account';

        Account acc2 = TestUtils.createAccount(GeneralUtils.currentUser);
        acc2.Name = 'Account 2';
        acc2.Description = 'Test Account';

        List<Account> accounts = new List<Account> { acc1, acc2 };
        insert accounts;

        System.assertEquals(2, GeneralUtils.getIDSetFromList(accounts, 'Id').size());
        System.assertEquals(2, GeneralUtils.getStringSetFromList(accounts, 'Name').size());
        System.assertEquals(1, GeneralUtils.getStringSetFromList(accounts, 'Description').size());
    }

    @isTest
    private static void testIsRecordFieldChanged () {
        TestUtils.autoCommit = false;

        Account acc1 = TestUtils.createAccount(GeneralUtils.currentUser);
        acc1.Name = 'Account 1';
        acc1.Description = 'Test Account';

        Account acc2 = TestUtils.createAccount(GeneralUtils.currentUser);
        acc2.Name = 'Account 2';
        acc2.Description = 'Test Account';

        List<Account> accounts = new List<Account> { acc1, acc2 };
        insert accounts;

        Map<Id, Account> oldAccountsMap = new Map<Id, Account>([SELECT Id, Name, Description FROM Account]);
        acc1.Description = 'updated description';

        // One field
        System.assert(!GeneralUtils.isRecordFieldChanged(acc1, oldAccountsMap, 'Name'));
        System.assert(GeneralUtils.isRecordFieldChanged(acc1, oldAccountsMap, 'Description'));

        // All fields need to be changed
        System.assert(!GeneralUtils.isRecordFieldChanged(acc1, oldAccountsMap, new List<String> {'Name', 'Description' }, true));

        // One field needs to be changed
        System.assert(GeneralUtils.isRecordFieldChanged(acc1, oldAccountsMap, new List<String> {'Name', 'Description' }, false));

        // Filter Changed
        List<Account> changed = (List<Account>)GeneralUtils.filterChangedRecords(accounts, oldAccountsMap, 'Description');
        System.assertEquals(1, changed.size());
    }

    @isTest
    private static void testGroupById () {
        Account acc1 = TestUtils.createAccount(GeneralUtils.currentUser);
        Account acc2 = TestUtils.createAccount(GeneralUtils.currentUser);

        List<Account> accounts = new List<Account> { acc1, acc2 };
        Map<Id, List<Account>> accountByOwner = (Map<Id, List<Account>>) GeneralUtils.groupByIDField(accounts, 'OwnerId');

        System.assertEquals(1, accountByOwner.keySet().size());
        System.assertEquals(true, accountByOwner.keySet().contains(GeneralUtils.currentUser.Id));
    }

    @isTest
    private static void testGetRecordTypeId () {
        Id customMethodRecTypeId = GeneralUtils.getRecordTypeIdByDeveloperName('Account', 'VF_Account');

        DescribeSObjectResult describeResult = SObjectType.Account;
        Id describeMethodRecTypeId = describeResult.getRecordTypeInfosByDeveloperName().get('VF_Account').getRecordTypeId();

        System.assertEquals(customMethodRecTypeId, describeMethodRecTypeId);
    }

    @isTest
    private static void testCreateLabelParameters () {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);

		PageReference pr = new PageReference(
            '/apex/RedirectWithVariables?object=Opportunity&nooverride=1&accid='
            + acct.Id + '&allowRTS=yes&AccountId=' + acct.Id
            + 'Name=Oppotest&CreatedDate=' + Date.today() + 'RecordType=Ziggo&cancelURL=/&retURL=/'
        );

        Test.SetCurrentPage(pr);
        Map<String, String> params = GeneralUtils.createLabelParameters(pr, new Opportunity(AccountId = acct.id));
        system.Debug('params ' + params);
        System.assert(params.containsKey('accid'));
        System.assertEquals(params.get('accid'), acct.Id);
    }

    @isTest
    private static void testGetParentPartnerUserIdsFromUserIds () {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;

        Account partnerAccount = TestUtils.createPartnerAccount();
        User partnerUser = TestUtils.createPortalUser(partnerAccount);

        List<Contact> contacts = new List<Contact> {
            new Contact(AccountId = acct.Id, LastName = 'Test 1', Userid__c = UserInfo.getUserId(), Marketing_Achiever__c = 'Nee', Marketing_Status__c = 'Uit dienst'),
            new Contact(AccountId = partnerAccount.Id, LastName = 'Test 2', Userid__c = partnerUser.Id, Marketing_Achiever__c = 'Nee', Marketing_Status__c = 'Uit dienst')
        };
        insert contacts;

        List<Dealer_Information__c> dis = new List<Dealer_Information__c> {
            new Dealer_Information__c(Contact__c = contacts[0].Id, Dealer_Code__c = '001001'),
            new Dealer_Information__c(
                Contact__c = contacts[1].Id,
                Dealer_Code__c = '001011',
                Parent_Dealer_Code__c = '001111',
                Status__c = 'Active'
            )
        };
        insert dis;

        Test.startTest();
        Map<Id, Id> userIdToParentPartnerUserIdMap = GeneralUtils.getParentPartnerUserIdsFromUserIds(new Set<Id> { owner.Id, partnerUser.Id });
        Test.stopTest();

        System.assert(userIdToParentPartnerUserIdMap.isEmpty());
    }
}