@IsTest
private class TestGetBillingArrangementsService
{
    @IsTest static void testGetBillingArrangements()
    {
        String billingCustomerId = prepareTestData();

        Map<String, String> billingMap = new Map<String, String>();

        billingMap.put('billingCustomerId', billingCustomerId);
        system.debug('##### Billing Map: ' + billingMap);

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/getbillingarrangements/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serializePretty(billingMap));
        RestContext.request = request;
        RestResponse response = new RestResponse();
        RestContext.response = response;

		Test.startTest();
        GetBillingArrangementsService.returnBillingArrangements();
        Test.stopTest();

        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(response.responsebody.tostring());
        System.assertEquals('OK', m.get('status'));
    }

    @IsTest static void testMissingBillingCustomerId()
    {
        String billingCustomerId = '';
        Map<String, String> billingMap = new Map<String, String>();
        billingMap.put('billingCustomerId', billingCustomerId);

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/getbillingarrangements/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serializePretty(billingMap));
        RestContext.request = request;
        RestResponse response = new RestResponse();
        RestContext.response = response;

		Test.startTest();
        GetBillingArrangementsService.returnBillingArrangements();
        Test.stopTest();

        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(response.responsebody.tostring());
        System.assertEquals('FAILED', m.get('status'));

        System.debug('#########' + m.get('errors'));
        List<Object> l  = (List<Object>) m.get('errors');
        Map<String, Object> a = (Map<String, Object>) l[0];
        System.assertEquals('SFEC-0001', a.get('errorCode'));
    }

    @IsTest static void testUnknownBillingCustomerId()
    {
        String billingCustomerId = '123456';
        Map<String, String> billingMap = new Map<String, String>();
        billingMap.put('billingCustomerId', billingCustomerId);

        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/getbillingarrangements/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serializePretty(billingMap));
        RestContext.request = request;
        RestResponse response = new RestResponse();
        RestContext.response = response;

		Test.startTest();
        GetBillingArrangementsService.returnBillingArrangements();
        Test.stopTest();

        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(response.responsebody.tostring());
        System.assertEquals('FAILED', m.get('status'));

        System.debug('#########' + m.get('errors'));
        List<Object> l  = (List<Object>) m.get('errors');
        Map<String, Object> a = (Map<String, Object>) l[0];
        System.assertEquals('SFEC-0002', a.get('errorCode'));
    }

    @IsTest static void testGenericError()
    {
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/getbillingarrangements/';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serializePretty('')); // Will cause conversion error
        RestContext.request = request;
        RestResponse response = new RestResponse();
        RestContext.response = response;

		Test.startTest();
        GetBillingArrangementsService.returnBillingArrangements();
        Test.stopTest();

        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(response.responsebody.tostring());
        System.assertEquals('FAILED', m.get('status'));

        System.debug('#########' + m.get('errors'));
        List<Object> l  = (List<Object>) m.get('errors');
        Map<String, Object> a = (Map<String, Object>) l[0];
        System.assertEquals('SFEC-9999', a.get('errorCode'));
    }
    
    private static String prepareTestData() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Contact c = new Contact(AccountId = acct.Id, LastName = 'Test');
        Financial_Account__c fa = TestUtils.createFinancialAccount(ban, c.Id);
        Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
        TestUtils.autoCommit = false;

        Ban__c newBan = [SELECT Id, Ban_Number__c FROM Ban__c LIMIT 1];
        String billingCustomerId = newBan.Ban_Number__c;
        return billingCustomerId;
    }
}