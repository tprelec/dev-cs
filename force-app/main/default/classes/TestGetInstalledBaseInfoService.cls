@IsTest
private class TestGetInstalledBaseInfoService {
	private static String installedBaseId;
	private static String siteId;

	@IsTest
	static void testGetInstalledBaseInfo() {
		prepareTestData();

		Map<String, Object> installedBaseMap = new Map<String, Object>{
			'installedBaseRows' => new List<Object>{
				new Map<String, Object>{ 'installedBaseId' => installedBaseId, 'siteId' => siteId },
				new Map<String, Object>{
					'installedBaseId' => installedBaseId,
					'siteId' => 'a061X000002fAHwXXX' // Unknown but valid siteId
				},
				new Map<String, Object>{
					'installedBaseId' => installedBaseId,
					'siteId' => '',
					'houseNumber' => TestUtils.theSite.Site_House_Number__c,
					'houseNumberSuffix' => TestUtils.theSite.Site_House_Number_Suffix__c,
					'postalCode' => TestUtils.theSite.Site_Postal_Code__c
				},
				new Map<String, Object>{
					'installedBaseId' => installedBaseId,
					'siteId' => '',
					'houseNumber' => TestUtils.theSite.Site_House_Number__c,
					'houseNumberSuffix' => TestUtils.theSite.Site_House_Number_Suffix__c + 'X', // Causing mismatch on suffix
					'postalCode' => TestUtils.theSite.Site_Postal_Code__c
				},
				new Map<String, Object>{ 'installedBaseId' => 'foobar', 'siteId' => siteId },
				new Map<String, Object>{
					'installedBaseId' => installedBaseId,
					'siteId' => 'foobar' // Invalid siteId for 'catch' coverage
				}
			}
		};

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/getinstalledbaseinfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(installedBaseMap));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		GetInstalledBaseInfoService.returnInstalledBaseItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('PARTIALLY FAILED', m.get('status'), 'Expected: Partially Failed');
		System.assertEquals(
			4,
			((List<Object>) m.get('errors')).size(),
			'Incorrect number of errors'
		);
	}

	private static void prepareTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		TestUtils.createCompleteContract();

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = TestUtils.theOrderType.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = TestUtils.theContract.Id;
		ord.O2C_Order__c = true;
		insert ord;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ;
		cp.VF_Contract__c = TestUtils.theContract.Id;
		cp.Billing_Status__c = 'New';
		cp.Product__c = TestUtils.theProduct.Id;
		cp.ProductCode__c = 'C106929';
		cp.Site__c = TestUtils.theSite.Id;
		insert cp;

		Customer_Asset__c ca = [SELECT Id, Installed_Base_Id__c, Site__c FROM Customer_Asset__c];
		ca.Billing_Status__c = 'Active';
		ca.Installation_Status__c = 'Active';
		update ca;

		installedBaseId = ca.Installed_Base_Id__c;
		siteId = ca.Site__c;
	}
}