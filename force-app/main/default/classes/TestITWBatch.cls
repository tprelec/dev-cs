@isTest
private class TestITWBatch {

	@isTest static void test_method_one() {

		//Data preparation
		TestUtils.autoCommit = true;

		User testuser = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(testuser);
		Ban__c ban = TestUtils.createBan(acc);
		TestUtils.createITW('388888888', 'Test Manager');
		TestUtils.createITW('399999998', 'Test User');
		TestUtils.createITW('388888888', 'Eelco');

		//Test
		Test.startTest();

		PageReference pageRef = Page.ITWBatchExecute;
		Test.setCurrentPage(pageRef);
		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new list<ITW_Account_Info__c>());
		ITWBatchExecuteController controller = new ITWBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		//Checks
		System.assertEquals(1, [SELECT Id FROM ITW_Account_Info__c where Error__c =: 'There is no corresponding Account with this Ban Number'].size());
		System.assertEquals(1, [SELECT Id FROM ITW_Account_Info__c where Error__c =: 'There is no corresponding User with this Name'].size());
		System.assertEquals(2, [SELECT Id FROM ITW_Account_Info__c].size());
		System.assertEquals(1, [select Id from AccountTeamMember where AccountId =: acc.Id and TeamMemberRole = 'ITW Sales Rep'].size());
		System.assertEquals(1, [select Id from AccountShare where RowCause = 'Team' and OpportunityAccessLevel = 'Edit'].size());
	}
}