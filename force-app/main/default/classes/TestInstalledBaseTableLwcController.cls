/**
 * @description     This is Test class for InstalledBaseTableLwcController
 * @author           Ashok Patel
 */

@isTest
private class TestInstalledBaseTableLwcController {
	private static Account acc;
	private static Opportunity opp;
	private static VF_Contract__c vfc;
	private static OrderType__c ot;
	private static Product2 prod;
	private static Site__c site;
	private static Competitor_Asset__c ca;
	private static VF_Family_Tag__c ft;
	private static PBX_Type__c pbxt;
	private static Order__c o;
	private static Contracted_Products__c cp;

	private static void createTestData() {
		Order_Form_Settings__c orderFormSetting = new Order_Form_Settings__c(
			Max_no_of_sites_in_list__c = 10
		);
		insert orderFormSetting;
		acc = TestUtils.createAccount(GeneralUtils.currentUser);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		vfc = TestUtils.createVFContract(acc, opp);
		ot = TestUtils.createOrderType();
		prod = TestUtils.createProduct();
		site = TestUtils.createSite(acc);
		ft = new VF_Family_Tag__c(Name = 'PBX_Trunking');
		insert ft;
		pbxt = new PBX_Type__c(Vendor__c = 'Onenet', Product_Name__c = 'Virtual PBX');
		insert pbxt;
		Id pabxRecordTypeId = [
			SELECT Id
			FROM RecordType
			WHERE SobjectType = 'Competitor_Asset__c' AND DeveloperName = 'PABX'
		][0]
		.Id;
		ca = new Competitor_Asset__c(
			Account__c = acc.Id,
			Site__c = site.Id,
			PBX_type__c = pbxt.Id,
			RecordTypeId = pabxRecordTypeId
		);
		insert ca;

		o = new Order__c(
			Account__c = acc.Id,
			VF_Contract__c = vfc.Id,
			OrderType__c = ot.Id,
			Status__c = 'Accepted',
			O2C_Order__c = true
		);
		insert o;

		TestUtils.createOrderValidationContractedProducts();

		OrderValidationField__c ovfo = new OrderValidationField__c(
			Name = 'Sales_Order_Id__c',
			Object__c = 'Order__c',
			Field__c = 'Sales_Order_Id__c'
		);
		insert ovfo;
	}

	@isTest
	static void testgetCustomerAssets() {
		createTestData();
		User owner = TestUtils.theAdministrator == null
			? TestUtils.createAdministrator()
			: TestUtils.theAdministrator;
		Account acct = TestUtils.theAccount == null
			? TestUtils.createAccount(owner)
			: TestUtils.theAccount;
		//	Ban__c ban = TestUtils.createBan(acct);
		TestUtils.autoCommit = false;
		string opptyId = opp.Id;
		Integer nrOfRows = 2;
		test.starttest();
		VF_Product__c p = new VF_Product__c();
		p.Name = 'Test productVF';
		p.Brand__c = 'Testing Brand';
		p.Cost_Price__c = 11;
		p.Description__c = 'This is a test product';
		p.Active__c = true;
		p.ProductCode__c = 'TEST_PRODUCT_CODE_VF';
		p.Product_Group__c = 'Priceplan';
		p.Quantity_type__c = 'Monthly';
		p.SDC_Product_matrix__c = 'Fixed - Data';
		p.Product_Line__c = 'fZiggo Only';
		p.OrderType__c = ot.id;
		p.ExternalID__c = 'VFP-02-ABCDEFG';

		insert p;
		List<Product2> products = new List<Product2>();
		for (Integer i = 0; i < nrOfRows; i++) {
			products.add(TestUtils.createProduct());
		}
		products[0].VF_Product__c = p.Id;
		products[0].ordertype__c = ot.id;
		products[1].VF_Product__c = p.Id;
		products[1].ordertype__c = ot.id;

		insert products;

		List<PricebookEntry> pbEntries = new List<PricebookEntry>();
		for (Integer i = 0; i < nrOfRows; i++) {
			pbEntries.add(
				TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i))
			);
		}
		insert pbEntries;
		Id recordTypeIdMac = Schema.SObjectType.Opportunity.getRecordTypeInfosByName()
			.get('MAC')
			.getRecordTypeId();
		External_Account__c exAccount = new External_Account__c();
		exAccount.Account__c = acct.Id;
		exAccount.External_Source__c = 'Unify';
		exAccount.External_Account_Id__c = '101010';
		insert exAccount;
		Ban__c banObj = new Ban__c(
			Account__c = acct.Id,
			Unify_Customer_Type__c = 'C',
			Unify_Customer_SubType__c = 'A',
			Name = '388888888',
			ExternalAccount__c = exAccount.Id
		);
		insert banObj;
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			banObj
		);
		opp.recordtypeid = recordTypeIdMac;
		opp.Pricebook2Id = Test.getStandardPricebookId();
		insert opp;
		Decimal totalAmount = 0.0;
		List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
		for (Integer i = 0; i < nrOfRows; i++) {
			OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(
				opp,
				pbEntries.get(i)
			);
			oppLineItems.add(oppLineItem);
			totalAmount +=
				oppLineItem.Product_Arpu_Value__c *
				oppLineItem.Duration__c *
				oppLineItem.Quantity;
		}

		insert oppLineItems;
		List<Customer_Asset__c> caList = InstalledBaseTableLwcController.getCustomerAssets(
			opptyId,
			10,
			10
		);
		List<Customer_Asset__c> caSearchList = InstalledBaseTableLwcController.searchCustomerAssetsByFields(
			'Test',
			'test',
			'test',
			'test',
			1,
			opptyId
		);
		System.assertEquals(0, caList.size(), 'test');
		System.assertEquals(0, caSearchList.size(), 'test');
		// create Contract
		VF_Contract__c c = new VF_Contract__c(Account__c = acct.Id, Opportunity__c = opp.id);

		insert c;

		Integer quantity = 4;
		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = quantity,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Product__c = products[0].Id,
			VF_Contract__c = c.Id,
			Site__c = site.Id
		);

		insert cp;
		List<string> customerassetId = new List<string>();
		customer_asset__c caObj = new customer_asset__c();
		caObj.Account__c = acct.Id;
		caObj.Site__c = site.Id;
		caObj.Product__c = products[0].Id;
		caObj.Price__c = 100;
		caObj.Discount__c = 10;
		caObj.Contract_Period__c = 10;
		caObj.Quantity__c = 1;
    caObj.External_Account__c=exAccount.Id;
		insert caObj;
		customerassetId.add(caObj.Id);
		List<String> productsId = new List<String>();
		productsId.add(caObj.Product__r.VF_Product__c);

		List<PriceBookEntry> priceBookEntryList = [
			SELECT
				Id,
				Product2.CLC__c,
				Product2.VF_Product__c,
				Product2.Family,
				Product2.Quantity_Type__c,
				Product2.Product_Line__c
			FROM PriceBookEntry
		];

		for (PriceBookEntry peObj : priceBookEntryList) {
			productsId.add(peObj.Product2.VF_Product__c);
		}

		Contracted_Products__c otherCp = new Contracted_Products__c(
			Proposition__c = 'One Net',
			Family_Condition__c = 'Test Family Condition',
			VF_Contract__c = vfc.Id,
			CLC__c = 'Acq',
			Order__c = o.Id
		);
		insert otherCp;
		List<OpportunityLineItem> oliList = InstalledBaseTableLwcController.createProduct(
			customerassetId,
			opptyId,
			productsId
		);
		System.assertEquals(1, oliList.size(), 'test');
		List<Contracted_Products__c> cpList = InstalledBaseTableLwcController.createContractedProduct(
			customerassetId,
			opptyId,
			o.Id
		);
		System.assertEquals(1, cpList.size(), 'test');
		Order__c ordderObj = InstalledBaseTableLwcController.orderDetails(o.Id);
		System.assertEquals(o.Id, ordderObj.Id, 'test');
		test.stoptest();
	}
}