@isTest
private class TestLC_NewCase_Controller {
    @isTest
    static void testfetchRecordTypeValues() {
        Test.startTest();
        List<LC_NewCase_Controller.RecordTypeRadio> recClassList = LC_NewCase_Controller.fetchRecordTypeValues('Case');
        System.assertNotEquals(0, recClassList.size());
        Test.stopTest();
    }

    @isTest
    static void testDetails() {

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
        opp.Ban__c = ban.Id;
        update opp;

        Test.startTest();
        Order__c ord = TestUtils.createOrder(contr);
        ord.Propositions__c = 'Legacy';
        ord.BEN_Number__c = '1234';
        update ord;

        Opportunity tmpOpp = LC_NewCase_Controller.fetchOpportunityDetails(opp.Id);
        Order__c tmpOrder = LC_NewCase_Controller.fetchOrderDetails(ord.Id);
        VF_Contract__c tmpContract = LC_NewCase_Controller.fetchContractVFDetails(contr.Id);
        Test.stopTest();
    }

    @isTest
    static void testBasketDetails() {
        Test.startTest();
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());

        Framework__c f = Framework__c.getOrgDefaults();
        f.Framework_Sequence_Number__c = 1;
        upsert f;

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c = opp.id;

        insert basket;
        cscfga__Product_Basket__c tmpBasket = LC_NewCase_Controller.basket(basket.Id);
        Test.stopTest();
    }

    @isTest
    static void testFetchHBO() {
        User owner = TestUtils.createAdministrator();
        System.runAs(TestUtils.theAdministrator) {
            Profile p = [SELECT ID FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
            UserRole ur = [SELECT ID, Name FROM UserRole WHERE DeveloperName = 'Network_Planner' LIMIT 1];
            User networkPlanner = TestUtils.generateTestUser('Network', 'Planner', p.Id, ur.Id);
            insert networkPlanner;
            GeneralUtils.userMap.put(networkPlanner.Id, networkPlanner);
        }
        
        Account acc = TestUtils.createAccount(owner);
        Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
        TestUtils.createSite(acc);

        Site_Postal_Check__c spc = new Site_Postal_Check__c(Access_Site_ID__c = TestUtils.theSite.Id, Access_Active__c = false, Access_Vendor__c = 'ZIGGO', Access_Result_Check__c = 'OFFNET');
        insert spc;
        User np = [SELECT Id, Name, UserRoleId, UserRole.DeveloperName FROM User WHERE FirstName = 'Network' AND LastName = 'Planner' LIMIT 1];
        HBO__c hbo = new HBO__c(hbo_account__c = TestUtils.theAccount.Id, hbo_opportunity__c = TestUtils.theOpportunity.Id, hbo_status__c = 'New', hbo_site__c = TestUtils.theSite.Id, hbo_network_planner__c = np.Id,
            hbo_delivery_time__c = 12, hbo_digging_distance__c = 100, hbo_total_costs__c = 500, hbo_redundancy_type__c = 'hbo', hbo_redundancy__c = false, hbo_availability__c = 'Yes', hbo_postal_check__c = spc.Id);
        insert hbo;
        
        Test.startTest();
        HBO__c hboCtrl = LC_NewCase_Controller.fetchHBODetails(hbo.Id);
        System.assertEquals(hboCtrl.hbo_account__c, acc.Id);
        Test.stopTest();
    }

}