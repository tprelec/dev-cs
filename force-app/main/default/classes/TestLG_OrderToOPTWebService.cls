@isTest
public with sharing class TestLG_OrderToOPTWebService {
	private static OrderType__c ot;
    private static Product2 prod;
    private static Account acc;
    private static Contact c;
	private static PricebookEntry pbe;
	private static Opportunity opp;
	private static OpportunityLineItem oli;

	private static void createTestData() {
        ot = TestUtils.createOrderType();
        prod = TestUtils.createProduct();

        acc = new Account(Name = 'Test');
        insert acc;

        c = new Contact(LastName = 'Test');
        insert c;

        pbe = new PricebookEntry(
            Pricebook2Id = Test.getStandardPricebookId(),
            Product2Id = prod.Id,
            UnitPrice = 1,
            IsActive = true
        );
        insert pbe;

        opp = new Opportunity(
            Name = 'Test',
            StageName = 'Ready For Order',
            CloseDate = Date.today(),
            Pricebook2Id = Test.getStandardPricebookId(),
            CreatedDate = Date.today()
        );
        insert opp;

        oli = new OpportunityLineItem(
            OpportunityId = opp.Id,
            OrderType__c = ot.Id,
            Product2Id = prod.Id,
            PricebookEntryId = pbe.Id,
            LG_Segment__c = 'SoHo',
            CLC__c = 'Acq',
            Duration__c = 1,
            Product_Arpu_Value__c = 1,
            Quantity = 1,
            TotalPrice = 1
        );
        insert oli;

        List<LG_OrderWebserviceVariables__c> csList = new List<LG_OrderWebserviceVariables__c>();

        LG_OrderWebserviceVariables__c stageCs = new LG_OrderWebserviceVariables__c();
        stageCs.Name = 'Opportunity_Status_for_SoHo_Products';
        stageCs.LG_Value__c = 'Ready For Order';
        csList.add(stageCs);

        LG_OrderWebserviceVariables__c segmentCs = new LG_OrderWebserviceVariables__c();
        segmentCs.Name = 'Segment_OpportunityLineItem_SoHo';
        segmentCs.LG_Value__c = 'SoHo';
        csList.add(segmentCs);

        LG_OrderWebserviceVariables__c billingCs = new LG_OrderWebserviceVariables__c();
        billingCs.Name = 'AttributeFieldName_For_BillingAccount';
        billingCs.LG_Value__c = 'BillingAccount';
        csList.add(billingCs);

        LG_OrderWebserviceVariables__c overstappenCs = new LG_OrderWebserviceVariables__c();
        overstappenCs.Name = 'Attribute_Names_Overstappen';
        overstappenCs.LG_Value__c = 'Overstappen,Current Internet Provider,ContractId,Accept Termination Fee';
        csList.add(overstappenCs);

        insert csList;

        buildLgAccount();
    }

    @isTest
    static void testGetOrdersJson() {
        createTestData();

        Site__c site = new Site__c(
            Site_Postal_Code__c = '1111AA',
            Site_Account__c = acc.Id,
            Site_Street__c = 'Test Street',
            Site_City__c = 'Utrecht',
            Site_House_Number__c = 69
        );
        insert site;

        OpportunityContactRole ocr = new OpportunityContactRole(
            OpportunityId = opp.Id,
            ContactId = c.Id,
            Role = 'Technical Contact'
        );
        insert ocr;

        LG_ProductDetail__c prodDetail = new LG_ProductDetail__c(
            LG_Opportunity__c = opp.Id,
            LG_Name__c = 'Test Pd',
            LG_Type__c = 'Bundle Detail',
            LG_Product__c = '1',
            LG_Sequence__c = 1
        );
        insert prodDetail;

        insertMockJsonData(opp.Id, c.Id, site.Id);

        Test.startTest();

        LG_OrderToOPTWebService.Response resp = LG_OrderToOPTWebService.getOrders(30, 'SoHo');

        Test.stopTest();

        System.assertEquals('Success', resp.status, 'Status should be success.');
        System.assertEquals(false, resp.orderData == null, 'Response should not be null.');
        System.assertEquals('', resp.error, 'Error should be empty.');
    }

    @isTest
    static void testGetOrdersBillingAcc() {
        createTestData();

        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
            Name = 'product basket for SoHo Test',
            cscfga__Opportunity__c = opp.Id,
            csordtelcoa__Synchronised_with_Opportunity__c = true
        );
        insert pb;

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
            Name = 'PC Name',
            cscfga__Description__c = 'PC Name',
            cscfga__Product_Family__c = 'PC Name',
            cscfga__Quantity__c = 2,
            cscfga__Unit_Price__c = 100,
            cscfga__Total_Price__c = 200,
            cscfga__Billing_Frequency__c = 12,
            cscfga__Recurrence_Frequency__c = 12,
            csordtelcoa__Hierarchy_Level__c = 0,
            cscfga__Product_Basket__c = pb.Id
        );
        insert pc;

        LG_PortingNumber__c pn = new LG_PortingNumber__c(
            LG_ProductConfiguration__c = pc.Id,
            LG_Opportunity__c = opp.Id,
            LG_DirectoryListingName__c = 'Test Name 2',
            LG_InDirectory__c = 'Yes',
            LG_PhoneNumber__c = '0403680610'
        );
        insert pn;

        csconta__Billing_Account__c ba = new csconta__Billing_Account__c(
            csconta__Billing_Channel__c = 'Paper Bill',
            csconta__Status__c = 'In Collection',
            csconta__Account__c = acc.Id,
            LG_BankAccountHolder__c = 'TestName',
            LG_PaymentType__c = 'Bank Transfer',
            LG_HouseNumber__c = '1'
        );
        insert ba;

        List<cscfga__Attribute__c> attrs = new List<cscfga__Attribute__c>();

        cscfga__Attribute__c attr1 = new cscfga__Attribute__c(
            Name = 'Overstappen',
            cscfga__Product_Configuration__c = pc.Id,
            cscfga__Is_Line_Item__c = true,
            cscfga__Display_Value__c = 'Yes'
        );
        attrs.add(attr1);

        cscfga__Attribute__c attr2 = new cscfga__Attribute__c(
            Name = 'Current Internet Provider',
            cscfga__Product_Configuration__c = pc.Id,
            cscfga__Is_Line_Item__c = true,
            cscfga__Display_Value__c = 'Yes'
        );
        attrs.add(attr2);

        cscfga__Attribute__c attr3 = new cscfga__Attribute__c(
            Name = 'ContractId',
            cscfga__Product_Configuration__c = pc.Id,
            cscfga__Is_Line_Item__c = true,
            cscfga__Display_Value__c = 'Yes'
        );
        attrs.add(attr3);

        cscfga__Attribute__c attr4 = new cscfga__Attribute__c(
            Name = 'Accept Termination Fee',
            cscfga__Product_Configuration__c = pc.Id,
            cscfga__Is_Line_Item__c = true,
            cscfga__Display_Value__c = 'Yes'
        );
        attrs.add(attr4);
        insert attrs;

        cscfga__Attribute_Field__c attrField = new cscfga__Attribute_Field__c(
            cscfga__Attribute__c = attr1.Id,
            cscfga__Value__c = ba.Id,
            Name = 'BillingAccount'
        );
        insert attrField;

        Test.startTest();

        LG_OrderToOPTWebService.Response resp = LG_OrderToOPTWebService.getOrders(30, 'SoHo');

        Test.stopTest();

        System.assertEquals('Success', resp.status, 'Status should be success.');
        System.assertEquals(false, resp.orderData == null, 'Response should not be null.');
        System.assertEquals('', resp.error, 'Error should be empty.');
    }

    @isTest
    static void testGetOrdersError() {
		createTestData();
        insertMockJsonData(opp.Id, null, null);

        Test.startTest();

        LG_OrderToOPTWebService.Response resp = LG_OrderToOPTWebService.getOrders(30, 'SoHo');

        Test.stopTest();

        System.assertEquals('Error', resp.status, 'Status should be error.');
        System.assertEquals(false, resp.error == null, 'Error should not be null.');
    }

    static void insertMockJsonData(Id oppId, Id contactId, Id siteId) {
        insert new Attachment(
            Name = 'OrderEntryData.json',
            Body = Blob.valueOf(
                '{"accountId":"0015r00000CgHNXAA3","addons":[{"bundleName":"National Calls","code":"PD-00154","id":"aFR5r0000004C9kGAE","name":"Zakelijk volop bellen Nationaal","parentProduct":"aFP5r0000008OIvGAM","parentType":"Telephony","quantity":1,"recurring":12.5,"totalRecurring":12.5,"type":"Telephony Additional"},{"bundleName":"Ziggo Movies and Series","code":"PD-00153","id":"aFR5r0000004C9GGAU","name":"Ziggo Movies and Series","parentProduct":"aFP5r0000008OIzGAM","parentType":"TV","quantity":1,"recurring":5.74,"totalRecurring":5.74,"type":"TV Channels"},{"bundleName":"Film1 + Ziggo Sport Totaal","code":"PD-00158","id":"aFR5r0000004C9EGAU","name":"Film1 + Ziggo Sport Totaal","parentProduct":"aFP5r0000008OIzGAM","parentType":"TV","quantity":1,"recurring":20.58,"totalRecurring":20.58,"type":"TV Channels"},{"bundleName":"Turks zenderpakket","code":"PD-00111","id":"aFR5r0000004C98GAE","name":"Turks zenderpakket","parentProduct":"aFP5r0000008OIzGAM","parentType":"TV","quantity":0,"recurring":6.57,"totalRecurring":0,"type":"TV Channels"},{"bundleName":"Extra Ziggo Mediabox Next","code":"PD-00098","id":"aFR5r0000004C9XGAU","name":"Ziggo CI+ Module","oneOff":0,"parentProduct":"aFP5r0000008OIzGAM","parentType":"TV","quantity":1,"recurring":4.13,"totalOneOff":0,"totalRecurring":4.13,"type":"TV Mediabox"},{"bundleName":"Gay Lifestyle","code":"PD-00162","id":"aFR5r0000004C9MGAU","name":"Gay Lifestyle","parentProduct":"aFP5r0000008OIzGAM","parentType":"TV","quantity":1,"recurring":8.22,"totalRecurring":8.22,"type":"TV Channels"},{"bundleName":"Safe Online","code":"PD-00091","id":"aFR5r0000004C99GAE","name":"Safe Online","oneOff":0,"parentProduct":"aFP5r0000008OJ2GAM","parentType":"Internet","quantity":1,"recurring":0,"totalOneOff":0,"totalRecurring":0,"type":"Internet Security"},{"bundleName":"Pin Zeker","code":"PD-00138","id":"aFR5r0000004C9OGAU","name":"Pin Zeker","oneOff":0,"parentProduct":"aFP5r0000008OJ2GAM","parentType":"Internet","quantity":1,"recurring":8.95,"totalOneOff":0,"totalRecurring":8.95,"type":"Pin Zeker"},{"bundleName":"3 smart Wifiboosters - koop","code":"3SWBBuy","id":"aFR5r0000004C9aGAE","name":"3 smart Wifiboosters - koop","oneOff":82.6,"parentProduct":"aFP5r0000008OJ2GAM","parentType":"Internet","quantity":1,"recurring":0,"totalOneOff":82.6,"totalRecurring":0,"type":"Internet Additional"},{"bundleName":"3 smart Wifiboosters","code":"3SWBRental","id":"aFR5r0000004C9fGAE","name":"3 smart Wifiboosters","oneOff":0,"parentProduct":"aFP5r0000008OJ2GAM","parentType":"Internet","quantity":1,"recurring":2.07,"totalOneOff":0,"totalRecurring":2.07,"type":"Internet Additional"}],"addressCheckResult":"On-Net","b2cInternetCustomer":false,"bundle":{"bundleName":"Zakelijk Internet Complete en Bellen en TV Start (Next)","code":"PD-00006","id":"aFQ5r0000004C99GAE","name":"Zakelijk Internet Complete+Zakelijk volop bellen Nationaal+TV Start Next","quantity":1,"recurring":59,"totalRecurring":59,"type":"Internet + Telephony + TV"},"contractTerm":"2","installation":{"assignTechnician":true,"earliestInstallationDate":"2021-06-24","preferredDate1":{"dayPeriod":"morning","selectedDate":"2021-06-25"},"preferredDate2":{"dayPeriod":"morning","selectedDate":"2021-06-26"},"preferredDate3":{"dayPeriod":"morning","selectedDate":"2021-06-27"}},"lastStep":"Installation","notes":"","offNetType":"","operatorSwitch":{"currentContractNumber":"","currentProvider":"","phone":"","potentialFeeAccepted":false,"requested":false},"opportunityId":"' +
                oppId +
                '","payment":{"bankAccountHolder":"Ivan Tadic","billingChannel":"Electronic Bill","iban":"","paymentType":""},"primaryContactId":"' +
                contactId +
                '","products":[{"id":"aFP5r0000008OIyGAM","type":"Main"},{"id":"aFP5r0000008OJ2GAM","type":"Internet"},{"id":"aFP5r0000008OIzGAM","type":"TV"},{"id":"aFP5r0000008OIvGAM","type":"Telephony"}],"promos":[{"connectionType":"On-net","contractTerms":"1;2","customerType":"New","discounts":[{"addon":"aFR5r0000004C9fGAE","id":"aFT5r000000CaRdGAK","level":"Add On","name":"Gratis 3 smart Wifiboosters","type":"Percentage","value":100},{"duration":2,"id":"aFT5r000000CaRFGA0","level":"Bundle","name":"De eerste 2 maanden 100% korting op uw Zakelijk Complete abonnement","product":"aFP5r0000008OJ2GAM","type":"Percentage","value":100},{"addon":"aFR5r0000004C9kGAE","duration":12,"id":"aFT5r000000CaRCGA0","level":"Add On","name":"12 maanden Volop Bellen National","type":"Percentage","value":100}],"id":"aFU5r0000008OLTGA2","name":"De eerste 2 maanden 100% korting op uw Zakelijk Complete abonnement plus Gratis 3 smart Wifiboosters plus 12 maanden Volop Bellen","type":"Standard"}],"siteCheck":{"availability":[{"available":true,"name":"dtv-horizon"},{"available":false,"name":"packages"},{"available":false,"name":"mobile_internet"},{"available":true,"name":"fp200"},{"available":true,"name":"internet"},{"available":true,"name":"dtv"},{"available":true,"name":"catv"},{"available":true,"name":"telephony"},{"available":false,"name":"mobile"},{"available":false,"name":"catvfee"},{"available":true,"name":"fp500"},{"available":true,"name":"fp1000"}],"city":"ROTTERDAM","footprint":"UPC","houseNumber":"100","houseNumberExt":"","status":[],"street":"LANGEGEER","zipCode":"3075JM"},"siteId":"' +
                siteId +
                '"}'
            ),
            ParentId = oppId
        );
    }

    static void buildLgAccount() {
        LG_OrderToOPTWebService.LG_Account lgAcc = new LG_OrderToOPTWebService.LG_Account();
        lgAcc.Name = 'Acc Name';
        lgAcc.CustomAccountNumber = '123';
        lgAcc.Phone = '0641234567';
        lgAcc.Fax = '0641234568';
        lgAcc.Website = 'test.test2.com';
        lgAcc.EmailAdministrativeContact = 'test@test2.com';
        lgAcc.ChamberOfCommerceNumber = '123';
        lgAcc.ExternalAccountID = '123';
        lgAcc.VisitCountry = 'NL';
        lgAcc.VisitPostalCode = '1234AB';
        lgAcc.VisitHouseNumber = '12';
        lgAcc.VisitStreet = 'Street';
        lgAcc.VisitHouseNumberExtension = 'B';
        lgAcc.VisitCity = 'Amsterdam';
        lgAcc.PostalCountry = 'NL';
        lgAcc.PostalPostalCode = '1234AB';
        lgAcc.PostalHouseNumber = '12';
        lgAcc.PostalStreet = 'Street';
        lgAcc.PostalHouseNumberExtension = 'B';
        lgAcc.PostalCity = 'Amsterdam';
    }
}