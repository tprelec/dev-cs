@isTest
private class TestLeadConvertPage {

    static testMethod void myUnitTest() {
        Account newAccount = new Account(
        	name = 'Test Account'
    	);
        Insert newAccount;
        
        Lead newLead = new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web',  
                        Status = 'Closed - Converted',
                        Town__c='testtown',
                        House_Number__c='1',
                        Salutation='Mr.',
                        Street__c='teststreet',
                        Postcode_number__c='1234',
                        Postcode_char__c='AB',
                        Visiting_Housenumber1__c=1);
        
        Insert newLead;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(newLead);
        leadConvertController leadController = new leadConvertController(stdController);
        
        leadcontroller.leadToConvert = newLead;
        
        leadController.getMyComponentController();
        leadController.getmyDescriptionComponentController();
        leadController.getmyTaskComponentController();
        leadController.getThis();
        
        PageControllerBase pgBase = new PageControllerBase();
        pgBase.getMyComponentController();
        pgBase.getmyDescriptionComponentController();
        pgBase.getmyTaskComponentController();
        pgBase.getThis();
        pgBase.getmyReminderComponentController();
        
        ComponentControllerBase compBase = new ComponentControllerBase();
        compBase.pageController = pgBase;
        compBase.pageControllerDescription = pgBase;
        compBase.pageControllerReminder = pgBase;
        compBase.pageControllerTask = pgBase;
        
        
        leadController.setComponentController(new leadConvertCoreComponentController());
        leadController.setDescriptionComponentController(new leadConvertTaskDescComponentController());
        leadController.setTaskComponentController(new leadConvertTaskInfoComponentController() );
        
        system.assert(leadController.myTaskComponentController != null);
        leadController.myTaskComponentController.taskID.Subject = 'TEST TASK';
        leadController.myTaskComponentController.taskID.Priority = 'High';
        leadController.myTaskComponentController.taskID.Status = 'Not Started';
        leadController.myTaskComponentController.taskID.ActivityDate = system.today();
        leadController.myComponentController.selectedAccount = newAccount.Id;
        leadController.myComponentController.leadConvert = newLead;
        
        
        Contact contactID = leadController.myComponentController.contactID;
        leadController.myComponentController.doNotCreateOppty = true;
        List<SelectOption> leadStatuses = leadController.myComponentController.LeadStatusOption;
        
        Opportunity opportunityID = leadController.myComponentController.opportunityID;
        //leadController.reminder = true;
        String reminderTime = leadController.myTaskComponentController.remCon.reminderTime;
        List<SelectOption> timeOptions = leadController.myTaskComponentController.remCon.ReminderTimeOption;
        leadController.myDescriptionComponentController.sendNotificationEmail = true;
        leadController.myComponentController.sendOwnerEmail = true;
        
        
        List<SelectOption> priorityOptions = leadController.myTaskComponentController.TaskPriorityOption;
        List<SelectOption> statusOptions = leadController.myTaskComponentController.TaskStatusOption;
        
        Test.StartTest();

        leadController.olbicoConvertLead();   
        leadController.PrintErrors(new List<Database.Error>());
        leadController.PrintError('Test');

        Test.StopTest();
        
        //see if the new account was created - OBSOLETE (and stupid) - ACCOUNT IS CREATED IN TEST CLASS
        /*Account [] checkAccount = [SELECT Id FROM Account WHERE Name ='Test Account' ];
        system.debug(checkAccount);
        system.assertEquals(1, checkAccount.size(), 'There was a problem converting lead to an account');
        */
        //see if the new contact was created - OBSOLETE - CONTROLLER ONLY RETURNS A PAGE REFERENCE
        /*Contact [] checkContact = [SELECT Id FROM Contact WHERE Name ='Test Lead' ];
        system.debug(checkContact);
        system.assertEquals(1, checkContact.size(), 'There was a problem converting lead to a contact');
        */
        //
        
        
    } 


    static testMethod void newKVK() {
        
        Lead newLead = new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'New KVK',  
                        Status = 'Closed - Converted',
                        Town__c='testtown',
                        House_Number__c='1',
                        Salutation='Mr.',
                        Street__c='teststreet',
                        Postcode_number__c='1234',
                        Postcode_char__c='AB',
                        Visiting_Housenumber1__c=1,
                        KVK_number__c='11111111');
        
        Insert newLead;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(newLead);
        leadConvertController leadController = new leadConvertController(stdController);
        
        leadcontroller.leadToConvert = newLead;
        
        leadController.getMyComponentController();
        leadController.getmyDescriptionComponentController();
        leadController.getmyTaskComponentController();
        leadController.getThis();
        
        leadController.setComponentController(new leadConvertCoreComponentController());
        leadController.setDescriptionComponentController(new leadConvertTaskDescComponentController());
        leadController.setTaskComponentController(new leadConvertTaskInfoComponentController() );
        
        system.assert(leadController.myTaskComponentController != null);
        leadController.myTaskComponentController.taskID.Subject = 'TEST TASK';
        leadController.myTaskComponentController.taskID.Priority = 'High';
        leadController.myTaskComponentController.taskID.Status = 'Not Started';
        leadController.myTaskComponentController.taskID.ActivityDate = system.today();
        leadController.myComponentController.leadConvert = newLead;              
        leadController.myDescriptionComponentController.sendNotificationEmail = true;
        leadController.myComponentController.sendOwnerEmail = true;
        
        Test.StartTest();
        leadController.olbicoConvertLead();   
        Test.StopTest(); 
    }     
}