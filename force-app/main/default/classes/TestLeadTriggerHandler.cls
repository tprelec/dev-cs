@isTest
private class TestLeadTriggerHandler {
	@testSetup
	private static void setupTestData() {
		TestUtils.autoCommit = false;

		ID csaDetailsRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName()
			.get('CSADetails')
			.getRecordTypeId();

		Lead leadTest = TestUtils.createLead();
		leadTest.RecordTypeId = csaDetailsRecordTypeId;
		leadTest.Company = 'Test Lead QueueUser';
		insert leadTest;

		Lead leadTestKvk = TestUtils.createLead();
		leadTestKvk.RecordTypeId = csaDetailsRecordTypeId;
		leadTestKvk.Company = 'Test Lead QueueUser KVK';
		leadTestKvk.KVK_number__c = '88446622';
		insert leadTestKvk;

		Account accountTest = TestUtils.createAccount(GeneralUtils.currentUser);
		accountTest.KVK_number__c = '88446622';
		insert accountTest;
	}

	@isTest
	static void testInsertLead() {
		TestUtils.autoCommit = false;
		Lead theLead = TestUtils.createLead();

		theLead.OwnerId = GeneralUtils.partnerQueueId;
		theLead.Phone = '  0203475845 ';
		theLead.Fax = '     0203475845';
		theLead.MobilePhone = '0603475845';
		theLead.Direct_Phone_Number__c = '0603475845';
		insert theLead;

		List<LeadShare> leadshare = [SELECT Id FROM LeadShare WHERE LeadId = :theLead.Id];
		System.assertEquals(leadshare.size(), 1, 'Lead should have one share record.');

		Lead postLead = [
			SELECT Id, Phone, Fax, Visiting_postalcode_merged_input__c
			FROM Lead
			WHERE Id = :theLead.Id
		];
		System.assertEquals(
			'0203475845',
			postLead.Phone,
			'Phone number should be clean of trailing whitespace.'
		);
		System.assertEquals(
			'0203475845',
			postLead.Fax,
			'Fax should be clean of trailing whitespace.'
		);
	}

	@isTest
	static void testAssignLead() {
		Account ultParent = TestUtils.createAccount(GeneralUtils.currentUser);
		Contact currContact = new Contact(
			LastName = 'CurrentUser',
			UserId__c = UserInfo.getUserId(),
			AccountId = [SELECT Id FROM Account LIMIT 1]
			.Id
		);
		insert currContact;
		Dealer_Information__c currDealer = new Dealer_Information__c(
			Contact__c = currContact.Id,
			Name = 'CurrentDealerInfo',
			Dealer_Code__c = '888888'
		);
		insert currDealer;

		TestUtils.autoCommit = false;
		Lead theLead = TestUtils.createLead();
		theLead.KVK_number__c = '43243424';
		theLead.Ultimate_Parent_Account__c = ultParent.Id;
		theLead.OwnerId = GeneralUtils.currentUser.Id;
		theLead.Workflow_Cancel__c = true;
		theLead.LeadSource = 'New KVK';

		Test.startTest();
		insert theLead;
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(theLead.Id);
		lc.setOwnerId(GeneralUtils.currentUser.Id);
		lc.setConvertedStatus(
			[SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1]
			.MasterLabel
		);
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		Test.stopTest();

		List<Account> accLst = [SELECT Id FROM Account];
		System.assertEquals(3, accLst.size(), 'There should be three accounts now.');
		System.assert(lcr.isSuccess(), 'Lead should have been converted.');
	}

	@isTest
	static void testAssignVGELead() {
		Account ultParent = TestUtils.createAccount(GeneralUtils.currentUser);

		Contact currContact = new Contact(
			LastName = 'CurrentUser',
			UserId__c = UserInfo.getUserId(),
			AccountId = [SELECT Id FROM Account LIMIT 1]
			.Id
		);
		insert currContact;
		Dealer_Information__c currDealer = new Dealer_Information__c(
			Contact__c = currContact.Id,
			Name = 'CurrentDealerInfo',
			Dealer_Code__c = '888888'
		);
		insert currDealer;

		TestUtils.autoCommit = false;
		Lead theLead = TestUtils.createLead();
		theLead.KVK_number__c = '43243424';
		theLead.VGE__c = true;
		theLead.OwnerId = GeneralUtils.currentUser.Id;
		theLead.Workflow_Cancel__c = true;
		theLead.LeadSource = 'New KVK';

		Test.startTest();
		insert theLead;
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(theLead.Id);
		lc.setOwnerId(GeneralUtils.currentUser.Id);
		lc.setConvertedStatus(
			[SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1]
			.MasterLabel
		);
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		Test.stopTest();

		List<Account> accLst = [SELECT Id FROM Account];
		System.assertEquals(3, accLst.size(), 'There should be three accounts now.');
		System.assert(lcr.isSuccess(), 'Lead should have been converted.');
		System.assertNotEquals(null, ultParent, 'Ultimate parent record should exist.');
	}

	@isTest
	static void testAssignLeadEmployeeConcern() {
		Account ultParent = TestUtils.createAccount(GeneralUtils.currentUser);
		Contact cvm = new Contact(
			LastName = 'DealerLastName',
			AccountId = ultParent.Id,
			UserId__c = GeneralUtils.currentUser.Id
		);
		insert cvm;
		Dealer_Information__c cvmDealer = new Dealer_Information__c(
			Contact__c = cvm.Id,
			Name = 'DealerInfoCVM',
			Dealer_Code__c = Label.Dealer_Code_CVM_Owner
		);
		insert cvmDealer;
		Contact cm = new Contact(
			LastName = 'DealerLastName',
			AccountId = ultParent.Id,
			UserId__c = GeneralUtils.currentUser.Id
		);
		insert cm;
		Dealer_Information__c cmDealer = new Dealer_Information__c(
			Contact__c = cm.Id,
			Name = 'DealerInfoCM',
			Dealer_Code__c = Label.Dealer_Code_CM_Park_Owner
		);
		insert cmDealer;
		Contact currContact = new Contact(
			LastName = 'CurrentUser',
			UserId__c = UserInfo.getUserId(),
			AccountId = [SELECT Id FROM Account LIMIT 1]
			.Id
		);
		insert currContact;
		Dealer_Information__c currDealer = new Dealer_Information__c(
			Contact__c = currContact.Id,
			Name = 'CurrentDealerInfo',
			Dealer_Code__c = '888888'
		);
		insert currDealer;

		TestUtils.autoCommit = false;
		Lead theLead = TestUtils.createLead();
		theLead.KVK_number__c = '43243424';
		theLead.No_of_Employees_Concern__c = 60;
		theLead.OwnerId = GeneralUtils.currentUser.Id;
		theLead.Workflow_Cancel__c = true;
		theLead.LeadSource = 'New KVK';

		Test.startTest();
		insert theLead;
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(theLead.Id);
		lc.setOwnerId(GeneralUtils.currentUser.Id);
		lc.setConvertedStatus(
			[SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1]
			.MasterLabel
		);
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		Test.stopTest();

		List<Account> accLst = [SELECT Id FROM Account];
		System.assertEquals(3, accLst.size(), 'There should be three accounts now.');
		System.assert(lcr.isSuccess(), 'Lead should have been converted.');
	}

	@isTest
	static void testAssignLeadEmployee() {
		Account ultParent = TestUtils.createAccount(GeneralUtils.currentUser);
		Account parteneracc = TestUtils.createPartnerAccount();
		Contact cvm = new Contact(
			LastName = 'DealerLastName',
			AccountId = ultParent.Id,
			UserId__c = GeneralUtils.currentUser.Id
		);
		insert cvm;
		Dealer_Information__c cvmDealer = new Dealer_Information__c(
			Contact__c = cvm.Id,
			Name = 'DealerInfoCVM',
			Dealer_Code__c = Label.Dealer_Code_CVM_Owner
		);
		insert cvmDealer;
		Contact cm = new Contact(
			LastName = 'DealerLastName',
			AccountId = ultParent.Id,
			UserId__c = GeneralUtils.currentUser.Id
		);
		insert cm;
		Dealer_Information__c cmDealer = new Dealer_Information__c(
			Contact__c = cm.Id,
			Name = 'DealerInfoCM',
			Dealer_Code__c = Label.Dealer_Code_CM_Park_Owner
		);
		insert cmDealer;
		Contact currContact = new Contact(
			LastName = 'CurrentUser',
			UserId__c = UserInfo.getUserId(),
			AccountId = [SELECT Id FROM Account LIMIT 1]
			.Id
		);
		insert currContact;
		Dealer_Information__c currDealer = new Dealer_Information__c(
			Contact__c = currContact.Id,
			Name = 'CurrentDealerInfo',
			Dealer_Code__c = '888888'
		);
		insert currDealer;

		TestUtils.autoCommit = false;
		Lead theLead = TestUtils.createLead();
		theLead.KVK_number__c = '43243424';
		theLead.NumberOfEmployees = 40;
		theLead.OwnerId = GeneralUtils.currentUser.Id;
		theLead.Workflow_Cancel__c = true;
		theLead.LeadSource = 'New KVK';
		theLead.Visiting_postalcode_merged_input__c = '1234 AB';
		theLead.Postcode_number__c = '1234AB';
		theLead.Preferred_Partner__c = parteneracc.Id;
		theLead.Approved_by_Partner_Manager__c = true;

		Test.startTest();
		try {
			insert theLead;
		} catch (Exception e) {
			System.debug(System.LoggingLevel.INTERNAL, e);
		}
		theLead.Approved_by_Partner_Manager__c = false;
		insert theLead;
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(theLead.Id);
		lc.setOwnerId(GeneralUtils.currentUser.Id);
		lc.setConvertedStatus(
			[SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1]
			.MasterLabel
		);
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		Test.stopTest();

		List<Account> accLst = [SELECT Id FROM Account];
		System.assertEquals(4, accLst.size(), 'There should be four accounts now.');
		System.assert(lcr.isSuccess(), 'Lead should have been converted.');
	}

	@isTest
	private static void testPopulateQueueUserAssignedOnAccountOnLeadConversion() {
		Lead testLead = [SELECT Id, OwnerId FROM Lead WHERE Company = 'Test Lead QueueUser'];
		LeadStatus convertStatus = [
			SELECT Id, MasterLabel
			FROM LeadStatus
			WHERE IsConverted = TRUE
		];

		Database.leadConvert lc = new Database.leadConvert();
		lc.setLeadId(testLead.Id);
		lc.setConvertedStatus(convertStatus.MasterLabel);

		Test.startTest();

		Database.LeadConvertResult lcr = Database.convertLead(lc);

		Test.stopTest();

		Lead resultLead = [SELECT Id, ConvertedAccountId FROM Lead WHERE Id = :testLead.Id];
		List<Account> lstAccount = [
			SELECT Id, Queue_User_Assigned__c
			FROM Account
			WHERE Id = :resultLead.ConvertedAccountId
		];

		System.assert(lcr.isSuccess(), 'Lead should be converted.');
		System.assert(lstAccount.size() == 1, 'There should be a new account created.');
		System.assertNotEquals(
			null,
			lstAccount[0].Queue_User_Assigned__c,
			'The Queue/User sharing should be populated on the Account record.'
		);
	}

	@isTest
	private static void testPopulateQueueUserAssignedOnAccountWithSameKVK() {
		TestUtils.autoCommit = false;
		Lead testLead = [SELECT Id, OwnerId FROM Lead WHERE KVK_number__c = '88446622'];

		Test.startTest();

		System.runAs(TestUtils.createAdministrator()) {
			testLead.OwnerId = UserInfo.getUserId();
			update testLead;
		}

		Test.stopTest();

		Lead resultLead = [SELECT Id, OwnerId FROM Lead WHERE KVK_number__c = '88446622'];
		List<Account> lstAccount = [
			SELECT Id, Queue_User_Assigned__c, Queue_User_Assigned__r.OwnerId
			FROM Account
			WHERE KVK_number__c = '88446622'
		];

		System.assert(lstAccount.size() == 1, 'There should be a new account created.');
		System.assertNotEquals(
			null,
			lstAccount[0].Queue_User_Assigned__c,
			'The Queue/User sharing should be populated on the Account record.'
		);
		System.assertEquals(
			resultLead.OwnerId,
			lstAccount[0].Queue_User_Assigned__r.OwnerId,
			'The Lead record Owner must be equal to the Queue/Use record Owner assigned to the Account.'
		);
	}

	@isTest
	private static void testKvkUpdate() {
		TestUtils.autoCommit = false;

		Account testAcc = [SELECT Id, KVK_number__c FROM Account WHERE KVK_number__c = '88446622'];
		Lead testLead = [
			SELECT Id, OwnerId, KVK_number__c
			FROM Lead
			WHERE KVK_number__c = '88446622'
		];

		testLead.KVK_number__c = '12345678';
		testLead.Account_name__c = testAcc.Id;
		update testLead;

		Lead postUpLead = [SELECT Id, OwnerId, KVK_number__c FROM Lead WHERE Id = :testLead.Id];
		Account acc = [SELECT Id, KVK_number__c FROM Account WHERE Id = :testAcc.Id];

		system.assertEquals('88446622', testAcc.KVK_number__c, 'Account kvk should stay the same.');
		system.assertEquals(
			'88446622',
			postUpLead.KVK_number__c,
			'Kvk nr. on lead should be same as the one on Account.'
		);
	}
}