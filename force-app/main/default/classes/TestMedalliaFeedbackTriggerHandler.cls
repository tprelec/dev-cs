@isTest
private class TestMedalliaFeedbackTriggerHandler {
    @isTest
    static void runFirst() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact con = TestUtils.createContact(acct);

        List<Medallia_Feedback__c> mfList = new List<Medallia_Feedback__c>();
        Medallia_Feedback__c mf = new Medallia_Feedback__c();
        mf.Account_Id__c = acct.Id;
        mf.Contact_Id__c = con.Id;
        mf.Record_Status__c = 'Completed';
        mfList.add(mf);
        insert mfList;

        Test.startTest();

        Account acc = [Select Total_NPS_responses__c From Account Where Id =: acct.Id];
        System.AssertEquals(1, acc.Total_NPS_responses__c);

        Test.stopTest();
    }

    @isTest
    static void runUpdate() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact con = TestUtils.createContact(acct);

        List<Medallia_Feedback__c> mfList = new List<Medallia_Feedback__c>();
        Medallia_Feedback__c mf = new Medallia_Feedback__c();
        mf.Account_Id__c = acct.Id;
        mf.Contact_Id__c = con.Id;
        mf.Record_Status__c = 'Completed';
        mfList.add(mf);

        Medallia_Feedback__c mf2 = new Medallia_Feedback__c();
        mf2.Account_Id__c = acct.Id;
        mf2.Contact_Id__c = con.Id;
        mf2.Record_Status__c = 'Invited';
        mfList.add(mf2);
        insert mfList;

        Test.startTest();

        Account acc = [Select Total_NPS_responses__c From Account Where Id =: acct.Id];
        System.AssertEquals(1, acc.Total_NPS_responses__c);
        for (Medallia_Feedback__c mfTmp : mfList) {
            mfTmp.Record_Status__c = 'Completed';
        }
        update mfList;

        acc = [Select Total_NPS_responses__c From Account Where Id =: acct.Id];
        System.AssertEquals(2, acc.Total_NPS_responses__c);

        Test.stopTest();
    }

    @isTest
    static void runDelete() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact con = TestUtils.createContact(acct);

        List<Medallia_Feedback__c> mfList = new List<Medallia_Feedback__c>();
        Medallia_Feedback__c mf = new Medallia_Feedback__c();
        mf.Account_Id__c = acct.Id;
        mf.Contact_Id__c = con.Id;
        mf.Record_Status__c = 'Completed';
        mfList.add(mf);
        insert mfList;

        Test.startTest();

        Account acc = [Select Total_NPS_responses__c From Account Where Id =: acct.Id];
        System.AssertEquals(1, acc.Total_NPS_responses__c);

        delete mfList;

        acc = [Select Total_NPS_responses__c From Account Where Id =: acct.Id];
        System.AssertEquals(0, acc.Total_NPS_responses__c);

        Test.stopTest();
    }
}