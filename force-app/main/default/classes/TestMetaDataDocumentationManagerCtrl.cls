@isTest
private class TestMetaDataDocumentationManagerCtrl {
	
	@isTest static void test_method_one() {

		// setup webservice mock
		System.Test.setMock(WebServiceMock.class, new TestMetadataService.WebServiceMockImpl());

		// load the page with the Account object
		Test.setCurrentPage(page.MetaDataDocumentationManager);
		MetaDataDocumentationManagerController mddmc = new MetaDataDocumentationManagerController();

		// update a field label
		mddmc.fieldList[0].theLabel = 'updatedLabel';
		//mddmc.fieldList[1].theLabel = 'updatedLabel2';
		mddmc.updateAll();

		mddmc.exportToCsv();

		system.debug(mddmc.fieldList);

		mddmc.importCsvName = 'thefile.csv';
		mddmc.importCsvBody = Blob.valueOf('test1,test2,test3,test4 \n value1,value2,value3,value4');
		mddmc.importFromCsv();


	}
	
	@isTest static void test_method_two() {
		// load the page with the BAN__c object


		// export a csv


		// import a csv


		// do an update
	}
	
}