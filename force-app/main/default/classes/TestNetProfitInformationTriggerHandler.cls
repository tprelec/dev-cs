@IsTest
public class TestNetProfitInformationTriggerHandler {
	@TestSetup
	static void makeData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;
		Order__c order = TestUtils.createOrder(TestUtils.theContract);
		List<Contracted_Products__c> cps = getContractedProducts();
		Contracted_Products__c cp = cps[0];
		cp.Quantity__c = 2;
		cp.UnitPrice__c = 10;
		cp.Arpu_Value__c = 10;
		cp.Duration__c = 1;
		cp.Order__c = order.Id;
		cp.Product_Family__c = 'MBB';
		cp.Deal_Type__c = 'New';
		update cp;
	}

	@IsTest
	static void testCopyOrderData() {
		Order__c order = getOrder();
		Test.startTest();

		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);
		Test.stopTest();
		npi = getNPI();
		System.assertEquals(
			order.VF_Contract__r.Opportunity__r.BAN__c,
			npi.BAN__c,
			'BAN should be copied from parent Order'
		);
		System.assertEquals(
			order.Account__c,
			npi.Customer__c,
			'Account should be copied from parent Order'
		);
	}

	@IsTest
	static void testGenerateCTNsSuccess() {
		Order__c order = getOrder();

		Test.startTest();

		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);

		Test.stopTest();

		List<NetProfit_CTN__c> ctns = getCTNs();
		System.assertEquals(2, ctns.size(), 'Number of new CTNs should match Quantity');
		for (NetProfit_CTN__c ctn : ctns) {
			System.assertEquals(
				npi.Id,
				ctn.NetProfit_Information__c,
				'CTN should be linked to NetProfit Information'
			);
			System.assertEquals('Future', ctn.CTN_Status__c, 'CTN Status should be Future');
			System.assertEquals('New', ctn.Action__c, 'CTN Status should be New');
			System.assertEquals(
				'Data',
				ctn.Price_Plan_Class__c,
				'CTN Price Plan Class should be Data'
			);
			System.assert(
				ctn.CTN_Number__c.startsWith('3197'),
				'CTN Number should start with 3197'
			);
		}
	}

	@IsTest
	static void testGenerateCTNsConnectionTypeError() {
		Order__c order = getOrder();

		List<Contracted_Products__c> cps = getContractedProducts();
		Contracted_Products__c cp = cps[0];
		cp.Product_Family__c = 'X';
		update cp;

		Test.startTest();

		String errMsg;
		try {
			TestUtils.createNetProfitInformation(order);
		} catch (Exception ex) {
			errMsg = ex.getMessage();
		}

		Test.stopTest();

		System.assert(
			errMsg != null && errMsg.contains('Invalid connection type found for order'),
			'Invalid Connection Type error should be thrown.'
		);
	}

	@IsTest
	static void testGenerateCTNsMissingPricePlanError() {
		Order__c order = getOrder();

		List<Contracted_Products__c> cps = getContractedProducts();
		Contracted_Products__c cp = cps[0];
		Product2 product = new Product2(Id = cp.Product__c, Product_Group__c = 'Addons');
		update product;

		Test.startTest();

		String errMsg;
		try {
			TestUtils.createNetProfitInformation(order);
		} catch (Exception ex) {
			errMsg = ex.getMessage();
		}

		Test.stopTest();

		System.assert(
			errMsg != null && errMsg.contains('Missing priceplan for order'),
			'Missing priceplan error should be thrown.'
		);
	}

	private static Order__c getOrder() {
		return [SELECT Id, VF_Contract__r.Opportunity__r.BAN__c, Account__c FROM Order__c LIMIT 1];
	}

	private static VF_Contract__c getContract() {
		return [SELECT Id FROM VF_Contract__c LIMIT 1];
	}

	private static List<Contracted_Products__c> getContractedProducts() {
		return [SELECT Id, Site__c, Product__c FROM Contracted_Products__c];
	}

	private static NetProfit_Information__c getNPI() {
		return [SELECT Id, BAN__c, Customer__c FROM NetProfit_Information__c LIMIT 1];
	}

	private static List<NetProfit_CTN__c> getCTNs() {
		return [
			SELECT
				Id,
				NetProfit_Information__c,
				CTN_Status__c,
				Action__c,
				Price_Plan_Class__c,
				CTN_Number__c
			FROM NetProfit_CTN__c
		];
	}
}