@isTest
private class TestNewApprovalsEmailController {

    @isTest
    static void createUpdateApproval() {

        User owner = TestUtils.createAdministrator();

        Account acct = TestUtils.createAccount(owner);

        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );

        Framework__c f = Framework__c.getOrgDefaults();
        f.Framework_Sequence_Number__c = 1;
        upsert f;



        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c=opp.id;
        basket.Name = 'New Basket '+system.now();
        insert basket;



        appro__Approval_Path__c testpath = new appro__Approval_Path__c();
        insert testpath;

        appro__Approval_Step__c teststep = new  appro__Approval_Step__c();
        teststep.appro__Approval_Path__c = testPath.Id;
        teststep.Approver_gets_edit_rights__c = true;
        teststep.IsPendingUsageTrigger__c = true;
        insert teststep;

        appro__Approval_Request__c testRequest = new appro__Approval_Request__c();
        testRequest.appro__RID__c = basket.Id;
        insert testRequest;


        appro__approval__c testapproval = new appro__approval__c();
        testapproval.appro__Approval_Request__c = testRequest.Id;
        testapproval.appro__Approval_Step__c = testStep.Id;
        testapproval.appro__Approver_User__c = UserInfo.getUserId();
        testapproval.appro__Status__c = 'New';
        insert testapproval;

        test.startTest();

        testapproval.appro__Status__c = 'Assigned';
        update testapproval;

        appro__Email_Activity__c emailAct = new appro__Email_Activity__c();
        emailAct.appro__Approval_Request__c = testRequest.Id;
        emailAct.appro__Approval__c = testapproval.Id;
        insert emailAct;

        NewApprovalsEmailController contr = new NewApprovalsEmailController();
        contr.entryId = emailAct.Id;
        appro__Email_Activity__c returnval = contr.emailActivity;

        String ctnOrSip = contr.ctnOrSip;
        String ownerName = contr.ownerName;
        String teamName = contr.teamName;
        String urlRecord = contr.urlRecord;
        List<appro__Email_Activity_Item__c> actList = contr.approvalList;
        test.stopTest();
    }

}