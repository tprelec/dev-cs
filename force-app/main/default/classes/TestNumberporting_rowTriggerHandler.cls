/**
 * @description		This is the test class for the Numberporting_rowTriggerHandler
 * @author        	Guy Clairbois
 */
 @isTest
private class TestNumberporting_rowTriggerHandler {
	
	@isTest
	static void test_create() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        //Pricebook2 pricebook = TestUtils.createPricebook(true);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
		VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
		Order__c ord = TestUtils.createOrder(contr);

		Test.startTest();

		Numberporting__c np = new Numberporting__c();
		np.Order__c = ord.Id;
		np.Customer__c = acct.Id;
		np.Name = 'testnp12345';
		insert np;
		
		Numberporting_row__c npr = new Numberporting_row__c();
		npr.Numberporting__c = np.Id;
		npr.Action__c = 'Porting_in';
		npr.Block_type__c = '10-block';
        npr.Name_on_bill__c = 'abc';		
		npr.Number_block__c = '011111111x';
		insert npr;

		// now try to insert an overlapping block
		Numberporting_row__c npr2 = npr.clone(false,true);
		try{
			insert npr2;
		} catch (dmlException de){}

		Test.stopTest();

		List<Numberporting_row__c> testResult = [Select Id From Numberporting_row__c Where Numberporting__c = :np.Id];
		system.assertEquals(1,testResult.size());
		

	}
	

}