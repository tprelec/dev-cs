@isTest
private class TestOlbicoCdp {

    static testMethod void validateOlbicoCdpController () {
        Account testAccount = new Account();
        ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount);
        OlbicoCdpController olbicoCdpController = new OlbicoCdpController(stdController);
        
        System.assertNotEquals(olbicoCdpController.getLanguage(), null);
        System.assertNotEquals(olbicoCdpController.getToken(), null);
        System.assertNotEquals(olbicoCdpController.getParams(), '*');
        
    }
    
    static testMethod void validateOlbicoNewAccountRedirectController() {
        Account testAccount = new Account();
        ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount);
        OlbicoNewAccountRedirectController testController = new OlbicoNewAccountRedirectController(stdController);
        
        System.assertNotEquals(testController.Redirect(), null);
    }
}