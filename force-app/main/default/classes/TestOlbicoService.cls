@isTest
public class TestOlbicoService {
    
    @isTest 
    static void testMakeRequestRestJson() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout()); 
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        OlbicoServiceJSON testClass = new OlbicoServiceJSON();
        //create OlbicoSettings__c mapping
        List<OlbicoSettings__c> OlbicoSettings = new List<OlbicoSettings__c>();
        OlbicoSettings.add(new OlbicoSettings__c(Olbico_JSon_Endpoint__c = 'https://api.dqbox.com/api/search', Olbico_Json_Username__c = 'usr_vod2325sitf', Olbico_Json_Password__c = 'testpass', Olbico_Json_BAG_Streets__c = 'ef62f95b-30fc-4520-9be1-8af222e57ef6'));
        Insert OlbicoSettings;
        //create BAG_Mapping__c mapping
        List<BAG_Mapping__c> BAGmappingList = new List<BAG_Mapping__c>();
        BAGmappingList.add(new BAG_Mapping__c(Account_Field__c = 'Name', Name = 'bedrijfsnaam'));
        BAGmappingList.add(new BAG_Mapping__c(Account_Field__c = 'NumberOfEmployees', Name = 'aantalmedewerkers'));
        BAGmappingList.add(new BAG_Mapping__c(Account_Field__c = 'KVK_number__c', Name = 'kvk_8'));
        BAGmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'FirstName', Name = 'bevoegd_functionaris_voornaam'));
        BAGmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'LastName', Name = 'bevoegd_functionaris_achternaam'));
        BAGmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'MobilePhone', Name = 'mobielnummer'));
        Insert BAGmappingList;
        test.startTest();
        testClass.setRequestRestJson('12345678'); //Param is a kvk code
        testClass.makeReqestRestJson('12345678'); //Param is a kvk code
        test.stopTest();
    }
    @isTest 
    static void testMakeRequestRestJsonStreet() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout()); 
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        OlbicoServiceJSON testClass = new OlbicoServiceJSON();
        //create OlbicoSettings__c mapping
        List<OlbicoSettings__c> OlbicoSettings = new List<OlbicoSettings__c>();
        OlbicoSettings.add(new OlbicoSettings__c(Olbico_JSon_Endpoint__c = 'https://api.dqbox.com/api/search', Olbico_Json_Username__c = 'usr_vod2325sitf', Olbico_Json_Password__c = 'testpass', Olbico_Json_BAG_Streets__c = 'ef62f95b-30fc-4520-9be1-8af222e57ef6'));
        Insert OlbicoSettings;
        test.startTest();
        testClass.setRequestRestJsonStreet('1234ab'); //Param is a zip code
        testClass.makeRequestRestJsonStreet('1234ab'); //Param is a zip code
        test.stopTest();
    }
}