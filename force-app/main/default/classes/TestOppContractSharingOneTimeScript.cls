@isTest
public class TestOppContractSharingOneTimeScript {
    @isTest static void check(){
        TestUtils.createCompleteContract();

        Test.startTest();
        OppContractSharingOneTimeScript x = new OppContractSharingOneTimeScript();
        database.executeBatch(x);
        Test.stopTest();
    }
}