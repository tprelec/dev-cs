@isTest
private class TestOpportunityClosedLostController {
	
	@isTest()	
	static void test_direct_sales() {
    	PageReference pageRef = Page.OpportunityClosedWon;
   		Test.setCurrentPage(pageRef);
   		TestUtils.createCompleteOpportunity();

   		Test.startTest();
   		Apexpages.StandardController sc = new Apexpages.standardController(TestUtils.theOpportunity);
   		OpportunityClosedLostController controller = new OpportunityClosedLostController(sc);	

   		controller.cancelClosedLost();

   		controller.confirmClosedLost();
   		// again to trigger 'already closed' error   		
   		controller.confirmClosedLost();   		

   		Test.stopTest();
	}
	
	@isTest static void test_indirect_sales() {
		// Implement test code
	}
	
}