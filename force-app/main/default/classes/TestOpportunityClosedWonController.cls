@isTest
@SuppressWarnings('pmd')
private class TestOpportunityClosedWonController {
	@isTest
	static void testDirectSales() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		Contact c = new Contact(
			LastName = 'TestName',
			AccountId = TestUtils.theAccount.Id,
			Authorized_to_sign__c = true
		);
		insert c;

		PageReference pageRef = Page.OpportunityClosedWon;
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController sc = new ApexPages.StandardController(
			TestUtils.theOpportunity
		);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);

		Test.startTest();
		controller.theOpp.Confirm_Mixed_Opportunity__c = true;
		controller.confirmMixedOpportunity();
		controller.createNewContact();
		controller.loadContacts();
		controller.saveQuickNewContact();
		controller.QuickNewContact.LastName = 'QuickCreate';
		controller.saveQuickNewContact();
		controller.checkNoneSelected();
		System.assert(controller.noneSelected);
		for (OpportunityClosedWonController.ContactWrapper cw : controller.contacts) {
			cw.selected = true;
		}
		controller.ccToCurrentUser = true;
		try {
			controller.sendWelcomeEmail(controller.contacts, true, TestUtils.theOpportunity.Id);
		} catch (Exception e) {
		}
		controller.checkNoneSelected();
		System.assert(!controller.noneSelected);

		controller.confirmMissingType();
		controller.cancelClosedWon();
		controller.confirmClosedWon();
		Boolean oppHasZiggoProduct = controller.oppHasZiggoProduct;
		controller.sendMessageToParentWindowToCloseFrame();
		Boolean customerReferenceOptional = controller.customerReferenceOptional;
		System.assertEquals(
			controller.templateId,
			[SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Welcome_Email_EBU']
			.Id
		);
		controller.backToForm();
		controller.getBaseUrl();
		controller.getContentUrl();
		// again to trigger 'already closed' error
		controller.confirmClosedWon();

		Test.stopTest();
	}

	@isTest
	static void testClosingBySAG() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		User accManager;
		System.runAs(GeneralUtils.currentUser) {
			accManager = new User(
				Id = TestUtils.theAccountManager.Id,
				UserRoleId = [SELECT ID FROM UserRole WHERE Name = 'MLE Sales Support' LIMIT 1]
				.Id
			);
			update accManager;
		}

		Test.startTest();
		PageReference pageRef = Page.OpportunityClosedWon;
		pageRef.getParameters().put('Id', String.valueOf(TestUtils.theOpportunity.Id));
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController sc = new ApexPages.StandardController(
			TestUtils.theOpportunity
		);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
		System.runAs(accManager) {
			controller.confirmClosingBySAG();
			System.assertEquals(controller.theOpp.StageName, 'Closing By SAG');
			controller.confirmClosingBySAG();
			System.assertEquals(controller.theOpp.StageName, 'Closed Won');
		}
		controller.theOpp.StageName = 'Closing';
		controller.theOpp.Primary_Basket__c = null;
		controller.confirmClosingBySAG();
		System.assertEquals(controller.theOpp.StageName, 'Closing By SAG');
		Test.stopTest();
	}

	@isTest
	static void testLightningClose() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOpportunityFieldMappings();
		OrderType__c ot = TestUtils.createOrderType();
		Product2 p = TestUtils.createProduct();
		PriceBookEntry pbEntry = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), p);
		VF_Product__c vfp = new VF_Product__c(
			Name = 'tst',
			Family__c = 'tst',
			Product_Line__c = 'fVodafone',
			OrderType__c = ot.Id,
			ExternalID__c = 'VFP-02-1234567'
		);
		insert vfp;
		p.VF_Product__c = vfp.Id;
		p.productCode = '123123';
		update p;

		TestUtils.autoCommit = false;
		Account partnerAcc = TestUtils.createPartnerAccount();
		partnerAcc.Frame_Work_Agreement__c = '24234235235';
		partnerAcc.Total_Licenses__c = 5;
		insert partnerAcc;
		External_Account__c externalAccountObj = new External_Account__c();
		externalAccountObj.Account__c = partnerAcc.Id;
		externalAccountObj.External_Account_Id__c = '178';
		externalAccountObj.External_Source__c = 'BOP';
		insert externalAccountObj;
		External_Account__c externalAccount = new External_Account__c();
		externalAccount.Account__c = partnerAcc.Id;
		externalAccount.External_Account_Id__c = '678';
		externalAccount.External_Source__c = 'Unify';
		// externalAccount.Related_External_Account__c=externalAccountObj.Id;
		insert externalAccount;

		BAN__c ban = new BAN__c();
		ban.Account__c = partnerAcc.Id;
		ban.Unify_Customer_Type__c = 'C';
		ban.Unify_Customer_SubType__c = 'A';
		ban.Name = '388888889';
		// ban.BAN_Status__c = 'Closed';
		ban.Dealer_Code__c = '543654';
		ban.ExternalAccount__c = externalAccount.Id;
		insert ban;
		/* Ban__c ban = TestUtils.createBan(partnerAcc);
        ban.Dealer_Code__c = '543654';
        insert ban;*/
		TestUtils.autoCommit = true;

		Site__c site = TestUtils.createSite(partnerAcc);

		User u = TestUtils.createPortalUser(partnerAcc);

		Test.startTest();
		// run as portal user
		System.runAs(u) {
			TestUtils.autoCommit = false;
			Opportunity opp = TestUtils.createOpportunityWithBan(
				partnerAcc,
				Test.getStandardPricebookId(),
				null,
				u
			); //ban
			insert opp;
			OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp, pbEntry);
			oppLineItem.Site_List__c = site.Id;
			oppLineItem.BigMachines__Synchronization_Id__c = null;
			oppLineItem.Source_system__c = 'Manual';
			insert oppLineItem;
			TestUtils.autoCommit = true;

			PageReference pageRef = Page.OpportunityClosedWon;
			pageRef.getParameters().put('Id', String.valueOf(opp.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.StandardController sc = new ApexPages.StandardController(opp);
			OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
			TestUtils.isPartnerCommunity = true;
			controller.cancelClosedWon();
			controller.confirmMixedOpportunity();
			controller.confirmClosingBySAG();

			partnerAcc.Channel__c = 'BP';
			update partnerAcc;
			ApexPages.StandardController sc2 = new ApexPages.StandardController(opp);
			OpportunityClosedWonController controller2 = new OpportunityClosedWonController(sc2);
			controller2.confirmClosingBySAG();
			controller2.showClosedWonWithEmail = false;
			controller2.confirmClosedWon();
			controller2.showClosedWonWithEmail = true;
			controller2.dummyContact = new Contact();
			controller2.dummyContact.Welcome_Email_Date__c = System.today() - 1;
			controller2.confirmClosedWon();
			Boolean hasMsg = false;
			for (ApexPages.Message msg : ApexPages.getMessages()) {
				if (msg.getDetail() == ('Please specify the solution sales contact')) {
					hasMsg = true;
				}
			}
			System.assert(hasMsg);
		}
		Test.stopTest();
	}

	@isTest
	static void testLightningCloseSAG() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		insert TestUtils.ov('Opportunity60', 'Opportunity', 'Vodafone_Products__c');
		TestUtils.theOpportunity.Confirm_Mixed_Opportunity__c = true;
		update TestUtils.theOpportunity;
		User accManager = new User(
			Id = TestUtils.theAccountManager.Id,
			ManagerId = TestUtils.theAdministrator.Id,
			UserRoleId = [SELECT ID FROM UserRole WHERE Name = 'MLE Sales Support' LIMIT 1]
			.Id
		);
		System.runAs(GeneralUtils.currentUser) {
			List<User> users = new List<User>();
			users.add(
				new User(
					Id = TestUtils.theAdministrator.Id,
					Signature_Document_Id__c = '3452345235'
				)
			);
			users.add(
				new User(Id = TestUtils.theManager.Id, Signature_Document_Id__c = '3452345234')
			);
			users.add(accManager);
			update users;
		}

		PageReference pageRef = Page.OpportunityClosedWon;
		pageRef.getParameters().put('Id', String.valueOf(TestUtils.theOpportunity.Id));
		Test.setCurrentPage(pageRef);
		Test.startTest();
		System.runAs(accManager) {
			ApexPages.StandardController sc = new ApexPages.StandardController(
				TestUtils.theOpportunity
			);
			OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
			TestUtils.isPartnerCommunity = true;
			controller.confirmClosingBySAG();
			System.assertEquals(controller.theOpp.StageName, 'Closing By SAG');
			controller.confirmClosedWon();
			System.assertEquals(controller.theOpp.StageName, 'Closed Won');
			controller.confirmClosingBySAG();
			Boolean hasMsg = false;
			for (ApexPages.Message msg : ApexPages.getMessages()) {
				if (msg.getDetail() == ('')) {
					hasMsg = true;
				}
			}
			System.assert(hasMsg);
		}
		Test.stopTest();
	}

	@isTest
	static void testLightningCloseWon() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		PageReference pageRef = Page.OpportunityClosedWon;
		Test.setCurrentPage(pageRef);
		Test.startTest();
		TestUtils.theOpportunity.No_Contract__c = true;
		TestUtils.theOpportunity.Confirm_Mixed_Opportunity__c = true;
		TestUtils.theOpportunity.Escalation__c = false;
		update TestUtils.theOpportunity;
		ApexPages.StandardController sc = new ApexPages.StandardController(
			TestUtils.theOpportunity
		);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
		TestUtils.isPartnerCommunity = true;
		controller.confirmClosedWon();
		System.assertEquals(controller.theOpp.StageName, 'Closed Won');
		// System.assertEquals(controller.theOpp.StageName, 'Closing');
		Test.stopTest();
	}

	@isTest
	static void testControllerError() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		PageReference pageRef = Page.OpportunityClosedWon;
		pageRef.getParameters().put('Id', String.valueOf(TestUtils.theOpportunity.Id));
		pageRef.getParameters().put('showBooleans', '1');
		pageRef.getParameters().put('isClosed', '1');
		Test.setCurrentPage(pageRef);
		Test.startTest();
		TestUtils.theOpportunity.No_Contract__c = true;
		TestUtils.theOpportunity.Escalation__c = false;
		TestUtils.theOpportunity.Confirm_Mixed_Opportunity__c = true;
		TestUtils.theOpportunity.StageName = 'Closing by SAG';
		update TestUtils.theOpportunity;
		ApexPages.StandardController sc = new ApexPages.StandardController(
			TestUtils.theOpportunity
		);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
		controller.confirmClosingBySAG();
		controller.confirmClosedWon();
		Boolean hasMsg = false;
		for (ApexPages.Message msg : ApexPages.getMessages()) {
			if (msg.getDetail().contains('Only Inside Sales can close this opportunity')) {
				hasMsg = true;
			}
		}
		System.assert(hasMsg);
		Test.stopTest();
	}

	@isTest
	static void testControllerNoOlis() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOpportunityFieldMappings();
		User owner = TestUtils.createAdministrator();
		TestUtils.createAccount(owner);
		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Closing',
			CloseDate = system.today(),
			AccountId = TestUtils.TheAccount.Id,
			Type = 'New Business',
			Bespoke__c = false,
			Pricebook2Id = Test.getStandardPricebookId(),
			Project_Comments__c = 'Test opp without opp lines'
		);
		insert opp;
		PageReference pageRef = Page.OpportunityClosedWon;
		pageRef.getParameters().put('Id', String.valueOf(opp.Id));
		Test.setCurrentPage(pageRef);
		Test.startTest();
		ApexPages.StandardController sc = new ApexPages.StandardController(opp);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
		controller.confirmClosingBySAG();
		System.assertEquals(controller.theOpp.StageName, 'Closed Won');
		//  System.assertEquals(controller.theOpp.StageName, 'Closing');
		System.assert(controller.showClosingBySAGMessage);
		Test.stopTest();
	}
}