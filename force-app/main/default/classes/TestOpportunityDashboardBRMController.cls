/**
 * 	@description	This class contains unit tests for the OpportunityDashboardBRMController class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestOpportunityDashboardBRMController {


	@isTest
	static void test_method_one() {
		system.runas(TestUtils.createAccountManager()){
			OpportunityDashboardBRMController odbc = new OpportunityDashboardBRMController();

            odbc.initializeVariables();
			odbc.getRunAsUserOptions();
			odbc.getDataTypeOptions();
			odbc.getVerticalOptions();
			odbc.getRunasUserLabel();

			//In order to make this test pass in any sandbox without inserting data
			GeneralUtils.MainSegmentToRoles.put('Solution Sales', new Set<String>{'Cloud Productivity Consultant'});

			String test1 = odbc.mainAcqDatapointsJSON;

			odbc.dataTypeSelected = 'CTN';
			String test2 = odbc.mainRetDatapointsJSON ;

			odbc.dataTypeSelected = 'TCV';

			// change top 10 selection
			odbc.fiscalQuarterSelected = '1';
			odbc.verticalSelected = 'Health';

			odbc.getFiscalYearOptions();
			odbc.activateSecondaryTabs();
			odbc.getDashboardTypeOptions();
			odbc.loadDashboard();
		}
	}

	@isTest
	static void generateDatapointList() {
		system.runas(TestUtils.createAccountManager()){
			OpportunityDashboardBRMController odbc = new OpportunityDashboardBRMController();
			TestUtils.createCompleteOpportunity();

			String queryString = 'SELECT CLC__c, PricebookEntry.Product2.Taxonomy__r.Product_Family__c, Opportunity.StageName, ' +
				'FISCAL_YEAR(Opportunity.CloseDate) fy, FISCAL_QUARTER(Opportunity.CloseDate) fq, SUM(MRR1__c) SumAmount, ' +
				'Opportunity.RoleCreateOrChangeOwner__c ' +
				'FROM OpportunityLineItem ' +
				'GROUP BY CLC__c, PricebookEntry.Product2.Taxonomy__r.Product_Family__c, Opportunity.StageName, ' +
				'FISCAL_YEAR(Opportunity.CloseDate), FISCAL_QUARTER(Opportunity.CloseDate), Opportunity.RoleCreateOrChangeOwner__c';
			odbc.generateDatapointList(queryString, ''); //in the method itself, a limit is added if a test is running
		}
	}

	@isTest
	static void addTypeToMap() {
		OpportunityDashboardBRMController odbc = new OpportunityDashboardBRMController();
		OpportunityDashboardBRMController.Datapoint testDatapoint = new OpportunityDashboardBRMController.Datapoint();
		testDatapoint.Amount = 100;
		String type = 'Prospect';
		Map<String, OpportunityDashboardBRMController.Datapoint> inputMap = new Map<String, OpportunityDashboardBRMController.Datapoint>{type => testDatapoint};

		System.assertEquals(
			100,
			odbc.addTypeToMap(
				type,
				testDatapoint,
				new Map<String, OpportunityDashboardBRMController.Datapoint>{type => testDatapoint}
			).get('Coverage').Amount
		);

		type = 'Qualify';
		System.assertEquals(
			100,
			odbc.addTypeToMap(
				type,
				testDatapoint,
				new Map<String, OpportunityDashboardBRMController.Datapoint>{type => testDatapoint}
			).get('Coverage').Amount
		);

		type = 'Offer';
		System.assertEquals(
			200,
			odbc.addTypeToMap(
				type,
				testDatapoint,
				new Map<String, OpportunityDashboardBRMController.Datapoint>{type => testDatapoint}
			).get('Coverage').Amount
		);

		type = 'Closing';
		System.assertEquals(
			200,
			odbc.addTypeToMap(
				type,
				testDatapoint,
				new Map<String, OpportunityDashboardBRMController.Datapoint>{type => testDatapoint}
			).get('Coverage').Amount
		);

		type = 'Closed Won';
		System.assertEquals(
			400,
			odbc.addTypeToMap(
				type,
				testDatapoint,
				new Map<String, OpportunityDashboardBRMController.Datapoint>{type => testDatapoint}
			).get('Closed Won').Amount
		);

		type = 'Target';
		System.assertEquals(
			-800,
			odbc.addTypeToMap(
				type,
				testDatapoint,
				new Map<String, OpportunityDashboardBRMController.Datapoint>{type => testDatapoint}
			).get('Gap').Amount
		);

		type = 'Closed Lost';
		System.assertEquals(
			1600,
			odbc.addTypeToMap(
				type,
				testDatapoint,
				new Map<String, OpportunityDashboardBRMController.Datapoint>{type => testDatapoint}
			).get('Closed Lost').Amount
		);
	}
}