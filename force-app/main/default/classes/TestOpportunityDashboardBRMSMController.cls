/**
 * 	@description	This class contains unit tests for the OpportunityDashboardBRMSMController class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestOpportunityDashboardBRMSMController {

	@isTest
	static void test_method_two() {
		system.runas(TestUtils.createAccountManager()){

			// run as sales mgr to prevent heap size error!
			OpportunityDashboardBRMSMController odbc = new OpportunityDashboardBRMSMController();

			odbc.initializeVariables();
			odbc.getRunAsUserOptions();
			odbc.getDataTypeOptions();
			odbc.getVerticalOptions();
			odbc.getRunasUserLabel();

			String test1 = odbc.mainAcqDatapointsJSON;

			odbc.dataTypeSelected = 'CTN';
			String test2 = odbc.mainRetDatapointsJSON ;

			odbc.dataTypeSelected = 'TCV';
			String test3 = odbc.solSalesDatapointsJSON ;

			String test4 = odbc.itwDatapointsJSON ;
			String test5 = odbc.msDatapointsJSON ;
			String test6 = odbc.miteDatapointsJSON ;
			String test7 = odbc.accountManagerSelected;

			odbc.fiscalQuarterSelected = '1';
			odbc.verticalSelected = 'Health';


			odbc.getFiscalYearOptions();
			odbc.activateSecondaryTabs();
			odbc.getAccountManagerOptions();
			odbc.getDashboardTypeOptions();
			odbc.loadDashboard();
		}
	}

	@isTest
	static void generateDatapointList() {
		system.runas(TestUtils.createAccountManager()){
			OpportunityDashboardBRMSMController odbc = new OpportunityDashboardBRMSMController();
			TestUtils.createCompleteOpportunity();

			String queryString = 'SELECT CLC__c, PricebookEntry.Product2.Taxonomy__r.Product_Family__c, Opportunity.StageName,' +
				'FISCAL_YEAR(Opportunity.CloseDate) fy, ' +
				'FISCAL_QUARTER(Opportunity.CloseDate) fq, ' +
				'SUM(TotalPrice) SumAmount,' +
				'Opportunity.Solution_Sales__r.Name AccountManager ' +
				'FROM OpportunityLineItem ' +
				'GROUP BY CLC__c,PricebookEntry.Product2.Taxonomy__r.Product_Family__c,Opportunity.StageName, ' +
				'FISCAL_YEAR(Opportunity.CloseDate),FISCAL_QUARTER(Opportunity.CloseDate),Opportunity.Solution_Sales__r.Name';
			odbc.generateDatapointList(queryString, ''); //in the method itself, a limit is added if a test is running
		}
	}
}