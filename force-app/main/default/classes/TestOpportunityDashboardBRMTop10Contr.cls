/**
 * 	@description	This class contains unit tests for the OpportunityDasboardBRMTop10Controller class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestOpportunityDashboardBRMTop10Contr {
	
	
	@isTest(seeAllData=true)
	static void test_method_one() {

		OpportunityDasboardBRMTop10Controller odc = new OpportunityDasboardBRMTop10Controller();
		odc.fiscalYearSelected = String.valueOf(system.today().year());
		odc.fiscalQuarterSelected = '1';
		odc.clc = 'Acq';
		odc.stageType = 'Closed Won';
		odc.visibleRoleNames = new List<String>();
		odc.getCurrenttop10();
		
	}
}