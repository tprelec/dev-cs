/**
 * 	@description	This class contains unit tests for the OpportunityDashboardUtils class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestOpportunityDashboardUtils {
	
	
	@isTest(seeAllData=true)
	static void test_method_one() {
		Test.setCurrentPageReference(new PageReference('Page.OpportunityDashboardDetailReport'));

		System.currentPageReference().getParameters().put('filters', 'Type=Commit');
		System.currentPageReference().getParameters().put('inclusions', 'Type=Commit');
		System.currentPageReference().getParameters().put('exclusions', 'Type=Funnel');
		System.currentPageReference().getParameters().put('fiscalYear', String.valueOf(system.today().addmonths(-12).year()));
		System.currentPageReference().getParameters().put('soqlFilter', ' FROM OpportunityLineItem WHERE Opportunity.StageName != \'Closed Lost\'  AND FISCAL_YEAR(Opportunity.CloseDate) =  :fiscalYearInteger AND Product_Family__c NOT IN :hiddenProductFamilies AND Opportunity.Owner.UserType != \'PowerPartner\' AND (PricebookEntry.Product2.CLC__c = \'Acq\'  OR PricebookEntry.Product2.CLC__c = null) AND Opportunity.RoleCreateOrChangeOwner__c in :visibleRoleNames');



		OpportunityDashboardUtils odu = new OpportunityDashboardUtils();

		List<OpportunityLineItem> oppLineItems = odu.oppLineItems;
	}
}