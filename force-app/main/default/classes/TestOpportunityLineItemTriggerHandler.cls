@isTest
private class TestOpportunityLineItemTriggerHandler {
	private static Account acc;
	private static Opportunity opp;
	private static VF_Contract__c vfc;
	private static OrderType__c ot;
	private static Product2 prod;
	private static PriceBookEntry pbe;
	private static Site__c site;
	private static Order__c o;
	private static OpportunityLineItem oli;

	private static void createTestData() {
		acc = TestUtils.createAccount(GeneralUtils.currentUser);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		vfc = TestUtils.createVFContract(acc, opp);
		ot = TestUtils.createOrderType();
		prod = TestUtils.createProduct();
		pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
		site = TestUtils.createSite(acc);

		o = new Order__c(
			Account__c = acc.Id,
			VF_Contract__c = vfc.Id,
			OrderType__c = ot.Id,
			Status__c = 'Accepted',
			O2C_Order__c = true
		);
		insert o;

		OrderValidationField__c ovfo = new OrderValidationField__c(
			Name = 'Sales_Order_Id__c',
			Object__c = 'Order__c',
			Field__c = 'Sales_Order_Id__c'
		);
		insert ovfo;
	}

	@isTest
	static void testCalculateFields() {
		createTestData();

		Test.startTest();

		oli = new OpportunityLineItem(
			OpportunityId = opp.Id,
			Product2Id = prod.Id,
			PricebookEntryId = pbe.Id,
			Duration__c = 1,
			Product_Arpu_Value__c = 1,
			Quantity = 5,
			TotalPrice = 1,
			Gross_List_Price__c = 10,
			DiscountNew__c = 2,
			Cost__c = 3,
			Net_Revenue__c = 55,
			Net_Unit_Price__c = 10,
			Proposition__c = 'Company Services',
			Model_Number_Identifier__c = 'Test MNI'
		);
		insert oli;

		Test.stopTest();

		OpportunityLineItem resultOli = [
			SELECT
				Gross_Revenues__c,
				DiscountPerProduct__c,
				ContributionMargin__c,
				Contribution_Margin__c,
				Net_Revenue__c,
				Proposition_Type__c,
				Model_Number__c
			FROM OpportunityLineItem
			WHERE Id = :oli.Id
		];

		System.assertEquals(50, resultOli.Gross_Revenues__c, 'Gross_Revenues__c should be 50.');
		System.assertEquals(1, resultOli.DiscountPerProduct__c, 'Gross_Revenues__c should be 1.');
		System.assertEquals(
			46,
			resultOli.ContributionMargin__c,
			'ContributionMargin__c should be 46.'
		);
		System.assertEquals(
			92,
			resultOli.Contribution_Margin__c,
			'Contribution_Margin__c should be 92.'
		);
		System.assertEquals(50, resultOli.Net_Revenue__c, 'Net_Revenue__c should be 50.');
		System.assertEquals(
			'COMPANYLEVELSERVICES',
			resultOli.Proposition_Type__c,
			'Proposition_Type__c should be COMPANYLEVELSERVICES.'
		);
		System.assertEquals(1, resultOli.Model_Number__c, 'Model_Number__c should be 1.');
	}

	@isTest
	static void testSetClc() {
		createTestData();

		Test.startTest();

		oli = new OpportunityLineItem(
			OpportunityId = opp.Id,
			Product2Id = prod.Id,
			PricebookEntryId = pbe.Id,
			Duration__c = 1,
			Product_Arpu_Value__c = 1,
			Quantity = 1,
			TotalPrice = 1,
			CLC__c = 'Acq'
		);
		insert oli;

		Test.stopTest();

		OpportunityLineItem resultOli = [SELECT CLC__c FROM OpportunityLineItem WHERE Id = :oli.Id];

		System.assertEquals('Acq', resultOli.CLC__c, 'CLC__c should be Acq.');
	}

	@isTest
	static void testCopySiteToSiteList() {
		createTestData();

		Test.startTest();

		oli = new OpportunityLineItem(
			OpportunityId = opp.Id,
			Product2Id = prod.Id,
			PricebookEntryId = pbe.Id,
			Duration__c = 1,
			Product_Arpu_Value__c = 1,
			Quantity = 1,
			TotalPrice = 1,
			Location__c = site.Id
		);
		insert oli;

		Test.stopTest();

		OpportunityLineItem resultOli = [
			SELECT Site_List__c
			FROM OpportunityLineItem
			WHERE Id = :oli.Id
		];

		System.assertEquals(site.Id, resultOli.Site_List__c, 'Site_List__c should be Site Id.');
	}

	@isTest
	static void testBlockClosedOppsEditingOnInsert() {
		createTestData();

		opp.StageName = 'Closed Lost';
		update opp;

		oli = new OpportunityLineItem(
			OpportunityId = opp.Id,
			Product2Id = prod.Id,
			PricebookEntryId = pbe.Id,
			Duration__c = 1,
			Product_Arpu_Value__c = 1,
			Quantity = 1,
			TotalPrice = 1
		);

		Test.startTest();

		Boolean expectedExceptionThrown = false;
		try {
			insert oli;
		} catch (Exception e) {
			expectedExceptionThrown = e.getMessage()
				.contains(
					'No edits are allowed to Opportunities with status \'Closed Won\' or \'Closed Lost\''
				);
		}
		System.assertEquals(true, expectedExceptionThrown, 'Exception should be thrown.');

		Test.stopTest();
	}

	@isTest
	static void testBlockClosedOppsEditingOnUpdate() {
		createTestData();

		Test.startTest();

		oli = new OpportunityLineItem(
			OpportunityId = opp.Id,
			Product2Id = prod.Id,
			PricebookEntryId = pbe.Id,
			Duration__c = 1,
			Product_Arpu_Value__c = 1,
			Quantity = 1,
			TotalPrice = 1
		);
		insert oli;

		oli.Group__c = '1';
		update oli;

		oli.Gross_List_Price__c = 10;
		update oli;

		oli.DiscountNew__c = 3;
		update oli;

		oli.Billing__c = true;
		update oli;

		opp.StageName = 'Closed Lost';
		update opp;

		Boolean expectedExceptionThrown = false;
		try {
			oli.Cost_Center__c = 'Test';
			update oli;
		} catch (Exception e) {
			expectedExceptionThrown = e.getMessage()
				.contains(
					'No edits are allowed to Opportunities with status \'Closed Won\' or \'Closed Lost\''
				);
		}
		System.assertEquals(true, expectedExceptionThrown, 'Exception should be thrown.');

		Test.stopTest();
	}

	@isTest
	static void testBlockClosedOppsEditingOnDelete() {
		createTestData();

		Test.startTest();

		oli = new OpportunityLineItem(
			OpportunityId = opp.Id,
			Product2Id = prod.Id,
			PricebookEntryId = pbe.Id,
			Duration__c = 1,
			Product_Arpu_Value__c = 1,
			Quantity = 1,
			TotalPrice = 1
		);
		insert oli;

		opp.StageName = 'Closed Lost';
		update opp;

		Boolean expectedExceptionThrown = false;
		try {
			delete oli;
		} catch (Exception e) {
			expectedExceptionThrown = e.getMessage()
				.contains(
					'No edits are allowed to Opportunities with status \'Closed Won\' or \'Closed Lost\''
				);
		}
		System.assertEquals(true, expectedExceptionThrown, 'Exception should be thrown.');

		Test.stopTest();
	}

	@isTest
	static void testUpdateForecastedActivationDate() {
		createTestData();

		Test.startTest();

		oli = new OpportunityLineItem(
			OpportunityId = opp.Id,
			Product2Id = prod.Id,
			PricebookEntryId = pbe.Id,
			ServiceDate = Date.newInstance(2021, 3, 2),
			Duration__c = 1,
			Product_Arpu_Value__c = 1,
			Quantity = 1,
			TotalPrice = 1,
			CLC__c = 'Acq'
		);

		Test.stopTest();

		insert oli;

		oli.ServiceDate = Date.newInstance(2021, 3, 1);
		update oli;

		Opportunity resultOpp = [
			SELECT Opportunity_Activation_Date_Forecast__c
			FROM Opportunity
			WHERE Id = :opp.Id
		];

		System.assertEquals(
			Date.newInstance(2021, 3, 1),
			resultOpp.Opportunity_Activation_Date_Forecast__c,
			'Opportunity_Activation_Date_Forecast__c should be 2020-03-01.'
		);
	}

	@isTest
	static void testSetOrderType() {
		createTestData();

		Id macOppRecordType = [
			SELECT Id
			FROM RecordType
			WHERE SobjectType = 'Opportunity' AND DeveloperName = 'MAC'
		][0]
		.Id;

		opp.RecordTypeId = macOppRecordType;
		update opp;

		Test.startTest();

		oli = new OpportunityLineItem(
			OpportunityId = opp.Id,
			Product2Id = prod.Id,
			PricebookEntryId = pbe.Id,
			Duration__c = 1,
			Product_Arpu_Value__c = 1,
			Quantity = 1,
			TotalPrice = 1,
			CLC__c = 'Acq'
		);
		insert oli;

		Test.stopTest();

		OpportunityLineItem resultOli = [
			SELECT OrderType__c
			FROM OpportunityLineItem
			WHERE Id = :oli.Id
		];

		System.assertEquals(
			ot.Id,
			resultOli.OrderType__c,
			'OrderType__c should be MAC Order Type Id.'
		);
	}

	@isTest
	static void testSetOppDealType() {
		createTestData();

		Test.startTest();

		oli = new OpportunityLineItem(
			OpportunityId = opp.Id,
			Product2Id = prod.Id,
			PricebookEntryId = pbe.Id,
			Duration__c = 1,
			Product_Arpu_Value__c = 1,
			Quantity = 1,
			TotalPrice = 1,
			CLC__c = 'Acq'
		);
		insert oli;

		oli.CLC__c = 'Ret';
		update oli;

		oli.CLC__c = 'Mig';
		update oli;

		oli.CLC__c = 'Churn';
		update oli;

		delete oli;

		Test.stopTest();

		Opportunity resultOpp = [SELECT Deal_Type__c FROM Opportunity WHERE Id = :opp.Id];
		System.assertEquals(
			'Cancellation',
			resultOpp.Deal_Type__c,
			'Deal_Type__c should be Cancellation.'
		);
	}

	@isTest
	static void testHandleFrameworkAgreementLogicByProposition() {
		GenericMock mock = new GenericMock();
		mock.returns.put('generateFrameworkAgreementIdAndInvokeFrameworkAgreementCreation', null);
		AccountService.instance = (AccountService) Test.createStub(AccountService.class, mock);
		TestUtils.createCompleteOpportunity();

		System.assertEquals(
			null,
			mock.callCount.get('generateFrameworkAgreementIdAndInvokeFrameworkAgreementCreation'),
			'mock.callCount should be null.'
		);

		//'Mock' the custom metadata type without it being in the database
		OpportunityLineItemTriggerHandler.propositionMap = new Map<String, Orderform_Proposition__mdt>{
			'TestVodafoneFrameworkAgreement' => new Orderform_Proposition__mdt(
				MasterLabel = 'TestVodafoneFrameworkAgreement',
				Framework_Agreement_Type__c = 'Vodafone'
			),
			'TestVodafoneZiggoFrameworkAgreement' => new Orderform_Proposition__mdt(
				MasterLabel = 'TestVodafoneZiggoFrameworkAgreement',
				Framework_Agreement_Type__c = 'VodafoneZiggo'
			),
			'AnyOtherProposition' => new Orderform_Proposition__mdt(
				MasterLabel = 'AnyOtherProposition',
				Framework_Agreement_Type__c = null
			)
		};

		TestUtils.autoCommit = false;
		PriceBookEntry pbe = [SELECT Id, UnitPrice FROM PriceBookEntry LIMIT 1];

		OpportunityLineItem newOli = TestUtils.createOpportunityLineItem(
			TestUtils.theOpportunity,
			pbe
		);
		newOli.Proposition__c = 'TestVodafoneFrameworkAgreement; TestVodafoneZiggoFrameworkAgreement; AnyOtherProposition';

		Test.startTest();

		insert newOli;

		System.assertEquals(
			1,
			mock.callCount.get('generateFrameworkAgreementIdAndInvokeFrameworkAgreementCreation'),
			'mock.callCount should be 1.'
		);

		Id accountId = TestUtils.theOpportunity.AccountId;

		System.assertEquals(
			new Set<Id>{ accountId },
			mock.args.get('generateFrameworkAgreementIdAndInvokeFrameworkAgreementCreation')[0],
			'mock.args[0] should contain Account Id.'
		);
		System.assertEquals(
			new Set<Id>{ accountId },
			mock.args.get('generateFrameworkAgreementIdAndInvokeFrameworkAgreementCreation')[1],
			'mock.args[1] should contain Account Id.'
		);

		OpportunityLineItem oliWithFakeFrameworkProposition = TestUtils.createOpportunityLineItem(
			TestUtils.theOpportunity,
			pbe
		);
		oliWithFakeFrameworkProposition.Proposition__c = 'gibberish';

		Exception expectedException;

		try {
			insert oliWithFakeFrameworkProposition;
		} catch (Exception e) {
			expectedException = e;
		}

		System.assert(expectedException != null, 'expectedException should not be null.');
		System.assert(
			expectedException.getMessage()
				.contains('is not included in the Orderform_Proposition__mdt Custom Metadata'),
			'expectedException should contain: is not included in the Orderform_Proposition__mdt Custom Metadata.'
		);

		Test.stopTest();

		TestUtils.autoCommit = true;
	}

	private class GenericMock implements System.StubProvider {
		private Map<String, Object> returns = new Map<String, Object>();
		private Map<String, Integer> callCount = new Map<String, Integer>();
		private Map<String, List<Object>> args = new Map<String, List<Object>>();

		public Object handleMethodCall(
			Object stubbedObject,
			String stubbedMethodName,
			Type returnType,
			List<Type> listOfParamTypes,
			List<String> listOfParamNames,
			List<Object> listOfArgs
		) {
			if (!callCount.containsKey(stubbedMethodName)) {
				callCount.put(stubbedMethodName, 0);
			}
			callCount.put(stubbedMethodName, callCount.get(stubbedMethodName) + 1);
			if (!args.containsKey(stubbedMethodName)) {
				args.put(stubbedMethodName, new List<Object>());
			}
			args.put(stubbedMethodName, listOfArgs);
			return returns.get(stubbedMethodName);
		}
	}
}