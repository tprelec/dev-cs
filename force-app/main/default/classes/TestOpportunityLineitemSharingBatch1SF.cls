/**
 * @description         This is the class contains tests for triggering 1SF sync for opps
 * @author              Marcel Vreuls
 * @history             Marcel Vreuls W-000022 :created testcase
 */

@isTest(SeeAllData=false) 
private class TestOpportunityLineitemSharingBatch1SF {


   private static INT_SF2SF_Handler sf2sf{
        get{
            if (sf2sf==null){
                //createBaseData();
                sf2sf = new INT_SF2SF_Handler();
                
            }
            return sf2sf;
        }
        
        set;}
    

  static testMethod void test_SF2SF() {

        OrderType__c ot = TestUtils.createOrderType();


        //Creating two accounts
        Account testAC1 = new Account(Name='Test Account1 Sharing');insert testAC1;
        Account testAC2 = new Account(Name='Test Account2 Sharing');insert testAC2;
        

        Product2 testProd = new Product2(Name='Test Product Sharing',INT_IsReadyForSharingWith__c = '1SF',Product_Line__c = 'fVodafone',Ordertype__c=ot.id);
        insert testProd;
        List<Product2> productList = new List<Product2>();
        productList.add(testProd);

        //Pricebook2 standardpricebook = [select Id, IsStandard from Pricebook2 where isStandard=true];
        //System.debug('Vreuls-standardpricebook' + standardpricebook.Id);
        Id standardPriceBookId = Test.getStandardPricebookId();

        // Create the PricebookEntry
        PricebookEntry testPbe = new PricebookEntry(Pricebook2Id = standardPricebookId, Product2Id = testProd.Id, UnitPrice = 100, IsActive = true);
        insert testPbe;
        // Re-Query the PBE
        testPbe = [SELECT Id, Pricebook2.IsStandard FROM PricebookEntry where Product2Id=:testProd.Id];
        System.debug('Vreuls-testPbe' + testPbe.Id);

        Opportunity testOpp1 = new Opportunity(Name='Test Opp 1 Sharing',AccountId=testAC1.Id,StageName='Prospect',INT_IsReadyForSharingWith__c = '1SF',CloseDate=Date.Today());
        insert testOpp1;
        System.debug('Vreuls-testOpp1' + testOpp1.Id);
        
        Opportunity testOpp2 = new Opportunity(Name='Test Opp 2 Sharing',AccountId=testAC2.Id,StageName='Prospect', CloseDate=Date.Today());
        insert testOpp2;
        System.debug('Vreuls-testOpp2' + testOpp2.Id);
        
        OpportunityLineItem oppProduct = new OpportunityLineItem();
        oppProduct.PricebookEntryId = testPbe.Id;
        oppProduct.OpportunityId = testOpp1.Id;
        oppProduct.Quantity= 100;
        oppProduct.TotalPrice = 11111;
        oppProduct.INT_IsReadyForSharingWith__c = '1SF';
        oppProduct.CLC__c = 'Acq';
        oppProduct.Product_Arpu_Value__c = 2;
        //oppProduct.Product_Arpu_Value__c= 10.0;
        //oppProduct.CLC__c = 'Acq';
        insert oppProduct;
        System.debug('Vreuls-oppProduct' + oppProduct.Id);


        //OpportunityLineItem oppProduct1 = new OpportunityLineItem();
        //oppProduct1.PricebookEntryId = testPbe.Id;
        //oppProduct1.OpportunityId = testOpp2.Id;
        //oppProduct1.Quantity= 100;
        //oppProduct1.TotalPrice = 11111;
        //oppProduct1.INT_IsReadyForSharingWith__c = '1SF';
        //oppProduct1.Product_Arpu_Value__c= 20.0;
        //oppProduct1.CLC__c = 'Acq';
        //insert oppProduct1;
        //System.debug('oppProduct1' + oppProduct1.Id);


        //List<OpportunityLineItem> oppProdList = new List<OpportunityLineItem>();
        //oppProdList.add(oppProduct);
        //oppProdList.add(oppProduct1);


        //List<Opportunity> opportunityList = new List<Opportunity>();
        //opportunityList.add(testOpp1);
        //opportunityList.add(testOpp2);

     
        List<Opportunity> scope = [Select Id From Opportunity Where LastModifiedDate >= YESTERDAY]; //Where LastModifiedDate >= YESTERDAY
        Set<Id> oppIds = new Set<Id>();
        
        for(Opportunity o : scope){
            oppIds.add(o.Id);     
            System.debug('Vreulsf' + o.Id);   
        }
        
        INT_SF2SF_Handler.ShareOpportunitiesOnline(oppIds);  
        


  }
}