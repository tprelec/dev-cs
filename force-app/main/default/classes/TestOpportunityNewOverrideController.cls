/**
 * @description			This is the test class that contains the tests for the Opportunity New Override Controller
 * @author				Ferdinand Bondt
 */
@isTest
private class TestOpportunityNewOverrideController {

    static testMethod void testController() {
    	
    	User thisUser = [SELECT Id, UserType FROM User WHERE UserType =:'PowerPartner' AND IsActive = true limit 1];
    	
    	Opportunity opp = new Opportunity(Name = 'Test Opportunity',
    									  StageName = 'Prospect',
    									  CloseDate = system.today());
    	insert opp;
    	
    	//test as partner
        System.runAs (thisUser) { 
		    PageReference pageRef = Page.OpportunityNewOverride;
		    Test.setCurrentPage(pageRef);
		    Apexpages.StandardController sc = new Apexpages.standardController(opp);
		    OpportunityNewOverrideController controller = new OpportunityNewOverrideController(sc);
		    controller.url();
        }
        
        //test regular
        PageReference pageRef = Page.OpportunityNewOverride;
	    Test.setCurrentPage(pageRef);
	    Apexpages.StandardController sc = new Apexpages.standardController(opp);
	    OpportunityNewOverrideController controller = new OpportunityNewOverrideController(sc);
	    controller.url();
        
	}
	
	@isTest
	public static void testLightning() {
		Account acc = new Account(Name = 'Test');
		insert acc;

		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Prospect',
			CloseDate = system.today(),
			AccountId = acc.Id
		);

		insert opp;

		Test.startTest();

		PageReference pageRef = Page.OpportunityNewOverride;
		pageRef.getParameters().put('accId', acc.Id);

		Test.setCurrentPage(pageRef);

		Apexpages.StandardController sc = new Apexpages.standardController(opp);
		OpportunityNewOverrideController controller = new OpportunityNewOverrideController(sc);
		PageReference newPr = controller.url();

		Test.stopTest();

		System.assertEquals(acc.Id, newPr.getParameters().get('accid'));
	}
}