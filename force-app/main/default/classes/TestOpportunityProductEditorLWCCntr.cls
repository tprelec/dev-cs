/**
 * @description     This is Test class for OpportunityProductEditorLWCController
 * @author           Ashok Patel
 */
 @SuppressWarnings('PMD.UnusedLocalVariable')
@isTest
private class TestOpportunityProductEditorLWCCntr {

  @testSetup
    static void testSetup() {
            
        TestUtils.createOrderValidationOpportunity();

        Order_Form_Settings__c orderFormSetting = new Order_Form_Settings__c(
                Max_no_of_sites_in_list__c = 10
        );
        insert orderFormSetting;

        User owner = TestUtils.theAdministrator == NULL ? TestUtils.createAdministrator() : TestUtils.theAdministrator;
        Account acct = TestUtils.createAccount(owner);
        Site__c site = TestUtils.createSite(acct);
        Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(),TestUtils.createBan(acct) );
        Contact c = TestUtils.createContact(acct);
        TestUtils.createOpportunityFieldMappings();
        
        //disable auto commit for the TestUtils class so we can insert Lists instead of entries one-by-one
        TestUtils.autoCommit = false;
        VF_Contract__c vfc = TestUtils.createVFContract(acct, opp);
        insert vfc;
        OrderType__c ot = TestUtils.createOrderType();
        ot.Name='MAC';
        insert ot;
        VF_Product__c p = new VF_Product__c();
        p.Name = 'Test productVF';
        p.Brand__c = 'Testing Brand';
        p.Cost_Price__c = 11;
        p.Description__c = 'This is a test product';
        p.Active__c = true;
        p.ProductCode__c = 'TEST_PRODUCT_CODE_VF';
        p.Product_Group__c = 'Priceplan';
        p.Quantity_type__c = 'Monthly';
        p.SDC_Product_matrix__c = 'Fixed - Data';
        p.Product_Line__c = 'fZiggo Only';
        p.OrderType__c = ot.id;
        p.ExternalID__c = 'VFP-02-ABCDEFG';
        insert p;

        Order__c o = new Order__c(
        Account__c = acct.Id,
        VF_Contract__c = vfc.Id,
        OrderType__c = ot.Id,
        Status__c = 'Accepted',
        O2C_Order__c = true
        );
        insert o;
        Integer nrOfRows = 3;
        List<Product2> products = new List<Product2>();
        for (Integer i = 0; i < nrOfRows; i++) {
                products.add(TestUtils.createProduct());
        }
        products[0].VF_Product__c=p.Id;
        products[0].CLC__c='Acq';
        products[1].VF_Product__c=p.Id;
        products[1].CLC__c='Acq';
        insert products;

        List<PricebookEntry> pbEntries = new List<PricebookEntry>();
        for (Integer i = 0; i < nrOfRows; i++) {
        pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId() , products.get(i)));
        }
        insert pbEntries;

        Decimal totalAmount = 0.0;
        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
        for (Integer i = 0; i < nrOfRows; i++) {
            OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp, pbEntries.get(i));
            oppLineItems.add(oppLineItem);
            totalAmount += oppLineItem.Product_Arpu_Value__c * oppLineItem.Duration__c * oppLineItem.Quantity;
        }

        insert oppLineItems;

    } 
    
    @isTest
    static void testOpportunityProductsMethods() {
        
        string massActivityType='Provide';
        string massClc='Acq';
        Date administrativeEndDate = system.today();
        Date contractEndDate =system.today();
        String queryFilterString='Test';
        Account acc =[select Id from Account LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];  
        Set<String> opptySet = new Set<String>{
                'Id',
                'Quantity',
                'UnitPrice',
                'Description',
                'CLC__c',
                'Activity_Type__c'
        
            };

           Set<Id> opportunityIdSet = new Set<Id>{ opp.Id };
       List<Product2> products =[select Id, Product2.VF_Product__c from Product2];
       String prodId = products[0].VF_Product__c;
       Site__c site = [select id, Site_City__c,Site_Street__c from Site__c limit 1];
       List<OpportunityLineItem> oliList = [ SELECT  Id, Quantity, CLC__c,   Activity_Type__c,  Group__c, Product_Arpu_Value__c, DiscountNew__c, Gross_List_Price__c,Discount,Duration__c, Location__c, OpportunityId,  Cost_Center__c,  Billing__c FROM opportunityLineItem];
        String jsonString =JSON.serialize( [SELECT Id, Quantity, CLC__c,  Activity_Type__c,Group__c, Product_Arpu_Value__c, DiscountNew__c, Gross_List_Price__c, Discount, Duration__c, Location__c,   OpportunityId, Cost_Center__c,   Billing__c FROM opportunityLineItem LIMIT 1]) ; 
        test.starttest();              
   List<String> opportunityLineItemsId = new List<String> ();
   list<Id> lstProdIds = new list<Id>();
   for(OpportunityLineItem oli:oliList ){
                opportunityLineItemsId.add(oli.Id);
                lstProdIds.add(oli.Id);
   }
   System.assertnotequals(0, opportunityLineItemsId.size(),'test');
    PricebookEntry pbEntries = [select Id,UnitPrice from PricebookEntry LIMIT 1];
  
     OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp,pbEntries);
     VF_Product__c vf = [select Id,Name,Brand__c,Product_Line__c,ProductCode__c,Quantity_type__c from VF_Product__c limit 1];
    string jsonStr ='{"activityType":"Provide","arpuValue":850,"billing":true,"clc":"Acq","costCenter":"1","discount":0,"doNotReturnHW":false,"duration":1,"hwDelivered":false,"id":"00k9E000007IIhHQAW","listprice":850,"monthly":0,"oneOff":850,"productId":"'+prodId+'","productLabel":"C113685 - KPN WEAS 950 Mb premium (E-LAN - Port Based) - Monthly","qType":"Monthly","quantity":1,"site":"'+site.Id+'","siteLabel":"Rotterdam, Driemanssteeweg 56, 3084CB","siteMulti":["'+site.Id+'"],"totalPrice":850}';
    
     string jsonStr1 ='{"activityType":"Provide","arpuValue":850,"billing":true,"clc":"Acq","costCenter":"1","discount":0,"doNotReturnHW":false,"duration":1,"hwDelivered":false,"id":"'+oppLineItem.Id+'","listprice":850,"monthly":0,"oneOff":850,"productId":"'+prodId+'","productLabel":"TEST_PRODUCT_CODE_VF - Testing Brand Test productVF - Monthly","qType":"Monthly","quantity":1,"site":"'+site.Id+'","siteLabel":"Rotterdam, Driemanssteeweg 56, 3084CB","siteMulti":["'+site.Id+'"],"totalPrice":850}';
      OpportunityProductEditorLWCController.ShoppingCartItem shoppingCartItemObj = new OpportunityProductEditorLWCController.ShoppingCartItem();
        shoppingCartItemObj.clc='Acq';
        shoppingCartItemObj.poNumber='Test';
        shoppingCartItemObj.listprice=100;
        shoppingCartItemObj.duration=1;
        shoppingCartItemObj.costCenter='test';
        shoppingCartItemObj.quantity=1;
        shoppingCartItemObj.discount=10;
        shoppingCartItemObj.productId=products[0].VF_Product__c;
        shoppingCartItemObj.invoiceDate=contractEndDate;
        shoppingCartItemObj.grp='ab';
      Map<Id, List<OpportunityLineItem>> opportunityLineItemreturn = OpportunityLineItemDAO.getOpportunityDetailsBasedOnId(opptySet,opportunityIdSet);
     OpportunityProductEditorLWCController.SiteWrapper siteWrapperObj = new OpportunityProductEditorLWCController.SiteWrapper();
     List<OpportunityProductEditorLWCController.ShoppingCartItem> shoppingCartItemOnj =  new List<OpportunityProductEditorLWCController.ShoppingCartItem>();
     OpportunityProductEditorLWCController.ShoppingCartDetails shoppingOnj = new  OpportunityProductEditorLWCController.ShoppingCartDetails(acc,opp,shoppingCartItemOnj);
     OpportunityProductEditorLWCController.SiteWrapper siteWrapperClass = new OpportunityProductEditorLWCController.SiteWrapper();
     List<OpportunityProductEditorLWCController.SiteWrapper> siteWrapperList = new  List<OpportunityProductEditorLWCController.SiteWrapper>();
    //  OpportunityProductEditorLWCController.ShoppingCartDetails wrp   = OpportunityProductEditorLWCController.queryShoppingCart(opp.Id);
     // string strr='[{"CLC__c":"Churn","Id":"a0P9E000005MsZcUAK"}]';
    // String str = OpportunityProductEditorLWCController.updateProducts(strr);
    
   

   
          String msgString = OpportunityProductEditorLWCController.massActivityProducts( opportunityLineItemsId,massActivityType, massClc);
          String msgStr = OpportunityProductEditorLWCController.massPriceChangeProducts(opportunityLineItemsId,shoppingCartItemObj); 
          String msgStr1 = OpportunityProductEditorLWCController.massEditProducts(opportunityLineItemsId,shoppingCartItemObj); 
          String msgStr2=OpportunityProductEditorLWCController.massCeaseProducts( opportunityLineItemsId, administrativeEndDate,contractEndDate); 
          String msgStr3= OpportunityProductEditorLWCController.saveData(shoppingCartItemObj);
          String vfStr = OpportunityProductEditorLWCController.vfProdToLabel(vf);
          String accId= OpportunityProductEditorLWCController.getAccountId(opp.Id);
          List<sObject> sObjList =  OpportunityProductEditorLWCController.getFilteredJsonProductPicklist(queryFilterString,opp.Id);
          siteWrapperList=OpportunityProductEditorLWCController.initializeSiteList(acc.Id,null);
          List<Site__c> siteList = OpportunityProductEditorLWCController.sitesJSON(opp.Id);
          OpportunityProductEditorLWCController.cloneProducts(opportunityLineItemsId);
          OpportunityProductEditorLWCController.ShoppingCartDetails wrp   = OpportunityProductEditorLWCController.queryShoppingCart(opp.Id); 
          shoppingCartItemOnj = OpportunityProductEditorLWCController.updateProduct( jsonStr1,opp.Id,false);
          shoppingCartItemOnj=  OpportunityProductEditorLWCController.createProduct( jsonStr1, opp.Id, false);
          OpportunityProductEditorLWCController.CLCOption  cLCOptions = new OpportunityProductEditorLWCController.CLCOption('Acq','Acq');
          List<OpportunityProductEditorLWCController.CLCOption>  cLCOptionList = new List<OpportunityProductEditorLWCController.CLCOption>();
          cLCOptionList = OpportunityProductEditorLWCController.getCLCOptions();
          List<PicklistService.PicklistOption> pList =  OpportunityProductEditorLWCController.getPicklistOptions('OpportunityLineItem', 'CLC__c' );
          shoppingOnj =OpportunityProductEditorLWCController.queryShoppingCart(opp.Id, 10, 10 );   
               try{
                    
                OpportunityProductEditorLWCController.deleteProducts(lstProdIds);
                
                }catch(exception e){
                  string msg = e.getMessage();
                }
     test.stoptest();
    }
    @isTest
    static void testOpportunityProductsSecondMethods() {
        
        string massActivityType='Provide';
        string massClc='Acq';
        Date administrativeEndDate = system.today();
        Date contractEndDate =system.today();
        String queryFilterString='Test';
        Account acc =[select Id from Account LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];  
       List<Product2> products =[select Id, Product2.VF_Product__c from Product2];
       String prodId = products[0].VF_Product__c;
       Site__c site = [select id, Site_City__c,Site_Street__c from Site__c limit 1];
       List<OpportunityLineItem> oliList = [ SELECT  Id, Quantity, CLC__c, Administrative_End_Date__c,ContractEnd_Date__c,  Activity_Type__c,  Group__c, Product_Arpu_Value__c, DiscountNew__c, Gross_List_Price__c,Discount,Duration__c, Location__c, OpportunityId,  Cost_Center__c,  Billing__c FROM opportunityLineItem];
        String jsonString =JSON.serialize( [SELECT Id, Quantity, CLC__c,  Activity_Type__c,Group__c, Product_Arpu_Value__c, DiscountNew__c, Gross_List_Price__c, Discount, Duration__c, Location__c,   OpportunityId, Cost_Center__c,   Billing__c FROM opportunityLineItem LIMIT 1]) ; 
        test.starttest();              
   List<String> opportunityLineItemsId = new List<String> ();
   list<Id> lstProdIds = new list<Id>();
   for(OpportunityLineItem oli:oliList ){
                opportunityLineItemsId.add(oli.Id);
                lstProdIds.add(oli.Id);
   }
   System.assertnotequals(0, opportunityLineItemsId.size(),'test');
    PricebookEntry pbEntries = [select Id,UnitPrice from PricebookEntry LIMIT 1];
     OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp,pbEntries);
     
     
     VF_Product__c vf = [select Id,Name,Brand__c,Product_Line__c,ProductCode__c,Quantity_type__c from VF_Product__c limit 1];
    string jsonStr ='{"activityType":"Provide","arpuValue":850,"billing":true,"clc":"Acq","costCenter":"1","discount":0,"doNotReturnHW":false,"duration":1,"hwDelivered":false,"id":"00k9E000007IIhHQAW","listprice":850,"monthly":0,"oneOff":850,"productId":"'+prodId+'","productLabel":"C113685 - KPN WEAS 950 Mb premium (E-LAN - Port Based) - Monthly","qType":"Monthly","quantity":1,"site":"'+site.Id+'","siteLabel":"Rotterdam, Driemanssteeweg 56, 3084CB","siteMulti":["'+site.Id+'"],"totalPrice":850}';
    
     string jsonStr1 ='{"activityType":"Provide","arpuValue":850,"billing":true,"clc":"Acq","costCenter":"1","discount":0,"doNotReturnHW":false,"duration":1,"hwDelivered":false,"id":"'+oppLineItem.Id+'","listprice":850,"monthly":0,"oneOff":850,"productId":"'+prodId+'","productLabel":"TEST_PRODUCT_CODE_VF - Testing Brand Test productVF - Monthly","qType":"Monthly","quantity":1,"site":"'+site.Id+'","siteLabel":"Rotterdam, Driemanssteeweg 56, 3084CB","siteMulti":["'+site.Id+'"],"totalPrice":850}';
      OpportunityProductEditorLWCController.ShoppingCartItem shoppingCartItemObj = new OpportunityProductEditorLWCController.ShoppingCartItem();
        shoppingCartItemObj.clc='Acq';
        shoppingCartItemObj.poNumber='Test';
        shoppingCartItemObj.listprice=100;
        shoppingCartItemObj.duration=1;
        shoppingCartItemObj.costCenter='test';
        shoppingCartItemObj.quantity=1;
        shoppingCartItemObj.discount=10;
        shoppingCartItemObj.productId='';
        shoppingCartItemObj.invoiceDate=contractEndDate;
        shoppingCartItemObj.site =site.Id;
    
     OpportunityProductEditorLWCController.SiteWrapper siteWrapperObj = new OpportunityProductEditorLWCController.SiteWrapper();
     List<OpportunityProductEditorLWCController.ShoppingCartItem> shoppingCartItemOnj =  new List<OpportunityProductEditorLWCController.ShoppingCartItem>();
     OpportunityProductEditorLWCController.ShoppingCartDetails shoppingOnj = new  OpportunityProductEditorLWCController.ShoppingCartDetails(acc,opp,shoppingCartItemOnj);
     OpportunityProductEditorLWCController.SiteWrapper siteWrapperClass = new OpportunityProductEditorLWCController.SiteWrapper();
     List<OpportunityProductEditorLWCController.SiteWrapper> siteWrapperList = new  List<OpportunityProductEditorLWCController.SiteWrapper>();
        
          String msgStr1 = OpportunityProductEditorLWCController.massEditProducts(opportunityLineItemsId,shoppingCartItemObj); 
        String msgStr2=OpportunityProductEditorLWCController.massCeaseProducts( opportunityLineItemsId, administrativeEndDate,contractEndDate); 
          
             
     test.stoptest();
    }
      @isTest
    static void testContractProductsMethods() {
            
          Date administrativeEndDate = system.today();
          Date contractEndDate =system.today(); 
          String massActivityType='Provide';
          String massClc='Acq';
          Account acc =[select Id from Account LIMIT 1];
          Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];  
          Site__c site = [select id, Site_City__c,Site_Street__c from Site__c limit 1];
          List<Product2> products =[select Id, Product2.VF_Product__c from Product2];
          List<VF_Contract__c> vfContract =[select Id from VF_Contract__c];
          Order__c ordObj = [select id,name,VF_Contract__c,VF_Contract__r.Opportunity__c from Order__c limit 1];
          OpportunityProductEditorLWCController.ShoppingCartItem shoppingCartItemObj = new OpportunityProductEditorLWCController.ShoppingCartItem();
            shoppingCartItemObj.clc='Acq';
            shoppingCartItemObj.poNumber='Test';
            shoppingCartItemObj.listprice=100;
            shoppingCartItemObj.duration=1;
            shoppingCartItemObj.costCenter='test';
            shoppingCartItemObj.quantity=1;
            shoppingCartItemObj.discount=10;
             shoppingCartItemObj.productId=products[0].VF_Product__c;
              shoppingCartItemObj.administrativeEndDate=administrativeEndDate;
               test.starttest();
            List<Contracted_Products__c> cpList = new List<Contracted_Products__c>();
            for(integer i=0;i<10;i++){
                Contracted_Products__c cp= new Contracted_Products__c(VF_Contract__c = vfContract[0].Id, Product__c = products[0].Id,CLC__c='Acq',Order__c=ordObj.Id,Site__c=site.Id,OpportunityLineItemId__c=null);
                cpList.add(cp);
            }
            insert cpList;
            Contracted_Products__c cp1= new Contracted_Products__c(VF_Contract__c = vfContract[0].Id,CLC__c='Acq',Order__c=ordObj.Id,Site__c=site.Id,OpportunityLineItemId__c=null,Product__c = products[0].Id);
            insert cp1;
            list<Id> lstProdIds = new list<Id>();
            for(Contracted_Products__c cp :cpList){
                lstProdIds.add(cp.Id);
            }    
            System.assertnotequals(0, lstProdIds.size(),'test');
      
  
   
       String msgString = OpportunityProductEditorLWCController.massActivityContractProducts(lstProdIds,massActivityType, massClc);
       String str = OpportunityProductEditorLWCController.updateContractedProducts(shoppingCartItemObj);
       String str1=OpportunityProductEditorLWCController.massCeaseContractProducts(lstProdIds,administrativeEndDate,contractEndDate);
       String str2=OpportunityProductEditorLWCController.massPriceChangeCntrctProducts(lstProdIds,shoppingCartItemObj);
       OpportunityProductEditorLWCController.deleteContractedProducts(lstProdIds);
       List<OpportunityProductEditorLWCController.ShoppingCartItem> shoppingCartItemOnj =  new List<OpportunityProductEditorLWCController.ShoppingCartItem>();
       OpportunityProductEditorLWCController.ShoppingCartDetails shoppingcartDetailsObj = new  OpportunityProductEditorLWCController.ShoppingCartDetails(acc,opp,shoppingCartItemOnj);
      shoppingcartDetailsObj =OpportunityProductEditorLWCController.queryShoppingCartForCP(ordObj.Id); 
    test.stoptest();
    }
}