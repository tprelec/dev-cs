@isTest
private class TestOpportunityTeamMemberTriggerHandler {
	private static Account acc;
	private static Opportunity opp;
	private static OrderType__c ot;
	private static Product2 prod;
	private static PriceBookEntry pbe;
	private static OpportunityTeamMember otm;

	private static void createTestData() {
		acc = TestUtils.createAccount(GeneralUtils.currentUser);
		ot = TestUtils.createOrderType();
		prod = TestUtils.createProduct();
		pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);

		opp = new Opportunity(
			Name = 'Tom Test Opp',
			StageName = 'Prospect',
			CloseDate = Date.today(),
			AccountId = acc.Id,
			Pricebook2Id = Test.getStandardPricebookId()
		);
		insert opp;
	}

	@isTest
	static void testBlockClosedOppsEditingOnInsert() {
		createTestData();
		opp.StageName = 'Closed Lost';
		update opp;

		OpportunityTeamMember otm = new OpportunityTeamMember(OpportunityId = opp.Id);

		Test.startTest();

		Boolean expectedExceptionThrown = false;
		try {
			insert otm;
		} catch (Exception e) {
			expectedExceptionThrown = e.getMessage()
				.contains(
					'No edits are allowed to Opportunities with status \'Closed Won\' or \'Closed Lost\''
				);
		}
		System.assertEquals(true, expectedExceptionThrown, 'Exception should be thrown.');

		Test.stopTest();
	}

	@isTest
	static void testBlockClosedOppsEditingOnDelete() {
		createTestData();

		OpportunityTeamMember otm = new OpportunityTeamMember(
			OpportunityId = opp.Id,
			UserId = UserInfo.getUserId()
		);
		insert otm;

		opp.StageName = 'Closed Lost';
		update opp;

		Test.startTest();

		Boolean expectedExceptionThrown = false;
		try {
			delete otm;
		} catch (Exception e) {
			expectedExceptionThrown = e.getMessage()
				.contains(
					'No edits are allowed to Opportunities with status \'Closed Won\' or \'Closed Lost\''
				);
		}
		System.assertEquals(true, expectedExceptionThrown, 'Exception should be thrown.');

		Test.stopTest();
	}

	@isTest
	static void testUpdateOpportunitySpecialists() {
		createTestData();
		opp.StageName = 'Closed Lost';
		update opp;

		Special_Authorizations__c specAuth = new Special_Authorizations__c(
			Edit_Closed_Opportunities__c = true
		);
		insert specAuth;

		OpportunityTeamMember otm = new OpportunityTeamMember(
			OpportunityId = opp.Id,
			UserId = UserInfo.getUserId()
		);
		insert otm;

		Test.startTest();

		otm.TeamMemberRole = 'Solution Specialist';
		update otm;

		otm.TeamMemberRole = 'ITW Sales Rep';
		update otm;

		otm.TeamMemberRole = 'Cloud Productivity Consultant';
		update otm;

		otm.TeamMemberRole = 'Solution Specialist Enterprise Services';
		update otm;

		Test.stopTest();

		Opportunity resultOpp = [
			SELECT
				Solution_Sales__c,
				Hidden_Inside_Sales_Specialist__c,
				Hidden_Microsoft_Solution_Specialist__c,
				Hidden_Ent_Services_Solution_Specialist__c
			FROM Opportunity
			WHERE Id = :opp.Id
		];
		System.assertEquals(
			UserInfo.getUserId(),
			resultOpp.Solution_Sales__c,
			'Opportunity Solution Sales should be the current user.'
		);
		System.assertEquals(
			UserInfo.getUserId(),
			resultOpp.Hidden_Inside_Sales_Specialist__c,
			'Opportunity Hidden Inside Sales Specialist should be the current user.'
		);
		System.assertEquals(
			UserInfo.getUserId(),
			resultOpp.Hidden_Microsoft_Solution_Specialist__c,
			'Opportunity Hidden Microsoft Solution Specialist should be the current user.'
		);
		System.assertEquals(
			UserInfo.getUserId(),
			resultOpp.Hidden_Ent_Services_Solution_Specialist__c,
			'Opportunity Hidden Ent Services Solution Specialist should be the current user.'
		);
	}

	@isTest
	static void testUpdateContractSharingRules() {
		createTestData();

		Test.startTest();

		OpportunityTeamMember otm = new OpportunityTeamMember(
			OpportunityId = opp.Id,
			UserId = UserInfo.getUserId()
			//OpportunityAccessLevel = 'All'
		);
		insert otm;

		Test.stopTest();

		List<VF_Contract__Share> resultVfcShares = [
			SELECT Id
			FROM VF_Contract__Share
			WHERE UserOrGroupId = :otm.UserId
		];
		System.assertEquals(0, resultVfcShares.size(), 'Contract share should be deleted.');
	}
}