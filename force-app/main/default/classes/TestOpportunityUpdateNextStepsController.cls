@isTest
private class TestOpportunityUpdateNextStepsController {
    
    @isTest
    static void verifyFirstNextStepCreation() {
        // create an Opportunity and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        //Pricebook2 pricebook = TestUtils.createPricebook(true);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );        
        
        ApexPages.StandardController controller = new ApexPages.Standardcontroller(opp);
        OpportunityUpdateNextStepsController ounsc = new OpportunityUpdateNextStepsController(controller);
        
        ounsc.nextStepText = 'test123';
        ounsc.updateOpp(); 
        
        opp = [Select Id, NextStep From Opportunity Where Id = :opp.Id];
        System.assert(opp.NextStep.endsWith('test123'));
    }
    
    @isTest
    static void verifyNextStepHistoryCreation() {
        // create an Opportunity and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        //Pricebook2 pricebook = TestUtils.createPricebook(true);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );        
        opp.NextStep = 'test123';
        update opp;
        
        Test.startTest();
        
        ApexPages.StandardController controller = new ApexPages.Standardcontroller(opp);
        OpportunityUpdateNextStepsController ounsc = new OpportunityUpdateNextStepsController(controller);
        
        PageReference p = Page.OpportunityUpdateNextSteps;
        Test.setCurrentPage(p);
        
        ounsc.nextStepText = 'test456';
        
        ounsc.updateOpp(); 
        
        Test.stopTest();
        
        opp = [Select Id, NextStep From Opportunity Where Id = :opp.Id];
        system.debug(opp);
    }
    
    @isTest
    static void doSomeTechnicalTests() {
        // create an Opportunity and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        //Pricebook2 pricebook = TestUtils.createPricebook(true);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );        
        
        ApexPages.StandardController controller = new ApexPages.Standardcontroller(opp);
        OpportunityUpdateNextStepsController ounsc = new OpportunityUpdateNextStepsController(controller);
        
        Integer i = ounsc.charsRemaining;
        Integer n = ounsc.nextStepLength;
        String[] s = ounsc.nextStepsHistory;
        ounsc.nothing();
        
        ounsc.nextStepText = 'test123';
        
        i = ounsc.charsRemaining;
        
        ounsc.updateOpp(); 
        
        opp = [Select Id, NextStep From Opportunity Where Id = :opp.Id];
        System.assert(opp.NextStep.endsWith('test123'));
    }    
        
}