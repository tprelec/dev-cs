/**
 * @description		This is the test class for OpportunityUtils class.
 * @author        	Gerhard Newman
 */
@isTest
private class TestOpportunityUtils {

    static testMethod void NewQuoteButton(){
		User u = TestUtils.createAdministrator();
		String profile = OpportunityUtils.getUserProfile(u.id);
		System.assert(profile=='System Administrator') ;
    }

}