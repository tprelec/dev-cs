@isTest
public class TestOrchExecuteCtnRetentionHandler {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);

		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			ban
		);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;

		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(
			claimerContact.Id,
			'123321'
		);
		contr.Dealer_Information__c = dealerInfo.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;

		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);

		NetProfit_CTN__c ctn1 = new NetProfit_CTN__c(
			NetProfit_Information__c = npi.Id,
			Order__c = order.Id,
			Service_Provider_Name__c = 'test Serv',
			Network_Provider_Code__c = 'Test Net',
			Simcard_number_current_provider__c = 'KPN',
			Simcard_number_VF__c = '2763572634275',
			APN__c = 'live.vodafone.com',
			apn2__c = 'FLEXNET',
			apn3__c = 'MP.TNF.NL',
			apn4__c = 'BLGG.NL',
			dataBlock__c = 'Yes',
			outgoingService__c = 'All_outgoing_enabled',
			SMSPremium__c = 'Enabled',
			thirdPartyContent__c = 'Yes',
			premiumDestinations__c = 'No_erotic_destinations',
			roamingspendingLimit__c = '100',
			Contract_Enddate__c = system.today().addDays(1),
			Contract_Number_Current_Provider__c = '2345234',
			Action__c = 'Retention',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			CTN_Status__c = 'MSISDN Assigned',
			CTN_Number__c = '31612310001',
			Assigned_Product_Id__c = '1234567'
		);

		insert ctn1;
		TestUtils.autoCommit = false;
		Integer nrOfRows = 2;

		List<Product2> products = new List<Product2>();
		for (Integer i = 0; i < nrOfRows; i++) {
			products.add(TestUtils.createProduct());
		}
		insert products;

		List<PricebookEntry> pbEntries = new List<PricebookEntry>();
		for (Integer i = 0; i < nrOfRows; i++) {
			pbEntries.add(
				TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i))
			);
		}
		insert pbEntries;

		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = 4,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Unify_Template_Id__c = '12345678A',
			Product__c = products[0].Id,
			VF_Contract__c = contr.Id,
			Order__c = order.Id
		);
		insert cp;
	}

	private static List<CSPOFA__Orchestration_Step__c> getSteps(Boolean isOrderIdIncluded) {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		if (isOrderIdIncluded) {
			String orderId = [SELECT Id FROM Order__c].Id;
			fieldKeyMap.put('VZ_Order__c', orderId);
		}

		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		return OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);
	}

	private static List<CSPOFA__Orchestration_Step__c> getOrchSteps(List<sObject> processedResult) {
		return (List<CSPOFA__Orchestration_Step__c>) processedResult;
	}

	@isTest
	public static void testValidResponse() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_Generic200NoData');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);
		List<CSPOFA__Orchestration_Step__c> steps = getSteps(true);

		Test.startTest();
		OrchExecuteCtnRetentionExecutionHandler executionHandler = new OrchExecuteCtnRetentionExecutionHandler();
		Boolean continueToProcess = executionHandler.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Should continue to process');

		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = getOrchSteps(
			executionHandler.process(steps)
		);
		System.assertEquals(
			'Complete',
			lstOrchSteps[0].CSPOFA__Status__c,
			'Status should be Complete'
		);
		Test.stopTest();

		List<NetProfit_CTN__c> ctns = [SELECT Id, CTN_Status__c, Action__c FROM NetProfit_CTN__c];

		System.assertEquals(1, ctns.size(), 'There should be a record');
		System.assertEquals(ctns[0].Action__c, 'Retention', 'The action should be: Retention');
		System.assertEquals(
			ctns[0].CTN_Status__c,
			'Retention Requested',
			'The status should be: Retention Requested'
		);
	}

	@isTest
	public static void testNoCallout() {
		List<CSPOFA__Orchestration_Step__c> steps = getSteps(true);

		Test.startTest();
		OrchExecuteCtnRetentionExecutionHandler executionHandler = new OrchExecuteCtnRetentionExecutionHandler();
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = getOrchSteps(
			executionHandler.process(steps)
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c, 'Error ocurred');
		Test.stopTest();
	}

	@isTest
	public static void testApiError() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_GenericAPI400_Error');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		List<CSPOFA__Orchestration_Step__c> steps = getSteps(true);

		Test.startTest();
		OrchExecuteCtnRetentionExecutionHandler executionHandler = new OrchExecuteCtnRetentionExecutionHandler();
		Boolean continueToProcess = executionHandler.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Process should continue the execution');

		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = getOrchSteps(
			executionHandler.process(steps)
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c, 'Status should be error');
		Test.stopTest();
	}

	@isTest
	public static void testErrorResponse() {
		EMP_TIBCO_ValidateOrderFEBulk.Response response = new EMP_TIBCO_ValidateOrderFEBulk.Response();
		response.status = 400;
		response.httpRequest = null;
		response.httpResponse = null;

		EMP_TIBCO_ValidateOrderFEBulk.ResponseData testData = new EMP_TIBCO_ValidateOrderFEBulk.ResponseData();
		testData.status = 'error';

		response.data = new List<EMP_TIBCO_ValidateOrderFEBulk.ResponseData>{ testData };

		EMP_TIBCO_ValidateOrderFEBulk.ResponseError errors = new EMP_TIBCO_ValidateOrderFEBulk.ResponseError();
		errors.code = 'error code';
		errors.message = 'errors';

		response.error = new List<EMP_TIBCO_ValidateOrderFEBulk.ResponseError>{ errors };
		response.getErrorMessages();

		System.assertNotEquals(0, response.error.size(), 'Should contain an error');
	}

	@isTest
	public static void testErrorDml() {
		Test.startTest();
		List<CSPOFA__Orchestration_Step__c> steps = getSteps(true);
		OrchExecuteCtnRetentionExecutionHandler executionHandler = new OrchExecuteCtnRetentionExecutionHandler();
		List<CSPOFA__Orchestration_Step__c> results = (List<CSPOFA__Orchestration_Step__c>) executionHandler.updateRecords(
			steps,
			new List<SObject>(),
			null
		);
		System.assertEquals(
			true,
			results[0].CSPOFA__Message__c.contains('exception thrown'),
			'Exception thrown'
		);

		List<NetProfit_CTN__c> ctns = [SELECT Id, CTN_Status__c, Action__c FROM NetProfit_CTN__c];

		executionHandler.performCallouts(steps);
		ctns[0].Action__c = 'Non existing action';
		results = (List<CSPOFA__Orchestration_Step__c>) executionHandler.updateRecords(
			steps,
			new List<SObject>(),
			ctns
		);
		System.assertEquals(
			true,
			results[0].CSPOFA__Message__c.contains('update failed'),
			'Exception whil DML update action'
		);
		Test.stopTest();
	}
}