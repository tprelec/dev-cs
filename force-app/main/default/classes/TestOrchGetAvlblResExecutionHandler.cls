@isTest
public class TestOrchGetAvlblResExecutionHandler {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('NetProfitCTNTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('NetProfitInformationTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			ban,
			owner
		);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(
			claimerContact.id,
			'123321'
		);
		contr.Dealer_Information__c = dealerInfo.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;
		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);

		NetProfit_CTN__c ctn2 = new NetProfit_CTN__c(
			Order__c = order.Id,
			Action__c = 'Porting',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			NetProfit_Information__c = npi.Id,
			CTN_Status__c = 'Future',
			CTN_Number__c = '31632110002',
			Simcard_number_current_provider__c = '87634587362',
			Service_Provider_Name__c = 'test',
			Current_Provider_name__c = 'testing',
			Contract_Enddate__c = System.today(),
			Contract_Number_Current_Provider__c = 'cncp-0001'
		);
		insert ctn2;
		NetProfit_CTN__c ctn2One = new NetProfit_CTN__c(
			Order__c = order.Id,
			Action__c = 'Porting',
			Product_Group__c = 'ConnectionCosts',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			NetProfit_Information__c = npi.Id,
			CTN_Status__c = 'Future',
			CTN_Number__c = '31612310002',
			Simcard_number_current_provider__c = '87634587364',
			Service_Provider_Name__c = 'test',
			Current_Provider_name__c = 'testing',
			Contract_Enddate__c = System.today(),
			Contract_Number_Current_Provider__c = 'cncp-0001'
		);
		insert ctn2One;
		NetProfit_CTN__c ctn3 = new NetProfit_CTN__c(
			Order__c = order.Id,
			Action__c = 'Porting',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			NetProfit_Information__c = npi.Id,
			CTN_Status__c = 'Future',
			CTN_Number__c = '31634510004',
			Simcard_number_current_provider__c = '87634587365',
			Service_Provider_Name__c = 'test',
			Current_Provider_name__c = 'testing',
			Contract_Enddate__c = System.today(),
			Contract_Number_Current_Provider__c = 'cncp-0001'
		);
		insert ctn3;
		NetProfit_CTN__c ctn4 = new NetProfit_CTN__c(
			Order__c = order.Id,
			Action__c = 'Retention',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			NetProfit_Information__c = npi.Id,
			CTN_Status__c = 'Future',
			CTN_Number__c = '31634510003'
		);
		insert ctn4;
		NetProfit_CTN__c ctn4One = new NetProfit_CTN__c(
			Order__c = order.Id,
			Action__c = 'Retention',
			Product_Group__c = 'ConnectionCosts',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			NetProfit_Information__c = npi.Id,
			CTN_Status__c = 'Future',
			CTN_Number__c = '31623410003'
		);
		insert ctn4One;
	}

	@isTest
	public static void testOrchGARHandlerApi200() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;
		String nPIId = [SELECT Id FROM NetProfit_Information__c].Id;

		NetProfit_CTN__c ctn1 = new NetProfit_CTN__c(
			Order__c = orderId,
			Action__c = 'New',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Voice',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			NetProfit_Information__c = nPIId,
			CTN_Status__c = 'Future',
			CTN_Number__c = '316XXX10001'
		);
		insert ctn1;
		NetProfit_CTN__c ctn5 = new NetProfit_CTN__c(
			Order__c = orderId,
			Action__c = 'New',
			Product_Group__c = 'ConnectionCosts',
			Price_Plan_Class__c = 'Voice',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			NetProfit_Information__c = nPIId,
			CTN_Status__c = 'Future',
			CTN_Number__c = '316XXX10001'
		);
		insert ctn5;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_200GetAvailableR');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchGetAvlblResourcesExecutionHandler getOrchGAR = new OrchGetAvlblResourcesExecutionHandler();
		Boolean continueToProcess = getOrchGAR.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Check that the Callout was successful');
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchGAR.process(
			steps
		);

		System.assertEquals(
			'Complete',
			lstOrchSteps[0].CSPOFA__Status__c,
			'Orchestrator Custom Step finalize without Error'
		);
		Test.stopTest();
	}

	@isTest
	public static void testOrchGARHandlerApi200Data() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;
		String nPIId = [SELECT Id FROM NetProfit_Information__c].Id;

		NetProfit_CTN__c ctn1 = new NetProfit_CTN__c(
			Order__c = orderId,
			Action__c = 'New',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			APN__c = 'live.vodafone.com',
			NetProfit_Information__c = nPIId,
			CTN_Status__c = 'Future',
			CTN_Number__c = '316XXX10001'
		);
		insert ctn1;
		NetProfit_CTN__c ctn5 = new NetProfit_CTN__c(
			Order__c = orderId,
			Action__c = 'New',
			Product_Group__c = 'ConnectionCosts',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			APN__c = 'live.vodafone.com',
			NetProfit_Information__c = nPIId,
			CTN_Status__c = 'Future',
			CTN_Number__c = '316XXX10001'
		);
		insert ctn5;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_200GetAvailableR');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchGetAvlblResourcesExecutionHandler getOrchGAR = new OrchGetAvlblResourcesExecutionHandler();
		Boolean continueToProcess = getOrchGAR.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Check that the Callout was successful');
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchGAR.process(
			steps
		);

		System.assertEquals(
			'Complete',
			lstOrchSteps[0].CSPOFA__Status__c,
			'Orchestrator Custom Step finalize without Error'
		);
		Test.stopTest();
	}

	@isTest
	public static void testOrchGARHandlerApi200Addmobile() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;
		String nPIId = [SELECT Id FROM NetProfit_Information__c].Id;
		Opportunity objOPP = [SELECT id, Select_Journey__c FROM opportunity];
		objOPP.Select_Journey__c = 'Add Mobile Product';
		objOPP.Segment__c = 'SoHo';
		update objOPP;

		NetProfit_CTN__c ctn1 = new NetProfit_CTN__c(
			Order__c = orderId,
			Action__c = 'New',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			APN__c = 'live.vodafone.com',
			resource_category__c = '097',
			NetProfit_Information__c = nPIId,
			CTN_Status__c = 'Future',
			CTN_Number__c = '316XXX10001'
		);
		insert ctn1;
		NetProfit_CTN__c ctn5 = new NetProfit_CTN__c(
			Order__c = orderId,
			Action__c = 'New',
			Product_Group__c = 'ConnectionCosts',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			APN__c = 'live.vodafone.com',
			resource_category__c = '097',
			NetProfit_Information__c = nPIId,
			CTN_Status__c = 'Future',
			CTN_Number__c = '316XXX10001'
		);
		insert ctn5;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_200GetAvailableR');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchGetAvlblResourcesExecutionHandler getOrchGAR = new OrchGetAvlblResourcesExecutionHandler();
		Boolean continueToProcess = getOrchGAR.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Check that the Callout was successful');
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchGAR.process(
			steps
		);

		System.assertEquals(
			'Complete',
			lstOrchSteps[0].CSPOFA__Status__c,
			'Orchestrator Custom Step finalize without Error'
		);
		Test.stopTest();
	}

	@isTest
	public static void testOrchGARHandlerApi200DMLException() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;
		String nPIId = [SELECT Id FROM NetProfit_Information__c].Id;

		NetProfit_CTN__c ctn1 = new NetProfit_CTN__c(
			Order__c = orderId,
			Action__c = 'New',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			APN__c = 'live.vodafone.com',
			resource_category__c = '097',
			NetProfit_Information__c = nPIId,
			CTN_Status__c = 'Future',
			CTN_Number__c = '316XXX10001'
		);
		insert ctn1;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_LongString_GetAvaliableR');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchGetAvlblResourcesExecutionHandler getOrchGAR = new OrchGetAvlblResourcesExecutionHandler();
		Boolean continueToProcess = getOrchGAR.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Check that the Callout was successful');
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchGAR.process(
			steps
		);

		System.assertEquals(
			'Error',
			lstOrchSteps[0].CSPOFA__Status__c,
			'Orchestrator Custom Step finalize with Error when DML exception is trigger for CTN Update'
		);
		Test.stopTest();
	}

	@isTest
	public static void testOrchGARHandlerApi200NoData() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;
		String nPIId = [SELECT Id FROM NetProfit_Information__c].Id;

		NetProfit_CTN__c ctn1 = new NetProfit_CTN__c(
			Order__c = orderId,
			Action__c = 'New',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			Simcard_number_VF__c = '123456789101',
			Sharing__c = 'No',
			APN__c = 'live.vodafone.com',
			resource_category__c = '097',
			NetProfit_Information__c = nPIId,
			CTN_Status__c = 'Future',
			CTN_Number__c = '316XXX10001'
		);
		insert ctn1;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_Generic200NoData');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchGetAvlblResourcesExecutionHandler getOrchGAR = new OrchGetAvlblResourcesExecutionHandler();
		Boolean continueToProcess = getOrchGAR.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Check that the Callout was successful');
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchGAR.process(
			steps
		);

		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c, 'Pool Exhausted');
		Test.stopTest();
	}
}