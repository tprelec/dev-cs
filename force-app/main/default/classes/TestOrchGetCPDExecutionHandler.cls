@isTest
public class TestOrchGetCPDExecutionHandler {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);

		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			ban
		);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;

		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(
			claimerContact.Id,
			'123321'
		);
		contr.Dealer_Information__c = dealerInfo.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;
	}

	private static List<CSPOFA__Orchestration_Step__c> getSteps(Boolean isOrderIdIncluded) {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		if (isOrderIdIncluded) {
			String orderId = [SELECT Id FROM Order__c].Id;
			fieldKeyMap.put('VZ_Order__c', orderId);
		}

		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		return OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);
	}

	@isTest
	public static void testValidResponse() {
		List<CSPOFA__Orchestration_Step__c> steps = getSteps(true);
		Test.setMock(HttpCalloutMock.class, new EMP_GetCustomerProductDetailsMock());

		Test.startTest();
		OrchGetCPDExecutionHandler getOrchCpd = new OrchGetCPDExecutionHandler();
		Boolean continueToProcess = getOrchCpd.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Should continue to process');

		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchCpd.process(
			steps
		);
		System.assertEquals(
			'Complete',
			lstOrchSteps[0].CSPOFA__Status__c,
			'Status should be Complete'
		);
		Test.stopTest();

		List<VF_Contract__c> contracts = [
			SELECT Id, Customer_Product_Details__c
			FROM VF_Contract__c
		];

		System.assertEquals(1, contracts.size(), 'There should be a contract');
		System.assert(
			contracts[0].Customer_Product_Details__c != null,
			'There should be a data in the field'
		);
	}

	@isTest
	public static void testNoCallout() {
		List<CSPOFA__Orchestration_Step__c> steps = getSteps(true);

		Test.startTest();
		OrchGetCPDExecutionHandler getOrchCpd = new OrchGetCPDExecutionHandler();
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchCpd.process(
			steps
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c, 'Error ocurred');
		Test.stopTest();
	}

	@isTest
	public static void testApiNoCpd() {
		List<CSPOFA__Orchestration_Step__c> steps = getSteps(true);

		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_GenericAPI400_Error');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, mock);
		Test.startTest();
		OrchGetCPDExecutionHandler getOrchCpd = new OrchGetCPDExecutionHandler();
		Boolean continueToProcess = getOrchCpd.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Process should not continue');

		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchCpd.process(
			steps
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c, 'Status should be error');
		Test.stopTest();
	}

	@isTest
	public static void testNoOrderId() {
		List<CSPOFA__Orchestration_Step__c> steps = getSteps(false);

		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_GenericAPI400_Error');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchGetCPDExecutionHandler getOrchCpd = new OrchGetCPDExecutionHandler();
		Boolean continueToProcess = getOrchCpd.performCallouts(steps);
		System.assertEquals(
			false,
			continueToProcess,
			'Exception thrown, proces should not continue'
		);

		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchCpd.process(
			steps
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c, 'Status is error');
		Test.stopTest();
	}

	@isTest
	public static void testErrorDml() {
		List<CSPOFA__Orchestration_Step__c> steps = getSteps(true);
		OrchGetCPDExecutionHandler executionHandler = new OrchGetCPDExecutionHandler();
		List<CSPOFA__Orchestration_Step__c> results = (List<CSPOFA__Orchestration_Step__c>) executionHandler.updateVfContracts(
			steps,
			new List<SObject>(),
			null
		);
		System.assertEquals(
			true,
			results[0].CSPOFA__Message__c.contains('exception thrown'),
			'Exception thrown'
		);

		List<VF_Contract__c> contracts = [
			SELECT Id, Customer_Product_Details__c
			FROM VF_Contract__c
		];

		executionHandler.performCallouts(steps);
		contracts[0]
			.Cloud_Productivity_Order__c = 'To long string for the given field to simulate DML failure';
		results = (List<CSPOFA__Orchestration_Step__c>) executionHandler.updateVfContracts(
			steps,
			new List<SObject>(),
			contracts
		);
		System.assertEquals(
			true,
			results[0].CSPOFA__Message__c.contains('update failed'),
			'Exception whil DML update action'
		);
	}
}