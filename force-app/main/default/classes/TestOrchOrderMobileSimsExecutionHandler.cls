@isTest
public class TestOrchOrderMobileSimsExecutionHandler {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('NetProfitCTNTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			ban,
			owner
		);
		opp.Select_Journey__c = 'Sales';
		update opp;
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, claimerContact.id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(
			claimerContact.id,
			'123321'
		);
		contr.Dealer_Information__c = dealerInfo.Id;
		contr.Unify_Framework_Id__c = '9634563A';
		contr.Signed_by_Customer__c = claimerContact.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			Billing_Arrangement__c = ba.id
		);
		insert order;

		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);

		NetProfit_CTN__c ctn1 = new NetProfit_CTN__c(
			NetProfit_Information__c = npi.Id,
			Order__c = order.Id,
			Service_Provider_Name__c = 'test Serv',
			Network_Provider_Code__c = 'Test Net',
			Simcard_number_current_provider__c = 'KPN',
			Simcard_number_VF__c = '2763572634275',
			APN__c = 'live.vodafone.com',
			apn2__c = 'FLEXNET',
			apn3__c = 'MP.TNF.NL',
			apn4__c = 'BLGG.NL',
			dataBlock__c = 'Yes',
			outgoingService__c = 'All_outgoing_enabled',
			SMSPremium__c = 'Enabled',
			thirdPartyContent__c = 'Yes',
			premiumDestinations__c = 'No_erotic_destinations',
			roamingspendingLimit__c = '100',
			Contract_Enddate__c = system.today().addDays(1),
			Contract_Number_Current_Provider__c = '2345234',
			Action__c = 'New',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			CTN_Status__c = 'MSISDN Assigned',
			CTN_Number__c = '31612310001'
		);
		insert ctn1;

		TestUtils.autoCommit = false;
		Integer nrOfRows = 2;

		List<Product2> products = new List<Product2>();
		for (Integer i = 0; i < nrOfRows; i++) {
			products.add(TestUtils.createProduct());
		}
		insert products;
		List<PricebookEntry> pbEntries = new List<PricebookEntry>();
		for (Integer i = 0; i < nrOfRows; i++) {
			pbEntries.add(
				TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i))
			);
		}
		insert pbEntries;

		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = 4,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Unify_Template_Id__c = '12345678A',
			Product__c = products[0].Id,
			VF_Contract__c = contr.Id,
			Order__c = order.Id
		);

		insert cp;
	}

	@isTest
	public static void testOrchASFTHandler200API() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_Tibco202Response');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchOrderMobileSimsExecutionHandler getOrchASFT = new OrchOrderMobileSimsExecutionHandler();
		Boolean continueToProcess = getOrchASFT.performCallouts(steps);
		System.assertEquals(true, continueToProcess);
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchASFT.process(
			steps
		);
		System.assertEquals('Complete', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testOrchASFTHandler400API() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_GenericAPI400_Error');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchOrderMobileSimsExecutionHandler getOrchASFT = new OrchOrderMobileSimsExecutionHandler();
		Boolean continueToProcess = getOrchASFT.performCallouts(steps);
		System.assertEquals(true, continueToProcess);
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchASFT.process(
			steps
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testOrchASFTHandlerNoCallout() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);

		Test.startTest();
		OrchOrderMobileSimsExecutionHandler getOrchASFT = new OrchOrderMobileSimsExecutionHandler();
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchASFT.process(
			steps
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}
}