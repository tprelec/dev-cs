@isTest
public class TestOrchSharedBundleNoAddonsExecHandler {
    
    @testSetup 
    public static void setupTestData() {

        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Site__c site = TestUtils.createSite(acct);
        Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban );
        opp.Select_Journey__c = 'Sales';
        Update opp;
        VF_Contract__c contr = TestUtils.createVFContract(acct,opp);

        
        OrderType__c ot = TestUtils.createOrderType();
        
        Contact claimerContact = TestUtils.createContact(acct);
        claimerContact.Userid__c = owner.Id;
        update claimerContact;
        Financial_Account__c fa = TestUtils.createFinancialAccount(ban, claimerContact.id);
        Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
        Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(claimerContact.id, '123321');
        contr.Dealer_Information__c = dealerInfo.Id;
        contr.Unify_Framework_Id__c = '9634563A';
        contr.Signed_by_Customer__c = claimerContact.Id;
        update contr;

        Order__c order = new Order__c(
            Status__c = 'New',
            Propositions__c = 'Legacy',
            OrderType__c = ot.Id,
            VF_Contract__c = contr.Id,
            Number_of_items__c = 100,
            O2C_Order__c = true,
            Billing_Arrangement__c = ba.id
        );
        insert order;
        NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);

        NetProfit_CTN__c ctn1 = new NetProfit_CTN__c( 
            NetProfit_Information__c = npi.Id,
            Order__c = order.Id,
            Service_Provider_Name__c = 'test Serv',
            Network_Provider_Code__c = 'Test Net',
            Simcard_number_current_provider__c = 'KPN',
            Simcard_number_VF__c = '2763572634275',
            APN__c = 'live.vodafone.com',
            apn2__c = 'FLEXNET',
            apn3__c = 'MP.TNF.NL',
            apn4__c = 'BLGG.NL',
            dataBlock__c = 'Yes',
            outgoingService__c = 'All_outgoing_enabled',
            SMSPremium__c = 'Enabled',
            thirdPartyContent__c = 'Yes',
            premiumDestinations__c = 'No_erotic_destinations',
            roamingspendingLimit__c = '100',
            Contract_Enddate__c = system.today().addDays(1),
            Contract_Number_Current_Provider__c = '2345234',
            Action__c = 'New',
            Product_Group__c = 'Priceplan' ,
            Price_Plan_Class__c = 'Data',
            CTN_Status__c ='MSISDN Assigned',
            CTN_Number__c = '31612310001');
        insert ctn1;

        TestUtils.autoCommit = false;
        VF_Product__c vfp = new VF_Product__c(Name = 'tst', Family__c = 'tst', Product_Line__c='fVodafone',ExternalID__c = 'VFP-02-1234567',ProductCode__c = 'DUMMYADDMOBILEPRODUCT', OrderType__c = ot.Id,AllowsGroupDataSharing__c = 'YES',
        Unify_Group_Level_Product_CatalogId__c = '3720595',
        Unify_Group_Level_Product_Code__c = 'GL_MASS_MARKET',
        Unify_Group_Lvl_Pricing_Element_Branch__c = 'Shared_Allowance',
        Unify_Group_Lvl_Billing_Offer_CatalogId__c = '8883473',
        Unify_Group_Level_Billing_Offer_Code__c = 'GL_AO_GROUP_DATA_3_BIZ_RED',
        SendtoUnifyforAutomaticProvisioning__c = 'Yes',
        Sharer_Product__c = true,
        Available_for_EnhancedMobileProvisioning__c = true);
        insert vfp;    
        List<Product2> products = new List<Product2>();
        products.add(TestUtils.createProduct()); 
        products[0].VF_Product__c = vfp.Id;      
        insert products;
        List<PricebookEntry> pbEntries = new List<PricebookEntry>();
        
        pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId() , products[0]));
        
        insert pbEntries;

        Contracted_Products__c cp = new Contracted_Products__c (
            Quantity__c = 4,
            UnitPrice__c = 10,
            Arpu_Value__c = 10,
            Duration__c = 1,
            Unify_Template_Id__c = '12345678A',
            Product__c = products[0].Id,
            VF_Contract__c = contr.Id,
            Order__c = order.Id
        );  
        insert cp;
    }

    @isTest
    public static void testOrchVOFPHandlerCalloutNotNeeded(){
        List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
        List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(processTemplate, null, 1, true);

        Map<String, String> fieldKeyMap = new Map<String, String>();

        String orderId = [SELECT Id from Order__c].Id;

        fieldKeyMap.put('VZ_Order__c', orderId);
        List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
        List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);

        Test.startTest();
            OrchSharedBundleNoAddonsExecutionHandler getOrchGLPs = new OrchSharedBundleNoAddonsExecutionHandler();
            Boolean continueToProcess = getOrchGLPs.performCallouts(steps);
            System.assertEquals(true, continueToProcess);
            List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>)getOrchGLPs.process(steps);
            System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
        Test.stopTest();
    }

    @isTest
    public static void testOrchVOFPHandler200ResAPI(){
        List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
        List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(processTemplate, null, 1, true);

        Map<String, String> fieldKeyMap = new Map<String, String>();

        String orderId = [SELECT Id from Order__c].Id;

        fieldKeyMap.put('VZ_Order__c', orderId);
        List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
        List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);
        //create mock 
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('EMP_ValOrderProd_res200');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        //Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
            OrchSharedBundleNoAddonsExecutionHandler getOrchGLPs = new OrchSharedBundleNoAddonsExecutionHandler();
            Boolean continueToProcess = getOrchGLPs.performCallouts(steps);
            System.assertEquals(true, continueToProcess);
            List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>)getOrchGLPs.process(steps);
            System.assertEquals('Complete', lstOrchSteps[0].CSPOFA__Status__c);
        Test.stopTest();
    }

    @isTest
    public static void testOrchVOFPHandler400ResAPI(){
        List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
        List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(processTemplate, null, 1, true);

        Map<String, String> fieldKeyMap = new Map<String, String>();
        String orderId = [SELECT Id from Order__c].Id;

        fieldKeyMap.put('VZ_Order__c', orderId);
        List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
        List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);
        //create mock 
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('EMP_GenericAPI400_Error');
        mock.setStatusCode(400);
        mock.setHeader('Content-Type', 'application/json');
        //Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
            OrchSharedBundleNoAddonsExecutionHandler getOrchGLPs = new OrchSharedBundleNoAddonsExecutionHandler();
            Boolean continueToProcess = getOrchGLPs.performCallouts(steps);
            System.assertEquals(true, continueToProcess);
            List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>)getOrchGLPs.process(steps);
            System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
        Test.stopTest();
    }
    
    @isTest
    public static void testGetSharerCatalogPricingIDs(){
        EMP_CustomerProductDetails.CustomerProductPricingReference ref1 = new EMP_CustomerProductDetails.CustomerProductPricingReference();
        ref1.catalogPricingID = 8883473;
        List<EMP_CustomerProductDetails.CustomerProductPricingReference> refs = new List<EMP_CustomerProductDetails.CustomerProductPricingReference>();
        refs.add(ref1);
        EMP_CustomerProductDetails.CustomerProductDetails cpd1 = new EMP_CustomerProductDetails.CustomerProductDetails();
        cpd1.pricingReferenceData = refs;
        List<EMP_CustomerProductDetails.CustomerProductDetails> cpdList = new List<EMP_CustomerProductDetails.CustomerProductDetails>();
        cpdList.add(cpd1);
        
        List<String> billingOfferIds = EMP_BSLintegrationHelper.getSharerCatalogPricingIDs(cpdList);
        
        System.assertEquals('8883473', billingOfferIds[0]);
    }
    
}