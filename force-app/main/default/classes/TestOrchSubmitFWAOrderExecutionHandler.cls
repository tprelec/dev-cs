@isTest
public class TestOrchSubmitFWAOrderExecutionHandler {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			ban,
			owner
		);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		contr.Unify_Order_Id__c = '12345678A';
		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(
			claimerContact.id,
			'123321'
		);
		contr.Dealer_Information__c = dealerInfo.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;
	}

	@isTest
	public static void testOrchSubmitOrderHandlerApi200() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_SubmitOrderFE_200');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchSubmitFWAOrderExecutionHandler getOrchSOFE = new OrchSubmitFWAOrderExecutionHandler();
		Boolean continueToProcess = getOrchSOFE.performCallouts(steps);
		System.assertEquals(true, continueToProcess);
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchSOFE.process(
			steps
		);
		System.assertEquals('Complete', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testOrchSubmitOrderHandlerApi400() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_GenericAPI400_Error');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');
		// Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchSubmitFWAOrderExecutionHandler getOrchSOFE = new OrchSubmitFWAOrderExecutionHandler();
		Boolean continueToProcess = getOrchSOFE.performCallouts(steps);
		System.assertEquals(true, continueToProcess);
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchSOFE.process(
			steps
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testOrchSubmitOrderHandlerParsingError() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_ErrorInParsing');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');
		// Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchSubmitFWAOrderExecutionHandler getOrchSOFE = new OrchSubmitFWAOrderExecutionHandler();
		Boolean continueToProcess = getOrchSOFE.performCallouts(steps);
		System.assertEquals(true, continueToProcess);
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchSOFE.process(
			steps
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testOrchSubmitOrderHandlerNoCallout() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_ErrorInParsing');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');
		// Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchSubmitFWAOrderExecutionHandler getOrchSOFE = new OrchSubmitFWAOrderExecutionHandler();
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchSOFE.process(
			steps
		);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}
}