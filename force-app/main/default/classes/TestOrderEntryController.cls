@isTest
public with sharing class TestOrderEntryController {
	private static List<OE_Product__c> oeProducts;
	private static List<OE_Add_On__c> oeAddons;

	@TestSetup
	static void makeData() {
		TestUtils.createCompleteOpportunity();
		TestUtils.createMavenDocumentsRecords();
	}

	@isTest
	static void testGetOpportunityData() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		List<Attachment> attPre = [
			SELECT Id
			FROM Attachment
			WHERE ParentId = :opp.Id AND Name = 'OrderEntryData.json'
		];
		System.assert(attPre.size() == 0, 'There should be no OrderEntryData attachment.');

		Test.startTest();
		OrderEntryData oed = OrderEntryController.getOrderEntryData(opp.Id);
		Test.stopTest();

		List<Attachment> attPost = [
			SELECT Id
			FROM Attachment
			WHERE ParentId = :opp.Id AND Name = 'OrderEntryData.json'
		];
		System.assertEquals(oed.opportunityId, opp.Id, 'Wrong opportunity Id.');
		System.assertEquals(oed.accountId, opp.AccountId, 'Wrong account Id.');
		System.assert(attPost.size() > 0, 'OrderEntryData attachment should be generated.');
	}

	@IsTest
	static void testGetHeaderInfo() {
		Opportunity opp = [SELECT Id, Account.Name FROM Opportunity LIMIT 1];

		Test.startTest();
		String headerInfo = OrderEntryController.getHeaderInfo(opp.Id);
		Test.stopTest();

		System.assertEquals(opp.Account.Name + '  •   ', headerInfo, 'Wrong header info format.');
	}

	@IsTest
	static void testGetAccountData() {
		Opportunity opp = [SELECT Id, Account.Id, Account.Name FROM Opportunity LIMIT 1];

		Test.startTest();
		Account acc = OrderEntryController.getAccountData(opp.Account.Id);
		Test.stopTest();

		System.assertEquals(opp.Account.Name, acc.Name, 'Wrong acc name.');
	}

	@IsTest
	static void testGetContactData() {
		Opportunity opp = [SELECT Id, Account.Id FROM Opportunity LIMIT 1];

		Test.startTest();
		List<Contact> contacts = OrderEntryController.getContactData(opp.Account.Id);
		Test.stopTest();

		System.assert(!contacts.isEmpty(), 'Contact list should not be empty.');
	}

	@IsTest
	static void testGetPrimaryContact() {
		Contact cont = [SELECT Id, LastName FROM Contact LIMIT 1];

		Test.startTest();
		Contact primCnt = OrderEntryController.getPrimaryContact(cont.Id);
		Test.stopTest();

		System.assertEquals(cont.Id, primCnt.Id, 'Wrong primary contact contact.');
		System.assertEquals(cont.LastName, primCnt.LastName, 'Wrong primary contact contact.');
	}

	@IsTest
	static void testSaveContact() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		List<Contact> cont = [SELECT Id FROM Contact];

		insert new OpportunityContactRole(
			ContactId = cont[0].Id,
			OpportunityId = opp.Id,
			IsPrimary = true
		);

		Contact newCont = new Contact(FirstName = 'Test13', LastName = 'Testoni');

		Test.startTest();
		Contact nc = OrderEntryController.saveContact(newCont, opp.Id);
		Test.stopTest();

		OpportunityContactRole ocr = [
			SELECT Id, Role, IsPrimary
			FROM OpportunityContactRole
			WHERE ContactId = :nc.Id
			LIMIT 1
		];

		List<Contact> contPost = [SELECT Id FROM Contact];

		System.assert(contPost.size() > cont.size(), 'There should be one more contact.');
		System.assertEquals('Administrative Contact', ocr.Role, 'Wrong role.');
		System.assertEquals(
			true,
			ocr.isPrimary,
			'Newly created contact role should be se as primary.'
		);
	}

	@IsTest
	static void testGetProductsPerType() {
		insert new OE_Product__c(Type__c = 'Main');

		List<String> mainType = new List<String>{ 'Main' };

		Test.startTest();
		List<OE_Product__c> mainProducts = OrderEntryController.getProducts(mainType);
		Test.stopTest();

		Set<String> types = new Set<String>();

		for (OE_Product__c oep : mainProducts) {
			types.add(oep.Type__c);
		}

		List<String> typesList = new List<String>();
		typesList.addAll(types);

		System.assertEquals(1, types.size(), 'There must be only one type');
		System.assertEquals(mainType[0], typesList[0], 'There must be Main');
	}

	@IsTest
	static void testGetProductsAllTypes() {
		List<OE_Product__c> products = new List<OE_Product__c>();

		OE_Product__c main = new OE_Product__c(Type__c = 'Main');
		products.add(main);

		OE_Product__c internet = new OE_Product__c(Type__c = 'Internet');
		products.add(internet);

		insert products;

		Test.startTest();
		List<OE_Product__c> allProducts = OrderEntryController.getProducts(null);
		Test.stopTest();

		Set<String> types = new Set<String>();

		for (OE_Product__c oep : allProducts) {
			types.add(oep.Type__c);
		}

		System.assert(types.size() > 1, 'There must be multiple types');
	}

	@IsTest
	static void testGetAddons() {
		OE_Product__c product = new OE_Product__c(Type__c = 'Internet');
		insert product;

		OE_Add_On__c addon = new OE_Add_On__c(
			Type__c = 'Internet Additional',
			Product_Code__c = '1111'
		);
		insert addon;

		List<OE_Product_Add_On__c> addons = new List<OE_Product_Add_On__c>();

		addons.add(new OE_Product_Add_On__c(Product__c = product.Id, Add_On__c = addon.Id));
		addons.add(new OE_Product_Add_On__c(Product__c = product.Id, Add_On__c = addon.Id));

		insert addons;

		Test.startTest();
		List<OE_Product_Add_On__c> allAddons = OrderEntryController.getAddons(product.Id);
		Test.stopTest();

		Set<Id> parentProductIds = new Set<Id>();

		for (OE_Product_Add_On__c oepao : allAddons) {
			parentProductIds.add(oepao.Product__c);
		}

		System.assert(allAddons.size() > 1, 'There must be more then 1 addon');
		System.assert(parentProductIds.size() == 1, 'There must be only one parent product item');
	}

	@IsTest
	static void testGetAddonPrice() {
		Decimal oneOffPrice = 5.5;
		Decimal recurringPrice = 2.5;
		TestUtils.createOrderType();
		TestUtils.createOneOffProduct();
		TestUtils.createRecurringProduct();

		Product2 ooprod = [
			SELECT Id, ProductCode
			FROM Product2
			WHERE ProductCode = :TestUtils.theOneOffProduct.ProductCode AND IsActive = TRUE
		];
		Product2 recprod = [
			SELECT Id, ProductCode
			FROM Product2
			WHERE ProductCode = :TestUtils.theRecurringProduct.ProductCode AND IsActive = TRUE
		];

		cspmb__Add_On_Price_Item__c aopi = new cspmb__Add_On_Price_Item__c();
		aopi.cspmb__One_Off_Charge_External_Id__c = ooprod.ProductCode;
		aopi.cspmb__Recurring_Charge_External_Id__c = recprod.ProductCode;
		aopi.cspmb__One_Off_Charge__c = oneOffPrice;
		aopi.cspmb__Recurring_Charge__c = recurringPrice;
		aopi.cspmb__Is_Active__c = true;
		insert aopi;

		Test.startTest();
		cspmb__Add_On_Price_Item__c price = OrderEntryController.getAddonPrice(
			TestUtils.theOneOffProduct.ProductCode
		);
		Test.stopTest();

		System.assertEquals(
			TestUtils.theOneOffProduct.ProductCode,
			price.cspmb__One_Off_Charge_External_Id__c,
			'Product code should match'
		);
		System.assertEquals(
			oneOffPrice,
			price.cspmb__One_Off_Charge__c,
			'Product price should match'
		);
		System.assertEquals(
			recurringPrice,
			price.cspmb__Recurring_Charge__c,
			'Product price should match'
		);
	}

	@IsTest
	static void testGetDefaultBundle() {
		List<OE_Product__c> products = new List<OE_Product__c>();

		OE_Product__c main = new OE_Product__c(Type__c = 'Main');
		products.add(main);

		OE_Product__c internet = new OE_Product__c(Type__c = 'Internet');
		products.add(internet);

		insert products;

		insert new OE_Product_Bundle__c(
			Main_Product__c = main.Id,
			Internet_Product__c = internet.Id,
			Bundle_Product_Code__c = '1111',
			Default__c = true
		);

		Test.startTest();
		OE_Product_Bundle__c defaultBundle = OrderEntryController.getDefaultBundle(main.Id);
		Test.stopTest();

		System.assertNotEquals(null, defaultBundle, 'Default bundle must exist');
		System.assertEquals(true, defaultBundle.Default__c, 'Bundle must be default');
	}

	@IsTest
	static void testGetBundle() {
		List<OE_Product__c> products = new List<OE_Product__c>();

		OE_Product__c main = new OE_Product__c(Type__c = 'Main');
		products.add(main);

		OE_Product__c internet = new OE_Product__c(Type__c = 'Internet');
		products.add(internet);

		insert products;

		Decimal oneOffPrice = 5.5;
		Decimal recurringPrice = 2.5;
		TestUtils.createOrderType();
		TestUtils.createOneOffProduct();
		TestUtils.createRecurringProduct();

		Product2 ooprod = [
			SELECT Id, ProductCode
			FROM Product2
			WHERE ProductCode = :TestUtils.theOneOffProduct.ProductCode AND IsActive = TRUE
		];
		Product2 recprod = [
			SELECT Id, ProductCode
			FROM Product2
			WHERE ProductCode = :TestUtils.theRecurringProduct.ProductCode AND IsActive = TRUE
		];

		insert new cspmb__Price_Item__c(
			cspmb__One_Off_Charge_External_Id__c = ooprod.ProductCode,
			cspmb__Recurring_Charge_External_Id__c = recprod.ProductCode,
			cspmb__One_Off_Charge__c = oneOffPrice,
			cspmb__Recurring_Charge__c = recurringPrice,
			cspmb__Is_Active__c = true
		);

		insert new OE_Product_Bundle__c(
			Main_Product__c = main.Id,
			Internet_Product__c = internet.Id,
			Bundle_Product_Code__c = ooprod.ProductCode,
			Default__c = true
		);

		OE_Product_Bundle__c defaultBundle = OrderEntryController.getDefaultBundle(main.Id);

		Test.startTest();
		OrderEntryData.OrderEntryBundle bundleData = OrderEntryController.getBundle(
			defaultBundle.Main_Product__c,
			defaultBundle.Internet_Product__c,
			defaultBundle.Telephony_Product__c,
			defaultBundle.TV_Product__c
		);
		Test.stopTest();

		System.assertNotEquals(null, bundleData, 'Bundle must exist');
	}

	@isTest
	static void testUpdateState() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		Test.startTest();
		insert new Attachment(
			Name = 'OrderEntryData.json',
			Body = Blob.valueOf('Blobby'),
			ParentId = opp.Id
		);

		Boolean updated = OrderEntryController.updateState(opp.Id, 'New State');
		Test.stopTest();

		List<Attachment> attPost = [
			SELECT Id, Body
			FROM Attachment
			WHERE ParentId = :opp.Id AND Name = 'OrderEntryData.json'
		];

		System.assert(
			attPost[0].Body == Blob.valueOf('New State'),
			'OrderEntryData attachment body not as expected.'
		);
		System.assert(updated, 'Attachment should be updated.');
	}

	@isTest
	static void testGetSite() {
		Site__c site = [SELECT Id, Site_Postal_Code__c FROM Site__c LIMIT 1];

		Test.startTest();

		Site__c fetchedSite = OrderEntryController.getSite(site.Id);
		Test.stopTest();

		System.assertEquals(site.Id, fetchedSite.Id, 'Ids don\'t match');
		System.assertEquals(
			site.Site_Postal_Code__c,
			fetchedSite.Site_Postal_Code__c,
			'Postal codes don\'t match.'
		);
	}

	@isTest
	static void testGetSites() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		insert new Site__c(
			Site_Street__c = 'Balistraat',
			Site_Postal_Code__c = '3531EC',
			Site_House_Number__c = 59,
			Site_City__c = 'Utrecht',
			Site_Account__c = acc.Id
		);

		Test.startTest();
		List<Site__c> fetchedSites = OrderEntryController.getSites(acc.Id);
		Test.stopTest();

		System.assert(fetchedSites.size() > 0, 'There should be at least one site.');
	}

	@isTest
	static void testUpdateSite() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Site__c site = new Site__c(
			Site_Street__c = 'Balistraat',
			Site_Postal_Code__c = '3531EC',
			Site_House_Number_Suffix__c = '',
			Site_House_Number__c = 59,
			Site_City__c = 'Utrecht',
			Site_Account__c = acc.Id
		);
		insert site;

		Site__c initSite = [
			SELECT
				Id,
				Site_Street__c,
				Site_Postal_Code__c,
				Site_House_Number_Suffix__c,
				Site_House_Number__c,
				Site_City__c,
				Site_Account__c
			FROM Site__c
			WHERE Id = :site.Id
		];
		initSite.Site_House_Number__c = 69;
		initSite.Site_City__c = 'Utrecht Noord';

		Test.startTest();
		Boolean isUpdated = OrderEntryController.updateSite(initSite, opp.Id);
		Test.stopTest();

		Site__c updatedSite = [
			SELECT Id, Site_Street__c, Site_Postal_Code__c, Site_House_Number__c, Site_City__c
			FROM Site__c
			WHERE Id = :site.Id
		];

		System.assert(isUpdated, 'Site should be updated.');
		System.assertEquals(site.Id, updatedSite.Id, 'Site id should match.');
		System.assertEquals(
			site.Site_Street__c,
			updatedSite.Site_Street__c,
			'Site street should match.'
		);
		System.assertNotEquals(
			site.Site_House_Number__c,
			updatedSite.Site_House_Number__c,
			'Site house number should not match.'
		);
		System.assert('Utrecht Noord' == updatedSite.Site_City__c, 'Site city should be updated.');
	}

	@isTest
	static void testSaveSite() {
		Account acc = [SELECT Id FROM Account LIMIT 1];

		List<Site__c> sites = [SELECT Id FROM Site__c WHERE Site_Account__c = :acc.Id];
		System.assert(sites.size() == 0, 'There should be no sites.');

		Test.startTest();
		Site__c newSIte = OrderEntryController.saveSite(
			'Medanstraat | 1 |  |  | 3531EC | Utrecht',
			acc.Id
		);
		Test.stopTest();

		List<Site__c> postSites = [SELECT Id FROM Site__c WHERE Site_Account__c = :acc.Id];
		System.assert(postSites.size() > 0, 'There should be one new site.');
		System.assertEquals(
			'Medanstraat',
			newSIte.Site_Street__c.trim(),
			'There should be one new site.'
		);
		System.assertEquals(1, newSIte.Site_House_Number__c, 'There should be one new site.');
		System.assertEquals(
			'3531EC',
			newSIte.Site_Postal_Code__c.trim(),
			'There should be one new site.'
		);
	}

	@isTest
	static void testSiteCheck() {
		Test.setMock(HttpCalloutMock.class, new MockHttpAddressCheck());

		Test.startTest();
		String result = OrderEntryController.siteCheck('1111XX', 11);
		Test.stopTest();

		System.assertEquals(result, '{"example":"test"}', 'Wrong result.');
	}

	@isTest
	static void testOblicoSearch() {
		OlbicoSettings__c oblico = new OlbicoSettings__c();
		oblico.Olbico_JSon_Endpoint__c = 'Fake endpoint';
		oblico.Olbico_Json_Username__c = 'Fake username';
		oblico.Olbico_Json_Password__c = 'Fake password';
		oblico.Olbico_Json_BAG_Streets__c = 'Fake bag street';
		insert oblico;

		Test.startTest();
		Map<String, String> result = OrderEntryController.oblicoSearch('1111XX');
		Test.stopTest();

		System.assertEquals(result.size(), 0, 'There shuld be no options returned.');
	}

	@isTest
	static void testSavePortingNumbers() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		insert new LG_PortingNumber__c(LG_Opportunity__c = opp.Id);

		createProductsAndAddons();
		insertMockJsonData(opp.Id, opp.Id);

		Test.startTest();
		OrderEntryController.savePortingNumbers(opp.Id);
		Test.stopTest();

		List<LG_PortingNumber__c> result = [
			SELECT Id
			FROM LG_PortingNumber__c
			WHERE LG_Opportunity__c = :opp.Id
		];
		System.assertEquals(1, result.size(), 'Old number should be deleted and new one added.');
	}

	@isTest
	static void testSaveProducts() {
		Account acc = new Account(Name = 'Test');
		insert acc;

		Contact c = new Contact(LastName = 'Test');
		insert c;

		Id standardPricebookId = Test.getStandardPricebookId();

		Id ziggoRecordTypeId = [
			SELECT Id
			FROM RecordType
			WHERE SobjectType = 'Opportunity' AND Name = 'Ziggo'
		][0]
		.Id;

		Opportunity opp = new Opportunity(
			Name = 'Test',
			AccountId = acc.Id,
			StageName = 'Prospecting',
			CloseDate = Date.today(),
			RecordTypeId = ziggoRecordTypeId,
			Pricebook2Id = standardPricebookId
		);
		insert opp;

		OrderType__c ziggoOrderType = new OrderType__c(
			Name = 'Ziggo',
			ExportSystem__c = 'EMAIL',
			Status__c = 'Processed manually',
			ExternalID__c = 'OT-04-0000003'
		);
		insert ziggoOrderType;

		Product2 prod = new Product2(
			Name = 'Test',
			Product_Line__c = 'fZiggo Only',
			OrderType__c = ziggoOrderType.Id,
			ProductCode = 'PD-00060'
		);
		insert prod;

		PricebookEntry pbe = new PricebookEntry(
			Product2Id = prod.Id,
			Pricebook2Id = standardPricebookId,
			UnitPrice = 1,
			IsActive = true
		);
		insert pbe;

		insert new LG_ProductDetail__c(LG_Name__c = 'Test', LG_Opportunity__c = opp.Id);

		createProductsAndAddons();

		insert new Order_Entry_Product_Detail__c(
			Product__c = oeProducts[1].Id,
			Category__c = 'Category',
			Value__c = 'Value',
			Type__c = 'Type'
		);

		insertMockJsonData(opp.Id, c.Id);

		Test.startTest();
		OrderEntryController.saveProducts(opp.Id);
		Test.stopTest();

		List<OpportunityLineItem> resultOlis = [
			SELECT Id
			FROM OpportunityLineItem
			WHERE OpportunityId = :opp.Id
		];
		List<LG_ProductDetail__c> resultDetails = [
			SELECT Id
			FROM LG_ProductDetail__c
			WHERE LG_Opportunity__c = :opp.Id
		];
		System.assertEquals(
			2,
			resultOlis.size(),
			'Two Opportunity Product records shoud be created.'
		);
		System.assertEquals(
			1,
			resultDetails.size(),
			'Old Product Detail record should be deleted and new one created.'
		);
	}

	@isTest
	static void testGetBundlePromotions() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		createProductsAndAddons();
		insertMockJsonData(opp.Id, opp.Id);

		OE_Promotion__c promotion = new OE_Promotion__c(
			Promotion_Name__c = 'De eerste maand 100% korting op uw Zakelijk Start abonnement',
			Type__c = 'Standard',
			Active__c = true,
			Customer_Type__c = 'Existing',
			Contract_Term__c = '1',
			Connection_Type__c = 'Off-Net',
			Off_net_Type__c = 'MTC'
		);
		insert promotion;

		OE_Discount__c discount = new OE_Discount__c(
			Name = 'De eerste maand Safe Online XL gratis',
			Type__c = 'Percentage',
			Value__c = 100,
			Level__c = 'Bundle'
		);
		insert discount;

		insert new OE_Promotion_Discount__c(Promotion__c = promotion.Id, Discount__c = discount.Id);

		Test.startTest();
		List<OE_Promotion__c> result = OrderEntryController.getBundlePromotions(
			opp.Id,
			true,
			null,
			null
		);
		Test.stopTest();

		System.assertEquals(result.size(), 1, 'One bundle promotion should be returned.');
	}

	@isTest
	static void testGetSpecialPromotions() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		createProductsAndAddons();
		insertMockJsonData(opp.Id, opp.Id);

		insert new OE_Promotion__c(
			Promotion_Name__c = 'De eerste maand Ziggo Sport Totaal gratis',
			Type__c = 'Special'
		);

		List<OrderEntryData.OrderEntryProduct> jsonProducts = new List<OrderEntryData.OrderEntryProduct>();

		OrderEntryData.OrderEntryProduct prod1 = new OrderEntryData.OrderEntryProduct();
		prod1.id = oeProducts[0].Id;
		prod1.type = 'Main';
		jsonProducts.add(prod1);

		OrderEntryData.OrderEntryProduct prod2 = new OrderEntryData.OrderEntryProduct();
		prod2.id = oeProducts[1].Id;
		prod2.type = 'Internet';
		jsonProducts.add(prod2);

		OrderEntryData.OrderEntryProduct prod3 = new OrderEntryData.OrderEntryProduct();
		prod3.id = oeProducts[2].Id;
		prod3.type = 'TV';
		jsonProducts.add(prod3);

		List<OrderEntryData.OrderEntryAddon> jsonAddons = new List<OrderEntryData.OrderEntryAddon>();

		OrderEntryData.OrderEntryAddon addon1 = new OrderEntryData.OrderEntryAddon();
		addon1.bundleName = 'Safe Online';
		addon1.code = 'PD-00060';
		addon1.id = oeAddons[0].Id;
		addon1.name = 'Safe Online';
		addon1.oneOff = 0;
		addon1.parentProduct = oeProducts[1].Id;
		addon1.parentType = 'Internet';
		addon1.quantity = 1;
		addon1.recurring = 0;
		addon1.totalOneOff = 0;
		addon1.totalRecurring = 0;
		addon1.type = 'Internet Security';
		jsonAddons.add(addon1);

		OrderEntryData.OrderEntryAddon addon2 = new OrderEntryData.OrderEntryAddon();
		addon2.bundleName = 'Extra internetpunt via stopcontact';
		addon2.code = 'PD-00095';
		addon2.id = oeAddons[1].Id;
		addon2.name = 'Extra internetpunt via stopcontact';
		addon2.oneOff = 53.72;
		addon2.parentProduct = oeProducts[1].Id;
		addon2.parentType = 'Internet';
		addon2.quantity = 0;
		addon2.recurring = 0;
		addon2.totalOneOff = 0;
		addon2.totalRecurring = 0;
		addon2.type = 'Internet Additional';
		jsonAddons.add(addon2);

		Test.startTest();
		List<OE_Promotion__c> result = OrderEntryController.getSpecialPromotions(
			opp.Id,
			jsonProducts,
			jsonAddons
		);
		Test.stopTest();

		System.assertEquals(result.size(), 1, 'One special promotion should be returned.');
	}

	@isTest
	static void testGetBankAccountHolder() {
		Contact c = new Contact(LastName = 'Test');
		insert c;

		Test.startTest();
		String result = OrderEntryController.getBankAccountHolder(c.Id);
		Test.stopTest();

		System.assertEquals(result, 'Test', 'Contact name should be returned.');
	}

	@isTest
	static void testCreateDiscountRelatedRecords() {
		Account acc = new Account(Name = 'Test');
		insert acc;

		Contact c = new Contact(LastName = 'Test');
		insert c;

		Id ziggoRecordTypeId = [
			SELECT Id
			FROM RecordType
			WHERE SobjectType = 'Opportunity' AND Name = 'Ziggo'
		][0]
		.Id;

		Opportunity opp = new Opportunity(
			Name = 'Test',
			AccountId = acc.Id,
			StageName = 'Prospecting',
			CloseDate = Date.today(),
			RecordTypeId = ziggoRecordTypeId
		);
		insert opp;

		createProductsAndAddons();
		insertMockJsonData(opp.Id, c.Id);

		insert new OrderType__c(
			Name = 'Ziggo',
			ExportSystem__c = 'EMAIL',
			Status__c = 'Processed manually',
			ExternalID__c = 'OT-04-0000003'
		);

		Pricebook2 standardPricebook = new Pricebook2(
			Id = Test.getStandardPricebookId(),
			IsActive = true
		);
		update standardPricebook;

		Test.startTest();
		OrderEntryController.createDiscountRelatedRecords(opp.Id);
		Test.stopTest();

		List<OpportunityLineItem> resultOlis = [
			SELECT Id
			FROM OpportunityLineItem
			WHERE OpportunityId = :opp.Id
		];
		List<csconta__Billing_Account__c> resultBillingAccs = [
			SELECT Id
			FROM csconta__Billing_Account__c
		];
		System.assertEquals(1, resultOlis.size(), 'Opportunity Product should be created.');
		System.assertEquals(1, resultBillingAccs.size(), 'Billing Account should be created.');
	}

	@isTest
	static void updateOpportunityStage() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		OrderEntryController.updateOpportunityStage(opp.Id, 'Customer Approved');
		Test.stopTest();
		Opportunity oppResult = [SELECT Id, StageName FROM Opportunity WHERE Id = :opp.Id];
		System.assertEquals(
			'Customer Approved',
			oppResult.StageName,
			'Opportunity should be updated.'
		);
	}

	@isTest
	static void updateOpportunityAutomatedQuoteDelivery() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		OrderEntryController.updateOpportunityAutomatedQuoteDelivery(opp.Id, 'Quote Requested');
		Test.stopTest();
		Opportunity oppResult = [
			SELECT Id, LG_AutomatedQuoteDelivery__c
			FROM Opportunity
			WHERE Id = :opp.Id
		];
		System.assertEquals(
			'Quote Requested',
			oppResult.LG_AutomatedQuoteDelivery__c,
			'Opportunity should be updated.'
		);
	}

	@isTest
	static void getOpportunity() {
		Opportunity opp = [SELECT Id, Name, StageName FROM Opportunity LIMIT 1];

		List<String> oppField = new List<String>();
		oppField.add('Name');
		oppField.add('StageName');
		Test.startTest();
		Opportunity result = OrderEntryController.getOpportunity(opp.Id, oppField);
		Test.stopTest();
		System.assertEquals(opp.Name, result.Name, 'Name is same');
		System.assertEquals(opp.StageName, result.StageName, 'StageName is same');
	}

	@isTest
	static void saveOpportunityDescription() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

		Test.startTest();
		Opportunity result = OrderEntryController.saveOpportunityDescription(opp.Id, 'Test Notes');
		Test.stopTest();
		Opportunity oppResult = [SELECT Id, Description FROM Opportunity WHERE Id = :opp.Id];
		System.assertEquals('Test Notes', oppResult.Description, 'Description should be updated. ');
	}

	@isTest
	static void testCloneOrder() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

		OrderEntryData data = new OrderEntryData();
		data.opportunityId = opp.Id;
		data.lastStep = 'Select Product';
		data.init();

		Attachment att = new Attachment(
			Name = 'OrderEntryData.json',
			Body = Blob.valueOf(JSON.serializePretty(data)),
			ParentId = opp.Id
		);

		insert att;

		Test.startTest();
		Opportunity newOpp = OrderEntryController.cloneOrder(opp.Id, 'Test Clone');
		Test.stopTest();
		System.assertEquals('Awareness of interest', newOpp.StageName, 'Opportunity is inserted.');
		System.assertEquals(Date.today(), newOpp.CloseDate, 'Opportunity is inserted. ');
	}

	@isTest
	static void updateOpportunitySingedQuote() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Attachment attachment = new Attachment(
			ParentId = opp.Id,
			Name = 'quot.pdf',
			Body = Blob.toPdf('string')
		);
		insert attachment;
		List<Attachment> attachmentList = [SELECT Id FROM Attachment WHERE ParentId = :opp.Id];
		Test.startTest();
		if (!attachmentList.isEmpty()) {
			OrderEntryController.updateOpportunitySingedQuote(opp.Id, attachmentList[0].Id, true);
		}
		Test.stopTest();
		Opportunity oppResult = [
			SELECT Id, LG_SignedQuoteAvailable__c
			FROM Opportunity
			WHERE Id = :opp.Id
		];
		System.assertEquals(
			'true',
			oppResult.LG_SignedQuoteAvailable__c,
			'Opportunity should be updated.'
		);
	}

	@isTest
	static void updateQuoteAttachment() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Attachment attachment = new Attachment(
			ParentId = opp.Id,
			Name = 'quot.pdf',
			Body = Blob.toPdf('string')
		);
		insert attachment;

		ContentVersion cv = new ContentVersion(
			Title = 'test',
			PathOnClient = 'quot.pdf',
			VersionData = Blob.toPdf('string')
		);
		insert cv;
		ContentVersion conVer = [
			SELECT Id, ContentDocumentId
			FROM ContentVersion
			WHERE Id = :cv.Id
		];
		ContentDocumentLink cdl = new ContentDocumentLink();
		cdl.ContentDocumentId = conVer.ContentDocumentId;
		cdl.LinkedEntityId = opp.Id;
		cdl.ShareType = 'V';
		insert cdl;

		List<Attachment> attachmentList = [SELECT Id FROM Attachment WHERE ParentId = :opp.Id];
		Test.startTest();

		Boolean attachmentUpdate = OrderEntryController.updateQuoteAttachment(
			attachmentList[0].Id,
			EncodingUtil.base64Encode(Blob.toPdf('String'))
		);
		Boolean contentVersionUpdate = OrderEntryController.updateQuoteAttachment(
			conVer.Id,
			EncodingUtil.base64Encode(Blob.toPdf('String'))
		);

		Test.stopTest();
		System.assertEquals(true, attachmentUpdate, 'Attachment should be updated.');
		System.assertEquals(true, contentVersionUpdate, 'ContentVersion should be updated.');
	}

	@isTest
	static void generateConfirmation() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		mmdoc__Document_Request__c doc = OrderEntryController.generateConfirmation(opp.Id);
		Test.stopTest();
		System.assertEquals(doc.mmdoc__Status__c, 'New', 'Confirmation should be created.');
	}

	@isTest
	static void generateQuote() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		mmdoc__Document_Request__c doc = OrderEntryController.generateQuote(opp.Id);
		Test.stopTest();
		OrderEntryController.getDocumentRequest(doc.Id);
		System.assertEquals(doc.mmdoc__Status__c, 'New', 'Quote should be created.');
	}

	static void createProductsAndAddons() {
		oeProducts = new List<OE_Product__c>();
		oeProducts.add(new OE_Product__c(Name = 'Main', Type__c = 'Main'));
		oeProducts.add(new OE_Product__c(Name = 'Internet', Type__c = 'Internet'));
		oeProducts.add(new OE_Product__c(Name = 'TV', Type__c = 'TV'));
		insert oeProducts;

		oeAddons = new List<OE_Add_On__c>();
		oeAddons.add(
			new OE_Add_On__c(
				Name = 'Safe Online',
				Product_Code__c = 'PD-00060',
				Type__c = 'Internet Security'
			)
		);
		oeAddons.add(
			new OE_Add_On__c(
				Name = 'Extra internetpunt via stopcontact',
				Product_Code__c = 'PD-00095',
				Type__c = 'Internet Additional'
			)
		);
		insert oeAddons;
	}

	static void insertMockJsonData(Id oppId, Id contactId) {
		insert new Attachment(
			Name = 'OrderEntryData.json',
			Body = Blob.valueOf(
				'{"accountId":"' +
				[SELECT Id FROM Account][0]
				.Id +
				'","addons":[{"bundleName":"Safe Online","code":"PD-00060","id":"' +
				oeAddons[0].Id +
				'","name":"Safe Online","oneOff":0,"parentProduct":"' +
				oeProducts[1].Id +
				'","parentType":"Internet","quantity":1,"recurring":0,"totalOneOff":0,"totalRecurring":0,"type":"Internet Security"},{"bundleName":"Extra internetpunt via stopcontact","code":"PD-00095","id":"' +
				oeAddons[1].Id +
				'","name":"Extra internetpunt via stopcontact","oneOff":53.72,"parentProduct":"' +
				oeProducts[1].Id +
				'","parentType":"Internet","quantity":0,"recurring":0,"totalOneOff":0,"totalRecurring":0,"type":"Internet Additional"}],"addressCheckResult":"Off-Net","b2cInternetCustomer":false,"bundle":{"bundleName":"Zakelijk Internet Start","code":"PD-00060","id":"aFQ5r0000004C9YGAU","name":"Zakelijk Internet Start","quantity":1,"recurring":42,"totalRecurring":42,"type":"Internet"},"contractTerm":"1","lastStep":"Product Configuration","notes":"Tests","offNetType":"MTC","operatorSwitch":{"currentContractNumber":"","currentProvider":"","phone":"","potentialFeeAccepted":false,"requested":false},"opportunityId":"' +
				oppId +
				'","payment":{"bankAccountHolder":"Tojo Tojo","billingChannel":"Paper bill","iban":"NL69BCDM0217046703","paymentType":"Direct Debit"},"primaryContactId":"' +
				contactId +
				'","products":[{"id":"' +
				oeProducts[0].Id +
				'","type":"Main"},{"id":"' +
				oeProducts[1].Id +
				'","type":"Internet"},{"id":"' +
				oeProducts[2].Id +
				'","type":"TV"}],"promos":[{"connectionType":"On-net","contractTerms":"1;2;3","customerType":"New","discounts":[{"duration":1,"id":"aFT5r000000CaRcGAK","level":"Bundle","name":"De eerste maand 100% korting op uw Zakelijk Start abonnement","product":"' +
				oeProducts[1].Id +
				'","type":"Percentage","value":100}],"id":"aFU5r0000008OKKGA2","name":"De eerste maand 100% korting op uw Zakelijk Start abonnement","type":"Standard"}],"siteCheck":{"availability":[{"available":false,"name":"dtv-horizon"},{"available":false,"name":"packages"},{"available":false,"name":"mobile_internet"},{"available":false,"name":"fp200"},{"available":false,"name":"internet"},{"available":false,"name":"dtv"},{"available":false,"name":"catv"},{"available":false,"name":"telephony"},{"available":false,"name":"mobile"},{"available":false,"name":"catvfee"},{"available":false,"name":"fp500"},{"available":false,"name":"fp1000"}],"city":"WAARDENBURG","houseNumber":"8","houseNumberExt":"","status":[],"street":"STEENWEG","zipCode":"4181AL"},"siteId":"' +
				[SELECT Id FROM Site__c][0]
				.Id +
				'","telephony":{"Telephony":{"portingEnabled":true,"portingNumber":"0641234567"}}}'
			),
			ParentId = oppId
		);
	}
}