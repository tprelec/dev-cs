@isTest
public with sharing class TestOrderEntryData {
	@TestSetup
	static void makeData() {
		TestUtils.createCompleteOpportunity();
	}

	@isTest
	static void testDataCreation() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Contact cnt = [SELECT Id FROM Contact LIMIT 1];

		OrderEntryData oeData = new OrderEntryData();
		oeData.accountId = acc.Id;
		oeData.opportunityId = opp.Id;
		oeData.primaryContactId = cnt.Id;
		oeData.siteId = acc.Id;
		oeData.lastStep = 'last step';
		oeData.contractTerm = 'contract term';
		oeData.addressCheckResult = 'addressCheckResult';
		oeData.offNetType = 'offNetType';
		oeData.b2cInternetCustomer = false;
		oeData.notes = 'notes';

		OrderEntryData.OrderEntryProduct oep = new OrderEntryData.OrderEntryProduct();
		oep.id = acc.Id;
		oep.type = 'type';
		oeData.products = new List<OrderEntryData.OrderEntryProduct>{ oep };

		OrderEntryData.OrderEntryAddon oea = new OrderEntryData.OrderEntryAddon();
		oea.id = acc.Id;
		oea.type = 'type';
		oea.name = 'name';
		oea.code = 'code';
		oea.quantity = 5;
		oea.recurring = 0.5;
		oea.oneOff = 5.0;
		oea.totalRecurring = 5.0;
		oea.totalOneOff = 5.0;
		oea.parentProduct = acc.Id;
		oea.bundleName = 'bundleName';
		oea.parentType = 'parentType';
		oeData.addons = new List<OrderEntryData.OrderEntryAddon>{ oea };

		OrderEntryData.OrderEntryBundle bundle = new OrderEntryData.OrderEntryBundle();
		bundle.id = acc.Id;
		bundle.type = 'type';
		bundle.name = 'name';
		bundle.code = 'code';
		bundle.quantity = 5;
		bundle.recurring = 0.5;
		bundle.oneOff = 5.0;
		bundle.totalRecurring = 5.0;
		bundle.totalOneOff = 5.0;
		bundle.parentProduct = acc.Id;
		bundle.bundleName = 'bundleName';
		oeData.bundle = bundle;

		OrderEntryData.Availability availability = new OrderEntryData.Availability();
		availability.available = true;
		availability.name = 'name';
		OrderEntryData.SiteCheck siteCheck = new OrderEntryData.SiteCheck();
		siteCheck.availability = new List<OrderEntryData.Availability>{ availability };
		siteCheck.city = 'city';
		siteCheck.street = 'street';
		siteCheck.houseNumber = 'houseNumber';
		siteCheck.houseNumberExt = 'houseNumberExt';
		siteCheck.zipCode = 'zipCode';
		siteCheck.status = new List<String>{ 'status1', 'status2' };
		siteCheck.footprint = 'footprint';
		oeData.siteCheck = siteCheck;

		OrderEntryData.Telephony tel = new OrderEntryData.Telephony();
		tel.enabled = false;
		tel.portingEnabled = true;
		tel.portingNumber = '13';
		Map<String, OrderEntryData.Telephony> telMap = new Map<String, OrderEntryData.Telephony>();
		telMap.put('tel1', tel);
		oeData.telephony = telMap;

		OrderEntryData.Discount disc = new OrderEntryData.Discount();
		disc.id = acc.Id;
		disc.name = 'name';
		disc.type = 'type';
		disc.duration = 12.0;
		disc.value = 13.0;
		disc.level = 'level';
		disc.product = acc.Id;
		disc.product = acc.Id;
		disc.addon = acc.Id;
		OrderEntryData.Promotion prom = new OrderEntryData.Promotion();
		prom.id = acc.Id;
		prom.name = 'name';
		prom.type = 'type';
		prom.contractTerms = 'contractTerms';
		prom.customerType = 'customerType';
		prom.connectionType = 'connectionType';
		prom.offNetType = 'offNetType';
		prom.discounts = new List<OrderEntryData.Discount>{ disc };
		oeData.promos = new List<OrderEntryData.Promotion>{ prom };

		OrderEntryData.Payment payment = new OrderEntryData.Payment();
		payment.bankAccountHolder = 'bankAccountHolder';
		payment.paymentType = 'paymentType';
		payment.iban = 'iban';
		payment.billingChannel = 'billingChannel';
		oeData.payment = payment;

		OrderEntryData.PreferredDate d = new OrderEntryData.PreferredDate();
		d.selectedDate = Date.today();
		d.dayPeriod = 'night';

		OrderEntryData.Installation install = new OrderEntryData.Installation();
		install.earliestInstallationDate = Date.today();
		install.assignTechnician = true;
		install.preferredDate1 = d;
		install.preferredDate2 = d;
		install.preferredDate3 = d;
		oeData.installation = install;

		OrderEntryData.OperatorSwitch oSwitch = new OrderEntryData.OperatorSwitch();
		oSwitch.requested = true;
		oSwitch.currentProvider = 'currentProvider';
		oSwitch.currentContractNumber = 'currentContractNumber';
		oSwitch.potentialFeeAccepted = true;
		oeData.operatorSwitch = oSwitch;

		System.assertEquals(oeData.accountId, acc.Id, 'Wrong value.');
		System.assertEquals(oeData.opportunityId, opp.Id, 'Wrong value.');
		System.assertEquals(oeData.primaryContactId, cnt.Id, 'Wrong value.');
		System.assertEquals(oeData.addressCheckResult, 'addressCheckResult', 'Wrong value.');
		System.assertEquals(oeData.b2cInternetCustomer, false, 'Wrong value.');
		System.assertEquals(oeData.addons[0].bundleName, 'bundleName', 'Wrong value.');
		System.assertEquals(
			oeData.operatorSwitch.currentProvider,
			'currentProvider',
			'Wrong value.'
		);
	}
}