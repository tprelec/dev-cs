@IsTest
public with sharing class TestOrderEntryRedirectController {
	@IsTest
	static void testIsCommunity() {
		Test.startTest();
		Boolean isCommunity = OrderEntryRedirectController.isCommunity();
		Test.stopTest();
		System.assert(!isCommunity, 'By default user is not running in community context.');
	}
}