@SuppressWarnings('PMD')
// Can be removed after refactoring
@isTest
private class TestOrderExport {
	@TestSetup
	static void makeData() {
		Testutils.createVFContractValidationOpportunity();
		Testutils.createOrderValidationOpportunity();
		Testutils.createOrderValidationNetProfitInformation();
		Testutils.createOrderValidationBilling();
		Testutils.createOrderValidationOrder();
		Testutils.createOrderValidationSite();
		Testutils.createOrderValidationContractedProducts();
		Testutils.createOrderValidations();
		Testutils.createOrderValidationNumberportingRow();
		TestUtils.createOrderValidationPhonebookRegistration();
		TestUtils.createOrderValidationHBO();
		TestUtils.createOrderValidationCompetitorAsset();
	}

	@isTest
	static void testStandardCreate() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		Map<String, User> admins = TestUtils.createAdmins(2);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		createCompleteContract(null, false);

		Test.setMock(WebServiceMock.class, new ECSSOAPOrderMocks.CreateOrdersResponse());
		System.runAs(admins.get('Admin1')) {
			Test.startTest();
			OrderExport.exportOrdersOffline(new Set<Id>{ [SELECT id FROM Order__c LIMIT 1].Id });
			Test.stopTest();
		}

	}

	@isTest
	static void testStandardUpdate() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		Map<String, User> admins = TestUtils.createAdmins(2);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		createCompleteContract('2', false);

		Test.setMock(WebServiceMock.class, new ECSSOAPOrderMocks.CreateOrdersResponse());
		System.runAs(admins.get('Admin1')) {
			Test.startTest();
			OrderExport.exportOrdersOffline(new Set<Id>{ [SELECT id FROM Order__c LIMIT 1].Id });
			Test.stopTest();
		}

	}

	@isTest
	public static void testOrderDelete() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		Map<String, User> admins = TestUtils.createAdmins(2);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		createCompleteContract('1', false);

		Test.setMock(WebServiceMock.class, new ECSSOAPOrderMocks.CancelOrdersResponse());
		System.runAs(admins.get('Admin1')) {
			Test.startTest();
			OrderExport.cancelOrdersOffline(new Set<Id>{ [SELECT id FROM Order__c LIMIT 1].Id });
			Test.stopTest();
		}
	}

	@isTest
	public static void testOrderLegacy() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		Map<String, User> admins = TestUtils.createAdmins(2);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		createCompleteContract('1', false);

		System.runAs(owner) {
			theOrder.Propositions__c = 'Legacy';
			theOrder.Infra_Contact__c = null;
			update theOrder;
			numPort.Phonebook_Registration_Option__c = 'Update the phonebook registration';
			update numPort;
		}
		Test.setMock(WebServiceMock.class, new ECSSOAPOrderMocks.LegacyOrdersResponse());
		System.runAs(admins.get('Admin1')) {
			Test.startTest();
			OrderExport.exportLegacyOrdersOffline(
				new Set<Id>{ [SELECT id FROM Order__c LIMIT 1].Id }
			);
			Test.stopTest();
		}
	}

	@isTest
	public static void testCatchExceptions() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		Map<String, User> admins = TestUtils.createAdmins(2);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		createCompleteContract(null, false);

		System.runAs(owner) {
			theOrder.Propositions__c = 'Legacy';
			theOrder.Infra_Contact__c = null;
			update theOrder;
			numPort.Phonebook_Registration_Option__c = 'Update the phonebook registration';
			update numPort;
		}
		Test.setMock(WebServiceMock.class, new ECSSOAPOrderMocks.CatchExceptionResponse());
		System.runAs(admins.get('Admin1')) {
			Test.startTest();
			OrderExport.exportOrdersOffline(new Set<Id>{ [SELECT id FROM Order__c LIMIT 1].Id });
			OrderExport.exportLegacyOrdersOffline(
				new Set<Id>{ [SELECT id FROM Order__c LIMIT 1].Id }
			);
			Test.stopTest();
		}
	}

	@isTest
	public static void testCatchExceptionsCancel() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		Map<String, User> admins = TestUtils.createAdmins(2);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		createCompleteContract('1', false);

		System.runAs(owner) {
			theOrder.Propositions__c = 'Legacy';
			theOrder.Infra_Contact__c = null;
			update theOrder;
			numPort.Phonebook_Registration_Option__c = 'Hide the number details in the phonebook';
			update numPort;
		}
		Test.setMock(WebServiceMock.class, new ECSSOAPOrderMocks.CatchExceptionResponse());
		System.runAs(admins.get('Admin1')) {
			Test.startTest();
			OrderExport.cancelOrdersOffline(new Set<Id>{ [SELECT id FROM Order__c LIMIT 1].Id });
			Test.stopTest();
		}
	}

	@isTest
	static void testStandardCreateWithPartner() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('DealerInfoTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('BanTriggerHandler', null, 0);
		Map<String, User> admins = TestUtils.createAdmins(2);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		createCompleteContract(null, true);
		User owner;

		System.runAs(admins.get('Admin2')) {
			Account a = TestUtils.createPartnerAccount('MIH');
			TestUtils.autoCommit = false;
			a.OwnerId = admins.get('Admin2').Id;
			update a;
			owner = TestUtils.createPortalUser(a);
			Contact c = TestUtils.portalContact(a);
			insert c;
			owner.ContactId = c.Id;
			insert owner;
		}

		System.runAs(admins.get('Admin1')) {
			Opportunity opp = [SELECT Id, OwnerId FROM Opportunity LIMIT 1];
			opp.OwnerId = owner.Id;
			update opp;
		}

		Test.setMock(WebServiceMock.class, new ECSSOAPOrderMocks.CreateOrdersResponse());
		System.runAs(admins.get('Admin1')) {
			Test.startTest();
			OrderExport.exportOrdersOffline(new Set<Id>{ [SELECT id FROM Order__c LIMIT 1].Id });
			Test.stopTest();
		}

	}

	public static Order__c theOrder;
	public static User owner;
	public static Numberporting__c numPort;

	public static void createCompleteContract(String bopOrderId, Boolean userPartnerAccount) {
		System.debug('Limit Queries  start --- ' + Limits.getQueries());
		owner = TestUtils.generateTestUser('admin', 'user', 'System Administrator');
		System.debug('Limit Queries TestUtils.generateTestUser --- ' + Limits.getQueries());

		System.runAs(owner) {
			Profile p = [SELECT Id FROM Profile WHERE Name = 'VF Indirect Inside Sales' LIMIT 1];
			User is = TestUtils.generateTestUser('testing', 'is', p.Id, null);
			insert is;
			UserRole ur = [
				SELECT ID, Name
				FROM UserRole
				WHERE DeveloperName = 'Network_Planner'
				LIMIT 1
			];
			User networkPlanner = TestUtils.generateTestUser('Network', 'Planner', p.Id, ur.Id);
			insert networkPlanner;
			Account acct = userPartnerAccount
				? TestUtils.createPartnerAccount()
				: TestUtils.createAccount(owner);
			System.debug('Limit Queries TestUtils.createAccount --- ' + Limits.getQueries());
			Contact generalContact = new Contact(
				AccountId = acct.Id,
				LastName = 'Current',
				FirstName = 'User',
				Phone = '06012345678',
				MobilePhone = '06012345678',
				Email = 'tstcontact@mail.com'
			);
			insert generalContact;
			Ban__c ban = createBan(acct);
			System.debug('Limit Queries  TestUtils.createBan --- ' + Limits.getQueries());
			OrderType__c ot = TestUtils.createOrderType();
			System.debug('Limit Queries  TestUtils.createOrderType --- ' + Limits.getQueries());
			Opportunity opp = TestUtils.createOpportunityWithBan(
				acct,
				Test.getStandardPricebookId(),
				ban
			);
			System.debug(
				'Limit Queries  TestUtils.createOpportunityWithBan --- ' + Limits.getQueries()
			);
			Site__c site = TestUtils.createSite(acct);
			System.debug('Limit Queries  TestUTils.createSite --- ' + Limits.getQueries());
			//disable auto commit for the TestUtils class so we can insert Lists instead of entries one-by-one
			TestUtils.autoCommit = false;
			List<String> productRoles = new List<String>{
				'Link (Internet)',
				'Link',
				'Carrier',
				'Numberporting',
				'Phone',
				'ERS',
				'Dark Fiber',
				'Router'
			};
			List<String> linkTypesProducts = new List<String>{
				'ADSL',
				'EOC',
				'FIBER',
				'SDSL',
				'VDSL',
				'WEAS',
				'WEAS',
				'ADSL'
			};

			List<Product2> products = new List<Product2>();
			for (Integer i = 0; i < productRoles.size(); i++) {
				Product2 prod = TestUtils.createProduct();
				prod.Role__c = productRoles[i];
				prod.Link_type__c = linkTypesProducts[i];
				prod.Quantity_type__c = 'Monthly';
				prod.Product_Group__c = 'Priceplan';
				if (productRoles[i] == 'Router') {
					prod.Brand__c = 'TestBrand';
					prod.Model__c = 'TestModel';
				}
				products.add(prod);
			}
			insert products;
			System.debug('Limit Queries  insert products --- ' + Limits.getQueries());

			List<PricebookEntry> pbEntries = new List<PricebookEntry>();
			for (Integer i = 0; i < productRoles.size(); i++) {
				pbEntries.add(
					TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i))
				);
			}
			insert pbEntries;
			System.debug('Limit Queries  insert pbentries --- ' + Limits.getQueries());
			Decimal totalAmount = 0.0;
			List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
			for (Integer i = 0; i < productRoles.size(); i++) {
				OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(
					opp,
					pbEntries.get(i)
				);
				oppLineItem.Site_List__c = site.Id + ',' + site.Id;
				oppLineItem.Quantity = 2;
				oppLineItems.add(oppLineItem);
				totalAmount +=
					oppLineItem.Product_Arpu_Value__c *
					oppLineItem.Duration__c *
					oppLineItem.Quantity;
			}

			Site_Availability__c sa = new Site_Availability__c(
				Site__c = site.Id,
				Access_Infrastructure__c = 'Coax',
				Bandwith_Down_Entry__c = 50,
				Bandwith_Down_Premium__c = 50,
				Bandwith_Up_Entry__c = 100,
				Bandwith_Up_Premium__c = 100,
				Existing_Infra__c = false,
				Vendor__c = 'ZIGGO',
				Premium_Vendor__c = 'ZIGGO',
				Result_Check__c = 'test',
				Region__c = 'Test'
			);
			insert sa;
			Site_Postal_Check__c spc = new Site_Postal_Check__c(
				Access_Site_ID__c = site.Id,
				Access_Active__c = false,
				Access_Vendor__c = 'ZIGGO',
				Access_Result_Check__c = 'OFFNET'
			);
			insert spc;

			User np = [
				SELECT Id, Name, UserRoleId, UserRole.DeveloperName
				FROM User
				WHERE FirstName = 'Network' AND LastName = 'Planner'
				LIMIT 1
			];
			HBO__c hbo = new HBO__c(
				hbo_account__c = acct.Id,
				hbo_opportunity__c = opp.Id,
				hbo_status__c = 'New',
				hbo_site__c = site.Id,
				hbo_network_planner__c = np.Id,
				hbo_delivery_time__c = 12,
				hbo_digging_distance__c = 100,
				hbo_total_costs__c = 500,
				hbo_redundancy_type__c = 'hbo',
				hbo_redundancy__c = false,
				hbo_availability__c = 'Yes',
				hbo_postal_check__c = spc.Id
			);
			insert hbo;
			hbo.hbo_status__c = 'Approved';
			update hbo;

			oppLineItems[0].Site_Availability_List__c = sa.Id + ';' + sa.Id;

			insert oppLineItems;
			System.debug('Limit Queries insert opplineitems --- ' + Limits.getQueries());
			// create Contract
			VF_Contract__c c = new VF_Contract__c(
				Account__c = acct.Id,
				Opportunity__c = opp.id,
				Solution_Sales__c = is.Id
			);

			insert c;
			System.debug('Limit Queries  VF_contract__c --- ' + Limits.getQueries());

			Decimal value = 10;
			Integer quantity = 1;

			theOrder = new Order__c();
			theOrder.VF_Contract__c = c.Id;
			theOrder.OrderType__c = ot.Id;
			theOrder.Account__c = c.Account__c;
			theOrder.Escalation_Level__c = 'high';
			theOrder.Export__c = 'SIAS';
			if (bopOrderId != null)
				theOrder.BOP_Order_Id__c = bopOrderId;
			theOrder.Status__c = 'Assigned to Inside Sales';
			theOrder.Inside_Sales_Owner__c = is.Id;
			theOrder.Project_Contact__c = generalContact.Id;
			theOrder.Infra_Contact__c = generalContact.Id;
			theOrder.CC_Contact__c = generalContact.Id;
			theOrder.Porting_Contact__c = generalContact.Id;
			theOrder.Propositions__c = 'Internet;IPVPN;One Net;One Fixed;Voice';
			insert theOrder;

			List<Contracted_Products__c> cpInsert = new List<Contracted_Products__c>();
			List<String> linkTypes = new List<String>{
				'EthernetOverFiber',
				'FTTH',
				'EthernetOverCopper',
				'VVDSL',
				'COAX'
			};
			Contracted_Products__c cp = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[0].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENETX2014,ONENET2014,ONENET2014SUB30,One Net Enterprise,One Net Express',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp);
			Contracted_Products__c cp0 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[0].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENET2014',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp0);
			Contracted_Products__c cp01 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[0].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENET2014SUB30',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp01);
			Contracted_Products__c cp02 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[0].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENETX2014',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp02);
			Contracted_Products__c cp1 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[1].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Link_type__c = linkTypes[1],
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'IPVPN',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp1);
			// COAX != link type and product role carrier/dark fiber
			Contracted_Products__c cp2 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[3].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENET2014SUB30,One Net Express',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp2);
			// role = phone, order preposition not legacy
			Contracted_Products__c cp3 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[4].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Fixed'
			);
			cpInsert.add(cp3);
			// ERS
			Contracted_Products__c cp4 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[5].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Link_type__c = linkTypes[2],
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Fixed',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp4);
			// CPE - router with brand and model
			Contracted_Products__c cp5 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[7].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Link_type__c = linkTypes[3],
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp5);

			for (Integer i = 0; i < linkTypes.size(); i++) {
				Contracted_Products__c cpi = new Contracted_Products__c(
					Quantity__c = quantity,
					UnitPrice__c = 10,
					Arpu_Value__c = 10,
					Duration__c = 1,
					Product__c = products[i].Id,
					VF_Contract__c = c.Id,
					Order__c = theOrder.Id,
					Proposition_Component__c = 'Voice; Internet; IPVPN',
					link_type__c = linkTypes[i]
				);
				cpInsert.add(cpi);
			}
			insert cpInsert;

			TestUtils.autoCommit = true;
			PBX_Type__c pbx = new PBX_Type__c(
				Name = 'Onenet Virtual PBX',
				Vendor__c = 'Onenet',
				Product_Name__c = 'Virtual PBX'
			);
			insert pbx;
			Competitor_Asset__c ca = new Competitor_Asset__c();
			ca = PBXSelectionWizardController.createAsset(site, ca, pbx.id);
			Competitor_Asset__c ca2 = new Competitor_Asset__c();
			ca2 = PBXSelectionWizardController.createAsset(site, ca2, pbx.id);
			insert ca;
			ca2.Overflow_PBX__c = ca.Id;
			insert ca2;

			numPort = TestUtils.createNumberPorting(theOrder);
			Numberporting_row__c npr = TestUtils.createNumberPortingRow(numPort, cpInsert[5]);
			npr.Location__c = site.Id;
			npr.PBX__c = ca2.Id;
			update npr;
			TestUtils.createPhonebookRegistration(numPort, 2);

			System.debug('Limit Queries insert contracted_Products__c --- ' + Limits.getQueries());
		}
	}

	public static Ban__c createBan(Account a) {
		System.debug('BanAccount  ' + a);
		Ban__c b = new Ban__c(
			Account__c = a.Id,
			Unify_Customer_Type__c = 'C',
			Unify_Customer_SubType__c = 'A',
			Name = '388888888',
			Dealer_code__c = '123456'
		);
		insert b;
		return b;
	}
}