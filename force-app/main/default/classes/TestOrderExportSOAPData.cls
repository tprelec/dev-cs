/**
 * @description		This is the test class for ECSSOAPOrder class. Because of a bug in mockservices, we have to cover this specifically
 * @author        	Guy Clairbois
 */
@isTest
private class TestOrderExportSOAPData {

    static testMethod void dataTest() {
  	
		ECSSOAPOrder.additionalArticleListType aalt = new ECSSOAPOrder.additionalArticleListType();
		ECSSOAPOrder.additionalArticleType aat = new ECSSOAPOrder.additionalArticleType();
		ECSSOAPOrder.additionalInfoType  ait = new ECSSOAPOrder.additionalInfoType();
		ECSSOAPOrder.articleType aty = new ECSSOAPOrder.articleType();
		ECSSOAPOrder.authenticationHeader_element ahe = new ECSSOAPOrder.authenticationHeader_element();
		ECSSOAPOrder.pbxInterfaceListType pilt = new ECSSOAPOrder.pbxInterfaceListType();
		ECSSOAPOrder.pbxInterfaceRefType pirt = new ECSSOAPOrder.pbxInterfaceRefType();
		ECSSOAPOrder.pbxListType plt = new ECSSOAPOrder.pbxListType();
		ECSSOAPOrder.cancelOrderResponseType cort = new ECSSOAPOrder.cancelOrderResponseType();
		ECSSOAPOrder.cancelOrdersRequest_element core = new ECSSOAPOrder.cancelOrdersRequest_element();
		ECSSOAPOrder.cancelOrdersResponse_element corse = new ECSSOAPOrder.cancelOrdersResponse_element();
		ECSSOAPOrder.cancelOrderType cot = new ECSSOAPOrder.cancelOrderType();
		ECSSOAPOrder.channelRestrictionsType crest = new ECSSOAPOrder.channelRestrictionsType();
		ECSSOAPOrder.clockSourceType cst = new ECSSOAPOrder.clockSourceType();
		ECSSOAPOrder.companyRefType crt = new ECSSOAPOrder.companyRefType();
		ECSSOAPOrder.contractType contrt = new ECSSOAPOrder.contractType();
		ECSSOAPOrder.cpeLinkListType cllt = new ECSSOAPOrder.cpeLinkListType();
		ECSSOAPOrder.cpeListType clt = new ECSSOAPOrder.cpeListType();
		ECSSOAPOrder.cpeType cpet = new ECSSOAPOrder.cpeType();
		ECSSOAPOrder.createOrderErsType coet = new ECSSOAPOrder.createOrderErsType ();
		ECSSOAPOrder.createOrderInternetProductType coipt = new ECSSOAPOrder.createOrderInternetProductType();
		ECSSOAPOrder.createOrderIpvpnProductType  coipvt = new ECSSOAPOrder.createOrderIpvpnProductType();
		ECSSOAPOrder.createOrderOneNetProductType coonpt = new ECSSOAPOrder.createOrderOneNetProductType ();
		ECSSOAPOrder.createOrderOneNet12ProductType coo12pt = new ECSSOAPOrder.createOrderOneNet12ProductType();
		ECSSOAPOrder.createOrderNumberportingProductType conpt = new ECSSOAPOrder.createOrderNumberportingProductType();
		ECSSOAPOrder.createOrderPhonebookRegistrationProductType coprpt = new ECSSOAPOrder.createOrderPhonebookRegistrationProductType();
		ECSSOAPOrder.createOrderProductsType copt = new ECSSOAPOrder.createOrderProductsType();
		ECSSOAPOrder.createOrderResponseType crort = new ECSSOAPOrder.createOrderResponseType();
		ECSSOAPOrder.createOrdersRequest_element crore = new ECSSOAPOrder.createOrdersRequest_element();
		ECSSOAPOrder.createOrdersResponse_element crorse = new ECSSOAPOrder.createOrdersResponse_element();
		ECSSOAPOrder.createOrderType crot = new ECSSOAPOrder.createOrderType();
		ECSSOAPOrder.createOrderVovProductType covpt = new ECSSOAPOrder.createOrderVovProductType();

		//ECSSOAPOrder.customerContactListType cclt = new ECSSOAPOrder.customerContactListType();
		//ECSSOAPOrder.customerContactType cct = new ECSSOAPOrder.customerContactType();
		ECSSOAPOrder.carrierType  cart = new ECSSOAPOrder.carrierType ();
		ECSSOAPOrder.contactDataType  cdt = new ECSSOAPOrder.contactDataType ();
		ECSSOAPOrder.cpeReferenceListType crlt = new ECSSOAPOrder.cpeReferenceListType ();
		ECSSOAPOrder.customerType ct = new ECSSOAPOrder.customerType();
		ECSSOAPOrder.escalationType  esct = new ECSSOAPOrder.escalationType ();
		ECSSOAPOrder.errorType et = new ECSSOAPOrder.errorType();
		ECSSOAPOrder.linkListType llt = new ECSSOAPOrder.linkListType();
		ECSSOAPOrder.linkType lt = new ECSSOAPOrder.linkType();
		//ECSSOAPOrder.locationContactListType lclt = new ECSSOAPOrder.locationContactListType();
		//ECSSOAPOrder.locationContactType lct = new ECSSOAPOrder.locationContactType();
		ECSSOAPOrder.legacyCancellationDetailsType lcdt = new ECSSOAPOrder.legacyCancellationDetailsType();
		ECSSOAPOrder.legacyCancelledItemListType lcilt = new ECSSOAPOrder.legacyCancelledItemListType ();
		ECSSOAPOrder.legacyOrdersRequest_element  loe = new ECSSOAPOrder.legacyOrdersRequest_element();
		ECSSOAPOrder.legacyOrderRequestType  loreqt = new ECSSOAPOrder.legacyOrderRequestType();
		 
		ECSSOAPOrder.legacyOrdersResponse_element  lore = new ECSSOAPOrder.legacyOrdersResponse_element();
		ECSSOAPOrder.legacyOrderResponseType lort = new ECSSOAPOrder.legacyOrderResponseType();
		ECSSOAPOrder.legacyOrderRowType  lorowt = new ECSSOAPOrder.legacyOrderRowType();
		ECSSOAPOrder.legacyOrderRowListType  lorowlt = new ECSSOAPOrder.legacyOrderRowListType();
		ECSSOAPOrder.legacyServicesListType  lslt = new ECSSOAPOrder.legacyServicesListType ();
		ECSSOAPOrder.locationListType lolt = new ECSSOAPOrder.locationListType();
		ECSSOAPOrder.locationRefType lrt = new ECSSOAPOrder.locationRefType();
		ECSSOAPOrder.locationType lot = new ECSSOAPOrder.locationType();
		ECSSOAPOrder.maintenanceType mt = new ECSSOAPOrder.maintenanceType();
		ECSSOAPOrder.numberportingListType nlt = new ECSSOAPOrder.numberportingListType();
		ECSSOAPOrder.numberportingType nt = new ECSSOAPOrder.numberportingType();
		ECSSOAPOrder.oneVoiceDataType ovdt = new ECSSOAPOrder.oneVoiceDataType ();
		ECSSOAPOrder.orderContactListType oclt = new ECSSOAPOrder.orderContactListType();
		ECSSOAPOrder.orderContactType oct = new ECSSOAPOrder.orderContactType();
		ECSSOAPOrder.OrderSOAP os = new ECSSOAPOrder.OrderSOAP();
		ECSSOAPOrder.pbxType pt = new ECSSOAPOrder.pbxType();
		ECSSOAPOrder.phoneType pht = new ECSSOAPOrder.phoneType();
		ECSSOAPOrder.phoneListType phlt = new ECSSOAPOrder.phoneListType();
		ECSSOAPOrder.phonebookRegistrationListType prlt = new ECSSOAPOrder.phonebookRegistrationListType();
		ECSSOAPOrder.phonebookRegistrationType prt = new ECSSOAPOrder.phonebookRegistrationType();
		ECSSOAPOrder.redundancyType  redt = new ECSSOAPOrder.redundancyType ();
		ECSSOAPOrder.serviceListType  slt = new ECSSOAPOrder.serviceListType();
		ECSSOAPOrder.sipOptionsType sot = new ECSSOAPOrder.sipOptionsType();
		ECSSOAPOrder.userRefType urt = new ECSSOAPOrder.userRefType();
 	
    	ECSSOAPOrder so = new ECSSOAPOrder();
		//ECSSOAPOrder.createOrderResponseType[] crortl = os.createOrders(null);
				 	
      	System.assert(true, 'dummy assertion');
 
    }
    
}