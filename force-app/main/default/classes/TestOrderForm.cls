/**
 * @description		This is the test class for ECSSOAPOrder class. Because of a bug in mockservices, we have to cover this specifically
 * @author        	Guy Clairbois
 */
@isTest
private class TestOrderForm {

    static testMethod void TestEmptyOrder(){
/*		PageReference orderForm = page.OrderFormPreload;
    	Test.setCurrentPage(orderForm);
    	OrderFormController cont = new OrderFormController();    	
    }

	@isTest(SeeAllData=false)
    static void TestVovPromoOrder(){
        TestUtils.createCompleteContract();
		List<VF_Contract__c> contracts = new List<VF_Contract__c>([
        	SELECT Id, Name, Account__c, Contract_Status__c FROM VF_Contract__c WHERE Opportunity__r.Name = 'Test Opportunity'
        ]);
        system.debug(contracts);
    	Contract_Attachment__c ca = new Contract_Attachment__c(Contract_VF__c=contracts[0].Id,Attachment_Type__c='Contract (Signed)');
    	insert ca;
    	
    	TestUtils.createContact(TestUtils.theAccount);
    	
    	VF_Contract__c co = contracts[0];
    	co.Contract_Activation_Date__c =  system.today();
    	co.Contract_Activation_Date_Actual_del__c =  system.today();
    	co.Contract_Duration__c = 36;
    	co.Signed_by_Customer__c = TestUtils.theContact.Id;
    	co.Contract_Sign_Date__c = system.today();
    	co.Type_of_Service__c = 'Fixed';
    	update co;
    	
    	List<Contracted_Products__c> cps = [Select Id, Name, Product__c, Product__r.Order_Form_Setup__c, Site_List__c,VF_Contract__c From Contracted_Products__c Where VF_Contract__c = :co.Id];
    	system.debug(cps);
    	
    	//Site__c s = TestUtils.createSite(TestUtils.theAccount); 
    	
    	//ContractedProduct2Site__c cp2s = new ContractedProduct2Site__c(Contracted_Product__c = cps[0].Id,Site__c = s.Id);
    	//insert cp2s;
    	  	
		Order_Form_Setup__c ofs  = new Order_Form_Setup__c(Name='testPriceplan',VFComponent1__c='OrderFormOrderData',VFComponent2__c='OrderFormNumberPorting',VFComponent3__c='OrderFormPhoneBookRegistration');
		insert ofs;
		
		// add the order form setup Id to the first product in the list of contractedProducts
		Product2 p = [Select Id, Order_Form_Setup__c From Product2 Where Id = :cps[0].Product__c];
		p.Order_Form_Setup__c = ofs.Id;
		update p;		
		
		system.debug([Select Id, Order_Form_Setup__c, Order_Form_Setup__r.Name From Product2 Where Order_Form_Setup__r.Name = 'testPriceplan']);
    	cps = [Select Id, Name, Product__c, Product__r.Order_Form_Setup__c, Site_List__c,VF_Contract__c From Contracted_Products__c Where VF_Contract__c = :co.Id];
    	system.debug(cps);

    	Test.startTest();
		
		PageReference orderFormPre = page.OrderFormPreload;
		orderFormPre.getParameters().put('contractId',co.Id);
    	Test.setCurrentPage(orderFormPre);
    	OrderFormController contPre = new OrderFormController();
    	
    	PageReference prepData = page.OrderForm;
    	prepData.getParameters().put('contractId',co.Id); 
    	contPre.prepareData();
    	OrderFormController.initialized = false;
    	
    	PageReference orderForm = page.OrderForm;
		orderForm.getParameters().put('contractId',co.Id);    	
    	Test.setCurrentPage(orderForm);
    	OrderFormController cont = new OrderFormController();
    	
    	// pick the first priceplan as 'currentorder', simulating the user that selected the priceplan in the order form
    	cont.siteSelectedString = 'testPriceplan';
    	//cont.currentOrder = priceplanToOrderMap.values()[0].order;
    	cont.getSiteSelectOptions();
    	cont.saveSite();
    	
		cont.saveOrder();
		cont.saveNumberPortings();
		cont.savePhonebookRegistrations();
		cont.submitOrder();
		cont.backTo();  
    	
    	
    }
    
    static testMethod void TestNumberportingOrder(){
		Account cust = TestUtils.createAccount(null);
		Numberporting__c np = new Numberporting__c(name = 'Dit is een test', customer__c = cust.Id);
		insert np;
		system.debug(np);
		Numberporting_row__c npr = new Numberporting_row__c(Numberporting__c=np.Id,Action__c='Porting_in',Block_type__c='single',Number_Block__c='0123123123',Leaving_Telco__c='abc',Name_on_bill__c='abc');
		insert npr;
		Numberporting_row__c npr2 = new Numberporting_row__c(Numberporting__c=np.Id,Action__c='Porting_in',Block_type__c='single',Number_Block__c='0123123124',Leaving_Telco__c='abc',Name_on_bill__c='abc');
		insert npr2;
		Phonebook_Registration__c pbr = new Phonebook_Registration__c(Numberporting__c=np.Id,Name__c='test',Address__c='test',Zipcode__c='1234TS',City__c='test',Phone__c='0321321321',Fax__c='0123123123'); 
		insert pbr;
		Phonebook_Registration__c pbr2 = new Phonebook_Registration__c(Numberporting__c=np.Id,Name__c='test2',Address__c='test',Zipcode__c='1234TS',City__c='test',Phone__c='0321321322',Fax__c='0123123122'); 
		insert pbr2;
		
		Order_Form_Setup__c ofs  = new Order_Form_Setup__c(Name='Numberporting',VFComponent1__c='OrderFormOrderData',VFComponent2__c='OrderFormNumberPorting',VFComponent3__c='OrderFormPhoneBookRegistration');
		insert ofs;
		
		PageReference orderFormPre = page.OrderFormPreload;
		orderFormPre.getParameters().put('numberportingId',np.Id);
    	Test.setCurrentPage(orderFormPre);
    	OrderFormController contPre = new OrderFormController();
    	
    	PageReference prepData = page.OrderForm;
    	prepData.getParameters().put('numberportingId',np.Id); 
    	contPre.prepareData();
    	OrderFormController.initialized = false;
    	
    	PageReference orderForm = page.OrderForm;
		orderForm.getParameters().put('numberportingId',np.Id);    	
    	Test.setCurrentPage(orderForm);
*/    	
    	
    	// REDO BELOW PART AFTER ORDERFORM CHANGES
    	/*OrderFormController cont = new OrderFormController();
    	
		cont.saveOrder();
		cont.saveNumberPortings();
		cont.savePhonebookRegistrations();
		cont.submitOrder();
		cont.backTo();  
		
		// testing the component rendering
		cont.getOrderFormComponents();
*/
    	
    }
}