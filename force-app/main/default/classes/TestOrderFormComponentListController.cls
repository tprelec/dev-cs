@isTest
public inherited sharing class TestOrderFormComponentListController {

    @TestSetup
    static void makeData(){
        TestUtils.createCompleteOpportunity();
        Opportunity opp = [SELECT Id, AccountId FROM Opportunity];
        Account acc = [SELECT Id FROM Account WHERE Id =: opp.AccountId];
        opp.Escalation__c = true;
        update opp;
        VF_Contract__c cont = TestUtils.createVFContract(acc, opp);

        TestUtils.createOrder(cont);
    }

    @isTest
    public static void testOrderWithFixedPropositions() {
        Order__c order = [SELECT Id, propositions__c FROM Order__c];
        order.propositions__c = 'One Fixed;One Net;IPVPN;Legacy;Internet;Numberporting';
        //order.Mobile_Fixed__c = 'Mobile';
        update order;

        Test.startTest();
            OrderFormComponentsListController controller = new OrderFormComponentsListController();
            controller.theCurrentOrder = order;
        Test.stopTest();

        System.assertEquals(controller.customerComponent.Name , 'Customer/Contract',
        'Verify that the controller loaded correctly');
    }

    @isTest
    public static void testOrderWithMobilePropositions() {
        Opportunity opp = [SELECT Id, Segment__c, SmallBusiness_Onderstroom__c FROM Opportunity];
        opp.Segment__c = 'SoHo';
        opp.SmallBusiness_Onderstroom__c = true;
        update opp;

        Order__c order = [SELECT Id, propositions__c FROM Order__c];
        order.propositions__c = 'One Mobile';
        update order;

        Test.startTest();
            OrderFormComponentsListController controller = new OrderFormComponentsListController();
            controller.theCurrentOrder = order;
        Test.stopTest();

        System.assertEquals(controller.customerComponent.Name , 'Customer/Contract',
        'Verify that the controller loaded correctly');
    }
}