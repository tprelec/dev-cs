@isTest
private class TestOrderFormTemplateController {
	@testSetup
	static void testSetup() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);

		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationSite();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createOrderValidations();
		TestUtils.createOrderValidationNumberportingRow();
		TestUtils.createOrderValidationPhonebookRegistration();
		TestUtils.createOrderValidationHBO();
		TestUtils.createOrderValidationCompetitorAsset();
		TestUtils.createDefaultOnenetPBXType();
		TestUtils.createCompleteContract();

		TestUtils.autoCommit = true;
		Order__c order = TestUtils.createOrder(TestUtils.theContract);
		order.Status__c = 'Clean';
		update order;
	}

	private static OrderFormTemplateController setCurrentPageAndGetController(
		Id vfContractId,
		Id orderId
	) {
		PageReference pageRef = Page.OrderFormTemplate;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('contractId', vfContractId);
		ApexPages.currentPage().getParameters().put('orderId', orderId);
		OrderFormTemplateController controller = new OrderFormTemplateController();
		controller.orderSelected = orderId;
		return controller;
	}

	@isTest
	static void controllerTest() {
		Test.startTest();
		VF_Contract__c vfContract = getContract();
		Order__c order = getOrder();

		OrderFormTemplateController cntrl = setCurrentPageAndGetController(vfContract.Id, null);
		cntrl.generateProvisioningXML();

		OrderFormTemplateController controller = setCurrentPageAndGetController(
			vfContract.Id,
			order.Id
		);

		controller.createNewOrder();
		for (OrderFormTemplateController.ContractedProductWrapper cpw : controller.cpWrappers) {
			cpw.selected = true;
		}

		controller.acceptOrder();
		controller.addProduct();
		controller.assignToInsideSales();
		controller.backTo();
		controller.cancel();
		controller.cancelCreateNewOrder();
		controller.changeOrderForm();
		controller.confirmAddToExistingOrder();
		TestUtils.createDefaultOnenetPBXType();
		controller.orderSelected = null;
		controller.confirmAddToOrder();
		controller.orderSelected = 'New';
		controller.confirmAddToOrder();
		controller.orderSelected = order.Id;
		controller.confirmAddToOrder();
		controller.confirmCreateNewOrder();
		controller.getAssignToLabel();
		controller.getAvailableOrders();
		controller.getShowAddProductButton();
		controller.getShowSubmitButton();
		controller.goToOrderProgress();
		controller.logCase();
		controller.submitOrder();
		Test.stopTest();
	}

	@isTest
	static void submitCleanOrder() {
		Test.startTest();
		VF_Contract__c vfContract = getContract();
		Order__c order = getOrder();
		OrderFormTemplateController controller = setCurrentPageAndGetController(
			vfContract.Id,
			order.Id
		);

		controller.createNewOrder();
		for (OrderFormTemplateController.ContractedProductWrapper cpw : controller.cpWrappers) {
			cpw.selected = true;
		}
		Test.setMock(
			HttpCalloutMock.class,
			new TestUtilMultiRequestMock.SingleRequestMock(200, 'Complete', 'Success message', null)
		);
		controller.submitOrder();
		System.assertEquals(1, controller.unorderedItems, 'There should be 1 unordered item');

		controller.generateProvisioningXML();
		Test.stopTest();
	}

	@isTest
	static void setOrderToProcessedManually() {
		TriggerHandler.preventRecursiveTrigger('OrderTriggerHandler', null, 0);
		VF_Contract__c vfContract = getContract();
		Order__c order = getOrder();
		OrderFormTemplateController controller = setCurrentPageAndGetController(
			vfContract.Id,
			order.Id
		);

		Test.startTest();
		controller.setOrderToProcessedManually();
		Test.stopTest();

		order = getOrder();

		System.assertEquals(
			Constants.ORDER_STATUS_PROCESSED_MANUALLY,
			order.Status__c,
			'Order should be processed manually'
		);
	}

	@isTest
	public static void pricePlanClassValidation() {
		Test.startTest();
		VF_Contract__c vfContract = getContract();
		Account acc = getAccount(vfContract.Account__c);
		Order__c order = getOrder();
		order.EMP_Automated_Mobile_order__c = true;
		update order;

		VF_Asset__c asset = TestUtils.createVFAsset(acc);
		asset.Priceplan_Class__c = 'Data Only';
		update asset;

		Contracted_Products__c cp = [
			SELECT Id, Deal_Type__c, Price_Plan_Class__c, Order__c
			FROM Contracted_Products__c
			WHERE VF_Contract__c = :vfContract.Id
		];
		cp.Deal_Type__c = 'Retention';
		cp.Order__c = order.Id;
		update cp;

		OrderFormTemplateController controller = setCurrentPageAndGetController(
			vfContract.Id,
			order.Id
		);

		controller.createNewOrder();
		for (OrderFormTemplateController.ContractedProductWrapper cpw : controller.cpWrappers) {
			cpw.selected = true;
		}
		Boolean isTrue = controller.pricePlanClassValidation(order);

		System.assertEquals(true, isTrue, 'Price Plan Class Validation should return true.');
	}

	@isTest
	public static void testProcedeWithManualProvisioning() {
		VF_Contract__c vfContract = getContract();
		Order__c order = getOrder();

		OrderFormTemplateController controller = setCurrentPageAndGetController(
			vfContract.Id,
			order.Id
		);

		Test.startTest();
		controller.createNewOrder();
		for (OrderFormTemplateController.ContractedProductWrapper cpw : controller.cpWrappers) {
			cpw.selected = true;
		}

		System.assertEquals(false, order.Remark__c, 'Remark on the order should be set to false.');
		System.assert(
			String.isBlank(order.Remark_Information__c),
			'There should be no remark information on the order.'
		);

		controller.procedeWithManualProvisioning();

		Order__c orderManual = [
			SELECT Id, Remark__c, Remark_Information__c, Remarks__c
			FROM Order__c
			WHERE Id = :order.Id
		];

		Test.stopTest();

		System.assertEquals(true, orderManual.Remark__c, 'Remark should be set to true.');
		System.assertEquals(
			System.Label.EMP_Proceed_With_Automated_Provisioning,
			orderManual.Remark_Information__c,
			'There should be a remark information on the order.'
		);
	}

	private static Order__c getOrder() {
		return [
			SELECT
				Id,
				Account__c,
				Status__c,
				Remark__c,
				Remark_Information__c,
				EMP_Automated_Mobile_order__c
			FROM Order__c
			LIMIT 1
		];
	}

	private static VF_Contract__c getContract() {
		return [SELECT Id, Account__c FROM VF_Contract__c LIMIT 1];
	}

	private static Account getAccount(Id accId) {
		return [SELECT Id FROM Account WHERE Id = :accId];
	}
}