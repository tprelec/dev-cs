/**
 * Test class for OrderPABXBatch.cls which is called from Order Trigger Handler
 *
 * @author: Rahul Sharma
 */
@IsTest
public class TestOrderPABXBatch {

    // verify the end to end positive testing from trigger -> batch -> web service
    private static testMethod void validatePositive_statusCode200() {

        // create an order with contracted product having a competiter asset
        Order__c order = prepareTestData();


        // change the status run once flag as trigger may already ran
        triggerHandler.resetAlreadyModified();
        TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

        External_WebService_Config__c config = setAuthenticationInSetting();

        List<Customer_Asset__c> customerAssets = [SELECT Id, Installed_Base_Id__c FROM Customer_Asset__c LIMIT 2];
        System.assertEquals(1, customerAssets.size());

        // verify no error on contracted product
        List<Contracted_Products__c> contractedProducts = [SELECT Id, PBX_Error_Info__c FROM Contracted_Products__c LIMIT 2];
        System.assertEquals(1, contractedProducts.size());
        System.assertEquals(null, contractedProducts[0].PBX_Error_Info__c);

        // verify response
        OrderPABXRest.OrderPABXSuccessResponse response = getResponse(order.Id, customerAssets[0].Installed_Base_Id__c);
        System.assertNotEquals(null, response);
        System.assertNotEquals(null, response.orderPABXResponse);

        // set mock
        TestUtilMultiRequestMock.setSingleMock(config.URL__c, 200, JSON.serialize(response));

        // start testing
        Test.startTest();


        // change the status run once flag as trigger may already ran
        triggerHandler.resetAlreadyModified();

        // change the status to 'Project Created', this would then call the batch's execute method in future context and do the processing of records
        changeBopOrderStatus(order.Id, Constants.ORDER_BOP_ORDER_STATUS_PROJECT_CREATED);

        Test.stopTest();

        // verify if the contracted products are updated
        List<Competitor_Asset__c> competitorAssets = [SELECT Id, Assigned_Product_Id__c, PABX_Id__c FROM Competitor_Asset__c LIMIT 2];
        // competiter assets must be updated
        System.assertEquals(response.orderPABXResponse.APID, competitorAssets[0].Assigned_Product_Id__c);
        System.assertEquals(response.orderPABXResponse.PABXID, competitorAssets[0].PABX_Id__c);

        // verify no error on contracted product
        contractedProducts = [SELECT Id, PBX_Error_Info__c FROM Contracted_Products__c LIMIT 2];
        System.assertEquals(1, contractedProducts.size());
        System.assertEquals(null, contractedProducts[0].PBX_Error_Info__c);

    }

    // verify the end to end positive testing from trigger -> batch -> web service
    private static testMethod void validateNegative_statusCode400() {

        // create an order with contracted product having a competiter asset
        Order__c order = prepareTestData();

        // change the status run once flag as trigger may already ran
        triggerHandler.resetAlreadyModified();
        TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

        External_WebService_Config__c config = setAuthenticationInSetting();

        List<Customer_Asset__c> customerAssets = [SELECT Id, Installed_Base_Id__c FROM Customer_Asset__c LIMIT 2];
        System.assertEquals(1, customerAssets.size());

        List<Contracted_Products__c> contractedProducts = [SELECT Id, PBX_Error_Info__c FROM Contracted_Products__c LIMIT 2];
        System.assertEquals(1, contractedProducts.size());
        System.assertEquals(null, contractedProducts[0].PBX_Error_Info__c);

        String errorResponse = '"Actual error message"';
        // set mock
        TestUtilMultiRequestMock.setSingleMock(config.URL__c, 400, errorResponse);

        // start testing
        Test.startTest();

        // change the status run once flag as trigger may already ran
        triggerHandler.resetAlreadyModified();

        // change the status to 'Project Created', this would then call the batch's execute method in future context and do the processing of records
        changeBopOrderStatus(order.Id, Constants.ORDER_BOP_ORDER_STATUS_PROJECT_CREATED);

        Test.stopTest();

        List<Competitor_Asset__c> competitorAssets = [SELECT Id, Assigned_Product_Id__c, PABX_Id__c FROM Competitor_Asset__c LIMIT 2];
        // competiter assets must not be updated
        System.assertEquals(null, competitorAssets[0].Assigned_Product_Id__c);
        System.assertEquals(null, competitorAssets[0].PABX_Id__c);


        // there must be an error on contracted product
        contractedProducts = [SELECT Id, PBX_Error_Info__c FROM Contracted_Products__c LIMIT 2];
        System.assertEquals(1, contractedProducts.size());
        System.assertNotEquals(null, contractedProducts[0].PBX_Error_Info__c);

    }

    // cover start and finish method
    private static testMethod void coverBatchStartFinish() {
        OrderPABXBatch orderPABXBatchInstance = new OrderPABXBatch(new Set<Id>());
        Database.QueryLocator queryLocator = orderPABXBatchInstance.start(null);
        orderPABXBatchInstance.finish(null);
    }

    // create custom setting with integration data
    private static External_WebService_Config__c setAuthenticationInSetting() {
        // create custom setting with integration data
        External_WebService_Config__c config = new External_WebService_Config__c(
            Name = OrderPABXRest.INTEGRATION_SETTING_NAME,
            URL__c = OrderPABXRest.Mock_Url,
            Username__c = 'username',
            Password__c = 'password'
        );
        insert config;
        System.assertNotEquals(null, config.Id);
        return config;
    }

    private static Order__c prepareTestData() {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Site__c site = TestUtils.createSite(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
        OrderType__c ot = TestUtils.createOrderType();
        System.assertNotEquals(null, ot.Id);

        TestUtils.autoCommit = false;

        VF_Family_Tag__c familyTag = new VF_Family_Tag__c(
            Name = Constants.PRODUCT2_FAMILYTAG_PBX_Trunking,
            ExternalID__c = 'FT-06-ABCDEFG'
        );
        insert familyTag;
        System.assertNotEquals(null, familyTag.Id);

        Product2 product = TestUtils.createProduct(new Map<String, Object> {
            'Billing_Type__c' => Constants.PRODUCT2_BILLINGTYPE_STANDARD,
            'Family_Tag__c' => familyTag.Id,
            'ProductCode' => 'C106929'
        });
        insert product;
        System.assertNotEquals(null, product.Id);

        // create an order with new status
        Order__c order = new Order__c(
            Status__c = 'New',
            Propositions__c = 'Legacy',
            OrderType__c = ot.Id,
            VF_Contract__c = contr.Id,
            Number_of_items__c = 100,
            O2C_Order__c = true
        );
        insert order;
        System.assertNotEquals(null, order.Id);

        Competitor_Asset__c compAsset = new Competitor_Asset__c();
        insert compAsset;
        System.assertNotEquals(null, compAsset.Id);

        Contracted_Products__c contractedProduct = new Contracted_Products__c(
            Quantity__c = 1,
            Arpu_Value__c = 10,
            Duration__c = 1,
            Product__c = product.Id,
            VF_Contract__c = contr.Id,
            Site__c = site.Id,
            Order__c = order.Id,
            CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ,
            PBX__c = compAsset.Id,
            ProductCode__c = 'C106929'
        );
        insert contractedProduct;
        System.assertNotEquals(null, contractedProduct.Id);

        return order;
    }

    // generate positive response
    public static OrderPABXRest.OrderPABXSuccessResponse getResponse(String SFOrderID, String SFIBID) {
        return new OrderPABXRest.OrderPABXSuccessResponse(
            new OrderPABXRest.OrderPABXRespData(
                '123',
                SFOrderID,
                '456',
                SFIBID));
    }

    // verify BOP order status change
    private static void changeBopOrderStatus(Id orderId, String status) {
        Order__c order = new Order__c(
            Id = orderId,
            BOP_Order_Status__c = status
        );
        update order;

        // validate BOP order status update
        order = [SELECT Id, BOP_Order_Status__c FROM Order__c WHERE Id = :order.Id];
        System.assertEquals(Constants.ORDER_BOP_ORDER_STATUS_PROJECT_CREATED, order.BOP_Order_Status__c);
    }
}