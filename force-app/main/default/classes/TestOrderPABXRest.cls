/**
 * Test class for OrderPABXRest.cls
 * 
 * @author: Jurgen van Westreenen
 */
@IsTest
public class TestOrderPABXRest {
    private static String SFOrderID = '';
    private static String SFIBID = '';

    @IsTest 
    private static void validateSuccessResponse() {
        String cpId = prepareTestData();
        
        // create custom setting with integration data
        External_WebService_Config__c config = new External_WebService_Config__c(
            Name = OrderPABXRest.INTEGRATION_SETTING_NAME,
            URL__c = OrderPABXRest.Mock_Url, 
            Username__c = 'username',
            Password__c = 'password'
        );
        insert config;

        setPositiveMock(config.URL__c);
        
        Test.startTest();
        OrderPABXRest.createOrderPABX(cpId);
        Test.stopTest();
    }

    @IsTest 
    private static void validateErrorResponse() {
        String cpId = prepareTestData();
        
        // create custom setting with integration data
        External_WebService_Config__c config = new External_WebService_Config__c(
            Name = OrderPABXRest.INTEGRATION_SETTING_NAME,
            URL__c = OrderPABXRest.Mock_Url, 
            Username__c = 'username',
            Password__c = 'password'
        );
        insert config;

        setNegativeMock(config.URL__c);
        
        Test.startTest();
        OrderPABXRest.createOrderPABX(cpId);
        Test.stopTest();
    }

    static void setPositiveMock(String url) {
        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
        endpoint2TestResp.put(url, new TestUtilMultiRequestMock.SingleRequestMock(200,
                                        'Complete',
                                        '{"orderPABXResponse": {"APID": "11246401007","SFOrderID": "' + SFOrderID + '","PABXID": "0040196","SFIBID": "' + SFIBID + '"}}',
                                        null));
        HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);

        Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);
    }
    
    static void setNegativeMock(String url) {
        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
        endpoint2TestResp.put(url, new TestUtilMultiRequestMock.SingleRequestMock(204,
                                        'Complete',
                                        '{"errorInfo": {"errorCode": "SIASERR-1500", "errorDescription": " "Error received from OMS CreateAndSubmit WS", "targetSystemName": "OMS", "targetServiceName": "CreateAndSubmit"}}',
                                        null));
        HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);

        Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);
    }

    private static String prepareTestData() {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Site__c site = TestUtils.createSite(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
        OrderType__c ot = TestUtils.createOrderType();        
        Contact c = new Contact(AccountId = acct.Id, LastName = 'Test');
        Financial_Account__c fa = TestUtils.createFinancialAccount(ban, c.Id);
        Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
        
        TestUtils.autoCommit = false;
        Product2 product = TestUtils.createProduct();
        insert product;

        Opportunity newOpp = [SELECT Id, Ban__c FROM Opportunity LIMIT 1];
        newOpp.Ban__c = ban.Id;
        update newOpp;
        
        Order__c ord = new Order__c();
        ord.Status__c = 'New';
        ord.Propositions__c = 'Legacy';
        ord.OrderType__c = ot.Id;
        ord.Number_of_items__c = 100;
        ord.BOP_Order_Status__c = 'In Progress';
        ord.PM_Email__c = 'test@test.com';
        ord.PM_First_Name__c = 'Test';
        ord.PM_Last_Name__c = 'von Test';
        ord.PM_Phone__c = '0031612345678';
        ord.VF_Contract__c = contr.Id;
        insert ord;

        Customer_Asset__c ca = new Customer_Asset__c();
        ca.Billing_Arrangement__c = ba.Id;
        ca.Order__c = ord.Id;
        ca.Site__c = site.Id;
        insert ca;

        Competitor_Asset__c compAsset = new Competitor_Asset__c();
        insert compAsset;

        Contracted_Products__c cp = new Contracted_Products__c();
        cp.Order__c = ord.Id;
        cp.External_Reference_Id__c = '7777777';
        cp.CLC__c = 'Acq'; // Required and fixed for BOP
        cp.VF_Contract__c = contr.Id;
        cp.Customer_Asset__c = ca.Id;
        cp.PBX__c = compAsset.Id;
        cp.Product__c = product.Id;
        insert cp;

        SFOrderID = ord.Id;
        Customer_Asset__c newCa = [SELECT Id, Installed_Base_Id__c FROM Customer_Asset__c LIMIT 1];
        SFIBID = newCa.Installed_Base_Id__c;

        return cp.Id;
    }
}