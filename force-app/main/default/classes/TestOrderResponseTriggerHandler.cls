/**
 * @description    This class contains unit tests for the OrderResponseTriggerHandler class
 * @Author         Guy Clairbois
 */
@isTest
private class TestOrderResponseTriggerHandler {

    @isTest
    static void testCreateOrder(){
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

        TestUtils.createOrderValidationOrder();
        TestUtils.createOrderValidationContractedProducts();

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact cont = TestUtils.createContact(acct);
        Site__c site = TestUtils.createSite(acct);
        Ban__c ban = TestUtils.createBan(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
        opp.Ban__c = ban.Id;
        update opp;
        Order__c ord = TestUtils.createOrder(contr);
    
        Test.startTest();
        
        // contact
        Order_Response__c or1 = new Order_Response__c();
        or1.Order__c = ord.Id;
        or1.Type__c = 'UnifyContact';
        or1.Unify_Error_Number__c = 'SIAS000';
        or1.SFDC_Contact_Id__c = cont.Id;
        or1.Unify_Contact_Id__c = '123';
        insert or1;

        // site
        Order_Response__c or2 = new Order_Response__c();
        or2.Order__c = ord.Id;
        or2.Type__c = 'UnifySite';
        or2.Unify_Error_Number__c = 'SIAS000';
        or2.SFDC_Site_Id__c = site.Id;
        or2.Unify_Site_Id__c = '123';
        insert or2;

        // customer
        Order_Response__c or3 = new Order_Response__c();
        or3.Order__c = ord.Id;
        or3.Type__c = 'UnifyAccount';
        or3.Unify_Error_Number__c = 'SIAS000';
        or3.Unify_Account_Id__c = '456';
        or3.Unify_Billing_Customer_Id__c = '135';
        or3.BAN_Corporate_Id__c = '246';
        or3.Unify_Financial_Account_Id__c = '357';
        or3.Unify_Billing_Arrangement_Id__c = '468';
        insert or3;

        // bop customer
        Order_Response__c or4 = new Order_Response__c();
        or4.Order__c = ord.Id;
        or4.Type__c = 'BOPCompany';
        or4.Unify_Error_Number__c = 'SIAS000';
        insert or4;

        // start order
        Order_Response__c or5 = new Order_Response__c();
        or5.Order__c = ord.Id;
        or5.Type__c = 'StartOrder';
        or5.Unify_Error_Number__c = 'SIAS000';
        insert or5;

        // contact - negative
        Order_Response__c or6 = new Order_Response__c();
        or6.Order__c = ord.Id;
        or6.Type__c = 'UnifyContact';
        or6.Unify_Error_Number__c = 'SIAS001';
        or6.SFDC_Contact_Id__c = cont.Id;
        or6.Unify_Contact_Id__c = '123';
        insert or6;

        // site - negative
        Order_Response__c or7 = new Order_Response__c();
        or7.Order__c = ord.Id;
        or7.Type__c = 'UnifySite';
        or7.Unify_Error_Number__c = 'SIAS001';
        or7.SFDC_Site_Id__c = site.Id;
        or7.Unify_Site_Id__c = '123';
        insert or7;

        // customer - negative
        Order_Response__c or8 = new Order_Response__c();
        or8.Order__c = ord.Id;
        or8.Type__c = 'UnifyAccount';
        or8.Unify_Error_Number__c = 'SIAS001';
        insert or8;

        // bop customer - negative
        Order_Response__c or9 = new Order_Response__c();
        or9.Order__c = ord.Id;
        or9.Type__c = 'BOPCompany';
        or9.Unify_Error_Number__c = 'SIAS001';
        insert or9;

        // start order - negative
        Order_Response__c or10 = new Order_Response__c();
        or10.Order__c = ord.Id;
        or10.Type__c = 'StartOrder';
        or10.Unify_Error_Number__c = 'SIAS001';
        ord.Status__c = 'Accepted';
        update ord;
        insert or10;

        Test.stopTest();
        
        List<Order_Response__c> orderRespProcessed = [SELECT Id FROM Order_Response__c WHERE Processing_result__c = 'Processed'];
        List<Order_Response__c> orderRespNotProcessed = [SELECT Id FROM Order_Response__c WHERE Processing_result__c != 'Processed'];
        System.assertEquals(5, orderRespProcessed.size());
        System.assertEquals(5, orderRespNotProcessed.size());
    }    

    @isTest
    static void testCreateOrder2(){
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

        TestUtils.createOrderValidationOrder();
        TestUtils.createOrderValidationContractedProducts();

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact cont = TestUtils.createContact(acct);
        Site__c site = TestUtils.createSite(acct);
        Ban__c ban = TestUtils.createBan(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
        opp.Ban__c = ban.Id;
        update opp;
        Order__c ord = TestUtils.createOrder(contr);
        
        Test.startTest();
        
        Order_Response__c or11 = new Order_Response__c();
        or11.Order__c = ord.Id;
        or11.Type__c = 'BopOrderCreation';
        or11.Unify_Error_Number__c = '0';
        insert or11;
        
        Order_Response__c or12 = new Order_Response__c();
        or12.Order__c = ord.Id;
        or12.Type__c = 'BopOrderCreation';
        or12.Unify_Error_Number__c = 'SIAS102';
        or12.Unify_Error_Description__c = 'WRONG!';
        insert or12;
        
        Order_Response__c or13 = new Order_Response__c();
        or13.Order__c = ord.Id;
        or13.Type__c = 'BopOrderCreation';
        or13.Unify_Error_Number__c = 'SIAS001';
        insert or13;
        
        Order_Response__c or14 = new Order_Response__c();
        or14.Order__c = ord.Id;
        or14.Type__c = 'UnifyOrderCreation';
        or14.Unify_Error_Number__c = 'SIAS000';
        insert or14;
         
        Order_Response__c or15 = new Order_Response__c();
        or15.Order__c = ord.Id;
        or15.Type__c = 'UnifyOrderCreation';
        or15.Unify_Error_Number__c = 'SIAS001';
        insert or15;
        
        Order_Response__c or16 = new Order_Response__c();
        or16.Order__c = ord.Id;
        or16.Type__c = 'SendMappingObject';
        or16.Unify_Error_Number__c = 'SIAS000';
        insert or16;
        
        Order_Response__c or17 = new Order_Response__c();
        or17.Order__c = ord.Id;
        or17.Type__c = 'SendMappingObject';
        or17.Unify_Error_Number__c = 'SIAS001';
        insert or17;

        Test.stopTest();
        
        List<Order_Response__c> orderRespProcessed = [SELECT Id FROM Order_Response__c WHERE Processing_result__c = 'Processed'];
        List<Order_Response__c> orderRespNotProcessed = [SELECT Id FROM Order_Response__c WHERE Processing_result__c != 'Processed'];
        System.assertEquals(0, orderRespProcessed.size());
        System.assertEquals(7, orderRespNotProcessed.size());
    }
}