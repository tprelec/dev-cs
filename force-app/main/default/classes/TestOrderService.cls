@isTest
public with sharing class TestOrderService {

    @isTest
    public static void testGetOpportunityNumberWithOrderId() {
        TestUtils.autoCommit = true;
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

        opp.Ban__c = ban.Id;
        update opp;
        Order__c ord = TestUtils.createOrder(contr);

        Test.startTest();
            String result = OrderService.getOpportunityNumberWithOrderId([SELECT Id, Name FROM Order__c WHERE Id =: ord.Id].Name);
        Test.stopTest();

        System.assertEquals([SELECT Id, Opportunity_Number__c FROM Opportunity WHERE id =: opp.Id].Opportunity_Number__c,
                            result,
                            'The method should return related Opportunities Opportunity_Number__c');
    }
}