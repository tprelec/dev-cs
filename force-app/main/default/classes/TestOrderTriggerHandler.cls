/**
 *     @description    This class contains unit tests for the OrderTriggerHandler class
 *    @Author            Guy Clairbois
 */
@isTest
private class TestOrderTriggerHandler {
	@isTest
	static void testCreateOrder() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		Test.startTest();
		opp.Ban__c = ban.Id;
		update opp;

		Order__c ord = TestUtils.createOrder(contr);
		ord.Propositions__c = 'Legacy';
		ord.BEN_Number__c = '1234';
		ord.Contract_Ready__c = true;
		update ord;
		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}

	@isTest
	static void testUpdateOrder1() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Contact con = TestUtils.createContact(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		opp.Ban__c = ban.Id;
		update opp;
		Order__c ord = TestUtils.createOrder(contr);

		Test.startTest();
		Test.setMock(
			HttpCalloutMock.class,
			new TestUtilMultiRequestMock.SingleRequestMock(200, 'Complete', 'Error message', null)
		);
		ord.Propositions__c = 'Legacy';
		ord.Customer_Main_Contact__c = con.Id;
		ord.Status__c = 'Submitted';
		update ord;
		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}

	@isTest
	static void testUpdateOrder2() {
		Test.startTest();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.createOrderValidationOrder();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);

		Contact con = TestUtils.createContact(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		opp.Ban__c = ban.Id;
		update opp;

		Order__c ord = TestUtils.createOrder(contr);
		ord.Export__c = 'BOP';
		ord.Propositions__c = 'Retention';
		ord.Customer_Main_Contact__c = con.Id;
		ord.Status__c = 'Submitted';
		ord.BOP_Order_Status__c = 'Project Created';
		ord.Unify_Customer_Export_Datetime__c = system.now();
		update ord;

		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}

	@isTest
	static void testUpdateOrder3() {
		Test.startTest();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.createOrderValidationOrder();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);

		Contact con = TestUtils.createContact(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		opp.Ban__c = ban.Id;
		update opp;

		CSPOFA__Orchestration_Process_Template__c orchTemp = new CSPOFA__Orchestration_Process_Template__c(
			Name = 'EMP Order Provisioning',
			CSPOFA__Unique_Name__c = 'EMP Order Provisioning'
		);
		insert orchTemp;

		Order__c ord = TestUtils.createOrder(contr);
		ord.Ordertype__c = null;
		ord.Propositions__c = 'IPVPN';
		ord.Export__c = 'SIAS';
		ord.Customer_Main_Contact__c = con.Id;
		ord.Status__c = 'Submitted';
		ord.BOP_Order_Status__c = 'Project Created';
		ord.Unify_Customer_Export_Datetime__c = system.now();
		ord.EMP_Automated_Mobile_order__c = true;
		update ord;

		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}

	@isTest
	static void testDeleteOrder() {
		Test.startTest();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);

		Contact con = TestUtils.createContact(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		opp.Ban__c = ban.Id;
		update opp;

		Order__c ord = TestUtils.createOrder(contr);
		delete ord;

		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}
}