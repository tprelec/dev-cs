@isTest
private class TestOrderValidation {

    
    @isTest
    static void testOpportunityValidation() {

        TestUtils.createOrderValidationOpportunity();
        TestUtils.createCompleteOpportunity();

        Test.startTest();
        Set<Id> oppids = new Set<Id>{TestUtils.theOpportunity.Id};
        String result = OrderValidation.checkCompleteOpportunitiesExt(null, oppids, true, null);
        Map<String,Map<String,Set<String>>> tstMap = OrderValidation.objectIdToFieldNameToChildrenTypes;
        OrderValidation.childrenTypesFound(TestUtils.theAccount.Id, 'contact_roles__r');
        OpportunityLineItem opli = [SELECT Id FROM OpportunityLineItem LIMIT 1];
        opli.family_condition__c = 'asdf';
        opli.proposition__c = 'Fixed';
        update opli;
        
        Opportunity_Attachment__c oa = new Opportunity_Attachment__c(Opportunity__c = TestUtils.TheOpportunity.Id, Attachment_Type__c = 'Other');
        insert oa;
        OrderValidation.attachmentTypesFound(TestUtils.theOpportunity.Id, 'opportunity_attachments__r');
        OrderValidation.attachmentTypesFound(TestUtils.theOpportunity.Id, 'opportunitylineItems');
        Test.stopTest();
        System.assertNotEquals(opli, null);
    }    
    
    @isTest
    static void testOrderValid() {
        // create the query fields
        EMP_Automatic_Order_Flow__c empSettings = TestUtils.createEMPCustomSettings();
        NetProfit_Settings__c settings = TestUtils.createNetProfitCustomSettings();
        TestUtils.createOrderValidationNetProfitInformation();
        TestUtils.createOrderValidationOrder();
        TestUtils.createOrderValidationContractedProducts();
        TestUtils.createVFContractValidationOpportunity();
        TestUtils.createOrderValidationBilling();
        TestUtils.createOrderValidationOpportunity();
        TestUtils.createOrderValidationSite();
        Testutils.createOrderValidationNumberportingRow();
        TestUtils.createOrderValidationCompetitorAsset();
        TestUtils.createOrderValidationPhonebookRegistration();
        // create the order validations
        TestUtils.createOrderValidations();
        Test.startTest();
        TestUtils.createCompleteContract();
        Test.stopTest();       

        // generate the order via the orderform
        PageReference pageRef = Page.OrderFormPreload;
        pageRef.getParameters().put('contractId', TestUtils.TheContract.Id);
        Test.setCurrentPage(pageRef);
        OrderFormController controller = new OrderFormController(null);
        controller.prepareData();   
        List<Order__c> orders = [Select Id, DeliveryReady__c, CustomerReady__c, BillingReady__c from Order__c];
        Id theOrderId = orders[0].Id;
        
        VF_Family_Tag__c familyTag = new VF_Family_Tag__c(Name = Constants.PRODUCT2_FAMILYTAG_PBX_Trunking, ExternalID__c = 'FT-00012345');
        insert familyTag;
        // add Numberporting, Numberporting_row and PhonebookRegistration
        Product2 p = [SELECT Id FROM Product2 LIMIT 1];
        p.Role__c = 'Numberporting';
        p.Mandatory_Attachment_Type__c = 'Contract Office 365;Globe Information';
        p.Family_Tag__r = familyTag;
        update p;
        PBX_Type__c pbx = new PBX_Type__c(Name = 'Onenet Virtual PBX', Vendor__c = 'Onenet', Product_Name__c = 'Virtual PBX');
        insert pbx;
        Contracted_Products__c cp = new Contracted_Products__c (
            Quantity__c = 1,
            UnitPrice__c = 10,
            Arpu_Value__c = 10,
            Duration__c = 1,
            Product__c = p.Id,
            VF_Contract__c = TestUtils.theContract.Id,
            Order__c = theOrderId,
            Site__c = TestUtils.theSite.Id,
            Family_Condition__c = 'asdf',
            Proposition__c = 'Fixed'
        );
        insert cp;

        TestUtils.autoCommit = true;
        Numberporting__c numPort = TestUtils.createNumberPorting(orders[0]);
        Competitor_Asset__c ca = PBXSelectionWizardController.createAsset(TestUtils.theSite, new Competitor_Asset__c(), pbx.id);
        insert ca;
        Numberporting_row__c npr = TestUtils.createNumberPortingRow(numPort, cp);
        npr.Location__c = TestUtils.theSite.Id;
        npr.PBX__c = ca.Id;
        update npr;
        List<Phonebook_registration__c> pr = TestUtils.createPhonebookRegistration(numPort, 2); 

        // by running update order status we run all the validations
        OrderUtils.updateOrderStatus(new Set<Id>{theOrderId}, null);

        OrderWrapper ow = OrderUtils.retrieveOrderDetails(theOrderId, TestUtils.theContract.Id);
        ow.SiteMap = OrderUtils.retrieveOrderIdToLocationIdToLocation(new Set<Id>{}, new Set<Id>{ow.theOrder.Id}).get(ow.theOrder.Id);
        ow.locations = ow.SiteMap.values();
        OrderUtils.updateOrderStatus(orders, ow);

        update new Order__c(Id = theOrderId, Status__c = 'Ready for Inside Sales');
        Order__c o = [
            SELECT Id, CustomerReady__c, BillingReady__c, DeliveryReady__c, Propositions__c, OrderReady__c, CTNReady__c,
                Status__c, Mobile_Fixed__c, PhonebookregistrationReady__c, Account__c, NumberportingReady__c,
                VF_Contract__c, VF_Contract__r.Contract_Sign_Date__c, VF_Contract__r.Opportunity__r.Escalation__c, VF_Contract__r.BAN__c, 
                VF_Contract__r.Apple_DEP_ID__c, VF_Contract__r.Count_Apple_DEP__c
            FROM Order__c 
            WHERE Id = :theOrderId
        ];
        OrderUtils.updateOrderStatus(o, true);
        
        o.Status__c = 'Clean';
        update o;
        OrderUtils.updateOrderStatus(o, true);

        //OrderValidation orderCtrl = new OrderValidation(new ApexPages.StandardController(o));
        OrderValidation.childrenTypesFound(TestUtils.theContract.Id, 'family_condition__c');
        OrderValidation.childrenTypesFound(cp.Id, 'contracted_Products__r');
        OrderValidation.childrenTypesFound(TestUtils.theOpportunity.Id, 'opportunityLineItems');
        OrderValidation.childrenTypesFound(TestUtils.theOpportunity.Id, 'family_condition__c');
        OrderValidation.childrenTypesFound(TestUtils.theOpportunity.Id, 'proposition__c');
        OrderValidation.childrenTypesFound(TestUtils.theOpportunity.Id, 'clc__c');
        OrderValidation.childrenTypesFound(TestUtils.theOpportunity.Id, 'deal_type__c');

        System.assertNotEquals(o, null);
    }

    @isTest
    public static void testOrderValidations() {
        
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
        EMP_Automatic_Order_Flow__c empSettings = TestUtils.createEMPCustomSettings();
        NetProfit_Settings__c settings = TestUtils.createNetProfitCustomSettings();
        TestUtils.createOrderValidationNetProfitInformation();
        TestUtils.createOrderValidationOrder();
        TestUtils.createOrderValidationContractedProducts();
        TestUtils.createVFContractValidationOpportunity();
        TestUtils.createOrderValidationBilling();
        TestUtils.createOrderValidationOpportunity();
        TestUtils.createOrderValidationSite();
        Testutils.createOrderValidationNumberportingRow();
        TestUtils.createOrderValidationCompetitorAsset();
        TestUtils.createOrderValidationPhonebookRegistration();
        TestUtils.createOrderValidationContactRoles();
        TestUtils.createOrderValidations();

        Ordertype__c ot = new Ordertype__c(
            Name = 'Enterprise Services',
            ExportSystem__c = 'BOP',
            Status__c = 'Assigned to Inside Sales',
            ExternalId__c = 'OD-04-' + Integer.valueOf(math.rint(math.random()*1000000))
        );
        insert ot;

        Account partnerAcc = TestUtils.createPartnerAccount();
        User pu = TestUtils.createPortalUser(partnerAcc);
        Test.startTest();
        TestUtils.createCompleteContract();
        Test.stopTest();
        
        Contact_Role__c cr = new Contact_Role__c();
        cr.Type__c = 'main';
        cr.Contact__c = TestUtils.thePortalContact.Id;
        cr.Contact__r = TestUtils.thePortalContact;
        cr.Account__c = TestUtils.TheContract.Account__c;
        insert cr;  

        // generate the order via the orderform
        PageReference pageRef = Page.OrderFormPreload;
        pageRef.getParameters().put('contractId', TestUtils.TheContract.Id);
        Test.setCurrentPage(pageRef);
        OrderFormController controller = new OrderFormController(null);
        controller.prepareData();   
        List<Order__c> orders = [SELECT Id, CustomerReady__c, BillingReady__c, DeliveryReady__c, Propositions__c, OrderReady__c, CTNReady__c,
                                    Status__c, Mobile_Fixed__c, PhonebookregistrationReady__c, Account__c, NumberportingReady__c,
                                    VF_Contract__c, VF_Contract__r.Contract_Sign_Date__c, VF_Contract__r.Opportunity__r.Escalation__c, VF_Contract__r.BAN__c, 
                                    VF_Contract__r.Apple_DEP_ID__c, VF_Contract__r.Count_Apple_DEP__c, Ready_for_Inside_Sales__c, Export_system_Customerdata__c
                                FROM Order__c];
        Order__c theOrder = orders[0];
        
        Contract_Attachment__c ca = new Contract_Attachment__c(Contract_VF__c = TestUtils.TheContract.Id, Attachment_Type__c = 'Other');
        insert ca;
        theOrder.Status__c = 'Processed manually';
        theOrder.Contract_Ready__c = false;
        theOrder.Ready_for_Inside_Sales__c = false;
        update theOrder;
        OrderUtils.updateOrderStatus(theOrder, true);
        
        OrderValidation.checkCompleteCustomerFlag(OrderUtils.getContractRecordById(TestUtils.theContract.Id), new List<Contact_Role__c>{cr}, theOrder);
        OrderValidation.checkObject(pu, theOrder);
        OrderValidation.checkObject(cr, theOrder);

        System.assertNotEquals(ca, null);
    }
    
    
    @isTest
    public static void testOrderFieldChecker() {
        TestUtils.createOrderValidations();
        Account partnerAcc = TestUtils.createPartnerAccount();
        User pu = TestUtils.createPortalUser(partnerAcc);
        Test.startTest();
        TestUtils.createCompleteContract();
        Test.stopTest();
        
        Contact_Role__c cr = new Contact_Role__c();
        cr.Type__c = 'main';
        cr.Contact__c = TestUtils.thePortalContact.Id;
        cr.Account__c = partnerAcc.Id;
        insert cr;
        
        update new Account(Id = TestUtils.theAccount.Id, NumberOfEmployees = 4);
        Account acc = [
            SELECT Id, Name, NumberOfEmployees, 
                (SELECT Id, Type__c, Account__c, Account__r.Name, Contact__c, 
                    Contact__r.FirstName, Contact__r.LastName, Contact__r.AccountId, Contact__r.Account.Name 
                 FROM Contact_Roles__r)
            FROM Account 
            WHERE Id = :TestUtils.TheAccount.Id
        ];
        
        OrderValidation.fieldChecker(acc, '!=', 'NumberOfEmployees', '3,4,5');
        OrderValidation.fieldChecker(acc, '>', 'NumberOfEmployees', '3');
        OrderValidation.fieldChecker(acc, '>=', 'NumberOfEmployees', '3');
        OrderValidation.fieldChecker(acc, '<', 'NumberOfEmployees', '5');
        OrderValidation.fieldChecker(acc, '<=', 'NumberOfEmployees', '5');
        OrderValidation.fieldChecker(acc, 'contains', 'Name', 'Test');
        OrderValidation.fieldChecker(acc, 'startsWith', 'Name', 'Test');
        OrderValidation.fieldChecker(acc, 'notStartsWith', 'Name', 'Account');
        OrderValidation.fieldChecker(acc, 'minlength', 'Name', '15');
        OrderValidation.fieldChecker(acc, 'maxlength', 'Name', '10');
        OrderValidation.fieldChecker(acc, 'isnotblank', 'Name', '');
        OrderValidation.fieldChecker(acc, 'requiredChildTypeOR', 'contact_roles__r', 'main');
        OrderValidation.fieldChecker(acc, 'hasChildType', 'contact_roles__r', 'main');
        OrderValidation.fieldChecker(acc, 'hasChildTypeOR', 'contact_roles__r', 'main');

        System.assertNotEquals(acc, null);
    }

}