@isTest
private class TestOrderWrapper {
    @isTest
    static void TestOrderWrapper() {
        OrderWrapper ow = new OrderWrapper();
        DeliveryWrapper dw = new DeliveryWrapper();

        User owner = TestUtils.createAdministrator();
	    Account acct = TestUtils.createAccount(owner);
        Contact cont = TestUtils.createContact(acct);        
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        VF_Contract__c vfContract = TestUtils.createVFContract(acct, opp);
        Order__c order = TestUtils.createOrder(vfContract);
        order.EMP_Automated_Mobile_order__c = true;
        update order;
        ow.theOrder = order;
		
		Contact_Role__c cr = new Contact_Role__c();
		cr.Account__c = acct.Id;
		cr.Contact__c = cont.Id;
		cr.Type__c = 'Main';
		cr.Active__c = true;
		insert cr;
        ow.customerMainContact = cr; 
        ow.customerMaintenanceContact = cr; 
        ow.customerIncidentContact = cr; 
        ow.customerChooserContact = cr;

        ow.locations.add(dw);
        Boolean getOrderHasOneNet = ow.getOrderHasOneNet();
        Boolean getOrderHasOneFixed = ow.getOrderHasOneFixed();
        String getLegacyOrderType = ow.getLegacyOrderType();
        Boolean getHasOneNetFromPropositions = OrderWrapper.getHasOneNetFromPropositions('One Net');
        Boolean getHasFixedVoiceFromPropositions = OrderWrapper.getHasFixedVoiceFromPropositions('One Net');

        System.assert(ow.getOrderIsEMPautomatic());        
    }
}