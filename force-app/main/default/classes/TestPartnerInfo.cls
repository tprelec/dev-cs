/**
 * @description			This is the test class that contains the tests for the PartnerInfo class
 * @author				Ferdinand Bondt
 */
 
@isTest
private class TestPartnerInfo {

    static testMethod void loginAsPartner() {

    	// create partner Account + user
    	Account partnerAccount = TestUtils.createPartnerAccount();
    	User partnerUser = TestUtils.createPortalUser(partnerAccount);
    	
    	// create solution sales and partner mgr users
    	User solSales = TestUtils.createDealer();
    	User partnerMgr = TestUtils.createManager();

    	// create partner TeamMembers
    	AccountTeamMember ss = new AccountTeamMember();
    	ss.UserId = solSales.Id;
    	ss.TeamMemberRole = 'Solution Specialist';
    	ss.AccountId = partnerAccount.Id;
    	insert ss;

    	AccountTeamMember pm = new AccountTeamMember();
    	pm.UserId = partnerMgr.Id;
    	pm.TeamMemberRole = 'Channel Manager';
    	pm.AccountId = partnerAccount.Id;
    	insert pm;


    	AccountTeamMember opam = new AccountTeamMember();
    	opam.UserId = partnerMgr.Id;
    	opam.TeamMemberRole = 'Operational Partner Manager';
    	opam.AccountId = partnerAccount.Id;
    	insert opam;		
                
        Test.startTest();

		// login as partner user
		System.runAs (partnerUser) {     
	        // open page
	        
	        PageReference pageRef = Page.Partner_Home;
			Test.setCurrentPage(pageRef);
			
			PartnerInfo controller = new PartnerInfo(); 
			controller.getThispartner();
			controller.getUserThis();
			controller.getuHotLine();
			controller.getUserChanMgr();
			controller.getUserSolSales();
			controller.getUserOPAM();
		}        



		Test.stopTest();
    }

    static testMethod void loginAsNonPartner() {
    	// login as non-partner user (no special action)
        Test.startTest();

        // open page
        PageReference pageRef = Page.Partner_Home;
		Test.setCurrentPage(pageRef);
		
		PartnerInfo controller = new PartnerInfo(); 
        
		Test.stopTest();

    }	

    static testMethod void loginAsPartnerWithoutSolutionSales() {

    	// create partner Account + user
    	Account partnerAccount = TestUtils.createPartnerAccount();
    	User partnerUser = TestUtils.createPortalUser(partnerAccount);
    	
    	// create solution sales and partner mgr users
    	User partnerMgr = TestUtils.createManager();

    	// create partner TeamMembers
    	AccountTeamMember pm = new AccountTeamMember();
    	pm.UserId = partnerMgr.Id;
    	pm.TeamMemberRole = 'Channel Manager';
    	pm.AccountId = partnerAccount.Id;
    	insert pm;
                
        Test.startTest();

		// login as partner user
		System.runAs (partnerUser) {     
	        // open page
	        
	        PageReference pageRef = Page.Partner_Home;
			Test.setCurrentPage(pageRef);
			
			PartnerInfo controller = new PartnerInfo(); 
		}        

		Test.stopTest();
    }

    static testMethod void loginAsPartnerWithoutChannelManager() {

    	// create partner Account + user
    	Account partnerAccount = TestUtils.createPartnerAccount();
    	User partnerUser = TestUtils.createPortalUser(partnerAccount);
    	
    	// create solution sales and partner mgr users
    	User solSales = TestUtils.createDealer();

    	// create partner TeamMembers
    	AccountTeamMember ss = new AccountTeamMember();
    	ss.UserId = solSales.Id;
    	ss.TeamMemberRole = 'Solution Specialist';
    	ss.AccountId = partnerAccount.Id;
    	insert ss;
                
        Test.startTest();

		// login as partner user
		System.runAs (partnerUser) {      
	        // open page
	        
	        PageReference pageRef = Page.Partner_Home;
			Test.setCurrentPage(pageRef);
			
			PartnerInfo controller = new PartnerInfo(); 
		}        

		Test.stopTest();
    }    

}