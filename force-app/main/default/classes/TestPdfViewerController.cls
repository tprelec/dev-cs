@isTest
public with sharing class TestPdfViewerController {
	@TestSetup
	static void makeData() {
		User owner = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(owner);
		Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

		Attachment attachment = new Attachment();
		attachment.ParentId = opp.Id;
		attachment.Name = 'PDFViewer.pdf';
		attachment.Body = Blob.toPdf('test string');
		insert attachment;

		insert new ContentVersion(
			PathOnClient = 'PDFViewer.pdf',
			Title = 'PDFViewer.pdf',
			ContentLocation = 'S',
			Origin = 'C',
			VersionData = Blob.toPdf('test string')
		);
	}

	@isTest
	public static void testGetContentFromContentVersion() {
		List<ContentVersion> cvList = [SELECT Id FROM ContentVersion WHERE Title = 'PDFViewer.pdf'];
		PdfViewerController.PdfFile pdf;
		if (!cvList.isEmpty()) {
			pdf = PdfViewerController.getContent(cvList[0].Id);
		}
		System.assertEquals(pdf.name, 'PDFViewer.pdf', 'PDF exists');
	}

	@isTest
	public static void testGetContentFromAttachment() {
		List<Attachment> cvList = [SELECT Id FROM Attachment WHERE Name = 'PDFViewer.pdf'];
		PdfViewerController.PdfFile pdf;
		if (!cvList.isEmpty()) {
			pdf = PdfViewerController.getContent(cvList[0].Id);
		}
		System.assertEquals(pdf.name, 'PDFViewer.pdf', 'PDF exists');
	}
}