/**
 * @description			This class is responsible for testing the preferredsupplier triggerhandler
 * @author				Guy Clairbois
 */
@isTest
private class TestPreferredSupplierTriggerHandler {
	static testMethod void TestCreateNewPreferredSupplier(){
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);		

		// create mappings
		TestUtils.createPortFolioVendorMappings();
        TestUtils.createSalesSettings();

		Site__c s = TestUtils.createSite(acct);

		Site_Postal_Check__c spc = new Site_Postal_Check__c();
		spc.Access_Site_ID__c = s.Id;
		spc.Technology_Type__c = 'tech1';
		spc.Access_Vendor__c = 'vendor1';
		insert spc;

        Test.startTest();


        Preferred_Supplier__c ps = new Preferred_Supplier__c();
        ps.Vendor__c = spc.Access_Vendor__c;
        ps.Technology_Type__c = spc.Technology_Type__c;
        ps.Order__c = 1;
        insert ps;

		
		Test.stopTest();
		List<Site_Postal_Check__c> pcs = [Select Id,Order__c from Site_Postal_Check__c where Access_Site_ID__c = :s.Id];
		// check if the right order is assigned via the preferred supllier
		System.assertEquals(pcs[0].Order__c,ps.Order__c);	
	}
}