@isTest
private class TestPriceItemTriggerHandler {

  @isTest
    static void createAddOn() {
        TestUtils.createOrderType();
        TestUtils.createOneOffProduct();
        TestUtils.createRecurringProduct();

        Product2[] ooprod = [select Id, ProductCode From Product2 Where ProductCode = :TestUtils.theOneOffProduct.ProductCode and IsActive = true];
        Product2[] recprod = [select Id, ProductCode From Product2 Where ProductCode = :TestUtils.theRecurringProduct.ProductCode and IsActive = true];

        cspmb__Price_Item__c aopi = new cspmb__Price_Item__c ();
        aopi.cspmb__One_Off_Charge_External_Id__c = ooprod [0].ProductCode;
        aopi.cspmb__Recurring_Charge_External_Id__c = recprod [0].ProductCode;
        aopi.cspmb__Is_Active__c = true;

        test.startTest();
        insert aopi;
        system.assertEquals(ooprod[0].Id,[Select One_Off_Charge_Product__c From cspmb__Price_Item__c LIMIT 1].One_Off_Charge_Product__c);
        system.assertEquals(recprod[0].Id,[Select Recurring_Charge_Product__c From cspmb__Price_Item__c LIMIT 1].Recurring_Charge_Product__c);


        test.stopTest();
    }

}