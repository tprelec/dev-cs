@isTest
private class TestProductBasketAttachmentMgrController {
    
    @isTest
    static void createAttachments() {
   
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );

        Framework__c f = Framework__c.getOrgDefaults();
        f.Framework_Sequence_Number__c = 1;
        upsert f;
        
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c=opp.id;

        insert basket;

        test.startTest();
        // first test with no basket
        try{
        	ProductBasketAttachmentManagerController pbamc = new ProductBasketAttachmentManagerController();
            system.assert(true,'Should not reach this point.');
        } catch (Exception e){
            // expected
        }
        ApexPages.StandardController controller = new ApexPages.StandardController(basket);
        ProductBasketAttachmentManagerController pbamc = new ProductBasketAttachmentManagerController(controller);

        
        
        // create a new attachment and save it

        
        pbamc.newBasketAttachments[0].pba.Attachment_Type__c = 'Other';
        
        ContentVersion cv=new Contentversion();
        cv.title='ABC';
        cv.PathOnClient ='test';
        cv.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        
        pbamc.newBasketAttachments[0].file = cv;
        
        pbamc.saveAttachments();

       	pbamc.SelectedBaId = pbamc.BasketAttachments[0].pba.Id;
        pbamc.DeleteBasketAttachment();
        
		// misc methods         
        pbamc.backToBasket();
        pbamc.manageAttachments();
        
        test.stopTest();
    }
    
}