@isTest
private class TestProductBasketTriggerHandler {

    @testSetup
    static void makeData(){
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        opp.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
        update opp;
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
            cscfga__Opportunity__c = opp.id
        );
        insert basket;
    }

    @isTest
    static void createFrameWorkIDAndChangePrimary() {
        cscfga__Product_Basket__c basket = [SELECT Id, Primary__c FROM cscfga__Product_Basket__c LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        // make primary
        basket.Primary__c = true;
        update basket;

        test.startTest();
        // create new basket and make primary
        cscfga__Product_Basket__c basket2 = new cscfga__Product_Basket__c();
        basket2.cscfga__Opportunity__c = opp.id;
        basket2.Name = 'New Basket '+ system.now();
        insert basket2;
        basket.Primary__c = true;

        // stop test to prevent recursive trigger protection
        test.stopTest();
        update basket;

        // also test delete
        delete basket2;
    }

    @isTest
    static void createBasketWithAccreditationLevelsAndJSONKPIValues() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        test.startTest();
        // create accreditation levels
        Accreditation_Category__c cloud = new Accreditation_Category__c(Name = ProductBasketTriggerHandler.ACCCATCLOUD);
        Accreditation_Category__c infra = new Accreditation_Category__c(Name = ProductBasketTriggerHandler.ACCCATINFRA);
        Accreditation_Category__c mobility = new Accreditation_Category__c(Name = ProductBasketTriggerHandler.ACCCATMOBILITY);
        insert new List<Accreditation_Category__c>{cloud, infra, mobility};

        // assign to current user
        String profileName = [SELECT Id, Name FROM Profile WHERE Id = :Userinfo.getProfileId()].Name;
        Id vodafoneAccountId = [SELECT Id, Name, BOPCode__c  FROM Account WHERE BOPCode__c = 'TNF'].Id;

        Accreditation_Level__c usercloud = new Accreditation_Level__c(Accreditation_Category__c = cloud.Id, Level__c = 2, Profile__c = profileName, Account__c = vodafoneAccountId);
        Accreditation_Level__c userinfra = new Accreditation_Level__c(Accreditation_Category__c = infra.Id, Level__c = 2, Profile__c = profileName, Account__c = vodafoneAccountId);
        Accreditation_Level__c usermobility = new Accreditation_Level__c(Accreditation_Category__c = mobility.Id, Level__c=2, Profile__c = profileName, Account__c = vodafoneAccountId);
        insert new List<Accreditation_Level__c>{usercloud, userinfra, usermobility};

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c = opp.id;
        basket.Name = 'New Basket '+ system.now();

        insert basket;

        basket.KPI_JSON_Values__c = '[{"Category":"Access","Payback_Period": 10,"NPV": 110,"Net_Incremental_Billed_Revenue": 123,"Free_cash_flow_div_Net_revenue": 0,"EBITDA_div_Net_revenue": 0,"Sales_Margin_div_Net_revenue": 0},{"Category":"Total","Payback_Period": 0,"NPV": 0,"Net_Incremental_Billed_Revenue": 0,"Free_cash_flow_div_Net_revenue": 0,"EBITDA_div_Net_revenue": 0,"Sales_Margin_div_Net_revenue": 0}]';
        update basket;
    }

    @isTest
    static void createBasketWithApprovalStatusesAndAttachments() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        test.startTest();

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c = opp.id;
        basket.Name = 'New Basket '+ system.now();
        basket.Basket_Approval_Status__c = 'Pending';
        insert basket;

        basket.Basket_Approval_Status__c = 'Submitted';
        update basket;

        // insert an attachment before approving
        ApexPages.StandardController controller = new ApexPages.StandardController(basket);
        ProductBasketAttachmentManagerController pbamc = new ProductBasketAttachmentManagerController(controller);

        pbamc.newBasketAttachments[0].pba.Attachment_Type__c = 'Other';
        ContentVersion cv=new Contentversion();
        cv.title ='ABC';
        cv.PathOnClient ='test';
        cv.versiondata = EncodingUtil.base64Decode('Unit Test Attachment Body');
        pbamc.newBasketAttachments[0].file = cv;

        pbamc.saveAttachments();

        basket.Basket_Approval_Status__c = 'Approved';
        update basket;

        basket.Basket_Approval_Status__c = 'Rejected';
        update basket;

        system.assertEquals('Rejected',[select cscfga__Basket_Status__c from cscfga__Product_Basket__c Where Id=:basket.Id].cscfga__Basket_Status__c);

        basket.Basket_Approval_Status__c = 'Recalled';
        update basket;
    }

    @isTest
    private static void setName() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        cscfga__Product_Basket__c basket = [SELECT Id, Basket_Number__c, Name FROM cscfga__Product_Basket__c LIMIT 1];
        System.assertEquals(basket.Basket_Number__c, basket.Name);
    }

    @isTest
    private static void checkIfBasketChangeInApproval() {
        cscfga__Product_Basket__c basket = [SELECT Id, cscfga__Basket_Status__c FROM cscfga__Product_Basket__c LIMIT 1];
        appro__Approval_Request__c approvalRequest = new appro__Approval_Request__c(
            Product_Basket_lookup__c = basket.Id,
            appro__RID__C = basket.Id
        );
        insert approvalRequest;
        basket.cscfga__Basket_Status__c = 'Pending Approval';
        Update basket;
        basket.cscfga__Basket_Status__c = 'Valid';
        Update basket;

        System.assertEquals(true, [SELECT Id, appro__Is_Edited__c FROM appro__Approval_Request__c WHERE Id = :approvalRequest.Id].appro__Is_Edited__c);
    }

    @isTest
    private static void updateApprovalStatus_netProfitApprovalStatusChange() {
        cscfga__Product_Basket__c basket = [SELECT Id, cscfga__Basket_Status__c, NetProfit_Approval_Status__c, Basket_Approval_Status__c FROM cscfga__Product_Basket__c LIMIT 1];

        basket.NetProfit_Approval_Status__c = 'Submitted';
        Update basket;
        cscfga__Product_Basket__c basketAfterUpdate = [SELECT Id, cscfga__Basket_Status__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
        System.assertEquals('Pending approval', basketAfterUpdate.cscfga__Basket_Status__c);

        basket.NetProfit_Approval_Status__c = 'Rejected';
        Update basket;
        basketAfterUpdate = [SELECT Id, cscfga__Basket_Status__c, NetProfit_Approval_Status__c, Basket_Approval_Status__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
        System.assertEquals('Rejected', basketAfterUpdate.cscfga__Basket_Status__c);
        System.assertEquals(null, basketAfterUpdate.Basket_Approval_Status__c);
        System.assertEquals(null, basketAfterUpdate.NetProfit_Approval_Status__c);

        basket.NetProfit_Approval_Status__c = 'Recalled';
        Update basket;
        basketAfterUpdate = [SELECT Id, cscfga__Basket_Status__c, NetProfit_Approval_Status__c, Basket_Approval_Status__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
        System.assertEquals('Valid', basketAfterUpdate.cscfga__Basket_Status__c);
        System.assertEquals(null, basketAfterUpdate.Basket_Approval_Status__c);
        System.assertEquals(null, basketAfterUpdate.NetProfit_Approval_Status__c);

        basket.NetProfit_Approval_Status__c = 'Approved';
        Update basket;
        basketAfterUpdate = [SELECT Id, cscfga__Basket_Status__c, NetProfit_Approval_Status__c, Basket_Approval_Status__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
        System.assertEquals('Approved', basketAfterUpdate.cscfga__Basket_Status__c);

        //test hier iets in het laatste blok van de method
        appro__Approval_Request__c approvalRequest = new appro__Approval_Request__c(
            Product_Basket_lookup__c = basket.Id,
            appro__RID__C = basket.Id,
            appro__Status__c = 'Approved'
        );
        insert approvalRequest;
        appro__Approval_Path__c approvalPath = new appro__Approval_Path__c();
        insert approvalPath;
        appro__Approval_Step__c approvalStep = new appro__Approval_Step__c(
            appro__Approval_Path__c = approvalPath.Id,
            IsNetProfitIdentificationStep__c = true
            );
            insert approvalStep;
        appro__Approval__c approval = new appro__Approval__c(
            appro__Approval_Request__c = approvalRequest.Id,
            appro__Status__c = 'Approved',
            appro__Approval_Step__c = approvalStep.Id
        );
        insert approval;
        basket.Basket_Approval_Status__c = 'Approved';
        update basket;

        basketAfterUpdate = [SELECT Id, cscfga__Basket_Status__c, NetProfit_Approval_Status__c, Basket_Approval_Status__c, Basket_Qualification__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
        System.assertEquals('Pending usage', basketAfterUpdate.cscfga__Basket_Status__c);
        System.assertEquals('Net profit', basketAfterUpdate.Basket_Qualification__c);
    }

    @isTest
    private static void deleteApprovalRequests() {
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
        appro__Approval_Request__c approvalRequest = new appro__Approval_Request__c(
            Product_Basket_lookup__c = basket.Id
        );
        insert approvalRequest;
        delete basket;
        System.assertEquals(
            0,
            [SELECT Id FROM appro__Approval_Request__c WHERE Id = :approvalRequest.Id].size()
        );
    }
}