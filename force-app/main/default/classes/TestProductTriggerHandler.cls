/**
 * @description         This is the test class for the ProductTriggerHandler. 
 * @author              Guy Clairbois
 */

@isTest
private class TestProductTriggerHandler {
	
	@isTest() static void test_newProduct() {
        OrderType__c ot = TestUtils.createOrderType();

		Product2 testProduct = new Product2();
		testProduct.Name = 'test123'+system.now();
		testProduct.ProductCode = 'abc12345'+system.now();
		testProduct.Product_Line__c = 'fVodafone';
		testProduct.OrderType__c=ot.id;

		insert testProduct;

		List<Product_History__c> phs = [Select Id,Field_Name__c From Product_History__c Where Product__c = :testProduct.Id];
		system.debug(phs);
		system.assertEquals(3,phs.size()); // 3 because BigMachines part number is also changed by workflow
	}
	
	@isTest() static void test_updateProduct() {
        OrderType__c ot = TestUtils.createOrderType();

		Product2 testProduct = new Product2();
		testProduct.Name = 'test123'+system.now();
		testProduct.ProductCode = 'abc12345'+system.now();
		testProduct.Product_Line__c = 'fVodafone';
		testProduct.BAP_SAP__c = 2;
		testProduct.OrderType__c=ot.id;		
		insert testProduct;

		testProduct.BAP_SAP__c = 3;
		update testProduct;

		testProduct.BAP_SAP__c = null;
		update testProduct;

		List<Product_History__c> phs = [Select Id, CreatedDate, Date__c, New_Value__c From Product_History__c Where Product__c = :testProduct.Id ];
		system.debug(phs);
		system.assertEquals(5,phs.size()); // 5 because BigMachines part number is also changed by workflow
	}

	@isTest() static void test_deleteProduct() {
        OrderType__c ot = TestUtils.createOrderType();
        
		Product2 testProduct = new Product2();
		testProduct.Name = 'test123'+system.now();
		testProduct.ProductCode = 'abc12345'+system.now();
		testProduct.Product_Line__c = 'fVodafone';
		testProduct.OrderType__c=ot.id;		
		insert testProduct;

		delete testProduct;

		List<Product_History__c> phs = [Select Id From Product_History__c Where Product__c = :testProduct.Id];
		system.assertEquals(phs.size(),0);

	}
	
}