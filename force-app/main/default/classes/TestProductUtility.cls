@isTest
public with sharing class TestProductUtility {

    @isTest
    public static void testCreateOlisAccess() {
        cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
        PriceReset__c priceReset = new PriceReset__c(ConfigurationName__c = 'testConfigurationName', ErrorMessage__c = 'testError', ErrorMessage2__c = 'testError2', MaxRecurringPrice__c = 5, RecurringAttributeName__c = 'raName');
        insert priceReset;
        
        Site__c site = [select id from Site__c limit 1];
        Site_Postal_Check__c sitePostalCheck = TestUtils.createSitePostalCheck(site.Id);
        Site_Availability__c siteAvailability = TestUtils.createSiteAvailability(site.Id, sitePostalCheck.Id);

        cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('testProdDefName');
        prodDefinition.Product_Type__c = 'Mobile';
        insert prodDefinition;
        cscfga__Attribute_Definition__c attributeDefiniton = CS_DataTest.createPackageSlotAttributeDefinition(prodDefinition.Id, 'testAttName', 'testAttValue');
        attributeDefiniton.cscfga__Type__c = 'Text Display';
        insert attributeDefiniton;

        cscfga__Product_Configuration__c parentConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'Company Level Fixed Voice', basket.Id);
        parentConfiguration.Recurring_Charge_Product__c = [select Id from product2 limit 1].Id;
        parentConfiguration.Site_Availability_Id__c = siteAvailability.Id;
        //parentConfiguration.Deal_type__c = 'Retention';
        parentConfiguration.Category__c = 'Access';
        insert parentConfiguration;

        cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'Company Level Fixed Voice', basket.Id);
        productConfiguration.Recurring_Charge_Product__c = [select Id from product2 limit 1].Id;
        productConfiguration.Site_Availability_Id__c = siteAvailability.Id;
        productConfiguration.Category__c = 'Access';
        productConfiguration.Deal_type__c = 'Acq';
        productConfiguration.cscfga__Parent_Configuration__c = parentConfiguration.Id;
        insert productConfiguration;

        cscfga__Attribute__c attribute = CS_DataTest.createAttribute('testAttName', attributeDefiniton, true, 5, productConfiguration, true, 'testValue');
        insert attribute;

        Test.startTest();
            ProductUtility.CreateOLIs(new Set<String>{basket.Id}, null);
        Test.stopTest();
    }

    @isTest
    public static void testMobileDisconnect() {
        cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
        PriceReset__c priceReset = new PriceReset__c(ConfigurationName__c = 'testConfigurationName', ErrorMessage__c = 'testError', ErrorMessage2__c = 'testError2', MaxRecurringPrice__c = 5, RecurringAttributeName__c = 'raName');
        insert priceReset;

        cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
        prodDefinition.Product_Type__c = 'Mobile';
        insert prodDefinition;

        cscfga__Product_Configuration__c parentConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'Mobile CTN profile', basket.Id);
        parentConfiguration.Recurring_Charge_Product__c = [SELECT Id FROM product2 limit 1].Id;
        insert parentConfiguration;

        cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'Mobile CTN Subscription', basket.Id);
        productConfiguration.Recurring_Charge_Product__c = [SELECT Id FROM product2 limit 1].Id;
        productConfiguration.Deal_type__c = 'Disconnect';
        productConfiguration.cscfga__Parent_Configuration__c = parentConfiguration.Id;
        insert productConfiguration;

        cscfga__Attribute__c attribute = CS_DataTest.createAttribute(productConfiguration.id, 'Connection type', 'Disconnect');
        insert attribute;

        Test.startTest();
            ProductUtility.CreateOLIs(new Set<String>{basket.Id}, null);
            List<OpportunityLineItem> olis = [SELECT Id, Deal_Type__c FROM OpportunityLineItem WHERE Deal_Type__c = 'Disconnect'];
            System.assertEquals(0, olis.size());
        Test.stopTest();
    }

    @isTest
    public static void testCreateOlisRouting() {
        cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
        PriceReset__c priceReset = new PriceReset__c(ConfigurationName__c = 'testConfigurationName', ErrorMessage__c = 'testError', ErrorMessage2__c = 'testError2', MaxRecurringPrice__c = 5, RecurringAttributeName__c = 'raName');
        insert priceReset;
        
        Site__c site = [select id from Site__c limit 1];
        Site_Postal_Check__c sitePostalCheck = TestUtils.createSitePostalCheck(site.Id);
        Site_Availability__c siteAvailability = TestUtils.createSiteAvailability(site.Id, sitePostalCheck.Id);

        cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('testProdDefName');
        prodDefinition.Product_Type__c = 'Mobile';
        insert prodDefinition;
        cscfga__Attribute_Definition__c attributeDefiniton = CS_DataTest.createPackageSlotAttributeDefinition(prodDefinition.Id, 'testAttName', 'testAttValue');
        attributeDefiniton.cscfga__Type__c = 'Text Display';
        insert attributeDefiniton;

        cscfga__Product_Configuration__c parentConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'Company Level Fixed Voice', basket.Id);
        parentConfiguration.Recurring_Charge_Product__c = [select Id from product2 limit 1].Id;
        parentConfiguration.Site_Availability_Id__c = siteAvailability.Id;
        parentConfiguration.Category__c = 'Routing';
        insert parentConfiguration;

        cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'Company Level Fixed Voice', basket.Id);
        productConfiguration.Recurring_Charge_Product__c = [select Id from product2 limit 1].Id;
        productConfiguration.Site_Availability_Id__c = siteAvailability.Id;
        productConfiguration.Category__c = 'Routing';
        productConfiguration.cscfga__Parent_Configuration__c = parentConfiguration.Id;
        insert productConfiguration;

        cscfga__Attribute__c attribute = CS_DataTest.createAttribute('testAttName', attributeDefiniton, true, 5, productConfiguration, true, 'testValue');
        insert attribute;

        Test.startTest();
            ProductUtility.CreateOLIs(new Set<String>{basket.Id}, null);
        Test.stopTest();
    }

    @isTest
    public static void testCreateOlisNoParent() {
        cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
        PriceReset__c priceReset = new PriceReset__c(ConfigurationName__c = 'testConfigurationName', ErrorMessage__c = 'testError', ErrorMessage2__c = 'testError2', MaxRecurringPrice__c = 5, RecurringAttributeName__c = 'raName');
        insert priceReset;
        
        Site__c site = [select id from Site__c limit 1];
        Site_Postal_Check__c sitePostalCheck = TestUtils.createSitePostalCheck(site.Id);
        Site_Availability__c siteAvailability = TestUtils.createSiteAvailability(site.Id, sitePostalCheck.Id);

        cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('testProdDefName');
        prodDefinition.Product_Type__c = 'Mobile';
        insert prodDefinition;
        cscfga__Attribute_Definition__c attributeDefiniton = CS_DataTest.createPackageSlotAttributeDefinition(prodDefinition.Id, 'testAttName', 'testAttValue');
        attributeDefiniton.cscfga__Type__c = 'Text Display';
        insert attributeDefiniton;


        cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'Company Level Fixed Voice', basket.Id);
        productConfiguration.Recurring_Charge_Product__c = [select Id from product2 limit 1].Id;
        productConfiguration.Site_Availability_Id__c = siteAvailability.Id;
        productConfiguration.Category__c = 'Routing';
        insert productConfiguration;

        cscfga__Attribute__c attribute = CS_DataTest.createAttribute('testAttName', attributeDefiniton, true, 5, productConfiguration, true, 'testValue');
        insert attribute;

        Test.startTest();
            ProductUtility.CreateOLIs(new Set<String>{basket.Id}, null);
        Test.stopTest();
    }

    @isTest
    public static void testDeleteHardOLIs() {
        cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
        PriceReset__c priceReset = new PriceReset__c(ConfigurationName__c = 'testConfigurationName', ErrorMessage__c = 'testError', ErrorMessage2__c = 'testError2', MaxRecurringPrice__c = 5, RecurringAttributeName__c = 'raName');
        insert priceReset;

        cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('testProdDefName');
        prodDefinition.Product_Type__c = 'Mobile';
        insert prodDefinition;
        cscfga__Attribute_Definition__c attributeDefiniton = CS_DataTest.createPackageSlotAttributeDefinition(prodDefinition.Id, 'testAttName', 'testAttValue');
        attributeDefiniton.cscfga__Type__c = 'Text Display';
        insert attributeDefiniton;
        cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'testConfigurationName', basket.Id);
        insert productConfiguration;
        cscfga__Attribute__c attribute = CS_DataTest.createAttribute('testAttName', attributeDefiniton, true, 5, productConfiguration, true, 'testValue');
        insert attribute;

        Test.startTest();
            ProductUtility.DeleteHardOLIs(new Set<String>{basket.Id});
        Test.stopTest();
    }

    @isTest
    public static void testWithDiscounts() {
        cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
        PriceReset__c priceReset = new PriceReset__c(ConfigurationName__c = 'testConfigurationName', ErrorMessage__c = 'testError', ErrorMessage2__c = 'testError2', MaxRecurringPrice__c = 5, RecurringAttributeName__c = 'raName');
        insert priceReset;

        cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('Mobile CTN Profile');
        prodDefinition.Product_Type__c = 'Mobile';
        insert prodDefinition;
        cscfga__Attribute_Definition__c attributeDefiniton = CS_DataTest.createAttributeDefinition(prodDefinition.Id, 'testAttName', 'testAttValue');
        attributeDefiniton.cscfga__Type__c = 'Text Display';
        insert attributeDefiniton;

        cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'Mobile CTN subscription', basket.Id);
        productConfiguration.cscfga__discounts__c = '{"discounts":[{"version":"3-0-0","recordType":"group","memberDiscounts":[{"version":"3-0-0","type":"percentage","source":"Manual 1","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 10%","chargeType":"recurring","amount":10},{"version":"3-0-0","type":"absolute","source":"Manual 2","recordType":"single","discountCharge":"__PRODUCT__","description":"OneOff Charge - 2","chargeType":"oneOff","amount":2}],"evaluationOrder":"serial"}]}';
        productConfiguration.Recurring_Charge_Product__c = [SELECT Id FROM product2 LIMIT 1].Id;
        insert productConfiguration;

        cscfga__Attribute__c attribute = CS_DataTest.createAttribute('testAttName', attributeDefiniton, true, 5, productConfiguration, true, 'testValue');
        insert attribute;

        Test.startTest();
            ProductUtility.CreateOLIs(new Set<String>{basket.Id}, null);
            List<OpportunityLineItem> olis = [SELECT Id, DiscountNew__c FROM OpportunityLineItem WHERE OpportunityId = :basket.cscfga__Opportunity__c AND DiscountNew__c = 0];
            System.assertEquals(0, olis.size());
        Test.stopTest();
    }
    
    @isTest
    public static void testOneMobileDef() {
        
        cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();

        cscfga__Product_Definition__c prodDefinition = CS_DataTest.createProductDefinition('OneMobile');
        prodDefinition.Product_Type__c = 'Mobile';
        insert prodDefinition;
    
        cscfga__Product_Configuration__c parentConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'OneMobile', basket.Id);
        parentConfiguration.Recurring_Charge_Product__c = [SELECT Id FROM product2 limit 1].Id;
        parentConfiguration.cscfga__Product_Family__c = 'OneMobile';
        insert parentConfiguration;
    
        cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(prodDefinition.Id, 'OneMobile AddOn', basket.Id);
        productConfiguration.Recurring_Charge_Product__c = [SELECT Id FROM product2 limit 1].Id;
        productConfiguration.Deal_type__c = 'New';
        parentConfiguration.cscfga__Product_Family__c = 'OneMobile';
        productConfiguration.cscfga__Parent_Configuration__c = parentConfiguration.Id;
        insert productConfiguration;
    
        cscfga__Attribute__c attribute = CS_DataTest.createAttribute(productConfiguration.id, 'Recurring', '10');
        attribute.cscfga__Is_Line_Item__c = true;
        attribute.cscfga__Recurring__c = true;
        attribute.cscfga__Price__c = 100;
        insert attribute;

        MockResponseGeneratorforConnectionsOE oegenerator = new MockResponseGeneratorforConnectionsOE(productConfiguration.Id);
        Map<Id,List<cssmgnt.ProductProcessingUtility.Component>> oeMap = oegenerator.createConnectionsTestOE();
        Map<Id,Map<String,List<cssmgnt.ProductProcessingUtility.Attribute>>> pcOeAttrMap = CustomButtonSynchronizeWithOpportunity.solutionOESchemaSimplifier(oeMap);
    
        Test.startTest();
        ProductUtility.CreateOLIs(new Set<String>{basket.Id}, pcOeAttrMap);
        //List<OpportunityLineItem> olis = [SELECT Id, Deal_Type__c FROM OpportunityLineItem WHERE Deal_Type__c = 'Disconnect'];
        //System.assertEquals(0, olis.size());
        Test.stopTest();
    }
}