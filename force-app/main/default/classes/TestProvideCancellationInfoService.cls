@IsTest
private class TestProvideCancellationInfoService {
	@IsTest
	static void testCancelProductSuccess() {
		String transactionId = prepareTestData();
		List<ProvideCancellationInfoService.CancellationResult> cancelResults = new List<ProvideCancellationInfoService.CancellationResult>();
		ProvideCancellationInfoService.CancellationResult cancelResult = new ProvideCancellationInfoService.CancellationResult();
		cancelResult.orderId = '7777777';
		cancelResult.result = 'OK';
		cancelResults.add(cancelResult);
		ProvideCancellationInfoService.ProvideCancellationInfo provCancelInfo = new ProvideCancellationInfoService.ProvideCancellationInfo();
		provCancelInfo.transactionID = transactionId;
		provCancelInfo.CancellationResults = cancelResults;

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/providecancellationinfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(provCancelInfo));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		ProvideCancellationInfoService.cancelOrderLineItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('OK', m.get('status'), 'Incorrect Status');
	}

	@IsTest
	static void testTransIdAndExtRefIdNotFound() {
		List<ProvideCancellationInfoService.CancellationResult> cancelResults = new List<ProvideCancellationInfoService.CancellationResult>();
		ProvideCancellationInfoService.CancellationResult cancelResult = new ProvideCancellationInfoService.CancellationResult();
		cancelResult.result = 'OK';
		cancelResults.add(cancelResult);
		ProvideCancellationInfoService.ProvideCancellationInfo provCancelInfo = new ProvideCancellationInfoService.ProvideCancellationInfo();
		provCancelInfo.transactionID = 'foobar';
		provCancelInfo.CancellationResults = cancelResults;

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/ProvideCancellationInfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(provCancelInfo));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		ProvideCancellationInfoService.cancelOrderLineItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('FAILED', m.get('status'), 'Incorrect Status');

		List<Object> l = (List<Object>) m.get('errors');
		Map<String, Object> a0 = (Map<String, Object>) l[0];
		System.assertEquals('SFEC-9999', a0.get('errorCode'), 'Incorrect Error Code');
		Map<String, Object> a1 = (Map<String, Object>) l[1];
		System.assertEquals('SFEC-9999', a1.get('errorCode'), 'Incorrect Error Code');
	}

	@IsTest
	static void testProductResultIsFailed() {
		String transactionId = prepareTestData();
		List<ProvideCancellationInfoService.Error> errors = new List<ProvideCancellationInfoService.Error>();
		ProvideCancellationInfoService.Error err = new ProvideCancellationInfoService.Error();
		err.errorCode = 'errorCode';
		err.errorMessage = 'errorMessage';
		errors.add(err);
		List<ProvideCancellationInfoService.CancellationResult> cancelResults = new List<ProvideCancellationInfoService.CancellationResult>();
		ProvideCancellationInfoService.CancellationResult cancelResult = new ProvideCancellationInfoService.CancellationResult();
		cancelResult.orderId = '7777777';
		cancelResult.result = 'FAILED';
		cancelResult.errors = errors;
		cancelResults.add(cancelResult);
		ProvideCancellationInfoService.ProvideCancellationInfo provCancelInfo = new ProvideCancellationInfoService.ProvideCancellationInfo();
		provCancelInfo.transactionID = transactionId;
		provCancelInfo.CancellationResults = cancelResults;

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/ProvideCancellationInfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(provCancelInfo));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		ProvideCancellationInfoService.cancelOrderLineItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('OK', m.get('status'), 'Incorrect Status');
	}

	@IsTest
	static void testProductUpdateFails() {
		String transactionId = prepareTestData();
		Contracted_Products__c cpTest = [SELECT Error_Info__c FROM Contracted_Products__c LIMIT 1];
		// Below code is used to force a Database.update error (exceeding the field size)
		for (Integer i = 0; i < 32760; i++) {
			cpTest.Error_Info__c += 'x';
		}
		update cpTest;
		List<ProvideCancellationInfoService.Error> errors = new List<ProvideCancellationInfoService.Error>();
		ProvideCancellationInfoService.Error err = new ProvideCancellationInfoService.Error();
		err.errorCode = 'errorCode';
		err.errorMessage = 'errorMessage';
		errors.add(err);
		List<ProvideCancellationInfoService.CancellationResult> cancelResults = new List<ProvideCancellationInfoService.CancellationResult>();
		ProvideCancellationInfoService.CancellationResult cancelResult = new ProvideCancellationInfoService.CancellationResult();
		cancelResult.orderId = '7777777';
		cancelResult.result = 'FAILED';
		cancelResult.errors = errors;
		cancelResults.add(cancelResult);
		ProvideCancellationInfoService.ProvideCancellationInfo provCancelInfo = new ProvideCancellationInfoService.ProvideCancellationInfo();
		provCancelInfo.transactionID = transactionId;
		provCancelInfo.CancellationResults = cancelResults;

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/ProvideCancellationInfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(provCancelInfo));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		ProvideCancellationInfoService.cancelOrderLineItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('FAILED', m.get('status'), 'Incorrect Status');

		List<Object> l = (List<Object>) m.get('errors');
		Map<String, Object> a = (Map<String, Object>) l[0];
		System.assertEquals('SFEC-9999', a.get('errorCode'), 'Incorrect Error Code');
	}

	private static String prepareTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test');
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, c.Id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct();
		insert product;

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = contr.Id;
		insert ord;

		Customer_Asset__c ca = new Customer_Asset__c();
		ca.Billing_Arrangement__c = ba.Id;
		ca.Order__c = ord.Id;
		ca.Site__c = site.Id;
		insert ca;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.CLC__c = 'Acq'; // Required and fixed for BOP
		cp.VF_Contract__c = contr.Id;
		cp.Customer_Asset__c = ca.Id;
		cp.Product__c = product.Id;
		cp.Billing_Order_Id__c = '7777777';
		insert cp;

		Order_Billing_Transaction__c obt = new Order_Billing_Transaction__c();
		obt.Transaction_Id__c = 'OBT-123456';
		insert obt;

		String transactionId = obt.Transaction_Id__c;
		return transactionId;
	}
}