@IsTest
private class TestProvideCreditInfoService {
	@IsTest
	static void testUpdateCreditLineSuccess() {
		String creditLineName = prepareTestData();
		List<ProvideCreditInfoService.CreditLineResult> credResults = new List<ProvideCreditInfoService.CreditLineResult>();
		ProvideCreditInfoService.CreditLineResult credResult = new ProvideCreditInfoService.CreditLineResult();
		credResult.sfdcCreditId = creditLineName;
		credResult.creditId = '55555';
		credResult.result = 'OK';
		credResults.add(credResult);
		ProvideCreditInfoService.ProvideCreditInfo provCredInfo = new ProvideCreditInfoService.ProvideCreditInfo();
		provCredInfo.transactionID = 'CT-TEST';
		provCredInfo.creditLineResults = credResults;

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/providecreditinfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(provCredInfo));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		ProvideCreditInfoService.updateCreditLineItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('OK', m.get('status'), 'Incorrect Status');
	}

	@IsTest
	static void testUpdateCreditLineError() {
		String creditLineName = prepareTestData();

		List<ProvideCreditInfoService.CreditLineResult> credResults = new List<ProvideCreditInfoService.CreditLineResult>();
		List<ProvideCreditInfoService.Error> errors = new List<ProvideCreditInfoService.Error>();
		ProvideCreditInfoService.Error err = new ProvideCreditInfoService.Error();
		err.errorCode = 'CODE';
		err.errorMessage = 'MSG';
		errors.add(err);
		ProvideCreditInfoService.CreditLineResult credResult = new ProvideCreditInfoService.CreditLineResult();
		credResult.sfdcCreditId = creditLineName;
		credResult.creditId = '55555';
		credResult.result = 'FAILED';
		credResult.errors = errors;
		credResults.add(credResult);
		ProvideCreditInfoService.ProvideCreditInfo provCredInfo = new ProvideCreditInfoService.ProvideCreditInfo();
		provCredInfo.transactionID = 'CT-TEST';
		provCredInfo.creditLineResults = credResults;

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/providecreditinfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(provCredInfo));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		ProvideCreditInfoService.updateCreditLineItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('OK', m.get('status'), 'Incorrect Status');
	}

	@IsTest
	static void testMissingCreditNote() {
		List<ProvideCreditInfoService.CreditLineResult> credResults = new List<ProvideCreditInfoService.CreditLineResult>();
		ProvideCreditInfoService.CreditLineResult credResult = new ProvideCreditInfoService.CreditLineResult();
		credResult.sfdcCreditId = '44444';
		credResult.creditId = '55555';
		credResult.result = 'OK';
		credResults.add(credResult);
		ProvideCreditInfoService.ProvideCreditInfo provCredInfo = new ProvideCreditInfoService.ProvideCreditInfo();
		provCredInfo.transactionID = 'CT-TEST';
		provCredInfo.creditLineResults = credResults;

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/providecreditinfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(provCredInfo));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		ProvideCreditInfoService.updateCreditLineItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('FAILED', m.get('status'), 'Incorrect Status');
	}

	@IsTest
	static void testCreditLineNotFound() {
		String creditLineName = prepareTestData();
		List<ProvideCreditInfoService.CreditLineResult> credResults = new List<ProvideCreditInfoService.CreditLineResult>();
		ProvideCreditInfoService.CreditLineResult credResult = new ProvideCreditInfoService.CreditLineResult();
		credResult.sfdcCreditId = creditLineName + 'x';
		credResult.creditId = '55555';
		credResult.result = 'OK';
		credResults.add(credResult);
		ProvideCreditInfoService.ProvideCreditInfo provCredInfo = new ProvideCreditInfoService.ProvideCreditInfo();
		provCredInfo.transactionID = 'CT-TEST';
		provCredInfo.creditLineResults = credResults;

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/providecreditinfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(provCredInfo));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		ProvideCreditInfoService.updateCreditLineItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('FAILED', m.get('status'), 'Incorrect Status');
	}

	@IsTest
	static void testDatabaseError() {
		String creditLineName = prepareTestData();

		String errInfo = 'More_Than_2000_Characters';
		for (Integer i = 0; i < 79; i++) {
			errInfo += 'More_Than_2000_Characters';
		}

		Credit_Line__c cLine = [SELECT Id FROM Credit_Line__c LIMIT 1];
		cLine.Error_Info__c = errInfo;
		update cLine;

		List<ProvideCreditInfoService.CreditLineResult> credResults = new List<ProvideCreditInfoService.CreditLineResult>();
		List<ProvideCreditInfoService.Error> errors = new List<ProvideCreditInfoService.Error>();
		ProvideCreditInfoService.Error err = new ProvideCreditInfoService.Error();
		err.errorCode = 'CODE';
		err.errorMessage = 'ErrorMessage';
		errors.add(err);
		ProvideCreditInfoService.CreditLineResult credResult = new ProvideCreditInfoService.CreditLineResult();
		credResult.sfdcCreditId = creditLineName;
		credResult.creditId = '55555';
		credResult.result = 'FAILED';
		credResult.errors = errors;
		credResults.add(credResult);
		ProvideCreditInfoService.ProvideCreditInfo provCredInfo = new ProvideCreditInfoService.ProvideCreditInfo();
		provCredInfo.transactionID = 'CT-TEST';
		provCredInfo.creditLineResults = credResults;

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/providecreditinfo/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(provCredInfo));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		ProvideCreditInfoService.updateCreditLineItems();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);
		System.assertEquals('FAILED', m.get('status'), 'Incorrect Status');
	}

	private static String prepareTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);

		TestUtils.createCompleteContract();
		Account partAcc = TestUtils.createPartnerAccount();
		insert partAcc;

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'OneMobile';
		ord.OrderType__c = TestUtils.theOrderType.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = TestUtils.theContract.Id;
		ord.O2C_Order__c = true;
		ord.Sales_Order_Id__c = '4444444';
		ord.Account__c = TestUtils.theAccount.Id;
		insert ord;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ;
		cp.VF_Contract__c = TestUtils.theContract.Id;
		cp.Billing_Status__c = 'New';
		cp.Product__c = TestUtils.theProduct.Id;
		cp.ProductCode__c = 'C106929';
		cp.Site__c = TestUtils.theSite.Id;
		cp.Credit__c = true;
		cp.Row_Type__c = 'Price Change';
		cp.Net_Unit_Price__c = 10;
		cp.Quantity__c = 1;
		cp.Start_Invoicing_Date__c = System.today();
		cp.Activation_Date__c = System.today().addDays(3);
		insert cp;

		Map<String, Schema.RecordTypeInfo> cnRecordTypes = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByDeveloperName();
		Credit_Note__c cn = new Credit_Note__c();
		cn.Order__c = ord.Id;
		cn.Credit_Note_Id__c = ord.Name + '-' + String.valueOf(System.today().month());
		cn.Account__c = ord.Account__c;
		cn.Partner__c = partAcc.Id;
		cn.RecordTypeId = cnRecordTypes.get('Credit_Note_Indirect').getRecordTypeId();
		cn.Subscription__c = 'Fixed';
		cn.Credit_Amount__c = 0;
		cn.Credit_Type__c = 'Contractual';
		cn.Transaction_Id__c = 'CT-TEST';
		insert cn;

		Customer_Asset__c ca = [SELECT Id FROM Customer_Asset__c LIMIT 1];

		Credit_Line__c cl = new Credit_Line__c();
		cl.Description__c = 'Test';
		cl.Calculated_Amount__c = 10;
		cl.Customer_Asset__c = ca.Id;
		cl.Credit_Note__c = cn.Id;
		insert cl;

		String transactionId = [SELECT Id, Name FROM Credit_Line__c LIMIT 1].Name;
		return transactionId;
	}
}