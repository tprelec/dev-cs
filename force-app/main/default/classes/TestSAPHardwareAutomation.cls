@isTest
private class TestSAPHardwareAutomation {

    @testSetup
    static void makeData() {
        TestUtils.createSAPHardwareFieldMappings();
        TestUtils.createSync('SAP_Hardware_Information__c -> cspmb__Price_Item__c', 'Item_Description__c', 'Name');
        OrderType__c orderType = TestUtils.createOrderType();
        insert new List<Field_Sync_Mapping__c>{
            new Field_Sync_Mapping__c(
                Object_Mapping__c = 'SAP_Hardware_Information__c -> VF_Product__c',
                Target_Field_Name__c = 'Product_Line__c',
                Target_Default_Value__c = 'fVodafone',
                Target_Default_Value_Type__c = 'STRING'
            ),
            new Field_Sync_Mapping__c(
                Object_Mapping__c = 'SAP_Hardware_Information__c -> VF_Product__c',
                Target_Field_Name__c = 'Quantity_type__c',
                Target_Default_Value__c = 'Each',
                Target_Default_Value_Type__c = 'STRING'
            )
        };
        insert new SAP_Hardware_Information__c(
            Item_Number_Vanilla__c = 'I am vanilla',
            Item_Number_VF__c = 'I am vf',
            Active_Start_Date__c = Date.today().addDays(-10),
            ACtive_End_Date__c = Date.today().addDays(10),
            Item_Description__c = 'Nokia 3310',
            Processed__c = false
        );
    }

    @isTest
    static void executeBatch() {
        OrderType__c orderType = [SELECT Id FROM OrderType__c LIMIT 1];
        SAPHardwareAutomation.orderTypeMobile = orderType.Id;

        Test.startTest();
        database.executebatch(new SAPHardwareAutomation());
        Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM VF_Product__c WHERE ProductCode__c = 'I am vanilla' OR ProductCode__c = 'I am vf'].size());
        System.assertEquals(2, [SELECT Id FROM cspmb__Price_Item__c WHERE cspmb__Price_Item_Code__c = 'I am vanilla' OR cspmb__Price_Item_Code__c = 'I am vf'].size());
        System.assertEquals(true, [SELECT Processed__c FROM SAP_Hardware_Information__c WHERE Item_Description__c = 'Nokia 3310'].Processed__c);
    }
}