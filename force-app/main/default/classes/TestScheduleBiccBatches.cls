@isTest
private class TestScheduleBiccBatches {
	
	@isTest static void test_method_one() {

        //Data preparation for sync table
        TestUtils.autoCommit = false;

        list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
            fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'DUNS_Number__c', 'DUNS_Number__c'));
            fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'COC_number__c', 'KVK_number__c'));
            fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'Unify_Account_Id__c', 'Unify_Ref_Id__c'));
            fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'Phone__c', 'Phone'));

            insert fsmlist;

        // Data preparation for destination records
        TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
        TestUtils.autoCommit = false;			
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c='99';
		insert acc; 
		acc = TestUtils.createAccount(admin);
		acc.DUNS_Number__c='88';
		insert acc; 
		acc = TestUtils.createAccount(admin);
		acc.KVK_number__c='12345678';	
		insert acc; 

        //Data preparation for load table

        list<BICC_Account__c> loadList = new list<BICC_Account__c>{
        		new BICC_Account__c(DUNS_Number__c='88',Phone__c='0653957801',Unify_Account_Id__c='199'),
        		new BICC_Account__c(COC_number__c='12345678',Phone__c='0653957801',Unify_Account_Id__c='299'),
        		new BICC_Account__c(Unify_Account_Id__c='99',Phone__c='0653957801'),
        		new BICC_Account__c(DUNS_Number__c='1',Phone__c='0653957801',Unify_Account_Id__c='399'),
        		new BICC_Account__c(COC_number__c='2',Phone__c='0653957801',Unify_Account_Id__c='499'),
        		new BICC_Account__c(Unify_Account_Id__c='3',Phone__c='0653957801')
         	};

        insert loadList;        

        //The actual Test
		
		Test.StartTest();
		ScheduleBiccBatches ssha = new ScheduleBiccBatches();
		String sch = '0 0 23 * * ?';
		system.schedule('Test Batch BiccBanInformationBatch', sch, ssha);
		Test.stopTest();
	}
}