public class TestScheduleSAPHardwareAutomation{
    
    static testMethod void testbatch() {

        TestUtils.createSAPHardwareFieldMappings();
        OrderType__c ot = TestUtils.createOrderType();

        SAP_Hardware_Information__c shi = new SAP_Hardware_Information__c();
        shi.Name = 'test';
        shi.Item_Number_VF__c = '13371337';
        shi.Item_Number_Vanilla__c = '13371337a';
        shi.Processed__c = false;
        shi.OrderType__c=ot.id;

        insert shi;


        Test.StartTest();
            ScheduleSAPHardwareAutomation ssha = new ScheduleSAPHardwareAutomation();
            SAPHardwareAutomation.isTest = true;
            String sch = '0 0 23 * * ?';
            system.schedule('Test Batch SAPHardwareAutomation', sch, ssha);
        Test.stopTest();        
    }
}