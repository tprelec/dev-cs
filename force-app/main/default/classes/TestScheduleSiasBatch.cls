/**
 * Test class for ScheduleSiasBatch.cls
 *
 * @author: Rahul Sharma
 */
@IsTest
public class TestScheduleSiasBatch {
	@IsTest
	private static void validateSendOrdersToSiasStatusOk() {
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		Order__c order = createOrderWithContractedProducts(
			2,
			Constants.PRODUCT2_BILLINGTYPE_STANDARD
		);

		// change the status run once flag as trigger may already ran
		TriggerHandler.resetAlreadyModified();
		TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

		List<Contracted_Products__c> cps = [
			SELECT Id
			FROM Contracted_Products__c
			WHERE Order__c = :order.Id
		];
		for (Contracted_Products__c cp : cps) {
			cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED;
			cp.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
			cp.ProductCode__c = 'C106929';
		}
		update cps;

		External_WebService_Config__c config = new External_WebService_Config__c(
			Name = CreateBSSOrderRest.INTEGRATION_SETTING_NAME,
			URL__c = CreateBSSOrderRest.MOCK_URL,
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;
		System.assertNotEquals(null, config.Id, 'Config Id is NULL');

		setMock(config.URL__c, CreateBSSOrderRest.STATUS_OK, 200);

		Test.startTest();

		new ScheduleSiasBatch().execute(null);

		Test.stopTest();

		for (Contracted_Products__c cp : [
			SELECT Id, Billing_Status__c, Number_of_Failed_Requests__c
			FROM Contracted_Products__c
			WHERE Order__c = :order.Id
		]) {
			System.assertEquals(
				CreateBSSOrderRest.BILLING_STATUS_REQUESTED,
				cp.Billing_Status__c,
				'Billing Status should be Requested'
			);
			System.assertEquals(0, cp.Number_of_Failed_Requests__c, 'Some requests failed');
		}

		System.assertEquals(
			1,
			[SELECT COUNT() FROM Order_Billing_Transaction__c],
			'No Billing Transaction found'
		);
	}

	@IsTest
	private static void validateSendOrdersToSiasSpecial() {
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		Order__c order = createOrderWithContractedProducts(
			2,
			Constants.PRODUCT2_BILLINGTYPE_SPECIAL
		);

		// change the status run once flag as trigger may already ran
		TriggerHandler.resetAlreadyModified();
		TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

		List<Contracted_Products__c> cps = [
			SELECT Id
			FROM Contracted_Products__c
			WHERE Order__c = :order.Id
		];
		for (Contracted_Products__c cp : cps) {
			cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED;
			cp.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
			cp.ProductCode__c = 'C106929';
		}
		update cps;

		External_WebService_Config__c config = new External_WebService_Config__c(
			Name = CreateBSSOrderRest.INTEGRATION_SETTING_NAME,
			URL__c = CreateBSSOrderRest.MOCK_URL,
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;
		System.assertNotEquals(null, config.Id, 'Config Id is NULL');

		setMock(config.URL__c, CreateBSSOrderRest.STATUS_OK, 200);

		Test.startTest();

		new ScheduleSiasBatch().execute(null);

		Test.stopTest();

		for (Contracted_Products__c cp : [
			SELECT Id, Billing_Status__c, Number_of_Failed_Requests__c
			FROM Contracted_Products__c
			WHERE Order__c = :order.Id
		]) {
			System.assertEquals(
				Constants.CUSTOMER_ASSET_BILLING_STATUS_SPECIAL_BILLING,
				cp.Billing_Status__c,
				'Billing Status should be Special Billing'
			);
			System.assertEquals(0, cp.Number_of_Failed_Requests__c, 'Some requests failed');
		}

		System.assertEquals(
			0,
			[SELECT COUNT() FROM Order_Billing_Transaction__c],
			'Billing Transaction was not expected'
		);
	}

	@IsTest
	private static void validateSendOrdersToSiasStatusCode400() {
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();

		Order__c order = createOrderWithContractedProducts(
			2,
			Constants.PRODUCT2_BILLINGTYPE_STANDARD
		);

		// change the status run once flag as trigger may already ran
		TriggerHandler.resetAlreadyModified();
		TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

		List<Contracted_Products__c> cps = [
			SELECT Id
			FROM Contracted_Products__c
			WHERE Order__c = :order.Id
		];
		for (Contracted_Products__c cp : cps) {
			cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED;
			cp.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
			cp.Number_of_Failed_Requests__c = CreateBSSOrderRest.MAX_NR_OF_FAILED_REQUESTS - 1;
			cp.ProductCode__c = 'C106929';
		}
		update cps;

		External_WebService_Config__c config = new External_WebService_Config__c(
			Name = CreateBSSOrderRest.INTEGRATION_SETTING_NAME,
			URL__c = CreateBSSOrderRest.MOCK_URL,
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;
		System.assertNotEquals(null, config.Id, 'Config Id is NULL');

		setMock(config.URL__c, CreateBSSOrderRest.STATUS_OK, 400);

		Test.startTest();

		new ScheduleSiasBatch().execute(null);

		Test.stopTest();

		for (Contracted_Products__c cp : [
			SELECT Id, Billing_Status__c, Number_of_Failed_Requests__c
			FROM Contracted_Products__c
			WHERE Order__c = :order.Id
		]) {
			System.assertEquals(
				CreateBSSOrderRest.BILLING_STATUS_REQUEST_FAILED,
				cp.Billing_Status__c,
				'Billing Status should be Failed'
			);
			System.assertEquals(
				CreateBSSOrderRest.MAX_NR_OF_FAILED_REQUESTS,
				cp.Number_of_Failed_Requests__c,
				'Number of Failed Requests is incorrect'
			);
		}

		System.assertEquals(
			1,
			[SELECT COUNT() FROM Order_Billing_Transaction__c],
			'No Billing Transaction found'
		);
	}

	@IsTest
	private static void validateCallScheduler() {
		Order__c order = createOrderWithContractedProducts(
			2,
			Constants.PRODUCT2_BILLINGTYPE_STANDARD
		);

		External_WebService_Config__c config = new External_WebService_Config__c(
			Name = CreateBSSOrderRest.INTEGRATION_SETTING_NAME,
			URL__c = CreateBSSOrderRest.MOCK_URL,
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;
		System.assertNotEquals(null, config.Id, 'Config Id is NULL');

		setMock(config.URL__c, CreateBSSOrderRest.STATUS_OK, 200);

		Test.startTest();

		ScheduleSiasBatch.abort();
		ScheduleSiasBatch.scheduleInFiveMinutes();
		ScheduleSiasBatch.abort();

		Test.stopTest();

		for (Contracted_Products__c cp : [
			SELECT Id, Billing_Status__c
			FROM Contracted_Products__c
			WHERE Order__c = :order.Id
		]) {
			System.assertEquals(
				Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW,
				cp.Billing_Status__c,
				'Billing Status should be New'
			);
		}

		System.assertEquals(
			0,
			[SELECT COUNT() FROM Order_Billing_Transaction__c],
			'Billing Transaction was not expected'
		);
	}

	@IsTest
	private static void validateBatchFinish() {
		Test.startTest();

		Database.executeBatch(new SiasBatch(new Set<Id>()), 50);

		Test.stopTest();

		List<AsyncApexJob> jobs = [SELECT Id FROM AsyncApexJob WHERE JobType = 'ScheduledApex'];
		System.assertNotEquals(0, jobs.size(), 'Expecting a scheduled job');
	}

	static void setMock(String url, String statusCode, Integer responseCode) {
		Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
		endpoint2TestResp.put(
			url,
			new TestUtilMultiRequestMock.SingleRequestMock(
				responseCode,
				'Complete',
				'{"status": "' +
				statusCode +
				'"}',
				null
			)
		);
		HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);

		Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);
	}

	static Order__c createOrderWithContractedProducts(
		Integer totalContractedProducts,
		String billingType
	) {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();

		acct.GT_Fixed__c = true;
		update acct;

		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct(
			new Map<String, Object>{ 'Billing_Type__c' => billingType }
		);
		insert product;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			Account__c = acct.Id
		);
		insert order;

		List<Contracted_Products__c> contractedProducts = new List<Contracted_Products__c>();
		Map<Integer, String> rowTypeMap = new Map<Integer, String>{
			0 => Constants.CONTRACTED_PRODUCT_ROW_TYPE_ADD,
			1 => Constants.CONTRACTED_PRODUCT_ROW_TYPE_REMOVE,
			2 => Constants.CONTRACTED_PRODUCT_ROW_TYPE_PRICE_CHANGE
		};
		for (Integer count = 0; count < totalContractedProducts; count++) {
			contractedProducts.add(
				new Contracted_Products__c(
					Quantity__c = 1,
					Arpu_Value__c = 100,
					Duration__c = 1,
					Product__c = product.Id,
					VF_Contract__c = contr.Id,
					Site__c = site.Id,
					Order__c = order.Id,
					CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ,
					Gross_List_Price__c = 50,
					ProductCode__c = 'C106929',
					Start_Invoicing_Date__c = System.today().toStartOfMonth(),
					Row_Type__c = rowTypeMap.get(count),
					Billing__c = true
				)
			);
		}
		Customer_Asset__c custAsset = new Customer_Asset__c(
			Quantity__c = 2,
			Price__c = 60,
			PO_Number__c = 'Test123'
		);
		insert custAsset;
		contractedProducts[0].OC_Charge__c = custAsset.Id;
		insert contractedProducts;

		return order;
	}
}