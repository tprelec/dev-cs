@IsTest
public with sharing class TestSendOrderConfirmationCtrl {
	@TestSetup
	static void makeData() {
		TestUtils.autoCommit = false;
		User u = TestUtils.createAdministrator();
		u.Email = 'ziggotestuser@vodafone.com';
		u.LG_SalesChannel__c = 'D2D';
		insert u;

		System.runAs(u) {
			TestUtils.createMavenDocumentsRecords();
			TestUtils.autoCommit = true;
			Account acc = TestUtils.createAccount(u);
			TestUtils.autoCommit = false;
			Contact con = TestUtils.createContact(acc);
			con.Email = 'test@test.com';
			insert con;
			Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
			opp.RecordTypeId = GeneralUtils.getRecordTypeIdByName('Opportunity', 'Ziggo');
			opp.OwnerId = u.Id;
			insert opp;
			OpportunityContactRole ocr = new OpportunityContactRole(
				OpportunityId = opp.Id,
				ContactId = con.Id,
				Role = 'Administrative Contact',
				IsPrimary = true
			);
			insert ocr;
			opp.LG_PrimaryContact__c = con.Id;
			update opp;
		}
	}

	@IsTest
	private static void testValidateOpportunity() {
		Opportunity opp = getOpportunity();
		String errorMsg;
		Test.startTest();
		try {
			SendOrderConfirmationCtrl.validateOpportunity(opp.Id);
		} catch (Exception ex) {
			errorMsg = ex.getMessage();
		}
		Test.stopTest();
		System.assert(errorMsg != null, 'Validation should be triggered.');
	}

	@IsTest
	private static void testGetAvailableOptions() {
		Opportunity opp = getOpportunity();
		Test.startTest();
		List<AuraSelectOption> options1 = SendOrderConfirmationCtrl.getAvailableOptions(opp.Id);
		opp.LG_SignedQuoteAvailable__c = 'true';
		update opp;
		List<AuraSelectOption> options2 = SendOrderConfirmationCtrl.getAvailableOptions(opp.Id);
		Test.stopTest();
		System.assertEquals(1, options1.size(), 'New confirmation should be available.');
		System.assertEquals(2, options2.size(), 'Signed and new confirmation should be available.');
	}

	@IsTest
	private static void testCreateNewConfirmation() {
		Opportunity opp = getOpportunity();
		Test.startTest();
		mmdoc__Document_Request__c docRequest = SendOrderConfirmationCtrl.createNewConfirmation(
			opp.Id
		);
		docRequest = SendOrderConfirmationCtrl.getDocumentRequest(docRequest.Id);
		Attachment att = new Attachment(
			Name = 'Test Attachment',
			body = Blob.valueOf('this is a test'),
			ContentType = 'pdf',
			IsPrivate = false,
			ParentId = opp.Id
		);
		insert att;
		SendOrderConfirmationCtrl.sendNewConfirmation(opp.Id, att.Id);
		Test.stopTest();
		System.assert(docRequest != null, 'Document Request should be created');
	}

	@IsTest
	private static void testSendSignedConfirmation() {
		Opportunity opp = getOpportunity();
		Attachment att = new Attachment(
			Name = 'Test Attachment',
			body = Blob.valueOf('this is a test'),
			ContentType = 'pdf',
			IsPrivate = false,
			ParentId = opp.Id
		);
		insert att;
		opp.LG_SignedQuoteAttachmentId__c = att.Id;
		update opp;
		Test.startTest();
		SendOrderConfirmationCtrl.sendSignedConfirmation(opp.Id);
		Test.stopTest();
		if (EmailService.checkDeliverability()) {
			List<EmailMessage> emails = [SELECT Id FROM EmailMessage];
			System.assert(!emails.isEmpty(), 'Email should be sent');
		}
	}

	private static Opportunity getOpportunity() {
		return [SELECT Id FROM Opportunity LIMIT 1];
	}
}