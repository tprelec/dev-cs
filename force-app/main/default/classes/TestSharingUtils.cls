@isTest
private class TestSharingUtils {

    @testSetup
    static void testSetup() {
        Account acc = TestUtils.createPartnerAccount();
        User u = TestUtils.createPortalUser(acc);
        Contact con = TestUtils.portalContact(acc);
        con.Userid__c = u.Id;
        con.NPS_contact__c = 'Yes';
        update con;
        Dealer_Information__c di = TestUtils.createDealerInformation(con.Id);
        acc.Mobile_Dealer__c = di.Id;
        update acc;
        Opportunity opp = TestUtils.createOpportunity(acc, null);
        VF_Contract__c contract = TestUtils.createVFContract(acc, opp);
    }

    @isTest
    static void rollUpNpsContacts() {
        Account acc = [SELECT Id, Total_NPS_Contacts__c FROM Account LIMIT 1];
        Contact con = [SELECT Id, AccountId FROM Contact WHERE AccountId = :acc.Id LIMIT 1];

        Test.startTest();
        acc.Total_NPS_Contacts__c = 100;
        update acc;

        SharingUtils.rollUpNpsContacts(new List<Contact>{con}, false);
        System.assertEquals(1, [SELECT Id, Total_NPS_Contacts__c FROM Account WHERE Id = :acc.Id].Total_NPS_Contacts__c);
        SharingUtils.rollUpNpsContacts(new List<Contact>{con}, true);
        System.assertEquals(1, [SELECT Id, Total_NPS_Contacts__c FROM Account WHERE Id = :acc.Id].Total_NPS_Contacts__c);
        Test.stopTest();
    }

    @isTest
    static void createSharingForMobileDealerNonFutureAndFuture() {
        Account acc = [SELECT Id, Mobile_Dealer__c FROM Account LIMIT 1];
        Dealer_Information__c di = [SELECT Id, Name FROM Dealer_Information__c LIMIT 1];

        Test.startTest();
        User u = [SELECT Id FROM User WHERE LastName = 'PortalUser' LIMIT 1];
        SharingUtils.createSharingForMobileDealerNonFuture(
            new Set<Id>{acc.Id},
            new Map<Id, Id>{acc.Mobile_Dealer__c => acc.Id},
            new Map<Id, Id>{acc.Id => acc.Mobile_Dealer__c},
            false
        );
        System.assertEquals(1, [SELECT Id, UserOrGroupId FROM OpportunityShare WHERE UserOrGroupId = :u.Id].size());

        SharingUtils.createSharingForMobileDealer(
            new Set<Id>{acc.Id},
            new Map<Id, Id>{acc.Mobile_Dealer__c => acc.Id},
            new Map<Id, Id>{acc.Id => acc.Mobile_Dealer__c},
            false
        );
        Test.stopTest();
    }

    //ToDo: Assertions are still needed for this method
    @isTest
    static void createAccountSharing() {
        Account acc = [SELECT Id, OwnerId FROM Account LIMIT 1];

        Test.startTest();
        SharingUtils.createAccountSharing(
            new Map<Id, Id>{acc.Id => acc.OwnerId},
            new Set<Id>{acc.OwnerId},
            null
        );
        Test.stopTest();
    }

    @isTest
    static void dealerShareHelper() {
        Dealer_Information__c di = [SELECT Id, Name FROM Dealer_Information__c LIMIT 1];
        SharingUtils instance = new SharingUtils();

        Test.startTest();
        instance.dealerShareHelper(new Set<Id>{di.Id});
        System.assert(instance.dealerToGroupMap.containsKey(di.Id));
        Test.stopTest();
    }

    @isTest
    static void innerClassWithoutSharingMethods() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Contact con = new Contact(LastName = 'TestSharingUtils', AccountId = acc.Id);

        Test.startTest();
        new SharingUtils.WithoutSharingmethods().insertSharingRules(new List<SObject>{con});
        System.assertEquals(1, [SELECT Id FROM Contact WHERE AccountId = :acc.Id AND Name = 'TestSharingUtils'].size());

        con.LastName = 'I\'m updated!';
        new SharingUtils.WithoutSharingmethods().upsertObjects(new List<SObject>{con});
        System.assertEquals(1, [SELECT Name FROM Contact WHERE AccountId = :acc.Id AND Name = 'I\'m updated!'].size());

        new SharingUtils.WithoutSharingmethods().deleteObjects(new List<SObject>{con});
        System.assert([SELECT Id FROM Contact WHERE AccountId = :acc.Id AND Name = 'I\'m updated!'].isEmpty());
        Test.stopTest();
    }

    @isTest
    static void staticMethods() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Contact con = new Contact(LastName = 'TestSharingUtils', AccountId = acc.Id);

        Test.startTest();
        Contact con2 = new Contact(LastName = 'TestSharingUtils2', AccountId = acc.Id);
        insert con2;

        SharingUtils.insertSharingRulesWithoutSharingStatic(new List<SObject>{con});
        System.assertEquals(1, [SELECT Id FROM Contact WHERE AccountId = :acc.Id AND Name = 'TestSharingUtils'].size());

        con.LastName = 'I\'m updated!';
        SharingUtils.upsertRecordsWithoutSharing(con);
        System.assertEquals(1, [SELECT Name FROM Contact WHERE AccountId = :acc.Id AND Name = 'I\'m updated!'].size());
        con.LastName = 'I\'m updated again!';
        SharingUtils.upsertRecordsWithoutSharing(new List<SObject>{con});
        System.assertEquals(1, [SELECT Name FROM Contact WHERE AccountId = :acc.Id AND Name = 'I\'m updated again!'].size());

        SharingUtils.deleteRecordsWithoutSharing(new List<SObject>{con});
        System.assert([SELECT Id FROM Contact WHERE AccountId = :acc.Id AND Name = 'I\'m updated again!'].isEmpty());
        SharingUtils.deleteRecordsWithoutSharing(con2);
        System.assert([SELECT Id FROM Contact WHERE Id = :Con2.Id].isEmpty());
        Test.stopTest();
    }
}