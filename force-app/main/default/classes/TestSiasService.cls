@isTest
public class TestSiasService {
    final static private String MOCK_URL = 'http://example.com/example/test';
    final static private String TRANSACTION_ID = '5823764289463';

    @TestSetup
    static void makeData() {
        setConfig();

        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Site__c site = TestUtils.createSite(acct);
        Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, null);
        VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
        OrderType__c ot = TestUtils.createOrderType();
        Contact c = new Contact(AccountId = acct.Id, LastName = 'Test');
        Financial_Account__c fa = TestUtils.createFinancialAccount(ban, c.Id);
        Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);

        TestUtils.autoCommit = false;
        Product2 product = TestUtils.createProduct();
        insert product;

        Order__c ord = new Order__c();
        ord.Status__c = 'New';
        ord.Propositions__c = 'Legacy';
        ord.OrderType__c = ot.Id;
        ord.Number_of_items__c = 100;
        ord.BOP_Order_Status__c = 'In Progress';
        ord.PM_Email__c = 'test@test.com';
        ord.PM_First_Name__c = 'Test';
        ord.PM_Last_Name__c = 'von Test';
        ord.PM_Phone__c = '0031612345678';
        ord.VF_Contract__c = contr.Id;
        insert ord;

        Customer_Asset__c ca = new Customer_Asset__c();
        ca.Billing_Arrangement__c = ba.Id;
        ca.Order__c = ord.Id;
        ca.Site__c = site.Id;
        insert ca;

        Competitor_Asset__c compAsset = new Competitor_Asset__c();
        compAsset.Assigned_Product_Id__c = '23468934345';
        insert compAsset;

        Contracted_Products__c cp = new Contracted_Products__c();
        cp.Order__c = ord.Id;
        cp.External_Reference_Id__c = '7777777';
        cp.CLC__c = 'Acq'; // Required and fixed for BOP
        cp.VF_Contract__c = contr.Id;
        cp.Customer_Asset__c = ca.Id;
        cp.PBX__c = compAsset.Id;
        cp.Product__c = product.Id;
        insert cp;
    }

    @IsTest
    private static void validateSuccessResponse() {
        Contracted_Products__c cp = getContractedProduct();

        setPositiveMock();

        Test.startTest();
        SiasService.changeOrderPBX(TRANSACTION_ID, cp.Id);
        Test.stopTest();

        Contracted_Products__c cpFetch = getContractedProduct();
        Competitor_Asset__c caFetch = getCompetitorAsset();

        System.assertNotEquals(
            null,
            cpFetch.Billing_Offer_synced_to_Unify_timestamp__c,
            'Timestamp is required'
        );
        System.assertNotEquals(
            null,
            caFetch.Billing_Offer_synced_to_Unify_timestamp__c,
            'Timestamp is required'
        );
        System.assertEquals(true, caFetch.PBX_Trunking_Done__c, 'Trunking must be true');
    }

    @IsTest
    private static void validateLineErrorResponse() {
        Contracted_Products__c cp = getContractedProduct();

        setNegativeLineMock();

        Test.startTest();
        SiasService.changeOrderPBX(TRANSACTION_ID, cp.Id);
        Test.stopTest();

        Contracted_Products__c cpFetch = getContractedProduct();

        System.assertNotEquals(null, cpFetch.PBX_Change_Error_Info__c, 'Error message is required');
    }

    @IsTest
    private static void validateErrorResponse() {
        Contracted_Products__c cp = getContractedProduct();

        setNegativeMock();

        Test.startTest();
        SiasService.changeOrderPBX(TRANSACTION_ID, cp.Id);
        Test.stopTest();

        Contracted_Products__c cpFetch = getContractedProduct();

        System.assertNotEquals(null, cpFetch.PBX_Change_Error_Info__c, 'Error message is required');
    }

    static void setPositiveMock() {
        Contracted_Products__c cp = getContractedProduct();
        Competitor_Asset__c ca = getCompetitorAsset();

        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
        endpoint2TestResp.put(
            MOCK_URL,
            new TestUtilMultiRequestMock.SingleRequestMock(
                200,
                'Complete',
                '{"changePBXOrderResponse": {"orderLinesList": [{"APID": "' +
                ca.Assigned_Product_Id__c +
                '","externalReferenceID": "' +
                cp.PBX__c +
                '"}]}}',
                null
            )
        );
        HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);
    }

    static void setNegativeLineMock() {
        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
        endpoint2TestResp.put(
            MOCK_URL,
            new TestUtilMultiRequestMock.SingleRequestMock(
                200,
                'Complete',
                '{"changePBXOrderResponse": {"orderLinesList": [{"errorInfo": {"errorCode": "SIASERR-1500", "errorDescription": "Error received from OMS CreateAndSubmit WS", "targetSystemName": "OMS", "targetServiceName": "CreateAndSubmit"}}]}}',
                null
            )
        );
        HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);

        Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);
    }

    static void setNegativeMock() {
        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
        endpoint2TestResp.put(
            MOCK_URL,
            new TestUtilMultiRequestMock.SingleRequestMock(
                204,
                'Complete',
                '{"errorInfo": {"errorCode": "SIASERR-1500", "errorDescription": "Error received from OMS CreateAndSubmit WS", "targetSystemName": "OMS", "targetServiceName": "CreateAndSubmit"}}',
                null
            )
        );
        HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);

        Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);
    }

    private static Contracted_Products__c getContractedProduct() {
        return [
            SELECT Id, Billing_Offer_synced_to_Unify_timestamp__c, PBX_Change_Error_Info__c, PBX__c
            FROM Contracted_Products__c
            LIMIT 1
        ];
    }

    private static Competitor_Asset__c getCompetitorAsset() {
        return [
            SELECT
                Id,
                PBX_Trunking_Done__c,
                Billing_Offer_synced_to_Unify_timestamp__c,
                Assigned_Product_Id__c
            FROM Competitor_Asset__c
            LIMIT 1
        ];
    }

    private static void setConfig() {
        External_WebService_Config__c config = new External_WebService_Config__c(
            Name = SiasService.INTEGRATION_SETTING_NAME,
            URL__c = MOCK_URL,
            Username__c = 'username',
            Password__c = 'password'
        );
        insert config;
    }
}