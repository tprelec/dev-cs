@isTest
private class TestSiteCheckEnrichDataController {
	
	@isTest static void test_method_one() {
		// create account + site
		Account a = TestUtils.createAccount(null);
		Site__c s = TestUtils.createSite(a);
		s.Last_fiber_check__c = system.now();
		update s;

		// create some existing (non-active) checks
		Site_Postal_Check__c spc = new Site_Postal_Check__c();
		spc.Access_Vendor__c = 'VF-FIBER';
		spc.Accessclass__c = 'Fiber';
		spc.Technology_Type__c = 'Fiber';
		spc.Access_Site_ID__c = s.Id;
		spc.Existing_Infra__c = false;
		insert spc;

		Site_Postal_Check__c spc2 = new Site_Postal_Check__c();
		spc2.Access_Vendor__c = 'EF-FIBER';
		spc2.Accessclass__c = 'Fiber';
		spc2.Technology_Type__c = 'Fiber';
		spc2.Access_Site_ID__c = s.Id;
		spc2.Existing_Infra__c = false;
		insert spc2;

		// add an existing feeditem
        FeedItem post = new FeedItem();
        post.ParentId = spc2.Id;
        post.Body = 'testfile.txt';
        post.ContentData = Blob.valueOf('testfileContent');
        post.ContentFileName = 'testfile.txt';
        post.Visibility = 'InternalUsers';
        insert post;  		

		// go to screen
		PageReference pr = new PageReference('/apex/SiteCheckEnrichData?id='+s.Id);
		Test.setCurrentPage(pr);

		// open controller
   		Apexpages.StandardController sc = new Apexpages.standardController(s);
		SiteCheckEnrichDataController seic = new SiteCheckEnrichDataController(sc);

		// simulate the user entering data onto the first line
		seic.activeChecks[0].dummyTask.Dummy_Number__c = 100;
		seic.activeChecks[0].dummyTask.Dummy_Date__c = system.today().addMonths(1);
		seic.activeChecks[0].f.ContentData = Blob.valueOf('test');
		seic.activeChecks[0].f.ContentFileName = 'test.txt';

		seic.saveUpdates();

		// check that there is only 1 active checks now
		List<Site_Postal_Check__c> checks = [Select Id From Site_Postal_Check__c Where Access_Active__c = true AND Access_Site_ID__c = :s.Id];
		System.AssertEquals(1,checks.size());

		seic.backToSite();
	}
	
}