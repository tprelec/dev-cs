@isTest
private class TestSiteExistingInfraController {
	
	@isTest static void test_method_one() {
		// create account + site
		Account a = TestUtils.createAccount(null);
		Site__c s = TestUtils.createSite(a);

		// go to screen
		PageReference pr = new PageReference('/apex/SiteExistingInfra?id='+s.Id+'&retURL=/apex/SiteManager?id=' + a.Id);
		Test.setCurrentPage(pr);

		// open controller
   		Apexpages.StandardController sc = new Apexpages.standardController(s);
		SiteExistingInfraController seic = new SiteExistingInfraController(sc);
	
        seic.existingInfra[0].TotalMB = 100;
        
		// add a row
		seic.addInfra();
		seic.existingInfra[1].TotalMB = 100;
		seic.existingInfra[1].Vendor = seic.getVendorPickList().get(1).getValue();
		seic.saveUpdates();

		// check if the row is really there
		List<Site_Postal_Check__c> checks = [Select Id From Site_Postal_Check__c Where Access_Site_ID__c = :s.Id];
		System.AssertEquals(2,checks.size());

		// remove the row again
		seic.numberOfRowToRemove = 0;
		seic.removeInfra();

		// and some other methods..
		seic.getVendorPickList();
		seic.backToSite();

		// lastly, check if the check is really removed
		List<Site_Postal_Check__c> checks2 = [Select Id From Site_Postal_Check__c Where Access_Site_ID__c = :s.Id];
		System.AssertEquals(1,checks2.size());

	}
	
	@isTest static void test_method_two() {

		Account a = TestUtils.createAccount(null);
		Site__c s1 = TestUtils.createSite(a);
		Site__c s2 = TestUtils.createSite(a);
		
		PageReference pr = new PageReference('/apex/SiteExistingInfra?ids='+s1.Id+','+s2.Id+'&retURL=/apex/SiteManager?id=' + a.Id);
		Test.setCurrentPage(pr);
   		Apexpages.StandardController sc = new Apexpages.standardController(s1);
		SiteExistingInfraController seic = new SiteExistingInfraController(sc);

		seic.saveUpdates();
		List<Site_Postal_Check__c> checks = [Select Id From Site_Postal_Check__c Where Access_Site_ID__c = :s1.Id OR Access_Site_ID__c = :s2.Id];
		System.AssertEquals(2, checks.size());
        
        for (SiteExistingInfraController.spcWrapper spc : seic.existingInfra) {
			spc.selected = true;
		}
		seic.addInfras();
        for (SiteExistingInfraController.spcWrapper spc : seic.existingInfra) {
            if (spc.theSPC.Id == null) {
                spc.TotalMB = 100;
                spc.Vendor = seic.getVendorPickList().get(1).getValue();
            }
        	System.debug(spc);
		}
		seic.saveUpdates();

		checks = [Select Id From Site_Postal_Check__c Where Access_Site_ID__c = :s1.Id OR Access_Site_ID__c = :s2.Id];
		System.AssertEquals(4, checks.size());

		for (SiteExistingInfraController.spcWrapper spc : seic.existingInfra) {
			spc.selected = true;
		}
		seic.removeInfras();

		checks = [Select Id From Site_Postal_Check__c Where Access_Site_ID__c = :s1.Id OR Access_Site_ID__c = :s2.Id];
		System.AssertEquals(0, checks.size());
	}

}