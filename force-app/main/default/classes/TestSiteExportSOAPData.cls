/**
 * @description		This is the test class for ECSSOAPLocation class. Because of a bug in mockservices, we have to cover this specifically
 * @author        	Guy Clairbois
 */
@isTest
private class TestSiteExportSOAPData {
	static testMethod void dataTest() {
		ECSSOAPLocation l = new ECSSOAPLocation();
		ECSSOAPLocation.authenticationHeader_element ah = new ECSSOAPLocation.authenticationHeader_element();
		ECSSOAPLocation.companyRefType crt = new ECSSOAPLocation.companyRefType();
		ECSSOAPLocation.createLocationsRequest_element clre = new ECSSOAPLocation.createLocationsRequest_element();
		ECSSOAPLocation.locationBaseType lbt = new ECSSOAPLocation.locationBaseType();
		ECSSOAPLocation.locationCreateType lct = new ECSSOAPLocation.locationCreateType();
		ECSSOAPLocation.locationResponseType lrt = new ECSSOAPLocation.locationResponseType();
		ECSSOAPLocation.locationSoap ls = new ECSSOAPLocation.locationSoap();
		ECSSOAPLocation.locationsResponse_element lre = new ECSSOAPLocation.locationsResponse_element();
		ECSSOAPLocation.locationUpdateType lut = new ECSSOAPLocation.locationUpdateType();
		ECSSOAPLocation.updateLocationsRequest_element ulre = new ECSSOAPLocation.updateLocationsRequest_element();
	}

	@isTest
	public static void testCreate() {
		ECSSOAPLocation.locationSoap soapClass = new ECSSOAPLocation.locationSoap();
		List<ECSSOAPLocation.locationResponseType> result = new List<ECSSOAPLocation.locationResponseType>();

		Test.setMock(WebServiceMock.class, new ECSSOAPLocationMocks.LocationResponse());

		Test.startTest();
		result = soapClass.createLocations(new List<ECSSOAPLocation.locationCreateType>());
		Test.stopTest();

		System.assert(result[0].referenceId == 'Test Completed');
	}

	@isTest
	public static void testUpdate() {
		ECSSOAPLocation.locationSoap soapClass = new ECSSOAPLocation.locationSoap();
		List<ECSSOAPLocation.locationResponseType> result = new List<ECSSOAPLocation.locationResponseType>();

		Test.setMock(WebServiceMock.class, new ECSSOAPLocationMocks.LocationResponse());

		Test.startTest();
		result = soapClass.updateLocations(new List<ECSSOAPLocation.locationUpdateType>());
		Test.stopTest();

		System.assert(result[0].referenceId == 'Test Completed');
	}
}