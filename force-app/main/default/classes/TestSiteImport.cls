/**
 * @description		This is the test class for SiteImport
 * @author        	Guy Clairbois
 */
@isTest
private class TestSiteImport {

    static testMethod void TestImportWithoutAccountId(){
		PageReference importPage = page.SiteImport;
		Test.setCurrentPage(importPage);
		SiteImport si = new SiteImport();
    }
    	
	static testMethod void TestImportWithEmptyCSV(){    	
		Account a = TestUtils.createAccount(null);
		
		PageReference importPage = page.SiteImport;
		importPage.getParameters().put('accountId',a.Id);
		Test.setCurrentPage(importPage);
		SiteImport si = new SiteImport();
		
		si.csvName = 'testcsv';
		String blobCreator = 'SITE NAME,PHONE,STREET,HOUSENUMBER,HOUSENUMBERSUFFIX,POSTALCODE,CITY' + '\r\n'; 
        
		si.csvBody = blob.valueOf(blobCreator);
		si.handleCSV();
    }

	static testMethod void TestImportWithProperCSV(){    	
		Account a = TestUtils.createAccount(null);
		
		PageReference importPage = page.SiteImport;
		importPage.getParameters().put('accountId',a.Id);
		Test.setCurrentPage(importPage);
		SiteImport si = new SiteImport();
		
		si.csvName = 'testcsv';
		String blobCreator = 'SITE NAME,PHONE,STREET,HOUSENUMBER,HOUSENUMBERSUFFIX,POSTALCODE,CITY' + '\r\n' 
								+ '1,1,1,1,1,1234AB,1';        
        
		si.csvBody = blob.valueOf(blobCreator);
		si.handleCSV();
		si.cancel();
    }
    
    static testMethod void TestRemainingMethods(){
   		SiteImportCSVParser parser = new SiteImportCSVParser();
   		SiteImportCSVParser.CSVRow row = new SiteImportCSVParser.CSVRow();
   		row.setupColumnPropertyMappings(); 
   		parser.generateCSV();
    }

}