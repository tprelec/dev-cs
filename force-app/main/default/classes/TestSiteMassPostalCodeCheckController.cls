@isTest
private class TestSiteMassPostalCodeCheckController {
	
	@isTest static void test_method_one() {
		TestUtils.createSalesSettings();

		// create account + site
		Account a = TestUtils.createAccount(null);
		Site__c s = TestUtils.createSite(a);
		s.Last_fiber_check__c = system.now();
		update s;

		// create some existing (non-active) checks
		Site_Postal_Check__c spc = new Site_Postal_Check__c();
		spc.Access_Vendor__c = 'VF-FIBER';
		spc.Accessclass__c = 'Fiber';
		spc.Technology_Type__c = 'Fiber';
		spc.Access_Site_ID__c = s.Id;
		spc.Existing_Infra__c = false;
		insert spc;

		Site_Postal_Check__c spc2 = new Site_Postal_Check__c();
		spc2.Access_Vendor__c = 'EF-FIBER';
		spc2.Accessclass__c = 'Fiber';
		spc2.Technology_Type__c = 'Fiber';
		spc2.Access_Site_ID__c = s.Id;
		spc2.Existing_Infra__c = false;
		insert spc2;

		Test.startTest();		

		// go to screen
		PageReference pr = new PageReference('/apex/SiteMassPostalCheck?id='+a.Id);
		Test.setCurrentPage(pr);

		// open controller
   		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new List<Site__c> {s});
		SiteMassPostalCodeCheckController smpcc = new SiteMassPostalCodeCheckController(sc);

		// simulate the user triggering the checks
		smpcc.siteList[0].selected = true;
		smpcc.scheduleDsl();
		smpcc.scheduleFiber();
		smpcc.scheduleBoth();

		// trigger the schedule
		Test.stopTest();

		// check that there is only 1 active checks now
		//List<Site_Postal_Check__c> checks = [Select Id From Site_Postal_Check__c Where Access_Active__c = true AND Access_Site_ID__c = :s.Id];
		//System.AssertEquals(1,checks.size());

		smpcc.backToAccount();
	}
	
}