@isTest
private class TestSlaSelectorController {

	// create a site and all objects needed for that
	static User owner;
	static Account acct;
	static Site__c site;
	static Sla__c sla;

	static String slaFieldValueObjectFilter = 'ContractedProducts';
	static String slaFieldValueProactiveFilter = 'Proactive';
	static String slaFieldValueReactionTimeFilter = '30';
	static String slaFieldValueRepairTimeFilter = '8 hours';
	static String slaFieldValueReplaceByFilter = 'Vodafone';
	static String slaFieldValueSlaTypeFilter = 'W1';

	
	static void initBasicInfo(){

		// create a site and all objects needed for that
		owner = TestUtils.createAdministrator();
		acct = TestUtils.createAccount(owner);
		site = TestUtils.createSite(acct);
		sla = TestUtils.createSla(slaFieldValueObjectFilter,
									slaFieldValueProactiveFilter,
									slaFieldValueReactionTimeFilter,
									slaFieldValueRepairTimeFilter,
									slaFieldValueReplaceByFilter,
									slaFieldValueSlaTypeFilter);

	}

	static void initSettingsEmpty(){
		List<Sla_Picker_Settings__c> slaSettings = new List<Sla_Picker_Settings__c>();

		Sla_Picker_Settings__c slaSetting = new Sla_Picker_Settings__c();
		slaSetting.Name='Site Screen';

		slaSetting.Use_Object_Filter__c = false;
		slaSetting.Use_Proactive_Filter__c = false;
		slaSetting.Use_Reaction_Time_Filter__c = false;
		slaSetting.Use_Repair_Time_Filter__c = false;
		slaSetting.Use_Replace_By_Filter__c = false;
		slaSetting.Use_Sla_Type_Filter__c = false;

		slaSetting.Enforced_Object_Filter__c='';
		slaSetting.Enforced_Proactive_Filter__c='';
		slaSetting.Enforced_Reaction_Time_Filter__c='';
		slaSetting.Enforced_Repair_Time_Filter__c='';
		slaSetting.Enforced_Replace_By_Filter__c='';
		slaSetting.Enforced_Sla_Type_Filter__c='';

		slaSetting.Default_Object_Filter__c='';
		slaSetting.Default_Proactive_Filter__c='';
		slaSetting.Default_Reaction_Time_Filter__c='';
		slaSetting.Default_Repair_Time_Filter__c='';
		slaSetting.Default_Replace_By_Filter__c='';
		slaSetting.Default_Sla_Type_Filter__c='';

		slaSettings.add(slaSetting);


		insert slaSettings;
	}

	static void initSettingsFull(){

		List<Sla_Picker_Settings__c> slaSettings = new List<Sla_Picker_Settings__c>();

		Sla_Picker_Settings__c slaSetting = new Sla_Picker_Settings__c();
		slaSetting.Name='Site Screen';

		slaSetting.Use_Object_Filter__c = true;
		slaSetting.Use_Proactive_Filter__c = true;
		slaSetting.Use_Reaction_Time_Filter__c = true;
		slaSetting.Use_Repair_Time_Filter__c = true;
		slaSetting.Use_Replace_By_Filter__c = true;
		slaSetting.Use_Sla_Type_Filter__c = true;

		slaSetting.Enforced_Object_Filter__c=slaFieldValueObjectFilter;
		slaSetting.Enforced_Proactive_Filter__c=slaFieldValueProactiveFilter;
		slaSetting.Enforced_Reaction_Time_Filter__c=slaFieldValueReactionTimeFilter;
		slaSetting.Enforced_Repair_Time_Filter__c=slaFieldValueRepairTimeFilter;
		slaSetting.Enforced_Replace_By_Filter__c=slaFieldValueReplaceByFilter;
		slaSetting.Enforced_Sla_Type_Filter__c=slaFieldValueSlaTypeFilter;

		slaSetting.Default_Object_Filter__c=slaFieldValueObjectFilter;
		slaSetting.Default_Proactive_Filter__c=slaFieldValueProactiveFilter;
		slaSetting.Default_Reaction_Time_Filter__c=slaFieldValueReactionTimeFilter;
		slaSetting.Default_Repair_Time_Filter__c=slaFieldValueRepairTimeFilter;
		slaSetting.Default_Replace_By_Filter__c=slaFieldValueReplaceByFilter;
		slaSetting.Default_Sla_Type_Filter__c=slaFieldValueSlaTypeFilter;

		slaSettings.add(slaSetting);


		insert slaSettings;
	}

	@isTest
	static void testInitWithBlanks(){
		
		initBasicInfo();
		initSettingsEmpty();

		test.startTest();

		ApexPages.StandardController controller = new ApexPages.Standardcontroller(site);
		SlaSelectorController slaSelectorController = new SlaSelectorController(controller);

		String testValue = '';

		testValue = slaSelectorController.selectedObjectFilter;
		testValue = slaSelectorController.selectedProactiveFilter;
		testValue = slaSelectorController.selectedreactionTimeFilter;
		testValue = slaSelectorController.selectedrepairTimeFilter;
		testValue = slaSelectorController.selectedreplaceByFilter;
		testValue = slaSelectorController.selectedslaTypeFilter;

		testValue = slaSelectorController.selectedSla;

		List<SelectOption> myFilteredSlaOptions = slaSelectorController.filteredSlaOptions;

		List<SelectOption> myObjectFilter = slaSelectorController.objectFilter;
		List<SelectOption> myReactiveProactiveFilter = slaSelectorController.reactiveProactiveFilter;
		List<SelectOption> myReactionTimeFilter = slaSelectorController.reactionTimeFilter;
		List<SelectOption> myRepairTimeFilter = slaSelectorController.repairTimeFilter;
		List<SelectOption> myReplaceByFilter = slaSelectorController.replaceByFilter;
		List<SelectOption> mySlaTypeFilter = slaSelectorController.slaTypeFilter;

		test.stopTest();
	}

	@isTest
	static void testWithoutBlanks(){

		initBasicInfo();
		initSettingsFull();

		test.startTest();

		ApexPages.StandardController controller = new ApexPages.Standardcontroller(site);
		SlaSelectorController slaSelectorController = new SlaSelectorController(controller);

		String testValue = '';

		testValue = slaSelectorController.selectedObjectFilter;
		testValue = slaSelectorController.selectedProactiveFilter;
		testValue = slaSelectorController.selectedreactionTimeFilter;
		testValue = slaSelectorController.selectedrepairTimeFilter;
		testValue = slaSelectorController.selectedreplaceByFilter;
		testValue = slaSelectorController.selectedslaTypeFilter;

		testValue = slaSelectorController.selectedSla;

		List<SelectOption> myFilteredSlaOptions = slaSelectorController.filteredSlaOptions;

		List<SelectOption> myObjectFilter = slaSelectorController.objectFilter;
		List<SelectOption> myReactiveProactiveFilter = slaSelectorController.reactiveProactiveFilter;
		List<SelectOption> myReactionTimeFilter = slaSelectorController.reactionTimeFilter;
		List<SelectOption> myRepairTimeFilter = slaSelectorController.repairTimeFilter;
		List<SelectOption> myReplaceByFilter = slaSelectorController.replaceByFilter;
		List<SelectOption> mySlaTypeFilter = slaSelectorController.slaTypeFilter;

		slaSelectorController.selectedSla = sla.id;

		slaSelectorController.UpdateSla();

		test.stopTest();
		
	}

	@isTest
	static void testRefreshFiltersWithAllSelected(){
		
	}

	@isTest
	static void testSaveSla(){
		
	}
}