@isTest
private class TestSnapshotTriggerHandler {
    
    @isTest
    static void createSnapshots() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        
        Framework__c f = Framework__c.getOrgDefaults();
        f.Framework_Sequence_Number__c = 1;
        upsert f;
        
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c = opp.Id;
        insert basket;

        TestUtils.createOrderType();
        TestUtils.autoCommit = false;
        Product2 p = TestUtils.createProduct();
        p.ThresholdGroup__c = 'group1';
        insert p;

        // create matrix lines
        appro__Matrix__c theMatrix = new appro__Matrix__c(
            Name = 'testMatrix',
            appro__Object__c = 'cscfga__Product_Basket__c',
            appro__Child_Object__c = 'CS_Basket_Snapshot_Transactional__c',
            appro__Child_Link_Field__c = 'Product_Basket__c'
        );
        insert theMatrix;
        appro__Matrix_Line__c line = new appro__Matrix_Line__c(
            appro__Matrix__c = theMatrix.Id,
            appro__Active__c = true,
            appro__Active_Today__c = 'Yes',
            Approval_Step__c = 'level2',
            Commercial_Terms__c = '*',
            Proposition__c = '*',
            Valid_From__c = System.today().addDays(-7),
            Valid_To__c = System.today().addDays(7),
            Duration_From__c = 2,
            Duration_To__c = 3,
            Treshold_Group__c = 'group1',
            ExternalId__c = 'ML-10-ABCDEFGH'
        );
        insert line;
        
        Test.startTest();
        CS_Basket_Snapshot_Transactional__c snappie = new CS_Basket_Snapshot_Transactional__c();
        snappie.Product_Basket__c = basket.Id;
        snappie.OneOffTresholdGroup__c = 'group1';
        snappie.RecurringTresholdGroup__c = 'group1';
        snappie.RecurringProduct__c = p.Id;
        snappie.Proposition__c = '*';
        snappie.FixedDuration__c = 2;
        insert snappie;

        CS_Basket_Snapshot_Transactional__c snappie2 = new CS_Basket_Snapshot_Transactional__c();
        snappie2.Product_Basket__c = basket.Id;
        snappie2.OneOffTresholdGroup__c = 'group1';
        snappie2.RecurringTresholdGroup__c = 'group1';
        snappie2.OneOffProduct__c = p.Id;
        snappie2.Proposition__c = '*';
        snappie2.MobileDuration__c = 2;
        insert snappie2;
        
        snappie.Quantity__c = 6;
        update snappie;
        System.assertEquals(6, [SELECT Id, Total_Product_Quantity__c FROM CS_Basket_Snapshot_Transactional__c WHERE Id = :snappie.Id LIMIT 1].Total_Product_Quantity__c);
        
        CS_Basket_Snapshot_Transactional__c snappie3 = new CS_Basket_Snapshot_Transactional__c();
        snappie3.Product_Basket__c = basket.Id;
        snappie3.OneOffTresholdGroup__c = 'group1';
        snappie3.RecurringTresholdGroup__c = 'group1';
        snappie3.RecurringProduct__c = p.Id;
        snappie3.Proposition__c = '*';
        snappie3.FixedDuration__c = 2;
        snappie3.Quantity__c = 3;
        insert snappie3;

        System.assertEquals(9, [SELECT Id, Total_Product_Quantity__c FROM CS_Basket_Snapshot_Transactional__c WHERE Id = :snappie.Id LIMIT 1].Total_Product_Quantity__c);
        System.assertEquals(9, [SELECT Id, Total_Product_Quantity__c FROM CS_Basket_Snapshot_Transactional__c WHERE Id = :snappie3.Id LIMIT 1].Total_Product_Quantity__c);
        Test.stopTest();
    }

}