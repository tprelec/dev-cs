/**
 * 	@description	This class contains unit tests for the StringUtils class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestStringUtils {
	
		
	@isTest static void test_method_one() {
		
		String test = StringUtils.cleanNLPhoneNumber('+31 (6) 46060311');
		
		system.assertEquals('0646060311',test);
		
		List<String> testString = new List<String>();
		testString = StringUtils.cleanHouseNumberSuffix('a123');
		
		StringUtils su = new StringUtils();
		String tsTest = su.getTimeZoneValue();
		
		
	}

	@isTest
	private static void testParseCSV(){
	    string contents='Field1,Field2,Field3\n1,,Smith\n2,Fred,O\'Connor\n3,Destiny,"Awaits, DDS"\n\n';
	    list<list<string>> parsedCSV=StringUtils.parseCSV(contents,true,',');
	    //line 1
	    system.assertEquals('1',parsedCSV[0][0]);
	    system.assertEquals('',parsedCSV[0][1]);
	    system.assertEquals('Smith',parsedCSV[0][2]);
	    //line 2
	    system.assertEquals('2',parsedCSV[1][0]);
	    system.assertEquals('Fred',parsedCSV[1][1]);
	    system.assertEquals('O\'Connor',parsedCSV[1][2]);
	    //line 3
	    system.assertEquals('3',parsedCSV[2][0]);
	    system.assertEquals('Destiny',parsedCSV[2][1]);
	    system.assertEquals('Awaits, DDS',parsedCSV[2][2]);
	}	
	
	
}