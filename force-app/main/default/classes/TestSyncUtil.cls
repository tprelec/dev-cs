@isTest
private class TestSyncUtil {

    @testSetup
    static void makeData() {
        TestUtils.createSAPHardwareFieldMappings();
        insert new SAP_Hardware_Information__c(
            Item_Description__c = 'Nokia 3310',
            BAP_EBU__c = 100.99
        );
    }

    @isTest
    static void fullMapping() {
        Map<String, Map<String, String>> mapping = SyncUtil.fullMapping('SAP_Hardware_Information__c -> VF_Product__c');
        System.assertEquals(true, mapping.containsKey('SAP_Hardware_Information__c -> VF_Product__c'));
        System.assertEquals(true, mapping.get('SAP_Hardware_Information__c -> VF_Product__c').containsKey('Name'));
        System.assertEquals('Item_Description__c', mapping.get('SAP_Hardware_Information__c -> VF_Product__c').get('Name'));
    }

    @isTest
    static void fullMappingPlus() {
        Map<String, Map<String, Field_Sync_Mapping__c>> mapping = SyncUtil.fullMappingPlus('SAP_Hardware_Information__c -> VF_Product__c');
        System.assertEquals(true, mapping.containsKey('SAP_Hardware_Information__c -> VF_Product__c'));
        System.assertEquals(true, mapping.get('SAP_Hardware_Information__c -> VF_Product__c').containsKey('BAP_SAP__c'));
        System.assertEquals('BAP_EBU__c', mapping.get('SAP_Hardware_Information__c -> VF_Product__c').get('BAP_SAP__c').Source_Field_Name__c);
        System.assertEquals('BAP_SAP__c', mapping.get('SAP_Hardware_Information__c -> VF_Product__c').get('BAP_SAP__c').Target_Field_Name__c);
    }

    @isTest
    static void getQueriedFields() {
        System.assertEquals(
            new Set<String>{'value'},
            SyncUtil.getQueriedFields(new Map<String, String>{'key' => 'value'})
        );
    }

    @isTest
    static void getSortedList() {
        Map<String, Schema.SObjectField> input = new Map<String, Schema.SObjectField>{'Bullocks' => Account.Name.getDescribe().getSObjectField()};
        System.assertEquals(new List<String>{'Name'}, SyncUtil.getSortedList(input));
    }

    @isTest
    static void getHeader() {
        String res = SyncUtil.getHeader(new List<String>{'Foxtrot', 'Potato', 'Jackfruit', 'CherryMXBrown'}, new Set<String>{'Foxtrot', 'CherryMXBrown'});
        System.assert(res.contains('Foxtrot'));
        System.assert(res.contains('CherryMXBrown'));
    }

    @isTest
    static void populateFields() {
        SAP_Hardware_Information__c shi = [SELECT Id, Item_Description__c, Brand__c, BAP_EBU__c, Cost_Price__c, OrderType__c FROM SAP_Hardware_Information__c WHERE Item_Description__c = 'Nokia 3310' LIMIT 1];
        Map<String, Field_Sync_Mapping__c> sapFieldToVFProductFieldMappingRecord = SyncUtil.fullMappingPlus('SAP_Hardware_Information__c -> VF_Product__c').get('SAP_Hardware_Information__c -> VF_Product__c');
        Map<String, Field_Sync_Mapping__c> sapFieldToVFProductFieldMappingRecordWitOneRecord = new Map<String, Field_Sync_Mapping__c>{
            'Name' => sapFieldToVFProductFieldMappingRecord.get('Name')
        };
        Map<String, Schema.SObjectField> destinationObjectSchemaFieldMap = new Map<String, Schema.SObjectField>{'Name' => VF_Product__c.Name.getDescribe().getSObjectField()};

        VF_Product__c mappedRecord = (VF_Product__c) SyncUtil.populatefields(new VF_Product__c(), shi, sapFieldToVFProductFieldMappingRecordWitOneRecord, destinationObjectSchemaFieldMap, true);
        System.assertEquals('Nokia 3310', mappedRecord.Name);
    }

    @isTest
    static void generateSingleFullMappingFromObjectName() {
        TestUtils.createOpportunityFieldMappings();
        System.assert(SyncUtil.generateSingleFullMappingFromObjectName('Opportunity').containsKey('Owner__c'));
    }

    @isTest
    static void getMappingFromObjectName() {
       System.assert(SyncUtil.getMappingFromObjectName('Opportunity').contains('Opportunity -> VF_Contract__c'));
    }

    @isTest
    static void getDestinationObjectFromMapping() {
        System.assertEquals('VF_Contract__c', SyncUtil.getDestinationObjectFromMapping('Opportunity'));
    }

    @isTest
    static void createSObjectWithDefaults() {
        Field_Sync_Mapping__c mappingRecord = [
            SELECT Id
            FROM Field_Sync_Mapping__c
            WHERE Object_Mapping__c = 'SAP_Hardware_Information__c -> VF_Product__c' AND Source_Field_Name__c = 'Brand__c'
        ];
        mappingRecord.Target_Default_Value__c = 'I am so default';
        mappingRecord.Target_Default_Value_Type__c = 'STRING';
        update mappingRecord;

        insert new List<Field_Sync_Mapping__c>{
            new Field_Sync_Mapping__c(
                Object_Mapping__c = 'SAP_Hardware_Information__c -> VF_Product__c',
                Target_Field_Name__c = 'Multiplier_Vodacom__c',
                Target_Default_Value__c = '1000',
                Target_Default_Value_Type__c = 'Integer'
            ),
            new Field_Sync_Mapping__c(
                Object_Mapping__c = 'SAP_Hardware_Information__c -> VF_Product__c',
                Target_Field_Name__c = 'Active__c',
                Target_Default_Value__c = 'TRuE',
                Target_Default_Value_Type__c = 'BOOLEAN'
            )
        };

        SAP_Hardware_Information__c shi = [SELECT Id, Item_Description__c, Brand__c, BAP_EBU__c, Cost_Price__c, OrderType__c FROM SAP_Hardware_Information__c WHERE Item_Description__c = 'Nokia 3310' LIMIT 1];
        Map<String, Field_Sync_Mapping__c> sapFieldToVFProductFieldMappingRecord = SyncUtil.fullMappingPlus('SAP_Hardware_Information__c -> VF_Product__c').get('SAP_Hardware_Information__c -> VF_Product__c');

        VF_Product__c mappedRecord = (VF_Product__c) SyncUtil.createSObjectWithDefaults(shi, sapFieldToVFProductFieldMappingRecord, new VF_Product__c());
        System.assertEquals('Nokia 3310', mappedRecord.Name);
        System.assertEquals(1000, mappedRecord.Multiplier_Vodacom__c);
        System.assertEquals(true, mappedRecord.Active__c);
    }

}