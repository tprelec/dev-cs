@isTest
private class TestTrainingRegistrationController {
	
	@isTest static void test_open_training() {
		// create training event
		TrainingRegistrationController trc = new TrainingRegistrationController();
		Id trainingRecordTypeId = trc.trainingRecordTypeId;
		Event e = new Event();
		e.recordTypeId = trainingRecordTypeId;
		e.Type = 'Training (Open)';
		e.Subject = 'testtraining1';
		e.StartDateTime = system.now().addDays(1);
		e.EndDateTime = system.now().addDays(1).addHours(1);
		insert e;

		// add attendees
		

		system.runas(TestUtils.createAccountManager()){
			
			List<Event> theTrainings = trc.availableTrainings;
			List<EventRelation> theRegistrations = trc.currentTrainingRegistrations;

			// check that no registration exists yet
			system.assertEquals(0,theRegistrations.size());

			trc.selectedTrainingId = e.Id;	
			trc.enroll();

			theRegistrations = trc.currentTrainingRegistrations;			

			// check there's now 1 registration
			system.assertEquals(1,theRegistrations.size());

			trc.selectedEnrollmentId = theRegistrations[0].Id;
			trc.deleteEnrollment();

			theRegistrations = trc.currentTrainingRegistrations;			

			// check that there are again 0 registrations
			system.assertEquals(0,theRegistrations.size());

		}
	}
	
	@isTest static void test_invited_training() {
		// create training event
		TrainingRegistrationController trc = new TrainingRegistrationController();
		Id trainingRecordTypeId = trc.trainingRecordTypeId;
		Event e = new Event();
		e.recordTypeId = trainingRecordTypeId;
		e.Type = 'Training (Invitation-based)';
		e.Subject = 'testtraining1';
		e.StartDateTime = system.now().addDays(1);
		e.EndDateTime = system.now().addDays(1).addHours(1);
		insert e;

		// create invitation
		User am = TestUtils.createAccountManager();
		EventRelation er = new EventRelation();
		er.Status = 'New';
		er.RelationId = am.Id;
		er.EventId = e.Id;
		insert er;


		// accept invitation
		
		system.runas(am){
			
			List<Event> theTrainings = trc.availableTrainings;
			List<EventRelation> theRegistrations = trc.currentTrainingRegistrations;

			// check that no registration exists yet
			system.assertEquals(0,theRegistrations.size());

			trc.selectedTrainingId = e.Id;	
			trc.enroll();

			theRegistrations = trc.currentTrainingRegistrations;			

			// check there's now 1 registration
			system.assertEquals(1,theRegistrations.size());

			trc.selectedEnrollmentId = theRegistrations[0].Id;
			trc.deleteEnrollment();

			theRegistrations = trc.currentTrainingRegistrations;			

			// check that there are again 0 registrations
			system.assertEquals(0,theRegistrations.size());

		}		
	}
	
}