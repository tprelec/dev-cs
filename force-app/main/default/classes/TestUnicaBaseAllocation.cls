@isTest
private class TestUnicaBaseAllocation {
	
	@isTest static void testCreation() {
		
		//create itw sales user
		User itwSalesUser = TestUtils.createAdministrator(); //it's ok if it's an admin
		//this also creates a manager for him
		
		//create account
		Account testAcc = TestUtils.createAccount(itwSalesUser);

		//create record, assign the fields
		Unica_Base_Allocation__c uba = new Unica_Base_Allocation__c();
		uba.Salesforce_Account_ID__c = testAcc.id;
		uba.Inside_Sales_Agent_Email__c = itwSalesUser.email;
		uba.Team_Name__c = 'ITW';
		uba.Allocation_Action__c = 'New';

		insert uba;

		uba = [select 	id, 
						Account__c, 
						Allocation_Action__c, 
						Assigned_User__c, 
						Assigned_User_s_Manager__c, 
						Assignment_Approved__c, 
						Error__c, 
						Inside_Sales_Agent_Email__c, 
						Processed__c, 
						ReadyForProcessing__c,
						Sales_Agent_Salesforce_ID__c,
						Salesforce_Account_ID__c,
						Team_Name__c
				from Unica_Base_Allocation__c 
				where id = :uba.id];

		//check if all lookup fields are correctly populated
		System.assertEquals(uba.Assigned_User__c, itwSalesUser.id);
		System.assertEquals(uba.Assigned_User_s_Manager__c, itwSalesUser.managerId);
		System.assertEquals(uba.Account__c,testAcc.id);


		//check if flags are as expected
		System.assertEquals(uba.Assignment_Approved__c, false);
		System.assertEquals(uba.ReadyForProcessing__c, true);
		System.assertEquals(uba.Processed__c, false);


		//approve it
		List<Unica_Base_Allocation__c> ubaList = new List<Unica_Base_Allocation__c>{uba};
		UnicaBaseAllocationMassApprovalControllr.approveSelected(ubaList);
		

		//refetch
		uba = [select 	id, 
						Account__c, 
						Allocation_Action__c, 
						Assigned_User__c, 
						Assigned_User_s_Manager__c, 
						Assignment_Approved__c, 
						Error__c, 
						Inside_Sales_Agent_Email__c, 
						Processed__c, 
						ReadyForProcessing__c,
						Sales_Agent_Salesforce_ID__c,
						Salesforce_Account_ID__c,
						Team_Name__c
				from Unica_Base_Allocation__c 
				where id = :uba.id];

		//check if flags are as expected
		System.assertEquals(uba.Assignment_Approved__c, true);
		System.assertEquals(uba.ReadyForProcessing__c, true);
		System.assertEquals(uba.Processed__c, false);

		UnicaBaseAllocationBatchExecuteCntrlr batchControler = new UnicaBaseAllocationBatchExecuteCntrlr(null);
		batchControler.callBatch();

		//xxx todo in future? check if processed correctly by batch? how?
	}
	
}