@isTest
private class TestUnicaCampaignBatch {
	
	static testMethod void myUnitTest() {

		insert new Unica_Campaign__c(Campaign_Name__c = 'Test1');
		insert new Unica_Campaign__c(Campaign_Name__c = 'Test2', Selection_Name__c = 'january', Ban__c = '312312311', NTA_Id__c = '12345678', Treatment_01__c = 'Test Treatment', Campaign_Type__c = 'Recommit');
		
		TestUtils.autoCommit = false;

		map<String, String> mapping = new map<String, String>{'Campaign_Name__c' => 'Name',
															  'Selection_Name__c' => 'Selection_Name__c',
															  'Campaign_Type__c' => 'Type'};
		list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
		for (String field : mapping.keyset()) {
			fsmlist.add(TestUtils.createSync('Unica_Campaign__c -> Campaign', field, mapping.get(field)));
		}
		fsmlist.add(TestUtils.createSync('Unica_Campaign__c -> CampaignMember', 'NTA_Id__c', 'NTA_Id__c'));
		fsmlist.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'NTA_Id__c', 'NTA_Id__c'));
		fsmlist.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'Ban__c', 'BAN_Number__c'));
		fsmlist.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'Treatment_01__c', 'Treatment_01__c'));
		fsmlist.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'Campaign_Name__c', 'LastName'));
		insert fsmlist;

		list<Account> accountList = new list<Account>{
		new Account(Name = 'Test Account 00', KVK_number__c = '22312351')};
		insert accountList;

		Account acc = [select Id from Account where KVK_number__c = '22312351'];
		list<Ban__c> banList = new list<Ban__c>{ new BAN__c (Account__c = acc.Id, Name = '312312311', Ban_Name__c = 'Test Ban')};
		insert banList;

		TestUtils.autoCommit = true;

		//Test
		Test.startTest();

		PageReference pageRef = Page.UnicaCampaignBatchExecute;
		Test.setCurrentPage(pageRef);
		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new list<Unica_Campaign__c>());
		UnicaCampaignBatchExecuteController controller = new UnicaCampaignBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

	}
}