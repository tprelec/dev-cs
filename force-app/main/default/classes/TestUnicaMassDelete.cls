@isTest
private class TestUnicaMassDelete {

	static testMethod void MassDeleteRecordsTest() {
		//Create
		Unica_Campaign__c pItem = new Unica_Campaign__c();
		insert pItem;
		Unica_Update__c oItem = new Unica_Update__c();
		insert oItem;

		list<Unica_Campaign__c> objList1 = [SELECT ID FROM Unica_Campaign__c Where ID = :pItem.ID];
		ApexPages.StandardSetController ssc1 = new ApexPages.StandardSetController(objList1);
		ssc1.setSelected(objList1);

		list<Unica_Update__c> objList2 = [SELECT ID FROM Unica_Update__c Where ID = :oItem.ID];
		ApexPages.StandardSetController ssc2 = new ApexPages.StandardSetController(objList2);
		ssc2.setSelected(objList2);

		//Invoke
		UnicaMassDelete mdr1 = new UnicaMassDelete(ssc1);
		mdr1.DoDelete();

		UnicaMassDelete mdr2 = new UnicaMassDelete(ssc2);
		mdr2.DoDelete();

		//Check
		objList1 = [SELECT ID FROM Unica_Campaign__c Where ID = :pItem.ID];
		system.assertEquals(objList1.size(), 0);

		objList2 = [SELECT ID FROM Unica_Update__c Where ID = :oItem.ID];
		system.assertEquals(objList2.size(), 0);
	}
}