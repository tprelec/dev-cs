@isTest
private class TestUpdateBillingStatusBatch {
	@isTest
	static void testUpdateBillingStatus() {
		prepareTestData();

		Test.startTest();
		ScheduleUpdateBillingStatusBatch subsb = new ScheduleUpdateBillingStatusBatch();
		String cronExp = '0 0 0 15 3 ? 2022';
		System.schedule('ScheduledApexTest', cronExp, subsb);
		subsb.execute(null); // Explicitly call the execute method to invoke batch processing
		Test.stopTest();

		Customer_Asset__c ca = [SELECT Id, Billing_Status__c FROM Customer_Asset__c LIMIT 1];
		System.assertEquals('Completed', ca.Billing_Status__c, 'Wrong Billing Status!');
	}
	private static void prepareTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createOrderValidationOrder();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct(
			new Map<String, Object>{
				'Billing_Type__c' => Constants.PRODUCT2_BILLINGTYPE_STANDARD,
				'ProductCode' => 'C106929'
			}
		);
		insert product;

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = contr.Id;
		ord.O2C_Order__c = true;
		insert ord;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ;
		cp.VF_Contract__c = contr.Id;
		cp.Billing_Status__c = 'New';
		cp.Product__c = product.Id;
		cp.ProductCode__c = 'C106929';
		insert cp;

		TestUtils.orderChangeStatus(ord.Id, Constants.ORDER_STATUS_SUBMITTED);
		Customer_Asset__c newCA = [SELECT Id FROM Customer_Asset__c LIMIT 1];
		newCA.Latest_Activation_Date__c = System.today();
		update newCA;
	}
}