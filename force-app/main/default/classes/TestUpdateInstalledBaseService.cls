@IsTest
private class TestUpdateInstalledBaseService {
	private static String accountId;
	private static String installedBaseId;
	private static String siteId;
	private static Decimal houseNumber;
	private static String houseNumberSuffix;
	private static String postalCode;
	private static String externalOrderId;

	@IsTest
	static void testAddInstalledBase() {
		String result = updateInstalledBase('ADD');
		System.assertEquals('OK', result, 'Success!');
	}

	@IsTest
	static void testRemoveInstalledBase() {
		String result = updateInstalledBase('REMOVE');
		System.assertEquals('OK', result, 'Success!');
	}

	@IsTest
	static void testChangeInstalledBase() {
		String result = updateInstalledBase('CHANGE');
		System.assertEquals('OK', result, 'Success!');
	}

	@IsTest
	static void testValidations() {
		prepareTestData();
		Map<String, Object> installedBaseMap = new Map<String, Object>{
			'accountId' => '0011X000' + '00iFXZIQA4', // split up the Id to avoid PMD errors
			'siteId' => 'a061X000' + '002eE9RQAU', // split up the Id to avoid PMD errors
			'installedBaseId' => installedBaseId,
			'houseNumber' => houseNumber,
			'houseNumberSuffix' => houseNumberSuffix + 'X',
			'postalCode' => postalCode,
			'action' => 'REMOVE',
			'managedItemId' => '1111111',
			'articleCode' => '2222222',
			'deviceServices' => 'Device1, Device2',
			'externalOrderId' => '3333333'
		};
		String result = invokeService(installedBaseMap);
		System.assertEquals('FAILED', result, 'Failure!');
	}

	@IsTest
	static void testInvalidInstalledBaseId() {
		Map<String, Object> installedBaseMap = new Map<String, Object>{
			'installedBaseId' => 'installedBaseId',
			'action' => 'CHANGE'
		};
		String result = invokeService(installedBaseMap);
		System.assertEquals('FAILED', result, 'Failure!');
	}

	@IsTest
	static void testInvalidAction() {
		Map<String, Object> installedBaseMap = new Map<String, Object>{ 'action' => 'action' };
		String result = invokeService(installedBaseMap);
		System.assertEquals('FAILED', result, 'Failure!');
	}

	@IsTest
	static void testUnhandledException() {
		prepareTestData();
		Map<String, Object> installedBaseMap = new Map<String, Object>{
			'accountId' => 'accountId',
			'installedBaseId' => installedBaseId,
			'action' => 'CHANGE'
		};
		String result = invokeService(installedBaseMap);
		System.assertEquals('FAILED', result, 'Failure!');
	}

	static String updateInstalledBase(String reqAction) {
		prepareTestData();
		Map<String, Object> installedBaseMap = new Map<String, Object>{
			'accountId' => accountId,
			'siteId' => siteId,
			'installedBaseId' => installedBaseId,
			'houseNumber' => houseNumber,
			'houseNumberSuffix' => houseNumberSuffix,
			'postalCode' => postalCode,
			'action' => reqAction,
			'managedItemId' => '1111111',
			'articleCode' => '2222222',
			'deviceServices' => 'Device1, Device2',
			'externalOrderId' => externalOrderId,
			'isVirtual' => 'true',
			'isShared' => 'false'
		};
		String result = invokeService(installedBaseMap);
		return result;
	}

	static void prepareTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		TestUtils.createCompleteContract();

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = TestUtils.theOrderType.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = TestUtils.theContract.Id;
		ord.O2C_Order__c = true;
		ord.Sales_Order_Id__c = '4444444';
		insert ord;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ;
		cp.VF_Contract__c = TestUtils.theContract.Id;
		cp.Billing_Status__c = 'New';
		cp.Product__c = TestUtils.theProduct.Id;
		cp.ProductCode__c = 'C106929';
		cp.Site__c = TestUtils.theSite.Id;
		insert cp;

		Customer_Asset__c ca = [
			SELECT
				Id,
				Account__c,
				Site__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Postal_Code__c,
				Installed_Base_Id__c,
				BOP_Managed_Item_ID__c,
				Order__r.Sales_Order_Id__c,
				Article_Code__c,
				BOP_HDN_Services__c
			FROM Customer_Asset__c
		];

		accountId = ca.Account__c;
		installedBaseId = ca.Installed_Base_Id__c;
		siteId = ca.Site__c;
		houseNumber = ca.Site__r.Site_House_Number__c;
		houseNumberSuffix = ca.Site__r.Site_House_Number_Suffix__c;
		postalCode = ca.Site__r.Site_Postal_Code__c;
		externalOrderId = ca.Order__r.Sales_Order_Id__c;
	}

	static String invokeService(Map<String, Object> installedBaseMap) {
		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/updateinstalledbase/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(JSON.serializePretty(installedBaseMap));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		UpdateInstalledBaseService.processCustomerAssets();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
			response.responsebody.tostring()
		);

		return (String) m.get('status');
	}
}