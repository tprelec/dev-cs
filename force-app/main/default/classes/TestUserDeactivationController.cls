@isTest
private class TestUserDeactivationController {
    @testSetup
    static void setup(){
        // Setup data for test
        Account partnerAccount = TestUtils.createPartnerAccount('ABC');
        Account partnerAccount2 = TestUtils.createPartnerAccount('DEF');
        User portalUser = TestUtils.createPortalUser(partnerAccount);
        User portalUser2 = TestUtils.createPortalUser(partnerAccount2);
        
        list<Contact> updateContacts = new list<Contact>();
        Contact temp1 = new contact(Id=portalUser.ContactId, UserID__C=portalUser.Id,Replacement_User__c=portalUser2.Id);
        updateContacts.add(temp1);
        Contact temp2 = new contact(Id=portalUser2.ContactId, UserID__C=portalUser2.Id,Replacement_User__c=portalUser.Id);
        updateContacts.add(temp2);
        update updateContacts;
        
        Dealer_Information__c di = TestUtils.createDealerInformation(portalUser.ContactId);
        Dealer_Information__c di2 = TestUtils.createDealerInformation(portalUser2.ContactId);
        Postal_Code_Assignment__c pca = TestUtils.createPCA('1234', di.Id);
        Postal_Code_Assignment__c pca2 = TestUtils.createPCA('5678', di2.Id);
        
        list<Contact> newContacts = [SELECT Id, Name, UserID__C, Replacement_User__c 
                                     FROM Contact];
        list<Dealer_Information__c> newDealers = [SELECT Name, Dealer_Code__c, Contact__c, Contact__r.UserId__c 
                                                  FROM Dealer_Information__c];
        list<Postal_Code_Assignment__c> newPCA = [SELECT Name, Postcode_Number__c, Dealer_Information__c
                                                  FROM Postal_Code_Assignment__c];
    }
    
    @isTest 
    static void withReplacement() {
        // Start test
        Test.startTest();
        List<Contact> testData = [SELECT Id, Name, UserID__C, Replacement_User__c FROM Contact];
        
        PageReference deactivationForm = page.UserDeactivation;
        Test.setCurrentPage(deactivationForm);
        ApexPages.currentPage().getParameters().put('Id', testData[0].UserID__C);
        ApexPages.currentPage().getParameters().put('contactId', testData[0].Id);    
        
        // Create an instance of the controller
        UserDeactivationController controller = new UserDeactivationController(); 
        controller.userContact=testData[1];
        Boolean recordsFound = controller.noRecordsFound;
        
        // Call controller methods
        Boolean b = controller.getIsPartnerUser();
        List<Dealer_Information__c> diList = controller.dealerInformations;
        List<Account> accList = controller.ownedAccounts;
        List<AccountTeamMember> atm = controller.accountTeamMembers;
        List<Opportunity> oppList = controller.openOpportunities;       
        List<Lead> leadList = controller.openLeads;
        List<User> subList = controller.subordinates; 
        List<User> primList = controller.primaryPartners;  
        List<User> adminList = controller.adminPartners;
        List<Dashboard> dashList = controller.runningUserDashboards; 
        List<VF_Contract__c> contractsVFList = controller.contractsVF; 
        List<Postal_Code_Assignment__c> pcaList = controller.postalCodeAssignments; 
        List<ProcessInstanceWorkitem> papList = controller.pendingApprovalProcesses;
        List<CreditNote_Approvals__c> cnaList = controller.creditNoteApprovals;

        PageReference p = controller.updateAllUsers();

        recordsFound = controller.noRecordsFound;
        
        Test.stopTest();

        System.assert(recordsFound);
    }
    
    @isTest 
    static void withoutReplacement() {
        // Start test
        Test.startTest();
        
        List<Contact> testData = [SELECT Id, Name, UserID__C, Replacement_User__c FROM Contact];
        Contact replacementContact = testData[1];
            
        replacementContact.Replacement_User__c=null;
        update replacementContact;
        
        PageReference deactivationForm = page.UserDeactivation;
        Test.setCurrentPage(deactivationForm);
        ApexPages.currentPage().getParameters().put('Id', testData[0].UserID__c);
        ApexPages.currentPage().getParameters().put('contactId', testData[0].Id);    
        
        // Create an instance of the controller
        UserDeactivationController controller = new UserDeactivationController(); 
        controller.userContact=replacementContact;
        Boolean recordsFound = controller.noRecordsFound;
        
        // Call controller methods
        Boolean b = controller.getIsPartnerUser();
        List<Dealer_Information__c> diList = controller.dealerInformations;
        List<Account> accList = controller.ownedAccounts;
        List<AccountTeamMember> atm = controller.accountTeamMembers;
        List<Opportunity> oppList = controller.openOpportunities;       
        List<Lead> leadList = controller.openLeads;
        List<User> subList = controller.subordinates; 
        List<User> primList = controller.primaryPartners;  
        List<User> adminList = controller.adminPartners;
        List<Dashboard> dashList = controller.runningUserDashboards; 
        List<VF_Contract__c> contractsVFList = controller.contractsVF; 
        List<Postal_Code_Assignment__c> pcaList = controller.postalCodeAssignments; 
        List<ProcessInstanceWorkitem> papList = controller.pendingApprovalProcesses;
        List<CreditNote_Approvals__c> cnaList = controller.creditNoteApprovals;
        
        PageReference p = controller.updateAllUsers();
        recordsFound = controller.noRecordsFound;
        
        // Stop test
        Test.stopTest();

        System.assert(!recordsFound);
    }
    
}