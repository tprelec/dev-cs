@isTest
private class TestUserManagementController {
	
	@isTest static void test_method_one() {
		
		TestUtils.createAdministrator();
		list<User> userList = [select Id, ManagerId from User where ManagerId != null];
		User run = new User();	
		for(User u: userList){
			User testUser = [select Id, IsActive, ManagerId, UserRole.Name, Name from User where Id =: u.ManagerId limit 1];
			if(testUser.IsActive == true){
				run = testUser;
				break;
			}
		}
		PageReference pref = Page.UserManagement;
		Test.setCurrentPage(pref);
		UserManagementController con = new UserManagementController();
		UserManagementController.Info infotest = new UserManagementController.Info(run);
		con.infoList = new List<UserManagementController.Info>();
		con.infoList.add(infotest);

		Test.startTest();

		System.runAs(run){
			pref = con.send();
		}		

		Test.stopTest(); 

	}	
	
}