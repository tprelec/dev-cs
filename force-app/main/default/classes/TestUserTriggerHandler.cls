@isTest
public class TestUserTriggerHandler {
    
    @isTest
    static void testActivateCommunityUser(){
        Account partnerAccount;
        Contact portalContact;
        User commUser;

        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount = TestUtils.createPartnerAccount();
            portalContact = TestUtils.createContact(partnerAccount);
        }

        System.runAs(adminsByNumber.get('Admin2')){
            TestUtils.autoCommit = false;
            commUser = TestUtils.createPortalUser(partnerAccount);
            commUser.isActive = false;
            commUser.contactId = portalContact.Id;
        }

        Test.startTest();
            System.runAs(adminsByNumber.get('Admin2')){
                insert commUser;
            }
        Test.stopTest();
    }

    @isTest
    static void testActivateUser(){
        User u;
        System.runAs(GeneralUtils.currentUser){
            TestUtils.autoCommit = false;
            u = TestUtils.createAdministrator();
            u.isActive = false;
            insert u;
        }

        Test.startTest();
                u.Email = 'You\'renotfired@vodafoneziggo.nl';
                u.IsActive = true;
                update u;
        Test.stopTest();
    }       
    
    @isTest
    static void testAssignCloudSenseLicense(){
        User testUser = TestUtils.generateTestUser('My Test User', 'McTesty', 'VF Sales Manager');
        
        Test.startTest();
            insert testUser;
        Test.stopTest();
        
        Set<Id> packageLicenseIds = UserTriggerHandler.GetPackageLicenseId();
        Integer foundAssignedLicenses = [select Id from UserPackageLicense where UserId = :testUser.Id and PackageLicenseId in :packageLicenseIds].size();
        System.assertEquals(packageLicenseIds.size(), foundAssignedLicenses, 'All the corresponding Cloudsense Licenses should be assigned to our new User');
    }

    /** 
    This test is taking a very long time to complete, for now I'm going to comment it out because
    it dosen't test any extra functionality than the normal testAssignCloudSenseLicense. Once other code
    has been optimalised a bit more we can uncomment this - Chris 08-01-2020

    @isTest static void testAssignCloudSenseLicenseBulk(){
        List<User> toInsert = new List<User>();
        Id vfSalesManagerProfileId = [select Id from Profile where Name = 'VF Sales Manager'].Id;

        for (Integer i = 0; i < 200; i++) {
            toInsert.add(TestUtils.generateTestUser('My Test User', 'McTesty', vfSalesManagerProfileId, null));
        }
        
        Test.startTest();
            insert toInsert;
        Test.stopTest();

        Map<Id, User> toVerify = new Map<Id, User>(toInsert);        
        Set<Id> packageLicenseIds = UserTriggerHandler.GetPackageLicenseId();
        Integer foundAssignedLicenses = [select Id from UserPackageLicense where UserId in :toVerify.keySet() and PackageLicenseId in :packageLicenseIds].size();
        System.assertEquals(packageLicenseIds.size(), foundAssignedLicenses / 200, 'Every User should have 7 Cloudsense licenses assigned');
    }
    */

    @isTest
    static void testCheckLocaleEnUs(){
        User testUser = TestUtils.generateTestUser('My Test User', 'McTesty', 'VF Sales Manager');
        testUser.LocaleSidKey = 'en_US';
        String result = null;

        Test.startTest();
            try {
                insert testUser;
            } catch (Exception e) {
                result = e.getDmlMessage(0);
            }
        Test.stopTest();

        System.assertEquals('Locales English (United States), English (Canada) are blocked by VF NL because they cause invalid /// number formats.', result, 'System should throw an error');
    }

    @isTest
    static void testCheckLocaleNl(){
        User testUser = TestUtils.generateTestUser('My Test User', 'McTesty', 'VF Sales Manager');
        testUser.LocaleSidKey = 'nl';
        String result = null;   

        Test.startTest();
            try {
                insert testUser;
            } catch (Exception e) {
                result = e.getDmlMessage(0);
            }
        Test.stopTest();

        System.assertEquals('Please select Dutch(Netherlands) for dutch locale.', result, 'System should throw an error');
    }

    /**The UserTriggerHandlerBatch has it's own TestClass as well*/
    @isTest
    static void testFreeUpCPQLicencse(){
        System.runAs(GeneralUtils.currentUser){
            TestUtils.createVodafoneAccount();
        }     
        User testUser = TestUtils.generateTestUser('My Test User', 'McTesty', 'VF Sales Manager');
        insert testUser;

        Test.startTest();
            testUser.isActive = false;
            update testUser;
        Test.stopTest();

        System.assertEquals([select id from BigMachines__Oracle_User__c where BigMachines__Salesforce_User__c = :testUser.Id and BigMachines__Bulk_Synchronization__c = true and BigMachines__Provisioned__c = true].size(), 
                            0, 
                            'BigMachines stuff should be deactivated');
    }

    @isTest 
    static void testPreventDeleteDealerInformation(){
        Account partnerAccount;
        Contact portalContact;
        User commUser;

        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

        // Create Account and set the Type to Dealer
        System.runAs(adminsByNumber.get('Admin1')){
            TestUtils.autoCommit = false;
            partnerAccount = TestUtils.createPartnerAccount();
            partnerAccount.Type = 'Dealer';
            insert partnerAccount;
            partnerAccount.IsPartner = true;
            update partnerAccount;
            TestUtils.autoCommit = true;
            portalContact = TestUtils.createContact(partnerAccount);
        }

        // Create a dealer user as owner for the dealer info
        System.runAs(adminsByNumber.get('Admin2')){
            TestUtils.autoCommit = false;
            commUser = TestUtils.createPortalUser(partnerAccount);
            commUser.contactId = portalContact.Id;
            insert commUser;
        }

        //Create the dealer info and update the contact with the ID from our created user
        System.runAs(adminsByNumber.get('Admin1')){
            TestUtils.autoCommit = true;
            portalContact.UserId__c = commUser.Id;
            update portalContact;
            Dealer_Information__c di = TestUtils.createDealerInformation(portalContact.Id);
        }

        String errorThrown;
        Test.startTest();
            System.runAs(adminsByNumber.get('Admin2')){
                commUser.IsActive = false;
                try {
                    TriggerHandler.resetRecursion('UserTriggerHandler');
                    update commUser;
                } catch (Exception e) {
                    errorThrown = e.getMessage() + e.getStackTraceString();
                }
            }
        Test.stopTest();

        System.assert(
            errorThrown != null && errorThrown.contains('This user is the main contact for Dealer Information(s)'), 
            'Trigger should throw a custom exception for Dealer Information. The caught exception is: ' + errorThrown
        );
    }

    @isTest
    static void testPreventDeleteAccountOwner(){
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
        Account acc1;
        System.runAs(adminsByNumber.get('Admin1')){
            acc1 = TestUtils.createAccount(adminsByNumber.get('Admin1'));
        }
        User toDeactivate = [SELECT Id, IsActive FROM User WHERE Id = :acc1.OwnerId];

        String errorThrown;
        Test.startTest();
        toDeactivate.IsActive = false;
        try {
            System.runAs(adminsByNumber.get('Admin2')){
                update toDeactivate;
            }
        } catch (Exception e) {
            errorThrown = e.getMessage() + e.getStackTraceString();
        }
        Test.stopTest();

        System.assert(
            errorThrown != null && errorThrown.contains('This user is the Account Owner for one or more accounts.'), 
            'Trigger should throw a custom exception for Account Ownership. The caught exception is: ' + errorThrown
        );
    }

    @isTest
    static void testPreventDeleteAccountTeamMember(){

        List<Account> accounts = new List<Account>();
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

        System.runAs(adminsByNumber.get('Admin1')){

            accounts.add(TestUtils.createAccount(adminsByNumber.get('Admin1')));
            accounts.add(TestUtils.createAccount(adminsByNumber.get('Admin1')));

            For(account a : accounts){
                a.IsPartner = true;
                a.Dealer_code__c = '999998';
            }

            update accounts;

            TestUtils.autoCommit = true;
            TestUtils.createAccountTeamMember(adminsByNumber.get('Admin1'), accounts[0]);
            TestUtils.createAccountTeamMember(adminsByNumber.get('Admin1'), accounts[1]);
        }

        String errorThrown;
        Test.startTest();
            adminsByNumber.get('Admin1').IsActive = false;
            try {
                System.runAs(adminsByNumber.get('Admin2')){
                    update adminsByNumber.get('Admin1');
                }
            } catch (Exception e) {
                errorThrown = e.getMessage() + e.getStackTraceString();
            }
        Test.stopTest();

        System.assert(
            errorThrown != null && errorThrown.contains('This user is an Account Team Member for one or more Partner Accounts.'), 
            'Trigger should throw a custom exception for Account Ownership. The caught exception is: ' + errorThrown
        );
    }

    @isTest
    static void testPreventDeleteOpportunityOwner(){

        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

        System.runAs(adminsByNumber.get('Admin1')){
            Account acct = TestUtils.createAccount(adminsByNumber.get('Admin1'));
            Pricebook2 pb = TestUtils.createPricebook(false);
            Ban__c ban = TestUtils.createBan(acct);

            TestUtils.createOpportunityWithBan(acct, pb.id, ban, adminsByNumber.get('Admin1'));
            TestUtils.createOpportunityWithBan(acct, pb.id, ban, adminsByNumber.get('Admin1'));
        }

        User toDeactivate = [select Id, IsActive from User where Id =: adminsByNumber.get('Admin1').id];

        String errorThrown;
        Test.startTest();
            toDeactivate.IsActive = false;
            try {
                System.runAs(adminsByNumber.get('Admin2')){
                    update toDeactivate;
                }
            } catch (Exception e) {
                errorThrown = e.getMessage() + e.getStackTraceString();
            }
        Test.stopTest();

        System.assert(
            errorThrown != null && errorThrown.contains('This user is the owner of one or more open Opportunities.'), 
            'Trigger should throw a custom exception for open Opportunity Ownership The caught exception is: ' + errorThrown
        );
    }    

    @isTest
    static void testPreventDeleteOpportunitySolutionSales(){

        Map<String, User> adminsByNumber = TestUtils.createAdmins(3);
        User setNewProfile;

        System.runAs(adminsByNumber.get('Admin3')){
            setNewProfile = [select id, profileId from user where id =: adminsByNumber.get('Admin1').Id];
            Profile prof = [select id from Profile where name = 'VF Sales Manager'];
            setNewProfile.ProfileId = prof.Id;
            update setNewProfile;
        }

        System.runAs(adminsByNumber.get('Admin2')){
            Account acct = TestUtils.createAccount(GeneralUtils.currentUser);
            Pricebook2 pb = TestUtils.createPricebook(false);
            Ban__c ban = TestUtils.createBan(acct);

            TestUtils.autoCommit = false;
            Opportunity Opp1 = TestUtils.createOpportunityWithBan(acct, pb.id, ban, GeneralUtils.currentUser);
            Opportunity Opp2 = TestUtils.createOpportunityWithBan(acct, pb.id, ban, GeneralUtils.currentUser);

            Opp1.Solution_Sales__c = setNewProfile.Id;
            Opp2.Solution_Sales__c = setNewProfile.Id;

            insert Opp1;
            insert Opp2;
        }

        //User toDeactivate = [select Id, IsActive from User where Id =: adminsByNumber.get('Admin1').Id];

        String errorThrown;
        Test.startTest();
            setNewProfile.IsActive = false;
            try {
                System.runAs(adminsByNumber.get('Admin3')){
                    TriggerHandler.resetRecursion('UserTriggerHandler');
                    update setNewProfile;
                }
            } catch (Exception e) {
                errorThrown = e.getMessage() + e.getStackTraceString();
            }
        Test.stopTest();

        System.assert(
            errorThrown != null && errorThrown.contains('This user is the solution sales of one or more open Opportunities.'), 
            'Trigger should throw a custom exception for open Opportunity Solution Sales. The caught exception is: ' + errorThrown
        );
    }   

    /*@isTest
    static void testPreventDeleteLeadOwner(){
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);  
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
        Lead l1;
        Lead l2;

        System.runAs(adminsByNumber.get('Admin1')){
            TestUtils.autoCommit = false;
            l1 = TestUtils.createLead();
            l2 = TestUtils.createLead();

            insert l1;
            insert l2;

            l1.OwnerId = adminsByNumber.get('Admin1').Id;
            l2.OwnerId = adminsByNumber.get('Admin1').Id;

            update l1;
            update l2;
        }

        User toDeactivate = [select Id, IsActive, name from User where Id =: adminsByNumber.get('Admin1').Id];
        String errorThrown;

        Test.startTest();
            toDeactivate.IsActive = false;
            try {
                System.runAs(adminsByNumber.get('Admin2')){
                    update toDeactivate;
                }
            } catch (Exception e) {
                errorThrown = e.getMessage() + e.getStackTraceString();
            }
        Test.stopTest();

        System.assert(
            errorThrown != null && errorThrown.contains('This user is the owner of one or more open Leads.'), 
            'Trigger should throw a custom exception for open Opportunity Solution Sales. The caught exception is: ' + errorThrown
        );
    }*/  
     
    @isTest 
    static void testPreventDeleteManager(){

        Map<String, User> adminsByNumber = TestUtils.createAdmins(4);

        System.runAs(adminsByNumber.get('Admin1')){
            adminsByNumber.get('Admin2').ManagerId = adminsByNumber.get('Admin1').Id;
            adminsByNumber.get('Admin3').ManagerId = adminsByNumber.get('Admin1').Id;
            update adminsByNumber.values();
        }

        String errorThrown;
        Test.startTest();
            TriggerHandler.resetRecursion('UserTriggerHandler');
            adminsByNumber.get('Admin1').IsActive = false;
            try {
                System.runAs(adminsByNumber.get('Admin4')){
                    update adminsByNumber.get('Admin1');
                }
            } catch (Exception e) {
                errorThrown = e.getMessage() + e.getStackTraceString();
            }
        Test.stopTest();

        System.assert(
            errorThrown != null && errorThrown.contains('This user is the manager of one or more other users.'), 
            'Trigger should throw a custom exception for open Opportunity Solution Sales. The caught exception is: ' + errorThrown
        );
    }   

    @isTest(SeeAllData='true') 
    static void testPreventDeleteDashboardRunningUser(){
        AggregateResult multipleDashboards;
        list<AggregateResult> dashboardsByRunningUser = [select RunningUserId, Count(Id) FROM Dashboard Where RunningUser.IsActive = true GROUP BY RunningUserId];

        For (AggregateResult ar : dashboardsByRunningUser){
            if (Integer.valueOf(ar.get('expr0')) >= 2) {
                multipleDashboards = ar;    
            }
        }
        if (multipleDashboards != null) {
            User toDeactivate = [select Id, IsActive from User where Id =: String.valueOf(multipleDashboards.get('RunningUserId'))];

            String errorThrown;
            Test.startTest();
                toDeactivate.IsActive = false;
                try {
                    update toDeactivate;
                } catch (Exception e) {
                    errorThrown = e.getMessage() + e.getStackTraceString();
                }
            Test.stopTest();
            System.assert(
                errorThrown != null && errorThrown.contains('This user is the running user on one or more dashboards.'), 
                'Trigger should throw a custom exception for being dashboard running user. The caught exception is: ' + errorThrown
            );            
        }
    }

    @isTest
    static void testCheckLicensesZeroLicense(){
        Account partnerAccount;
        
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount = TestUtils.createPartnerAccount();   
            partnerAccount.IsPartner = true;
            partnerAccount.Total_Licenses__c = 0;           
            update partnerAccount;
        }  
        System.runAs(adminsByNumber.get('Admin2')){                     
            String error;
            try {
                User testPortalUser = TestUtils.createPortalUser(partnerAccount);
            } catch (Exception e) {
                error = e.getMessage() + e.getStackTraceString();
            }
            System.assert(
                error != null && error.contains('There are no available licenses left.'), 
                'Trigger should throw an error. The caught exception is: ' + error
            );
        } 
    }

    @isTest
    static void testCheckLicensesNull(){
        Account partnerAccount;
        
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount = TestUtils.createPartnerAccount();   
            partnerAccount.IsPartner = true;
            partnerAccount.Total_Licenses__c = null;           
            update partnerAccount;
        }  
        System.runAs(adminsByNumber.get('Admin2')){
            String error;
            try {
                User testPortalUser = TestUtils.createPortalUser(partnerAccount);
            } catch (Exception e) {
                error = e.getMessage();
            }
            System.assert(
                error != null && error.contains('There are no licenses defined.'),
                'Trigger should throw an error. The caught exception is: ' + error
            );
        } 
    }

    @isTest
    static void testCheckLicensesUpdateNull(){
        Account partnerAccount;
        User testPortalUser;

        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount = TestUtils.createPartnerAccount();   
        }
        
        System.runAs(adminsByNumber.get('Admin2')){
            testPortalUser = TestUtils.createPortalUser(partnerAccount);
        }

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount.IsPartner = true;
            partnerAccount.Total_Licenses__c = null;           
            update partnerAccount;
        }

        TriggerHandler.resetRecursion();
        System.runAs(adminsByNumber.get('Admin2')){
            String error;
            try {
                testPortalUser.isActive = false;
                update testPortalUser;
            } catch (Exception e) {
                error = e.getMessage() + e.getStackTraceString();
            }
            System.assert(
                error != null && error.contains('There are no licenses defined.'), 
                'Trigger should throw an error. The caught exception is: ' + error
            );
        }
    }

    @isTest
    static void testCheckLicensesUpdateZero(){
        Account partnerAccount;
        User testPortalUser;

        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 1);
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);

        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount = TestUtils.createPartnerAccount();   
        }
        
        System.runAs(adminsByNumber.get('Admin2')){
            testPortalUser = TestUtils.createPortalUser(partnerAccount);
        }

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount.IsPartner = true;
            partnerAccount.Total_Licenses__c = 0;           
            update partnerAccount;
        }

        System.runAs(adminsByNumber.get('Admin2')){
            String error;
            testPortalUser.isActive = false;
            update testPortalUser;

            TriggerHandler.resetRecursion('UserTriggerHandler');
            try {
                testPortalUser.isActive = true;
                update testPortalUser;
            } catch (Exception e) {
                error = e.getMessage() + e.getStackTraceString();
            }
            System.assert(
                error != null && error.contains('There are no available licenses left.'), 
                'Trigger should throw an error. The caught exception is: ' + error
            );
        }
    }

/*     @isTest static void testCheckLicensesDeactivatePortalUser(){
        Account partnerAccount;
        User testPortalUser;

        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        //Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

        UserRole AdminRole = [Select Id From UserRole Where PortalType = 'None' and Name = 'Administrator' Limit 1];
        Profile adminProfile = [SELECT ID FROM Profile WHERE Name = 'VF System Admin Pardot' LIMIT 1];


        User deactivator = TestUtils.generateTestUser('fName', 'lName', adminProfile.Id, AdminRole.Id);
        User admin1 = TestUtils.generateTestUser('fName', 'lName', adminProfile.Id, AdminRole.Id);
        User admin2 = TestUtils.generateTestUser('fName', 'lName', adminProfile.Id, AdminRole.Id);        

        System.runAs(admin1){
            partnerAccount = TestUtils.createPartnerAccount(); 
        }

        System.runAs(admin2){
            testPortalUser = TestUtils.createPortalUser(partnerAccount);
        }

        String error;

        System.debug('deactivator   ' + deactivator);
        TriggerHandler.resetRecursion('UserTriggerHandler');
        
        Test.startTest();
            System.runAs(deactivator){
                try {
                    //testPortalUser.IsPortalEnabled = false;
                    testPortalUser.isActive = false;
                    update testPortalUser;                    
                } catch (Exception e) {
                    error = e.getMessage();
                }
            }
        Test.stopTest();
            

        System.debug('error   '+   error);
        System.assert(error.contains('You can\'t deactivate a user via this button'), 
                                        'Error should be thrown for deactivating a Portal user with the wrong profile');
    } */

    @isTest 
    static void testCheckPrimaryUserUpdate(){
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
        User primaryUser1;
        User primaryUser2;
        Account partnerAccount;
        Contact portalContact1;
        Contact portalContact2;

        System.runAs(adminsByNumber.get('Admin1')){
            TestUtils.autoCommit = false;
            partnerAccount = TestUtils.createPartnerAccount();
            partnerAccount.Dealer_code__c = '945312';
            insert partnerAccount;
            TestUtils.autoCommit = true;
            portalContact1 = TestUtils.createContact(partnerAccount);
            portalContact2 = TestUtils.createContact(partnerAccount);
        }

        System.runAs(adminsByNumber.get('Admin2')){
            TestUtils.autoCommit = false;

            primaryUser1 = TestUtils.createPortalUser(partnerAccount);
            primaryUser1.ContactId = portalContact1.Id;
            primaryUser1.Primary_Partner__c = true;

            primaryUser2 = TestUtils.createPortalUser(partnerAccount);
            primaryUser2.ContactId = portalContact2.Id;

            TriggerHandler.resetRecursion('UserTriggerHandler');
            insert primaryUser1;
            insert primaryUser2;

            TriggerHandler.resetRecursion('UserTriggerHandler');

            primaryUser2.Primary_Partner__c = true;
        }

        String error;

        Test.startTest();
            System.runAs(adminsByNumber.get('Admin2')){
                try {
                    update primaryUser2;                    
                } catch (Exception e) {
                    error = e.getMessage();
                }
            }
        Test.stopTest();

        System.assert(
            error != null && error.contains('There can only be one Primary Partner User'), 
            'Error should be thrown for making a second primary partner. The caught exception is: ' + error);
    }

    @isTest
    static void testCheckPrimaryUserInsert(){
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
        User primaryUser1;
        User primaryUser2;
        Account partnerAccount;
        Contact portalContact1;
        Contact portalContact2;

        System.runAs(adminsByNumber.get('Admin1')){
            TestUtils.autoCommit = false;
            partnerAccount = TestUtils.createPartnerAccount();
            partnerAccount.Dealer_code__c = '154365';
            insert partnerAccount;
            TestUtils.autoCommit = true;
            portalContact1 = TestUtils.createContact(partnerAccount);
            portalContact2 = TestUtils.createContact(partnerAccount);
        }

        System.runAs(adminsByNumber.get('Admin2')){
            TestUtils.autoCommit = false;

            primaryUser1 = TestUtils.createPortalUser(partnerAccount);
            primaryUser1.ContactId = portalContact1.Id;
            primaryUser1.Primary_Partner__c = true;

            primaryUser2 = TestUtils.createPortalUser(partnerAccount);
            primaryUser2.ContactId = portalContact2.Id;
            primaryUser2.Primary_Partner__c = true;
        }

        String error;

        Test.startTest();
            System.runAs(adminsByNumber.get('Admin2')){
                try {
                    TriggerHandler.resetRecursion('UserTriggerHandler');
                    insert primaryUser1;
                    insert primaryUser2;                   
                } catch (Exception e) {
                    error = e.getMessage();
                }
            }
        Test.stopTest();
        System.debug('Error    ' + error);
        System.assert(
            error != null && error.contains('There can only be one Primary Partner User'), 
            'Error should be thrown for making a second primary partner. The caught exception is: ' + error
        );
    }

    @isTest
    static void testAssignPermissionsSetToPartnerInsert(){
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
        Account partnerAccount;
        Contact portalContact;
        User testPortalUser;

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount = TestUtils.createPartnerAccount();
            portalContact = TestUtils.createContact(partnerAccount);
        }
        
        Test.startTest();
            System.runAs(adminsByNumber.get('Admin2')){
                TestUtils.autoCommit = false;
                testPortalUser = TestUtils.createPortalUser(partnerAccount);
                testPortalUser.ContactId = portalContact.Id;
                testPortalUser.Admin_Partner__c = true;
                insert testPortalUser;
            }
        Test.stopTest();

        list<PermissionSetAssignment> pSetAssignment = [select id from PermissionSetAssignment where PermissionSet.Name = 'Delegated_External_User_Administrator' and AssigneeId =: testPortalUser.Id]; 

        System.assertEquals(1, pSetAssignment.size(), 'PermissionSet should be assigned to user');
    }

    @isTest
    static void testAssignPermissionsSetToPartnerUpdate(){
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
        Account partnerAccount;
        User testPortalUser;

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount = TestUtils.createPartnerAccount();
        }

        System.runAs(adminsByNumber.get('Admin2')){
            testPortalUser = TestUtils.createPortalUser(partnerAccount);
        }
        TriggerHandler.resetRecursion();
        Test.startTest();
            System.runAs(adminsByNumber.get('Admin2')){
                testPortalUser.Admin_Partner__c = true;
                update testPortalUser;
            }
        Test.stopTest();

        list<PermissionSetAssignment> pSetAssignment = [select id from PermissionSetAssignment where PermissionSet.Name = 'Delegated_External_User_Administrator' and AssigneeId =: testPortalUser.Id]; 

        System.assertEquals(1, pSetAssignment.size(), 'PermissionSet should be assigned to user');
    }

    @isTest
    static void testAssignPermissionsSetToPartnerDelete(){
        Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
        Account partnerAccount;
        Contact portalContact;
        User testPortalUser;

        System.runAs(adminsByNumber.get('Admin1')){
            partnerAccount = TestUtils.createPartnerAccount();
            portalContact = TestUtils.createContact(partnerAccount);
        }
        
        System.runAs(adminsByNumber.get('Admin2')){
            TestUtils.autoCommit = false;
            testPortalUser = TestUtils.createPortalUser(partnerAccount);
            testPortalUser.ContactId = portalContact.Id;
            testPortalUser.Admin_Partner__c = true;
            insert testPortalUser;
        }

        Test.startTest();
            TriggerHandler.resetRecursion('UserTriggerHandler');
            testPortalUser.Admin_Partner__c = false;
            update testPortalUser;
        Test.stopTest();

        list<PermissionSetAssignment> pSetAssignment = [select id from PermissionSetAssignment where PermissionSet.Name = 'Delegated_External_User_Administrator' and AssigneeId =: testPortalUser.Id]; 

        System.assertEquals(0, pSetAssignment.size(), 'PermissionSet should be deleted for user');
    }
}