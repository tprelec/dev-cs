@isTest
private class TestUserTriggerHandlerBatch {
	
	@isTest static void testFreeCPQLicense() {
		// Implement test code
		Account partnerAccount = TestUtils.createPartnerAccount();
    	User partnerUser = TestUtils.createPortalUser(partnerAccount);	

    	BigMachines__Configuration_Record__c cpqCOnfig = New BigMachines__Configuration_Record__c(
    		BigMachines__action_id_copy__c='1', 
    		BigMachines__action_id_open__c='1', 
    		BigMachines__bm_site__c='1', 
    		BigMachines__document_id__c='1', 
    		BigMachines__process__c='1', 
    		BigMachines__process_id__c='1', 
    		BigMachines__version_id__c='1'
    	);
    	insert cpqCOnfig;

    	Test.StartTest();
    	Set<Id> newUserIds = new Set<Id>(); 
    	newUserIds.Add(partnerUser.Id);

        System.enqueueJob(new UserTriggerHandlerBatch(newUserIds));

    	Test.StopTest();
	}
	
	
	
}