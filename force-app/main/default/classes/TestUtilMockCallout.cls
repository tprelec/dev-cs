/**
 * @File Name          : TestUtilMockCallout.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 4/15/2020, 6:37:59 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    4/15/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@isTest
global class TestUtilMockCallout implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        // salesforce rest api getDeleted functionality
        if(req.getEndpoint().contains('/deleted/')){
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        // make sure the response is different for each object
			String variableString = StringUtils.randomString(18);
	        
	        res.setBody('{"deletedRecords":[{"deletedDate":"2016-12-13T11:32:31.000+0000","id":"'+variableString+'"}],"earliestDateAvailable":"2016-11-07T12:49:00.000+0000","latestDateCovered":"2016-12-13T12:03:00.000+0000"}');
	        res.setStatusCode(200);
	        return res;

        } 
        
        if(req.getbody().contains('Free_field1')){
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        // make sure the response is different for each object
			String variableString = StringUtils.randomString(18);
	        res.setBody('{"Contacts":[{"bevoegd_functionaris_voornaam":"Contact Test 1","bevoegd_functionaris_achternaam":"Last Name","mobielnummer":"1234567890"}],"kvk_8":"12345678","aantalmedewerkers":"50","bedrijfsnaam":"Account Test 1"}');
	        res.setStatusCode(200);
	        return res;
        } 
        if(req.getbody().contains('Zipcode1')){
	        // Create a fake response
	        HttpResponse res = new HttpResponse();
	        res.setHeader('Content-Type', 'application/json');
	        // make sure the response is different for each object
			String variableString = StringUtils.randomString(18);
	        res.setBody('{"straatnaam":"Street","huisnummer":"12","huisletter":"b","huisnummertoevoeging":"1","postcode":"1234AB","woonplaatsnaam":"Utrecht"}');
	        res.setStatusCode(200);
	        return res;
        }

        return null;

    }
}