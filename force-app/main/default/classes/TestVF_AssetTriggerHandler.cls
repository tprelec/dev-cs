/**
 * @description		This is the test class for VF_AssetTriggerHandler class
 * @author        	Guy Clairbois
 */
@isTest
public with sharing class TestVF_AssetTriggerHandler {

    static testMethod void verifyCorrectVFAssetAndAccountUpdates() {
        User admin = TestUtils.createAdministrator();
        User dealer = TestUtils.createDealer();

        List<Contact> contactsList = new List<Contact>();
        List<VF_Asset__c> assetsList = new List<VF_Asset__c>();
        
        Account acc     = new Account();
        acc.Name        = 'test1';
        acc.OwnerId     = dealer.Id;
        insert acc;
        
        contactsList.add(new Contact(AccountId = acc.Id, 
                                     LastName = 'TestLastname', 
                                     FirstName = 'TestFirstname',
                                     Userid__c = dealer.Id,
                                     isactive__c = True));
        contactsList.add(new Contact(AccountId = acc.Id, 
                                     LastName = 'parentTestLastname', 
                                     FirstName = 'parentTestFirstname',
                                     Userid__c = admin.Id,
                                     isactive__c = True));
        insert contactsList;
        
        String random4charNumericString =  String.valueOf(Math.mod(Math.round(Math.random()*1000),1000)+1000);
        String random4charNumericString2 =  String.valueOf(Math.mod(Math.round(Math.random()*1000),1000)+1000);
        Dealer_Information__c newDealerParent = TestUtils.createDealerInformation(contactsList[1].Id, '99'+random4charNumericString);
        Dealer_Information__c newDealer = TestUtils.createDealerInformation(contactsList[0].Id, '99'+random4charNumericString2, '99'+random4charNumericString);
        
        Test.startTest();
        //to have a contract created
        VF_Asset__c initContract = createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', false);
        insert initContract;
        
        //found -> retention -> status (on Insert)
        //contract found/status Active/not retention
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', false));
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', false));
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', false));
        //contract not found/status Active/not retention
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '2', 'Active', '399999932', false));
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '2', 'Active', '399999932', false));
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '2', 'Active', '399999932', false));
        //contract found/status Active/ retention
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', true));
        //contract not found/status Active/ retention
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '3', 'Active', '399999932', true));
        //contract found/status Suspended/not retention
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Suspended', '399999932', false));
        //contract not found/status Suspended/not retention
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '4', 'Suspended', '399999932', false));
        //contract found/status Suspended/ retention
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Suspended', '399999932', true));
        //contract not found/status Suspended/ retention
        assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '5', 'Suspended', '399999932', true));
        insert assetsList;
        
        // contracts 1, 2 and 3 created
        //found -> retention -> status (on Update)
        //contract found/status Active/not retention
        assetsList[0].Contract_Number__c = '2';
        //contract not found/status Active/not retention
        assetsList[1].Contract_Number__c = '10';
        //contract found/status Active/ retention
        assetsList[2].Contract_Number__c = '2';
        assetsList[2].Retention_Date__c = system.today().addDays(2);
        //contract not found/status Active/ retention
        assetsList[3].Contract_Number__c = '11';
        assetsList[3].Retention_Date__c = system.today().addDays(2);
        //contract found/status Suspended/not retention
        assetsList[4].Contract_Number__c = '2';
        //contract not found/status Suspended/not retention
        assetsList[5].Contract_Number__c = '12';
        //contract found/status Suspended/ retention
        assetsList[6].Contract_Number__c = '2';
        assetsList[6].Retention_Date__c = system.today().addDays(2);
        //contract not found/status Suspended/ retention
        assetsList[7].Contract_Number__c = '13';
        assetsList[7].Retention_Date__c = system.today().addDays(2);
        update assetsList;

        System.assertNotEquals(null, [SELECT Id, Error__c, Contract_VF__c FROM VF_Asset__c WHERE Id = :assetsList[5].Id].Contract_VF__c);

        delete initContract;
        Test.stopTest();
    }
    
    public static VF_Asset__c createAsset(Id AccId, String dealerCode, String contractNumber, String ctnStatus, String banNumber, Boolean isRetention) 
    {
        VF_Asset__c newAsset = new VF_Asset__c();
        newAsset.Account__c = AccId;
        newAsset.Initial_dealer_code__c = dealerCode;
        newAsset.Contract_Number__c = contractNumber;
        newAsset.CTN_Status__c = ctnStatus;
        newAsset.BAN_Number__c = banNumber;
        newAsset.Contract_Start_Date__c = system.today().addMonths(-6);
        newAsset.Contract_End_Date__c = newAsset.Contract_Start_Date__c.addMonths(6);
        If (isRetention){
            newAsset.Retention_Date__c = system.today();
            newAsset.Retention_Dealer_code__c = dealerCode;
            newAsset.Contract_Start_Date__c = system.today();
            newAsset.Contract_End_Date__c = newAsset.Contract_Start_Date__c.addMonths(12);
        }
        return newAsset;
    }
    
}