/*
 *	@Description:	Test class for VF_ContractAttachmentManagerController
 *	@Author:		Guy Clairbois
 */
@isTest
private class TestVF_ContractAttachmentManagerCtrl {

	static void test_method_no_contract() {

		// go to screen
		PageReference pr = new PageReference('/apex/VF_ContractAttachmentManager');
		Test.setCurrentPage(pr);

		// open attachmentmgr
		VF_ContractAttachmentManagerController vfcamc = new VF_ContractAttachmentManagerController();
	}

	@isTest()
	static void test_method_success() {
		// create contract
		TestUtils.createCompleteContract();

		Test.startTest();

		// add existing attachment
		Contract_Attachment__c ca = new Contract_Attachment__c();
		ca.Attachment_Type__c = 'Contract (Signed)';
		ca.Contract_VF__c = TestUtils.theContract.Id;
		insert ca;

		Blob xmlBlob = Blob.valueOf('Hello World');
		ContentVersion cv = new ContentVersion();
		cv.Title = 'Provisining XML';
		cv.PathOnClient = 'Provisining_XML.xml';
		cv.VersionData = xmlBlob;
		cv.FirstPublishLocationId = ca.id; // Record to link to
		insert cv;

		cv = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id];

		// go to screen
		PageReference pr = new PageReference('/apex/VF_ContractAttachmentManager?contractId='+TestUtils.theContract.Id);
		Test.setCurrentPage(pr);

		// open attachmentmgr
		VF_ContractAttachmentManagerController vfcamc = new VF_ContractAttachmentManagerController();

		// add more attachments on screen
		vfcamc.addMore();

		// add attachment
		VF_ContractAttachmentManagerController.AttachmentWrapper aw = vfcamc.newContractAttachments[0];
		aw.ca = ca.clone();
		aw.file = cv.clone();
		aw.file.Title = 'Title';
		aw.file.PathOnClient = 'test.txt';
		aw.file.VersionData = xmlBlob;

		// save
		vfcamc.saveAttachments();

		// delete attachment
		Id caToDelete = [Select Id From Contract_Attachment__c Where Contract_VF__c = :testUtils.theContract.Id Limit 1][0].Id;
		vfcamc.SelectedCaId = caToDelete;
		vfcamc.DeleteContractAttachment();

		// return to contract
		vfcamc.backToContract();

		// verify that 1 attachment remains
		List<Contract_Attachment__c> caRemaining = [Select Id From Contract_Attachment__c Where Contract_VF__c = :testUtils.theContract.Id];

		system.AssertEquals(1,caRemaining.size());

		FeedItem fi = aw.getHiddenFeedItem();
		pr = vfcamc.manageAttachments();
		pr = vfcamc.nothing();
		List<SelectOption> sco = vfcamc.signedContractOptions;
		Test.stopTest();
	}
}