@isTest
private class TestVF_ContractBanManagerController {
	@isTest static void test_method_success() {
        VF_Contract__c TheContract = new VF_Contract__c();

        // dismantled createCompleteContract
		User owner = TestUtils.generateTestUser('admin', 'user', 'System Administrator');

        System.runAs(owner) {
            Account acct = TestUtils.createAccount(owner);
            Ban__c ban = TestUtils.createBan(acct);
            OrderType__c ot = TestUtils.createOrderType();
            Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId() ,ban);
            User dealer = TestUtils.createDealer();
            OpportunityTeamMember teamMember = new OpportunityTeamMember();
            teamMember.OpportunityAccessLevel = 'Edit';
            teamMember.OpportunityId = opp.Id;
            teamMember.UserId = dealer.Id;
            insert teamMember;
            Site__c site = TestUtils.createSite(acct);

            //disable auto commit for the TestUtils class so we can insert Lists instead of entries one-by-one
            TestUtils.autoCommit = false;
            Integer nrOfRows = 2;

            List<Product2> products = new List<Product2>();
            for (Integer i = 0; i < nrOfRows; i++) {
                products.add(TestUtils.createProduct());
            }
            insert products;

            List<PricebookEntry> pbEntries = new List<PricebookEntry>();
            for (Integer i = 0; i < nrOfRows; i++) {
                pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId() , products.get(i)));
            }
            insert pbEntries;

            Decimal totalAmount = 0.0;
            List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
            for (Integer i = 0; i < nrOfRows; i++) {
                OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp, pbEntries.get(i));
                oppLineItem.Site_List__c = site.Id;
                oppLineItems.add(oppLineItem);
                totalAmount += oppLineItem.Product_Arpu_Value__c * oppLineItem.Duration__c * oppLineItem.Quantity;
            }
            insert oppLineItems;

            // create Contract
            TheContract.Account__c = acct.Id;
            TheContract.Opportunity__c = opp.id;
            insert TheContract;

            Decimal value = 10;
            Integer quantity = 4;
            Contracted_Products__c cp = new Contracted_Products__c (
                Quantity__c = quantity,
                UnitPrice__c = 10,
                Arpu_Value__c = 10,
                Duration__c = 1,
                Product__c = products[0].Id,
                VF_Contract__c = TheContract.Id,
                Site__c = site.Id
            );
            insert cp;

            test.startTest();
            // go to screen
            PageReference pr = new PageReference('/apex/VF_ContractBanManager?contractId='+ TheContract.Id);
            Test.setCurrentPage(pr);

            // open attachmentmgr
            ApexPages.StandardController controller = new ApexPages.Standardcontroller(TheContract);
            VF_ContractBanManagerController vfcbmc = new VF_ContractBanManagerController(controller);

            vfcbmc.updateBan();
            vfcbmc.cancelUpdateBan();

            //insert contract attachment
            Contract_Attachment__c ca = new Contract_Attachment__c();
            ca.Attachment_Type__c = 'Number List';
            ca.Contract_VF__c = TheContract.Id;
            insert ca;

            //update contract sign date
            TheContract.Contract_Sign_Date__c = date.today().addDays(2);
            update TheContract;

            Delete TheContract;
            test.stopTest();
        }
	}
}