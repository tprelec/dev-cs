@isTest
private class TestVF_ContractTriggerHandler {

    @testSetup
    private static void makeData() {
        TestUtils.createAccount(null);
        TestUtils.createOpportunity(TestUtils.theAccount, Test.getStandardPricebookId());
        VF_Contract__c c = TestUtils.createVFContract(TestUtils.theAccount, TestUtils.theOpportunity);

        Contact owner = new Contact(AccountId = TestUtils.theAccount.Id, LastName = 'Current', FirstName = 'User', UserId__c = TestUtils.theAccountManager.Id);
        insert owner;
        Dealer_Information__c dealerInfoCU = TestUtils.createDealerInformation(owner.Id, '123322');
        c.Dealer_Information__c = dealerInfoCU.Id;
        update c;
    }

    @isTest
    private static void createSharingRulesFromOpportunity() {
        Account a = [SELECT Id FROM Account LIMIT 1];
        Opportunity o = [SELECT Id, OwnerId FROM Opportunity WHERE AccountId = :a.Id LIMIT 1];
        insert new OpportunityTeamMember(
            UserId = UserInfo.getUserId(),
            OpportunityId = o.Id
        );
        VF_Contract__c c = TestUtils.createVFContract(a, o);
        UserRecordAccess rec = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :c.Id LIMIT 1];
        System.assertEquals(true, rec.HasEditAccess);
    }

    @isTest
    private static void adjustAccountSharing() {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        VF_Contract__c c = [SELECT Id, Account__c, Dealer_Information__c, OwnerId FROM VF_Contract__c LIMIT 1];
        Account partnerAcc = TestUtils.createPartnerAccount();
        User portalUser = TestUtils.createPortalUser(partnerAcc);
        Test.startTest();
        update new Contact(Id = TestUtils.thePortalContact.Id, UserId__c = portalUser.Id);
        Dealer_Information__c dealerInfoPU = TestUtils.createDealerInformation(TestUtils.thePortalContact.Id, '123324');
        c.Dealer_Information__c = dealerInfoPU.Id;
        update c;
        System.assertEquals(portalUser.Id, [SELECT OwnerId FROM VF_Contract__c WHERE Id = :c.Id].OwnerId);
        UserRecordAccess rec = [SELECT RecordId, HasReadAccess FROM UserRecordAccess WHERE UserId = :portalUser.Id AND RecordId = :c.Account__c LIMIT 1];
        System.assertEquals(true, rec.HasReadAccess);

        c.OwnerId = UserInfo.getUserId();
        update c;
        System.assertEquals(portalUser.Id, [SELECT OwnerId FROM VF_Contract__c WHERE Id = :c.Id].OwnerId);
        Test.stopTest();
    }

    @isTest
    private static void updateFrameworkAgreementDateAndVersionOnAccount() {
        GenericMock mock = new GenericMock();
        mock.returns.put('updateFrameworkAgreementDateAndVersionForNewContract', null);
        AccountService.instance = (AccountService)Test.createStub(AccountService.class, mock);

        Account a = [SELECT Id FROM Account LIMIT 1];
        VF_Contract__c contract = [SELECT Id, Contract_Sign_Date__c FROM VF_Contract__c WHERE Account__c = :a.Id LIMIT 1];
        contract.Contract_Sign_Date__c = Date.newInstance(2000, 10, 27);
        update contract;

        System.assertEquals(1, mock.callCount.get('updateFrameworkAgreementDateAndVersionForNewContract'));
        Map<Id, Date> passedArg = (Map<Id, Date>) mock.args.get('updateFrameworkAgreementDateAndVersionForNewContract')[0];
        System.assertEquals(true, passedArg.containsKey(a.Id));
        System.assertEquals(contract.Contract_Sign_Date__c, passedArg.get(a.Id));
    }

    //Added for coverage, assertions should still one day be added.
    @isTest
    private static void revertContractAttachments() {
        VF_Contract__c contract = [SELECT Id, Contract_Sign_Date__c FROM VF_Contract__c LIMIT 1];
        insert new Contract_Attachment__c(Contract_VF__c = contract.Id, Attachment_Type__c = 'Contract (Signed)');
        delete contract;
    }

    @IsTest
    static void testContractCreationInOrchestrationProcess() {
        List<Profile> pList = CS_DataTest.returnSystemAdminProfile();
        List<UserRole> roleList = CS_DataTest.returnUserRoleWithoutParentRole();

        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs (simpleUser) {
            VF_Contract__c vfContract1 = CS_DataTest.createDefaultVfContract(simpleUser, true);
            vfContract1.Total_Connection__c = 20;
            upsert vfContract1;

            List<CSPOFA__Orchestration_Process__c> processList = new List<CSPOFA__Orchestration_Process__c>([
                SELECT Id
                FROM CSPOFA__Orchestration_Process__c
                WHERE Contract_VF__c = :vfContract1.Id
            ]);

            System.assertNotEquals(1, processList.size());
        }
    }

    private class GenericMock implements System.StubProvider {
        private Map<String, Object> returns = new Map<String, Object>();
        private Map<String, Integer> callCount = new Map<String, Integer>();
        private Map<String, List<Object>> args = new Map<String, List<Object>>();

        public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, List<Object> listOfArgs) {
            if (!callCount.containsKey(stubbedMethodName)) {
                callCount.put(stubbedMethodName, 0);
            }
            callCount.put(stubbedMethodName, callCount.get(stubbedMethodName) + 1);
            if (!args.containsKey(stubbedMethodName)) {
                args.put(stubbedMethodName, new List<Object>());
            }
            args.put(stubbedMethodName, listOfArgs);
            return returns.get(stubbedMethodName);
        }
    }
}