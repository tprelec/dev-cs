/**
 *  @description    This class contains unit tests for the VF_ProductTriggerHandler class
 *  @Author         Guy Clairbois
 */
@isTest
private class TestVF_ProductTriggerHandler {
    
    @isTest() static void testCreateUpdateDeleteVF_Product(){

        OrderType__c ot = TestUtils.createOrderType();
        
        VF_Product__c p = new VF_Product__c();
        p.Name = 'Test productVF';
        p.Brand__c = 'Testing Brand';
        p.Cost_Price__c = 11;
        p.Description__c = 'This is a test product';
        p.Active__c = true;
        p.ProductCode__c = 'TEST_PRODUCT_CODE_VF';
        p.Product_Group__c = 'Priceplan';
        p.Quantity_type__c = 'Monthly';
        p.SDC_Product_matrix__c = 'Fixed - Data';
        p.Product_Line__c = 'fZiggo Only';
        p.OrderType__c = ot.id;
        p.ExternalID__c = 'VFP-02-ABCDEFG';

        insert p; 

        p.Description__c = 'not';
        update p;

        delete p;
        System.assert(true, 'dummy assertion');
    }   

    @isTest() static void testCreateUpdateVF_ProductWithSplit(){

        OrderType__c ot = TestUtils.createOrderType();
        
        VF_Product__c p = new VF_Product__c();
        p.Name = 'Test productVF';
        p.Brand__c = 'Testing Brand';
        p.Cost_Price__c = 11;
        p.Description__c = 'This is a test product';
        p.Active__c = true;
        p.ProductCode__c = 'TEST_PRODUCT_CODE_VF';
        p.Product_Group__c = 'Priceplan';
        p.Quantity_type__c = 'Monthly';
        p.SDC_Product_matrix__c = 'Fixed - Data';
        p.Split_Product_Family_into_Ret_Acq_Churn__c = true;
        p.Product_Line__c = 'fZiggo Only';
        p.OrderType__c = ot.id;
        p.ExternalID__c = 'VFP-02-ABCDEFGH';

        insert p; 

        System.assert(true, 'dummy assertion');
    }       
}