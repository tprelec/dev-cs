@isTest
public class TestWS_GetHboInfo {

    @isTest
    public static void testGetRecord() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/get_HBO_info'; //Request URL
        req.httpMethod = 'GET'; //HTTP Request Type
        RestContext.request = req;
        RestContext.response = res;
        
        WS_GetHboInfo.getRecord();
        System.assertEquals(RestContext.response.statuscode, 400, 'Required params are not set.');
    }
    
    @isTest
    public static void testGetRecordSite() {        
        prepareData();
        // fail 404
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/get_HBO_info';
        req.params.put('orderNo', 'O-012345.1');
        req.params.put('zipcode', '1234AB');
        req.params.put('houseNo', '1');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_GetHboInfo.getRecord();
        System.assertEquals(RestContext.response.statuscode, 404, 'Site doesn\'t exist.');
    }
    
    @isTest
    public static void testGetRecordOrder() {        
        prepareData();
        // fail 404
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/get_HBO_info';
        req.params.put('orderNo', 'O-012345.3');
        req.params.put('zipcode', '1234AB');
        req.params.put('houseNo', String.valueOf(TestUtils.theSite.Site_House_Number__c));
        req.params.put('houseNoExt', String.valueOf(TestUtils.theSite.Site_House_Number_Suffix__c));
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_GetHboInfo.getRecord();
        System.assertEquals(RestContext.response.statuscode, 404, 'Order doesn\'t exist.');
    }
    
    @isTest
    public static void testGetRecordHBO() {        
        prepareData();
        // fail 404
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        Order__c o = getOrder();
        req.requestURI = '/services/apexrest/get_HBO_info';
        req.params.put('orderNo', o.BOP_Export_Order_Id__c);
        req.params.put('zipcode', '1234AB');
        req.params.put('houseNo', String.valueOf(TestUtils.theSite.Site_House_Number__c));
        req.params.put('houseNoExt', TestUtils.theSite.Site_House_Number_Suffix__c);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_GetHboInfo.getRecord();
        System.assertEquals(RestContext.response.statuscode, 404, 'HBO doesn\'t exist.');
    }
    
    @isTest
    public static void testGetRecordSuccess() {        
        prepareData();
        createHBO();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        Order__c o = getOrder();
        req.requestURI = '/services/apexrest/get_HBO_info';
        req.params.put('orderNo', o.BOP_Export_Order_Id__c);
        req.params.put('zipcode', '1234AB');
        req.params.put('houseNo', String.valueOf(TestUtils.theSite.Site_House_Number__c));
        req.params.put('houseNoExt', TestUtils.theSite.Site_House_Number_Suffix__c);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        WS_GetHboInfo.getRecord();
        System.assertEquals(RestContext.response.statuscode, 200, 'Success');
    }
    
    private static void prepareData() {
        User owner = TestUtils.createAdministrator();
        Account acc = TestUtils.createAccount(owner);
        TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
        TestUtils.createSite(acc);
        VF_Contract__c contr = TestUtils.createVFContract(acc, TestUtils.theOpportunity);
        Order__c ord = TestUtils.createOrder(contr);
    }
    
    private static void createHBO() {
        System.runAs(TestUtils.theAdministrator) {
            Profile p = [SELECT Id FROM Profile WHERE Name = 'VF Indirect Inside Sales' LIMIT 1];
            UserRole ur = [SELECT ID, Name FROM UserRole WHERE DeveloperName = 'Network_Planner' LIMIT 1];
            User networkPlanner = TestUtils.generateTestUser('Network', 'Planner', p.Id, ur.Id);
            insert networkPlanner;
        }
        
        Site_Postal_Check__c spc = new Site_Postal_Check__c(
            Access_Site_ID__c = TestUtils.theSite.Id,
            Access_Active__c = false,
            Access_Vendor__c = 'ZIGGO',
            Access_Result_Check__c = 'OFFNET'
        );
        insert spc;
        
        User np = [
            SELECT Id
            FROM User 
            WHERE FirstName = 'Network' 
                AND LastName = 'Planner'
            LIMIT 1
        ];
        HBO__c hbo = new HBO__c(
            hbo_account__c = TestUtils.theAccount.Id,
            hbo_opportunity__c = TestUtils.theOpportunity.Id,
            hbo_status__c = 'New',
            hbo_site__c = TestUtils.theSite.Id,
            hbo_network_planner__c = np.Id,
            hbo_delivery_time__c = 12,
            hbo_digging_distance__c = 100,
            hbo_total_costs__c = 500,
            hbo_redundancy_type__c = 'hbo',
            hbo_redundancy__c = false,
            hbo_availability__c = 'Yes',
            hbo_postal_check__c = spc.Id
        );
        insert hbo;
        hbo.hbo_status__c = 'Approved';
        update hbo;
    }
    
    private static Order__c getOrder() {
        return [
            SELECT Id, BOP_Export_Order_Id__c 
            FROM Order__c 
            LIMIT 1
        ];
    }
}