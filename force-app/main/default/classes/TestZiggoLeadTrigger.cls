/**
 * Test class for Ziggo Lead creation, 
 * Date            2019-05  	W-001579 Marcel Vreuls: removed the authtenication from custom settings, now this is named credentials
 */
@IsTest
public class TestZiggoLeadTrigger {
    
    private static final String COMMENT_FIELD = '',
        OBJECT_API_NAME = Ziggo_Lead__c.sObjectType.getDescribe().getName(),
        ZIPCODE = TestZipcodeService.ZIPCODE,
        OTHER_ZIPCODE = TestZipcodeService.OTHER_ZIPCODE,
        PROVINCE = TestZipcodeService.PROVINCE;

    /**
     * TestSetup, creates:
     * - Custom setting
     * - Zipcode data
     * - Demographic data
     */
    @TestSetup
    static void prepareTest() {
        TestEinsteinDiscoveryUtil.discoveryTestSetup(OBJECT_API_NAME);
        createDemographics();
        createZipcode();
        
    }
    
    /**
     * Create zipcode data
     */
    private static void createZipcode() {
    	List<Zipcode_Info__c> ziList = new List<Zipcode_Info__c>();
        Zipcode_Info__c z = new Zipcode_Info__c();
        z.postalcode__c = ZIPCODE;
        z.lead_province_cat__c = PROVINCE;
        
        ziList.add(z);
        
        Zipcode_Info__c z2 = new Zipcode_Info__c();
        z2.postalcode__c = OTHER_ZIPCODE;
        z2.lead_province_cat__c = PROVINCE;
        ziList.add(z2);
        
        insert ziList;
    }
    
    /**
     * Create Demographic data
     */
    private static void createDemographics() {
    	List<Demographic__c> dList = new List<Demographic__c>();
        Demographic__c d = new Demographic__c();
        d.ein_pc4__c = ZIPCODE;
        dList.add(d);
        
        Demographic__c d2 = new Demographic__c();
        d2.ein_pc4__c = OTHER_ZIPCODE;
        dList.add(d2);
        insert dList;
    }
    
    /**
     * Test default Ziggo Lead creation
     */
    @IsTest
    private static void createZiggoLead() {
    	List<Ziggo_Lead__c> zList = createZiggoLeadList();
		
        Test.startTest();
            insert zList;
        Test.stopTest();
        Set<Id> zIdSet = new Map<Id, Ziggo_Lead__c>(zList).keySet();
        
        Demographic__c d = [select Id from Demographic__c where ein_pc4__c = :ZIPCODE];
		Zipcode_Info__c zi = [select Id from Zipcode_Info__c where postalcode__c = :ZIPCODE];
        
        zList = [select Id, ein_Demographic__c, ein_Zipcode_Info__c from Ziggo_Lead__c where Id in :zIdSet and ein_Demographic__c = :d.Id and ein_Zipcode_Info__c = :zi.Id];
        
        
        for (Ziggo_Lead__c z : zList) {
        	System.assert(z.Id!=null, 'Ziggo lead should be saved');
        	System.assert(z.ein_Demographic__c != null, 'Ziggo lead should be linked to cbs data (demographic)');
        	System.assert(z.ein_Zipcode_Info__c != null, 'Ziggo lead should be linked to Zipcode info');
        }
    }
    
    /**
     * Test default Ziggo Lead update 
     */
    @IsTest
    private static void updateZiggoLead() {

        List<Ziggo_Lead__c> zList = createZiggoLeadList();
        insert zList;
        for (Ziggo_Lead__c z : zList) {
    		z.ein_pc4__c = OTHER_ZIPCODE;
        }
        Test.startTest();
            update zList;
        Test.stopTest();
		
        Set<Id> zIdSet = new Map<Id, Ziggo_Lead__c>(zList).keySet();
		
		Demographic__c d = [select Id from Demographic__c where ein_pc4__c = :OTHER_ZIPCODE];
		Zipcode_Info__c zi = [select Id from Zipcode_Info__c where postalcode__c = :OTHER_ZIPCODE];
        
        zList = [select Id, ein_Demographic__c, ein_Zipcode_Info__c from Ziggo_Lead__c where Id in :zIdSet and ein_Demographic__c = :d.Id and ein_Zipcode_Info__c = :zi.Id];
        for (Ziggo_Lead__c z : zList) {
        	System.assert(z.Id!=null, 'Ziggo lead should be saved');
        	System.assert(z.ein_Demographic__c != null, 'Ziggo lead should be linked to cbs data (demographic)');
        	System.assert(z.ein_Zipcode_Info__c != null, 'Ziggo lead should be linked to Zipcode info');
        }
    }
    
    /**
     * Test default Ziggo Lead update - clear the zipcode should clear the lookups
     */
    @IsTest
    private static void updateZiggoLeadClearZipcode() {
        List<Ziggo_Lead__c> zList = createZiggoLeadList();
        insert zList;
        for (Ziggo_Lead__c z : zList) {
    		z.ein_pc4__c = null;
    	}
        Test.startTest();
            update zList;
        Test.stopTest();
		Set<Id> zIdSet = new Map<Id, Ziggo_Lead__c>(zList).keySet();
        
        zList = [select Id, ein_Demographic__c, ein_Zipcode_Info__c from Ziggo_Lead__c where Id in :zIdSet];
        for (Ziggo_Lead__c z : zList) {
        	System.assert(z.Id!=null, 'Ziggo lead should be saved');
        	System.assert(z.ein_Demographic__c == null, 'Ziggo lead should be cleared from cbs data (demographic)');
        	System.assert(z.ein_Zipcode_Info__c == null, 'Ziggo lead should be cleared from Zipcode info');
        }
    }
    
    /**
     * The list to use in the testing
     */
    private static List<Ziggo_Lead__c> createZiggoLeadList() {
    	List<Ziggo_Lead__c> zList = new List<Ziggo_Lead__c>();
        Ziggo_Lead__c zl = new Ziggo_Lead__c();
		zl.ein_pc4__c = ZIPCODE;
		zl.ein_fiber__c = 1.0;
		zl.ein_lg_dateofestablishment__c = Date.today();
		
		Ziggo_Lead__c zl2 = new Ziggo_Lead__c();
		zl2.ein_pc4__c = ZIPCODE;
		zl2.ein_fiber__c = 0.0;
		zl2.ein_lg_dateofestablishment__c = Date.today().addDays(-200);
		
		Ziggo_Lead__c zl3 = new Ziggo_Lead__c();
		zl3.ein_pc4__c = ZIPCODE;
		zl3.ein_fiber__c = 0.0;
		zl3.ein_lg_dateofestablishment__c = Date.today().addDays(-1000);
		zList.add(zl);
		zList.add(zl2);
		zList.add(zl3);
		
		return zList;
    }
    
}