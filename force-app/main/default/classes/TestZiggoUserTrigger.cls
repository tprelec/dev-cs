/**
 * Test class for ziggo user creation
 */
@IsTest
public class TestZiggoUserTrigger {

    public static final String ZIPCODE = '1000',
    OTHER_ZIPCODE = '1010',
    PROVINCE = 'ZH',
    OTHER_PROVINCE ='NH';
        
    private static final Set<String> ZIPCODE_SET = new Set<String>{ZIPCODE, OTHER_ZIPCODE};
        
    private static final String ZIPCODE_FIELD_NAME = Zipcode_Info__c.postalcode__c.getDescribe().getName();

    /**
     * Test the creation of the Ziggo user
     */
    static testmethod void createZiggoUser(){

        Ziggo_User__c zu = new Ziggo_User__c();
        zu.ein_salesperson_createddate__c = Date.today().addDays(-500);
        zu.ein_ziggo_user_id__c = UserInfo.getUserId();
        zu.ein_sp_zipcode__c = ZIPCODE;
        zu.ein_sp_company_name__c = 'Ziggo';
        zu.ein_ziggo_user_fullname__c = 'Ziggo user 1';
        test.startTest();
        insert zu;
        
        test.stopTest();
        zu = [select Id, ein_zipcode_info__c from Ziggo_User__c where Id = :zu.Id];
        Zipcode_Info__c zi = [select Id from Zipcode_Info__c where postalcode__c = :ZIPCODE];

        System.assert(zu.ein_zipcode_info__c == zi.Id, 'Zipcode reference must be populated');
    }

    /**
     * Test the update of the Ziggo user
     */
    static testmethod void updateZiggoUser(){

        Ziggo_User__c zu = new Ziggo_User__c();
        zu.ein_salesperson_createddate__c = Date.today().addDays(-500);
        zu.ein_ziggo_user_id__c = UserInfo.getUserId();
        zu.ein_sp_zipcode__c = ZIPCODE;
        zu.ein_sp_company_name__c = 'Ziggo';
        zu.ein_ziggo_user_fullname__c = 'Ziggo user 1';
        insert zu;
        test.startTest();
        zu.ein_sp_zipcode__c = OTHER_ZIPCODE;
        update zu;
        
        test.stopTest();
        zu = [select Id, ein_zipcode_info__c from Ziggo_User__c where Id = :zu.Id];

        Zipcode_Info__c zi = [select Id from Zipcode_Info__c where postalcode__c = :OTHER_ZIPCODE];

        System.assert(zu.ein_zipcode_info__c == zi.Id, 'Zipcode reference must be populated');
    }

    @TestSetup
	private static void prepareZipcodeTestEnv() {
		createZipcodeInfo();
	}

	/**
     * Create zipcode data
     */
    private static void createZipcodeInfo() {
    	List<Zipcode_Info__c> ziList = new List<Zipcode_Info__c>();
        Zipcode_Info__c z = new Zipcode_Info__c();
        z.postalcode__c = ZIPCODE;
        z.lead_province_cat__c = PROVINCE;
        
        ziList.add(z);
        
        Zipcode_Info__c z2 = new Zipcode_Info__c();
        z2.postalcode__c = OTHER_ZIPCODE;
        z2.lead_province_cat__c = OTHER_PROVINCE;
        ziList.add(z2);
        
        insert ziList;
	}
}