/**
 *	Provides methods for static flags that can be used to turn on and off particular trigger methods.
 */
public inherited sharing class TriggerFlagsService {

    // Map HandlerName > Method Name > Flag
    private static Map<String, Map<String, Boolean>> flags = new Map<String, Map<String, Boolean>>();

    public static void turnOff(String handlerName, String methodName) {
        setFlag(handlerName, methodName, false);
    }

    public static void turnOn(String handlerName, String methodName) {
        setFlag(handlerName, methodName, true);
    }

    private static void setFlag(String handlerName, String methodName, Boolean flag) {
        if (!flags.containsKey(handlerName)) {
            flags.put(handlerName, new Map<String, Boolean>());
        }
        flags.get(handlerName).put(methodName, flag);
    }

    public static Boolean getFlag(String handlerName, String methodName) {
        if (!flags.containsKey(handlerName) || !flags.get(handlerName).containsKey(methodName)) {
            return true;
        }
        return flags.get(handlerName).get(methodName);
    }

}