/**
 * @description         This is the abstract trigger handler. It's purpose is to work out which method in
 *                      a trigger handler to call depending on the context of the executing trigger.
 * @author              Guy Clairbois
 *
 *                      Changed the way the recurssionHandler works, previously it would block all Handlers from executing
 *                      now it should check the name of the handler and the operation being executed and allow multiple recursions
*                       if defined -- Chris Appels 19-12-2019
 */
public abstract class TriggerHandler {

    //Stores information on Triggers that should be bypassed and how often they ran already
    @TestVisible private static Map<String, Map<String, recurssionHandler>> bypassedHandlers = new Map<String, Map<String, recurssionHandler>>();

    //Stores info on current running TriggerHandler and operation
    private static String handlerName;
    private static String triggerContext;

    /**
     * @description These 4 Maps and Lists will contain the Trigger variables (assigned in Execute method)
     */
    protected Map<Id,SObject> oldMap;
    protected Map<Id,SObject> newMap;
    protected List<SObject> oldList;
    protected List<SObject> newList;

    private static Boolean hasExecuted = false;
    /**
     * @description This will be called in the trigger before insert event
     * @param newObjects List of new sObjects to be inserted
     */
    public virtual void beforeInsert(){}


    /**
     * @description This will be called in the trigger before update event
     * @param oldObjects List of the sObjects being updated with their original values
     * @param newObjects List of the sObjects being updated with their new values
     */
    public virtual void beforeUpdate(){}


    /**
     * @description This will be called in the trigger before delete event
     * @param objects List of the sObjects being deleted
     */
    public virtual void beforeDelete(){}


    /**
     * @description This will be called in the trigger after insert event
     * @param newObjects List of the sObjects being inserted
     */
    public virtual void afterInsert(){}


    /**
     * @description This will be called in the trigger after update event
     * @param oldObjects List of the sObjects being updated with their original values
     * @param newObjects List of the sObjects being updated with their new values
     */
    public virtual void afterUpdate(){}


    /**
     * @description This will be called in the trigger after delete event
     * @param objects List of objects to be deleted
     */
    public virtual void afterDelete(){}

    /**
     * @description This will be called in the trigger after Undelete event
     */
    public virtual void afterUnDelete(){}
    /**
     * @description This will call the relevant method in the trigger handler for the current trigger event
     */
    public void execute(){
        handlerName = getHandlerName();
        this.oldMap = Trigger.oldMap;
        this.newMap = Trigger.newMap;
        this.newList = Trigger.new;
        this.oldList = Trigger.old;

        hasExecuted = true;

        // Call the relevant trigger event method
        if(Trigger.isBefore){
            if(Trigger.isDelete){
                triggerContext = 'beforeDelete';
                if(bypassedHandlers.containsKey(handlerName) &&
                    bypassedHandlers.get(handlerName).containsKey(triggerContext)){
                    if (!bypassedHandlers.get(handlerName).get(triggerContext).canRunAgain()) {
                        return;
                    } else {
                        beforeDelete();
                        bypassedHandlers.get(handlerName).get(triggerContext).timesRan += 1;
                    }
                } else {
                beforeDelete();
                }
            } else if (Trigger.isInsert){
                triggerContext = 'beforeInsert';
                if(bypassedHandlers.containsKey(handlerName) &&
                    bypassedHandlers.get(handlerName).containsKey(triggerContext)){
                    if (!bypassedHandlers.get(handlerName).get(triggerContext).canRunAgain()) {
                        return;
                    } else {
                        beforeInsert();
                        bypassedHandlers.get(handlerName).get(triggerContext).timesRan += 1;
                    }
                } else {
                beforeInsert();
                }
            } else if (Trigger.isUpdate){
                triggerContext = 'beforeUpdate';
                if(bypassedHandlers.containsKey(handlerName) &&
                    bypassedHandlers.get(handlerName).containsKey(triggerContext)){
                    if (!bypassedHandlers.get(handlerName).get(triggerContext).canRunAgain()) {
                        return;
                    } else {
                beforeUpdate();
                        bypassedHandlers.get(handlerName).get(triggerContext).timesRan += 1;
                    }
                } else {
                    beforeUpdate();
                }
            }
        } else {
            if(Trigger.isDelete){
                triggerContext = 'afterDelete';
                if(bypassedHandlers.containsKey(handlerName) &&
                    bypassedHandlers.get(handlerName).containsKey(triggerContext)){
                    if (!bypassedHandlers.get(handlerName).get(triggerContext).canRunAgain()) {
                        return;
                    } else {
                        afterDelete();
                        bypassedHandlers.get(handlerName).get(triggerContext).timesRan += 1;
                    }
                } else {
                afterDelete();
                }
            } else if(Trigger.isInsert){
                triggerContext = 'afterInsert';
                if(bypassedHandlers.containsKey(handlerName) &&
                    bypassedHandlers.get(handlerName).containsKey(triggerContext)){
                    if (!bypassedHandlers.get(handlerName).get(triggerContext).canRunAgain()) {
                        return;
                    } else {
                        afterInsert();
                        bypassedHandlers.get(handlerName).get(triggerContext).timesRan += 1;
                    }
                } else {
                afterInsert();
                }
            } else if(Trigger.isUpdate){
                triggerContext = 'afterUpdate';
                if(bypassedHandlers.containsKey(handlerName) &&
                    bypassedHandlers.get(handlerName).containsKey(triggerContext)){
                    if (!bypassedHandlers.get(handlerName).get(triggerContext).canRunAgain()) {
                        return;
                    } else {
                        afterUpdate();
                        bypassedHandlers.get(handlerName).get(triggerContext).timesRan += 1;
                    }
                } else {
                afterUpdate();
                }
            } else if (Trigger.isUndelete){
                triggerContext = 'afterUndelete';
                if(bypassedHandlers.containsKey(handlerName) &&
                    bypassedHandlers.get(handlerName).containsKey(triggerContext)){
                    if (!bypassedHandlers.get(handlerName).get(triggerContext).canRunAgain()) {
                        return;
                    } else {
                        afterUndelete();
                        bypassedHandlers.get(handlerName).get(triggerContext).timesRan += 1;
                    }
                } else {
                    afterUndelete();
                }
            }
        }
    }


    /**
     * First to preventRecursiveTrigger methods can be called from the methods in a TriggerHandler to control how often
     * it's executed.ApexPages
     *
     * The last one can be used to set a bypass for TriggerHandlers if you are outside of TriggerContext
     *
     */
    public void preventRecursiveTrigger(){
        if (bypassedHandlers.containsKey(handlerName) &&
            !bypassedHandlers.get(handlerName).containsKey(triggerContext)) {
            bypassedHandlers.get(handlerName).put(triggerContext, new recurssionHandler(1, 1));
        }
        if (!bypassedHandlers.containsKey(handlerName)) {
            bypassedHandlers.put(handlerName, new Map<String, recurssionHandler>());
            bypassedHandlers.get(handlerName).put(triggerContext, new recurssionHandler(1, 1));
        }
    }

    public void preventRecursiveTrigger(Integer timesAllowed){
        if (bypassedHandlers.containsKey(handlerName) &&
            !bypassedHandlers.get(handlerName).containsKey(triggerContext)) {
            bypassedHandlers.get(handlerName).put(triggerContext, new recurssionHandler(timesAllowed, 1));
        }
        if (!bypassedHandlers.containsKey(handlerName)) {
            bypassedHandlers.put(handlerName, new Map<String, recurssionHandler>());
            bypassedHandlers.get(handlerName).put(triggerContext, new recurssionHandler(timesAllowed, 1));
        }
    }


    public static void preventRecursiveTrigger(String handlerToBypass, List<String> triggerOperations, Integer timesAllowed){
        // Default without param is allow the TriggerHandler to run once
        if (timesAllowed == null)
            timesAllowed = 1;

        // Default if no operations are specified is to bypass each operation
        if (triggerOperations == null || triggerOperations.isEmpty())
            triggerOperations = new List<String>{'beforeDelete', 'beforeInsert', 'beforeUpdate', 'afterInsert', 'afterUpdate', 'afterUndelete', 'afterDelete'};


        if (bypassedHandlers.containsKey(handlerToBypass)) {
           Map<String, recurssionHandler> byPass = bypassedHandlers.get(handlerToBypass);

           for (String operation : triggerOperations) {
               if (byPass.containsKey(operation)) {
                   /** If a trigger operation is already bypassed somewhere you shouldn't be able to overwrite it
                       and break the existing functionality. If you need it changed go fix it at the original place
                       the bypass was set*/
                   continue;
               } else {
                   byPass.put(operation, new recurssionHandler(timesAllowed));
               }
           }

            bypassedHandlers.put(handlerToBypass, byPass);
        }  else {
            Map<String, recurssionHandler> toBypass = new Map<String, recurssionHandler>();

            for (String operation : triggerOperations) {
                toBypass.put(operation, new recurssionHandler(timesAllowed));
            }
            bypassedHandlers.put(handlerToBypass, toBypass);
            System.debug('bypassedHandlers    ' + bypassedHandlers);
        }
    }

    //Get's the name from the currently executing TriggerHandler
    private String getHandlerName() {
        return String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
    }

    // Helper class to track how many times each handler can run
    public Class recurssionHandler{

        public Integer timesRan;
        public Integer timesAllowed;

        recurssionHandler(Integer timesAllowedToRun){
            timesRan = 0;
            timesAllowed = timesAllowedToRun;
        }

        recurssionHandler(Integer timesAllowedToRun, Integer timesAlreadyRan){
            timesRan = timesAlreadyRan;
            timesAllowed = timesAllowedToRun;
        }

        public Boolean canRunAgain(){
            Boolean canRunAgain = timesRan < timesAllowed;
            if (!canRunAgain) {
                System.debug('BYPASSING: ' + handlerName.toUpperCase() + ' ---- OPERATION: ' + triggerContext.toLowerCase());
            }
            return canRunAgain;
        }

    }

    //Helpers to reset recursion limits
    public static void resetRecursion(){
        bypassedHandlers = new Map<String, Map<String, recurssionHandler>>();
    }

    public static void resetRecursion(String handlerName){
        if (bypassedHandlers.containsKey(handlerName)) {
            bypassedHandlers.remove(handlerName);
        }
    }

    // initialize a variable to hold state
    private static boolean alreadyModified = false;

    // get the state
    public static boolean isAlreadyModified() {
        return alreadyModified;
    }

    // set this to true to keep track of and avoid recursive updates.  Generally set after first time through
    // a trigger.  We can access this in the trigger and avoid recursive updates...
    public static void setAlreadyModified() {
        alreadyModified = true;
    }

    // use method to reset already modified flag in test classes
    // an example would be to test insert and update trigger scenarios in single transaction or test method
    @TestVisible
    private static void resetAlreadyModified() {
        alreadyModified = false;
    }

    // Consolidation SF team: this custom setting is currently only used by Ziggo code
    // in future maybe we can extend it for Vodafone trigger handlers
    public Boolean isTriggerActive() {
        No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        return notriggers == null || !notriggers.Flag__c;
    }

}