public  class TriggerOpportunityContactRole {
	public static void primaryContactUpdate(List<Opportunity> opptyList) {
		       		           
		Set<Id> OppIds = new Set<Id>();         
		for (Opportunity o : opptyList) {		
            	//PBI000000210917 : removed null check
				OppIds.add(o.id);			
		}	
                    
		if (OppIds.size() > 0) {

			Map<Id, List<OpportunityContactRole>> OCRmap = new Map<Id, List<OpportunityContactRole>>();

			for (OpportunityContactRole ocr : [SELECT id, contactid, Opportunityid, role, isprimary, createddate FROM OpportunityContactRole WHERE opportunityid in :OppIds AND isprimary = true]) {
				List<OpportunityContactRole> ocrlist = new List<OpportunityContactRole>();
				ocrlist = OCRmap.get(ocr.opportunityid);

				if (ocrlist == null) {
					OCRmap.put(ocr.opportunityid, new List<OpportunityContactRole> {ocr});
				} else {
					OCRmap.put(ocr.opportunityid, ocrlist);
				}
			}

			// for each Opp modified in the trigger, trying to find relevant contacts
			for (Opportunity opps : opptyList) {
				// temporary list of OpptyContactRoles for this Opportunity populated from the main map
				List<OpportunityContactRole>  contactrole = new List<OpportunityContactRole>();
				contactrole = OCRmap.get(opps.id);

				if (contactrole == null) {
					opps.LG_primarycontact__c = null;
				} else {

					for (OpportunityContactRole r : contactrole) {

						// if the role is primary, track this Contact id
						if (r.isprimary) {
                            //PBI000000210917 : new logic
                            if(opps.LG_primarycontact__c!= null && opps.LG_primarycontact__c != r.contactid){//primary contact changed
                            	opps.LG_primarycontact__c = r.contactid;    
                            }
                            else if(opps.LG_primarycontact__c == null){
                                opps.LG_primarycontact__c = r.contactid;
                            }
                            //PBI000000210917
						}
					}
				}
			}
		}
	}
}