@isTest
public class TriggerOpportunityContactRole_Test {
    static testMethod void Opcontactrole() {
        // creating a test Lead
        lead l = new lead (lastname = 'leadtest', status = 'Not contracted', Company = 'Accenture', LG_VisitCountry__c = 'Netherlands',
                           KVK_number__c = '02938493', Town__c='Utrecht', Street__c='Von Kleistlaan', Postcode_number__c='3533', Postcode_char__c='BA',
                            RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('CSADetails').getRecordTypeId());
        insert l ;

        // creating test account
        Account a = new Account(name = 'Test1', LG_VisitCountry__c = 'Netherlands', KVK_number__c = '03438493', LG_Footprint__c = 'UPC');
        insert a;

        // creating  test contacts
        Contact c1 = new Contact(firstname = 'Ram', lastname = 'Reddy');
        Contact c2 = new Contact(firstname = 'Indra', lastname = 'G');
        Contact c3 = new Contact(firstname = 'san', lastname = 'mateo');
        Contact[] cont = new Contact[] {c1, c2, c3};
        insert cont;

        // creating test opp
        Opportunity opp = new Opportunity(name = 'test Opp', closedate = system.today(), stagename = 'Prospect', RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SOHO_SMALL').getRecordTypeId());
        insert opp;

        // test 1: no OCRs == null
        opp = [select LG_PrimaryContact__c from opportunity where id = :opp.id];
        opp.description = 'First test';
        update opp;
        system.assert(opp.LG_PrimaryContact__c == null);

        // test 2: 1 OCR, not Primary == null
        OpportunityContactRole ocr1 = new OpportunityContactRole(opportunityid = opp.id, contactid = c1.id, role = 'Administrative Contact');
        insert ocr1;
        ocr1 = [select createddate, opportunityid, contactid, role, isprimary from opportunitycontactrole where id = :ocr1.id];

        opp.description = 'test';
        update opp;
        opp = [select LG_PrimaryContact__c from opportunity where id = :opp.id];
        system.assert(opp.LG_PrimaryContact__c == null);

        // test 3: 1 OCR, Primary == c1
        ocr1.isprimary = true;
        update ocr1;
        opp.description = 'test2';
        update opp;
        opp = [select LG_PrimaryContact__c from opportunity where id = :opp.id];
        system.assert(opp.LG_PrimaryContact__c  == c1.id);
    }
}