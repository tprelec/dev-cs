global class UnicaBaseAllocationBatch implements Database.Batchable<sObject> {
	
	
	String itwTeamMemberRoleName;
	
	global UnicaBaseAllocationBatch() {
		
		



		//xxx todo: store a map of strings that will have mapping from incoming team name and to existing account team member role, and make it dynamic in code so it works for every possibility

		UnicaBaseAllocationSettings__c ubaSettings = UnicaBaseAllocationSettings__c.getValues('Main');

		if(ubaSettings != null){

			itwTeamMemberRoleName = ubaSettings.ITW_Account_Team_name__c;
		}
		else{
			itwTeamMemberRoleName = 'ITW Sales Rep'; //hard coded in case for some reason custom settings fail
		}

	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		//only records that are ready for processing (based on the incoming data, 
		//see UnicaBaseAllocationTriggerHandler.validateAndPrepareData method)
		String query;
		query = 	'SELECT id,';
		query +=		' Account__c,';
		query += 		' Assigned_User__c,';
		query += 		' Assignment_Approved__c,';
		query +=		' Processed__c,';
		query +=		' ReadyForProcessing__c,';
		query += 		' Team_Name__c,';
		query += 		' Allocation_Action__c';
		query += 	' FROM 	Unica_Base_Allocation__c'; 
		query += 	' WHERE	Processed__c = false'; 
		query += 		' AND ReadyForProcessing__c = true';
		query += 		' AND Assignment_Approved__c = true';


		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Unica_Base_Allocation__c> scope) {
		
		system.debug('***SP*** scope: '+ scope);

		List<AccountTeamMember> atmsToDelete = new List<AccountTeamMember>();
		List<AccountTeamMember> atmsToInsert = new List<AccountTeamMember>();
		List<Unica_Base_Allocation__c> ubasToDelete = new List<Unica_Base_Allocation__c>();
		List<Unica_Base_Allocation__c> ubasToUpdate = new List<Unica_Base_Allocation__c>();
		List<Account> accsToUpdate = new List<Account>();


		List<Id> accountIdsToDeleteItwMembers = new List<Id>();
		List<Id> allAccIds = new List<Id>();

		//we need a full list of accounts
		for (Unica_Base_Allocation__c uba : scope){
			allAccIds.add(uba.Account__c);
		}

		Map<Id,Account> allAccsMap = new Map<Id,Account> ([select id, name, ITW_Team_Member__c, ITW_Indicator__c from Account where id in :allAccIds]);


		for (Unica_Base_Allocation__c uba : scope){

			if(uba.Allocation_Action__c == 'New'){
				AccountTeamMember atm = createNewAccountTeamMember(uba.Account__c, uba.Assigned_User__c, itwTeamMemberRoleName);
				atmsToInsert.add(atm);

				Account acc = allAccsMap.get(uba.Account__c);
				if (acc != null) {
					acc.ITW_Team_Member__c = uba.Assigned_User__c;
					acc.ITW_Indicator__c = true;
					accsToUpdate.add(acc);
				}

				uba.Processed__c = true;
				ubasToUpdate.add(uba);
			}

			else if(uba.Allocation_Action__c == 'Delete') {
				ubasToDelete.add(uba);
				accountIdsToDeleteItwMembers.add(uba.Account__c);
				uba.Processed__c = true;
				ubasToUpdate.add(uba);
			}
		}

		List<AccountTeamMember> atmsToCheckForDeletion = [select id, accountId, userId, TeamMemberRole from AccountTeamMember where accountid in :accountIdsToDeleteItwMembers and TeamMemberRole =: itwTeamMemberRoleName];
		

		for(AccountTeamMember atm : atmsToCheckForDeletion){
			for(Unica_Base_Allocation__c uba : ubasToDelete){
				if (atm.accountId == uba.Account__c && atm.userId == uba.Assigned_User__c){
					atmsToDelete.add(atm);

					Account acc = allAccsMap.get(atm.accountId);
					if (acc != null) {
						acc.ITW_Team_Member__c = null;
						acc.ITW_Indicator__c = false;
						accsToUpdate.add(acc);
					}
				}
			}
		}
	

		insert atmsToInsert;
		delete atmsToDelete;
		update ubasToUpdate;
		update accsToUpdate;

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	private AccountTeamMember createNewAccountTeamMember(Id accountId, Id userId, string roleName){

		AccountTeamMember atm = new AccountTeamMember();
		atm.accountId = accountId;
		atm.userId = userId;
		atm.TeamMemberRole = roleName;
		return atm;
	}
	
}