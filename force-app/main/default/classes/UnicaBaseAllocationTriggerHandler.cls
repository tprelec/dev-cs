/**
 * @description			This is the trigger handler for the Unica Base Approval  sObject.
 *						Main purpose is to populate two lookups on the object based on the
 *						ID being sent from external system, and to initiate approval process
 * @author				Stjepan Pavuna
 */
public with sharing class UnicaBaseAllocationTriggerHandler extends TriggerHandler {


	/**
	 * @description			This handles the before insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeInsert(){
		List<Unica_Base_Allocation__c > newBaseAllocations = (List<Unica_Base_Allocation__c >) this.newList;

		validateAndPrepareData(newBaseAllocations);
	}

    
	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterInsert(){

		List<Unica_Base_Allocation__c > newBaseAllocations = (List<Unica_Base_Allocation__c >) this.newList;
		startApprovalProcess(newBaseAllocations);
	}

	/**
	 * @description			This handles the before update trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeUpdate(){
		//List<Unica_Base_Allocation__c > newBaseAllocations = (List<Unica_Base_Allocation__c >) this.newList;
		//Map<Id,Unica_Base_Allocation__c > oldBaseAllocationsMap = (Map<Id,Unica_Base_Allocation__c >) this.oldMap;

	}
	

	/**
	 * @description			This handles the after update trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterUpdate(){
		//List<Unica_Base_Allocation__c > newBaseAllocations = (List<Unica_Base_Allocation__c >) this.newList;
		//Map<Id,Unica_Base_Allocation__c > oldBaseAllocationsMap = (Map<Id,Unica_Base_Allocation__c >) this.oldMap;
	
	}	

	/**
	 * @description			This handles the before delete trigger event.
	 * @param	newObjects	List of sObjects that are being deleted.
	 */
	public override void BeforeDelete(){
		//List<Unica_Base_Allocation__c > oldBaseAllocations = (List<Unica_Base_Allocation__c >) this.oldList;
				
	}

	/**
	 * @description			Populates user, manager and account lookups if it finds the values
	 */	
	private void validateAndPrepareData(List<Unica_Base_Allocation__c> newBaseAllocations){
		
		List<Id> userIds = new List<Id>();
		List<Id> accountIds = new List<Id>();
		List<String> userEmails = new List<String>();

		for(Unica_Base_Allocation__c uba : newBaseAllocations){

			/*String agentId = uba.Sales_Agent_Salesforce_ID__c;
			if (agentId != null && GeneralUtils.isValidId(agentId)){

				userIds.add(agentId);
			}*/

			String agentEmail = uba.Inside_Sales_Agent_Email__c;
			if (agentEmail != null){

				userEmails.add(agentEmail);
			}

			String accountId = uba.Salesforce_Account_ID__c;
			if (accountId != null && GeneralUtils.isValidId(accountId)){

				accountIds.add(accountId);
			}
		}

		Map<Id,Account> relatedAccounts = new Map<Id,Account> ([select id, name from Account where id in :accountIds]);
		//Map<Id,User> relatedUsers = new Map<Id,User>([select id, name, managerid from User where id in :userIds]);
		Map<Id,User> relatedUsers = new Map<Id,User>([select id, name, email, managerid from User where email in :userEmails]);

		Map<String,User> emailToUsersMap = new Map<String,User>();

		Set<String> flaggedDuplicateEmails = new Set<String>();

		for(Id userId : relatedUsers.keyset()){
			User usr = relatedUsers.get(userId);

			//if there already is an entry with the same email
			if(emailToUsersMap.get(usr.email) != null){
				flaggedDuplicateEmails.add(usr.email);
			}
			else{
				emailToUsersMap.put(usr.email, usr);
			}
		}

		for(Unica_Base_Allocation__c uba : newBaseAllocations){

			uba.Error__c = '';
			uba.ReadyForProcessing__c = true;
			String email = uba.Inside_Sales_Agent_Email__c;
			if (email != null){

				if(flaggedDuplicateEmails.contains(email)){
					uba.Error__c =  Label.Unica_Base_Allocation_Duplicate_User_Email_Error + ' | ';
					uba.ReadyForProcessing__c = false;
				}
				else{

					if(emailToUsersMap.containsKey(email)){
						User usr = emailToUsersMap.get(email);			 		//if it found the user with this ID
						uba.Assigned_User__c = usr.id;							//add it to the lookup
						uba.Assigned_User_s_Manager__c = usr.managerid; 		//and add his manager

						if(uba.Allocation_Action__c == 'Delete'){
							uba.Assignment_Approved__c = true;
						}
					}
					else{
						uba.Error__c =  Label.Unica_Base_Allocation_User_Not_Found_Error + ' | ';
						uba.ReadyForProcessing__c = false;
					}
				}
			}

			String accountId = uba.Salesforce_Account_ID__c;
			if (accountId != null && GeneralUtils.isValidId(accountId)){

				Id accId = Id.valueOf(accountId);
				if(relatedAccounts.containsKey(accId)){	 			//if it found the account with this ID
				uba.Account__c = accId;								//add it to the lookup
				}
				else{
					uba.Error__c += Label.Unica_Base_Allocation_Account_Not_Found_Error + ' | ';
					uba.ReadyForProcessing__c = false;
				}
			}

			UnicaBaseAllocationSettings__c ubaSettings = UnicaBaseAllocationSettings__c.getValues('Main');

			if(ubaSettings != null){
				if(!ubaSettings.AccountTeams__c.contains(';'+uba.Team_Name__c+';')){
					uba.Error__c += Label.Unica_Base_Allocation_Team_Not_Found_Error + ' | ';
					uba.ReadyForProcessing__c = false;
				}
			}

			if(uba.Error__c.contains(' | ')){
				uba.Error__c = uba.Error__c.left(uba.Error__c.length()-3);
			}

			
		}
		
	}

	/**
	 * @description			This starts the approval proces automatically for every record that is ready for processing.
	 */	
	private void startApprovalProcess(List<Unica_Base_Allocation__c> newBaseAllocations){
		
		for (Unica_Base_Allocation__c uba : newBaseAllocations){

			if(uba.ReadyForProcessing__c && uba.Allocation_Action__c == 'New'){

				// create the new approval request to submit
		        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
		        req.setComments('Submitted for approval. Please approve.');
		        req.setObjectId(uba.Id);
		        // submit the approval request for processing
		        Approval.ProcessResult result = Approval.process(req);
		        // display if the reqeust was successful
		        //System.debug('Submitted for approval successfully: '+result.isSuccess());	
			}
		}
		
	}
	
}