global class UnicaCampaignBatch implements Database.Batchable <sObject>, Database.Stateful{

    global final map<String,String> fieldMappingCampaign = SyncUtil.fullMapping('Unica_Campaign__c -> Campaign').get('Unica_Campaign__c -> Campaign');
    global final map<String,String> fieldMappingCampaignMember = SyncUtil.fullMapping('Unica_Campaign__c -> CampaignMember').get('Unica_Campaign__c -> CampaignMember');
    global final map<String,String> fieldMappingLead = SyncUtil.fullMapping('Unica_Campaign__c -> Lead').get('Unica_Campaign__c -> Lead');
    global final map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    global final Schema.SObjectType leadSchema = schemaMap.get('Lead');
    global final Schema.SObjectType campaignSchema = schemaMap.get('Campaign');
    global final map<String, Schema.SObjectField> leadFieldMap = leadSchema.getDescribe().fields.getMap();
    global final map<String, Schema.SObjectField> campaignFieldMap = campaignSchema.getDescribe().fields.getMap();
    global map<String, Id> leadIdsBP = new map<String, Id>();
    global map<String, Id> leadIdsITRD = new map<String, Id>();
    global map<String, Id> leadIdsZSP = new map<String, Id>();
    global map<String, Id> leadIdsBPXUSell = new map<String, Id>();
    global map<String, Id> leadIdsBPSaves = new map<String, Id>();
    global map<String, Id> leadIdsITRDSaves = new map<String, Id>();
    global final Schema.SObjectType unicaSchema = schemaMap.get('Unica_Campaign__c');
    global final map<String, Schema.SObjectField> unicaFieldMap = unicaSchema.getDescribe().fields.getMap();
    // include the fields that are mapped to lead in the error report
    global final set<String> queriedFields = SyncUtil.getQueriedFields(fieldMappingLead);
    global final list<String> sortList = SyncUtil.getSortedList(unicaFieldMap);
    global String header = SyncUtil.getHeader(sortList, queriedFields);
    global String dataError = '';
    global Map<String, String> dealerZPOwnerCampaign = new Map<String, String>{'ETRECO0020' => 'ETRECO0020'};


    global Database.QueryLocator start(Database.BatchableContext BC) {

        String query = 'SELECT ';
        set<String> testset = new set<String>();
        testset.addall(fieldMappingCampaign.values());
        testset.addall(fieldMappingCampaignMember.values());
        testset.addall(fieldMappingLead.values());
        for (String ucField : testset) {
            query += ucField + ',';
        }
        query = query.subString(0, query.length() - 1);
        // add postcode manually. Needed for allocation but is not mapped to lead
        // same for campaign channel
        query += ' , Post_Code__c, Campaign_Channel__c FROM Unica_Campaign__c Where Processed__c = false';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Unica_Campaign__c> scope) {

        map<String, Id> campaignMapParent = new map<String, Id>();
        map<String, Id> campaignMapChild = new map<String, Id>();
        list<Campaign> parentCampaignToInsert = new list<Campaign>();
        list<Campaign> childCampaignToInsert = new list<Campaign>();
        map<String, Unica_Campaign__c> ucToCreate = new map<String, Unica_Campaign__c>();
        list<CampaignMember> campaignMemberList = new list<CampaignMember>();
        map<String, Ban__c> banMap = new map<String, Ban__c>();
        map<String, Unica_Campaign__c> cmCreate = new map<String, Unica_Campaign__c>();
        list<Lead> leadList = new list<Lead>();
        map<String, Id> leadMap = new map<String, Id>();
        set<String> campaignSet = new set<String>();
        map<String, Id> campaignMemberMap = new map<String, Id>();
        map<Id, Integer> accountMap = new map<Id, Integer>();
        map<Id, String> accountMapTemplate = new map<Id, String>();
        map<Id, Account> accountFullMapTemplate = new map<Id, Account>();
        map<Id, String> banMapTemplate = new map<Id, String>();
        map<Id, Id> contactMap = new map<Id, Id>();
        list<Unica_Campaign__c> ucToBeDeleted = new list<Unica_Campaign__c>();
        list<Unica_Campaign__c> cmList = new list<Unica_Campaign__c>();
        map<String, Contact> emailMap = new map<String, Contact>();
        map<Id, String> profileMap = new map<Id, String>();

        Id recordType = [select Id from RecordType where SobjectType =: 'Campaign' AND DeveloperName =: 'Unica_Campaign' limit 1].Id;
        Id vodafone = [SELECT Id FROM User WHERE Name =: 'Vodafone' LIMIT 1].Id;

        //Data preparation
        map<String, Unica_Campaign__c> scopeMap = new map<String, Unica_Campaign__c>();
        for(Unica_Campaign__c uc : scope){
            uc.Processed__c = true;
            if(uc.NTA_Id__c == null){
                uc.Error__c = 'The NTA ID is not supplied';
                ucToBeDeleted.add(uc);
            } else if (uc.Campaign_Name__c==null) {
                uc.Error__c = 'The Campaign_Name__c is not supplied';
                ucToBeDeleted.add(uc);
            } else {
                if(!cmCreate.containsKey(uc.NTA_Id__c)){
                    cmCreate.put(uc.NTA_Id__c, uc);
                    if(uc.Selection_Name__c != null){
                        campaignMapParent.put(uc.Campaign_Name__c, null);
                    }
                    campaignMapChild.put(uc.Campaign_Name__c + ',' + uc.Selection_Name__c, null);
                    ucToCreate.put(uc.Campaign_Name__c + ',' + uc.Selection_Name__c, uc);
                    banMap.put(uc.Ban__c, null);
                    campaignSet.add(uc.Campaign_Name__c);
                    emailMap.put(uc.Ban_Contact_Email__c, null);
                    scopeMap.put(uc.ID, uc);
                }
            }
        }

        for(Ban__c ban : [select Id, Account__c, Name, BAN_Name__c, Owner.Email, OwnerId, Owner.IsActive, Owner.Profile.Name, Dealer_code__c, Owner_Dealercode__c, Class_4_Name__c from Ban__c where Name in: banMap.keySet()]){
            banMap.put(ban.Name, ban);
            banMapTemplate.put(ban.Id,ban.OwnerId);
            accountMap.put(ban.Account__c, null);
            profileMap.put(ban.OwnerId, ban.Owner.Profile.Name);
        }

        // if ban dealercode owner is vodafone or dealercode has no contact/user, replace it by the parent partner account contact user
        Map<String,Id> parentDealerCodeToParentOwner = new Map<String,Id>();
        Map<String,Id> dealerCodeToOwner = new Map<String,Id>();
        Map<String,Id> mobileDealerCodeToOwner = new Map<String,Id>();
        Map<String,String> dealerCodeToDealerName = new Map<String,String>();
        Map<String,String> parentDealerCodeToDealerCode = new Map<String,String>();
        Map<String,String> dealerCodeToParentDealerCode = new Map<String,String>();

        system.debug(banMap);
        for(String banId : banMap.keySet()){
            if(banId != null){
                system.debug('***SP*** banId: '+ banId);
                if(banMap.get(banId) != null){
                    system.debug('***SP*** GeneralUtils.stripDealerCode(banMap.get(banId).Dealer_Code__c): '+ GeneralUtils.stripDealerCode(banMap.get(banId).Dealer_Code__c));
                    dealerCodeToOwner.put(GeneralUtils.stripDealerCode(banMap.get(banId).Dealer_Code__c),null);
                    mobileDealerCodeToOwner.put(GeneralUtils.stripDealerCode(banMap.get(banId).Owner_Dealercode__c),null);
                }
            }
        }
        system.debug(dealerCodeToOwner);

        // collect (active) dealer contact users + additional mappings
        for(Dealer_Information__c di : [SELECT Id, Name, Dealer_Code__c,Contact__r.UserId__c,Contact__r.UserId__r.IsActive,Contact__r.UserId__r.Email,Parent_Dealer_Code__c
        FROM
                Dealer_Information__c
        WHERE
                Dealer_Code__c in :dealerCodeToOwner.keySet()]){

            if(di.Contact__r.UserId__c != null && di.Contact__r.UserId__r.IsActive){
                dealerCodeToOwner.put(di.Dealer_Code__c,di.Contact__r.UserId__c);
            }
            dealerCodeToDealerName.put(di.Dealer_Code__c,di.Name);
            if(di.Parent_Dealer_Code__c != null){
                parentDealerCodeToDealerCode.put(di.Parent_Dealer_Code__c,di.Dealer_Code__c);
                dealerCodeToParentDealerCode.put(di.Dealer_Code__c,di.Parent_Dealer_Code__c);
            }
        }
        // collect (active) parent contact users
        for(Dealer_Information__c pdi : [SELECT Id, Name, Dealer_Code__c,Contact__r.UserId__c
        FROM
                Dealer_Information__c
        WHERE
        Contact__c != null
        AND
        Contact__r.UserId__r.IsActive = true
        AND
        Dealer_Code__c in :parentDealerCodeToDealerCode.keySet()]){

            parentDealerCodeToParentOwner.put(pdi.Dealer_Code__c,pdi.Contact__r.UserId__c);
        }

        // collect mobile dealer contact users
        system.debug('***SP*** searching for mobile dealer');
        for(Dealer_Information__c di : [SELECT Id, Name, Dealer_Code__c,Contact__r.UserId__c,Contact__r.UserId__r.IsActive,Contact__r.UserId__r.Email,Parent_Dealer_Code__c, Contact__r.Account.CVM_Lead_Owner__c
        FROM
                Dealer_Information__c
        WHERE
                Dealer_Code__c in :mobileDealerCodeToOwner.keySet()]){

            if(di.Contact__r.UserId__c != null && di.Contact__r.UserId__r.IsActive){
                mobileDealerCodeToOwner.put(di.Dealer_Code__c,di.Contact__r.UserId__c);
                system.debug('***SP*** found mobile dealer');
                system.debug('***SP*** di ' + di);
                system.debug('***SP*** di.Contact__r.UserId__c: ' + di.Contact__r.UserId__c);
            }
            dealerCodeToDealerName.put(di.Dealer_Code__c,di.Name);
            if(di.Parent_Dealer_Code__c != null){
                parentDealerCodeToDealerCode.put(di.Parent_Dealer_Code__c,di.Dealer_Code__c);
                dealerCodeToParentDealerCode.put(di.Dealer_Code__c,di.Parent_Dealer_Code__c);
            }

            //if there is a prefered CVM lead owner, that will be used instead of the dealer contact
            if(di.Contact__r.Account.CVM_Lead_Owner__c != null){
                mobileDealerCodeToOwner.put(di.Dealer_Code__c,di.Contact__r.Account.CVM_Lead_Owner__c);
            }
        }

        // fetch channel via owner->contact->account (partner account)
        Map<Id,String> userIdToChannel = new Map<Id,String>();
        for(User u : [Select Id, Contact.Account.Channel__c From User Where Id in :banMapTemplate.values()]){
            userIdToChannel.put(u.Id,u.Contact.Account.Channel__c);
        }
        for(Id banId : banMapTemplate.keySet()){
            String channel = userIdToChannel.get(banMapTemplate.get(banId));
            banMapTemplate.put(banId,channel);
        }


        for(Lead lead : [select Id, NTA_Id__c from Lead where NTA_Id__c in: cmCreate.keySet()]){
            leadMap.put(lead.NTA_Id__c, Lead.Id);
        }

        for(CampaignMember cm : [select Id, NTA_Id__c from CampaignMember where NTA_Id__c in: cmCreate.keySet()]){
            campaignMemberMap.put(cm.NTA_Id__c, cm.Id);
        }

        for(AggregateResult ar : [select AccountId, count(Id) from Contact where AccountId in: accountMap.keySet() group by AccountId]){
            if(ar.get('expr0') == 1){
                contactMap.put((Id) ar.get('AccountId'), null);
            }
        }

        for(Account acc : [select Id, Channel__c, ITW_Team_Member__c, ITW_Indicator__c, ITW_Account_Manager_Overwrite__c from Account where Id in: accountMap.keySet()]){
            accountMapTemplate.put(acc.Id, acc.Channel__c);
            accountFullMapTemplate.put(acc.Id, acc);
        }

        for(Contact c : [select Id, AccountId from Contact where AccountId in: contactMap.keySet()]){
            contactMap.put(c.AccountId, c.Id);
        }

        for(Contact c : [select Id, AccountId, Email from Contact where Email in: emailMap.keySet() and Email != null]){
            emailMap.put(c.Email, c);
        }

        //Parent campaign creation
        for(Campaign c : [select Id, Selection_Name__c, Name from Campaign where Name in: campaignSet]){
            if(c.Selection_Name__c != null){
                campaignMapChild.put(c.Name + ',' + c.Selection_Name__c, c.Id);
            } else if(campaignMapParent.containsKey(c.Name)){
                campaignMapParent.put(c.Name, c.Id);
            } else {
                campaignMapChild.put(c.Name + ',' + c.Selection_Name__c, c.Id);
            }
        }

        for(String s : campaignMapParent.keySet()){
            if(campaignMapParent.get(s) == null){
                Campaign c = new Campaign(Name = s,
                        RecordTypeId = recordType);
                parentCampaignToInsert.add(c);
            }
        }

        insert parentCampaignToInsert;

        for(Campaign c : parentCampaignToInsert){
            campaignMapParent.put(c.Name, c.Id);
        }

        //Child campaign creation
        for(String s : campaignMapChild.keySet()){
            if(campaignMapChild.get(s) == null){
                Unica_Campaign__c uc = ucToCreate.get(s);
                Campaign c = new Campaign();
                for (String campaignField : fieldMappingCampaign.keySet()) {
                    String unicaField = fieldMappingCampaign.get(campaignField);
                    Object unicaValue;
                    if(campaignFieldMap.get(campaignField).getDescribe().getType().name() == 'DATE' && uc.get(unicaField)!=null){
                        unicaValue = Date.valueOf(String.valueOf(uc.get(unicaField)));
                    } else{
                        unicaValue = uc.get(unicaField);
                    }
                    c.put(campaignField,unicaValue);
                }
                c.RecordTypeId = recordType;
                if(s.substringAfter(',') != null){
                    c.ParentId = campaignMapParent.get(s.substringBefore(','));
                }
                childCampaignToInsert.add(c);
            }
        }

        insert childCampaignToInsert;

        for(Campaign c : childCampaignToInsert){
            campaignMapChild.put(c.Name + ',' + c.Selection_Name__c, c.Id);
        }

        //Lead creation
        Id rtID = [SELECT Id FROM RecordType WHERE DeveloperName =: 'Leads_CVM'].Id;

        for (Unica_Campaign__c uc : cmCreate.values()){
            if(leadMap.get(uc.NTA_Id__c) == null){
                system.debug('***SP*** found NTA');
                if(banMap.get(uc.Ban__c) != null /*&& banMap.get(uc.Ban__c).Id != vodafone*/){
                    system.debug('***SP*** found BAN');
                    Lead lead = new Lead();
                    for (String leadField : fieldMappingLead.keySet()) {
                        String ucField = fieldMappingLead.get(leadField);
                        Object ucValue;
                        if(leadFieldMap.get(leadField).getDescribe().getType().name() == 'BOOLEAN'){
                            if(uc.get(ucField) == 'Y'){
                                ucValue = Boolean.valueOf('true');
                            } else {
                                ucValue = Boolean.valueOf('false');
                            }
                        } else if(leadFieldMap.get(leadField).getDescribe().getType().name() == 'DATE'){
                            if(uc.get(ucField) != null && String.valueOf(uc.get(ucField)) != null)
                                ucValue = Date.valueOf(String.valueOf(uc.get(ucField)));
                        } else {
                            ucValue = uc.get(ucField);
                        }
                        lead.put(leadField,ucValue);
                    }
                    Lead.Ban__c = banMap.get(uc.Ban__c).Id;

                    string companyName = banMap.get(uc.Ban__c).BAN_Name__c;
                    if(companyName == null || companyName == '') companyName = 'No Ban Name';
                    lead.Company = companyName;
                    lead.Account_name__c = banMap.get(uc.Ban__c).Account__c;
                    lead.Email = banMap.get(uc.Ban__c).Owner.Email;



                    system.debug(uc.Ban__c);
                    system.debug(banMap);
                    system.debug(banMap.get(uc.Ban__c));
                    String dealerCode = GeneralUtils.stripDealerCode(banMap.get(uc.Ban__c).Dealer_Code__c);
                    String mobileDealerCode = GeneralUtils.stripDealerCode(banMap.get(uc.Ban__c).Owner_Dealercode__c);
                    system.debug(dealerCode);
                    Boolean ownerFound = false;

                    //first try to assign it to owner dealer code, then if it fails, go to standard process
                    system.debug('***SP*** mobileDealerCodeToOwner: ' + mobileDealerCodeToOwner);
                    system.debug('***SP*** dealerCode: ' + dealerCode);
                    system.debug('***SP*** mobileDealerCodeToOwner.get(dealerCode): ' + mobileDealerCodeToOwner.get(dealerCode));

                    Boolean isDealerCampaign = false;
                    for (String dealerCamString : dealerZPOwnerCampaign.keySet()) {
                        if (!String.isEmpty(uc.Campaign_Name__c)) {
                            if (uc.Campaign_Name__c.contains(dealerCamString)) {
                                isDealerCampaign = true;
                            }
                        }
                    }
                    if(isDealerCampaign) {
                        system.debug(GeneralUtils.postalCodeToZSPMap);
                        if(uc.Post_Code__c != null && GeneralUtils.postalCodeToZSPMap.containsKey(uc.Post_Code__c.subString(0,4))){

                            //Only bans with Class_4_Name like ‘VFW’, ‘Telesales’ or ‘E-Sales’ can be assigned to a ZSP based on their postal code when not having a contact on dealer or parent dealer level.
                            Ban__c myBan = banMap.get(uc.Ban__c);

                            if(myBan != null) {

                                //moved these lines here so it does not break in test classes, where there is no post code, it would also break if post code is empty
                                system.debug(uc.Post_Code__c);
                                system.debug(uc.Post_Code__c.subString(0,4));

                                lead.OwnerId = GeneralUtils.postalCodeToZSPMap.get(uc.Post_Code__c.subString(0,4));
                                ownerFound = true;
                                system.debug('***SP*** 0');
                            }
                        }
                    } else if(mobileDealerCodeToOwner.containsKey(mobileDealerCode) && mobileDealerCodeToOwner.get(mobileDealerCode) != null){
                        // base owner on ban mobile dealercode owner
                        lead.OwnerId = mobileDealerCodeToOwner.get(mobileDealerCode); //owner is dealer code contact
                        ownerFound = true;
                        system.debug('***SP*** 1a');
                    } else if(dealerCodeToOwner.containsKey(dealerCode) && dealerCodeToOwner.get(dealerCode) != null){
                        lead.OwnerId = dealerCodeToOwner.get(dealerCode); //owner is dealer code contact
                        ownerFound = true;
                        system.debug('***SP*** 1b');
                    } else {
                        // if no ban dealercode owner found, or it is inactive, see if dealer has a parent deal with a contact-user
                        if(dealerCodeToParentDealerCode.containsKey(dealerCode)){
                            if(parentDealerCodeToParentOwner.containsKey(dealerCodeToParentDealerCode.get(dealerCode))){

                                lead.OwnerId = parentDealerCodeToParentOwner.get(dealerCodeToParentDealerCode.get(dealerCode));
                                ownerFound = true;
                                system.debug('***SP*** 2');
                            }
                        }
                    }
                    system.debug(ownerFound);
                    if(!ownerFound) {

                        system.debug(GeneralUtils.postalCodeToZSPMap);
                        if(uc.Post_Code__c != null && GeneralUtils.postalCodeToZSPMap.containsKey(uc.Post_Code__c.subString(0,4))){

                            //Only bans with Class_4_Name like ‘VFW’, ‘Telesales’ or ‘E-Sales’ can be assigned to a ZSP based on their postal code when not having a contact on dealer or parent dealer level.
                            Ban__c myBan = banMap.get(uc.Ban__c);

                            if(myBan != null && (myBan.Class_4_Name__c == 'VFW' || myBan.Class_4_Name__c == 'Telesales' || myBan.Class_4_Name__c == 'E-Sales')) {

                                //moved these lines here so it does not break in test classes, where there is no post code, it would also break if post code is empty
                                system.debug(uc.Post_Code__c);
                                system.debug(uc.Post_Code__c.subString(0,4));

                                lead.OwnerId = GeneralUtils.postalCodeToZSPMap.get(uc.Post_Code__c.subString(0,4));
                                ownerFound = true;
                                system.debug('***SP*** 3');
                            }
                        }
                    }

                    //if there is an ITW lead owner on account level, take that and assign it to him

                    Account acc = accountFullMapTemplate.get(banMap.get(uc.Ban__c).Account__c);

                    //if it's ITW campaign and if the account is ITW flagged, assign it to the ITW team member
                    if(uc.Campaign_Channel__c == 'ITW'){

                        if (acc.ITW_Indicator__c == true && acc.ITW_Account_Manager_Overwrite__c == false && acc.ITW_Team_Member__c != null){
                            Lead.OwnerId = acc.ITW_Team_Member__c;
                        }
                    }


                    if(!ownerFound) {
                        uc.Error__c = 'No active SFDC user (ban dealercode contactuser or parent dealer contactuser) found to assign Lead ownership';
                        ucToBeDeleted.add(uc);
                        continue;
                    }


                    system.debug(lead.OwnerId);
                    lead.LeadSource = 'CVM Campaign';
                    Lead.RecordTypeId = rtId;
                    Lead.Dealer_Code__c = dealerCode;
                    // also fill Dealer_Name__c field so that distributor can route to the right dealer
                    if(dealerCodeToDealerName.containsKey(dealerCode)){
                        lead.Dealer_Name__c = dealerCodeToDealerName.get(dealerCode);
                    }

                    Lead.Status = 'No follow up yet';
                    lead.CVM_Participation__c = true;
                    if(uc.Selection_Name__c != null){
                        lead.Campaign__c = campaignMapChild.get(uc.Campaign_Name__c + ',' + uc.Selection_Name__c);
                    } else if(campaignMapParent.containsKey(uc.Campaign_Name__c)){
                        lead.Campaign__c = campaignMapParent.get(uc.Campaign_Name__c);
                    } else {
                        lead.Campaign__c = campaignMapChild.get(uc.Campaign_Name__c + ',' + uc.Selection_Name__c);
                    }
                    system.debug(lead.Campaign__c);
                    if(emailMap.containsKey(Lead.Contact_Email__c) && emailMap.get(Lead.Contact_Email__c) != null){
                        Contact c = emailMap.get(Lead.Contact_Email__c);
                        if(c.AccountId == lead.Account_name__c){
                            lead.Contact__c = c.Id;
                        }
                    }
                    if(contactMap.containsKey(lead.Account_name__c) && lead.Contact__c == null){
                        lead.Contact__c = contactMap.get(lead.Account_name__c);
                    }
                    leadlist.add(lead);
                    cmList.add(uc);
                } else {
                    system.debug('***SP*** not found ban');
                    uc.Error__c = 'The BAN doesn\'t exist';
                    ucToBeDeleted.add(uc);
                }
            }
            else{
                system.debug('***SP*** not found NTA');
            }
        }
        system.debug(ucToBeDeleted);

        Database.insert(leadlist,false);

        //Fill maps for initial mail
        Set<Id> leadIds = new Set<Id>();
        for(Lead l : leadlist){
            leadIds.add(l.Id);
        }

        // requery to get the related field from campaign
        List<Lead> allLeads = [Select Id, Lead.Campaign__c, Lead.Campaign__r.Type, BAN__c, NTA_Id__c, OwnerId, Email, Owner.Email From Lead Where Id in :leadIds];
        for(Lead lead : allLeads ){

            //set the email, now owner should be populated
            lead.Email = Lead.Owner.Email; //email should be the lead owner, and not the BAN owner


            leadMap.put(lead.NTA_Id__c, lead.Id);
            if(lead.Campaign__r.Type != null){
                if(lead.Campaign__r.Type.toLowerCase() == 'recommit'){
                    if(banMapTemplate.get(lead.BAN__c) == 'BP'){
                        if(!leadIdsBP.containsKey(lead.OwnerId + ',' + lead.Campaign__c)){
                            leadIdsBP.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
                        }
                    } else if(banMapTemplate.get(lead.Ban__c) == 'Distri'){
                        if(!leadIdsITRD.containsKey(lead.OwnerId + ',' + lead.Campaign__c)){
                            leadIdsITRD.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
                        }
                    } else if(profileMap.get(lead.OwnerId) == 'VF Retail ZS'){
                        if(!leadIdsZSP.containsKey(lead.OwnerId + ',' + lead.Campaign__c)){
                            leadIdsZSP.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
                        }
                    }
                } else if(lead.Campaign__r.Type.toLowerCase() == 'saves'){
                    if(banMapTemplate.get(lead.Ban__c) == 'BP'){
                        if(!leadIdsBPSaves.containsKey(lead.OwnerId + ',' + lead.Campaign__c)){
                            leadIdsBPSaves.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
                        }
                    } else if(banMapTemplate.get(lead.Ban__c) == 'Distri'){
                        if(!leadIdsITRDSaves.containsKey(lead.OwnerId + ',' + lead.Campaign__c)){
                            leadIdsITRDSaves.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
                        }
                    }
                } else if(lead.Campaign__r.Type.toLowerCase() == 'xusell'){
                    if(banMapTemplate.get(lead.Ban__c) == 'BP'){
                        if(!leadIdsBPXUSell.containsKey(lead.OwnerId + ',' + lead.Campaign__c)){
                            leadIdsBPXUSell.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
                        }
                    }
                }
            }
        }

        update allLeads;

        //Campaignmember creation
        for (Unica_Campaign__c uc : cmCreate.values()){
            if(campaignMemberMap.get(uc.NTA_Id__c) == null && uc.Error__c==null){
                if(banMap.get(uc.Ban__c) != null && leadMap.get(uc.NTA_Id__c) != null){
                    CampaignMember cm = new CampaignMember();
                    for (String campaignField : fieldMappingCampaignMember.keySet()) {
                        String ucField = fieldMappingCampaignMember.get(campaignField);
                        Object ucValue = uc.get(ucField);
                        cm.put(campaignField,ucValue);
                    }
                    if(uc.Selection_Name__c != null){
                        cm.CampaignId = campaignMapChild.get(uc.Campaign_Name__c + ',' + uc.Selection_Name__c);
                    } else if(campaignMapParent.containsKey(uc.Campaign_Name__c)){
                        cm.CampaignId = campaignMapParent.get(uc.Campaign_Name__c);
                    } else {
                        cm.CampaignId = campaignMapChild.get(uc.Campaign_Name__c + ',' + uc.Selection_Name__c);
                    }
                    cm.LeadId = leadMap.get(uc.NTA_Id__c);
                    cm.Ban_Number__c = banMap.get(uc.Ban__c).Id;
                    campaignMemberList.add(cm);
                }
            }
        }

        List<Database.SaveResult> UR = Database.insert(campaignMemberList,false);
        For (Integer i=0;i<UR.Size();i++){
            Unica_Campaign__c loadRecord = scopeMap.get(String.valueof(campaignMemberList[i].UnicaCampaignID__c));
            if (!UR[i].isSuccess()){
                loadRecord.Error__c=String.valueof(UR[i].getErrors()).left(255);
                ucToBeDeleted.add(loadRecord);
            }
        }

        for(Unica_Campaign__c uc : ucToBeDeleted){
            for(String field : sortList){
                if(queriedFields.contains(field)){
                    dataError += '"' + uc.get(field) + '",';
                }
            }
            dataError += '"' + uc.Error__c + '"\n';
        }

        //Update uploaded info
        for(Unica_Campaign__c uc : scope){
            uc.Lead__c = leadMap.get(uc.NTA_Id__c);
        }

        update scope;

        //delete ucToBeDeleted;

    }

    global void finish(Database.BatchableContext BC) {

        AsyncApexJob a = [SELECT
                Status,
                NumberOfErrors,
                JobItemsProcessed,
                ExtendedStatus
        FROM
                AsyncApexJob
        WHERE
                Id =: BC.getJobId()];




        //Send emails to BP to tell them the campaign is ready
        if(leadIdsBP.values().size() > 0 ){
            Id bPRecommit = [select Id from EmailTemplate where DeveloperName =: 'Lead_CVM_BP_Recommit_Initial'].Id;
            Messaging.MassEmailMessage mails1 = new Messaging.MassEmailMessage();
            mails1.setTargetObjectIds(leadIdsBP.values());
            mails1.setTemplateId(bPRecommit);
            mails1.setReplyTo('noreply@vodafone.com');
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { mails1 });
        }

        //Send emails to ITRD to tell them the campaign is ready
        if(leadIdsITRD.values().size() > 0 ){
            Id iTRDRecommit = [select Id from EmailTemplate where DeveloperName =: 'Lead_CVM_ITR_D_Recommit_Initial'].Id;
            Messaging.MassEmailMessage mails2 = new Messaging.MassEmailMessage();
            mails2.setTargetObjectIds(leadIdsITRD.values());
            mails2.setTemplateId(iTRDRecommit);
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { mails2 });
        }

        //Send emails to ZSP to tell them the campaign is ready
        if(leadIdsZSP.values().size() > 0){
            Id zSPRecommit = [select Id from EmailTemplate where DeveloperName =: 'Lead_CVM_ZSP_Recommit_Initial'].Id;
            Messaging.MassEmailMessage mails3 = new Messaging.MassEmailMessage();
            mails3.setTargetObjectIds(leadIdsZSP.values());
            mails3.setTemplateId(zSPRecommit);
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { mails3 });
        }

        //Send emails to ZSP to tell them the campaign is ready
        if(leadIdsBPXUSell.values().size() > 0){
            Id bPXUSell = [select Id from EmailTemplate where DeveloperName =: 'Lead_CVM_BP_XUSell_Initial'].Id;
            Messaging.MassEmailMessage mails4 = new Messaging.MassEmailMessage();
            mails4.setTargetObjectIds(leadIdsBPXUSell.values());
            mails4.setTemplateId(bPXUSell);
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { mails4 });
        }

        //Send emails to BP to tell them the campaign is ready
        if(leadIdsBPSaves.values().size() > 0){
            Id bPSaves = [select Id from EmailTemplate where DeveloperName =: 'Lead_CVM_BP_Saves_Initial'].Id;
            Messaging.MassEmailMessage mails5 = new Messaging.MassEmailMessage();
            mails5.setTargetObjectIds(leadIdsBPSaves.values());
            mails5.setTemplateId(bPSaves);
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { mails5 });
        }

        //Send emails to ITRD to tell them the campaign is ready
        if(leadIdsITRDSaves.values().size() > 0){
            Id iTRDSaves = [select Id from EmailTemplate where DeveloperName =: 'Lead_CVM_ITR_D_Saves_Initial'].Id;
            Messaging.MassEmailMessage mails6 = new Messaging.MassEmailMessage();
            mails6.setTargetObjectIds(leadIdsITRDSaves.values());
            mails6.setTemplateId(iTRDSaves);
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { mails6 });
        }


        // Send an email to the user that processed the batch, notifying of job completion.
        // SPavuna: added custom settings for subject, text, and additional email addresses

        UnicaCampaignSettings__c emailSetting = UnicaCampaignSettings__c.getValues('Main');

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        header = header.subString(0, header.length() - 1) + ',"Error__c"\n';
        Blob b = blob.valueOf(header + dataError);
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('UnicaCampaignErrors.csv');
        efa.setBody(b);

        String[] toAddresses = new String[] {UserInfo.getUserEmail()};
        String emailSubject = '';
        String emailStatusText = '';
        String emailErrorText = '';

        if(emailSetting != null)
        {
            if(emailSetting.StatusEmailRecepient1__c != null) toAddresses.add(emailSetting.StatusEmailRecepient1__c);
            if(emailSetting.StatusEmailRecepient2__c != null) toAddresses.add(emailSetting.StatusEmailRecepient2__c);
            if(emailSetting.StatusEmailRecepient3__c != null) toAddresses.add(emailSetting.StatusEmailRecepient3__c);
            if(emailSetting.StatusEmailRecepient4__c != null) toAddresses.add(emailSetting.StatusEmailRecepient4__c);
            if(emailSetting.StatusEmailRecepient5__c != null) toAddresses.add(emailSetting.StatusEmailRecepient5__c);

            if(emailSetting.StatusEmailSubject__c != null) emailSubject = String.format(emailSetting.StatusEmailSubject__c, new List<String>{a.Status});

            if(emailSetting.StatusEmailText__c != null) emailStatusText = String.format(emailSetting.StatusEmailText__c, new List<String>{String.ValueOf(a.JobItemsProcessed), String.ValueOf(a.NumberOfErrors)});

            if(a.NumberOfErrors > 0){
                if(emailSetting.ErrorTextEmail__c != null) emailStatusText = emailStatusText + String.format(emailSetting.ErrorTextEmail__c, new List<String>{a.ExtendedStatus});
            }
        }
        else{
            //default values if it is not configured in custom settings:
            emailSubject = 'Unica Campaign database import ' + a.Status;
            emailStatusText = 'The Unica Update database import job processed ' + a.JobItemsProcessed +' records with '+ a.NumberOfErrors + ' failures.';
            if(a.NumberOfErrors > 0){
                emailStatusText = emailStatusText + ' The following text might help with identifying possible errors: ' + a.ExtendedStatus;
            }
        }

        mail.setToAddresses(toAddresses);

        mail.setSubject(emailSubject);
        mail.setPlainTextBody(emailStatusText);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}