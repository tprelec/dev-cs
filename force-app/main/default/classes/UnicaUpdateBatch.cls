global class UnicaUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		String query = 'SELECT Action__c, Participant_ID__c, Status__c, Status_Date__c, Error__c FROM Unica_Update__c order by Status_Date__c';
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Unica_Update__c> scope) {

		list<Unica_Update__c> ucToBeUpdated = new list<Unica_Update__c>();
		map<String, Lead> leadMap = new map<String, Lead>();
		list<Unica_Update__c> uc2ndCheck = new list<Unica_Update__c>();
		list<Unica_Update__c> ucToBeDeleted = new list<Unica_Update__c>();
		list<Lead> leadsToDelete = new list<Lead>();
		List<Lead> leadsToUpdate = new List<Lead>();

        map<String, Unica_Update__c> scopeMap = new map<String, Unica_Update__c>();
   		for(Unica_Update__c uc : scope){
			if(uc.Participant_ID__c == null){
				uc.Error__c = 'The Participant ID is not supplied';
				ucToBeUpdated.add(uc);
			} else {
	        	// Put the Participant/NTA externalID in a map   
	        	if (!scopeMap.containsKey(uc.Participant_ID__c)) {
	            	scopeMap.put(uc.Participant_ID__c, uc);   	    
					leadMap.put(uc.Participant_ID__c, null);
					uc2ndCheck.add(uc);	            	    		
	        	} else {
					uc.Error__c = 'The Participant ID is a duplicate';
					ucToBeUpdated.add(uc);
	        	}	
			}
		}

		// The Participant ID / NTA ID is the unique ID for the lead from Unica
		for(Lead lead : [SELECT Status, CVM_Update__c, NTA_Id__c, IsConverted FROM Lead WHERE NTA_Id__c in: leadMap.keySet()]){
			leadMap.put(lead.NTA_Id__c, lead);
		}

		for(Unica_Update__c uc : uc2ndCheck){
			if(leadMap.get(uc.Participant_ID__c) == null){
				uc.Error__c = 'The Lead can\'t be found';
				ucToBeUpdated.add(uc);
			} 
			else if(leadMap.get(uc.Participant_ID__c).IsConverted == true){
				uc.Error__c = 'The Lead is already converted';
				ucToBeUpdated.add(uc);	
			}
			else if(uc.Action__c == null) {
				uc.Error__c = 'Action value is missing';
				ucToBeUpdated.add(uc);				
			}
			else if(uc.Status__c == null) {
				uc.Error__c = 'Status value is missing';
				ucToBeUpdated.add(uc);							
			} else {
				List<String> statusUpdates = uc.Status__c.split(';');
				if (uc.Action__c == 'Update' && (statusUpdates.size() != 6 || statusUpdates[0] != 'XXX')) {
					uc.Error__c = 'Status field has invalid format';
					ucToBeUpdated.add(uc);
				}
				else {
					if(uc.Action__c == 'Delete'){
						leadsToDelete.add(leadMap.get(uc.Participant_ID__c));
						ucToBeDeleted.add(uc);
					} else { 
						Lead lead = leadMap.get(uc.Participant_ID__c).clone(true,true,true,true);
						lead.CVM_Update__c = uc.Status__c + ' - ' + uc.Status_Date__c;
						try{
							lead.Unica_Status_Date__c = datetime.valueOf(uc.Status_Date__c);
						} catch( Exception e ){
							lead.Unica_Status_Date__c = null;
						}
						if(uc.Action__c == 'Close'){
							lead.Status = 'Reject - CVM Withdrawal';
						}

						//new change to allow update, case 4669
						//format of status:  XXX;[status];[email];[phone number];[first name];[last name]

						if(uc.Action__c == 'Update'){
							String unicastatus = statusUpdates[1];
							String unicaEmail = statusUpdates[2];
							String unicaphoneNumber = statusUpdates[3];
							String unicaFirstName = statusUpdates[4];
							String unicaLastName = statusUpdates[5];
							
							lead.Email = unicaEmail;
							//all of these phones? why not?
							lead.MobilePhone = unicaphoneNumber;
							lead.Phone = unicaphoneNumber;
							lead.Direct_Phone_Number__c = unicaphoneNumber;
							lead.FirstName = unicaFirstName;
							lead.LastName = unicaLastName;
							lead.Status = unicastatus;
						}

						leadsToUpdate.add(lead);
					}
				}
			}
		}

		List<Database.SaveResult> UR = Database.update(leadsToUpdate,false);
		For (Integer i=0;i<UR.Size();i++){
			Unica_Update__c loadRecord = scopeMap.get(String.valueof(leadsToUpdate[i].NTA_ID__c));
			if (!UR[i].isSuccess()){
				loadRecord.Error__c=String.valueof(UR[i].getErrors()).left(255);
				ucToBeUpdated.add(loadRecord);
			} else {
				ucToBeDeleted.add(loadRecord);
			}
		}

		update ucToBeUpdated;		// This puts any errors back on the load record
		delete ucToBeDeleted;		// This deletes the load record as it loaded successfully
		delete leadsToDelete;		// deletes leads
	}

	global void finish(Database.BatchableContext BC) {

		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							JobItemsProcessed,
							ExtendedStatus
						  FROM
							AsyncApexJob
						  WHERE
							Id =: BC.getJobId()];

		List<Unica_Update__c> failures = [SELECT Participant_ID__c, Error__c FROM Unica_Update__c order by Status_Date__c];
		String failMessage='';
		if (failures.size()>0) {
			failmessage='. RECORDS FAILED!';
		}
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] {UserInfo.getUserEmail()};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Unica Update database import ' + a.Status + failmessage);
		String text = 'The Unica Update database import job processed ' + a.JobItemsProcessed +
					  ' batch(es) with '+ a.NumberOfErrors + ' batch failures.';
		if(a.NumberOfErrors > 0){
			text = text + ' The following text might help with identifying possible errors: ' + a.ExtendedStatus;
		}
		if (failures.size()>0) {
			text = text + '\n'+failures.size()+' records have errors. \nSee the following list:\n';
			text = text + 'Participant_ID__c, Error__c'+'\n';
			for (Unica_Update__c f:failures) {
				text = text + f.Participant_ID__c+', '+f.Error__c+'\n';
			}
		}

		mail.setPlainTextBody(text);
		system.debug(text);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}