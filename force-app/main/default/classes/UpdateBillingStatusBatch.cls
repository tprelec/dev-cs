/**
 * @description         This batch class updates the Billing Status of Customer Assets
 * @author              Jurgen van Westreenen
 */
public class UpdateBillingStatusBatch implements Database.Batchable<sObject> {
	public Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(
			'SELECT Id, Billing_Status__c, (SELECT Id, Is_Recurring__c, Quantity__c FROM Contracted_Products__r WHERE BOP_Status__c = \'OPEN\') FROM Customer_Asset__c WHERE Latest_Activation_Date__c = TODAY'
		);
	}
	public void execute(Database.BatchableContext bc, List<Customer_Asset__c> scope) {
		List<Customer_Asset__c> caList = new List<Customer_Asset__c>();
		for (Customer_Asset__c ca : scope) {
			if (ca.Contracted_Products__r.size() > 0) {
				Contracted_Products__c cp = ca.Contracted_Products__r[0];
				ca.Billing_Status__c = !cp.Is_Recurring__c
					? 'Completed'
					: (cp.Quantity__c > 0 ? 'Active' : 'Cancelled');
				caList.add(ca);
			}
		}
		update caList;
	}
	@SuppressWarnings('PMD.EmptyStatementBlock')
	public void finish(Database.BatchableContext bc) {
		// Nothing to do here
	}
}