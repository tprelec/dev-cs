@RestResource(urlMapping='/updateinstalledbase/*')
/**
 * @description: Performs MACD operations on Customer Assets
 * @author: Jurgen van Westreenen
 */
global without sharing class UpdateInstalledBaseService {
	static List<Error> returnErrors = new List<Error>();
	static String installedBaseId;
	static UpdateInstalledBase reqBody;

	@HttpPost
	global static void processCustomerAssets() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		try {
			reqBody = (UpdateInstalledBase) JSON.deserializeStrict(
				req.requestbody.tostring(),
				UpdateInstalledBase.class
			);
			switch on reqBody.action {
				when 'ADD' {
					performAddAction();
				}
				when 'REMOVE' {
					performRemoveAction();
				}
				when 'CHANGE' {
					performChangeAction();
				}
				when else {
					returnErrors.add(
						new Error('SFEC-0001', 'Missing or incorrect value for field: action')
					);
				}
			}
		} catch (Exception e) {
			// Any other occuring exceptions
			returnErrors.add(
				new Error(
					'SFEC-9999',
					e.getLineNumber() +
					': An unhandled exception occured: ' +
					e.getMessage()
				)
			);
		} finally {
			// Populate response
			Response resp = new Response();
			resp.installedbaseId = installedbaseId;
			if (returnErrors.size() > 0) {
				resp.status = 'FAILED';
				resp.errors = returnErrors;
			} else {
				resp.status = 'OK';
			}
			res.addHeader('Content-Type', 'text/json');
			res.statusCode = 200;
			res.responseBody = Blob.valueOf(JSON.serializePretty(resp));
		}
	}

	private static void performAddAction() {
		// Process ADD request
		Customer_Asset__c ca = new Customer_Asset__c(
			Account__c = reqBody.accountId,
			BOP_Managed_Item_ID__c = reqBody.managedItemId,
			BOP_HDN_Services__c = reqBody.deviceServices,
			Article_Code__c = reqBody.articleCode,
			Is_Shared__c = Boolean.valueOf(reqBody.isVirtual),
			Is_Virtual__c = Boolean.valueOf(reqBody.isShared)
		);
		Id siteId = [
			SELECT Id
			FROM Site__c
			WHERE
				Site_House_Number__c = :Decimal.valueOf(reqBody.houseNumber)
				AND Site_House_Number_Suffix__c = :reqBody.houseNumberSuffix
				AND Site_Postal_Code__c = :reqBody.postalCode
			LIMIT 1
		]
		?.Id;
		ca.Site__c = siteId;
		Id orderId = [
			SELECT Id
			FROM Order__c
			WHERE Sales_Order_Id__c = :reqBody.externalOrderId
			LIMIT 1
		]
		?.Id;
		ca.Order__c = orderId;
		insert ca;
		installedBaseId = [SELECT Id, Installed_Base_Id__c FROM Customer_Asset__c WHERE Id = :ca.Id]
		.Installed_Base_Id__c;
	}

	private static void performRemoveAction() {
		// Process REMOVE request
		Customer_Asset__c ca = retrieveInstBaseInfo();
		if (ca != null) {
			ca.Quantity__c = 0;
			ca.Installation_Status__c = 'Removed';
			update ca;
		}
		installedBaseId = reqBody.installedbaseId;
	}

	private static void performChangeAction() {
		// Process CHANGE request
		Customer_Asset__c ca = retrieveInstBaseInfo();
		if (ca != null) {
			ca.BOP_Managed_Item_ID__c = reqBody.managedItemId != null
				? reqBody.managedItemId
				: ca.BOP_Managed_Item_ID__c;
			ca.Article_Code__c = reqBody.articleCode != null
				? reqBody.articleCode
				: ca.Article_Code__c;
			ca.BOP_HDN_Services__c = reqBody.deviceServices != null
				? reqBody.deviceServices
				: ca.BOP_HDN_Services__c;
			ca.Is_Shared__c = reqBody.isVirtual != null
				? Boolean.valueOf(reqBody.isVirtual)
				: ca.Is_Shared__c;
			ca.Is_Virtual__c = reqBody.isShared != null
				? Boolean.valueOf(reqBody.isShared)
				: ca.Is_Virtual__c;
			update ca;
		}
		installedBaseId = reqBody.installedbaseId;
	}

	private static Customer_Asset__c retrieveInstBaseInfo() {
		List<Customer_Asset__c> caList = [
			SELECT
				Id,
				Account__c,
				Site__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Postal_Code__c,
				Installed_Base_Id__c,
				BOP_Managed_Item_ID__c,
				Order__r.Sales_Order_Id__c,
				Article_Code__c,
				BOP_HDN_Services__c
			FROM Customer_Asset__c
			WHERE Installed_Base_Id__c = :reqBody.installedbaseId
		];
		if (caList.size() > 0) {
			Customer_Asset__c ca = caList[0];
			Boolean accMatch = (ca.Account__c == reqbody.accountId);
			Boolean siteMatch = (String.valueOf(ca.Site__r?.Site_House_Number__c) ==
			reqBody.houseNumber &&
			ca.Site__r?.Site_House_Number_Suffix__c == reqBody.houseNumberSuffix &&
			ca.Site__r?.Site_Postal_Code__c == reqBody.postalCode);
			Boolean ordMatch = (reqBody.externalOrderId != null
				? ca.Order__r?.Sales_Order_Id__c == reqBody.externalOrderId
				: true);
			if (accMatch && siteMatch && ordMatch) {
				return ca;
			} else {
				String errMessage;
				if (!accMatch) {
					errMessage =
						'Supplied accountId (' +
						reqbody.accountId +
						') does not match the value in SF: ' +
						ca.Account__c;
					returnErrors.add(new Error('SFEC-0007', errMessage));
				}
				if (!siteMatch) {
					errMessage =
						'Supplied location data (' +
						reqBody.houseNumber +
						' ' +
						reqBody.houseNumberSuffix +
						', ' +
						reqBody.postalCode +
						') does not match the value in SF: ' +
						ca.Site__r?.Site_House_Number__c +
						' ' +
						ca.Site__r?.Site_House_Number_Suffix__c +
						', ' +
						ca.Site__r?.Site_Postal_Code__c;
					returnErrors.add(new Error('SFEC-0007', errMessage));
				}
				if (!ordMatch) {
					errMessage =
						'Supplied externalOrderId (' +
						reqbody.externalOrderId +
						') does not match the value in SF: ' +
						ca.Order__r?.Sales_Order_Id__c;
					returnErrors.add(new Error('SFEC-0007', errMessage));
				}
			}
		} else {
			returnErrors.add(
				new Error(
					'SFEC-0002',
					'No record found with installedbaseId: ' + reqBody.installedbaseId
				)
			);
		}
		return null;
	}

	@TestVisible
	class UpdateInstalledBase {
		@TestVisible
		String accountId;
		@TestVisible
		String siteId;
		@TestVisible
		String houseNumber;
		@TestVisible
		String houseNumberSuffix;
		@TestVisible
		String postalCode;
		@TestVisible
		String action;
		@TestVisible
		String installedbaseId;
		@TestVisible
		String managedItemId;
		@TestVisible
		String externalOrderId;
		@TestVisible
		String articleCode;
		@TestVisible
		String deviceServices;
		@TestVisible
		String isVirtual;
		@TestVisible
		String isShared;
	}
	@TestVisible
	class Response {
		@TestVisible
		String status;
		@TestVisible
		String installedBaseId;
		@TestVisible
		List<Error> errors;
	}
	@TestVisible
	class Error {
		@TestVisible
		String errorCode;
		@TestVisible
		String errorMessage;
		@TestVisible
		Error(String errorCode, String errorMessage) {
			this.errorCode = errorCode;
			this.errorMessage = errorMessage;
		}
	}
}