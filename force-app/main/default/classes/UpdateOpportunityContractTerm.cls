public class UpdateOpportunityContractTerm implements Database.Batchable<sObject>, Database.Stateful {
    public Integer recordsProcessed = 0;
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT Id, '+ 
                'cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c, '+
                'cscfga__Value__c '+ 
            'FROM cscfga__Attribute__c '+
            'WHERE cscfga__Attribute_Definition__r.LG_ProductDetailType__c = \'Contract Term\' '+  
            'AND cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c!=null '+
            'AND cscfga__Product_Configuration__r.cscfga__Product_Basket__r.LG_CreatedFrom__c =\'Tablet\''
        );
    }
    
    public void execute(Database.BatchableContext bc, List<cscfga__Attribute__c> scope) {
        Map<Id,Opportunity> oppsForUpdate = new Map<Id,Opportunity>();
        for (cscfga__Attribute__c attr : scope) {
            Opportunity opp = new Opportunity();
            opp.Id = attr.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c;
            opp.LG_ContractTermMonths__c = Integer.valueof(attr.cscfga__Value__c);
            if (oppsForUpdate != null && !oppsForUpdate.containsKey(opp.Id)) {
                oppsForUpdate.put(opp.Id, opp);
            }
        }
        if (oppsForUpdate != null) {
            recordsProcessed = recordsProcessed + oppsForUpdate.size();
            System.debug('*****Contract Term update ' + oppsForUpdate.values());
            update oppsForUpdate.values();
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        System.debug('*****Contract Term update finished - number of processed records = ' + recordsProcessed);
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()
        ];
    }    
}