@isTest
private class UpdateOpportunityContractTermTest {
    
    @testSetup 
    static void setup() {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;
        Account account = LG_GeneralTest.CreateAccount('Account', '12345678', 'Ziggo', true);
        Opportunity opp = LG_GeneralTest.CreateOpportunity(account, true);
        opp.LG_ContractTermMonths__c = null;
        opp.LG_CreatedFrom__c = 'Tablet';
        upsert opp;
        cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket Test Handler', account, null, opp, false);
        basket.LG_CreatedFrom__c = 'Tablet';
        basket.cscfga__Opportunity__c = opp.Id;
        insert basket;
        cscfga__Product_Category__c prodCategory = LG_GeneralTest.createProductCategory('Test Category', true);
        cscfga__Product_Definition__c prodDefinitionMkbInternet = LG_GeneralTest.createProductDefinition('Zakelijk Internet Pro', false);   
        prodDefinitionMkbInternet.cscfga__Product_Category__c = prodCategory.Id;
        insert prodDefinitionMkbInternet;
        cscfga__Product_Configuration__c prodConfigurationMkbInternet = LG_GeneralTest.createProductConfiguration('MKB Configuration', 3, basket, prodDefinitionMkbInternet, false);
        prodConfigurationMkbInternet.cscfga__Product_Family__c = 'Zakelijk Internet Pro';
        prodConfigurationMkbInternet.LG_MarketSegment__c = 'Small';
        insert prodConfigurationMkbInternet;
        cscfga__Attribute_Definition__c attDefString = LG_GeneralTest.createAttributeDefinition('Internet Product In Basket', prodDefinitionMkbInternet, 'User Input', 'String', null, null, null, true);
        attDefString.LG_ProductDetailType__c = 'Contract Term';
        upsert attDefString;
        LG_GeneralTest.createAttribute('Contract Term', attDefString, false, null, prodConfigurationMkbInternet, false, '12', true);
    }
    
    @isTest
    static void test() {        
        Test.startTest();
        UpdateOpportunityContractTerm uocm = new UpdateOpportunityContractTerm();
        Id batchId = Database.executeBatch(uocm);
        Test.stopTest();
        Opportunity opp = [
            SELECT ID,
                LG_ContractTermMonths__c
            FROM Opportunity
        ];
        System.assertEquals(12, opp.LG_ContractTermMonths__c);
    }
}