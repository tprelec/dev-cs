public with sharing class UserDeactivationController {
 
    public User theUser {get;set;}
    public Id userId {get;set; }
    public Contact userContact {get;set;}
    public Boolean errorFound {get;set;}
    public Boolean noRecordsFound {
        get{
            if( dealerInformations.isEmpty()
               && ownedAccounts.isEmpty()
               && accountTeamMembers.isEmpty()
               && openOpportunities.isEmpty()
               && openLeads.isEmpty()
               && subordinates.isEmpty()
               && runningUserDashboards.isEmpty()
               && primaryPartners.isEmpty()
               && adminPartners.isEmpty()
               && contractsVF.isEmpty()
               && postalCodeAssignments.isEmpty()
               && pendingApprovalProcesses.isEmpty()
               && creditNoteApprovals.isEmpty()
            ){
                return true;
            } else {
                return false;
            }
        }
        set;
    }

    public UserDeactivationController() {
        errorFound = false;
        userId = ApexPages.CurrentPage().getParameters().get('Id');
        Id contactId = ApexPages.CurrentPage().getParameters().get('contactId');
        
        List<User> theUsers = [SELECT Name, UserType, UserName, AccountId, Admin_Partner__c, Profile_Name__c FROM User WHERE Id = :userId OR (contactId != null AND contactId = :contactId) LIMIT 1];
        if (theUsers.isEmpty()) {
            Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'You are not an Admin user for your Partner Account, so you cannot deactivate users. Please contact your admin user.'));
            errorFound = true;            
        } else {
            theUser = theUsers[0];
            userId = theUser.Id;
            // if the current user is a partner user, check if he/she has the correct authorizations
            User currentUser = [SELECT Id, AccountId, Admin_Partner__c FROM User WHERE Id = :userInfo.getUserId()];
            if (currentUser.AccountId != null && !currentUser.Admin_Partner__c) {
                Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'You are not an Admin user for your partner account, so you cannot deactivate users. Please contact your admin user.'));
                errorFound = true;            
            }

            if (userContact == null) {
                List<Contact> userContacts = [SELECT Name, Replacement_User__c, Replacement_User__r.ContactId FROM Contact WHERE UserId__c = :userId];
                if (userContacts.isEmpty()) {
                    Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'User is not synced  with a Contact record. Please make sure that is repaired before trying to deactivate (e.g. by editing the user without doing any changes)!'));
                    errorFound = true;
                } else {
                    userContact = userContacts[0];
                    noRecordsFound = true;
                }
            }
        }
    }

    public Boolean getIsPartnerUser() {
        return GeneralUtils.IsPartnerUser(theUser);
    }

    public pageReference updateAllUsers() {
        if (checkReplacementUser()) return null;
        if (!openOpportunitiesSolutionSales.isEmpty() && checkSolutionSales()) return null;

        updateDealerInformations();
        updateAccounts();
        updateAccountTeamMembers();
        updateOpportunities();
        updateOpportunitiesSolutionSales();
        updateLeads();
        updateSubordinates();
        updatePrimaryPartners();
        updateAdminPartners();
        updateContractsVF();
        updatePostalCodeAssignments();
        updatePendingApprovalProcesses();
        updateCreditNoteApprovals();
        return reloadPage();
    }

    public Boolean checkReplacementUser() {
        if (userContact.Replacement_User__c == null) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, 'Please fill in a replacement user first!'));
            return true;
        } else {
            return false;
        }
    }

    public Boolean checkSolutionSales() {
        User currentUser = [SELECT Id, Name, Profile_Name__c, BigMachines__Delegated_Approver__c FROM User WHERE Id = :userContact.Replacement_User__c];
        String msg = '';
        if (currentUser.Profile_Name__c != 'VF Solution Sales') {  msg = 'Replacement user must have VF Solution Sales profile!'; }
        if (currentUser.BigMachines__Delegated_Approver__c != null) { msg = 'User has a Bigmachines Delegate Approver, remove/replace this to disable the user!'; }
        
        if (String.isNotBlank(msg)) { 
            ApexPages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR, msg));
            return true;
        } else {
            return false;
        }
    }

    public pageReference deactivateUser() {
        theUser.isActive = false;
        SharingUtils.updateRecordsWithoutSharing(theUser);
        return new PageReference('/'+userId);
    }

    public pageReference reloadPage() {
        return null;
        //return new PageReference('/apex/UserDeactivation?Id='+userId).setRedirect(true) ;
    }


    public List<Dealer_Information__c> dealerInformations {
        get {
            if (dealerInformations == null) dealerInformations = [SELECT Name, Dealer_Code__c, Contact__c, Contact__r.UserId__c FROM Dealer_Information__c WHERE Contact__c != null AND Contact__r.UserId__c  = :userId];
            return dealerInformations;
        }
        set;
    }
    
    public pageReference updateDealerInformations() {
        // If no Dealer_Information__c records need to be updated exit now rather than run validation
        if (dealerInformations.isEmpty() && !test.isRunningTest()) {
            return reloadPage();
        } else {
            dealerInformations = [SELECT Name, Dealer_Code__c, Contact__c, Contact__r.UserId__c
                                  FROM Dealer_Information__c 
                                  WHERE Contact__c != null AND Contact__r.UserId__c  = :userId];
            //Account type while on test is prospect, so the test will never be able to find a dealer info with the current filters
        } 

        if (checkReplacementUser()) return null;

        // check replacement user's contact data (requery replacement user to fetch contact id)
        User replacementUser = [SELECT Id, ContactId FROM User WHERE Id = :userContact.Replacement_User__c];
        if (replacementUser.ContactId == null) {
            Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'Replacement User is not a partner user.'));
            return null;            
        }

        for (Dealer_Information__c di : dealerInformations) {
            di.Contact__c = replacementUser.ContactId;
        }
        SharingUtils.updateRecordsWithoutSharing( dealerInformations);
        dealerInformations.clear();
        return reloadPage();
    }

    public List<Account> ownedAccounts {
        get {
            // Will only display the first 100 rows (VF breaks after 1000). All records however will be updated as a new query is run there
            if (ownedAccounts == null) ownedAccounts = [SELECT OwnerId, Name, Mobile_Dealer__r.Contact__r.UserId__c FROM Account WHERE OwnerId = :userId LIMIT 100];     
            return ownedAccounts;
        }
        set;
    }

    public pageReference updateAccounts() {
        if (checkReplacementUser()) return null;

        // lookup dealerinformation data for the replacement user
        Dealer_Information__c[] dis = [SELECT Id, Contact__r.UserId__c FROM Dealer_Information__c WHERE Contact__r.UserId__c = :userContact.Replacement_User__c LIMIT 1];
        if (dis.isEmpty()) {
            Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'Replacement user does not have a Dealer Information entry. Please make sure that is added before transferring any Accounts to that user.'));
            return null;            
        }

        List<Account> accountsForUpdate = [SELECT Id FROM Account WHERE OwnerId = :userId];
        for (Account a : accountsForUpdate) {
            a.Mobile_Dealer__c = dis[0].Id;
            a.Claimed__c = true;
            a.OwnerId = userContact.Replacement_User__c;
        }
        update accountsForUpdate;
        ownedAccounts.clear();
        return reloadPage();
    }        

    public List<AccountTeamMember> accountTeamMembers {
        get {
            if (accountTeamMembers == null) accountTeamMembers = [SELECT AccountId, UserId,TeamMemberRole FROM AccountTeamMember WHERE UserId = :userId AND Account.Type = 'Dealer'];
            return accountTeamMembers;
        }
        set;
    }

    public pageReference updateAccountTeamMembers() {
        if (checkReplacementUser()) return null;

        List<AccountTeamMember> recordsToInsert = new List<AccountTeamMember>();
        for (AccountTeamMember atm : accountTeamMembers) {
            AccountTeamMember newAtm = new AccountTeamMember();
            newAtm.userId = userContact.Replacement_User__c;
            newAtm.AccountId = atm.AccountId;
            newAtm.TeamMemberRole = atm.TeamMemberRole;
            recordsToInsert.add(newAtm);

        }
        
        SharingUtils.deleteRecordsWithoutSharing(accountTeamMembers);
        SharingUtils.insertRecordsWithoutSharing(recordsToInsert);
        accountTeamMembers.clear();
        return reloadPage();
    }

    public List<Opportunity> openOpportunitiesSolutionSales {
        get {
            if (openOpportunitiesSolutionSales == null) openOpportunitiesSolutionSales = [SELECT OwnerId, Name, AccountId FROM Opportunity WHERE Solution_Sales__c = :userId AND IsClosed = false];
            return openOpportunitiesSolutionSales;
        }
        set;
    }

    public List<Opportunity> openOpportunities {
        get {
            if (openOpportunities == null) openOpportunities = [SELECT OwnerId, Name, AccountId FROM Opportunity WHERE OwnerId = :userId AND IsClosed = false];
            return openOpportunities;
        }
        set;
    }

    public pageReference updateOpportunities() {
        if (checkReplacementUser()) return null;

        for (Opportunity o : openOpportunities) {
            o.OwnerId = userContact.Replacement_User__c;
        }
        update openOpportunities;
        openOpportunities.clear();
        return reloadPage();
    }      

    public pageReference updateOpportunitiesSolutionSales() {
        if (openOpportunitiesSolutionSales.isEmpty()) return reloadPage();

        if (checkReplacementUser()) return null;
        if (checkSolutionSales()) return null;

        for (Opportunity o : openOpportunitiesSolutionSales) {
            o.Solution_Sales__c = userContact.Replacement_User__c;
        }

        update openOpportunitiesSolutionSales;
        openOpportunitiesSolutionSales.clear();
        return reloadPage();
    }

    public List<Lead> openLeads {
        get {
            if (openLeads == null) openLeads = [SELECT OwnerId, Name, Status FROM Lead WHERE OwnerId = :userId AND IsClosed__c = false AND IsConverted = false];
            return openLeads;
        }
        set;
    }

    public pageReference updateLeads() {
        if (checkReplacementUser()) return null;

        for (Lead o : openLeads) {
            o.OwnerId = userContact.Replacement_User__c;
        }
        update openLeads;
        openLeads.clear();
        return reloadPage();
    }     

    public List<User> subordinates {
        get {
            if (subordinates == null) subordinates = [SELECT Id, ManagerId, Name, UserName FROM User WHERE ManagerId = :userId AND IsActive = true];
            return subordinates;
        }
        set;
    }

    public pageReference updateSubordinates() {
        if (checkReplacementUser()) return null;

        for (User u : subordinates) {
            u.ManagerId = userContact.Replacement_User__c;
        }
        update subordinates;
        subordinates.clear();
        return reloadPage();
    } 

    public List<User> primaryPartners { // kan er maar 1 zijn
        get {
            if (primaryPartners == null) primaryPartners = [SELECT Id, Name, UserName, Primary_Partner__c FROM User WHERE Id = :userId AND Primary_Partner__c = true];
            return primaryPartners;
        }
        set;
    }

    public pageReference updatePrimaryPartners() {
        if (checkReplacementUser()) return null;

        // Nothing to do if user was not primary partner
        if (!primaryPartners.isEmpty()) {
            primaryPartners[0].Primary_Partner__c = false;
            SharingUtils.updateRecordsWithoutSharing(primaryPartners[0]);
            primaryPartners.clear();

            User replacementUser = new User(Id = userContact.Replacement_User__c);
            replacementUser.Primary_Partner__c = true;
            SharingUtils.updateRecordsWithoutSharing(replacementUser);
        }

        return reloadPage();
    }        

    public List<User> adminPartners { 
        get {
            if (adminPartners == null) adminPartners = [SELECT Id, Admin_Partner__c, Name, UserName 
                                                        FROM User 
                                                        WHERE AccountId = :theUser.AccountId 
                                                                AND Admin_Partner__c = true
                                                                AND isactive=true];
            // only show if the deactivated user is the last active admin user for this partner account
            if (adminPartners.size() == 1 && adminPartners[0].Id == theUser.Id)
                return adminPartners;
            else
                return new List<User>();
        }
        set;
    }

    public pageReference updateAdminPartners() {
        if (checkReplacementUser()) return null;

        // If this is not the last admin user for the partner there is nothing to do.
        if (!adminPartners.isEmpty()) {
            adminPartners[0].Admin_Partner__c = false;
            SharingUtils.updateRecordsWithoutSharing(adminPartners[0]);
            adminPartners.clear();

            User replacementUser = new User(Id = userContact.Replacement_User__c);
            replacementUser.Admin_Partner__c = true;
            SharingUtils.updateRecordsWithoutSharing(replacementUser);
        }

        return reloadPage();
    }       

    public List<Dashboard> runningUserDashboards {
        get {
            if (runningUserDashboards == null) runningUserDashboards = [SELECT Id, Title, FolderName, RunningUserId FROM Dashboard WHERE RunningUserId = :userId];
            return runningUserDashboards;
        }
        set;
    }
    
    /**
    * @author      : Jefferson Absalon
    * @description : W-002369 -> VisualForcePage deactivate user -> Contract VF records where the owner is the user
    */ 
    public List<VF_Contract__c> contractsVF {
        get {
            if (contractsVF == null) contractsVF = [SELECT Id, Contract_Status__c, Dealer_code_responsible__c, Dealer_Information__c, OwnerId, 
                                                   Name, Account__r.Name, Contract_Duration__c, Dealer_Information__r.Contact__r.Userid__c
                                                   FROM VF_Contract__c 
                                                   WHERE Dealer_Information__r.Contact__r.Userid__c = :userId];
            return contractsVF;
        }
        set;
    }

    public pageReference updateContractsVF() {
        if (checkReplacementUser()) return null;

        Dealer_Information__c replacementDI = new Dealer_Information__c();
        List<Dealer_Information__c> replacementDIList = [SELECT Id, Dealer_Code__c, Contact__r.UserId__c 
                                                         FROM Dealer_Information__c 
                                                         WHERE Contact__r.UserId__c = :userContact.Replacement_User__c 
                                                         LIMIT 1];
        if (replacementDIList.isEmpty()) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR,'Replacement user does not have a Dealer Information entry. Please make sure that is added before transferring any Contracts VF to that user.'));
            return null;            
        }
        replacementDI = replacementDIList[0];
        if (!contractsVF.isEmpty()) {
            for (VF_Contract__c CVFtemp : contractsVF) {
                CVFtemp.Dealer_code_responsible__c = replacementDI.Dealer_Code__c;
                CVFtemp.Dealer_Information__c = replacementDI.Id;
                CVFtemp.OwnerId = replacementDI.Contact__r.UserId__c;
            }
        }
        SharingUtils.updateRecordsWithoutSharing(contractsVF);
        contractsVF.clear();
        return reloadPage();
    }    
    
    /**
    * @author      : Jefferson Absalon
    * @description : W-002369 -> VisualForcePage deactivate user -> Postal Code Assignment records where the owner is assigned to
    */ 
    public List<Postal_Code_Assignment__c> postalCodeAssignments {
        get {
            if (postalCodeAssignments == null) postalCodeAssignments = [SELECT Id, Name, Channel__c, hbo_region__c, Dealer_Information__c, UserId__c 
                                                                           FROM Postal_Code_Assignment__c 
                                                                           WHERE Dealer_Information__r.Contact__r.Userid__c = :userId];
            return postalCodeAssignments;
        }
        set;
    }

    public pageReference updatePostalCodeAssignments() {
        if (checkReplacementUser()) return null;
        
        Dealer_Information__c replacementDI = new Dealer_Information__c();
        List<Dealer_Information__c> replacementDIList = [SELECT Id, Dealer_Code__c, Contact__r.UserId__c 
                                                         FROM Dealer_Information__c 
                                                         WHERE Contact__r.UserId__c = :userContact.Replacement_User__c 
                                                         LIMIT 1];
        if (replacementDIList.isEmpty()) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR,'Replacement user does not have a Dealer Information entry. Please make sure that is added before transferring any Postal Code Assignments to that user.'));
            return null;            
        }
        replacementDI = replacementDIList[0];
        if (!postalCodeAssignments.isEmpty()) {
            for (Postal_Code_Assignment__c PCAtemp : postalCodeAssignments) {
                PCAtemp.Dealer_Information__c = replacementDI.Id;
            }
        }
        SharingUtils.updateRecordsWithoutSharing(postalCodeAssignments);
        postalCodeAssignments.clear();
        return reloadPage();
    }
    
    /**
    * @author      : Jefferson Absalon
    * @description : W-002369 -> VisualForcePage deactivate user -> Approvel Process, Workflow rules, flows, that the user is used in
    */ 
    public List<ProcessInstanceWorkitem> pendingApprovalProcesses {
        get {
            if (pendingApprovalProcesses == null) pendingApprovalProcesses = [SELECT ActorId, Actor.Name, Actor.Email, CreatedDate, ProcessInstance.Status, 
                                                                             ProcessInstance.TargetObjectId, ProcessInstance.TargetObject.Name
                                                                             FROM ProcessInstanceWorkitem
                                                                             WHERE ActorId = :userContact.Replacement_User__c AND ProcessInstance.Status = 'Pending'];
            return pendingApprovalProcesses;
        }
        set;
    }

    public pageReference updatePendingApprovalProcesses() {
        if (checkReplacementUser()) return null;

        if (!pendingApprovalProcesses.isEmpty()) {
            for (ProcessInstanceWorkitem PIWtemp : pendingApprovalProcesses) {
                PIWtemp.ActorId = userContact.Replacement_User__c;
            }
        }
        SharingUtils.updateRecordsWithoutSharing(pendingApprovalProcesses);
        pendingApprovalProcesses.clear();
        return reloadPage();
    }
    
    /**
    * @author      : Jefferson Absalon
    * @description : W-002369 -> VisualForcePage deactivate user -> 
    */ 
    public List<CreditNote_Approvals__c> creditNoteApprovals {
        get {
            if (creditNoteApprovals == null) creditNoteApprovals = [SELECT Id, Name, CreditNote_RecordTypes__c, Is_Director__c,
                                                                   Is_Partner_Salesmanager__c, Is_SalesManager__c, User__c
                                                                   FROM CreditNote_Approvals__c
                                                                   WHERE User__c = :userContact.Replacement_User__c];
            return creditNoteApprovals;
        }
        set;
    }

    public pageReference updateCreditNoteApprovals() {
        if (checkReplacementUser()) return null;

        if (!creditNoteApprovals.isEmpty()) {
            for (CreditNote_Approvals__c CNAtemp : creditNoteApprovals) {
                CNAtemp.User__c = userContact.Replacement_User__c;
            }
        }
        SharingUtils.updateRecordsWithoutSharing(creditNoteApprovals);
        creditNoteApprovals.clear();
        return reloadPage();
    }

}