/**
 * @description         This is the class that contains logic for performing the User export to other systems
 *                      Requires WITHOUT SHARING as data might have to be exported (and thus updated) that is not owned by the user
 * @author              Guy Clairbois
 */

public with sharing class UserExport {

    public static Set<Id> usersInExport {
        get{
            if(usersInExport == null){
                usersInExport = new Set<Id>();
            }
            return usersInExport;
        }
        set;
    }

    /*
     *  Description:    This method can be used to schedule the export of a set of Users based on ids
     */
    public static void scheduleUserExportFromUserIds(Set<Id> userIds){
        // By putting a 'dummy' error in the User export results, the scheduled export is triggered
        List<User> usersToUpdate = new List<User>();
        for(Id uId : userIds){
            User u = new User(Id=uId);
            u.BOP_export_Errormessage__c = 'Pending scheduled export..';
            usersToUpdate.add(u);
        }
        if(!usersToUpdate.isEmpty()){
            try{
                update usersToUpdate;
            } catch (dmlException e){
                ExceptionHandler.handleException(e);
            }
        }
    }

    @future(callout=true)
    public static void exportUpdatedUsersOffline(Set<Id> userIds){
        // allow partial success
        Database.update(exportUpdatedUsers(userIds),false);     
        // TODO: handle failed updates      
    }
    
    public static List<User> exportUpdatedUsers(Set<Id> userIds){
        Set<User> usersToUpdate = new Set<User>();
        Set<Id> accountIds = new Set<Id>();
        List<User> updateUsers = new List<User>();
        for(User u : [Select 
                                        Id, 
                                        AccountId,
                                        ContactId,
                                        Email,
                                        Firstname,
                                        Lastname,
                                        Phone,
                                        MobilePhone,
                                        Fax,
                                        Title,
                                        Postalcode,
                                        Street,
                                        City,
                                        Country,
                                        BOP_reference__c,
                                        BOP_export_Errormessage__c,
                                        BOP_export_datetime__c                                      
                                    from 
                                        User 
                                    Where 
                                        Id in :userIds
                                        and BOP_User_Type__c=true])
        {
            updateUsers.add(u);
            accountIds.add(u.AccountId);    
        }
                                        
        Map<Id,Account> acctIdToAccount = getAcctIdToAccount(accountIds);                                       
                
        if(!updateUsers.isEmpty()){
            ECSUserService updateService = new ECSUserService();
            List<ECSUserService.request> requests = new List<ECSUserService.request>();
            Map<String,User> banToUser = new Map<String,User>(); 
            for(User u : updateUsers){
                system.debug(u);
                ECSUserService.request req = new ECSUserService.request();
                if(acctIdToAccount.containsKey(u.AccountId)){
                    if(acctIdToAccount.get(u.AccountId).BOPCode__c != null){
                        req.companyBanNumber = acctIdToAccount.get(u.AccountId).BAN_Number__c;
                        req.companyBopCode = acctIdToAccount.get(u.AccountId).BOPCode__c;
                        //req.companyCorporateId = acctIdToAccount.get(u.AccountId).Corporate_Id__c;
                    } else {
                        u.BOP_export_Errormessage__c = 'Account with BOPCode is required for exporting a user'; // TODO: this might not be required in the future (partners have no BANs!)
                        usersToUpdate.add(u);
                        continue;               
                    }                   
                } else {
                    req.companyBanNumber = GeneralUtils.VodafoneAccount.BAN_Number__c;
                    req.companyBopCode = GeneralUtils.VodafoneAccount.BOPCode__c;
                    //req.companyCorporateId = GeneralUtils.VodafoneAccount.Corporate_Id__c;                    
                }
                req.BOPemail = u.BOP_reference__c!=null?u.BOP_reference__c:u.Email;
                req.email = u.Email;

                req.address = u.Street;
                req.city = u.City;
                req.countryId = 'nl'; //TODO: make this variable
                
                req.fax = u.Fax;
                req.firstname = u.FirstName;
                req.jobTitle = u.Title;
                req.lastname = u.LastName;
                req.mobile = u.MobilePhone;
                req.phone = u.Phone;
                req.zipcode = u.PostalCode; 
                system.debug(req);
                //banToUser.put(req.companyBopCode+u.Email,u);
                banToUser.put(req.companyBopCode+req.BOPemail,u);
                requests.add(req);
                
            }
            updateService.setRequest(requests);
            Boolean success = false;
            try{
                updateService.makeRequest('update');
                success = true;
            } catch (ExWebServiceCalloutException ce){
                for(User u : updateUsers){
                    if(u.BOP_export_Errormessage__c != ce.get255CharMessage()){
                        u.BOP_export_Errormessage__c = ce.get255CharMessage();
                        usersToUpdate.add(u);
                    }   
                }
            }           
            
            if(success){
                for (ECSUserService.response response : updateService.getResponse()){
                    system.debug(response);
                    system.debug(banToUser);
                    String key = response.bopCode+response.email;
                    if(banToUser.containsKey(key)){
                        // retrieve correct user by ban/bop/corporate/zip/housenr/housenrsuffix
                        User u = new User(Id = banToUser.get(key).Id);      
                    
                        if(response.errorCode == null || response.errorCode == '' || response.errorCode == '0'){                
                            // success
                            u.BOP_export_datetime__c = system.now();
                            u.BOP_export_Errormessage__c = null;
                            // set BOP reference to (new) email address
                            u.BOP_reference__c = banToUser.get(key).Email;
                        } else {
                            // failure
                            // add errormessage to user
                            u.BOP_export_Errormessage__c = response.errorCode+' - '+response.errorMessage;
                            
                        }
                        usersToUpdate.add(u);
                    } else {
                        ExceptionHandler.handleException('No BAN+email returned by BOP for key '+key+'. Response: '+response+'banToUser map: '+banToUser);
                        //throw new ExMissingDataException('No BAN+email returned by BOP');
                    }
                }
            }
        }
        return new List<User>(usersToUpdate);
    }       



    @future(callout=true)
    public static void exportNewUsersOffline(Set<Id> userIds){
        // allow partial success and report errors to admin
        List<User> userList = exportNewUsers(userIds);
        Database.SaveResult[] results = Database.update(userList,false);
        for(Integer i=0;i<results.size();i++){
            // handle errors
            Database.SaveResult result = results[i];
            if(!result.isSuccess()){
                system.debug(result.getErrors());
                ExceptionHandler.handleException('Error updating user '+results[i].Id+' after export: '+result.getErrors());
            }
        }

    }
        
    public static List<User> exportNewUsers(Set<Id> userIds){
        List<User> usersToUpdate = new List<User>();
        Set<Id> accountIds = new Set<Id>();
        List<User> createUsers = new List<User>();
        for(User u : [Select 
                                        Id, 
                                        AccountId,
                                        ContactId,
                                        Email,
                                        Firstname,
                                        Lastname,
                                        Phone,
                                        MobilePhone,
                                        Fax,
                                        Title,
                                        Postalcode,
                                        Street,
                                        City,
                                        Country,
                                        BOP_reference__c,
                                        BOP_export_Errormessage__c,
                                        BOP_export_datetime__c                                      
                                    from 
                                        User 
                                    Where                       
                                        Id in :userIds
                                        and BOP_User_Type__c=true])
        {
            createUsers.add(u);
            accountIds.add(u.AccountId);    
        }
                                        
        Map<Id,Account> acctIdToAccount = getAcctIdToAccount(accountIds);
                
        if(!createUsers.isEmpty()){
            ECSUserService createService = new ECSUserService();
            List<ECSUserService.request> requests = new List<ECSUserService.request>();
            Map<String,User> banToUser = new Map<String,User>(); 
            for(User u : createUsers){
                ECSUserService.request req = new ECSUserService.request();
                if(acctIdToAccount.containsKey(u.AccountId)){
                    if(acctIdToAccount.get(u.AccountId).BAN_Number__c != null){
                        req.companyBanNumber = acctIdToAccount.get(u.AccountId).BAN_Number__c;
                        req.companyBopCode = acctIdToAccount.get(u.AccountId).BOPCode__c;
                        //req.companyCorporateId = acctIdToAccount.get(u.AccountId).Corporate_Id__c;
                    } else {
                        u.BOP_export_Errormessage__c = 'Valid Account BAN Number is required for exporting a user'; // TODO: this might not be required in the future (partners have no BANs!)
                        usersToUpdate.add(u);
                        continue;               
                    }
                } else {
                    req.companyBanNumber = GeneralUtils.VodafoneAccount.BAN_Number__c;
                    req.companyBopCode = GeneralUtils.VodafoneAccount.BOPCode__c;
                    //req.companyCorporateId = GeneralUtils.VodafoneAccount.Corporate_Id__c;                    
                }
                req.BOPemail = u.Email;
                
                req.address = u.Street;
                req.city = u.City;
                req.countryId = 'nl'; //TODO: make this variable
                req.email = u.Email;
                req.fax = u.Fax;
                req.firstname = u.FirstName;
                //req.groupEmail
                //req.infix 
                req.jobTitle = u.Title;
                //req.language
                req.lastname = u.LastName;
                req.mobile = u.MobilePhone;
                //req.notes
                req.phone = u.Phone;
                //req.prefix
                req.zipcode = u.PostalCode; 
                
                banToUser.put(req.companyBopCode+u.Email,u);
                requests.add(req);
                
            }
            createService.setRequest(requests);
            Boolean success = false;
            try{
                createService.makeRequest('create');
                success = true;
            } catch (ExWebServiceCalloutException ce){
                for(User u : createUsers){
                    if(u.BOP_export_Errormessage__c != ce.get255CharMessage()){
                        u.BOP_export_Errormessage__c = ce.get255CharMessage();
                        usersToUpdate.add(u);
                    }   
                }
            }           
            
            if(success){
                for (ECSUserService.response response : createService.getResponse()){
                    system.debug(response);
                    String key = response.bopCode+response.email;
                    system.debug(banToUser);
                    system.debug(key);
                    if(banToUser.containsKey(key)){
                        // retrieve correct user by ban/bop/corporate/zip/housenr/housenrsuffix
                        User u = new User(Id = banToUser.get(key).Id);      
                    
                        if(response.errorCode == null || response.errorCode == '' || response.errorCode == '0'){                
                            // success
                            u.BOP_export_datetime__c = system.now();
                            u.BOP_export_Errormessage__c = null;
                            // set BOP reference to (new) email address
                            u.BOP_reference__c = banToUser.get(key).Email;                          
                        } else {
                            // failure
                            if(response.errorCode == '45' || response.errorCode == '3005'){
                                // user already exists. Make this an update instead of a create
                                u.BOP_export_datetime__c = system.now();
                                u.BOP_reference__c = response.email;
                                // also overwrite the errormessage, since errormsgs starting with a number will not be retried
                                u.BOP_export_Errormessage__c = 'awaiting update';
                                
                            } else {
                                // add errormessage to user
                                u.BOP_export_Errormessage__c = response.errorCode+' - '+response.errorMessage;
                            }                                                   
                        }
                        usersToUpdate.add(u);
                    } else {
                        ExceptionHandler.handleException('No BAN+email returned by BOP for key '+key+'. Response: '+response+'banToUser map: '+banToUser);
                        //throw new ExMissingDataException('No BAN+email returned by BOP');
                    }
                }
            }
        }
        return usersToUpdate;
    }
    
    private static Map<Id,Account> getAcctIdToAccount(Set<Id> accountIds){
        return new Map<Id,Account>([Select 
                            Id,
                            Name,
                            BAN_Number__c,
                            BOPCode__c
                        From
                            Account
                        Where
                            Id in :accountIds]);
        
    }                                       

}