/**
 * @description			No exporting of user is done anymore. This class now sync's the user email address to contact email address
 * @author				Gerhard Newman
 */
global class UserExportBatch implements Database.Batchable <sObject>, Database.AllowsCallouts{
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id, Email FROM User Where BOP_Email_Out_of_Sync__c=true and IsActive=true');
    } 
 
    global void execute(Database.BatchableContext BC, List<User> scope){
        Map<Id,user> checkContactUserMap = new map<id,user>();
        for(user u:scope){
            checkContactUserMap.put(u.id,u);
        } 
        checkContactEmail(checkContactUserMap);	
    }

    // Check if the contact email is different from the user email
    // If it is then update the contact email to the value of the user email
    // This scenario occurs when the user confirms an email change via an email link which does not fire a trigger
    public static void checkContactEmail(Map<Id,user> checkContactUserMap){
        List<contact> contactsForUpdate = new List<contact>();

        // Query the contact object and get all contacts that link with these users
        List<Contact> contactList = [select id, email, Userid__c from contact where userid__c in:checkContactUserMap.keySet()];

        // Loop through the contacts and see if the matching user has a different email
        for (contact c:contactList) {
            // If the email is different update the email on the contact
            if (c.email!=checkContactUserMap.get(c.userid__c).email) {
                c.email=checkContactUserMap.get(c.userid__c).email;
                c.Hidden_allow_update_contact__c = system.now();
                contactsForUpdate.add(c);
            }
        }

        // update the contacts
        if (contactsForUpdate.size()>0) {
            list<Database.SaveResult> UR = Database.update(contactsForUpdate, false);
            for (Integer i = 0; i < UR.size(); i++) {
                if(!UR[i].isSuccess()){
                    System.Debug('ID:'+String.valueOf(contactsForUpdate[i].get('Id')));
                    System.Debug('Error:'+String.Valueof(UR[i].getErrors()));    
                    System.Debug('Email:'+String.valueOf(contactsForUpdate[i].get('email')));            
                    ExceptionHandler.handleException('UserExportBatch failed to update contact. ID:'+String.valueOf(contactsForUpdate[i].get('Id'))+' Error:'+String.Valueof(UR[i].getErrors())+' Email:'+String.valueOf(contactsForUpdate[i].get('email')));
                }
            }                                   
        }
    }

 
    global void finish(Database.BatchableContext BC){
		ExportUtils.startNextJob('Sites');
    }
 
}