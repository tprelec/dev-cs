/**
 * @description         This is the trigger handler for the User sObject.
 * @author              Guy Clairbois
                        Marcel Vreuls: US-0000301 -> W-001603: autoassign cloudsense licenes

                        Chris Appels: Refactored AssignCloudsense to remove Queries in for loops, deleted obsolete code 19-12-2019
 */
public without sharing class UserTriggerHandler extends TriggerHandler {



    /**
     * @description         This handles the before insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeInsert(){
        List<User> newUsers = (List<User>) this.newList;

        preventRecursiveTrigger(2);
        checkLocale(newUsers);
    }

    /**
     * @description         This handles the after insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterInsert(){
        List<User> newUsers = (List<User>) this.newList;
        Map<Id,User> newUsersMap = (Map<Id,User>) this.newMap;

        preventRecursiveTrigger(2);
        AssignCloudsense(newUsersMap);
        checkLicenses(newUsers, null);
        checkPrimaryUser(newUsers, null);
        prepareForCases(newUsersMap, null);
        preparePartnerContacts(newUsers, null);
        assignPermissionSetToPrimaryPartnerUser(newUsers, null);

    }

    /**
     * @description         This handles the after insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeUpdate(){
        List<User> newUsers = (List<User>) this.newList;
        Map<Id,User> oldUsersMap = (Map<Id,User>) this.oldMap;

        preventRecursiveTrigger(2);
        checkLocale(newUsers);
        preventDisablePortalUser(newUsers, oldUsersMap);
        checkPrimaryUser(newUsers, oldUsersMap);
        updateApprover(newUsers, oldUsersMap);
        checkRelatedRecordsOnDeactivation(newUsers,oldUsersMap);

    }

    /**
     * @description         This handles the after update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterUpdate(){
        List<User> newUsers = (List<User>) this.newList;
        Map<Id,User> oldUsersMap = (Map<Id,User>) this.oldMap;
        Map<Id,User> newUsersMap = (Map<Id,User>) this.newMap;

        preventRecursiveTrigger(2);
        freeUpCPQLicense(newUsers,oldUsersMap);
        AssignCloudsense(newUsersMap);
        checkLicenses(newUsers, oldUsersMap);
        prepareForCases(newUsersMap,oldUsersMap);
        preparePartnerContacts(newUsers, oldUsersMap);
        assignPermissionSetToPrimaryPartnerUser(newUsers, oldUsersMap);

    }

    public static Set<Id> licenseIds = new Set<Id>();
    @testVisible private static Set<Id> getPackageLicenseId(){
        List<String> csLicenseNames = new List<String>();
        csLicenseNames.add('csbb');
        csLicenseNames.add('csclm');
        csLicenseNames.add('csclmcb');
        csLicenseNames.add('csordtelcoa');
        csLicenseNames.add('cscfga');
        csLicenseNames.add('cspl');
        csLicenseNames.add('csdiscounts');

        if (licenseIds.isEmpty()) {
            for (PackageLicense pl : [SELECT Id FROM PackageLicense WHERE NamespacePrefix in :csLicenseNames]){
                licenseIds.add(pl.id);
            }
        }
        return licenseIds;
    }


    private boolean ListFind(List<String> a, String b){
        Set<String> tempSet = new Set<String>();
        tempSet.addAll(a);
        return tempSet.contains(b);
    }

    private boolean ListFindLicense(string uId, String pId){
        Boolean bExist;
        UserPackageLicense[] newlicenses =  [SELECT Id FROM UserPackageLicense WHERE UserId = :uId  AND PackageLicenseId = :pId];

        if(newlicenses.size() > 0)
        {
            bExist = false;
        }
        else
        {
            bExist = true;
        }
        system.debug('ListFindLicense: ' + bExist);
        return bExist;
    }


    private static Set<String> getUserProfileId() {

        Set<String> csProfiles = new Set<String>();
        csProfiles.add('VF Solution Sales');
        csProfiles.add('VF Sales Support');
        csProfiles.add('VF Retail ZS');
        csProfiles.add('VF Retail manager ZS');
        csProfiles.add('VF Sales Manager');
        csProfiles.add('VF Proposal User');
        csProfiles.add('VF Partner Portal User');
        csProfiles.add('VF Manager EBU');
        csProfiles.add('VF Indirect Inside Sales');
        csProfiles.add('VF Head Sales Manager');
        csProfiles.add('VF Financial Sales Control');
        csProfiles.add('VF EBU/WBU Control');
        csProfiles.add('VF Enterprise Legal');
        csProfiles.add('VF Direct Inside Sales');
        csProfiles.add('VF Business Partner Manager');
        csProfiles.add('VF Account Manager');
        csProfiles.add('VF Business Improvement');
        csProfiles.add('System Administrator');
        csProfiles.add('VF Customer Advocacy');

        return csProfiles;
    }

    /** Assign Cloudsense license to Acitve users and deletes them for
        Inactive users*/
    private void AssignCloudsense(Map<Id, User> newUsers)
    {
        Map<Id, User> activeUsersById = new Map<Id, User>();
        Map<Id, User> inActiveUsersById = new Map<Id, User>();
        Set<Id> packageLicenseIds = getPackageLicenseId();
        Set<String> vfUserProfiles = getUserProfileId();
        for (User u : newUsers.values()){
            if (u.IsActive &&
                vfUserProfiles.contains(u.Profile_Name__c)){
                activeUsersById.put(u.Id, u);
            } else if (!u.IsActive) {
                inActiveUsersById.put(u.Id, u);
            }
        }
        Map<Id, Set<Id>> assignedPackagelicenseByUser = new Map<Id, Set<Id>>();
        List<UserPackageLicense> toInsert = new List<UserPackageLicense>();
        List<UserPackageLicense> toDelete = new List<UserPackageLicense>();
        if (!inActiveUsersById.isEmpty()) {
            toDelete = [Select Id from UserPackageLicense where UserId in :inActiveUsersById.keySet() and PackageLicenseId in :packageLicenseIds];
        }
        if (!activeUsersById.isEmpty()) {
            for (UserPackageLicense upl : [Select Id, PackageLicenseId, UserId from UserPackageLicense where UserId in :activeUsersById.keySet() and PackageLicenseId in :packageLicenseIds]) {
                if (assignedPackagelicenseByUser.containsKey(upl.UserId)) {
                    assignedPackagelicenseByUser.get(upl.UserId).add(upl.PackageLicenseId);
                } else {
                    assignedPackagelicenseByUser.put(upl.UserId, new Set<Id>{upl.PackageLicenseId});
                }
            }
            for (User u : activeUsersById.values()){
                for (Id plId : packageLicenseIds) {
                    if (!assignedPackagelicenseByUser.containsKey(u.Id) ||
                        !assignedPackagelicenseByUser.get(u.Id).contains(plId)) {
                        toInsert.add(new UserPackageLicense(UserId = u.Id, PackageLicenseId = plId));
                    }
                }
            }
        }

        database.insert(toInsert, false);
        database.delete(toDelete, false);
    }

    /**
     * @description    W-000572 Cleanup  phonenumbers
     */
    private void CleanUpPhone(List<User> newUsers,Map<Id,User> oldUsersMap){

        for (User mUser : newUsers){
            if (mUser.Phone != null)
            {
                mUser.Phone = StringUtils.cleanNLPhoneNumberInternal(mUser.Phone);
            }
            if (mUser.Fax != null)
            {
                mUser.Fax = StringUtils.cleanNLPhoneNumberInternal(mUser.Fax);
            }
            if (mUser.MobilePhone != null)
            {
                mUser.MobilePhone = StringUtils.cleanNLPhoneNumberInternal(mUser.MobilePhone);
            }
        }
    }

     /**
     * @description    W-0000136 Cleanup licenses if user is disabled
     */
    private void freeUpCPQLicense(List<User> newUsers,Map<Id,User> oldUsersMap){

        Set<Id> usersToDeactivate = new Set<Id>();
        for(User u : newUsers){
            if(u.IsActive == false && oldUsersMap.get(u.Id).IsActive == true){
                usersToDeactivate.add(u.Id);
            }
        }
        if(!usersToDeactivate.isEmpty()){
            System.enqueueJob(new UserTriggerHandlerBatch(usersToDeactivate));
        }
    }

    /**
     * @description         User Locale is checked for non-allowed locales.
     */
    private void checkLocale(List<User> newUsers){
        for(User u : newUsers){
            if(u.LocaleSidKey == 'en_US' || u.LocaleSidKey == 'en_CA'){
                u.LocaleSidKey.addError('Locales English (United States), English (Canada) are blocked by VF NL because they cause invalid /// number formats.');
            }
            else if(u.LocaleSidKey == 'nl'){
                u.LocaleSidKey.addError('Please select Dutch(Netherlands) for dutch locale.');
            }
        }
    }

    /**
     * @description     On deactivation, check if there is any related data that needs to be transferred. If so, check if there is a replacement. If so, transfer the data.
     */
    private void checkRelatedRecordsOnDeactivation(List<User> newUsers, Map<Id,User> oldUsersMap){
        Set<Id> userIdsToCheck = new Set<Id>();
        Set<Id> userIdsWithRecords = new Set<Id>();

        // find all deactivated users
        for(User u : newUsers){
            if(u.IsActive == false && oldUsersMap.get(u.Id).IsActive == true){
                userIdsToCheck.add(u.Id);
            }
        }

        Map<Id,List<Dealer_Information__c>> userIdToDealerInformation = new Map<Id,List<Dealer_Information__c>>();
        Map<Id,List<Account>> userIdToAccount = new Map<Id,List<Account>>();
        Map<Id,List<AccountTeamMember>> userIdToAccountTeamMember = new Map<Id,List<AccountTeamMember>>();
        Map<Id,List<Opportunity>> userIdToOpportunity = new Map<Id,List<Opportunity>>();
        Map<Id,List<Opportunity>> userIdToOpportunitySolutionSales = new Map<Id,List<Opportunity>>();
        Map<Id,List<Lead>> userIdToLead = new Map<Id,List<Lead>>();
        Map<Id,List<User>> userIdToChildUsers = new Map<Id,List<User>>();
        Map<Id,List<Dashboard>> userIdToDashboards = new Map<Id,List<Dashboard>>();

        if (!userIdsToCheck.isEmpty()) {

            // check if there's any relevant data that needs to be transferred
            // Dealer_Information__c
            //Map<Id,List<Dealer_Information__c>> userIdToDealerInformation = new Map<Id,List<Dealer_Information__c>>();
            for(Dealer_Information__c di : [Select Contact__r.UserId__c From Dealer_Information__c Where Contact__r.UserId__c  in :userIdsToCheck AND Is_Dealer_in_SFDC__c = true]){
                userIdsWithRecords.add(di.Contact__r.UserId__c);
                if(userIdToDealerInformation.containsKey(di.Contact__r.UserId__c)){
                    userIdToDealerInformation.get(di.Contact__r.UserId__c).add(di);
                } else {
                    userIdToDealerInformation.put(di.Contact__r.UserId__c,new List<Dealer_Information__c>{di});
                }
            }

            // Account owner and/or partner account owner (BPM)
            //Map<Id,List<Account>> userIdToAccount = new Map<Id,List<Account>>();
            for(Account a : [Select OwnerId From Account Where OwnerId in :userIdsToCheck OR Mobile_Dealer__r.Contact__r.UserId__c in :userIdsToCheck]){
                userIdsWithRecords.add(a.OwnerId);
                if(userIdToAccount.containsKey(a.OwnerId)){
                    userIdToAccount.get(a.OwnerId).add(a);
                } else {
                    userIdToAccount.put(a.OwnerId,new List<Account>{a});
                }
            }

            // partner account team roles
            //Map<Id,List<AccountTeamMember>> userIdToAccountTeamMember = new Map<Id,List<AccountTeamMember>>();
            for(AccountTeamMember atm : [Select AccountId, UserId,TeamMemberRole From AccountTeamMember Where UserId in :userIdsToCheck AND Account.Type = 'Dealer']){
                userIdsWithRecords.add(atm.UserId);
                if(userIdToAccountTeamMember.containsKey(atm.UserId)){
                    userIdToAccountTeamMember.get(atm.UserId).add(atm);
                } else {
                    userIdToAccountTeamMember.put(atm.UserId,new List<AccountTeamMember>{atm});
                }
            }

            // Open Opportunities
            //Map<Id,List<Opportunity>> userIdToOpportunity = new Map<Id,List<Opportunity>>();
            for(Opportunity o : [Select OwnerId From Opportunity Where OwnerId in :userIdsToCheck AND IsClosed = false]){
                userIdsWithRecords.add(o.OwnerId);
                if(userIdToOpportunity.containsKey(o.OwnerId)){
                    userIdToOpportunity.get(o.OwnerId).add(o);
                } else {
                    userIdToOpportunity.put(o.OwnerId,new List<Opportunity>{o});
                }
            }

            // Open Opportunities
            //Map<Id,List<Opportunity>> userIdToOpportunitySolutionSales = new Map<Id,List<Opportunity>>();
            for(Opportunity o : [Select Solution_Sales__c From Opportunity Where Solution_Sales__c in :userIdsToCheck AND IsClosed = false]){
                userIdsWithRecords.add(o.Solution_Sales__c);
                if(userIdToOpportunitySolutionSales.containsKey(o.Solution_Sales__c)){
                    userIdToOpportunitySolutionSales.get(o.Solution_Sales__c).add(o);
                } else {
                    userIdToOpportunitySolutionSales.put(o.Solution_Sales__c,new List<Opportunity>{o});
                }
            }


            // Open Leads
            //Map<Id,List<Lead>> userIdToLead = new Map<Id,List<Lead>>();
            for(Lead l : [Select OwnerId From Lead Where OwnerId in :userIdsToCheck AND IsClosed__c = false and isconverted = false]){
                userIdsWithRecords.add(l.OwnerId);
                if(userIdToLead.containsKey(l.OwnerId)){
                    userIdToLead.get(l.OwnerId).add(l);
                } else {
                    userIdToLead.put(l.OwnerId,new List<Lead>{l});
                }
            }

            // managing other people
            //Map<Id,List<User>> userIdToChildUsers = new Map<Id,List<User>>();
            for(User u : [Select Id, ManagerId From User Where ManagerId in :userIdsToCheck and IsActive = true]){
                userIdsWithRecords.add(u.ManagerId);
                if(userIdToChildUsers.containsKey(u.ManagerId)){
                    userIdToChildUsers.get(u.ManagerId).add(u);
                } else {
                    userIdToChildUsers.put(u.ManagerId,new List<User>{u});
                }
            }

            // Scheduled Reports TBA if needed
            //Map<Id,List<Report>> userIdToReports = new Map<Id,List<Report>>();
            //for(Report r : [Select Id, RunningUserId From Report Where Id = null]){

            //}

            // Dashboards
            //Map<Id,List<Dashboard>> userIdToDashboards = new Map<Id,List<Dashboard>>();
            for(Dashboard d : [Select Id, RunningUserId From Dashboard Where RunningUserId in :userIdsToCheck]){
                userIdsWithRecords.add(d.RunningUserId);
                if(userIdToDashboards.containsKey(d.RunningUserId)){
                    userIdToDashboards.get(d.RunningUserId).add(d);
                } else {
                    userIdToDashboards.put(d.RunningUserId,new List<Dashboard>{d});
                }
            }

        }

        for(User u : newUsers){
            // only go through the users that actually have data to be moved and raise errors for those.
            if(userIdsWithRecords.contains(u.Id)){
                String link = '<a href="/apex/UserDeactivation?Id='+u.Id+'" >user deactivation page</a>';

                if(userIdToDealerInformation.containsKey(u.Id)){
                    u.addError('This user is the main contact for Dealer Information(s). Please go to the '+link+' to deactivate this user.',false);
                }
                if(userIdToAccount.containsKey(u.Id)){
                    u.addError('This user is the Account Owner for one or more accounts. Please go to the '+link+' to deactivate this user.',false);
                }
                if(userIdToAccountTeamMember.containsKey(u.Id)){
                    u.addError('This user is an Account Team Member for one or more Partner Accounts. Please go to the '+link+' to deactivate this user.',false);
                }
                if(userIdToOpportunity.containsKey(u.Id)){
                    u.addError('This user is the owner of one or more open Opportunities. Please go to the '+link+' to deactivate this user.',false);
                }

                if(userIdToOpportunitySolutionSales.containsKey(u.ID)){
                  u.addError('This user is the solution sales of one or more open Opportunities. Please go to the '+link+' to deactivate this user.',false);
                }

                if(userIdToLead.containsKey(u.Id)){
                    u.addError('This user is the owner of one or more open Leads. Please go to the '+link+' to deactivate this user.',false);
                }
                if(userIdToChildUsers.containsKey(u.Id)){
                    u.addError('This user is the manager of one or more other users. Please go to the '+link+' to deactivate this user.',false);
                }
                if(userIdToDashboards.containsKey(u.Id)){
                    u.addError('This user is the running user on one or more dashboards. Please go to the '+link+' to deactivate this user.',false);
                }

            }
        }

    }

    /**
     * @description     This checks if a partneruser can be set to active, based on the available licenses on Account.
     */
    private void checkLicenses(List<User> newUsers, Map<Id,User> oldUsersMap){

        // Sort users per account
        Map<Id, List<User>> usersPerAccountPerTrigger = new Map<Id, List<User>>();
        for(User u : newUsers){
            if(oldUsersMap != null){
                if((u.AccountId != null || oldUsersMap.get(u.Id).AccountId != null) && GeneralUtils.IsPartnerUser(u) && u.IsActive != oldUsersMap.get(u.Id).IsActive){
                    Id accountId = u.AccountId != null ? u.AccountId : oldUsersMap.get(u.Id).AccountId;
                    if(!usersPerAccountPerTrigger.containsKey(accountId)) {
                        usersPerAccountPerTrigger.put(accountId, new List<User>());
                    }
                    usersPerAccountPerTrigger.get(accountId).add(u);
                }
            } else if(u.AccountId != null && GeneralUtils.IsPartnerUser(u) && u.IsActive){
                if(!usersPerAccountPerTrigger.containsKey(u.AccountId)) {
                    usersPerAccountPerTrigger.put(u.AccountId, new List<User>());
                }
                usersPerAccountPerTrigger.get(u.AccountId).add(u);
            }
        }

        // Sort all active users per account to be able to check licenses
        Map<Id, List<User>> usersPerAccount = new Map<Id, List<User>>();
        if(!usersPerAccountPerTrigger.isEmpty()){
            // TO DO: User just one query, instead of 2 : Account.Total_Licenses__c
            for(User u : [SELECT Id, AccountId FROM User WHERE AccountId in : usersPerAccountPerTrigger.keySet() and IsActive =: true]){
                if(!usersPerAccount.containsKey(u.AccountId)) {
                    usersPerAccount.put(u.AccountId, new List<User>());
                }
                usersPerAccount.get(u.AccountId).add(u);
            }


            // Check final amount of licenses
            List<Account> accountList = [Select Id, Total_Licenses__c from Account where Id in : usersPerAccountPerTrigger.keySet()];
            for(Account acc : accountList){
                if(acc.Total_Licenses__c == null){
                    for(User u : usersPerAccountPerTrigger.get(acc.Id)){
                        u.addError('There are no licenses defined. Either enter the total amount of licenses, or request these at your Vodafone partner manager');
                    }
                } else if(usersPerAccount.get(acc.Id) != null && acc.Total_Licenses__c < usersPerAccount.get(acc.Id).size()){
                    for(User u : usersPerAccountPerTrigger.get(acc.Id)){
                        if(u.Isactive){
                            u.addError('There are no available licenses left. Either disable another user, or request more licenses at your Vodafone partner manager');
                        }
                    }
                }
            }
            if(!usersPerAccountPerTrigger.keySet().isEmpty())
                updateAccounts(usersPerAccountPerTrigger.keySet());
        }

        //special check for disabling users though the portal
        Map<Id, List<User>> specialMap = new Map<Id, List<User>>();
        for(User u : newUsers){
            if(oldUsersMap != null){
                /** I don't think below if statement will ever pass because it has both u.IsPortalEnabled == false and
                    u.isActive but I'm leaving it for now because i don't want to break anything -- Chris Appels 19-12-2019*/
                if(u.IsPortalEnabled == false && oldUsersMap.get(u.Id).IsPortalEnabled == true && u.IsActive && GeneralUtils.IsPartnerUser(u) && u.AccountId == oldUsersMap.get(u.Id).AccountId){
                    Id accountId = u.AccountId != null ? u.AccountId : oldUsersMap.get(u.Id).AccountId;
                    if(!specialMap.containsKey(accountId)) {
                        specialMap.put(accountId, new List<User>());
                    }
                    specialMap.get(accountId).add(u);
                }
            }
        }
        if(!specialMap.isEmpty()){
            List<Account> specialList = [Select Id, Total_Licenses__c from Account where Id in : specialMap.keySet()];
            for(Account acc : specialList){
                if(acc.Total_Licenses__c == null){
                    for(User u : specialMap.get(acc.Id)){
                        u.addError('There are no licenses defined. Either enter the total amount of licenses, or request these at your Vodafone partner manager');
                    }
                }
            }
            Map<Id, integer> specialMap2 = new Map<Id, integer>();
            if(!specialMap.keySet().isEmpty()){
                for(Id test : specialMap.keySet()){
                    specialMap2.put(test, specialMap.get(test).size());
                }
                specialUpdateAccounts(specialMap2);
            }
        }
    }

    /**
     * @description     Helper method for checkLicenses
     */
    @future
    private static void updateAccounts(Set<Id> accountIds){
        List<Account> accountList = [SELECT Id, Used_Licenses__c FROM Account WHERE Id in : accountIds];
        Map<Id, List<User>> usersPerAccount = new Map<Id, List<User>>();
        for(User u : [SELECT Id, AccountId FROM User WHERE AccountId in : accountIds and IsActive =: true]){
            if (usersPerAccount.get(u.AccountId) == null) {
                usersPerAccount.put(u.AccountId, new List<User>());
            }
            usersPerAccount.get(u.AccountId).add(u);
        }
        for(Account acc : accountList) {
            if (usersPerAccount.get(acc.Id) != null) {
                acc.Used_Licenses__c = usersPerAccount.get(acc.Id).size();
            }
        }
        if (Test.isRunningTest()) {
            // W-000572: disabled, because in test modus makes no sense to export.
            // start new context via system.runAs() for the same user for test code only
            //System.runAs(new User(Id = Userinfo.getUserId())) {
            //    update accountList;
            //}
        } else {
            // in non-test code insert normally
            update accountList;
        }

    }

    /**
     * @description     Helper method for checkLicenses
     */
    @future
    private static void specialUpdateAccounts(Map<Id, integer> specialMap2){
        List<Account> accountList = [SELECT Id, Used_Licenses__c FROM Account WHERE Id in : specialMap2.keySet()];
        Map<Id, List<User>> usersPerAccount = new Map<Id, List<User>>();
        for(User u : [SELECT Id, AccountId FROM User WHERE AccountId in : specialMap2.keySet() and IsActive =: true]){
            if (usersPerAccount.get(u.AccountId) == null) {
                usersPerAccount.put(u.AccountId, new List<User>());
            }
            usersPerAccount.get(u.AccountId).add(u);
        }
        for(Account acc : accountList){
            acc.Used_Licenses__c = usersPerAccount.get(acc.Id).size() - specialMap2.get(acc.Id);
        }
        if (Test.isRunningTest()) {
            // start new context via system.runAs() for the same user for test code only
            System.runAs(new User(Id = Userinfo.getUserId())) {
                update accountList;
            }
        } else {
            // in non-test code insert normally
            update accountList;
        }
    }

    private void preventDisablePortalUser(List<User> newUsers, Map<Id,User> oldUsersMap){
        String currentProfile = GeneralUtils.currentUser.Profile.Name;
        for(User u : newUsers){
            if(u.IsPortalEnabled == false && oldUsersMap.get(u.Id).IsPortalEnabled == true && currentProfile != 'System Administrator'){
                u.addError('You can\'t deactivate a user via this button.');
                u.IsPrmSuperUser = true;
            }
        }
    }

    private void checkPrimaryUser(List<User> newUsers, Map<Id,User> oldUsersMap){
        set<Id> excludingIds = new set<Id>();
        map<Id, integer> accountUsers = new map<Id, integer>();
        for(User u : newUsers){
            if(u.AccountId != null){
                excludingIds.add(u.Id);
                if (accountUsers.get(u.AccountId) == null) {
                    accountUsers.put(u.AccountId, 0);
                }
                if(u.Primary_Partner__c == true){
                    accountUsers.put(u.AccountId, accountUsers.get(u.AccountId) + 1);
                }
            }
        }
        if(!accountUsers.isEmpty()){
            for(User u : [select AccountId from User where accountId in: accountUsers.keyset() and Primary_Partner__c =: true and Id not in: excludingIds]){
                accountUsers.put(u.AccountId, accountUsers.get(u.AccountId) + 1);
            }
        }
        for(User u : newUsers){
            if(oldUsersMap != null){
                if(u.Primary_Partner__c == true && oldUsersMap.get(u.Id).Primary_Partner__c == false){
                    if(accountUsers.get(u.AccountId) > 1){
                        u.addError('There can only be one Primary Partner User');
                    }
                }
            } else {
                if(u.Primary_Partner__c == true){
                    if(accountUsers.get(u.AccountId) > 1){
                        u.addError('There can only be one Primary Partner User');
                    }
                }
            }
        }
    }

    public static PermissionSet delegatedAdminPS {
        get {
            if (delegatedAdminPS == null) delegatedAdminPS = [SELECT Id FROM PermissionSet WHERE Name = :'Delegated_External_User_Administrator' LIMIT 1];
            return delegatedAdminPS;
        }
        private set;
    }

    private void assignPermissionSetToPrimaryPartnerUser(List<User> newUsers, Map<Id,User> oldUsersMap){

        Map<Id, PermissionSetAssignment> usersWithAssignment = new Map<Id, PermissionSetAssignment>();
        List<PermissionSetAssignment> insertAssignment = new List<PermissionSetAssignment>();
        List<PermissionSetAssignment> deleteAssignment = new List<PermissionSetAssignment>();

        //PermissionSet permSet = [select Id from PermissionSet where Name =: 'Delegated_External_User_Administrator' limit 1];
        for(PermissionSetAssignment psa : [select AssigneeId from PermissionSetAssignment where PermissionSetId =: delegatedAdminPS.Id]){
            usersWithAssignment.put(psa.AssigneeId, psa);
        }

        for(User u : newUsers){
            //Update
            if(oldUsersMap != null){
                if(u.Admin_Partner__c != oldUsersMap.get(u.Id).Admin_Partner__c){
                    if(u.Admin_Partner__c == true){
                        if(!usersWithAssignment.containsKey(u.Id)){
                            insertAssignment.add(new PermissionSetAssignment(AssigneeId = u.Id,
                                                                             PermissionSetId = delegatedAdminPS.Id));
                        }
                    } else {
                        if(usersWithAssignment.containsKey(u.Id)){
                            deleteAssignment.add(usersWithAssignment.get(u.Id));
                        }
                    }
                }
            //Insert
            } else {
                if(u.Admin_Partner__c == true){
                    if(!usersWithAssignment.containsKey(u.Id)){
                        insertAssignment.add(new PermissionSetAssignment(AssigneeId = u.Id,
                                                                         PermissionSetId = delegatedAdminPS.Id));
                    }
                }
            }
        }
        insert insertAssignment;
        delete deleteAssignment;
    }

    private static void updateApprover(List<User> newUsers, Map<Id,User> oldUsersMap){
        for(User u : newUsers){
            if(u.delegatedApproverId != oldUsersMap.get(u.Id).delegatedApproverId){
                u.BigMachines__Delegated_Approver__c = u.delegatedApproverId;
            }
        }
    }

    /**
     * @description     Method to prepare data for contactsForCases method
     */
    private void prepareForCases(Map<Id,User> newUsersMap, Map<Id,User> oldUsersMap){
        Id recordType = GeneralUtils.recordTypeMap.get('Contact').get('Internal_Users');
        Map<Id, Contact> existingContacts = new Map<Id, Contact>();
        List<String> serializedContacts = new List<String>();
        for(Contact c : [select Id, Userid__c, Firstname, LastName, Email, Title, Phone, MobilePhone, IsActive__c, ReportsToId from Contact where accountId =:GeneralUtils.vodafoneAccount.Id]){
            existingContacts.put(c.Userid__c, c);
        }
        for(User u : newUsersMap.values()){
            // only for internal users
            if(!GeneralUtils.IsPartnerUser(u)){
                // only for new user or if relevant data changed
                if(oldUsersMap == null || (
                        u.FirstName != oldUsersMap.get(u.Id).FirstName
                        || u.LastName != oldUsersMap.get(u.Id).LastName
                        || u.Email != oldUsersMap.get(u.Id).Email
                        || u.Title != oldUsersMap.get(u.Id).Title
                        || u.Phone != oldUsersMap.get(u.Id).Phone
                        || u.MobilePhone != oldUsersMap.get(u.Id).MobilePhone
                        || u.IsActive != oldUsersMap.get(u.Id).IsActive
                        || u.ManagerId != oldUsersMap.get(u.Id).ManagerId
                    ))
                {
                    Contact c;
                    Boolean populate;
                    ID ReportsToId = (u.ManagerId!=null?(existingContacts.containsKey(u.ManagerId)?existingContacts.get(u.ManagerId).id:null):null);

                    // Use existing contact if we have it and check if a value actually changed otherwise we are doing things we just dont need to do
                    if (existingContacts.containsKey(u.Id)) {
                        c = existingContacts.get(u.Id);
                        if (u.FirstName != c.FirstName
                            || u.LastName != c.LastName
                            || u.Email != c.Email
                            || u.Title != c.Title
                            || u.Phone != c.Phone
                            || u.MobilePhone != c.MobilePhone
                            || u.IsActive != c.IsActive__c
                            || ReportsToId != c.ReportsToId) {
                                populate=true;
                            } else {
                                populate=false;
                            }
                    } else {
                        // Otherwise create a new contact
                        c = new Contact();
                        populate=true;
                    }

                    if (populate) {
                        c.Userid__c = u.Id;
                        c.Firstname = u.FirstName;
                        c.LastName = u.LastName;
                        c.AccountId = GeneralUtils.vodafoneAccount.Id;
                        c.Email = u.Email;
                        c.Title = u.Title;
                        c.Phone = u.Phone;
                        c.MobilePhone = u.MobilePhone;
                        c.OwnerId = u.Id;
                        c.IsActive__c = u.IsActive;
                        c.Hidden_allow_update_contact__c = system.now();
                        c.RecordTypeId = recordType;
                        c.ReportsToId = ReportsToId;
                        String s = JSON.serialize(c);
                        serializedContacts.add(s);
                    }
                }
            }
        }
        if(!serializedContacts.isEmpty()){
            contactsForCases(serializedContacts);
        }
    }

    private static void preparePartnerContacts(List<User> newUsers, Map<Id,User> oldUsersMap){
        Map<Id, User> contactMap = new Map<Id, User>();
        List<String> serializedContacts = new List<String>();
        for(User u : newUsers){
            if(oldUsersMap == null){
                contactMap.put(u.ContactId, u);
            } else if(GeneralUtils.IsPartnerUser(u) && (u.Phone != oldUsersMap.get(u.Id).Phone ||
                                                        u.MobilePhone != oldUsersMap.get(u.Id).MobilePhone ||
                                                        u.Email != oldUsersMap.get(u.Id).Email ||
                                                        u.IsActive != oldUsersMap.get(u.Id).IsActive ||
                                                        u.FirstName != oldUsersMap.get(u.Id).FirstName ||
                                                        u.LastName != oldUsersMap.get(u.Id).LastName)){
                contactMap.put(u.ContactId, u);
            }
        }
        if(!contactMap.isEmpty()){
            List<Contact> contactList = [select Id, IsActive__c, Phone, MobilePhone, Email, FirstName, LastName, Userid__c from Contact where Id in: contactMap.keySet()];
            // This loop needs to compare the contact and user values otherwise a mini loop can occur where a contact update triggers a user update
            // which then triggers a contact update again (resource wasteful)
            for(Contact c : contactList){
                User u = contactMap.get(c.Id);
                if (c.Phone!=u.Phone ||
                    c.MobilePhone != u.MobilePhone ||
                    c.Email != u.Email ||
                    c.FirstName != u.FirstName ||
                    c.LastName != u.LastName ||
                    c.IsActive__c != u.IsActive ||
                    c.Userid__c != u.Id) {
                        c.Phone = u.Phone;
                        c.MobilePhone = u.MobilePhone;
                        c.Email = u.Email;
                        c.FirstName = u.FirstName;
                        c.LastName = u.LastName;
                        c.IsActive__c = u.IsActive;
                        c.Hidden_allow_update_contact__c = system.now();
                        c.Userid__c = u.Id;
                        String s = JSON.serialize(c);
                        serializedContacts.add(s);
                }
            }
            if(!serializedContacts.isEmpty()){
                updatePartnerContacts(serializedContacts);
            }
        }
    }

    private static void updatePartnerContacts(List<String> serializedContacts){
        if(system.isFuture() || System.isBatch()){
                updatePartnerContactsOnline(serializedContacts);
            } else {
                updatePartnerContactsOffline(serializedContacts);
            }
    }

    @future
    private static void updatePartnerContactsOffline(List<String> serializedContacts){
        updatePartnerContactsOnline(serializedContacts);
    }
    private static void updatePartnerContactsOnline(List<String> serializedContacts){
        List<Contact> contacts = new List<Contact>();
        for(String s : serializedContacts){
            Contact c = (Contact)JSON.deserialize(s, Contact.class);
            contacts.add(c);
        }
        Database.update(contacts, false);
    }

    /**
     * @description     Method to create or update Contacts from Users under the Vodafone NL Account for case management
     */
    private static void contactsForCases(List<String> serializedContacts){
        if(system.isFuture() || System.isBatch()){
                contactsForCasesOnline(serializedContacts);
            } else {
                contactsForCasesOffline(serializedContacts);
            }
    }

    @future
    private static void contactsForCasesOffline(List<String> serializedContacts){
        contactsForCasesOnline(serializedContacts);
    }
    private static void contactsForCasesOnline(List<String> serializedContacts){
        List<Contact> contacts = new List<Contact>();
        for(String s : serializedContacts){
            Contact c = (Contact)JSON.deserialize(s, Contact.class);
            contacts.add(c);
        }
        /**This will fail if a contact is created and then updated for test purposes
            because the method is future*/

            Database.upsert(contacts, false);

    }
}