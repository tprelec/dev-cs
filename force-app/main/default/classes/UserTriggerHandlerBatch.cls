/**
 * @description			This is the class that contains logic for de-activating CPQ user license
 * @author				Marcel Vreuls
 */

global class UserTriggerHandlerBatch implements Queueable {
	
	 
    //Added constructor to pass the userlist to de-activate
    private Set<Id> mUsers;
	public UserTriggerHandlerBatch(Set<Id> userIds) {
		system.debug('vreuls constructor gezet');
        mUsers = userIds;
    }

   	global void execute(QueueableContext context) {

   		if(!mUsers.isEmpty()){
   			List<BigMachines__Oracle_User__c> usersToUpdate = new List<BigMachines__Oracle_User__c>();	
	        // fetch existing bmusers
	        Map<Id,BigMachines__Oracle_User__c> usersWithBmUser = new Map<Id,BigMachines__Oracle_User__c>();
	        for (BigMachines__Oracle_User__c bmUser : [Select BigMachines__Salesforce_User__c  from BigMachines__Oracle_User__c where BigMachines__Salesforce_User__c in :mUsers]){
	            usersWithBmUser.put(bmUser.BigMachines__Salesforce_User__c,bmUser);
	        }
			
		
			for(Id uId : mUsers){					
				system.debug('vreuls ' + uId);
				//existing BM users should be deactivated
	            if(usersWithBmUser.containsKey(uId)) {
	                BigMachines__Oracle_User__c bmUsr = usersWithBmUser.get(uId);
	                bmUsr.BigMachines__Bulk_Synchronization__c = false;
	                bmUsr.BigMachines__Provisioned__c = false;
	                system.debug('vreuls updatNewUsersObject not empty' + uId);
	                usersToUpdate.add(bmUsr);
	            }
			}
			if(usersToUpdate.size() > 0){
	            update usersToUpdate;
	    	}			
		}

	}
	
}