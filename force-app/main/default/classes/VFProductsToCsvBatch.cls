/**
 * @description			This is the class to create the CSV file based on products so it can be inserted into BigMachines easily
 *						Product Names need to be sort alphabetically in the output
 *						There are duplicate product names in the data, but they have unique product codes
 * @author				Stjepan Pavuna & Gerhard Newman
 */
global class VFProductsToCsvBatch implements Database.Batchable <sObject>, Database.Stateful {
	
	global String stringDevices = '';
	global String stringBrands = '';
	global String stringCodes = '';
	global Integer counter = 0;
	global static boolean isTest = False; 
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		
		//Get required fields
//		String query = 'SELECT Id, Brand__c, Description__c, ProductCode__c FROM VF_Product__c WHERE Active__c = true and Family__c = \'Mobile Hardware\'';
		String query = 'SELECT Id, Brand__c, name, ProductCode__c FROM VF_Product__c WHERE Active__c = true and Family__c = \'Mobile Hardware\' order by name';
		
        if(isTest)
            query += ' Limit 200';
		
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<VF_Product__c> scope){
		
    	List<String> brands = new List<String>();
    	//List<String> productNames = new List<String>();
    	//Map<String, String> productNameWithCode = new Map<String, String>();

    	for (VF_Product__c product : scope) {
    		
    		//productNames.add(product.Description__c);
    		//productNameWithCode.put(product.Description__c, product.ProductCode__c);

    		if(product.Brand__c != null) brands.add(String.valueOf(product.Brand__c));

    		stringDevices += product.Name +'~';
			stringCodes += product.ProductCode__c +'~';

    		counter++;
		}

/*
		productNames.sort();

		for(String productName : productNames){

			stringDevices += productName +'~';
	   		stringCodes += productNameWithCode.get(productName) +'~';
	
		}
*/

		if(stringDevices.length()>0) stringDevices = stringDevices.substring(0, stringDevices.length() -1);
		if(stringCodes.length()>0) stringCodes = stringCodes.substring(0, stringCodes.length() -1);

		brands.sort();

		Set<String> brandSet = new Set<String>();
		brandSet.addAll(brands);
		
		for(String str : brandSet){
			stringBrands += str +'~';
		}

		if(stringBrands.length()>0) stringBrands = stringBrands.substring(0, stringBrands.length() -1);

    }
    
    global void finish(Database.BatchableContext BC){
    	
    	String finalString = generateFinalString(stringDevices, stringBrands, stringCodes);
    	System.Debug(finalString);
    	String csvName= 'config_attr.csv';
    	String emailSubject ='BigMachines products CSV';
    	String plainTextBody ='BigMachines products CSV - ' + counter + ' products processed.';
        
    	//Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							TotalJobItems
						  FROM
						  	AsyncApexJob
						  WHERE
						  	Id =: BC.getJobId()];


		Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
		blob csvBlob = Blob.valueOf(finalString);
		
		csvAttc.setFileName(csvname);
		csvAttc.setBody(csvBlob);
		
		Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
		
		String[] toAddresses = new List<String> {'EBUSalesForce.nl@vodafone.com'};
		
		
		email.setSubject(emailSubject);
		email.setToAddresses(toAddresses);
		email.setPlainTextBody(plainTextBody);
		email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
		
		Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        
	}

	private static String generateFinalString(String stringDevices, String stringBrands, String stringCodes){

		String retVal = '';

		//populate header, rows, etc, based on the csv file

		retVal += 'Configuration Attributes,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n';
		retVal += ',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n';
		retVal += 'update,ref_var_name,ref_type,ref_category,config_category,name: en,name: nl_NL,description: en,description: nl_NL,variable_name,menu_type,data_type,display_type,set_type,required,hidden,default_value,validation_type,item_level_process,auto_lock,cons_values_display,status,hide_in_trans,additional,start_date,end_date,order_number,is_array_attr,is_array_control_attr,pricing_type,pricing_display_type,is_display_pricing_at_bottom,include_in_total_price,guid,set_variable_name,menu_display_text: en,menu_display_text: nl_NL,menu_item_price: EUR,menu_item_price: USD,menu_variable_name,menu_item_images\n';
		retVal += 'modify,allEquipment:vodafoneNL:mBB,Product Line,1,Configurable Attributes,Device,,,,mBBhardwareIndicator,TRUE,Text,Single Select Menu,None,FALSE,FALSE,,None,FALSE,FALSE,Removed,Active,FALSE,image_menu_layout:false,,,18,TRUE,FALSE,None,None,FALSE,FALSE,testvodafonenl1_5013863,mBBHardware,' + stringDevices + ',,,,' + stringCodes + ',\n';
		retVal += 'modify,allEquipment:vodafoneNL:voice,Product Line,1,Configurable Attributes,Device,,,,vhardwareIndicator,TRUE,Text,Single Select Menu,None,FALSE,FALSE,,None,FALSE,FALSE,Removed,Active,FALSE,image_menu_layout:false,,,207,TRUE,FALSE,None,None,FALSE,FALSE,testvodafonenl1_5148580,devicesArray,' + stringDevices + ',,,,' + stringCodes + ',\n';
		retVal += 'modify,allEquipment:vodafoneNL:voice,Product Line,1,Configurable Attributes,HHBD Device,,,,hHHBDhardwareIndicator,TRUE,Text,Single Select Menu,None,FALSE,FALSE,,None,FALSE,FALSE,Removed,Active,FALSE,image_menu_layout:false,,,277,TRUE,FALSE,None,None,FALSE,FALSE,testvodafonenl1_5152164,hHBD_Hardware,' + stringDevices + ',,,,' + stringCodes + ',\n';
		retVal += 'modify,allEquipment:vodafoneNL:mBB,Product Line,1,Configurable Attributes,Brand,,,,mBBhardwareBrandFilter,TRUE,Text,Single Select Menu,None,FALSE,FALSE,,None,TRUE,FALSE,Marked,Active,FALSE,image_menu_layout:false,,,15,TRUE,FALSE,None,None,FALSE,FALSE,testvodafonenl1_5013857,mBBHardware,'+ stringBrands + ',,,,' + stringBrands + ',\n';
		retVal += 'modify,allEquipment:vodafoneNL:voice,Product Line,1,Configurable Attributes,Brand,,,,vhardwareBrandFilter,TRUE,Text,Single Select Menu,None,FALSE,FALSE,,None,TRUE,FALSE,Marked,Active,FALSE,image_menu_layout:false,,,206,TRUE,FALSE,None,None,FALSE,FALSE,testvodafonenl1_5148547,devicesArray,'+ stringBrands + ',,,,' + stringBrands + ',\n';
		retVal += 'modify,allEquipment:vodafoneNL:voice,Product Line,1,Configurable Attributes,HHBD Brand,,,,hHHBDhardwareBrandFilter,TRUE,Text,Single Select Menu,None,FALSE,FALSE,,None,TRUE,FALSE,Marked,Active,FALSE,image_menu_layout:false,,,276,TRUE,FALSE,None,None,FALSE,FALSE,testvodafonenl1_5152121,hHBD_Hardware,'+ stringBrands + ',,,,' + stringBrands + ',\n';


		return retVal;
	}
}