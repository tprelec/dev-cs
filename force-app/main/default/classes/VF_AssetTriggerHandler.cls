/**
 * @description         This is the trigger handler for the VF_Asset sObject.
 * @author              Guy Clairbois
 * @fix                 added
 */
public with sharing class VF_AssetTriggerHandler extends TriggerHandler {
	/**
	 * @description         This handles the before insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void beforeInsert() {
		List<VF_Asset__c> newAssets = (List<VF_Asset__c>) this.newList;

		setOwnerByDealerCode(newAssets, null);
		processContract(newAssets, null);
		updateOwnership(newAssets, null);
	}

	/**
	 * @description         This handles the after insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void afterInsert() {
		List<VF_Asset__c> newAssets = (List<VF_Asset__c>) this.newList;

		updateAccountData(newAssets);
		updateSharingRules(newAssets, null);
		countAssets(newAssets, null);
	}

	/**
	 * @description         This handles the after insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void beforeUpdate() {
		List<VF_Asset__c> newAssets = (List<VF_Asset__c>) this.newList;
		List<VF_Asset__c> oldAssets = (List<VF_Asset__c>) this.oldList;
		Map<Id, VF_Asset__c> newAssetMap = (Map<Id, VF_Asset__c>) this.newMap;
		Map<Id, VF_Asset__c> oldAssetMap = (Map<Id, VF_Asset__c>) this.oldMap;

		setOwnerByDealerCode(newAssets, oldAssetMap);
		processContract(newAssets, oldAssetMap);
		updateOwnership(newAssets, oldAssetMap);
	}

	/**
	 * @description         This handles the after update trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void afterUpdate() {
		List<VF_Asset__c> newAssets = (List<VF_Asset__c>) this.newList;
		List<VF_Asset__c> oldAssets = (List<VF_Asset__c>) this.oldList;
		Map<Id, VF_Asset__c> oldAssetMap = (Map<Id, VF_Asset__c>) this.oldMap;

		updateSharingRules(newAssets, oldAssetMap);
		countAssets(newAssets, oldAssets);
		removeContractClaims(oldAssets);
	}

	/**
	 * @description         This handles the after delete trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void afterDelete() {
		List<VF_Asset__c> oldAssets = (List<VF_Asset__c>) this.oldList;
		Map<Id, VF_Asset__c> oldAssetMap = (Map<Id, VF_Asset__c>) this.oldMap;

		updateSharingRules(null, oldAssetMap);
	}

	// contract creation or link to VF Asset and its tools, before update and before insert

	private void processContract(List<VF_Asset__c> newAssets, Map<Id, VF_Asset__c> oldAssets) {
		Map<String, VF_Contract__c> newContractsMap = new Map<String, VF_Contract__c>();
		Map<Id, VF_Contract__c> updateContractsMap = new Map<Id, VF_Contract__c>();

		Map<String, VF_Contract__c> matchesFoundMap = processMatchesMap(newAssets);
		Map<String, Dealer_Information__c> dealersFoundMap = processDealersMap(newAssets);

		//if contract not found, create a new one using Retention DC first, then Initial DC
		//if contract found, just link Asset to the contract
		for (VF_Asset__c tempAsset : newAssets) {
			Boolean doProcessContract = GeneralUtils.isRecordFieldChanged(tempAsset, oldAssets, 'BAN__c')
				|| GeneralUtils.isRecordFieldChanged(tempAsset, oldAssets, 'Account__c')
				|| GeneralUtils.isRecordFieldChanged(tempAsset, oldAssets, 'Contract_Number__c')
				|| GeneralUtils.isRecordFieldChanged(tempAsset, oldAssets, 'CTN_Status__c');
			if (tempAsset.Contract_Number__c == null || !doProcessContract) {
				continue;
			}

			if (tempAsset.CTN_Status__c == Constants.VF_ASSET_CTN_STATUS_CANCELLED) {
				tempAsset.Contract_VF__c = null;
				continue;
			}

			if (
				matchesFoundMap.containsKey(tempAsset.Contract_Number__c) ||
				newContractsMap.containsKey(tempAsset.Contract_Number__c)
			) {
				VF_Contract__c contractFound = matchesFoundMap.containsKey(
						tempAsset.Contract_Number__c
					)
					? matchesFoundMap.get(tempAsset.Contract_Number__c)
					: newContractsMap.get(tempAsset.Contract_Number__c);
				if (tempAsset.Contract_Number__c != contractFound.Contract_Number__c) {
					tempAsset.Error__c = 'Asset Contract_Number does not match with existing Contract_VF.Contract_Number';
					continue;
				} else {
					tempAsset.Error__c = null;
				}
				if (contractFound.Id != null) {
					tempAsset.Contract_VF__c = contractFound.Id;
					
					if (String.isNotBlank(tempAsset.CTN__c)) {
						contractFound.Type_of_Service__c = Constants.VF_CONTRACT_MOBILE_SERVICE;
					}
					
					if (!updateContractsMap.containsKey(contractFound.Id)) {
						updateContractsMap.put(contractFound.Id, contractFound);
					}
				}
				continue;
			}

			if (
				tempAsset.CTN_Status__c == Constants.VF_ASSET_CTN_STATUS_ACTIVE ||
				tempAsset.CTN_Status__c == Constants.VF_ASSET_CTN_STATUS_SUSPENDED
			) {
				VF_Contract__c newContract = createNewContract(tempAsset);
				newContract = setDealerCode(newContract, tempAsset, dealersFoundMap);
				newContractsMap.put(tempAsset.Contract_Number__c, newContract);
			} else if (tempAsset.CTN_Status__c == Constants.VF_ASSET_CTN_STATUS_CANCELLED) {
				tempAsset.Contract_VF__c = null;
			}
		}

		upsertContracts(newContractsMap, updateContractsMap);
	}

	private VF_Contract__c createNewContract(VF_Asset__c tempAsset) {
		Id activeRecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName(
			SObjectType.VF_Contract__c.getName(),
			'Contract_Active'
		);
		Id unlockedRecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName(
			SObjectType.VF_Contract__c.getName(),
			'Contract_Retainable_Unlocked'
		);

		VF_Contract__c newContract = new VF_Contract__c();
		newContract.Contract_Number__c = tempAsset.Contract_Number__c;
		newContract.Type_of_Service__c = String.isNotBlank(tempAsset.CTN__c)
			? Constants.VF_CONTRACT_MOBILE_SERVICE
			: null;

		newContract.Account__c = tempAsset.Account__c;
		if (tempAsset.Contract_End_Date__c > System.today()) {
			newContract.RecordTypeId = activeRecordTypeId;
			newContract.Contract_Status__c = Constants.VF_CONTRACT_STATUS_ACTIVE;
		} else {
			newContract.RecordTypeId = unlockedRecordTypeId;
			newContract.Contract_Status__c = Constants.VF_CONTRACT_STATUS_UNLOCKED;
		}
		newContract.Contract_Activation_Date_Actual_del__c = tempAsset.Contract_Start_Date__c;
		if (tempAsset.Contract_Start_Date__c != null && tempAsset.Contract_End_Date__c != null) {
			newContract.Contract_Duration__c = tempAsset.Contract_Start_Date__c.monthsBetween(
				tempAsset.Contract_End_Date__c
			);
		}

		return newContract;
	}

	private VF_Contract__c setDealerCode(
		VF_Contract__c newContract,
		VF_Asset__c tempAsset,
		Map<String, Dealer_Information__c> dealersFoundMap
	) {
		String dealerCode = '';
		String retDealCode = String.isNotBlank(tempAsset.Retention_Dealer_code__c)
			? tempAsset.Retention_Dealer_code__c.right(6)
			: '';
		String iniDealCode = String.isNotBlank(tempAsset.Initial_dealer_code__c)
			? tempAsset.Initial_dealer_code__c.right(6)
			: '';

		if (dealersFoundMap.containsKey(retDealCode)) {
			dealerCode = retDealCode;
		} else if (dealersFoundMap.containsKey(iniDealCode)) {
			dealerCode = iniDealCode;
		}

		if (String.isNotBlank(dealerCode)) {
			newContract.Dealer_code_responsible__c = dealersFoundMap.get(dealerCode).Dealer_Code__c;
			newContract.Dealer_Information__c = dealersFoundMap.get(dealerCode).Id;
		}
		return newContract;
	}

	public void upsertContracts(
		Map<String, VF_Contract__c> newContractsMap,
		Map<Id, VF_Contract__c> updateContractsMap
	) {
		List<VF_Asset__c> newAssets = (List<VF_Asset__c>) this.newList;
		Map<String, List<VF_Asset__c>> contractToAsset = GeneralUtils.groupByStringField(
			newAssets,
			'Contract_Number__c'
		);
		if (!newContractsMap.values().isEmpty()) {
			List<Database.UpsertResult> irList = Database.upsert(
				newContractsMap.values(),
				VF_Contract__c.Fields.Contract_Number__c,
				false
			);

			for (Integer i = 0; i < newContractsMap.values().size(); i++) {
				if (!irList[i].isSuccess()) {
					for (
						VF_Asset__c a : contractToAsset.get(
							newContractsMap.values()[i].Contract_Number__c
						)
					) {
						a.Error__c = String.valueOf(irList[i].getErrors()[0].getMessage()).left(255);
					}
				} else {
					for (
						VF_Asset__c a : contractToAsset.get(
							newContractsMap.values()[i].Contract_Number__c
						)
					) {
						a.Contract_VF__c = irList[i].getId();
						a.Error__c = a?.Error__c;
					}
				}
			}
		}

		if (!updateContractsMap.isEmpty()) {
			List<Database.SaveResult> urList = Database.update(updateContractsMap.values(), false);
			for (Integer i = 0; i < updateContractsMap.values().size(); i++) {
				if (!urList[i].isSuccess()) {
					for (
						VF_Asset__c a : contractToAsset.get(
							updateContractsMap.values()[i].Contract_Number__c
						)
					) {
						a.Error__c = String.valueOf(urList[i].getErrors()[0].getMessage()).left(255);
					}
				}
			}
		}
	}

	//searches on contract VF between the fields Opportunity__r.Primary_Basket__r.Name,
	// Opportunity__r.Primary_Quote__r.Name and Contract_Number__c and returns a map of results with the match as key
	private Map<String, VF_Contract__c> processMatchesMap(List<VF_Asset__c> newAssets) {
		Set<String> uniqueContractNumber = new Set<String>();
		for (VF_Asset__c tempAsset : newAssets) {
			if (tempAsset.Contract_Number__c != null) {
				uniqueContractNumber.add(tempAsset.Contract_Number__c);
			}
		}

		Map<String, VF_Contract__c> matchesFoundMap = new Map<String, VF_Contract__c>();
		//3 ways to look for the contract: Quote ID, Basket ID, Contract_Number__c
		//Quote ID and Basket ID, the same process just from different objects (will try to use lookups)
		//Contract_Number__c, new field on contract VF
		if (uniqueContractNumber.isEmpty()) {
			return matchesFoundMap;
		}

		//get VF Contracts using the 3 methods
		List<VF_Contract__c> matchesFound = [
			SELECT
				Id,
				Name,
				Contract_Status__c,
				Opportunity__r.Primary_Basket__c,
				Opportunity__r.Primary_Basket__r.Name,
				Opportunity__r.Primary_Quote__c,
				Opportunity__r.Primary_Quote__r.Name,
				Contract_Number__c,
				Dealer_Information__c,
				Type_of_Service__c
			FROM VF_Contract__c
			WHERE
				Opportunity__r.Primary_Basket__r.Name IN :uniqueContractNumber
				OR Opportunity__r.Primary_Quote__r.Name IN :uniqueContractNumber
				OR Contract_Number__c IN :uniqueContractNumber
				OR Name IN :uniqueContractNumber
		];

		//map contract matches
		for (VF_Contract__c tempMatches : matchesFound) {
			matchesFoundMap.put(tempMatches.Name, tempMatches);
			if (tempMatches.Opportunity__r.Primary_Basket__c != null) {
				matchesFoundMap.put(tempMatches.Opportunity__r.Primary_Basket__r.Name, tempMatches);
			} else if (tempMatches.Opportunity__r.Primary_Quote__c != null) {
				matchesFoundMap.put(tempMatches.Opportunity__r.Primary_Quote__r.Name, tempMatches);
			} else if (tempMatches.Contract_Number__c != null) {
				matchesFoundMap.put(tempMatches.Contract_Number__c, tempMatches);
			}
		}

		return matchesFoundMap;
	}

	// feed some dealer codes, and it will reply back with a map of dealer code ->
	// dealer information record of the highest authority between this and its parent
	private Map<String, Dealer_Information__c> processDealersMap(List<VF_Asset__c> newAssets) {
		Set<String> uniqueDealerCodes = new Set<String>();
		for (VF_Asset__c tempAsset : newAssets) {
			if (tempAsset.Retention_Dealer_code__c != null) {
				uniqueDealerCodes.add(tempAsset.Retention_Dealer_code__c.right(6));
			}
			if (tempAsset.Initial_dealer_code__c != null) {
				uniqueDealerCodes.add(tempAsset.Initial_dealer_code__c.right(6));
			}
		}

		Map<String, Dealer_Information__c> dealersFoundMap = new Map<String, Dealer_Information__c>();
		Set<String> uniqueDealerParentsCodes = new Set<String>();
		Map<String, Dealer_Information__c> dealersParentsFoundMap = new Map<String, Dealer_Information__c>();

		if (uniqueDealerCodes.isEmpty()) {
			return dealersFoundMap;
		}

		//get Dealer Information using the unique codes
		List<Dealer_Information__c> dealersFound = [
			SELECT
				Id,
				Name,
				Dealer_Code__c,
				Parent_Dealer_Code__c,
				HasParent__c,
				ContactUserId__c,
				Contact__c,
				Status__c
			FROM Dealer_Information__c
			WHERE Dealer_Code__c IN :uniqueDealerCodes
		];
		//map the dealers
		for (Dealer_Information__c tempDealers : dealersFound) {
			if (!tempDealers.HasParent__c) {
				dealersFoundMap.put(tempDealers.Dealer_Code__c, tempDealers);
			} else {
				uniqueDealerParentsCodes.add(tempDealers.Parent_Dealer_Code__c);
			}
		}

		if (uniqueDealerParentsCodes.isEmpty()) {
			return dealersFoundMap;
		}

		//now we can get the dealer's parents
		List<Dealer_Information__c> dealersParentsFound = [
			SELECT Id, Name, Dealer_Code__c, Parent_Dealer_Code__c, HasParent__c, ContactUserId__c
			FROM Dealer_Information__c
			WHERE Dealer_Code__c IN :uniqueDealerParentsCodes
		];

		//map the parents
		for (Dealer_Information__c tempDealers : dealersParentsFound) {
			dealersParentsFoundMap.put(tempDealers.Dealer_Code__c, tempDealers);
		}
		//add parents to the main list
		for (Dealer_Information__c tempDealers : dealersFound) {
			if (tempDealers.HasParent__c) {
				dealersFoundMap.put(
					tempDealers.Dealer_Code__c,
					dealersParentsFoundMap.get(tempDealers.Parent_Dealer_Code__c)
				);
			}
		}

		System.debug(
			'--------------------------- dealersFoundMap.size() = ' + dealersFoundMap.size()
		);
		return dealersFoundMap;
	}
	/**
	 * @author      : Jefferson Absalon
	 * @description : After the Assets have been moved we verify pending claimed contracts approval request
	 * are not left just with contracts without assets, if that the case the claim is removed and the owner
	 * is notified by an email
	 */
	private void removeContractClaims(List<VF_Asset__c> oldAssets) {
		Set<Id> oldContractsIDs = new Set<Id>();
		Set<Id> claimedContractsToCheck = new Set<Id>();
		Set<Id> claimedContractsToReject = new Set<Id>();
		List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
		List<Claimed_Contract_Approval_Request__c> claimedContractsToUpdate = new List<Claimed_Contract_Approval_Request__c>();
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
		List<Approval.ProcessWorkitemRequest> listToReject = new List<Approval.ProcessWorkitemRequest>();

		for (VF_Asset__c tempOldAsset : oldAssets) {
			if (tempOldAsset.Contract_VF__c != null) {
				oldContractsIDs.add(tempOldAsset.Contract_VF__c);
			}
		}
		if (!oldContractsIDs.isEmpty()) {
			System.debug(
				'--------------------------- oldContractsIDs.size() = ' + oldContractsIDs.size()
			);
			for (VF_Contract__c tempContract : [
				SELECT
					Id,
					Name,
					Claimed_Contract_Approval_Request__c,
					(SELECT Id, Name FROM Assets__r)
				FROM VF_Contract__c
				WHERE Id IN :oldContractsIDs AND Claimed_Contract_Approval_Request__c != NULL
			]) {
				Boolean hasAssets = false;
				for (VF_Asset__c a : tempContract.Assets__r) {
					hasAssets = true;
					break;
				}
				if (!hasAssets) {
					claimedContractsToCheck.add(tempContract.Claimed_Contract_Approval_Request__c);
					VF_Contract__c updateContract = new VF_Contract__c(
						Id = tempContract.Id,
						Claimed_Contract_Approval_Request__c = null
					);
					contractsToUpdate.add(updateContract);
				}
			}

			if (contractsToUpdate.isEmpty()) {
				return;
			}

			update contractsToUpdate;

			for (Claimed_Contract_Approval_Request__c tempContractApproval : [
				SELECT
					Id,
					Name,
					(
						SELECT
							Id,
							Name,
							Claimed_Contract_Approval_Request__c,
							Account__c,
							Account__r.Name
						FROM Contracts_VF__r
					)
				FROM Claimed_Contract_Approval_Request__c
				WHERE Id IN :claimedContractsToCheck
			]) {
				Boolean hasContracts = false;
				for (VF_Contract__c c : tempContractApproval.Contracts_VF__r) {
					hasContracts = true;
					break;
				}
				if (!hasContracts) {
					claimedContractsToReject.add(tempContractApproval.Id);
				}
			}
			if (!claimedContractsToReject.isEmpty()) {
				System.debug(
					'--------------------------- claimedContractsToReject.size() = ' +
					claimedContractsToReject.size()
				);
				for (ProcessInstance tempProcessInstance : [
					SELECT Id, (SELECT Id, ActorId, ProcessInstanceId FROM Workitems)
					FROM ProcessInstance
					WHERE Status = 'Pending' AND TargetObjectId IN :claimedContractsToReject
				]) {
					for (ProcessInstanceWorkitem tempPIW : tempProcessInstance.Workitems) {
						Approval.ProcessWorkitemRequest rejectThis = new Approval.ProcessWorkitemRequest();
						rejectThis.setComments(
							'Auto Reject, contracts to be claimed have no Assets left.'
						);
						rejectThis.setAction('Reject'); //to approve use 'Approve'
						rejectThis.setWorkitemId(tempPIW.Id);
						listToReject.add(rejectThis);
					}
				}
				if (!listToReject.isEmpty()) {
					System.debug(
						'--------------------------- listToReject.size() = ' + listToReject.size()
					);
					Approval.ProcessResult[] resultsFromRejections = Approval.process(listToReject);
				}
			}
		}
	}

	// set contract owner as asset owner if asset is related to contract
	private void updateOwnership(List<VF_Asset__c> newAssets, Map<Id, VF_Asset__c> oldAssetsMap) {
		Set<Id> contractIds = new Set<Id>();
		for (VF_Asset__c a : newAssets) {
			if (
				a.Contract_VF__c != null &&
				(oldAssetsMap == null ||
				(oldAssetsMap != null &&
				a.Contract_VF__c != oldAssetsMap.get(a.Id).Contract_VF__c))
			) {
				contractIds.add(a.Contract_VF__c);
			}
		}

		if (!contractIds.isEmpty()) {
			Map<Id, VF_Contract__c> contractsMap = new Map<Id, VF_Contract__c>(
				[SELECT Id, OwnerId FROM VF_Contract__c WHERE Id IN :contractIds]
			);
			for (VF_Asset__c a : newAssets) {
				if (a.Contract_VF__c != null && contractsMap.containsKey(a.Contract_VF__c)) {
					a.OwnerId = contractsMap.get(a.Contract_VF__c).OwnerId;
				}
			}
		}
	}

	/**
	 * @author      : Marcel Vreuls
	 * @description : W-000683 Account Rentainablity
	 */
	private void countAssets(List<VF_Asset__c> newAssets, List<VF_Asset__c> oldAssets) {
		List<AccountToUpdate__c> accountToUpdateList = new List<AccountToUpdate__c>();
		AccountToUpdate__c atu;

		for (VF_Asset__c a : newAssets) {
			atu = new AccountToUpdate__c();
			atu.AccountId__c = a.Account__c;
			accountToUpdateList.add(atu);
		}

		if (oldAssets != null) {
			for (VF_Asset__c a : oldAssets) {
				atu = new AccountToUpdate__c();
				atu.AccountId__c = a.Account__c;
				accountToUpdateList.add(atu);
			}
		}

		Database.insert(accountToUpdateList, false);
	}

	/*
	 * Description:     If this is the first asset for an account, possibly change the account type to 'customer'
	 */
	private void updateAccountData(List<VF_Asset__c> newAssets) {
		// collect accountids
		Set<Id> accountIds = new Set<Id>();
		for (VF_Asset__c vfa : newAssets) {
			accountIds.add(vfa.Account__c);
		}

		if (!accountIds.isEmpty()) {
			List<Account> accountsToUpdate = new List<Account>();
			for (Account a : [
				SELECT Id, Type
				FROM Account
				WHERE Id IN :accountIds AND Type = 'Prospect' AND IsPartner = FALSE
			]) {
				// update account type where needed
				a.Type = 'Customer';
				accountsToUpdate.add(a);
			}
			update accountsToUpdate;
		}
	}

	/**
	 * @description         This method updates the owner of the Asset based on the Dealercode
	 * @author              Guy Clairbois
	 */
	private void setOwnerByDealerCode(
		List<VF_Asset__c> newAssets,
		Map<Id, VF_Asset__c> oldAssetsMap
	) {
		// then update owner on asset
		Map<String, Dealer_Information__c> ownerMap = new Map<String, Dealer_Information__c>();
		for (VF_Asset__c vfa : newAssets) {
			if (vfa.Dealer_Code__c != null && vfa.Contract_VF__c == null) {
				ownerMap.put(vfa.Dealer_Code__c.removeStart('00'), null);
			}
		}

		if (ownerMap.isEmpty()) {
			return;
		}

		//Get active owner Id's
		for (Dealer_Information__c di : [
			SELECT Id, Dealer_code__c, Contact__r.UserId__c, Name
			FROM Dealer_Information__c
			WHERE Dealer_code__c IN :ownerMap.keySet() AND Contact__r.IsActive__c = TRUE
		]) {
			if (di.Contact__r.UserId__c != null) {
				ownerMap.put(di.Dealer_code__c, di);
			}
		}

		for (VF_Asset__c vfa : newAssets) {
			if (vfa.Contract_VF__c != null) {
				continue;
			}
			if (vfa.Dealer_Code__c != null) {
				String dc = vfa.Dealer_Code__c.removeStart('00');
				if (ownerMap.get(dc) != null) {
					vfa.OwnerId = ownerMap.get(dc).Contact__r.UserId__c;
				} else {
					vfa.OwnerId = GeneralUtils.vodafoneUser.Id;
				}
			} else {
				vfa.OwnerId = GeneralUtils.vodafoneUser.Id;
			}
		}
	}

	/**
	 * @description         This method updates the sharing rules on both Asset and Account if applicable
	 * @author              Guy Clairbois
	 */
	private void updateSharingRules(
		List<VF_Asset__c> newAssets,
		Map<Id, VF_Asset__c> oldAssetsMap
	) {
		List<VF_Asset__share> assetSharingRulesToInsert = new List<VF_Asset__share>();
		List<AccountShare> accountSharingRulesToInsert = new List<AccountShare>();

		Set<Id> assetIds = new Set<Id>();
		Map<Id, Id> accountToAssetOwner = new Map<Id, Id>();
		Map<Id, Id> banToAssetOwner = new Map<Id, Id>();
		Map<Id, Id> accountToDeletedAssetOwner = new Map<Id, Id>();

		if (oldAssetsMap == null) {
			// insert. Always add
			for (VF_Asset__c vfa : newAssets) {
				assetIds.add(vfa.Id);
				// also possibly create view rights on account if owner is different and rights do not exist yet
				accountToAssetOwner.put(vfa.Account__c, vfa.OwnerId);
				banToAssetOwner.put(vfa.Ban__c, vfa.OwnerId);
			}
		} else if (newAssets == null) {
			// delete. do nothing (cleanup will be handled by household jobs)
		} else {
			//update. check if account changed
			for (VF_Asset__c vfa : newAssets) {
				if (
					vfa.Account__c != oldAssetsMap.get(vfa.Id).Account__c ||
					vfa.Ban__c != oldAssetsMap.get(vfa.Id).Ban__c
				) {
					assetIds.add(vfa.Id);
				}
				// also possibly create view rights on account if owner is different and rights do not exist yet
				if (vfa.OwnerId != oldAssetsMap.get(vfa.Id).OwnerId) {
					accountToAssetOwner.put(vfa.Account__c, vfa.OwnerId);
					banToAssetOwner.put(vfa.Ban__c, vfa.OwnerId);
				}
			}
		}

		if (!assetIds.isEmpty()) {
			// remove all existing (manual) rules
			new WithoutSharingMethods()
				.deleteSharingRules(
					[
						SELECT Id
						FROM VF_Asset__Share
						WHERE ParentId IN :assetIds AND RowCause = 'Manual'
					]
				);

			// fetch all assets to create new asset sharing rule if account or ban owner cannot see it yet
			for (VF_Asset__c vfa : [
				SELECT
					Id,
					OwnerId,
					Account__c,
					Account__r.OwnerId,
					Account__r.Owner.IsActive,
					Ban__c,
					Ban__r.OwnerId,
					Ban__r.Owner.IsActive,
					Contract_VF__c
				FROM VF_Asset__c
				WHERE Id IN :assetIds
			]) {
				if (vfa.Contract_VF__c == null) {
					if (
						vfa.Account__c != null &&
						vfa.OwnerId <> vfa.Account__r.OwnerId &&
						vfa.Account__r.Owner.IsActive
					) {
						VF_Asset__share vfas = new VF_Asset__share();
						vfas.UserOrGroupId = vfa.Account__r.OwnerId;
						vfas.ParentId = vfa.Id;
						vfas.AccessLevel = 'Read';
						assetSharingRulesToInsert.add(vfas);
					}
					if (
						vfa.Ban__c != null &&
						vfa.OwnerId <> vfa.Ban__r.OwnerId &&
						vfa.Ban__r.OwnerId <> vfa.Account__r.OwnerId &&
						vfa.Ban__r.Owner.IsActive
					) {
						VF_Asset__share vfas = new VF_Asset__share();
						vfas.UserOrGroupId = vfa.Ban__r.OwnerId;
						vfas.ParentId = vfa.Id;
						vfas.AccessLevel = 'Read';
						assetSharingRulesToInsert.add(vfas);
					}
				}
			}
		}

		if (!accountToAssetOwner.isEmpty()) {
			// create sharing rules on Asset for parent partners

			// replace ban owners by their role group and parent role groups, if applicable
			Set<Id> partnerOwnerIds = new Set<Id>();
			for (Id ownerId : accountToAssetOwner.values()) {
				partnerOwnerIds.add(ownerId);
			}

			// add dealercode hierarchy sharing
			// fetch all relevant dealercodes by owner accountid

			// create sharing for the other dealercode contacts (only 1 per parent is necessary, the others will
			// get it automatically because they are all partner superusers)
			Map<Id, Id> parentPartnerUserIds = GeneralUtils.getParentPartnerUserIdsFromUserIds(
				partnerOwnerIds
			);
			System.debug(parentPartnerUserIds);
			if (!parentPartnerUserIds.isEmpty()) {
				for (VF_Asset__c vfa : newAssets) {
					if (parentPartnerUserIds.containsKey(vfa.OwnerId)) {
						VF_Asset__share vfas = new VF_Asset__share(
							RowCause = Schema.VF_Asset__share.RowCause.Dealer_Hierarchy__c
						);
						vfas.UserOrGroupId = parentPartnerUserIds.get(vfa.OwnerId);
						vfas.ParentId = vfa.Id;
						vfas.AccessLevel = 'Read';
						assetSharingRulesToInsert.add(vfas);
					}
				}

				// add the parent owners to the owner set, preparing for account sharing rules creation
				partnerOwnerIds.addAll(parentPartnerUserIds.values());
			}

			// add account sharing rule if the asset owner cannot view the account yet
			SharingUtils.createAccountSharing(accountToAssetOwner, partnerOwnerIds, null);
		}

		new WithoutSharingMethods().insertSharingRules(assetSharingRulesToInsert);
	}

	/**
	 * @description         A without sharing util class to do the updates/inserts/deletes without interfering with authorizations
	 * @author              Guy Clairbois
	 */
	public without sharing class WithoutSharingMethods {
		public void insertSharingRules(List<sObject> sharingRulestoInsert) {
			insert sharingRulestoInsert;
		}
		public void deleteSharingRules(List<sObject> sharingRulestoDelete) {
			delete sharingRulestoDelete;
		}
	}
}