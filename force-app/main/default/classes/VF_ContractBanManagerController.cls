public without sharing class VF_ContractBanManagerController {
	
    private final VF_Contract__c vfc;
    public Opportunity opp{get;private set;}
    public BanManagerData theBmData{get;private set;}
    public Boolean banIsEditable{get;private set;}
    
    public VF_ContractBanManagerController(ApexPages.StandardController stdController) {
       
        vfc = [Select Id,Account__c,Contract_Status__c,Implementation_Status__c,Opportunity__c From VF_Contract__c Where Id = :stdController.getId()];

        // fetch the Opp separately to circumvent license limitations
        opp = [Select Id, Ban__c From Opportunity Where Id = :this.vfc.Opportunity__c];

        // fetch account bans and create a bandata instance
        //List<Ban__c> bans = [Select Id, Name, Ban_Number__c, BAN_Status__c, Account__c From Ban__c where Account__c = :this.vfc.Account__c]; //new withSharingMethods().visibleBans(this.vfc.Account__c);
        Boolean fixed = false;
        Boolean onenet = false;
        for(OpportunityLineItem oli : [Select Id, Fixed_Mobile__c, Proposition__c, Opportunity.Hidden_Fixed_Products__c From OpportunityLineItem Where OpportunityId = :this.opp.Id]){
            if(oli.Proposition__c != null && oli.Proposition__c.contains('One Net')){
                onenet = true;
            } else if (oli.Fixed_Mobile__c == 'Fixed' || oli.opportunity.Hidden_Fixed_Products__c > 0){
                // any order under an opportunity that has fixed products should follow the 'fixed' rules. Even if the order is mobile.
                fixed = true;
            }
        } 
        system.debug('check1');       
        system.debug(fixed);
        system.debug(onenet);
        //theBmData = new BanManagerData(bans,this.opp.Ban__c,fixed,onenet,false);
        theBmData = new BanManagerData(new Set<Id>{vfc.Account__c},this.opp.Ban__c,fixed,onenet,false,false);

        // fetch the contract status to determine if the BAN should be editable
        banIsEditable = true;
        for(Order__c o : [Select Id, Status__c From Order__c Where VF_Contract__c = :this.vfc.Id]){
        	if(o.Status__c == 'Submitted' || o.Status__c == 'Accepted'){
        		banIsEditable = false;
        	}
        }    
        if(this.vfc.Implementation_Status__c == 'Completed'){
        	banIsEditable = false;
        }
    }

    public PageReference updateBan(){
    	theBmData.errorText = null;

		Savepoint sp = Database.setSavepoint();

		Ban__c banForContract = new Ban__c();

		try{
			banForContract = theBmData.getBan(this.vfc.Account__c);
			Opportunity oppToUpdate = new Opportunity(Id=vfc.Opportunity__c);
			
			if(banForContract != null){
				oppToUpdate.Ban__c = banForContract.Id;
			} else {
				oppToUpdate.Ban__c = null;
			}			
			update oppToUpdate;
			theBmData.banChanged = false;
			theBmData.infoText = 'Ban updated';
		} catch (Exception e){
			theBmData.errorText = e.getMessage();
			Database.rollback(sp);
		}	
		return null;

    }

    public PageReference cancelUpdateBan(){
    	theBmData.errorText = null;
		theBmData.banChanged = false;
		// requery the current ban to get the correct value
		theBmData.banSelected = [Select Opportunity__r.Ban__r.Name From VF_Contract__c Where Id = :this.vfc.Id].Opportunity__r.Ban__r.Name;
		return null;

    }    

}