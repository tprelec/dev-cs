public with sharing class VF_ContractRenewController {

	private final VF_Contract__c theContract;
	public VF_ContractRenewController(ApexPages.StandardController stdController) {
		stdController.addFields(new List<String>{'Implementation_Status__c'});
		theContract = (VF_Contract__c) stdController.getRecord();
	}

	public pageReference updateContractStatus(){
		if(theContract.Implementation_Status__c != 'Rejected') {
			ApexPages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_Renew_Contract_Status));
			return null;
		} else {
			theContract.Implementation_Status__c = 'Renewed';		
			update theContract;
		}
		return new pageReference('/'+theContract.Id);
	}

	public pageReference backToContract(){
		return new pageReference('/'+theContract.Id);
	}
}