public with sharing class VF_ContractTierEditorController {
	public VF_ContractTierEditorController() {
		
	}

 	public Integer rowNum{get;set;}	
	public ContractTierGroupWrapper theWrapper {get;set;}

	public pageReference saveChanges(){
		List<Contract_Tiers__c> tiersToUpsert = new List<Contract_Tiers__c>();
		// copy tier type to all children
		for(Contract_Tiers__c ct : theWrapper.theTiers){
			ct.Flatfee_type__c = theWrapper.maintier.Flatfee_type__c;
		}
		tiersToUpsert.add(theWrapper.maintier);
		tiersToUpsert.addAll(theWrapper.theTiers);
		upsert tiersToUpsert;
		return null;
	}

	public pageReference addRow(){
		// if the main tier is not yet saved, save it now. Also save any other updates (because a reload/reshuffle will happen)
		saveChanges();

		Contract_Tiers__c ct = new Contract_Tiers__c();
		ct.Flatfee_type__c = theWrapper.mainTier.Flatfee_type__c;
		ct.VF_Contract__c = theWrapper.maintier.VF_Contract__c;
		// if the list of tiers is filled, by default continue with the end of the last tier
		system.debug(theWrapper.theTiers);
		if(!theWrapper.theTiers.isEmpty()){
			// use the previous usage to + 1
			if(theWrapper.theTiers[theWrapper.theTiers.size()-1].Usage_To__c!= null)
				ct.Usage_From__c = String.valueOf(Decimal.ValueOf(theWrapper.theTiers[theWrapper.theTiers.size()-1].Usage_To__c)+1);
			ct.Flatfee_Adjustment__c = theWrapper.theTiers[theWrapper.theTiers.size()-1].Flatfee_Adjustment__c;
			ct.Sequence_Number__c = theWrapper.theTiers[theWrapper.theTiers.size()-1].Sequence_Number__c + 1;
		} else {
			ct.Usage_From__c = '0';
			ct.Flatfee_Adjustment__c = '0';
			ct.Sequence_Number__c = 0;
		}
		insert ct;
		return null;
	}

    public void delRow(){
        rowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('index'));
        if(rowNum != null) delete theWrapper.theTiers[rowNum];
        //theWrapper.remove(rowNum);   
    }    	

    public pageReference deleteAll(){
    	List<Contract_Tiers__c> tiersToDelete = new List<Contract_Tiers__c>();
    	for(Contract_Tiers__c ct : theWrapper.theTiers){
    		if(ct.Id != null) tiersToDelete.add(ct);
    	}
    	if(theWrapper.maintier.Id != null) tiersToDelete.add(theWrapper.maintier);

    	delete tiersToDelete;
    	return null;
    }
}