public with sharing class VF_ContractTiersController {

	public static final Set<String> profilesWithEdit = new Set<String>{'VF Enterprise Legal','System Administrator'};

	public VF_Contract__c theContract {get;set;}

	public Boolean readOnly {
		get {
			if(readOnly == null){
				if(profilesWithEdit.contains(GeneralUtils.ProfileIdToProfileName.get(UserInfo.getProfileId())) ){
					readOnly = false;
				} else {
					readOnly = true;
				}
			}
			return readOnly;
		}
		set;
	}

	public List<ContractTierGroupWrapper> theTierGroups {
		get{
			//if(theTierGroups == null){
				//theTierGroups = retrieveTiers(theContract.Id,null).values();
				return retrieveTiers(theContract.Id,null).values();
			//}
			//return theTierGroups;
		}
		set;
	}



	public VF_ContractTiersController(ApexPages.StandardController controller) {
		 if (!Test.isRunningTest()) { 
			controller.addFields(new List<String>{'Id','Name'});
		}
		theContract = (VF_Contract__c)controller.getRecord();
		
	}

	public Map<String,ContractTierGroupWrapper> retrieveTiers(Id contractId, String type){
		Map<String,ContractTierGroupWrapper> tierTypeToTier = new Map<String,ContractTierGroupWrapper>();

		for(Contract_Tiers__c ct : [Select Id, 
										VF_Contract__c,
										Extra_Info__c,
										Flatfee_type__c, 
										Usage_To__c, 
										Usage_From__c, 
										Flatfee_Adjustment__c,
										Sequence_Number__c,
										Active_Tier__c
									From 
										Contract_Tiers__c 
									Where 
										VF_Contract__c = :theContract.Id
									Order by
										Flatfee_type__c ASC,
										Sequence_Number__c ASC
									] ){
			ContractTierGroupWrapper ctw;
			if(tierTypeToTier.containsKey(ct.Flatfee_type__c)){
				ctw = tierTypeToTier.get(ct.Flatfee_type__c);
			} else {
				// initialize a new wrapper
				ctw = new ContractTierGroupWrapper();
				ctw.theTiers = new List<Contract_Tiers__c>();
			}

			// sequence -9999 indicates initial fee record

			if(ct.Sequence_Number__c == -9999){
				ctw.maintier = ct;
			} else {
				// otherwise add the tier to the list of tiers
				ctw.theTiers.add(ct);
			}

			tierTypeToTier.put(ct.Flatfee_type__c,ctw);

			
		}

		// check if there are detail tiers but no main tier. If there isn't, generate one on the fly (this is an exception scenario only)
		for(ContractTierGroupWrapper ctw : tierTypeToTier.values()){
			if(ctw.maintier == null){
				if(!ctw.theTiers.isEmpty()){
					ctw.maintier = new Contract_Tiers__c();
					ctw.maintier.VF_Contract__c = theContract.Id;
					ctw.maintier.Flatfee_type__c = ctw.theTiers[0].Flatfee_type__c;
					ctw.maintier.Flatfee_Adjustment__c = '0';
					ctw.maintier.Sequence_Number__c = -9999;
				} 
			}
		}

		return tierTypeToTier;
	}

	public pageReference addType(){
		// create a new tier with an undefined type
		Contract_Tiers__c ct = new Contract_Tiers__c();
		ct.VF_Contract__c = theContract.Id;
		ct.Flatfee_Adjustment__c = '0';
		ct.Flatfee_type__c = 'new'+theTierGroups.size();
		ct.Sequence_Number__c = -9999;
		insert ct;
		return null;
	}

	public pageReference saveContract(){
		update theContract;
		return null;
	}

	public pageReference backToContract(){

		return new PageReference('/'+theContract.Id);
	}

}