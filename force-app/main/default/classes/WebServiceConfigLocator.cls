/**
 * @description      This class is responsible for retrieving the configuration for external webservices.
 * @author        Guy Clairbois
 */
public class WebServiceConfigLocator {
    
  /**
   * @description      This inner class holds all of the information for connecting to the external
   *            web service.
   * @author        Guy Clairbois
   */
  private class WebServiceConfig implements IWebServiceConfig {
    
    private String username;
    
    private String password;
    
    private String endpoint;

    private String certificateName;
    
    public String getUsername(){
      return username;
    }
    
    public void setUsername(String username){
      this.username = username;
    }
    
    public String getPassword(){
      return password;
    }
    
    public void setPassword(String password){
      this.password = password;
    }
    
    public String getEndpoint(){
      return endpoint;
    }
    
    public void setEndpoint(String endpoint){
      this.endpoint = endpoint;
    }

    public String getCertificateName(){
      return certificateName;
    }
    
    public void setCertificateName(String certificateName){
      this.certificateName = certificateName;
    }    
    
  }
  
  
  /**
   * @description      This holds all of the stored configuration held in custom settings. The reason
   *            it is a static property is because we don't want to keep retrieving the data unneccessarily.
   */
  private static Map<String, External_WebService_Config__c> webserviceConfig {
    get {
      if(webserviceConfig == null) webserviceConfig = External_WebService_Config__c.getAll();
      return webserviceConfig;
    }
    set;
  }
  
  
  /**
   * @description      This will retrieve from custom settings a data set with a matching name and will 
   *            instantiate a new web service configuration object and will populate it with the stored
   *            configuration.
   * @param  name    The name of the data set to use in custom settings.
   * @return        New instance of a web service configuration class.
   */
  public static IWebServiceConfig getConfig(String name){
    return new WebServiceConfigLocator().getStoredConfig(name);
  }
  
  
  /**
   * @description      This creates a new instance of the webservice config class. This then allows the 
   *            calling method to configure the object with requiring to fetch data from custom settings.
   */
  public static IWebServiceConfig createConfig(){
    return new WebServiceConfig();
  }
  
  
  /**
   * @description      This class should not be instantiated, this constructor is private to prevent
   *            developers from doing this.
   */
  private WebServiceConfigLocator(){
    // Class should not be instantiated
  }
  
  
  /**
   * @description      This is an instance method to allow us to override this functionality in the future
   *            if the need be ;)
   * @param  name    The name of the data set to use in custom settings.
   * @return        New instance of a web service configuration class.    
   */
  public IWebServiceConfig getStoredConfig(String name){
    // Retrieve the stored config
    External_WebService_Config__c storedConfig = webserviceConfig.get(name);
    if(storedConfig == null) throw new ExMissingDataException('Invalid data set name provided');
    
    // Retrieve the prod ip and check if this is a production environment
    // This is done to double check and prevent sandbox messages from going to production
    External_WebService_Config__c prodIP = webserviceConfig.get('ECSProduction_IP'); // TODO: replace this by a map, to enable multiple prod ip's for different systems
    if(prodIP == null) throw new ExMissingDataException('No custom setting for Production_IP provided');
    
    if(storedConfig.URL__c.startsWith(prodIP.URL__c)
        // the 3 first characters identify the target system (e.g. 'ECS')
        && storedConfig.Name.subString(0,3) == prodIP.Name.substring(0,3) 
        && !GeneralUtils.IsProduction() ){
      throw new ExInvalidConfigurationException('Attempt to export to production IP from non-production instance!');
      return null;
    }
    if(!storedConfig.URL__c.startsWith(prodIP.URL__c)
        // the 3 first characters identify the target system (e.g. 'ECS')
        && storedConfig.Name.subString(0,3) == prodIP.Name.substring(0,3) 
        && GeneralUtils.IsProduction() ){
      throw new ExInvalidConfigurationException('Attempt to export to non-production IP from production instance!');
      return null;
    }      
    
    // Create the web service config instance
    IWebServiceConfig config = new WebServiceConfig();

    config.setUsername(storedConfig.username__c);
    config.setPassword(storedConfig.password__c);
    config.setEndpoint(storedConfig.URL__c);
    config.setCertificateName(storedConfig.certificate_name__c);

    return config;

  }
  
  
}