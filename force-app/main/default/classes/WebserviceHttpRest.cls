/**
 * @description			Class to handle HTTP Rest calls to other SFDC instances
 * @author				Marcel Vreuls
 * @History				Apr 2019: initial creation for einstein writeback
 *                      2019-05  	W-001579 Marcel Vreuls: removed the authtenication from custom settings, now this is named credentials
 * @Defaults   			Requires the creation and update of  Custom settings for user credentials
 *                       //  [HTTP-00]   - Reached limit on callouts
 *                       //  [HTTP-01]   - Unable to get OAuth 2.0 token from remote SFDC
 *                       //  [HTTP-02]   - Error in SOQL REST query
 *  
 *
 * WebserviceHttpRest myCall = new WebserviceHttpRest();
 * myCall.doSoqlQuery('SELECT ID,Company, Fiber__C,LG_DateOfEstablishment__c,LG_InternetCustomer__c,LG_LegalForm__c,LG_MobileCustomer__c,LG_Segment__c,LG_VisitPostalCode__c FROM lead WHERE LG_NewSalesChannel__c =  \'D2D\'  AND status != 'Qualified' limit 50'); 
 *  
 *  WebserviceHttpRest myCall = new WebserviceHttpRest();
 *  List<Ziggo_lead__c> allKvkLeads = [select id, ein_outcome_opendoor__c,ein_ziggo_source_id__c from Ziggo_lead__c limit 50];
 *  myCall.UpdateLeads(allKvkLeads);
 */

public with sharing class WebserviceHttpRest {

    private class mZiggo_Lead_Object{
        String Company {set;get;}
        String Name {set;get;}
        String Fiber {set;get;}
        Id Id {set;get;}
        Date LG_DateOfEstablishment {set;get;}
        String LG_InternetCustomer {set;get;}
        String LG_LegalForm {set;get;}
        String LG_MobileCustomer {set;get;}
        String LG_Segment {set;get;}
        String LG_VisitPostalCode {set;get;}
        Integer NumberOfEmployees {set;get;}
    }
    
    public string Response ;
    String  accessToken;                    // OAuth 2.0 access token
    String  sfdcInstanceUrl;                // Endpoint URL for SFDC instance

    private HttpResponse sendNamedCredentials(String uri, string Body)
    {
         if (Limits.getCallouts() == Limits.getLimitCallouts())
            system.debug('[HTTP-00] Callout limit: ' + Limits.getCallouts() + ' reached. No more callouts permitted.');
        
        Http        h       = new Http();
        HttpRequest hRqst   = new HttpRequest();
        
        //system.debug('vreuls-uri' + uri);
        //uri = uri.replace('https://cs107.salesforce.com/null/','');   
        //
         uri = uri.replace('null/','');
        String endpoint = 'callout:ZiggoOrg/' + uri;        
        //system.debug('vreuls-endpoint' + endpoint);
        

        hRqst.setEndpoint(endpoint);
        hRqst.setTimeout(90000);
        hRqst.setHeader('Content-Type', 'application/json');
        hRqst.setMethod('GET');
       
        if (!String.isBlank(Body))
        {
            hRqst.setBody(Body);
        }

        return h.send(hRqst);
    }  
    
    public List<Ziggo_Lead__c> UpdateLeads(List<Ziggo_lead__c> leadList){

        String uri          = this.sfdcInstanceUrl + '/services/data/v45.0/sobjects/Lead/';
        //String uri          = '/services/data/v45.0/sobjects/Lead/';
        for(Ziggo_lead__c lead : leadList){
    		//string Body = '{"ein_outcome_conversion__c":"'+ lead.ein_outcome_conversion__c + '","ein_outcome_opendoor__c":"'+ lead.ein_outcome_opendoor__c + '","ein_explanation_conversion__c":"' + EncodingUtil.urlEncode(lead.ein_explanation_conversion__c,'UTF-8') + '", "ein_explanation_opendoor__c":"' + EncodingUtil.urlEncode(lead.ein_explanation_opendoor__c,'UTF-8') + '", "ein_prescription_conversion__c":"' + EncodingUtil.urlEncode(lead.ein_prescription_conversion__c,'UTF-8') + '", "ein_prescription_opendoor__c":"' + EncodingUtil.urlEncode(lead.ein_prescription_opendoor__c,'UTF-8') + '" }';           
            string Body = '{"ein_outcome_conversion__c":"'+ lead.ein_outcome_conversion__c + '","ein_outcome_opendoor__c":"'+ lead.ein_outcome_opendoor__c + '" '; //
             if(!String.isBlank(lead.ein_explanation_conversion__c)){
                Body = Body + ',"ein_explanation_conversion__c":"' + EncodingUtil.urlEncode(lead.ein_explanation_conversion__c,'UTF-8') + '" '; 
            }
            if(!String.isBlank(lead.ein_explanation_opendoor__c)){
            Body = Body + ',"ein_explanation_opendoor__c":"' + EncodingUtil.urlEncode(lead.ein_explanation_opendoor__c,'UTF-8') + '" '; 
            }
            if(!String.isBlank(lead.ein_prescription_conversion__c)){
                Body = Body + ',"ein_prescription_conversion__c":"' + EncodingUtil.urlEncode(lead.ein_prescription_conversion__c,'UTF-8') + '" }'; 
            } 

            system.debug('Marcel: Body: ' + Body);
            // HttpResponse hRes = this.send(uri + lead.ein_ziggo_source_id__c,'POST',body, true);
            HttpResponse hRes = this.sendNamedCredentials(uri + lead.ein_ziggo_source_id__c +'?_HttpMethod=PATCH', Body); 
            string mRespnse = hRes.getBody();
            //system.debug('response ' + mRespnse);
            system.debug('Marcel: response: ' + mRespnse);
    	}

        return leadList;        
    }

 
    //  -----------------------------------------------------------------------
    //  doSoqlQuery: Executes a REST query on a remote SFDC and returns a list of SObjects
    //  -----------------------------------------------------------------------
    public List<mZiggo_Lead_Object> doSoqlQuery(String query) {
       
        
        List<mZiggo_Lead_Object>    newZiggoLeads   = new List<mZiggo_Lead_Object>(); 
        List<Ziggo_Lead__c>         Ziggoleads      = new List<Ziggo_Lead__c>();      
        PageReference urlPg = new PageReference(this.sfdcInstanceUrl + '/services/data/v45.0/query');
               
        urlPg.getParameters().put('q',query);  
        String uri              = urlPg.getUrl();               // let APEX do the URL encoding of the parms as necessary        
        

        //HttpResponse hRes = this.send(uri,'GET');
        HttpResponse hRes = this.sendNamedCredentials(uri, null);
        string mRespnse = hRes.getBody();
        mRespnse = mRespnse.replace('Fiber__c', 'Fiber');
        mRespnse = mRespnse.replace('LG_DateOfEstablishment__c', 'LG_DateOfEstablishment');
        mRespnse = mRespnse.replace('LG_InternetCustomer__c', 'LG_InternetCustomer');
        mRespnse = mRespnse.replace('LG_LegalForm__c', 'LG_LegalForm');
        mRespnse = mRespnse.replace('LG_MobileCustomer__c', 'LG_MobileCustomer');
        mRespnse = mRespnse.replace('LG_Segment__c', 'LG_Segment');
        mRespnse = mRespnse.replace('LG_VisitPostalCode__c', 'LG_VisitPostalCode');

        system.debug('Marcel: Rest Response: ' + mRespnse);
        response = response + mRespnse;
        //if (hRes.getStatusCode() != 200) 
        // throw new MyException('[HTTP-02] Error in query ' + uri + ' StatusCode=' +
        //                                      hRes.getStatusCode() + ' statusMsg=' + hRes.getStatus());
        JSONParser jp = JSON.createParser(mRespnse);
        
        while (jp.nextToken() != null) {
        // Start at the array of invoices.
        if (jp.getCurrentToken() == JSONToken.START_ARRAY) 
            while (jp.nextToken() != null) {
                // Advance to the start object marker to
                //  find next invoice statement object.
                if (jp.getCurrentToken() == JSONToken.START_OBJECT) {
                    // Read entire invoice object, including its array of line items.
                    mZiggo_Lead_Object inv = (mZiggo_Lead_Object)jp.readValueAs(mZiggo_Lead_Object.class);
                    //system.debug('Invoice number: ' + inv.Company);
                    //system.debug('Size of list items: ' + inv.lineItems.size());
                    // For debugging purposes, serialize again to verify what was parsed.
                    String s = JSON.serialize(inv);
                    //system.debug('Serialized invoice: ' + s);
                    // Skip the child start array and start object markers.
                    newZiggoLeads.add(inv);
                    jp.skipChildren();
                }
            }
        }

        if(!newZiggoLeads.isEmpty())
        {
    
            for (mZiggo_Lead_Object mylead :newZiggoLeads)
            {
                Ziggo_Lead__c myNewLead =  new Ziggo_Lead__c();
                
                myNewLead.ein_ziggo_source_id__c        = (ID) mylead.Id; //record.get('Id');
                myNewLead.ein_fiber__c                  = mylead.Fiber=='True' ? 1 : 0; //Boolean.valueOf(mylead.Fiber);  // (Boolean) ;
                myNewLead.ein_lg_dateofestablishment__c = (Date)   mylead.LG_DateOfEstablishment; 
                myNewLead.ein_lg_internetcustomer__c    = mylead.LG_InternetCustomer=='True' ? 1 : 0; //;
                myNewLead.ein_lg_LegalForm__c           = (String) mylead.LG_LegalForm;
                myNewLead.ein_lg_MobileCustomer__c      = mylead.LG_MobileCustomer=='True' ? 1 : 0; //;
                //myNewLead.LG_Segment__c               = (String) record.get('LG_Segment'); 
                if(!String.isBlank( mylead.LG_VisitPostalCode)){
                    myNewLead.ein_pc4__c                    = (String) mylead.LG_VisitPostalCode.left(4); 
                }
                myNewLead.ein_numberofemployees__c      = (Integer) mylead.NumberOfEmployees;
                system.debug('Marcel: New Lead: ' + mylead.Id);
                Ziggoleads.add(myNewLead);
                    
            }
            upsert Ziggoleads ein_ziggo_source_id__c;
        }
        return newZiggoLeads;
    }

    /**
    * Plain callout to be used outside this class
    *
    * @param query
    *
    * @return
    */
    public HttpResponse doSoqlQueryLite(String instanceUrlAddOn, String query) {
        String instanceUrl = this.sfdcInstanceUrl;
        if (String.isEmpty(instanceUrlAddOn)) {
            instanceUrl += '/services/data/v45.0/query';
        } else {
            instanceUrl += instanceUrlAddOn;
        }
        PageReference urlPg = new PageReference(instanceUrl);
        if (!String.isEmpty(query)) {
            urlPg.getParameters().put('q', query);
        }
        String uri = urlPg.getUrl();               // let APEX do the URL encoding of the parms as necessary
        HttpResponse hRes = this.sendNamedCredentials(uri, null);
        return hRes;
    }
}