/**
* Custom Settings for Endpoints Callouts
* 
* Class is used implement URL mappings for specific country
* 
* @author Petar Miletic
* @ticket SFDT-1270
* @since  22/07/2016
*/
public class ZG_EndpointResolver extends LG_EndpointResolver {

    /*
     * Generate Address Check Endpoint via house number, extension and postocde
    */
    public override string getAddressCheckEndpoint(string houseFlatNumber, string houseFlatExt, string postcode) {
        
        string uri = addressCheckEndpoint;

        uri += appendValue('houseFlatNumber', houseFlatNumber);
        uri += appendValue('houseFlatExt', houseFlatExt);
        uri += appendValue('postcode', postcode);
        return 'callout:' + uri;
    }
    
    /*
     * Generate Address Check Endpoint using values defined in Custom Settings (ZG specific)
    */
    public string getAddressCheckEndpoint(ZG_EndpointResolver.mapAndDisplay mapping, sObject searchData) {
        
        string uri = addressCheckEndpoint;

        if (mapping.fieldMap != null && !mapping.fieldMap.isEmpty()) {
            
            for (ZG_EndpointResolver.fieldMap o : mapping.fieldMap) {

                /*
                 * Address validation failing in JIT
                 *
                 * @author Petar Miletic
                 * @ticket SFDT-1591
                 * @since 17/10/2016
                */
                if (!isValuePopulated(o.sfFieldName, searchData)) {
                    continue;
                }

                // string fieldData = (string)searchData.get(o.sfFieldName);
                String fieldData = (string)searchData.get(o.sfFieldName);
                // added the below if statement by Shreyas for BPE-1755
                if(o.sfFieldName == 'LG_VisitPostalCode__c'){
                    string postcode_CorrectedFormat = fieldData;
                    postcode_CorrectedFormat = postcode_CorrectedFormat.replaceAll( '\\s+', '');
                    postcode_CorrectedFormat = postcode_CorrectedFormat.toUpperCase();
                    fieldData = postcode_CorrectedFormat;
                }
                
                if (o.searchFilter && fieldData != null) {
                    uri += appendValue(o.webServiceParam, fieldData);
                    
                    // Regex replace is used to remove empty space (example: aaaa  bbbb = aaaabbbb). Not sure if this is needed
                    // uri += appendValue(o.webServiceParam, fieldData.replaceAll( '\\s+', ''));
                }
            }
        }

        return 'callout:' + uri;
    }
    
    /*
     * Generate Availability Check Endpoint using Address Id
    */
    public override string getAvailabilityCheckEndpoint(string addressId) {

        if (String.isBlank(addressId)) {
            throw new LG_Exception('Address Id can not be null or empty');
        }
        
        string uri = availabilityCheckEndpoint;

        uri = uri.replace('{addressId}', addressId);

        return 'callout:' + uri;
    }
    
    
    /*
     * Generate endpoint for Order new (ZG Specific)
    */
    public string getOrderNewEndpoint(string countryCode) {
        
        LG_ServiceRequestUrl__c req = LG_ServiceRequestUrl__c.getInstance(UserInfo.getUserId());
        
        string uri = req.LG_OrderRequestNew__c;
        
        uri = uri.replace('{countryCode}', countryCode);
        
        return 'callout:' + uri;
    }

    /*
     * Helper classes used in LG_AddressValidationController for mapping LG_DisplayFields and LG_DisplayFieldsPremisse 
     *
     * (Those setting contains the display field for address validator for specific countries)
    */
    public class mapAndDisplay {
        public boolean isActive;
        public List<fieldMap> fieldMap {get; set;}
    }

    public class fieldMap {
        public String sfFieldName {get; set;}
        public String jsonField {get; set;}
        public Boolean resultVisible {get; set;}
        public String displayLabel {get; set;}
        public Boolean searchFilter {get; set;}
        public Boolean requiredParam {get; set;}
        public String webServiceParam {get; set;}
    }
}