/**
* Used as a utility class for Penalty Fee Calculation.
* Ziggo specific class.
* Holds inner class for calculating the Penalty Fees.
* 
* @author Tomislav Blazek
* @ticket SFDT-1298
* @since  12/07/2016
*/
public class ZG_PenaltyFeeCalculation extends LG_PenaltyFeeCalculation {
	
	public override Map<Id, Decimal> calculatePenaltyFees(List<csord__Service__c> services,
															Map<Id, cscfga__Product_Basket__c> subIdToBasket,
															Map<String, Date> basketAddressToWishdate)
	{
		Map<Id, Decimal> sliPrice = new Map<Id, Decimal>();

		Map<Id, Integer> servicesToCheck = new Map<Id, Integer>();
        //Added for modified penalty fee logic start
		Map<Id, Date> servceiIdContractEndDate = new Map<Id, Date>();
        Map<Id,Date> serviceIdTerminationWishDate = new Map<Id,Date>();
        //Added for modified penalty fee logic end
        
		for(csord__Service__c service : services)
		{
			Id basketId = subIdToBasket.get(service.csord__Subscription__c).Id;
			Id addressId = service.LG_Address__c;

			Date terminationWishDate = basketAddressToWishdate.get(basketId + '#' + addressId);
			
			if (service.LG_ContractEndDate__c != null)
			{
				//need to account the tolerated days...
				if (terminationWishDate.addDays(numberOfDaysTolerated).daysBetween(service.LG_ContractEndDate__c) > 0)
				{
					//counting in all the days, tolerated days included.
					servicesToCheck.put(service.Id, terminationWishDate.daysBetween(service.LG_ContractEndDate__c));
                    //Added for modified penalty fee logic start
                    servceiIdContractEndDate.put(service.Id,service.LG_ContractEndDate__c);
                    serviceIdTerminationWishDate.put(service.Id,terminationWishDate);
                    //Added for modified penalty fee logic end
				}
			}
		}
		
		if (!servicesToCheck.isEmpty())
		{
			List<csord__Service_Line_Item__c> serviceLineItems = [SELECT Id,
																	csord__Total_Price__c,
																	csord__Service__c
																	FROM csord__Service_Line_Item__c
																	WHERE csord__Service__c IN :servicesToCheck.keySet()
																	AND LG_Commitment__c = 'Yes'
																	AND csord__Is_Recurring__c = true];
			if (!serviceLineItems.isEmpty())
			{
				
				
				for(csord__Service_Line_Item__c sli : serviceLineItems)
				{
                    //Added for modified penalty fee logic start
                    Decimal penaltyForTerminationMonth = 0;
                    Decimal penaltyFeeForMonthsInBetween = 0;
                    Decimal penaltyFeeForContractEndMonth = 0;
                    Integer monthsBetweenTerminationAndContractDateExcludingLastPartialMonth = 0;
                    Date contractEndDate = servceiIdContractEndDate.get(sli.csord__Service__c);
                    Date terminationWishDate = serviceIdTerminationWishDate.get(sli.csord__Service__c);
                    Integer numberOfDaysInTerminationMonth = Date.daysInMonth(terminationWishDate.year(), terminationWishDate.month());
                    Date lastDayOfTerminationMonth = Date.newInstance(terminationWishDate.year(), terminationWishDate.month(),numberOfDaysInTerminationMonth);
                    Integer daysLeftInTerminationMonth = terminationWishDate.daysBetween(lastDayOfTerminationMonth)+1;
                    if(daysLeftInTerminationMonth!=numberOfDaysInTerminationMonth){
                    	penaltyForTerminationMonth = (sli.csord__Total_Price__c/numberOfDaysInTerminationMonth)*(daysLeftInTerminationMonth);
                    }
                    else{
                        monthsBetweenTerminationAndContractDateExcludingLastPartialMonth+=1;
                    }
					
                    Integer numberOfDaysInContractEndMonth = Date.daysInMonth(contractEndDate.year(), contractEndDate.month());
                    if(contractEndDate.day()!=numberOfDaysInContractEndMonth){
                    	penaltyFeeForContractEndMonth = (sli.csord__Total_Price__c/numberOfDaysInContractEndMonth)*(contractEndDate.day()-1);                        
                    }
                    else{
                        monthsBetweenTerminationAndContractDateExcludingLastPartialMonth+=1;
                    }
                    
					monthsBetweenTerminationAndContractDateExcludingLastPartialMonth+= terminationWishDate.monthsBetween(contractEndDate)-1;
                    
                    if(monthsBetweenTerminationAndContractDateExcludingLastPartialMonth>0){
                        penaltyFeeForMonthsInBetween = monthsBetweenTerminationAndContractDateExcludingLastPartialMonth*sli.csord__Total_Price__c;
                    }
					//Added for modified penalty fee logic end
					sliPrice.put(sli.Id,(penaltyForTerminationMonth+ penaltyFeeForContractEndMonth + penaltyFeeForMonthsInBetween).setScale(2));
				}
			}
		}
		return sliPrice;
	}
}