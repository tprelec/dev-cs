public class ZiggoActivityTriggerHandler extends TriggerHandler {
    public override void BeforeInsert() {
        List<ZiggoActivities__c> newZiggoOppList = (List<ZiggoActivities__c>) this.newList;

        List<String> kvkList = new List<String>();
        for (ZiggoActivities__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Kvk__c)) {
                kvkList.add(zo.Kvk__c);
            }
        }
        Map<String, Map<String,Id>> kvkMap = CSZiggoSync.seachKvk(kvkList);
        for (ZiggoActivities__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Kvk__c)) {
                if (kvkMap.containsKey(zo.Kvk__c)) {
                    Map<String, Id> kvkMapExt = kvkMap.get(zo.Kvk__c);
                    for (String tpe : kvkMapExt.keySet()) {
                        if (tpe == 'Lead') {
                            zo.Lead__c = kvkMapExt.get(tpe);
                        } else {
                            zo.Account__c = kvkMapExt.get(tpe);
                        }
                    }
                }
            }
        }
    }

    public override void BeforeUpdate() {
        List<ZiggoActivities__c> newZiggoOppList = (List<ZiggoActivities__c>) this.newList;

        List<String> kvkList = new List<String>();
        for (ZiggoActivities__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Kvk__c)) {
                kvkList.add(zo.Kvk__c);
            }
        }
        system.debug('@@kvkList: '+kvkList);
        Map<String, Map<String,Id>> kvkMap = CSZiggoSync.seachKvk(kvkList);
        system.debug('@@kvkMap: '+kvkMap);
        for (ZiggoActivities__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Kvk__c)) {
                if (kvkMap.containsKey(zo.Kvk__c)) {
                    Map<String, Id> kvkMapExt = kvkMap.get(zo.Kvk__c);
                    for (String tpe : kvkMapExt.keySet()) {
                        if (tpe == 'Lead') {
                            zo.Lead__c = kvkMapExt.get(tpe);
                        } else {
                            zo.Account__c = kvkMapExt.get(tpe);
                        }
                    }
                }
            }
        }
    }
}