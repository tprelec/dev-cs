@RestResource(urlMapping='/ZiggoLead/*')
global with sharing class ZiggoLeadObject {
    @HttpPost
    global static ID createZiggoLead(String Name, String Zipcode, String SourceID)
    {
        Ziggo_Lead__c thisLead = new Ziggo_Lead__c(
            //Name=Name,
            ein_ziggo_source_id__c=SourceID,
            ein_pc4__c=Zipcode);
        insert thisLead;
        return thisLead.Id;
    }   
}