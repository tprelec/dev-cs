/**
 * @description			This class is responsible for calling the ziggo org in bul
 * @author				Marcel Vreuls
 * @History				Juni 2019: Calling new version of API service 
  * 
 * @Defaults   			Requires the creation and update of named credentials
 *
 *
 */
global class ZiggoLeadScoreUpdateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    String ErrorMessage = 'No Errors occurred';

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id FROM Ban__c limit 1');
    } 
    
    global void execute(Database.BatchableContext BC, List<Account> scope) {
        
        try{					
			WebserviceHttpRest myCall = new WebserviceHttpRest();
            myCall.doSoqlQuery('SELECT ID,Company, Fiber__C,LG_DateOfEstablishment__c,LG_InternetCustomer__c,LG_LegalForm__c,LG_MobileCustomer__c,LG_Segment__c,LG_VisitPostalCode__c FROM lead WHERE LG_NewSalesChannel__c =  \'D2D\'  AND status != \'Qualified\'  and ein_outcome_conversion__c = null limit 50'); 
            ErrorMessage = ErrorMessage + mycall.response;
	 	} catch (Exception e) {
            ErrorMessage = e.getMessage();
	        System.debug(e.getMessage());
	    }
        
    }
       
    
    global void finish(Database.BatchableContext BC) {
        
        //Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							TotalJobItems
						  FROM
							AsyncApexJob
						  WHERE
							Id =: BC.getJobId()];

		// Send an email to the running user notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		String[] toAddresses = new String[] {'marcel.vreuls@vodafoneziggo.com'};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Ziggo Einstein Scoring ' + a.Status);
		mail.setPlainTextBody('The Ziggo Einstein Scoring job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures. With the following messages: ' + ErrorMessage );
		// stopped sending mail. to much	
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

		ExportUtils.startNextJob('ZiggoLeadScoreUpdate');
    }
}