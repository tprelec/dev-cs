public class ZiggoLeadStatusTriggerHandler extends TriggerHandler {
    public override void BeforeInsert() {
        List<ZiggoLeadStatus__c> newZiggoOppList = (List<ZiggoLeadStatus__c>) this.newList;

        List<String> kvkList = new List<String>();
        for (ZiggoLeadStatus__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Kvk__c)) {
                kvkList.add(zo.Kvk__c);
            }
        }
        Map<String, Map<String,Id>> kvkMap = CSZiggoSync.seachKvk(kvkList);
        for (ZiggoLeadStatus__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Kvk__c)) {
                if (kvkMap.containsKey(zo.Kvk__c)) {
                    Map<String, Id> kvkMapExt = kvkMap.get(zo.Kvk__c);
                    for (String tpe : kvkMapExt.keySet()) {
                        if (tpe == 'Lead') {
                            zo.Lead__c = kvkMapExt.get(tpe);
                        }
                    }
                }
            }
        }
    }

    public override void BeforeUpdate() {
        List<ZiggoLeadStatus__c> newZiggoOppList = (List<ZiggoLeadStatus__c>) this.newList;

        List<String> kvkList = new List<String>();
        for (ZiggoLeadStatus__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Kvk__c)) {
                kvkList.add(zo.Kvk__c);
            }
        }
        system.debug('@@kvkList: '+kvkList);
        Map<String, Map<String,Id>> kvkMap = CSZiggoSync.seachKvk(kvkList);
        system.debug('@@kvkMap: '+kvkMap);
        for (ZiggoLeadStatus__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Kvk__c)) {
                if (kvkMap.containsKey(zo.Kvk__c)) {
                    Map<String, Id> kvkMapExt = kvkMap.get(zo.Kvk__c);
                    for (String tpe : kvkMapExt.keySet()) {
                        if (tpe == 'Lead') {
                            zo.Lead__c = kvkMapExt.get(tpe);
                        }
                    }
                }
            }
        }
    }
}