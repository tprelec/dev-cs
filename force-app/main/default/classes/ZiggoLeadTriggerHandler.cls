/**
 * Description:    W-001473     Einstein Discovery
 * Date            2019-02      Marcel Vreuls, ada
 * Date            2019-05  	W-001579 Marcel Vreuls: removed the authtenication from custom settings, now this is named credentials
 */
public class ZiggoLeadTriggerHandler extends TriggerHandler {
    
    private static final String ZIPCODE_FIELDNAME = Ziggo_Lead__c.ein_pc4__c.getDescribe().getName();
    
    private static final Set<String> RELATED_LOOKUP_FIELDS = new Set<String> {
    	Ziggo_Lead__c.Ein_Demographic__c.getDescribe().getName(),
    	Ziggo_Lead__c.ein_Zipcode_Info__c.getDescribe().getName()
    };
        
    /**
     * Handles the before insert logic
     * - Enrich demographics
     * - added ein_Zipcode_Info__c 
     */
    public override void BeforeInsert(){
        List<Ziggo_Lead__c> newZiggoLeads = (List<Ziggo_Lead__c>) this.newList;
        populateZiggoLeadLookups(newZiggoLeads);//by reference
        
    }
    
    /**
     * For each lead for which the zipcode change, update necessary lookups again
     * - Demographics
     * - Zipcode information
     */
    public override void BeforeUpdate() {
      List<Ziggo_Lead__c> newZiggoLeads = (List<Ziggo_Lead__c>) this.newList;
      Map<Id, Ziggo_Lead__c> updatedZiggoLeadMap = (Map<Id, Ziggo_Lead__c>) this.oldMap;
      List<Ziggo_Lead__c> zipcodeChangedList = getZipcodeChangedList(newZiggoLeads, updatedZiggoLeadMap);
      populateZiggoLeadLookups(zipcodeChangedList);
    }
    
    /**
     * Currently just call the einstein discovery service to get the necessary predictions
     */
    public override void AfterInsert() {
      EinsteinDiscoveryService.getPrediction(Ziggo_Lead__c.sObjectType.getDescribe().getName());
    }
    
    /**
     * Currently just call the einstein discovery service to get the necessary predictions
     */
    public override void AfterUpdate() {
      EinsteinDiscoveryService.getPrediction(Ziggo_Lead__c.sObjectType.getDescribe().getName());

      List<string> myLead = new List<string>();
      for (sObject xLead : this.newList){
          myLead.add(JSON.serialize(xlead));
      }

      // call webservice in order to update
        if(!Test.isRunningTest()){
            updateZiggoSalesforce(myLead);
            }
      
    }
    
    @Future(callout=true)
    private static void updateZiggoSalesforce(List<string> newZiggoLead){
        List<Ziggo_lead__c> newleadlist = new List<Ziggo_lead__c>();
        Ziggo_lead__c newLead = null;
        system.debug('vreuls: ' +newZiggoLead );        
        for (String ser:newZiggoLead) {
            Ziggo_lead__c dsh = (Ziggo_lead__c) JSON.deserialize(ser, Ziggo_lead__c.class);
            newleadlist.add(dsh);
         }
 
        WebserviceHttpRest myCall = new WebserviceHttpRest();
       // myCall.authenticateByUserNamePassword('3MVG9GXbtnGKjXe6vOdZk7UMRDAb8NPpTV_f4AEVtRV5fv5sL0NQM6Yk9ULUePPGorzSwoQ226L_XIWjMaZRj','8557AE08AF61AF768C97315EF3A54101A705E820CE53AC58F2C5A6A5C564B3A1','myName','myPass', true);
        myCall.UpdateLeads(newleadlist);


    }
    /**
     * Determine if the zipcode on the ziggo lead got changed:
     * - If set to null/blank -> directly clear it
     * - Add it to the list to be reexamined later on 
     */
    private List<Ziggo_Lead__c> getZipcodeChangedList(List<Ziggo_Lead__c> newZiggoLeads, Map<Id, Ziggo_Lead__c> updatedZiggoLeadMap) {
        return ZipcodeService.getZipcodeChangedList(newZiggoLeads, updatedZiggoLeadMap, ZIPCODE_FIELDNAME, RELATED_LOOKUP_FIELDS);
    }
    
    /**
     * This method will try to populate the necessary lookups:
     * - Demographics based on zipcode (digits only) - Could exist
     * - Province (zipcode match - digits only) - Must exist
     */
    private void populateZiggoLeadLookups(List<Ziggo_Lead__c> ziggoLeadList) {
        if (ziggoLeadList.isEmpty()) {
            return;
        }
        Map<String, List<Ziggo_Lead__c>> zipcodeLeadMap = getZipcodeLeadMap(ziggoLeadList);
        Set<String> zipcodeSet = zipcodeLeadMap.keySet();
        Map<String, Id> zipcodeDemographicMap = getZipcodeDemographicsMap(zipcodeSet);
        Map<String, Id> zipcodeInformationMap = zipcodeInformationMap(zipcodeSet);
        for (String zipcode : zipcodeSet) {
            List<Ziggo_Lead__c> zLeadSet = zipcodeLeadMap.get(zipcode);
            for (Ziggo_Lead__c zLead : zLeadSet) {
                zLead.Ein_Demographic__c = zipcodeDemographicMap.containsKey(zipcode) ? zipcodeDemographicMap.get(zipcode) : null;
                zLead.ein_Zipcode_Info__c = zipcodeInformationMap.containsKey(zipcode) ? zipcodeInformationMap.get(zipcode) : null;
            }
        }
    }
    
    /**
     * Creates a mapping between the zipcode and the demographics (Id)
     */
    private Map<String, Id> getZipcodeDemographicsMap(Set<String> zipcodeSet) {
      Map<String, Id> zipcodeDemographicMap = new Map<String, Id>();
      List<Demographic__c> dList = [select Id, ein_pc4__c from Demographic__c where ein_pc4__c in :zipcodeSet];
      for (Demographic__c d : dList) {
        zipcodeDemographicMap.put(d.ein_pc4__c, d.Id);
      }
      
      return zipcodeDemographicMap;
    }
    
    /**
     * Creates a mapping between the zipcode and the zipcode information (Id)
     */
    private Map<String, Id> zipcodeInformationMap(Set<String> zipcodeSet) {
        return ZipcodeService.zipcodeInformationMap(zipcodeSet);
    }
    
    /**
     * Create a mapping between zipcode/leads for easy use later
     */
    private Map<String, List<Ziggo_Lead__c>> getZipcodeLeadMap(List<Ziggo_Lead__c> ziggoLeadList) {
        return ZipcodeService.getZipcodeSObjectMap(ziggoLeadList, ZIPCODE_FIELDNAME);
    }
}