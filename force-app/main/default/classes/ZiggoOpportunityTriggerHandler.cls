public class ZiggoOpportunityTriggerHandler extends TriggerHandler {
    public override void BeforeInsert() {
        List<ZiggoOpportunity__c> newZiggoOppList = (List<ZiggoOpportunity__c>) this.newList;

        List<String> kvkList = new List<String>();
        for (ZiggoOpportunity__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Account_LG_ChamberOfCommerceNumber__c)) {
                kvkList.add(zo.Account_LG_ChamberOfCommerceNumber__c);
            }
        }
        Map<String, Map<String,Id>> kvkMap = CSZiggoSync.seachKvk(kvkList);
        for (ZiggoOpportunity__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Account_LG_ChamberOfCommerceNumber__c)) {
                if (kvkMap.containsKey(zo.Account_LG_ChamberOfCommerceNumber__c)) {
                    Map<String, Id> kvkMapExt = kvkMap.get(zo.Account_LG_ChamberOfCommerceNumber__c);
                    for (String tpe : kvkMapExt.keySet()) {
                        if (tpe == 'Lead') {
                            zo.Lead__c = kvkMapExt.get(tpe);
                        } else {
                            zo.Account__c = kvkMapExt.get(tpe);
                        }
                    }
                }
            }
        }
    }

    public override void BeforeUpdate() {
        List<ZiggoOpportunity__c> newZiggoOppList = (List<ZiggoOpportunity__c>) this.newList;

        List<String> kvkList = new List<String>();
        for (ZiggoOpportunity__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Account_LG_ChamberOfCommerceNumber__c)) {
                kvkList.add(zo.Account_LG_ChamberOfCommerceNumber__c);
            }
        }
        system.debug('@@kvkList: '+kvkList);
        Map<String, Map<String,Id>> kvkMap = CSZiggoSync.seachKvk(kvkList);
        system.debug('@@kvkMap: '+kvkMap);
        for (ZiggoOpportunity__c zo : newZiggoOppList) {
            if (!String.isEmpty(zo.Account_LG_ChamberOfCommerceNumber__c)) {
                if (kvkMap.containsKey(zo.Account_LG_ChamberOfCommerceNumber__c)) {
                    Map<String, Id> kvkMapExt = kvkMap.get(zo.Account_LG_ChamberOfCommerceNumber__c);
                    for (String tpe : kvkMapExt.keySet()) {
                        if (tpe == 'Lead') {
                            zo.Lead__c = kvkMapExt.get(tpe);
                        } else {
                            zo.Account__c = kvkMapExt.get(tpe);
                        }
                    }
                }
            }
        }
    }

    public override void AfterInsert(){
        List<ZiggoOpportunity__c> newZiggoOppList = (List<ZiggoOpportunity__c>) this.newList;
        CSZiggoSync.addProducts(newZiggoOppList);
    }

    public override void AfterUpdate() {
        List<ZiggoOpportunity__c> newZiggoOppList = (List<ZiggoOpportunity__c>) this.newList;
        CSZiggoSync.addProducts(newZiggoOppList);
    }
}