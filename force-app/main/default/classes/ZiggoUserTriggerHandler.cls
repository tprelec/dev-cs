/**
 * Description:    ...          Einstein Discovery
 * Date            2019-02      Stefan Botman
 * Ziggo User trigger handler
 */
public class ZiggoUserTriggerHandler extends TriggerHandler {
    
    private static final String ZIPCODE_FIELDNAME = Ziggo_User__c.ein_sp_zipcode__c.getDescribe().getName();
    
    private static final Set<String> RELATED_LOOKUP_FIELDS = new Set<String> {
    	Ziggo_User__c.ein_zipcode_info__c.getDescribe().getName()
    };

    /**
     * Handles the before insert logic
     * - populate necessary lookups
     */
    public override void BeforeInsert(){
        List<Ziggo_User__c> newZiggoUserList = (List<Ziggo_User__c>) this.newList;
        populateZiggoUserLookups(newZiggoUserList);//by reference
    }
    
    /**
     * For each Ziggo User for which the zipcode changes, update necessary lookups again
     * - Zipcode information
     */
    public override void BeforeUpdate() {
      List<Ziggo_User__c> newZiggoUserList = (List<Ziggo_User__c>) this.newList;
      Map<Id, Ziggo_User__c> updatedZiggoUserMap = (Map<Id, Ziggo_User__c>) this.oldMap;
      List<Ziggo_User__c> zipcodeChangedList = getZipcodeChangedList(newZiggoUserList, updatedZiggoUserMap);
      populateZiggoUserLookups(zipcodeChangedList);
    }
    
    /**
     * This method will try to populate the necessary lookups:
     * - Province (zipcode match - digits only) - Must exist
     */
    private void populateZiggoUserLookups(List<Ziggo_User__c> ziggoUserList) {
        if (ziggoUserList.isEmpty()) {
            return;
        }
        Map<String, List<Ziggo_User__c>> zipcodeUserMap = getZipcodeUserMap(ziggoUserList);
        Set<String> zipcodeSet = zipcodeUserMap.keySet();
        Map<String, Id> zipcodeInformationMap = zipcodeInformationMap(zipcodeSet);
        for (String zipcode : zipcodeSet) {
            List<Ziggo_User__c> zUserSet = zipcodeUserMap.get(zipcode);
            for (Ziggo_User__c zUser : zUserSet) {
                zUser.ein_Zipcode_Info__c = zipcodeInformationMap.containsKey(zipcode) ? zipcodeInformationMap.get(zipcode) : null;
            }
        }
    }
    
    /**
     * Create a mapping between zipcode/ziggo users for easy use later
     */
    private Map<String, List<Ziggo_User__c>> getZipcodeUserMap(List<Ziggo_User__c> ziggoUserList) {
        return ZipcodeService.getZipcodeSObjectMap(ziggoUserList, ZIPCODE_FIELDNAME);
    }
    
    
    /**
     * Creates a mapping between the zipcode and the zipcode information (Id)
     */
    private Map<String, Id> zipcodeInformationMap(Set<String> zipcodeSet) {
        return ZipcodeService.zipcodeInformationMap(zipcodeSet);
    }
    
    /**
     * Determine if the zipcode on the ziggo user got changed:
     * - If set to null/blank -> directly clear it
     * - Add it to the list to be reexamined later on 
     */
    private List<Ziggo_User__c> getZipcodeChangedList(List<Ziggo_User__c> newZiggoUserList, Map<Id, Ziggo_User__c> updatedZiggoUserMap) {
        return ZipcodeService.getZipcodeChangedList(newZiggoUserList, updatedZiggoUserMap, ZIPCODE_FIELDNAME, RELATED_LOOKUP_FIELDS);
    }
}