/**
 * Description:    ...	     	Einstein Discovery
 * Date            2019-02  	Stefan Botman
 * Date            2019-04  	W-001579 Marcel Vreuls: added Map 4 digits zipcode.
 * Date            2019-05  	W-001579 Marcel Vreuls: 
 * This is a service class to encapsulate all kinds of zipcode functionalities
 */
public class ZipcodeService {



    /**
     * W-001579 Einstein D2D Implementation
     * Creates a mapping between the kvk and the sizo information (Id)
     */
    public static Map<String, Id> SizoInformationMap(Set<String> SizoSet) {
        Map<String, Id> SizoInformationMap = new Map<String, Id>();
        List<Sizo_Data__c> dList = [select Id, ein_sizo_kvk__c from Sizo_Data__c WHERE ein_sizo_kvk__c != null AND ein_sizo_kvk__c in :SizoSet];
        for (Sizo_Data__c d : dList) {
            SizoInformationMap.put(d.ein_sizo_kvk__c, d.Id);
        }        
        return SizoInformationMap;
    }

    /**
     * W-001579 Einstein D2D Implementation
     * Create a mapping between kvk/sObject for easy use
     */
    public static Map<String, List<sObject>> getSizoSObjectMap(List<sObject> sObjectList, String kvkFieldName) {
        Map<String, List<sObject>> SizoSObjectMap = new Map<String, List<sObject>>();
        Integer i = 0, j = sObjectList.size();
        for (; i<j; i ++) {
            sObject s = sObjectList.get(i);
            String kvk = (String)s.get(kvkFieldName);
            if (!SizoSObjectMap.containsKey(kvk)) {
                SizoSObjectMap.put(kvk, new List<sObject>());
            }
            SizoSObjectMap.get(kvk).add(s);
        }
        return SizoSObjectMap;
    }
 

	/**
     * Creates a mapping between the zipcode and the zipcode information (Id)
     */
    public static Map<String, Id> zipcodeInformationMap(Set<String> zipcodeSet) {
        Map<String, Id> zipcodeInformationMap = new Map<String, Id>();
        List<Zipcode_Info__c> dList = [select Id, postalcode__c from Zipcode_Info__c where postalcode__c in :zipcodeSet];
        for (Zipcode_Info__c d : dList) {
            zipcodeInformationMap.put(d.postalcode__c, d.Id);
        }
        
        return zipcodeInformationMap;
    }
    
    /**
     * Create a mapping between zipcode/sObject for easy use
     */
    public static Map<String, List<sObject>> getZipcodeSObjectMap(List<sObject> sObjectList, String zipcodeFieldName) {
        Map<String, List<sObject>> zipcodeSObjectMap = new Map<String, List<sObject>>();
        Integer i = 0, j = sObjectList.size();
        for (; i<j; i ++) {
            sObject s = sObjectList.get(i);
            String zipcode = (String)s.get(zipcodeFieldName);
            if (!zipcodeSObjectMap.containsKey(zipcode)) {
                zipcodeSObjectMap.put(zipcode, new List<sObject>());
            }
            zipcodeSObjectMap.get(zipcode).add(s);
        }
        return zipcodeSObjectMap;
    }
    
    /**
     * Determine if the zipcode on the sObject got changed:
     * - If set to null/blank -> directly clear it
     * - Add it to the list to be reexamined later on 
     */
    public static List<sObject> getZipcodeChangedList(List<sObject> newsObjectList, Map<Id, sObject> updatedSObjectMap, String zipcodeFieldName, Set<String> relatedLookupFields) {
        List<sObject> zipcodeChangedList = new List<sObject>();
        for (sObject s : newsObjectList) {
            String newZipcode = s.get(zipcodeFieldName) == null ? '' : (String)s.get(zipcodeFieldName);
            String oldZipcode = updatedSObjectMap.get(s.Id).get(zipcodeFieldName) == null ? '' : (String)updatedSObjectMap.get(s.Id).get(zipcodeFieldName);

            if (newZipcode!= oldZipcode) {
                // Two actions: either lookup again (later) or clear it:
                if (String.isNotBlank(newZipcode)) {
                    zipcodeChangedList.add(s);
                } else {
                    // Clear:
                    for (String lookup : relatedLookupFields) {
                        s.put(lookup, null);
                    }
                }
            }
        }
        return zipcodeChangedList;
    }
}////