// Retrieves additional context for agreement document generation
public with sharing class customAgreementController
{
    // Initialise
    public List<OpportunityLineItem> lines {get;set;}
    public List<OpportunityContactRole> cRoles {get;set;}
    public OpportunityContactRole cRole {get;set;} 
    public String customerNameSetting;
    public String customerLogoSetting;
    public csclm__Agreement__c agreement;
    public Opportunity opp {get;set;}
    public String opportunityId;
    public String customerName {get;set;}
    public String customerLogo {get;set;}

    public cscfga__Product_Basket__c pBasket {get;set;}
    public List<cscfga__Product_Configuration__c> csProdConfigList {get;set;}
    public String identifier {get;set;}
    
    // Constructor
    public customAgreementController(ApexPages.StandardController controller)
    {
        // Initialise
        system.debug('+++++++++ controller.getRecord(): ' + controller.getRecord());
        agreement = (csclm__Agreement__c) controller.getRecord();
        agreement = [select id, name, csclm__Opportunity__c, csclm__Last_Generated_Transactional_Document__c,
                     csclm__Latest_Transactional_Document__c 
                     from csclm__Agreement__c
                     where id = :agreement.id];
        system.debug('+++++++++ Agreement: ' + agreement);
        opportunityId = agreement.csclm__Opportunity__c;
        // Retrieve opportunity line items and other context
        opp = [select Id, Name, Account.name, Account.BillingStreet, Account.BillingCity, Account.BillingState,
               Account.BillingCountry, Account.BillingPostalCode, Account.ShippingStreet, Account.ShippingPostalCode, 
               Account.ShippingCity, Account.ShippingCountry, Owner.FirstName, Owner.LastName, CloseDate,
               (select id, name, description
                from OpportunityLineItems)
               from opportunity
               where Id = :opportunityId];
        lines = opp.OpportunityLineItems;
        //Querying the Primary Contact details
        cRoles=[select contactId, contact.name, contact.phone 
                from OpportunityContactRole 
                where OpportunityId=:opp.Id and isPrimary=true]; 
        if(cRoles.size()>0)
        {
            cRole=cRoles[0];
        }
        
        //Quering the Bakset details
        pBasket = [select id, cscfga__total_contract_value__c 
                   from cscfga__Product_Basket__c 
                   where cscfga__Opportunity__c=:opp.Id 
                   and csbb__Synchronised_With_Opportunity__c=true limit 1];
        
        Demo_Customer_Info__c customerSetting = Demo_Customer_Info__c.getInstance(UserInfo.getUserId());
        customerName = customerSetting.Demo_Organisation_Name__c;
        customerLogo = customerSetting.Demo_Organisation_Logo__c;

    }

}