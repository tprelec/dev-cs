global class demoDiscountConfiurationResultObserver implements csdiscounts.IDiscountConfigurationResultObserver{
     
    public demoDiscountConfiurationResultObserver () {
         
    }
 
    public csdiscounts.ConfigurationQueryResult execute(Id basketId, csdiscounts.ConfigurationQueryResult configurationQueryResult){
 
        system.debug('Update configurationQueryResult:: '+configurationQueryResult);
 
        return configurationQueryResult;
    }
}