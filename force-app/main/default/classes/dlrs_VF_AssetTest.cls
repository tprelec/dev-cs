/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_VF_AssetTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_VF_AssetTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new VF_Asset__c());
    }
}