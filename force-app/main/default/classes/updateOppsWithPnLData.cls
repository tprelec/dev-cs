public class updateOppsWithPnLData implements Database.Batchable<sObject>
{
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT id, StageName, Primary_Basket__r.Id, Primary_Basket__r.cscfga__Basket_Status__c, Name, Total_credit_amount_loyalty__c, ' 
        + 'Total_cost_of_sales__c, Revenue_share__c, Overhead_allocation__c, Other_Costs__c, Opportunity_cost__c, '
        + 'Operator_other_costs_other_migration__c, Network_Opex__c, Network_Depreciation__c, Incremental_OPEX__c, '
        + 'Direct_Capex__c, Capex__c, Benchmark__c, A_R__c ' 
        + 'FROM Opportunity '
        + 'WHERE Primary_Basket__c != \'\' AND (Capex__c = null OR Operator_other_costs_other_migration__c = null '
        + 'OR Total_credit_amount_loyalty__c = null OR Benchmark__c = null OR Total_cost_of_sales__c = null '
        + 'OR Opportunity_cost__c = null OR Other_Costs__c = null OR Revenue_share__c = null OR A_R__c = null '
        + 'OR Incremental_OPEX__c = null OR Overhead_allocation__c = null OR Network_Opex__c = null '
        + 'OR Direct_Capex__c = null OR Network_Depreciation__c = null)';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Opportunity> oppList){
        cscfga__Product_Basket__c targetBasket = [SELECT Id, cscfga__Basket_Status__c, Name
                                 FROM cscfga__Product_Basket__c
                                 WHERE Id =: oppList[0].Primary_Basket__r.Id];
        if (oppList[0].StageName == 'Closed Lost' || targetBasket.cscfga__Basket_Status__c == 'Contract created' || targetBasket.cscfga__Basket_Status__c == 'Pending approval' || targetBasket.cscfga__Basket_Status__c == 'Approved')
        {
            String basketId = oppList[0].Primary_Basket__r.Id;
            CustomButtonSubmitForApproval.updateKpiFields(basketId);
            List<cscfga__Product_Basket__c> basket = [SELECT id,cscfga__Opportunity__c,KPI_EBITDA_Net_revenue__c,KPI_Free_cash_flow_Net_revenue__c,KPI_Net_Incremental_Billed_Revenue__c,
                                                             KPI_NPV__c,KPI_Payback_Period_Months__c,KPI_Sales_Margin_Net_revenue__c, ProfitLoss_JSON__c 
                                                      FROM cscfga__Product_Basket__c 
                                                      WHERE id =: oppList[0].Primary_Basket__r.Id ];
            SyncWithOpportunityUtility.SynchronizeProfitLossOppFields(basket[0]);
        }
    }

    public void finish(Database.BatchableContext bc){
    }  
}