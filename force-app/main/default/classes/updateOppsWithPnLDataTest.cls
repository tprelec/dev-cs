@isTest
public with sharing class updateOppsWithPnLDataTest {

    @TestSetup
    private static void testSetup()
    {
        Account testAcc = CS_DataTest.createAccount('testAcc');
        insert testAcc;
        
        Opportunity testOpp = CS_DataTest.createVodafoneOpportunity(testAcc, 'testOpp', UserInfo.getUserId());
        testOpp.StageName = 'Qualify';
        testOpp.Total_credit_amount_loyalty__c = null;
        testOpp.Total_cost_of_sales__c = null;
        testOpp.Revenue_share__c = null;
        testOpp.Overhead_allocation__c = null;
        testOpp.Other_Costs__c = null;
        testOpp.Opportunity_cost__c = null;
        testOpp.Operator_other_costs_other_migration__c = null;
        testOpp.Network_Opex__c = null;
        testOpp.Network_Depreciation__c = null;
        testOpp.Incremental_OPEX__c = null;
        testOpp.Direct_Capex__c = null;
        testOpp.Capex__c = null;
        testOpp.Benchmark__c = null;
        testOpp.A_R__c = null;
        insert testOpp;
        
        cscfga__Product_Definition__c testProdDefn = CS_DataTest.createProductDefinitionRegular('testProdDefn');
        insert testProdDefn;
        
        cscfga__Product_Basket__c testBasket = CS_DataTest.createProductBasket(testOpp, 'testBasket');
        testBasket.cscfga__Basket_Status__c = 'Pending approval';
        testBasket.KPI_EBITDA_Net_revenue__c = 0;
        testBasket.KPI_Free_cash_flow_Net_revenue__c = 0;
        testBasket.KPI_Net_Incremental_Billed_Revenue__c = 0;
        testBasket.KPI_NPV__c = 0;
        testBasket.KPI_Payback_Period_Months__c = 0;
        testBasket.KPI_Sales_Margin_Net_revenue__c = 0;
        testBasket.ProfitLoss_JSON__c = '';
        testBasket.csbb__Synchronised_With_Opportunity__c = true;
        insert testBasket;
        
        cscfga__Product_Configuration__c testConfig = CS_DataTest.createProductConfiguration(testProdDefn.Id, 'testConfig', testBasket.Id);
        testConfig.Proposition__c = 'Vodafone Calling';
        testConfig.cspl__Type__c = 'Vodafone Calling';
        insert testConfig;
        
        testOpp.Primary_Basket__c = testBasket.Id;
        update testOpp;
    }
    
    @isTest
    private static void testUpdateOppsWithPnLData()
    {
        List<cscfga__Product_Basket__c> targetBasket = [SELECT Id, cscfga__Basket_Status__c, Name
                                 FROM cscfga__Product_Basket__c
                                 WHERE csbb__Synchronised_With_Opportunity__c = true];
        targetBasket[0].cscfga__Basket_Status__c ='Pending approval';
        update targetBasket;
        Test.startTest();
            updateOppsWithPnLData obj = new updateOppsWithPnLData();
            DataBase.executeBatch(obj);
        Test.stopTest();
        List<Opportunity> resultOpp = [Select Capex__c 
                                FROM Opportunity
                                WHERE Name = 'testOpp'];
        
        System.assertNotEquals(null, resultOpp[0].Capex__c);
    }
}