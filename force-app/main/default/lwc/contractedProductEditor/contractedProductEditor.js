import { LightningElement, api, wire, track } from "lwc";

import { OPPTY_COLUMNS, LINE_ITEM_COLUMNS } from "./tabledefinitions.js";
import { Labels } from "./labels.js";

import { refreshApex } from "@salesforce/apex";
import { deleteRecord } from "lightning/uiRecordApi";
import { NavigationMixin } from "lightning/navigation";

import { reduceErrors, showToast } from "c/ldsUtils";


import sitesJSON from "@salesforce/apex/OpportunityProductEditorLWCController.sitesJSON";
import queryShoppingCartForCP from '@salesforce/apex/OpportunityProductEditorLWCController.queryShoppingCartForCP';
import deleteContractedProducts from '@salesforce/apex/OpportunityProductEditorLWCController.deleteContractedProducts';
import updateContractedProducts from '@salesforce/apex/OpportunityProductEditorLWCController.updateContractedProducts';
import massCeaseContractProducts from '@salesforce/apex/OpportunityProductEditorLWCController.massCeaseContractProducts';
import updateCPProducts from '@salesforce/apex/OpportunityProductEditorLWCController.updateCPProducts';
import massPriceChangeCntrctProducts from '@salesforce/apex/OpportunityProductEditorLWCController.massPriceChangeCntrctProducts';
import massActivityContractProducts from '@salesforce/apex/OpportunityProductEditorLWCController.massActivityContractProducts';


import LIST_PRICE_FIELD from "@salesforce/schema/OpportunityLineItem.Gross_List_Price__c";
import QUANTITY_FIELD from "@salesforce/schema/OpportunityLineItem.Quantity";
import DISCOUNT_FIELD from "@salesforce/schema/OpportunityLineItem.DiscountNew__c";
import DURATION_FIELD from "@salesforce/schema/OpportunityLineItem.Duration__c";
import ARPU_FIELD from "@salesforce/schema/OpportunityLineItem.Product_Arpu_Value__c";
import CLC_FIELD from "@salesforce/schema/Contracted_Products__c.CLC__c";
import ID_FIELD from "@salesforce/schema/Contracted_Products__c.Id";
import ACTIVITYTYPE_FIELD from "@salesforce/schema/Contracted_Products__c.Activity_Type__c";

//Objects:
import OPPORTUNITY_OBJECT from "@salesforce/schema/Opportunity";
import OPPORTUNITY_LINE_ITEM_OBJECT from "@salesforce/schema/OpportunityLineItem";

import { getObjectInfo } from "lightning/uiObjectInfoApi";

const NO_SITE_NAME = "No site";

const ACQ = "Acq",
	RET = "Ret",
	MIG = "Mig";
const ONE_OFF_COSTS = "One off costs",
	MONTHLY_COSTS = "Monthly costs";
export default class ContractedProductEditor extends NavigationMixin(
	LightningElement
) {
	@api recordId;

	// All labels:
	labels = new Labels();
	// Datatable properties:
	@track errors;
	@track allProducts = [];
	@track shownProducts = [];
	draftValues = [];

	// Needed to support the refreshing of the retrieved products based on the wire js method. Without this, it will not track the changes (eventhough @track is not needed here)
	shoppingCartResults;

	@track selectedRowIds = [];

	@track opptyAccount; // Account related to the oppty
	@track opportunity;
	@track opptyLoaded = false;

	// Modal properties:
	@track showInstalledBase = false;
	@track isModalOpen = false;
	@track showMainMenu = true;
	@track isLoading = true;
	@track isPriceModalOpen = false;
	@track isCeasesModalOpen = false;
	@track selectedRow;
	activityOptions;
	activitySearchOptions;

	@track opptySummary;

	@track
	opptyColumns = OPPTY_COLUMNS;

	// Filter inputs:
	@track prodSearch;
	@track siteSearch;
	@track activityTypeSearch;

	@track
	columns = LINE_ITEM_COLUMNS;

	// delete Action
	@track isDeleteModal = false;
	@track deleteId;
	// For Sites
	@track siteOptions = [];
	clcOptions;
	clcForPCOptions;
	// Product lookup
	@api objName = "VF_Product__c";
	@api iconName;
	@api filter = "";
	@api searchPlaceholder = this.labels.search;

	installedBaseSelectLabel =
		this.labels.select + " " + this.labels.installedBase;
	returnToOpptyLabel = this.labels.returnTo + " " + this.labels.opportunity;
	filterOnSiteLabel = this.labels.filterOn + " " + this.labels.site;
	filterOnProductLabel = this.labels.filterOn + " " + this.labels.product;
	filterOnActivityTypeLabel =
		this.labels.filterOn + " " + this.labels.activityType;

	@track selectedProducts = [];
	@track selectedName;
	@track records;
	@track isValueSelected;
	@track blurTimeout;
	searchTerm;

	@wire(getObjectInfo, { objectApiName: OPPORTUNITY_LINE_ITEM_OBJECT })
	opportunityLineItem;

	@wire(getObjectInfo, { objectApiName: OPPORTUNITY_OBJECT })
	// eslint-disable-next-line no-unused-vars -- method signature
	oppInfo({ data, error }) {
		if (data) {
			console.log("CloseDate Label => ", data.fields.CloseDate.label);
		}
	}

	//css
	@track boxClass =
		"slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus";
	@track inputClass = "";
	// New Button
	@track isNewModalOpen = false;
	siteMulti = [];
	@track opportunityProdRecord = {
		clc: "Acq",
		quantity: QUANTITY_FIELD,
		listprice: LIST_PRICE_FIELD,
		discount: 0,
		duration: 1,
		costCenter: "",
		billing: true,
		site: "",
		productId: "",
		siteMulti: this.siteMulti,
		activityType: "Provide"
	};
	// Mass delete
	@track buttonLabel = "Delete";
	@track isMassDelete = true;
	@track recordsCount = 0;
	selectedRecords = [];
	@track isMassDeleteModal = false;
	@track isSingle = false;

	// Mass Clone
	@track isMassClone = true;
	 
	// Mass Edit & Cease 
	@track isMassEditModal = false;
	@track isMassCease = false;
	@track isMassPriceChange = false;
	@track isMassEdit = true;
	@track administrativeEndDate;
	@track contractEndDate;
	selectedCeaseRecords = [];
	selectedPriceChangeRecords = [];
	@track recordsCeaseCount = 0;
	@track isSingleCease = false;
	@track isSingleCeaseMsg = false;
	@track recordsPriceChangeCount = 0;
	@track isSinglePriceChange = false;
	@track isSinglePriceChangeMsg = false;
	@track isFinalMsgCease = false;
	@track isFinalMsgPriceChange = false;
	
   @track opportunityEditPcRecord = {
	clc: "",
	listprice: '',
	discount: '',
	duration: '',
	costCenter: "",
	billing: true,
	poNumber:"",
	invoiceDate:""
};

    // Contracted Product 
	@api vfContractId;
    @api orderId;
    @track allCPProducts = [];
    @track shownCPProducts = [];
	shoppingCartCPResults;
	@track sortDirection;
    @track sortBy;

	// Pagination 
	@track page = 1
	@track startingRecord = 1; //start record position per page
	@track endingRecord = 0; //end record position per page
	@track pageSize = 50; //default value we are assigning
	@track totalRecountCount = 0; //total record count received from all retrieved records
	@track totalPage = 0; //total number of page is needed to display all records
    // Keep track of and maintain selected customer installed base items
	@track selectedCustomerBaseItems = [];
	
	// mass update Activity types & clc 
	@track isEditActivity= false;
	@track isMassEditActivity=false;
	@track  isFinalMsgActivity = false;
	massActivityType;
	massClc;
	clcForActOptions;
	isClc=true;
    // For opportunity deal Type 
	@track isDealType;
	/**
	 * Constructor, adds listener to listen to picklist value changes coming from the custom lightning datatable
	 */
	constructor() {
		super();
		this.addEventListener(
			"onpicklistchanged",
			this.picklistChangeListener.processChange
		);
	}

	/**
	 * Listens to product line item creation based on install base assets
	 */
	@track
	installbaseListener = {
		installbaseLineItemsCreated(event) {
			console.log('inside event');
			event.stopPropagation();
			
			let installedBaseArray = event.detail;
			this.toggleShowInstalledBase();
			this.isMassEdit=true;
			this.isMassDelete=true;
            //return refreshApex(this.shoppingCartResults);
            return refreshApex(this.shoppingCartCPResults);
		}
	};

	/**
	 * Listen to picklist changes for the clc field
	 */
	@track
	picklistChangeListener = {
		processChange(event) {
			event.stopPropagation();
			let dataRecieved = event.detail.data;
			console.log('*****picklist'+JSON.stringify(dataRecieved));
			console.log('*****picklistval'+dataRecieved.value);
			let updatedItem={};
			/*let updatedItem = {
				id: dataRecieved.context,
				clc: dataRecieved.value
			};*/
			if(dataRecieved.value==='Price Change' || dataRecieved.value==='Provide' || dataRecieved.value==='Cease'){
				updatedItem = {
					id: dataRecieved.context,
					activityType: dataRecieved.value
				};
			}else{
				updatedItem = {
					id: dataRecieved.context,
					clc: dataRecieved.value
				};
			}
			console.log('*****updatedItem'+JSON.stringify(updatedItem));
			this.updateDraftValues(updatedItem);
		}
	};

	/**
	 * Updates the value received through the custom field type (clc picklist)
	 * Updating the draft values shows the yellow background color
	 * @param {Object} updateItem
	 */
	updateDraftValues(updateItem) {
		
		//for (let index in this.shownProducts) {
			for (let index in this.shownCPProducts) {
			if (
				Object.prototype.hasOwnProperty.call(this.shownCPProducts, index)
			) {
				let prodLineItem = this.shownCPProducts[index];
				if (prodLineItem.id === updateItem.id) {
					updateItem.Id = "row-" + index;
				}
			}
		}
		let draftValueChanged = false;
		let copyDraftValues = [...this.draftValues];
		copyDraftValues.forEach((item) => {
			if (item.id === updateItem.id) {
				for (let field in updateItem) {
					if (
						Object.prototype.hasOwnProperty.call(updateItem, field)
					) {
						item[field] = updateItem[field];
					}
				}
				draftValueChanged = true;
			}
		});

		if (draftValueChanged) {
			this.draftValues = [...copyDraftValues];
		} else {
			this.draftValues = [...copyDraftValues, updateItem];
		}
	}

	

	/**
	 * Handles the selected action in the data table
	 * @param {Object} event
	 */
	handleRowAction(event) {
		const action = event.detail.action;
		const row = event.detail.row;

		if (action.name === "delete") {
			//console.log('inside delete action');
			this.deleteId = row.id;
			this.isDeleteModal = true;
		}
		/*if (action.name === "copy") {
			this.isLoading = true;
			createProduct({
				jsonString: JSON.stringify(row),
				oppId: this.recordId,
				moreThanOne: false
			})
				.then(() => {
					this.isLoading = false;
					showToast("Success", "Copied Product", "success");
					return refreshApex(this.shoppingCartResults);
				})
				.catch((error) => {
					this.isLoading = false;
					showToast(
						"Error copying record",
						reduceErrors(error).join(", "),
						"error"
					);
				});
		}*/
		if (action.name === "edit") {
			// this.selectedRow = row;
			this.selectedRow = JSON.parse(JSON.stringify(row));
			if (this.selectedRow.activityType === "Cease") {
				this.selectedRow = JSON.parse(JSON.stringify(row));
				this.isCeasesModalOpen = true;
			} else if (this.selectedRow.activityType === "Price Change") {
				this.selectedRow = JSON.parse(JSON.stringify(row));
				this.isPriceModalOpen = true;
			} /*else {
				this.isModalOpen = true;
				this.isValueSelected = true;
				let selectedName = this.selectedRow.productLabel;
				this.selectedName = selectedName;
				this.productId = this.selectedRow.id;
			}*/
		}
	}

	/**
	 * Filter products based on input fields. Filter must at least contain 2 characters to be taken into account
	 * @param {Object} event
	 */
	filterProducts(event) {
		let filter = event.target.value;
		let filterField = event.target.name;

		//Track filters:
		if (filterField === "site") {
			this.siteSearch = filter;
		} else if (filterField === "activityType") {
			this.activityTypeSearch = filter;
		} else {
			this.prodSearch = filter;
		}

		let siteFilter = filterField === "site" ? filter : this.siteSearch;
		let productFilter =
			filterField === "product" ? filter : this.prodSearch;
		let activityTypeFilter =
			filterField === "activityType" ? filter : this.activityTypeSearch;

		//let filteredProducts = this.allProducts; // reset
		let filteredProducts = this.allCPProducts; // reset
		// Filter on site first
		if (siteFilter && siteFilter.length >= 2) {
			filteredProducts = filteredProducts.filter(function (product) {
				let siteLabel =
					Object.prototype.hasOwnProperty.call(
						product,
						"siteLabel"
					) && product.siteLabel
						? product.siteLabel
						: NO_SITE_NAME;
				return siteLabel
					.toLowerCase()
					.includes(siteFilter.toLowerCase());
			});
		}
		// Additional filtering on product
		if (productFilter && productFilter.length >= 2) {
			filteredProducts = filteredProducts.filter(function (product) {
				return product.productLabel
					.toLowerCase()
					.includes(productFilter.toLowerCase());
			});
		}
		// Filter on activity Type
		if (activityTypeFilter && activityTypeFilter.length >= 2) {
			filteredProducts = filteredProducts.filter(function (product) {
				let activityTypeLabel =
					Object.prototype.hasOwnProperty.call(
						product,
						"activityType"
					) && product.activityType
						? product.activityType
						: NO_SITE_NAME;
				return activityTypeLabel
					.toLowerCase()
					.includes(activityTypeFilter.toLowerCase());
			});
		}
		//this.shownProducts = filteredProducts;
		this.shownCPProducts = filteredProducts;
	}

	/**
	 * Toggle visibility of the install base selection modal
	 */
	toggleShowInstalledBase() {
		this.showMainMenu = !this.showMainMenu;
		this.showInstalledBase = !this.showInstalledBase;
	}

	/**
	 * Save the records
	 * @param {Object} event
	 */
	async handleSave(event) {
		const updatedFields = event.detail.draftValues;

		console.log("updatedFields: " + JSON.stringify(updatedFields));

		//Mapping the table values to the sObject fields:
		const mappedRecords = updatedFields.map((row) => {
			let rec = {};
			/*if (Object.prototype.hasOwnProperty.call(row, "listprice"))
				rec[LIST_PRICE_FIELD.fieldApiName] = row.listprice;
			if (Object.prototype.hasOwnProperty.call(row, "quantity"))
				rec[QUANTITY_FIELD.fieldApiName] = row.quantity;
			if (Object.prototype.hasOwnProperty.call(row, "discount"))
				rec[DISCOUNT_FIELD.fieldApiName] = row.discount;
			if (Object.prototype.hasOwnProperty.call(row, "duration"))
				rec[DURATION_FIELD.fieldApiName] = row.duration;
			if (Object.prototype.hasOwnProperty.call(row, "arpuValue"))
				rec[ARPU_FIELD.fieldApiName] = row.arpuValue;*/
			if (Object.prototype.hasOwnProperty.call(row, "clc"))
				rec[CLC_FIELD.fieldApiName] = row.clc;
				if (Object.prototype.hasOwnProperty.call(row, "activityType")){
					rec[ACTIVITYTYPE_FIELD.fieldApiName] = row.activityType;
				}		
			//rec[ID_FIELD.fieldApiName] = this.shownProducts[
				rec[ID_FIELD.fieldApiName] = this.shownCPProducts[
				row.Id.replace("row-", "")
			].id;
			return rec;
		});
		console.log("***********mappedRecords: " + JSON.stringify(mappedRecords));
		//await updateProducts({ data: mappedRecords })
		await updateCPProducts({ data: mappedRecords })
			// eslint-disable-next-line no-unused-vars -- method signature
			.then((result) => {
				console.log(JSON.stringify("Apex update result: " + result));
				showToast("Success", "Order line items updated", "success");

				// Refresh LDS cache and wires
				//getRecordNotifyChange(notifyChangeIds);

				// Display fresh data in the datatable
				refreshApex(this.shoppingCartCPResults).then(() => {
					// Clear all draft values in the datatable
					this.draftValues = [];
				});
			})
			.catch((error) => {
				showToast(
					"Error updating or refreshing records",
					error.body.message,
					"error"
				);
			});
	}

	closeModal() {
		this.isModalOpen = false;
		this.isNewModalOpen = false;
		this.isPriceModalOpen = false;
		this.isCeasesModalOpen = false;
		this.isDeleteModal = false;
		this.isMassDeleteModal = false;
		this.isMassEditModal=false;
		this.isFinalMsgPriceChange=false;
		this.isFinalMsgCease = false;
		this.isFinalMsgActivity=false;
	}

	/**************
	 * New Code Added by Ashok For Price Changes and Ceases Action
	 *
	 */
	/**************
	 * This event assign change value on specific value onchange
	 *
	 */
	changeValues(event) {
		switch (event.target.name) {
			case "Quantity":
				this.selectedRow.quantity = event.target.value;
				break;
			case "Allocation Code":
				this.selectedRow.costCenter = event.target.value;
				break;
			case "Contract Period":
				this.selectedRow.duration = event.target.value;
				break;
			case "PO Number":
				this.selectedRow.poNumber = event.target.value;
				break;
			case "Invoice Start Date":
				this.selectedRow.invoiceDate = event.target.value;
				break;
			case "Price":
				this.selectedRow.listprice = event.target.value;
				break;
			case "No Unify Invoice":
				this.selectedRow.billing = event.target.checked;
				break;
			case "Discount(%)":
				this.selectedRow.discount = event.target.value;
				break;
			case "Administrative End Date":
				this.selectedRow.administrativeEndDate = event.target.value;
				break;
			case "Do no return HW":
				this.selectedRow.doNotReturnHW = event.target.checked;
				break;
			case "Contractual End Date":
				this.selectedRow.contractEndDate = event.target.value;
				break;
			case "Group":
				this.selectedRow.grp = event.target.value;
				break;
			case "CLC":
				this.selectedRow.clc = event.target.value;
				break;
			case "Billing":
				this.selectedRow.billing = event.target.checked;
				break;
			case "Site":
				this.selectedRow.site = event.target.value;
				break;
			case "NewQuantity":
				this.opportunityProdRecord.quantity = event.target.value;
				break;
			case "NewAllocationCode":
				this.opportunityProdRecord.costCenter = event.target.value;
				break;
			case "NewContractPeriod":
				this.opportunityProdRecord.duration = event.target.value;
				break;
			case "NewPrice":
				this.opportunityProdRecord.listprice = event.target.value;
				break;
			case "NewDiscount":
				this.opportunityProdRecord.discount = event.target.value;
				break;
			case "NewCLC":
				this.opportunityProdRecord.clc = event.target.value;
				break;
			case "NewBilling":
				this.opportunityProdRecord.billing = event.target.checked;
				break;
			case "NewSite":
				this.opportunityProdRecord.site = event.target.value;
				break;
				case "NewQuantity":this.opportunityProdRecord.quantity=event.target.value;
				break;
				case "NewAllocationCode":this.opportunityProdRecord.costCenter=event.target.value;
				break;
				case "NewContractPeriod":this.opportunityProdRecord.duration=event.target.value;
				break;
				case "NewPrice":this.opportunityProdRecord.listprice=event.target.value;
				break;
				case "NewDiscount":this.opportunityProdRecord.discount=event.target.value;
				break;
				case "NewCLC":this.opportunityProdRecord.clc=event.target.value;
				break;
				case "NewBilling":this.opportunityProdRecord.billing=event.target.checked;
				break;
				case "NewSite":this.opportunityProdRecord.site=event.target.value;
				break;
				case "AdministrativeEndDate":this.AdministrativeEndDate=event.target.value;
				break;
				case "ContractualEndDate":this.contractEndDate=event.target.value;
				break;
				case "EditPcAllocationCode":this.opportunityEditPcRecord.costCenter=event.target.value;
				break;
				case "EditPcContractPeriod":this.opportunityEditPcRecord.duration=event.target.value;
				break;
				case "EditPcPrice":this.opportunityEditPcRecord.listprice=event.target.value;
				break;
				case "EditPcDiscount":this.opportunityEditPcRecord.discount=event.target.value;
				break;
				case "EditPcCLC":this.opportunityEditPcRecord.clc=event.target.value;
				break;
				case "EditPcBilling":this.opportunityEditPcRecord.billing=event.target.checked;
				break;
				case "EditPcPONumber":this.opportunityEditPcRecord.poNumber=event.target.value;
				break;
				case "EditPcInvoiceStartDate":this.opportunityEditPcRecord.invoiceDate=event.target.value;
				break;
				case "actActivityType":this.massActivityType=event.target.value;
				break;
				case "actCLC":this.massClc=event.target.value;
			

			default:
				break;
		}
	}
	/**
	 * update the records
	 * @param {Wrapper object} event
	 */
	// eslint-disable-next-line no-unused-vars -- method signature
	async handleSavePriceChange(event) {
		
		const allValid = [
			...this.template.querySelectorAll("lightning-input")
		].reduce((validSoFar, inputCmp) => {
			inputCmp.reportValidity();
			return validSoFar && inputCmp.checkValidity();
		}, true);

		if (allValid) {
			this.isLoading = true;

		//	saveData({ item: this.selectedRow })
		updateContractedProducts({ item: this.selectedRow })
				// eslint-disable-next-line no-unused-vars -- method signature
				.then((result) => {
					this.isLoading = false;
					showToast("Success", "Products updated", "success");

					// Display fresh data in the datatable
					//refreshApex(this.shoppingCartResults).then(() => {
				refreshApex(this.shoppingCartCPResults).then(() => {
						// Clear all draft values in the datatable
						this.draftValues = [];
					});

					this.isPriceModalOpen = false;
					this.isCeasesModalOpen = false;
				})
				.catch((error) => {
					this.isLoading = false;
					showToast(
						"Error updating or refreshing records",
						error.body.message,
						"error"
					);
				});
		} else {
			showToast(
				"Required Fields",
				"Please enter required fields",
				"error"
			);
		}
	}

	

	/**
	 * Delete  product line item for the provided id
	 *
	 */

	deleteData(event) {
		this.isLoading = true;
		
		deleteRecord(this.deleteId)
			.then(() => {
				this.isLoading = false;
				this.isDeleteModal = false;
				showToast("Success", "Product deleted", "success");
				return refreshApex(this.shoppingCartCPResults);
			})
			.catch((error) => {
				this.isLoading = false;
				showToast(
					"Error deleting record",
					reduceErrors(error).join(", "),
					"error"
				);
			});
	}

	// Navigate to Detail Opportunity Page -Start
	navigateToViewOpportunityPage() {
		console.log('inisde');
		this[NavigationMixin.Navigate]({
			type: "standard__recordPage",
			attributes: {
				recordId: this.orderId,
				objectApiName: "Order__c",
				actionName: "view"
			}
		});
	}

	// Getting selected rows
	getSelectedRecords(event) {
		// getting selected rows
		const selectedRows = event.detail.selectedRows;
		this.recordsCount = event.detail.selectedRows.length;

		// this set elements the duplicates if any
		let OpportunityLineItemIds = new Set();
         // for mass cease and Provide records
		let OpptyLICeaseIds = new Set();
		let OpptyLIProvideIds = new Set();
		// getting selected record id
		for (let i = 0; i < selectedRows.length; i++) {
			OpportunityLineItemIds.add(selectedRows[i].id);
			if(selectedRows[i].activityType ==='Cease'){
				OpptyLICeaseIds.add(selectedRows[i].id);
				this.recordsCeaseCount++;
			}
			if(selectedRows[i].activityType ==='Price Change'){
				OpptyLIProvideIds.add(selectedRows[i].id);
				this.recordsPriceChangeCount++;
			}
		}
        
		// coverting to array
		// mass cease
		this.selectedCeaseRecords = Array.from(OpptyLICeaseIds);
		if(this.selectedCeaseRecords.length==1){
			this.isSingleCease=true;
			this.isMassCease=false;
			this.isSingleCeaseMsg= true;
			this.isMassEdit=false;
		  
		}else if(this.selectedCeaseRecords.length>1){
			this.isMassCease=true;
			this.isSingleCeaseMsg= false;
			this.isSingleCease=true;
			this.isMassEdit=false;
		}else{
			this.isSingleCease=false;
			this.isSingleCeaseMsg= false;
			this.isMassCease=false;
		}

		 // mass price change
		this.selectedPriceChangeRecords = Array.from(OpptyLIProvideIds);
		if( this.selectedPriceChangeRecords.length>1){
			this.isMassPriceChange=true;
			this.isSinglePriceChangeMsg=false;
			this.isSinglePriceChange=true;
			this.isMassEdit=false;
			this.isMassClone = false;
		}else  if( this.selectedPriceChangeRecords.length==1){
			this.isSinglePriceChange=true;
			this.isMassPriceChange=false;  
			this.isSinglePriceChangeMsg=true;
			this.isMassEdit=false;
			this.isMassClone = true;
		}  else{
			this.isSinglePriceChange=false;
			this.isMassPriceChange=false;
			this.isSinglePriceChangeMsg=false;
			this.isMassClone = true;
		}

		// mass delete or mass activity types
		this.selectedRecords = Array.from(OpportunityLineItemIds);
		
		if (this.recordsCount) {
		
			this.isMassDelete = false;
			this.isEditActivity= true;
			this.isMassEditActivity=true;
		} else {
			this.isMassDelete = true;
			this.isMassClone = true;
			this.isMassEdit= true;
			this.isEditActivity= false;
			this.isMassEditActivity=false;
		}
		// checkbox
	/*	let selectList = JSON.parse(
			JSON.stringify(this.shownCPProducts)
		); // deep copy

		const tempList = this.shownCPProducts;

		selectList = selectList.filter(function (selectedRow) {
			let found = false;
			tempList.forEach((visibleRow) => {
				if (visibleRow.Id == selectedRow.Id) {
					found = true;
				}
			});
			return found;
		});
		this.selectedCustomerBaseItems = selectList.map((x) => x.Id);*/
	}

	// delete records process function
	deleteDataProd() {
		//console.log("inside mass delete");
		this.isMassDeleteModal = true;
		if (this.recordsCount === 1) {
			this.isSingle = true;
		} else {
			this.isSingle = false;
		}

		if (this.selectedRecords) {
			this.isMassDelete = false;
		}
		
	}

	// delete records Method
	deleteCP() {
		this.isLoading = true;
	deleteContractedProducts({ lstProdIds: this.selectedRecords })
			// eslint-disable-next-line no-unused-vars -- method signature
			.then((result) => {
				this.isTrue = false;
				this.isMassDeleteModal = false;
				this.isLoading = false;
				this.isMassClone = true;
				this.isMassDelete = true;
				this.isMassEdit = true;
				// showing success message
				if (this.recordsCount === 1) {
					showToast("Success", "Product deleted", "success");
				} else {
					showToast(
						"Success!!",
						this.recordsCount + " Products has been deleted.",
						"success",
						"dismissable"
					);
				}

				this.recordsCount = 0;
				// refreshing table data using refresh apex
				return refreshApex(this.shoppingCartCPResults);
			})
			.catch((error) => {
				showToast(
					"Error while deleting Products",
					error.message,
					"error",
					"dismissable"
				);
			});
	}


	 //mass Actions 
	 massActions(){
		this.isMassEditModal=true;
		console.log('******  this.isMassCease'+  this.isMassCease);
		}
		
		// mass Cease 
		massCeasesActions() {
       
			const allValid = [...this.template.querySelectorAll('lightning-input')]
			   .reduce((validSoFar, inputCmp) => {
						   inputCmp.reportValidity();
						   return validSoFar && inputCmp.checkValidity();
			   }, true);
	
			   if (allValid) {
			   this.isLoading = true;
			  massCeaseContractProducts({contractedProdId: this.selectedCeaseRecords,administrativeEndDate: this.AdministrativeEndDate,contractEndDate: this.contractEndDate})
			   .then(result => {
				   
				   this.isLoading = false;
				   this.isMassClone = true;
				   this.isMassDelete = true;
				   this.isMassCease=false;
				   this.isMassEdit= true;
				   this.isFinalMsgCease = true;
				   // showing success message
				   
					   showToast(
						   'Success!!', 
						   'Opportunity line items updated.', 
						   'success'
					   );
				  this.selectedCeaseRecords=[];
				   // refreshing table data using refresh apex
				return refreshApex(this.shoppingCartCPResults);
	   
			   })
			   .catch(error => {
				   this.isLoading = false;
				   console.log(error);
				   
					   showToast(
						   'Error while updating Products', 
						   error.message, 
						   'error'
					   );
				   
			   });
			}else{
				showToast(
					'Required Fields',
					'Please enter required fields',
					'error',
				   'dismissable'
				);
				
	
			}
		} 
		
	// this method get all the records that displaying on table
		@wire(queryShoppingCartForCP,{
            orderId:'$orderId'
        })
        queryShoppingCartForCP(shoppingCartResult) {

            this.shoppingCartCPResults = shoppingCartResult; // Needed for the refresh apex to work
            const {data, error} = shoppingCartResult;

            if (data) {
                
                this.allCPProducts = data.cartItems;
                 
              //  this.shownCPProducts = data.cartItems;
                 
                this.opportunity = data.oppty;
                this.opptyAccount = data.accnt;
				this.opptyLoaded = true;
				this.recordId = data.oppty.Id;
				console.log('*****this.opportunity dealer '+this.opportunity.Deal_Type__c);
				this.isDealType =this.opportunity.Deal_Type__c;
				// pagination code 
			let total=0;
			for (let index in this.allCPProducts) {
				total ++;
			
				}
				
			//this.totalRecountCount = data.length; //here it is 
			this.totalRecountCount = total;
			
            this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); //here it is 5
            //initial data to be displayed
            this.shownCPProducts = this.allCPProducts.slice(0,this.pageSize); 
            this.endingRecord = this.pageSize;

				
            }
			this.isLoading = false;
		// Need to remove after activity type to be merge with picklist component
		if(this.isDealType==="New" || this.isDealType==="Acquisition" || this.isDealType==="Cancellation"){
			this.activityOptions = [
				{ value: "Cease", label: "Cease" }
			];
		}else{
		this.activityOptions = [
			{ value: "Cease", label: "Cease" },
			{ value: "Price Change", label: "Price Change" }
		];
	}
		this.clcOptions = [
			{ value: "Acq", label: "Acq" },
			{ value: "Ret", label: "Ret" },
			{ value: "Churn", label: "Churn" },
			{ value: "Mig", label: "Mig" }
		];
			
		this.clcForPCOptions= [
			{ value: "Ret", label: "Ret" },
			{ value: "Mig", label: "Mig" }
		];
		this.activitySearchOptions  = [
			{ value: "", label: "All" },
			{ value: "Cease", label: "Cease" },
			{ value: "Price Change", label: "Price Change" }
		];

   // this.checkSelectedRows();
 
		}      
		
	//Sorting 
	handleSortdata(event) {
		
		// field name
		this.sortBy = event.detail.fieldName;
        console.log('sortBy'+this.sortBy);  
		// sort direction
		this.sortDirection = event.detail.sortDirection;
		console.log('sortDirection'+this.sortDirection);  
		// calling sortdata function to sort the data based on direction and selected field
		this.sortData(event.detail.fieldName, event.detail.sortDirection);
	}

	sortData(fieldname, direction) {
		
		// serialize the data before calling sort function
		let parseData = JSON.parse(JSON.stringify(this.shownCPProducts));

		// Return the value stored in the field
		let keyValue = (a) => {
			return a[fieldname];
		};

		// cheking reverse direction
		let isReverse = direction === "asc" ? 1 : -1;
		console.log('isReverse'+isReverse);  
		// sorting data
		parseData.sort((x, y) => {
			x = keyValue(x) ? keyValue(x) : ""; // handling null values
			y = keyValue(y) ? keyValue(y) : "";

			// sorting values based on direction
			return isReverse * ((x > y) - (y > x));
		});
		console.log('isReverse'+isReverse);  
		// set the sorted data to data table data
		this.shownCPProducts = parseData;
	} 	
	// Pagination 
	 //clicking on previous button this method will be called
	 previousHandler() {
		
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
    }

    //clicking on next button this method will be called
    nextHandler() {
		
        if((this.page<this.totalPage) && this.page !== this.totalPage){
		
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);            
        }             
	}
	  //this method displays records page by page
	  displayRecordPerPage(page){
		
        /*let's say for 2nd page, it will be => "Displaying 6 to 10 of 23 records. Page 2 of 5"
        page = 2; pageSize = 5; startingRecord = 5, endingRecord = 10
        so, slice(5,10) will give 5th to 9th records.
        */
        this.startingRecord = ((page -1) * this.pageSize) ;
        this.endingRecord = (this.pageSize * page);

        this.endingRecord = (this.endingRecord > this.totalRecountCount) 
                            ? this.totalRecountCount : this.endingRecord; 

        this.shownCPProducts = this.allCPProducts.slice(this.startingRecord, this.endingRecord);

        //increment by 1 to display the startingRecord count, 
        //so for 2nd page, it will show "Displaying 6 to 10 of 23 records. Page 2 of 5"
		this.startingRecord = this.startingRecord + 1;
	//	this.checkSelectedRows();
	}   
	
	/**
	 * Check the checkboxes for the visible rows if selected before
	 */
	checkSelectedRows() {
		// Make sure the checkboxes are ticket:
		let selectList = JSON.parse(
			JSON.stringify(this.shownCPProducts)
		); // deep copy

		const tempList = this.shownCPProducts;

		selectList = selectList.filter(function (selectedRow) {
			let found = false;
			tempList.forEach((visibleRow) => {
				if (visibleRow.Id == selectedRow.Id) {
					found = true;
				}
			});
			return found;
		});
		this.selectedCustomerBaseItems = selectList.map((x) => x.Id);
	}
// Mass Price change method 
async massPriceChangeData(){
			
		
	if(!this.opportunityEditPcRecord.listprice){
		this.opportunityEditPcRecord.listprice=-1;
	}
	if(!this.opportunityEditPcRecord.discount){
		this.opportunityEditPcRecord.discount=-1;
	}
	if(!this.opportunityEditPcRecord.duration){
		this.opportunityEditPcRecord.duration=-1;
	}
	if(!this.opportunityEditPcRecord.invoiceDate){
		this.opportunityEditPcRecord.invoiceDate=null;
	}
   
	this.isLoading = true;
	massPriceChangeCntrctProducts({contractedProductsId:this.selectedPriceChangeRecords ,item: this.opportunityEditPcRecord})
	.then(result => {
		
		this.isLoading = false;
		this.isMassClone = true;
		this.isMassDelete = true;
		this.isMassPriceChange=false;
		this.isMassEdit= true;
		this.isFinalMsgPriceChange= true;
		
		// showing success message
		
			showToast(
				'Success!!', 
				' Products are updated.', 
				'success'
			);
		
		this.opportunityEditPcRecord = {
			clc: "",
			listprice: '',
			discount: '',
			duration: '',
			costCenter: "",
			billing: true,
			 poNumber:"",
		   invoiceDate:""
		};
		this.selectedPriceChangeRecords = [];
		// refreshing table data using refresh apex
		 return refreshApex(this.shoppingCartCPResults);

	})
	.catch(error => {
		this.isLoading = false;
		console.log(error);
		
			showToast(
				'Error while getting Products', 
				 error.message, 
				'error'
			);
		
	});
}  
	
changeActivityValues(event){
	this.isClc=true;
 let activityName =event.target.name;
 let activityvalues = event.target.value;
 
	
		if(activityvalues==='Price Change'){
			this.isClc=false;
			this.massActivityType=event.target.value
			this.clcForActOptions = [
				{ value: "Ret", label: "Ret" },
				{ value: "Mig", label: "Mig" }
			];
			
		}
		if(activityvalues==='Cease'){
			this.isClc=false;
			this.massActivityType=event.target.value
			this.clcForActOptions = [
				{ value: "Churn", label: "Churn" }
			];
		}
 }

 massActivityActions(event){

	this.isLoading = true;
   if(!this.massActivityType){
	   this.isLoading = false;
	   showToast(
		   'Required Field',
		   'Please select Activity Type field',
		   'error',
		  'dismissable'
	   );
	   
   }else if(!this.massClc){
	   this.isLoading = false;
	   showToast(
		   'Required Field',
		   'Please select CLC field',
		   'error',
		  'dismissable'
	   );	
   } 
   if(this.massActivityType && this.massClc){
massActivityContractProducts({contractedProductsId: this.selectedRecords,massActivityType: this.massActivityType,massClc: this.massClc})
	   .then(result => {
		  
		   this.isLoading = false;
		   this.isMassClone = true;
		   this.isMassDelete = true;
		   this.isMassCease=false;
		   this.isMassEdit= true;
		   this.isFinalMsgActivity = true;
		   this.isMassEditActivity=false;
		   this.massActivityType="";
		   this.massClc="";
		   this.isClc=true;
		   // showing success message
		   
			   showToast(
				   'Success!!', 
				   'Opportunity line items updated.', 
				   'success'
			   );
		   
		  this.selectedRecords=[];
		   // refreshing table data using refresh apex
			return refreshApex(this.shoppingCartCPResults);

	   })
	   .catch(error => {
		   this.isLoading = false;
		   console.log(error);
		   
			   showToast(
				   'Error while updating Products', 
				   error.message, 
				   'error'
			   );
		   
	   });

   }
}

}