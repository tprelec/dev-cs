import { LightningElement, api, wire, track } from "lwc";
import { refreshApex } from "@salesforce/apex";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import basketContractIsCreated from "@salesforce/apex/CreateMobileInformationController.basketContractIsCreated";
import getOpportunity from "@salesforce/apex/CreateMobileInformationController.getOpportunity";
import save from "@salesforce/apex/CreateMobileInformationController.save";
import getNetProfitCtn from "@salesforce/apex/CreateMobileInformationController.getNetProfitCtn";
import importCSV from "@salesforce/apex/CreateMobileInformationController.importCSV";
import createNumberListPDF from "@salesforce/apex/CreateMobileInformationController.createNumberListPDF";
import getQuoteProfiles from "@salesforce/apex/CreateMobileInformationController.getQuoteProfiles";
import styleOverride from "@salesforce/resourceUrl/createMobileInformationStyle";
import { reduceErrors } from "c/ldsUtils";
import { loadStyle } from "lightning/platformResourceLoader";

export default class CreateMobileInformation extends LightningElement {
	fileData;
	csvSeparator = ";";
	error;
	sortedBy = "Quote_Profile__c";
	sortedDirection = "asc";
	@api recordId;
	@track netProfitCtns;
	@track dummy = [];
	@track draftValues = [];
	@track quoteProfiles;
	@track quoteProfilesDummy;
	@track doneLoading = false;

	@wire(basketContractIsCreated, { oppId: "$recordId" }) contractCreated;
	@wire(getOpportunity, { oppId: "$recordId" }) theOpp;
	@wire(getNetProfitCtn, {
		oppId: "$recordId",
		sortField: this != undefined ? this.sortedBy : "Quote_Profile__c",
		sortDirection: this != undefined ? this.sortedDirection : "asc"
	})
	wiredCtns(result) {
		this.dummy = result;
		if (result.data) {
			this.netProfitCtns = result.data;
			this.error = undefined;
		} else if (result.error) {
			this.error = result.error;
			this.toastError(reduceErrors(this.error)[0]);
		}
	}

	@wire(getQuoteProfiles, { oppId: "$recordId" })
	wiredQuoteProfiles(result) {
		this.quoteProfilesDummy = result;
		if (result.data) {
			this.quoteProfiles = result.data;
			this.error = undefined;
		} else if (result.error) {
			this.error = result.error;
			this.toastError(reduceErrors(this.error)[0]);
		}
	}

	renderedCallback() {
		Promise.all([loadStyle(this, styleOverride)]);
	}

	@track fieldToLabel = {
		Id: "Id",
		"Model Number": "Quote_Profile__c",
		"Product Name": "Price_Plan_Description__c",
		"Discount %": "Discount__c",
		Duration: "Duration__c",
		"BAP SAP": "Gross_List_Price__c",
		"Quantity Type": "Product_Quantity_type__c",
		"Deal Type": "Action__c",
		CTN: "CTN_Number__c",
		"Simcard Number VF": "Simcard_number_VF__c",
		"Sharing Yes/No": "Sharing__c",
		"Data Limit": "Data_Limit__c",
		"Data Blocking Yes/No": "dataBlock__c",
		"Contract Number Current Provider": "Contract_Number_Current_Provider__c",
		"Current Provider Name": "Current_Provider_name__c",
		"Service Provider Name": "Service_Provider_Name__c",
		"Billing Arrangement": "Billing_Arrangement__c",
		"Porting Wishdate (yyyy-mm-dd)": "Contract_Enddate__c"
	};
	@track columns = [
		{
			label: "Model Number",
			fieldName: "Quote_Profile__c",
			type: "text",
			sortable: true
		} /**Model Number 1 **/,
		{
			label: "Product Name",
			fieldName: "Price_Plan_Description__c",
			type: "text",
			sortable: true
		} /**Product Name 2 **/,
		{
			label: "Discount %",
			fieldName: "Discount__c",
			type: "percent",
			sortable: true,
			typeAttributes: {
				maximumFractionDigits: "3"
			}
		} /**Discount 3 **/,
		{
			label: "Duration",
			fieldName: "Duration__c",
			type: "number",
			sortable: true
		} /**Duration 4 **/,
		{
			label: "BAP SAP",
			fieldName: "Gross_List_Price__c",
			type: "currency",
			sortable: true
		} /**BAP SAP 5 **/,
		{
			label: "Quantity Type",
			fieldName: "Product_Quantity_type__c",
			type: "text",
			sortable: true
		} /**Quantity Type 6 **/,
		{
			label: "Deal Type",
			fieldName: "Action__c",
			type: "text",
			sortable: true
		} /**Deal Type 7 **/,
		{
			label: "CTN",
			fieldName: "CTN_Number__c",
			type: "text",
			sortable: true,
			editable: true
		} /**CTN 8 **/,
		{
			label: "Simcard Number VF",
			fieldName: "Simcard_number_VF__c",
			type: "text",
			sortable: true,
			editable: true
		} /**Simcard number Vodafone 9 **/,
		{
			label: "Sharing Yes/No",
			fieldName: "Sharing__c",
			type: "picklist",
			sortable: true,
			editable: true,
			typeAttributes: {
				options: [
					{ label: "Yes", value: "Yes" },
					{ label: "No", value: "No" }
				],
				value: { fieldName: "Sharing__c" },
				context: { fieldName: "Id" },
				fieldname: "Sharing__c"
			}
		} /**Sharing 10**/ /****/,
		{
			label: "Data Limit",
			fieldName: "Data_Limit__c",
			type: "picklist",
			sortable: true,
			editable: true,
			typeAttributes: {
				options: [
					{ label: "50", value: "50" },
					{ label: "75", value: "75" },
					{ label: "100", value: "100" }
				],
				value: { fieldName: "Data_Limit__c" },
				context: { fieldName: "Id" },
				fieldname: "Data_Limit__c"
			}
		} /**Data Limit 11**/,
		{
			label: "Data Blocking Yes/No",
			fieldName: "dataBlock__c",
			type: "picklist",
			sortable: true,
			editable: true,
			typeAttributes: {
				options: [
					{ label: "Yes", value: "Yes" },
					{ label: "No", value: "No" }
				],
				value: { fieldName: "dataBlock__c" },
				context: { fieldName: "Id" },
				fieldname: "dataBlock__c"
			}
		} /**Data Blocking 12**/,
		{
			label: "Contract Number Current Provider",
			fieldName: "Contract_Number_Current_Provider__c",
			type: "text",
			sortable: true,
			editable: true
		} /**Contract Number Current Provider 13**/,
		{
			label: "Current Provider Name",
			fieldName: "Current_Provider_name__c",
			type: "text",
			sortable: true,
			editable: true
		} /**Current Provider Name 14**/,
		{
			label: "Service Provider Name",
			fieldName: "Service_Provider_Name__c",
			type: "text",
			sortable: true,
			editable: true
		} /**Service Provider Name 15**/,
		{
			label: "Billing Arrangement",
			fieldName: "Billing_Arrangement__c",
			type: "text",
			sortable: true,
			editable: true
		} /**Billing Arrangement 16**/,
		{
			label: "Porting Wishdate",
			fieldName: "Contract_Enddate__c",
			type: "date",
			sortable: true,
			editable: true
		} /**Wish Date 17**/
	];

	get options() {
		return [
			{ label: ";", value: ";" },
			{ label: ",", value: "," }
		];
	}

	handleSeparatorChange(event) {
		this.csvSeparator = event.detail.value;
	}

	doSave(event) {
		this.draftValues = event.detail.draftValues;
		save({
			original: this.netProfitCtns,
			valuesToSaveList: event.detail.draftValues,
			oppId: this.recordId
		})
			.then((result) => {
				let title = `Values saved successfully!!`;
				this.toastSucces(title);
				this.draftValues = null;
			})
			.catch((error) => {
				this.toastError(reduceErrors(error)[0]);
			})
			.then((result) => {
				this.refresh();
			});
	}

	refresh() {
		refreshApex(this.dummy);
		refreshApex(this.npInfo);
		this.doneLoading = true;
	}

	exportToCSV() {
		let rowEnd = "\n";
		let csvString = "";

		let rowData = new Set([
			"Id",
			"Model Number",
			"Product Name",
			"Discount %",
			"Duration",
			"BAP SAP",
			"Quantity Type",
			"Deal Type",
			"CTN",
			"Simcard Number VF",
			"Sharing Yes/No",
			"Data Limit",
			"Data Blocking Yes/No",
			"Contract Number Current Provider",
			"Current Provider Name",
			"Service Provider Name",
			"Billing Arrangement",
			"Porting Wishdate (yyyy-mm-dd)"
		]);

		// Array.from() method returns an Array object from any object with a length property or an iterable object.
		rowData = Array.from(rowData);

		// splitting using ','
		csvString += rowData.join(this.csvSeparator);
		csvString += rowEnd;

		// main for loop to get the data based on key value
		for (let i = 0; i < this.netProfitCtns.length; i++) {
			let colValue = 0;

			// validating keys in data
			for (let key in rowData) {
				if (rowData.hasOwnProperty(key)) {
					// Key value
					// Ex: Id, Name
					let rowKey = rowData[key];
					if (colValue > 0) {
						csvString += this.csvSeparator;
					}
					let value =
						this.netProfitCtns[i][this.fieldToLabel[rowKey]] === undefined
							? ""
							: this.netProfitCtns[i][this.fieldToLabel[rowKey]];
					if (rowKey == "Discount %" && value != "") {
						value = (value * 100).toFixed(2);
					}
					if (
						(rowKey == "Simcard Number VF" ||
							rowKey == "Contract Number Current Provider") &&
						value != ""
					) {
						value = "\t" + value;
					}
					if (rowKey == "Porting Wishdate (yyyy-mm-dd)" && value != "") {
						value = "\t" + this.formatDate(value);
					}
					if (rowKey == "") {
					}
					csvString += value;
					colValue++;
				}
			}
			csvString += rowEnd;
		}

		// Creating anchor element to download
		let downloadElement = document.createElement("a");

		// This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
		downloadElement.href = "data:text/csv;charset=utf-8," + encodeURI(csvString);
		downloadElement.target = "_self";
		// CSV File Name
		downloadElement.download = "NetProfitCTN.csv";
		// below statement is required if you are using firefox browser
		document.body.appendChild(downloadElement);
		// click() Javascript function to download CSV file
		downloadElement.click();
	}

	formatDate(date) {
		var d = new Date(date),
			month = "" + (d.getMonth() + 1),
			day = "" + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) {
			month = "0" + month;
		}
		if (day.length < 2) {
			day = "0" + day;
		}
		let returnDate = [year, month, day].join("-");
		return returnDate;
	}

	importCSV(event) {
		const file = event.target.files[0];
		var reader = new FileReader();
		reader.onload = () => {
			var base64 = reader.result.split(",")[1];
			this.fileData = {
				filename: file.name,
				base64: base64,
				recordId: this.recordId,
				separator: this.csvSeparator
			};
		};
		reader.readAsDataURL(file);
	}

	saveCSV() {
		//fail check added when there's no file added
		if (this.fileData) {
			const { base64, filename, recordId, separator } = this.fileData;
			importCSV({ base64, filename, recordId, separator })
				.then((result) => {
					this.fileData = null;
					let title = `${filename} uploaded successfully!!`;
					this.toastSucces(title);
					this.refresh();
				})
				.then(this.refresh())
				.catch((error) => {
					this.toastError(reduceErrors(error)[0]);
				});
		} else {
			this.toastError("Please select a file to import");
		}
	}

	createNumberList() {
		createNumberListPDF({ oppId: this.recordId, ctns: this.netProfitCtns })
			.then((result) => {
				let title = `Numberlist created successfully!!`;
				this.toastSucces(title);
				location.reload();
			})
			.catch((error) => {
				this.toastError(reduceErrors(error)[0]);
			});
	}

	toastSucces(title) {
		const toastEvent = new ShowToastEvent({
			title,
			variant: "success"
		});
		this.dispatchEvent(toastEvent);
	}

	toastError(title) {
		const toastEvent = new ShowToastEvent({
			title,
			variant: "error"
		});
		this.dispatchEvent(toastEvent);
	}

	updateColumnSorting(event) {
		this.sortedBy = event.detail.fieldName;
		this.sortedDirection = event.detail.sortDirection;

		getNetProfitCtn({
			oppId: this.recordId,
			sortField: this.sortedBy,
			sortDirection: this.sortedDirection
		})
			.then((result) => {
				this.netProfitCtns = result;
				this.error = undefined;
			})
			.catch((error) => {
				this.error = error;
				this.contacts = undefined;
				this.toastError(reduceErrors(this.error)[0]);
			});
	}

	updateDraftValues(updateItem) {
		if (this.draftValues == null) {
			this.draftValues = [];
		}
		let draftValueChanged = false;
		let copyDraftValues = [...this.draftValues];
		//store changed value to do operations
		//on save. This will enable inline editing &
		//show standard cancel & save button
		copyDraftValues.forEach((item) => {
			if (item.Id === updateItem.Id) {
				for (let field in updateItem) {
					item[field] = updateItem[field];
				}
				draftValueChanged = true;
			}
		});

		if (draftValueChanged) {
			this.draftValues = [...copyDraftValues];
		} else {
			this.draftValues = [...copyDraftValues, updateItem];
		}
	}

	//listener handler to get the context and data
	//updates datatable
	picklistChanged(event) {
		event.stopPropagation();
		let dataRecieved = event.detail.data;

		if (event.detail.data.fieldname == "Sharing__c") {
			let updateItem = {
				Id: dataRecieved.context,
				Sharing__c: dataRecieved.value
			};
			this.updateDraftValues(updateItem);
		}
		if (event.detail.data.fieldname == "Data_Limit__c") {
			let updateItem = {
				Id: dataRecieved.context,
				Data_Limit__c: dataRecieved.value
			};
			this.updateDraftValues(updateItem);
		}
		if (event.detail.data.fieldname == "dataBlock__c") {
			let updateItem = {
				Id: dataRecieved.context,
				dataBlock__c: dataRecieved.value
			};
			this.updateDraftValues(updateItem);
		}
	}

	//handler to handle cell changes & update values in draft values
	handleCellChange(event) {
		this.updateDraftValues(event.detail.draftValues[0]);
	}
}