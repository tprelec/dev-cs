import { LightningElement, api, track } from "lwc";

import getPicklistOptions from "@salesforce/apex/OpportunityProductEditorLWCController.getPicklistOptions";
import { showToast } from "c/ldsUtils";

export default class CustomDatatablePicklist extends LightningElement {
	@api label;
	@api placeholder;
	//@api value;
	@api context;

	sObjectName;
	fieldName;

	@track
	visibleValue;

	picklistOptions;

	sObjectFieldCombi;

	@api
	get value() {
		return this.visibleValue;
	}
	set value(value) {
		this.visibleValue = value;
	}

	get providersobjectfieldcombi() {
		return this.sObjectFieldCombi;
	}
	@api
	set providersobjectfieldcombi(value) {
		let splitValues = value.split(":");
		this.sObjectName = splitValues[0];
		this.fieldName = splitValues[1];
		this.sObjectFieldCombi = value;
		this.loadPicklistValues();
	}

	@track showValue = true;

	handleChange(event) {
		if (this.value !== event.detail.value) {
			// Only if it changed
			this.value = event.detail.value;

			//fire event to send context and selected value to the data table
			this.dispatchEvent(
				new CustomEvent("picklistchanged", {
					composed: true,
					bubbles: true,
					cancelable: true,
					detail: {
						data: {
							context: this.context,
							value: event.detail.value
						}
					}
				})
			);
		}
		this.hideEdit();
	}

	hideEdit() {
		this.showValue = true;
	}

	showEdit() {
		this.showValue = false;
	}

	async loadPicklistValues() {
		await getPicklistOptions({
			sObjectType: this.sObjectName,
			fieldName: this.fieldName
		})
			.then((result) => {
				if (result) {
					this.picklistOptions = result;
				}
			})
			.catch((error) => {
				showToast(
					"Error getting picklist values",
					error.body.message,
					"error"
				);
			});
	}
}