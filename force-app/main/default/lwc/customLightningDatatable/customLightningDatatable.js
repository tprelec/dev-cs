import LightningDatatable from "lightning/datatable";
//import the template so that it can be reused
import customLightningDatatable from "./customLightningDatatable.html";
//import { loadStyle } from 'lightning/platformResourceLoader';
//import CustomDataTableResource from '@salesforce/resourceUrl/CustomDataTable';

export default class CustomLightningDatatable extends LightningDatatable {
	static customTypes = {
		picklist: {
			template: customLightningDatatable,
			standardCellLayout: false,
			typeAttributes: [
				"label",
				"placeholder",
				"options",
				"value",
				"context",
				"providerSObjectFieldCombi"
			]
		}
	};
}