import { LightningElement, api, track } from 'lwc';

export default class DatatablePicklist extends LightningElement {
    @api label;
    @api placeholder;
    @api options;
    @api value;
    @api context;
    @api fieldname;
    @api theField;

    handleChange(event) {
        //show the selected value on UI
        this.value = event.detail.value;

        console.log(JSON.stringify('this.label' + this.label));
        console.log(JSON.stringify('this.placeholder' + this.placeholder));
        console.log(JSON.stringify('this.options' + this.options));
        console.log(JSON.stringify('this.value' + this.value));
        console.log(JSON.stringify('this.context' + this.context));
        console.log(JSON.stringify('this.fieldname' + this.fieldname));
        console.log(JSON.stringify('this.theField' + this.theField));
        //fire event to send context and selected value to the data table
        this.dispatchEvent(new CustomEvent('picklistchanged', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                data: { context: this.context, value: this.value, fieldname: this.fieldname}
            }
        }));
    }

}