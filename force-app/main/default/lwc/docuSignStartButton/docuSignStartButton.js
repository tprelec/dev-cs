import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import prepareOpportunityAndGetURL from '@salesforce/apex/DocuSignButtonController.prepareOpportunityAndGetURL';

export default class DocuSignStartButton extends NavigationMixin(LightningElement) {
  @api recordId;
  @api buttonName
  @track errors = [];
  @track isLoading = false;

  handleClick() {
    this.isLoading = true;
    console.log('this.recordId: ' + this.recordId);

    prepareOpportunityAndGetURL({ inputOpportunityId: this.recordId })
    .then(result => {
      const refToDocusignPage = result;
      console.log('refToDocusignPage: ' + refToDocusignPage);

      this[NavigationMixin.GenerateUrl]({
        type: 'standard__webPage',
        attributes: {
          url: refToDocusignPage
        }
      }).then(generatedUrl => {
        window.open(generatedUrl);
      });
      this.isLoading = false;
    })
    .catch(error => {
      const errorToAdd = JSON.parse(JSON.stringify(error)); // Because the original is immutable
      errorToAdd.index = this.errors.length.toString();
      this.errors = [...this.errors, errorToAdd];

      console.log('this.errors: ' + JSON.stringify(this.errors));
      const evt = new ShowToastEvent({
        title: 'Application Error',
        message: error.body.message,
        variant: 'error',
        mode: 'dismissable'
      });
      this.dispatchEvent(evt);
      this.isLoading = false;
    });
  }
}