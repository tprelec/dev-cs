import { LightningElement, api, track, wire } from "lwc";
import getFWATempFromAPI from "@salesforce/apex/EMP_AddmobileProductController.getFWATempFromAPI";
import createOLI from "@salesforce/apex/EMP_AddmobileProductController.createOLI";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getRecord, getRecordNotifyChange } from "lightning/uiRecordApi";
import STAGE_FIELD from "@salesforce/schema/Opportunity.StageName";
import JOURNEY_FIELD from "@salesforce/schema/Opportunity.Select_Journey__c";
import BAN_NUMBER from "@salesforce/schema/Opportunity.BAN__r.Name";
import { loadStyle } from "lightning/platformResourceLoader";
import WrappedHeaderTable from "@salesforce/resourceUrl/WrappedHeaderTable";
//Labels section
import errorNoAddMobileJourney from "@salesforce/label/c.EMP_ErrorNoAddMobile";
import errorCloseWonOpp from "@salesforce/label/c.EMP_ErrorCloseWonOpp";
import disclaimerEmailtoTES from "@salesforce/label/c.EMP_DisclaimerEmailTES";
import refreshCalloutMessage from "@salesforce/label/c.EMP_RefreshCalloutConfirmMessage";

export default class Emp_AddMobileProductByFWATemplate extends LightningElement {
	@track wiredApexData;
	mapAllSelection = new Map();
	isSearch = false;
	@api recordId;
	@api isQuickAction = false;
	showSpinner = false;
	//new for display in RecordPage
	@api isRecordPage = false;
	//new for pagination
	@track data = [];
	recordId;
	pageNumber = 1;
	pageSize = 10;
	resultSize = 0;
	selection = [];
	selectedRows = [];
	dataHasChanged;
	initialLoad = true;
	//record count
	recordFrom = 0;
	recordTo = 0;
	//for filter
	timerId;
	searchKey;
	//to inline edit
	draftValues = [];
	//for pagination coming from the API
	allRecords;
	filteredRecords;
	totalPages = 0;
	//for wired call
	isError = false;
	//api loaded check
	isCallingApi = true;
	//error checking
	message = "";
	//label object
	label = {
		errorNoAddMobileJourney,
		errorCloseWonOpp,
		disclaimerEmailtoTES,
		refreshCalloutMessage
	};
	//object used to stencil a table
	tableLst = [
		{
			id: 1,
			opacity: "opacity : 1; border-bottom: 1px solid rgb(211,211,211);"
		},
		{
			id: 2,
			opacity: "opacity : 0.9; border-bottom: 1px solid rgb(211,211,211,0.9);"
		},
		{
			id: 3,
			opacity: "opacity : 0.8; border-bottom: 1px solid rgb(211,211,211,0.8);"
		},
		{
			id: 4,
			opacity: "opacity : 0.7; border-bottom: 1px solid rgb(211,211,211,0.7);"
		},
		{
			id: 5,
			opacity: "opacity : 0.6; border-bottom: 1px solid rgb(211,211,211,0.6);"
		},
		{
			id: 6,
			opacity: "opacity : 0.5; border-bottom: 1px solid rgb(211,211,211,0.5);"
		},
		{
			id: 7,
			opacity: "opacity : 0.4; border-bottom: 1px solid rgb(211,211,211,0.4);"
		},
		{
			id: 8,
			opacity: "opacity : 0.3; border-bottom: 1px solid rgb(211,211,211,0.3);"
		},
		{
			id: 9,
			opacity: "opacity : 0.2; border-bottom: 1px solid rgb(211,211,211,0.2);"
		},
		{
			id: 10,
			opacity: "opacity : 0.1; border-bottom: 1px solid rgb(211,211,211,0.1);"
		}
	];

	columns = [
		{
			label: "Contract Number",
			fieldName: "contractNumber",
			type: "text",

			sortable: false,
			wrapText: true
		},
		{
			label: "Template Id",
			fieldName: "templateId",
			type: "text",

			sortable: false,
			wrapText: true
		},
		{
			label: "Description",
			fieldName: "templateDescription",
			type: "text",

			sortable: false,
			wrapText: true
		},
		{
			label: "Base Plan Name",
			fieldName: "basePlanName",
			type: "text",

			sortable: false,
			wrapText: true
		},
		{
			label: "Resource Category",
			fieldName: "resourceCategory",
			type: "text",
			sortable: false,
			wrapText: true
		},
		{
			label: "Quantity Acquisition",
			fieldName: "quantityAcquisition",
			type: "number",
			editable: true,
			sortable: false,
			wrapText: true
		},
		{
			label: "Quantity Porting",
			fieldName: "quantityPortin",
			type: "number",
			editable: true,
			sortable: false,
			wrapText: true
		},
		{
			label: "Quantity Retention",
			fieldName: "quantityRetention",
			type: "number",
			editable: true,
			sortable: false,
			wrapText: true
		}
	];
	opp;
	@track originalMessage;
	@track isDialogVisible = false;
	handleOpen() {
		//it can be set dynamically based on your logic
		this.originalMessage =
			"Confirmation Dialog to allow the SAM & BP User to choose if wants to re-invoke callout to Unify";
		//shows the component
		this.isDialogVisible = true;
	}

	handleClick(event) {
		if (event.target && event.target.name) {
			if (event.target.name === "confirmModal") {
				//when user clicks outside of the dialog area, the event is dispatched with detail value  as 1
				if (event.detail !== 1) {
					//you can do some custom logic here based on your scenario
					if (event.detail.status === "confirm") {
						//do something
						this.dorefresh();
					}
				}
				//hides the component
				this.isDialogVisible = false;
			}
		}
	}

	dorefresh() {
		this.isError = false;
		this.isCallingApi = true;
		this.draftValues = [];
		this.selection = [];
		this.selectedRows = [];
		this.allRecords = [];
		this.filteredRecords = [];
		getRecordNotifyChange([{ recordId: this.recordId }]);
		clearTimeout(this.timerId);
		this.timerId = window.setTimeout(this.wiredRecord.bind(this), 1000, this.wiredApexData);
	}

	renderedCallback() {
		if (!this.stylesLoaded) {
			Promise.all([loadStyle(this, WrappedHeaderTable)])
				.then(() => {
					this.stylesLoaded = true;
				})
				.catch((error) => {
					console.error("Error loading custom styles");
				});
		}
	}

	//	Closed Won
	@wire(getRecord, { recordId: "$recordId", fields: [STAGE_FIELD, JOURNEY_FIELD, BAN_NUMBER] })
	wiredRecord(result) {
		this.wiredApexData = result;
		const disableButton = new CustomEvent("error");
		// Dispatches the event.
		this.dispatchEvent(disableButton);
		if (result.error) {
			if (Array.isArray(result.error.body)) {
				this.message = result.error.body.map((e) => e.message).join(", ");
			} else if (typeof result.error.body.message === "string") {
				this.message = result.error.body.message;
			}
			this.isError = true;
			this.isCallingApi = false;
		} else if (result.data) {
			this.opp = result.data;
			if (this.opp.fields.Select_Journey__c.value === "Add Mobile Product") {
				if (this.opp.fields.StageName.value !== "Closed Won") {
					this.getFWATempWithAPICall();
				} else {
					this.isError = true;
					this.isCallingApi = false;
					this.message = this.label.errorCloseWonOpp;
				}
			} else {
				this.message = this.label.errorNoAddMobileJourney;
				this.isError = true;
				this.isCallingApi = false;
			}
		}
	}

	startSearchTimer(evt) {
		this.searchKey = evt.target.value;
		clearTimeout(this.timerId);
		this.timerId = setTimeout(this.handleSearchText.bind(this), 200);
	}

	handleSearchText(evt) {
		this.isSearch = true;
		if (this.searchKey) {
			var filteredRecords = [];
			for (var index in this.allRecords) {
				var record = this.allRecords[index];
				let foundKey = false;
				for (var key in record) {
					var value = record[key].toString();
					if (
						key === "templateDescription" ||
						key === "contractNumber" ||
						key === "basePlanName" ||
						key === "templateId"
					) {
						if (
							value &&
							value.length > 0 &&
							value.toLowerCase().includes(this.searchKey.toLowerCase())
						) {
							foundKey = true;
						}
					}
				}
				if (foundKey) {
					filteredRecords.push(record);
				}
			}
			this.filteredRecords = filteredRecords;
		} else {
			this.filteredRecords = this.allRecords;
		}
		this.filterRecords(0);
	}

	rowSelection(evt) {
		// Avoid any operation if page has changed
		// as this event will be fired when new data will be loaded in page
		// after clicking on next or prev page
		if (!this.dataHasChanged || this.initialLoad) {
			// set initial load to false
			if (this.isSearch) {
				this.isSearch = false;
			}
			this.initialLoad = false;

			// Get currently select rows, This will only give the rows available on
			// current page
			let selectedRows = evt.detail.selectedRows;

			// Get all selected rows from datatable, this will give all the selected data
			// from all the pages
			let allSelectedRows = this.selection;

			var table = this.template.querySelector('[data-id="datarow"]');
			var rows = table.data;

			let mapRows = new Map();
			rows.forEach((element) => {
				mapRows.set(element.keyId, element);
			});

			// Process the rows now
			// Condition 1 - if any new row selected, add to our allSelectedRows attribute
			// Condition 2 - any row is deselected, remove from allSelectedRows attribute
			// Solution - Remove all rows from current page from allSelectedRows
			// attribute and then add again

			// Removing all rows coming from current page from allSelectedRows
			for (let j = allSelectedRows.length - 1; j >= 0; j--) {
				if (mapRows.has(allSelectedRows[j])) {
					allSelectedRows.splice(j, 1);
				}
			}

			//Adding all the new selected rows in allSelectedRows
			for (let k = 0; k < selectedRows.length; k++) {
				allSelectedRows.push(selectedRows[k].keyId);
			}
			//Setting new value in selection attribute
			this.selection.push(...allSelectedRows);
			this.selection = [...new Set(this.selection)];
		} else {
			this.dataHasChanged = false;
		}
	}

	previousEve() {
		this.pageNumber = this.pageNumber - 1;
		var newIndex = Number(this.pageSize) * this.pageNumber - Number(this.pageSize);
		//Setting pageChange variable to true
		this.dataHasChanged = true;
		this.filterRecords(newIndex);
	}

	nextEve() {
		let newIndex = Number(this.pageSize) * this.pageNumber;
		this.pageNumber = this.pageNumber + 1;
		//Setting pageChange variable to true
		this.dataHasChanged = true;
		this.filterRecords(newIndex);
	}

	filterRecords(_startingIndex) {
		if (this.filteredRecords && this.filteredRecords.length > 0) {
			this.resultSize = this.filteredRecords.length;
			this.totalPages = Math.ceil(this.resultSize / Number(this.pageSize));
			var viewRecords = [];

			if (_startingIndex === 0) {
				this.pageNumber = 1;
				this.recordFrom = 1;
			} else {
				this.recordFrom = _startingIndex + 1;
			}
			this.currentPage = Math.ceil(this.recordFrom / Number(this.pageSize));

			let _endIndex = _startingIndex + Number(this.pageSize);
			viewRecords = this.filteredRecords.slice(_startingIndex, _endIndex);
			this.recordTo = _startingIndex + viewRecords.length;
			this.data = viewRecords;
			if (!this.initialLoad) {
				let mapSel = new Map();
				let ltSelected = [];
				this.selection.forEach((e) => {
					mapSel.set(e, e);
				});
				viewRecords.forEach((element) => {
					if (mapSel.has(element.keyId)) {
						ltSelected.push(element.keyId);
					}
				});
				let _bfSelectedRows = this.template.querySelector('[data-id="datarow"]')
					.selectedRows;
				_bfSelectedRows.push(...ltSelected);
				this.selection.push(..._bfSelectedRows);
				this.selection = [...new Set(this.selection)];
				this.template.querySelector('[data-id="datarow"]').selectedRows = this.selection;
				if (this.isSearch) {
					this.dataHasChanged = true;
					this.isSearch = false;
				}
			}
		} else {
			this.resultSize = 0;
			this.currentPage = 0;
			this.recordFrom = 0;
			this.recordTo = 0;
			this.data = [];
			this.totalPages = 0;
		}
	}

	get recordCount() {
		return `Page ${this.pageNumber} | Showing records from ${this.recordFrom} to ${this.recordTo}`;
	}

	get disPre() {
		return this.pageNumber === 1 ? true : false;
	}

	get disNext() {
		return this.totalPages === this.currentPage ? true : false;
	}

	getFWATempWithAPICall() {
		getFWATempFromAPI({
			strBANNumber: this.opp.fields.BAN__r.displayValue,
			oppId: this.recordId
		})
			.then((result) => {
				let vfAssetData = JSON.parse(JSON.stringify(result));
				this.isCallingApi = false;
				this.allRecords = vfAssetData;
				this.filteredRecords = vfAssetData;
				this.filterRecords(0);
				const disableButton = new CustomEvent("error");
				// enable the button
				this.dispatchEvent(disableButton);
			})
			.catch((error) => {
				if (Array.isArray(error.body)) {
					this.message = error.body.map((e) => e.message).join(", ");
				} else if (typeof error.body.message === "string") {
					this.message = error.body.message;
				}
				this.isError = true;
				this.isCallingApi = false;
			});
	}

	//function to save close and show toast Start
	@api
	close() {
		const closeQA = new CustomEvent("close");
		// Dispatches the event.
		this.dispatchEvent(closeQA);
	}

	@api
	selectSaveOLI() {
		let _mapAllSelection = new Map();
		// Assign to map
		let setSelected = new Set();
		this.selection.forEach((keySel) => {
			setSelected.add(keySel);
		});
		this.allRecords.forEach((record) => {
			if (setSelected.has(record.keyId)) {
				_mapAllSelection.set(record.keyId, record);
			}
		});
		let draft = this.template.querySelector('[data-id="datarow"]').draftValues;
		draft.forEach((element) => {
			if (_mapAllSelection.has(element.keyId)) {
				let val = _mapAllSelection.get(element.keyId);
				val = {
					...val,
					quantityAcquisition: element.quantityAcquisition,
					quantityPortin: element.quantityPortin,
					quantityRetention: element.quantityRetention
				};
				_mapAllSelection.set(val.keyId, val);
			}
		});
		let isAllValid = true;
		if (_mapAllSelection && _mapAllSelection.size > 0) {
			let lst = [];
			for (let mapSel of _mapAllSelection.values()) {
				let hasQuantityA = false;
				let hasQuantityP = false;
				let hasQuantityR = false;
				if (mapSel.quantityAcquisition && parseInt(mapSel.quantityAcquisition) > 0) {
					let acq = {
						fwaId: mapSel.fwaId,
						templateId: mapSel.templateId,
						quantity: mapSel.quantityAcquisition,
						type: "New",
						baseplan: mapSel.basePlanName,
						connectionType: mapSel.connectionType,
						resourceCat: mapSel.resourceCategory
					};
					lst.push(acq);
					hasQuantityA = true;
				}
				if (mapSel.quantityPortin && parseInt(mapSel.quantityPortin) > 0) {
					let port = {
						fwaId: mapSel.fwaId,
						templateId: mapSel.templateId,
						quantity: mapSel.quantityPortin,
						type: "Portin",
						baseplan: mapSel.basePlanName,
						connectionType: mapSel.connectionType,
						resourceCat: mapSel.resourceCategory
					};
					lst.push(port);
					hasQuantityP = true;
				}
				if (mapSel.quantityRetention && parseInt(mapSel.quantityRetention) > 0) {
					let port = {
						fwaId: mapSel.fwaId,
						templateId: mapSel.templateId,
						quantity: mapSel.quantityRetention,
						type: "Retention",
						baseplan: mapSel.basePlanName,
						connectionType: mapSel.connectionType,
						resourceCat: mapSel.resourceCategory
					};
					lst.push(port);
					hasQuantityR = true;
				}

				if (!hasQuantityP && !hasQuantityA && !hasQuantityR) {
					isAllValid = false;
					break;
				}
			}
			if (isAllValid) {
				this.showSpinner = true;
				createOLI({
					strJSONToDeserialize: JSON.stringify(lst),
					oppId: this.recordId
				})
					.then((result) => {
						this.showSpinner = false;
						this.showToast("success", "Opportunity line Items created");
					})
					.catch((error) => {
						this.showSpinner = false;
						this.showToast("error", error.body.message);
					});
			} else {
				this.showToast("warning", "One or more rows has no Quantity selected");
			}
		} else {
			this.showToast("warning", "Please select at least one Asset");
		}
	}

	showToast(variant, msg) {
		const event = new ShowToastEvent({
			title: variant,
			variant: variant,
			message: msg
		});
		this.dispatchEvent(event);
	}

	get disableActions() {
		return this.isError || this.isCallingApi;
	}

	get variantButton() {
		return this.isRecordPage ? "Neutral" : "destructive-text";
	}
}