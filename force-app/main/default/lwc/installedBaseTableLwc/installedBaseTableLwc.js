//import { LightningElement, track, wire, api } from "lwc";
import { LightningElement, track, api } from "lwc";
//import { refreshApex } from "@salesforce/apex";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getCustomerAssets from "@salesforce/apex/InstalledBaseTableLwcController.getCustomerAssets";
import searchCustomerAssetsByFields from "@salesforce/apex/InstalledBaseTableLwcController.searchCustomerAssetsByFields";
import createProduct from "@salesforce/apex/InstalledBaseTableLwcController.createProduct";
//import cscrm__Passport_Number__c from "@salesforce/schema/Contact.cscrm__Passport_Number__c";
//import Technical_Sales_Consultant__c from "@salesforce/schema/Case.Technical_Sales_Consultant__c";
import createContractedProduct from "@salesforce/apex/InstalledBaseTableLwcController.createContractedProduct";

const columns = [
	// { label: 'Id', fieldName: 'Id', type: 'text',sortable: true },
	// { label: 'Customer Asset Name', fieldName: 'Name', type: 'text', sortable: true},
	// { label: 'Installed Base Id', fieldName: 'Installed_Base_Id__c', type: 'text', sortable: true},
	{
		label: "Charge Description",
		fieldName: "Charge_Description__c",
		type: "text",
		sortable: true
	},
	{ label: "Price", fieldName: "Price__c", type: "currency", sortable: true },
	{
		label: "Quantity",
		fieldName: "Quantity__c",
		type: "number",
		sortable: true
	},
	{
		label: "Discount",
		fieldName: "Discount__c",
		type: "percent",
		sortable: true
	},
	{
		label: "Contract Period",
		fieldName: "Contract_Period__c",
		type: "Number",
		sortable: true
	},
	{
		label: "Article Code",
		fieldName: "Article_Code__c",
		type: "text",
		sortable: true
	},
	{
		label: "Article Description",
		fieldName: "BOP_Article_Description__c",
		type: "text",
		sortable: true
	},
	{ label: "Site", fieldName: "Site_Name", type: "text", sortable: true },
	// { label: 'Site', fieldName: 'asset.Site__r.Name', type: 'text',sortable: true},
	{
		label: "Allocation Code",
		fieldName: "Cost_Center__c",
		type: "text",
		sortable: true
	},
	{
		label: "Purchase Order Number",
		fieldName: "PO_Number__c",
		type: "text",
		sortable: true
	},
	{
		label: "Contract End Date",
		fieldName: "Contract_End_Date__c",
		type: "date",
		sortable: true
	}
];

const DEFAULT_SELECTED_TAB_LABEL = "Selected Installed base items ";

export default class InstalledBaseTableLwc extends LightningElement {
	@track customerAssets = [];
	@track error;
	@track sortBy;
	@track sortDirection;
	@track searchContractPeriod = 0;
	@track chargeDescription = "";
	@track searchSite = "";
	@track searchPurchaseOrder = "";
	@track searchAllocationCode = "";
	@track isLoading = false;
	@api opportunityrecordid;
	@api progressValue = false;
	@api OpportunityLineItems = [];

	// Selected items tab:
	@track selectedCustomerAssets = [];
	@track selectedCustomerAssetsCopy = [];
	@track selectedCustomerAssetLabel = DEFAULT_SELECTED_TAB_LABEL + "(0)";

	// Keep track of and maintain selected customer installed base items
	@track selectedCustomerBaseItems = [];

	columns = columns;
	allrecords = [];
	TempselectList = [];
	buttonLabel = "Return to Add/Edit";
	// Contracted Product 
	@api vfcontractid;
	@api orderid;
	@track isCP = false;
	buttonLabelCP = "Return to Edit CPs";
	// Needed for the apex refresh due to wire setup
	assetResults;
	@track hasRendered = true;
	// Scrolling 
	rowLimit =50;
	rowOffSet=0;
	@track isLoaded = false;
	@track totalCountRecords;
     
	connectedCallback() {
		this.loadData();
		this.isLoaded = true;
		if(this.orderid  ){
			this.isCP = true;
			
		}
	
	}

	loadData() {
	//	getCustomerAssets({ OpportunityId: this.opportunityrecordid })
	return getCustomerAssets({ opportunityId: this.opportunityrecordid,limitSize: this.rowLimit , offset : this.rowOffSet })
			.then((result) => {
				console.log(
					this.customerAssets
						? this.customerAssets.length
						: "customer assets is empty"
				);
				let retrievedAssets = [...this.customerAssets];
				this.error = undefined;
				result.forEach((asset) => {
					try {
						let preparedAsset = { ...asset };
						if (asset.hasOwnProperty("Site__r")) {
							preparedAsset.Site_Name = asset.Site__r.Name;
						}

						retrievedAssets.push(preparedAsset);
					} catch (err) {
						console.log(err);
					}
				});

				this.customerAssets = retrievedAssets;
				//this.assetResults =retrievedAssets;
				console.log('**********refresh data'+this.customerAssets.length);
				this.totalCountRecords=this.customerAssets.length;  
				this.checkSelectedRows();
			})
			.catch((error) => {
				this.error = error;
				this.customerAssets = undefined;
			});
			
	}

	findSearchResult(event) {
		
		if (event.target.name === "ChargeDescription") {
			this.chargeDescription = event.target.value;
		}

		if (event.target.name === "ContractPeriod") {
			this.searchContractPeriod = event.target.value;
			if (this.searchContractPeriod === "") {
				this.searchContractPeriod = 0;
			}
		}

		if (event.target.name === "Site") {
			this.searchSite = event.target.value;
		}

		if (event.target.name === "PurchaseOrder") {
			this.searchPurchaseOrder = event.target.value;
		}

		if (event.target.name === "AllocationCode") {
			this.searchAllocationCode = event.target.value;
		}
		
		
		if (
			event.target.name === "ChargeDescription" ||
			event.target.name === "ContractPeriod" ||
			event.target.name === "Site" ||
			event.target.name === "PurchaseOrder" ||
			event.target.name === "AllocationCode"
		) {
			// console.log('inside');
			searchCustomerAssetsByFields({
				searchChargeDesc: this.chargeDescription,
				searchsite: this.searchSite,
				searchPurchaseOrder: this.searchPurchaseOrder,
				searchAllocationCode: this.searchAllocationCode,
				searchContrtPeriod: this.searchContractPeriod,
				opportunityId: this.opportunityrecordid
				
			}).then((result) => {
				this.customerAssets = [];

				// console.log('inside event');
				let retrievedAssets = [...this.customerAssets];

				this.error = undefined;
				// console.log("assets retrieved");

				result.forEach((asset) => {
					try {
						let preparedAsset = { ...asset };
						if (asset.hasOwnProperty("Site__r")) {
							preparedAsset.Site_Name = asset.Site__r.Name;
						}

						retrievedAssets.push(preparedAsset);
					} catch (err) {
						this.isLoading = false;
						this.error = err;
						console.log(err);
					}
				});
				this.customerAssets = retrievedAssets;
				if (retrievedAssets.length === 0) {
					// console.log('inside else load');
					this.loadData();
					return;
				}
				this.checkSelectedRows();
			});
		} else {
			// this.customerAssets = undefined;

			console.log("inside event else");
		}
	}

	/**
	 * Check the checkboxes for the visible rows if selected before
	 */
	checkSelectedRows() {
		// Make sure the checkboxes are ticket:
		let selectList = JSON.parse(
			JSON.stringify(this.selectedCustomerAssets)
		); // deep copy

		const tempList = this.customerAssets;

		selectList = selectList.filter(function (selectedRow) {
			let found = false;
			tempList.forEach((visibleRow) => {
				if (visibleRow.Id === selectedRow.Id) {
					found = true;
				}
			});
			return found;
		});
		this.selectedCustomerBaseItems = selectList.map((x) => x.Id);
	}

	handleSortdata(event) {
		// field name
		this.sortBy = event.detail.fieldName;
		console.log('sortBy'+this.sortBy);  
		// sort direction
		this.sortDirection = event.detail.sortDirection;
		console.log('this.sortDirection '+this.sortDirection );  
		// calling sortdata function to sort the data based on direction and selected field
		this.sortData(event.detail.fieldName, event.detail.sortDirection);
	}

	sortData(fieldname, direction) {
		// serialize the data before calling sort function
		let parseData = JSON.parse(JSON.stringify(this.customerAssets));

		// Return the value stored in the field
		let keyValue = (a) => {
			return a[fieldname];
		};

		// cheking reverse direction
		let isReverse = direction === "asc" ? 1 : -1;

		// sorting data
		parseData.sort((x, y) => {
			x = keyValue(x) ? keyValue(x) : ""; // handling null values
			y = keyValue(y) ? keyValue(y) : "";

			// sorting values based on direction
			return isReverse * ((x > y) - (y > x));
		});

		// set the sorted data to data table data
		this.customerAssets = parseData;
	}

	/**
	 * Process the row selection. Make sure the assets on the selected-tab are in sync with what is selected, what was already selected earlier and what was selected earlier but is not anymore
	 * @param {object} event
	 */
	processRowSelection(event) {
		// All current selected rows:
		const selectedRows = event.detail.selectedRows;
		this.allrecords = selectedRows;

		// All previously selected rows:
		const allSelectedRows = this.selectedCustomerAssets;

		let selectList = JSON.parse(
			JSON.stringify(this.selectedCustomerAssets)
		); // deep copy

		// Make sure to keep track of all selected rows:
		selectedRows.forEach((item) => {
			let found = false;
			allSelectedRows.forEach((selectedItem) => {
				if (selectedItem.Id === item.Id) {
					found = true;
				}
			});
			if (!found) {
				selectList.push(item);
			}
		});

		// Now remove from the selected rows any row that is currently visible but not selected:
		let visibleRows = JSON.parse(JSON.stringify(this.customerAssets)); // deep copy

		visibleRows = visibleRows.filter(function (asset) {
			let found = false;
			selectedRows.forEach((selectedItem) => {
				if (asset.Id === selectedItem.Id) {
					found = true;
				}
			});
			return !found;
		});

		selectList = selectList.filter(function (selectedRow) {
			let found = false;
			visibleRows.forEach((visibleRow) => {
				if (visibleRow.Id === selectedRow.Id) {
					found = true;
				}
			});
			return !found;
		});

		this.selectedCustomerAssets = selectList;
		let selectedCustomerAssetsSize = this.selectedCustomerAssets
			? this.selectedCustomerAssets.length
			: 0;
		this.selectedCustomerAssetLabel =
			DEFAULT_SELECTED_TAB_LABEL + "(" + selectedCustomerAssetsSize + ")";

		// Make sure the checkboxes are ticket:
		this.selectedCustomerAssetsCopy = selectList.map((x) => x.Id);

		this.updateLabel();
	}

	updateLabel() {
		if (this.selectedCustomerAssets.length > 0) {
			this.buttonLabel = "Add to Add/Edit & Return";
			this.buttonLabelCP="Add to Edit CPs & Return";
		} else {
			this.buttonLabel = "Return to Add/Edit";
			this.buttonLabelCP="Return to Edit CPs";
		}
	}

	handleClick() {
		this.isLoading = true;
		let FinalList = [];
		let productIdList = [];
		for (let i = 0; i < this.allrecords.length; i++) {
			FinalList.push(this.allrecords[i].Id);
			productIdList.push(this.allrecords[i].Product__r.VF_Product__c);
		}

		this.progressValue = false;
		if (FinalList.length !== 0) {
			let tempList = [];
			createProduct({
				customerAssetsId: FinalList,
				opportunityId: this.opportunityrecordid,
				productsId: productIdList
			})
				.then((result) => {
					this.OpportunityLineItems = [];
					this.OpportunityLineItems = JSON.stringify(result);
					if (this.OpportunityLineItems.length !== 0) {
						this.isLoading = false;

						console.log("inside event to fire");
						const selectedEvent = new CustomEvent(
							"callinstalledbasetablelwc",
							{
								detail: this.OpportunityLineItems
							}
						);

						// Dispatches the event.
						this.dispatchEvent(selectedEvent);
					}
				})
				.catch((error) => {
					this.isLoading = false;
					console.log("inside catch");
					/* if (Array.isArray(error.body)){
                    this.error = error.body.map(e => e.message).join(', ');
                  
                }else if(typeof error.body.message === 'string'){
                    this.error =  error.body.message;
                }*/

					//this.error = error.body.fieldErrors.Duration__c[0].message;
					let errorList = [];

					// console.log('inside  this.error'+error.body.fieldErrors.CLC__c[0].message);
					if (error.body.fieldErrors) {
						
						errorList = JSON.stringify(error.body.fieldErrors);
						if (errorList.includes("CLC__c")) {
							console.log("inside errorrrr ");
							this.error =
								error.body.fieldErrors.CLC__c[0].message;
						} else if (errorList.includes("Duration__c")) {
							this.error =
								error.body.fieldErrors.Duration__c[0].message;
						} else if (errorList.includes("Quantity")) {
							this.error =
								error.body.fieldErrors.Quantity[0].message;
						}
					}
					console.log("inside for " + this.error);
					const evt = new ShowToastEvent({
						title: "Error",
						message: this.error,
						variant: "error",
						mode: "dismissable"
					});
					this.dispatchEvent(evt);
				});
		} else {
			this.isLoading = false;
			this.OpportunityLineItems = [];
			const selectedEvent = new CustomEvent("callinstalledbasetablelwc", {
				detail: this.OpportunityLineItems
			});

			// Dispatches the event.
			this.dispatchEvent(selectedEvent);
		}
	}

	// Temparory
	toggleSelection(event) {
		// All current selected rows:
		const selectedRows = event.detail.selectedRows;
		this.allrecords = selectedRows;

		// All previously selected rows:
		const allSelectedRows = this.selectedCustomerAssets;

		let selectList = JSON.parse(
			JSON.stringify(this.selectedCustomerAssets)
		); // deep copy

		// Make sure to keep track of all selected rows:
		selectedRows.forEach((item) => {
			let found = false;
			allSelectedRows.forEach((selectedItem) => {
				if (selectedItem.Id === item.Id) {
					found = true;
				}
			});
			if (!found) {
				selectList.push(item);
			}
		});

		// Now remove from the selected rows any row that is currently visible but not selected:
		let visibleRows = JSON.parse(JSON.stringify(this.customerAssets)); // deep copy

		visibleRows = visibleRows.filter(function (asset) {
			let found = false;
			selectedRows.forEach((selectedItem) => {
				if (asset.Id === selectedItem.Id) {
					found = true;
				}
			});
			return !found;
		});

		selectList = selectList.filter(function (selectedRow) {
			let found = false;
			visibleRows.forEach((visibleRow) => {
				if (visibleRow.Id === selectedRow.Id) {
					found = true;
				}
			});
			return !found;
		});

		this.selectedCustomerAssets = selectList;
		let selectedCustomerAssetsSize = this.selectedCustomerAssets
			? this.selectedCustomerAssets.length
			: 0;
		this.selectedCustomerAssetLabel =
			DEFAULT_SELECTED_TAB_LABEL + "(" + selectedCustomerAssetsSize + ")";

		// Make sure the checkboxes are ticket:
		this.selectedCustomerBaseItems = selectList.map((x) => x.Id);

		this.updateLabel();
	}

	createContractedProd() {
		this.isLoading = true;
		let FinalList = [];
		// For temporary assining hardcode values of below items 
	// this.vfContractid='a0Q9E0000054avaUAA';
	// this.orderid='a0V9E000007g1OKUAY';
	
		for (let i = 0; i < this.allrecords.length; i++) {
			FinalList.push(this.allrecords[i].Id);
		}
		console.log('********FinalList'+FinalList);
		this.progressValue = false;
		if (FinalList.length !== 0) {
			let tempList = [];
			/*createContractedProduct({
				customerAssetsId: FinalList,
				OpportunityId: this.opportunityrecordid,
				vfContractId:this.vfcontractid,
				orderId:this.orderid
				
			})*/
			createContractedProduct({
				customerAssetsId: FinalList,
				opportunityId: this.opportunityrecordid,
				orderId:this.orderid
				
			})
				.then((result) => {
					
					let contractedProdutcts =[];
					contractedProdutcts = JSON.stringify(result);
					console.log('********contractedProdutcts'+contractedProdutcts);
					this.isLoading = false;
					this.OpportunityLineItems = [];
					this.OpportunityLineItems = JSON.stringify(result);
					if (this.OpportunityLineItems.length !== 0) {
						this.isLoading = false;

						console.log("inside event to fire");
						const selectedEvent = new CustomEvent(
							"callinstalledbasetablelwc",
							{
								detail: this.OpportunityLineItems
							}
						);

						// Dispatches the event.
						this.dispatchEvent(selectedEvent);
					}
				})
				.catch((error) => {
					this.isLoading = false;
					console.log("inside catch"+error.body.message);
					console.log("inside catch"+JSON.stringify(error));
					console.log("inside catch"+JSON.stringify(error.body.fieldErrors));
					console.log("inside catch"+JSON.stringify(error.body.pageErrors));
					console.log("inside catch"+JSON.stringify(error.body.pageErrors[0].message));
					
					let errorList = [];
					if (error.body.pageErrors) {
						console.log("inside errorrrr ");
						this.error =error.body.pageErrors[0].message;
	
					} else if (error.body.fieldErrors) {
						console.log("inside errorrrr ");
						errorList = JSON.stringify(error.body.fieldErrors);
						console.log("inside errorrrr "+errorList);
						if (errorList.includes("CLC__c")) {
							console.log("inside errorrrr ");
							this.error =
								error.body.fieldErrors.CLC__c[0].message;
						} else if (errorList.includes("Duration__c")) {
							this.error =
								error.body.fieldErrors.Duration__c[0].message;
						} else if (errorList.includes("Quantity")) {
							this.error =
								error.body.fieldErrors.Quantity[0].message;
						} else if (errorList.includes("Site__c")) {
							this.error =
								error.body.fieldErrors.Site__c[0].message;
						}
						else if (errorList.includes("Quantity__c")) {
							this.error =
								error.body.fieldErrors.Quantity__c[0].message;
						}
						
					}
					console.log("inside for " + this.error);
				/*	const evt = new ShowToastEvent({
						title: "Error",
						message: this.error,
						variant: "error",
						mode: "dismissable"
					});
					this.dispatchEvent(evt);*/
					this.dispatchEvent(
						new ShowToastEvent({
								  title: 'Error',
								  message: this.error,
								  variant: 'error',
								  mode: 'dismissable'
							  })
					 );
				});
		} else {
			this.isLoading = false;
			this.OpportunityLineItems = [];
			const selectedEvent = new CustomEvent("callinstalledbasetablelwc", {
				detail: this.OpportunityLineItems
			});

			// Dispatches the event.
			this.dispatchEvent(selectedEvent);
		}
	}	

	// more load 

	loadMoreData(event) {
        const currentRecord = this.customerAssets;
        const { target } = event;
        target.isLoading = true;

		this.rowOffSet = this.rowOffSet + this.rowLimit;
        this.loadData()
            .then((result)=> {
				target.isLoading = false;
				
			})
			.catch(error => {
				this.error = error;
				
			});
			if(this.rowOffSet > this.totalCountRecords){
				this.isLoaded = false;
			}   
    }
		

}