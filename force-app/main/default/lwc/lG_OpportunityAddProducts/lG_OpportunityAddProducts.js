import { api, LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class LG_OpportunityAddProducts extends NavigationMixin(LightningElement)  {

    @api recordId;

    navigateToPage() {
        if(this.recordId) {
            this[NavigationMixin.Navigate]({
                type: `standard__webPage`,
                attributes: {
                    url: `/apex/OpportunityProductEditor?id=${this.recordId}`
                }
            },
            true);
        }
    }
}