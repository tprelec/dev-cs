import { LightningElement, api, wire, track } from "lwc";

import { OPPTY_COLUMNS, LINE_ITEM_COLUMNS } from "./tabledefinitions.js";
import { Labels } from "./labels.js";

import { refreshApex } from "@salesforce/apex";
import { deleteRecord } from "lightning/uiRecordApi";
import { NavigationMixin } from "lightning/navigation";

import { reduceErrors, showToast } from "c/ldsUtils";

import queryShoppingCart from "@salesforce/apex/OpportunityProductEditorLWCController.queryShoppingCart";
import updateProducts from "@salesforce/apex/OpportunityProductEditorLWCController.updateProducts";
import createProduct from "@salesforce/apex/OpportunityProductEditorLWCController.createProduct";
import updateProduct from "@salesforce/apex/OpportunityProductEditorLWCController.updateProduct";
import saveData from "@salesforce/apex/OpportunityProductEditorLWCController.saveData";
import sitesJSON from "@salesforce/apex/OpportunityProductEditorLWCController.sitesJSON";
import getFilteredJsonProductPicklist from "@salesforce/apex/OpportunityProductEditorLWCController.getFilteredJsonProductPicklist";
import deleteProducts from "@salesforce/apex/OpportunityProductEditorLWCController.deleteProducts";
import cloneProducts from "@salesforce/apex/OpportunityProductEditorLWCController.cloneProducts";
import massCeaseProducts from '@salesforce/apex/OpportunityProductEditorLWCController.massCeaseProducts';
import massEditProducts from '@salesforce/apex/OpportunityProductEditorLWCController.massEditProducts';
import massPriceChangeProducts from '@salesforce/apex/OpportunityProductEditorLWCController.massPriceChangeProducts';
import massActivityProducts from '@salesforce/apex/OpportunityProductEditorLWCController.massActivityProducts';


import LIST_PRICE_FIELD from "@salesforce/schema/OpportunityLineItem.Gross_List_Price__c";
import QUANTITY_FIELD from "@salesforce/schema/OpportunityLineItem.Quantity";
import DISCOUNT_FIELD from "@salesforce/schema/OpportunityLineItem.DiscountNew__c";
import DURATION_FIELD from "@salesforce/schema/OpportunityLineItem.Duration__c";
import ARPU_FIELD from "@salesforce/schema/OpportunityLineItem.Product_Arpu_Value__c";
import CLC_FIELD from "@salesforce/schema/OpportunityLineItem.CLC__c";
import ACTIVITYTYPE_FIELD from "@salesforce/schema/OpportunityLineItem.Activity_Type__c";
import ID_FIELD from "@salesforce/schema/OpportunityLineItem.Id";

//Objects:
import OPPORTUNITY_OBJECT from "@salesforce/schema/Opportunity";
import OPPORTUNITY_LINE_ITEM_OBJECT from "@salesforce/schema/OpportunityLineItem";

import { getObjectInfo } from "lightning/uiObjectInfoApi";

const NO_SITE_NAME = "No site";

const ACQ = "Acq",
	RET = "Ret",
	MIG = "Mig";
const ONE_OFF_COSTS = "One off costs",
	MONTHLY_COSTS = "Monthly costs";
export default class OpportunityProductEditor extends NavigationMixin(
	LightningElement
) {
	@api recordId;

	// All labels:
	labels = new Labels();
	// Datatable properties:
	@track errors;
	@track allProducts = [];
	@track shownProducts = [];
	draftValues = [];

	// Needed to support the refreshing of the retrieved products based on the wire js method. Without this, it will not track the changes (eventhough @track is not needed here)
	shoppingCartResults;

	@track selectedRowIds = [];

	@track opptyAccount; // Account related to the oppty
	@track opportunity;
	@track opptyLoaded = false;

	// Modal properties:
	@track showInstalledBase = false;
	@track isModalOpen = false;
	@track showMainMenu = true;
	@track isLoading = true;
	@track isPriceModalOpen = false;
	@track isCeasesModalOpen = false;
	@track selectedRow;
	activityOptions;
	activitySearchOptions;
	@track isEditMode = false;
	@track opptySummary;

	@track
	opptyColumns = OPPTY_COLUMNS;

	// Filter inputs:
	@track prodSearch;
	@track siteSearch;
	@track activityTypeSearch;

	@track
	columns = LINE_ITEM_COLUMNS;

	// delete Action
	@track isDeleteModal = false;
	@track deleteId;
	// For Sites
	@track siteOptions = [];
	clcOptions;
	clcForNewOptions;
	clcForPCOptions;

	// Product lookup
	@api objName = "VF_Product__c";
	@api iconName;
	@api filter = "";
	@api searchPlaceholder = this.labels.search;

	installedBaseSelectLabel =
		this.labels.select + " " + this.labels.installedBase;
	returnToOpptyLabel = this.labels.returnTo + " " + this.labels.opportunity;
	filterOnSiteLabel = this.labels.filterOn + " " + this.labels.site;
	filterOnProductLabel = this.labels.filterOn + " " + this.labels.product;
	filterOnActivityTypeLabel =
		this.labels.filterOn + " " + this.labels.activityType;

	@track selectedProducts = [];
	@track selectedName;
	@track records;
	@track isValueSelected;
	@track blurTimeout;
	searchTerm;

	@wire(getObjectInfo, { objectApiName: OPPORTUNITY_LINE_ITEM_OBJECT })
	opportunityLineItem;

	@wire(getObjectInfo, { objectApiName: OPPORTUNITY_OBJECT })
	// eslint-disable-next-line no-unused-vars -- method signature
	oppInfo({ data, error }) {
		if (data) {
			console.log("CloseDate Label => ", data.fields.CloseDate.label);
		}
	}

	//css
	@track boxClass =
		"slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus";
	@track inputClass = "";
	// New Button
	@track isNewModalOpen = false;
	siteMulti = [];
	@track opportunityProdRecord = {
		clc: "Acq",
		quantity: QUANTITY_FIELD,
		listprice: LIST_PRICE_FIELD,
		discount: 0,
		duration: 1,
		costCenter: "",
		billing: true,
		site: "",
		productId: "",
		siteMulti: this.siteMulti,
		activityType: "Provide"
	};
	// Mass delete
	@track buttonLabel = "Delete";
	@track isMassDelete = true;
	@track recordsCount = 0;
	selectedRecords = [];
	@track isMassDeleteModal = false;
	@track isSingle = false;

	// Mass Clone
	@track isMassClone = true;
	selectedMassProvideRecords = [];
	 
	// Mass Provide,Price change  & Cease 
	@track isMassEditModal = false;
	@track isMassCease = false;
	@track isMassProvide = false;
	@track isMassPriceChange = false;
	@track isMassEdit = true;
	@track administrativeEndDate;
	@track contractEndDate;
	selectedCeaseRecords = [];
	selectedProvideRecords = [];
	selectedPriceChangeRecords = [];
	@track recordsCeaseCount = 0;
	@track isSingleCease = false;
	@track isSingleCeaseMsg = false;
	@track recordsProvideCount = 0;
	@track isSingleProvide = false;
	@track isSingleProvideMsg = false;
	@track isFinalMsgCease = false;
	@track isFinalMsgProvide = false;
	@track recordsPriceChangeCount = 0;
	@track isSinglePriceChange = false;
	@track isSinglePriceChangeMsg = false;
	@track isFinalMsgPriceChange = false;
	@track opportunityEditProdRecord = {
	   clc: "",
	   quantity: '',
	   listprice: '',
	   discount: '',
	   duration: '',
	   costCenter: "",
	   billing: true,
	   site: "",
	   productId: ""
   };
   @track opportunityEditPcRecord = {
	clc: "",
	listprice: '',
	discount: '',
	duration: '',
	costCenter: "",
	billing: true,
	poNumber:"",
	invoiceDate:""
};
  // mass update Activity types & clc 
  @track isActivity= false;
  @track isEditActivity= false;
  @track isMassEditActivity=false;
  @track  isFinalMsgActivity = false;
  massActivityType;
  massClc;
  clcForActOptions;
  isClc=true;
  selectedCeaseRCOCRecords = [];
  opptyLICloneProvideListForEdit=[];
   // Inline edit Validation
   isValidCease = true;
   isValidInlineEdit = 0;
   isValidInlineEditProvide=0;
   isValidProvide = true;
   @track sortDirection;
   @track sortBy;
   // Pagination 
   @track page = 1
   @track startingRecord = 1; //start record position per page
   @track endingRecord = 0; //end record position per page
   @track pageSize = 50; //default value we are assigning
   @track totalRecountCount = 0; //total record count received from all retrieved records
   @track totalPage = 0; //total number of page is needed to display all records
	@track allTrackedSelectedList = [];
	@track visibleSelectedRows = [];
   // for Deal_Type__c 
   @track isDealType;
   @track isNewButton = false;
	/**
	 * Constructor, adds listener to listen to picklist value changes coming from the custom lightning datatable
	 */
	constructor() {
		super();
		this.addEventListener(
			"onpicklistchanged",
			this.picklistChangeListener.processChange
		);
	}

	/**
	 * Listens to product line item creation based on install base assets
	 */
	@track
	installbaseListener = {
		installbaseLineItemsCreated(event) {
			event.stopPropagation();
			//let installedBaseArray = event.detail;
			this.toggleShowInstalledBase();
			this.isMassEdit= true;
            this.isMassDelete = true;
            this.isMassClone = true;
			return refreshApex(this.shoppingCartResults);
		}
	};

	/**
	 * Listen to picklist changes for the clc field
	 */
	@track
	picklistChangeListener = {
		processChange(event) {
			event.stopPropagation();
			let dataRecieved = event.detail.data;
			/*let updatedItem = {
				id: dataRecieved.context,
				clc: dataRecieved.value
			};*/
			let updatedItem={};
			if(dataRecieved.value==='Price Change' || dataRecieved.value==='Provide' || dataRecieved.value==='Cease'){
				updatedItem = {
					id: dataRecieved.context,
					activityType: dataRecieved.value
				};
			}else{
				updatedItem = {
					id: dataRecieved.context,
					clc: dataRecieved.value
				};
			}
			
		/*	for (let index in this.shownProducts) {
				if (
					Object.prototype.hasOwnProperty.call(this.shownProducts, index)
				) {
					let prodLineItem = this.shownProducts[index];
					//console.log("**********prodLineItem " + JSON.stringify(prodLineItem));
					if (prodLineItem.id === updatedItem.id) {
						
						console.log("**********prodLineItem " + JSON.stringify(prodLineItem.activityType));
						console.log("**********prodLineItem " + JSON.stringify(updatedItem.clc));
						if((prodLineItem.activityType==='Price Change'||prodLineItem.activityType==='Cease') && updatedItem.clc !='Churn'){
							
							this.isValidInlineEdit++;
						}else if((prodLineItem.activityType==='Price Change'||prodLineItem.activityType==='Cease') && updatedItem.clc ==='Churn'){
						
							this.isValidInlineEdit--;
						}
						if(prodLineItem.activityType==='Provide' && updatedItem.clc ==='Churn'){
							this.isValidInlineEditProvide++;
						}else if(prodLineItem.activityType==='Provide' && updatedItem.clc !='Churn'){
						
							this.isValidInlineEditProvide--;
						}
					}
				}
			}
			if(this.isValidInlineEdit >0){
				showToast(
					"Field validation",
					"This CLC type is not allowed when activity Type is Cease/Price Change",
					"error"
				);
			}
			if(this.isValidInlineEditProvide >0){
				showToast(
					"Field validation",
					"This CLC type is not allowed when activity Type is provide",
					"error"
				);
			}*/
		
			
			this.updateDraftValues(updatedItem);
		}
	};

	/**
	 * Updates the value received through the custom field type (clc picklist)
	 * Updating the draft values shows the yellow background color
	 * @param {Object} updateItem
	 */
	updateDraftValues(updateItem) {
		for (let index in this.shownProducts) {
			if (
				Object.prototype.hasOwnProperty.call(this.shownProducts, index)
			) {
				let prodLineItem = this.shownProducts[index];
				if (prodLineItem.id === updateItem.id) {
					updateItem.Id = "row-" + index;
				}
			}
		}
		let draftValueChanged = false;
		let copyDraftValues = [...this.draftValues];
		copyDraftValues.forEach((item) => {
			if (item.id === updateItem.id) {
				for (let field in updateItem) {
					if (
						Object.prototype.hasOwnProperty.call(updateItem, field)
					) {
						item[field] = updateItem[field];
					}
				}
				draftValueChanged = true;
			}
		});

		if (draftValueChanged) {
			this.draftValues = [...copyDraftValues];
		} else {
			this.draftValues = [...copyDraftValues, updateItem];
		}
	}

	/**
	 * Retrieves all child product line items for the provided oppty id
	 * @param {Id} oppId - Opportunity Id
	 */
	@wire(queryShoppingCart, {
		oppId: "$recordId"
	})
	queryShoppingCartForOppty(shoppingCartResult) {
		this.shoppingCartResults = shoppingCartResult; // Needed for the refresh apex to work
		// eslint-disable-next-line no-unused-vars -- method signature
		const { data, error } = shoppingCartResult;
		if (data) {
			// Just take the first one to be used in getting picklist info:
			let cartItems = data.cartItems; //[]; // reset

			// Update percentage for proper displaying (ie divide by 100)
			this.allProducts = cartItems.map((row) => {
				let p = JSON.parse(JSON.stringify(row)); // deep copy
				//p.discount = p.discount / 100;
				p.discountHidden = p.discountHidden / 100;
				return p;
			});
			console.log('*****this.allProducts'+JSON.stringify(this.allProducts) );
			//this.shownProducts = data.cartItems;

			this.opportunity = data.oppty;
			this.opptyAccount = data.accnt;
			this.opptyLoaded = true;
			//console.log('*****this.opportunity'+JSON.stringify(this.opportunity));
			//console.log('*****this.opportunity dealer '+this.opportunity.Deal_Type__c);
			this.isDealType =this.opportunity.Deal_Type__c;
			//console.log('*****this.isDealType'+this.isDealType);
			// pagination code 
			let total=0;
			for (let index in this.allProducts) {
				total ++;
			
				}
			//	console.log('*****total'+total);
			//this.totalRecountCount = data.length; //here it is 
			this.totalRecountCount = total;
			//console.log('*****length'+this.totalRecountCount );

            this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); //here it is 5
            
            //initial data to be displayed ----------->
            //slice will take 0th element and ends with 5, but it doesn't include 5th element
            //so 0 to 4th rows will be displayed in the table
            this.shownProducts = this.allProducts.slice(0,this.pageSize); 
            this.endingRecord = this.pageSize;
		}
		this.isLoading = false;
		// Need to remove after activity type to be merge with picklist component
		if(this.isDealType==="New" || this.isDealType==="Acquisition"){
			this.isNewButton=false;
			this.activityOptions = [
				{ value: "Provide", label: "Provide" },
				{ value: "Cease", label: "Cease" },
			];
		}else if(this.isDealType==="Cancellation") {
			this.isNewButton=true;
			this.activityOptions = [
				{ value: "Cease", label: "Cease" }
			];
		}else{
			this.isNewButton=false;
			this.activityOptions = [
				{ value: "Provide", label: "Provide" },
				{ value: "Cease", label: "Cease" },
				{ value: "Price Change", label: "Price Change" }
			];
		}
		
		this.activitySearchOptions  = [
			{ value: "", label: "All" },
			{ value: "Provide", label: "Provide" },
			{ value: "Cease", label: "Cease" },
			{ value: "Price Change", label: "Price Change" }
		];
		this.clcOptions = [
			{ value: "Acq", label: "Acq" },
			{ value: "Ret", label: "Ret" },
			{ value: "Churn", label: "Churn" },
			{ value: "Mig", label: "Mig" }
		];
		this.clcForNewOptions = [
			{ value: "Acq", label: "Acq" },
			{ value: "Ret", label: "Ret" },
			{ value: "Mig", label: "Mig" }
		];
		this.clcForPCOptions= [
			{ value: "Ret", label: "Ret" },
			{ value: "Mig", label: "Mig" }
		];
		this.updateSummary();
	}

	/**
	 * Handles the selected action in the data table
	 * @param {Object} event
	 */
	handleRowAction(event) {
		const action = event.detail.action;
		const row = event.detail.row;

		if (action.name === "delete") {
			//console.log('inside delete action');
			this.deleteId = row.id;
			this.isDeleteModal = true;
		}
		if (action.name === "copy") {
			this.isLoading = true;
			createProduct({
				jsonString: JSON.stringify(row),
				oppId: this.recordId,
				moreThanOne: false
			})
				.then(() => {
					this.isLoading = false;
					showToast("Success", "Copied Product", "success");
					return refreshApex(this.shoppingCartResults);
				})
				.catch((error) => {
					this.isLoading = false;
					showToast(
						"Error copying record",
						reduceErrors(error).join(", "),
						"error"
					);
				});
		}
		if (action.name === "edit") {
			// this.selectedRow = row;

			this.selectedRow = JSON.parse(JSON.stringify(row));
			//console.log('*****all values'+JSON.stringify(this.selectedRow));
		//	console.log('*****customerAssetId'+JSON.stringify(this.selectedRow.customerAssetId));
			if (this.selectedRow.activityType === "Cease" && this.selectedRow.customerAssetId) {
				this.selectedRow = JSON.parse(JSON.stringify(row));
				this.isCeasesModalOpen = true;
			} else if (this.selectedRow.activityType === "Price Change" && this.selectedRow.customerAssetId) {
				this.selectedRow = JSON.parse(JSON.stringify(row));
				this.isPriceModalOpen = true;
			} else {
				if (this.selectedRow.activityType === "Provide" && this.selectedRow.customerAssetId) {
                      this.isEditMode = true;
				}else{
					this.isEditMode = false;
				}
				//console.log('*****all values'+JSON.stringify(this.selectedRow));
				this.isModalOpen = true;
				this.isValueSelected = true;
				let selectedName = this.selectedRow.productLabel;
				this.selectedName = selectedName;
				this.productId = this.selectedRow.productId;
			}
		}
	}

	/**
	 * Filter products based on input fields. Filter must at least contain 2 characters to be taken into account
	 * @param {Object} event
	 */
	filterProducts(event) {
		let filter = event.target.value;
		let filterField = event.target.name;

		//Track filters:
		if (filterField === "site") {
			this.siteSearch = filter;
		} else if (filterField === "activityType") {
			this.activityTypeSearch = filter;
		} else {
			this.prodSearch = filter;
		}

		let siteFilter = filterField === "site" ? filter : this.siteSearch;
		let productFilter =
			filterField === "product" ? filter : this.prodSearch;
		let activityTypeFilter =
			filterField === "activityType" ? filter : this.activityTypeSearch;

		let filteredProducts = this.allProducts; // reset
		// Filter on site first
		if (siteFilter && siteFilter.length >= 2) {
			filteredProducts = filteredProducts.filter(function (product) {
				let siteLabel =
					Object.prototype.hasOwnProperty.call(
						product,
						"siteLabel"
					) && product.siteLabel
						? product.siteLabel
						: NO_SITE_NAME;
				return siteLabel
					.toLowerCase()
					.includes(siteFilter.toLowerCase());
			});
		}
		// Additional filtering on product
		if (productFilter && productFilter.length >= 2) {
			filteredProducts = filteredProducts.filter(function (product) {
				return product.productLabel
					.toLowerCase()
					.includes(productFilter.toLowerCase());
			});
		}
		// Filter on activity Type
		if (activityTypeFilter && activityTypeFilter.length >= 2) {
			filteredProducts = filteredProducts.filter(function (product) {
				let activityTypeLabel =
					Object.prototype.hasOwnProperty.call(
						product,
						"activityType"
					) && product.activityType
						? product.activityType
						: NO_SITE_NAME;
				return activityTypeLabel
					.toLowerCase()
					.includes(activityTypeFilter.toLowerCase());
			});
		}
		this.shownProducts = filteredProducts;
	}

	/**
	 * Toggle visibility of the install base selection modal
	 */
	toggleShowInstalledBase() {
		this.showMainMenu = !this.showMainMenu;
		this.showInstalledBase = !this.showInstalledBase;
	}

	/**
	 * Save the records
	 * @param {Object} event
	 */
	async handleSave(event) {
		const updatedFields = event.detail.draftValues;
		//console.log("********** " + JSON.stringify(event.detail));
		//console.log("updatedFields: " + JSON.stringify(updatedFields));
       
		//Mapping the table values to the sObject fields:
		const mappedRecords = updatedFields.map((row) => {
			let rec = {};
			if (Object.prototype.hasOwnProperty.call(row, "listprice"))
				rec[LIST_PRICE_FIELD.fieldApiName] = row.listprice;
			if (Object.prototype.hasOwnProperty.call(row, "quantity"))
				rec[QUANTITY_FIELD.fieldApiName] = row.quantity;
			if (Object.prototype.hasOwnProperty.call(row, "discount"))
				rec[DISCOUNT_FIELD.fieldApiName] = row.discount;
			if (Object.prototype.hasOwnProperty.call(row, "duration"))
				rec[DURATION_FIELD.fieldApiName] = row.duration;
			if (Object.prototype.hasOwnProperty.call(row, "arpuValue"))
				rec[ARPU_FIELD.fieldApiName] = row.arpuValue;
			if (Object.prototype.hasOwnProperty.call(row, "clc")){
				//console.log('*******Activity'+row.clc);
				//console.log('*******Activity'+row.activityType);
				rec[CLC_FIELD.fieldApiName] = row.clc;
			}
				
			if (Object.prototype.hasOwnProperty.call(row, "activityType")){
			//	console.log('*******Activity'+row.activityType);
			//	console.log('*******Activity'+row.clc);
				rec[ACTIVITYTYPE_FIELD.fieldApiName] = row.activityType;
			}
					
			rec[ID_FIELD.fieldApiName] = this.shownProducts[
				row.Id.replace("row-", "")
			].id;
			return rec;
		});
	//	console.log('*********isValidInlineEdit'+this.isValidInlineEdit);
	/*	if(this.isValidInlineEdit > 0){
			showToast(
				"Field validation",
				"This CLC type is not allowed when activity Type is Cease/Price Change",
				"error"
			);

		}else if(this.isValidInlineEditProvide > 0){
			showToast(
				"Field validation",
				"This CLC type is not allowed when activity Type is provide",
				"error"
			);
		}*/
		   //console.log('**************mappedRecords'+mappedRecords);
			this.isLoading = true;
		await updateProducts({ data: mappedRecords })
			// eslint-disable-next-line no-unused-vars -- method signature
			.then((result) => {
				this.isLoading = false;
				console.log(JSON.stringify("Apex update result: " + result));
				showToast("Success", "Order line items updated", "success");
				//this.isValidInlineEdit=0;
				//this.isValidInlineEditProvide=0;
				// Refresh LDS cache and wires
				//getRecordNotifyChange(notifyChangeIds);

				// Display fresh data in the datatable
				refreshApex(this.shoppingCartResults).then(() => {
					// Clear all draft values in the datatable
					this.draftValues = [];
				});
			})
			.catch((error) => {
				showToast(
					"Error updating or refreshing records",
					error.body.message,
					"error"
				);
			});
			
	}

	closeModal() {
		this.isModalOpen = false;
		this.isNewModalOpen = false;
		this.isPriceModalOpen = false;
		this.isCeasesModalOpen = false;
		this.isDeleteModal = false;
		this.isMassDeleteModal = false;
		this.isMassEditModal=false;
		this.isFinalMsgProvide=false;
		this.isFinalMsgCease = false;
		this.isFinalMsgPriceChange = false;
		this.isFinalMsgActivity=false;
	}

	/**************
	 * New Code Added by Ashok For Price Changes and Ceases Action
	 *
	 */
	/**************
	 * This event assign change value on specific value onchange
	 *
	 */
	changeValues(event) {
		switch (event.target.name) {
			case "Quantity":
				this.selectedRow.quantity = event.target.value;
				break;
			case "Allocation Code":
				this.selectedRow.costCenter = event.target.value;
				break;
			case "Contract Period":
				this.selectedRow.duration = event.target.value;
				break;
			case "PO Number":
				this.selectedRow.poNumber = event.target.value;
				break;
			case "Invoice Start Date":
				this.selectedRow.invoiceDate = event.target.value;
				break;
			case "Price":
				this.selectedRow.listprice = event.target.value;
				break;
			case "No Unify Invoice":
				this.selectedRow.billing = event.target.checked;
				break;
			case "Discount(%)":
				this.selectedRow.discount = event.target.value;
				break;
			case "Administrative End Date":
				this.selectedRow.administrativeEndDate = event.target.value;
				break;
			case "Do no return HW":
				this.selectedRow.doNotReturnHW = event.target.checked;
				break;
			case "Contractual End Date":
				this.selectedRow.contractEndDate = event.target.value;
				break;
			case "Group":
				this.selectedRow.grp = event.target.value;
				break;
			case "CLC":
				this.selectedRow.clc = event.target.value;
				break;
			case "Billing":
				this.selectedRow.billing = event.target.checked;
				break;
			case "Site":
				this.selectedRow.site = event.target.value;
				break;
			case "NewQuantity":
				this.opportunityProdRecord.quantity = event.target.value;
				break;
			case "NewAllocationCode":
				this.opportunityProdRecord.costCenter = event.target.value;
				break;
			case "NewContractPeriod":
				this.opportunityProdRecord.duration = event.target.value;
				break;
			case "NewPrice":
				this.opportunityProdRecord.listprice = event.target.value;
				break;
			case "NewDiscount":
				this.opportunityProdRecord.discount = event.target.value;
				break;
			case "NewCLC":
				this.opportunityProdRecord.clc = event.target.value;
				break;
			case "NewBilling":
				this.opportunityProdRecord.billing = event.target.checked;
				break;
			case "NewSite":
				this.opportunityProdRecord.site = event.target.value;
				break;
			//	case "NewQuantity":this.opportunityProdRecord.quantity=event.target.value;
			//	break;
			//	case "NewAllocationCode":this.opportunityProdRecord.costCenter=event.target.value;
			//	break;
			//	case "NewContractPeriod":this.opportunityProdRecord.duration=event.target.value;
			//	break;
			//	case "NewPrice":this.opportunityProdRecord.listprice=event.target.value;
			//	break;
			//	case "NewDiscount":this.opportunityProdRecord.discount=event.target.value;
		//		break;
			//	case "NewCLC":this.opportunityProdRecord.clc=event.target.value;
			//	break;
		//		case "NewBilling":this.opportunityProdRecord.billing=event.target.checked;
		//		break;
			//	case "NewSite":this.opportunityProdRecord.site=event.target.value;
			//	break;
				case "AdministrativeEndDate":this.AdministrativeEndDate=event.target.value;
				break;
				case "ContractualEndDate":this.contractEndDate=event.target.value;
				break;
				case "EditQuantity":this.opportunityEditProdRecord.quantity=event.target.value;
				break;
				case "EditAllocationCode":this.opportunityEditProdRecord.costCenter=event.target.value;
				break;
				case "EditContractPeriod":this.opportunityEditProdRecord.duration=event.target.value;
				break;
				case "EditPrice":this.opportunityEditProdRecord.listprice=event.target.value;
				break;
				case "EditDiscount":this.opportunityEditProdRecord.discount=event.target.value;
				break;
				case "EditCLC":this.opportunityEditProdRecord.clc=event.target.value;
				break;
				case "EditBilling":this.opportunityEditProdRecord.billing=event.target.checked;
				break;
				case "EditPONumber":this.opportunityEditProdRecord.poNumber=event.target.value;
				break;
				case "EditInvoiceStartDate":this.opportunityEditProdRecord.invoiceDate=event.target.value;
				break;
				case "EditPcAllocationCode":this.opportunityEditPcRecord.costCenter=event.target.value;
				break;
				case "EditPcContractPeriod":this.opportunityEditPcRecord.duration=event.target.value;
				break;
				case "EditPcPrice":this.opportunityEditPcRecord.listprice=event.target.value;
				break;
				case "EditPcDiscount":this.opportunityEditPcRecord.discount=event.target.value;
				break;
				case "EditPcCLC":this.opportunityEditPcRecord.clc=event.target.value;
				break;
				case "EditPcBilling":this.opportunityEditPcRecord.billing=event.target.checked;
				break;
				case "EditPcPONumber":this.opportunityEditPcRecord.poNumber=event.target.value;
				break;
				case "EditPcInvoiceStartDate":this.opportunityEditPcRecord.invoiceDate=event.target.value;
				break;
				case "actActivityType":this.massActivityType=event.target.value;
				break;
				case "actCLC":this.massClc=event.target.value;
				break;
				case "EditSite":this.opportunityEditProdRecord.site=event.target.value;

			default:
				break;
		}
	}
	/**
	 * update the records
	 * @param {Wrapper object} event
	 */
	// eslint-disable-next-line no-unused-vars -- method signature
	async handleSavePriceChange(event) {
		const allValid = [
			...this.template.querySelectorAll("lightning-input")
		].reduce((validSoFar, inputCmp) => {
			inputCmp.reportValidity();
			return validSoFar && inputCmp.checkValidity();
		}, true);

		if (allValid) {
			this.isLoading = true;
			 if(this.selectedRow.customerAssetQuantity < this.selectedRow.quantity){
				this.isLoading = false;
				showToast(
					"Validation Quantity",
					"Please enter equal or less quantity",
					"error"
				);
			 }else{
			saveData({ item: this.selectedRow })
				// eslint-disable-next-line no-unused-vars -- method signature
				.then((result) => {
					this.isLoading = false;
					showToast("Success", "Order line items updated", "success");

					// Display fresh data in the datatable
					refreshApex(this.shoppingCartResults).then(() => {
						// Clear all draft values in the datatable
						this.draftValues = [];
					});

					this.isPriceModalOpen = false;
					this.isCeasesModalOpen = false;
				})
				.catch((error) => {
					this.isLoading = false;
					showToast(
						"Error updating or refreshing records",
						error.body.message,
						"error"
					);
				});
			 }	
		} else {
			showToast(
				"Required Fields",
				"Please enter required fields",
				"error"
			);
		}
	}

	async updateSummary() {
		let //pageTotalValue = 0,
			pageTotalOneOff = 0,
			pageTotalOneOffAcq = 0,
			pageTotalOneOffRet = 0,
			pageTotalOneOffMig = 0,
			pageTotalPerMonth = 0,
			pageTotalPerMonthAcq = 0,
			pageTotalPerMonthRet = 0,
			pageTotalPerMonthMig = 0,
			//totalValue = 0, // TODO - What to display? Page total or this one?
			totalPerMonth = 0,
			totalOneOff = 0,
			totalOneOffAcq = 0,
			totalOneOffRet = 0,
			totalOneOffMig = 0,
			totalPerMonthAcq = 0,
			totalPerMonthRet = 0,
			totalPerMonthMig = 0;
		this.allProducts.forEach((product) => {
			let duration = product.duration;
			let rowValue =
				((100 - product.discount) / 100) *
				product.quantity *
				product.listprice;
			// the totalValue is the only value that keeps into account the duration
			//totalValue += rowValue * duration;
			// One-off products:
			if (duration === 1) {
				totalOneOff += rowValue;
				switch (product.clc) {
					case ACQ:
						totalOneOffAcq += rowValue;
						pageTotalOneOffAcq+= rowValue;
						break;
					case RET:
						totalOneOffRet += rowValue;
						pageTotalOneOffRet+= rowValue;
						break;
					case MIG:
						totalOneOffMig += rowValue;
						pageTotalOneOffMig += rowValue;
						break;
					default:
						break;
				}
			} else {
				totalPerMonth += rowValue;
				switch (product.clc) {
					case ACQ:
						totalPerMonthAcq += rowValue;
						pageTotalPerMonthAcq += rowValue;
						break;
					case RET:
						totalPerMonthRet += rowValue;
						pageTotalPerMonthRet  += rowValue;
						break;
					case MIG:
						totalPerMonthMig += rowValue;
						pageTotalPerMonthMig += rowValue;
						break;
					default:
						break;
				}
			}
			this.opptySummary = [
				{
					description: ONE_OFF_COSTS,
					opptyAcq: pageTotalOneOffAcq,
					opptyTotalAcq: totalOneOffAcq,
					opptyRet: pageTotalOneOffRet,
					opptyTotalRet: totalOneOffRet,
					opptyMig: pageTotalOneOffMig,
					opptyTotalMig: totalOneOffMig,
					opptyAll: pageTotalOneOff,
					totalAcq: totalOneOff
				},
				{
					description: MONTHLY_COSTS,
					opptyAcq: pageTotalPerMonthAcq,
					opptyTotalAcq: totalPerMonthAcq,
					opptyRet: pageTotalPerMonthRet,
					opptyTotalRet: totalPerMonthRet,
					opptyMig: pageTotalPerMonthMig,
					opptyTotalMig: totalPerMonthMig,
					opptyAll: pageTotalPerMonth,
					totalAcq: totalPerMonth
				}
			];
		});
	}

	/**
	 * Delete  product line item for the provided id
	 *
	 */
	// eslint-disable-next-line no-unused-vars -- method signature
	deleteData(event) {
		this.isLoading = true;
		//this.isMassDeleteModal=false;
		//this.recordsCount=0;
		//console.log('inside delete');
		deleteRecord(this.deleteId)
			.then(() => {
				this.isLoading = false;
				this.isDeleteModal = false;
				showToast("Success", "Product deleted", "success");
				return refreshApex(this.shoppingCartResults);
			})
			.catch((error) => {
				this.isLoading = false;
				showToast(
					"Error deleting record",
					reduceErrors(error).join(", "),
					"error"
				);
			});
	}

	// Navigate to Detail Opportunity Page -Start
	navigateToViewOpportunityPage() {
		this[NavigationMixin.Navigate]({
			type: "standard__recordPage",
			attributes: {
				recordId: this.recordId,
				objectApiName: "Opportunity",
				actionName: "view"
			}
		});
	}

	// this method validates the data and creates the csv file to download
	downloadCSVFile() {
		let rowEnd = "\n";
		let csvString = "";
		// this set elminates the duplicates if have any duplicate keys
		let rowData = new Set();

		// getting keys from data
		this.shownProducts.forEach(function (record) {
			Object.keys(record).forEach(function (key) {
				rowData.add(key);
			});
		});

		// Array.from() method returns an Array object from any object with a length property or an iterable object.
		rowData = Array.from(rowData);

		// splitting using ','
		csvString += rowData.join(",");
		csvString += rowEnd;

		// main for loop to get the data based on key value
		for (let i = 0; i < this.shownProducts.length; i++) {
			let colValue = 0;

			// validating keys in data
			for (let key in rowData) {
				if (Object.prototype.hasOwnProperty.call(rowData, key)) {
					// Key value
					// Ex: Id, Name
					let rowKey = rowData[key];
					// add , after every value except the first.
					if (colValue > 0) {
						csvString += ",";
					}
					// If the column is undefined, it as blank in the CSV file.
					let value =
						this.shownProducts[i][rowKey] === undefined
							? ""
							: this.shownProducts[i][rowKey];
					csvString += '"' + value + '"';
					colValue++;
				}
			}
			csvString += rowEnd;
		}

		// Creating anchor element to download
		let downloadElement = document.createElement("a");

		// This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
		downloadElement.href =
			"data:text/csv;charset=utf-8," + encodeURI(csvString);
		downloadElement.target = "_self";
		// CSV File Name
		downloadElement.download =
			"Manage Products  Salesforce - Unlimited Edition.csv";
		// below statement is required if you are using firefox browser
		document.body.appendChild(downloadElement);
		// click() Javascript function to download CSV file
		downloadElement.click();
	}

	/**
	 * Retrieves all Sites for the provided oppty id
	 * @param {Id} oppId - Opportunity Id
	 */
	@wire(sitesJSON, {
		oppId: "$recordId"
	})
	// eslint-disable-next-line no-unused-vars -- method signature
	getSitesPicklist({ error, data }) {
		// console.log('************inside picklist'+JSON.stringify(data));
		if (data) {
			for (let i = 0; i < data.length; i++) {
				this.siteOptions = [
					...this.siteOptions,
					{ value: data[i].Id, label: data[i].Unify_Site_Name__c }
				];
			}
			// this.siteOptions = data;
		}
		//console.log('************this.siteOptions'+this.siteOptions);
	}

	// Product looup code -Start
	/**
	 * Retrieves all Product looup  for the provided oppty id
	 * @param {Id} oppId - Opportunity Id and search value
	 */

	@wire(getFilteredJsonProductPicklist, {
		queryFilterString: "$searchTerm",
		oppId: "$recordId"
	})
	wiredRecords({ error, data }) {
		if (data) {
			this.error = undefined;
			this.records = data;
			//console.log('***********getFilteredJsonProductPicklist'+JSON.stringify(this.records));
		} else if (error) {
			this.error = error;
			this.records = undefined;
		}
	}

	handleClick() {
		this.searchTerm = "";
		this.inputClass = "slds-has-focus";
		this.boxClass =
			"slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus slds-is-open";
	}

	onBlur() {
		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.blurTimeout = setTimeout(() => {
			this.boxClass =
				"slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus";
		}, 300);
	}

	onSelect(event) {
		// console.log('************edit records'+this.selectedRow.listprice);
		let selectedId = event.currentTarget.dataset.id;
		//console.log('*************selectedId'+selectedId);
		this.productId = selectedId;
		//console.log('************select- this.productId'+ this.productId);
		//console.log('*************selectedId-datalist'+JSON.stringify(this.records));

		for (let i = 0; i < this.records.length; i++) {
			if (this.records[i].Id === this.productId) {
				//console.log('*************selectedId-datalist-item'+this.records[i].Id);
				//console.log('*************selectedId-datalist-item'+this.records[i].BAP_SAP__c);
				this.opportunityProdRecord.listprice = this.records[
					i
				].BAP_SAP__c;
				if (this.isModalOpen) {
					this.selectedRow.listprice = this.records[i].BAP_SAP__c;
				}
				if(this.isMassProvide){
					this.opportunityEditProdRecord.listprice = this.records[i].BAP_SAP__c;
				}
			}
		}
		//  console.log('*************selectedId-datalist-item'+item);
		let selectedName = event.currentTarget.dataset.name;
		//console.log('*************selectedName'+selectedName);
		const valueSelectedEvent = new CustomEvent("lookupselected", {
			detail: selectedId
		});
		this.dispatchEvent(valueSelectedEvent);
		this.isValueSelected = true;
		this.selectedName = selectedName;
		if (this.blurTimeout) {
			clearTimeout(this.blurTimeout);
		}
		this.boxClass =
			"slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus";
	}

	handleRemovePill() {
		this.isValueSelected = false;
	}

	onChange(event) {
		this.searchTerm = event.target.value;
	}

	// Product looup code -End
	// This method is used for edit records -Start
	// eslint-disable-next-line no-unused-vars -- method signature
	async submitDetails(event) {
		//console.log('******************values- this.productId'+ this.productId);
		this.selectedRow.productId = this.productId;
		//console.log('******************valuesthis.selectedRow.productId'+JSON.stringify(this.selectedRow.productId));
		// console.log('******************values'+JSON.stringify(this.selectedRow));
		const allValid = [
			...this.template.querySelectorAll("lightning-input")
		].reduce((validSoFar, inputCmp) => {
			inputCmp.reportValidity();
			return validSoFar && inputCmp.checkValidity();
		}, true);

		if (allValid) {
			this.isLoading = true;
			// console.log('******************this.isLoading '+this.isLoading);
			try {
				updateProduct({
					jsonString: JSON.stringify(this.selectedRow),
					oppId: this.recordId,
					moreThanOne: false
				})
					.then(() => {
						this.isLoading = false;
						showToast("Success", "Product updated", "success");
						this.isModalOpen = false;
						return refreshApex(this.shoppingCartResults);
					})
					.catch((error) => {
						this.isLoading = false;

						showToast(
							"Error updating or refreshing records",
							error.body.message,
							"error"
						);
					});
			} catch (error) {
				// console.log('******************erroe');
				this.isLoading = false;
				showToast(
					"Error updating record",
					reduceErrors(error).join(", "),
					"error"
				);
			}
		} else {
			showToast(
				"Required Fields",
				"Please enter required fields",
				"error",
				"dismissable"
			);
		}
	}
	// edit records -End

	// This Method open new entry modal
	newProduct() {
		this.productId = "";
		// console.log('*******prod'+this.productId);

		this.selectedName = "";
		this.isValueSelected = false;
		this.opportunityProdRecord.listprice = 0;

		this.isNewModalOpen = true;
	}

	// This Method create Opportunity Product Item
	CreateData() {
		const allValid = [
			...this.template.querySelectorAll("lightning-input")
		].reduce((validSoFar, inputCmp) => {
			inputCmp.reportValidity();
			return validSoFar && inputCmp.checkValidity();
		}, true);

	//	if (allValid && this.opportunityProdRecord.site) {
		if(allValid) {
			if(this.opportunityProdRecord.site){
			// console.log('***********new'+this.opportunityProdRecord);
			this.opportunityProdRecord.productId = this.productId;
			this.siteMulti.push(this.opportunityProdRecord.site);
			// console.log('***********new-this.siteMulti'+this.siteMulti);
			this.opportunityProdRecord.siteMulti = this.siteMulti;
			// console.log('***********new'+JSON.stringify(this.opportunityProdRecord));

			if (this.opportunityProdRecord) {
				this.isLoading = true;
				createProduct({
					jsonString: JSON.stringify(this.opportunityProdRecord),
					oppId: this.recordId,
					moreThanOne: false
				})
					.then(() => {
						this.isLoading = false;
						this.isNewModalOpen = false;
					//	this.opportunityProdRecord = {};
					this.opportunityProdRecord = {
						clc: "Acq",
						quantity:'',
						listprice:'',
						discount: 0,
						duration: 1,
						costCenter: "",
						billing: true,
						site: "",
						productId: "",
						siteMulti:"",
						activityType: "Provide"
					};
						this.productId = "";
						this.selectedName = "";
						this.isValueSelected = false;
						this.opportunityProdRecord.clc = "Acq";
						this.opportunityProdRecord.discount = 0;
						this.opportunityProdRecord.duration = 1;
						this.opportunityProdRecord.billing= true;

						showToast(
							"Success",
							"Product has been created successfully",
							"success"
						);
						return refreshApex(this.shoppingCartResults);
					})
					.catch((error) => {
					   // console.log('*****error'+error);
					//	console.log('*****error'+JSON.stringify(error));
					//	console.log("inside body "+JSON.stringify(error.body));
                    //    console.log("inside errorrrr "+JSON.stringify(error.body.fieldErrors));
					//	console.log("inside pageErrors "+JSON.stringify(error.body.pageErrors));
						//console.log("inside pageErrors1 "+JSON.stringify(error.body.pageErrors[0].message));
						//console.log("inside pageErrors2 "+JSON.stringify(error.body.pageErrors.message));
						this.isLoading = false;
						if(error.body.pageErrors){
                           if(error.body.pageErrors[0].message){
							showToast(
								"Error creating record",
								error.body.pageErrors[0].message,
								"error"
							);

						   }  

						}else{
						showToast(
							"Error creating record",
							reduceErrors(error).join(", "),
							"error"
						);
					}
					});
			}
		     }else {
				showToast(
					"Required Fields",
					"Please enter site Field",
					"error",
					"dismissable"
				);
			}
		} else {
			showToast(
				"Required Fields",
				"Please enter required fields",
				"error",
				"dismissable"
			);
		}
	}

	// Getting selected rows
	getSelectedRecords(event) {
		// getting selected rows
		const selectedRows = event.detail.selectedRows;
		//console.log("rows" + JSON.stringify(selectedRows));
		this.getTrackedSelectedRecords(selectedRows);
		this.recordsCount = event.detail.selectedRows.length;
		//console.log(" this.recordsCount" + this.recordsCount);

		// this set elements the duplicates if any
		let OpportunityLineItemIds = new Set();
         // for mass cease ,Price Change and Provide records
		let OpptyLICeaseIds = new Set();
		let OpptyLIProvideIds = new Set();
		let OpptyLIPriceChangeIds = new Set();
		let OpptyLICloneProvideIds = new Set();
		let OpptyLICeaseForRCOCIds = new Set();
		let OpptyLICloneProvideIdsForEdit = new Set();
		// getting selected record id
		for (let i = 0; i < selectedRows.length; i++) {
			OpportunityLineItemIds.add(selectedRows[i].id);
			if(selectedRows[i].activityType ==='Cease'){
				OpptyLICeaseIds.add(selectedRows[i].id);
				this.recordsCeaseCount++;
			}
			//console.log('**********'+selectedRows[i].customerAssetId);
			//console.log('**********'+selectedRows[i].isRCOC);
			// for RC/OC Records  
			if(selectedRows[i].activityType ==='Cease' && selectedRows[i].isRCOC !=='OC'){
				OpptyLICeaseForRCOCIds.add(selectedRows[i].id);
			}
			if(selectedRows[i].activityType ==='Price Change'){
				OpptyLIPriceChangeIds.add(selectedRows[i].id);
				OpptyLICeaseForRCOCIds.add(selectedRows[i].id);
				this.recordsPriceChangeCount++;
			}
			
			if(selectedRows[i].activityType ==='Provide' && selectedRows[i].customerAssetId === undefined){
			
				OpptyLIProvideIds.add(selectedRows[i].id);
				this.recordsProvideCount++;
				OpptyLICeaseForRCOCIds.add(selectedRows[i].id);
			}
			if(selectedRows[i].activityType ==='Provide'){
			     if(selectedRows[i].customerAssetId === undefined){
				OpptyLICloneProvideIds.add(selectedRows[i].id);
				 }else if(selectedRows[i].customerAssetId){
					OpptyLICloneProvideIdsForEdit.add(selectedRows[i].id);
				 }
				OpptyLICeaseForRCOCIds.add(selectedRows[i].id);

			}
		}
        
		// coverting to array
		// RC/OC records 
		this.selectedCeaseRCOCRecords =Array.from(OpptyLICeaseForRCOCIds);
  //console.log('**************this.selectedCeaseRCOCRecords'+this.selectedCeaseRCOCRecords);
  

		// Mass cease 
		this.selectedCeaseRecords = Array.from(OpptyLICeaseIds);
		if(this.selectedCeaseRecords.length===1){
			this.isSingleCease=true;
			this.isMassCease=false;
			this.isSingleCeaseMsg= true;
			this.isMassEdit=false;
		  
		}else if(this.selectedCeaseRecords.length>1){
			this.isMassCease=true;
			this.isSingleCeaseMsg= false;
			this.isSingleCease=true;
			this.isMassEdit=false;
		}else{
			this.isSingleCease=false;
			this.isSingleCeaseMsg= false;
			this.isMassCease=false;
		}
		//Price Change 
		this.selectedPriceChangeRecords = Array.from(OpptyLIPriceChangeIds);
		if(this.selectedPriceChangeRecords.length===1){
			this.isSinglePriceChange=true;
			this.isMassPriceChange=false;
			this.isSinglePriceChangeMsg= true;
			this.isMassEdit=false;
		  
		}else if(this.selectedPriceChangeRecords.length>1){
			this.isMassPriceChange=true;
			this.isSinglePriceChangeMsg= false;
			this.isSinglePriceChange=true;
			this.isMassEdit=false;
		}else{
			this.isSinglePriceChange=false;
			this.isSinglePriceChangeMsg= false;
			this.isMassPriceChange=false;
		}
		// Mass Provide 
		this.selectedProvideRecords = Array.from(OpptyLIProvideIds);
		//console.log('***********this.selectedProvideRecords'+this.selectedProvideRecords);
		if( this.selectedProvideRecords.length>1){
			this.isMassProvide=true;
			this.isSingleProvideMsg=false;
			this.isSingleProvide=true;
			this.isMassEdit=false;
		//	this.isMassClone = false;
		}else  if( this.selectedProvideRecords.length===1){
			this.isSingleProvide=true;
			this.isMassProvide=false;  
			this.isSingleProvideMsg=true;
			this.isMassEdit=false;
			//this.isMassClone = true;
		}  else{
			this.isSingleProvide=false;
			this.isMassProvide=false;
			this.isSingleProvideMsg=false;
		//	this.isMassClone = true;
		}
	   // mass Provide clone
	  
	this.selectedMassProvideRecords = Array.from(OpptyLICloneProvideIds);
	if( this.selectedMassProvideRecords.length>1){
	//	this.isMassProvide=true;
	//	this.isSingleProvideMsg=false;
	//	this.isSingleProvide=true;
		this.isMassEdit=false;
		this.isMassClone = false;
	}else  if( this.selectedMassProvideRecords.length===1){
	//	this.isSingleProvide=true;
	//	this.isMassProvide=false;  
	//	this.isSingleProvideMsg=true;
		this.isMassEdit=false;
		this.isMassClone = true;
	}  else{
	//	this.isSingleProvide=false;
	//	this.isMassProvide=false;
	//	this.isSingleProvideMsg=false;
		this.isMassClone = true;
	}
	
	this.opptyLICloneProvideListForEdit = Array.from(OpptyLICloneProvideIdsForEdit);
	if(this.opptyLICloneProvideListForEdit.length>0){

		this.isMassEdit=false;
	}
		this.selectedRecords = Array.from(OpportunityLineItemIds);
		//console.log("selectedIds" + this.selectedRecords);
		if (this.recordsCount) {
			//console.log("if***" + this.isMassDelete);
			this.isMassDelete = false;
			//this.isEditActivity= true;
			//this.isMassEditActivity=true;
			//this.isMassClone = false;
			//this.isMassEdit=false;
		} else {
			//console.log("else***" + this.isMassDelete);
			this.isMassDelete = true;
			this.isMassClone = true;
			this.isMassEdit= true;
			//this.isEditActivity= false;
			//this.isMassEditActivity = false;
		}
		if(this.selectedCeaseRCOCRecords.length>0){
			this.isEditActivity= true;
			this.isMassEditActivity=true;
		}else{
			this.isEditActivity= false;
			this.isMassEditActivity = false;
		}
	}
	getTrackedSelectedRecords(selectedRows) {
		//console.log("start getTrackedSelectedRecords");
		let selectedRowIds = selectedRows.map((x) => x.id);
		//console.log("selectedRows: " + JSON.stringify(selectedRowIds));

	//	console.log("this.allTrackedSelectedList: " + this.allTrackedSelectedList);

		let allSelectedIds = [...new Set(this.allTrackedSelectedList.concat(selectedRowIds))]; // Concat and get rid of duplicates

	//	console.log("allSelectedIds before unticking taking into account: " + JSON.stringify(allSelectedIds));

		let visibleRows = JSON.parse(JSON.stringify(this.shownProducts)).map((x) => x.id); // deep copy

		// Filter the unselected rows that are visible on screen now as to make sure these are not tracked:
		let unselectedRowIds = visibleRows.filter(function (item) {
			let ticked = false;
			selectedRowIds.forEach((selectedItem) => {
				if (item === selectedItem) {
					ticked = true; // Item is selected
				}
			});
			return !ticked;
		});

		//console.log("Unticked rows: " + JSON.stringify(unselectedRowIds));

		allSelectedIds = allSelectedIds.filter(function (recId) {
			let keep = true;
			unselectedRowIds.forEach((unselectedRowId) => {
				if (recId === unselectedRowId) {
					keep = false;
				}
			});
			return keep;
		});

	//	console.log("remaining rowids: " + JSON.stringify(allSelectedIds));

		this.allTrackedSelectedList = allSelectedIds;
		//console.log("end getTrackedSelectedRecords");
	}
	// delete records process function
	deleteDataProd() {
		//console.log("inside mass delete");
		this.isMassDeleteModal = true;
		if (this.recordsCount === 1) {
			this.isSingle = true;
		} else {
			this.isSingle = false;
		}

		if (this.selectedRecords) {
			///console.log("selectedIds-inside" + this.selectedRecords);
			// setting values to reactive variables
			// this.buttonLabel = 'Processing....';
			this.isMassDelete = false;

			// calling apex class to delete selected records.
			// this.deleteOLI();
		}
	}

	// delete records Method
	deleteOLI() {
		this.isLoading = true;
		deleteProducts({ lstProdIds: this.selectedRecords })
			// eslint-disable-next-line no-unused-vars -- method signature
			.then((result) => {
				//console.log("result ====> " + result);

				this.buttonLabel = "Delete Selected Products";
				this.isTrue = false;
				this.isMassDeleteModal = false;
				this.isLoading = false;
				this.isMassClone = true;
				this.isMassDelete = true;
				this.isMassEdit = true;
				//const messageVal = this.recordsCount + " Products are deleted.";
				// showing success message
				if (this.recordsCount === 1) {
					showToast("Success", "Product deleted", "success");
				} else {
					showToast(
						"Success!!",
						this.recordsCount + " Products has been deleted.",
						"success",
						"dismissable"
					);
				}

				// Clearing selected row indexs
				this.template.querySelector(
					"lightning-datatable"
				).selectedRows = [];

				this.recordsCount = 0;

				// refreshing table data using refresh apex
				return refreshApex(this.shoppingCartResults);
			})
			.catch((error) => {
				//console.log(error);

				showToast(
					"Error while deleting Products",
					error.message,
					"error",
					"dismissable"
				);
			});
	}

	// Clone records process function
	cloneData() {
	//	if (this.selectedRecords) {
	//	if (this.selectedProvideRecords) {
		if (this.selectedMassProvideRecords) {
		
			//console.log("selectedIds-inside" + this.selectedRecords);

			this.isMassClone = true;
			this.isMassDelete = true;
			// calling apex class to clone selected records.
			this.cloneOLI();
		}
	}
	cloneOLI() {
		this.isLoading = true;
		//cloneProducts({ lstProdIds: this.selectedRecords })
	//	cloneProducts({ lstProdIds: this.selectedProvideRecords })
			// eslint-disable-next-line no-unused-vars -- method signature
			cloneProducts({ lstProdIds: this.selectedMassProvideRecords })
			.then((result) => {
				console.log("result ====> " + result);
				this.isLoading = false;
				this.isMassClone = true;
				this.isMassDelete = true;
				this.isMassEdit = true;
				//console.log("Count :" + this.recordsCount);
				// showing success message
				if (this.recordsCount === 1) {
					showToast("Success", "Copied Product", "success");
				} else {
					showToast(
						"Success!!",
					    " Duplicate records  has been created",
						"success"
					);
				}

				// Clearing selected row indexs
				this.template.querySelector(
					"lightning-datatable"
				).selectedRows = [];
				this.recordsCount = 0;

				// refreshing table data using refresh apex
				return refreshApex(this.shoppingCartResults);
			})
			.catch((error) => {
				this.isLoading = false;
				//console.log(error);

				showToast("Error copying record", error.message, "error");
			});
	}
	 //mass Actions 
	 massActions(){
		this.isMassEditModal=true;
		//console.log('******  this.isMassCease'+  this.isMassCease);
		}
		
		// mass Cease 
		massCeasesActions() {
       
			const allValid = [...this.template.querySelectorAll('lightning-input')]
			   .reduce((validSoFar, inputCmp) => {
						   inputCmp.reportValidity();
						   return validSoFar && inputCmp.checkValidity();
			   }, true);
	
			   if (allValid) {
			   this.isLoading = true;
			   massCeaseProducts({opportunityLineItemsId: this.selectedCeaseRecords,administrativeEndDate: this.AdministrativeEndDate,contractEndDate: this.contractEndDate})
			   .then(result => {
				   console.log('result ====> ' + result);
				   this.isLoading = false;
				   this.isMassClone = true;
				   this.isMassDelete = true;
				   this.isMassCease=false;
				   this.isMassEdit= true;
				   this.isFinalMsgCease = true;
				   // showing success message
				   
					   showToast(
						   'Success!!', 
						   'Opportunity line items updated.', 
						   'success'
					   );
				   
					 // Clearing selected row indexs 
				   
				  this.template.querySelector('lightning-datatable').selectedRows = [];
				  this.selectedCeaseRecords=[];
				   // refreshing table data using refresh apex
					return refreshApex(this.shoppingCartResults);
	   
			   })
			   .catch(error => {
				   this.isLoading = false;
				   console.log(error);
				   
					   showToast(
						   'Error while updating Products', 
						   error.message, 
						   'error'
					   );
				   
			   });
			}else{
				showToast(
					'Required Fields',
					'Please enter required fields',
					'error',
				   'dismissable'
				);
				
	
			}
		} 
		
		async massEditData(){
			// console.log('this.productId ' + this.productId);
			// console.log('this.opportunityProdRecord.productId ' +this.opportunityEditProdRecord.productId);
			 if(!this.opportunityEditProdRecord.quantity){
				 this.opportunityEditProdRecord.quantity=-1;
			 }
			 if(!this.opportunityEditProdRecord.listprice){
				 this.opportunityEditProdRecord.listprice=-1;
			 }
			 if(!this.opportunityEditProdRecord.discount){
				 this.opportunityEditProdRecord.discount=-1;
			 }
			 if(!this.opportunityEditProdRecord.duration){
				 this.opportunityEditProdRecord.duration=-1;
			 }
			 this.opportunityEditProdRecord.productId=this.productId;
			// console.log('********ProductId ' +  this.opportunityEditProdRecord.productId);
			// console.log('***********new'+JSON.stringify(this.opportunityEditProdRecord));
			 this.isLoading = true;
			 massEditProducts({opportunityLineItemsId:this.selectedProvideRecords ,item: this.opportunityEditProdRecord})
			 .then(result => {
				 console.log('result ====> ' + result);
				 this.isLoading = false;
				 this.isMassClone = true;
				 this.isMassDelete = true;
				 this.isMassProvide=false;
				 this.isMassEdit= true;
				 this.isFinalMsgProvide= true;
				 this.template.querySelector('lightning-datatable').selectedRows = [];
				 // showing success message
				 
					 showToast(
						 'Success!!', 
						 ' Products are updated.', 
						 'success'
					 );
				 
	 
				   // Clearing selected row indexs 
				  // this.template.querySelector('lightning-datatable').selectedRows = [];
				 //this.recordsCount = 0;
				 this.opportunityEditProdRecord = {
					 clc: "",
					 quantity: '',
					 listprice: '',
					 discount: '',
					 duration: '',
					 costCenter: "",
					 billing: true,
					 site: "",
					 productId: "",
				 };
				 this.selectedProvideRecords = [];
				 // refreshing table data using refresh apex
				  return refreshApex(this.shoppingCartResults);
	 
			 })
			 .catch(error => {
				 this.isLoading = false;
				 console.log(error);
				 
					 showToast(
						 'Error while getting Products', 
						  error.message, 
						 'error'
					 );
				 
			 });
		 }  


	//Sorting 
	handleSortdata(event) {
		
		// field name
		this.sortBy = event.detail.fieldName;
       // console.log('sortBy'+this.sortBy);  
		// sort direction
		this.sortDirection = event.detail.sortDirection;
	//	console.log('sortDirection'+this.sortDirection);  
		// calling sortdata function to sort the data based on direction and selected field
		this.sortData(event.detail.fieldName, event.detail.sortDirection);
	}

	sortData(fieldname, direction) {
		
		// serialize the data before calling sort function
		let parseData = JSON.parse(JSON.stringify(this.shownProducts));

		// Return the value stored in the field
		let keyValue = (a) => {
			return a[fieldname];
		};

		// cheking reverse direction
		let isReverse = direction === "asc" ? 1 : -1;
		//console.log('isReverse'+isReverse);  
		// sorting data
		parseData.sort((x, y) => {
			x = keyValue(x) ? keyValue(x) : ""; // handling null values
			y = keyValue(y) ? keyValue(y) : "";

			// sorting values based on direction
			return isReverse * ((x > y) - (y > x));
		});
		//console.log('isReverse'+isReverse);  
		// set the sorted data to data table data
		this.shownProducts = parseData;
	} 

	// Pagination 
	 //clicking on previous button this method will be called
	 previousHandler() {
		
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
    }

    //clicking on next button this method will be called
    nextHandler() {
		
        if((this.page<this.totalPage) && this.page !== this.totalPage){
		
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);            
        }             
	}
	  //this method displays records page by page
	  displayRecordPerPage(page){
		
        /*let's say for 2nd page, it will be => "Displaying 6 to 10 of 23 records. Page 2 of 5"
        page = 2; pageSize = 5; startingRecord = 5, endingRecord = 10
        so, slice(5,10) will give 5th to 9th records.
        */
        this.startingRecord = ((page -1) * this.pageSize) ;
        this.endingRecord = (this.pageSize * page);

        this.endingRecord = (this.endingRecord > this.totalRecountCount) 
                            ? this.totalRecountCount : this.endingRecord; 

        this.shownProducts = this.allProducts.slice(this.startingRecord, this.endingRecord);

        //increment by 1 to display the startingRecord count, 
        //so for 2nd page, it will show "Displaying 6 to 10 of 23 records. Page 2 of 5"
        this.startingRecord = this.startingRecord + 1;

		this.populateCheckbox(this.shownProducts);
	}

	populateCheckbox(shownProducts) {
	//	console.log("start populating checkbox");
	//	console.log("shownProducts: " + JSON.stringify(shownProducts));
		let visibleRows = shownProducts.map((x) => x.id);
		const selectedRows = this.allTrackedSelectedList;

	//	console.log("shownProducts: " + JSON.stringify(visibleRows));
	//	console.log("selectedRows: " + JSON.stringify(selectedRows));

		// Filter all rows that are and selected and visible
		let visibleSelectedRows = visibleRows.filter(function (oppty) {
			let found = false;
			selectedRows.forEach((selectedItem) => {
				if (oppty === selectedItem) {
					found = true;
				}
			});
			return found;
		});

	//	console.log("visibleSelectedRows: " + JSON.stringify(visibleSelectedRows));

		// Make sure the checkboxes are ticket:
		this.visibleSelectedRows = visibleSelectedRows;
	//	console.log("end populating checkbox");
	}
	
	// Mass Price change method 
	async massPriceChangeData(){
			
		
		if(!this.opportunityEditPcRecord.listprice){
			this.opportunityEditPcRecord.listprice=-1;
		}
		if(!this.opportunityEditPcRecord.discount){
			this.opportunityEditPcRecord.discount=-1;
		}
		if(!this.opportunityEditPcRecord.duration){
			this.opportunityEditPcRecord.duration=-1;
		}
		if(!this.opportunityEditPcRecord.invoiceDate){
			this.opportunityEditPcRecord.invoiceDate=null;
		}
	   
		//console.log('***********new'+JSON.stringify(this.opportunityEditPcRecord));
		this.isLoading = true;
		massPriceChangeProducts({opportunityLineItemsId:this.selectedPriceChangeRecords ,item: this.opportunityEditPcRecord})
		.then(result => {
			console.log('result ====> ' + result);
			this.isLoading = false;
			this.isMassClone = true;
			this.isMassDelete = true;
			this.isMassPriceChange=false;
			this.isMassEdit= true;
			this.isFinalMsgPriceChange= true;
			this.template.querySelector('lightning-datatable').selectedRows = [];
			// showing success message
			
				showToast(
					'Success!!', 
					' Products are updated.', 
					'success'
				);
			

			  // Clearing selected row indexs 
			 // this.template.querySelector('lightning-datatable').selectedRows = [];
			//this.recordsCount = 0;
			this.opportunityEditPcRecord = {
				clc: "",
				listprice: '',
				discount: '',
				duration: '',
				costCenter: "",
				billing: true,
				 poNumber:"",
			   invoiceDate:""
			};
			this.selectedProvideRecords = [];
			// refreshing table data using refresh apex
			 return refreshApex(this.shoppingCartResults);

		})
		.catch(error => {
			this.isLoading = false;
			console.log(error);
			
				showToast(
					'Error while getting Products', 
					 error.message, 
					'error'
				);
			
		});
	}  
	
	changeActivityValues(event){
		this.isClc=true;
	 let activityName =event.target.name;
	 let activityvalues = event.target.value;
	 console.log('***********activityName'+activityName);
	// console.log('***********activityvalues'+activityvalues);
			if(activityvalues==='Provide'){
				this.isClc=false;
				this.massActivityType=event.target.value
				this.clcForActOptions = [
					{ value: "Acq", label: "Acq" },
					{ value: "Ret", label: "Ret" },
					{ value: "Mig", label: "Mig" }
				]; 
			}
			if(activityvalues==='Price Change'){
				this.isClc=false;
				this.massActivityType=event.target.value
				this.clcForActOptions = [
					{ value: "Ret", label: "Ret" },
					{ value: "Mig", label: "Mig" }
				];
				
			}
			if(activityvalues==='Cease'){
				this.isClc=false;
				this.massActivityType=event.target.value
				this.clcForActOptions = [
					{ value: "Churn", label: "Churn" }
				];
			}
	}

	massActivityActions(event){
		
			//console.log('**************Activity'+this.massActivityType);
			// console.log('**************clc'+this.massClc);
			// console.log('**************clc'+this.selectedCeaseRCOCRecords);
			 
			
			 this.isLoading = true;
			if(!this.massActivityType){
				this.isLoading = false;
				showToast(
					'Required Field',
					'Please select Activity Type field',
					'error',
				   'dismissable'
				);
				
			}else if(!this.massClc){
				this.isLoading = false;
				showToast(
					'Required Field',
					'Please select CLC field',
					'error',
				   'dismissable'
				);	
			} 
		/*	if(this.selectedCeaseRCOCRecords.length>0){

				this.isLoading = false;
				showToast(
					'Validation',
					'You can not change OC  Opportunity Product activity types',
					'error',
				   'dismissable'
				);
			 }else{*/
			if(this.massActivityType && this.massClc){
			//	massActivityProducts({OpportunityLineItemsId: this.selectedRecords,massActivityType: this.massActivityType,massClc: this.massClc})
			massActivityProducts({opportunityLineItemsId: this.selectedCeaseRCOCRecords,massActivityType: this.massActivityType,massClc: this.massClc})
			.then(result => {
					console.log('result ====> ' + result);
					this.isLoading = false;
					this.isMassClone = true;
					this.isMassDelete = true;
					this.isMassCease=false;
					this.isMassEdit= true;
					this.isFinalMsgActivity = true;
					this.isMassEditActivity=false;
					this.massActivityType="";
					this.massClc="";
					this.isClc=true;
					// showing success message
					
						showToast(
							'Success!!', 
							'Opportunity line items updated.', 
							'success'
						);
					
					  // Clearing selected row indexs 
					
				   this.template.querySelector('lightning-datatable').selectedRows = [];
				   this.selectedRecords=[];
					// refreshing table data using refresh apex
					 return refreshApex(this.shoppingCartResults);
		
				})
				.catch(error => {
					this.isLoading = false;
					console.log(error);
					
						showToast(
							'Error while updating Products', 
							error.message, 
							'error'
						);
					
				});

			}
		//}
	}
}