import { Labels } from "./labels.js";

let labels = new Labels();
/**
 * Dynamic row Actions
 *
 */
let getRowActions = (row, doneCallback) => {
	
	if (row.customerAssetId && row.activityType !== "Provide") {
		doneCallback([
			{ label: labels.edit, iconName: "utility:edit", name: "edit" },
			{ label: labels.delete, iconName: "utility:delete", name: "delete" }
		]);
	} else if(row.activityType === "Provide" && row.customerAssetId) {
		doneCallback([
			{ label: labels.edit, iconName: "utility:edit", name: "edit" },
			{
				label: labels.delete,
				iconName: "utility:delete",
				name: "delete"
			},
			
		]);
	}else if(row.activityType === "Provide" && row.customerAssetId ===undefined) {
		doneCallback([
			{ label: labels.edit, iconName: "utility:edit", name: "edit" },
			{
				label: labels.delete,
				iconName: "utility:delete",
				name: "delete"
			},
			{ label: labels.clone, iconName: "utility:copy", name: "copy" }
		]);
	}
};

const OPPTY_COLUMNS = [
	{
		label: "",
		fieldName: "description",
		type: "text",
		sortable: false,
		hideDefaultActions: true,
		cellAttributes: {
			alignment: "right"
		}
	},
	{
		label: "Acq",
		fieldName: "opptyAcq",
		type: "currency",
		sortable: false,
		hideDefaultActions: true
	},
	{
		label: "Acq-total",
		fieldName: "opptyTotalAcq",
		type: "currency",
		sortable: false,
		hideDefaultActions: true
	},
	{
		label: "Ret",
		fieldName: "opptyRet",
		type: "currency",
		sortable: false,
		hideDefaultActions: true
	},
	{
		label: "Ret-total",
		fieldName: "opptyTotalRet",
		type: "currency",
		sortable: false,
		hideDefaultActions: true
	},
	{
		label: "Mig",
		fieldName: "opptyMig",
		type: "currency",
		sortable: false,
		hideDefaultActions: true
	},
	{
		label: "Mig-total",
		fieldName: "opptyTotalMig",
		type: "currency",
		sortable: false,
		hideDefaultActions: true
	},
	{
		label: "All",
	//	fieldName: "opptyAll",
	fieldName: "totalAcq",
		type: "currency",
		sortable: false,
		hideDefaultActions: true
	},
	{
		label: "opptyAllTotal",
		fieldName: "totalAcq",
		type: "currency",
		sortable: false,
		hideDefaultActions: true
	}
];

const LINE_ITEM_COLUMNS = [
	{
		label: "action",
		type: "action",
		initialWidth: "150px",
		typeAttributes: {
			rowActions: getRowActions,
			menuAlignment: "left"
		},
		cellAttributes: { alignment: "left" }
	},
	/*{
		label: "Activity Type",
		fieldName: "activityType",
		wrapText: true,
		type: "picklist",
		sortable: true,
		editable: true,
		typeAttributes: {
			placeholder: { fieldName: "activityType" },
			alignment: "right",
			value: { fieldName: "activityType" },
			context: { fieldName: "id" },
			providerSObjectFieldCombi: "OpportunityLineItem:Activity_Type__c"
		}
	},*/
	{
		label: "Activity Type",
		fieldName: "activityType",
		type: "text",
		sortable: true
	},
	/*{
		label: "CLC",
		fieldName: "clc",
		type: "picklist",
		wrapText: true,
		sortable: true,
		editable: true,
		typeAttributes: {
			placeholder: { fieldName: "clc" },
			alignment: "right",
			value: { fieldName: "clc" }, // default value for picklist
			context: { fieldName: "id" }, // binding account Id with context variable to be returned back
			providerSObjectFieldCombi: "OpportunityLineItem:CLC__c"
		}
	},*/
	{
		label: "CLC",
		fieldName: "clc",
		type: "text",
		sortable: true
	},
	{
		initialWidth: 250,
		label: "Product",
		fieldName: "productLabel",
		type: "text",
		sortable: true
	},
	{ initialWidth: 250, label: "Site", fieldName: "siteLabel", type: "text", sortable: true },
	{
		//initialWidth: 50,
		label: "Quantity",
		fieldName: "quantity",
		type: "number",
		sortable: true
		
		
	},
	{
		label: "List Price",
		fieldName: "listprice",
		type: "currency",
		typeAttributes: { currencyCode: "EUR" },
		sortable: true
	},
	{
		//label: "Discount",
		label: "Discount(%)",
	//	fieldName: "discount",
	    fieldName: "discountHidden",
		type: "percent",
		//type: "currency",
		sortable: true,
		typeAttributes: {
			step: 1
		}
		
	},
	{
		label: "Duration",
		fieldName: "duration",
		type: "number",
		sortable: true
	},
	{
		label: "ARPU/Value",
		fieldName: "arpuValue",
		type: "decimal",
		sortable: true
	
	},
	{
		label: "Total price",
		fieldName: "totalPrice",
		type: "currency",
		typeAttributes: { currencyCode: "EUR" },
		sortable: true
	},
	{
		label: "Customer AssetId",
		fieldName: "customerAssetId",
		type: "text",
		sortable: true
	},
];

export { OPPTY_COLUMNS, LINE_ITEM_COLUMNS };