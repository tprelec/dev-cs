import { LightningElement, api } from "lwc";

export default class OrderEntryAccountAndContact extends LightningElement {
	@api state;

	// Call the validation on Contact child component as Account component is read only
	@api validate() {
		return this.template.querySelector("c-order-entry-contact-details").validate();
	}

	updateState(e) {
		this.dispatchEvent(new CustomEvent("statechanged", { detail: e.detail, bubbles: false }));
	}
}