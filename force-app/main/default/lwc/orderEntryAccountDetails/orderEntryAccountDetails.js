import { LightningElement, api, track, wire } from "lwc";
import getAccountData from "@salesforce/apex/OrderEntryController.getAccountData";

export default class OrderEntryAccountDetails extends LightningElement {
	@api state;
	@track account;

	@wire(getAccountData, { accId: "$state.accountId" })
	getAccData({ error, data }) {
		if (data) {
			this.account = data;
		} else if (error) {
			this.error = error;
		}
	}
}