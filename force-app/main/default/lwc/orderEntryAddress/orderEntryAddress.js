import { LightningElement, api, track } from "lwc";

export default class OrderEntryAddress extends LightningElement {
	@api state;
	@track adrinfo;
	@track loading = false;

	@api validate() {
		if (this.validateAddressCheck()) {
			return this.validateInstallationAddress();
		}
		return false;
	}

	validateInstallationAddress() {
		return this.template
			.querySelector("c-order-entry-installation-address")
			.validate()
			.then((result) => {
				return result;
			});
	}

	validateAddressCheck() {
		return this.template.querySelector("c-order-entry-address-check").validate();
	}

	updateState(e) {
		this.dispatchEvent(new CustomEvent("statechanged", { detail: e.detail, bubbles: false }));
	}
}