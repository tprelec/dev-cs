import { LightningElement, api, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import siteCheck from "@salesforce/apex/OrderEntryController.siteCheck";
import getSite from "@salesforce/apex/OrderEntryController.getSite";

export default class OrderEntryAddressCheck extends LightningElement {
	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = value;
		this.setComboboxValues();
		this.getSelectedSiteData();
	}

	@track _state = null;
	// Holds the selected site info
	@track addressinfo = {
		Site_Postal_Code__c: "",
		Site_House_Number__c: "",
		Site_House_Number_Suffix__c: "",
		Site_Street__c: "",
		Site_City__c: ""
	};
	@track loading = false;
	@track addressCheckResult = "";
	@track offNetType = "";
	// Holds the address check results
	@track adrCheckRes;

	get offNetTypeDissabled() {
		if (this.addressCheckResult === "Off-Net") {
			return false;
		} else {
			return true;
		}
	}
	set offNetTypeDissabled(value) {
		this.setAttribute("offNetTypeDissabled", value);
	}

	get offNetTypeRequired() {
		if (this.addressCheckResult === "Off-Net") {
			return true;
		} else {
			return false;
		}
	}
	set offNetTypeRequired(value) {
		this.setAttribute("offNetTypeRequired", value);
	}

	get resetValError() {
		if (this.addressCheckResult === "Off-Net") {
			return true;
		} else {
			return false;
		}
	}
	set resetValError(value) {
		this.setAttribute("offNetTypeRequired", value);
	}

	get addressCheckResultOptions() {
		return [
			{
				label: "On-Net",
				value: "On-Net"
			},
			{
				label: "Off-Net",
				value: "Off-Net"
			}
		];
	}

	get offNetTypeOptions() {
		return [
			{
				label: "MTC",
				value: "MTC"
			},
			{
				label: "Near-Net",
				value: "Near-Net"
			},
			{
				label: "BVG",
				value: "BVG"
			}
		];
	}

	handleAddressCheck(event) {
		this.addressCheckResult = event.target.value;
		this.offNetType = "";

		if (event.target.value === "Off-Net") {
			this.offNetTypeDissabled = false;
			this.offNetTypeRequired = true;
			this.resetValError = true;
		} else {
			this.resetValError = false;
			this.offNetTypeDissabled = true;
			this.offNetTypeRequired = false;
		}

		// Reflect changes to state
		const stateClone = Object.assign({}, this.state);
		stateClone.addressCheckResult = event.target.value;
		stateClone.offNetType = "";
		this.dispatchEvent(new CustomEvent("statechanged", { detail: stateClone, bubbles: false }));
	}

	handleOffNetChange(event) {
		this.offNetType = event.target.value;

		// Reflect changes to state
		const stateClone = Object.assign({}, this.state);
		stateClone.offNetType = event.target.value;
		this.dispatchEvent(new CustomEvent("statechanged", { detail: stateClone, bubbles: false }));
	}

	setComboboxValues() {
		this.addressCheckResult = this.state.addressCheckResult;
		this.offNetType = this.state.offNetType;
	}

	getSelectedSiteData() {
		if (this.state.siteId && this.state.siteId !== this.addressinfo.Id) {
			getSite({ siteId: this.state.siteId })
				.then((data) => {
					this.addressinfo = data;

					if (
						!this.state.siteCheck ||
						this.state.siteCheck.zipCode !== data.Site_Postal_Code__c ||
						this.state.siteCheck.houseNumber !== data.Site_House_Number__c + ""
					) {
						this.newAddressCheck();
					}
				})
				.catch((error) => {
					this.dispatchEvent(
						new ShowToastEvent({
							title: error.description,
							message: error.message,
							variant: "warning"
						})
					);
					console.error("Error: " + JSON.stringify(error));
				});
		}
	}

	newAddressCheck() {
		if (this.addressinfo.Site_Postal_Code__c) {
			this.loading = true;
			siteCheck({
				postCode: this.addressinfo.Site_Postal_Code__c,
				houseNumber: this.addressinfo.Site_House_Number__c
			})
				.then((data) => {
					this.loading = false;
					this.parseResult(JSON.parse(data));
				})
				.catch((error) => {
					this.loading = false;
					this.dispatchEvent(
						new ShowToastEvent({
							title: error.description,
							message: error.message,
							variant: "warning"
						})
					);
					console.error("Error: " + JSON.stringify(error));
				});
		} else {
			this.dispatchEvent(
				new ShowToastEvent({
					title: "No Site Selected",
					message: "Please select a Site",
					variant: "warning"
				})
			);
		}
	}

	parseResult(data) {
		this.adrCheckRes = data;
		if (data.availability) {
			// Based on the returned data set the Address Check dropdown accordingly
			data.availability.forEach((item, index) => {
				if (item.name === "internet") {
					if (item.available) {
						this.addressCheckResult = "On-Net";
						this.offNetType = "";
						this.offNetTypeDissabled = true;
						this.offNetTypeRequired = false;
					} else {
						this.addressCheckResult = "Off-Net";
						this.offNetType = "";
						this.offNetTypeDissabled = false;
						this.offNetTypeRequired = true;
					}
				}
			});

			// Update state
			const stateClone = Object.assign({}, this.state);
			stateClone.siteCheck = this.adrCheckRes;
			stateClone.addressCheckResult = this.addressCheckResult;
			stateClone.offNetType = "";
			if (this.offNetTypeRequired) {
				stateClone.offNetType = this.offNetType;
			}
			this.dispatchEvent(
				new CustomEvent("statechanged", { detail: stateClone, bubbles: false })
			);
		} else {
			this.addressCheckResult = "";
			this.offNetType = "";

			// Even if the site check fail save the result with blank availibility
			const stateClone = Object.assign({}, this.state);
			stateClone.siteCheck = {
				street: this.addressinfo.Site_Street__c,
				houseNumber: this.addressinfo.Site_House_Number__c,
				houseNumberExt: this.addressinfo.Site_House_Number_Suffix__c,
				zipCode: this.addressinfo.Site_Postal_Code__c,
				city: this.addressinfo.Site_City__c,
				status: [],
				availability: [],
				footprint: "ZIGGO"
			};
			stateClone.addressCheckResult = "";
			stateClone.offNetType = "";
			this.dispatchEvent(
				new CustomEvent("statechanged", { detail: stateClone, bubbles: false })
			);

			this.dispatchEvent(
				new ShowToastEvent({
					title: data.description,
					message: data.message,
					variant: "warning"
				})
			);
		}
	}

	@api validate() {
		let result = true;

		const addressCheck = this.template.querySelector("lightning-combobox.address-check-result");
		const offNetType = this.template.querySelector("lightning-combobox.off-net-type");

		if (!this.addressCheckResult) {
			result = false;
			addressCheck.setCustomValidity("Please run address check or select values manually.");
		} else if (this.offNetTypeRequired && !this.offNetType) {
			result = false;
			addressCheck.setCustomValidity("");
		} else {
			result = true;
			addressCheck.setCustomValidity("");
		}
		addressCheck.reportValidity();
		if (offNetType) {
			offNetType.reportValidity();
		}
		return result;
	}
}