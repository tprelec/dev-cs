import { api, track, LightningElement } from "lwc";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryBundlePromotion extends LightningElement {
	@api state = null;
	@api promotions = [];

	get b2cChecked() {
		return this.state?.b2cInternetCustomer;
	}

	get picklistPlaceholder() {
		return this.promotions && this.promotions.length > 0
			? "Select Bundle Promotion"
			: "No Bundle Promotions available";
	}

	get formattedPromotions() {
		return this.promotions.map((bp) => ({
			value: bp.Id,
			label: bp.Promotion_Name__c
		}));
	}

	connectedCallback() {
		const bundlePromotion =
			this.state && this.state.promos
				? this.state.promos.find((p) => p.type === CONSTANTS.STANDARD_PROMOTION_TYPE)
				: null;

		this.selectedPromotion = bundlePromotion ? bundlePromotion.id : null;
	}

	/**
	 * Handle change on bundle promotion picklist
	 */
	handlePromotionChange(e) {
		this.selectPromotion(e.detail.value);
	}

	/**
	 * Set selected bundle promotion and dispatch to parent components
	 */
	selectPromotion(id) {
		const type = CONSTANTS.STANDARD_PROMOTION_TYPE;
		const promotionData = { id, type };
		this.dispatchEvent(
			new CustomEvent("selectedpromotion", { detail: promotionData, bubbles: false })
		);
	}

	handleB2cChange(e) {
		this.dispatchEvent(
			new CustomEvent("b2ctoggle", { detail: e.detail.checked, bubbles: false })
		);
	}

	handleRemovePromotion() {
		const promotionPicklist = this.template.querySelector(
			"lightning-combobox.bundle-promotions"
		);
		if (promotionPicklist.value) {
			promotionPicklist.value = null;
			this.selectPromotion(null);
		}
	}
}