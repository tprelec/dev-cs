/**
 * a util class for storing Constants
 */
const getConstants = () => {
	return {
		MAIN_PRODUCT_TYPE: "Main",
		INTERNET_PRODUCT_TYPE: "Internet",
		TELEPHONY_PRODUCT_TYPE: "Telephony",
		SECONDARY_TELEPHONY_PRODUCT_TYPE: "Secondary Telephony",
		TV_PRODUCT_TYPE: "TV",

		INTERNET_ADDITIONAL_TYPE: "Internet Additional",
		INTERNET_SECURITY_TYPE: "Internet Security",
		PIN_ZEKER_TYPE: "Pin Zeker",
		TV_CHANNELS_TYPE: "TV Channels",
		TV_MEDIABOX_TYPE: "TV Mediabox",
		TELEPHONY_LINE_TYPE: "Telephony Line",

		SELECT_PRODUCT: "Select Product",
		ACCOUNT_CONTACT: "Account & Contact",
		ADDRESS: "Address",
		PRODUCT_CONFIG: "Product Configuration",
		DISCOUNT_PAYMENT: "Discount & Payment",
		INSTALLATION: "Installation",
		QUOTE_SIGNATURE: "Quote & Signature",

		FP1000_CHECK: "fp1000",
		ON_NET_CHECK: "On-Net",
		GIGA_INTERNET: "Zakelijk Internet Giga",

		STANDARD_PROMOTION_TYPE: "Standard",
		SPECIAL_PROMOTION_TYPE: "Special",

		INSTALLATION_DATE_DAYS_DEFAULT: 14,
		INSTALLATION_DATE_DAYS_OVERSTAPSERVICE: 31
	};
};

export { getConstants };