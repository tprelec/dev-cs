import { LightningElement, api, track, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getContactData from "@salesforce/apex/OrderEntryController.getContactData";
import getPrimaryContact from "@salesforce/apex/OrderEntryController.getPrimaryContact";
import saveContact from "@salesforce/apex/OrderEntryController.saveContact";

export default class OrderEntryContactDetails extends LightningElement {
	@api state;
	@track loading = false;
	@track contactSelected = false;
	@track openSelectionModal = false;
	@track openModal = false;
	@track contacts = [];
	@track contactsList = [];
	@track primaryContact = {
		Id: "",
		FirstName: "",
		LastName: "",
		MobilePhone: "",
		Phone: "",
		Email: ""
	};

	connectedCallback() {
		if (this.state.primaryContactId) {
			this.getPrimContactData();
		}
	}

	@wire(getContactData, { accId: "$state.accountId" })
	getContData({ error, data }) {
		if (data) {
			if (this.contacts.length === 0) {
				this.contacts = data;
			}
		} else if (error) {
			this.handleError(error);
		}
	}

	getPrimContactData() {
		getPrimaryContact({ contId: this.state.primaryContactId })
			.then((result) => {
				this.primaryContact = {
					Id: result.Id,
					FirstName: result.FirstName,
					LastName: result.LastName,
					MobilePhone: result.MobilePhone,
					Phone: result.Phone,
					Email: result.Email
				};
			})
			.catch((error) => {
				this.handleError(error);
			});
	}

	handleFormInputChange(event) {
		this.primaryContact[event.target.name] = event.target.value;
	}

	selectContact() {
		this.openSelectionModal = true;
	}
	newContact() {
		this.openModal = true;
	}

	closeModal(event) {
		const isNew = this.openModal;
		this.openModal = false;
		this.openSelectionModal = false;
		if (event.detail.Id) {
			this.primaryContact = {
				Id: event.detail.Id,
				FirstName: event.detail.FirstName,
				LastName: event.detail.LastName,
				MobilePhone: event.detail.MobilePhone,
				Phone: event.detail.Phone,
				Email: event.detail.Email
			};
			if (isNew) {
				let tempContacts = [...this.contacts];

				tempContacts.push(this.primaryContact);

				this.contacts = tempContacts;
			}
			// Change the contact id on state
			const stateClone = Object.assign({}, this.state);
			stateClone.primaryContactId = this.primaryContact.Id;
			this.dispatchEvent(
				new CustomEvent("statechanged", { detail: stateClone, bubbles: false })
			);
		}
	}

	@api validate() {
		this.loading = true;

		let isValid = true;
		this.template.querySelectorAll("lightning-input").forEach((element) => {
			element.reportValidity();
			isValid = isValid && element.checkValidity();
		});

		// If no contact is selected as primary invalidate
		if (!this.primaryContact.Id) {
			this.handleError("There is no primary contact selected.");
			return false;
		}

		return saveContact({ newContact: this.primaryContact, oppId: this.state.opportunityId })
			.then((result) => {
				if (result) {
					return true;
				}
			})
			.catch((error) => {
				this.loading = false;
				for (const key in error.body.fieldErrors) {
					this.handleError(error.body.fieldErrors[key][0].message);
				}
				return false;
			});
	}

	handleError(error) {
		console.error("Error", error);
		this.dispatchEvent(
			new ShowToastEvent({
				title: "Something went wrong",
				message: error,
				variant: "warning"
			})
		);
	}
}