import { api, track, LightningElement } from "lwc";
import getBundlePromotions from "@salesforce/apex/OrderEntryController.getBundlePromotions";
import getSpecialPromotions from "@salesforce/apex/OrderEntryController.getSpecialPromotions";
import getBankAccountHolder from "@salesforce/apex/OrderEntryController.getBankAccountHolder";
import createDiscountRelatedRecords from "@salesforce/apex/OrderEntryController.createDiscountRelatedRecords";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryDiscountAndPayment extends LightningElement {
	@api state = null;

	@api async afterSave() {
		await createDiscountRelatedRecords({ opportunityId: this._state.opportunityId });
	}

	@api validate() {
		const paymentDetailsCmp = this.template.querySelector("c-order-entry-payment-details");
		return paymentDetailsCmp.validate();
	}

	@track _state = null;
	@track bankAccountHolder;
	@track bundlePromotions = [];
	@track specialPromotions = [];
	@track specialPromotionsExist = false;
	@track showLoader = true;

	// Set default values
	connectedCallback() {
		this.init();
	}

	async init() {
		if (this.state) {
			this._state = JSON.parse(JSON.stringify(this.state));

			if (!this._state.b2cInternetCustomer) {
				this._state.b2cInternetCustomer = false;
			}

			try {
				this.bankAccountHolder = await getBankAccountHolder({
					contactId: this._state.primaryContactId
				});

				this.bundlePromotions = await this.getBundlePromotions();
				this.specialPromotions = await this.getSpecialPromotions();

				this.filterAvailablePromotions(
					this.specialPromotions,
					CONSTANTS.STANDARD_PROMOTION_TYPE
				);
				this.filterAvailablePromotions(
					this.bundlePromotions,
					CONSTANTS.SPECIAL_PROMOTION_TYPE
				);
				this.updateSpecialPromotionsExist();
				this.showLoader = false;
			} catch (e) {
				this.dispatchEvent(
					new ShowToastEvent({
						title: "Error occured",
						message: e.body ? e.body.message : "",
						variant: "error",
						mode: "sticky"
					})
				);
			}

			if (!this._state.promos) {
				this._state.promos = [];
			}
			if (!this._state.payment) {
				this._state.payment = {
					bankAccountHolder: this.bankAccountHolder,
					iban: "",
					paymentType: "Direct Debit",
					billingChannel: "Electronic Bill"
				};
			}

			this.dispatchStateChange();
		}
	}

	async getBundlePromotions() {
		return await getBundlePromotions({
			opportunityId: this._state.opportunityId,
			b2cChecked: this._state.b2cInternetCustomer,
			products: this._state.products,
			addons: this._state.addons
		});
	}

	async getSpecialPromotions() {
		return await getSpecialPromotions({
			opportunityId: this._state.opportunityId,
			products: this._state.products,
			addons: this._state.addons
		});
	}

	updateSpecialPromotionsExist() {
		this.specialPromotionsExist = this.specialPromotions && this.specialPromotions.length;
	}

	/**
	 * Handle selection of promotions from child components
	 */
	async handleSelectedPromotion(e) {
		if (e.detail.id) {
			const promotionData = this.buildPromotionData(e.detail);
			this.setPromotion(promotionData);
		} else {
			// Deselect bundle promotion
			let index = -1;
			index = this._state.promos.findIndex((p) => p.type === e.detail.type);
			this._state.promos.splice(index, 1);
		}

		this.showLoader = true;
		this.bundlePromotions = await this.getBundlePromotions();
		this.specialPromotions = await this.getSpecialPromotions();

		this.filterAvailablePromotions(this.specialPromotions, CONSTANTS.STANDARD_PROMOTION_TYPE);
		this.filterAvailablePromotions(this.bundlePromotions, CONSTANTS.SPECIAL_PROMOTION_TYPE);
		this.updateSpecialPromotionsExist();
		this.showLoader = false;
		this.dispatchStateChange();
	}

	buildPromotionData({ id, type }) {
		const promotion =
			type === CONSTANTS.STANDARD_PROMOTION_TYPE
				? this.bundlePromotions.find((bp) => bp.Id === id)
				: this.specialPromotions.find((sp) => sp.Id === id);

		if (promotion) {
			return {
				id,
				type,
				name: promotion.Promotion_Name__c,
				contractTerms: promotion.Contract_Term__c,
				customerType: promotion.Customer_Type__c,
				connectionType: promotion.Connection_Type__c,
				offNetType: promotion.Off_net_Type__c,
				discounts: this.buildDiscountData(promotion.Discounts__r)
			};
		}
		return null;
	}

	buildDiscountData(discounts) {
		return discounts.map((d) => ({
			id: d.Discount__c,
			name: d.Discount__r.Name,
			type: d.Discount__r.Type__c,
			value: d.Discount__r.Value__c,
			duration: d.Discount__r.Duration__c,
			level: d.Discount__r.Level__c,
			product: d.Discount__r.Product__c,
			addon: d.Discount__r.Add_On__c
		}));
	}

	setPromotion(promotionData) {
		if (promotionData) {
			const promotions = this._state.promos || [];

			if (promotionData.type === CONSTANTS.STANDARD_PROMOTION_TYPE) {
				let bundlePromotionIndex = -1;
				bundlePromotionIndex = promotions.findIndex(
					(p) => p.type === CONSTANTS.STANDARD_PROMOTION_TYPE
				);

				if (bundlePromotionIndex === -1) {
					promotions.push(promotionData);
				} else {
					promotions[bundlePromotionIndex] = promotionData;
				}
			} else {
				let specialPromotionIndex = -1;
				specialPromotionIndex = promotions.findIndex((p) => p.id === promotionData.id);

				if (specialPromotionIndex === -1) {
					promotions.push(promotionData);
				}
			}

			this._state.promos = promotions;
		}
	}

	/**
	 * Handle removal of special promotions from child component
	 */
	async handleRemoveSpecialPromotion(e) {
		let index = -1;
		index = this._state.promos.findIndex((p) => p.id === e.detail);
		this._state.promos.splice(index, 1);

		this.showLoader = true;
		this.bundlePromotions = await this.getBundlePromotions();
		this.filterAvailablePromotions(this.bundlePromotions, CONSTANTS.SPECIAL_PROMOTION_TYPE);
		this.updateSpecialPromotionsExist();
		this.showLoader = false;
		this.dispatchStateChange();
	}

	/**
	 * Handle toggle of B2C Internet Cuwtomer checkbox from child component
	 */
	async handleB2cToggle(e) {
		this._state.b2cInternetCustomer = e.detail;
		this.showLoader = true;
		this.bundlePromotions = await this.getBundlePromotions();
		this.specialPromotions = await this.getSpecialPromotions();

		this.removeUnavailablePromotions(this._state, this.bundlePromotions, this.specialPromotions);
		this.showLoader = false;
		this.dispatchStateChange();
	}

	/**
	 * Handle change of payment details from child component
	 */
	handlePaymentChange(e) {
		if (e.detail.element === "bankAccountHolder") {
			this._state.payment.bankAccountHolder = e.detail.value;
		} else if (e.detail.element === "paymentType") {
			this._state.payment.paymentType = e.detail.value;
		} else if (e.detail.element === "iban") {
			this._state.payment.iban = e.detail.value;
		} else if (e.detail.element === "billingChannel") {
			this._state.payment.billingChannel = e.detail.value;
		}
		this.dispatchStateChange();
	}

	/**
	 * Dispatch changes on state to parent components
	 */
	dispatchStateChange() {
		this.dispatchEvent(
			new CustomEvent("statechanged", { detail: this._state, bubbles: false })
		);
	}

	filterAvailablePromotions(promotions, type) {
		let selectedProdsAddons = [];

		if (this._state.promos) {
			this._state.promos.forEach((p) => {
				p.discounts.forEach((d) => {
					if (p.type === type) {
						if (d.product) {
							selectedProdsAddons.push(d.product);
						} else if (d.addon) {
							selectedProdsAddons.push(d.addon);
						}
					}
				});
			});
		}

		if (promotions) {
			promotions.forEach((p, index) => {
				p.Discounts__r.forEach((d) => {
					if (d.Discount__r) {
						if (
							(d.Discount__r.Product__c &&
								selectedProdsAddons.includes(d.Discount__r.Product__c)) ||
							(d.Discount__r.Add_On__c &&
								selectedProdsAddons.includes(d.Discount__r.Add_On__c))
						) {
							promotions.splice(index, 1);
						}
					}
				});
			});
		}
	}

	removeUnavailablePromotions(_state, bundlePromotions, specialPromotions) {
		if (_state && _state.promos && _state.promos.length > 0) {
			_state.promos.forEach((p, index) => {
				let promoFound = -1;
				promoFound =
					p.type === CONSTANTS.STANDARD_PROMOTION_TYPE
						? bundlePromotions.findIndex((bp) => bp.Id === p.id)
						: specialPromotions.findIndex((sp) => sp.Id === p.id);

				if (promoFound === -1) {
					_state.promos.splice(index, 1);
				}
			});
		}
	}
}