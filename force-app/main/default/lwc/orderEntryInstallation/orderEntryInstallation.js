import { LightningElement, api, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import saveOpportunityDescription from "@salesforce/apex/OrderEntryController.saveOpportunityDescription";

export default class OrderEntryInstallation extends LightningElement {
	@track _state = null;

	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = JSON.parse(JSON.stringify(value));
	}

	@api
	validate() {
		var resultInformation = this.template
			.querySelector("c-order-entry-installation-information")
			.validate();

		if (!resultInformation) {
			return false;
		}

		saveOpportunityDescription({
			opportunityId: this.state.opportunityId,
			notes: this.state.notes
		})
			.then((res) => {
				if (res) {
					resultInformation = true;
				}
			})
			.catch((error) => {
				this.loading = false;
				console.log(error);
				this.handleError(error.body.pageErrors[0].message);
				resultInformation = false;
			});

		return resultInformation;
	}

	connectedCallback() {
		if (this.state) {
			if (!this.state.installation) {
				this.state.installation = {
					earliestInstallationDate: "",
					assignTechnician: true,
					preferredDate1: {
						selectedDate: "",
						dayPeriod: "morning"
					},
					preferredDate2: {
						selectedDate: "",
						dayPeriod: "morning"
					},
					preferredDate3: {
						selectedDate: "",
						dayPeriod: "morning"
					}
				};
			}
			if (!this.state.operatorSwitch) {
				this.state.operatorSwitch = {
					requested: false,
					currentProvider: "",
					currentContractNumber: "",
					potentialFeeAccepted: false
				};
			}
			if (!this.state.notes) {
				this.state.notes = "";
			}
		}
	}

	installationInformationChange(e) {
		if (typeof e.detail.value !== "object") {
			this.state.installation[e.detail.key] = e.detail.value;
		} else {
			this.state.installation[e.detail.key] = Object.assign({}, e.detail.value);
		}

		this.dispatchEvent(new CustomEvent("statechanged", { detail: this.state, bubbles: false }));
	}
	preferredDates(e) {
		var prefeDates = Object.assign({}, e.detail);
		Object.keys(prefeDates).forEach((k) => {
			this.state.installation[k] = Object.assign({}, prefeDates[k]);
		});

		this.dispatchEvent(new CustomEvent("statechanged", { detail: this.state, bubbles: false }));
	}

	overstapservicestagechange(e) {
		var overstapservicestage = Object.assign({}, e.detail);
		Object.keys(overstapservicestage).forEach((k) => {
			this.state.operatorSwitch[k] = overstapservicestage[k];
		});

		this.dispatchEvent(new CustomEvent("statechanged", { detail: this.state, bubbles: false }));
	}

	notesChange(e) {
		this.state.notes = e.detail;

		this.dispatchEvent(new CustomEvent("statechanged", { detail: this.state, bubbles: false }));
	}
}