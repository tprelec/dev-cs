import { LightningElement, api, track, wire } from "lwc";
import { refreshApex } from "@salesforce/apex";
import { loadScript, loadStyle } from "lightning/platformResourceLoader";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getSites from "@salesforce/apex/OrderEntryController.getSites";
import updateSite from "@salesforce/apex/OrderEntryController.updateSite";
import orderEntryAssets from "@salesforce/resourceUrl/orderEntryAssets";

export default class OrderEntryInstallationAddress extends LightningElement {
	@api state;
	@track loading = false;
	@track openSelectionModal = false;
	@track openCreationModal = false;
	@track sites = [];
	@track selectedSite = {
		Id: "",
		Site_Street__c: "",
		Site_Postal_Code__c: "",
		Site_House_Number__c: "",
		Site_City__c: "",
		Site_House_Number_Suffix__c: ""
	};
	wiredSites;

	async renderedCallback() {
		await Promise.all([
			loadStyle(this, orderEntryAssets + "/orderEntryInstallationAddress.css")
		]);
	}

	@wire(getSites, { accId: "$state.accountId" })
	getSites(value) {
		this.wiredSites = value;
		const { data, error } = value;
		if (data) {
			this.sites = data;
			data.map((item, index) => {
				if (this.state.siteId === item.Id) {
					this.selectedSite = { ...item };
				}
			});
		} else if (error) {
			this.error = error;
		}
	}

	handleFormInputChange(event) {
		this.selectedSite[event.target.name] = event.target.value;
	}

	selectSite() {
		this.openSelectionModal = true;
	}

	newSite() {
		this.openCreationModal = true;
	}

	closeModal(event) {
		const isNew = this.openCreationModal;
		this.openCreationModal = false;
		this.openSelectionModal = false;
		if (event.detail.Id) {
			this.selectedSite = {
				Id: event.detail.Id,
				Site_Street__c: event.detail.Site_Street__c,
				Site_Postal_Code__c: event.detail.Site_Postal_Code__c,
				Site_House_Number__c: event.detail.Site_House_Number__c,
				Site_City__c: event.detail.Site_City__c,
				Site_House_Number_Suffix__c: event.detail.Site_House_Number_Suffix__c
			};

			if (isNew) {
				refreshApex(this.wiredSites);
			}
			// Change the site id on state
			const stateClone = Object.assign({}, this.state);
			stateClone.siteId = this.selectedSite.Id;
			this.dispatchEvent(
				new CustomEvent("statechanged", { detail: stateClone, bubbles: false })
			);
		}
	}

	@api validate() {
		this.loading = true;
		return updateSite({
			updatedSite: this.selectedSite,
			opportunityId: this.state.opportunityId
		})
			.then((result) => {
				if (result) {
					this.loading = false;
					return true;
				}
			})
			.catch((error) => {
				this.loading = false;

				this.dispatchEvent(
					new ShowToastEvent({
						title: "Something went wrong",
						message: error.body.message,
						variant: "warning"
					})
				);
				return false;
			});
	}
}