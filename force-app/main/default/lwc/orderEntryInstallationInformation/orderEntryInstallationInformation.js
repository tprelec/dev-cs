import { LightningElement, api, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryInstallationInformation extends LightningElement {
	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = value;
		if (this.operatorSwitchRequested === null || this.operatorSwitchRequested === undefined) {
			this.operatorSwitchRequested = value.operatorSwitch.requested;
		}
		this.setValues();
	}

	@track earliestInstallationDate;

	@track firstPreferredInstallationDayPeriod;
	@track secondPreferredInstallationDayPeriod;
	@track thirdPreferredInstallationDayPeriod;

	@track firstPreferredInstallationDate = null;
	@track secondPreferredInstallationDate = null;
	@track thirdPreferredInstallationDate = null;

	operatorSwitchRequested;

	get disableInstallationDate() {
		return !this.assignTechnician;
	}

	get maxInstallationDate() {
		let d = new Date();
		d.setDate(d.getDate() + 90);
		return d.toISOString().slice(0, 10);
	}
	@track assignTechnician;

	get dayPeriods() {
		return [
			{ label: "Morning", value: "morning" },
			{ label: "Afternoon", value: "afternoon" }
		];
	}

	@api
	validate() {
		if (!this.firstPreferredInstallationDate) {
			this.handleError(
				"Missing 1st preferred installation date",
				"Please select 1st preferred installation date."
			);
			return false;
		}

		if (
			Date.parse(this.firstPreferredInstallationDate) <
			Date.parse(this.earliestInstallationDate)
		) {
			this.handleError(
				"Invalid 1st preferred installation date",
				"1st preferred installation date can't be before earliest installation date."
			);
			return false;
		}

		if (!this.disableInstallationDate) {
			if (!this.secondPreferredInstallationDate) {
				this.handleError(
					"Missing 2nd preferred installation date",
					"Please select 2nd preferred installation date"
				);
				return false;
			}
			if (!this.thirdPreferredInstallationDate) {
				this.handleError(
					"Missing 3rd preferred installation date",
					"Please select 3rd preferred installation date"
				);
				return false;
			}
			if (
				Date.parse(this.secondPreferredInstallationDate) <
				Date.parse(this.firstPreferredInstallationDate)
			) {
				this.handleError(
					"Invalid 2nd preferred installation date",
					"2nd preferred installation date can't be before 1st preferred installation date."
				);
				return false;
			}
			if (
				Date.parse(this.secondPreferredInstallationDate) >
				Date.parse(this.maxInstallationDate)
			) {
				this.handleError(
					"Invalid 2nd preferred installation date",
					"2nd preferred installation date can't be more than 3 months in the future."
				);
				return false;
			}

			if (
				Date.parse(this.thirdPreferredInstallationDate) <
				Date.parse(this.secondPreferredInstallationDate)
			) {
				this.handleError(
					"Invalid 3rd preferred installation date",
					"3rd preferred installation date can't be before 2nd preferred installation date."
				);
				return false;
			}

			if (
				Date.parse(this.thirdPreferredInstallationDate) >
				Date.parse(this.maxInstallationDate)
			) {
				this.handleError(
					"Invalid 3rd preferred installation date",
					"3rd preferred installation date can't be more than 3 months in the future."
				);
				return false;
			}
		}

		return true;
	}

	handleError(title, message) {
		this.dispatchEvent(
			new ShowToastEvent({
				title,
				message,
				variant: "warning"
			})
		);
	}

	// set value from state
	setValues() {
		// Check if Operator Switch has been changed
		let operatorSwitchChanged = false;
		if (this.operatorSwitchRequested != this.state?.operatorSwitch?.requested) {
			operatorSwitchChanged = true;
			this.operatorSwitchRequested = this.state?.operatorSwitch?.requested;
		}
		// Calculate default earliest install date
		let defaultEarliestInstallationDate = new Date();
		if (operatorSwitchChanged && this.state.operatorSwitch.requested) {
			defaultEarliestInstallationDate.setDate(
				new Date().getDate() + CONSTANTS.INSTALLATION_DATE_DAYS_OVERSTAPSERVICE
			);
		} else {
			defaultEarliestInstallationDate.setDate(
				new Date().getDate() + CONSTANTS.INSTALLATION_DATE_DAYS_DEFAULT
			);
		}

		if (!this.state.installation.earliestInstallationDate || operatorSwitchChanged) {
			this.dispatchStateChange(
				"earliestInstallationDate",
				defaultEarliestInstallationDate.toISOString().slice(0, 10)
			);
			this.earliestInstallationDate = defaultEarliestInstallationDate
				.toISOString()
				.slice(0, 10);
			this.firstPreferredInstallationDate = null;
		} else {
			this.earliestInstallationDate = this.state.installation.earliestInstallationDate;
			this.firstPreferredInstallationDayPeriod = this.state.installation.preferredDate1.dayPeriod;
			this.secondPreferredInstallationDayPeriod = this.state.installation.preferredDate2.dayPeriod;
			this.thirdPreferredInstallationDayPeriod = this.state.installation.preferredDate3.dayPeriod;
			this.firstPreferredInstallationDate = this.state.installation.preferredDate1.selectedDate;
			this.secondPreferredInstallationDate = this.state.installation.preferredDate2.selectedDate;
			this.thirdPreferredInstallationDate = this.state.installation.preferredDate3.selectedDate;
		}
		this.assignTechnician = this.state.installation.assignTechnician;
		if (!this.firstPreferredInstallationDate) {
			this.setFirstPreferredInstallationDate();
		}
	}

	// Change Assign Technician
	assignTechnicianChange(event) {
		this.assignTechnician = event.detail.checked;
		this.dispatchStateChange(event.target.name, event.detail.checked);
		this.setSecondPreferredInstallationDate();
		this.setThirdPreferredInstallationDate();
		this.dispatchPreferredInstallationDateChange();
	}

	// Set first preferred installation date to earliest date + 1 day
	setFirstPreferredInstallationDate() {
		let preferredInstallationDate = new Date(this.earliestInstallationDate);
		preferredInstallationDate.setDate(preferredInstallationDate.getDate() + 1);
		this.firstPreferredInstallationDate = preferredInstallationDate.toISOString().slice(0, 10);
		this.firstPreferredInstallationDayPeriod = "morning";
		this.setSecondPreferredInstallationDate();
		this.setThirdPreferredInstallationDate();
		this.dispatchPreferredInstallationDateChange();
	}

	// Set second  preferred installation date to first + 1
	setSecondPreferredInstallationDate() {
		if (this.disableInstallationDate) {
			this.secondPreferredInstallationDate = null;
		} else {
			let preferredInstallationDate = new Date(this.firstPreferredInstallationDate);
			preferredInstallationDate.setDate(preferredInstallationDate.getDate() + 1);
			this.secondPreferredInstallationDate = preferredInstallationDate
				.toISOString()
				.slice(0, 10);
			this.secondPreferredInstallationDayPeriod = "morning";
		}
	}

	// Set third preferred installation date to first + 2
	setThirdPreferredInstallationDate() {
		if (this.disableInstallationDate) {
			this.thirdPreferredInstallationDate = null;
		} else {
			let preferredInstallationDate = new Date(this.firstPreferredInstallationDate);
			preferredInstallationDate.setDate(preferredInstallationDate.getDate() + 2);
			this.thirdPreferredInstallationDate = preferredInstallationDate
				.toISOString()
				.slice(0, 10);
			this.thirdPreferredInstallationDayPeriod = "morning";
		}
	}

	// Change day period for all preferred installation date
	dayPeriodChange(event) {
		this[event.target.name] = event.target.value;
		if (
			event.target.name === "firstPreferredInstallationDayPeriod" &&
			this.firstPreferredInstallationDate !== ""
		) {
			this.dispatchStateChange("preferredDate1", {
				selectedDate: this.firstPreferredInstallationDate,
				dayPeriod: event.target.value
			});
		} else if (
			event.target.name === "secondPreferredInstallationDayPeriod" &&
			this.secondPreferredInstallationDate !== ""
		) {
			this.dispatchStateChange("preferredDate2", {
				selectedDate: this.secondPreferredInstallationDate,
				dayPeriod: event.target.value
			});
		} else if (
			event.target.name === "thirdPreferredInstallationDayPeriod" &&
			this.thirdPreferredInstallationDate !== ""
		) {
			this.dispatchStateChange("preferredDate3", {
				selectedDate: this.thirdPreferredInstallationDate,
				dayPeriod: event.target.value
			});
		}
	}

	// Change preferred installation data
	// If firstPreferredInstallationDate add 1 day on secondPreferredDate and 2 days on thiredPreferredaDate
	preferredInstallationDateChange(event) {
		this[event.target.name] = event.target.value;

		if (event.target.name === "firstPreferredInstallationDate") {
			this.setSecondPreferredInstallationDate();
			this.setThirdPreferredInstallationDate();
		}
		this.dispatchPreferredInstallationDateChange();
	}

	// dispatch chagne without preferredInstalationdDate
	dispatchStateChange(key, value) {
		this.dispatchEvent(
			new CustomEvent("statechanged", {
				detail: {
					key,
					value
				},
				bubbles: false
			})
		);
	}

	// dispatch preferred installation date change
	dispatchPreferredInstallationDateChange() {
		var value = {
			preferredDate1: {
				selectedDate: this.firstPreferredInstallationDate,
				dayPeriod: this.firstPreferredInstallationDayPeriod
			},
			preferredDate2: {
				selectedDate: this.secondPreferredInstallationDate,
				dayPeriod: this.secondPreferredInstallationDayPeriod
			},
			preferredDate3: {
				selectedDate: this.thirdPreferredInstallationDate,
				dayPeriod: this.thirdPreferredInstallationDayPeriod
			}
		};

		this.dispatchEvent(
			new CustomEvent("preferreddates", {
				detail: value,
				bubbles: false
			})
		);
	}
}