import { LightningElement, api, track } from "lwc";

export default class OrderEntryInstallationNotes extends LightningElement {
	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = value;
		this.setInitialValue();
	}

	@track notes;

	setInitialValue() {
		this.notes = this.state.notes;
	}

	notesChange(event) {
		this.notes = event.target.value;
		this.dispatchStageChange();
	}

	dispatchStageChange() {
		this.dispatchEvent(
			new CustomEvent("noteschange", {
				detail: this.notes,
				bubbles: false
			})
		);
	}
}