import { LightningElement, api, track } from "lwc";

export default class OrderEntryInstallationOverstapservice extends LightningElement {
	@track requested;
	@track currentProvider;
	@track currentContractNumber;
	@track potentialFeeAccepted;
	@track currentContractNumberRequired;
	@track disabledOperatorSwitch;
	get operatorSwitchNotRequested() {
		return !this.requested;
	}

	get currentProviderOptions() {
		return [
			{ label: "KPN", value: "KPN" },
			{ label: "MTTM", value: "MTTM" },
			{ label: "Budget Alles-in-1", value: "Budget Alles-in-1" },
			{ label: "SpeakUp", value: "SpeakUp" },
			{ label: "TELE2", value: "TELE2" },
			{ label: "XS4ALL", value: "XS4ALL" },
			{ label: "YOUF", value: "YOUF" },
			{ label: "DELTA", value: "DELTA" },
			{ label: "Netvisit", value: "Netvisit" },
			{ label: "Plinq", value: "Plinq" },
			{ label: "TRINED", value: "TRINED" },
			{ label: "OXXIO", value: "OXXIO" },
			{ label: "BREEDBAND HELMOND", value: "BREEDBAND HELMOND" },
			{ label: "T-Mobile Thuis B.V.", value: "T-Mobile Thuis B.V." },
			{ label: "Kabeltex B.V.", value: "Kabeltex B.V." },
			{
				label: "Solcon Internetdiensten B.V.",
				value: "Solcon Internetdiensten B.V."
			},
			{ label: "Caiway", value: "Caiway" },
			{
				label: "Stichting Kabeltelevisie Pijnacker",
				value: "Stichting Kabeltelevisie Pijnacker"
			},
			{ label: "Belcentrale B.V", value: "Belcentrale B.V" },
			{ label: "Multifiber", value: "Multifiber" },
			{ label: "RapidXS", value: "RapidXS" },
			{ label: "Weserve", value: "Weserve" },
			{ label: "Digihero", value: "Digihero" },
			{ label: "Freedom Internet B.V.", value: "Freedom Internet B.V." },
			{ label: "Qonnected B.V.", value: "Qonnected B.V." },
			{ label: "Onvi B.V", value: "Onvi B.V" }
		];
	}

	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = value;
		this.setInitialValue();
	}

	setInitialValue() {
		if (this.state.b2cInternetCustomer) {
			this.disabledOperatorSwitch = true;
			if (this.requested || this.requested === undefined) {
				this.requested = false;
				this.cleanOperatorSwitch();
				this.dispatchStageChange();
			}
		} else {
			this.disabledOperatorSwitch = false;
			this.requested = this.state.operatorSwitch.requested;
			this.currentProvider = this.state.operatorSwitch.currentProvider;
			this.currentContractNumber = this.state.operatorSwitch.currentContractNumber;
			this.potentialFeeAccepted = this.state.operatorSwitch.potentialFeeAccepted;
			this.setCurrentContractNumberRequired();
		}
	}
	requestedChange(event) {
		this.requested = event.detail.checked;
		this.cleanOperatorSwitch();
		this.dispatchStageChange();
	}

	cleanOperatorSwitch() {
		this.currentProvider = "";
		this.currentContractNumber = "";
		this.potentialFeeAccepted = false;
	}

	handleFormInputChange(event) {
		if (event.target.type === "toggle") {
			this.potentialFeeAccepted = event.detail.checked;
		} else {
			this[event.target.name] = event.target.value;
		}
		this.dispatchStageChange();
		this.setCurrentContractNumberRequired();
	}

	setCurrentContractNumberRequired() {
		this.currentContractNumberRequired =
			this.currentProvider || this.currentProvider.length !== 0;
	}

	dispatchStageChange() {
		var value = {
			requested: this.requested,
			currentProvider: this.currentProvider,
			currentContractNumber: this.currentContractNumber,
			potentialFeeAccepted: this.potentialFeeAccepted
		};

		this.dispatchEvent(
			new CustomEvent("overstapservicestagechange", {
				detail: value,
				bubbles: false
			})
		);
	}
}