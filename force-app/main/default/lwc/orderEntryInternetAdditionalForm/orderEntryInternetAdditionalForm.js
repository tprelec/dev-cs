import { api, LightningElement, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryInternetAdditionalForm extends LightningElement {
	@track isValid = true;

	@api
	validate() {
		return this.isValid;
	}

	@api
	get addons() {
		return this._addons;
	}
	set addons(value) {
		this.setAttribute("addons", value);
		this._addons = value;
		this.buildInputs();
	}

	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = JSON.parse(JSON.stringify(value));
		this.buildInputs();
	}

	@track _state = null;
	@track _addons = [];
	@track additionalAddons = [];

	get addonsChunked() {
		return this.chunk(this.additionalAddons, 2);
	}

	get indexedChunks() {
		return this.addonsChunked.map((c, i) => ({
			items: c,
			i
		}));
	}

	chunk(arr, size) {
		return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
			arr.slice(i * size, i * size + size)
		);
	}

	buildInputs() {
		if (this.state && this.addons && this.addons.length) {
			const items = this.addons.filter(
				(a) => a.Add_On__r.Type__c === CONSTANTS.INTERNET_ADDITIONAL_TYPE
			);
			const stateAddons = this.state.addons || [];

			this.additionalAddons = items.map((i) => {
				const stateAddon = stateAddons.find((sa) => sa.id === i.Add_On__r.Id);
				return {
					...i,
					quantity: stateAddon ? stateAddon.quantity : 0
				};
			});
		}
	}

	handleInputChange(e) {
		e.target.setCustomValidity("");
		e.target.checkValidity();

		let limit = 0;
		const selectedAddOnInput = this.addons.filter((a) => a.Add_On__r.Id === e.target.name);

		if (selectedAddOnInput[0].Add_On__r.hasOwnProperty("Max_Quantity__c")) {
			limit = selectedAddOnInput[0].Add_On__r.Max_Quantity__c;
		}

		if (limit === 0 || parseInt(e.detail.value, 10) <= limit) {
			this.buildAddonData(e.target.name, parseInt(e.detail.value, 10));
			this.isValid = true;
		} else {
			this.isValid = false;
			e.target.setCustomValidity(`Max Quantity is ${limit}.`);
			e.target.checkValidity();
		}
	}

	buildAddonData(id, quantity) {
		const addonData = {
			id,
			quantity,
			type: CONSTANTS.INTERNET_ADDITIONAL_TYPE
		};
		this.dispatchEvent(new CustomEvent("changedaddon", { detail: addonData, bubbles: false }));
	}
}