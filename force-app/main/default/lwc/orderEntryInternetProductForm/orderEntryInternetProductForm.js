import { api, LightningElement, track } from "lwc";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryInternetProductForm extends LightningElement {
	@api state = null;
	@api products = [];
	@api addons = [];

	@api validate() {
		const productInput = this.template.querySelector("lightning-combobox.internet-product");
		productInput.reportValidity();
		const isProductValid = productInput.checkValidity();

		const termInput = this.template.querySelector("lightning-combobox.contract-term");
		termInput.reportValidity();
		const isTermValid = termInput.checkValidity();

		return isProductValid && isTermValid;
	}

	@track pinZeker = false;

	/**
	 * Get all internet security addons formated for picklist
	 */
	get internetSecurityAddons() {
		return this.addons
			.filter((a) => a.Add_On__r.Type__c === CONSTANTS.INTERNET_SECURITY_TYPE)
			.map((a) => ({
				value: a.Add_On__r.Id,
				label: a.Add_On__r.Name
			}));
	}

	/**
	 * Get Pin Zeker addon from all addons
	 */
	get pinZekerAddon() {
		return this.addons.find((a) => a.Add_On__r.Type__c === CONSTANTS.PIN_ZEKER_TYPE);
	}

	get pinZekerValue() {
		const selected = (this.state?.addons || []).find(
			(a) => a?.type === CONSTANTS.PIN_ZEKER_TYPE
		);
		return this.pinZekerAddon && selected && selected.quantity;
	}

	get selectedProduct() {
		const internetProduct = (this.state?.products || []).find(
			(p) => p.type === CONSTANTS.INTERNET_PRODUCT_TYPE
		);
		return internetProduct ? internetProduct.id : null;
	}

	get contractTerm() {
		return this.state?.contractTerm || "1";
	}

	get selectedSecurityAddon() {
		const selected = (this.state?.addons || []).find(
			(a) => a?.type === CONSTANTS.INTERNET_SECURITY_TYPE
		);
		return selected ? selected.id : null;
	}

	get filteredProducts() {
		const siteCheckAvailability = this.state?.siteCheck?.availability || [];
		const fp1000Check = siteCheckAvailability.find((a) => a.name === CONSTANTS.FP1000_CHECK);
		const onNetCheck = this.state?.addressCheckResult === CONSTANTS.ON_NET_CHECK;
		const hideGiga = fp1000Check && !fp1000Check.available && onNetCheck;
		return this.products.filter((p) => {
			return (
				p.Name !== CONSTANTS.GIGA_INTERNET ||
				(p.Name === CONSTANTS.GIGA_INTERNET && !hideGiga)
			);
		});
	}

	/**
	 * Get list of formated products for picklist
	 */
	get formattedProducts() {
		return this.filteredProducts.map((p) => ({
			value: p.Id,
			label: p.Name
		}));
	}

	contractTerms = [
		{ label: "1 year", value: "1" },
		{ label: "2 years", value: "2" },
		{ label: "3 years", value: "3" }
	];

	/**
	 * Handle change on product picklist
	 */
	handleProductChange(e) {
		if (
			e.detail.value !==
			this.state.products.find((p) => p.type === CONSTANTS.INTERNET_PRODUCT_TYPE).id
		) {
			this.selectProduct(e.detail.value);
		}
	}

	/**
	 * Set selected product and dispatch to parent components
	 */
	selectProduct(productId) {
		this.dispatchEvent(
			new CustomEvent("selectedproduct", { detail: productId, bubbles: false })
		);
	}

	/**
	 * Handle contract term picklist change
	 */
	handleContractTermChange(e) {
		this.selectContractTerm(e.detail.value);
	}

	/**
	 * Set contract term value and dispatch event to parent components
	 */
	selectContractTerm(value) {
		this.dispatchEvent(
			new CustomEvent("selectedcontractterm", { detail: value, bubbles: false })
		);
	}

	/**
	 * Handle selection of internet security addon from picklist
	 */
	handleSelectedSecurity(e) {
		this.buildAddonData(CONSTANTS.INTERNET_SECURITY_TYPE, e.detail.value, 1);
	}

	/**
	 * Handle change of Pin Zeker toggle button value
	 */
	handlePinZeker(e) {
		this.togglePinZeker(e.detail.checked);
	}

	/**
	 * Change value od Pin Zeker property
	 */
	togglePinZeker(value) {
		if (this.pinZekerAddon) {
			this.buildAddonData(
				CONSTANTS.PIN_ZEKER_TYPE,
				this.pinZekerAddon.Add_On__r.Id,
				value === true ? 1 : 0
			);
		}
	}

	/**
	 * Build addon data and dispatch to parent component
	 */
	buildAddonData(type, id, quantity) {
		const addonData = {
			id,
			quantity,
			type
		};
		this.dispatchEvent(new CustomEvent("changedaddon", { detail: addonData, bubbles: false }));
	}
}