import { LightningElement, api, track, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { NavigationMixin } from "lightning/navigation";
import getOrderEntryData from "@salesforce/apex/OrderEntryController.getOrderEntryData";
import getHeaderInfo from "@salesforce/apex/OrderEntryController.getHeaderInfo";
import updateState from "@salesforce/apex/OrderEntryController.updateState";
import getDefaultBundle from "@salesforce/apex/OrderEntryController.getDefaultBundle";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryMainNavigation extends NavigationMixin(LightningElement) {
	@api
	get recordId() {
		return this._recordId;
	}
	set recordId(value) {
		this.setAttribute("recordId", value);
		this._recordId = value;
		this.getInitialData();
	}
	@track _recordId;

	steps = [
		CONSTANTS.SELECT_PRODUCT,
		CONSTANTS.ACCOUNT_CONTACT,
		CONSTANTS.ADDRESS,
		CONSTANTS.PRODUCT_CONFIG,
		CONSTANTS.DISCOUNT_PAYMENT,
		CONSTANTS.INSTALLATION,
		CONSTANTS.QUOTE_SIGNATURE
	];
	stepIds = [
		"product-selection",
		"account-and-contact",
		"address",
		"product-config",
		"discount-and-payment",
		"installation",
		"quote-and-signature"
	];
	@api state = {};
	@track currentStep = null;
	@track headerInfo = "";
	@track error;
	@track showLoader = true;
	@wire(getHeaderInfo, { oppId: "$recordId" })
	initHeaderInfo({ error, data }) {
		if (data) {
			this.headerInfo = data;
		} else if (error) {
			this.error = error;
		}
	}

	async getInitialData() {
		var data = null;
		try {
			await getOrderEntryData({ oppId: this.recordId })
				.then((result) => {
					console.log(result);
					data = result;
				})
				.catch((error) => {
					console.log(error);
					this.error = error;
					this.showLoader = false;
				});

			if (data) {
				console.log("loaded state", JSON.stringify(data));
				await this.setState(data, true);
				if (data.lastStep) {
					this.currentStep = data.lastStep;
				} else {
					this.currentStep = CONSTANTS.SELECT_PRODUCT;
				}
			}
			this.showLoader = false;
		} catch (error) {
			console.error("Error: ", error);
			this.error = error;
			this.showLoader = false;
		}
	}

	toggleLoader(e) {
		this.showLoader = e.detail;
	}

	handlePaymentDetailsRequired(e) {
		this.currentStep = CONSTANTS.DISCOUNT_PAYMENT;
	}

	get isLastIndex() {
		return this.steps.indexOf(this.currentStep) === this.steps.length - 1;
	}
	get isFirstIndex() {
		return this.steps.indexOf(this.currentStep) === 0;
	}
	get activeId() {
		return this.stepIds[this.steps.indexOf(this.currentStep)];
	}
	get activeStepComponent() {
		return this.template.querySelector(`c-order-entry-${this.activeId}`);
	}
	get isProductSelection() {
		return this.currentStep === CONSTANTS.SELECT_PRODUCT;
	}
	get isAccountAndContact() {
		return this.currentStep === CONSTANTS.ACCOUNT_CONTACT;
	}
	get isAddress() {
		return this.currentStep === CONSTANTS.ADDRESS;
	}
	get isProductConfiguration() {
		return this.currentStep === CONSTANTS.PRODUCT_CONFIG;
	}
	get isDiscountAndPayment() {
		return this.currentStep === CONSTANTS.DISCOUNT_PAYMENT;
	}
	get isInstallation() {
		return this.currentStep === CONSTANTS.INSTALLATION;
	}
	get isQuoteSignature() {
		return this.currentStep === CONSTANTS.QUOTE_SIGNATURE;
	}

	// Set the correct slds style classes to the path element
	get processedSteps() {
		return this.steps.map((item, index) => {
			let name = item;
			let computedClass = "slds-path__item";

			if (this.steps.indexOf(this.currentStep) > index) {
				computedClass += " slds-is-complete";
			} else if (this.steps.indexOf(this.currentStep) === index) {
				computedClass += " slds-is-active";
			} else {
				computedClass += " slds-is-incomplete";
			}

			return {
				name,
				computedClass
			};
		});
	}

	async setActive(event) {
		let parentId = event.currentTarget.parentElement.id.split("-")[0];
		if (this.steps.indexOf(parentId) <= this.steps.indexOf(this.currentStep)) {
			this.showLoader = true;
			const currentStep = this.steps[this.steps.indexOf(parentId)];
			await this.setState(Object.assign({}, this.state, { lastStep: currentStep }));
			await this.saveState();
			this.currentStep = currentStep;
			this.showLoader = false;
		} else {
			this.dispatchEvent(
				new ShowToastEvent({
					title: "Unable to go to that step",
					message: "Use Next button to go to that step",
					variant: "warning"
				})
			);
		}
	}

	/**
	 * Update state from child component events
	 */
	async updateState(e) {
		await this.setState(e.detail);
	}

	/**
	 * Set state data
	 */
	async setState(newState, initialLoad) {
		let newStateClone = JSON.parse(JSON.stringify(newState));
		let oldStateClone = JSON.parse(JSON.stringify(this.state));
		const oldMainProductIndex = this.getProductIndexByType(
			oldStateClone.products,
			CONSTANTS.MAIN_PRODUCT_TYPE
		);
		const newMainProductIndex = this.getProductIndexByType(
			newStateClone.products,
			CONSTANTS.MAIN_PRODUCT_TYPE
		);
		const oldMainProductId =
			oldMainProductIndex > -1 ? oldStateClone.products[oldMainProductIndex].id : null;
		const newMainProductId =
			newMainProductIndex > -1 ? newStateClone.products[newMainProductIndex].id : null;

		if (
			(initialLoad && (!newStateClone.products || !newMainProductId)) ||
			(!initialLoad && newMainProductId && newMainProductId !== oldMainProductId)
		) {
			const defaultBundle = await getDefaultBundle({
				mainProductId: newMainProductId
			});
			if (defaultBundle) {
				newStateClone.products = this.setDefaultProducts(
					newStateClone.products || [],
					defaultBundle
				);
			}
		}
		this.state = newStateClone;
	}

	/**
	 * Get product from list of state products by product type
	 */
	getProductIndexByType(products, type) {
		return products ? products.findIndex((p) => p.type === type) : -1;
	}

	setDefaultProducts(products, bundle) {
		[
			CONSTANTS.INTERNET_PRODUCT_TYPE,
			CONSTANTS.TV_PRODUCT_TYPE,
			CONSTANTS.TELEPHONY_PRODUCT_TYPE
		].forEach((type) => {
			const productIndex = products.findIndex((p) => p.type === type);
			const id = bundle[`${type}_Product__c`];

			if (productIndex === -1 && id) {
				products.push({
					type,
					id
				});
			}
		});
		return products;
	}

	// Persist state changes made to JSON
	async saveState() {
		try {
			const result = await updateState({
				oppId: this.state.opportunityId,
				state: JSON.stringify(this.state)
			});
			if (result) {
				// Show message that state was updated?
				console.info("saved", JSON.stringify(this.state));
			}
		} catch (err) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: "Error",
					message: "Something went wrong when saving state",
					variant: "error"
				})
			);
		}
	}

	// NAV BUTTONS
	async next() {
		this.showLoader = true;
		if (
			this.activeStepComponent &&
			typeof this.activeStepComponent.validate !== "undefined" &&
			(await this.activeStepComponent.validate())
		) {
			const lastActiveComponent = this.activeStepComponent;
			let currentStep = this.steps[this.steps.indexOf(this.currentStep) + 1];

			// Limit the possibility to go to a step further than the last validate one
			await this.setState(Object.assign({}, this.state, { lastStep: currentStep }));
			// Save changes to the state
			await this.saveState();

			// Check if after save function exists on last child component
			if (lastActiveComponent && typeof lastActiveComponent.afterSave !== "undefined") {
				try {
					await lastActiveComponent.afterSave();
					this.currentStep = currentStep;
				} catch (e) {
					this.dispatchEvent(
						new ShowToastEvent({
							title: "Error occured",
							message: e.body.message,
							variant: "error",
							mode: "sticky"
						})
					);
					this.state.lastStep = this.currentStep;
					await this.saveState();
				}
			} else {
				this.currentStep = currentStep;
			}
		}
		this.showLoader = false;
	}
	async back() {
		this.showLoader = true;
		const currentStep = this.steps[this.steps.indexOf(this.currentStep) - 1];
		await this.setState(Object.assign({}, this.state, { lastStep: currentStep }));
		await this.saveState();
		this.currentStep = currentStep;
		this.showLoader = false;
	}
	backToOpp() {
		// Navigate back to Oppotrunity
		this[NavigationMixin.Navigate](
			{
				type: "standard__recordPage",
				attributes: {
					recordId: this.recordId,
					actionName: "view"
				}
			},
			true
		);
	}
}