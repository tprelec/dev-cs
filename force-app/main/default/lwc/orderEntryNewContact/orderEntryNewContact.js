import { LightningElement, api, track, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import saveContact from "@salesforce/apex/OrderEntryController.saveContact";

export default class OrderEntryNewContact extends LightningElement {
	@api state;
	@track error;
	@track loading = false;
	// Dutch phone numbers patern
	@track telPattern =
		"^((+|00(s|s?-s?)?)31(s|s?-s?)?((0)[-s]?)?|0)[1-9]((s|s?-s?)?[0-9])((s|s?-s?)?[0-9])((s|s?-s?)?[0-9])s?[0-9]s?[0-9]s?[0-9]s?[0-9]s?[0-9]$";
	@track newContact = { FirstName: "", LastName: "", MobilePhone: "", Phone: "", Email: "" };

	handleFormInputChange(event) {
		this.newContact[event.target.name] = event.target.value;
	}

	closeModal() {
		this.dispatchEvent(new CustomEvent("closemodal", { detail: {}, bubbles: false }));
	}

	handleSave() {
		this.loading = true;
		const accId = { AccountId: this.state.accountId };
		this.newContact = { ...this.newContact, ...accId };
		saveContact({ newContact: this.newContact, oppId: this.state.opportunityId })
			.then((result) => {
				if (result) {
					this.newContact = result;
					this.dispatchEvent(
						new CustomEvent("closemodal", { detail: this.newContact, bubbles: false })
					);
				}
			})
			.catch((error) => {
				this.loading = false;
				console.error("Error", error);
				let errorMsg = "";

				for (let propName in error.body.fieldErrors) {
					if (error.body.fieldErrors.hasOwnProperty(propName)) {
						errorMsg = error.body.fieldErrors[propName][0].message.split(".")[0];
					}
				}
				if (errorMsg === "") {
					error.body.pageErrors.forEach((element) => {
						errorMsg = element.message.split(".")[0];
					});
				}
				this.dispatchEvent(
					new ShowToastEvent({
						title: "Something went wrong",
						message: errorMsg,
						variant: "warning"
					})
				);
			});
	}
}