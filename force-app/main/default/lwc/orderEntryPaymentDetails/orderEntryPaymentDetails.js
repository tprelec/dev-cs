import { api, LightningElement } from "lwc";
import getCountryIbanLength from "@salesforce/apex/OrderEntryController.getCountryIbanLength";

export default class OrderEntryPaymentDetails extends LightningElement {
	@api state = null;

	@api async validate() {
		const ibanInput = this.template.querySelector("lightning-input.iban");
		const ibanCode = this.iban.replace(/\s/g, "").toUpperCase();

		if (this.ibanAllTrue(ibanCode)) {
			ibanInput.setCustomValidity("");
		} else if (
			!this.validatePaymentType(ibanCode) &&
			this.validateIbanFormat(ibanCode) &&
			this.validateIbanLength(ibanCode) &&
			this.validateIbanSanity(ibanCode)
		) {
			ibanInput.setCustomValidity(
				"IBAN is required if Direct Debit is selected as Payment Type."
			);
		} else if (this.validatePaymentType(ibanCode)) {
			if (!this.validateIbanFormat(ibanCode)) {
				ibanInput.setCustomValidity(
					"Invalid IBAN format."
				);
			} else if (!(await this.validateIbanLength(ibanCode))) {
				ibanInput.setCustomValidity("Invalid IBAN length.");
			} else if (!this.validateIbanSanity(ibanCode)) {
				ibanInput.setCustomValidity("IBAN sanity check failed.");
			}
		}
		ibanInput.reportValidity();

		return this.ibanAllTrue(ibanCode);
	}

	get bankAccountHolder() {
		return this.state?.payment?.bankAccountHolder || "";
	}

	get iban() {
		return this.state?.payment?.iban || "";
	}

	get paymentType() {
		return this.state?.payment?.paymentType || "";
	}

	get billingChannel() {
		return this.state?.payment?.billingChannel || "Electronic Bill";
	}

	paymentTypes = [
		{ label: "--- None ---", value: "" },
		{ label: "Direct Debit", value: "Direct Debit" },
		{ label: "Bank Transfer", value: "Bank Transfer" }
	];

	billingChannels = [
		{ label: "Electronic Bill", value: "Electronic Bill" },
		{ label: "Paper Bill", value: "Paper bill" }
	];

	validatePaymentType(ibanCode) {
		if (!ibanCode.length && this.paymentType === "Direct Debit") {
			return false;
		}
		return true;
	}

	validateIbanFormat(ibanCode) {
		if (ibanCode && ibanCode.length) {
			const rgx = new RegExp("^[A-Z]{2}[0-9]{2}[A-Z0-9]+$");

			if (ibanCode.length < 5 || !rgx.test(ibanCode)) {
				return false;
			}
		}
		return true;
	}

	async validateIbanLength(ibanCode) {
		if (ibanCode && ibanCode.length) {
			const countryCode = ibanCode.substring(0, 2);
			const countryIbanLength = await getCountryIbanLength({ countryCode: countryCode });

			if (countryIbanLength && countryIbanLength !== ibanCode.length) {
				return false;
			}
		}
		return true;
	}

	validateIbanSanity(ibanCode) {
		if (ibanCode && ibanCode.length) {
			const ibanCodeMap = this.createIbanCodeMap();
			const reformattedCode = ibanCode.substring(4) + ibanCode.substring(0, 4);
			const max = 999999999;
			let total = 0;
			let charValue;

			for (let i = 0; i < reformattedCode.length; i++) {
				charValue = ibanCodeMap.find(
					(ibanCode) => ibanCode.char === reformattedCode.substring(i, i + 1)
				).code;
				total = (charValue > 9 ? total * 100 : total * 10) + charValue;

				if (total > max) {
					total = total % 97;
				}
			}
			if (total % 97 !== 1) {
				return false;
			}
		}
		return true;
	}

	handlePaymentChange(e) {
		let eventValue = e.target.value;

		if (e.target.dataset.id === "iban") {
			// this.validate();
			eventValue = eventValue.toUpperCase();			
		}

		const eventData = { element: e.target.dataset.id, value: eventValue };
		this.dispatchEvent(
			new CustomEvent("paymentchanged", { detail: eventData, bubbles: false })
		);
	}

	ibanAllTrue(ibanCode) {
		return (
			this.validatePaymentType(ibanCode) &&
			this.validateIbanFormat(ibanCode) &&
			this.validateIbanLength(ibanCode) &&
			this.validateIbanSanity(ibanCode)
		);
	}

	createIbanCodeMap() {
		const allChars = [
			"0",
			"1",
			"2",
			"3",
			"4",
			"5",
			"6",
			"7",
			"8",
			"9",
			"A",
			"B",
			"C",
			"D",
			"E",
			"F",
			"G",
			"H",
			"I",
			"J",
			"K",
			"L",
			"M",
			"N",
			"O",
			"P",
			"Q",
			"R",
			"S",
			"T",
			"U",
			"V",
			"W",
			"X",
			"Y",
			"Z"
		];
		let ibanCodeMap = [];

		allChars.forEach((c, index) => {
			ibanCodeMap.push({
				char: c,
				code: index
			});
		});

		return ibanCodeMap;
	}
}