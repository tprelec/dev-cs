import { LightningElement, api, track, wire } from "lwc";
import getProducts from "@salesforce/apex/OrderEntryController.getProducts";
import getBundle from "@salesforce/apex/OrderEntryController.getBundle";
import saveProducts from "@salesforce/apex/OrderEntryController.saveProducts";
import savePortingNumbers from "@salesforce/apex/OrderEntryController.savePortingNumbers";
import getBundlePromotions from "@salesforce/apex/OrderEntryController.getBundlePromotions";
import getSpecialPromotions from "@salesforce/apex/OrderEntryController.getSpecialPromotions";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryProductConfig extends LightningElement {
	INTERNET_PRODUCT_TYPE = CONSTANTS.INTERNET_PRODUCT_TYPE;
	TV_PRODUCT_TYPE = CONSTANTS.TV_PRODUCT_TYPE;
	TELEPHONY_PRODUCT_TYPE = CONSTANTS.TELEPHONY_PRODUCT_TYPE;

	@track _state = null;
	@track products = [];

	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = JSON.parse(JSON.stringify(value));
		console.log("product config state", this._state);
	}

	activeTab = this.INTERNET_PRODUCT_TYPE;

	handleSelectTab(e) {
		this.activeTab = e.detail.name;
	}

	@api validate() {
		const internetFormValidation = this.template.querySelector(
			"c-order-entry-product-internet"
		);
		const tvFormValidation = this.template.querySelector("c-order-entry-product-tv");
		const telephonyFormValidation = this.template.querySelector(
			"c-order-entry-product-telephony"
		);

		return (
			internetFormValidation.validate() &&
			tvFormValidation.validate() &&
			telephonyFormValidation.validate()
		);
	}

	@api async afterSave() {
		await saveProducts({ opportunityId: this._state.opportunityId });
		await savePortingNumbers({ opportunityId: this._state.opportunityId });
	}

	get productTypes() {
		return [this.INTERNET_PRODUCT_TYPE, this.TV_PRODUCT_TYPE, this.TELEPHONY_PRODUCT_TYPE];
	}

	get isInternet() {
		return this.activeTab === this.INTERNET_PRODUCT_TYPE ? "" : "slds-hide";
	}
	get isTv() {
		return this.activeTab === this.TV_PRODUCT_TYPE ? "" : "slds-hide";
	}
	get isTelephony() {
		return this.activeTab === this.TELEPHONY_PRODUCT_TYPE ? "" : "slds-hide";
	}

	@wire(getProducts, { productTypes: "$productTypes" })
	productsWired({ error, data }) {
		if (data) {
			this.products = data;
		} else if (error) {
			this.products = [];
		}
	}

	async handleStateChange(e) {
		const _state = JSON.parse(JSON.stringify(e.detail));
		const params = {};

		if (_state.products) {
			// build params for requesting bundle
			_state.products.forEach((p) => {
				params[p.type.toLowerCase() + "Id"] = p.id;
			});

			_state.bundle = await getBundle(params);
		}

		const bundlePromotions = await getBundlePromotions({
			opportunityId: _state.opportunityId,
			b2cChecked: _state.b2cInternetCustomer,
			products: _state.products,
			addons: _state.addons
		});

		const specialPromotions = await getSpecialPromotions({
			opportunityId: _state.opportunityId,
			products: _state.products,
			addons: _state.addons
		});

		this.removeUnavailablePromotions(_state, bundlePromotions, specialPromotions);

		// dispatch event to parent component
		if (_state) {
			this.dispatchEvent(new CustomEvent("statechanged", { detail: _state, bubbles: false }));
		}
	}

	removeUnavailablePromotions(_state, bundlePromotions, specialPromotions) {
		if (_state && _state.promos && _state.promos.length > 0) {
			_state.promos.forEach((p, index) => {
				let promoFound = -1;
				promoFound =
					p.type === CONSTANTS.STANDARD_PROMOTION_TYPE
						? bundlePromotions.findIndex((bp) => bp.Id === p.id)
						: specialPromotions.findIndex((sp) => sp.Id === p.id);

				if (promoFound === -1) {
					_state.promos.splice(index, 1);
				}
			});
		}
	}
}