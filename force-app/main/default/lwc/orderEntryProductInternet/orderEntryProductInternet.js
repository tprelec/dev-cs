import { api, LightningElement, track } from "lwc";
import getAddons from "@salesforce/apex/OrderEntryController.getAddons";
import getAddonPrice from "@salesforce/apex/OrderEntryController.getAddonPrice";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryProductInternet extends LightningElement {
	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = JSON.parse(JSON.stringify(value));
		this.init();
	}
	@api products = [];
	@api defaultBundle = null;

	@api validate() {
		const internetAdditionalForm = this.template.querySelector(
			"c-order-entry-internet-additional-form"
		);
		const productInput = this.template.querySelector("c-order-entry-internet-product-form");

		return internetAdditionalForm.validate() && productInput.validate();
	}

	@track addons = [];
	@track _state = null;

	async init() {
		let changed = false;
		const selectedProduct = (this._state?.products || []).find(
			(p) => p.type === CONSTANTS.INTERNET_PRODUCT_TYPE
		);
		const securityAddon = (this._state?.addons || []).find(
			(a) => a.type === CONSTANTS.INTERNET_SECURITY_TYPE
		);

		if (selectedProduct) {
			await this.buildAddons(selectedProduct.id);
		}

		// select default security addon if not already selected
		if (selectedProduct && !securityAddon) {
			const firstAddon = this.addons.find(
				(a) => a.Add_On__r.Type__c === CONSTANTS.INTERNET_SECURITY_TYPE
			);

			if (firstAddon) {
				const newSecurityAddon = await this.buildAddonData({
					type: CONSTANTS.INTERNET_SECURITY_TYPE,
					quantity: 1,
					id: firstAddon.Add_On__r.Id
				});
				this.setAddon(newSecurityAddon);
				changed = true;
			}
		}

		// select default contract term
		if (!this._state.contractTerm) {
			this._state.contractTerm = "1";
			changed = true;
		}

		// if something changed trigger state update
		if (changed) {
			this.dispatchStateChange();
		}
	}

	/**
	 * Get Internet type products
	 */
	get internetProducts() {
		return this.products.filter((p) => p.Type__c === CONSTANTS.INTERNET_PRODUCT_TYPE);
	}

	/**
	 * Get index of Internet product
	 */
	get internetProductIndex() {
		return (this._state?.products || []).findIndex(
			(p) => p.type === CONSTANTS.INTERNET_PRODUCT_TYPE
		);
	}

	/**
	 * Handle product selection from child component
	 */
	async handleProductSelection(e) {
		const payloadProducts = [...this.state.products];

		if (this.internetProductIndex === -1) {
			// if does not exist add it
			payloadProducts.push({
				type: CONSTANTS.INTERNET_PRODUCT_TYPE,
				id: e.detail
			});
		} else {
			// if exists update it
			payloadProducts[this.internetProductIndex].id = e.detail;
		}
		// update products in payload clone
		this.state.products = payloadProducts;
		await this.buildAddons(e.detail);
		this.dispatchStateChange();
	}

	async buildAddons(parentProductId) {
		// get addons from api for picklist
		this.addons = await getAddons({ parentProduct: parentProductId });

		// apply automatic addons
		for (let i = 0; i < this.addons.length; i++) {
			const addon = this.addons[i];
			if (addon.Automatic_Add_On__c) {
				this.setAddon(
					await this.buildAddonData({
						type: addon.Add_On__r.Type__c,
						id: addon.Add_On__r.Id,
						quantity: 1
					})
				);
			}
		}

		// rebuild list of selected addons and remove ones that are not related to selected internet product
		const stateAddons = this.state?.addons || [];
		const otherAddons = stateAddons.filter(
			(a) => a.parentType !== CONSTANTS.INTERNET_PRODUCT_TYPE
		);
		const internetAddons = stateAddons.filter(
			(a) =>
				a.parentType === CONSTANTS.INTERNET_PRODUCT_TYPE &&
				a.parentProduct === parentProductId
		);
		this.state.addons = [...otherAddons, ...internetAddons];
	}

	/**
	 * Handle contract term change from child component
	 */
	handleContractTermChange(e) {
		this._state.contractTerm = e.detail;
		this.dispatchStateChange();
	}

	/**
	 * Dispatch changes on state to parent components
	 */
	dispatchStateChange() {
		this.dispatchEvent(
			new CustomEvent("statechanged", { detail: this._state, bubbles: false })
		);
	}

	async buildAddonData({ type, id, quantity }) {
		const addon = this.addons.find((a) => a.Add_On__r.Id === id);

		if (addon) {
			const price = await getAddonPrice({ code: addon.Add_On__r.Product_Code__c });
			const recurring =
				addon.Recurring_Price_Override__c || addon.Recurring_Price_Override__c === 0
					? addon.Recurring_Price_Override__c
					: price.cspmb__Recurring_Charge__c;
			const oneOff =
				addon.One_Off_Price_Override__c || addon.One_Off_Price_Override__c === 0
					? addon.One_Off_Price_Override__c
					: price.cspmb__One_Off_Charge__c;
			return {
				id,
				quantity,
				type,
				name: addon.Add_On__r.Name,
				parentProduct: addon.Product__c,
				code: addon.Add_On__r.Product_Code__c,
				recurring: recurring,
				oneOff: oneOff,
				totalRecurring: parseFloat((recurring * quantity).toFixed(2)),
				totalOneOff: parseFloat((oneOff * quantity).toFixed(2)),
				bundleName: price.Product_Name__c,
				parentType: CONSTANTS.INTERNET_PRODUCT_TYPE
			};
		}
		return null;
	}

	setAddon(addonData) {
		if (addonData) {
			const addons = this.state.addons || [];
			let addonIndex = -1;
			if (addonData.type === CONSTANTS.INTERNET_ADDITIONAL_TYPE) {
				addonIndex = addons.findIndex((a) => a.id === addonData.id);
			} else {
				addonIndex = addons.findIndex((a) => a.type === addonData.type);
			}

			if (addonIndex === -1) {
				addons.push(addonData);
			} else {
				addons[addonIndex] = addonData;
			}

			this.state.addons = addons;
		}
	}

	/**
	 * Handle change of addons from child components
	 */
	async handleChangedAddon(e) {
		this.setAddon(await this.buildAddonData(e.detail));
		this.dispatchStateChange();
	}
}