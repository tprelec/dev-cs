import { LightningElement, api, wire, track } from 'lwc';
import getProducts from '@salesforce/apex/OrderEntryController.getProducts';

const MAIN_PRODUCT_TYPE_NAME = 'Main';

export default class OrderEntryProductSelection extends LightningElement {
	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute('state', value);
		this._state = value;
		this.init();
	}

	@track products = [];
	@track _state = null;

	selectedMainProductIndex = -1;
	selectedMainProduct = null;

	init() {
		this.selectedMainProductIndex = this._state && this._state.products ? this._state.products.findIndex((p) => p.type === MAIN_PRODUCT_TYPE_NAME) : -1;

		if (this.products.length) {
			if (this.selectedMainProductIndex > -1) {
				this.selectedMainProduct = this._state.products[this.selectedMainProductIndex].id;
			} else {
				this.selectedMainProduct = this.products[0].value;
				this.setState(this.products[0].value);
			}
		}
	}

	@wire(getProducts, { productTypes: [MAIN_PRODUCT_TYPE_NAME] })
	productsWired({ error, data }) {
		if (data) {
			// make data ready for lwc combobox component
			this.products = data.map((p) => ({
				label: p.Name,
				value: p.Id
			}));
		} else if (error) {
			this.products = [];
		}

		this.init();
	}

	/**
	 * Validate input data
	 */
	@api validate() {
		const input = this.template.querySelector('lightning-combobox');
		input.reportValidity();
		return input.checkValidity();
	}

	handleChange(e) {
		this.setState(e.detail.value);
	}

	setState(productId) {
		// create payload clone to be able to update it
		const payloadDataClone = JSON.parse(JSON.stringify(this.state));
		const payloadProducts = payloadDataClone.products || [];

		if (this.selectedMainProductIndex === -1) { // if does not exist add it
			payloadProducts.push({
				type: MAIN_PRODUCT_TYPE_NAME,
				id: productId
			});
		} else { // if exists update it
			payloadProducts[this.selectedMainProductIndex].id = productId;
		}

		// update products in payload clone
		payloadDataClone.products = payloadProducts;

		// fire changed event to parent components
		const changedEvent = new CustomEvent('statechanged', { detail: payloadDataClone });

		this.dispatchEvent(changedEvent);
	}
}