import { api, LightningElement, track } from "lwc";
import getAddons from "@salesforce/apex/OrderEntryController.getAddons";
import getAddonPrice from "@salesforce/apex/OrderEntryController.getAddonPrice";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryProductTelephony extends LightningElement {
	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = JSON.parse(JSON.stringify(value));
	}
	@api products = [];

	@api validate() {
		let valid = true;
		this.template.querySelectorAll("c-order-entry-telephony-product-form").forEach((c) => {
			if (!c.validate()) {
				valid = false;
			}
		});

		return valid;
	}

	@track _state = null;

	secondaryType = CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE;

	get telProducts() {
		return this.products.filter((p) => p.Type__c === CONSTANTS.TELEPHONY_PRODUCT_TYPE);
	}

	get secondaryDisabled() {
		return !(this._state?.products || []).find(
			(p) => p.type === CONSTANTS.TELEPHONY_PRODUCT_TYPE
		);
	}

	/**
	 * Dispatch changes on state to parent components
	 */
	dispatchStateChange() {
		this.dispatchEvent(
			new CustomEvent("statechanged", { detail: this._state, bubbles: false })
		);
	}

	async handleProductSelection(e) {
		let payloadProducts = [...this._state.products];

		if (!e.detail.id) {
			if (e.detail.type === CONSTANTS.TELEPHONY_PRODUCT_TYPE) {
				payloadProducts = payloadProducts.filter(
					(p) =>
						p.type !== CONSTANTS.TELEPHONY_PRODUCT_TYPE &&
						p.type !== CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE
				);
				this._state.telephony = {};
			} else {
				payloadProducts = payloadProducts.filter((p) => p.type !== e.detail.type);
				delete this._state.telephony[e.detail.type];
			}
		} else {
			const productIndex = (this.state?.products || []).findIndex(
				(p) => p.type === e.detail.type
			);

			if (productIndex === -1) {
				// if does not exist add it
				payloadProducts.push(e.detail);
			} else {
				// if exists update it
				payloadProducts[productIndex] = e.detail;
			}
		}
		// update products in payload clone
		this.state.products = payloadProducts;
		await this.buildAddons(e.detail);
		this.dispatchStateChange();
	}

	handleUpdatedSettings(e) {
		this._state.telephony = e.detail;
		this.dispatchStateChange();
	}

	async buildAddons({ id, type }) {
		// rebuild list of selected addons and remove ones that are not related to selected telephony product
		const stateAddons = this.state?.addons || [];
		const stateProducts = (this.state?.products || []).map((p) => p.id);
		const otherAddons = stateAddons.filter((a) => {
			if (type === CONSTANTS.TELEPHONY_PRODUCT_TYPE) {
				return stateProducts.indexOf(a.parentProduct) > -1;
			}
			return stateProducts.indexOf(a.parentProduct) > -1 && a.parentType !== type;
		});

		// get addons from api
		const newAddons = [];
		if (id) {
			const addons = await getAddons({ parentProduct: id });
			const automaticAddons = addons.filter((a) => {
				if (type === CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE) {
					return (
						a.Automatic_Add_On__c ||
						a.Add_On__r.Type__c === CONSTANTS.TELEPHONY_LINE_TYPE
					);
				}
				return a.Automatic_Add_On__c;
			});

			for (const addon of automaticAddons) {
				// eslint-disable-next-line no-await-in-loop
				const builtAddon = await this.buildAddonData(addon, type);
				newAddons.push(builtAddon);
			}
		}

		this.state.addons = [...otherAddons, ...newAddons];
	}

	async buildAddonData(addon, type) {
		if (addon) {
			const quantity = 1;
			const price = await getAddonPrice({ code: addon.Add_On__r.Product_Code__c });
			const recurring =
				addon.Recurring_Price_Override__c || addon.Recurring_Price_Override__c === 0
					? addon.Recurring_Price_Override__c
					: price.cspmb__Recurring_Charge__c;
			const oneOff =
				addon.One_Off_Price_Override__c || addon.One_Off_Price_Override__c === 0
					? addon.One_Off_Price_Override__c
					: price.cspmb__One_Off_Charge__c;
			return {
				id: addon.Add_On__r.Id,
				quantity,
				type: addon.Add_On__r.Type__c,
				name: addon.Add_On__r.Name,
				parentProduct: addon.Product__c,
				code: addon.Add_On__r.Product_Code__c,
				recurring: recurring,
				oneOff: oneOff,
				totalRecurring: parseFloat((recurring * quantity).toFixed(2)),
				totalOneOff: parseFloat((oneOff * quantity).toFixed(2)),
				bundleName: price.Product_Name__c,
				parentType: type
			};
		}
		return null;
	}
}