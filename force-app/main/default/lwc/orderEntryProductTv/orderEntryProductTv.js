import { api, LightningElement, track } from "lwc";
import getAddons from "@salesforce/apex/OrderEntryController.getAddons";
import getAddonPrice from "@salesforce/apex/OrderEntryController.getAddonPrice";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryProductTv extends LightningElement {
	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = JSON.parse(JSON.stringify(value));
		this.init();
	}
	@api products = [];
	@api defaultBundle = null;

	@api validate() {
		const productInput = this.template.querySelector("c-order-entry-tv-product-form");
		const mediaboxInput = this.template.querySelector("c-order-entry-tv-additional-form");
		return productInput.validate() && (mediaboxInput ? mediaboxInput.validate() : true);
	}

	@track addons = [];
	@track addonsExist = false;
	@track _state = null;

	async init() {
		const selectedProduct = (this._state?.products || []).find(
			(p) => p.type === CONSTANTS.TV_PRODUCT_TYPE
		);
		if (selectedProduct) {
			this.setAddon(await this.buildAddons(selectedProduct.id));
		}
	}

	/**
	 * Get TV type products
	 */
	get tvProducts() {
		return this.products.filter((p) => p.Type__c === CONSTANTS.TV_PRODUCT_TYPE);
	}

	/**
	 * Get index of TV product
	 */
	get tvProductIndex() {
		return (this._state?.products || []).findIndex((p) => p.type === CONSTANTS.TV_PRODUCT_TYPE);
	}

	/**
	 * Handle product selection from child component
	 */
	async handleProductSelection(e) {
		const payloadProducts = [...this.state.products];

		if (this.tvProductIndex === -1) {
			// if does not exist add it
			payloadProducts.push({
				type: CONSTANTS.TV_PRODUCT_TYPE,
				id: e.detail
			});
		} else {
			// if exists update it
			payloadProducts[this.tvProductIndex].id = e.detail;
		}
		// update products in payload clone
		this.state.products = payloadProducts;
		await this.buildAddons(e.detail);
		this.dispatchStateChange();
	}

	async buildAddons(parentProductId) {
		// get addons from api for picklist
		this.addons = await getAddons({ parentProduct: parentProductId });
		this.addonsExist = this.addons && this.addons.length;

		// apply automatic addons
		for (let i = 0; i < this.addons.length; i++) {
			const addon = this.addons[i];
			if (addon.Automatic_Add_On__c) {
				this.setAddon(
					await this.buildAddonData({
						type: addon.Add_On__r.Type__c,
						id: addon.Add_On__r.Id,
						quantity: 1
					})
				);
			}
		}

		// rebuild list of selected addons and remove ones that are not related to selected internet product
		const stateAddons = this.state?.addons || [];
		const otherAddons = stateAddons.filter((a) => a.parentType !== CONSTANTS.TV_PRODUCT_TYPE);
		const tvAddons = stateAddons.filter(
			(a) => a.parentType === CONSTANTS.TV_PRODUCT_TYPE && a.parentProduct === parentProductId
		);

		this.state.addons = [...otherAddons, ...tvAddons];
	}

	/**
	 * Dispatch changes on state to parent components
	 */
	dispatchStateChange() {
		this.dispatchEvent(
			new CustomEvent("statechanged", { detail: this._state, bubbles: false })
		);
	}

	async buildAddonData({ type, id, quantity }) {
		const addon = this.addons.find((a) => a.Add_On__r.Id === id);

		if (addon) {
			const price = await getAddonPrice({ code: addon.Add_On__r.Product_Code__c });
			const recurring =
				addon.Recurring_Price_Override__c || addon.Recurring_Price_Override__c === 0
					? addon.Recurring_Price_Override__c
					: price.cspmb__Recurring_Charge__c;
			const oneOff =
				addon.One_Off_Price_Override__c || addon.One_Off_Price_Override__c === 0
					? addon.One_Off_Price_Override__c
					: price.cspmb__One_Off_Charge__c;
			return {
				id,
				quantity,
				type,
				name: addon.Add_On__r.Name,
				parentProduct: addon.Product__c,
				code: addon.Add_On__r.Product_Code__c,
				recurring: recurring,
				oneOff: oneOff,
				totalRecurring: parseFloat((recurring * quantity).toFixed(2)),
				totalOneOff: parseFloat((oneOff * quantity).toFixed(2)),
				bundleName: price.Product_Name__c,
				parentType: CONSTANTS.TV_PRODUCT_TYPE
			};
		}
		return null;
	}

	/**
	 * Handle change of addons from child components
	 */
	setAddon(addonData) {
		if (addonData) {
			const addons = this.state.addons || [];
			let addonIndex = -1;
			if (
				addonData.type === CONSTANTS.TV_MEDIABOX_TYPE ||
				addonData.type === CONSTANTS.TV_CHANNELS_TYPE
			) {
				addonIndex = addons.findIndex((a) => a.id === addonData.id);
			} else {
				addonIndex = addons.findIndex((a) => a.type === addonData.type);
			}

			if (addonIndex === -1) {
				addons.push(addonData);
			} else {
				addons[addonIndex] = addonData;
			}

			this.state.addons = addons;
		}
	}

	/**
	 * Handle change of addons from child components
	 */
	async handleChangedAddon(e) {
		this.setAddon(await this.buildAddonData(e.detail));
		this.dispatchStateChange();
	}
}