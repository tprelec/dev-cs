import { LightningElement, api, track, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getRecord } from "lightning/uiRecordApi";
import generateQuote from "@salesforce/apex/OrderEntryController.generateQuote";
import generateConfirmation from "@salesforce/apex/OrderEntryController.generateConfirmation";
import getDocumentRequest from "@salesforce/apex/OrderEntryController.getDocumentRequest";
import updateOpportunitySingedQuote from "@salesforce/apex/OrderEntryController.updateOpportunitySingedQuote";
import getOpportunity from "@salesforce/apex/OrderEntryController.getOpportunity";
import updateOpportunityAutomatedQuoteDelivery from "@salesforce/apex/OrderEntryController.updateOpportunityAutomatedQuoteDelivery";
import updateOpportunityStage from "@salesforce/apex/OrderEntryController.updateOpportunityStage";
import { NavigationMixin } from "lightning/navigation";
import POFILE_NAME_FIELD from "@salesforce/schema/User.Profile.Name";
import user from "@salesforce/user/Id";

export default class OrderEntryQuoteSignature extends NavigationMixin(LightningElement) {
	@api state;
	@track loading = false;
	docRequest;
	@track fileId;
	@track displaySignatureModal = false;
	@track showViewLastComponent = false;
	@track signedQuote = false;
	@track profileName;
	@track probability;

	@wire(getRecord, {
		recordId: user,

		optionalFields: [POFILE_NAME_FIELD]
	})
	async getProfileName({ error, data }) {
		if (data) {
			console.log(data.fields.Profile.displayValue);
			this.profileName = await data.fields.Profile.displayValue;
		} else if (error) {
			this.handleError(error);
		}
	}

	get showSendQuote() {
		if (this.profileName === "LG_NL D2D_Partner Sales User" && this.probability >= 85) {
			return false;
		}
		return true;
	}

	get showSendSignedConfirmation() {
		if (!this.showViewLastComponent) {
			return false;
		} else {
			return !(
				this.profileName === "LG_NL D2D_Partner Sales User" &&
				(this.probability >= 85 || this.signedQuote === false)
			);
		}
	}

	get sendConfirmationLabel() {
		return this.signedQuote === false ? "Send Confirmation" : "Send Signed Confirmation";
	}

	detectMob() {
		var hasTouchscreen = "ontouchstart" in window;
		return hasTouchscreen ? true : false;
	}

	get detectMobile() {
		if (this.detectMob() && !this.signedQuote) {
			return true;
		} else if (!this.detectMob()) {
			return false;
		}
		return false;
	}
	// getOpportunity information and set on form
	connectedCallback() {
		getOpportunity({
			oppId: this.state.opportunityId,
			fields: ["LG_SignedQuoteAttachmentId__c", "LG_SignedQuoteAvailable__c", "Probability"]
		})
			.then((res) => {
				this.showViewLastComponent = res.LG_SignedQuoteAttachmentId__c ? true : false;
				this.signedQuote = res.LG_SignedQuoteAvailable__c === "true" ? true : false;
				this.probability = res.Probability;
			})
			.catch((error) => {
				this.handleError(error);
				this.showViewLastComponent = false;
			});
	}

	// get Last Quote and open in preview
	async viewLastQuote() {
		this.loading = true;
		await getOpportunity({
			oppId: this.state.opportunityId,
			fields: ["LG_SignedQuoteAttachmentId__c"]
		})
			.then((res) => {
				if (res.LG_SignedQuoteAttachmentId__c) {
					var pdfViewer = this.template.querySelector("c-pdf-viewer");
					this.fileId = res.LG_SignedQuoteAttachmentId__c;
					pdfViewer.fileId = res.LG_SignedQuoteAttachmentId__c;
					pdfViewer.preview();
					this.loading = false;
				}
			})
			.catch((error) => {
				this.handleError(error);
				this.loading = false;
			});
	}

	// create quote and open in preview
	@api createQuote() {
		this.isViewLastComponent;
		this.loading = true;
		return generateQuote({ opportunityId: this.state.opportunityId })
			.then((result) => {
				if (result) {
					this.docRequest = result;
					this.checkDocRequestStatus();
					return true;
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	// open modal with signature component
	openSignatureModal() {
		var signatureModal = this.template.querySelector("c-order-entry-signature-modal");
		signatureModal.displayModal(true, this.fileId, this.state.primaryContactId);
	}

	// close modal
	closeModal() {
		var signatureModal = this.template.querySelector("c-order-entry-signature-modal");
		signatureModal.displayModal(false, this.fileId, this.state.primaryContactId);
	}

	// set AutomatedQuoteDelivery and send quote on email
	sendQuote() {
		this.loading = true;
		return updateOpportunityAutomatedQuoteDelivery({
			oppId: this.state.opportunityId,
			value: "Quote Requested"
		})
			.then((result) => {
				this.navigateToRecordPage();
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	// update opportuniti after signing
	confirmationSignature(e) {
		this.loading = false;
		if (e.detail) {
			this.fileId = e.detail;
			updateOpportunitySingedQuote({
				oppId: this.state.opportunityId,
				attachmentId: null,
				singed: true
			});
			this.signedQuote = true;
			this.showViewLastComponent = true;
			this.previewFile();
		}
	}

	// create confirmation
	@api createConfirmation() {
		this.loading = true;
		return generateConfirmation({ opportunityId: this.state.opportunityId })
			.then((result) => {
				if (result) {
					this.docRequest = result;
					this.checkDocRequestStatus();
					return true;
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	checkDocRequestStatus() {
		this.loading = true;
		return getDocumentRequest({ docRequestId: this.docRequest.Id })
			.then((result) => {
				if (result) {
					this.docRequest = result;
					if (this.docRequest.mmdoc__Status__c == "Completed") {
						this.loading = false;
						this.fileId = this.docRequest.mmdoc__File_Id__c;
						this.signedQuote = false;
						updateOpportunitySingedQuote({
							oppId: this.state.opportunityId,
							attachmentId: this.fileId,
							singed: false
						});
						this.showViewLastComponent = true;
						this.previewFile();
						return true;
					} else if (this.docRequest.mmdoc__Status__c == "Error") {
						this.loading = false;
						this.handleError({
							body: {
								message: "Error while generating document."
							}
						});
						return true;
					} else {
						this.sleep(2000);
						this.checkDocRequestStatus();
					}
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	// preview file
	previewFile() {
		var pdfViewer = this.template.querySelector("c-pdf-viewer");
		pdfViewer.fileId = this.fileId;
		pdfViewer.preview();
	}

	// sending a signed certificate
	async sendSignedConfirmation() {
		if (this.state.payment && this.state.payment.paymentType) {
			this.loading = true;
			return updateOpportunityStage({
				oppId: this.state.opportunityId,
				value: "Customer Approved"
			})
				.then((result) => {
					this.navigateToRecordPage();
				})
				.catch((error) => {
					this.handleError(error);
					return false;
				});
		} else {
			this.dispatchMessage(
				"Payment information",
				"Payment details are required to send signed confirmation",
				"warning"
			);
		}
	}

	sleep(ms) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}

	handleError(error) {
		console.log(error);
		this.loading = false;
		this.dispatchMessage("Something went wrong", error.body.message, "warning");
	}

	dispatchMessage(title, message, variant) {
		this.dispatchEvent(
			new ShowToastEvent({
				title,
				message,
				variant
			})
		);
	}

	// switch on opportunity record
	navigateToRecordPage() {
		this[NavigationMixin.Navigate]({
			type: "standard__recordPage",
			attributes: {
				recordId: this.state.opportunityId,
				objectApiName: "Opportunity",
				actionName: "view"
			}
		});
	}
}