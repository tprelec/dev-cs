import { LightningElement, api, track } from "lwc";

export default class OrderEntrySelectContact extends LightningElement {
	@track selectedContact = {};

	@track openSelectionModal = false;
	@api contacts;

	get options() {
		return this.contacts.map((item, index) => {
			let label = item.FirstName + " " + item.LastName;
			label = label.replace("undefined", "");
			let value = item.Id;

			return {
				label,
				value
			};
		});
	}

	closeModal() {
		this.openSelectionModal = false;
		this.dispatchEvent(new CustomEvent("closemodal", { detail: {}, bubbles: false }));
	}

	handleSelect() {
		this.openSelectionModal = false;
		this.dispatchEvent(
			new CustomEvent("closemodal", { detail: this.selectedContact, bubbles: false })
		);
	}

	handleSelected(event) {
		this.contacts.map((item, index) => {
			if (item.Id === event.detail.value) {
				this.selectedContact = item;
			}
		});
	}
}