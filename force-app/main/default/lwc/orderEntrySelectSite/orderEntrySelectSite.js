import { LightningElement, api, track } from "lwc";

export default class OrderEntrySelectSite extends LightningElement {
	@api sites;
	@track selectedSite = {};

	get options() {
		return this.sites.map((item, index) => {
			let label =
				item.Site_Street__c +
				", " +
				item.Site_House_Number__c +
				" " +
				item.Site_House_Number_Suffix__c;
			let value = item.Id;
			label = label.replace("undefined", "");
			return {
				label,
				value
			};
		});
	}

	closeModal() {
		this.dispatchEvent(new CustomEvent("closemodal", { detail: {}, bubbles: false }));
	}

	handleSelect() {
		this.dispatchEvent(
			new CustomEvent("closemodal", { detail: this.selectedSite, bubbles: false })
		);
	}

	handleSelected(event) {
		this.sites.map((item, index) => {
			if (item.Id === event.detail.value) {
				this.selectedSite = item;
			}
		});
	}
}