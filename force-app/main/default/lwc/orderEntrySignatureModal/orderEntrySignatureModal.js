import { LightningElement, track, api } from "lwc";
import getPrimaryContact from "@salesforce/apex/OrderEntryController.getPrimaryContact";

export default class OrderEntryCaptureSignatureModal extends LightningElement {
	@track displaySignatureModal = false;
	@track nameOfSignatoryfileId;
	@track nameOfSignatory;
	@track loading = false;

	@api
	async displayModal(value, fileId, primaryContactId) {
		this.loading = true;
		await getPrimaryContact({ contId: primaryContactId }).then((res) => {
			this.nameOfSignatory = res.FirstName + " " + res.LastName;
		});
		this.fileId = fileId;
		this.displaySignatureModal = value;
		this.loading = false;
	}

	// dispatch event for close modal
	closeModal() {
		this.dispatchEvent(
			new CustomEvent("closemodal", {
				bubbles: false
			})
		);
	}

	// clear signature
	clearSignature() {
		this.templates.querySelector("c-order-entry-signature").handleClearSignature();
	}

	changeNameOfSignatory(e) {
		this.nameOfSignatory = e.target.value;
	}

	// confirm signature
	confirmSignature() {
		this.loading = true;
		var signature = this.template.querySelector("c-order-entry-signature");
		signature.fileId = this.fileId;
		signature.nameOfSignatory = this.nameOfSignatory;
		this.template.querySelector("c-order-entry-signature").handleConfirmSignature();
	}

	// dispat event for confirmsignature and close modal
	confirmationSignature(e) {
		this.dispatchEvent(
			new CustomEvent("confirmsignature", {
				detail: e.detail,
				bubbles: false
			})
		);
		this.loading = false;
		this.closeModal();
	}
}