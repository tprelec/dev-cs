import { api, track, LightningElement } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryBundlePromotion extends LightningElement {
	@api state = null;
	@api promotions = [];

	@track selectedPromo = {};
	@track selectedPromotions = [];

	get formattedPromotions() {
		return this.promotions.map((sp) => ({
			value: sp.Id,
			label: sp.Promotion_Name__c
		}));
	}

	connectedCallback() {
		const specialPromotions =
			this.state && this.state.promos
				? this.state.promos.filter((p) => p.type === CONSTANTS.SPECIAL_PROMOTION_TYPE)
				: [];

		specialPromotions.forEach((sp) => {
			this.selectedPromotions.push({
				value: sp.id,
				label: sp.name
			});
		});
	}

	handlePromotionChange(e) {
		const id = e.detail.value;
		const name = e.target.options.find((opt) => opt.value === id).label;
		this.selectedPromo = { value: id, label: name };
	}

	/**
	 * Handle adding a special promotion
	 */
	addSpecialPromotion() {
		let promotionIndex = -1;
		promotionIndex = this.selectedPromotions.findIndex(
			(sp) => sp.value === this.selectedPromo.value
		);

		if (promotionIndex === -1 && this.selectedPromo.value) {
			this.selectedPromotions.push(this.selectedPromo);
			const id = this.selectedPromo.value;
			const type = CONSTANTS.SPECIAL_PROMOTION_TYPE;
			const promotionData = { id, type };

			const promotionPicklist = this.template.querySelector(
				"lightning-combobox.special-promotions"
			);
			promotionPicklist.value = null;

			this.dispatchEvent(
				new CustomEvent("selectedpromotion", { detail: promotionData, bubbles: false })
			);
		} else if (promotionIndex !== -1) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: "Unable to add Promotion",
					message: "Promotion already added",
					variant: "warning"
				})
			);
		} else if (!this.selectedPromo.value) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: "No Promotion selected",
					message: "Select a Promotion first",
					variant: "warning"
				})
			);
		}
	}

	/**
	 * Handle removing a special promotion
	 */
	removeSpecialPromotion(e) {
		const index = e.target.value;
		console.log(index);
		const id = this.selectedPromotions[index].value;
		this.selectedPromotions.splice(index, 1);
		this.dispatchEvent(
			new CustomEvent("removespecialpromotion", { detail: id, bubbles: false })
		);
	}
}