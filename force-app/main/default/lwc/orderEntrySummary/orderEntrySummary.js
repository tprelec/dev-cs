import { api, LightningElement } from "lwc";

export default class OrderEntrySummary extends LightningElement {
	@api state = null;

	opened = false;

	get classes() {
		let clss = "slds-docked-composer slds-grid slds-grid_vertical ";

		if (this.opened) {
			clss += "slds-is-open";
		} else {
			clss += "slds-is-closed";
		}

		return clss;
	}

	toggleVisibility() {
		this.opened = !this.opened;
	}
}