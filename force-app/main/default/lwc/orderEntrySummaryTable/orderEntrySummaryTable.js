import { api, LightningElement } from "lwc";

export default class OrderEntrySummaryTable extends LightningElement {
	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = JSON.parse(JSON.stringify(value));
		console.log("summary state", this._state);
	}

	_state = null;

	get contractTerm() {
		return this.state.contractTerm ? parseInt(this.state.contractTerm, 10) * 12 : 0;
	}

	get showBody() {
		return this.contractTerm || this.state?.bundle;
	}

	/**
	 * Extract all discounts from promotions and sort them by duration
	 */
	get discounts() {
		const discounts = [];
		(this.state?.promos || []).forEach((p) => {
			discounts.push(...(p.discounts || []));
		});
		discounts.forEach((d) => {
			if (!d.duration) {
				d.duration = this.contractTerm;
			}
		});
		return discounts.sort(this.compare);
	}

	/**
	 * Map discounts to array of durations
	 */
	get discountDurations() {
		return this.discounts.map((d) => d.duration);
	}

	/**
	 * Get array of unique durations
	 */
	get uniqueDurations() {
		return [...new Set(this.discountDurations)];
	}

	/**
	 * Make array of duration breakpoints
	 */
	get discountDurationBreakpoints() {
		const breakpoints = [];
		const longestDuration = this.uniqueDurations[this.uniqueDurations.length - 1];
		let durations =
			longestDuration < this.contractTerm
				? [...this.uniqueDurations, this.contractTerm]
				: [...this.uniqueDurations];
		let last = 0;

		if (!durations.length) {
			const from = 1;
			const to = this.contractTerm;
			breakpoints.push({
				label: `${from}-${to}`,
				from,
				to,
				to
			});
			return breakpoints;
		}

		durations.forEach((duration) => {
			const from = last + 1;
			const to = duration;
			const diff = to - last;
			last = to;
			breakpoints.push({
				label: `${from}-${to}`,
				from,
				to,
				diff
			});
		});
		return breakpoints;
	}

	/**
	 * Get selected bundle discount
	 */
	get bundleDiscount() {
		return this.discounts.find((d) => d.level === "Bundle");
	}

	/**
	 * Get only selected addon discounts
	 */
	get addonDiscounts() {
		return this.discounts.filter((d) => d.level === "Add On");
	}

	get productsWithDiscounts() {
		const data = [];
		const bundle = this.state?.bundle;
		const addons = this.state?.addons || [];

		// build bundle item
		if (bundle) {
			data.push({
				...bundle,
				name: bundle.name.replaceAll("+", " & "),
				discount: this.bundleDiscount,
				prices: {
					monthly: this.discountDurationBreakpoints.map((bp) => {
						return {
							...bp,
							priceData: this.calculateRecurringDiscount(
								bp,
								bundle,
								this.bundleDiscount
							)
						};
					}),
					oneOff: {
						priceData: this.calculateOneOffDiscount(bundle, this.bundleDiscount)
					}
				},
				promoDescription: bundle.promoDescription
			});
		}

		// build addon items
		addons
			.filter((a) => a.quantity)
			.forEach((a) => {
				const adc = this.addonDiscounts.find((ad) => ad.addon === a.id);
				data.push({
					...a,
					discount: adc,
					prices: {
						monthly: this.discountDurationBreakpoints.map((bp) => {
							return {
								...bp,
								priceData: this.calculateRecurringDiscount(bp, a, adc)
							};
						}),
						oneOff: {
							priceData: this.calculateOneOffDiscount(a, adc)
						}
					}
				});
			});

		return data;
	}

	/**
	 * Calculate totals
	 */
	get totals() {
		const data = [];
		this.productsWithDiscounts.forEach((product) => {
			product.prices.monthly.forEach((p) => {
				let tempIndex = data.findIndex((d) => d.label === `${p.from}-${p.to}`);
				if (tempIndex === -1) {
					data.push({ label: `${p.from}-${p.to}`, price: p.priceData.price });
				} else {
					data[tempIndex].price = data[tempIndex].price + p.priceData.price;
				}
			});

			const oneOffLabel = "oneOff";
			const oneOffTempIndex = data.findIndex((d) => d.label === oneOffLabel);

			if (oneOffTempIndex === -1) {
				data.push({
					label: oneOffLabel,
					price: parseFloat(product.prices.oneOff.priceData.price) || 0
				});
			} else {
				data[oneOffTempIndex].price =
					parseFloat(data[oneOffTempIndex].price) +
					(parseFloat(product.prices.oneOff.priceData.price) || 0);
			}
		});
		return data.map((i) => ({
			label: i.label,
			price: (Math.round((i.price + Number.EPSILON) * 100) / 100).toFixed(2)
		}));
	}

	/**
	 * Helper compare function for sorting discounts by duration
	 */
	compare(a, b) {
		const durationA = a.duration;
		const durationB = b.duration;

		let comparison = 0;
		if (durationA > durationB) {
			comparison = 1;
		} else if (durationA < durationB) {
			comparison = -1;
		}
		return comparison;
	}

	/**
	 * Calculate discount for product, breakpoint and discount for monthly recurring prices
	 */
	calculateRecurringDiscount(breakpoint, item, discount) {
		let price = parseFloat(item.totalRecurring);
		const oneOffPrice = parseFloat(item.totalOneOff);

		if (discount && discount.duration >= breakpoint.to) {
			price = parseFloat(item.recurring);
			const quantity = item.quantity;

			if (discount.type === "Percentage") {
				price = price
					? price - (price * discount.value) / 100 + price * (quantity - 1)
					: price;
			} else if (discount.type === "Discounted Amount") {
				price = price - discount.value + price * (quantity - 1);
			} else if (discount.type === "Amount") {
				price = discount.value + price * (quantity - 1);
			}
		}

		// if one off price exists and recurring is 0 then return null, otherwise return price
		return {
			hasPrice: !!(price || !oneOffPrice),
			isNull: price <= 0,
			formattedPrice:
				price !== undefined && price !== null ? parseFloat(price).toFixed(2) : null,
			price
		};
	}

	/**
	 * Calculate discount for product, breakpoint and discount for one off prices
	 */
	calculateOneOffDiscount(item, discount) {
		let price = parseFloat(item.totalOneOff);

		if (price && discount) {
			price = parseFloat(item.oneOff);
			const quantity = item.quantity;

			if (discount.type === "Percentage") {
				price = price
					? price - (price * discount.value) / 100 + price * (quantity - 1)
					: price;
			} else if (discount.type === "Discounted Amount") {
				price = price - discount.value + price * (quantity - 1);
			} else if (discount.type === "Amount") {
				price = discount.value + price * (quantity - 1);
			}
		}

		return {
			hasPrice:
				price >= 0 &&
				item.oneOff >= 0 &&
				!item.recurring > 0 &&
				item.type !== "Internet Security",
			isNull: price <= 0,
			formattedPrice:
				price !== undefined && price !== null ? parseFloat(price).toFixed(2) : null,
			price
		};
	}
}