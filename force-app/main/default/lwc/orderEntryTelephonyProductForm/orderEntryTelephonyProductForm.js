import { api, LightningElement } from "lwc";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();
const DELAY = 400;

export default class OrderEntryTelephonyProductForm extends LightningElement {
	@api state = null;
	@api products = [];
	@api disableable = false;
	@api title = "Telephony";
	@api productType = CONSTANTS.TELEPHONY_PRODUCT_TYPE;
	@api disabled = false;

	@api validate() {
		const input = this.template.querySelector('lightning-input[data-type="porting-number"]');
		input.reportValidity();
		return input.checkValidity();
	}

	get settings() {
		const allSettings = this.state?.telephony;
		return allSettings ? allSettings[this.productType] : null;
	}

	get isActive() {
		return this.settings?.enabled || !this.disableable;
	}

	get allDisabled() {
		return !this.isActive;
	}

	get selectedProduct() {
		const product = (this.state?.products || []).find((p) => p.type === this.productType);
		return product ? product.id : null;
	}

	get isToggleDisabled() {
		return !this.selectedProduct;
	}

	get formattedProducts() {
		return this.products.map((p) => ({
			value: p.Id,
			label: p.Name
		}));
	}

	get portingNumberEnabled() {
		return this.settings?.portingEnabled;
	}

	get portingNumberValue() {
		return this.settings?.portingNumber || null;
	}

	get isPortingNumberRequired() {
		return this.portingNumberEnabled;
	}

	get isPortingNumberDisabled() {
		return !this.portingNumberEnabled;
	}

	handleProductChange(e) {
		this.dispatchEvent(
			new CustomEvent("selectedproduct", {
				detail: { id: e.detail.value, type: this.productType },
				bubbles: false
			})
		);
	}

	handlePortingNumberChange(e) {
		window.clearTimeout(this.delayTimeout);
		const telephonySettings = this.state?.telephony
			? JSON.parse(JSON.stringify(this.state.telephony))
			: {};
		const value = e.detail.value;

		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			telephonySettings[this.productType] = Object.assign(
				{},
				telephonySettings[this.productType] || {},
				{
					portingNumber: value
				}
			);

			this.dispatchEvent(
				new CustomEvent("updatedsettings", {
					detail: telephonySettings,
					bubbles: false
				})
			);
		}, DELAY);
	}

	handlePortingStatusChange(e) {
		const telephonySettings = this.state?.telephony
			? JSON.parse(JSON.stringify(this.state.telephony))
			: {};
		const checked = e.detail.checked;

		telephonySettings[this.productType] = Object.assign(
			{},
			telephonySettings[this.productType] || {},
			{
				portingEnabled: checked,
				portingNumber: null
			}
		);

		if (!checked) {
			// eslint-disable-next-line @lwc/lwc/no-async-operation
			setTimeout(() => {
				const input = this.template.querySelector(
					'lightning-input[data-type="porting-number"]'
				);
				input.reportValidity();
			}, 10);
		}

		this.dispatchEvent(
			new CustomEvent("updatedsettings", {
				detail: telephonySettings,
				bubbles: false
			})
		);
	}

	handleLineStatusChange(e) {
		const telephonySettings = this.state?.telephony
			? JSON.parse(JSON.stringify(this.state.telephony))
			: {};

		if (e.detail.checked) {
			telephonySettings[this.productType] = Object.assign(
				{},
				telephonySettings[this.productType] || {},
				{
					enabled: e.detail.checked
				}
			);
		} else {
			// eslint-disable-next-line @lwc/lwc/no-async-operation
			setTimeout(() => {
				const input = this.template.querySelector(
					'lightning-input[data-type="porting-number"]'
				);
				input.reportValidity();
			}, 10);

			delete telephonySettings[this.productType];
			this.handleRemoveProduct();
		}

		this.dispatchEvent(
			new CustomEvent("updatedsettings", {
				detail: telephonySettings,
				bubbles: false
			})
		);
	}

	handleRemoveProduct() {
		if (this.selectedProduct) {
			this.dispatchEvent(
				new CustomEvent("selectedproduct", {
					detail: { id: null, type: this.productType },
					bubbles: false
				})
			);
		}
	}
}