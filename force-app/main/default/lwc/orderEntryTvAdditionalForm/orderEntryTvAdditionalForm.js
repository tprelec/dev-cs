import { api, LightningElement, track } from "lwc";
import { getConstants } from "c/orderEntryConstants";
const CONSTANTS = getConstants();
export default class OrderEntryTvAdditionalForm extends LightningElement {
	@api
	get addons() {
		return this._addons;
	}
	set addons(value) {
		this.setAttribute("addons", value);
		this._addons = value;
		this.buildMediabox();
		this.buildToggles();
	}
	@api
	get state() {
		return this._state;
	}
	set state(value) {
		this.setAttribute("state", value);
		this._state = JSON.parse(JSON.stringify(value));
		this.buildMediabox();
		this.buildToggles();
	}
	@api validate() {
		const mediaboxInput = this.template.querySelector("lightning-input.mediabox");
		if (mediaboxInput) {
			mediaboxInput.reportValidity();
			return mediaboxInput.checkValidity();
		}
		return true;
	}
	@track _state = null;
	@track _addons = [];
	@track mediaboxAddons = [];
	@track channelsAddons = [];
	get mediaboxChunked() {
		return this.chunk(this.mediaboxAddons, 2);
	}
	get indexedMediaboxChunks() {
		return this.mediaboxChunked.map((c, i) => ({
			items: c,
			i
		}));
	}
	get channelsChunked() {
		return this.chunk(this.channelsAddons, 2);
	}
	get indexedChannelsChunks() {
		return this.channelsChunked.map((c, i) => ({
			items: c,
			i
		}));
	}
	chunk(arr, size) {
		return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
			arr.slice(i * size, i * size + size)
		);
	}
	buildMediabox() {
		this.mediaboxAddons = [];
		if (this.state && this.addons && this.addons.length) {
			this.addonsExist = true;
			const items = this.addons.filter(
				(a) => a.Add_On__r.Type__c === CONSTANTS.TV_MEDIABOX_TYPE
			);
			const stateAddons = this.state.addons || [];
			this.mediaboxAddons = items.map((i) => {
				const stateAddon = stateAddons.find((sa) => sa.id === i.Add_On__r.Id);
				return {
					...i,
					quantity: stateAddon ? stateAddon.quantity : 0
				};
			});
		}
	}
	buildToggles() {
		this.channelsAddons = [];
		if (this.state && this.addons && this.addons.length) {
			this.addonsExist = true;
			const items = this.addons.filter(
				(a) => a.Add_On__r.Type__c === CONSTANTS.TV_CHANNELS_TYPE
			);
			const stateAddons = this.state.addons || [];
			this.channelsAddons = items.map((i) => {
				const stateAddon = stateAddons.find((sa) => sa.id === i.Add_On__r.Id);
				return {
					...i,
					quantity: stateAddon ? stateAddon.quantity : 0
				};
			});
		}
	}
	handleMediabox(e) {
		this.buildAddonData(
			CONSTANTS.TV_MEDIABOX_TYPE,
			e.target.name,
			parseInt(e.detail.value, 10)
		);
	}
	handleTvChannels(e) {
		this.toggleTvChannels(e.target.name, e.detail.checked);
	}
	toggleTvChannels(id, value) {
		this.buildAddonData(CONSTANTS.TV_CHANNELS_TYPE, id, value === true ? 1 : 0);
	}
	buildAddonData(type, id, quantity) {
		const addonData = {
			id,
			quantity,
			type
		};
		this.dispatchEvent(new CustomEvent("changedaddon", { detail: addonData, bubbles: false }));
	}
}