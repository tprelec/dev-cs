import { api, LightningElement, track } from "lwc";
import { getConstants } from "c/orderEntryConstants";

const CONSTANTS = getConstants();

export default class OrderEntryTvProductForm extends LightningElement {
	@api state = null;
	@api products = [];
	@api addons = [];

	@api validate() {
		const productInput = this.template.querySelector("lightning-combobox");
		productInput.reportValidity();
		return productInput.checkValidity();
	}

	get selectedProduct() {
		const tvProduct =
			this.state && this.state.products
				? this.state.products.find((p) => p.type === CONSTANTS.TV_PRODUCT_TYPE)
				: null;

		return tvProduct ? tvProduct.id : null;
	}

	/**
	 * Get list of formated products for picklist
	 */
	get formattedProducts() {
		return this.products.map((p) => ({
			value: p.Id,
			label: p.Name
		}));
	}

	/**
	 * Handle change on product picklist
	 */
	handleProductChange(e) {
		if (
			e.detail.value !==
			this.state.products.find((p) => p.type === CONSTANTS.TV_PRODUCT_TYPE).id
		) {
			this.selectProduct(e.detail.value);
		}
	}

	/**
	 * Set selected product and dispatch to parent components
	 */
	selectProduct(productId) {
		this.dispatchEvent(
			new CustomEvent("selectedproduct", { detail: productId, bubbles: false })
		);
	}

	/**
	 * Build addon data and dispatch to parent component
	 */
	buildAddonData(type, id, quantity) {
		const addonData = {
			id,
			quantity,
			type
		};
		this.dispatchEvent(new CustomEvent("changedaddon", { detail: addonData, bubbles: false }));
	}
}