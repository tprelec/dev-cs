import { LightningElement, api, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { loadScript, loadStyle } from "lightning/platformResourceLoader";
import pdfjs from "@salesforce/resourceUrl/pdfjs";
import getContent from "@salesforce/apex/PdfViewerController.getContent";

export default class PdfViewer extends LightningElement {
	@api
	fileId;
	file;
	isFilePreviewed = false;
	pdfJsInitialized = false;
	pdfjs;
	pdf;
	@track
	loading = false;

	async renderedCallback() {
		if (this.pdfJsInitialized) {
			return;
		}
		this.pdfJsInitialized = true;
		await Promise.all([loadScript(this, pdfjs + "/pdfjs/pdf.js")])
			.then(() => {
				this.initPdfJs();
			})
			.catch((error) => {
				this.handleError(error);
			});
	}

	initPdfJs() {
		this.pdfjs = pdfjsLib;
		this.pdfjs.GlobalWorkerOptions.workerSrc = pdfjs + "/pdfjs/pdf.worker.js";
	}

	@api
	preview() {
		this.loading = true;
		if (!this.pdfjs) {
			this.handleError({ body: "PDF Library not initialized" });
			return;
		}
		if (!this.fileId) {
			this.handleError({ body: "Please provide PDF Id" });
			return;
		}
		return getContent({ fileId: this.fileId })
			.then((result) => {
				if (result) {
					this.file = result;
					this.file.data = atob(this.file.data);
					this.isFilePreviewed = true;
					this.loading = false;
					this.renderPDF();
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	renderPDF() {
		var loadingTask = this.pdfjs.getDocument({
			data: this.file.data,
			ownerDocument: document
		});
		return loadingTask.promise
			.then((pdf) => {
				this.pdf = pdf;
				this.template.querySelector("div.canvas-container").innerHTML = "";
				this.renderPDFPage(1, true);
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	renderPDFPage(pageNumber, renderNext) {
		if (this.pdf.numPages < pageNumber) {
			return;
		}
		this.pdf.getPage(pageNumber).then((page) => {
			var scale = 1.5;
			var viewport = page.getViewport({ scale: scale });
			// Prepare canvas using PDF page dimensions
			var canvas = document.createElement("canvas");
			var context = canvas.getContext("2d");
			canvas.height = viewport.height;
			canvas.width = viewport.width;
			// Render PDF page into canvas context
			var renderContext = {
				canvasContext: context,
				viewport: viewport
			};

			page.render(renderContext);
			this.template.querySelector("div.canvas-container").appendChild(canvas);
			if (renderNext) {
				this.renderPDFPage(pageNumber + 1, true);
			}
		});
	}

	handleError(error) {
		this.loading = false;
		this.isFilePreviewed = false;
		this.dispatchEvent(
			new ShowToastEvent({
				title: "Something went wrong",
				message: error.body ? error.body.message : error.message,
				variant: "warning"
			})
		);
	}

	@api
	close() {
		this.isFilePreviewed = false;
	}
}