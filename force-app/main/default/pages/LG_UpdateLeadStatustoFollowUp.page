<!--
* File Name     :  LG_UpdateLeadStatustoFollowUp
* Description   :  This page controls the SF1 action "Follow Up" on lead record

* @author       :   Shreyas
* Modification Log
===================================================================================================
* Ver.    Date          Author              Modification
---------------------------------------------------------------------------------------------------
* 1.0      9th-Aug-16    Shreyas             Created the class for release R1.5

-->

<apex:page standardController="Lead" extensions="LG_SalesforceOneLeadActionsController" action="{!updateLeadStatusToFollowUp_OnLoadAction}" doctype="html-5.0">

    <script>

        function updateLeadStatusToFollowUp(){
        }

        function redirectToLeadRecord(){
            var leadRecordId = '{!leadId}';

            var status = document.getElementById('j_id0:form:blk:j_id5:j_id6').value;
            var date_StringFormat = document.getElementById('j_id0:form:blk:date').value;
            var date_DateFormat = new Date(date_StringFormat);
            var time = document.getElementById('j_id0:form:blk:time').value;
            var desc = document.getElementById('j_id0:form:blk:description').value;

            Visualforce.remoting.Manager.invokeAction( '{!$RemoteAction.LG_SalesforceOneLeadActionsController.updateLeadStatusToFollowUp_Remote}',
                                                       leadRecordId, status, date_DateFormat, time, desc ,
                                                       function(result, event){

                                                           if (result == 'Success') {
                                                               sforce.one.navigateToSObject(leadRecordId);
                                                           }
                                                           else{
                                                               var customErrorMessage = "Something went wrong, please contact system administrator.";
                                                               customErrorMessage = customErrorMessage + "<br>\n</br>" + result;
                                                               document.getElementById("{!$Component.form.blk.errorMessage}").innerHTML = customErrorMessage;
                                                           }
                                                       }

            );


        }



    </script>

    <apex:stylesheet value="{!URLFOR($Resource.LG_BootstrapSF1, 'css/bootstrap-namespaced.css')}" />

    <style>
        html, body, p {
            font-family: "ProximaNovaSoft-Regular", Calibri,
                "Gill Sans", "Gill Sans MT", Candara, Segoe,
                "Segoe UI", Arial, sans-serif;
            font-size: 100%;
            background-color: white;
        }
        a:link, a:hover, a:visited, a:active{
            text-decoration: none;
            color: black;
        }
        tr td a {
            display:block;
            height:100%;
            width:100%;
            padding-left: 0;
            padding-right: 0;
        }
        tr.border_bottom td {
          border-bottom:1pt solid #F1F1F3;
        }
        tr.empty td{
          border-bottom:0pt;
        }
        #event-menu{
            display: inline-block;
            width: 100%;
            padding-bottom: 20px;
            margin-bottom: 0px;
        }
        #filter-div{
            display: inline-block;
            text-align: left;
            width: 80%;
        }
        #button-div{
            display: inline-block;
            width: 19%;
        }
        #view-list{
            display: inline-block;
            font-weight: bold;
        }
        #icon{
            display: inline-block;
            margin-right: 10px;
        }
        #button {
            display: table;
            width: 100%;
        }
        input {
            font-size: 95%;
        }
    </style>

    <apex:form id="form">

         <div class="bootstrap-sf1">
             <apex:pageBlock id="blk">
                <apex:pageblockSection >
                 <apex:inputfield value="{!leadRecord.Status}" label="Lead Status: " />
                </apex:pageblockSection>


                <br></br>

                <apex:outputLabel value="*Preferred Contact Date:  " for="date"/>
                <apex:input id="date" value="{!lead_PreferredContactDate}" type="date" required="true"/><br/>

                <br></br>

                <apex:outputLabel value="*Preferred Contact Time:  " for="time"/>
                <apex:input id="time" value="{!lead_PreferredContactTime}" type="text" required="true"/><br/>

                <br></br>

                <apex:outputLabel value="Follow-Up Description: " for="description"/>
                <apex:input id="description" value="{!lead_FollowUpDescription}" type="text"/><br/>



                <br></br>
                <apex:pageblockButtons location="bottom">
                    <apex:commandButton value="Save" styleClass="btn btn-lg btn-default" onclick="updateLeadStatusToFollowUp()" oncomplete="redirectToLeadRecord()"/>
                </apex:pageblockButtons>

                <apex:outputText id="errorMessage"/>

            </apex:pageBlock>

            <apex:actionFunction action="{!updateLeadStatusToFollowUp}" name="updateLeadStatusToFollowUp" rerender="showstate">
                <apex:param name="firstParam" assignTo="{!leadId}" value="{!leadId}"  />
            </apex:actionFunction>


        </div>
    </apex:form>

</apex:page>