<apex:page title="Order Form" tabStyle="Order__c" showHeader="false" sidebar="false" controller="OrderFormTemplateController" extensions="PP_LightningController" standardstylesheets="true" lightningStylesheets="true">



    <style type="text/css">
        @import "{!URLFOR($Resource.jQuery_1_12, '/js/ui/1.12.1/themes/base/jquery-ui.css')}";
    </style>

    <style type="text/css">
        .ui-state-highlight {
            background: #B0E1FA!Important;
            border: 1px solid #FED22F;
            color: #363636;
        }

        .hide {
            display: none;
        }

        .darkClass {
            background-color: white;
            filter: alpha(opacity=50);
            /* IE */
            opacity: 0.5;
            /* Safari, Opera */
            -moz-opacity: 0.50;
            /* FireFox */
            z-index: 20;
            height: 100%;
            width: 100%;
            background-repeat: no-repeat;
            background-position: center;
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .customPopup {
            background-color: white;
            border-style: solid;
            border-width: 1px;
            left: 50%;
            padding: 10px;
            position: absolute;
            z-index: 9999;
            /* These are the 3 css properties you will need to tweak so the pop
            up displays in the center of the screen. First set the width. Then set
            margin-left to negative half of what the width is. You can also add
            the height property for a fixed size pop up.*/
            width: 500px;
            margin-left: -250px;
            top: 100px;
        }

        /* this is to save some page real estate at the top*/

        body .bPageTitle {
            padding: 0px 0 0;
        }

        .colClassName {
            vertical-align: top;
        }

        .firstcolClassName {
            vertical-align: top;
            width: 200px;
        }
    </style>

    <style type="text/css" title="currentStyle">
        @import "{!URLFOR($Resource.DataTables, 'media/css/demo_table.css')}";
    </style>


    <script type="text/javascript" src="{!URLFOR($Resource.jQuery, '/jquery-1.8.3.min.js')}"></script>
    <script type="text/javascript" src="{!URLFOR($Resource.DataTables, 'media/js/jquery.dataTables.js')}"></script>
    <script type="text/javascript" src="{!URLFOR($Resource.jQuery, '/jquery.maskedinput-1.3.min.js')}"></script>

    <script type="text/javascript">
        $j = jQuery.noConflict();
    </script>

    <script type="text/javascript" src="{!URLFOR($Resource.formatter, '/jquery.formatter.min.sfdc.js')}"></script>


    <script type="text/javascript" charset="UTF-8">
        $j = jQuery.noConflict();

        function addOrderDataTableMarkup() {
            var oTable = $j("table[id$='orderTable']").dataTable({
                "bFilter": false,
                "bPaginate": false,
                "bInfo": false,
                "bAutoWidth": false,
                "aoColumns": [
                    /* Id */   { "bVisible": false },
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ]
            });

            /* Add a click handler to the orderTable rows - this could be used as a callback */
            $j('#orderTable tbody tr').click(function (event) {
                $j(oTable.fnSettings().aoData).each(function () {
                    $j(this.nTr).removeClass('ui-state-highlight');
                });
                $j(event.target.parentNode).addClass('ui-state-highlight');
            });

            $j('#orderTable').delegate('tr', 'click', function (event) {
                var aPos = oTable.fnGetPosition(this);
                var aData = oTable.fnGetData(aPos);
                oIDNumber = aData[0];
                setSelectedOrderRow(oIDNumber);
            });
        }

        function dimOff() {
            document.getElementById("darkLayer").style.display = "none";
        }
        function dimOn() {
            document.getElementById("darkLayer").style.display = "";
        }

        function initializeSaveButton(unhide) {
            // add the current value to the select to track any change
            $j('select[id$=Select], select[id$=Select_selected]').each(function () {
                $j(this).data("previous-value", $j('option:selected', $j(this)).val());
            });

            var inputs = $j('input[id$=Input], select[id$=Select], select[id$=Select_selected],textarea[id$=Textarea]');
            inputs.on('keyup change DOMSubtreeModified', function () {
                var dataChanged = inputs.filter(function () {
                    if ($j(this).is('select')) {
                        console.log($j(this).val());
                        var currentValue = $j(this).val();
                        var originalValue = $j(this).data('previous-value');
                    } else if ($j(this).is('multiselect')) {
                        console.log($j(this).val());
                        var currentValue = $j(this).val();
                        var originalValue = $j(this).data('previous-value');
                    } else if ($j(this).is(':checkbox')) {
                        var originalValue = this.defaultChecked;
                        var currentValue = this.checked;
                    } else {
                        var originalValue = this.defaultValue;
                        var currentValue = this.value;
                    }
                    return originalValue != currentValue;
                }).length;
                if (dataChanged == 0) {
                    $j('input[id$=SaveButton]').addClass('hide');
                } else {
                    $j('input[id$=SaveButton]').removeClass('hide');
                }

            });

            // add the option to initially make the button visible (e.g. after rerender)
            if (unhide) {
                $j('input[id$=SaveButton]').removeClass('hide');
            }

        }

        function setFocusOnLoad() { }

        function checkMe() {
            if (confirm("{!$Label.LABEL_Order_Confirm_Submit}")) {
                disableMe;
                goToProgressPage();
                return true;
            } else {
                return false;
            }
        }

        function submitAfterUpdate() {
            submitOrder();
        }



    </script>

    <html>

    <body>
        <div id="darkLayer" class="darkClass" style="display:none"></div>

        <!-- HEADER BAR -->
        <apex:outputPanel id="orderform-container">
            <apex:form >

                <!-- POPUP FOR NEW ORDER CREATION -->
                <apex:outputPanel rendered="{!showCreateNewOrder}">

                    <div id="darkLayerPopup" class="darkClass"></div>
                    <apex:outputPanel styleClass="customPopup" layout="block">
                        <apex:pageBlock title="Select contracted products">
                            <apex:pageBlockButtons >
                                <apex:commandButton value="Confirm" action="{!confirmAddToOrder}" rerender="page-container,menu-container,orderform-container"
                                    status="loading-status" />

                                <apex:commandButton value="Cancel" action="{!cancelCreateNewOrder}" rerender="page-container,menu-container,orderform-container"
                                    status="loading-status" />
                            </apex:pageBlockButtons>
                            <apex:pageMessages />
                            <apex:pageBlockTable value="{!cpWrappers}" var="cpw">
                                <apex:column >
                                    <apex:inputcheckBox value="{!cpw.selected}" />
                                </apex:column>
                                <apex:column value="{!cpw.cp.Name}" />
                                <apex:column value="{!cpw.cp.Line_Description__c}" />
                                <apex:column value="{!cpw.cp.Site__c}" />
                                <apex:column value="{!cpw.cp.Quantity__c}" />
                                <apex:column value="{!cpw.cp.Duration__c}" />

                            </apex:pageBlockTable>
                            <apex:pageBlockSection columns="1">
                                <apex:pageBlockSectionItem >
                                    <apex:outputLabel value="Select order for the selected items" />
                                    <apex:selectList value="{!orderSelected}">
                                        <apex:selectOptions value="{!availableOrders}" />
                                    </apex:selectList>
                                </apex:pageBlockSectionItem>
                            </apex:pageBlockSection>
                        </apex:pageBlock>
                    </apex:outputPanel>
                </apex:outputPanel>


                <!-- TITLE -->
                <apex:outputPanel style="float:left;padding: 15px 30px 3px 10px;">
                    <h1 class="pageType">{!$Label.LABEL_Order_Form}</h1>
                </apex:outputPanel>
                <!--/div-->

                <apex:pageBlock rendered="{! showValidationMessage}">
                    <apex:actionFunction name="submitOrder" action="{!submitOrder}" />
                    <apex:pageMessage severity="info" title="No Price Plan Class “Data”" detail="{!$Label.EMP_Proceed_With_Automated_Provisioning}"
                        rendered="{! showValidationMessage}" strength="2" />
                    <apex:commandButton action="{!submitOrder}" rendered="{! showValidationMessage}" value="Yes" />
                    <apex:commandButton action="{!procedeWithManualProvisioning}" rendered="{! showValidationMessage}" value="No" oncomplete="submitAfterUpdate()"
                    />
                </apex:pageBlock>


                <!-- WARNING FOR UNORDERED ITEMS -->
                <apex:outputPanel rendered="{!unorderedItems>0 && ShowSubmitButton}">
                    <!-- custom warning box (based on apex:message) TODO: replace by visualforce component? -->
                    <div style="float:left;padding: 3px 10px 3px 50px;">
                        <div class="message infoM2" role="alert">
                            <table class="messageTable" border="0" cellspacing="0" cellpadding="0" style="padding:0px;margin:0px;">
                                <tbody>
                                    <tr valign="top">
                                        <td>
                                            <img class="msgIcon" title="info" src="/s.gif" alt="info" />
                                        </td>
                                        <td class="messageCell">
                                            <div class="messageText">
                                                <span>
                                                    <h4></h4>
                                                </span>
                                                <apex:outputText value="{!unorderedItems} non-ordered item(s) found. " />
                                                <apex:commandLink style="font-size:100%" action="{!createNewOrder}" rerender="page-container,menu-container,orderform-container"
                                                    status="loading-status">Click to order</apex:commandLink>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </apex:outputPanel>

                <!-- NOTIFICATION FOR O2C ORDER -->
                <apex:outputPanel rendered="{!theOrder.O2C_Order__c}">
                    <div style="float:left;padding: 3px 10px 3px 50px;">
                        <div class="message infoM2" role="alert">
                            <table class="messageTable" border="0" cellspacing="0" cellpadding="0" style="padding:0px;margin:0px;">
                                <tbody>
                                    <tr valign="top">
                                        <td>
                                            <img class="msgIcon" title="info" src="/s.gif" alt="info" />
                                        </td>
                                        <td class="messageCell">
                                            <div class="messageText">
                                                <span>
                                                    <h4></h4>
                                                </span>
                                                <apex:outputText value="This order will be processed with automated billing (O2C)." />
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </apex:outputPanel>

                <!-- ORDER SELECTOR -->
                <apex:outputPanel id="orderSelector" style="width:100%;">
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="orderTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>{!$Label.LABEL_Order_Number}</th>
                                <th>{!$Label.LABEL_Created_Date} </th>
                                <th>{!$Label.LABEL_Created_By} </th>
                                <th>{!$Label.LABEL_Proposition} </th>
                                <th>{!$Label.LABEL_Nr_of_Items} </th>
                                <th>
                                    <c:CustomHelptextBubble label="Status" helptext="{!$ObjectType.Order__c.fields.Status__c.inlineHelpText}" />
                                </th>
                                <th>{!$Label.LABEL_Sales_Assistance} </th>
                                <th>{!$Label.LABEL_BOP_Pre_Order_Id} </th>
                                <th>{!$Label.LABEL_BOP_Sales_Order_ID} </th>
                                <th>BOP {!$Label.LABEL_Export_Date_Time} </th>
                            </tr>
                        </thead>
                        <tbody>
                            <apex:repeat value="{!orderCaches}" var="of">
                                <tr class="{!IF(of.Id=orderId, 'ui-state-highlight', '')}">
                                    <td>{!of.Id}</td>
                                    <td>
                                        <a href="{!$Site.Prefix}/{!of.Id}">{!of.Name}</a>
                                    </td>
                                    <td>
                                        <apex:outputField value="{!of.CreatedDate}" />
                                    </td>
                                    <td>
                                        <apex:outputField value="{!of.CreatedById}" />
                                    </td>
                                    <td>
                                        <apex:outputField value="{!of.Propositions__c}" />
                                    </td>
                                    <td>
                                        <apex:outputField value="{!of.Number_of_items__c}" />
                                    </td>
                                    <td>
                                        <apex:outputText value="{!of.Status__c}" />
                                    </td>
                                    <td>
                                        <apex:outputField value="{!of.Ready_for_Inside_Sales_Datetime__c}" />
                                    </td>
                                    <td>
                                        <apex:outputField value="{!of.BOP_Order_Id__c}" />
                                    </td>
                                    <td>
                                        <apex:outputField value="{!of.Sales_Order_Id__c}" />
                                    </td>
                                    <td>
                                        <apex:outputField value="{!of.BOP_export_datetime__c}" />
                                    </td>
                                </tr>
                            </apex:repeat>
                        </tbody>
                    </table>
                    <script> addOrderDataTableMarkup();</script>
                </apex:outputPanel>
                <apex:actionFunction name="setSelectedOrderRow" action="{!changeOrderForm}" rerender="" status="loading-status">
                    <apex:param name="selLocId" value="" assignTo="{!orderformSelected}" />
                </apex:actionFunction>
            </apex:form>

        </apex:outputPanel>
        <!-- END HEADER BAR -->

        <!-- INTERACTION PANEL -->
        <apex:panelGrid columns="2" width="100%" columnClasses="firstcolClassName,colClassName" id="grid" style="border-top: 4px solid rgb(234, 234, 234);">
            <apex:outputPanel id="menu-container" style="width:200px;">

                <apex:pageBlock >
                    <!-- moved the title out of the pageblock title to make it fit on 1 line -->
                    <apex:outputPanel styleclass="pbTitle">
                        <h2 class="mainTitle"> {!$Label.LABEL_Order} {!theOrder.Name} </h2>
                    </apex:outputPanel>

                    <apex:form >
                        <apex:actionFunction name="gotoProgressPage" action="{!goToOrderProgress}" />

                        <apex:outputPanel id="orderButtons">
                            <apex:commandButton action="{!assignToInsideSales}" rendered="{!!theOrder.Ready_for_Inside_Sales__c}" disabled="{!(theOrder.Status__c != 'Ready for Inside Sales' && theOrder.Ordertype__r.Support_Needed__c == 'SAG')
                                            || (theOrder.Status__c != 'Clean' && theOrder.Ordertype__r.Support_Needed__c == 'No')}"
                                value="{!assignToLabel}" style="width:150px;margin-left:1px" onclick="return confirm('{!$Label.LABEL_Order_Confirm_Submit}');"
                            />
                            <apex:commandButton action="{!acceptOrder}" rendered="{!ShowSubmitButton && theOrder.Ready_for_Inside_Sales__c && theOrder.Status__c=='Assigned to Inside Sales' && theOrder.Ordertype__r.Support_needed__c == 'SAG'}"
                                value="Accept Order" disabled="{!theOrder.Status__c!='Assigned to Inside Sales'}" status="loading-status"
                                style="width:150px;margin-left:1px" onclick="return confirm('{!$Label.LABEL_OrderForm_Accept_Order_Confirm}')"
                            />
                            <apex:commandButton action="{!submitOrder}" rendered="{!ShowSubmitButton && theOrder.Ready_for_Inside_Sales__c}" value="{!$Label.Label_Submit_Order}"
                                disabled="{!theOrder.Status__c != 'Clean'}" status="loading-status" style="width:150px;margin-left:1px"
                                onclick="return checkMe()" />
                            <apex:commandlink action="{!backTo}" target="_blank" rendered="{! AND(NOT(isLightningCommunity), contractId != null) }" onclick="return confirm('{!$Label.LABEL_Confirm_Cancel}');">
                                <apex:commandButton value="{!$Label.LABEL_Back_To_Opportunity}" style="width:150px;margin-left:1px" />
                            </apex:commandLink>
                            <apex:commandButton action="{!addProduct}" rendered="{!ShowAddProductButton}" value="{!$Label.LABEL_Add_Product}" onclick="return confirm('{!$Label.LABEL_Confirm_Add_Product}');"
                                style="width:150px;margin-left:1px" />
                            <apex:commandButton action="{!setOrderToProcessedManually}" value="{!$Label.Orderform_Set_Status_To_Processed_Manually}"
                                rendered="{!ShowSubmitButton}" disabled="{!theOrder.Status__c == ORDER_STATUS_PROCESSED_MANUALLY}"
                                onclick="return confirm('{!$Label.Orderform_Confirm_Set_Status_To_Processed_Manually}');" status="loading-status"
                                style="width:150px;margin-left:1px" />
                            <apex:commandButton action="{!logCase}" value="Log a Case" style="width:150px;margin-left:1px" />
                        </apex:outputPanel>
                    </apex:form>
                </apex:pageBlock>
                <apex:insert name="component-menu" />

                <apex:actionstatus id="loading-status" onstart="dimOn()" onstop="dimOff()" stopText="">
                    <apex:facet name="start">
                        <apex:outputpanel >
                            <div id="ui-blocker">
                                <div id="ui-blocker-message">
                                    <span>{!$Label.LABEL_Loading}</span>
                                    <img src="/img/loading.gif" />
                                </div>
                            </div>
                        </apex:outputpanel>
                    </apex:facet>
                </apex:actionstatus>
            </apex:outputPanel>

            <apex:outputPanel id="page-container">
                <div id="page-messages">
                    <apex:pagemessages escape="false" rendered="{!!showCreateNewOrder}" />
                </div>
                <div>
                    <apex:insert name="page-body" />
                </div>
                <script type="text/javascript">
                    initializeSaveButton();

                </script>
            </apex:outputPanel>
        </apex:panelGrid>
    </body>


    </html>
</apex:page>