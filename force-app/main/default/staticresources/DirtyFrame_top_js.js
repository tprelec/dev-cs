/**
 *
 * Usage:
 * var dirtyFrame = new DirtyFrame( targetDomainName );
 * dirtyFrame.sendClosePageMessage('AttachmentManager');
 *
 * targetDomainName - parent window origin 
 * (e.g. for Community: $Site.domain 'https://nl-vodafone.force.com')
 *
 *  <script type="text/javascript">
 *      var PARENT_WINDOW_ORIGIN = "{! 'https://' + $Site.Domain }";
 *      var PAGE_NAME = "{! $CurrentPage.Name }";
 *  </script>
 *  <script type="text/javascript" src="{! $Resource.DirtyFrame }"></script>
 **/
var DirtyFrame = function(targetDomainName) {
    console.log('DirtyFrame; targetOrigin = ' + targetDomainName);
    var targetOrigin = targetDomainName;

    function sendClosePageMessage(pageName) {
        var message = {
            'type': 'frameEvent', 
            'pageName': pageName ? pageName : '', 
            'actionName': 'close'
        };
        console.log('DirtyFrame.sendClosePageMessage');
        sendMessageToParent(message);
    }

    function sendPageHeightMessage(pageName) {
        var height = getHeight() + 20 + 'px';
        var message = {
            'type': 'frameEvent', 
            'pageName': pageName ? pageName : '',
            'actionName': 'updateHeight', 
            'value': height
        };
        console.log('DirtyFrame.sendPageHeightMessage: ' + height);
        sendMessageToParent(message);
    }

    function sendMessageToParent(message) {
        parent.postMessage(message, targetOrigin);
    }

    function getHeight() {
        var container = document.getElementById('page_content');
        if (container) {
            height = container.scrollHeight;
        }
        else {
            height = document.body.scrollHeight;
        }
        return height;
    }

    return {
        sendClosePageMessage:  sendClosePageMessage,
        sendPageHeightMessage: sendPageHeightMessage
    }
};
