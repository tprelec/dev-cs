// CUSTOMER COMMITMENT ATTRIBUTE FUNCTIONS

function initializeGDSPCustomScreen() {
	removeRedundantComponents();
	initializeCustomerCommitment();
	initializeFootprint();
}

function removeRedundantComponents() {
	jQuery('[data-cs-screen-section-ref="Footprint-0"]').parent().remove();
}

// Initial function that builds the table and populates the fields
function initializeCustomerCommitment() {
	renderCustomerCommitmentTable();
	populateCustomerCommitmentsFromJSON();
	calculateTotalCustomerCommitment();
}

// Builds the initial table that's later populated and appended to the configuration screen
function renderCustomerCommitmentTable() {
	if (jQuery('#customerCommitment')[0] !== undefined) {
		jQuery('#customerCommitment')[0].remove();
	}
	var numberOfYears = parseInt(CS.getAttributeValue('Duration_0')) / 12;
	var divElement = document.createElement('div');
	divElement.setAttribute('id', 'customerCommitment');

	var customerCommitmentTable = document.createElement('table');
	customerCommitmentTable.setAttribute('class', 'list');
	customerCommitmentTable.setAttribute('id', 'customerCommitmentTable');

	var confirmButton = createButton('createCustomerCommitmentJSONEntry()', 'Confirm', null);

	var headerTr = document.createElement('tr');
	headerTr.setAttribute('class', 'headerRow');
	var headerTh1 = document.createElement('th');
	headerTh1.setAttribute('class', 'firstColumn');
	var headerTh2 = document.createElement('th');
	var label1 = createLabel('Year');
	var label2 = createLabel('Commitment of new SIMs');

	headerTh1.appendChild(label1);
	headerTh2.appendChild(label2);

	headerTr.appendChild(headerTh1);
	headerTr.appendChild(headerTh2);

	customerCommitmentTable.appendChild(headerTr);

	for (var i = 1; i <= numberOfYears; i++) {
		var tempTr = document.createElement('tr');

		var tempTd1 = document.createElement('td');
		var tempLabel = createLabel('Year ' + i);
		tempTd1.appendChild(tempLabel);

		var tempTd2 = document.createElement('td');
		var tempInput = createInputElement('customerCommitmentYear' + i, 'customerCommitmentByYear', false);
		tempTd2.appendChild(tempInput);

		tempTr.appendChild(tempTd1);
		tempTr.appendChild(tempTd2);

		customerCommitmentTable.appendChild(tempTr);
	}

	var trTotal = document.createElement('tr');
	var tdTotal1 = document.createElement('td');
	var labelTotal = createLabel('Total');
	tdTotal1.appendChild(labelTotal);

	var tdTotal2 = document.createElement('td');
	var inputTotal = createInputElement(null, 'customerCommitmentTotal', true);

	tdTotal2.appendChild(inputTotal);

	trTotal.appendChild(tdTotal1);
	trTotal.appendChild(tdTotal2);

	customerCommitmentTable.appendChild(trTotal);

	divElement.appendChild(customerCommitmentTable);
	divElement.appendChild(confirmButton);

	jQuery('[data-cs-binding="Customer_Commitment_0"]').html(null).append(divElement);
}

// Function that calculates the totals and populates the Total field
function calculateTotalCustomerCommitment() {
	var commitmentsByYear = document.getElementsByName("customerCommitmentByYear");
	var totalCommitmentField = document.getElementsByName("customerCommitmentTotal")[0];
	var totalCommitment = 0;

	commitmentsByYear.forEach(function (commitment) {
		if (commitment.value && !isNaN(parseInt(commitment.value))) totalCommitment += parseInt(commitment.value);
	});

	if (totalCommitmentField) totalCommitmentField.value = totalCommitment;
}

// Populates the table using a JSON that's fetched from the Customer Commitment attribute on the GDSP Product Configuration
function populateCustomerCommitmentsFromJSON() {
	var numberOfYears = parseInt(CS.getAttributeValue('Duration_0')) / 12;
	var commitmentsJSON = CS.getAttributeValue('Customer_Commitment_0');
	if (commitmentsJSON) commitmentsJSON = JSON.parse(commitmentsJSON);

	if (commitmentsJSON.length == numberOfYears) {
		commitmentsJSON.forEach(function (element, index) {
			var customerCommitment = document.getElementById("customerCommitmentYear" + element.year);
			if (customerCommitment) customerCommitment.value = element.numberOfSims;
		});
	}
}

// Creates a JSON that's pushed to the Customer Commitment attribute on the GDSP Product Configuration
function createCustomerCommitmentJSONEntry() {

	calculateTotalCustomerCommitment();

	var commitmentsByYear = document.getElementsByName("customerCommitmentByYear");
	var commitmentsJSON = [];

	var flagNaN = false;
	var flagIsZero = false;

	commitmentsByYear.forEach(function (element, index) {
		commitmentsJSON.push({ "year": index + 1, "numberOfSims": parseInt(element.value) });
		flagNaN = isNaN(parseInt(element.value));
		if (!flagNaN) flagIsZero = parseInt(element.value) == 0;
	});

	if (flagNaN) {
		CS.markConfigurationInvalid('Please only enter whole numbers as Commitment of new SIMs, and enter a value for each year.');
	} else if (flagIsZero) {
		CS.markConfigurationInvalid('Commitment of new SIMs cannot be 0.');
	} else {
		CS.setAttributeValue('Customer_Commitment_0', JSON.stringify(commitmentsJSON));
		CS.Rules.evaluateAllRules();
	}
}

// FOOTPRINT ATTRIBUTE FUNCTIONS

// Initial function that builds the table and populates the fields
function initializeFootprint() {
	renderFootprintTable();
	populateFootprintDataFromJSON();
}

// Builds the initial table that's later populated and appended to the configuration screen
function renderFootprintTable() {
	if (jQuery('#footprint')[0] !== undefined) {
		jQuery('#footprint')[0].remove();
	}
	var div = document.createElement('div');
	div.setAttribute('id', 'footprint');

	var table = document.createElement('table');
	table.setAttribute('class', 'list');
	table.setAttribute('id', 'footprintTable');

	var tbody = document.createElement('tbody');
	tbody.setAttribute('id', 'footprintTableBody')

	var tr1 = document.createElement('tr');
	tr1.setAttribute('class', 'headerRow');

	var th11 = document.createElement('th');
	th11.setAttribute('class', 'firstColumn');
	var label11 = createLabel('Country');

	th11.appendChild(label11);

	var th12 = document.createElement('th');
	var label2 = createLabel('Data Allowance Percentage');

	th12.appendChild(label2);

	tr1.appendChild(th11);
	tr1.appendChild(th12);

	var tr2 = document.createElement('tr');

	var td21 = document.createElement('td');
	var label2 = createLabel('Netherlands');

	td21.appendChild(label2);

	var td22 = document.createElement('td');
	var input2 = createInputElement('footprintNetherlands', 'footprintAllowanceStandard', false);

	td22.appendChild(input2);

	tr2.appendChild(td21);
	tr2.appendChild(td22);

	var tr3 = document.createElement('tr');

	var td31 = document.createElement('td');
	var label3 = createLabel('Europe');

	td31.appendChild(label3);

	var td32 = document.createElement('td');
	var input3 = createInputElement('footprintEurope', 'footprintAllowanceStandard', false);

	td32.appendChild(input3);

	tr3.appendChild(td31);
	tr3.appendChild(td32);

	tbody.appendChild(tr1);
	tbody.appendChild(tr2);
	tbody.appendChild(tr3);
	table.appendChild(tbody);

	var button1 = createButton('footprintConfirm()', 'Confirm', null);
	var button2 = createButton('footprintAddCountry()', 'Add Country', null);

	div.appendChild(table);
	div.appendChild(button1);
	div.appendChild(button2);

	jQuery('[data-cs-binding="Footprint_0"]').html(null).append(div);
}

// Populates the table using a JSON that's fetched from the Footprint attribute on the GDSP Product Configuration
function populateFootprintDataFromJSON() {
	var footprintJSON = CS.getAttributeValue('Footprint_0');
	if (footprintJSON) footprintJSON = JSON.parse(footprintJSON);

	if (footprintJSON.length > 0) {
		footprintJSON.forEach(function (element) {
			if (document.getElementById(element.id)) {
				document.getElementById(element.id).value = element.percentage;
			} else {
				var footprintTableBody = document.getElementById("footprintTableBody");
				if (footprintTableBody) footprintTableBody.appendChild(addFootprintTableRow(element));
			}
		});
	}
}

// Function for adding a custom country to the table
function footprintAddCountry() {
	var footprintTableBody = document.getElementById("footprintTableBody");
	footprintTableBody.appendChild(addFootprintTableRow(null));
}

// Function that calculates the total Data Allowance, performs validations and pushes the data to the Footprint attribute on the GDSP Product Configuration
function footprintConfirm() {
	var footprintJSON = [];
	var totalPercentage = 0;

	var footprintNL = parseInt(document.getElementById("footprintNetherlands").value);
	var footprintEU = parseInt(document.getElementById("footprintEurope").value);
	var footprintsCustom = document.getElementsByName("footprintAllowanceCustom");
	
	var isZero = false;

	footprintJSON.push(createFootprintJSONEntry("footprintNetherlands", "Netherlands", footprintNL));
	footprintJSON.push(createFootprintJSONEntry("footprintEurope", "Europe", footprintEU));

	footprintsCustom.forEach(function (element, index) {
		var country = document.getElementById(element.id + 'Name').value;
		footprintJSON.push(createFootprintJSONEntry(element.id, country, element.value));
		totalPercentage += parseInt(element.value);
		isZero = parseInt(element.value) == 0;
	});

	totalPercentage += footprintNL;
	totalPercentage += footprintEU;

	if (isNaN(totalPercentage)) {
		CS.markConfigurationInvalid('Please only enter whole numbers as Data Allowance Percentage, and enter a value for each country.')
	} else if (isZero) {
		CS.markConfigurationInvalid('Data Allowance Percentage cannot be 0.');
	} else if (totalPercentage != 100) {
		CS.markConfigurationInvalid('Total Data Allowance Percentage needs to be 100%!');
	} else {
		CS.setAttributeValue('Footprint_0', JSON.stringify(footprintJSON));
		CS.Rules.evaluateAllRules();
	}
}

// Placeholder function for creating a correctly-formatted entry for the JSON that's later pushed to the Footprint attribute on the GDSP Product Configuration
function createFootprintJSONEntry(id, country, percentage) {
	var tempArray = {
		"id": id,
		"country": country,
		"percentage": parseInt(percentage)
	};

	return tempArray;
}

// Function that adds a custom country when clicking "Add country"
function addFootprintTableRow(countryData) {
	var numberOfCustomCountries = document.getElementsByName("footprintAllowanceCustom").length;

	var tableRow = document.createElement('tr');
	var id;

	var td1 = document.createElement('td');
	var td2 = document.createElement('td');

	var input1 = createInputElement(null, 'footprintCountryName', false);
	var input2 = createInputElement(null, 'footprintAllowanceCustom', false);

	if (countryData) {
		id = countryData.id;
		input1.setAttribute('id', id + 'Name');
		input1.value = countryData.country;
		input2.setAttribute('id', id);
		input2.value = countryData.percentage;
	} else {
		id = 'footprintCustom' + (numberOfCustomCountries + 1)
		input1.setAttribute('id', id + 'Name');
		input2.setAttribute('id', id);
	}

	tableRow.appendChild(td1);
	tableRow.appendChild(td2);

	td1.appendChild(input1);
	td2.appendChild(input2);

	td2.appendChild(createButton('removeFootprintTableRow("' + id + '")', 'Remove', 'removeFootprint' + (numberOfCustomCountries + 1)));

	return tableRow;
}

// Function that removes a custom country 
function removeFootprintTableRow(id) {

	var entryValues = document.getElementsByName("footprintAllowanceCustom");

	var elementToDelete = document.getElementById(id);
	var flag = false;

	entryValues.forEach(function (element, index) {
		if (flag) {
			document.getElementById('footprintCustom' + (index + 1)).setAttribute('id', 'footprintCustom' + index);
			document.getElementById('footprintCustom' + (index + 1) + 'Name').setAttribute('id', 'footprintCustom' + index + 'Name');
			var button = document.getElementById('removeFootprint' + (index + 1));
			button.setAttribute('id', 'removeFootprint' + index);
			button.setAttribute('onclick', 'removeFootprintTableRow("footprintCustom' + index + '")');
		}
		if (elementToDelete == element) {
			flag = true;
		}
	});


	elementToDelete.parentElement.parentElement.remove();

}

// HTML ELEMENT BUILDERS

function createInputElement(id, name, disabled) {
	var input = document.createElement('input');
	input.setAttribute('type', 'text');
	input.setAttribute('step', 'any');
	input.setAttribute('class', 'slds-input');
	if (id) input.setAttribute('id', id);
	if (name) input.setAttribute('name', name);
	if (disabled) input.setAttribute('disabled', '');
	input.setAttribute('style', 'width: 50%');


	return input;
}

function createButton(onClickFunction, text, id) {
	var button = document.createElement('button');
	button.setAttribute('onclick', onClickFunction);
	button.textContent = text;
	if (id) button.setAttribute('id', id);
	return button;
}

function createLabel(content) {
	var label = document.createElement('label');
	label.textContent = content;
	return label;
}