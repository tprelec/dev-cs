import { LightningElement, wire } from 'lwc';
import OPPORTUNITY_OBJECT from '@salesforce/schema/Opportunity';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';

export default class OpportunityService extends LightningElement {

    @wire(getObjectInfo, { objectApiName: OPPORTUNITY_OBJECT })
	opportunity;
    /*oppInfo({ data, error }) {
        if (data) console.log('CloseDate Label => ', data.fields.CloseDate.label);
    }*/

}