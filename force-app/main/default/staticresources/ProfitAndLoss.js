var getAllFunctions = function(){
	
	// Temporary FIX to remove Totals from KPIs
	var numberOfColumns = jQuery(rows[6]).children().find('span').size();
	var numberOfRows = 7;
	var startingRow = 35;
	
	var totalGrossMarginRow = 23;
	var totalNetRevenueRow = 14;
	var ebitdaRow = 29;
	var netResultRow = 34;
	var directCapexRow = 30;
	
	for(var currentColumnNumber = 1; currentColumnNumber<numberOfColumns; currentColumnNumber++) {
	
	
		for(var i=startingRow; i<numberOfRows+startingRow; i++){
			jQuery(rows[i]).children().find('span')[numberOfColumns - currentColumnNumber].innerText = '';
		}
		
		var kpiSalesMarginNetRevenueRow = startingRow;
		var valueKpiSalesMarginNetRevenue = 100 * parseFloat(jQuery(rows[totalGrossMarginRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText.replace(/\./g, "")) / parseFloat(jQuery(rows[totalNetRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText.replace(/\./g, ""));
		jQuery(rows[kpiSalesMarginNetRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText = (valueKpiSalesMarginNetRevenue.toFixed(2) + ' %').replace('.', ',');
		
		var kpiEbitdaRevenueRow = startingRow + 1;
		var valueKpiEbitdaRevenue = 100 * parseFloat(jQuery(rows[ebitdaRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText.replace(/\./g, "")) / parseFloat(jQuery(rows[totalNetRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText.replace(/\./g, ""));
		jQuery(rows[kpiEbitdaRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText = (valueKpiEbitdaRevenue.toFixed(2) + ' %').replace('.', ',');
		
		var freeCashFlowRevenueRow = startingRow + 2;
		var valueFreeCashFlowRevenue = 100 * parseFloat(jQuery(rows[netResultRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText.replace(/\./g, "")) / parseFloat(jQuery(rows[totalNetRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText.replace(/\./g, ""));
		jQuery(rows[freeCashFlowRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText = (valueFreeCashFlowRevenue.toFixed(2) + ' %').replace('.', ',');
		
		var netIncrementalBilledRevenueRow = startingRow + 3;
		jQuery(rows[netIncrementalBilledRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText = '0,00 %';
		
		var npvRevenueRow = startingRow + 4;
		jQuery(rows[npvRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText = '0,00 %';
		
		var netPaybackPeriodRevenueRow = startingRow + 5;
		jQuery(rows[netPaybackPeriodRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText = '0,00 %';
		
		var directCapexRevenueRow = startingRow + 6;
		var valuedirectCapexRevenue = 100 * parseFloat(jQuery(rows[directCapexRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText.replace(/\./g, "")) / parseFloat(jQuery(rows[totalNetRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText.replace(/\./g, ""));
		jQuery(rows[directCapexRevenueRow]).children().find('span')[numberOfColumns - currentColumnNumber].innerText = (valuedirectCapexRevenue.toFixed(2) + ' %').replace('.', ',');
	}
	
    var allfunctions=[];
    for (var i in window) {
        if((typeof window[i]).toString()=="function"){
            allfunctions.push(window[i].name);
        }
    }
    return allfunctions;
}
window.getAllFunctions = getAllFunctions;