/**
 * Plugin for common functions being used by multiple products in Solution Console
 * The main purpose is to define the following functions:
 *  - Functions that are executing general logic (e.g. calculations), which is not attribute specific,
 *  - Functions that are attribute specific, but common for multiple products.
 *    This is valid for attributes that have same behaviour accros different products.
 *
 * NOTE: Must be invoked after SC_Constants and before all other product plugins!
 */

console.log('Loaded Common Functions plugin');

let basketDetails;

// Add On Attributes
const ADDON_REF_ATTRIBUTE = 'AddOnReference';
const ADDON_GEN_ATTRIBUTE = 'AddOnGenerator';

// Map between Solution Main Component attributes and Basket fields
const SOLUTION_ATTRIBUTE_FIELD_MAPPING = {
    [SC_COMMON_ATTRIBUTES.SALES_CHANNEL] : 'opportunityDirectIndirect'
}

// Register logic to be executed when Solution is set to active
if (CS.SM.registerPlugin) {
    window.document.addEventListener('SolutionSetActive', initDefaults);
}

async function fetchBasketInformation() {
    return new Promise(async function(resolve, reject) {
        setInterval( async function() {
            const b = await CS.SM.getActiveBasket();
            if (b) {
                resolve(b);
            }
        },500);
    });
}

async function initDefaults() {
    const solution = await CS.SM.getActiveSolution();

    switch (solution.name) {

        case SC_SOLUTIONS.ONEMOBILE_SOLUTION:
            return initOneMobileSolutionDefaults(solution);
    }
}

// Set the current system date on main Solution component
async function setCurrentSystemDate(solution) {
    const mainSolutionPC = getMainConfiguration(solution);

    const dateAttribute = [{
        name: SC_COMMON_ATTRIBUTES.TODAY,
        value: getCurrentISODateFormat()
    }];

    return updateConfigurationAttribute(solution, mainSolutionPC.guid, dateAttribute);
}

// Get configuration from main Solution component
function getMainConfiguration(solution) {
    return solution.getConfigurations()[0];
}

// Prevent save of Solution with error
// Invoked from all Solution plugins!
async function beforeSolutionSave(solution, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
    // Check if Solution has more than 1 configuration, and prevent save solutions with only main configuration
    const solutionValidForSave = await checkForSolutionConfigurations(solution);
    if (!solutionValidForSave) {
        return false;
    }

    return !solution.error;
}

async function validateConfiguration(configuration, valid, messageText) {
    configuration.updateStatus({ status : valid, message : messageText });
}

async function checkForSolutionConfigurations(solution) {
    const configurations = solution.getAllConfigurations();

    return Object.values(configurations).length > 1;
}

// Propagate requested attribute value to all configurations belonging to the requested Component
async function propagateAttributeToComponent(componentName, attributeName, attributeValue) {
    const solution = await CS.SM.getActiveSolution();
    const component = solution.getComponentByName(componentName);
    if (!component) {
        return;
    }

    return Promise.all(Object.values(component.getConfigurations()).map(configuration =>
        setAttribute(component, configuration, attributeName, attributeValue)
    ));
}

// Set requested attribute
async function setAttribute(component, configuration, attributeName, attributeValue) {
    const attributeData = [{
        name: attributeName,
        value: attributeValue
    }];

    return updateConfigurationAttribute(component, configuration.guid, attributeData);
}

/**
 * @description Invokes package updateConfigurationAttribute function and initiates changes,
 *              only if current and new values are different
 * @param component Component that configuration belongs to
 * @param configurationGuid GUID of configuration which attributes should be updated
 * @param attributeList List of attributes that should be updated
 */
async function updateConfigurationAttribute(component, configurationGuid, attributeList) {
    const configuration = component.getConfiguration(configurationGuid);

    const newAttributeList = [];
    attributeList.forEach(attribute => {
        const configAttribute = getAttribute(configuration, attribute.name);
        // TODO: Double-check validating attribute type (!= instead of !==)
        if (configAttribute && configAttribute.value !== attribute.value) {
            newAttributeList.push(attribute);
        }
    });

    if (newAttributeList && newAttributeList.length) {
        return component.updateConfigurationAttribute(configuration.guid, newAttributeList);
    }
}

async function beforeAddOnDataInitialized(aomData, component, configuration, priceItemAttribute) {
    return sortAddOns(aomData);
}

// Sorts AddOns by Comm. Prod. Add On Assoc. field Sequence ASC
function sortAddOns(aomData) {
    if (Object.values(aomData.addOnData.data) && Object.values(aomData.addOnData.data).length) {
        Object.values(aomData.addOnData.data)[0].sort((a, b) => {
            return a.cspmb__Sequence__c - b.cspmb__Sequence__c;
        });
    }

    return aomData;
}

/**
 * General functions
 */
// Get current date in ISO format
function getCurrentISODateFormat() {
    const todayDate = new Date().toISOString().slice(0, 10);

    return todayDate;
}

/**
 * Functions with backend interaction
 */
async function getBasketInfo() {
    if (basketDetails === undefined || basketDetails === null) {
        const basket = await CS.SM.getActiveBasket();
        const inputMapObject = {
            'method': SC_REMOTE_CLASS_METHOD.PRODUCT_BASKET_DETAILS,
            'productBasketId': basket.basketId
        };

        basketDetails = await basket.performRemoteAction(SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS, inputMapObject);
    }

    return basketDetails;
}

// Set Sales Channel attribute on Main Solution Component,
// originally fetched from the Basket and Opportunity fields
async function updateFromBasket(solution, attributeNames) {
    const basketData = await getBasketInfo();
    const mainSolutionPC = getMainConfiguration(solution);
    let attributeData = [];

    attributeNames.forEach(attName => {
        const attValue = mainSolutionPC.getAttribute(attName).value;
        const basketFieldValue = basketData[SOLUTION_ATTRIBUTE_FIELD_MAPPING[attName]];
        if (attValue === "" && basketFieldValue != null) {
            attributeData.push({
                name: attName,
                value: basketFieldValue
            });
        }
    });

    return updateConfigurationAttribute(solution, mainSolutionPC.guid, attributeData);
}



/**
 * Attribute specific functions
 */

function getAttribute(configuration, attributeName) {
    return configuration.attributes[ attributeName.toLowerCase() ];
}

function getAttributeValue(configuration, attributeName) {
    return configuration.attributes[ attributeName.toLowerCase() ].value;
}

function getAttributeVisibility(configuration, attributeName) {
    return configuration.attributes[ attributeName.toLowerCase() ].showInUi;
}

function getAttributeRequired(configuration, attributeName) {
    return configuration.attributes[ attributeName.toLowerCase() ].required;
}