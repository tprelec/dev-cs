console.log('Loaded Constant file');

const SC_SOLUTIONS = Object.freeze({
    "ONEMOBILE_SOLUTION": "Mobile"
});

const SC_SCHEMA_NAME = Object.freeze({
    "ONEMOBILE_SOLUTION": "Mobile Solution"
});

const SC_COMPONENTS = Object.freeze({
    "ONEMOBILE_COMPONENT": "Mobile",
    "ONEMOBILE_PLAN_COMPONENT": "Mobile Plan",
    "ONEMOBILE_PLAN_OE_COMPONENT": "Mobile Plan OE"
});

const SC_COMMON_ATTRIBUTES = Object.freeze({
    "TODAY": "Today",
    "CONTRACT_TERM": "ContractTerm",
    "MOBILE_PLAN_NAME": "MobilePlanName",
    "TIER": "tier",
    "PRODUCT_VARIANT": "productVariant",
    "PRODUCT_VARIANT_NAME": "productVariantName",
    "SALES_CHANNEL": "SalesChannel",
    "QUANTITY": "Quantity",
    "GUID": "guid",
    "CONNECTION_TYPE": "ConnectionType",
    "CONFIG_ID": "ConfigId",
    "BASE_MRC": "BaseMRC"
});

const SC_ADDON_GROUPS = Object.freeze({
    "DATA_AND_VOICE_UPGRADES": "Data and voice upgrades",
    "ROAMING_UPGRADES": "Roaming upgrades"
});

const SC_RELATED_PRODUCT_TYPES = Object.freeze({
    "ADDON": "Add On Component",
    "RELATED_PRODUCT": "Related Component"
});

const SC_WARNING_MESSAGES = Object.freeze({
    "CANNOT_ADD_ROAMING": "Can't add \'World permanent upgrade\' Add-On without adding Add-On from the group \'Data and voice upgrades\'",
    "CANNOT_DELETE_LAST_VOICE_AND_DATA": "At least 1 Add-On from the group \'Data and voice upgrades\' needs to be added in order to have Add-On \'World permanent upgrade\'",
    "QUANTITY_NOT_MATCH": "The Quantity on the configuration does not match the total Quantity of it's Order Enrichment Quantities",
    "DUPLICATE_CONNECTION_TYPE": "Duplicate Connection Type selected!"
});

const SC_REMOTE_CLASS_NAME = Object.freeze({
    "REMOTE_APEX_CLASS": "RemoteActionProvider"
});

const SC_REMOTE_CLASS_METHOD = Object.freeze({
    "PRODUCT_BASKET_DETAILS": "productBasketDetails"
});