if (!CS || !CS.SM) {
    throw Error('Solution Console Api not loaded!');
}

// Register OneMobile Solution plugin
if (CS.SM.registerPlugin) {
    window.document.addEventListener('SolutionConsoleReady', registerOneMobile);
}

async function registerOneMobile() {
    const oneMobilePlugin = await CS.SM.registerPlugin(SC_SCHEMA_NAME.ONEMOBILE_SOLUTION);
    updateOneMobilePlugin(oneMobilePlugin);

    console.log('OneMobile plugin registered!');
    return;
}

function updateOneMobilePlugin(Plugin) {

    Plugin.beforeSave = beforeSolutionSave;

    Plugin.afterSave = afterOneMobileSolutionSave;

    Plugin.afterAttributeUpdated = afterOneMobileAttributeUpdated;

    Plugin.beforeAddonDataInitialized = beforeAddOnDataInitialized;

    Plugin.afterRelatedProductAdd = afterOneMobileRelatedProductAdd;

    Plugin.beforeRelatedProductDelete = beforeOneMobileRelatedProductDelete;
}

/**
 * @description Perform logic required after Solution is saved
 */
async function afterOneMobileSolutionSave(solutionResult, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
    const solution = solutionResult.solution;
    if (!solution) {
        return;
    }

    /*return Promise.all([ 
        afterOneMobilePlanSave(solution, configurationGuids)
    ]);*/
}

async function afterOneMobileAttributeUpdated(component, configuration, attribute, oldValueMap) {
    switch (component.name) {
        case SC_COMPONENTS.ONEMOBILE_COMPONENT:
            return afterOneMobileSolutionAttributeUpdated(attribute);

        case SC_COMPONENTS.ONEMOBILE_PLAN_COMPONENT:
            return afterOneMobilePlanAttributeUpdated(component, configuration, attribute, oldValueMap);

        case SC_COMPONENTS.ONEMOBILE_PLAN_OE_COMPONENT:
            return afterOneMobilePlanOEAttributeUpdated(component, configuration, attribute, oldValueMap);
    }

    return true;
}

async function afterOneMobileSolutionAttributeUpdated(attribute) {

    if (attribute.name === SC_COMMON_ATTRIBUTES.TODAY) {
        return Promise.all([
            propagateAttributeToComponent(SC_COMPONENTS.ONEMOBILE_PLAN_COMPONENT, attribute.name, attribute.value)
        ]);
    }

}

async function afterOneMobileRelatedProductAdd(component, configuration, relatedProduct) {
    switch (component.name) {

        case SC_COMPONENTS.ONEMOBILE_PLAN_COMPONENT:
            return afterOneMobilePlanRelatedProductAdd(component, configuration, relatedProduct);
    }

    return true;
}

async function beforeOneMobileRelatedProductDelete(component, configuration, relatedProduct) {
    switch (component.name) {

        case SC_COMPONENTS.ONEMOBILE_PLAN_COMPONENT:
            return beforeOneMobilePlanRelatedProductDelete(component, configuration, relatedProduct);
    }

    return true;
}

/**
 *
 * Support functions
 *
 */

// Initialization logic
async function initOneMobileSolutionDefaults(solution) {
    console.log('Init OneMobile Solution defaults');
    if (!solution) {
        solution = await CS.SM.getActiveSolution();
    }

    const oneMobileSolutionComponent = solution.getComponentByName(SC_SOLUTIONS.ONEMOBILE_SOLUTION);

    if (!oneMobileSolutionComponent) {
        return;
    }

    return Promise.all([
        setCurrentSystemDate(solution),
        updateFromBasket(solution, [SC_COMMON_ATTRIBUTES.SALES_CHANNEL])
    ]);
}

