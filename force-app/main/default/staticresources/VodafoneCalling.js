// The reason is that "Basic Deployment Service" related product list (attribute) was renamed to "Activation Multi Tenant or Private Tenant" at the final stage of development
// NOTE: there is naming in this file refering to "Basic Deployment Service"
// (Tried to use the label on Attribute, but it didn't work)
// Also, existing PD called "Basic deployment service VC" was not renamed

var vcProductTypes = {
    "SBCSessions" : "SBC_sessions_0",
}

var vcCategories = {
    "SBCSessions" : "SBC sessions"
}

var vcCategoryAtt = 'Category_0';
var vcQuantityAtt = 'Quantity_0';
var vcProductTypesArray = Object.values(vcProductTypes);

var commonAttributes;

function getCommonAttributes() {
    let common = {};
    common.Scenario = CS.getAttributeValue("Scenario_0");
    common.DealType = CS.getAttributeValue("Deal_Type_0");
    return common;
}

window.getVcProductsPerCategory = function getVcProductsPerCategory() {
    var results = {};
    vcProductTypesArray.forEach(function(element) {
        for (var index in CS.Service.config[element].relatedProducts) {
            var item = CS.Service.config[element].relatedProducts[index];
            if (item.reference) {
                let category = CS.getAttributeValue(item.reference+':'+vcCategoryAtt);
                let quantity = CS.getAttributeValue(item.reference+':'+vcQuantityAtt);

                if (results[category]) {
                    results[category].quantity += quantity;
                }
                else {
                    results[category] = {};
			    	results[category].quantity = quantity;
                }
            }
        }
    });

    return results;
}

// NOTE: Basic Deployment Service was later renamed to Activation Multi Tenant or Private Tenant (Activation_Multi_Tenant_or_Private_Tenant)
// Remove specific product from list of Basic Deployment Service related products
// Discriminator is matched with value in attribute Product_group_0
function removeBasicDeploymentServiceRelatedProductAndClearFlag(relatedProductDescriminator, flagReference) {
    for (var i = 0; i < CS.Service.config["Activation_Multi_Tenant_or_Private_Tenant_0"].relatedProducts.length; i++) {
        var groupValue = CS.getAttributeValue('Activation_Multi_Tenant_or_Private_Tenant_'+ i +':Product_group_0');
        
        if ((groupValue.includes(relatedProductDescriminator))) {
            CS.Service.removeRelatedProduct('Activation_Multi_Tenant_or_Private_Tenant_'+ i);
            i--;
        }
    }
 
    if (CS.getAttributeValue(flagReference)!='') {
        CS.setAttributeValue(flagReference, '');
    }
 }

 function removeBasicSupport() {
    for (var i = 0; i < CS.Service.config["Vodafone_support_0"].relatedProducts.length; i++) {
        var groupValue = CS.getAttributeValue('Vodafone_support_'+ i +':Product_group_0');
        
        if (groupValue !== undefined && (groupValue.includes('Basic support'))) {
            CS.Service.removeRelatedProduct('Vodafone_support_'+ i);
            break;
        }
    }
 
    if (CS.getAttributeValue('Basic_Support_set_0')!='') {
        CS.setAttributeValue('Basic_Support_set_0', '');
    }
 }

function addBasicSupport() {
    if (allSBCSessionProductsSelected()) {
        checkPGA('Vodafone_support_0','Basic_Support_set_0','Basic_Support_lookup_0');
    }
}

function addBasicDeploymentService() {
    checkPGA('Activation_Multi_Tenant_or_Private_Tenant_0','Basic_Deployment_set_0','Basic_Deployment_lookup_0');
}

function removeBasicDeploymentService() {
    removeBasicDeploymentServiceRelatedProductAndClearFlag('Vodafone Calling in Office 365 infrastructure basic','Basic_Deployment_set_0');
}

function addPrivateInfrastructureProduct() {
    checkPGA('Activation_Multi_Tenant_or_Private_Tenant_0','Private_infrastructure_set_0','Private_infrastructure_lookup_0');
}

function removePrivateInfrastructureProduct() {
    removeBasicDeploymentServiceRelatedProductAndClearFlag('Private infrastructure upgrade product','Private_infrastructure_set_0');
}

function addPrivateInfrastructureDeployment() {
    checkPGA('Activation_Multi_Tenant_or_Private_Tenant_0','Private_infrastructure_deployment_set_0','Private_infrastructure_deployment_lookup_0');
}

function removePrivateInfrastructureDeployment() {
    removeBasicDeploymentServiceRelatedProductAndClearFlag('Private infrastructure upgrade deployment','Private_infrastructure_deployment_set_0');
}

function addSupportForHighCapacityUpgrade() {
    checkPGA('Activation_Multi_Tenant_or_Private_Tenant_0','High_capacity_set_0','High_capacity_lookup_0');
}

function removeHighCapacityUpgrade() {
    removeBasicDeploymentServiceRelatedProductAndClearFlag(';Redundant','High_capacity_set_0');
}

// Method for adding products to the related list IF the same related list already contains some other product with specific value of the Product_group_0 attribute
//
// relatedList - list of the related products we are extending with new product (attribute without trailig zero i.e. Anywhere_365_ NO Anywhere_365_0)
// autoProductFlagAttribute - flag used for checkig whether the product is added
// lookupAttribute - lookup from Vodafone Calling used for selecting specific automatically added product
// prerequisiteProductGroup - value of the Product_group_0 attribute on the prerequisite product that needs to be matched
function conditionalAddRelatedProduct(relatedList, autoProductFlagAttribute, lookupAttribute, prerequisiteProductGroup) {
    var relatedListWithZero = relatedList+'0';
    if (CS.Service.config[relatedListWithZero].relatedProducts.length == 0) {
        return;
    }

    if (CS.getAttributeValue(autoProductFlagAttribute) !== '') {
        return;
    }

    var hasPrerequisiteProduct = false;

    for (var i = 0; i < CS.Service.config[relatedListWithZero].relatedProducts.length; i++) {
        var group = CS.getAttributeValue(relatedList + i +':Product_group_0');
        
        if (group !== undefined && group === prerequisiteProductGroup) {
            hasPrerequisiteProduct = true; 
            break;
        }
    }

    if (hasPrerequisiteProduct) {
        checkPGA(relatedListWithZero,autoProductFlagAttribute,lookupAttribute);
        return true;
    }

    return false;
}

// Method for removing products from related list IF the same related list doesn't contain some other prerequisite product with specific value of the Product_group_0 attribute
//
// relatedList - list of the related products we are changing (attribute without trailig zero i.e. Anywhere_365_ NO Anywhere_365_0)
// autoProductFlagAttribute - flag used for checkig whether the product is added
// prerequisiteProductGroup - value of the Product_group_0 attribute on the prerequisite product that needs to be matched
// targetProductGroup - value of the Product_group_0 attribute on the product that we are removing
// removeWhenDealType - Deal Type(s) for which product needs to be removed
function conditionalRemoveRelatedProduct(relatedList, autoProductFlagAttribute, prerequisiteProductGroup, targetProductGroup, removeWhenDealType) {
    var relatedListWithZero = relatedList+'0';

    var productRequired = false;

    var dealType = CS.getAttributeValue('Deal_Type_0');

    for (var i = 0; i < CS.Service.config[relatedListWithZero].relatedProducts.length; i++) {
        var group = CS.getAttributeValue(relatedList + i +':Product_group_0');
        
        if ((group !== undefined && group.includes(prerequisiteProductGroup))) {
            productRequired = true; 
            break;
        }
    }

    // remove for specific deal type(s) or if prerequisite product is not added
    if (!productRequired || removeWhenDealType.includes(dealType)) { //(checkRetention && dealType == 'Retention')
        for (var i = 0; i < CS.Service.config[relatedListWithZero].relatedProducts.length; i++) {
            var groupValue = CS.getAttributeValue(relatedList+ i +':Product_group_0');
            
            if (groupValue !== undefined && groupValue.includes(targetProductGroup)) {
                CS.Service.removeRelatedProduct(relatedList + i);
                break;
            }
        }
     
        if (CS.getAttributeValue(autoProductFlagAttribute)!='') {
            CS.setAttributeValue(autoProductFlagAttribute, '');
        }

        return true;
    }

    return false;
}

// if there are specific licence products added to Anywhere 365 (with Group__c containing "Basic deployment voor Anywhere 365;""), then add Basic deployment voor Anywhere 365
function addAnywhere365Basic() {
    conditionalAddRelatedProduct('Anywhere_365_', 'Basic_Anywhere_set_0', 'Basic_Anywhere_lookup_0', 'Anywhere 365 Enterprise/Corporate licence;');
}

function removeAnywhere365Basic() {
    conditionalRemoveRelatedProduct('Anywhere_365_', 'Basic_Anywhere_set_0', 'Anywhere 365 Enterprise/Corporate licence;', ';Basic deployment voor Anywhere 365', ["Retention"]);
}

function addStandardCRMConnector() {
    // add if it already wasn't added manually
    if (CS.getAttributeValue('StandardCRMConnector_set_0') == '') {
        conditionalAddRelatedProduct('Anywhere_365_', 'StandardCRMConnector_set_auto_0', 'StandardCRMConnector_lookup_0', 'CRM connector (read);');
    }
}

function removeStandardCRMConnector() {
    // try to remove it only if it was added automatically
    if (CS.getAttributeValue('StandardCRMConnector_set_auto_0') != '') {
        conditionalRemoveRelatedProduct('Anywhere_365_', 'StandardCRMConnector_set_auto_0', 'CRM connector (read);', ';CRM connector (read)', ["Retention"]);
    }
}

function addAnywhere365Hosting() {
    conditionalAddRelatedProduct('Anywhere_365_', 'Anywhere_Hosting_set_0', 'Anywhere_Hosting_lookup_0', 'Anywhere 365 Enterprise/Corporate licence;');
}

function removeAnywhere365Hosting() {
    conditionalRemoveRelatedProduct('Anywhere_365_', 'Anywhere_Hosting_set_0', 'Anywhere 365 Enterprise/Corporate licence;', ';Anywhere 365 Hosting', []);
}

function addWorkBuddyProduct() {
    conditionalAddRelatedProduct('Adoption_and_training_', 'Workplace_Buddy_set_0', 'Workplace_Buddy_lookup_0', 'Workplace Buddy;');
}

function removeWorkBuddyProduct() {
    conditionalRemoveRelatedProduct('Adoption_and_training_', 'Workplace_Buddy_set_0', 'Workplace Buddy;', ';Workplace Buddy', ["Retention"]);
}

function addExtendedAnywhere365() {
    // add if it already wasn't added manually
    if (CS.getAttributeValue('Extended_Anywhere_set_0') == '') {
        conditionalAddRelatedProduct('Anywhere_365_', 'Extended_Anywhere_set_auto_0', 'Extended_Anywhere_lookup_0', 'Anywhere 365 Web Agent;');
    }
}

function removeExtendedAnywhere365() {
    // try to remove it only if it was added automatically
    if (CS.getAttributeValue('Extended_Anywhere_set_auto_0') != '') {
        conditionalRemoveRelatedProduct('Anywhere_365_', 'Extended_Anywhere_set_auto_0', 'Anywhere 365 Web Agent;', ';Extended deployment voor Anywhere 365', ["Retention"]);
    }
}

function addReceptionQueueImplementation() {
    conditionalAddRelatedProduct('Anywhere_365_', 'Reception_Queue_set_0', 'Reception_Queue_lookup_0', 'Reception Queue;');
}

function removeReceptionQueueImplementation() {
    conditionalRemoveRelatedProduct('Anywhere_365_', 'Reception_Queue_set_0', 'Reception Queue;', ';Reception Queue', ["Retention", "Migration"]);
}

/*
    There are 2 Anywhere 365 products which are added automatically in some cases, but can also be added manually
    Product_group_0 contains "Auto;CRM connector (read)" ; Set flag: StandardCRMConnector_set_0
    Product_group_0 contains "Auto;Extended deployment voor Anywhere 365" ; Set flag: Extended_Anywhere_set_0
    Additional flags are used to determine whether product is added automatically - StandardCRMConnector_set_auto_0 and Extended_Anywhere_set_auto_0
*/
function checkAnywhere365Products() {
    if (CS.Service.config["Anywhere_365_0"].relatedProducts.length == 0) {
        return;
    }

    var countConnector = 0;
    var countExtended = 0;

    // if product is added manully, set respective flag
    for (var i = 0; i < CS.Service.config["Anywhere_365_0"].relatedProducts.length; i++) {
        var groupValue = CS.getAttributeValue('Anywhere_365_'+ i +':Product_group_0');
        
        if ((groupValue.includes(";CRM connector (read)"))) {
            if (CS.getAttributeValue('StandardCRMConnector_set_0') == '') {
                CS.setAttributeValue('StandardCRMConnector_set_0', 'set');
            }
            countConnector++;
        }

        if ((groupValue.includes(";Extended deployment voor Anywhere 365"))) {
            if (CS.getAttributeValue('Extended_Anywhere_set_0') == '') {
                CS.setAttributeValue('Extended_Anywhere_set_0', 'set');
            }
            countExtended++;
        }
    }

    if (countConnector > 1) {
        CS.markConfigurationInvalid('There can be only 1 product Anywhere 365 Standard CRM connector (read) in the configuration. Please remove additional instance(s)');
    }

    if (countConnector == 0) {
        if (CS.getAttributeValue('StandardCRMConnector_set_0') != '' && CS.getAttributeValue('StandardCRMConnector_set_auto_0') != '') {
            CS.setAttributeValue('StandardCRMConnector_set_0', '');
            CS.setAttributeValue('StandardCRMConnector_set_auto_0', '');
        }
        else if (CS.getAttributeValue('StandardCRMConnector_set_auto_0') == '') {
            CS.setAttributeValue('StandardCRMConnector_set_0', '');
        }
    }

    if (countExtended > 1) {
        CS.markConfigurationInvalid('There can be only 1 product Implementatie voor Anywhere 365 Configuratie Web Agent in the configuration. Please remove additional instance(s)');
    }

    if (countExtended == 0) {
        if (CS.getAttributeValue('Extended_Anywhere_set_0') != '' && CS.getAttributeValue('Extended_Anywhere_set_auto_0') != '') {
            CS.setAttributeValue('Extended_Anywhere_set_0', '');
            CS.setAttributeValue('Extended_Anywhere_set_auto_0', '');
        }
        else if (CS.getAttributeValue('Extended_Anywhere_set_auto_0') == '') {
            CS.setAttributeValue('Extended_Anywhere_set_0', '');
        }
    }
}

function addSipTrunk() {
    if (allSBCSessionProductsSelected()) {
        checkPGA('SIP_Trunk_0','SIP_Trunk_set_0','SIP_Trunk_lookup_0');
    }
}

function removeSipTrunk() {
    removeRelatedProductAndClearFlag('SIP_Trunk_0','SIP_Trunk_set_0');
}

function removeSipTrunkActions() {
    jQuery("button[data-cs-ref='SIP_Trunk_0']").remove();
    removeRelatedProductButtons(['SIP_Trunk_'],['All']);
}

function invalidateSBCSessions() {
    if (CS.Service.config['SBC_sessions_0'].relatedProducts.length > 0) { 
        _.each(CS.Service.config['SBC_sessions_0'].relatedProducts,function(p) {
                 CS.Service.removeRelatedProduct('SBC_sessions_0');
           }); 
        } 

    var duration = CS.getAttributeValue('Contract_Duration_0');
    CS.setAttributeValue('Contract_Duration_Shadow_0', duration);
}

/*
    Checks if some specific product is added to the basic deployment service list
*/
function checkBasicDeploymentProduct(productGroupReference) {
    for (var i = 0; i < CS.Service.config["Activation_Multi_Tenant_or_Private_Tenant_0"].relatedProducts.length; i++) {
        var groupValue = CS.getAttributeValue('Activation_Multi_Tenant_or_Private_Tenant_'+ i +':Product_group_0');
        
        if ((groupValue.includes(productGroupReference))) {
            return true;
        }
    }
}

function isIPVPNRequired() {
    // if there is a product where Group__c contains 'Deployment Private infrastructure upgrade;' then IPVPN is required in the basket
    // such products can be in Basic or Extendent deployment service
    for (var i = 0; i < CS.Service.config["Activation_Multi_Tenant_or_Private_Tenant_0"].relatedProducts.length; i++) {
        var groupValue = CS.getAttributeValue('Activation_Multi_Tenant_or_Private_Tenant_'+ i +':Product_group_0');
        
        if ((groupValue.includes('Deployment Private infrastructure upgrade;'))) {
            return true;
        }
    }
}

function SBCSessionRules(productsPerCategory, commonAttributes) {
    if (productsPerCategory[vcCategories.SBCSessions] == undefined) {
        CS.markConfigurationInvalid("SBC sessions must be added to the configuration");
        CS.setAttributeValue('SIP_Channels_0', 0);
        return;
    }

    if (productsPerCategory[vcCategories.SBCSessions] && CS.Service.config['SBC_sessions_0'].relatedProducts.length != 1) {
        CS.markConfigurationInvalid("Exactly 1 SBC session product must be added");
    }

    if (productsPerCategory[vcCategories.SBCSessions].quantity > 25 && commonAttributes.Scenario == "Shared" && CS.getAttributeValue('Deal_Type_0') == 'Acquisition') {
        CS.markConfigurationInvalid("Since number of SBC sessions exceeds 250, scenario should be Private");
        return;
    }

    // finally, set value for total number of SBC sessions
    // each "session product" has 10 underlying SBC sessions
    CS.setAttributeValue('Session_count_0', productsPerCategory[vcCategories.SBCSessions].quantity*10);
    
    // in case of Private scenario, value is set to 0 because SIP from OneFixed is taken into account --> this is now reverted since OneFixed is not mandatory anymore
    // if (commonAttributes.Scenario == "Shared")
        CS.setAttributeValue('SIP_Channels_0', productsPerCategory[vcCategories.SBCSessions].quantity*10);
    // else
    //     CS.setAttributeValue('SIP_Channels_0', 0);
}

function checkQuantityVC(relatedList, referentGroupValue, minimum, maximum) {
    var sum = 0;
    var name = '';
    // check quantities for particular products
    for (var i = 0; i < CS.Service.config[relatedList+"0"].relatedProducts.length; i++) {
        var groupValue = CS.getAttributeValue(relatedList+ i +':Product_group_0');
        var quantity = CS.getAttributeValue(relatedList+ i +':Quantity_0');

        if (groupValue !== undefined && (groupValue.includes(referentGroupValue))) {
            sum += quantity;
            name = CS.getAttributeValue(relatedList+ i +':PartName_0');
        }
    }

    if (name !== '') {
        // when only minimal value is required then for method arguments has to be --> minimum > maximum
        if (minimum > maximum && sum < minimum) {
            CS.markConfigurationInvalid("Quantity of the product(s) " + name + " needs to be greater than or equal to " + minimum);
        }
        // when exact value is required then for method arguments has to be --> minimum == maximum
        if (minimum == maximum && sum != maximum) {
            CS.markConfigurationInvalid("Quantity of the product(s) " + name + " needs to be exactly " + minimum);
        }
        // when value must be in specific range (for now, minimum is 1)
        if (minimum < maximum && (sum < minimum || sum > maximum)) {
            CS.markConfigurationInvalid("Maximum quantity of the product(s) <b>" + name + "</b> in the configuration is " + maximum + ". Please remove additional instance(s) or quantities");
        }
    }
}

function checkProductDependencies() {

    var activatieTeamsProduct = false;
    var unallowedProduct = false;
    var unallowedProductName;

    for (var i = 0; i < CS.Service.config["Activation_Multi_Tenant_or_Private_Tenant_0"].relatedProducts.length; i++) {
       var groupValue = CS.getAttributeValue('Activation_Multi_Tenant_or_Private_Tenant_' + i + ':Product_group_0');
       if (groupValue !== undefined) {
            if (groupValue.includes(';Activatie Teams')) {
                activatieTeamsProduct = true;
            }
            if (groupValue.includes('Unallowed;')) {
                unallowedProduct = true;
                unallowedProductName = CS.getAttributeValue('Activation_Multi_Tenant_or_Private_Tenant_' + i + ':PartName_0');
            }
       }
    }

    var implementationProduct = false;

    for (var i = 0; i < CS.Service.config["Implementation_services_0"].relatedProducts.length; i++) {
        var groupValue = CS.getAttributeValue('Implementation_services_' + i + ':Product_group_0');
        if (groupValue !== undefined)
        {
            if (groupValue.includes(';Implementatie Service voor Teams')) {
                implementationProduct = true;
            }
        }
     }

    if (unallowedProduct && (activatieTeamsProduct || implementationProduct)) {
        CS.markConfigurationInvalid("Unallowed product combination: Please remove product " + unallowedProductName +  " from Activation Multi Tenant or Private Tenant");
    }

    checkQuantityVC("Vodafone_support_", ";Converged service management", 5, -1);
    checkQuantityVC("Implementation_services_", ";Implementatie Service voor Teams", 1, 1);
    //checkQuantityVC("Anywhere_365_", ";Contact Center as a Service", 20, -1);
    checkQuantityVC("Anywhere_365_", "Reception Queue;", 1, 5);
    checkQuantityVC("Anywhere_365_", "CRM connector (read);", 1, 999);

    checkQuantityVC("Anywhere_365_", "Anywhere 365 Web Agent;Extension window", 1, 999);
    checkQuantityVC("Anywhere_365_", "Anywhere 365 Web Agent;CRM", 1, 999);
    checkQuantityVC("Anywhere_365_", "Anywhere 365 Web Agent;Receptie", 1, 999);

    checkAnywhere365Products();
   
    // set IPVPN required
    CS.setAttributeValue('RequireIPVPN_0', isIPVPNRequired() ? 'true' : 'false'); //AFTER_REMOVE_RELATED_PRODUCT_ACTION
}

window.renameProductConfigTitle = function renameProductConfigTitle() {
    jQuery('[class^="slds-page-header__title"]').each(function() {
        if (jQuery(this) && jQuery(this).attr('title')) {
          if (jQuery(this).attr('title').indexOf("Vodafone Calling") == 0) {
            jQuery(this).text('Vodafone Calling in Office 365');
            return;
          }
        }
    });
}

function selectSBCSessionProduct() {
    for (var i = 0; i < CS.Service.config["SBC_sessions_0"].relatedProducts.length; i++) {
        var attRef = 'SBC_sessions_'+ i +':Product_0'
        var attWrapper = CS.Service.config[attRef];
        var product = CS.getAttributeValue(attRef);
        if (product == '') {
            CS.EAPI.getAddOnAssociationsListForAttr(attWrapper).then(function(result) {
                try{
                    if (result != undefined && !(Object.entries(result).length === 0 && result.constructor === Object)) {
                        CS.setAttributeValue(attRef, result[0].id);
                    }
                } catch (err) {
                    
                }
            });
        }
    }
}

function allSBCSessionProductsSelected() {
    if (CS.Service.config["SBC_sessions_0"].relatedProducts.length == 0) {
        return false;
    }

    for (var i = 0; i < CS.Service.config["SBC_sessions_0"].relatedProducts.length; i++) {
        var prod = CS.getAttributeValue('SBC_sessions_'+ i +':Product_0');
        
        if ((prod == undefined || prod == '')) {
            return false;
        }

        var quantity = CS.getAttributeValue('SBC_sessions_'+ i +':Quantity_0');
        if ((quantity == undefined || quantity == '')) {
            return false;
        }
    }

    return true;
}

window.executeVodafoneCallingRules = function executeVodafoneCallingRules() {
    let productsPerCategory = getVcProductsPerCategory();
    commonAttributes = getCommonAttributes();
    SBCSessionRules(productsPerCategory, commonAttributes);
}

var autoProducts = 
{
    "Activation_Multi_Tenant_or_Private_Tenant_":["Vodafone Calling in Office 365 infrastructure basic",
                                 "Private infrastructure upgrade product",
                                 "Private infrastructure upgrade deployment",
                                 ";Redundant"],
    "Vodafone_support_":["Basic support"],
    "Adoption_and_training_":[";Workplace Buddy"],
    "Anywhere_365_":[";Basic deployment voor Anywhere 365",";CRM connector (read)", ";Anywhere 365 Hosting", 
                     ";Extended deployment voor Anywhere 365", ";Reception Queue"]
}

function shouldRemoveDeleteButton(relatedList, productName) {
    var relevantGroups = autoProducts[relatedList];
    if (relevantGroups !== undefined && relevantGroups !== null) {
        for (var i=0; i<CS.Service.config[relatedList+'0'].relatedProducts.length; i++) {
            if (CS.getAttributeValue(relatedList + i +':PartName_0') == productName) {
                var group = CS.getAttributeValue(relatedList + i +':Product_group_0');
                if (group !== undefined && group !== null) {
                    var relevantGroups = autoProducts[relatedList];
                    for (var g=0; g<relevantGroups.length; g++) {
                        // following 2 products can also be added manully so we cannot remove DEL button
                        if (relevantGroups[g] == ";CRM connector (read)" || relevantGroups[g] == ";Extended deployment voor Anywhere 365") {
                            continue;
                        }
                        if (group.includes(relevantGroups[g])) {
                            return true;
                        }
                    }
                    return false;
                }
            }
        }
    }
    return false;
}

function makeQuantityReadOnly(relatedList) {
    if (autoProducts[relatedList] !== undefined) {
        for (var i=0; i<CS.Service.config[relatedList+'0'].relatedProducts.length; i++) {
            var group = CS.getAttributeValue(relatedList + i +':Product_group_0');
            if (group !== undefined) {
                for (var g=0; g<autoProducts[relatedList].length; g++) {
                    if (group.includes(autoProducts[relatedList][g])) {
                        CS.makeAttributeReadOnly(relatedList+i+':Quantity_0');
                    }
                }
            }
        }
    }
}

// checks quantity for automatically added products (Quantity = 1)
function ensureCorrectQuantity() {
    var keys = ["Activation_Multi_Tenant_or_Private_Tenant_","Vodafone_support_","Anywhere_365_"];
    keys.forEach(function(relatedList,index, array) {
        for (var i=0; i<CS.Service.config[relatedList+'0'].relatedProducts.length; i++) {
            var group = CS.getAttributeValue(relatedList + i +':Product_group_0');
            if (group !== undefined) {
                for (var g=0; g<autoProducts[relatedList].length; g++) {
                    // 4 products with specific groups from condition below have different quantity level
                    if ((autoProducts[relatedList][g] !== ";Basic deployment voor Anywhere 365" && 
                        !autoProducts[relatedList][g].includes(';Extended deployment voor Anywhere 365') &&
                        !autoProducts[relatedList][g].includes(';CRM connector (read)') &&
                        !autoProducts[relatedList][g].includes(';Reception Queue'))  && group.includes(autoProducts[relatedList][g])) {
                        var quantity = CS.getAttributeValue(relatedList+i+':Quantity_0');
                        if (quantity != 1) {
                            var name = CS.getAttributeValue(relatedList + i +':PartName_0');
                            CS.markConfigurationInvalid('Quantity of the product <b>' + name + '</b> must be 1!');
                        }
                    }
                }
            }
        }
    });
}

function handleAnywhere365Availability() {
    var scenario = CS.getAttributeValue('Scenario_0');
    if (scenario != undefined) {
        if (scenario == 'Shared') {
            if (CS.Service.config["Anywhere_365_0"].relatedProducts.length > 0) { 
                _.each(CS.Service.config["Anywhere_365_0"].relatedProducts,function(p) {
                         CS.Service.removeRelatedProduct("Anywhere_365_0");
                   }); 
            } 
            if (CS.getAttributeValue('Basic_Anywhere_set_0') != '') {
                CS.setAttributeValue('Basic_Anywhere_set_0', '');
            }
            if (CS.getAttributeValue('StandardCRMConnector_set_auto_0') != '') {
                CS.setAttributeValue('StandardCRMConnector_set_auto_0', '');
            }
            if (CS.getAttributeValue('Anywhere_Hosting_set_0') != '') {
                CS.setAttributeValue('Anywhere_Hosting_set_0', '');
            }
            if (CS.getAttributeValue('Extended_Anywhere_set_auto_0') != '') {
                CS.setAttributeValue('Extended_Anywhere_set_auto_0', '');
            }
            if (CS.getAttributeValue('Reception_Queue_set_0') != '') {
                CS.setAttributeValue('Reception_Queue_set_0', '');
            }

            setTimeout(function() {jQuery("button[data-cs-ref='Anywhere_365_0']").hide();},0);
        }
        else if (scenario == 'Private') {
            setTimeout(function() {jQuery("button[data-cs-ref='Anywhere_365_0']").show();},0);
        }
    }
}

function checkConfigurationProductsSelected(relatedLists) {
    // assumption is that each related list checked here has lookup attribute named Product which has to be populated
    relatedLists.forEach(function(item) {
        var relatedListName = item.endsWith("_") ? item : (item + '_');
        var relatedListNameWithZero = relatedListName + '0';
        for (var i = 0; i < CS.Service.config[relatedListNameWithZero].relatedProducts.length; i++) {
            var productValue = CS.getAttributeValue(relatedListNameWithZero + ':Product_0');
            if (productValue === '') {
                CS.markConfigurationInvalid('Make sure that all configurations from list <b>' + item.replace('_','') + '</b> have Product selected');
                return;
            }
        }
    });
}

function testEvents() {
    subscribeEvent(CS.EventHandler.Event.RULES_ITERATION_START, function() { console.log('--------------> RULES_ITERATION_START'); }, false);
}

function vfWarningMessageAnywhere365(){
    var cnfg = CS.Service.config;
    var allAnywhere365length = cnfg["Anywhere_365_0"].relatedProducts.length;


    for (var j=0; j < allAnywhere365length; j++) {
        var strBuilderName =  "Anywhere_365_"+ j +":PartName_0";
        var strBuilderQty =   "Anywhere_365_" + j + ":Quantity_0";
        var nameOfProd = cnfg[strBuilderName].attr.cscfga__Value__c;
        var qty = parseInt(cnfg[strBuilderQty].attr.cscfga__Value__c);
        
        if (nameOfProd == 'Anywhere 365 Reception Queue') {
            if (qty > 3) {
                CS.displayInfo('Anywhere 365 Reception Queue: Maximum 3 per customer allowed. Approval from supplier Workstreampeople necessary for exceptions');
            }
        }

        if(nameOfProd == 'Anywhere 365 Contact Center as a Service') {
            if (qty < 20) {
                CS.displayInfo('Anywhere 365 Contact Center as a Service: Minimum 20 per customer. Approval from supplier Workstreampeople necessary for exceptions');
            }
        }
        
    }
}


//_____________________________________________________________________________________________________________________________________________________
// Zakelijke Toestelbetaling
// testing
//_____________________________________________________________________________________________________________________________________________________

function autoSelectIndirectMobileDevice() {
    // there is only one mobile device for Zakelijke Toestelbetaling Indirect Sales, so query params are defined here
    var queryName = 'B2BHardwareQuery';
    var attribute = 'MobileDevice_0';
    var flagVal = CS.getAttributeValue('QueryFlag_0');
    if(flagVal == '') {
        CS.setAttributeValue('QueryFlag_0', 'set');
        var prodId = CS.Service.config[""].config.cscfga__Product_Definition__c;
        var newDyn = {};
        doLookupQueryAndInvalidateAddons(queryName, newDyn, prodId, CS.Service.config[attribute]).then(
            function(pgaResult){
                if(pgaResult.res != null){
                    _.each(pgaResult.res, function(result){
                        CS.setAttributeValue(attribute, result.Id);
                    });
                }
            }
        );
    }
}

