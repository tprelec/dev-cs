///attributes for automatic addition
var priceItemAttributes =

	{
		"oneNetIntegration": {
			"lookupAttributeParentONE": "ONEEnablerLookup_0",
			"lookupAttributeParentONX": "ONXEnablerLookup_0",
			"lookupAttributeChild": "Price_Item_0",
			"relProductAttribute": "OneNet_Integration_0",
			"setFlagONE": "ONEEnabler_set_0",
			"setFlagONX": "ONXEnabler_set_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"errorFlagONE": "ONEEnabler_error_0",
			"errorFlagONX": "ONXEnabler_error_0",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"additionType": "AdditionType_0"
		},
		"redundancy": {
			"lookupAttributeParent": "Redundancy_lookup_0",
			"lookupAttributeChild": "Price_Item_0",
			"relProductAttribute": "Miscellaneous_0",
			"setFlag": "Redundancy_set_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"errorFlag": "Redundancy_error_0",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"additionType": "AdditionType_0"
		},
		"bundles": {
			"lookupAttributeParent": "Bundle_0",
			"lookupAttributeChild": "Bundle_lookup_0",
			"relProductAttribute": "Bundle_Products_0",
			"setFlag": "Bundle_set_0",
			"pGALookupAttribute": "BundlePGAlookup_0",
			"pgaSetFlag": "BundlePGAset_0",
			"errorMessage": "Please select a bundle product."

		},
		"fixedFlatFee": {
			"lookupAttributeParent": "FixedFlatFeeLookup_0",
			"lookupAttributeChild": "FlatFee_0",
			"relProductAttribute": "FlatFee_product_0",
			"setFlag": "FlatFee_set_0",
			"errorFlag": "FlatFee_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Fixed Flat Fee product available for selection.",
			"errorMessageNone": "There is no Fixed Flat Fee product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"

		},
		"fixedFlatFee4": {
			"lookupAttributeParent": "FFF4Lookup_0",
			"lookupAttributeChild": "FlatFee_0",
			"relProductAttribute": "FlatFee_product_0",
			"setFlag": "FlatFee4_set_0",
			"errorFlag": "FlatFee_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Fixed Flat Fee product available for selection.",
			"errorMessageNone": "There is no Fixed Flat Fee product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"

		},
		"fixedFlatFee8": {
			"lookupAttributeParent": "FFF8Lookup_0",
			"lookupAttributeChild": "FlatFee_0",
			"relProductAttribute": "FlatFee_product_0",
			"setFlag": "FlatFee8_set_0",
			"errorFlag": "FlatFee_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Fixed Flat Fee product available for selection.",
			"errorMessageNone": "There is no Fixed Flat Fee product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"

		},
		"mobileusage": {
			"lookupAttributeParent": "Mobile_Usage_Lookup_0",
			"lookupAttributeChild": "Price_Item_0",
			"relProductAttribute": "Mobile_Usage_Product_0",
			"setFlag": "Mobile_Usage_Set_0",
			"errorFlag": "Mobile_Usage_Error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Mobile Usage product available for selection.",
			"errorMessageNone": "There is no Mobile Usage product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"

		},
		"row": {
			"lookupAttributeParent": "RoW_Lookup_0",
			"lookupAttributeChild": "RoW_bundle_0",
			"relProductAttribute": "RoW_bundle_product_0",
			"setFlag": "RoW_bundle_set_0",
			"errorFlag": "ROW_bundle_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more RoW product available for selection.",
			"errorMessageNone": "There is no RoW product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"

		},
		"payu": {
			"lookupAttributeParent": "PAYU_Lookup_0",
			"lookupAttributeChild": "PAYU_0",
			"relProductAttribute": "PayAsYouUse_product_0",
			"setFlag": "PAYU_set_0",
			"errorFlag": "PAYU_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more PAYU product available for selection.",
			"errorMessageNone": "There is no PAYU product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"

		},
		"vcBasicDeploymentService": {
			"lookupAttributeParent": "Basic_Deployment_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Activation_Multi_Tenant_or_Private_Tenant_0",
			"setFlag": "Basic_Deployment_set_0",
			"errorFlag": "Basic_Deployment_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Basic deployment service products available for selection.",
			"errorMessageNone": "There is no Basic deployment service product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcPrivateInfrastructureProduct": {
			"lookupAttributeParent": "Private_infrastructure_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Activation_Multi_Tenant_or_Private_Tenant_0",
			"setFlag": "Private_infrastructure_set_0",
			"errorFlag": "Private_infrastructure_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Private infrastructure upgrade products available for selection.",
			"errorMessageNone": "There is no Private infrastructure upgrade product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcPrivateInfrastructureDeployment": {
			"lookupAttributeParent": "Private_infrastructure_deployment_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Activation_Multi_Tenant_or_Private_Tenant_0",
			"setFlag": "Private_infrastructure_deployment_set_0",
			"errorFlag": "Private_infrastructure_deployment_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Private infrastructure upgrade deployment products available for selection.",
			"errorMessageNone": "There is no Private infrastructure upgrade deployment product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcSipTrunk": {
			"lookupAttributeParent": "SIP_Trunk_lookup_0",
			"lookupAttributeChild": "SIP_Trunk_0",
			"relProductAttribute": "SIP_Trunk_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "SIP_Trunk_set_0",
			"errorFlag": "SIP_Trunk_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no SIP Trunk price item record with the exact match for the bandwidth requested.",
			"errorMessageNone": "There are no SIP Trunk products available. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"vcBasicSupport": {
			"lookupAttributeParent": "Basic_Support_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Vodafone_support_0",
			"setFlag": "Basic_Support_set_0",
			"errorFlag": "Basic_Support_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Basic Support products available for selection.",
			"errorMessageNone": "There is no Basic Support product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcHighCapacity": {
			"lookupAttributeParent": "High_capacity_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Activation_Multi_Tenant_or_Private_Tenant_0",
			"setFlag": "High_capacity_set_0",
			"errorFlag": "High_capacity_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more High capacity products available for selection.",
			"errorMessageNone": "There is no High capacity product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcBasicAnywhere": {
			"lookupAttributeParent": "Basic_Anywhere_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Anywhere_365_0",
			"setFlag": "Basic_Anywhere_set_0",
			"errorFlag": "Basic_Anywhere_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Basic deployment Anywhere 365 products available for selection.",
			"errorMessageNone": "There is no Basic deployment Anywhere 365 product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcStandardCRMConnector": {
			"lookupAttributeParent": "StandardCRMConnector_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Anywhere_365_0",
			"setFlag": "StandardCRMConnector_set_0",
			"errorFlag": "StandardCRMConnector_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more StandardCRMConnector products available for selection.",
			"errorMessageNone": "There is no StandardCRMConnector product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcAnywhereHosting": {
			"lookupAttributeParent": "Anywhere_Hosting_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Anywhere_365_0",
			"setFlag": "Anywhere_Hosting_set_0",
			"errorFlag": "Basic_Anywhere_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Anywhere 365 Hosting products available for selection.",
			"errorMessageNone": "There is no Anywhere 365 Hosting product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcWorkplaceBuddy": {
			"lookupAttributeParent": "Workplace_Buddy_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Adoption_and_training_0",
			"setFlag": "Workplace_Buddy_set_0",
			"errorFlag": "Basic_Anywhere_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Workplace Buddy products available for selection.",
			"errorMessageNone": "There is no Workplace Buddy product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcExtendedAnywhere": {
			"lookupAttributeParent": "Extended_Anywhere_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Anywhere_365_0",
			"setFlag": "Extended_Anywhere_set_0",
			"errorFlag": "Basic_Anywhere_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Implementatie voor Anywhere 365 Reception Queue products available for selection.",
			"errorMessageNone": "There is no Implementatie voor Anywhere 365 Reception Queue product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"vcReceptionQueue": {
			"lookupAttributeParent": "Reception_Queue_lookup_0",
			"lookupAttributeChild": "Product_0",
			"relProductAttribute": "Anywhere_365_0",
			"setFlag": "Reception_Queue_set_0",
			"errorFlag": "Basic_Anywhere_error_0",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "There are more Extended deployment Anywhere 365 products available for selection.",
			"errorMessageNone": "There is no Extended deployment Anywhere 365 product available for selection.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"oneFixedCUBE": {
			"lookupAttributeParent": "OneFixedCUBE_lookup_0",
			"lookupAttributeChild": "CPE_Product_0",
			"relProductAttribute": "CPE_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "OneFixedCUBE_set_0",
			"errorFlag": "OneFixedCUBE_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no CUBE price item record with the exact match for the bandwidth requested.",
			"errorMessageNone": "There are no CUBE products available.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"oneFixedSBCSIP": {
			"lookupAttributeParent": "OneFixed_SBC_SIP_Sessions_0",
			"lookupAttributeChild": "CPE_Product_0",
			"relProductAttribute": "CPE_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "OneFixed_SBC_SIP_Sessions_set_0",
			"errorFlag": "NOT USED",
			"customLookupAttributes": '[]',
			"errorMessageMoreThanOne": "NOT USED",
			"errorMessageNone": "NOT USED",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0"
		},
		"oneFixedIAD": {
			"lookupAttributeParent": "OneFixedIAD_lookup_0",
			"lookupAttributeChild": "CPE_Product_0",
			"relProductAttribute": "CPE_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "OneFixedIAD_set_0",
			"errorFlag": "OneFixedIAD_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no IAD price item record with the exact match for the bandwidth requested.",
			"errorMessageNone": "There are no IAD products available.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"sipTrunk": {
			"lookupAttributeParent": "OneFixed_SIP_Trunk_0",
			"lookupAttributeChild": "SIP_Trunk_0",
			"relProductAttribute": "SIP_Package_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "OneFixed_SIP_Trunk_set_0",
			"errorFlag": "OneFixed_SIP_Trunk_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no SIP Trunk price item record with the exact match for the bandwidth requested.",
			"errorMessageNone": "There are no SIP Trunk products available. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"sipTrunkOneNet": {
			"lookupAttributeParent": "OneNet_SIP_Trunk_0",
			"lookupAttributeChild": "SIP_Trunk_0",
			"relProductAttribute": "SIP_Package_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "OneNet_SIP_Trunk_set_0",
			"errorFlag": "OneNet_SIP_Trunk_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no SIP Trunk price item record with the exact match for the bandwidth requested.",
			"errorMessageNone": "There are no SIP Trunk products available. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"evcOneFixed": {
			"lookupAttributeParent": "EVCPVC_OneFixed_0",
			"lookupAttributeChild": "EVC_PVC_0",
			"relProductAttribute": "EVC_PVC_dep_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "EVCPVC_OneFixed_set_0",
			"errorFlag": "EVCPVC_OneFixed_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no One Fixed EVC price item record with the exact match for the bandwidth requested. Please add an EVC.",
			"errorMessageNone": "There are no available One Fixed EVC products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"qualityOfService": "QoS_OneFixed_0"
		},
		"evcOneNet": {
			"lookupAttributeParent": "EVCPVC_OneNet_0",
			"lookupAttributeChild": "EVC_PVC_0",
			"relProductAttribute": "EVC_PVC_dep_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "EVCPVC_OneNet_set_0",
			"errorFlag": "EVCPVC_OneNet_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no One Net EVC price item record with the exact match for the bandwidth requested. Please add an EVC.",
			"errorMessageNone": "There are no available One Net EVC products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"transportEVC": {
			"lookupAttributeParent": "TransportEVCLookup_0",
			"lookupAttributeChild": "EVC_PVC_0",
			"relProductAttribute": "EVC_PVC_dep_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "TransportEVC_set_0",
			"errorFlag": "TransportEVC_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no Transport EVC price item record with the exact match for the bandwidth requested. Please add an EVC.",
			"errorMessageNone": "There are no available Transport EVC products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "TrasnportEVC_PGA_set_0",
			"qualityOfService": "QoS_Transport_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"evc2": {
			"lookupAttributeParent": "EVCPVC_2_0",
			"lookupAttributeChild": "EVC_PVC_0",
			"relProductAttribute": "EVC_PVC_dep_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "EVC2_set_0",
			"errorFlag": "EVC2_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no EVC VoIP price item record with the exact match for the bandwidth requested. Please add an EVC.",
			"errorMessageNone": "There are no available EVC Voip products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"qualityOfService": "QoS_VoiP_0"
		},
		"evc2Vlan": {
			"lookupAttributeParent": "EVCPVC_2_VLAN_0",
			"lookupAttributeChild": "EVC_PVC_0",
			"relProductAttribute": "EVC_PVC_dep_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "EVC2_VLAN_set_0",
			"errorFlag": "EVC2_VLAN_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType"]',
			"errorMessageMoreThanOne": "There is no EVC VoIP price item record with the exact match for the bandwidth requested. Please add an EVC.",
			"errorMessageNone": "There are no available EVC Voip VLAN products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"qualityOfService": "QoS_VoiP_0"
		},
		"evc1": {
			"lookupAttributeParent": "EVCPVC_1_0",
			"lookupAttributeChild": "EVC_PVC_0",
			"relProductAttribute": "EVC_PVC_dep_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "EVC1_set_0",
			"errorFlag": "EVC1_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType", "Min. Upstream Bandwidth", "Min. Upstream Bandwidth 2", "Min.Downstream Bandwidth", "Min.Downstream Bandwidth 2", "Total Bandwidth Up", "Total Bandwidth Down"]',
			"errorMessageMoreThanOne": "There is no EVC Data price item record with the exact match for the bandwidth requested. Please add an EVC.",
			"errorMessageNone": "There are no available EVC Data products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"qualityOfService": "Quality_of_Service_0"
		},
		"evc1Vlan": {
			"lookupAttributeParent": "EVCPVC_1_VLAN_0",
			"lookupAttributeChild": "EVC_PVC_0",
			"relProductAttribute": "EVC_PVC_dep_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "EVC1_VLAN_set_0",
			"errorFlag": "EVC1_VLAN_error_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType", "Min. Upstream Bandwidth", "Min. Upstream Bandwidth 2", "Min.Downstream Bandwidth", "Min.Downstream Bandwidth 2", "Total Bandwidth Up", "Total Bandwidth Down"]',
			"errorMessageMoreThanOne": "There is no EVC Data VLAN price item record with the exact match for the bandwidth requested. Please add an EVC.",
			"errorMessageNone": "There are no available EVC Data VLAN products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"qualityOfService": "Quality_of_Service_0"
		},
		"managedInternetEVC": {
			"lookupAttributeParent": "ManagedInternetEVCPVC_0",
			"lookupAttributeChild": "EVC_PVC_0",
			"relProductAttribute": "EVC_PVC_dep_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "ManagedInternetEVCPVCSet_0",
			"errorFlag": "ManagedInternetEVCPVCError_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType", "Min. Upstream Bandwidth", "Min. Upstream Bandwidth 2", "Min.Downstream Bandwidth", "Min.Downstream Bandwidth 2", "Total Bandwidth Up", "Total Bandwidth Down"]',
			"errorMessageMoreThanOne": "There is no Managed Internet EVC price item record with the exact match for the bandwidth requested. Please add an EVC.",
			"errorMessageNone": "There are no available Managed Internet EVC products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"managedInternetEVCVLAN": {
			"lookupAttributeParent": "ManagedInternetEVCPVC_VLAN_0",
			"lookupAttributeChild": "EVC_PVC_0",
			"relProductAttribute": "EVC_PVC_dep_0",
			"additionType": "AdditionType_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"setFlag": "ManagedInternetEVCPVCVLANSet_0",
			"errorFlag": "ManagedInternetEVCPVCVLANError_0",
			"customLookupAttributes": '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType", "Min. Upstream Bandwidth", "Min. Upstream Bandwidth 2", "Min.Downstream Bandwidth", "Min.Downstream Bandwidth 2", "Total Bandwidth Up", "Total Bandwidth Down"]',
			"errorMessageMoreThanOne": "There is no Managed Internet EVC VLAN price item record with the exact match for the bandwidth requested. Please add an EVC.",
			"errorMessageNone": "There are no available Managed Internet EVC VLAN products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"access": {
			"lookupAttributeParent": "Acccess_0",
			"lookupAttributeChild": "Access_0",
			"relProductAttribute": "Access_Infrastructure_0",
			"setFlag": "Access_set_0",
			"errorFlag": "Access_error_0",
			"customLookupAttributes": '["Vendor","Access Type Calc","Region","Infra SLA","Contract Duration","Available bandwidth up","Available bandwidth down","Result Check", "Proposition", "Overbooking Voip","Contract Duration"]',
			"errorMessageMoreThanOne": "There are more available Access Infrastructure products.Please select one.",
			"errorMessageNone": "There are no available Access products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"LineTypeInstallationFeeLookup": "AccessLineTypeInstallationFee_0",
			"LineTypeInstallationFeeSet": "AccessLineTypeInstallationFeeSet_0",
			"DealTypeInstallationFeeLookup": "AccessDealTypeInstallationFee_0",
			"DealTypeInstallationFeeSet": "AccessDealTypeInstallationFeeSet_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0"
		},
		"wirelessInstallation": {
			"lookupAttributeParent": "Wireless_Installation_lookup_0",
			"lookupAttributeChild": "Wireless_0",
			"category": "Category_0",
			"relProductAttribute": "Price_Item_Wireless_0",
			"setFlag": "Wireless_Installation_fee_set_0",
			"errorFlag": "Wireless_Installation_fee_error_0",
			"errorMessageMoreThanOne": "There are more available Wireless Installation products.Please select one.",
			"errorMessageNone": "There are no available Wireless Installation products.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0"
		},
		"wirelessPricePlan": {
			"lookupAttributeParent": "Wireless_Price_Plan_lookup_0",
			"lookupAttributeChild": "Wireless_0",
			"category": "Category_0",
			"relProductAttribute": "Price_Item_Wireless_0",
			"setFlag": "Wireless_Price_Plan_set_0",
			"errorFlag": "Wireless_Price_Plan_error_0",
			"errorMessageMoreThanOne": "There are more available Wireless Price Plan products.Please select one.",
			"errorMessageNone": "There are no available Wireless Price Plan products.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0"
		},
		"wirelessPricePlanInternet": {
			"lookupAttributeParent": "Wireless_Price_Plan_Internet_lookup_0",
			"lookupAttributeChild": "Wireless_0",
			"category": "Category_0",
			"relProductAttribute": "Price_Item_Wireless_0",
			"setFlag": "Wireless_Price_Plan_Internet_set_0",
			"errorFlag": "Wireless_Price_Plan_Internet_error_0",
			"errorMessageMoreThanOne": "There are more available Wireless Price Plan Internet products.Please select one.",
			"errorMessageNone": "There are no available Wireless Price Plan Internet products.",
			"pGALookupAttribute": "PGALookupInternet_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0"
		},
		"wirelessOnly": {
			"lookupAttributeParent": "Price_Item_Fee_0",
			"lookupAttributeChild": "Wireless_0",
			"setFlag": "Price_Item_set_0",
			"relProductAttribute": "Price_Item_Wireless_Standalone_0",
			"pgaSetFlag": "PGAset_0",
			"pGALookupAttribute": "PGALookup_0",
			"errorFlag": "Wireless_Only_error_0",
			"errorMessageNone": "There are no available Wireless Only products.",
			"errorMessageMoreThanOne": "There are more available Wireless Only products.Please select one.",
			"hirarchyLevel": "HirarchyLevel_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0"
		},
		"cpe": {
			"lookupAttributeParent": "CPE_lookup_0",
			"lookupAttributeParentIAD": "CPE_IAD_lookup_0",
			"lookupAttributeChild": "CPE_Product_0",
			"hardwareToeslagh": "Hardware_Toeslag_Id_0",
			"relProductAttribute": "CPE_0",
			"setFlag": "CPE_set_0",
			"errorFlag": "CPE_error_0",
			"customLookupAttributes": '["Available bandwidth down","Access Type","Available bandwidth up","Num LAN","Num WAN","Num WANLAN","Wireless backup","Quality of Service"]',
			"errorMessageMoreThanOne": "There are more available CPE products.Please select one.",
			"errorMessageNone": "There are no available CPE products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0",
			"qualityOfService": "Quality_of_Service_0"

		},
		"cpeSla": {
			"lookupAttributeParent": "CPE_SLA_lookup_0",
			"lookupAttributeChild": "CPE_SLA_lookup_0",
			"relProductAttribute": "CPE_SLA_items_0",
			"setFlag": "CPE_SLA_set_0",
			"errorFlag": "CPE_SLA_error_0",
			"errorMessageMoreThanOne": "There are more available CPE SLA products.Please select one.",
			"errorMessageNone": "There are no available CPE SLA products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0"
		},
		"hardwareToeslagh": {
			"lookupAttributeParent": "Hardware_Toeslag_Id_0",
			"lookupAttributeChild": "Hardware_Toeslag_0",
			"hardwareToeslagh": "Hardware_Toeslag_Id_0",
			"relProductAttribute": "Price_Item_Hardware_Toeslag_0",
			"setFlag": "Hardware_Toeslag_set_0",
			"errorFlag": "Hardware_Toeslag_error_0",
			"errorMessageMoreThanOne": "There are more available Hardware_Toeslag products.Please select one.",
			"errorMessageNone": "There are no available Hardware Toeslagh products. Please perform location check with different parameters.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0"
		},
		"voorloop": {
			"lookupAttributeParent": "Voorloop_Wireless_lookup_0",
			"lookupAttributeChild": "Voorloop_0",
			"relProductAttribute": "Voorloop_Wireless_items_0",
			"setFlag": "Voorloop_Wireless_set_0",
			"errorFlag": "Voorloop_Wireless_error_0",
			"errorMessageMoreThanOne": "There are more available Voorloop wireless products.Please select one.",
			"errorMessageNone": "There are no available Voorloop wireless products.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"category": "Category_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0"
		},
		"voorloopInternet": {
			"lookupAttributeParent": "Voorloop_Wireless_Internet_lookup_0",
			"lookupAttributeChild": "Voorloop_0",
			"relProductAttribute": "Voorloop_Wireless_items_0",
			"setFlag": "Voorloop_Wireless_Internet_set_0",
			"errorFlag": "Voorloop_Wireless_Internet_error_0",
			"errorMessageMoreThanOne": "There are more available Voorloop wireless Internet products.Please select one.",
			"errorMessageNone": "There are no available Voorloop wireless Internet products.",
			"pGALookupAttribute": "PGALookup_0",
			"pgaSetFlag": "PGAset_0",
			"category": "Category_0",
			"hirarchyLevel": "HirarchyLevel_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0"
		},
		"sfp": {
			"lookupAttributeParent": "SFP_Lookup_0",
			"lookupAttributeChild": "SFP_0",
			"relProductAttribute": "SFP_0",
			"setFlag": "SFP_Data_set_0",
			"overbookingType": "Overbooking_Type_0",
			"overbookingTypeMapping": "Overbooking_Type_for_mapping_0"
		}
	}

var emptyPromise = Promise.resolve();

window.priceItemAttributes = priceItemAttributes;

var constants = {
	"set": "set",
	"unset": ""
}

window.constants = constants;
///


//AN CU 

function withoutIndex(attrRef) {
	return attrRef.substring(0, attrRef.length - 1);

}

function setRuleAttributes(attWrapper) {
	if (attWrapper.reference == priceItemAttributes.redundancy.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.redundancy.errorFlag, priceItemAttributes.transportEVC.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.redundancy.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.redundancy.relProductAttribute) + index + ':' + priceItemAttributes.redundancy.overbookingType, 'Redundancy');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.redundancy.relProductAttribute) + index + ':' + priceItemAttributes.redundancy.overbookingTypeMapping, getOverbookingTypeCalculated());
		CS.setAttributeValue(withoutIndex(priceItemAttributes.redundancy.relProductAttribute) + index + ':' + priceItemAttributes.redundancy.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.redundancy.relProductAttribute) + index + ':' + priceItemAttributes.redundancy.hirarchyLevel, 'Parent');
	} else if (attWrapper.reference == priceItemAttributes.transportEVC.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.transportEVC.errorFlag, priceItemAttributes.transportEVC.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.transportEVC.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.transportEVC.relProductAttribute) + index + ':' + priceItemAttributes.transportEVC.overbookingType, 'Transport EVC');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.transportEVC.relProductAttribute) + index + ':' + priceItemAttributes.transportEVC.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.transportEVC.relProductAttribute) + index + ':' + priceItemAttributes.transportEVC.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.transportEVC.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.sipTrunk.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.sipTrunk.errorFlag, priceItemAttributes.sipTrunk.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.sipTrunk.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunk.relProductAttribute) + index + ':' + priceItemAttributes.sipTrunk.overbookingType, 'SIP Trunk');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunk.relProductAttribute) + index + ':' + priceItemAttributes.sipTrunk.overbookingTypeMapping, getOverbookingTypeCalculated());
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunk.relProductAttribute) + index + ':' + priceItemAttributes.sipTrunk.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunk.relProductAttribute) + index + ':' + priceItemAttributes.sipTrunk.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.sipTrunk.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.sipTrunkOneNet.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.sipTrunkOneNet.errorFlag, priceItemAttributes.sipTrunkOneNet.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.sipTrunkOneNet.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunkOneNet.relProductAttribute) + index + ':' + priceItemAttributes.sipTrunkOneNet.overbookingType, 'SIP Trunk');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunkOneNet.relProductAttribute) + index + ':' + priceItemAttributes.sipTrunkOneNet.overbookingTypeMapping, getOverbookingTypeCalculated());
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunkOneNet.relProductAttribute) + index + ':' + priceItemAttributes.sipTrunkOneNet.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunkOneNet.relProductAttribute) + index + ':' + priceItemAttributes.sipTrunkOneNet.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.sipTrunk.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.evcOneFixed.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.evcOneFixed.errorFlag, priceItemAttributes.evcOneFixed.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.evcOneFixed.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneFixed.relProductAttribute) + index + ':' + priceItemAttributes.evcOneFixed.overbookingType, 'OneFixed');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneFixed.relProductAttribute) + index + ':' + priceItemAttributes.evcOneFixed.overbookingTypeMapping, 'Voice');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneFixed.relProductAttribute) + index + ':' + priceItemAttributes.evcOneFixed.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneFixed.relProductAttribute) + index + ':' + priceItemAttributes.evcOneFixed.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.evcOneFixed.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.evcOneNet.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.evcOneNet.errorFlag, priceItemAttributes.evcOneNet.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.evcOneNet.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneNet.relProductAttribute) + index + ':' + priceItemAttributes.evcOneNet.overbookingType, 'OneNet');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneNet.relProductAttribute) + index + ':' + priceItemAttributes.evcOneNet.overbookingTypeMapping, 'Voice');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneNet.relProductAttribute) + index + ':' + priceItemAttributes.evcOneNet.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneNet.relProductAttribute) + index + ':' + priceItemAttributes.evcOneNet.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.evcOneFixed.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.evc1.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.evc1.errorFlag, priceItemAttributes.evc1.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.evc1.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1.relProductAttribute) + index + ':' + priceItemAttributes.evc1.overbookingType, 'Data');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1.relProductAttribute) + index + ':' + priceItemAttributes.evc1.overbookingTypeMapping, calculateOverbookingType(CS.getAttributeValue("Scenario_0", "String"), 1));
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1.relProductAttribute) + index + ':' + priceItemAttributes.evc1.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1.relProductAttribute) + index + ':' + priceItemAttributes.evc1.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.evc1.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.evc1Vlan.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.evc1Vlan.errorFlag, priceItemAttributes.evc1Vlan.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.evc1Vlan.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1Vlan.relProductAttribute) + index + ':' + priceItemAttributes.evc1Vlan.overbookingType, 'Transport Data');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1Vlan.relProductAttribute) + index + ':' + priceItemAttributes.evc1Vlan.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1Vlan.relProductAttribute) + index + ':' + priceItemAttributes.evc1Vlan.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.evc1Vlan.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.evc2.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.evc2.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.evc2.errorFlag, priceItemAttributes.evc2.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2.relProductAttribute) + index + ':' + priceItemAttributes.evc2.overbookingType, 'Voip');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2.relProductAttribute) + index + ':' + priceItemAttributes.evc2.overbookingTypeMapping, calculateOverbookingType(CS.getAttributeValue("Scenario_0", "String"), 2));
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2.relProductAttribute) + index + ':' + priceItemAttributes.evc2.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2.relProductAttribute) + index + ':' + priceItemAttributes.evc2.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.evc2.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.evc2Vlan.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.evc2Vlan.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.evc2Vlan.errorFlag, priceItemAttributes.evc2Vlan.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2Vlan.relProductAttribute) + index + ':' + priceItemAttributes.evc2Vlan.overbookingType, 'Transport Voip');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2Vlan.relProductAttribute) + index + ':' + priceItemAttributes.evc2Vlan.overbookingTypeMapping, calculateOverbookingType(CS.getAttributeValue("Scenario_0", "String"), 2));
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2Vlan.relProductAttribute) + index + ':' + priceItemAttributes.evc2Vlan.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2Vlan.relProductAttribute) + index + ':' + priceItemAttributes.evc2Vlan.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.evc2Vlan.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.managedInternetEVC.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.managedInternetEVC.errorFlag, priceItemAttributes.managedInternetEVC.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.managedInternetEVC.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVC.relProductAttribute) + index + ':' + priceItemAttributes.managedInternetEVC.overbookingType, 'Internet');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVC.relProductAttribute) + index + ':' + priceItemAttributes.managedInternetEVC.overbookingTypeMapping, 'Internet');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVC.relProductAttribute) + index + ':' + priceItemAttributes.managedInternetEVC.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVC.relProductAttribute) + index + ':' + priceItemAttributes.managedInternetEVC.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.managedInternetEVC.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.managedInternetEVCVLAN.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.managedInternetEVCVLAN.errorFlag, priceItemAttributes.managedInternetEVCVLAN.errorMessageMoreThanOne);
		var index = CS.Service.config[priceItemAttributes.managedInternetEVCVLAN.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVCVLAN.relProductAttribute) + index + ':' + priceItemAttributes.managedInternetEVCVLAN.overbookingType, 'Transport Internet');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVCVLAN.relProductAttribute) + index + ':' + priceItemAttributes.managedInternetEVCVLAN.overbookingTypeMapping, 'Internet');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVCVLAN.relProductAttribute) + index + ':' + priceItemAttributes.managedInternetEVCVLAN.additionType, 'Automatic');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVCVLAN.relProductAttribute) + index + ':' + priceItemAttributes.managedInternetEVCVLAN.hirarchyLevel, 'Parent');
		//CS.setAttributeValue(priceItemAttributes.managedInternetEVCVLAN.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.access.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.access.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.access.errorFlag, priceItemAttributes.access.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.access.relProductAttribute) + index + ':' + priceItemAttributes.access.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.access.relProductAttribute) + index + ':' + priceItemAttributes.access.overbookingType, 'Access');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.access.relProductAttribute) + index + ':' + priceItemAttributes.access.overbookingTypeMapping, getOverbookingTypeCalculated());
		//CS.setAttributeValue(priceItemAttributes.access.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.cpeSla.lookupAttributeParent) {
		CS.setAttributeValue(priceItemAttributes.cpeSla.errorFlag, priceItemAttributes.cpeSla.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.cpeSla.relProductAttribute) + index + ':' + priceItemAttributes.cpeSla.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.cpeSla.relProductAttribute) + index + ':' + priceItemAttributes.cpeSla.overbookingType, 'CPE SLA');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.cpeSla.relProductAttribute) + index + ':' + priceItemAttributes.cpeSla.overbookingTypeMapping, getOverbookingTypeCalculated());
		//CS.setAttributeValue(priceItemAttributes.cpeSla.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.voorloop.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.voorloop.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloop.relProductAttribute) + index + ':' + priceItemAttributes.voorloop.category, 'Voorloop');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloop.relProductAttribute) + index + ':' + priceItemAttributes.voorloop.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloop.relProductAttribute) + index + ':' + priceItemAttributes.voorloop.overbookingType, 'Voorloop');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloop.relProductAttribute) + index + ':' + priceItemAttributes.voorloop.overbookingTypeMapping, getOverbookingTypeCalculated());
		CS.setAttributeValue(priceItemAttributes.voorloop.errorFlag, priceItemAttributes.voorloop.errorMessageMoreThanOne);
		//CS.setAttributeValue(priceItemAttributes.voorloop.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.voorloopInternet.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.voorloopInternet.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.voorloopInternet.errorFlag, priceItemAttributes.voorloopInternet.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute) + index + ':' + priceItemAttributes.voorloopInternet.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute) + index + ':' + priceItemAttributes.voorloopInternet.overbookingType, 'Voorloop Internet');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute) + index + ':' + priceItemAttributes.voorloopInternet.overbookingTypeMapping, getOverbookingTypeCalculated());
		//CS.setAttributeValue(priceItemAttributes.voorloopInternet.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.wirelessInstallation.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.wirelessInstallation.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.wirelessInstallation.errorFlag, priceItemAttributes.wirelessInstallation.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessInstallation.relProductAttribute) + index + ':' + priceItemAttributes.wirelessInstallation.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessInstallation.relProductAttribute) + index + ':' + priceItemAttributes.wirelessInstallation.overbookingType, 'Wireless Installation');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessInstallation.relProductAttribute) + index + ':' + priceItemAttributes.wirelessInstallation.overbookingTypeMapping, 'IPVPN Data');
		//CS.setAttributeValue(priceItemAttributes.wirelessInstallation.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.wirelessPricePlan.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.wirelessPricePlan.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.wirelessPricePlan.errorFlag, priceItemAttributes.wirelessPricePlan.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute) + index + ':' + priceItemAttributes.wirelessPricePlan.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute) + index + ':' + priceItemAttributes.wirelessPricePlan.overbookingType, 'Wireless Price Plan');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute) + index + ':' + priceItemAttributes.wirelessPricePlan.overbookingTypeMapping, 'IPVPN Data');
		//CS.setAttributeValue(priceItemAttributes.wirelessPricePlan.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.wirelessPricePlanInternet.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.wirelessPricePlanInternet.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.wirelessPricePlanInternet.errorFlag, priceItemAttributes.wirelessPricePlanInternet.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute) + index + ':' + priceItemAttributes.wirelessPricePlanInternet.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute) + index + ':' + priceItemAttributes.wirelessPricePlanInternet.overbookingType, 'Wireless Price Plan Internet');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute) + index + ':' + priceItemAttributes.wirelessPricePlanInternet.overbookingTypeMapping, 'IPVPN Data');
		//CS.setAttributeValue(priceItemAttributes.wirelessPricePlanInternet.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.cpe.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.cpe.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.cpe.errorFlag, priceItemAttributes.cpe.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.cpe.relProductAttribute) + index + ':' + priceItemAttributes.cpe.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.cpe.relProductAttribute) + index + ':' + priceItemAttributes.cpe.overbookingType, 'CPE');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.cpe.relProductAttribute) + index + ':' + priceItemAttributes.cpe.overbookingTypeMapping, getOverbookingTypeCalculated());
		var qualityOfService = getCPEQualityOfService();
		CS.setAttributeValue(withoutIndex(priceItemAttributes.cpe.relProductAttribute) + index + ':' + priceItemAttributes.cpe.qualityOfService, qualityOfService);
		console.log('Attribute value --> CPE set ==' + qualityOfService);
		//CS.setAttributeValue(priceItemAttributes.cpe.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.hardwareToeslagh.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.hardwareToeslagh.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.hardwareToeslagh.errorFlag, priceItemAttributes.hardwareToeslagh.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.hardwareToeslagh.relProductAttribute) + index + ':' + priceItemAttributes.hardwareToeslagh.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.hardwareToeslagh.relProductAttribute) + index + ':' + priceItemAttributes.hardwareToeslagh.overbookingType, 'Hardware Toeslagh');
		//CS.setAttributeValue(priceItemAttributes.hardwareToeslagh.setFlag, constants.set);
	} else if (attWrapper.reference == priceItemAttributes.wirelessOnly.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.wirelessOnly.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.wirelessOnly.errorFlag, priceItemAttributes.wirelessOnly.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute) + index + ':' + priceItemAttributes.wirelessOnly.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute) + index + ':' + priceItemAttributes.wirelessOnly.overbookingType, 'Wireless Only');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute) + index + ':' + priceItemAttributes.wirelessOnly.overbookingTypeMapping, 'IPVPN Data');
		//CS.setAttributeValue(priceItemAttributes.wirelessOnly.setFlag, constants.set);
		//CS.markConfigurationInvalid(priceItemAttributes.wirelessOnly.errorMessageMoreThanOne);
	} else if (attWrapper.reference == priceItemAttributes.sfp.lookupAttributeParent) {
		var index = CS.Service.config[priceItemAttributes.sfp.relProductAttribute].relatedProducts.length - 1;
		CS.setAttributeValue(priceItemAttributes.sfp.errorFlag, priceItemAttributes.sfp.errorMessageMoreThanOne);
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sfp.relProductAttribute) + index + ':' + priceItemAttributes.sfp.hirarchyLevel, 'Parent');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sfp.relProductAttribute) + index + ':' + priceItemAttributes.sfp.overbookingType, 'SFP');
		CS.setAttributeValue(withoutIndex(priceItemAttributes.sfp.relProductAttribute) + index + ':' + priceItemAttributes.sfp.overbookingTypeMapping, 'SFP');
		//CS.setAttributeValue(priceItemAttributes.wirelessOnly.setFlag, constants.set);
		//CS.markConfigurationInvalid(priceItemAttributes.wirelessOnly.errorMessageMoreThanOne);
	}
}


//NC new Overbooking Type calculation for EVCs
function calculateOverbookingType(ipvpnScenario, type) {
	var retValue = '';

	if (ipvpnScenario == 'Data') {
		retValue = 'IPVPNdata';
	} else if (ipvpnScenario == 'Data & VoiP') {
		retValue = 'IPVPNvoip';
	} else {
		if (type == 1) {
			retValue = 'IPVPNdata';
		} else {
			retValue = 'IPVPNvoice';
		}
	}

	return retValue;
}

function getOverbookingTypeCalculated() {
	var anchorRef = CS.Service.config["EVC_PVC_dep_0"];
	var overbokingTypes = [];

	for (var i = 0; i < anchorRef.relatedProducts.length; i++) {
		overbokingTypes.push(CS.getAttributeValue("EVC_PVC_dep_" + i + ":Overbooking_Type_0"));
	}

	return overbokingTypes.join(";");

}

function getOverbookingTypeForBundles() {
	var internet = CS.getAttributeValue('Managed_Internet_0');
	var oneFixed = CS.getAttributeValue('One_Fixed_0');
	var oneNet = CS.getAttributeValue('One_Net_0');

	var overbookingCalculated = [];

	if (internet !== '') {
		overbookingCalculated.push('Internet');
	}

	if (oneFixed !== '') {
		overbookingCalculated.push('Voice');
	}

	if (oneNet !== '') {
		overbookingCalculated.push('Voice');
	}

	return overbookingCalculated.join(';');
}
//end NC new Overbooking Type calculation for EVCs

//END AN CU
window.checkPGA = function checkPGA(rel, flagSetRef, lookupAttr) {
	var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
	var relProductRef = rel;
	var lookupAttr = lookupAttr;
	var setAccess = CS.getAttributeValue(flagSetRef);
	if (setAccess === '') {
		CS.setAttributeValue(flagSetRef, 'set');
		CS.Service.loadProduct(productId, function () {
			CS.Service.loadProductTemplateHtml(productId, function () {
				var productIndex = CS.Service.getProductIndex(productId);
				jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
				if (CS.Service.config[lookupAttr] !== undefined) {
					CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config[lookupAttr]);
				}
			});
		});

	}
}


window.getCPEQualityOfService = function getCPEQualityOfService() {
	var qualityOfService = false;
	//
	if ((CS.getAttributeValue(priceItemAttributes.evcOneFixed.qualityOfService) == true) || (CS.getAttributeValue(priceItemAttributes.transportEVC.qualityOfService) == true) || (CS.getAttributeValue(priceItemAttributes.evc2.qualityOfService) == true) || (CS.getAttributeValue(priceItemAttributes.evc2Vlan.qualityOfService) == true) || (CS.getAttributeValue(priceItemAttributes.evc1.qualityOfService) == true) || (CS.getAttributeValue(priceItemAttributes.evc1Vlan.qualityOfService) == true)) {
		qualityOfService = true;
	}

	return qualityOfService;
}

window.showHideModularSections = function showHideModularSections() {
	//Show or hide screen sections

	//IPVPN section
	if (CS.getAttributeValue('IPVPN_0') == '') {
		jQuery('h3:contains("' + 'IPVPN' + '")').parent()['hide']();
	} else {
		jQuery('h3:contains("' + 'IPVPN' + '")').parent()['show']();
	}

	//Internet section
	if (CS.getAttributeValue('Managed_Internet_0') == '') {
		jQuery('h3:contains("' + 'Internet' + '")').parent()['hide']();
	} else {
		jQuery('h3:contains("' + 'Internet' + '")').parent()['show']();
	}

	//One Fixed section
	if (CS.getAttributeValue('One_Fixed_0') == '') {
		jQuery('h3:contains("' + 'One Fixed' + '")').parent()['hide']();
	} else {
		jQuery('h3:contains("' + 'One Fixed' + '")').parent()['show']();
	}

	//One Net section
	if (CS.getAttributeValue('One_Net_0') == '') {
		jQuery('h3:contains("' + 'One Net' + '")').parent()['hide']();
	} else {
		jQuery('h3:contains("' + 'One Net' + '")').parent()['show']();
	}

	jQuery('h3:contains("' + 'PVC Information' + '")').parent()['hide']();
}

window.addRelProductInlineAndSetLookupPGA = function addRelProductInlineAndSetLookupPGA(rel, flagSetRef, lookupAttr, results, pgaResAttr) {
	var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
	var relProductRef = rel;
	var lookupAttr = lookupAttr;
	var setAccess = CS.getAttributeValue(flagSetRef);

	var attr = CS.Service.config[rel];
	var emptyPromise = Promise.resolve();
	var parent = CS.getConfigurationWrapper(CS.Util.getParentReference(attr.reference));
	var defId = CS.Service.getAvailableProducts(attr.reference)[0].cscfga__Product_Definition__c;
	loadProduct(defId)
		.then(function () {
			return CS.Service.addRelatedProduct(attr.reference, defId, false).then(function (cfgWrapper) {
				if (pgaResAttr != null) {
					setRuleAttributes(pgaResAttr);
				} else if (results) {
					return CS.EAPI.setLookupAttributeIfOne(CS.Service.config[cfgWrapper.reference], results, lookupAttr).then(function () {
						console.info('Created Configuration: ');
						CS.Rules.evaluateAllRules();
					});
				}
			});
		});
		/*.then(function() {
    		CS.Service.addRelatedProduct(attr.reference, defId, false);
    		if(pgaResAttr!=null){
    		    setRuleAttributes(pgaResAttr);
    		}
    		else if(results){
    	        CS.EAPI.setLookupAttributeIfOne(CS.Service.config[lookupAttr],results);
    	    }
    		
    	})
		.then(function () {
			console.info('Created Configuration: ');
			CS.Rules.evaluateAllRules();

		});*/

	/*
	CS.Service.loadProduct(productId, function(){
		CS.Service.loadProductTemplateHtml(productId, function() {
			var productIndex = CS.Service.getProductIndex(productId);
			jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
			addRelProductInline(relProductRef);
			CS.EAPI.setLookupAttributeIfOne(CS.Service.config[lookupAttr],results);
		});
	});
	*/
}
/*
window.addBundles = function addBundles(rel, flagSetRef){
		var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
		var relProductRef = rel;
		var setAccess = CS.getAttributeValue(flagSetRef);
		if(setAccess === ''){
			CS.setAttributeValue(flagSetRef,'set');
			CS.Service.loadProduct(productId, function(){
				CS.Service.loadProductTemplateHtml(productId, function() {
					var productIndex = CS.Service.getProductIndex(productId);
					jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
					//add aditional Related Products if Coax is selected
					addRelProductInline(relProductRef);
					var index = CS.Service.config['Bundle_Products_0'].relatedProducts.length-1;
					CS.setAttributeValue('Bundle_Products_' + index +':HirarchyLevel_0', 'Parent');
				});
			}).then(function(){
			displayButtons();CS.Rules.evaluateAllRules();});          
		}    
}
*/

window.addBundles = function addBundles(rel, flagSetRef) {
	var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
	var relProductRef = rel;
	var setAccess = CS.getAttributeValue(flagSetRef);
	if (setAccess === '') {
		CS.setAttributeValue(flagSetRef, 'set');
		//addOnlyBundle(rel, productId);
		addBundleNEW();
	}
}


window.waitForRulesToCompleteBundle = function waitForRulesToCompleteBundle(state) {
	function continueWaiting(state) {
		return new Promise(function (resolve, reject) {
			window.setTimeout(function (state) {
				return waitForRulesToCompleteBundle(state).then(resolve);
			}, 300, state);
		});
	}

	if (!state) {
		state = {
			c: 1,
			sc: 1
		};
	}

	if (state.c > 40 || (CS.rulesTimer === undefined && !CS.lookupQueriesAreQueued())) {
		if (state.sc < 2) {
			state.sc += 1;
			state.c = 1;
			console.log('waitForRulesToComplete: waiting for secondary evaluation...');
			//displayButtons();-- CU 4/2018
			hideModularRelatedProducts();
			showHideModularSections();
			return continueWaiting(state);
		}
		console.log('waitForRulesToComplete: rules complete.');
		//displayButtons();-- CU 4/2018
		hideModularRelatedProducts();
		showHideModularSections();
		return Promise.resolve();
	} else {
		state.c += 1;
		state.sc = 1;
		console.log('waitForRulesToComplete: waiting... ' + state.c);
		return continueWaiting(state);
	}
}


function loadProduct(id) {
	var index = CS.Service.getProductIndex(id);
	if (!index) {
		return CS.Service.loadProduct(id, function (index) {
			jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(index));
			return index;
		});
	} else {
		return emptyPromise;
	}
}

function addBundleNEW() {
	var attr = CS.Service.config['Bundle_Products_0'];
	var emptyPromise = Promise.resolve();
	var parent = CS.getConfigurationWrapper(CS.Util.getParentReference(attr.reference));
	var defId = CS.Service.getAvailableProducts(attr.reference)[0].cscfga__Product_Definition__c;
	loadProduct(defId)
		.then(function () {
			//CS.Service.addRelatedProduct(anchorAttributeReference, productDefinitionId, suppressRules) - returns a Promise object
			return CS.Service.addRelatedProduct(attr.reference, defId, false);
		})
		.then(function () {
			console.info('Created Configuration: ');
			var index = CS.Service.config['Bundle_Products_0'].relatedProducts.length - 1;
			CS.setAttributeValue('Bundle_Products_' + index + ':HirarchyLevel_0', 'Parent');
			CS.setAttributeValue('Bundle_Products_' + index + ':Overbooking_Type_0', 'Bundle');
			CS.setAttributeValue('Bundle_Products_' + index + ':Overbooking_Type_for_mapping_0', getOverbookingTypeForBundles());
		});
}

window.addOnlyBundle = function addOnlyBundle(relProductRef, productId) {

	CS.Service.loadProduct(productId, function () {
		CS.Service.loadProductTemplateHtml(productId, function () {
			var productIndex = CS.Service.getProductIndex(productId);
			jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
			addRelProductInline(relProductRef);
			var index = CS.Service.config[relProductRef].relatedProducts.length - 1;
			CS.setAttributeValue('Bundle_Products_' + index + ':HirarchyLevel_0', 'Parent');
		});
	}).then(function () {
		waitForRulesToCompleteBundle().then(function () {
			CS.Rules.evaluateAllRules();
			//displayButtons();-- CU 4/2018
			hideModularRelatedProducts();
			return waitForRulesToCompleteBundle();
		});
	});

}

window.addRelProductInline = function addRelProductInline(rel, flagSetRef, lookupAttr, results, pgaResAttr) {
	var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
	var relProductRef = rel;
	var lookupAttr = lookupAttr;
	var setAccess = CS.getAttributeValue(flagSetRef);
	//if(setAccess === ''){
	//CS.setAttributeValue(flagSetRef,'set');

	var attr = CS.Service.config[rel];
	var emptyPromise = Promise.resolve();
	var parent = CS.getConfigurationWrapper(CS.Util.getParentReference(attr.reference));
	var defId = CS.Service.getAvailableProducts(attr.reference)[0].cscfga__Product_Definition__c;
	loadProduct(defId)
		.then(function () {
			//CS.Service.addRelatedProduct(anchorAttributeReference, productDefinitionId, suppressRules) - returns a Promise object
			CS.Service.addRelatedProduct(attr.reference, defId, false); //.then(function(){renderLookupsForAccessInfra();});
			//CS.InlineEdit.addRelatedProduct(attr.reference, defId);
			if (pgaResAttr != null) {
				setRuleAttributes(pgaResAttr);
			}
			var indexNumberOfCopiedRelProd = CS.getAttributeWrapper(relProductRef).relatedProducts.length;
			var indexItem = indexNumberOfCopiedRelProd - 1;

			var wrapperDef = relProductRef.substring(0, relProductRef.length - 1);
			CS.Service.config[wrapperDef + (indexItem)].customLookupReferencedAttributes = getCustomAttributesPerRef(relProductRef);
		})
		.then(function () {
			console.info('Created Configuration: ');
			/*
			if(pgaResAttr!=null){
				setRuleAttributes(pgaResAttr);
			}
			*/
			var indexNumberOfCopiedRelProd = CS.getAttributeWrapper(relProductRef).relatedProducts.length;
			var indexItem = indexNumberOfCopiedRelProd - 1;

			var wrapperDef = relProductRef.substring(0, relProductRef.length - 1);

			//delete CS.getAttributeWrapper(wrapperDef + (indexItem)).unsaved;

			CS.Rules.evaluateAllRules();
			//debugger;
			CS.Service.config[wrapperDef + (indexItem)].customLookupReferencedAttributes = getCustomAttributesPerRef(relProductRef);
		});

	/*
	CS.Service.loadProduct(productId, function(){
		CS.Service.loadProductTemplateHtml(productId, function() {
			var productIndex = CS.Service.getProductIndex(productId);
			jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
			//debugger;
			addRelProductInline(relProductRef);
		});
	});
	*/
	//}
}

window.addRelProductInlineAndSetLookup = function addRelProductInlineAndSetLookup(rel, flagSetRef, lookupAttr) {
	//var productId = "a4F9E0000009JlV";
	var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
	var relProductRef = rel;
	var lookupAttr = lookupAttr;
	var setAccess = CS.getAttributeValue(flagSetRef);
	if (setAccess === '') {
		CS.setAttributeValue(flagSetRef, 'set');
		CS.Service.loadProduct(productId, function () {
			CS.Service.loadProductTemplateHtml(productId, function () {
				var productIndex = CS.Service.getProductIndex(productId);
				jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
				addRelProductInline(relProductRef);
				CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config[lookupAttr]);
				//displayButtons(); -- CU 4/2018

			});
		});
	}
}

window.reWireConfigurationControllerActions = function reWireConfigurationControllerActions() {
	if (navigator && navigator.device) {
		CS.App.getController('configuration').delegate.wireActions(jQuery('section[data-cs-displays="Viewport"]'));
	} else {
		//applyActions('#configurationContainer');
	}
}
/*
	window.addRelProductInline = function addRelProductInline(relProductRef){

		addRelatedWithoutChangingScreen(relProductRef);

		var indexNumberOfCopiedRelProd = CS.getAttributeWrapper(relProductRef).relatedProducts.length;
	   
		var indexItem = indexNumberOfCopiedRelProd -1;

		var wrapperDef = relProductRef.substring(0, relProductRef.length-1);
		delete CS.getAttributeWrapper(wrapperDef + (indexItem)).unsaved;
		
		//CS.setAttributeValue('Access_set_0','set');
		
		validateCurrentConfig();
		CS.Service.displayScreen(0, function(){
			reWireConfigurationControllerActions();
			
		});
		
		CS.Rules.evaluateAllRules();
		//debugger;
		CS.Service.config[wrapperDef+(indexItem)].customLookupReferencedAttributes = getCustomAttributesPerRef(relProductRef);
	}
*/
function getCustomAttributesPerRef(relProductRef) {

	var customAttributesListString = '';
	var keyString = '';
	var lookupObj = {};
	if (relProductRef == 'EVC_PVC_dep_0') {
		keyString = CS.Service.config["EVC_PVC_dep_0:EVC_PVC_0"].definition.cscfga__Lookup_Config__c;
		lookupObj[keyString] = '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType", "Min. Upstream Bandwidth", "Min. Upstream Bandwidth 2", "Min.Downstream Bandwidth", "Min.Downstream Bandwidth 2", "Total Bandwidth Up", "Total Bandwidth Down", "Total Bandwidth Up Entry", "Total Bandwidth Down Entry", "Total Bandwidth Up Premium", "Total Bandwidth Down Premium", "Available bandwidth up premium", "Available bandwidth down premium", "Premium Applicable", "Overbooking Internet", "Min. Upstream Bandwidth Internet", "Min.Downstream Bandwidth Internet", "Proposition Internet", "Overbooking Transport", "Proposition OneFixed", "Overbooking OneFixed", "Min Bandwidth OneFixed", "IPVPN", "Managed Internet", "OneFixed", "Entry BW 6MB","Overbooking OneNet","Min Bandwidth OneNet","Proposition OneNet", "One Net"]';
		customAttributesListString = lookupObj;
	}
	if (relProductRef == 'Access_Infrastructure_0') {
		keyString = CS.Service.config["Access_Infrastructure_0:Access_0"].definition.cscfga__Lookup_Config__c;
		lookupObj[keyString] = '["Total Bandwidth Down","Total Bandwidth Up","Vendor","Access Type Calc","Region","Infra SLA","Contract Duration","Available bandwidth up","Available bandwidth down","Result Check", "Proposition", "Overbooking Voip","Contract Duration", "Line Type", "Min. Upstream Bandwidth", "Min.Downstream Bandwidth", "Min. Upstream Bandwidth 2", "Min.Downstream Bandwidth 2", "Min. Upstream Bandwidth Internet", "Min.Downstream Bandwidth Internet","Min Bandwidth OneFixed"]';
		customAttributesListString = lookupObj;
	}
	if (relProductRef == 'CPE_0') {
		keyString = CS.Service.config["CPE_0:CPE_Product_0"].definition.cscfga__Lookup_Config__c;
		lookupObj[keyString] = '["Available bandwidth down","Quality of Service","Access Type","Available bandwidth up","Num LAN","Num WAN","Num WANLAN","Wireless backup final","Num SFP", "Overbooking Type", "Technology type", "Codec", "SIP Channels"]';
		customAttributesListString = lookupObj;
	}
	if (relProductRef == 'Bundle_Products_0') {
		keyString = CS.Service.config["Bundle_Products_0:Bundle_lookup_0"].definition.cscfga__Lookup_Config__c;
		lookupObj[keyString] = '["Vendor", "Access Type", "Result Check", "Region", "IPVPN", "Managed Internet", "OneFixed", "Scenario OneFixed", "Min. Upstream Bandwidth Internet", "Min.Downstream Bandwidth Internet", "Min Bandwidth OneFixed", "Available bandwidth up", "Available bandwidth down", "Proposition", "Proposition Internet", "Proposition OneFixed", "Wireless backup", "SIP Channels", "Codec", "Technology type", "Contract Duration", "CPE SLA"]';
		customAttributesListString = lookupObj;
	}
	if (relProductRef == 'Add_on_Licenses_0') {
		keyString = CS.Service.config["Add_on_Licenses_0:Product_0"].definition.cscfga__Lookup_Config__c;
		lookupObj[keyString] = '["TempVal"]';
		customAttributesListString = lookupObj;
	}
	return customAttributesListString;

}

window.addRelatedWithoutChangingScreen = function addRelatedWithoutChangingScreen(relProductRef) {


	var defId = CS.Service.getAvailableProducts(relProductRef)[0].cscfga__Product_Definition__c;

	var productId = defId;
	var anchorRef = relProductRef;
	var products,
		productDef,
		parent = CS.Service.config[CS.Service.getCurrentConfigRef()],
		anchorWrapper = CS.Service.config[anchorRef],
		attrDef = CS.Service.getAttributeDefinitionForReference(anchorRef);

	if (!attrDef) {
		return;
	}

	var prefix = CS.Util.configuratorPrefix;

	if (attrDef[prefix + 'Type__c'] != 'Related Product') {
		CS.Log.error('Cannot add related product on Attribute of type ', attrDef[prefix + 'Type__c']);
		return;
	}

	var min = attrDef[prefix + 'Min__c'];
	var max = attrDef[prefix + 'Max__c'];
	if (max == anchorWrapper.relatedProducts.length) {
		CS.displayInfo('New item cannot be added - the maximum number of ' + anchorWrapper.definition.Name + ' records is ' + max);
		return;
	}

	if (!productId) {
		products = CS.Service.getAvailableProducts(anchorRef);
		productId = products[0][prefix + 'Product_Definition__c'];
	}


	var initialAttrs = [];
	jQuery.each(CS.Service.config, function (key, value) {
		initialAttrs.push(key);
	});

	var ROOT_REFERENCE = '';
	var newProductId = productId;
	var productIndex = CS.Service.getProductIndex(newProductId);


	var configWrapper = createConfiguration(CS.Service.config, anchorRef, productId, parent);

	var numRelatedProducts = anchorWrapper.relatedProducts.length;
	var maxIndex = 0;
	var rpAttrName = attrDef['Name'];
	for (var i = 0; i < numRelatedProducts; i++) {
		var rpPcName = anchorWrapper.relatedProducts[i].config['Name'];
		if (rpPcName.indexOf(rpAttrName) === 0) {
			rpPcName = rpPcName.substring(rpAttrName.length - 1);
		}
		var tokens = rpPcName.split(' ');
		var lastToken = tokens[tokens.length - 1];
		if (jQuery.isNumeric(lastToken)) {
			var sufixIndex = parseInt(lastToken);
			if (maxIndex < sufixIndex) {
				maxIndex = sufixIndex;
			}
		}
	}

	maxIndex++;


	configWrapper.config.Name = attrDef['Name'];
	anchorWrapper.attr[prefix + 'Display_Value__c'] = configWrapper.config.Name;
	configWrapper.config[CS.Util.configuratorPrefix + 'Configuration_Status__c'] = 'Valid';


	CS.Service.config[''].bound = false;
	relatedProductsRendered();

}

function populateScreens(productId, config) {
	var productIndex = CS.Service.getProductIndex(productId),
		screensByProduct = productIndex.screensByProduct[productId],
		configScreens = [],
		idx = 0,
		attrRefsByDef = {},
		newAttrDefs = productIndex.attributeDefsByProduct[productId],
		defId,
		attrContext = {
			ref: '',
			index: 0
		};

	for (defId in newAttrDefs) {
		ref = CS.Util.generateReference(newAttrDefs[defId].Name, attrContext);
		attrRefsByDef[defId] = ref;
	}

	for (idx in screensByProduct) {
		var screen = screensByProduct[idx],
			attrs = productIndex.attributeDefsByScreen[screen.Id],
			attrRefs = [];

		for (var attrId in attrs) {
			attrRefs.push(attrRefsByDef[attrId]);
		}

		configScreens[idx] = {
			id: screen.Id,
			reference: screen._reference,
			attrs: attrRefs
		};
	}

	config.screens = configScreens;
}



function relatedProductsRendered() {
	if (typeof CS.Service.config[''].bound !== 'undefined' && !CS.Service.config[''].bound) {
		CS.Log.warn('Rebinding');
		CS.binding = CS.DataBinder.bind(CS.Service.config, CS.Service.getProductIndex(), undefined);

		/*
		CS.binding.on('afterUpdate', function(ref, properties, event) {
			CS.Log.debug('After Update', ref, properties, event);
			
			//CS.Rules.evaluateAllRules('after update: ' + ref);
			//CS.rulesTimer = undefined;
			
			if (!CS.rulesTimer) {
				CS.rulesTimer = window.setTimeout(function() {
					CS.Rules.evaluateAllRules('after update: ' + ref);
					CS.rulesTimer = undefined;
				}, 400);
			}
			
			
		});
*/
		CS.Service.config[''].bound = true;
	}
}


function validateCurrentConfig(highlight) {
	//return validateScreens(CS.Service.config, currentScreen.reference, highlight);
	return validateScreens(CS.Service.config, CS.Service.getCurrentScreen().reference, highlight);
}


function validateScreens(data, spec, highlight) {
	var index = CS.Service.getProductIndex(),
		result = {
			isValid: true,
			fieldErrors: {},
			screens: {}
		},
		configRef = typeof spec === 'object' ? spec.reference : spec,
		screenIndex = typeof spec === 'object' ? spec.index : undefined,
		wrapper = data[configRef],
		screens = wrapper ? wrapper.screens : undefined;

	if (highlight === undefined) {
		highlight = true;
	}

	if (!screens) {
		CS.Log.error('No screens found for validation');
		return result;
	}

	if (screenIndex || screenIndex === 0) {
		result = doScreenValidation(data, screens[screenIndex], highlight, configRef);
		if (highlight) {
			wrapper.screens[screenIndex].validation = result;
		}
	} else {
		jQuery.each(screens, function (i, screen) {
			var r = doScreenValidation(data, screen, highlight, configRef);
			if (highlight) {
				wrapper.screens[i].validation = r;
			}
			jQuery.extend(result.screens, r.screens);
			if (!r.isValid) {
				result.isValid = false;
				if (highlight) {
					jQuery.extend(result.fieldErrors, r.fieldErrors);
				}
			}
		});
	}

	return result;
}

function doScreenValidation(data, screen, highlight, configRef) {
	var result = {
		isValid: true,
		fieldErrors: {},
		screens: {}
	};

	if (highlight === undefined) {
		highlight = true;
	}

	result.screens[screen.id] = true;
	if (typeof configRef === 'undefined') {
		configRef = CS.Service.getCurrentConfigRef();
	}

	var prefix = CS.Util.configuratorPrefix;
	jQuery.each(screen.attrs, function (i, ref) {
		var attrRef = (configRef === '' ? ref : configRef + ':' + ref),
			it = data[attrRef],
			valueField = prefix + 'Value__c',
			fieldError = {
				validationError: false,
				validationMessage: undefined
			};

		if (it && it.attr) {
			var isRequiredActiveVisible = (it.attr[prefix + 'Is_Required__c'] && it.attr[prefix + 'is_active__c'] && !it.attr[prefix + 'Hidden__c']);
			if (isRequiredActiveVisible && isValueBlank(it)) {
				fieldError.validationError = true;
				fieldError.validationMessage = 'This value is required';
			} else if (it.definition[prefix + 'Type__c'] === 'Related Product' &&
				(it.relatedProducts instanceof Array) &&
				it.relatedProducts.length > 0
			) {
				for (var c = 0; c < it.relatedProducts.length; c++) {
					var relatedConfig = it.relatedProducts[c].config;
					if (relatedConfig[prefix + 'Configuration_Status__c'] !== 'Valid') {
						fieldError.validationError = true;
						fieldError.validationMessage = 'Related product attribute contains incomplete configurations';
						break;
					}
				}
			}

			result.fieldErrors[attrRef] = fieldError;
			if (fieldError.validationError) {
				result.screens[screen.id] = false;
				result.isValid = false;
			}
			if (highlight) {
				CS.binding.update(attrRef, result.fieldErrors[attrRef]);
			}
		}
	});

	return result;

	function isValueBlank(item) {
		var valueField = prefix + 'Value__c';
		var retValue = (item.attr[valueField] === null || item.attr[valueField] === '');

		if (item.definition) {
			if (item.definition[prefix + 'Type__c'] in {
					'Select List': '',
					'Radio Button': ''
				}) {
				retValue = retValue || (item.attr[valueField] === CS.NULL_OPTION_VALUE);
			} else if (item.definition[prefix + 'Type__c'] === 'Related Product') {
				// Ignore the Value field contents, check only related products array's size
				retValue = (item.relatedProducts.length < 1);
			}
		}

		return retValue;
	}
}

/*
function loadRulesForConfig(reference, productDef) {
		if (!CS.Rules.hasRules(reference)) {
			var tpl = jQuery('#' + CS.Util.generateId(productDef[CS.Util.offlinePrefix + 'Reference__c']) + '__rules'),
				rules,
				idx = 0; // this will be for 'leaf' Attributes which presently will always be index 0 (this will change if attribute arrays are introduced using the leaf node index)

			if (tpl.size() === 0) {
				Log.warn('Could not find rules template with reference: ' + productDef[Util.offlinePrefix + 'Reference__c']);
			} else {
				var ref = reference ? reference + ':' : '';
				rules = tpl.get(0).innerHTML.replace(/%idx%/g, idx).replace(/%ctx%/g, ref);
				CS.Rules.addRules(reference, rules);
			}
		}                   
	}
	*/

function loadRulesForConfig(reference, productDef) {
	CS.Log.info('EAPI compatibilty: loadRulesForConfig', reference, productDef);
	if (CS.Rules.hasRules(reference)) {
		return;
	}

	var referenceField = CS.Util.configuratorPrefix + 'reference__c';
	if (!productDef.hasOwnProperty(referenceField)) {
		CS.Log.error('Could not find the field reference__c in the current product definition', productDef);
		return;
	}

	var definitionRef = productDef[referenceField];
	if (!definitionRef) {
		CS.Log.error('Current product definition\'s reference is not defined: ', productDef);
		return;
	}

	var tpl = jQuery('#' + CS.Util.generateId(definitionRef) + '__rules');
	var idx = 0; // this will be for 'leaf' Attributes which presently will always be index 0 (this will change if attribute arrays are introduced using the leaf node index)

	if (tpl.size() === 0) {
		CS.Log.warn('Could not find rules template with reference: ' + definitionRef);
	} else {
		var rules = CS.Util.applyContext(tpl.get(0).innerHTML, idx, reference);
		CS.Rules.addRules(reference, rules);
	}
}


function updateAttribute(input) {
	var wrapper = CS.getAttributeWrapper(input.name);
	wrapper.attr['cscfga__Value__c'] = input.value;
	if (input.type === 'select-one') {
		// CS.setAttributeValue(input.name, jQuery(input).find('option:selected').text(), false);
		var value = jQuery(input).find('option:selected').text();
		wrapper.attr['cscfga__Display_Value__c'] = value;
	} else {
		// CS.setAttributeValue(input.name, input.value, false);
		wrapper.attr['cscfga__Display_Value__c'] = input.value;
	}
}

function buildAttribute(def, context, selectOptions, attributeFields) {
	context = context || {};
	var prefix = CS.Util.configuratorPrefix;
	var wrapper = {
		"attr": {
			"attributes": {
				"type": prefix + "Attribute__c"
			}
		},
		"attributeFields": {},
		"definitionId": def.Id,
		"displayInfo": context.displayInfo || def[prefix + 'Type__c'],
		"reference": CS.Util.generateReference(def.Name, context),
		"relatedProducts": [],
		"selectOptions": selectOptions
	};

	var typeInfo = {
		'type': def[prefix + 'Data_Type__c'],
		'scale': def[prefix + 'Scale__c']
	};

	wrapper.attr[prefix + "Attribute_Definition__c"] = def.Id;
	wrapper.attr[prefix + 'Cascade_value__c'] = def[prefix + 'Cascade_value__c'];
	wrapper.attr[prefix + 'Display_Value__c'] = (
		def[prefix + 'Type__c'] === 'Calculation' ?
		null :
		def[prefix + 'Default_Value__c']
	);
	wrapper.attr[prefix + 'Hidden__c'] = def[prefix + 'Hidden__c'];
	wrapper.attr[prefix + 'is_active__c'] = true;
	wrapper.attr[prefix + 'Is_Line_Item__c'] = def[prefix + 'Is_Line_Item__c'];
	wrapper.attr[prefix + 'Is_Required__c'] = def[prefix + 'Required__c'];
	wrapper.attr[prefix + 'Line_Item_Sequence__c'] = def[prefix + 'Line_Item_Sequence__c'];
	wrapper.attr[prefix + 'Line_Item_Description__c'] = def[prefix + 'Line_Item_Description__c'];
	wrapper.attr.Name = def.Name;
	wrapper.attr[prefix + 'Price__c'] = def[prefix + 'Base_Price__c'] || 0;

	wrapper.attr[prefix + 'Value__c'] = (
		def[prefix + 'Type__c'] === 'Calculation' ?
		'' :
		def[prefix + 'Default_Value__c']
	);

	wrapper.attr[prefix + 'Recurring__c'] = def[prefix + 'Recurring__c'];

	if (def[prefix + 'Type__c'] === 'Select List' && def[prefix + 'Default_Value__c'] && selectOptions) {
		for (var i = 0; i < selectOptions.length; i++) {
			if (selectOptions[i] == def[prefix + 'Default_Value__c']) {
				wrapper.attr[prefix + 'Display_Value__c'] = selectOptions[i].Name;
				break;
			}
		}
	}

	_.each(attributeFields, function (a) {
		CS.Service.setAttributeField(wrapper, a.Name, a[prefix + 'Default_Value__c']);
	});

	return wrapper;
}

function getContext(ref, attrName, idx, parent) {
	return {
		ref: ref,
		attrName: attrName,
		index: (idx || 0),
		parent: parent
	};
}

function buildConfig(def, reference, context) {
	var wrapper = {
		"reference": reference,
		"config": {
			"attributes": {
				"type": "Product_Configuration__c"
			}
		}
	};
	var prefix = CS.Util.configuratorPrefix;
	wrapper.config[prefix + 'Attribute_Name__c'] = context.attrName;
	wrapper.config[prefix + 'Billing_Frequency__c'] = CS.getFrequencyValueForName(def[prefix + 'Default_Billing_Frequency__c']);
	wrapper.config[prefix + 'Contract_Term__c'] = def[prefix + 'Default_Contract_Term__c'];
	wrapper.config[prefix + 'Contract_Term_Period__c'] = CS.getPeriodValueForName(def[prefix + 'Default_Contract_Term_Period__c']);
	wrapper.config[prefix + 'Description__c'] = def[prefix + 'Description__c'];
	wrapper.config[prefix + 'Index__c'] = context.index;
	wrapper.config[prefix + 'Last_Screen_Index__c'] = 0;
	wrapper.config.Name = reference;
	wrapper.config[prefix + 'Product_Definition__c'] = def.Id;
	wrapper.config[prefix + 'Recurrence_Frequency__c'] = CS.getFrequencyValueForName(def[prefix + 'Default_Frequency__c']);
	wrapper.config[prefix + 'Configuration_Status__c'] = 'Incomplete';
	wrapper.config[prefix + 'Validation_Message__c'] = '';
	wrapper.config[prefix + 'Product_Family__c'] = (def.Name.length > 40) ? def.Name.substr(0, 40) : def.Name;

	return wrapper;
}

function createConfiguration(configData, anchorRef, newProductId, parent) {

	var ROOT_REFERENCE = '';
	var productIndex = CS.Service.getProductIndex(newProductId);

	if (!productIndex) {
		throw 'Product index for ' + newProductId + ' not found';
	}

	var productDef = productIndex.productsById[newProductId],
		wrapper = configData[anchorRef],
		newAttrDefs = productIndex.attributeDefsByProduct[newProductId],
		screensByProduct = productIndex.screensByProduct[newProductId],
		configScreens = [],
		idx = 0,
		name,
		newConfig = {},
		context,
		attr,
		defId,
		ref;

	if (anchorRef !== ROOT_REFERENCE && !wrapper) {
		return error('Could not locate reference ', anchorRef, configData);
	}

	if (!productDef) {
		return error('Could not find product definition for id', newProductId);
	}

	if (!newAttrDefs) {
		return error('Could not find attribute definitions for product id', newProductId);
	}

	if (anchorRef === ROOT_REFERENCE) {
		// root config
		ref = anchorRef;
	} else {
		// attaching a related product configuration to an attribute on the parent
		idx = wrapper.relatedProducts.length;
		name = wrapper.attr.Name;
		ref = CS.Util.stripReference(anchorRef) + idx;
	}
	context = getContext(ref, name, idx, parent);

	newConfigWrapper = buildConfig(productDef, ref, context);

	CS.Log.info('Creating configuration for reference ' + ref);

	if (anchorRef !== ROOT_REFERENCE) {
		// Link related product to parent and mark as unsaved
		newConfigWrapper.parent = parent;
		newConfigWrapper.unsaved = true;
		var relatedProducts = wrapper.relatedProducts.slice(0);
		relatedProducts[idx] = newConfigWrapper;
		CS.binding.update(anchorRef, {
			relatedProducts: relatedProducts
		});
	}

	var attrContext = {
		ref: context.ref,
		index: 0
	};

	for (defId in newAttrDefs) {
		attr = buildAttribute(newAttrDefs[defId], attrContext, productIndex.find('selectOptionsByAttribute', defId), productIndex.find('attributeFieldDefsByAttributeDef', defId));
		attr.definition = productIndex.all[attr.definitionId];
		configData[attr.reference] = attr;
	}

	populateScreens(newProductId, newConfigWrapper);

	if (configData[ref]) {
		jQuery.extend(configData[anchorRef], newConfigWrapper);
	} else {
		configData[ref] = newConfigWrapper;
	}

	loadRulesForConfig(ref, productDef);


	return newConfigWrapper;
}