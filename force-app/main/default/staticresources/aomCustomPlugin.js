/*
* Default options:
    * hideDefaultAddons: true
	* showManagerButton: true
	* hideAdded: true
	* deleteAddOnsOnTransition: false
	* allowQuantities: false
	* renderInSection: false
*/
addOnConfig = {
	hideDefaultAddons: true,
	showManagerButton: false,
	hideAdded: true,
	deleteAddOnsOnTransition: false,
	allowQuantities: true,
	renderInSection: false
};