var addFlatFeeProduct = false;
var addRowProduct = false;

function replaceMobileCtnButton() {
	//jQuery("#generateCtnButtonId").hide();
	//jQuery("button[data-cs-ref='Mobile_CTN_addons_0']").after("<button id='generateCtnButtonId' class='btn btn-primary' onclick='beforeSave()' data-role='none'>Generate Mobile CTN's</button>");
	jQuery("button[data-cs-ref='Mobile_CTN_addons_0']").remove();
}

function finish(buttonElement) {
    	CS.Log.info('ANA TEST  !!!!n...');
	beforeSave();
}

function beforeSave() {
	function saveConfiguration(buttonElement) {
		var basketSpec = {
			Id: params.basketId,
			linkedId: params.linkedId,
			packageSlotId: params.packageSlotId
		};
		if (typeof buttonElement === 'undefined') {
			buttonElement = this;
		}
		/*
		CS.Log.info('Persisting configuration...');
		CS.Service.persistConfiguration(basketSpec, (function(p) {
			return function(result, redirectCallback) {
				if (result._success) {
					//var redirectUrl = buildAfterFinishUrl(p, result);
					
					//CS.Log.info('Configuration persisted');
					//CS.UI.navigateTo(redirectUrl);
				} else {
					CS.markConfigurationInvalid(result._message);
					// updateFinishButtonUI(buttonElement, true);
				}
			}
		})(params));*/
	}

	var self = this;
	CS.Util.waitFor(
		function until() {
			CS.Log.info('*** finish(): Waiting for rules to finish... ');
			return (typeof CS.rulesTimer === 'undefined');
		},
		function payload() {
			var validationResult = CS.UI.getCurrentInstance().validateCurrentConfiguration(); // CS.Service.validateCurrentConfig(true)
			var configurationStatus = CS.getConfigurationProperty('', 'status');
	
			if (configurationStatus === 'Incomplete' || !(validationResult.isValid)) {
			    
			    if(CS.Service.getCurrentScreen().reference == 'Mobile_CTN_profile:Addons'){
    			    CS.markConfigurationInvalid('Addons will be added only for valid configurations! ');
    			    if(CS.Service.config["Mobile_CTN_addons_0"].relatedProducts.length > 0){
                    	_.each(CS.Service.config["Mobile_CTN_addons_0"].relatedProducts,function(p){
                    			   CS.Service.removeRelatedProduct('Mobile_CTN_addons_0'); 
                    		});
                    }
			    }

				// updateFinishButtonUI(self, true);
				// There are validation errors within the configuration. The configuration can still be saved but the basket cannot be synchronised with an Opportunity until the errors have been corrected.
				CS.markConfigurationInvalid('There are validation errors within the configuration. The configuration can still be saved but the basket cannot be synchronised with an Opportunity until the errors have been corrected.');

			} else {
			    
			    if(CS.Service.getCurrentScreen().reference == 'Mobile_CTN_profile:Addons'){
			        
			        //check if system needs to regenerate Addons
			        var generate = checkIfGenerateAddons();
			        if(generate) {
    			        console.log('Right config -- mobile customisation');
    			        
    			        //comments started here
    			        if(CS.Service.config["Mobile_CTN_addons_0"].relatedProducts.length > 0){
                    	_.each(CS.Service.config["Mobile_CTN_addons_0"].relatedProducts,function(p){
                    			   CS.Service.removeRelatedProduct('Mobile_CTN_addons_0'); 
                    		});
                    	}
                    	var num = getNumberOfNewRelProducts();
                    	if(num>0){
                    		addRelProductInlineOnly('Mobile_CTN_addons_0',num);
                    		saveConfiguration(self);
                   		}
                    	else{
                    	    saveConfiguration(self);
                    	}
			        }
                	
			    } else if(CS.Service.config[""].config.cscfga__Product_Family__c == 'Access Infrastructure'){
			        console.log('Right config -- mobile customisation');
			        
			        //comments started here
			        
			        //marking invalid --> bundle && advance clone
			        if((CS.getAttributeValue('Modular_pricing_0') == false) || (CS.getAttributeValue('RequiresReClone_0') == 'cloned') || (CS.getAttributeValue('RequiresReClone_0') == 'needsReclone')){
			            if(CS.getAttributeValue('Modular_pricing_0') == false){
        			        if(CS.Service.config["Bundle_Products_0"].relatedProducts.length <=1){
        			            CS.markConfigurationInvalid("No price items - setting to invalid!");
        			        }
			            }
			            
			            if((CS.getAttributeValue('RequiresReClone_0') == 'cloned') || (CS.getAttributeValue('RequiresReClone_0') == 'needsReclone')){
			                if(CS.getAttributeValue('RequiresReClone_0') == 'cloned'){
			                    CS.setAttributeValue('RequiresReClone_0','needsReclone');
			                }
			                CS.markConfigurationInvalid("Please perform another advance clone!");
			            }
			            
			            saveConfiguration(self);
			        }
			        
			        /*
			        if(CS.getAttributeValue('Modular_pricing_0') == false){
    			        if(CS.Service.config["Bundle_Products_0"].relatedProducts.length <=1){
    			        
    			            CS.markConfigurationInvalid("No price items - setting to invalid!");
    			            saveConfiguration(self);
    			        }
			            else{
			               saveConfiguration(self);
			            }
			        }
			        */
                	else{
                	    saveConfiguration(self);
                	}
                	
			    }
			    
			    else if(CS.Service.getCurrentScreen().reference == 'Vodafone_Products:Company_Level_Fixed_Voice:Fixed_Usage_Profile') {
			        console.log('FUP finish');
                	
                	if(addRowProduct) {
                		addRelProductInlineOnlyUpdated('RoW_bundle_product_0', 1, 'RoW_Lookup_0');
                	} else {
						CS.Service.removeRelatedProduct('RoW_bundle_product_0'); 
                	}
                	saveConfiguration(self);
			    } else{
					saveConfiguration(self);
			    }
			}
		}
	);
}

window.checkIfGenerateAddons = function checkIfGenerateAddons(){
    var generate = false;
    var numberOfAddons = CS.Service.config["Mobile_CTN_addons_0"].relatedProducts.length;
    var numberOfAddonSelect = CS.Service.config["Mobile_CTN_addon_select_0"].relatedProducts.length;
    var numberOfSubscription = CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts.length;
    
    if(numberOfAddons == (numberOfSubscription*numberOfAddonSelect)){
        var data = getMapForFields();
        var relevantFields = ['Addon_category_0','CTN_quantity_0','Connection_type_0', 'Duration_0', 'Price_item_0', 'Profile_Id_0', 'Recurring_price_0', 'Scenario_0', 'Start_month_0', 'Subscription_0', 'Network_Promo_OneOff_0', 'Network_Promo_Recurring_0', 'Originating_subscription_0'];
        var relevantFieldsForMobileCTNAddonsSelect = ['Addon_hardware_commission_0', 'Network_Promo_Recurring_0','Network_Promo_OneOff_0'];
    
    	if(CS.Service.config["Mobile_CTN_addons_0"].relatedProducts.length > 0){
    		for(var idx=0;idx<data.length;idx++){
    			for(var idF=0;idF<relevantFields.length;idF++){
                    if(relevantFields[idF] != 'Recurring_price_0' && relevantFields[idF] != 'Network_Promo_Recurring_0' && relevantFields[idF] != 'Network_Promo_OneOff_0' && relevantFields[idF] != 'Addon_category_0'){
        				var key = "Mobile_CTN_addons_"+idx+':'+relevantFields[idF];
        				if(CS.getAttributeValue(key) != data[idx][relevantFields[idF]]){
                            generate = true;
    
                            console.log('Reset mobile addon generation because values have changed');
                            console.log(key);
                            console.log(CS.getAttributeValue(key));
                            console.log(data[idx][relevantFields[idF]]);
                            console.log('-------------------------------------');
                        }
                        
        			}
        			else {
        			    if((relevantFields[idF] == 'Network_Promo_Recurring_0')||(relevantFields[idF] == 'Network_Promo_OneOff_0')){
        			        var key = "Mobile_CTN_addons_"+idx+':'+relevantFields[idF];
        			        if(CS.getAttributeValue(key) != data[idx][relevantFields[idF]]){
                                if((data[idx][relevantFields[idF]] == null) && CS.getAttributeValue(key)== 0){
                                    //ignore this difference
                                    console.log('Do not generate mobile addons - 0 equals null for Network Promo!!!');
                                }
                                else{
                                    generate = true;
                                    console.log('Reset mobile addon generation because not equal Network Promo!');
                                    console.log(key);
                                    console.log(CS.getAttributeValue(key));
                                    console.log(data[idx][relevantFields[idF]]);
                                    console.log('-------------------------------------');
                                }
                            }
        			    }
        			    if(relevantFields[idF] == 'Recurring_price_0'){
        			        var keyForLookup = 'Mobile_CTN_addons_'+idx+':Price_item_0';
                            var keyForSearch = CS.getAttributeValue(keyForLookup);
    			            var recPrice = getLookupVal(keyForSearch, 'cspmb__recurring_charge__c');//CS.getAttributeValue('Mobile_CTN_addons_'+idx+':Recurring_price_0');
                            var ctnQuantity = CS.getAttributeValue('Mobile_CTN_addons_'+idx+':CTN_quantity_0');
                            var priceVal = 0;//CS.getAttributeValue('Mobile_CTN_addons_'+idx+':Recurring_price_0') * CS.getAttributeValue('Mobile_CTN_addons_'+idx+':CTN_quantity_0');
                            var priceAttrKey = 'Mobile_CTN_addons_'+idx+':Recurring_price_0';
                            if(ctnQuantity!='' && recPrice!=''){
                                priceVal = recPrice*ctnQuantity;
                            }
                            
                            if(CS.getAttributeValue(priceAttrKey) != (priceVal.toFixed(2))){
                                generate = true;
                                console.log('Reset mobile addon generation because recurring price is different!');
                            }
                            
        			    }
        			}
                }
    
                for(var idF=0;idF<relevantFieldsForMobileCTNAddonsSelect.length;idF++){
                     if(relevantFieldsForMobileCTNAddonsSelect[idF] != 'Recurring_price_0' && relevantFieldsForMobileCTNAddonsSelect[idF] != 'Network_Promo_Recurring_0' && relevantFieldsForMobileCTNAddonsSelect[idF] != 'Network_Promo_OneOff_0' && relevantFieldsForMobileCTNAddonsSelect[idF] != 'Addon_category_0'){
        				var key = "Mobile_CTN_addons_"+idx+':'+relevantFieldsForMobileCTNAddonsSelect[idF];
                        if(CS.getAttributeValue(key) != data[idx][relevantFieldsForMobileCTNAddonsSelect[idF]]){
                            generate = true;
    
                            console.log('Reset mobile addon generation because values have changed');
                            console.log(key);
                            console.log(CS.getAttributeValue(key));
                            console.log(data[idx][relevantFieldsForMobileCTNAddonsSelect[idF]]);
                            console.log('-------------------------------------');
                        }
                    }
                    else {
        			    if((relevantFieldsForMobileCTNAddonsSelect[idF] == 'Network_Promo_Recurring_0')||(relevantFieldsForMobileCTNAddonsSelect[idF] == 'Network_Promo_OneOff_0')){
        			        var key = "Mobile_CTN_addons_"+idx+':'+relevantFieldsForMobileCTNAddonsSelect[idF];
        			        if(CS.getAttributeValue(key) != data[idx][relevantFieldsForMobileCTNAddonsSelect[idF]]){
                                if((data[idx][relevantFieldsForMobileCTNAddonsSelect[idF]] == null) && CS.getAttributeValue(key)== 0){
                                    //ignore this difference
                                    console.log('Do not generate mobile addons - 0 equals null for Network Promo!!!');
                                }
                                else{
                                    generate = true;
                                    console.log('Reset mobile addon generation because not equal Network Promo!!!');
                                    console.log(key);
                                    console.log(CS.getAttributeValue(key));
                                    console.log(data[idx][relevantFieldsForMobileCTNAddonsSelect[idF]]);
                                    console.log('-------------------------------------');
                                }
                            }
        			    }
        			}
        		}
            
        	}
        }
    }
    else {
        generate = true;
    }
    return generate;
}

window.customAddRelatedProduct =	function customAddRelatedProduct(anchorRef) {
			var attr = CS.Service.config[anchorRef];
			var emptyPromise = Promise.resolve();
			var parent = CS.getConfigurationWrapper(CS.Util.getParentReference(attr.reference));
			var defId = CS.Service.getAvailableProducts(attr.reference)[0].cscfga__Product_Definition__c;
			loadProduct(defId)
		 .then(function() {
		  //CS.Service.addRelatedProduct(anchorAttributeReference, productDefinitionId, suppressRules) - returns a Promise object
		  return CS.Service.addRelatedProduct(attr.reference, defId, false);
		 })
		 .then(function () {
		  console.info('Created Configuration: ');
		  var index = CS.Service.config[anchorRef].relatedProducts.length-1;
		  setRightValues(getMapForFields());
		  // TODO After discussion with Nikola this needs to be uncommented and this attribute needs to be added for Mobile CTN.
		  // CS.setAttributeValue(anchorRef +':HirarchyLevel_0', 'Parent');

		  });
		}

window.addRelProductInlineOnlyUpdated = function addRelProductInlineOnlyUpdated(rel, num, lookupAttr){
        //var productId = "a4F9E0000009JlV";
        var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
        var relProductRef = rel;
        CS.Service.loadProduct(productId, function(){
        	CS.Service.loadProductTemplateHtml(productId, function() {
        		var productIndex = CS.Service.getProductIndex(productId);
    			jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
    			for(var x = 0;x<num;x++) {
    				customAddRelatedProduct(relProductRef);
                	//addRelProductInline(relProductRef);
           		}
           		//CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config['FixedFlatFeeLookup_0']);
           		//addRelProductInlineAndSetLookup('FlatFee_product_0', 'FlatFee_set_0', 'FixedFlatFeeLookup_0');
           		//CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config['FixedFlatFeeLookup_0']);
  				//CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config[lookupAttr]);
    		});
        }).then(function(){        	
           saveConfiguration(self);
        })
}


window.getNumberOfNewRelProducts = function getNumberOfNewRelProducts(){
    var numOfSubs = CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts.length;
    var mobileAddons = CS.Service.config["Mobile_addons_0"].attr.cscfga__Value__c;
	var numOfAddons = 0;
	if(mobileAddons!=''){
	    var mobileAddonsItems = mobileAddons.split(',');
	    numOfAddons = mobileAddonsItems.length;
	}
   
   var result = numOfAddons*numOfSubs;
   
   return result;
    
}


window.doAllThree = function doAllThree(rel){
    for(var x=0; x<3; x++){
	    addRelProductInlineOnly(rel);
	}
}
/*
window.addRelProductInlineOnly = function addRelProductInlineOnly(rel, num){
        //var productId = "a4F9E0000009JlV";
        var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
        var relProductRef = rel;
        CS.Service.loadProduct(productId, function(){
        	CS.Service.loadProductTemplateHtml(productId, function() {
        		var productIndex = CS.Service.getProductIndex(productId);
    			jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
    			for(var x = 0;x<num;x++){
                addRelProductInline(relProductRef);}
  
    		});
        }).then(function(){finish();})
}
*/


function setRightValues(data){
    var relevantFields = ['Addon_category_0','CTN_quantity_0','Connection_type_0', 'Duration_0', 'Price_item_0', 'Profile_Id_0', 'Recurring_price_0', 'Scenario_0', 'Start_month_0', 'Subscription_0', 'Network_Promo_OneOff_0', 'Network_Promo_Recurring_0', 'Originating_subscription_0'];
    var relevantFieldsForMobileCTNAddonsSelect = ['Addon_hardware_commission_0', 'Network_Promo_Recurring_0','Network_Promo_OneOff_0'];

	if(CS.Service.config["Mobile_CTN_addons_0"].relatedProducts.length > 0){
		console.log('Has products');
		for(var idx=0;idx<data.length;idx++){
			for(var idF=0;idF<relevantFields.length;idF++){
				var key = "Mobile_CTN_addons_"+idx+':'+relevantFields[idF];
				CS.setAttributeValue(key, data[idx][relevantFields[idF]]);
			}

            for(var idF=0;idF<relevantFieldsForMobileCTNAddonsSelect.length;idF++){
				var key = "Mobile_CTN_addons_"+idx+':'+relevantFieldsForMobileCTNAddonsSelect[idF];
				CS.setAttributeValue(key, data[idx][relevantFieldsForMobileCTNAddonsSelect[idF]]);
			}
            
            var keyForLookup = 'Mobile_CTN_addons_'+idx+':Price_item_0';
            var keyForSearch = CS.getAttributeValue(keyForLookup);
            
            var addonToSet = getLookupVal(keyForSearch, 'mobile_add_on_category__c');
            CS.setAttributeValue('Mobile_CTN_addons_'+idx+':Addon_category_0');
            
            var recPrice = getLookupVal(keyForSearch, 'cspmb__recurring_charge__c');//CS.getAttributeValue('Mobile_CTN_addons_'+idx+':Recurring_price_0');
            
            var ctnQuantity = CS.getAttributeValue('Mobile_CTN_addons_'+idx+':CTN_quantity_0');
            var priceVal = 0;//CS.getAttributeValue('Mobile_CTN_addons_'+idx+':Recurring_price_0') * CS.getAttributeValue('Mobile_CTN_addons_'+idx+':CTN_quantity_0');
            
            if(ctnQuantity!='' && recPrice!=''){
                priceVal = recPrice*ctnQuantity;
            }
            CS.setAttributeField('Mobile_CTN_addons_'+idx+':Recurring_price_0','price',priceVal);
            CS.setAttributeValue('Mobile_CTN_addons_'+idx+':Recurring_price_0',recPrice);
            
            
            var description = getLookupVal(keyForSearch, 'name')+' '+CS.getAttributeValue('Mobile_CTN_addons_'+idx+':Connection_type_0');
            CS.setAttributeField('Mobile_CTN_addons_'+idx+':Recurring_price_0','lineitemdescription',description);
            
            
		}
	}
}


window.getLookupVal = function getLookupVal(idLookupItem, fieldName){
    var result = CS.lookupRecords[idLookupItem][fieldName];
    return result;
}

function getMapForFields(){
	var mobileAddons = CS.Service.config["Mobile_addons_0"].attr.cscfga__Value__c;
	var mobileAddonsItems = mobileAddons.split(',');

	var mobileSubscriptions = [];

	var relevantFields = ['Addon_category_0','CTN_quantity_0','Connection_type_0', 'Duration_0', 'Price_item_0', 'Profile_Id_0', 'Recurring_price_0', 'Scenario_0', 'Start_month_0', 'Subscription_0'];
	var relevantFieldsForMobileCTNAddonsSelect = ['Addon_hardware_commission_0'];

	if(CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts.length > 0){
		_.each(CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts,function(p){
			  console.log(p.reference);
			  for(var idx=0;idx<mobileAddonsItems.length;idx++){
			  if(mobileAddonsItems[idx]){
				  var mobileSubscription = {};
				  	for(var idF in relevantFields){
				  		var key = p.reference+':'+relevantFields[idF];
				  		if(CS.Service.config[key]){
				  			mobileSubscription[relevantFields[idF]] = CS.Service.config[key].attr.cscfga__Value__c;
				  			
				  			//quick fix for Addon hardware commission
				  			if(CS.Service.config['Mobile_CTN_addon_select_'+idx+':Addon_hardware_commission_0'] !== undefined && CS.Service.config['Mobile_CTN_addon_select_'+idx+':Addon_hardware_commission_0'].attr !== undefined){
				  			    mobileSubscription['Addon_hardware_commission_0'] = CS.Service.config['Mobile_CTN_addon_select_'+idx+':Addon_hardware_commission_0'].attr.cscfga__Value__c;
				  			}
				  			//
				  			
					  		if(relevantFields[idF] == 'Price_item_0'){
					  			mobileSubscription[relevantFields[idF]] = mobileAddonsItems[idx];
					  		}
				  		}
					  }
					// effectively we're saving position of the subscription in the list  
				  	mobileSubscription['Originating_subscription_0'] = p.reference;
				  	mobileSubscriptions.push(mobileSubscription);
				}
			}
		});
	}


	return mobileSubscriptions;
}




//last v working
/*
window.addRelProductInlineOnly = function addRelProductInlineOnly(rel, num){
        //var productId = "a4F9E0000009JlV";
        var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
        var relProductRef = rel;
        CS.Service.loadProduct(productId, function(){
        	CS.Service.loadProductTemplateHtml(productId, function() {
        		var productIndex = CS.Service.getProductIndex(productId);
    			jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
    			for(var x = 0;x<num;x++){
                addRelProductInline(relProductRef);}
  
    		});
        }).then(function(){saveConfiguration(self);})
}
*/
window.addRelProductInlineOnly = function addRelProductInlineOnly(rel, num){
        //var productId = "a4F9E0000009JlV";
        var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
        var relProductRef = rel;
        CS.Service.loadProduct(productId, function(){
        	CS.Service.loadProductTemplateHtml(productId, function() {
        		var productIndex = CS.Service.getProductIndex(productId);
    			jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(productIndex));
    			for(var x = 0;x<num;x++){
    			customAddRelatedProduct(relProductRef);
                //addRelProductInline(relProductRef);
            	}
    		});
        }).then(function(){
            //var data = getMapForFields();
            //console.log('Got values');
            //setRightValues(data);
            //console.log('Set values');
            //console.log('Before save');
            saveConfiguration(self);
            
        })
}