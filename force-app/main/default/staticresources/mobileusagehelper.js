//__________________________________________________________________________________________________________________________________________
// start stuff moved from productdefinitionhelper

window.hideForIndirect = function hideForIndirect() {
	var typeOfSalesChannel = CS.getAttributeValue('DirectIndirect_0');

	if (typeOfSalesChannel == 'Indirect') {
		jQuery("div[data-cs-binding='Mobile_CTN_hardware_0']").hide();

	}
	// hide always according to W-002179 (temporary hidding 25/10/2019)
	jQuery("div[data-cs-binding='Mobile_CTN_loyalty_fee_0']").hide();
}

function hideSectionsHardwareAndLoyalty() {
	jQuery("div[data-cs-binding='Mobile_CTN_hardware_0']").hide();
	jQuery("div[data-cs-binding='Mobile_CTN_loyalty_fee_0']").hide();

}

var mobileBANScenario = "";
var mobileCTNProfileScenario = "";
/***
* 
* Mobile product rules
* 
*/

var mobileConstants = {
	"AllesInOpReisWereld": "Alles-in-1 op reis Wereld",
	"ExtraInternetEuropa": "Extra Internet Europa",
	"AllesInOpReisWereldDaily": "Alles-in-1 op reis Wereld Daily",
	"RowWorryFree": "RoW shared data and worry-free voice bundle"


};

var redProSubscription = {
	"Connect": "Connect subscription",
	"Select": "Select subscription",
	"Elite": "Elite subscription",
	"Basic": "Basic subscription"

};

var redProScenarios = {
	"NewPorting": "New-Porting RedPro",
	"RetentionMigration": "Retention-Migration RedPro"
}

var mobileErrorMessages =
{
	"euDataAccountBundle_none": "Please add one EU data account bundle to your OneBusiness configuration",
	"euDataAccountBundle_moreThanOne": "Only one EU data account bundle is allowed per OneBusiness configuration",
	"euDataAccountBundleFlex_none": "Please add one EU data account bundle to your configuration",
	"euDataAccountBundleFlex_moreThanOne": "Only one EU data account bundle per configuration is allowed",
	"euDataAccountBundleRoW_none": "To offer a RoW data account bundle, please add one EU data account bundle to your OneBusiness configuration first.",
	"euDataAccountBundleRoW_moreThanOne": "Only one RoW data account bundle is allowed per OneBusiness configuration",
	"IOTeuDataAccountBundle_none": "Please add one IOT EU data account bundle to your OneBusiness IOT configuration",
	"IOTeuDataAccountBundle_moreThanOne": "Only one IOT EU data account bundle is allowed per OneBusiness IOT configuration",
	"IOTeuDataAccountBundleRoW_none": "To offer an IOT RoW data account bundle, please add one IOT EU data account bundle to your OneBusiness IOT configuration first.",
	"IOTeuDataAccountBundleRoW_moreThanOne": "Only one IOT RoW data account bundle is allowed per OneBusiness IOT configuration.",
	"AllesInOpReis": "Only one Alles-in-1 op reis Wereld Daily bundle is allowed on the DataOnly, RedPro configuration. Please remove option(s) from your configuration accordingly",
	"AllesInOpReis_notAllowed": "A single Alles-in-1 op reis Wereld Daily bundle is allowed in the DataOnly, RedPro Connect, Select and Elite subscription only. Please remove it.",
	"rowDataAccountBundle_moreThenOne": "Only one RoW data account bundle is allowed per OneBusiness configuration",
	"extraInternetEuropa": "Only one Extra Internet Europa add-on is allowed on the RedPro and DataOnly configurations. Please remove option(s) from your configuration accordingly",
	"consumeProductErrorMessage": "A single Consumption product is allowed on the OneMobile configuration. Please remove option(s) from your configuration accordingly.",
	"singleToestelbetalingErrorMessage": "A single Toestelbetaling option is allowed on the RedPro configuration. Please remove option(s) from your configuration accordingly",
	"singleGarantAddonErrorMessage": "A single Garant add-on is allowed on this configuration. Please remove option(s) from your configuration accordingly.",
	"multiSim2deNeedsMultiSim1ste": "To offer MultiSim 2de aansluiting, please add one MultiSim 1ste aansluiting to your RedPro configuration first.",
	"rowSharedDataNeedsEuSharedData": "To offer a RoW shared data and worry-free voice bundle, please add one EU shared data and worry-free voice bundle to your OneMobile configuration first.",
	"dataOnlyAndMBBGroupBundlesNotAtSameTimeOneBusiness": "Data only subscriptions and MBB Group bundles cannot be added simultaneously to the same OneBusiness configuration. Please remove one of these add-ons from your configuration.",
	"dataOnlyAndMBBGroupBundlesNotAtSameTimeOneMobile": "Data only subscriptions and MBB Group bundles cannot be added simultaneously to the same OneMobile configuration. Please remove one of these add-ons from your configuration.",
	"OneMobileROWAllesDaily": "The RoW shared data and worry-free voice bundle and the Alles-in-1 op reis Wereld Daily dagbundle cannot be added simultaneously to the same OneMobile configuration. Please remove one of these add-ons from your configuration",
	"consumeProductDataOnlyErrorMessage": "To use consumption products, please add OneMobile Data only to your OneMobile configuration first.",

	"RedProOnly_BeperktInternet": "You cannot add Beperkt Internet when you have Data Bundel or Groepsbundel.",
	"RedProOnly_GroepsbundelAndDataBundel": "You are not allowed to add Data Bundel and Groepsbundel in the same configuration. Please remove the Data Bundel or the Groepsbundel to proceed.",
	"RedProOnly_DubbeleDataDataBundelGroepsbundel": "You can only have a Dubbele Data on Data Bundel or Groepsbundel.",
	"RedProOnly_OneDataBundel": "You can only add one Data Bundel addon",
	"RedProOnly_OneGroepsbundel": "You can only add one Groepsbundel addon",
	"RedProOnly_DubbeleData": "You can only add one Dubbele Data addon"


};

var mobileProductTypes = {
	"MobileAddonSelect": "Mobile_CTN_addon_select_0"
}

var mobileScenario = {
	"RedPro": "RedPro"
}

var mobileAddonCategories = {
	"DataBundel": "Data Bundel",
	"Groepsbundel": "Groepsbundel",
	"DubbeleData": "Dubbele Data",
	"BeperktInternet": "Beperkt Internet"


}

var mobileSubscription = {
	"Elite": "Elite subscription",
	"Connect": "Connect subscription",
	"Select": "Select subscription"

}

var mobileAttribute = {
	"Subscription": "Subscription_0",
	"Category": "Category_0"
}


window.redProOnlyRules = function redProOnlyRules() {
	var mobileAddonsPerCategories = getMobileAddonSelectPerCategory(mobileProductTypes.MobileAddonSelect, mobileAttribute.Category);

	//Requirement 1 -> 
	//"RedProOnly_OneDataBundel" : "You can only add one Data Bundel addon",
	//"RedProOnly_OneGroepsbundel" : "You can only add one Groepsbundel addon"
	if (mobileAddonsPerCategories && mobileAddonsPerCategories[mobileAddonCategories.DataBundel]) {
		if (mobileAddonsPerCategories[mobileAddonCategories.DataBundel].quantity > 1) {
			CS.markConfigurationInvalid(mobileErrorMessages.RedProOnly_OneDataBundel);
		}
	}
	if (mobileAddonsPerCategories && mobileAddonsPerCategories[mobileAddonCategories.Groepsbundel]) {
		if (mobileAddonsPerCategories[mobileAddonCategories.Groepsbundel].quantity > 1) {
			CS.markConfigurationInvalid(mobileErrorMessages.RedProOnly_OneGroepsbundel);
		}
	}

	if (mobileAddonsPerCategories && mobileAddonsPerCategories[mobileAddonCategories.DubbeleData]) {
		if (mobileAddonsPerCategories[mobileAddonCategories.DubbeleData].quantity > 1) {
			CS.markConfigurationInvalid(mobileErrorMessages.RedProOnly_DubbeleData);
		}
	}

	//Requirement 2 -> 
	// "RedProOnly_DubbeleDataDataBundelGroepsbundel":"You can only have a Dubbele Data on Data Bundel or Groepsbundel."
	//You can only have a Dubbele Data on Data Bundel or Groepsbundel.
	if (mobileAddonsPerCategories && mobileAddonsPerCategories[mobileAddonCategories.DubbeleData]) {
		if (!mobileAddonsPerCategories[mobileAddonCategories.DataBundel] && !mobileAddonsPerCategories[mobileAddonCategories.Groepsbundel]) {
			CS.markConfigurationInvalid(mobileErrorMessages.RedProOnly_DubbeleDataDataBundelGroepsbundel);
		}
	}


	//Requirement 3 ->
	//"RedProOnly_GroepsbundelAndDataBundel" : "You are not allowed to add Data Bundel and Groepsbundel in the same configuration. Please remove the Data Bundel or the Groepsbundel to proceed.",
	//You are not allowed to add Data Bundel and Groepsbundel in the same configuration. Please remove the Data Bundel or the Groepsbundel to proceed.
	if (mobileAddonsPerCategories && mobileAddonsPerCategories[mobileAddonCategories.Groepsbundel] && mobileAddonsPerCategories[mobileAddonCategories.DataBundel]) {
		CS.markConfigurationInvalid(mobileErrorMessages.RedProOnly_GroepsbundelAndDataBundel);
	}

	//Requirement 4 -> 
	//"RedProOnly_BeperktInternet" : "You cannot add Beperkt Internet when you have Data Bundel or Groepsbundel.",
	if (mobileAddonsPerCategories && mobileAddonsPerCategories[mobileAddonCategories.BeperktInternet]) {
		if (mobileAddonsPerCategories[mobileAddonCategories.DataBundel] || mobileAddonsPerCategories[mobileAddonCategories.Groepsbundel]) {
			CS.markConfigurationInvalid(mobileErrorMessages.RedProOnly_BeperktInternet);
		}
	}


}

window.getMobileAddonSelectPerCategory = function getMobileAddonSelectPerCategory(mobileProductType, mobileAddonCategoryAttribute) {
	var results = {};

	for (var x in CS.Service.config[mobileProductType].relatedProducts) {
		var itm = CS.Service.config[mobileProductType].relatedProducts[x];
		if (itm.reference) {

			var category = CS.getAttributeValue(itm.reference + ':' + mobileAddonCategoryAttribute);

			if (results[category]) {
				results[category].quantity += 1;
			}
			else {

				results[category] = {};
				results[category].quantity = 1;
			}
		}
	}

	return results;
}


function restrictAllesInOneDailyRowSharedDataAndWorryFreeVoiceBundle() {
	var scenarioOneMobile = CS.getAttributeValue('Scenario_0');
	if (scenarioOneMobile == 'OneMobile' || scenarioOneMobile == 'OneMobile4') {
		var numberOfRowSharedData = getNumberOfMobileAddonsPerName(mobileConstants.RowWorryFree);
		var numberOfAllesInOneDaily = getNumberOfMobileAddonsPerName(mobileConstants.AllesInOpReisWereldDaily);
		if (numberOfRowSharedData > 0) {
			if (numberOfAllesInOneDaily > 0) {
				CS.markConfigurationInvalid(mobileErrorMessages.OneMobileROWAllesDaily);
			}
		}
	}
}

window.getNumberOfMobileAddonsPerCategoryPDH = function getNumberOfMobileAddonsPerCategoryPDH(category) {
	var addonsAllIds = CS.getAttributeValue("Mobile_addons_0");
	if (addonsAllIds == '') {
		return 0;
	} else {
		var eachAddonId = addonsAllIds.split(",");
		var count = 0;
		for (var i = 0; i < eachAddonId.length; i++) {
			if (CS.lookupRecords[eachAddonId[i]]['mobile_add_on_category__c'] == category) {
				count++;
			}
		}
		return count;
	}
}

function restrictGarants() {
	var numberOfAddonsPerGarantCategory = getNumberOfMobileAddonsPerCategoryPDH('Garant');

	if (numberOfAddonsPerGarantCategory > 1) {
		CS.markConfigurationInvalid(mobileErrorMessages.singleGarantAddonErrorMessage);
	}
}

function restrictMultiSim2De() {
	var numberOfMultiSim1Ste = getNumberOfMobileAddonsPerCategoryPDH('MultiSim 1ste aansluiting');
	var numberOfMultiSim2De = getNumberOfMobileAddonsPerCategoryPDH('MultiSim 2de aansluiting');

	if (numberOfMultiSim1Ste === 0) {
		if (numberOfMultiSim2De == 1) {
			CS.markConfigurationInvalid(mobileErrorMessages.multiSim2deNeedsMultiSim1ste);
		}
	}
}

function restrictRowSharedDataAndWorryFreeVoiceBundle() {
	var numberOfRowSharedData = getNumberOfMobileAddonsPerCategoryPDH('RoW shared data bundle');
	var numberOfEuSharedData = getNumberOfMobileAddonsPerCategoryPDH('EU shared data bundle');

	if (numberOfRowSharedData > 0) {
		if (numberOfEuSharedData === 0) {
			CS.markConfigurationInvalid(mobileErrorMessages.rowSharedDataNeedsEuSharedData);
		}
	}
}

function restrictToOnlyOneEuDataAndWorryFree() {
	var numberOfEuSharedData = getNumberOfMobileAddonsPerCategoryPDH('EU shared data bundle');

	if (numberOfEuSharedData > 1) {
		CS.markConfigurationInvalid('Only one EU shared data bundle allowed.');
	}
}

function restrictToOnlyOneRowDataAndWorryFree() {
	var numberOfRowSharedData = getNumberOfMobileAddonsPerCategoryPDH('RoW shared data bundle');

	if (numberOfRowSharedData > 1) {
		CS.markConfigurationInvalid('Only one RoW shared data bundle allowed.');
	}
}

function restrictAddingOfMobileBroadbandWithOneBusinessData() {
	if (CS.getAttributeValue('Scenario_0') == "OneBusiness") {
		var numberOfOneBussinessDataAddons = getNumberOfMobileAddonsPerName('OneBusiness Data only');
		var numberOfMobileBroadband = getNumberOfMobileAddonsPerName('Mobile Broadband');

		if (numberOfOneBussinessDataAddons > 1) {
			if (numberOfMobileBroadband > 1) {
				CS.markConfigurationInvalid(mobileErrorMessages.dataOnlyAndMBBGroupBundlesNotAtSameTimeOneBusiness);
			}
		}
	}
}

function restrictAddingOfMobileBroadbandWithOneMobileData() {
	if (CS.getAttributeValue('Scenario_0') == "OneMobile" || CS.getAttributeValue('Scenario_0') == "OneMobile4") {
		var numberOfOneBussinessDataAddons = getNumberOfMobileAddonsPerName('OneMobile Data only');
		var numberOfMobileBroadband = getNumberOfMobileAddonsPerName('Mobile Broadband');

		if (numberOfOneBussinessDataAddons > 1) {
			if (numberOfMobileBroadband > 1) {
				CS.markConfigurationInvalid(mobileErrorMessages.dataOnlyAndMBBGroupBundlesNotAtSameTimeOneMobile);
			}
		}
	}
}

function getNumberOfAddons() {
	var addonsAllIds = CS.getAttributeValue("Mobile_addons_0");
	if (addonsAllIds === '') {
		return 0;
	} else {
		var eachAddonId = addonsAllIds.split(",");
		return eachAddonId.length;
	}
}

function getNumberOfMobileAddonsPerName(name) {
	var addonsAllIds = CS.getAttributeValue("Mobile_addons_0");
	if (addonsAllIds === '') {
		return 0;
	} else {
		var eachAddonId = addonsAllIds.split(",");
		var count = 0;
		for (var i = 0; i < eachAddonId.length; i++) {
			if (CS.lookupRecords[eachAddonId[i]]['name'] == name) {
				count++;
			}
		}
		return count;
	}
}

function getNumberOfMobileAddonsPerNameContains(name) {
	var addonsAllIds = CS.getAttributeValue("Mobile_addons_0");
	if (addonsAllIds === '') {
		return 0;
	} else {
		var eachAddonId = addonsAllIds.split(",");
		var count = 0;
		for (var i = 0; i < eachAddonId.length; i++) {
			if (name.indexOf(CS.lookupRecords[eachAddonId[i]].name)) {
				count++;
			}
		}
		return count;
	}
}

function getNumberOfMobileAddonsPerNameAndCategory(name, category) {
	var addonsAllIds = CS.getAttributeValue("Mobile_addons_0");
	if (addonsAllIds === '') {
		return 0;
	} else {
		var eachAddonId = addonsAllIds.split(",");
		var count = 0;
		for (var i = 0; i < eachAddonId.length; i++) {
			if (CS.lookupRecords[eachAddonId[i]]['mobile_add_on_category__c'] == category || category == '*') {
				if (CS.lookupRecords[eachAddonId[i]]['name'] == name || category == '*') {
					count++;
				}
			}
		}
		return count;
	}
}

function checkIfThereAreMoreThenOne(name, category, errorMessage) {
	var numberOfItems = getNumberOfMobileAddonsPerNameAndCategory(name, category);

	if (numberOfItems > 1) {
		CS.markConfigurationInvalid(errorMessage);
	}
}

function checkIfThereAreNone(name, category, errorMessage) {
	var numberOfItems = getNumberOfMobileAddonsPerNameAndCategory(name, category);

	if (numberOfItems == 1) {
		CS.markConfigurationInvalid(errorMessage);
	}
}

function checkIfThereIsMoreThenOneRowBundle() {
	checkIfThereAreMoreThenOne('*', 'RoW data account bundle', mobileErrorMessages.rowDataAccountBundle_moreThenOne);
}

function euAccountBundleOneBusinessBAN() {
	//checkMobileBanScenarioAndRemoveData();

	//only for One Business scenario
	var mobileBANScenario = CS.getAttributeValue('Scenario_0');
	if (mobileBANScenario == 'OneBusiness Customer Level') {

		var directIndirect = CS.getAttributeValue('DirectIndirect_0');

		if (directIndirect === 'Direct') {
			var numberOfEUAccount = mobileErrorMessagesJustOneRequired("Mobile_BAN_product_0", "Addon_category_0", "*Subscription*", mobileErrorMessages.euDataAccountBundle_none, mobileErrorMessages.euDataAccountBundle_moreThanOne, "Subscription_0", "EU data account bundle");
		}

		var numberOfRoW = mobileErrorMessagesJustOne("Mobile_BAN_product_0", "Addon_category_0", "RoW data account bundle", mobileErrorMessages.euDataAccountBundleRoW_none, mobileErrorMessages.euDataAccountBundleRoW_moreThanOne);

		if (numberOfRoW > 0 && numberOfEUAccount == '0') {
			CS.markConfigurationInvalid(mobileErrorMessages.euDataAccountBundleRoW_none);
		}

	}
	else if (mobileBANScenario == 'OneBusiness IOT Customer Level') {

		var numberOfEUAccountIOT = mobileErrorMessagesJustOneRequired("Mobile_BAN_product_0", "Addon_category_0", "*Subscription*", mobileErrorMessages.IOTeuDataAccountBundle_none, mobileErrorMessages.IOTeuDataAccountBundle_moreThanOne, "Subscription_0", "EU data account bundle");
		var numberOfRoWIOT = mobileErrorMessagesJustOne("Mobile_BAN_product_0", "Addon_category_0", "IOT RoW data account bundle", mobileErrorMessages.IOTeuDataAccountBundleRoW_none, mobileErrorMessages.IOTeuDataAccountBundleRoW_moreThanOne);

		if (numberOfRoWIOT > 0 && numberOfEUAccountIOT == '0') {
			CS.markConfigurationInvalid(mobileErrorMessages.IOTeuDataAccountBundleRoW_none);
		}

	}
	else if (mobileBANScenario == 'Flex Mobile Customer Level') {

		mobileErrorMessagesJustOneRequired("Mobile_BAN_product_0", "Addon_category_0", "*Subscription*", mobileErrorMessages.euDataAccountBundleFlex_none, mobileErrorMessages.euDataAccountBundleFlex_moreThanOne, "Subscription_0", "EU data account bundle");

	}

}
function checkMobileBanScenarioAndRemoveData() {
	var scenario = CS.getAttributeValue('Scenario_0');
	if (scenario != mobileBANScenario) {
		//remove all Mobile_BAN_product_0
		removeRelatedProduct("Mobile_BAN_product_0");
		mobileBANScenario = scenario;
	}
}


function checkMobileCTNProfileScenarioAndRemoveData() {
	var scenario = CS.getAttributeValue('Scenario_0');
	if (scenario != mobileCTNProfileScenario) {
		//remove all Mobile_BAN_product_0
		removeRelatedProduct("Mobile_CTN_subscription_0");
		CS.setAttributeValue('Mobile_addons_0', '')
		mobileCTNProfileScenario = scenario;
	}
}

//error message if NONE or if more than 1
function mobileErrorMessagesJustOneRequired(relAttrRef, attrRef, value, errorIfNone, errorIfMoreThanOne, additionalAttribute, additionalAttributeValue) {
	//only for One Business scenario
	var showErrorNeedsToBeOne = true;
	var moreThanOneEU = 0;
	if (CS.Service.config[relAttrRef].relatedProducts.length > 0) {
		for (var idx in CS.Service.config[relAttrRef].relatedProducts) {
			var key = CS.Service.config[relAttrRef].relatedProducts[idx].reference + ':' + attrRef;
			if (CS.Service.config[key] && (CS.getAttributeValue(key) == value)) {
				var additionalAttribute = additionalAttribute || '';
				var additionalAttributeValue = additionalAttributeValue || '';

				if (additionalAttribute != '' && additionalAttributeValue != '') {
					var keyAdditional = CS.Service.config[relAttrRef].relatedProducts[idx].reference + ':' + additionalAttribute;
					if (CS.Service.config[keyAdditional] && (CS.getAttributeValue(keyAdditional) == additionalAttributeValue)) {
						showErrorNeedsToBeOne = false;
						moreThanOneEU = moreThanOneEU + 1;
					}
				}
				else {
					showErrorNeedsToBeOne = false;
					moreThanOneEU = moreThanOneEU + 1;
				}
			}
		}
	}

	if (showErrorNeedsToBeOne) {
		CS.markConfigurationInvalid(errorIfNone);
	}
	if (moreThanOneEU > 1) {
		CS.markConfigurationInvalid(errorIfMoreThanOne);
	}

	return moreThanOneEU;
}

//error message if more than 1
function mobileErrorMessagesJustOne(relAttrRef, attrRef, value, errorIfNone, errorIfMoreThanOne, additionalAttribute, additionalAttributeValue) {
	var moreThanOneEU = 0;
	if (CS.Service.config[relAttrRef].relatedProducts.length > 0) {
		for (var idx in CS.Service.config[relAttrRef].relatedProducts) {
			var key = CS.Service.config[relAttrRef].relatedProducts[idx].reference + ':' + attrRef;
			if (CS.Service.config[key] && (CS.getAttributeValue(key) == value)) {
				var additionalAttribute = additionalAttribute || '';
				var additionalAttributeValue = additionalAttributeValue || '';

				if (additionalAttribute != '' && additionalAttributeValue != '') {
					var keyAdditional = CS.Service.config[relAttrRef].relatedProducts[idx].reference + ':' + additionalAttribute;
					if (CS.Service.config[keyAdditional] && (CS.getAttributeValue(keyAdditional) == additionalAttributeValue)) {
						moreThanOneEU = moreThanOneEU + 1;
					}
				}
				else {
					moreThanOneEU = moreThanOneEU + 1;
				}
			}
		}
	}
	if (moreThanOneEU > 1) {
		CS.markConfigurationInvalid(errorIfMoreThanOne);
	}

	return moreThanOneEU;

}

//CTN

function ctnAddonRules() {
	//checkMobileCTNProfileScenarioAndRemoveData();
	extraInternetEuropa();
	allesInOpReis();
	//consumeProductEuSharedDataBundle();
	singleToestelbetaling();

	restrictGarants();
	restrictMultiSim2De();
	if (CS.getAttributeValue('Scenario_0') != 'Flex Mobile')
		restrictRowSharedDataAndWorryFreeVoiceBundle();
	restrictAllesInOneDailyRowSharedDataAndWorryFreeVoiceBundle();
	if (CS.getAttributeValue('Scenario_0') != 'Flex Mobile')
		restrictRowSharedDataAndWorryFreeVoiceBundle();
	restrictAddingOfMobileBroadbandWithOneBusinessData();
	restrictToOnlyOneEuDataAndWorryFree();
	restrictToOnlyOneRowDataAndWorryFree();
	//restrictAddingOfMobileBroadbandWithOneMobileData(); Removed after talk with Abdul
}

function getMobileAddonsPerAddonCategory(category) {
	var addonsAllIds = CS.getAttributeValue("Mobile_addons_0");

	var itemsPerCategory = [];
	var eachAddonId = addonsAllIds.split(",");
	var count = 0;
	if (addonsAllIds == '') {
		return null;
	} else {
		var eachAddonId = addonsAllIds.split(",");

		for (var i = 0; i < eachAddonId.length; i++) {
			if (CS.lookupRecords[eachAddonId[i]]['mobile_add_on_category__c'].trim() == category.trim()) {
				itemsPerCategory.push(CS.lookupRecords[eachAddonId[i]]);
				console.log('Equal - '); console.log(CS.lookupRecords[eachAddonId[i]]);



			} else {
				console.log('not equal -');
				console.log(CS.lookupRecords[eachAddonId[i]].mobile_add_on_category__c.trim());
				console.log(category.trim());
			}
		}
		return itemsPerCategory;
	}

}


//Alles-in-op reis Wereld Daily bundle - RedPro Connect, Select and Elite subscription
function allesInOpReis() {
	var scenario = CS.getAttributeValue('Scenario_0');
	var subscription = CS.getAttributeValue('Subscription_0');
	
	if ((scenario == 'Data only') || ((scenario == redProScenarios.NewPorting || scenario == redProScenarios.RetentionMigration) && ((subscription == redProSubscription.Connect) || (subscription == redProSubscription.Select) || (subscription == redProSubscription.Elite)))) {
		var currentAllesAddons = getMobileAddonsPerAddonCategory(mobileConstants.AllesInOpReisWereld);

		if (currentAllesAddons != null && currentAllesAddons.length > 1) {
			CS.markConfigurationInvalid(mobileErrorMessages.AllesInOpReis);
		}


	}
	if (scenario && !(scenario == redProScenarios.NewPorting || scenario == redProScenarios.RetentionMigration) && scenario != 'Data only') {
		var currentAllesAddons = getMobileAddonsPerAddonCategory(mobileConstants.AllesInOpReisWereld);
		if (currentAllesAddons != null && currentAllesAddons.length > 0) {
			CS.markConfigurationInvalid(mobileErrorMessages.AllesInOpReis_notAllowed);
		}
		//show error message since it shouldn't be there
	}
}

function extraInternetEuropa() {
	var scenario = CS.getAttributeValue('Scenario_0');
	var subscription = CS.getAttributeValue('Subscription_0');

	if (scenario == redProScenarios.NewPorting || scenario == redProScenarios.RetentionMigration || scenario == 'Data only') {
		var currentAllesAddons = getMobileAddonsPerAddonCategory(mobileConstants.ExtraInternetEuropa);
		if (currentAllesAddons != null) {
			if (currentAllesAddons.length > 1) {
				CS.markConfigurationInvalid(mobileErrorMessages.extraInternetEuropa);
			}
		}
	}
	else {
		var currentAllesAddons = getMobileAddonsPerAddonCategory(mobileConstants.ExtraInternetEuropa);
		if (currentAllesAddons != null && currentAllesAddons.length > 0) {
			CS.markConfigurationInvalid(mobileErrorMessages.extraInternetEuropa);
		}
		//show error message since it shouldn't be there
	}
}

//only one Consume product Addon in combination with EU shared Data bundle
function consumeProductEuSharedDataBundle() {
	var scenario = CS.getAttributeValue('Scenario_0');

	if (scenario == 'OneMobile' || scenario == 'OneMobile4') {
		var numOfConsumeProducts = getNumberOfMobileAddonsPerCategoryPDH('Consuming products');

		if (numOfConsumeProducts == 1) {
			var numOfEuSharedDataBundle = getNumberOfMobileAddonsPerCategoryPDH('EU shared data bundle');
			var numOfOneMobileDataOnly = getNumberOfMobileAddonsPerCategoryPDH('OneMobile Data Only');

			if (numOfEuSharedDataBundle == 0) {
				CS.markConfigurationInvalid(mobileErrorMessages.consumeProductErrorMessage);
			}

			if (numOfOneMobileDataOnly == 0) {
				CS.markConfigurationInvalid(mobileErrorMessages.consumeProductDataOnlyErrorMessage);
			}
		} else if (numOfConsumeProducts > 1) {
			CS.markConfigurationInvalid(mobileErrorMessages.consumeProductErrorMessage);
		}
	}
}

//only one Toestelbetaling option is available
function singleToestelbetaling() {
	var scenario = CS.getAttributeValue('Scenario_0');

	if (scenario == redProScenarios.NewPorting || scenario == redProScenarios.RetentionMigration) {
		var numOfToestelbetaling = getNumberOfMobileAddonsPerCategoryPDH('Toestelbetaling');

		if (numOfToestelbetaling > 1) {
			CS.markConfigurationInvalid(mobileErrorMessages.singleToestelbetalingErrorMessage);
		}
	}
}


// end stuff moved from productdefinitionhelper
//__________________________________________________________________________________________________________________________________________

/**
 * Flag that controlls if console logs will be shown or not.
 */
var logsEnabled = false;

var allTableInformation = {};

var ctnQuantitiesToRemove = [];

function addOptionToSelect(selectName, optionValue, optionText) {
	jQuery('[name="' + selectName + '"]').append(jQuery('<option>', {
		value: optionValue,
		text: optionText
	}));
}

function showAllTables() {
	showTroubleshootingTable(true);
	showAllAddonsTroubleshootingTable();
}

/**
 * Shows general information custom table.
 */
function showTroubleshootingTable(showTable) {

	jQuery('#troubleshootingInformation').remove();

	var troubleshootingTable = ''
		+ '<div id="troubleshootingInformation">'
		+ '<div class="slds-box" style="overflow-x:auto;max-width:1400px;">'
		+ '<table class="list" id="troubleshootingTable" style="text-align:left;">'
		+ '</table>'
		+ '</div>'
		+ '</div>';

	troubleshootingTable = jQuery(troubleshootingTable);

	if (jQuery('#troubleshootingTable')[0] == undefined) {
		jQuery('[data-cs-binding=UsageTable_0]').parent().parent().prepend(troubleshootingTable);
	}

	var tableDuration = parseInt(CS.getAttributeValue('Duration_0')) + 1;

	var troubleshootingTableInformationArray = [];

	var durationAdded = 0;

	var sumOfOpeningBase = 0;
	var sumOfRetentions = 0;
	var sumOfNewConnections = 0;
	var sumOfDisconnections = 0;
	var sumOfMigrations = 0;
	var sumOfClosingBase = 0;
	var averageBase = 0;
	var sumOfLoyaltyFeePerCTN = 0;
	var sumOfLoyaltyFeeTotal = 0;

	currentInformation = {
		_openingBase: 0,
		_retentions: 0,
		_newConnections: 0,
		_disconnections: 0,
		_migration: 0,
		_closingBase: 0,
		_averageBase: 0,
		_loyaltyFeePerCTN: 0,
		_loyaltyFeeTotal: 0,
		_removeCTN: 0
	}

	for (i = 1; i <= tableDuration; i++) {

		currentInformation._migration = getMobileSubscriptionInformation(i, 'Migration');

		currentInformation._retentions = getMobileSubscriptionInformation(i, 'Retention');

		currentInformation._newConnections = getMobileSubscriptionInformation(i, 'New') + getMobileSubscriptionInformation(i, 'Porting') + currentInformation._migration;

		ctnQuantitiesToRemove[i + CS.getAttributeValue('Duration_0')] = currentInformation._newConnections + currentInformation._retentions;

		if (ctnQuantitiesToRemove[i] != null) {
			currentInformation._removeCTN = ctnQuantitiesToRemove[i];
		}

		currentInformation._disconnections = getMobileSubscriptionInformation(i, 'Disconnect') + currentInformation._removeCTN;

		currentInformation._openingBase = currentInformation._openingBase + currentInformation._retentions;

		currentInformation._closingBase = currentInformation._openingBase + currentInformation._newConnections - currentInformation._disconnections;

		if (currentInformation._closingBase < 0) {
			currentInformation._closingBase = 0;
		}

		if (currentInformation._openingBase < 0) {
			currentInformation._openingBase = 0;
		}

		currentInformation._loyaltyFeePerCTN = getLoyaltyFeeInformation(i);

		currentInformation._loyaltyFeeTotal = currentInformation._loyaltyFeePerCTN * currentInformation._averageBase;

		if (currentInformation._disconnections > (currentInformation._openingBase + currentInformation._newConnections)) {
			currentInformation._disconnections = currentInformation._openingBase + currentInformation._newConnections;
		}

		if (tableDuration == i) {
			currentInformation._disconnections = currentInformation._openingBase + currentInformation._newConnections;
			currentInformation._closingBase = 0;
		}

		console.log(currentInformation._openingBase + ' ' + currentInformation._closingBase + ' ' + currentInformation._averageBase);
		currentInformation._averageBase = (currentInformation._openingBase + currentInformation._closingBase) / 2;


		if ((currentInformation._newConnections > 0 || currentInformation._retentions > 0) && i > 1 && !CS.getAttributeValue('Block_Expiration_0')) {
			if (i > durationAdded) {
				tableDuration = tableDuration + i - durationAdded;
				durationAdded = i;
			}
		}

		monthInformation = {
			_monthOrder: i,
			_openingBase: currentInformation._openingBase,
			_retentions: currentInformation._retentions,
			_newConnections: currentInformation._newConnections,
			_disconnections: currentInformation._disconnections,
			_migration: currentInformation._migration,
			_closingBase: currentInformation._closingBase,
			_averageBase: currentInformation._averageBase,
			_loyaltyFeePerCTN: currentInformation._loyaltyFeePerCTN,
			_loyaltyFeeTotal: currentInformation._loyaltyFeeTotal,
			_removeCTN: currentInformation._removeCTN
		}
		troubleshootingTableInformationArray.push(monthInformation);

		sumOfOpeningBase += monthInformation._openingBase;
		sumOfRetentions += monthInformation._retentions;
		sumOfNewConnections += monthInformation._newConnections;
		sumOfDisconnections += monthInformation._disconnections;
		sumOfMigrations += monthInformation._migration;
		sumOfClosingBase += monthInformation._closingBase;
		averageBase = (sumOfOpeningBase + sumOfClosingBase) / 2;
		sumOfLoyaltyFeePerCTN += monthInformation._loyaltyFeePerCTN;
		sumOfLoyaltyFeeTotal += monthInformation._loyaltyFeeTotal;

		currentInformation._openingBase = currentInformation._openingBase + currentInformation._newConnections - currentInformation._disconnections;
	}


	var d = CS.getAttributeValue('Expected_delivery_date_0');

	if (d == '') {
		d = new Date();
	}

	// Data generation
	var content = '';
	content += '<tr>' + '<td><label>Month/Year</label></td>';
	content += '<td>' + '</td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + (d.getMonth() + 1) + '/' + d.getFullYear() + '</label></td>';
		d.setMonth(d.getMonth() + 1);
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Month Order</label></td>';
	content += '<td><label>' + 'Total' + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + i + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Openings Base</label></td>';
	content += '<td><label>' + sumOfOpeningBase + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._openingBase + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Retentions</label></td>';
	content += '<td><label>' + sumOfRetentions + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._retentions + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>New Connections</label></td>';
	content += '<td><label>' + sumOfNewConnections + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + (troubleshootingTableInformationArray[i - 1]._newConnections - troubleshootingTableInformationArray[i - 1]._migration) + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Disconnections</label></td>';
	content += '<td><label>' + sumOfDisconnections + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._disconnections + '</label></td>';
	}
	content += '</tr>';
	/*
	content += '<tr>' + '<td><label>Ended last period</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i-1]._removeCTN + '</label></td>';
	}
	content += '</tr>';
	*/
	content += '<tr>' + '<td><label>Migration</label></td>';
	content += '<td><label>' + sumOfMigrations + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._migration + '</label></td>';
	}
	content += '</tr>';
	var closingBaseToStore = 'Total;' + sumOfClosingBase;
	content += '<tr>' + '<td><label>Closing Base</label></td>';
	content += '<td><label>' + sumOfClosingBase + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._closingBase + '</label></td>';
		closingBaseToStore = closingBaseToStore + '_' + i + ';' + troubleshootingTableInformationArray[i - 1]._closingBase;
	}
	content += '</tr>';
	CS.setAttributeValue('ClosingBaseContainer_0', closingBaseToStore);

	var averageBaseToStore = 'Total;' + averageBase;
	content += '<tr>' + '<td><label>Average Base</label></td>';
	content += '<td><label name="averagebasetotal">' + averageBase + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label name="averagebase' + i + '">' + troubleshootingTableInformationArray[i - 1]._averageBase + '</label></td>';
		averageBaseToStore = averageBaseToStore + '_' + i + ';' + troubleshootingTableInformationArray[i - 1]._averageBase;
	}
	content += '</tr>';
	CS.setAttributeValue('AverageBaseContainer_0', averageBaseToStore);

	content += '<tr>' + '<td><label>Loyalty Fee Per CTN (€)</label></td>';
	content += '<td><label>' + sumOfLoyaltyFeePerCTN.toFixed(2) + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._loyaltyFeePerCTN.toFixed(2); + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Loyalty Fee Total (€)</label></td>';
	content += '<td><label>' + sumOfLoyaltyFeeTotal.toFixed(2) + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._loyaltyFeeTotal.toFixed(2); + '</label></td>';
	}
	content += '</tr>';


	if (showTable) {
		jQuery('#troubleshootingTable').append(content);
	} else {
		jQuery('#troubleshootingInformation').remove();
	}

	// TODO Move this to CSS.
	jQuery('#troubleshootingTable').find('td').css("width", "none");
	jQuery('#troubleshootingTable').find('th').css("padding", "3px");
	jQuery('#troubleshootingTable').find('tr').css("padding", "2px");
	jQuery('#troubleshootingTable').find('input').css("width", "250px");
	jQuery('#troubleshootingTable').find('input').css("width", "250px");
	jQuery('#troubleshootingTable').find('input').css("border-color", "#e6ebf2");
	jQuery('#troubleshootingTable').find('label').addClass("slds-form-element__label");
	jQuery('#troubleshootingTable').find('select').css("width", "250px");
	jQuery('#troubleshootingInformation').css("width", "1400px");
}

function showAllAddonsTroubleshootingTable() {
	jQuery('[id^="troubleshootingInformationAddons"]').each(function () {
		jQuery(this).remove();
	});

	var priceItemsDoneList = [];
	jQuery('[data-cs-binding=UsageAddonsTable_0]').parent().parent().prepend('');
	if (CS.Service.config["Mobile_CTN_addons_0"].relatedProducts.length > 0) {
		for (var idx in CS.Service.config['Mobile_CTN_addons_0'].relatedProducts) {
			var reference = CS.Service.config["Mobile_CTN_addons_0"].relatedProducts[idx].reference;
			if (reference != null) {
				var displayValue = CS.Service.config[reference + ":Price_item_0"]['attr'].cscfga__Display_Value__c;
				var id = CS.Service.config[reference + ":Price_item_0"]['attr'].Id;
				console.log('ID: ' + id);
				if (priceItemsDoneList.indexOf(id) >= 0) {
					jQuery('#troubleshootingInformationAddons' + id).remove();
					// This price item table has already been shown.
				} else {
					showAddonsTroubleshootingTable(displayValue, id);
					priceItemsDoneList.push(id);
				}
			}
		}
	}
}

/**
 * Shows general information custom table.
 */
function showAddonsTroubleshootingTable(name, priceItem) {

	jQuery('#troubleshootingInformationAddons' + priceItem).remove();

	var troubleshootingTable = ''
		+ '<div id="troubleshootingInformationAddons' + priceItem + '">'
		+ '<div class="slds-box" style="overflow-x:auto;max-width:1400px;">'
		+ '<p><b>' + name + '</b></p>'
		+ '<table class="list" id="troubleshootingTableAddons' + priceItem + '" style="text-align:left;">'
		+ '</table>'
		+ '</div>'
		+ '</div>';

	troubleshootingTable = jQuery(troubleshootingTable);

	if (jQuery('#troubleshootingTableAddons' + priceItem)[0] == undefined) {
		jQuery('[data-cs-binding=UsageAddonsTable_0]').parent().parent().prepend(troubleshootingTable);
	}

	var tableDuration = parseInt(CS.getAttributeValue('Duration_0')) + 1;

	var troubleshootingTableInformationArray = [];

	var durationAdded = 0;

	var sumOfOpeningBase = 0;
	var sumOfRetentions = 0;
	var sumOfNewConnections = 0;
	var sumOfDisconnections = 0;
	var sumOfMigrations = 0;
	var sumOfClosingBase = 0;
	var averageBase = 0;
	var sumOfLoyaltyFeePerCTN = 0;
	var sumOfLoyaltyFeeTotal = 0;

	currentInformation = {
		_openingBase: 0,
		_retentions: 0,
		_newConnections: 0,
		_disconnections: 0,
		_migration: 0,
		_closingBase: 0,
		_averageBase: 0,
		_loyaltyFeePerCTN: 0,
		_loyaltyFeeTotal: 0,
		_removeCTN: 0
	}

	for (i = 1; i <= tableDuration; i++) {

		currentInformation._migration = getAddonInformation(i, 'Migration', priceItem);

		currentInformation._retentions = getAddonInformation(i, 'Retention', priceItem);

		currentInformation._newConnections = getAddonInformation(i, 'New', priceItem) + getAddonInformation(i, 'Porting', priceItem) + currentInformation._migration;

		ctnQuantitiesToRemove[i + CS.getAttributeValue('Duration_0')] = currentInformation._newConnections + currentInformation._retentions;

		if (ctnQuantitiesToRemove[i] != null) {
			currentInformation._removeCTN = ctnQuantitiesToRemove[i];
		}

		currentInformation._disconnections = getAddonInformation(i, 'Disconnect', priceItem) + currentInformation._removeCTN;

		currentInformation._openingBase = currentInformation._openingBase + currentInformation._retentions;

		currentInformation._closingBase = currentInformation._openingBase + currentInformation._newConnections - currentInformation._disconnections;

		if (currentInformation._closingBase < 0) {
			currentInformation._closingBase = 0;
		}

		if (currentInformation._openingBase < 0) {
			currentInformation._openingBase = 0;
		}

		currentInformation._loyaltyFeePerCTN = getLoyaltyFeeInformation(i);

		currentInformation._loyaltyFeeTotal = currentInformation._loyaltyFeePerCTN * currentInformation._averageBase;

		if (currentInformation._disconnections > (currentInformation._openingBase + currentInformation._newConnections)) {
			currentInformation._disconnections = currentInformation._openingBase + currentInformation._newConnections;
		}

		if (tableDuration == i) {
			currentInformation._disconnections = currentInformation._openingBase + currentInformation._newConnections;
			currentInformation._closingBase = 0;
		}

		console.log(currentInformation._openingBase + ' ' + currentInformation._closingBase + ' ' + currentInformation._averageBase);
		currentInformation._averageBase = (currentInformation._openingBase + currentInformation._closingBase) / 2;


		if ((currentInformation._newConnections > 0 || currentInformation._retentions > 0) && i > 1 && !CS.getAttributeValue('Block_Expiration_0')) {
			if (i > durationAdded) {
				tableDuration = tableDuration + i - durationAdded;
				durationAdded = i;
			}
		}

		monthInformation = {
			_monthOrder: i,
			_openingBase: currentInformation._openingBase,
			_retentions: currentInformation._retentions,
			_newConnections: currentInformation._newConnections,
			_disconnections: currentInformation._disconnections,
			_migration: currentInformation._migration,
			_closingBase: currentInformation._closingBase,
			_averageBase: currentInformation._averageBase,
			_loyaltyFeePerCTN: currentInformation._loyaltyFeePerCTN,
			_loyaltyFeeTotal: currentInformation._loyaltyFeeTotal,
			_removeCTN: currentInformation._removeCTN
		}
		troubleshootingTableInformationArray.push(monthInformation);

		sumOfOpeningBase += monthInformation._openingBase;
		sumOfRetentions += monthInformation._retentions;
		sumOfNewConnections += monthInformation._newConnections;
		sumOfDisconnections += monthInformation._disconnections;
		sumOfMigrations += monthInformation._migration;
		sumOfClosingBase += monthInformation._closingBase;
		averageBase = (sumOfOpeningBase + sumOfClosingBase) / 2;
		sumOfLoyaltyFeePerCTN += monthInformation._loyaltyFeePerCTN;
		sumOfLoyaltyFeeTotal += monthInformation._loyaltyFeeTotal;

		currentInformation._openingBase = currentInformation._openingBase + currentInformation._newConnections - currentInformation._disconnections;
	}


	var d = CS.getAttributeValue('Expected_delivery_date_0');

	if (d == '') {
		d = new Date();
	}

	// Data generation
	var content = '';
	content += '<tr>' + '<td><label>Month/Year</label></td>';
	content += '<td>' + '</td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + (d.getMonth() + 1) + '/' + d.getFullYear() + '</label></td>';
		d.setMonth(d.getMonth() + 1);
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Month Order</label></td>';
	content += '<td><label>' + 'Total' + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + i + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Openings Base</label></td>';
	content += '<td><label>' + sumOfOpeningBase + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._openingBase + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Retentions</label></td>';
	content += '<td><label>' + sumOfRetentions + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._retentions + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>New Connections</label></td>';
	content += '<td><label>' + sumOfNewConnections + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + (troubleshootingTableInformationArray[i - 1]._newConnections - troubleshootingTableInformationArray[i - 1]._migration) + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Disconnections</label></td>';
	content += '<td><label>' + sumOfDisconnections + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._disconnections + '</label></td>';
	}
	content += '</tr>';
	/*
	content += '<tr>' + '<td><label>Ended last period</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i-1]._removeCTN + '</label></td>';
	}
	content += '</tr>';
	*/
	content += '<tr>' + '<td><label>Migration</label></td>';
	content += '<td><label>' + sumOfMigrations + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._migration + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Closing Base</label></td>';
	content += '<td><label>' + sumOfClosingBase + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._closingBase + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Average Base</label></td>';
	content += '<td><label>' + averageBase + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._averageBase + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Loyalty Fee Per CTN (€)</label></td>';
	content += '<td><label>' + sumOfLoyaltyFeePerCTN.toFixed(2) + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._loyaltyFeePerCTN.toFixed(2); + '</label></td>';
	}
	content += '</tr>';

	content += '<tr>' + '<td><label>Loyalty Fee Total (€)</label></td>';
	content += '<td><label>' + sumOfLoyaltyFeeTotal.toFixed(2) + '</label></td>';
	for (i = 1; i <= tableDuration; i++) {
		content += '<td><label>' + troubleshootingTableInformationArray[i - 1]._loyaltyFeeTotal.toFixed(2); + '</label></td>';
	}
	content += '</tr>';
	jQuery('#troubleshootingTableAddons' + priceItem).append(content);


	// TODO Move this to CSS.
	jQuery('#troubleshootingTableAddons' + priceItem).find('td').css("width", "none");
	jQuery('#troubleshootingTableAddons' + priceItem).find('th').css("padding", "3px");
	jQuery('#troubleshootingTableAddons' + priceItem).find('tr').css("padding", "2px");
	jQuery('#troubleshootingTableAddons' + priceItem).find('input').css("width", "250px");
	jQuery('#troubleshootingTableAddons' + priceItem).find('input').css("width", "250px");
	jQuery('#troubleshootingTableAddons' + priceItem).find('input').css("border-color", "#e6ebf2");
	jQuery('#troubleshootingTableAddons' + priceItem).find('label').addClass("slds-form-element__label");
	jQuery('#troubleshootingTableAddons' + priceItem).find('select').css("width", "250px");
	jQuery('#troubleshootingInformationAddons' + priceItem).css("width", "1400px");
}

function getMobileSubscriptionInformation(monthOrder, connectionType) {
	var subscriptionArray = [];
	if (CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts.length > 0) {
		for (var idx in CS.Service.config['Mobile_CTN_subscription_0'].relatedProducts) {
			var reference = CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts[idx].reference;
			if (reference != null) {
				if (CS.Service.config[reference + ":Start_month_0"]['attr'].cscfga__Value__c == monthOrder) {
					if (CS.Service.config[reference + ":Connection_type_0"]['attr'].cscfga__Value__c == connectionType) {
						return parseInt(CS.Service.config[reference + ":CTN_quantity_0"]['attr'].cscfga__Value__c);
					}
				}
			}
		}
	}
	return 0;
}

function getAddonInformation(monthOrder, connectionType, priceItem) {
	var subscriptionArray = [];
	if (CS.Service.config["Mobile_CTN_addons_0"].relatedProducts.length > 0) {
		for (var idx in CS.Service.config['Mobile_CTN_addons_0'].relatedProducts) {
			var reference = CS.Service.config["Mobile_CTN_addons_0"].relatedProducts[idx].reference;
			if (reference != null) {
				if (CS.Service.config[reference + ":Start_month_0"]['attr'].cscfga__Value__c == monthOrder) {
					if (CS.Service.config[reference + ":Connection_type_0"]['attr'].cscfga__Value__c == connectionType) {
						if (CS.Service.config[reference + ":Price_item_0"]['attr'].Id == priceItem) {
							return parseInt(CS.Service.config[reference + ":CTN_quantity_0"]['attr'].cscfga__Value__c);
						}
					}
				}
			}
		}
	}
	return 0;
}

function getLoyaltyFeeInformation(month) {
	if (CS.Service.config["Mobile_CTN_loyalty_fee_0"].relatedProducts.length > 0) {
		for (var idx in CS.Service.config['Mobile_CTN_loyalty_fee_0'].relatedProducts) {
			var reference = CS.Service.config["Mobile_CTN_loyalty_fee_0"].relatedProducts[idx].reference;
			if (reference != null) {
				var moments = CS.Service.config[reference + ":Reimbursement_moments_0"]['attr'].cscfga__Value__c.split(',');
				var amounts = CS.Service.config[reference + ":Reimbursement_amounts_0"]['attr'].cscfga__Value__c.split(',');

				for (var i = 0; i < moments.length; i++) {
					if (moments[i] == month) {
						if (amounts[i] != null) {
							return parseFloat(amounts[i]);
						}
					}

				}
			}
		}
	}
	return 0;
}

function removeRelatedProductNewButton() {
	jQuery("button[data-cs-ref='Mobile_Usage_Product_0']").remove();
}

function removeRelatedEditDelCopy() {
	jQuery("[data-cs-action='editRelatedProduct']").each(function () {
		if (jQuery(this).text() == 'Edit') {
			jQuery(this).after("<span></span>");
			jQuery(this).remove();
		}
	});
	/*
	jQuery("[data-cs-action='removeRelatedProduct']").each(function() {    
		if (jQuery(this).text() == 'Del') {
			jQuery(this).after("<span>" + jQuery(this).text() + "</span>");
			jQuery(this).remove();
		}
	});
	*/

	jQuery("[data-cs-action='copyRelatedProduct']").each(function () {
		if (jQuery(this).text() == 'Copy') {
			jQuery(this).after("<span></span>");
			jQuery(this).remove();
		}
	});
}

function removeRelatedProductButtons(arrayProducts, arrayButtons) {
	arrayProducts.forEach(function (item, index, array) {
		jQuery("table tr").each(function () {
			var attr = jQuery(this).attr('data-cs-ref');
			// For some browsers, attr is undefined; for others, attr is false. Check for both.

			if (typeof attr !== typeof undefined && attr !== false) {
				var item2 = item.endsWith("_") ? item : (item + '_');
				if (attr.indexOf(item2) >= 0) {
					if (arrayButtons.length == 1 && (arrayButtons[0] == 'All') || (arrayButtons[0] == 'all')) {
						jQuery(this).find("td:eq(0)").html("<span>-</span>");
					}
					else {
						for (var i = 0; i < arrayButtons.length; i++) {
							jQuery(this).find("td:first:contains('Edit')").find("span:contains('" + arrayButtons[i] + "')").after("<span>-</span>").remove();
						}
					}
				}
			}
		})
	})
}

function calculateNetTariff() {
	var prefixList = ['WithinCustomer', 'ToFixed', 'ToMobile', 'ToMobileOutside', 'NationalWithinVpn', 'NationalToVF', 'NationalToOthers', 'EuToEu', 'EuToRow', 'EuToExceptions', 'RowToEu', 'RowToRow', 'RowToExceptions', 'ExceptionsToEu', 'ExceptionsToRow', 'ExceptionsToExceptions', 'EuIncoming', 'RowIncoming', 'ExceptionsIncoming', 'EuOutgoing', 'RowOutgoing', 'ExceptionsOutgoing', 'SmsNational', 'SmsRmoEu', 'SmsRmoRow', 'SmsRmoExceptions', 'SmsIddToEu', 'SmsIddToRow', 'SmsIddToExceptions', 'DataNational', 'DataRoamingEu', 'DataRaomingRow', 'DataRoaming', 'DataRoamingVF', 'DataRoamingNonVF'];
	var netTariffSufix = 'NetTariff';
	var costPriceSufix = 'CostTariff';
	var grossTariffSufix = 'GrossTariff';
	var minutenSufix = 'Minuten';
	var mbSufix = 'Mb';
	var quantitySufix = 'Quantity';
	var sumNetTariff = 0;
	var sumCostPriceTariff = 0;

	for (var prefix in prefixList) {
		var gorssTariffValue = jQuery('[name="' + prefixList[prefix] + grossTariffSufix + '"]').attr('value');
		if (gorssTariffValue == null || gorssTariffValue == '') {
			gorssTariffValue = 0;
		}
		var multiplier = 0;
		var minutenValue = jQuery('[name="' + prefixList[prefix] + minutenSufix + '"]').attr('value');
		if (minutenValue == null || minutenValue == '') {
			minutenValue = 0;
		}
		var mbValue = jQuery('[name="' + prefixList[prefix] + mbSufix + '"]').attr('value');
		if (mbValue == null || mbValue == '') {
			mbValue = 0;
		}
		var quantityValue = jQuery('[name="' + prefixList[prefix] + quantitySufix + '"]').attr('value');
		if (quantityValue == null || quantityValue == '') {
			quantityValue = 0;
		}
		var costPriceValue = jQuery('[name="' + prefixList[prefix] + costPriceSufix + '"]').attr('value');
		if (costPriceValue == null || costPriceValue == '') {
			costPriceValue = 0;
		}
		multiplier = parseFloat(minutenValue) + parseFloat(mbValue) + parseFloat(quantityValue);
		sumNetTariff = sumNetTariff + (parseFloat(gorssTariffValue) * multiplier);
		sumCostPriceTariff = sumCostPriceTariff + (parseFloat(costPriceValue) * multiplier);
		muSetValueOfAttribute(prefixList[prefix] + netTariffSufix, gorssTariffValue);
	}
	//removeRelatedProductAndClearFlag('Mobile_Usage_Product_0', 'Mobile_Usage_Set_0');


	CS.setAttributeValue('TotalNetValueUsage_0', sumNetTariff);
	muSetValueOfAttribute('muTotalNetTariff', sumNetTariff.toFixed(2));

	//removeRelatedProductAndClearFlag('Mobile_Usage_Product_0', 'Mobile_Usage_Set_0');


	if (CS.getAttributeValue('Mobile_Usage_Set_0') == '') {
		CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config['Mobile_Usage_Lookup_0']);
		CS.setAttributeValue('Mobile_Usage_Set_0', 'set');
	}

	if (CS.getAttributeValue('Mobile_Usage_Set_0') != '') {
		try {
			CS.setAttributeValue('Mobile_Usage_Product_0:RC_List_Price_0', sumNetTariff);
			CS.setAttributeField('Mobile_Usage_Product_0:Recurring_Charge_0', 'Price', sumNetTariff);
			CS.setAttributeField('Mobile_Usage_Product_0:Recurring_Charge_0', 'list_price', sumNetTariff);
			CS.setAttributeValue('Mobile_Usage_Product_0:RC_Cost_0', sumCostPriceTariff);
		} catch (err) {
			console.log('Mobile usage related product has been set but product can not be found. Maybe just a timing issue.');
		}
	}
}

/**
 * Sets the value of attribute that is not currency. 
 */
function muSetValueOfAttributeNotCurrency(attributeName, attributeValue) {
	log('muSetValueOfAttributeNotCurrency<==attributeName: ' + attributeName + ';attributeValue: ' + attributeValue);

	if (isNaN(attributeValue)) {
		attributeValue = 0;
	}

	jQuery('[name="' + attributeName + '"]').val(attributeValue);
	allTableInformation[attributeName] = attributeValue;
}

/**
 * Sets the value of attribute that is a currency.
 */
function muSetValueOfAttribute(attributeName, attributeValue) {
	log('muSetValueOfAttributeNotCurrency<==attributeName: ' + attributeName + ';attributeValue: ' + attributeValue);

	if (isNaN(attributeValue)) {
		attributeValue = 0;
	}

	// attributeValue = returnFormatedDecimalNumber(attributeValue);

	jQuery('[name="' + attributeName + '"]').val(attributeValue);
	allTableInformation[attributeName] = attributeValue;
}

/**
 *	Formats decimal number so that it is two decimal points rounded and commas are added for larger numbers.
 */
function returnFormatedDecimalNumber(unformatedDecimalNumber) {
	log('returnFormatedDecimalNumber<==unformatedDecimalNumber: ' + unformatedDecimalNumber);
	unformatedDecimalNumber = unformatedDecimalNumber.toFixed(2);
	unformatedDecimalNumber = unformatedDecimalNumber.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	log('returnFormatedDecimalNumber==>unformatedDecimalNumber: ' + unformatedDecimalNumber);
	return unformatedDecimalNumber;
}

/**
 * Get float value from string.
 */
function getFloatValueFromString(floatValueInString) {
	return parseFloat(floatValueInString.replace(",", "."));
}

/**
 * Checks to see if value of input fields is numerical. Before checking, removes all commas.
 */
function muCheckIfAllValuesAreNumerical() {
	jQuery('.slds-input').each(function () {
		if (jQuery(this).attr('value') != '') {
			if (jQuery(this).attr('name') != 'muPriceplanCombination' && jQuery(this).attr('name') != 'muEuAddon' && jQuery(this).attr('name') != 'muProposition') {
				var value = jQuery(this).attr('value').replace(/,/g, '');
				if (!jQuery.isNumeric(value)) {
					invalidateInputField(jQuery(this).attr('name'));
				} else {
					validateInputField(jQuery(this).attr('name'));
				}
			}
		}
	});
}

function invalidateInputField(fieldName) {
	jQuery('[name=' + fieldName + ']').css("border-color", "red");
}

function validateInputField(fieldName) {
	jQuery('[name=' + fieldName + ']').css("border-color", "#d8dde6");
}

function storeData() {
	/*
	jQuery('.slds-input, .slds-select').change(function() {
		
		allTableInformation[jQuery(this).attr('name')] = jQuery(this).attr('value');
		
		var toStore = '';
		for (var key in allTableInformation) {
			if (key !== 'Subscription_hardware_commission_0' && key !== 'muQuoteStatus' && key !== 'Data_Capping_0' && key !== 'Scenario_0' && key !== 'Other_0') {
					toStore = toStore + key + "|" + allTableInformation[key] + ";";
			}
		}
	
		CS.setAttributeValue("DataStorageHidden_0", toStore);
	});
	*/

	jQuery('.slds-input, .slds-select').each(function () {
		allTableInformation[jQuery(this).attr('name')] = jQuery(this).attr('value');

		var toStore = '';
		for (var key in allTableInformation) {
			toStore = toStore + key + "|" + allTableInformation[key] + ";";
		}

		CS.setAttributeValue("DataStorageHidden_0", toStore);
	});

}

function mobileCTNProfileBeforeFinish() {
	console.log("Before finish is now");
	checkNationalUsage();
	calculateNetTariff();

	jQuery('.slds-input, .slds-select').each(function () {
		allTableInformation[jQuery(this).attr('name')] = jQuery(this).attr('value');

		var toStore = '';
		for (var key in allTableInformation) {
			if (key !== 'Subscription_hardware_commission_0' && key !== 'muQuoteStatus' && key !== 'Data_Capping_0' && key !== 'Scenario_0' && key !== 'Other_0') {
				toStore = toStore + key + "|" + allTableInformation[key] + ";";
			}
		}

		CS.setAttributeValue("DataStorageHidden_0", toStore);
	});
}

/**
 * Main caluclation class.
 */
function muExecuteAllCalculations() {


	var values = CS.getAttributeValue("DataStorageHidden_0").split(';');

	for (var i = 0; i < values.length; i++) {
		var key = values[i].split('|')[0];
		var value = values[i].split('|')[1];

		if (jQuery('[name="' + key + '"]').length > 0) {
			//NC - FIX: Subscription_hardware_commission_0 value not getting cleared by configurator Action - have no idea if this breaks anything else
			if (key !== 'Subscription_hardware_commission_0' && key !== 'muQuoteStatus' && key !== 'Data_Capping_0' && key !== 'Scenario_0' && key !== 'Other_0' &&
				key !== 'muProposition' && key !== 'muPriceplanCombination' && key !== 'muCommitment') {
				jQuery('[name="' + key + '"]').val(value);

				if (key != undefined && value != undefined) {
					allTableInformation[key] = value;
				}
			}
		}
	}

	/*
	allTableInformation[jQuery(this).attr('name')] = jQuery(this).attr('value');
		
	var toStore = '';
	for (var key in allTableInformation) {
			 toStore = toStore + key + "|" + allTableInformation[key] + ";";
	}

	CS.setAttributeValue("DataStorageHidden_0", toStore);

	var values = CS.getAttributeValue("DataStorageHidden_0").split(';');

	for (var i=0; i<values.length;i++) {
		var key = values[i].split('|')[0];
		var value = values[i].split('|')[1];
		
		if (jQuery('[name="'+ key + '"]').length > 0) {
			jQuery('[name="'+ key + '"]').val(value);
			if (key != undefined && value != undefined) {
				allTableInformation[key] = value;
			}
		}
	}*/
}

function hideAllCostColumns() {

	var allowedProfiles = ["System Administrator", "VF EBU/WBU Control", "VF Financial Sales Control"];

	if (allowedProfiles.indexOf(CS.getUserProfile()) == -1) {
		jQuery('.slds-input').each(function () {
			if (jQuery(this).attr('name') != null) {
				if (jQuery(this).attr('name').indexOf("Cost") > -1) {
					jQuery(this).hide();
				}
			}
		});

		jQuery('.slds-form-element__label:contains("Cost Tariff")').each(function () {
			jQuery(this).hide();
		});
		jQuery('.slds-form-element__label:contains("Total Costs")').each(function () {
			jQuery(this).hide();
		});
	}
}

/**
 * Updates value of amount input fields to currency.
 */
function myQuoteStatusChanged() {
	var myQuoteStatus = jQuery('select[name=muQuoteStatus]').find(":selected").text();
	jQuery('.slds-input').each(function () {
		if (jQuery(this).attr('name') != null) {
			if (jQuery(this).attr('name').indexOf("Minuten") >= 0 || jQuery(this).attr('name').indexOf("Mb") >= 0 || jQuery(this).attr('name').indexOf("Quantity") >= 0) {
				if (jQuery(this).attr('name').indexOf("Total") == -1) {
					if (CS.Service.config[''].linkedObjectProperties.Mobile_Usage_Available__c == false) {
						jQuery(this).prop("disabled", true);
					}
					else {
						jQuery(this).prop("disabled", false);
					}
				}
			}
		}
	});
}

function checkNationalUsage() {
	if (CS.Service.config[''].linkedObjectProperties.Mobile_Usage_Available__c == false)
		return;

	var counter = 0;
	if (allTableInformation['NationalWithinVpnMinuten'] > 0)
		counter++;
	if (allTableInformation['NationalToVFMinuten'] > 0)
		counter++;
	if (allTableInformation['NationalToOthersMinuten'] > 0)
		counter++;
	if (counter > 1)
		CS.markConfigurationInvalid('Only one \'National usage\' minutes (National from PBX to within VPN (on-net), National from PBX to VF (off-net), National from PBX to other operators (off-net)) can be set')
}

function oneBusinessIotSelected() {
	var scenarioName = CS.getAttributeValue('Scenario_0');

	if (scenarioName != null && scenarioName.toLowerCase().indexOf('iot') > -1) {
		jQuery('[name="NationalWithinVpnMinuten"]').prop("disabled", true);
		jQuery('[name="NationalToVFMinuten"]').prop("disabled", true);
		jQuery('[name="NationalToOthersMinuten"]').prop("disabled", true);
		jQuery('[name="DataRoamingEuMb"]').prop("disabled", true);

		jQuery('[name=NationalWithinVpnMinuten]').val(0);
		jQuery('[name=NationalToVFMinuten]').val(0);
		jQuery('[name=NationalToOthersMinuten]').val(0);
		jQuery('[name=DataRoamingEuMb]').val(0);
		allTableInformation['NationalWithinVpnMinuten'] = 0;
		allTableInformation['NationalToVFMinuten'] = 0;
		allTableInformation['NationalToOthersMinuten'] = 0;
		allTableInformation['DataRoamingEuMb'] = 0;

		var dataUsageDetails = parseFloat(allTableInformation['DataNationalMb']) + parseFloat(allTableInformation['DataRoamingEuMb']) + parseFloat(allTableInformation['DataRoamingRowMb']) + parseFloat(allTableInformation['DataRoamingMb']) + parseFloat(allTableInformation['DataRoamingVFMb']) + parseFloat(allTableInformation['DataRoamingNonVFMb']);
		muSetValueOfAttribute('DataUsageDetailsMbTotal', dataUsageDetails);
		CS.setAttributeValue('DataUsageMbTotal_0', dataUsageDetails, true);

		jQuery('[name=DataRoamingVFMb]').parent().parent().show();
		jQuery('[name=DataRoamingNonVFMb]').parent().parent().show();
	} else if (CS.Service.config[''].linkedObjectProperties.Mobile_Usage_Available__c == true) {
		jQuery('[name="NationalWithinVpnMinuten"]').prop("disabled", false);
		jQuery('[name="NationalToVFMinuten"]').prop("disabled", false);
		jQuery('[name="NationalToOthersMinuten"]').prop("disabled", false);
		jQuery('[name="DataRoamingEuMb"]').prop("disabled", false);
		jQuery('[name=DataRoamingVFMb]').parent().parent().hide();
		jQuery('[name=DataRoamingNonVFMb]').parent().parent().hide();

		jQuery('[name=DataRoamingVFMb]').val(0);
		jQuery('[name=DataRoamingNonVFMb]').val(0);
		allTableInformation['DataRoamingVFMb'] = 0;
		allTableInformation['DataRoamingNonVFMb'] = 0;

		CS.setAttributeValue("DataUsageRoamingVF_0", 0, true);
		CS.setAttributeValue("DataUsageRoamingNonVF_0", 0, true);

		var dataUsageDetails = parseFloat(allTableInformation['DataNationalMb']) + parseFloat(allTableInformation['DataRoamingEuMb']) + parseFloat(allTableInformation['DataRoamingRowMb']) + parseFloat(allTableInformation['DataRoamingMb']) + parseFloat(allTableInformation['DataRoamingVFMb']) + parseFloat(allTableInformation['DataRoamingNonVFMb']);
		muSetValueOfAttribute('DataUsageDetailsMbTotal', dataUsageDetails);
		CS.setAttributeValue('DataUsageMbTotal_0', dataUsageDetails, true);

	} else {
		jQuery('[name=DataRoamingVFMb]').parent().parent().hide();
		jQuery('[name=DataRoamingNonVFMb]').parent().parent().hide();
		jQuery('[name=DataRoamingVFMb]').val(0);
		jQuery('[name=DataRoamingNonVFMb]').val(0);
		allTableInformation['DataRoamingVFMb'] = 0;
		allTableInformation['DataRoamingNonVFMb'] = 0;

		CS.setAttributeValue("DataUsageRoamingVF_0", 0, true);
		CS.setAttributeValue("DataUsageRoamingNonVF_0", 0, true);

		var dataUsageDetails = parseFloat(allTableInformation['DataNationalMb']) + parseFloat(allTableInformation['DataRoamingEuMb']) + parseFloat(allTableInformation['DataRoamingRowMb']) + parseFloat(allTableInformation['DataRoamingMb']) + parseFloat(allTableInformation['DataRoamingVFMb']) + parseFloat(allTableInformation['DataRoamingNonVFMb']);
		muSetValueOfAttribute('DataUsageDetailsMbTotal', dataUsageDetails);
		CS.setAttributeValue('DataUsageMbTotal_0', dataUsageDetails, true);
	}
}

/**
 * Shows general information custom table.
 */
function muShowGeneralInformation() {

	var muGeneralInfomration = ''
		+ '<div id="muGeneralInformation">'
		+ '<div class="slds-box">'
		+ '<table class="list" id="muGeneralInformationTable" style="text-align:left;">'
		+ '<tr>'
		+ '<td><label>Commitment</label></td>'
		+ '<td><input type="text" step="any" name="muCommitment" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr>'
		+ '<td><label>Proposition</label></td>'
		+ '<td><input type="text" step="any" name="muProposition" disabled="" class="slds-input"></td>'
		+ '</tr>'
		+ '</tr>'
		+ '<tr>'
		+ '<td><label>MAF (Gross)</label></td>'
		+ '<td><input type="text" step="any" name="muMafGross" disabled="" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr>'
		+ '<td><label>Total Net Tariff</label></td>'
		+ '<td><input type="text" step="any" name="muTotalNetTariff" disabled="" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr>'
		+ '<td><label>Priceplan Combination</label></td>'
		+ '<td><input type="text" step="any" name="muPriceplanCombination" disabled="" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr>'
		+ '<td><label>Quote Status</label></td>'
		+ '<td>'
		+ '<select class="slds-select" name="muQuoteStatus">'
		+ '<option value="netprofit">NETPROFIT</option>'
		+ '<option value="sacsrc">SACSRC</option>'
		+ '</select>'
		+ '</td>'
		+ '</tr>'
		+ '</table>'
		+ '</div>'
		+ '</div>';

	muGeneralInfomration = jQuery(muGeneralInfomration);

	if (jQuery('#muGeneralInformationTable')[0] == undefined) {
		jQuery('[data-cs-binding=GeneralInfoPart_0]').parent().parent().prepend(muGeneralInfomration);
	}

	// TODO Move this to CSS.
	jQuery('#muGeneralInformationTable').find('td').css("width", "none");
	jQuery('#muGeneralInformationTable').find('th').css("border", "none");
	jQuery('#muGeneralInformationTable').find('th').css("border-color", "white");
	jQuery('#muGeneralInformationTable').find('th').css("padding", "3px");
	jQuery('#muGeneralInformationTable').find('td').css("border", "none");
	jQuery('#muGeneralInformationTable').find('td').css("border-color", "white");
	jQuery('#muGeneralInformationTable').find('tr').css("border", "none");
	jQuery('#muGeneralInformationTable').find('tr').css("padding", "2px");
	jQuery('#muGeneralInformationTable').find('tr').css("border-color", "white");
	jQuery('#muGeneralInformationTable').find('input').css("width", "250px");
	jQuery('#muGeneralInformationTable').find('input').css("width", "250px");
	jQuery('#muGeneralInformationTable').find('input').css("border-color", "#e6ebf2");
	jQuery('#muGeneralInformationTable').find('label').addClass("slds-form-element__label");
	jQuery('#muGeneralInformationTable').find('select').css("width", "250px");
	jQuery('#muGeneralInformation').css("width", "450px");
}

/**
 * Shows outgoing calls netherlands custom table.  style="display:none;"
 */
function muShowOutgoingCallsNetherlands() {

	var muOutgoingCallsNetherlands = '<div id="muOutgoingCallsNetherlands"><table class="list" id="muOutgoingCallsNetherlandsTable"><tr class="headerRow">'
		+ '<th class="firstColumn"><label>Outgoing phonecalls within in The Netherlands (National MO)</label></th>'
		+ '<th><label>Minutes</label></th>'
		+ '<th><label>Gross Tariff</label></th>'
		+ '<th><label>Net Tariff</label></th>'
		+ '<th><label>Cost Tariff</label></th></tr>'
		+ '<tr><td><label>Within Customer (Mobile + Fixed)</label></td>'
		+ '<td><input type="text" step="any" name="WithinCustomerMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="WithinCustomerGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="WithinCustomerNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="WithinCustomerCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>To Fixed (outside customer)</label></td>'
		+ '<td><input type="text" step="any" name="ToFixedMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ToFixedGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ToFixedNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ToFixedCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>To Mobile other VF (outside customer)</label></td>'
		+ '<td><input type="text" step="any" name="ToMobileMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ToMobileGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ToMobileNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ToMobileCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>To Mobile (outside customer)</label></td>'
		+ '<td><input type="text" step="any" name="ToMobileOutsideMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ToMobileOutsideGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ToMobileOutsideNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ToMobileOutsideCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>National from PBX to within VPN (on-net)</label></td>'
		+ '<td><input type="text" step="any" name="NationalWithinVpnMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="NationalWithinVpnGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="NationalWithinVpnNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="NationalWithinVpnCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>National from PBX to VF (off-net)</label></td>'
		+ '<td><input type="text" step="any" name="NationalToVFMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="NationalToVFGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="NationalToVFNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="NationalToVFCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>National from PBX to other operators (off-net)</label></td>'
		+ '<td><input type="text" step="any" name="NationalToOthersMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="NationalToOthersGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="NationalToOthersNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="NationalToOthersCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Total</label></td>'
		+ '<td><input type="text" step="any" disabled="" name="OutgoingCallsMinutesTotal" class="slds-input"></td>'
		+ '</tr></table></div>';



	muOutgoingCallsNetherlands = jQuery(muOutgoingCallsNetherlands);

	if (jQuery('#muOutgoingCallsNetherlandsTable')[0] == undefined) {
		jQuery('[data-cs-binding=OutgoingCallsNetherlandsPart_0]').parent().parent().prepend(muOutgoingCallsNetherlands);
	}

	// TODO Move this to CSS.
	jQuery('#muOutgoingCallsNetherlandsTable').find('td').css("width", "none");
	jQuery('#muOutgoingCallsNetherlandsTable').find('th').css("border", "none");
	jQuery('#muOutgoingCallsNetherlandsTable').find('th').css("border-color", "white");
	jQuery('#muOutgoingCallsNetherlandsTable').find('th').css("padding", "3px");
	jQuery('#muOutgoingCallsNetherlandsTable').find('td').css("border", "none");
	jQuery('#muOutgoingCallsNetherlandsTable').find('td').css("border-color", "white");
	jQuery('#muOutgoingCallsNetherlandsTable').find('tr').css("border", "none");
	jQuery('#muOutgoingCallsNetherlandsTable').find('tr').css("padding", "2px");
	jQuery('#muOutgoingCallsNetherlandsTable').find('tr').css("border-color", "white");
	jQuery('#muOutgoingCallsNetherlandsTable').find('input').css("width", "200px");
	jQuery('#muOutgoingCallsNetherlandsTable').find('th').css("width", "200px");
	jQuery('#muOutgoingCallsNetherlandsTable').find('td').css("width", "200px");
	jQuery('#muOutgoingCallsNetherlandsTable').find('select').css("width", "140px");
	jQuery('#muOutgoingCallsNetherlandsTable').find('input').css("border-color", "#e6ebf2");
	jQuery('#muOutgoingCallsNetherlandsTable').find('label').addClass("slds-form-element__label");
	jQuery('#muOutgoingCallsNetherlandsTable').find('input').css("text-align", "right");
}

/**
 * Shows outgoing calls abroad custom table.
 */
function muShowOutgoingCallsAbroad() {

	var muOutgoingCallsAbroad = '<div id="muOutgoingCallsAbroad"><table class="list" id="muOutgoingCallsAbroadTable"><tr class="headerRow">'
		+ '<th class="firstColumn"><label>Outgoing phonecalls abroad (Roaming MO)</label></th>'
		+ '<th><label>Minutes</label></th>'
		+ '<th><label>Gross Tariff</label></th>'
		+ '<th><label>Net Tariff</label></th>'
		+ '<th><label>Cost Tariff</label></th></tr>'
		+ '<tr><td><label>EU+ (excl. NL) to EU+ (incl. NL)</label></td>'
		+ '<td><input type="text" step="any" name="EuToEuMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuToEuGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuToEuNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuToEuCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>EU+ (excl. NL) to RoW</label></td>'
		+ '<td><input type="text" step="any" name="EuToRowMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuToRowGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuToRowNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuToRowCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>EU+ (excl. NL) to Exceptions</label></td>'
		+ '<td><input type="text" step="any" name="EuToExceptionsMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuToExceptionsGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuToExceptionsNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuToExceptionsCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>RoW to EU+ (incl. NL)</label></td>'
		+ '<td><input type="text" step="any" name="RowToEuMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowToEuGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowToEuNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowToEuCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>RoW to RoW</label></td>'
		+ '<td><input type="text" step="any" name="RowToRowMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowToRowGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowToRowNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowToRowCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>RoW to Exceptions</label></td>'
		+ '<td><input type="text" step="any" name="RowToExceptionsMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowToExceptionsGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowToExceptionsNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowToExceptionsCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Exceptions to EU+ (incl. NL)</label></td>'
		+ '<td><input type="text" step="any" name="ExceptionsToEuMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsToEuGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsToEuNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsToEuCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Exceptions to RoW</label></td>'
		+ '<td><input type="text" step="any" name="ExceptionsToRowMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsToRowGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsToRowNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsToRowCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Exceptions to Exceptions</label></td>'
		+ '<td><input type="text" step="any" name="ExceptionsToExceptionsMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsToExceptionsGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsToExceptionsNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsToExceptionsCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Total</label></td>'
		+ '<td><input type="text" step="any" disabled="" name="OutgoingCallsAbroadMinutesTotal" class="slds-input"></td>'
		+ '</tr></table></div>';



	muOutgoingCallsAbroad = jQuery(muOutgoingCallsAbroad);

	if (jQuery('#muOutgoingCallsAbroadTable')[0] == undefined) {
		jQuery('[data-cs-binding=OutgoingCallsAbroadPart_0]').parent().parent().prepend(muOutgoingCallsAbroad);
	}

	// TODO Move this to CSS.
	jQuery('#muOutgoingCallsAbroadTable').find('td').css("width", "none");
	jQuery('#muOutgoingCallsAbroadTable').find('th').css("border", "none");
	jQuery('#muOutgoingCallsAbroadTable').find('th').css("border-color", "white");
	jQuery('#muOutgoingCallsAbroadTable').find('th').css("padding", "3px");
	jQuery('#muOutgoingCallsAbroadTable').find('td').css("border", "none");
	jQuery('#muOutgoingCallsAbroadTable').find('td').css("border-color", "white");
	jQuery('#muOutgoingCallsAbroadTable').find('tr').css("border", "none");
	jQuery('#muOutgoingCallsAbroadTable').find('tr').css("padding", "2px");
	jQuery('#muOutgoingCallsAbroadTable').find('tr').css("border-color", "white");
	jQuery('#muOutgoingCallsAbroadTable').find('input').css("width", "200px");
	jQuery('#muOutgoingCallsAbroadTable').find('th').css("width", "200px");
	jQuery('#muOutgoingCallsAbroadTable').find('td').css("width", "200px");
	jQuery('#muOutgoingCallsAbroadTable').find('select').css("width", "140px");
	jQuery('#muOutgoingCallsAbroadTable').find('input').css("border-color", "#e6ebf2");
	jQuery('#muOutgoingCallsAbroadTable').find('label').addClass("slds-form-element__label");
	jQuery('#muOutgoingCallsAbroadTable').find('input').css("text-align", "right");
}

/**
 * Shows incoming calls abroad custom table.
 */
function muShowIncomingCallsAbroad() {

	var muIncomingCallsAbroad = '<div id="muIncomingCallsAbroad"><table class="list" id="muIncomingCallsAbroadTable"><tr class="headerRow">'
		+ '<th class="firstColumn"><label>Incoming phonecalls abroad (Roaming MT)</label></th>'
		+ '<th><label>Minutes</label></th>'
		+ '<th><label>Gross Tariff</label></th>'
		+ '<th><label>Net Tariff</label></th>'
		+ '<th><label>Cost Tariff</label></th></tr>'
		+ '<tr><td><label>EU+</label></td>'
		+ '<td><input type="text" step="any" name="EuIncomingMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuIncomingGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuIncomingNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuIncomingCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>RoW</label></td>'
		+ '<td><input type="text" step="any" name="RowIncomingMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowIncomingGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowIncomingNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowIncomingCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Exceptions</label></td>'
		+ '<td><input type="text" step="any" name="ExceptionsIncomingMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsIncomingGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsIncomingNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsIncomingCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Total</label></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsIncomingMinutesTotal" class="slds-input"></td>'
		+ '</tr></table></div>';


	muIncomingCallsAbroad = jQuery(muIncomingCallsAbroad);

	if (jQuery('#muIncomingCallsAbroadTable')[0] == undefined) {
		jQuery('[data-cs-binding=IncomingCallsAbroadPart_0]').parent().parent().prepend(muIncomingCallsAbroad);
	}

	// TODO Move this to CSS.
	jQuery('#muIncomingCallsAbroadTable').find('td').css("width", "none");
	jQuery('#muIncomingCallsAbroadTable').find('th').css("border", "none");
	jQuery('#muIncomingCallsAbroadTable').find('th').css("border-color", "white");
	jQuery('#muIncomingCallsAbroadTable').find('th').css("padding", "3px");
	jQuery('#muIncomingCallsAbroadTable').find('td').css("border", "none");
	jQuery('#muIncomingCallsAbroadTable').find('td').css("border-color", "white");
	jQuery('#muIncomingCallsAbroadTable').find('tr').css("border", "none");
	jQuery('#muIncomingCallsAbroadTable').find('tr').css("padding", "2px");
	jQuery('#muIncomingCallsAbroadTable').find('tr').css("border-color", "white");
	jQuery('#muIncomingCallsAbroadTable').find('input').css("width", "200px");
	jQuery('#muIncomingCallsAbroadTable').find('th').css("width", "200px");
	jQuery('#muIncomingCallsAbroadTable').find('td').css("width", "200px");
	jQuery('#muIncomingCallsAbroadTable').find('select').css("width", "140px");
	jQuery('#muIncomingCallsAbroadTable').find('input').css("border-color", "#e6ebf2");
	jQuery('#muIncomingCallsAbroadTable').find('label').addClass("slds-form-element__label");
	jQuery('#muIncomingCallsAbroadTable').find('input').css("text-align", "right");
}

/**
 * Shows outgoing calls to international custom table.
 */
function muShowOutgoingCallsToInternational() {

	var muOutgoingCallsToInternational = '<div id="muOutgoingCallsToInternational"><table class="list" id="muOutgoingCallsToInternationalTable"><tr class="headerRow">'
		+ '<th class="firstColumn"><label>Outgoing phonecalls to international numbers (IDD)</label></th>'
		+ '<th><label>Minutes</label></th>'
		+ '<th><label>Gross Tariff</label></th>'
		+ '<th><label>Net Tariff</label></th>'
		+ '<th><label>Cost Tariff</label></th></tr>'
		+ '<tr><td><label>EU+</label></td>'
		+ '<td><input type="text" step="any" name="EuOutgoingMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuOutgoingGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuOutgoingNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="EuOutgoingCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>RoW</label></td>'
		+ '<td><input type="text" step="any" name="RowOutgoingMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowOutgoingGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowOutgoingNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="RowOutgoingCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Exceptions</label></td>'
		+ '<td><input type="text" step="any" name="ExceptionsOutgoingMinuten" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsOutgoingGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsOutgoingNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="ExceptionsOutgoingCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Total</label></td>'
		+ '<td><input type="text" step="any" disabled="" name="OutgoingCallsToInternationalMinutesTotal" class="slds-input"></td>'
		+ '</tr></table></div>';


	muOutgoingCallsToInternational = jQuery(muOutgoingCallsToInternational);

	if (jQuery('#muOutgoingCallsToInternationalTable')[0] == undefined) {
		jQuery('[data-cs-binding=OutgoingCallsToInternationalPart_0]').parent().parent().prepend(muOutgoingCallsToInternational);
	}

	// TODO Move this to CSS.
	jQuery('#muOutgoingCallsToInternationalTable').find('td').css("width", "none");
	jQuery('#muOutgoingCallsToInternationalTable').find('th').css("border", "none");
	jQuery('#muOutgoingCallsToInternationalTable').find('th').css("border-color", "white");
	jQuery('#muOutgoingCallsToInternationalTable').find('th').css("padding", "3px");
	jQuery('#muOutgoingCallsToInternationalTable').find('td').css("border", "none");
	jQuery('#muOutgoingCallsToInternationalTable').find('td').css("border-color", "white");
	jQuery('#muOutgoingCallsToInternationalTable').find('tr').css("border", "none");
	jQuery('#muOutgoingCallsToInternationalTable').find('tr').css("padding", "2px");
	jQuery('#muOutgoingCallsToInternationalTable').find('tr').css("border-color", "white");
	jQuery('#muOutgoingCallsToInternationalTable').find('input').css("width", "200px");
	jQuery('#muOutgoingCallsToInternationalTable').find('th').css("width", "200px");
	jQuery('#muOutgoingCallsToInternationalTable').find('td').css("width", "200px");
	jQuery('#muOutgoingCallsToInternationalTable').find('select').css("width", "140px");
	jQuery('#muOutgoingCallsToInternationalTable').find('input').css("border-color", "#e6ebf2");
	jQuery('#muOutgoingCallsToInternationalTable').find('label').addClass("slds-form-element__label");
	jQuery('#muOutgoingCallsToInternationalTable').find('input').css("text-align", "right");
}

/**
 * Shows SMS usage details custom table.
 */
function muShowSmsUsageDetails() {

	var muSmsUsageDetails = '<div id="muSmsUsageDetails"><table class="list" id="muSmsUsageDetailsTable"><tr class="headerRow">'
		+ '<th class="firstColumn"><label>SMS usage details</label></th>'
		+ '<th><label>Quantity</label></th>'
		+ '<th><label>Gross Tariff</label></th>'
		+ '<th><label>Net Tariff</label></th>'
		+ '<th><label>Cost Tariff</label></th></tr>'
		+ '<tr><td><label>SMS National</label></td>'
		+ '<td><input type="text" step="any" name="SmsNationalQuantity" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsNationalGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsNationalNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsNationalCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>SMS RMO EU+ (excl. NL)</label></td>'
		+ '<td><input type="text" step="any" name="SmsRmoEuQuantity" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsRmoEuGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsRmoEuNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsRmoEuCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>SMS RMO RoW</label></td>'
		+ '<td><input type="text" step="any" name="SmsRmoRowQuantity" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsRmoRowGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsRmoRowNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsRmoRowCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>SMS RMO Exceptions</label></td>'
		+ '<td><input type="text" step="any" name="SmsRmoExceptionsQuantity" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsRmoExceptionsGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsRmoExceptionsNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsRmoExceptionsCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>SMS IDD To EU+ (excl. NL)</label></td>'
		+ '<td><input type="text" step="any" name="SmsIddToEuQuantity" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsIddToEuGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsIddToEuNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsIddToEuCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>SMS IDD To RoW</label></td>'
		+ '<td><input type="text" step="any" name="SmsIddToRowQuantity" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsIddToRowGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsIddToRowNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsIddToRowCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>SMS IDD To Exceptions</label></td>'
		+ '<td><input type="text" step="any" name="SmsIddToExceptionsQuantity" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsIddToExceptionsGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsIddToExceptionsNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsIddToExceptionsCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Total</label></td>'
		+ '<td><input type="text" step="any" disabled="" name="SmsUsageDetailsQuantityTotal" class="slds-input"></td>'
		+ '</tr></table></div>';


	muSmsUsageDetails = jQuery(muSmsUsageDetails);

	if (jQuery('#muSmsUsageDetailsTable')[0] == undefined) {
		jQuery('[data-cs-binding=SmsUsageDetailsPart_0]').parent().parent().prepend(muSmsUsageDetails);
	}

	// TODO Move this to CSS.
	jQuery('#muSmsUsageDetailsTable').find('td').css("width", "none");
	jQuery('#muSmsUsageDetailsTable').find('th').css("border", "none");
	jQuery('#muSmsUsageDetailsTable').find('th').css("border-color", "white");
	jQuery('#muSmsUsageDetailsTable').find('th').css("padding", "3px");
	jQuery('#muSmsUsageDetailsTable').find('td').css("border", "none");
	jQuery('#muSmsUsageDetailsTable').find('td').css("border-color", "white");
	jQuery('#muSmsUsageDetailsTable').find('tr').css("border", "none");
	jQuery('#muSmsUsageDetailsTable').find('tr').css("padding", "2px");
	jQuery('#muSmsUsageDetailsTable').find('tr').css("border-color", "white");
	jQuery('#muSmsUsageDetailsTable').find('input').css("width", "200px");
	jQuery('#muSmsUsageDetailsTable').find('th').css("width", "200px");
	jQuery('#muSmsUsageDetailsTable').find('td').css("width", "200px");
	jQuery('#muSmsUsageDetailsTable').find('select').css("width", "140px");
	jQuery('#muSmsUsageDetailsTable').find('input').css("border-color", "#e6ebf2");
	jQuery('#muSmsUsageDetailsTable').find('label').addClass("slds-form-element__label");
	jQuery('#muSmsUsageDetailsTable').find('input').css("text-align", "right");
}

/**
 * Shows Data usage details custom table.
 */
function muShowDataUsageDetails() {

	var muDataUsageDetails = '<div id="muDataUsageDetails"><table class="list" id="muDataUsageDetailsTable"><tr class="headerRow">'
		+ '<th class="firstColumn"><label>Data usage details</label></th>'
		+ '<th><label>Mb</label></th>'
		+ '<th><label>Gross Tariff</label></th>'
		+ '<th><label>Net Tariff</label></th>'
		+ '<th><label>Cost Tariff</label></th></tr>'
		+ '<tr><td><label>Data Nationaal</label></td>'
		+ '<td><input type="text" step="any" name="DataNationalMb" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataNationalGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataNationalNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataNationalCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Data Roaming EU+ (Ecl. NL)</label></td>'
		+ '<td><input type="text" step="any" name="DataRoamingEuMb" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingEuGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingEuNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingEuCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Data Roaming Rest of World</label></td>'
		+ '<td><input type="text" step="any" name="DataRoamingRowMb" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingRowGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRaomingRowNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRaomingRowCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Data Roaming Exceptions</label></td>'
		+ '<td><input type="text" step="any" name="DataRoamingMb" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Data Roaming VF (excl. NL)</label></td>'
		+ '<td><input type="text" step="any" name="DataRoamingVFMb" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingVFGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingVFNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingVFCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Data Roaming Non-VF EU (excl. NL)</label></td>'
		+ '<td><input type="text" step="any" name="DataRoamingNonVFMb" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingNonVFGrossTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingNonVFNetTariff" class="slds-input"></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataRoamingNonVFCostTariff" class="slds-input"></td>'
		+ '</tr>'
		+ '<tr><td><label>Total</label></td>'
		+ '<td><input type="text" step="any" disabled="" name="DataUsageDetailsMbTotal" class="slds-input"></td>'
		+ '</tr></table></div>';


	muDataUsageDetails = jQuery(muDataUsageDetails);

	if (jQuery('#muDataUsageDetailsTable')[0] == undefined) {
		jQuery('[data-cs-binding=DataUsageDetailsPart_0]').parent().parent().prepend(muDataUsageDetails);
	}

	// TODO Move this to CSS.
	jQuery('#muDataUsageDetailsTable').find('td').css("width", "none");
	jQuery('#muDataUsageDetailsTable').find('th').css("border", "none");
	jQuery('#muDataUsageDetailsTable').find('th').css("border-color", "white");
	jQuery('#muDataUsageDetailsTable').find('th').css("padding", "3px");
	jQuery('#muDataUsageDetailsTable').find('td').css("border", "none");
	jQuery('#muDataUsageDetailsTable').find('td').css("border-color", "white");
	jQuery('#muDataUsageDetailsTable').find('tr').css("border", "none");
	jQuery('#muDataUsageDetailsTable').find('tr').css("padding", "2px");
	jQuery('#muDataUsageDetailsTable').find('tr').css("border-color", "white");
	jQuery('#muDataUsageDetailsTable').find('input').css("width", "200px");
	jQuery('#muDataUsageDetailsTable').find('th').css("width", "200px");
	jQuery('#muDataUsageDetailsTable').find('td').css("width", "200px");
	jQuery('#muDataUsageDetailsTable').find('select').css("width", "140px");
	jQuery('#muDataUsageDetailsTable').find('input').css("border-color", "#e6ebf2");
	jQuery('#muDataUsageDetailsTable').find('label').addClass("slds-form-element__label");
	jQuery('#muDataUsageDetailsTable').find('input').css("text-align", "right");
}

function clearAllMinutenValues() {

	if (CS.getAttributeValue('ClearMobileUsage_0') == true && jQuery('select[name=muQuoteStatus]').val() == 'netprofit') {

		var minutesDefaultValue = 0;

		muSetValueOfAttribute('WithinCustomerMinuten', minutesDefaultValue);

		muSetValueOfAttribute('ToFixedMinuten', minutesDefaultValue);

		muSetValueOfAttribute('ToMobileMinuten', minutesDefaultValue);

		muSetValueOfAttribute('ToMobileOutsideMinuten', minutesDefaultValue);

		muSetValueOfAttribute('NationalWithinVpnMinuten', minutesDefaultValue);

		muSetValueOfAttribute('NationalToVFMinuten', minutesDefaultValue);

		muSetValueOfAttribute('NationalToOthersMinuten', minutesDefaultValue);

		muSetValueOfAttribute('EuToEuMinuten', minutesDefaultValue);

		muSetValueOfAttribute('EuToRowMinuten', minutesDefaultValue);

		muSetValueOfAttribute('EuToExceptionsMinuten', minutesDefaultValue);

		// CS.setAttributeValue('TotalEuRmoMinutesExNational', minutesDefaultValue);

		muSetValueOfAttribute('RowToEuMinuten', minutesDefaultValue);

		muSetValueOfAttribute('RowToRowMinuten', minutesDefaultValue);

		muSetValueOfAttribute('RowToExceptionsMinuten', minutesDefaultValue);

		// CS.setAttributeValue('RowRmoMinutes', minutesDefaultValue);

		muSetValueOfAttribute('ExceptionsToEuMinuten', minutesDefaultValue);

		muSetValueOfAttribute('ExceptionsToRowMinuten', minutesDefaultValue);

		muSetValueOfAttribute('ExceptionsToExceptionsMinuten', minutesDefaultValue);

		muSetValueOfAttribute('EuIncomingMinuten', minutesDefaultValue);

		muSetValueOfAttribute('RowIncomingMinuten', minutesDefaultValue);

		muSetValueOfAttribute('ExceptionsIncomingMinuten', minutesDefaultValue);

		muSetValueOfAttribute('EuOutgoingMinuten', minutesDefaultValue);

		// CS.setAttributeValue('TotalEuIddMinutes', minutesDefaultValue);

		muSetValueOfAttribute('RowOutgoingMinuten', minutesDefaultValue);

		// CS.setAttributeValue('RowIddMinutes', minutesDefaultValue);

		muSetValueOfAttribute('ExceptionsOutgoingMinuten', minutesDefaultValue);

		muSetValueOfAttribute('SmsNationalQuantity', minutesDefaultValue);

		muSetValueOfAttribute('SmsRmoEuQuantity', minutesDefaultValue);

		muSetValueOfAttribute('SmsRmoRowQuantity', minutesDefaultValue);

		muSetValueOfAttribute('SmsRmoExceptionsQuantity', minutesDefaultValue);

		muSetValueOfAttribute('SmsIddToEuQuantity', minutesDefaultValue);

		muSetValueOfAttribute('SmsIddToRowQuantity', minutesDefaultValue);

		muSetValueOfAttribute('SmsIddToExceptionsQuantity', minutesDefaultValue);

		muSetValueOfAttribute('DataNationalMb', minutesDefaultValue);

		muSetValueOfAttribute('DataRoamingEuMb', minutesDefaultValue);

		muSetValueOfAttribute('DataRoamingRowMb', minutesDefaultValue);

		muSetValueOfAttribute('DataRoamingMb', minutesDefaultValue);

		muSetValueOfAttribute('DataRoamingVFMb', minutesDefaultValue);

		CS.setAttributeValue("DataUsageRoamingVF_0", minutesDefaultValue, true);

		muSetValueOfAttribute('DataRoamingNonVFMb', minutesDefaultValue);

		CS.setAttributeValue("DataUsageRoamingNonVF_0", minutesDefaultValue, true);

		var toStore = '';

		for (var key in allTableInformation) {
			toStore = toStore + key + "|" + allTableInformation[key] + ";";
		}

		CS.setAttributeValue("DataStorageHidden_0", toStore);

		CS.setAttributeValue('ClearMobileUsage_0', false);
	}
}

function loadMinutesWhenSacsrc() {
	if (jQuery('select[name=muQuoteStatus]').val() == 'sacsrc' && CS.Service.config[''].linkedObjectProperties.Mobile_Usage_Available__c == false) {
		if (allTableInformation['muPriceplanCombination'] != null && allTableInformation['muPriceplanCombination'] != '') {

			// Outgoing phonecalls within in The Netherlands (National MO)
			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'WithinCustomerMF');
			muSetValueOfAttribute('WithinCustomerMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'NationalToFixed');
			muSetValueOfAttribute('ToFixedMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'NationalToMobileVF');
			muSetValueOfAttribute('ToMobileMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'NationalToMobileOth');
			muSetValueOfAttribute('ToMobileOutsideMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'NationalPBXtoVPN');
			muSetValueOfAttribute('NationalWithinVpnMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'NationalPBXtoVF');
			muSetValueOfAttribute('NationalToVFMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'NationalPBXtoOth');
			muSetValueOfAttribute('NationalToOthersMinuten', minutesDefaultValue);

			var totalEuRmoMinutesExNational = 0;

			// Outgoing phonecalls abroad (Roaming MO)
			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMOEUEU');
			muSetValueOfAttribute('EuToEuMinuten', minutesDefaultValue);

			totalEuRmoMinutesExNational = totalEuRmoMinutesExNational + minutesDefaultValue;

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMOEURoW');
			muSetValueOfAttribute('EuToRowMinuten', minutesDefaultValue);

			totalEuRmoMinutesExNational = totalEuRmoMinutesExNational + minutesDefaultValue;

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMOEUExceptions');
			muSetValueOfAttribute('EuToExceptionsMinuten', minutesDefaultValue);

			totalEuRmoMinutesExNational = totalEuRmoMinutesExNational + minutesDefaultValue;

			muSetValueOfAttribute('totalEuRmoMinutesExNational', totalEuRmoMinutesExNational);

			// CS.setAttributeValue('TotalEuRmoMinutesExNational', totalEuRmoMinutesExNational);

			var sumRowRmoMinutes = 0;

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMORoWEU');
			muSetValueOfAttribute('RowToEuMinuten', minutesDefaultValue);

			sumRowRmoMinutes = sumRowRmoMinutes + minutesDefaultValue;

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMORoWRoW');
			muSetValueOfAttribute('RowToRowMinuten', minutesDefaultValue);

			sumRowRmoMinutes = sumRowRmoMinutes + minutesDefaultValue;

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMORoWExcepioins');
			muSetValueOfAttribute('RowToExceptionsMinuten', minutesDefaultValue);

			sumRowRmoMinutes = sumRowRmoMinutes + minutesDefaultValue;

			muSetValueOfAttribute('sumRowRmoMinutes', sumRowRmoMinutes);

			// CS.setAttributeValue('RowRmoMinutes', sumRowRmoMinutes);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMOExceptionEU');
			muSetValueOfAttribute('ExceptionsToEuMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMOExceptionsRoW');
			muSetValueOfAttribute('ExceptionsToRowMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMOExceptionsExcepti');
			muSetValueOfAttribute('ExceptionsToExceptionsMinuten', minutesDefaultValue);

			// Incoming phonecalls abroad (Roaming MT)
			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMTEU');
			muSetValueOfAttribute('EuIncomingMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMTRoW');
			muSetValueOfAttribute('RowIncomingMinuten', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'RMTExceptions');
			muSetValueOfAttribute('ExceptionsIncomingMinuten', minutesDefaultValue);

			// Outgoing phonecalls to international numbers (IDD)
			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'IDDEU');
			muSetValueOfAttribute('EuOutgoingMinuten', minutesDefaultValue);
			// CS.setAttributeValue('TotalEuIddMinutes', minutesDefaultValue);	

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'IDDRow');
			muSetValueOfAttribute('RowOutgoingMinuten', minutesDefaultValue);


			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'IDDExceptions');
			muSetValueOfAttribute('ExceptionsOutgoingMinuten', minutesDefaultValue);

			// SMS usage details
			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'SMSNational');
			muSetValueOfAttribute('SmsNationalQuantity', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'SMSRMOSMSEU');
			muSetValueOfAttribute('SmsRmoEuQuantity', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'SMSRMOSMSRoW');
			muSetValueOfAttribute('SmsRmoRowQuantity', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'SMSRMOSMSExceptions');
			muSetValueOfAttribute('SmsRmoExceptionsQuantity', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'SMSIDDSMSEU');
			muSetValueOfAttribute('SmsIddToEuQuantity', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'SMSIDDSMSRoW');
			muSetValueOfAttribute('SmsIddToRowQuantity', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'SMSIDDSMSExceptions');
			muSetValueOfAttribute('SmsIddToExceptionsQuantity', minutesDefaultValue);

			// Data usage details
			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'DataNational');
			muSetValueOfAttribute('DataNationalMb', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'DataRoamingEU');
			muSetValueOfAttribute('DataRoamingEuMb', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'DataRoamingRoW');
			muSetValueOfAttribute('DataRoamingRowMb', minutesDefaultValue);

			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'DataRoamingExpection');
			muSetValueOfAttribute('DataRoamingMb', minutesDefaultValue);

			// TODO needs to be updated for IOT
			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'DataRoamingExpection');
			muSetValueOfAttribute('DataRoamingVFMb', minutesDefaultValue);
			CS.setAttributeValue("DataUsageRoamingVF_0", minutesDefaultValue, true);

			// TODO needs to be updated for IOT
			var minutesDefaultValue = getMinutesDefaults(allTableInformation['muPriceplanCombination'], 'DataRoamingExpection');
			muSetValueOfAttribute('DataRoamingNonVFMb', minutesDefaultValue);
			CS.setAttributeValue("DataUsageRoamingNonVF_0", minutesDefaultValue, true);
		}
	}
}

window.getNumberOfMobileAddonsPerCategory = function getNumberOfMobileAddonsPerCategory(category) {
	var addonsAllIds = CS.getAttributeValue("Mobile_addons_0");
	if (addonsAllIds == '') {
		return 0;
	} else {
		var eachAddonId = addonsAllIds.split(",");
		var count = 0;
		for (var i = 0; i < eachAddonId.length; i++) {
			if (eachAddonId[i] != '' && CS.lookupRecords[eachAddonId[i]]['mobile_add_on_category__c'] == category) {
				count++;
			}
		}
		return count;
	}

}

function parseFloatAndRepresentNullAsZero(value) {
	var parsedFloat = parseFloat(value);
	if (isNaN(parsedFloat)) {
		return 0;
	}
	return parsedFloat;
}


function calculateTotal() {
	var totalOutgoingWithinNetherlands = parseFloatAndRepresentNullAsZero(allTableInformation['WithinCustomerMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['ToFixedMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['ToMobileMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['ToMobileOutsideMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['NationalWithinVpnMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['NationalToVFMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['NationalToOthersMinuten']);
	muSetValueOfAttribute('OutgoingCallsMinutesTotal', totalOutgoingWithinNetherlands);

	var totalOutgoingAbroad = parseFloatAndRepresentNullAsZero(allTableInformation['EuToEuMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['EuToRowMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['EuToExceptionsMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['RowToEuMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['RowToRowMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['RowToExceptionsMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['ExceptionsToEuMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['ExceptionsToRowMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['ExceptionsToExceptionsMinuten']);
	muSetValueOfAttribute('OutgoingCallsAbroadMinutesTotal', totalOutgoingAbroad);

	var totalIncomingAbroad = parseFloatAndRepresentNullAsZero(allTableInformation['EuIncomingMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['RowIncomingMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['ExceptionsIncomingMinuten']);
	muSetValueOfAttribute('ExceptionsIncomingMinutesTotal', totalIncomingAbroad);

	var outgoingInternationalNumbers = parseFloatAndRepresentNullAsZero(allTableInformation['EuOutgoingMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['RowOutgoingMinuten']) + parseFloatAndRepresentNullAsZero(allTableInformation['ExceptionsOutgoingMinuten']);
	muSetValueOfAttribute('OutgoingCallsToInternationalMinutesTotal', outgoingInternationalNumbers);

	var smsUsageDetails = parseFloatAndRepresentNullAsZero(allTableInformation['SmsNationalQuantity']) + parseFloatAndRepresentNullAsZero(allTableInformation['SmsRmoEuQuantity']) + parseFloatAndRepresentNullAsZero(allTableInformation['SmsRmoRowQuantity']) + parseFloatAndRepresentNullAsZero(allTableInformation['SmsRmoExceptionsQuantity']) + parseFloatAndRepresentNullAsZero(allTableInformation['SmsIddToEuQuantity']) + parseFloatAndRepresentNullAsZero(allTableInformation['SmsIddToRowQuantity']) + parseFloatAndRepresentNullAsZero(allTableInformation['SmsIddToExceptionsQuantity']);
	muSetValueOfAttribute('SmsUsageDetailsQuantityTotal', smsUsageDetails);

	var dataUsageDetails = parseFloatAndRepresentNullAsZero(allTableInformation['DataNationalMb']) + parseFloatAndRepresentNullAsZero(allTableInformation['DataRoamingEuMb']) + parseFloatAndRepresentNullAsZero(allTableInformation['DataRoamingRowMb']) + parseFloatAndRepresentNullAsZero(allTableInformation['DataRoamingMb']) + parseFloatAndRepresentNullAsZero(allTableInformation['DataRoamingVFMb']) + parseFloatAndRepresentNullAsZero(allTableInformation['DataRoamingNonVFMb']);
	muSetValueOfAttribute('DataUsageDetailsMbTotal', dataUsageDetails);
	CS.setAttributeValue('DataUsageMbTotal_0', dataUsageDetails, true);
}

//var numberOfRowAddon = getNumberOfMobileAddonsPerCategory('RoW shared data bundle');
//var numberOfEuAddon = getNumberOfMobileAddonsPerCategory('EU shared data bundle');
//var numberOfOtherAddon = getNumberOfAddons() - numberOfRowAddon - numberOfEuAddon;
var hardcodedPriceItemCode = '';


function calculateGrossTariff() {
	if (CS.Service.config['Mobile_CTN_subscription_0:Price_item_group_name_0'] != null && CS.Service.config['Mobile_CTN_subscription_0:Price_item_group_name_0']['attr'] != null) {
		var hardcodedPriceItemCode = CS.Service.config['Mobile_CTN_subscription_0:Price_item_group_name_0']['attr']["cscfga__Value__c"]; //RCBOONEMOB3
	}
	var numberOfEuAddon = getNumberOfMobileAddonsPerCategory('EU shared data bundle');
	if (numberOfEuAddon > 0) {
		// Outgoing phonecalls within in The Netherlands (National MO)	Minutes
		var WithinCustomerGrossTariff = getNmoTariffs(hardcodedPriceItemCode, 'PiekMobileLand');
		muSetValueOfAttribute('WithinCustomerGrossTariff', WithinCustomerGrossTariff);

		var ToFixedGrossTariff = getNmoTariffs(hardcodedPriceItemCode, 'PiekMobileOther');
		muSetValueOfAttribute('ToFixedGrossTariff', ToFixedGrossTariff);

		var ToMobileGrossTariff = getNmoTariffs(hardcodedPriceItemCode, 'DalMobileOther');
		muSetValueOfAttribute('ToMobileGrossTariff', ToMobileGrossTariff);

		var ToMobileOutsideGrossTariff = getNmoTariffs(hardcodedPriceItemCode, 'PiekOnMobileMobile');
		muSetValueOfAttribute('ToMobileOutsideGrossTariff', ToMobileOutsideGrossTariff);

		var NationalWithinVpnGrossTariff = getNmoTariffs(hardcodedPriceItemCode, 'PiekOnPabx');
		muSetValueOfAttribute('NationalWithinVpnGrossTariff', NationalWithinVpnGrossTariff);

		var NationalToVFGrossTariff = getNmoTariffs(hardcodedPriceItemCode, 'PiekPabxMobileOwn');
		muSetValueOfAttribute('NationalToVFGrossTariff', NationalToVFGrossTariff);

		var NationalToOthersGrossTariff = getNmoTariffs(hardcodedPriceItemCode, 'PiekPabxMobileOther');
		muSetValueOfAttribute('NationalToOthersGrossTariff', NationalToOthersGrossTariff);

		// Outgoing phonecalls abroad (Roaming MO)
		var EuToEuGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'EuropaPassport');
		muSetValueOfAttribute('EuToEuGrossTariff', EuToEuGrossTariff);

		// Incoming phonecalls abroad (Roaming MT)
		var EuIncomingGrossTariff = getRmtTariffs(hardcodedPriceItemCode, 'EuropaPassport');
		muSetValueOfAttribute('EuIncomingGrossTariff', EuIncomingGrossTariff);

		// Outgoing phonecalls to international numbers (IDD)
		var EuOutgoingGrossTariff = getIddTariffs(hardcodedPriceItemCode, 'Buurlanden');
		muSetValueOfAttribute('EuOutgoingGrossTariff', EuOutgoingGrossTariff);

		// SMS usage details
		var SmsNationalGrossTariff = getNmoTariffs(hardcodedPriceItemCode, 'SMS');
		muSetValueOfAttribute('SmsNationalGrossTariff', SmsNationalGrossTariff);

		var SmsRmoEuGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'RMO_SMS_EU_exclNL');
		muSetValueOfAttribute('SmsRmoEuGrossTariff', SmsRmoEuGrossTariff);

		var SmsIddToEuGrossTariff = getIddTariffs(hardcodedPriceItemCode, 'IDD_SMS_ToEU_exclNL');
		muSetValueOfAttribute('SmsIddToEuGrossTariff', SmsIddToEuGrossTariff);

		// Data usage details
		var DataNationalGrossTariff = getNmoTariffs(hardcodedPriceItemCode, 'DataNationaal');
		muSetValueOfAttribute('DataNationalGrossTariff', DataNationalGrossTariff);

		var DataRoamingEuGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'RMO_Data_EU');
		muSetValueOfAttribute('DataRoamingEuGrossTariff', DataRoamingEuGrossTariff);

		var DataRoamingVFGrossTariff = 0;
		muSetValueOfAttribute('DataRoamingVFGrossTariff', DataRoamingVFGrossTariff);

		var DataRoamingNonVFGrossTariff = 0;
		muSetValueOfAttribute('DataRoamingNonVFGrossTariff', DataRoamingNonVFGrossTariff);
	} else {
		// Outgoing phonecalls within in The Netherlands (National MO)	Minutes
		var WithinCustomerGrossTariff = 0;
		muSetValueOfAttribute('WithinCustomerGrossTariff', WithinCustomerGrossTariff);

		var ToFixedGrossTariff = 0;
		muSetValueOfAttribute('ToFixedGrossTariff', ToFixedGrossTariff);

		var ToMobileGrossTariff = 0;
		muSetValueOfAttribute('ToMobileGrossTariff', ToMobileGrossTariff);

		var ToMobileOutsideGrossTariff = 0;
		muSetValueOfAttribute('ToMobileOutsideGrossTariff', ToMobileOutsideGrossTariff);

		var NationalWithinVpnGrossTariff = 0;
		muSetValueOfAttribute('NationalWithinVpnGrossTariff', NationalWithinVpnGrossTariff);

		var NationalToVFGrossTariff = 0;
		muSetValueOfAttribute('NationalToVFGrossTariff', NationalToVFGrossTariff);

		var NationalToOthersGrossTariff = 0;
		muSetValueOfAttribute('NationalToOthersGrossTariff', NationalToOthersGrossTariff);

		// Outgoing phonecalls abroad (Roaming MO)
		var EuToEuGrossTariff = 0;
		muSetValueOfAttribute('EuToEuGrossTariff', EuToEuGrossTariff);

		// Incoming phonecalls abroad (Roaming MT)
		var EuIncomingGrossTariff = 0;
		muSetValueOfAttribute('EuIncomingGrossTariff', EuIncomingGrossTariff);

		// Outgoing phonecalls to international numbers (IDD)
		var EuOutgoingGrossTariff = 0;
		muSetValueOfAttribute('EuOutgoingGrossTariff', EuOutgoingGrossTariff);

		// SMS usage details
		var SmsNationalGrossTariff = 0;
		muSetValueOfAttribute('SmsNationalGrossTariff', SmsNationalGrossTariff);

		var SmsRmoEuGrossTariff = 0;
		muSetValueOfAttribute('SmsRmoEuGrossTariff', SmsRmoEuGrossTariff);

		var SmsIddToEuGrossTariff = 0;
		muSetValueOfAttribute('SmsIddToEuGrossTariff', SmsIddToEuGrossTariff);

		// Data usage details
		var DataNationalGrossTariff = 0;
		muSetValueOfAttribute('DataNationalGrossTariff', DataNationalGrossTariff);

		var DataRoamingEuGrossTariff = 0;
		muSetValueOfAttribute('DataRoamingEuGrossTariff', DataRoamingEuGrossTariff);

		var DataRoamingVFGrossTariff = 0;
		muSetValueOfAttribute('DataRoamingVFGrossTariff', DataRoamingVFGrossTariff);

		var DataRoamingNonVFGrossTariff = 0;
		muSetValueOfAttribute('DataRoamingNonVFGrossTariff', DataRoamingNonVFGrossTariff);
	}
	var numberOfRowAddon = getNumberOfMobileAddonsPerCategory('RoW shared data bundle');
	if (numberOfRowAddon > 0) {
		var EuToRowGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'EurotariffEuropa');
		muSetValueOfAttribute('EuToRowGrossTariff', EuToRowGrossTariff);

		var RowToEuGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'EuropaOtherWorld');
		muSetValueOfAttribute('RowToEuGrossTariff', RowToEuGrossTariff);

		var RowToRowGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'EurotariffWorld');
		muSetValueOfAttribute('RowToRowGrossTariff', RowToRowGrossTariff);

		var RowIncomingGrossTariff = getRmtTariffs(hardcodedPriceItemCode, 'USACanada');
		muSetValueOfAttribute('RowIncomingGrossTariff', RowIncomingGrossTariff);

		var RowOutgoingGrossTariff = getIddTariffs(hardcodedPriceItemCode, 'NoordAmerika');
		muSetValueOfAttribute('RowOutgoingGrossTariff', RowOutgoingGrossTariff);

		var SmsRmoRowGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'RMO_SMS_RoW');
		muSetValueOfAttribute('SmsRmoRowGrossTariff', SmsRmoRowGrossTariff);

		var SmsIddToRowGrossTariff = getIddTariffs(hardcodedPriceItemCode, 'IDD_SMS_ToRoW');
		muSetValueOfAttribute('SmsIddToRowGrossTariff', SmsIddToRowGrossTariff);

		var DataRoamingRowGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'RMO_Data_RoW');
		muSetValueOfAttribute('DataRoamingRowGrossTariff', DataRoamingRowGrossTariff);
	} else {
		var EuToRowGrossTariff = 0;
		muSetValueOfAttribute('EuToRowGrossTariff', EuToRowGrossTariff);

		var RowToEuGrossTariff = 0;
		muSetValueOfAttribute('RowToEuGrossTariff', RowToEuGrossTariff);

		var RowToRowGrossTariff = 0;
		muSetValueOfAttribute('RowToRowGrossTariff', RowToRowGrossTariff);

		var RowIncomingGrossTariff = 0;
		muSetValueOfAttribute('RowIncomingGrossTariff', RowIncomingGrossTariff);

		var RowOutgoingGrossTariff = 0;
		muSetValueOfAttribute('RowOutgoingGrossTariff', RowOutgoingGrossTariff);

		var SmsRmoRowGrossTariff = 0;
		muSetValueOfAttribute('SmsRmoRowGrossTariff', SmsRmoRowGrossTariff);

		var SmsIddToRowGrossTariff = 0;
		muSetValueOfAttribute('SmsIddToRowGrossTariff', SmsIddToRowGrossTariff);

		var DataRoamingRowGrossTariff = 0;
		muSetValueOfAttribute('DataRoamingRowGrossTariff', DataRoamingRowGrossTariff);
	}

	var EuToExceptionsGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'EuropaOtherEuropa');
	muSetValueOfAttribute('EuToExceptionsGrossTariff', EuToExceptionsGrossTariff);

	var RowToExceptionsGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'USACanadaEurope');
	muSetValueOfAttribute('RowToExceptionsGrossTariff', RowToExceptionsGrossTariff);

	var ExceptionsToEuGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'USACanadaWorld');
	muSetValueOfAttribute('ExceptionsToEuGrossTariff', ExceptionsToEuGrossTariff);

	var ExceptionsToRowGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'Others');
	muSetValueOfAttribute('ExceptionsToRowGrossTariff', ExceptionsToRowGrossTariff);

	var ExceptionsToExceptionsGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'Uitzonderingen');
	muSetValueOfAttribute('ExceptionsToExceptionsGrossTariff', ExceptionsToExceptionsGrossTariff);

	var ExceptionsIncomingGrossTariff = getRmtTariffs(hardcodedPriceItemCode, 'Uitzonderingen');
	muSetValueOfAttribute('ExceptionsIncomingGrossTariff', ExceptionsIncomingGrossTariff);

	var ExceptionsOutgoingGrossTariff = getIddTariffs(hardcodedPriceItemCode, 'RestWorld');
	muSetValueOfAttribute('ExceptionsOutgoingGrossTariff', ExceptionsOutgoingGrossTariff);

	var SmsRmoExceptionsGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'RMO_SMS_Exceptions');
	muSetValueOfAttribute('SmsRmoExceptionsGrossTariff', SmsRmoExceptionsGrossTariff);

	var SmsIddToExceptionsGrossTariff = getIddTariffs(hardcodedPriceItemCode, 'IDD_SMS_ToExceptions');
	muSetValueOfAttribute('SmsIddToExceptionsGrossTariff', SmsIddToExceptionsGrossTariff);

	var DataRoamingGrossTariff = getRmoTariffs(hardcodedPriceItemCode, 'RMO_Data_Exceptions');
	muSetValueOfAttribute('DataRoamingGrossTariff', DataRoamingGrossTariff);

}

function calculateMaf() {
	if (CS.Service.config['Mobile_CTN_subscription_0:Price_item_group_name_0'] != null && CS.Service.config['Mobile_CTN_subscription_0:Price_item_group_name_0']['attr'] != null) {
		var hardcodedPriceItemCode = CS.Service.config['Mobile_CTN_subscription_0:Price_item_group_name_0']['attr']["cscfga__Value__c"]; //RCBOONEMOB3
	}
	var today = new Date();
	var contractPeriod = CS.getAttributeValue('Duration_0');
	for (j in PP_MAFCalculations) {
		if (j == hardcodedPriceItemCode) {
			if (contractPeriod >= PP_MAFCalculations[j][0]['ContractPeriodFrom'] && contractPeriod < PP_MAFCalculations[j][0]['ContractPeriodTo']) {
				if (today >= Date.parse(PP_MAFCalculations[j][0]['validFrom']) && today < Date.parse(PP_MAFCalculations[j][0]['validTo'])) {
					muSetValueOfAttribute('muMafGross', PP_MAFCalculations[j][0]['MonthlyAccessFee']);
				}
			}
		}
	}
}

function getMinutesDefaults(name, columnName) {
	for (j in PP_MinuteDefaults) {
		if (j == name) {
			return parseFloat(PP_MinuteDefaults[j][columnName].toString().replace(',', '.'));
		}
	}
}

function getNmoTariffs(name, columnName) {
	for (j in PP_NmoTariffs) {
		if (j == name) {
			return parseFloat(PP_NmoTariffs[j][columnName].toString().replace(',', '.'));
		}
	}
}

function getRmoTariffs(name, columnName) {
	for (j in PP_RmoTariffs) {
		if (j == name) {
			return parseFloat(PP_RmoTariffs[j][columnName].toString().replace(',', '.'));
		}
	}
}

function getCostPrices(name, columnName) {
	for (j in Cost_Prices) {
		if (j == name) {
			return parseFloat(Cost_Prices[j][0][columnName].toString().replace(',', '.'));
		}
	}
}

function getIddTariffs(name, columnName) {
	for (j in PP_IddTariffs) {
		if (j == name) {
			return parseFloat(PP_IddTariffs[j][columnName].toString().replace(',', '.'));
		}
	}
}

function getRmtTariffs(name, columnName) {
	for (j in PP_RmtTariffs) {
		if (j == name) {
			console.log(PP_RmtTariffs[j]);
			return parseFloat(PP_RmtTariffs[j][columnName].toString().replace(',', '.'));
		}
	}
}

/**
 * Main render class. Shows all table/parts.
 */
function renderCustomScreen() {

	if (CS.Service.getCurrentScreenRef() == "Mobile_CTN_profile:Mobile_usage") {
		hideAllCostColumns();

		muExecuteAllCalculations();
		if (CS.getAttributeValue('Mobile_CTN_subscription_0') !== undefined && CS.getAttributeValue('Mobile_CTN_subscription_0') != '') {
			jQuery('[name="muProposition"]').val(CS.Service.config["Mobile_CTN_subscription_0"].attr.cscfga__Display_Value__c);
			allTableInformation['muProposition'] = CS.Service.config["Mobile_CTN_subscription_0"].attr.cscfga__Display_Value__c;
			if (CS.Service.config['Mobile_CTN_subscription_0:Price_item_group_name_0'] != null && CS.Service.config['Mobile_CTN_subscription_0:Price_item_group_name_0']['attr'] != null) {
				hardcodedPriceItemCode = CS.Service.config['Mobile_CTN_subscription_0:Price_item_group_name_0']['attr']["cscfga__Value__c"];
			}
			var numberOfRowAddon = getNumberOfMobileAddonsPerCategory('RoW shared data bundle');
			var numberOfEuAddon = getNumberOfMobileAddonsPerCategory('EU shared data bundle');
			var numberOfOtherAddon = getNumberOfAddons() - numberOfRowAddon - numberOfEuAddon;

			var pricePlanCombination = '';

			if (allTableInformation['muProposition'] != null && allTableInformation['muProposition'].toLowerCase().indexOf('one mobile') > -1) {
				pricePlanCombination = 'ONE Mobile';
			}

			if (allTableInformation['muProposition'] != null && allTableInformation['muProposition'].toLowerCase().indexOf('one business') > -1) {
				pricePlanCombination = 'ONE Business';
			}

			if (allTableInformation['muProposition'] != null && allTableInformation['muProposition'].toLowerCase().indexOf('flex mobile') > -1) {
				pricePlanCombination = 'Flex Mobile';
			}

			if (pricePlanCombination != '') {
				if (numberOfEuAddon > 0) {
					pricePlanCombination = pricePlanCombination + '+EUAddon';
				}

				if (numberOfRowAddon > 0) {
					pricePlanCombination = pricePlanCombination + '+RoWAddon';
				}
			}

			jQuery('[name="muPriceplanCombination"]').val(pricePlanCombination);
			allTableInformation['muPriceplanCombination'] = pricePlanCombination;

		}

		setQuoteStatus();

		//Additions
		fillCommitment();
		fillCostPrices();

		muShowGeneralInformation();

		muShowOutgoingCallsNetherlands();
		muShowOutgoingCallsAbroad();
		muShowIncomingCallsAbroad();
		muShowOutgoingCallsToInternational();
		muShowSmsUsageDetails();

		muShowDataUsageDetails();

		jQuery('.firstColumn').find('label').css("width", "360px");

		muExecuteAllCalculations();
		myQuoteStatusChanged();

		if (CS.getAttributeValue('Mobile_CTN_subscription_0') !== undefined && CS.getAttributeValue('Mobile_CTN_subscription_0') != '') {
			oneBusinessIotSelected();
			loadMinutesWhenSacsrc();
			calculateGrossTariff();
			calculateNetTariff();
			calculateTotal();
			calculateMaf();
			muCheckIfAllValuesAreNumerical();
			checkNationalUsage();

			if (jQuery('select[name=muQuoteStatus]').val() == 'SACSRC') {
				CS.setAttributeValue('ClearMobileUsage_0', true);
			}

			//validateUniqueConnectionType();

			storeData();
		}
		removeRelatedProductButtons(['Mobile_Usage_Product_'],['All']);
		jQuery("button[data-cs-ref='Mobile_Usage_Product_0']").remove();
	}
	else {
		removeRelatedProductNewButton();
		removeRelatedEditDelCopy();
		removeRelatedProductButtons(['Mobile_CTN_addons_'], ['All']);
		removeSohoHardware();
	}

	if (CS.getAttributeValue('Scenario_0') == 'Flex Mobile')
		jQuery('label[data-cs-label ="Add_Basic_VPN_0"]').closest('td').hide();
	else
		jQuery('label[data-cs-label ="Add_Basic_VPN_0"]').closest('td').show();
	
	var typeOfSalesChannel = CS.getAttributeValue('DirectIndirect_0');

	if (typeOfSalesChannel == 'Indirect'){
		disableHardwareSelection();
	}
}

function fillCostPrices() {
	muSetValueOfAttribute('WithinCustomerCostTariff', getCostPrices('NMO_NationalToFixed_Off-Net', 'CostPrice'));
	muSetValueOfAttribute('ToFixedCostTariff', getCostPrices('NMO_NationalToMobile _Off-Net', 'CostPrice'));

	muSetValueOfAttribute('ToMobileCostTariff', getCostPrices('NMO_NationalToMobileOtherVF_OutsideCustomer', 'CostPrice'));
	muSetValueOfAttribute('ToMobileOutsideCostTariff', getCostPrices('NMO_National_On-Net', 'CostPrice'));
	muSetValueOfAttribute('NationalWithinVpnCostTariff', getCostPrices('NMO_NationalFromPBXWithinVPN_On-Net', 'CostPrice'));
	muSetValueOfAttribute('NationalToVFCostTariff', getCostPrices('NMO_NationalFromPBXToVF_ Off-Net', 'CostPrice'));
	muSetValueOfAttribute('NationalToOthersCostTariff', getCostPrices('NMO_NationalFromPBXToOtherOperators_Off-Net', 'CostPrice'));

	muSetValueOfAttribute('EuToEuCostTariff', getCostPrices('RMO_EU_exclNL_ToEU_inclNL', 'CostPrice'));
	muSetValueOfAttribute('EuToRowCostTariff', getCostPrices('RMO_EU_exclNL_ToRoW', 'CostPrice'));
	muSetValueOfAttribute('EuToExceptionsCostTariff', getCostPrices('RMO_EU _exclNL_ToExceptions', 'CostPrice'));
	muSetValueOfAttribute('RowToEuCostTariff', getCostPrices('RMO_RoWToEU_inclNL', 'CostPrice'));
	muSetValueOfAttribute('RowToRowCostTariff', getCostPrices('RMO_RoWToRoW', 'CostPrice'));
	muSetValueOfAttribute('RowToExceptionsCostTariff', getCostPrices('RMO_RoWToExceptions', 'CostPrice'));
	muSetValueOfAttribute('ExceptionsToEuCostTariff', getCostPrices('RMO_ExceptionsToEU_inclNL', 'CostPrice'));
	muSetValueOfAttribute('ExceptionsToRowCostTariff', getCostPrices('RMO_ExceptionsToRoW', 'CostPrice'));
	muSetValueOfAttribute('ExceptionsToExceptionsCostTariff', getCostPrices('RMO_ExceptionsToExceptions', 'CostPrice'));

	muSetValueOfAttribute('EuIncomingCostTariff', getCostPrices('RMT_EU_exclNL', 'CostPrice'));
	muSetValueOfAttribute('RowIncomingCostTariff', getCostPrices('RMT_RoW', 'CostPrice'));
	muSetValueOfAttribute('ExceptionsIncomingCostTariff', getCostPrices('RMT_Exceptions', 'CostPrice'));

	muSetValueOfAttribute('EuOutgoingCostTariff', getCostPrices('IDD_ToEU_exclNL', 'CostPrice'));
	muSetValueOfAttribute('RowOutgoingCostTariff', getCostPrices('IDD_ToRoW', 'CostPrice'));
	muSetValueOfAttribute('ExceptionsOutgoingCostTariff', getCostPrices('IDD_ToExceptions', 'CostPrice'));

	muSetValueOfAttribute('SmsNationalCostTariff', getCostPrices('SMS Nat.', 'CostPrice'));
	muSetValueOfAttribute('SmsRmoEuCostTariff', getCostPrices('SMS Roaming', 'CostPrice'));
	muSetValueOfAttribute('SmsRmoRowCostTariff', getCostPrices('SMS Roaming', 'CostPrice'));
	muSetValueOfAttribute('SmsRmoExceptionsCostTariff', getCostPrices('SMS Roaming', 'CostPrice'));
	muSetValueOfAttribute('SmsIddToEuCostTariff', getCostPrices('SMS IDD', 'CostPrice'));
	muSetValueOfAttribute('SmsIddToRowCostTariff', getCostPrices('SMS IDD', 'CostPrice'));
	muSetValueOfAttribute('SmsIddToExceptionsCostTariff', getCostPrices('SMS IDD', 'CostPrice'));

	muSetValueOfAttribute('DataNationalCostTariff', getCostPrices('DataNational', 'CostPrice'));
	muSetValueOfAttribute('DataRoamingEuCostTariff', getCostPrices('DataRMOEU', 'CostPrice'));
	muSetValueOfAttribute('DataRaomingRowCostTariff', getCostPrices('DataRMORoW', 'CostPrice'));
	muSetValueOfAttribute('DataRoamingCostTariff', getCostPrices('DataRMOExc', 'CostPrice'));
}

function setQuoteStatus() {
	jQuery('select[name=muQuoteStatus]').prop("disabled", false);
	if (CS.getAttributeValue('BasketQualification_0') == 'SAC SRC') {
		allTableInformation['muQuoteStatus'] = 'sacsrc'; // sacsrc
		jQuery('select[name=muQuoteStatus]').val('sacsrc');
	} else if (CS.getAttributeValue('BasketQualification_0') == 'Net profit') {
		allTableInformation['muQuoteStatus'] = 'netprofit';
		jQuery('select[name=muQuoteStatus]').val('netprofit');
	}
	jQuery('select[name=muQuoteStatus]').prop("disabled", true);
}

function fillCommitment() {
	muSetValueOfAttribute('muCommitment', CS.getAttributeValue('Duration_0'));
	jQuery('[name=muCommitment]').prop("disabled", true);
}

function validateUniqueConnectionType() {
	var seenConnectionType = [];
	if (CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts.length > 0) {
		for (var idx in CS.Service.config['Mobile_CTN_subscription_0'].relatedProducts) {
			var reference = CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts[idx].reference;
			if (reference != null) {
				if (seenConnectionType.includes(CS.Service.config[reference + ":Connection_type_0"]['attr'].cscfga__Value__c)) {
					CS.markConfigurationInvalid('Only one of each type of connection allowed for Subscription');
					return;
				}
				else {
					seenConnectionType.push(CS.Service.config[reference + ":Connection_type_0"]['attr'].cscfga__Value__c);
				}
			}
		}
	}

	if (seenConnectionType.length == 1 && seenConnectionType[0] == 'Disconnect')
		CS.markConfigurationInvalid('Please add some other type of connection for Subscription other than Disconnect');
}

function checkFlexMobileRoW() {
	var hasDaily = false;
	var hasPermanent = false;

	for (var i = 0; i < CS.Service.config["Mobile_CTN_addon_select_0"].relatedProducts.length; i++) {
		var currentGroup = CS.getAttributeValue('Mobile_CTN_addon_select_' + i + ':Product_group_0');
		if (currentGroup == 'Daily')
			hasDaily = true;
		else if (currentGroup == 'Permanent')
			hasPermanent = true;
	}

	if (hasDaily && hasPermanent)
		CS.markConfigurationInvalid('Daily and Permanent RoW products cannot be combined');

}

function calculateTotalCtnWithoutDisconnects() {
	var totalCount = 0;
	for (var i = 0; i < CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts.length; i++) {
		if (CS.getAttributeValue('Mobile_CTN_subscription_' + i + ':Connection_type_0') != 'Disconnect') {
			totalCount += CS.getAttributeValue('Mobile_CTN_subscription_' + i + ':CTN_quantity_0');
		}
	}
	CS.setAttributeValue('TotalCtnCountNoDisconnects_0', totalCount);
}

// only for OneMobile4
function setOneMonthDuration() {
	for (var i = 0; i < CS.Service.config["Mobile_CTN_addon_select_0"].relatedProducts.length; i++) {
		if (CS.getAttributeValue('Mobile_CTN_addon_select_' + i + ':Product_group_0') == 'OneMobile4OneMonth') {
			CS.setAttributeValue('Duration_0', 1);
			return;
		}
	}
}

function checkConnectionTypeForRedPro() {
    var scenario = CS.getAttributeValue('Scenario_0');
    if (scenario == redProScenarios.NewPorting || scenario == redProScenarios.RetentionMigration) {
        var counter = 0;
        var disallowedConnectionType = false;
        if(CS.Service.config['Mobile_CTN_subscription_0'].relatedProducts.length > 0) {
            _.each(CS.Service.config['Mobile_CTN_subscription_0'].relatedProducts,function(p){
                if (CS.Service.config['Mobile_CTN_subscription_0'].relatedProducts.length > counter) {
                    var subsConnectionType = CS.Service.config['Mobile_CTN_subscription_' + counter + ':Connection_type_0']['attr'].cscfga__Value__c;
                    counter++;
                    if (scenario == redProScenarios.NewPorting && (subsConnectionType == 'Retention' || subsConnectionType == 'Migration' )) {
                        disallowedConnectionType = true;
                    } else if (scenario == redProScenarios.RetentionMigration && (subsConnectionType == 'New' || subsConnectionType == 'Porting' )) {
                        disallowedConnectionType = true;
                    }
                }
            }); 
        }
        if (disallowedConnectionType && scenario == redProScenarios.NewPorting) {
            CS.markConfigurationInvalid('Connection type Retention or Migration is not allowed with connection type New or Porting. You need to add another Mobile CTN profile and configure subscription(s) with connection type New or Porting in there.');
        } else if (disallowedConnectionType && scenario == redProScenarios.RetentionMigration) {
            CS.markConfigurationInvalid('Connection type New or Porting is not allowed with connection type Retention or Migration. You need to add another Mobile CTN profile and configure subscription(s) with connection type Retention or Migration in there.');
        }
    }
}

function checkOneMobileRetentionOrMigration() {
	var scenarioOneMobile = CS.getAttributeValue('Scenario_0');
	if (scenarioOneMobile == 'OneMobile') {
		for (var i = 0; i < CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts.length; i++) {
			if ((CS.getAttributeValue('Mobile_CTN_subscription_' + i + ':Connection_type_0') == 'Retention') ||(CS.getAttributeValue('Mobile_CTN_subscription_' + i + ':Connection_type_0') == 'Migration')) {
				CS.markConfigurationInvalid('Subscriptions with connection type Retention or Migration are not allowed anymore.');
			}
		}
	}
	
}

// disable hardware addition for OM4 and FM
function disableHardwareSelection() {
	var scenario = CS.getAttributeValue('Scenario_0');
	if (CS.Service.getCurrentScreen().reference == 'Mobile_CTN_profile:Default_Screen' && (scenario == 'OneMobile4' || scenario == 'Flex Mobile')) {
		CS.displayInfo('Hardware can be added to this basket by selecting Zakelijke Toestelbetaling in the Add Product page');
		jQuery("button[data-cs-ref='Mobile_CTN_hardware_0']").remove();
		removeRelatedProduct("Mobile_CTN_hardware_0");
	}
}

function removeSohoHardware() {
	var duration = parseInt(CS.getAttributeValue('Duration_0'));
	if (duration != 24) {
		for (var i = 0; i < CS.Service.config['Mobile_CTN_hardware_0'].relatedProducts.length; i++) {
			var segmentValue = CS.getAttributeValue('Mobile_CTN_hardware_' + i +':Segment_0');
			
			if (segmentValue !== undefined && segmentValue.includes('SoHo')) {
				CS.Service.removeRelatedProduct('Mobile_CTN_hardware_' + i);
				i--;
			}
		}
	}
}

function checkToestelbetalingAndHardware() {
    var scenario = CS.getAttributeValue('Scenario_0');
    if (scenario == redProScenarios.NewPorting || scenario == redProScenarios.RetentionMigration) {
        var counterTSB = 0;
        hasToestelbetaling = false;
        hasHardware = false;
        if(CS.Service.config['Mobile_CTN_addon_select_0'].relatedProducts.length > 0) {
            _.each(CS.Service.config['Mobile_CTN_addon_select_0'].relatedProducts,function(p){
                if (CS.Service.config['Mobile_CTN_addon_select_0'].relatedProducts.length > counterTSB) {
                    var addOnType = CS.Service.config['Mobile_CTN_addon_select_' + counterTSB + ':Category_0']['attr'].cscfga__Value__c;
                    counterTSB++;
                    if (addOnType == 'Toestelbetaling' ) {
                        hasToestelbetaling = true;
                    }
                }
            }); 
        }
		
        if(CS.Service.config['Mobile_CTN_hardware_0'].relatedProducts.length > 0) {
            hasHardware = true;
        }
	
        if (hasToestelbetaling == true && hasHardware == false ) {
            CS.markConfigurationInvalid('You cannot have Toestelbetaling addon without a Hardware device. Please add a Hardware device.');
        }
    }
}