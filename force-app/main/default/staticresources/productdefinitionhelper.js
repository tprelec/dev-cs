
window.hideConfigurationQuantity = function hideConfigurationQuantity(){
   setTimeout(function(){jQuery("table[data-cs-control='Addons_0']").each(function() {
       jQuery(this).find('tr').each(function(){
           jQuery(this).find('th:nth-child(2)').hide();
           jQuery(this).find('td:nth-child(2)').hide();
       });
   })},0)
}

/**
* Flag that controlls if console logs will be shown or not.
*/
var logsEnabled = false;

var counter = 0;


/**
* Clean codec items and reset flags
* 
*/ 
window.cleanCodecItems = function cleanCodecItems(){
   if(CS.getAttributeValue('CodecRefresh_0') == 'set'){
       
       //remove related CPE
       removeRelatedProductAndClearFlag('CPE_0','CPE_set_0');
       //remove SIP
       removeRelatedProduct('SIP_Package_0');
       removeRelatedProductAndClearFlag('Bundle_Products_0','Bundle_set_0');
       
       CS.setAttributeValue('CodecRefresh_0', '');
   }
}


/**
* TODO: We can remove this. Moved to rules.
*/
function checkAndProcessModularPricingCheckbox() {
  var records = CS.lookupRecords;

  // 

  var hasIpvpn = false;
  var hasInternet = false;
  var hasOneFixed = false;
  var hasOneNet = false;

  var numberOfIpvpn = 0;
  _.each(CS.lookupRecords,function(p){
           console.log(p['name']);
           if (p['name'] == 'IPVPN') {
                 numberOfIpvpn++;
                 if (numberOfIpvpn > 1) {
                    hasIpvpn = true;
                 }
              
           } else if (p['name'] == 'Managed Internet') {
                 hasInternet = true;
           } else if (p['name'] == 'One Fixed') {
                 hasOneFixed = true;
           } else if (p['name'] == 'One Net') {
                 hasOneNet = true;
           }
     });

  if (hasIpvpn) {
     CS.setAttributeValue('Modular_pricing_0', true);
     jQuery('[name="Modular_pricing_0"]').prop("disabled", true);
  } else {
     if (hasInternet && !hasOneNet && !hasOneFixed) {
        jQuery('[name="Modular_pricing_0"]').prop("disabled", false);
     } else if (!hasInternet && !hasOneNet && !hasOneFixed) {
        CS.setAttributeValue('Modular_pricing_0', true);
        jQuery('[name="Modular_pricing_0"]').prop("disabled", true);
     } else {
        CS.setAttributeValue('Modular_pricing_0', false);
        jQuery('[name="Modular_pricing_0"]').prop("disabled", true);
     }
  }

}

/**
* Logging function.
*/
function log(logMessage) {
   if (logsEnabled === true) {
      console.log(logMessage);
   }
}

function removeRelatedProduct(relProductReference){
   if(CS.Service.config[relProductReference].relatedProducts.length > 0){ 
  _.each(CS.Service.config[relProductReference].relatedProducts,function(p){
           CS.Service.removeRelatedProduct(relProductReference);
     }); 
  } 
}

/**
* Sets the value of attribute that is not currency. 
*/
function setValueOfAttributeNotCurrency(attributeName, attributeValue) {
  log('setValueOfAttributeNotCurrency<==attributeName: ' + attributeName + ';attributeValue: ' + attributeValue);

  if (isNaN(attributeValue)) {
     attributeValue = 0;
  }

  jQuery('[name="'+ attributeName + '"]').val(attributeValue);
  allTableInformation[attributeName] = attributeValue;
}

/**
* Sets the value of attribute that is a currency.
*/ 
function setValueOfAttribute(attributeName, attributeValue) {
  log('setValueOfAttributeNotCurrency<==attributeName: ' + attributeName + ';attributeValue: ' + attributeValue);

  if (isNaN(attributeValue)) {
     attributeValue = 0;
  }

  attributeValue = returnFormatedDecimalNumber(attributeValue);

  jQuery('[name="'+ attributeName + '"]').val(attributeValue);
  allTableInformation[attributeName] = attributeValue;
}

/**
*	Formats decimal number so that it is two decimal points rounded and commas are added for larger numbers.
*/
function returnFormatedDecimalNumber(unformatedDecimalNumber) {
  log('returnFormatedDecimalNumber<==unformatedDecimalNumber: ' + unformatedDecimalNumber);
  unformatedDecimalNumber = unformatedDecimalNumber.toFixed(2);
  unformatedDecimalNumber = unformatedDecimalNumber.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
  log('returnFormatedDecimalNumber==>unformatedDecimalNumber: ' + unformatedDecimalNumber);
  return unformatedDecimalNumber;
}


/**
* Calculates Naar International Total Gross.
*/
function calculateNaarInternationaalTotalGross() {
  var totalGrossNlEu = allTableInformation['TotalGrossNlEu'];
  var totalGrossNlRow = allTableInformation['TotalGrossNlRow'];
  var totalGrossNlExc = allTableInformation['TotalGrossNlExc'];
  log('calculateNaarInternationaalTotalGross<==totalGrossNlEu: ' + totalGrossNlEu + ';totalGrossNlRow: ' + totalGrossNlRow + ';totalGrossNlExc: ' + totalGrossNlExc);

  var result = parseFloat(totalGrossNlEu.replace(/,/g, '')) + parseFloat(totalGrossNlRow.replace(/,/g, '')) + parseFloat(totalGrossNlExc.replace(/,/g, ''));
  log('calculateNaarInternationaalTotalGross==>TotalGrossNaarInter: ' + result);
  setValueOfAttribute('TotalGrossNaarInter', result);
}

/**
* Calculates Naar International Min Total.
*/
function calculateNaarInternationaalMinTotal() {
  var minTotalNlEu = allTableInformation['MinTotalNlEu'];
  var minTotalNlRow = allTableInformation['MinTotalNlRow'];
  var minTotalNlExc = allTableInformation['MinTotalNlExc'];
  log('calculateNaarInternationaalMinTotal<==minTotalNlEu: ' + minTotalNlEu + ';minTotalNlRow: ' + minTotalNlRow + ';minTotalNlExc: ' + minTotalNlExc);

  var result = parseFloat(minTotalNlEu) + parseFloat(minTotalNlRow) + parseFloat(minTotalNlExc);
  log('calculateNaarInternationaalTotalGross==>TotalGrossNaarInter: ' + result);
   if(CS.getAttributeValue('DDISectionEnabled_0')) {
      setValueOfAttributeNotCurrency('MinTotalNaarInter', Math.round(result));
   } else {
       calculateMinTotalNarrInter();
   }
}

function calculateMinTotalNarrInter() {
   var competitorInvoice = getCompetitorInvoice();
  var calls = parseFloat(allTableInformation['CallsNaarInter']);
  var hrs = parseFloat(allTableInformation['HrsNaarInter']);
  var min = parseFloat(allTableInformation['MinNaarInter']);
  var sec = parseFloat(allTableInformation['SecNaarInter']);

  var result = 0;

  if (sec > 29) {
     result = (hrs * 60) + min + 1;
  } else {
     result = (hrs * 60) + min;
  }
  
  setValueOfAttributeNotCurrency('MinTotalNaarInter', Math.round(result));
}

/**
* Calculates Flatfee Tariff values
*/
function calculateFlatfeeTariffValue() {
    
	log('calculateFlatfeeTariffValue<==');
	if (allTableInformation['flatFeeDropdown'] == 'payasyouuse') {
		log('calculateFlatfeeTariffValue==>Tariff: 0');
		setValueOfAttribute('Tariff', 0);
		//CS.setAttributeValue('Tariff_0', 0);
		// CS.setAttributeValue('CCode_0', 0);
	} else {
	    var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');
	    if(scenarioVal == 'One Fixed Express' && CS.getAttributeValue('Number_of_SIP_8_0') > 0 && CS.getAttributeValue('Number_of_SIP_4_0') > 0) {
           
	        try {
	            if(CS.Service.config['FlatFee_product_0'].relatedProducts.length > 2) {
        	        for (var idx in CS.Service.config['FlatFee_product_0'].relatedProducts) {
                		var partName = CS.Service.config['FlatFee_product_' + idx + ':PartName_0']['attr'].cscfga__Display_Value__c;
                		
                		if(partName == '') {
                			 CS.Service.removeRelatedProduct('FlatFee_product_' + idx);
                		}
                	}
               }
               
               if(CS.Service.config['FlatFee_product_0'].relatedProducts.length != 0)
               {
                     // WARNING - we rely on a sequence of items in the related list
                  // sequence is defined in scenarioOFExpressAddFFF()
                  var identifier = 1;
               
               CS.setAttributeValue('FlatFee_product_0:Override_Values_0', 'true');
               
               var ccodeSip4FromFlatfeeFixedVoiceJSON = getCCodeFromFlatfeeFixedVoiceJSON(4);
               
               var tariffSip4FromFlatfeeFixedVoiceJSON = getTariffFromFlatfeeFixedVoiceJSON(4);
               
               CS.setAttributeValue('FlatFee_product_' + identifier + ':Override_Tariff_0', tariffSip4FromFlatfeeFixedVoiceJSON * 4);
               
               CS.setAttributeValue('FlatFee_product_' + identifier + ':Override_Quantity_0', CS.getAttributeValue('Number_of_SIP_4_0'));
               
               CS.setAttributeValue('FlatFee_product_' + identifier + ':Override_Ccode_0', ccodeSip4FromFlatfeeFixedVoiceJSON);
               
               CS.setAttributeValue('FlatFee_product_' + identifier + ':Override_RC_List_Price_0', tariffSip4FromFlatfeeFixedVoiceJSON * 4);
            
                  identifier = 0;
                  
               CS.setAttributeValue('FlatFee_product_1:Override_Values_0', 'true');
               
               var ccodeSip8FromFlatfeeFixedVoiceJSON = getCCodeFromFlatfeeFixedVoiceJSON(8);
               
               var tariffSip8FromFlatfeeFixedVoiceJSON = getTariffFromFlatfeeFixedVoiceJSON(8);
               
               CS.setAttributeValue('FlatFee_product_' + identifier + ':Override_Tariff_0', tariffSip8FromFlatfeeFixedVoiceJSON * 8);
               
               CS.setAttributeValue('FlatFee_product_' + identifier + ':Override_Quantity_0', CS.getAttributeValue('Number_of_SIP_8_0'));
               
               CS.setAttributeValue('FlatFee_product_' + identifier + ':Override_Ccode_0', ccodeSip8FromFlatfeeFixedVoiceJSON);
               
               CS.setAttributeValue('FlatFee_product_' + identifier + ':Override_RC_List_Price_0', tariffSip8FromFlatfeeFixedVoiceJSON * 8);
               
               setValueOfAttribute('Tariff', tariffSip8FromFlatfeeFixedVoiceJSON);
               
                  setValueOfAttribute('Ccode', ccodeSip8FromFlatfeeFixedVoiceJSON);
               }
               
              
	        } catch (err) {
	            // nothing to catch
	        }
	        
	    } else {
	        if(scenarioVal == 'One Fixed Express') {
	            if(CS.getAttributeValue('Number_of_SIP_8_0') > 0) {
	                var tariffFromFlatfeeFixedVoiceJSON = getTariffFromFlatfeeFixedVoiceJSON(8);
            		var ccodeFromFlatfeeFixedVoiceJSON = getCCodeFromFlatfeeFixedVoiceJSON(8);
            		log('calculateFlatfeeTariffValue==>Tariff: ' + tariffFromFlatfeeFixedVoiceJSON);
            		setValueOfAttribute('Tariff', tariffFromFlatfeeFixedVoiceJSON);
            		setValueOfAttribute('Ccode', ccodeFromFlatfeeFixedVoiceJSON);
            		// CS.setAttributeValue('Tariff_0', tariffFromFlatfeeFixedVoiceJSON);
            		CS.setAttributeValue('CCode_0', ccodeFromFlatfeeFixedVoiceJSON);
	            } else if(CS.getAttributeValue('Number_of_SIP_4_0') > 0) {
	                var tariffFromFlatfeeFixedVoiceJSON = getTariffFromFlatfeeFixedVoiceJSON(4);
            		var ccodeFromFlatfeeFixedVoiceJSON = getCCodeFromFlatfeeFixedVoiceJSON(4);
            		log('calculateFlatfeeTariffValue==>Tariff: ' + tariffFromFlatfeeFixedVoiceJSON);
            		setValueOfAttribute('Tariff', tariffFromFlatfeeFixedVoiceJSON);
            		setValueOfAttribute('Ccode', ccodeFromFlatfeeFixedVoiceJSON);
            		// CS.setAttributeValue('Tariff_0', tariffFromFlatfeeFixedVoiceJSON);
            		CS.setAttributeValue('CCode_0', ccodeFromFlatfeeFixedVoiceJSON);
	            }
	        } else {
               var tariffFromFlatfeeFixedVoiceJSON = getTariffFromFlatfeeFixedVoiceJSON();
               var ccodeFromFlatfeeFixedVoiceJSON = getCCodeFromFlatfeeFixedVoiceJSON();
               log('calculateFlatfeeTariffValue==>Tariff: ' + tariffFromFlatfeeFixedVoiceJSON);
               setValueOfAttribute('Tariff', tariffFromFlatfeeFixedVoiceJSON);
               setValueOfAttribute('Ccode', ccodeFromFlatfeeFixedVoiceJSON);
               // CS.setAttributeValue('Tariff_0', tariffFromFlatfeeFixedVoiceJSON);
               CS.setAttributeValue('CCode_0', ccodeFromFlatfeeFixedVoiceJSON);
	        }
	    }
	}
}

/**
* Getter for tariff from flatfee fixed voice JSON.
*/
function getTariffFromFlatfeeFixedVoiceJSON() {
	log('getTariffFromFlatfeeFixedVoiceJSON<==');
	
	var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');
	
	var oneFixedArray = flatFeeFixedVoice['One Fixed'];    
	
	if(scenarioVal == 'One Fixed Express') {
	    var oneFixedArray = flatFeeFixedVoice['One Fixed Express'];
	} 
	
	var result = 0;
	for (j in oneFixedArray) { 
		if(oneFixedArray[j]['UsageFrom'] < getTotalMinutenPerSip()) {
			result = parseFloat(oneFixedArray[j]['NetTarif'].toString().replace(',', '.'));
		}
	}
	log('getTariffFromFlatfeeFixedVoiceJSON==>' + result);
	return result;
}

/**
 * Getter for tariff from flatfee fixed voice JSON.
 */
function getCCodeFromFlatfeeFixedVoiceJSON() {
	log('getTariffFromFlatfeeFixedVoiceJSON<==');
	
	var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');
	
	var oneFixedArray = flatFeeFixedVoice['One Fixed'];    
	
	if(scenarioVal == 'One Fixed Express') {
	    var oneFixedArray = flatFeeFixedVoice['One Fixed Express'];
	} 
	
	var result = 0;
	for (j in oneFixedArray) { 
		if(oneFixedArray[j]['UsageFrom'] < getTotalMinutenPerSip()) {
			result = oneFixedArray[j]['ProductCode'];
		}
	}
	log('getTariffFromFlatfeeFixedVoiceJSON==>' + result);
	return result;
}

function getCCodeFromFlatfeeFixedVoiceJSON(numberOfSip) {
	log('getTariffFromFlatfeeFixedVoiceJSON<==');
	
	var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');
	
	var oneFixedArray = flatFeeFixedVoice['One Fixed'];    
	
	if(scenarioVal == 'One Fixed Express') {
	    var oneFixedArray = flatFeeFixedVoice['One Fixed Express'];
	} 
	
	var result = 0;
	for (j in oneFixedArray) { 
		if(oneFixedArray[j]['UsageFrom'] < getTotalMinutenPerSip()) {
		    if(oneFixedArray[j]['SIP'] == numberOfSip) {
			    result = oneFixedArray[j]['ProductCode'];
		    }
		}
	}
	log('getTariffFromFlatfeeFixedVoiceJSON==>' + result);
	return result;
}

function getTariffFromFlatfeeFixedVoiceJSON(numberOfSip) {
	log('getTariffFromFlatfeeFixedVoiceJSON<==');
	
	var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');
	
	var oneFixedArray = flatFeeFixedVoice['One Fixed'];    
	
	if(scenarioVal == 'One Fixed Express') {
	    var oneFixedArray = flatFeeFixedVoice['One Fixed Express'];
	} 
	
	var result = 0;
	for (j in oneFixedArray) { 
		if(oneFixedArray[j]['UsageFrom'] < getTotalMinutenPerSip()) {
		    if(oneFixedArray[j]['SIP'] == numberOfSip) {
			    result = parseFloat(oneFixedArray[j]['NetTarif'].toString().replace(',', '.'));
		    }
		}
	}
	log('getTariffFromFlatfeeFixedVoiceJSON==>' + result);
	return result;
}

/**
* Calculates NetPrice based on tariff and discount.
*/
function calculateNetPriceWithDiscount() {
  log('calculateNetPriceWithDiscount<==');
  var discount = allTableInformation['Discount'];
  var tariff = allTableInformation['Tariff'];

  if (isNaN(discount)) {
     discount = 0;
  }
  
  var result = tariff * ( 100 - discount ) / 100;
  log('calculateNetPriceWithDiscount==>NetPrice: ' + result);
  setValueOfAttribute('NetPrice', result);
}

/**
* Calculates gross total.
*/
function calculateGrossTotal() {
  log('calculateGrossTotal<==');
  var tariff = allTableInformation['Tariff'];
  var totalSipQuant = allTableInformation['TotalSipQuant'];
  // var duration = allTableInformation['duration'];

  // var result = tariff * totalSipQuant * duration;
  var result = tariff * totalSipQuant;

  if (isNaN(result)) {
     result = 0;
  }

  log('calculateGrossTotal==>GrossTotal: ' + result);
  setValueOfAttribute('GrossTotal', result);
}

/**
* Calculates net total.
*/
function calculateNetTotal() {
  log('calculateNetTotal<==');
  var netPrice = allTableInformation['NetPrice'];
  var totalSipQuant = allTableInformation['TotalSipQuant'];
  //var duration = allTableInformation['duration'];

  var result = netPrice * totalSipQuant;
  var resultPerMonth = netPrice;

  if (isNaN(result)) {
     result = 0;
  }
  
  if (isNaN(resultPerMonth)) {
     resultPerMonth = 0;
  }
  
  log('calculateNetTotal==>NetTotal: ' + result);
  setValueOfAttribute('NetTotal', result);
  CS.setAttributeValue('WorryFreeNetTotal_0', parseFloat(resultPerMonth.toString().replace(/,/g, '')));
}

/**
* Getter for values from fixed belspend ref JSON.
*/
function getValueFromFixedBelspendRefJSON(nameOfAttribute) {
  log('getValueFromFixedBelspendRefJSON<==nameOfattribute: ' + nameOfAttribute);
  for (j in fixedBelspendRefJson) { 
     if(j == nameOfAttribute) {
        log('getValueFromFixedBelspendRefJSON==>' + fixedBelspendRefJson[j]['Value']);
        return fixedBelspendRefJson[j]['Value'];
     }
  }
  log('getValueFromFixedBelspendRefJSON==>Could not find value based on nameOfAttribute: ' + nameOfAttribute);
}

/**
* Calculates all min total attributes.
*/
function calculateAllMinTotal() {
  log('calculateAllMinTotal<==');
  calculateMinTotalBinnen();
  calculateMinTotalBuiten();
  calculateMinTotalNaarMobile();
  calculateMinTotalNaar088();
  calculateMinTotalNaar0900();
  calculateMinTotalNaarBTW();
  calculateMinTotalOverige();

  /**
   * IDD part.
   */
  calculateMinTotalNlEu();
  calculateMinTotalNlRow();
  calculateMinTotalNlExc();
  calculateTotalGrossRevenueForUsage();
  log('calculateAllMinTotal==>');
}

function calculateTotalGrossRevenueForUsage() {
   var sum = 0;
   jQuery('.slds-input').each(function() {
       if(jQuery(this) && jQuery(this).attr('name')) {
         if (jQuery(this).attr('name').indexOf("MinTotal") >= 0) {
             if (jQuery(this).attr('name').indexOf("NlEu") == -1 && jQuery(this).attr('name').indexOf("NlRow") == -1 && jQuery(this).attr('name').indexOf("NlExc") == -1) {
                 sum += parseInt(jQuery(this).val());
             }
         }
       }
   });
   CS.setAttributeValue('TotalGrossRevenue_0', sum * parseInt(allTableInformation['Tariff']));
}

function getCompetitorInvoice() {
  if (allTableInformation['competitorInvoice'] == null) {
     return jQuery('[name="competitorInvoice"]').attr('value');
  }
  return allTableInformation['competitorInvoice'];
}

function hideOneNetScenarioAI() {
   if (CS.getAttributeValue('Scenario_OneNet_0') == 'One Net Enterprise') {
      jQuery('[data-cs-label="Scenario_OneNet_0"]').parent().parent().hide();
   }
}

function putAllRequiredInputFieldsToReadOnly() {
  log('putAllRequiredInputFieldsToReadOnly<==');
  var competitorInvoice = getCompetitorInvoice();
  
  jQuery("[data-cs-action='editQuantityRelatedProduct']").prop('disabled', true);

  jQuery('[name="CallsBinnen"').prop("disabled", false);
  jQuery('[name="HrsNaarInter"').prop("disabled", false);
  jQuery('[name="MinNaarInter"').prop("disabled", false);
  jQuery('[name="SecNaarInter"').prop("disabled", false);
  jQuery('[name="CallsNaarInter"').prop("disabled", false);
  jQuery('[name="CallsNaar0900"').prop("disabled", false);
  jQuery('[name="CallsNaarBTW"').prop("disabled", false);
  jQuery('[name="CallsOverige"').prop("disabled", false);

  jQuery('[name="HrsBinnen"').prop("disabled", false);
  jQuery('[name="HrsBuiten"').prop("disabled", false);
  jQuery('[name="HrsNaarInter"').prop("disabled", false);
  jQuery('[name="HrsNaarMobile"').prop("disabled", false);
  jQuery('[name="HrsNaar088"').prop("disabled", false);
  jQuery('[name="HrsNaar0900"').prop("disabled", false);
  jQuery('[name="HrsNaarBTW"').prop("disabled", false);
  jQuery('[name="HrsOverige"').prop("disabled", false);

  jQuery('[name="MinBinnen"').prop("disabled", false);
  jQuery('[name="MinBuiten"').prop("disabled", false);
  jQuery('[name="MinNaarInter"').prop("disabled", false);
  jQuery('[name="MinNaarMobile"').prop("disabled", false);
  jQuery('[name="MinNaar088"').prop("disabled", false);
  jQuery('[name="MinNaar0900"').prop("disabled", false);
  jQuery('[name="MinNaarBTW"').prop("disabled", false);
  jQuery('[name="MinOverige"').prop("disabled", false);

  jQuery('[name="SecBinnen"').prop("disabled", false);
  jQuery('[name="SecBuiten"').prop("disabled", false);
  jQuery('[name="SecNaarInter"').prop("disabled", false);
  jQuery('[name="SecNaarMobile"').prop("disabled", false);
  jQuery('[name="SecNaar088"').prop("disabled", false);
  jQuery('[name="SecNaar0900"').prop("disabled", false);
  jQuery('[name="SecNaarBTW"').prop("disabled", false);
  jQuery('[name="SecOverige"').prop("disabled", false);

  jQuery('[name="AmountBinnen"').prop("disabled", false);
  jQuery('[name="AmountBuiten"').prop("disabled", false);
  jQuery('[name="AmountNaarMobile"').prop("disabled", false);
  jQuery('[name="AmountNaar088"').prop("disabled", false);
  
  if (competitorInvoice == 'default') {
     jQuery('[name="CallsNaarInter"').prop("disabled", true);
     jQuery('[name="HrsNaarInter"').prop("disabled", true);
     jQuery('[name="MinNaarInter"').prop("disabled", true);
     jQuery('[name="SecNaarInter"').prop("disabled", true);
     setValueOfAttributeNotCurrency('CallsNaarInter', 0);
     setValueOfAttributeNotCurrency('HrsNaarInter', 0);
     setValueOfAttributeNotCurrency('MinNaarInter', 0);
     setValueOfAttributeNotCurrency('SecNaarInter', 0);
  }

  if (competitorInvoice == 'vrijtotaal') {
     jQuery('[name="CallsNaarInter"').prop("disabled", true);
     jQuery('[name="CallsNaar0900"').prop("disabled", true);
     jQuery('[name="CallsNaarBTW"').prop("disabled", true);
     jQuery('[name="CallsOverige"').prop("disabled", true);
     
     setValueOfAttributeNotCurrency('CallsNaarInter', 0);
     setValueOfAttributeNotCurrency('CallsNaar0900', 0);
     setValueOfAttributeNotCurrency('CallsNaarBTW', 0);
     setValueOfAttributeNotCurrency('CallsOverige', 0);

     jQuery('[name="HrsBinnen"').prop("disabled", true);
     jQuery('[name="HrsBuiten"').prop("disabled", true);
     jQuery('[name="HrsNaarInter"').prop("disabled", true);
     jQuery('[name="HrsNaarMobile"').prop("disabled", true);
     jQuery('[name="HrsNaar088"').prop("disabled", true);
     jQuery('[name="HrsNaar0900"').prop("disabled", true);
     jQuery('[name="HrsNaarBTW"').prop("disabled", true);
     jQuery('[name="HrsOverige"').prop("disabled", true);
     
     setValueOfAttributeNotCurrency('HrsBinnen', 0);
     setValueOfAttributeNotCurrency('HrsBuiten', 0);
     setValueOfAttributeNotCurrency('HrsNaarInter', 0);
     setValueOfAttributeNotCurrency('HrsNaarMobile', 0);
     setValueOfAttributeNotCurrency('HrsNaar088', 0);
     setValueOfAttributeNotCurrency('HrsNaar0900', 0);
     setValueOfAttributeNotCurrency('HrsNaarBTW', 0);
     setValueOfAttributeNotCurrency('HrsOverige', 0);

     jQuery('[name="MinBinnen"').prop("disabled", true);
     jQuery('[name="MinBuiten"').prop("disabled", true);
     jQuery('[name="MinNaarInter"').prop("disabled", true);
     jQuery('[name="MinNaarMobile"').prop("disabled", true);
     jQuery('[name="MinNaar088"').prop("disabled", true);
     jQuery('[name="MinNaar0900"').prop("disabled", true);
     jQuery('[name="MinNaarBTW"').prop("disabled", true);
     jQuery('[name="MinOverige"').prop("disabled", true);
     
     setValueOfAttributeNotCurrency('MinBinnen', 0);
     setValueOfAttributeNotCurrency('MinBuiten', 0);
     setValueOfAttributeNotCurrency('MinNaarInter', 0);
     setValueOfAttributeNotCurrency('MinNaarMobile', 0);
     setValueOfAttributeNotCurrency('MinNaar088', 0);
     setValueOfAttributeNotCurrency('MinNaar0900', 0);
     setValueOfAttributeNotCurrency('MinNaarBTW', 0);
     setValueOfAttributeNotCurrency('MinOverige', 0);

     jQuery('[name="SecBinnen"').prop("disabled", true);
     jQuery('[name="SecBuiten"').prop("disabled", true);
     jQuery('[name="SecNaarInter"').prop("disabled", true);
     jQuery('[name="SecNaarMobile"').prop("disabled", true);
     jQuery('[name="SecNaar088"').prop("disabled", true);
     jQuery('[name="SecNaar0900"').prop("disabled", true);
     jQuery('[name="SecNaarBTW"').prop("disabled", true);
     jQuery('[name="SecOverige"').prop("disabled", true);
     
     setValueOfAttributeNotCurrency('SecBinnen', 0);
     setValueOfAttributeNotCurrency('SecBuiten', 0);
     setValueOfAttributeNotCurrency('SecNaarInter', 0);
     setValueOfAttributeNotCurrency('SecNaarMobile', 0);
     setValueOfAttributeNotCurrency('SecNaar088', 0);
     setValueOfAttributeNotCurrency('SecNaar0900', 0);
     setValueOfAttributeNotCurrency('SecNaarBTW', 0);
     setValueOfAttributeNotCurrency('SecOverige', 0);

     jQuery('[name="AmountBinnen"').prop("disabled", true);
     jQuery('[name="AmountBuiten"').prop("disabled", true);
     jQuery('[name="AmountNaarMobile"').prop("disabled", true);
     jQuery('[name="AmountNaar088"').prop("disabled", true);
     
     setValueOfAttributeNotCurrency('AmountBinnen', 0);
     setValueOfAttributeNotCurrency('AmountBuiten', 0);
     setValueOfAttributeNotCurrency('AmountNaarMobile', 0);
     setValueOfAttributeNotCurrency('AmountNaar088', 0);
  }

  if (competitorInvoice == 'others') {
     jQuery('[name="CallsNaarInter"').prop("disabled", true);
     setValueOfAttributeNotCurrency('CallsNaarInter', 0);

     jQuery('[name="HrsBinnen"').prop("disabled", true);
     jQuery('[name="HrsBuiten"').prop("disabled", true);
     jQuery('[name="HrsNaarInter"').prop("disabled", true);
     jQuery('[name="HrsNaarMobile"').prop("disabled", true);
     jQuery('[name="HrsNaar088"').prop("disabled", true);
     jQuery('[name="HrsNaar0900"').prop("disabled", true);
     jQuery('[name="HrsNaarBTW"').prop("disabled", true);
     jQuery('[name="HrsOverige"').prop("disabled", true);
     
     setValueOfAttributeNotCurrency('HrsBinnen', 0);
     setValueOfAttributeNotCurrency('HrsBuiten', 0);
     setValueOfAttributeNotCurrency('HrsNaarInter', 0);
     setValueOfAttributeNotCurrency('HrsNaarMobile', 0);
     setValueOfAttributeNotCurrency('HrsNaar088', 0);
     setValueOfAttributeNotCurrency('HrsNaar0900', 0);
     setValueOfAttributeNotCurrency('HrsNaarBTW', 0);
     setValueOfAttributeNotCurrency('HrsOverige', 0);

     jQuery('[name="MinBinnen"').prop("disabled", true);
     jQuery('[name="MinBuiten"').prop("disabled", true);
     jQuery('[name="MinNaarInter"').prop("disabled", true);
     jQuery('[name="MinNaarMobile"').prop("disabled", true);
     jQuery('[name="MinNaar088"').prop("disabled", true);
     jQuery('[name="MinNaar0900"').prop("disabled", true);
     jQuery('[name="MinNaarBTW"').prop("disabled", true);
     jQuery('[name="MinOverige"').prop("disabled", true);
     
     setValueOfAttributeNotCurrency('MinBinnen', 0);
     setValueOfAttributeNotCurrency('MinBuiten', 0);
     setValueOfAttributeNotCurrency('MinNaarInter', 0);
     setValueOfAttributeNotCurrency('MinNaarMobile', 0);
     setValueOfAttributeNotCurrency('MinNaar088', 0);
     setValueOfAttributeNotCurrency('MinNaar0900', 0);
     setValueOfAttributeNotCurrency('MinNaarBTW', 0);
     setValueOfAttributeNotCurrency('MinOverige', 0);

     jQuery('[name="SecBinnen"').prop("disabled", true);
     jQuery('[name="SecBuiten"').prop("disabled", true);
     jQuery('[name="SecNaarInter"').prop("disabled", true);
     jQuery('[name="SecNaarMobile"').prop("disabled", true);
     jQuery('[name="SecNaar088"').prop("disabled", true);
     jQuery('[name="SecNaar0900"').prop("disabled", true);
     jQuery('[name="SecNaarBTW"').prop("disabled", true);
     jQuery('[name="SecOverige"').prop("disabled", true);
     
     setValueOfAttributeNotCurrency('SecBinnen', 0);
     setValueOfAttributeNotCurrency('SecBuiten', 0);
     setValueOfAttributeNotCurrency('SecNaarInter', 0);
     setValueOfAttributeNotCurrency('SecNaarMobile', 0);
     setValueOfAttributeNotCurrency('SecNaar088', 0);
     setValueOfAttributeNotCurrency('SecNaar0900', 0);
     setValueOfAttributeNotCurrency('SecNaarBTW', 0);
     setValueOfAttributeNotCurrency('SecOverige', 0);
     
     setValueOfAttributeNotCurrency('CallsNaarInter', 0);
  }
  
  if (CS.getAttributeValue('DDISectionEnabled_0')) {
      jQuery('[name="naarInternationalDiscount').prop("disabled", true);
      setValueOfAttributeNotCurrency('naarInternationalDiscount', 0);
      
      jQuery('[name="nlToEuOnlyWhenIDDCheckedDiscount').prop("disabled", false);
      jQuery('[name="nlToRowOnlyWhenIDDCheckedDiscount').prop("disabled", false);
      jQuery('[name="nlToExceptionsOnlyWhenIDDCheckedDiscount').prop("disabled", false);
  } else {
      jQuery('[name="naarInternationalDiscount').prop("disabled", false);
      
      jQuery('[name="nlToEuOnlyWhenIDDCheckedDiscount').prop("disabled", true);
      setValueOfAttributeNotCurrency('nlToEuOnlyWhenIDDCheckedDiscount', 0);
      
      jQuery('[name="nlToRowOnlyWhenIDDCheckedDiscount').prop("disabled", true);
      setValueOfAttributeNotCurrency('nlToRowOnlyWhenIDDCheckedDiscount', 0);
      
      jQuery('[name="nlToExceptionsOnlyWhenIDDCheckedDiscount').prop("disabled", true);
      setValueOfAttributeNotCurrency('nlToExceptionsOnlyWhenIDDCheckedDiscount', 0);
  }
  
  if (jQuery('[name="flatFeeDropdown"]').val() == 'payasyouuse') {
      jQuery('[id="discountToRemove"]').show();
      jQuery('[id="iddDiscountToRemove"]').show();
      jQuery('[name="TotalSipQuant"').prop("disabled", true);
      jQuery('.slds-input').each(function() {
          if(jQuery(this) && jQuery(this).attr('name')) {
            if (jQuery(this).attr('name').indexOf("Discount") >= 0) {
                jQuery(this).show();
            }
          }
     });
      
  } else if (jQuery('[name="flatFeeDropdown"]').val() == 'fixedflatfee') {
      jQuery('[id="discountToRemove"]').hide();
      jQuery('[id="iddDiscountToRemove"]').hide();
      jQuery('[name="TotalSipQuant"').prop("disabled", false);
      jQuery('.slds-input').each(function() {
          if(jQuery(this) && jQuery(this).attr('name')) {
            if (jQuery(this).attr('name').indexOf("Discount") >= 0) {
                jQuery(this).hide();
                setValueOfAttributeNotCurrency(jQuery(this).attr('name'), 0);
            }
          }
     });
     //jQuery('[name="Discount"').show();
  }

  log('putAllRequiredInputFieldsToReadOnly==>');
}

/**
* Calculates Total Minuten Per Sip.
*/
function getTotalMinutenPerSip() {
  log('getTotalMinutenPerSip<==totalFlatfeeMinuten: ' + parseFloat(allTableInformation['totalFlatfeeMinuten']) + ';TotalSipQuant: ' + parseFloat(allTableInformation['TotalSipQuant']));
  var totalMinutenPerSip = parseFloat(allTableInformation['totalFlatfeeMinuten']) / parseFloat(allTableInformation['TotalSipQuant']);
  log('getTotalMinutenPerSip==>totalMinutenPerSip: ' + totalMinutenPerSip);
  return totalMinutenPerSip;
}

/**
* Calculate Min Total Idd generic.
*/
function calculateMinTotalIdd(nameSufix) { 
  log('calculateMinTotalIdd<==nameSufix: ' + nameSufix);
  var competitorInvoice = getCompetitorInvoice();
  var calls = parseFloat(allTableInformation['Calls' + nameSufix]);
  var hrs = parseFloat(allTableInformation['Hrs' + nameSufix]);
  var min = parseFloat(allTableInformation['Min' + nameSufix]);
  var sec = parseFloat(allTableInformation['Sec' + nameSufix]);

  var result = 0;

  if (competitorInvoice == 'default') {
     if (sec > 29) {
        result = (hrs * 60) + min + 1;
     } else {
        result = (hrs * 60) + min;
     }
  } else {
     if (sec > 29) {
        result = ((hrs * 60) + min + 1) / 2;
     } else {
        result = ((hrs * 60) + min) / 2;
     }
  }

  log('calculateMinTotalIdd==>MinTotal' + nameSufix + ': ' + nameSufix);
  setValueOfAttributeNotCurrency('MinTotal' + nameSufix, Math.round(result));
}

/**
* Calculate Min Total NL to Exceptions.
*/
function calculateMinTotalNlExc() {
  log('calculateMinTotalNlExc<==');
  calculateMinTotalIdd('NlExc');
  log('calculateMinTotalNlExc==>');
}

/**
* Calculate Min Total NL to RoW.
*/
function calculateMinTotalNlRow() {
  log('calculateMinTotalNlRow<==');
  calculateMinTotalIdd('NlRow');
  log('calculateMinTotalNlRow==>');
}

/**
* Calculate Min Total NL to EU+
*/
function calculateMinTotalNlEu() {
  log('calculateMinTotalNlEu<==');
  calculateMinTotalIdd('NlEu');
  log('calculateMinTotalNlEu==>');
}

/**
* Calculate Min Total Invoice.
*/
function calculateMinTotalInvoice(nameSufix) {
  var competitorInvoice = getCompetitorInvoice();
  var calls = parseFloat(allTableInformation['Calls' + nameSufix]);
  var hrs = parseFloat(allTableInformation['Hrs' + nameSufix]);
  var min = parseFloat(allTableInformation['Min' + nameSufix]);
  var sec = parseFloat(allTableInformation['Sec' + nameSufix]);

  var result = 0;

  if (competitorInvoice == 'default') {
     if (sec > 29) {
        result = (hrs * 60) + min + 1;
     } else {
        result = (hrs * 60) + min;
     }
  } else {
     result = 0;
  }

  setValueOfAttributeNotCurrency('MinTotal' + nameSufix, Math.round(result));
}

/**
* Calculate Min Total Overige.
*/
function calculateMinTotalOverige() {
  log('calculateMinTotalOverige<==');
  calculateMinTotalInvoice('Overige');
  log('calculateMinTotalOverige==>');
}

/**
* Calculate Min Total Naar BTW.
*/
function calculateMinTotalNaarBTW() {
  log('calculateMinTotalNaarBTW<==');
  calculateMinTotalInvoice('NaarBTW');
  log('calculateMinTotalNaarBTW==>');
}

/**
* Calculate Min Total Naar 0900.
*/
function calculateMinTotalNaar0900() {
  log('calculateMinTotalNaar0900<==');
  calculateMinTotalInvoice('Naar0900');
  log('calculateMinTotalNaar0900==>');
}

/**
* Calculate Min Total Invoice.
*/
function calculateMinTotalInvoiceOther(nameSufix, fixedBelspendRefAverageCallLength) {
   // TODO check value of fixedBelspendRefAverageCallLength
   
   
  var competitorInvoice = getCompetitorInvoice();
  var calls = parseFloat(allTableInformation['Calls' + nameSufix]);
  var hrs = parseFloat(allTableInformation['Hrs' + nameSufix]);
  var min = parseFloat(allTableInformation['Min' + nameSufix]);
  var sec = parseFloat(allTableInformation['Sec' + nameSufix]);

  var result = 0;

  var averageCallLength = getValueFromFixedBelspendRefJSON(fixedBelspendRefAverageCallLength);

  if (competitorInvoice == 'default') {
     if (sec > 29) {
        result = (hrs * 60) + min + 1;
     } else {
        result = (hrs * 60) + min;
     }
  } else {
     result = (calls * averageCallLength / 60) / 2;
  }

  setValueOfAttributeNotCurrency('MinTotal' + nameSufix, Math.round(result));
}

/**
* Calculate Min Total Invoice Naar 088.
*/
function calculateMinTotalNaar088() {
  log('calculateMinTotalNaar088<==');
  calculateMinTotalInvoiceOther('Naar088', 'AvgCallLength 088 (sec)');
  log('calculateMinTotalNaar088==>');
}

/**
* Calculate Min Total Invoice Naar Mobile.
*/
function calculateMinTotalNaarMobile() {
  log('calculateMinTotalNaarMobile<==');
  calculateMinTotalInvoiceOther('NaarMobile', 'AvgCallLength Mobile (sec)');
  log('calculateMinTotalNaarMobile==>');
}

/**
* Calculate Min Total Invoice Buiten.
*/
function calculateMinTotalBuiten() {
  log('calculateMinTotalBuiten<==');
  calculateMinTotalInvoiceOther('Buiten', 'AvgCallLength BuitenRegioVast (sec)');
  log('calculateMinTotalBuiten==>');
}

/**
* Calculate Min Total Invoice Binnen.
*/
function calculateMinTotalBinnen() {
  calculateMinTotalInvoiceOther('Binnen', 'AvgCallLength BinnenRegioVast (sec)');
}

/**
* Order of all Total Gross calculations.
*/
function calculateAllTotalGross() {
  calculateTotalGrossBinnen();
  calculateTotalGrossBuiten();
  calculateTotalGrossNaarMobile();
  calculateTotalGrossNaar088();
  calculateTotalGrossNaar0900();
  calculateTotalGrossNaarBTW();
  calculateTotalGrossOverige();

  // DDI
  calculateTotalGrossNlEu();
  calculateTotalGrossNlRow();
  calculateTotalGrossNlExc();
}

/**
* Calculate Total Gorss Generic.
*/
function calculateTotalGrossGeneric(nameSufix) {
  var competitorInvoice = getCompetitorInvoice();
  var amount = 0;

  if (allTableInformation['Amount' + nameSufix] != undefined) {
     amount = getFloatValueFromString((allTableInformation['Amount' + nameSufix]).replace(/,/g, ''));
  }
  
  var result = 0;

  if (competitorInvoice == 'default') {
     result = amount;
  } else {
     result = amount / 2;
  }

  setValueOfAttribute('TotalGross' + nameSufix, result);
}

/**
* Calculate Total Gorss Overige.
*/
function calculateTotalGrossOverige() {
  calculateTotalGrossGeneric('Overige');
}

/**
* Calculate Total Gorss Naar BTW.
*/
function calculateTotalGrossNaarBTW() {
  calculateTotalGrossGeneric('NaarBTW');
}

/**
* Calculate Total Gorss Naar BTW.
*/
function calculateTotalGrossNaar0900() {
  calculateTotalGrossGeneric('Naar0900');
}

/**
* Calculate Total Gorss IDD.
*/
function calculateTotalGrossIddGeneric(nameSufix, uct) {
  var competitorInvoice = getCompetitorInvoice();
  var calls = parseFloat(allTableInformation['Calls' + nameSufix]);
  var minTotal = parseFloat(allTableInformation['MinTotal' + nameSufix]);
  
  var result = 0;

  var cst = getPpIddCst();

  if (competitorInvoice == 'default') {
     result = (calls * cst) + (minTotal * uct);
  } else {
     result = (calls * cst) + (minTotal * uct) / 2;
  }
  if (nameSufix == 'NlRow' && allTableInformation['flatFeeDropdown'] == 'fixedflatfee')
  {
	  result = 0;
  }

  setValueOfAttribute('TotalGross' + nameSufix, result);
}

/**
* Calculate Total Gorss NL to Exception.
*/
function calculateTotalGrossNlExc() {
  calculateTotalGrossIddGeneric('NlExc', getUctFRestWorldToFixed());
}

/**
* Calculate Total Gorss NL to RoW.
*/
function calculateTotalGrossNlRow() {
  calculateTotalGrossIddGeneric('NlRow', getUctFRestEuropaToFixed());
}

/**
* Getter for Onnet Ratio.
*/
function getOnnetRatio(description) {
  return parseFloat(getValueFromFixedBelspendRefJSON(description).toString().replace(',', '.'));
}

/**
* Getter for Offnet Ratio.
*/
function getOffnetRatio(description) {
  if(allTableInformation['combinationMobile'] == 'false') {
     return 100;
  } else {
     return 100 - getOnnetRatio(description);
  }
}

/**
* Calculate Total Gross NL to EU+.
*/
function calculateTotalGrossNlEu() {
  var flatFeeDropdown = allTableInformation['flatFeeDropdown'];
  var competitorInvoice = getCompetitorInvoice();
  var totalSipQuant = parseFloat(allTableInformation['TotalSipQuant']);
  var callsNlEu = parseFloat(allTableInformation['CallsNlEu']);
  var minTotalNlEu = parseFloat(allTableInformation['MinTotalNlEu']);

  // TODO 
  var cst = getPpIddCst();
  var uct = getUctFBuurlandenToFixed();

  var result = 0;

  if (flatFeeDropdown != 'payasyouuse') {
     if (totalSipQuant > 0) {
        result = 0;
     } else {
        if (competitorInvoice == 'default') {
           result = (callsNlEu * cst) + (minTotalNlEu * uct);
        } else {
           result = (callsNlEu * cst) + (minTotalNlEu * uct) / 2;
        }
     }
  } else {
     if (competitorInvoice == 'default') {
        result = (callsNlEu * cst) + (minTotalNlEu * uct);
     } else {
           result = (callsNlEu * cst) + (minTotalNlEu * uct) / 2;
     }
  }

  setValueOfAttribute('TotalGrossNlEu', result);
}

/**
* Calculate Total Gross Naar 088.
*/
function calculateTotalGrossNaar088() {
  var flatFeeDropdown = allTableInformation['flatFeeDropdown'];
  var totalSipQuant = parseFloat(allTableInformation['TotalSipQuant']);
  var callsNaar088 = parseFloat(allTableInformation['CallsNaar088']);
  var minTotalNaar088 = parseFloat(allTableInformation['MinTotalNaar088']);

  // TODO 
  var cst = getPpNmotCst();
  var offnetRate = getOffnetRatio('Onnet ratio 088 (%)');
  var uct = getUctFixedToNationalFixed();

  var result = 0;

  if (flatFeeDropdown != 'payasyouuse') {
     if (totalSipQuant > 0) {
        result = 0;
     } else {
        result = (callsNaar088 * cst * offnetRate) + (minTotalNaar088 * uct * (offnetRate / 100));
     }
  } else {
     result = (callsNaar088 * cst * offnetRate) + (minTotalNaar088 * uct * (offnetRate / 100));
  }

  setValueOfAttribute('TotalGrossNaar088', result);
}

/**
* Calculate Total Gross Naar Mobile.
*/
function calculateTotalGrossNaarMobile() {
  var flatFeeDropdown = allTableInformation['flatFeeDropdown'];
  var totalSipQuant = parseFloat(allTableInformation['TotalSipQuant']);
  var callsNaarMobile = parseFloat(allTableInformation['CallsNaarMobile']);
  var minTotalNaarMobile = parseFloat(allTableInformation['MinTotalNaarMobile']);

  // TODO 
  var cst = getPpNmotCst();
  var offnetRate = getOffnetRatio('Onnet ratio Mobile (%)');
  var uct = getUctFixedToMobileKPN();

  var result = 0;

  if (flatFeeDropdown != 'payasyouuse') {
     //jQuery('[name="TotalSipQuant"').prop("disabled", false);
     //jQuery('[name="Discount"').prop("disabled", false);
     
     if (totalSipQuant > 0) {
        result = 0;
     } else {
        result = (callsNaarMobile * cst * offnetRate) + (minTotalNaarMobile * uct * (offnetRate / 100));
     }
  } else {
     //jQuery('[name="TotalSipQuant"').prop("disabled", true);
     //jQuery('[name="Discount"').prop("disabled", true);
     setValueOfAttributeNotCurrency('TotalSipQuant', 0);
     setValueOfAttributeNotCurrency('Discount', 0);
     
     result = (callsNaarMobile * cst * offnetRate) + (minTotalNaarMobile * uct * (offnetRate / 100));
  }

  setValueOfAttribute('TotalGrossNaarMobile', result);
}

/**
* Calculate Total Gross Buiten.
*/
function calculateTotalGrossBuiten() {
  var flatFeeDropdown = allTableInformation['flatFeeDropdown'];
  var totalSipQuant = parseFloat(allTableInformation['TotalSipQuant']);
  var callsBuiten = parseFloat(allTableInformation['CallsBuiten']);
  var minTotalBuiten = parseFloat(allTableInformation['MinTotalBuiten']);

  // TODO 
  var cst = getPpNmotCst();
  var offnetRate = getOffnetRatio('Onnet ratio BuitenRegioVast (%)');
  var uct = getUctFixedToRegionalFixed();

  var result = 0;

  if (flatFeeDropdown != 'payasyouuse') {
     if (totalSipQuant > 0) {
        result = 0;
     } else {
        result = (callsBuiten * cst * offnetRate) + (minTotalBuiten * uct * (offnetRate / 100));
     }
  } else {
     result = (callsBuiten * cst * offnetRate) + (minTotalBuiten * uct * (offnetRate / 100));
  }

  setValueOfAttribute('TotalGrossBuiten', result);
}

/**
* Calculate Total Gross Binnen.
*/
function calculateTotalGrossBinnen() {
  var flatFeeDropdown = allTableInformation['flatFeeDropdown'];
  var totalSipQuant = parseFloat(allTableInformation['TotalSipQuant']);
  var callsBinnen = parseFloat(allTableInformation['CallsBinnen']);
  var minTotalBinnen = parseFloat(allTableInformation['MinTotalBinnen']);

  // TODO 
  var cst = getPpNmotCst();
  var offnetRate = getOffnetRatio('Onnet ratio BinnenRegioVast (%)');
  var uct = getUctFixedToRegionalFixed();

  var result = 0;

  if (flatFeeDropdown != 'payasyouuse') {
     if (totalSipQuant > 0) {
        result = 0;
     } else {
        result = (callsBinnen * cst * offnetRate) + (minTotalBinnen * uct * (offnetRate / 100));
     }
  } else {
     result = (callsBinnen * cst * offnetRate) + (minTotalBinnen * uct * (offnetRate / 100));
  }

  setValueOfAttribute('TotalGrossBinnen', result);
}

function calculateValueWithDiscount(value, discount) {
   if (value) {
       return parseFloat(value.toString().replace(/,/g, '')) * (100 - discount) / 100;
   } else {
       return 0; 
   }
}

/**
* Calculate All Net Order Intake.
*/
function calulcateAllNetOrderIntake() {
  jQuery('.slds-input').each(function() {
      if(jQuery(this) && jQuery(this).attr('name')) {
        if (jQuery(this).attr('name').indexOf("NetOrder") >= 0) {
           var nameSufix = jQuery(this).attr('name').split("NetOrder")[1];
           var totalGross = allTableInformation['TotalGross' + nameSufix];
           var duration = parseFloat(allTableInformation['duration']);
           // Based on talk with Eelco, we are going to remove duration multiplication and just calculate durations into Net Order.
           // var result = duration * totalGross;
           var result =  totalGross;
           var discount = 0;
           var nameOfFieldForInvalidation = '';
           
           if (jQuery(this).attr('name').indexOf("Binnen") >= 0 || jQuery(this).attr('name').indexOf("Buiten") >= 0 || jQuery(this).attr('name').indexOf("Naar088") >= 0) {
               if (!isValueInteger(allTableInformation['naarVasteNummersDiscount'])) {
                   allTableInformation['naarVasteNummersDiscount'] = 0;
               }
               discount = allTableInformation['naarVasteNummersDiscount'];
           }
           
           if (jQuery(this).attr('name').indexOf("NaarMobile") >= 0) {
               if (!isValueInteger(allTableInformation['naarMobieleNummersDiscount'])) {
                   allTableInformation['naarMobieleNummersDiscount'] = 0;
               }
               discount = allTableInformation['naarMobieleNummersDiscount'];
           }
           
           if (jQuery(this).attr('name').indexOf("NaarInter") >= 0 && !CS.getAttributeValue('DDISectionEnabled_0')) {
                   if (!isValueInteger(allTableInformation['naarInternationalDiscount'])) {
                   allTableInformation['naarInternationalDiscount'] = 0;
               }
               discount = allTableInformation['naarMobieleNummersDiscount'];
           }
           
           if (CS.getAttributeValue('DDISectionEnabled_0')) {
               if (jQuery(this).attr('name').indexOf("NlEu") >= 0) {
                       if (!isValueInteger(allTableInformation['nlToEuOnlyWhenIDDCheckedDiscount'])) {
                       allTableInformation['nlToEuOnlyWhenIDDCheckedDiscount'] = 0;
                   }
                    discount = allTableInformation['nlToEuOnlyWhenIDDCheckedDiscount'];
               }
               
               if (jQuery(this).attr('name').indexOf("NlRow") >= 0) {
                       if (!isValueInteger(allTableInformation['nlToRowOnlyWhenIDDCheckedDiscount'])) {
                       allTableInformation['nlToRowOnlyWhenIDDCheckedDiscount'] = 0;
                   }
                    if (allTableInformation['flatFeeDropdown'] == 'fixedflatfee')
				   {
					   discount = 100;
				   }
				   else{
                    discount = allTableInformation['nlToRowOnlyWhenIDDCheckedDiscount'];
				   }
               }
               
               if (jQuery(this).attr('name').indexOf("NlExc") >= 0) {
                   if (!isValueInteger(allTableInformation['nlToExceptionsOnlyWhenIDDCheckedDiscount'])) {
                       allTableInformation['nlToExceptionsOnlyWhenIDDCheckedDiscount'] = 0;
                   }
                    discount = allTableInformation['nlToExceptionsOnlyWhenIDDCheckedDiscount'];
               }
           }
           
           if (discount < 0 || discount > 100) {
               CS.markConfigurationInvalid('Discount has to be a value between 0 and 100.');
           }
               
               result = calculateValueWithDiscount(totalGross, discount);

           setValueOfAttribute('NetOrder' + nameSufix, result);
        }
      }
     });
}

function isValueInteger(valueToCheck) {
   if (Math.floor(valueToCheck) == valueToCheck && jQuery.isNumeric(valueToCheck)) {
       return true;
   }
   return false;
}

/**
* Calculate Total Flatfee Minuten.
*/
function calculateTotalFlatfeeMinuten() {
  var minTotalBinnen = parseFloat(allTableInformation['MinTotalBinnen']);
  var minTotalBuiten = parseFloat(allTableInformation['MinTotalBuiten']);
  var minTotalNaarMobile = parseFloat(allTableInformation['MinTotalNaarMobile']);
  var minTotalNaar088 = parseFloat(allTableInformation['MinTotalNaar088']);
  var minTotalNlEu = parseFloat(allTableInformation['MinTotalNlEu']);

  var result = minTotalBinnen + minTotalBuiten + minTotalNaarMobile + minTotalNaar088;
  // TP - minTotalNlEu is taken into account only if DDI section is enabled
  if(CS.getAttributeValue('DDISectionEnabled_0'))
     result += minTotalNlEu * 2;

  if (isNaN(result)) {
     result = 0;
  }

  setValueOfAttributeNotCurrency('totalFlatfeeMinuten', Math.round(result));
  CS.setAttributeValue('TotalFlatfeeMinutes_0', Math.round(result), true);
}

/**
* Get float value from string.
*/
function getFloatValueFromString(floatValueInString) {
  return parseFloat(floatValueInString.toString().replace(",", "."));
}

/**
* Calculate Min NL to EU+.
*/
function calculateMinNlToEu() {
  var min = (allTableInformation['AmountNaarInter'] * (getFloatValueFromString(getValueFromFixedBelspendRefJSON('IDD NL to EU (%)')) / 100)) / getFloatValueFromString(getValueFromFixedBelspendRefJSON('IDD NL to EU tariff'));
  if(min != null) {
     if(!CS.getAttributeValue('DDISectionEnabled_0')) {
        setValueOfAttributeNotCurrency('MinNlEu', min);
     }
  }
}

/**
* Calculate Min NL to RoW.
*/
function calculateMinNlToRoW() {
  var min = (allTableInformation['AmountNaarInter'] * (getFloatValueFromString(getValueFromFixedBelspendRefJSON('IDD NL to RoW (%)')) / 100)) / getFloatValueFromString(getValueFromFixedBelspendRefJSON('IDD NL to RoW tariff'));
  if(min != null) {
     if(!CS.getAttributeValue('DDISectionEnabled_0')) {
        setValueOfAttributeNotCurrency('MinNlRow', min);
     }
  }
}

/**
* Calculate Min NL to Exceptions.
*/
function calculateMinNlToExc() {
  var min = (allTableInformation['AmountNaarInter'] * (getFloatValueFromString(getValueFromFixedBelspendRefJSON('IDD NL to EXC (%)')) / 100)) / getFloatValueFromString(getValueFromFixedBelspendRefJSON('IDD NL to EXC tariff'));
  if(min != null) {
     if(!CS.getAttributeValue('DDISectionEnabled_0')) {
        setValueOfAttributeNotCurrency('MinNlExc', min);
     }
  }
}

/**
* Main variable that hold all data in table.
*/
var allTableInformation = {};

function showRelatedVoiceTable() {
  var relatedVoiceTable = ''
  + '<div id="relatedVoiceToStore">'
  + 	'<div class="slds-box">'
  + 		'<table class="list" id="relatedVoiceTable" style="text-align:left;">'
  + 			'<tr style="display: none;">'
  + 				'<td><label>Related voice proposition</label></td>'
  + 				'<td>'
  + 					'<select name="relatedVoiceProposition" class="slds-select">'
  + 					'</select>'
  + 				'</td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>Duration</label></td>'
  + 				'<td><input type="text" step="any" name="duration" class="slds-input"></td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>Total Flatfee Minuten</label></td>'
  + 				'<td><input type="text" step="any" name="totalFlatfeeMinuten" disabled="" class="slds-input"></td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>Competitor invoice</label></td>'
  + 				'<td>'
  + 					'<select name="competitorInvoice" class="slds-select">'
  + 						'<option selected="selected" value="default">Default</option>'
  + 						'<option value="vrijtotaal">KPN - Zakelijk BelVrij Totaal</option>'
  + 						'<option value="others">KPN - Others</option>'
  + 					'</select>'
  + 				'</td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>Combination with VF Mobile?</label></td>'
  + 				'<td>'
  + 					'<select name="combinationMobile" class="slds-select">'
  + 						'<option value="true">Yes</option>'
  + 						'<option value="false">No</option>'
  + 					'</select>'
  + 				'</td>'
  + 			'</tr>'
  + 		'</table>'
  + 	'</div>'
  + '</div>';

  relatedVoiceTable = jQuery(relatedVoiceTable);
  
  if (jQuery('#relatedVoiceTable')[0] == undefined) {
     jQuery('[data-cs-binding=RelatedVoice_0]').parent().parent().prepend(relatedVoiceTable);
  }
  
  jQuery('#relatedVoiceTable').find('td').css("width", "none");
  jQuery('#relatedVoiceTable').find('th').css("border", "none");
  jQuery('#relatedVoiceTable').find('th').css("border-color", "white");
  jQuery('#relatedVoiceTable').find('th').css("padding", "3px");
  jQuery('#relatedVoiceTable').find('td').css("border", "none");
  jQuery('#relatedVoiceTable').find('td').css("border-color", "white");
  jQuery('#relatedVoiceTable').find('input').css("text-align", "right");
  jQuery('#relatedVoiceTable').find('tr').css("border", "none");
  jQuery('#relatedVoiceTable').find('tr').css("padding", "2px");
  jQuery('#relatedVoiceTable').find('tr').css("border-color", "white");
  jQuery('#relatedVoiceTable').find('input').css("border-color", "#e6ebf2");
  jQuery('#relatedVoiceTable').find('label').addClass("slds-form-element__label");

}

/**
* Goes through all input and select fields and stores and loads all table data.
*/
window.refreshAllData = function refreshAllData() {
   /*
   jQuery('.slds-input').each(function() {
      if (!jQuery(this).val()) {
         setValueOfAttributeNotCurrency(jQuery(this).attr('name'), 0);
      }
   });*/
  
  jQuery('.slds-input, .slds-select').change(function() {
     
     allTableInformation[jQuery(this).attr('name')] = jQuery(this).attr('value');
     
     var toStore = '';
     for (var key in allTableInformation) {
          toStore = toStore + key + "|" + allTableInformation[key] + ";";
     }
  
     CS.setAttributeValue("DataStorageHidden_0", toStore);
     
     removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
     clearOFExpressFlags();

  });

  var values = CS.getAttributeValue("DataStorageHidden_0").split(';');

  for(var i=0; i<values.length;i++) {
     var key = values[i].split('|')[0];
     var value = values[i].split('|')[1];
     
     if(jQuery('[name="'+ key + '"]').length > 0) {		
        jQuery('[name="'+ key + '"]').val(value);
        if(key != undefined && value != undefined) {
           allTableInformation[key] = value;
        }
     }
  }
  
  setFlatFeedropdownForOneNet();
  setrelatedVoiceProposition();
  
  /* Commented during ROW bundle implementation 
  jQuery('[name="RoW_bundle_selection_0"]').change(function() {
     
     if (jQuery(this).prop('checked') == true && CS.getAttributeValue('RoW_bundle_set_0') == '') {
           // CS.Service.removeRelatedProduct('RoW_bundle_product_0');
           CS.setAttributeValue('RoW_bundle_set_0', 'set');
           CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config['RoW_Lookup_0']);
           jQuery("[data-cs-action='editQuantityRelatedProduct']").prop('disabled', true);
     } 
     
     if (jQuery(this).prop('checked') == false) {
        CS.setAttributeValue('RoW_bundle_set_0', '');
        //removeRelatedProductAndClearFlag('RoW_bundle_product_0','RoW_bundle_set_0');
        CS.Service.removeRelatedProduct('RoW_bundle_product_0');
        
     }
  });
  */
}

function setFlatFeeDropdown() {
   jQuery('select[name=flatFeeDropdown]').prop("disabled", false);
   if (CS.getAttributeValue('One_Fixed_Scenario_0').indexOf('OneNet') != -1 || CS.getAttributeValue('One_Fixed_Scenario_0').indexOf('One Net') != -1) {
       allTableInformation['flatFeeDropdown'] = 'payasyouuse';
       jQuery('select[name=flatFeeDropdown]').val('payasyouuse');
       jQuery('select[name=flatFeeDropdown]').prop("disabled", true);
       jQuery("option[value='fixedflatfee']").remove();
       jQuery('[name="TotalSipQuant"').prop("disabled", true);
   }
}

/*
Used in javascript action to start rules reevaluation when One Fixed Express scenario is active and
View is returned from second to first screen
The goal is tu recalculate prices on FlatFee related products for SIP4 and SIP8
*/
function fixOneFixedExpress()
{
   try {
      if(CS.Service.config['FlatFee_product_0'].relatedProducts.length == 2 && (CS.Service.getCurrentScreenRef()!="Company_Level_Fixed_Voice:Addons")) {
         var override = CS.getAttributeValue('FlatFee_product_0:Override_Values_0');
         var rcValue1 = CS.getAttributeValue('FlatFee_product_0:Add_On_Recurring_Charge_0');
         var rcValue2 = CS.getAttributeValue('FlatFee_product_1:Add_On_Recurring_Charge_0');
         if(override && CS.getAttributeValue('ScreenControl_0') == 'empty' && rcValue1 == rcValue2)
         {
            CS.setAttributeValue('ScreenControl_0','done');
            CS.Rules.evaluateAllRules();
         }
      }

   } catch (err) {
         // nothing to catch
   }
}

function setFlatFeedropdownForOneNet() {
   // if fixed flat fee option is not available, we have to make sure that, after loading, allTableInformation has proper selection too
   // it could be that value 'fixedflatfee' is loaded from DataStorageHidden_0 and that value is no longer available for selection if scenario is One Net (express or enterprise)
   if (jQuery("option[value='fixedflatfee']").length == 0) {
      allTableInformation['flatFeeDropdown'] = 'payasyouuse';
   }
}

/**
* Function that is going to store all data and execute all calculations.
*/
function loadAllCalculations() {
   if (CS.Service.getCurrentScreenRef()=="Company_Level_Fixed_Voice:Fixed_Usage_Profile") {
      setFlatFeeDropdown();
      //refreshAllData();  //This functionality has been moved to the calculate totals button and the code is under renderAlways function - this has been left in case there is a need for reversal of the functionality
      var values = CS.getAttributeValue("DataStorageHidden_0").split(';');

      for (var i=0; i<values.length;i++) {
         var key = values[i].split('|')[0];
         var value = values[i].split('|')[1];
       
         if (jQuery('[name="'+ key + '"]').length > 0) {		
            jQuery('[name="'+ key + '"]').val(value);
            if (key != undefined && value != undefined) {
               allTableInformation[key] = value;
            }
         }
      }
     
      setFlatFeedropdownForOneNet();
      executeAllCalculations();
      fixedScenario();
      setRelatedVoicePropositionValue();
      checkTotalMinutesForAllCallStreams();
      //calculateFlatfeeTariffValue();
      calculateNetPriceWithDiscount();
      calculateNetTotal();
      calculateGrossTotal();
      fillFixedCostPrices();
      numOfSIPRule(); 
      calculateCost();
      initDealType(); // W-002467
      //scenarioRules();
      calculateFlatfeeTariffValue();
   }
}

function setRelatedVoicePropositionValue() {
  if (CS.getAttributeValue('One_Fixed_Scenario_0') != 'NONE' && CS.getAttributeValue('One_Fixed_Scenario_0') != '') {
     if(CS.getAttributeValue('One_Fixed_Scenario_0').indexOf('One Fixed') != -1 || CS.getAttributeValue('One_Fixed_Scenario_0').indexOf('OneFixed') != -1) {
        jQuery('select[name=relatedVoiceProposition]').val('One Fixed 2');
        allTableInformation['relatedVoiceProposition'] = 'One Fixed 2';
        jQuery('[name="relatedVoiceProposition"').prop("disabled", true);
     } else if (CS.getAttributeValue('One_Fixed_Scenario_0').indexOf('OneNet') != -1 || CS.getAttributeValue('One_Fixed_Scenario_0').indexOf('One Net') != -1){
      jQuery('select[name=relatedVoiceProposition]').val('One Fixed 2');
      allTableInformation['relatedVoiceProposition'] = 'One Fixed 2';
      jQuery('[name="relatedVoiceProposition"').prop("disabled", true);
     }
  } else {
     jQuery('[name="relatedVoiceProposition"').prop("disabled", false);
  }
  CS.setAttributeValue('PropositionName_0', allTableInformation['relatedVoiceProposition']);
}

// Extracted from mobileusagehelper.js (part of Cost_Prices)
var CostPricesFixed = {
  "FixedToRegionalFixed": [
     {
        "CostPrice": 0.0067,
        "CallStreamVariable": "fVBinnenUwRegioLabel",
        "CallStreamLabel": "Binnen uw regio"
     },
     {
        "CostPrice": 0.00139,
        "CallStreamVariable": "fVBinnenUwRegioLabel",
        "CallStreamLabel": "Binnen uw regio"
     }
  ],
  "FixedToNationalFixed": [
     {
        "CostPrice": 0.0067,
        "CallStreamVariable": "fVBuitenUwRegioLabel",
        "CallStreamLabel": "Buiten uw regio"
     },
     {
        "CostPrice": 0.00139,
        "CallStreamVariable": "fVBuitenUwRegioLabel",
        "CallStreamLabel": "Buiten uw regio"
     }
  ],
  "FixedToMobileKPN": [
     {
        "CostPrice": 0.0195,
        "CallStreamVariable": "fVMobieleNummersLabel",
        "CallStreamLabel": "Naar mobiele nummers"
     },
     {
        "CostPrice": 0.00581,
        "CallStreamVariable": "fVMobieleNummersLabel",
        "CallStreamLabel": "Naar mobiele nummers"
     }
  ],
  "FixedtoOthers": [
     {
        "CostPrice": 0.092,
        "CallStreamVariable": "fVOverigeGesprekkenLabel",
        "CallStreamLabel": "Overige gesprekken"
     },
     {
        "CostPrice": 0.5,
        "CallStreamVariable": "fVOverigeGesprekkenLabel",
        "CallStreamLabel": "Overige gesprekken"
     }
  ],
  "IDD_Buurlanden_Fixed": [
     {
        "CostPrice": 0.010857143,
        "CallStreamVariable": "fVIDDzone1Label",
        "CallStreamLabel": "Zone 1: Buurlanden"
     },
     {
        "CostPrice": 0.0225,
        "CallStreamVariable": "fVIDDzone1Label",
        "CallStreamLabel": "NL to EU+"
     }
  ],
  "IDD_RestEuropa_Fixed": [
     {
        "CostPrice": 0.063050087,
        "CallStreamVariable": "fVIDDzone3Label",
        "CallStreamLabel": "Zone 3: Rest Europa"
     },
     {
        "CostPrice": 0.0447,
        "CallStreamVariable": "fVIDDzone3Label",
        "CallStreamLabel": "NL to RoW"
     }
  ],
  "IDD_RestWorld_Fixed": [
     {
        "CostPrice": 0.093375064,
        "CallStreamVariable": "fVIDDzone5Label",
        "CallStreamLabel": "Zone 5: Rest Wereld"
     },
     {
        "CostPrice": 0.3967,
        "CallStreamVariable": "fVIDDzone5Label",
        "CallStreamLabel": "NL To Exceptions"
     }
  ],
};

/*
  Simmilar to getCostPrices from mobileusagehelper.js but alway takes data from second array member (index = 1)
*/
function getFixedCostPrices(name, columnName) {
  for (j in Cost_Prices) {
     if (j == name) {
        return parseFloat(Cost_Prices[j][1][columnName].toString().replace(',', '.'));
     }
  }
}

function fillFixedCostPrices() {
  setValueOfAttributeNotCurrency('CostTariffBinnen', getFixedCostPrices('FixedToRegionalFixed', 'CostPrice'));
  setValueOfAttributeNotCurrency('CostTariffBuiten', getFixedCostPrices('FixedToNationalFixed', 'CostPrice'));
  setValueOfAttributeNotCurrency('CostTariffNaarInter', 0); // not in the table
  setValueOfAttributeNotCurrency('CostTariffNaarMobile', getFixedCostPrices('FixedToMobileKPN', 'CostPrice'));
  setValueOfAttributeNotCurrency('CostTariffNaar088', 0); // not in the table
  setValueOfAttributeNotCurrency('CostTariffNaar0900', 0); // not in the table
  setValueOfAttributeNotCurrency('CostTariffNaarBTW', 0); // not in the table
  setValueOfAttributeNotCurrency('CostTariffOverige', getFixedCostPrices('FixedtoOthers', 'CostPrice'));
  setValueOfAttributeNotCurrency('CostTariffNlEu', getFixedCostPrices('IDD_Buurlanden_Fixed', 'CostPrice'));
  setValueOfAttributeNotCurrency('CostTariffNlRow', getFixedCostPrices('IDD_RestEuropa_Fixed', 'CostPrice'));
  setValueOfAttributeNotCurrency('CostTariffNlExc', getFixedCostPrices('IDD_RestWorld_Fixed', 'CostPrice'));
}

/**
* Order of all calculations.
*/
function executeAllCalculations() {
  // don't calculate anything unless on right screen
  if (!(CS.Service.getCurrentScreenRef()=="Company_Level_Fixed_Voice:Fixed_Usage_Profile"))
  return;

  putAllRequiredInputFieldsToReadOnly();
  calculateFlatfeeTariffValue();
  calculateNetPriceWithDiscount();
  calculateNetTotal();
  calculateGrossTotal();

  if (CS.getAttributeValue('DDISectionEnabled_0') == false) {
     calculateMinNlToEu();
     calculateMinNlToRoW();
     calculateMinNlToExc();
  }

  calculateTotalFlatfeeMinuten();
  
  calculateAllMinTotal();
  calculateNaarInternationaalMinTotal();
  calculateAllTotalGross();
  calculateNaarInternationaalTotalGross();
  calulcateAllNetOrderIntake();
  updateAmountToCurrancy();
  calculateTotalGrossSum();
  calculateNetOrderIntakeSum();
  calculateNaarInternationalNumberOfCalls();
  calculateNaarInternationalNumberOfHrs();
  calculateNaarInternationalNumberOfMin();
  calculateNaarInternationalNumberOfSec();
  checkTenPercentIddRule();
  checkIfAllValuesAreNumerical();
  checkIfAllDicountsAreValid();
  calculateNaarInternationaalMinTotal();
  calculateTotalFlatfeeMinuten();
  calculateFlatfeeTariffValue();
}

function calculateNaarInternationalNumberOfCalls() {
  if(CS.getAttributeValue('DDISectionEnabled_0') == false) {
      setValueOfAttributeNotCurrency('CallsNaarInter', 0);
  } else {
      var naarInternationalNumberOfCalls = parseFloat(allTableInformation['CallsNlEu']) + parseFloat(allTableInformation['CallsNlRow']) + parseFloat(allTableInformation['CallsNlExc']);
      setValueOfAttributeNotCurrency('CallsNaarInter', naarInternationalNumberOfCalls);
  }
}

function calculateNaarInternationalNumberOfHrs() {
  if(CS.getAttributeValue('DDISectionEnabled_0') == false) {
      setValueOfAttributeNotCurrency('HrsNaarInter', 0);
  } else {
      var naarInternationalNumberOfHrs = parseFloat(allTableInformation['HrsNlEu']) + parseFloat(allTableInformation['HrsNlRow']) + parseFloat(allTableInformation['HrsNlExc']);
      setValueOfAttributeNotCurrency('HrsNaarInter', naarInternationalNumberOfHrs);
  }
}

function calculateNaarInternationalNumberOfMin() {
  if(CS.getAttributeValue('DDISectionEnabled_0') == false) {
      var naarInternationalNumberOfMin = parseFloat(allTableInformation['MinNlEu']) + parseFloat(allTableInformation['MinNlRow']) + parseFloat(allTableInformation['MinNlExc']);
      setValueOfAttributeNotCurrency('MinNaarInter', naarInternationalNumberOfMin);
  } else {
      var naarInternationalNumberOfMin = parseFloat(allTableInformation['MinNlEu']) + parseFloat(allTableInformation['MinNlRow']) + parseFloat(allTableInformation['MinNlExc']);
      setValueOfAttributeNotCurrency('MinNaarInter', naarInternationalNumberOfMin);
  }
}

function calculateNaarInternationalNumberOfSec() {
  if(CS.getAttributeValue('DDISectionEnabled_0') == false) {
      setValueOfAttributeNotCurrency('SecNaarInter', 0);
  } else {
      var naarInternationalNumberOfSec = parseFloat(allTableInformation['SecNlEu']) + parseFloat(allTableInformation['SecNlRow']) + parseFloat(allTableInformation['SecNlExc']);
      setValueOfAttributeNotCurrency('SecNaarInter', naarInternationalNumberOfSec);
  }
}

window.checkIfAllDicountsAreValid = function checkIfAllDicountsAreValid() {
   jQuery('.slds-input').each(function() {
       if(jQuery(this) && jQuery(this).attr('name')) {
         if (jQuery(this).attr('name').indexOf("Discount") >= 0) {
             if (jQuery(this).val() > 100 || jQuery(this).val() < 0) {
                 invalidateInputField(jQuery(this).attr('name'));
             } else {
                 validateInputField(jQuery(this).attr('name'));
             }
         }
       }
  });
}

/**
* Checks to see if value of input fields is numerical. Before checking, removes all commas.
*/
window.checkIfAllValuesAreNumerical = function checkIfAllValuesAreNumerical() {
  jQuery('.slds-input').each(function() {
      if(jQuery(this) && jQuery(this).attr('name')) {
         if (jQuery(this).attr('value') != '') {
            var value = jQuery(this).attr('value').toString().replace(/,/g, '');
            if(!jQuery.isNumeric(value)) {
               invalidateInputField(jQuery(this).attr('name'));
            } else {
               validateInputField(jQuery(this).attr('name'));
            }
         } else {
             setValueOfAttributeNotCurrency(jQuery(this).attr('name'), jQuery(this).attr('value'));
         }	
      }
  });
  
  if (allTableInformation['relatedVoiceProposition'] == '') {		
     invalidateRelatedVoicePropositionInput();
  } else {
     validateRelatedVoicePropositionInput();
  }

  if (allTableInformation['TotalSipQuant'] == null || allTableInformation['TotalSipQuant'].toString() == '') {		
     invalidateInputField('TotalSipQuant');
     removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
     clearOFExpressFlags();
     CS.markConfigurationInvalid('Total SIP Quantity has to be set.');
     
  } else {
     validateInputField('TotalSipQuant');
     //addPAYUItem();
  }
}

/**
* Updates value of amount input fields to currency.
*/
function updateAmountToCurrancy() {
  jQuery('.slds-input').each(function() {
          if(jQuery(this) && jQuery(this).attr('name')) {
            if (jQuery(this).attr('name').indexOf("Amount") >= 0) {
               setValueOfAttribute(jQuery(this).attr('name'), parseFloat(jQuery(this).attr('value').toString().replace(/,/g, '')));
            }	
          }
     });
}

function calculateAmountSum() {
  var amountSum = 0;
  jQuery('.slds-input').each(function() {
          if(jQuery(this) && jQuery(this).attr('name')) {
            if (jQuery(this).attr('name').indexOf("Amount") >= 0) {
               amountSum = amountSum + parseFloat(jQuery(this).attr('value').toString().replace(/,/g, ''));
            }	
          }
     });
  return amountSum;
}

function calculateMinutesTotal() {
  var sum = 0;
  jQuery('.slds-input').each(function() {
      if(jQuery(this) && jQuery(this).attr('name')) {
         if (jQuery(this).attr('name').indexOf("MinTotal") >= 0) {
             if (jQuery(this).attr('name').indexOf("NlEu") == -1 && jQuery(this).attr('name').indexOf("NlRow") == -1 && jQuery(this).attr('name').indexOf("NlExc") == -1) {
                 sum += parseInt(jQuery(this).val());
             }
         }
      }
   });
  return sum;
}

function checkTenPercentIddRule() {
  var minutesTotal = calculateMinutesTotal();
  if(minutesTotal == 0) {
     return;
  } else {
     if(parseFloat(jQuery('[name="MinTotalNaarInter"]').val()) / minutesTotal > 0.1 && CS.getAttributeValue('DDISectionEnabled_0') == false) {
        CS.markConfigurationInvalid("International minutes is more then 10% of whole minutes SUM. You have to specify IDD!");
     } 
  }
}

/**
* Creates 
*/
function invalidateInputField(fieldName) {
  jQuery('[name=' + fieldName + ']').css("border-color", "red");
}

function validateInputField(fieldName) {
  jQuery('[name=' + fieldName + ']').css("border-color", "#d8dde6");
}

function invalidateRelatedVoicePropositionInput() {
  jQuery('[name=relatedVoiceProposition]').css("border-color", "red");
}

function validateRelatedVoicePropositionInput() {
  jQuery('[name=relatedVoiceProposition]').css("border-color", "#d8dde6");
}

function getPpNmotCst() {
  if (allTableInformation['relatedVoiceProposition'] != '') {		
     for (j in ppNmoTariffs) {
        if (j == allTableInformation['relatedVoiceProposition']) {
           return parseFloat(ppNmoTariffs[j]['PiekOnPabx']);
        }
     }
  }
}

/*
* Binnen uw regio. 28
*/
function getUctFixedToRegionalFixed() {
  if (allTableInformation['relatedVoiceProposition'] != '') {		
     for (j in ppNmoTariffs) {
        if (j == allTableInformation['relatedVoiceProposition']) {
           return parseFloat(ppNmoTariffs[j]['FixedToRegionalFixed'].toString().replace(',', '.'));
        }
     }
  }	
}

/*
* Buiten uw regio. 29
*/
function getUctFixedToNationalFixed() {
  if (allTableInformation['relatedVoiceProposition'] != '') {		
     for (j in ppNmoTariffs) {
        if (j == allTableInformation['relatedVoiceProposition']) {
           return parseFloat(ppNmoTariffs[j]['FixedToNationalFixed'].toString().replace(',', '.'));
        }
     }
  }	
}

/*
* Naar mobiele nummers
*/
function getUctFixedToMobileKPN() {
  if (allTableInformation['relatedVoiceProposition'] != '') {		
     for (j in ppNmoTariffs) {
        if (j == allTableInformation['relatedVoiceProposition']) {
           return parseFloat(ppNmoTariffs[j]['FixedToMobileKPN'].toString().replace(',', '.'));
        }
     }
  }	
}


function getUctFBuurlandenToFixed() {
  if (allTableInformation['relatedVoiceProposition'] != '') {		
     for (j in ppIddTariffs) {
        if (j == allTableInformation['relatedVoiceProposition']) {
           return parseFloat(ppIddTariffs[j]['FBuurlandenToFixed'].toString().replace(',', '.'));
        }
     }
  }
}

function getUctFRestEuropaToFixed() {
  if (allTableInformation['relatedVoiceProposition'] != '') {		
     for (j in ppIddTariffs) {
        if (j == allTableInformation['relatedVoiceProposition']) {
           return parseFloat(ppIddTariffs[j]['FRestEuropaToFixed'].toString().replace(',', '.'));
        }
     }
  }
}

function getUctFRestWorldToFixed() {
  if (allTableInformation['relatedVoiceProposition'] != '') {		
     for (j in ppIddTariffs) {
        if (j == allTableInformation['relatedVoiceProposition']) {
           return parseFloat(ppIddTariffs[j]['FRestWorldToFixed'].toString().replace(',', '.'));
        }
     }
  }
}

function getPpIddCst() {
  if (allTableInformation['relatedVoiceProposition'] != '') {		
     for (j in ppIddTariffs) {
        if (j == allTableInformation['relatedVoiceProposition']) {
           return parseFloat(ppIddTariffs[j]['StartTariff'].toString().replace(',', '.'));
        }
     }
  }
}

function getValueFromPpNmoTariffsJSON(nameOfattribute) {
  for (j in fixedBelspendRefJson) { 
     if(j == nameOfattribute) {
        return fixedBelspendRefJson[j]['Value'];
     }
  }
}

function showMonthlyInvoiceTable() {
  
  var monthlyInvoiceTable = '<div id="monthlyInvoiceToStore"><table class="list" id="myUsageTable"><tr class="headerRow">'
                          + '<th></th>'
                          + '<th><label># Calls</label></th>'
                          + '<th><label>Hrs.</label></th>'
                          + '<th><label>Min.</label></th>'
                          + '<th><label>Sec.</label></th>'
                          + '<th><label>Amount (€ excl. VAT)</label></th>'
                          + '<th><label id="discountToRemove">Discount (%)</label></th>'
                          + '<th><label>Net Order Intake (€)</label></th>'
                          + '<th><label>Total Gross (€)</label></th>'
                          + '<th><label>Min. (Total)</label></th>'
                          + '<th><label>Cost Tariff</label></th></tr>' // TP
                          + '<tr>'
                          +	'<td><label>Binnen uw regio<label></td>'
                          +	'<td><input type="text" step="any" name="CallsBinnen" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsBinnen" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinBinnen" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecBinnen" class="slds-input" onChange=""></td>'
                          +	'<td><input type="text" step="any" name="AmountBinnen" class="slds-input"></td>'
                          + 	'<td><input type="text" step="any" name="naarVasteNummersDiscount" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderBinnen" class="slds-input intake"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossBinnen" class="slds-input gross"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalBinnen" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffBinnen" class="slds-input"></td>'
                          + '</tr>'
                          + '<tr>'
                          +	'<td><label>Buiten uw regio<label></td>'
                          +	'<td><input type="text" step="any" name="CallsBuiten" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsBuiten" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinBuiten" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecBuiten" class="slds-input" onChange=""></td>'
                          +	'<td><input type="text" step="any" name="AmountBuiten" class="slds-input"></td>'
                          + 	'<td><input type="text" step="any" name="naarVasteNummersDiscount" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderBuiten" class="slds-input intake"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossBuiten" class="slds-input gross"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalBuiten" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffBuiten" class="slds-input"></td>'
                          + '</tr>'
                          + '<tr>'
                          +	'<td><label>Naar internationaal<label></td>'
                          +	'<td><input type="text" step="any" name="CallsNaarInter" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsNaarInter" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinNaarInter" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecNaarInter" class="slds-input" onChange=""></td>'
                          +	'<td><input type="text" step="any" name="AmountNaarInter" class="slds-input"></td>'
                          + 	'<td><input type="text" step="any" name="naarInternationalDiscount" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderNaarInter" class="slds-input intake"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossNaarInter" class="slds-input gross"></td><td>'
                          +	'<input type="text" step="any" disabled="" name="MinTotalNaarInter" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffNaarInter" class="slds-input"></td>'
                          + '</tr>'
                          + '<tr><td><label>Naar mobiele nummers<label></td>'
                          +	'<td><input type="text" step="any" name="CallsNaarMobile" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsNaarMobile" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinNaarMobile" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecNaarMobile" class="slds-input" onChange=""></td>'
                          +	'<td><input type="text" step="any" name="AmountNaarMobile" class="slds-input"></td>'
                          + 	'<td><input type="text" step="any" name="naarMobieleNummersDiscount" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderNaarMobile" class="slds-input intake"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossNaarMobile" class="slds-input gross"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalNaarMobile" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffNaarMobile" class="slds-input"></td>'
                          + '</tr>'
                          + '<tr>'
                          +	'<td><label>Naar 088 bedrijfsnummers<label></td>'
                          +	'<td><input type="text" step="any" name="CallsNaar088" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsNaar088" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinNaar088" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecNaar088" class="slds-input" onChange=""></td>'
                          +	'<td><input type="text" step="any" name="AmountNaar088" class="slds-input"></td>'
                          + 	'<td><input type="text" step="any" name="naarVasteNummersDiscount" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderNaar088" class="slds-input intake"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossNaar088" class="slds-input gross"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalNaar088" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffNaar088" class="slds-input"></td>'
                          + '</tr>'
                          + '<tr>'
                          +	'<td><label>Naar 0900 servicenummers<label></td>'
                          +	'<td><input type="text" step="any" name="CallsNaar0900" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsNaar0900" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinNaar0900" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecNaar0900" class="slds-input" onChange=""></td>'
                          +	'<td><input type="text" step="any" name="AmountNaar0900" class="slds-input"></td>'
                          +   '<td></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderNaar0900" class="slds-input intake"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossNaar0900" class="slds-input gross"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalNaar0900" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffNaar0900" class="slds-input"></td>'
                          + '</tr>'
                          + '<tr>'
                          +	'<td><label>Naar Service nummers BTW vrij<label></td>'
                          +	'<td><input type="text" step="any" name="CallsNaarBTW" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsNaarBTW" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinNaarBTW" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecNaarBTW" class="slds-input" onChange=""></td>'
                          +	'<td><input type="text" step="any" name="AmountNaarBTW" class="slds-input"></td>'
                          +   '<td></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderNaarBTW" class="slds-input intake"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossNaarBTW" class="slds-input gross"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalNaarBTW" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffNaarBTW" class="slds-input"></td>'
                          + '</tr>'
                          + '<tr>'
                          +	'<td><label>Overige gesprekken<label></td>'
                          +	'<td><input type="text" step="any" name="CallsOverige" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsOverige" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinOverige" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecOverige" class="slds-input" onChange=""></td>'
                          +	'<td><input type="text" step="any" name="AmountOverige" class="slds-input"></td>'
                          +   '<td></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderOverige" class="slds-input intake"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossOverige" class="slds-input gross"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalOverige" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffOverige" class="slds-input"></td>'
                          + '</tr>'
                          + '<td><b><label>Total Gross (€)<label></b></td>'
                          + '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td id="totalGross"><input type="text" step="any" disabled="" name="totalGrossSum" class="slds-input"></td>'
                          + '<tr>'
                          + '</tr>'
                          + '<td><b><label>Net Order Intake (€)</label></b></td>'
                          + '<td></td><td></td>'
                          + '<td></td><td></td><td></td><td></td><td id="netOrder"><input type="text" step="any" disabled="" name="totalNetOrderSum" class="slds-input">'
                          + '</td>'
                          + '<tr>'
                          + '</tr>'
                          + '<td><b><label>Total Costs (€)</label></b></td>'
                          + '<td></td><td></td><td></td><td></td><td></td><td></td></td><td></td></td></td><td></td><td></td>'
                          + '<td id="totalCosts"><input type="text" step="any" disabled="" name="TotalCosts" class="slds-input"></td>'
                          + '<tr>'
                          + '</table>'
                          + '</div>';

  monthlyInvoiceTable = jQuery(monthlyInvoiceTable);

  if (jQuery('#myUsageTable')[0] == undefined) {
     jQuery('[data-cs-binding=UsageTable_0]').parent().parent().prepend(monthlyInvoiceTable);
  }

  // TODO Move this to CSS.
  jQuery('#myUsageTable').find('td').css("width", "none");
  jQuery('#myUsageTable').find('th').css("border", "none");
  jQuery('#myUsageTable').find('th').css("border-color", "white");
  jQuery('#myUsageTable').find('th').css("padding", "3px");
  jQuery('#myUsageTable').find('td').css("border", "none");
  jQuery('#myUsageTable').find('td').css("border-color", "white");
  jQuery('#myUsageTable').find('tr').css("border", "none");
  jQuery('#myUsageTable').find('tr').css("padding", "2px");
  jQuery('#myUsageTable').find('tr').css("border-color", "white");
  jQuery('#myUsageTable').find('input').css("width", "112px");
  jQuery('#myUsageTable').find('input').css("border-color", "#e6ebf2");
  jQuery('#myUsageTable').find('label').addClass("slds-form-element__label");
  jQuery('#myUsageTable').find('input').css("text-align", "right");
}

function calculateNetOrderIntakeSum() {
  if (!(CS.Service.getCurrentScreenRef()=="Company_Level_Fixed_Voice:Fixed_Usage_Profile"))
     return;
     var totalNetOrderSum = 0;
     jQuery('.intake').each(function() {
        totalNetOrderSum += parseFloat(allTableInformation[jQuery(this).attr('name')].toString().replace(/,/g, ''));
     });
     
     setValueOfAttribute('totalNetOrderSum', totalNetOrderSum);
     CS.setAttributeValue('TotalNetValueUsage_0', totalNetOrderSum);
     try {
         CS.setAttributeValue('PayAsYouUse_product_0:RC_List_Price_0', totalNetOrderSum);
          CS.setAttributeField('PayAsYouUse_product_0:Recurring_Charge_0', 'Price', totalNetOrderSum);
          CS.setAttributeField('PayAsYouUse_product_0:Recurring_Charge_0', 'list_price', totalNetOrderSum);
          //CS.setAttributeValue('PayAsYouUse_product_0:RC_Cost_0', sumCostPriceTariff);
     } catch(err){
         // IGNORE
     }
}

/**
 * We have a special use case, according to W-002396
 * From Luuk's mail:
 * (1) Worry Free:Yes – RoW bundle:No – IDD RoW:/Exceptions has value:Yes then PAYU>0 (RoW+Exceprions minutes) and Fixed Voice usage>0 (RoW+Exceprions minutes)
 * (2) Worry Free:Yes – RoW bundle:Yes – IDD RoW:/Exceptions has value:Yes then PAYU>0 (but only from Exceptions minutes) and Fixed Voice usage>0(but only from Exceptions minutes)
 * Basically, when we have WorryFree on CLFV, but IDD RoW and/or exceptions have some value, then this price becomes reccuring price on PAYU
 * However that value will NOT BE in the contract
 */
function calculateNetOrderExceptionsAndRoW()
{
   if (!(CS.Service.getCurrentScreenRef()=="Company_Level_Fixed_Voice:Fixed_Usage_Profile"))
      return;
   if(allTableInformation['flatFeeDropdown'] != 'fixedflatfee')
      return;

   let hasRoWbundle = CS.Service.config["RoW_bundle_product_0"].relatedProducts.length;

   var partialNetOrderSum = 0;
   let nlRow = parseFloat(allTableInformation['NetOrderNlRow'].toString().replace(/,/g, ''));
   let nlExc = parseFloat(allTableInformation['NetOrderNlExc'].toString().replace(/,/g, ''));

   partialNetOrderSum = nlExc;
   if(!hasRoWbundle)
      partialNetOrderSum += nlRow;

   // TODO - how to actually calculate this

   try {
       CS.setAttributeValue('PayAsYouUse_product_0:RC_List_Price_0', partialNetOrderSum);
        CS.setAttributeField('PayAsYouUse_product_0:Recurring_Charge_0', 'Price', partialNetOrderSum);
        CS.setAttributeField('PayAsYouUse_product_0:Recurring_Charge_0', 'list_price', partialNetOrderSum);
   } catch(err){
       // IGNORE
   }
}


function calculateCost()
{
  if (!(CS.Service.getCurrentScreenRef()=="Company_Level_Fixed_Voice:Fixed_Usage_Profile"))
     return;
  var totalCost = 0.0;
  jQuery('.slds-input').each(function() {
     var attributeName = jQuery(this).attr('name');
     if (attributeName != null) {
         if(attributeName.indexOf("CostTariff") > -1) {
            var attributeSuffix = attributeName.substring(attributeName.indexOf("CostTariff")+ 10);
            var minutesAttribute = 'MinTotal' + attributeSuffix;
            let currentCostTariff = parseFloat(allTableInformation[attributeName].toString().replace(/,/g, ''));
            let currentMinutes =  parseFloat(allTableInformation[minutesAttribute].toString().replace(/,/g, ''));
            totalCost += currentCostTariff * currentMinutes;
         }
     }
 });

  try {
     CS.setAttributeValue('PayAsYouUse_product_0:RC_Costs_0', totalCost);
     setValueOfAttribute('TotalCosts', totalCost);
  } catch(err){
     // IGNORE
  }

}

function calculateTotalGrossSum() {
     var totalGross = 0;
     jQuery('.gross').each(function() {
        totalGross += parseFloat(allTableInformation[jQuery(this).attr('name')].toString().replace(/,/g, ''));
     });
     setValueOfAttribute('totalGrossSum', totalGross);
}

function showDiscounts() {
  var discountTable = ''
  + '<div id="discounts">'
  + 	'<div class="slds-box">'
  + 		'<table class="list" id="discountTable" style="text-align:left;">'
  + 			'<tr>'
  + 				'<td><label>Discounts (%)</label></td>'
  + 				'<td>'
  + 				'</td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>Naar vaste nummers</label></td>'
  + 				'<td><input type="text" step="any" name="naarVasteNummersDiscount" class="slds-input"></td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>Naar mobiele nummers</label></td>'
  + 				'<td><input type="text" step="any" name="naarMobieleNummersDiscount" class="slds-input"></td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>Naar internationaal</label></td>'
  + 				'<td><input type="text" step="any" name="naarInternationalDiscount" class="slds-input"></td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>NL to EU+ (only when IDD checked)</label></td>'
  + 				'<td><input type="text" step="any" name="nlToEuOnlyWhenIDDCheckedDiscount" class="slds-input"></td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>NL to RoW (only when IDD checked)</label></td>'
  + 				'<td><input type="text" step="any" name="nlToRowOnlyWhenIDDCheckedDiscount" class="slds-input"></td>'
  + 			'</tr>'
  + 			'<tr>'
  + 				'<td><label>NL To Exceptions (only when IDD checked)</label></td>'
  + 				'<td><input type="text" step="any" name="nlToExceptionsOnlyWhenIDDCheckedDiscount" class="slds-input"></td>'
  + 			'</tr>'
  + 		'</table>'
  + 	'</div>'
  + '</div>';

  discountTable = jQuery(discountTable);
  if (jQuery('#discountTable')[0] == undefined) {
     jQuery('[data-cs-binding=Discount_Table_0]').parent().parent().prepend(discountTable);
  }

  // TODO Move this to CSS.
  jQuery('#discountTable').find('td').css("width", "none");
  jQuery('#discountTable').find('th').css("border", "none");
  jQuery('#discountTable').find('th').css("border-color", "white");
  jQuery('#discountTable').find('th').css("padding", "3px");
  jQuery('#discountTable').find('td').css("border", "none");
  jQuery('#discountTable').find('td').css("border-color", "white");
  jQuery('#discountTable').find('tr').css("border", "none");
  jQuery('#discountTable').find('tr').css("padding", "2px");
  jQuery('#discountTable').find('tr').css("border-color", "white");
  jQuery('#discountTable').find('input').css("width", "120px");
  jQuery('#discountTable').find('th').css("width", "120px");
  jQuery('#discountTable').find('td').css("width", "120px");
  jQuery('#discountTable').find('select').css("width", "140px");
  jQuery('#discountTable').find('input').css("border-color", "#e6ebf2");
  jQuery('#discountTable').find('label').addClass("slds-form-element__label");
  jQuery('#discountTable').find('input').css("text-align", "right");
}

function showFlatFee() {
  var scenarioCheck = CS.getAttributeValue('One_Fixed_Scenario_0');
  var flatFeeTable = '<div id="flatFeeToStore">'
                 +	'<table class="list" id="flatFeeTable" style="width:100%;text-align:left;">'
                 +		'<tr>'
                 +			'<th><label></label></th>'
                 +			'<th><label>Deal Type</label></th>'
                 +			'<th><label>Total SIP Quantity</label></th>'
                 +			'<th><label>Tariff (€)</label></th>'
                 +			'<th><label>Net Price (€)</label></th>'
                 +			'<th><label>Gross Total (€)</label></th>'
                 +			'<th><label>Net Total (€)</label></th>'
                 +		'</tr>' 
                    +		'<tr>'
                    +			'<td>'
                    +				'<select class="slds-select" name="flatFeeDropdown" onchange="flatFeeDropdownChanged(this)">'
                    +					'<option selected="selected" value="fixedflatfee">Worry Free</option>'
                    +					'<option value="payasyouuse">Pay as You Use</option>'
                    +				'</select>'
                    +			'</td>'
                    +			'<td>'
                    +           '<select class="slds-select" name="DealType">'
                    +					'<option selected="selected" value="Acquisition">Acquisition</option>'
                    +                   '<option value="Retention">Retention</option>'
                    +                   '<option value="Retention">Migration</option>'
                    +			'</select>'
                    +			'</td>'
                    +			'<td>'
                    +				'<input type="text" step="any" name="TotalSipQuant" class="slds-input">'
                    +			'</td>'
                    +			'<td>'
                    +				'<input type="text" disabled="" step="any" name="Tariff" class="slds-input">'
                    +			'</td>'
                    +			'<td>'
                    +				'<input type="text" disabled="" step="any" name="NetPrice" class="slds-input">'
                    +			'</td>'
                    +			'<td>'
                    +				'<input type="text" step="any" disabled="" name="GrossTotal" class="slds-input">'
                    +			'</td>'
                    +			'<td>'
                    +				'<input type="text" step="any" name="NetTotal" disabled="" class="slds-input">'
                    +			'</td>'
                    +		'</tr>'
                    +	'</table>'
                    + '</div>';

  flatFeeTable = jQuery(flatFeeTable);
  if (jQuery('#flatFeeTable')[0] == undefined) {
     jQuery('[data-cs-binding=FlatFee_0]').parent().parent().prepend(flatFeeTable);
  }

  // TODO Move this to CSS.
  jQuery('#flatFeeTable').find('td').css("width", "none");
  jQuery('#flatFeeTable').find('th').css("border", "none");
  jQuery('#flatFeeTable').find('th').css("border-color", "white");
  jQuery('#flatFeeTable').find('th').css("padding", "3px");
  jQuery('#flatFeeTable').find('td').css("border", "none");
  jQuery('#flatFeeTable').find('td').css("border-color", "white");
  jQuery('#flatFeeTable').find('tr').css("border", "none");
  jQuery('#flatFeeTable').find('tr').css("padding", "2px");
  jQuery('#flatFeeTable').find('tr').css("border-color", "white");
  jQuery('#flatFeeTable').find('input').css("width", "120px");
  jQuery('#flatFeeTable').find('th').css("width", "120px");
  jQuery('#flatFeeTable').find('td').css("width", "120px");
  jQuery('#flatFeeTable').find('select').css("width", "140px");
  jQuery('#flatFeeTable').find('input').css("border-color", "#e6ebf2");
  jQuery('#flatFeeTable').find('label').addClass("slds-form-element__label");
  jQuery('#flatFeeTable').find('input').css("text-align", "right");

}

function hideDDI() {
  jQuery('#ddiTable').css("display", "none");
}


function showDDI() {
  
  var monthlyInvoiceTable = '<div id="iddToStore"><table class="list" id="ddiTable"><tr class="headerRow">'
                          + '<th></th>'
                          + '<th><label># Calls</label></th>'
                          + '<th><label>Hrs.</label></th>'
                          + '<th><label>Min.</label></th>'
                          + '<th><label>Sec.</label></th>'
                          + '<th><label id="iddDiscountToRemove">Discount (%)</label></th>'
                          + '<th><label>Net Order Intake (€)</label></th>'
                          + '<th><label>Total Gross (€)</label></th>'
                          + '<th><label>Min. (Total)</label></th>'
                          + '<th><label>Cost Tariff</label></th></tr>'
                          + '<tr><td><label>NL to EU+</label></td>'
                          +	'<td><input type="text" step="any" name="CallsNlEu" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsNlEu" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinNlEu" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecNlEu" class="slds-input" onChange=""></td>'
                          +  '<td><input type="text" step="any" name="nlToEuOnlyWhenIDDCheckedDiscount" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderNlEu" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossNlEu" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalNlEu" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffNlEu" class="slds-input"></td></tr>'
                          + '<tr><td><label>NL to RoW</label></td>'
                          +	'<td><input type="text" step="any" name="CallsNlRow" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsNlRow" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinNlRow" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecNlRow" class="slds-input" onChange=""></td>'
                          +	'<td><input type="text" step="any" name="nlToRowOnlyWhenIDDCheckedDiscount" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="NetOrderNlRow" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="TotalGrossNlRow" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalNlRow" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="CostTariffNlRow" class="slds-input"></td></tr>'
                          + '<tr><td><label>NL to Exceptions</label></td>'
                          +	'<td><input type="text" step="any" name="CallsNlExc" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="HrsNlExc" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="MinNlExc" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="SecNlExc" class="slds-input" onChange=""></td>'
                          + 	'<td><input type="text" step="any" name="nlToExceptionsOnlyWhenIDDCheckedDiscount" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="NetOrderNlExc" disabled="" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" name="TotalGrossNlExc" disabled="" class="slds-input"></td>'
                          +	'<td><input type="text" step="any" disabled="" name="MinTotalNlExc" class="slds-input"></td>'
                          +  '<td><input type="text" step="any" disabled="" name="CostTariffNlExc" class="slds-input"></td></tr></table></div>';
                          

  monthlyInvoiceTable = jQuery(monthlyInvoiceTable);

  if (jQuery('#ddiTable')[0] == undefined) {
     jQuery('[data-cs-binding=DDI_0]').parent().parent().prepend(monthlyInvoiceTable);
  }

  // TODO Move this to CSS.
  jQuery('#ddiTable').find('td').css("width", "none");
  jQuery('#ddiTable').find('th').css("border", "none");
  jQuery('#ddiTable').find('th').css("border-color", "white");
  jQuery('#ddiTable').find('th').css("padding", "3px");
  jQuery('#ddiTable').find('td').css("border", "none");
  jQuery('#ddiTable').find('td').css("border-color", "white");
  jQuery('#ddiTable').find('tr').css("border", "none");
  jQuery('#ddiTable').find('tr').css("padding", "2px");
  jQuery('#ddiTable').find('tr').css("border-color", "white");
  jQuery('#ddiTable').find('input').css("width", "112px");
  jQuery('#ddiTable').css("display", "block");
  jQuery('#ddiTable').find('input').css("border-color", "#e6ebf2");
  jQuery('#ddiTable').find('label').addClass("slds-form-element__label");
  jQuery('#ddiTable').find('input').css("text-align", "right");
}


/**
*Usage getters 
* 
*/
function getUsageFromForTariff() {
log('getUsageFromForTariff<==');
var oneFixedArray = flatFeeFixedVoice['One Fixed'];
var result = 0;

for (j in oneFixedArray) { 
 if(parseFloat(oneFixedArray[j]['NetTarif'].toString().replace(',', '.')) == allTableInformation['Tariff']) {
  result = oneFixedArray[j]['UsageFrom'];
  log('getUsageFromForTariff==>' + result);
  return result;
 }
}
log('getUsageFromForTariff==>Result was not found in table.');
return 0;
}

function getUsageToForTariff() {
log('getUsageToForTariff<==');
var oneFixedArray = flatFeeFixedVoice['One Fixed'];
var result = 0;

for (j in oneFixedArray) { 
 if(parseFloat(oneFixedArray[j]['NetTarif'].toString().replace(',', '.')) == allTableInformation['Tariff']) {
  result = oneFixedArray[j]['UsageTo'];
  log('getUsageToForTariff==>' + result);
  return result;
 }
}
log('getUsageToForTariff==>Result was not found in table.');
return 0;
}

/**
* end usage getters 
*/

/**
*FUP rulling 
* check total mins for ALL callstreams
*/

//
function checkTotalMinutesForAllCallStreams(){
   var callType = ['Binnen','Buiten','NaarInter','NaarMobile','Naar088','Naar0900','NaarBTW','Overige'];
   var pref = ['Hrs', 'Min', 'Sec'];
   var hasOneValue = false;
   for(var bIdx in callType){
      for(var prefIdx in pref){
         var comb =  pref[prefIdx]+callType[bIdx];
         if(allTableInformation[comb] && allTableInformation[comb] != '0.00'&& allTableInformation[comb] != '0'){
            hasOneValue = true;
         }
      }
   }
   
   if(hasOneValue==false){
       var message = getErrorMessageNoInputsBasedOnCompetitorType();
       CS.markConfigurationInvalid(message);
     log('>>>>>>>Does not have one value.');
   } else {
     log('>>>>>>>Does have one value.');
  }
}

function getErrorMessageNoInputsBasedOnCompetitorType(){
   if(allTableInformation["competitorInvoice"] == 'default' || jQuery('[name=competitorInvoice]').val() == 'default'){
       return 'Please fill in the monthly invoice details';
   }
   else{
       return 'Please fill in the exact values from the KPN invoice. These are 2 month totals but the total order intake calculation will recalculate accordingly.';
   }
}

function numOfSIPRule(){
  if (allTableInformation['flatFeeDropdown'] != 'payasyouuse') {
      var sipInput = jQuery('input[name=TotalSipQuant]').val();
      if(sipInput == null) {
          sipInput = allTableInformation['TotalSipQuant'];
      }
      var basketSIP = CS.getAttributeValue('Basket_Number_SIP_0').toString();
      if(sipInput!='' && basketSIP!='' && sipInput != basketSIP){
          removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
          clearOFExpressFlags();
          if (jQuery('[name="flatFeeDropdown"]').val() != 'payasyouuse') {
            CS.markConfigurationInvalid('Please make sure the number of SIP in the basket matches the number of SIP in Company Level Fixed Voice ('+basketSIP+')');
          }
      }
  }
}

function fixedScenario(){
   var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');
   var dependentConfigurations = CS.getAttributeValue('Dependent_Configurations_0');
   if((scenarioVal == '' || scenarioVal =='NONE') && (dependentConfigurations == null || dependentConfigurations == '')){
       CS.markConfigurationInvalid('Please add one of the following products to the basket and choose scenario where needed:' + 
       'One Fixed, One Net, ONE NET (Flex Only), Skype for Business, Vodafone Calling');
   }
   if(scenarioVal == 'ERROR'){
       CS.markConfigurationInvalid('Either One Fixed Express or One Fixed Enterprise scenario can be present in the basket. Please correct one fixed scenario values in access infrastructure.');
   }
}

function scenarioRules(){
   
   if(allTableInformation['flatFeeDropdown'] == null) {
       allTableInformation['flatFeeDropdown'] = 'fixedflatfee';
   }
   
   if (CS.Service.getCurrentScreenRef()=="Company_Level_Fixed_Voice:Fixed_Usage_Profile")
   {
      setDealType();
   }
      
  //rowBundleAddition();

  // in case we have Vodafone Calling or Skype for Business in the basket, scenario will be 'One Fixed Enterprise'
  // this is according to agreement with Eelco
  // this setting is done through CLFV PD rule with grouping name 'Set scenario for dependent products'
   
   var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');
   
   if(scenarioVal.indexOf('One Fixed') == -1 && scenarioVal.indexOf('OneFixed') == -1) {
       jQuery("[data-cs-binding='FlatFee_product_0']").hide();
       CS.Service.removeRelatedProduct('FlatFee_product_0');
   } else {
       if(scenarioVal == 'One Fixed Enterprise' || allTableInformation['flatFeeDropdown'] == 'payasyouuse'){
           scenarioOneFixedEnterprise();
       } 
       
       if(scenarioVal == 'One Fixed Express'  || allTableInformation['flatFeeDropdown'] == 'fixedflatfee'){
           scenarioOneFixedExpress();
       }
       
       if(scenarioVal == 'One Fixed Express'  && allTableInformation['flatFeeDropdown'] == 'fixedflatfee') {
           /* Removed during ROW bundle implementation
           jQuery('[name="RoW_bundle_selection_0"]').prop("disabled", false);
           */
       } else {
           /* Removed during ROW bundle implementation
           jQuery('[name="RoW_bundle_selection_0"]').prop('checked', false).attr('checked', false).removeAttr('checked');
           CS.setAttributeValue('RoW_bundle_selection_0', false, true);
           //jQuery('[name="RoW_bundle_selection_0"]').prop('checked', false);
           jQuery('[name="RoW_bundle_selection_0"]').prop("disabled", true);
           
           if (CS.Service.config["RoW_bundle_product_0"].relatedProducts.length > 0) {
               for (var idx in CS.Service.config['RoW_bundle_product_0'].relatedProducts) {
                   if(idx != 'remove') {
                       CS.Service.removeRelatedProduct('RoW_bundle_product_' + idx);
                   }
               }  
           }
           */
       }
   }
   
   if(scenarioVal.indexOf('One Net') == -1 && scenarioVal.indexOf('OneNet') == -1) {
       // jQuery("[data-cs-binding='FlatFee_product_0']").hide();
       // CS.Service.removeRelatedProduct('FlatFee_product_0');
   } else {
       if(scenarioVal == 'One Net Enterprise' || allTableInformation['flatFeeDropdown'] == 'payasyouuse'){
           scenarioOneFixedEnterprise();
       } 
       
       if(scenarioVal == 'One Net Express'  || allTableInformation['flatFeeDropdown'] == 'fixedflatfee'){
           scenarioOneFixedExpress();
       }
       
       if(scenarioVal == 'One Net Express'  && allTableInformation['flatFeeDropdown'] == 'fixedflatfee') {
           /* Removed during ROW bundle implementation
           jQuery('[name="RoW_bundle_selection_0"]').prop("disabled", false);
           */
       } else {
           /* Removed during ROW bundle implementation
           jQuery('[name="RoW_bundle_selection_0"]').prop('checked', false).attr('checked', false).removeAttr('checked');
           CS.setAttributeValue('RoW_bundle_selection_0', false, true);
           //jQuery('[name="RoW_bundle_selection_0"]').prop('checked', false);
           jQuery('[name="RoW_bundle_selection_0"]').prop("disabled", true);
           
           if (CS.Service.config["RoW_bundle_product_0"].relatedProducts.length > 0) {
               for (var idx in CS.Service.config['RoW_bundle_product_0'].relatedProducts) {
                   if(idx != 'remove') {
                       CS.Service.removeRelatedProduct('RoW_bundle_product_' + idx);
                   }
               }  
           }
           */
       }
   }

   //addPAYUItem(); // TP - W-002115 - always add PAYU product to company level fixed voice
   
}

function setrelatedVoiceProposition(){
  if(jQuery('select[name=relatedVoiceProposition] option').length < 2) {
     for (j in ppNmoTariffs) { 
        jQuery('select[name=relatedVoiceProposition]').append(jQuery("<option></option>").attr("value", j).text(j));
     }
  }
}

function scenarioOneFixedEnterprise(){
  iiDSet();
  //add fixed flat fee item
  optionalFlatFee();
   payuSelected(); // TP moved up so always is called
   //CS.Service.removeRelatedProduct('FlatFee_product_0');
}

function scenarioOneFixedExpress(){
  //flatfee optional 
  fixedFlatFee();
  fixedFlatFeeItemAddition();
  //CS.Service.removeRelatedProduct('PayAsYouUse_product_0');
}

function iiDSet(){
   var minNlEu = allTableInformation['MinNlEu'];
   var minNlRow = allTableInformation['MinNlRow'];
   var minNlExc = allTableInformation['MinNlExc'];
   
   if((minNlEu == null || minNlEu == '0') && (minNlRow == null || minNlRow == '0') && (minNlExc == null || minNlExc == '0')){
       //CS.markConfigurationInvalid('IDD Specification is not complete');
   }
}

function fixedFlatFee(){
   //jQuery('select[name=flatFeeDropdown]').val('fixedflatfee'); //***changed as per Maria's request 
   jQuery('select[name=flatFeeDropdown]').attr('disabled', false); //***changed as per Maria's request 
  
  jQuery('[name="TotalSipQuant"').prop("disabled", false);
  //jQuery('[name="Discount"').prop("disabled", false);
}

function optionalFlatFee(){
   jQuery('select[name=flatFeeDropdown]').attr('disabled', false); 
}

function payuSelected(){
   var flatFeeVal = jQuery('select[name=flatFeeDropdown]').val();
   if(flatFeeVal=='payasyouuse'){
       //remove fixed flat fee
       removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
       clearOFExpressFlags();
       //add PAYU
       //addPAYUItem();
   }
   else{
       fixedFlatFeeItemAddition();
   }
}
// removeRelatedProductButtons(['PayAsYouUse_product'],['Copy','Edit'];
// this function enables removing link buttons (Edit, Copy, Delete) from related products
// example: if you want to remove Edit and Copy links for related products attributes "Hardware" and "Services", make following function call
//          removeRelatedProductButtons(['Hardware_', 'Services_'], ['Copy', 'Edit'])
//          if you want to remove all buttons for related product Licences, make following call
//          removeRelatedProductButtons(['Licences'], ['All'])
//          removeRelatedProductButtons(['Mobile_CTN_Addons_'],['All'])
function removeRelatedProductButtons(arrayProducts, arrayButtons)
{
  arrayProducts.forEach(function(item, index, array) {
  jQuery("table tr").each(function() {
  var attr = jQuery(this).attr('data-cs-ref');
  // For some browsers, attr is undefined; for others, attr is false. Check for both.
  
  if (typeof attr !== typeof undefined && attr !== false) {
     var item2 = item.endsWith("_") ? item : (item + '_');
     if(attr.indexOf(item2) >= 0) {
        if(arrayButtons.length == 1 && (arrayButtons[0] == 'All') || (arrayButtons[0] == 'all'))
        {
           jQuery(this).find("td:eq(0)").html("<span>-</span>");
        }
        else
        {
           for (var i = 0; i < arrayButtons.length; i++) {
              jQuery(this).find("td:first:contains('Edit')").find("span:contains('" + arrayButtons[i] + "')").after("<span>-</span>").remove();
           }
           // arrayButtons.forEach(function(item){
           //    jQuery(this).find("td:first:contains('Edit')").find("span:contains('" + item + "')").after("<span>-</span>").remove();
           // })
        }
     }
  }})
  })
}

// Add FixedFlatFee related products in case scenario is One Fixed Express
function scenarioOFExpressAddFFF()
{
   CS.setAttributeValue('CCode4_0', getCCodeFromFlatfeeFixedVoiceJSON(4));
   CS.setAttributeValue('CCode8_0', getCCodeFromFlatfeeFixedVoiceJSON(8));

   setTimeout(function(){ checkPGA('FlatFee_product_0','FlatFee8_set_0','FFF8Lookup_0');}, 0);
   setTimeout(function(){ checkPGA('FlatFee_product_0','FlatFee4_set_0','FFF4Lookup_0');}, 0);

}

function fixedFlatFeeItemAddition(){
   var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');

   if (scenarioVal.indexOf('OneNet') != -1 || scenarioVal.indexOf('One Net') != -1) {
      return;
   }

   if(CS.getAttributeValue('FlatFee_set_0') == '') {

      try{
         var sipInput = jQuery('input[name=TotalSipQuant]').val();
         if(sipInput == null) {
            sipInput = allTableInformation['TotalSipQuant'];
         }
         var basketSIP = CS.getAttributeValue('Basket_Number_SIP_0').toString();

         if (sipInput == '' || sipInput == '0') {
            return;
         }
      
         if(basketSIP != '' && sipInput != basketSIP)
            return;

         var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');

         if(scenarioVal == 'One Fixed Express' && CS.getAttributeValue('Number_of_SIP_8_0') > 0 && CS.getAttributeValue('Number_of_SIP_4_0') > 0)
         {
            scenarioOFExpressAddFFF();
         }
         else 
         {
            CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config['FixedFlatFeeLookup_0']);
         }  

         CS.setAttributeValue('FlatFee_set_0','set');
         jQuery("[data-cs-action='editQuantityRelatedProduct']").prop('disabled', true);
      }
      catch(err){
         removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
         clearOFExpressFlags();
     }
   }
}

function fixedOLDFlatFeeItemAdditionOLD(){
   // removeRelatedProductAndClearFlag('PayAsYouUse_product_0', 'PAYU_set_0');
   if(CS.getAttributeValue('FlatFee_set_0') == '') {
       //add flatFee price item
       //checkPGAUpdated('FlatFee_product_0','FlatFee_set_0','FixedFlatFeeLookup_0');
       
       try{
           //CS.setAttributeValue('FlatFee_set_0','set');
           //jQuery.when(checkPGAUpdated('FlatFee_product_0','FlatFee_set_0','FixedFlatFeeLookup_0')).then(renderAlways());
         // jQuery.when(checkPGAUpdated('FlatFee_product_0','FlatFee_set_0','FixedFlatFeeLookup_0'));
         addFlatFeeProduct = true;
         //CS.setAttributeValue('FlatFee_set_0', 'set');
         
         var sipInput = jQuery('input[name=TotalSipQuant]').val();
          if(sipInput == null) {
              sipInput = allTableInformation['TotalSipQuant'];
          }

          var basketSIP = CS.getAttributeValue('Basket_Number_SIP_0').toString();

          if(sipInput!='' && basketSIP!='' && sipInput != basketSIP){
               
          } else {
            var scenarioVal = CS.getAttributeValue('One_Fixed_Scenario_0');
				if(scenarioVal == 'One Fixed Express' && CS.getAttributeValue('Number_of_SIP_8_0') > 0 && CS.getAttributeValue('Number_of_SIP_4_0') > 0) {
					CS.setAttributeValue('CCode_0', getCCodeFromFlatfeeFixedVoiceJSON(4));
					CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config['FixedFlatFeeLookup_0']);
					
					CS.Service.copyRelatedProduct('FlatFee_product_0', 1);
					
					CS.setAttributeValue('CCode_0', getCCodeFromFlatfeeFixedVoiceJSON(8));
					CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config['FixedFlatFeeLookup_0']);
					
				   } else {
					CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config['FixedFlatFeeLookup_0']);
				   }   
              CS.setAttributeValue('FlatFee_set_0','set');
              jQuery("[data-cs-action='editQuantityRelatedProduct']").prop('disabled', true);
          }
         
          
           
           // CS.setAttributeValue('FlatFee_set_0','set');
           //removeRelatedProductAndClearFlag('PayAsYouUse_product_0', 'PAYU_set_0');
       }
       catch(err){
           log('New render for error');
           removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
           clearOFExpressFlags();
           // renderAlways();
          // CS.setAttributeValue('FlatFee_set_0','set');
       }
   }
}

// W-00239x & T-47080
// called from JS action Add PAYU
function addPAYUItem(){
   if(CS.getAttributeValue('PAYU_set_0') == '') {
       //add flatFee price item
       log('Before addition');
       try{       
           checkPGA('PayAsYouUse_product_0', 'PAYU_set_0', 'PAYU_Lookup_0');
           //CS.EAPI.getAddOnAssociationsListForAttrMoreThanOne(CS.Service.config['PAYU_Lookup_0']);
           jQuery("[data-cs-action='editQuantityRelatedProduct']").prop('disabled', true);
           //CS.setAttributeValue('PAYU_set_0','set');
          //}
       }
       catch(err){
           log('New render for error');
           //renderAlways();
       }
   }
}

function initAddons()
{
   var customPluginJsonObject = {};

   // show or hide widget button
   customPluginJsonObject.hideWidgetButton = true;

   // show only one list of AddOn Price Items and not per Related Product
   customPluginJsonObject.singleAddOnAssociationList = false;

   // hide Delete button in PW
   customPluginJsonObject.disableDefaultAddOnsDeletion = false;

   // use Custom_Group__c in PW
   customPluginJsonObject.useCustomGroups = false;

   // hide all default addons in PW
   customPluginJsonObject.hideDefaultAddOns = false;

   // ability to bulk delete AddOns in PW - only if Max = 1
   customPluginJsonObject.bulkDeletionEnabled = false;

   // restrict adding more than Max value AddOns - currently not being used
   customPluginJsonObject.maxValidationEnabled = false;

   // restrict Min number of AddOns - currently not being used
   customPluginJsonObject.minValidationEnabled = false;

   customPluginJsonObject.addOnPriceItemAssociationLookupAttributeName = 'CS_PricePlan';
   customPluginJsonObject.addOnPriceItemAttributeName = 'CS_AddOn';

   CS.EAPI.initializeWidget(CS.UI.getCurrentInstance().getCurrentConfigReference(), customPluginJsonObject);

   hideConfigurationQuantity();
}

function saveDataStorageHidden() {
   var toStore = '';

   jQuery('.slds-input, .slds-select').each(function() {
      allTableInformation[jQuery(this).attr('name')] = jQuery(this).attr('value');
   });

   for (var key in allTableInformation) {
       toStore = toStore + key + "|" + allTableInformation[key] + ";";
   }
   
   CS.setAttributeValue("DataStorageHidden_0", toStore);
}

function flatFeeDropdownChanged(selectObject) {
   if(selectObject.value =='payasyouuse') {
      jQuery('[name="TotalSipQuant"').prop("disabled", true);
   }
   else {
      jQuery('[name="TotalSipQuant"').prop("disabled", false);
   }
}

function renderAlways() {
   if (CS.Service.getCurrentScreenRef()=="Company_Level_Fixed_Voice:Fixed_Usage_Profile") {
      setTimeout(function(){jQuery('[data-cs-action="Finish"]').hide() }, 0);
      var basketStatus = CS.Service.config[''].linkedObjectProperties.cscfga__Basket_Status__c;
      var buttonCheck = CS.getAttributeValue('ButtonChecker_0');
      if (!buttonCheck && (basketStatus != 'Approved' && basketStatus != 'Contract Created' && basketStatus != 'Pending Approval')) {
         setTimeout(function(){jQuery('[data-cs-action="NextScreen"]').attr("disabled", true); }, 0);
      }
     
      if (jQuery('#calcTotalsBtn').length > 0 && (basketStatus == 'Approved' || basketStatus == 'Contract Created' || basketStatus == 'Pending Approval')) {
        document.getElementById("calcTotalsBtn").disabled = true;
      } else if (jQuery('#calcTotalsBtn').length > 0) {
         document.getElementById("calcTotalsBtn").disabled = false;
      }
	  
      //setTimeout(function(){jQuery('[data-cs-action="NextScreen"]').text('Next'); jQuery('[data-cs-action="NextScreen"]').attr("disabled", false); }, 10000);
      setTimeout(function(){jQuery("button[data-cs-ref='PayAsYouUse_product_0']").hide();},0); 

      setTimeout(function(){jQuery("button[data-cs-ref='FlatFee_product_0']").hide();},0); 
      setTimeout(function(){jQuery("[data-cs-action='editQuantityRelatedProduct']").hide();},0); 
      setTimeout(function(){jQuery("[data-cs-action='editQuantityRelatedProduct']").next('div').remove();},0);
      setTimeout(function(){jQuery("[data-cs-action='editQuantityRelatedProduct']").after('<div>' + allTableInformation['TotalSipQuant'] + '</div>');},0);
     
      //setTimeout(function(){jQuery('[data-cs-action="NextScreen"]').text('Calculate Totals First'); jQuery('[data-cs-action="NextScreen"]').attr("disabled", true); }, 0);
     
      subscribeEvent(CS.EventHandler.Event.BEFORE_NEXT_SCREEN_ACTION,calcBeforeNext,false);
      //setTimeout(function(){jquery ('<div class="CS_configButtons">').children('.slds-col .slds-no-flex .slds-grid .slds-align-top').append('<div class="slds-button-group"><button data-cs-group="CalculateTotals" data-cs-action="CalculateTotals" class="slds-button slds-button--neutral" onclick="refreshAllData()">Calculate Totals</button></div>');},0);
      //setTimeout(function(){jquery('.slds-section__content').append('<input type="button" onclick="refreshAllData()" value="Calculate Totals" />');},0);


      //  // hide finish button
      //  if (CS.Service.getCurrentScreenRef()=="Company_Level_Fixed_Voice:Fixed_Usage_Profile")
      //    setTimeout(function(){ jQuery('[data-cs-action="Finish"]').hide() }, 0);
      var logPrefix = '[Fixed Usage Profile Screen] '; 
      
      log(logPrefix + 'Screen has been loaded.'); 

      showMonthlyInvoiceTable(); 
      showFlatFee(); 
      showRelatedVoiceTable();
      hideAllCostColumns();
      
      if (CS.getAttributeValue('DDISectionEnabled_0') === true) { 
         if (CS.getAttributeValue('DDI_0') == '') { 
            log(logPrefix + 'DDI will be shown.'); 
            showDDI(); 
         } 
      } else { 
         log(logPrefix + 'DDI will NOT be shown.'); 
         hideDDI(); 
      } 
  
      loadAllCalculations(); 
      refreshRelatedFlatFeeProducts();
      hideDiscounts();
      
      if (jQuery('[name="flatFeeDropdown"]').val() == 'payasyouuse') {
         jQuery('[name="TotalSipQuant"').prop("disabled", true);
      }
      else {
         jQuery('[name="TotalSipQuant"').prop("disabled", false);
      }
     
      setTimeout(function() {
         if (jQuery('#calcTotalsBtn').length < 1) {
            var newDiv = document.createElement("div");	
            newDiv.classList.add("cs-btn-group");	
            var btn = document.createElement('input');	
            btn.type = 'button';	
            btn.value = 'Calculate Totals';	
			   btn.style.color = "#0070d2";	
            btn.classList.add("cs-btn");	
            btn.classList.add("cs-btn-default");	
			   btn.classList.add("cs-btn-initial");	
			   btn.classList.add("cs-btn-no-icon");	
			   btn.classList.add("cs-btn-label");	
			   btn.classList.add("icon-breadcrumbs-left");	
            btn.setAttribute("id", "calcTotalsBtn");	
			   btn.setAttribute("data-cs-group", "Previous");
			
            newDiv.appendChild(btn); 
               
            btn.addEventListener('click', function() {
               buttonCheck = true;
               if (buttonCheck) {
                  setTimeout(function(){
                        jQuery('[data-cs-action="NextScreen"]').attr("disabled", false); }
                  , 0);
                  CS.setAttributeValue("ButtonChecker_0", buttonCheck);
               }
               
               saveDataStorageHidden();
               removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
               clearOFExpressFlags();

               var values = CS.getAttributeValue("DataStorageHidden_0").split(';');

               for(var i=0; i<values.length;i++) {
                  var key = values[i].split('|')[0];
                  var value = values[i].split('|')[1];
                  
                  if(jQuery('[name="'+ key + '"]').length > 0) {		
                     jQuery('[name="'+ key + '"]').val(value);
                     if(key != undefined && value != undefined) {
                        allTableInformation[key] = value;
                     }
                  }
               }
               setFlatFeedropdownForOneNet();
               calculateAllMinTotal();
               calculateNaarInternationaalMinTotal();
               setrelatedVoiceProposition();

               // TP - added following lines to ensure all calculations are done with single Calcuate Totals click
               saveDataStorageHidden();
               removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
               clearOFExpressFlags();

               setRelatedVoicePropositionValue();
               loadAllCalculations();
               executeAllCalculations();

               saveDataStorageHidden();
               hideDiscounts();
               
            }, false);
            if (document.getElementsByClassName("CS_configButtons").length != 0 && document.getElementsByClassName("CS_configButtons")[0].firstChild != null) {
               document.getElementsByClassName("CS_configButtons")[0].firstChild.prepend(newDiv);
            }
         }
	  },0);
   }
   else if (CS.Service.getCurrentScreenRef()=="Company_Level_Fixed_Voice:Addons") {
      setTimeout(function(){jQuery('[data-cs-action="PreviousScreen"]').hide(); }, 0);
      var buttonCheck = CS.getAttributeValue('ButtonChecker_0');
      if (buttonCheck == true || buttonCheck == 'true') {
         CS.setAttributeValue('ButtonChecker_0', 'false');
      }
      //initAddons();
      numOfSIPRule();
      fixedScenario();
      checkTotalMinutesForAllCallStreams();
      // make sure that in case we have SIP4 and/or SIP8, overrides are applied on configuration (since this is done in a rule)
      CS.setAttributeValue('ScreenControl_0','empty');
      refreshRelatedFlatFeeProducts();
   }
   
}

function refreshRelatedFlatFeeProducts()
{
   try {
      if(CS.Service.config['FlatFee_product_0'].relatedProducts.length == 2) {
         if(CS.getAttributeValue('FlatFee_product_0:Override_Values_0') == true)
         {
            CS.setAttributeValue('FlatFee_product_0:Override_Values_0', 'false');
            CS.setAttributeValue('FlatFee_product_0:Override_Values_0', 'true');
         }

         if(CS.getAttributeValue('FlatFee_product_1:Override_Values_0') == true)
         {
            CS.setAttributeValue('FlatFee_product_1:Override_Values_0', 'false');
            CS.setAttributeValue('FlatFee_product_1:Override_Values_0', 'true');
         }
      }
   }
   catch (err) {
      
   }
}

function clearOFExpressFlags()
{
   CS.setAttributeValue('FlatFee4_set_0', '');
   CS.setAttributeValue('FlatFee8_set_0', '');
}

function removeRelatedProductAndClearFlag(relProductReference, flagReference){
   if(CS.Service.config[relProductReference].relatedProducts.length > 0){ 
  _.each(CS.Service.config[relProductReference].relatedProducts,function(p){
           CS.Service.removeRelatedProduct(relProductReference);
     }); 
  } 
  
  if(CS.getAttributeValue(flagReference)!=''){
      CS.setAttributeValue(flagReference, '');
  }
}


function removeRelatedProduct(relProductReference){
   if(CS.Service.config[relProductReference].relatedProducts.length > 0){ 
  _.each(CS.Service.config[relProductReference].relatedProducts,function(p){
           CS.Service.removeRelatedProduct(relProductReference);
     }); 
  } 
}

function initDealType()
{
   var initialized = CS.getAttributeValue('Deal_Type_Initialized_0');
   var initialDealType = CS.getAttributeValue('Dependent_Deal_Type_0');

   // do nothing if basket deal type is unavailable
   if(initialDealType == '')
      return;

   if(!initialized && jQuery('select[name=DealType]').val() != null && jQuery('select[name=DealType]').val() != '')
   {
      if(initialDealType != 'Acquisition' && initialDealType != 'Retention')
      {
         console.log("Unexpected initial Deal Type for CLFV: " + initialDealType);
         return;
      }

      CS.setAttributeValue('Deal_Type_Initialized_0', true);
      jQuery('select[name=DealType]').val(initialDealType);
   }
   else
   {
      // set warning if there is a deal type mismatch
      var dealTypeVal = jQuery('select[name=DealType]').val();
      if(initialDealType != dealTypeVal)
         CS.displayInfo("Deal type doesn't match deal type of other configurations in the basket (" + initialDealType + ")");
   }
}

function setDealType(){
   var dealTypeVal = jQuery('select[name=DealType]').val();
   
   if(dealTypeVal!=''){
       CS.setAttributeValue('Deal_Type_0', dealTypeVal);
   }
}

function hideDiscounts()
{
	var allowedProfiles = ["System Administrator"];

    if(allowedProfiles.indexOf(CS.getUserProfile()) == -1 && (CS.getAttributeValue('One_Fixed_Scenario_0').indexOf('OneNet') != -1 || CS.getAttributeValue('One_Fixed_Scenario_0').indexOf('One Net') != -1))
	{
		jQuery('#iddDiscountToRemove').hide();
		jQuery('.slds-input').each(function() {
            if (jQuery(this).attr('name') != null) {
                if(jQuery(this).attr('name').indexOf("OnlyWhenIDDCheckedDiscount") > -1) {
                    jQuery(this).hide();
                }
            }
        });
		jQuery('#discountToRemove').hide();
		jQuery('.slds-input').each(function() {
            if (jQuery(this).attr('name') != null) {
                if((jQuery(this).attr('name').indexOf("NummersDiscount") > -1 ) ) { 
                    jQuery(this).hide();
                }
            }
			if (jQuery(this).attr('name') != null) {
                if((jQuery(this).attr('name').indexOf("naarInternationalDiscount") > -1 ) ) { 
                    jQuery(this).hide();
                }
            }
        });
		
	}
}

function checkIfBasketFrozen() {
   var freezeBasket = CS.Service.config[''].linkedObjectProperties.Freeze_Basket__c;
   var basketStatus = CS.Service.config[''].linkedObjectProperties.cscfga__Basket_Status__c;
   var stageName = CS.Service.config[''].linkedObjectProperties.StageName;
   var applicableStatuses = ['Contract created', 'Closed Won', 'Approved', 'Pending approval'];
   return freezeBasket || (applicableStatuses.includes(basketStatus)) || stageName == 'Closed Won';
}

function addOverlay() {
   if (jQuery('#basket-freeze-overlay').length == 0) {
      jQuery("<div id='basket-freeze-overlay'></div>").css({
         position: "absolute",
         width: "100%",
         height: "100%",
         opacity: 0.2,
         top: 0,
         left: 0,
         background: "#cccc"
      }).appendTo(jQuery(".screen-content").css("position", "relative"));
   }
}

function disableFinishButton() {
   let finishButtons = jQuery('button[data-cs-group="Finish"]').get();
   if (finishButtons.length) {
      for (let btn of finishButtons) {
         btn.disabled = true;
      }
   }
}

function frozenBasketWarning() {
   if (checkIfBasketFrozen()) {
      addOverlay();
      disableFinishButton();
      CS.markConfigurationInvalid('If a change of the product configuration is needed, please use Revise Basket button first.');
  }
}

function calcBeforeNext() {
   saveDataStorageHidden();
   removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
   clearOFExpressFlags();

   var values = CS.getAttributeValue("DataStorageHidden_0").split(';');

   for (var i=0; i<values.length;i++) {
      var key = values[i].split('|')[0];
      var value = values[i].split('|')[1];
      
      if (jQuery('[name="'+ key + '"]').length > 0) {		
      jQuery('[name="'+ key + '"]').val(value);
      if (key != undefined && value != undefined) {
         allTableInformation[key] = value;
      }
      }
   }
   setFlatFeedropdownForOneNet();
   calculateAllMinTotal();
   calculateNaarInternationaalMinTotal();
   setrelatedVoiceProposition();

   saveDataStorageHidden();
   removeRelatedProductAndClearFlag('FlatFee_product_0', 'FlatFee_set_0');
   clearOFExpressFlags();

   setRelatedVoicePropositionValue();
   loadAllCalculations();
   executeAllCalculations();

   saveDataStorageHidden();
}


/***
* end FUP ruling 
*/ 

/**
* Start of JSON files.
*/ 

var flatFeeFixedVoice = {
  "One Fixed": [
     {
        "ProductCode": "C111737",
        "UsageFrom": 0,
        "UsageTo": 50,
        "Discount": 50,
        "NetTarif": "2,5"
     },
     {
        "ProductCode": "C111737",
        "UsageFrom": 51,
        "UsageTo": 60,
        "Discount": 40,
        "NetTarif": "3"
     },
     {
        "ProductCode": "C111737",
        "UsageFrom": 61,
        "UsageTo": 70,
        "Discount": 30,
        "NetTarif": "3,5"
     },
     {
        "ProductCode": "C111737",
        "UsageFrom": 71,
        "UsageTo": 80,
        "Discount": 20,
        "NetTarif": "4"
     },
     {
        "ProductCode": "C111737",
        "UsageFrom": 81,
        "UsageTo": 90,
        "Discount": 10,
        "NetTarif": "4,5"
     },
     {
        "ProductCode": "C111737",
        "UsageFrom": 91,
        "UsageTo": 100,
        "Discount": 0,
        "NetTarif": "5"
     },
     {
        "ProductCode": "C111738",
        "UsageFrom": 101,
        "UsageTo": 125,
        "Discount": 50,
        "NetTarif": "6,25"
     },
     {
        "ProductCode": "C111738",
        "UsageFrom": 126,
        "UsageTo": 150,
        "Discount": 40,
        "NetTarif": "7,5"
     },
     {
        "ProductCode": "C111738",
        "UsageFrom": 151,
        "UsageTo": 175,
        "Discount": 30,
        "NetTarif": "8,75"
     },
     {
        "ProductCode": "C111738",
        "UsageFrom": 176,
        "UsageTo": 200,
        "Discount": 20,
        "NetTarif": "10"
     },
     {
        "ProductCode": "C111738",
        "UsageFrom": 201,
        "UsageTo": 225,
        "Discount": 10,
        "NetTarif": "11,25"
     },
     {
        "ProductCode": "C111738",
        "UsageFrom": 226,
        "UsageTo": 250,
        "Discount": 0,
        "NetTarif": "12,5"
     },
     {
        "ProductCode": "C111739",
        "UsageFrom": 251,
        "UsageTo": 300,
        "Discount": 40,
        "NetTarif": "15"
     },
     {
        "ProductCode": "C111739",
        "UsageFrom": 301,
        "UsageTo": 350,
        "Discount": 30,
        "NetTarif": "17,5"
     },
     {
        "ProductCode": "C111739",
        "UsageFrom": 351,
        "UsageTo": 400,
        "Discount": 20,
        "NetTarif": "20"
     },
     {
        "ProductCode": "C111739",
        "UsageFrom": 401,
        "UsageTo": 450,
        "Discount": 10,
        "NetTarif": "22,5"
     },
     {
        "ProductCode": "C111739",
        "UsageFrom": 451,
        "UsageTo": 500,
        "Discount": 0,
        "NetTarif": "25"
     },
     {
        "ProductCode": "C111740",
        "UsageFrom": 501,
        "UsageTo": 600,
        "Discount": 50,
        "NetTarif": "30"
     },
     {
        "ProductCode": "C111740",
        "UsageFrom": 601,
        "UsageTo": 700,
        "Discount": 40,
        "NetTarif": "35"
     },
     {
        "ProductCode": "C111740",
        "UsageFrom": 701,
        "UsageTo": 800,
        "Discount": 30,
        "NetTarif": "40"
     },
     {
        "ProductCode": "C111740",
        "UsageFrom": 801,
        "UsageTo": 900,
        "Discount": 20,
        "NetTarif": "45"
     },
     {
        "ProductCode": "C111740",
        "UsageFrom": 901,
        "UsageTo": 1000,
        "Discount": 10,
        "NetTarif": "50"
     },
     {
        "ProductCode": "C111741",
        "UsageFrom": 1001,
        "UsageTo": 1200,
        "Discount": 40,
        "NetTarif": "60"
     },
     {
        "ProductCode": "C111741",
        "UsageFrom": 1201,
        "UsageTo": 1400,
        "Discount": 30,
        "NetTarif": "70"
     },
     {
        "ProductCode": "C111741",
        "UsageFrom": 1401,
        "UsageTo": 1600,
        "Discount": 20,
        "NetTarif": "80"
     },
     {
        "ProductCode": "C111741",
        "UsageFrom": 1601,
        "UsageTo": 1800,
        "Discount": 10,
        "NetTarif": "90"
     },
     {
        "ProductCode": "C111741",
        "UsageFrom": 1801,
        "UsageTo": 2000,
        "Discount": 0,
        "NetTarif": "100"
     },
     {
        "ProductCode": "C111741",
        "UsageFrom": 2001,
        "UsageTo": 99999999,
        "Discount": 0,
        "NetTarif": "500"
     },
     {
        "ProductCode": "PAYU",
        "UsageFrom": 999999999,
        "UsageTo": 999999999,
        "Discount": 0,
        "NetTarif": "0"
     }
  ],
  "One Fixed Express": [
   {
      "ProductCode": "C111742",
      "UsageFrom": 0,
      "UsageTo": 225,
      "Discount": 0,
      "SIP": 4,
      "NetTarif": "10"
   },
   {
      "ProductCode": "C111743",
      "UsageFrom": 0,
      "UsageTo": 225,
      "Discount": 0,
      "SIP": 8,
      "NetTarif": "10"
   },
   {
      "ProductCode": "C111742",
      "UsageFrom": 225,
      "UsageTo": 325,
      "Discount": 0,
      "SIP": 4,
      "NetTarif": "15"
   },
   {
      "ProductCode": "C111743",
      "UsageFrom": 225,
      "UsageTo": 325,
      "Discount": 0,
      "SIP": 8,
      "NetTarif": "15"
   },
   {
      "ProductCode": "C111742",
      "UsageFrom": 325,
      "UsageTo": 999999999,
      "Discount": 0,
      "SIP": 4,
      "NetTarif": "20"
   },
   {
      "ProductCode": "C111743",
      "UsageFrom": 325,
      "UsageTo": 999999999,
      "Discount": 0,
      "SIP": 8,
      "NetTarif": "20"
   },
   {
      "ProductCode": "PAYU",
      "UsageFrom": 999999999,
      "UsageTo": 999999999,
      "Discount": 0,
      "NetTarif": "0"
   }
]
};

var ppIddTariffs = {
  "BUSND4": {
     "NameForDocument": "InBusiness 4",
     "Buurlanden": "0,3075",
     "WestEuropa": "0,3075",
     "RestEuropa": "0,5637",
     "NoordAmerika": "0,3075",
     "RestWorld": "0,9737",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "BUSND4W": {
     "NameForDocument": "InBusiness 4 Web",
     "Buurlanden": "0,3075",
     "WestEuropa": "0,3075",
     "RestEuropa": "0,5637",
     "NoordAmerika": "0,3075",
     "RestWorld": "0,9737",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "BUS4O": {
     "NameForDocument": "InBusiness 4 Onderling",
     "Buurlanden": "0,3075",
     "WestEuropa": "0,3075",
     "RestEuropa": "0,5637",
     "NoordAmerika": "0,3075",
     "RestWorld": "0,9737",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "BUS4OW": {
     "NameForDocument": "InBusiness 4 Web Onderling",
     "Buurlanden": "0,3075",
     "WestEuropa": "0,3075",
     "RestEuropa": "0,5637",
     "NoordAmerika": "0,3075",
     "RestWorld": "0,9737",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "WOBUS4": {
     "NameForDocument": "Wireless Office InBusiness 4",
     "Buurlanden": "0,3075",
     "WestEuropa": "0,3075",
     "RestEuropa": "0,5637",
     "NoordAmerika": "0,3075",
     "RestWorld": "0,9737",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "WOBUS4W": {
     "NameForDocument": "Wireless Office InBusiness 4 Web",
     "Buurlanden": "0,3075",
     "WestEuropa": "0,3075",
     "RestEuropa": "0,5637",
     "NoordAmerika": "0,3075",
     "RestWorld": "0,9737",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "CNWO": {
     "NameForDocument": "Wireless Office Corporate Net 4",
     "Buurlanden": "0,3075",
     "WestEuropa": "0,3075",
     "RestEuropa": "0,5637",
     "NoordAmerika": "0,3075",
     "RestWorld": "0,9737",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "CNWOW": {
     "NameForDocument": "Wireless Office Corporate Net 4 Web",
     "Buurlanden": "0,3075",
     "WestEuropa": "0,3075",
     "RestEuropa": "0,5637",
     "NoordAmerika": "0,3075",
     "RestWorld": "0,9737",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "WOBUS4WE": {
     "NameForDocument": "Wireless Office InBusiness 4 Passport",
     "Buurlanden": "0,15",
     "WestEuropa": "0,15",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "BUS4OWE": {
     "NameForDocument": "InBusiness 4 Web Passport Onderling",
     "Buurlanden": "0,15",
     "WestEuropa": "0,15",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "BUSND4WE": {
     "NameForDocument": "InBusiness 4 Web Passport",
     "Buurlanden": "0,15",
     "WestEuropa": "0,15",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "CNOV": {
     "NameForDocument": "Wireless Office Corporate Net 4 Web Passport",
     "Buurlanden": "0,15",
     "WestEuropa": "0,15",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "VOV": {
     "NameForDocument": "Vodafone Office Voice",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0,03",
     "FWestEuropaToFixed": "0,07",
     "FRestEuropaToFixed": "0,21",
     "FNoordAmerikaToFixed": "0,03",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,28",
     "FRestEuropaToMobile": "0,42",
     "FNordAmerikaToMobile": "0,03",
     "FRestWorldToMobile": "0,61",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "VOV+Internet": {
     "NameForDocument": "Vodafone Office Voice + internet",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0,03",
     "FWestEuropaToFixed": "0,07",
     "FRestEuropaToFixed": "0,21",
     "FNoordAmerikaToFixed": "0,03",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,28",
     "FRestEuropaToMobile": "0,42",
     "FNordAmerikaToMobile": "0,03",
     "FRestWorldToMobile": "0,61",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "IPCMOBILE": {
     "NameForDocument": "One Net Mobiel",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "IPCMOBWEB": {
     "NameForDocument": "One Net Mobiel Web",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "IPCMOBWEP": {
     "NameForDocument": "One Net Mobiel Web Passport",
     "Buurlanden": "0,15",
     "WestEuropa": "0,15",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "IPCMOBILEC": {
     "NameForDocument": "One Net Mobiel (combi)",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "IPCMOBWEBC": {
     "NameForDocument": "One Net Mobiel Web (combi)",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "IPCMOBWEPC": {
     "NameForDocument": "One Net Mobiel Web Passport (combi)",
     "Buurlanden": "0,15",
     "WestEuropa": "0,15",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONLIG4": {
     "NameForDocument": "One Net Mobiel Light",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONLIG4W": {
     "NameForDocument": "One Net Mobiel Web Light",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONLIG4WE": {
     "NameForDocument": "One Net Mobiel Web Passport Light",
     "Buurlanden": "0,15",
     "WestEuropa": "0,15",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONEVOIR": {
     "NameForDocument": "One Net Mobiel Red Essential",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONEVOIRPL": {
     "NameForDocument": "One Net Mobiel Red",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONEVOIRPR": {
     "NameForDocument": "One Net Mobiel Red Super",
     "Buurlanden": "0,15",
     "WestEuropa": "0,15",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "OFFVOICEF": {
     "NameForDocument": "OneVoice Vast abonnement",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0,03",
     "FWestEuropaToFixed": "0,07",
     "FRestEuropaToFixed": "0,21",
     "FNoordAmerikaToFixed": "0,03",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,28",
     "FRestEuropaToMobile": "0,42",
     "FNordAmerikaToMobile": "0,03",
     "FRestWorldToMobile": "0,61",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "SEBUEM": {
     "NameForDocument": "One  Mobiel  SO",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0,3",
     "FWestEuropaToFixed": "0,3",
     "FRestEuropaToFixed": "0,55",
     "FNoordAmerikaToFixed": "0,3",
     "FRestWorldToFixed": "0,95",
     "FBuurlandenToMobile": "0,52",
     "FWestEuropaToMobile": "0,52",
     "FRestEuropaToMobile": "0,77",
     "FNordAmerikaToMobile": "0,52",
     "FRestWorldToMobile": "1,17",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUEM1BAS": {
     "NameForDocument": "One  Mobiel Basic",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0,3",
     "FWestEuropaToFixed": "0,3",
     "FRestEuropaToFixed": "0,55",
     "FNoordAmerikaToFixed": "0,3",
     "FRestWorldToFixed": "0,95",
     "FBuurlandenToMobile": "0,52",
     "FWestEuropaToMobile": "0,52",
     "FRestEuropaToMobile": "0,77",
     "FNordAmerikaToMobile": "0,52",
     "FRestWorldToMobile": "1,17",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUEMA": {
     "NameForDocument": "One  Mobiel A",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0,3",
     "FWestEuropaToFixed": "0,3",
     "FRestEuropaToFixed": "0,55",
     "FNoordAmerikaToFixed": "0,3",
     "FRestWorldToFixed": "0,95",
     "FBuurlandenToMobile": "0,52",
     "FWestEuropaToMobile": "0,52",
     "FRestEuropaToMobile": "0,77",
     "FNordAmerikaToMobile": "0,52",
     "FRestWorldToMobile": "1,17",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUEMB": {
     "NameForDocument": "One  Mobiel B",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0,3",
     "FWestEuropaToFixed": "0,3",
     "FRestEuropaToFixed": "0,55",
     "FNoordAmerikaToFixed": "0,3",
     "FRestWorldToFixed": "0,95",
     "FBuurlandenToMobile": "0,52",
     "FWestEuropaToMobile": "0,52",
     "FRestEuropaToMobile": "0,77",
     "FNordAmerikaToMobile": "0,52",
     "FRestWorldToMobile": "1,17",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUEMC": {
     "NameForDocument": "One  Mobiel C",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0,3",
     "FWestEuropaToFixed": "0,3",
     "FRestEuropaToFixed": "0,55",
     "FNoordAmerikaToFixed": "0,3",
     "FRestWorldToFixed": "0,95",
     "FBuurlandenToMobile": "0,52",
     "FWestEuropaToMobile": "0,52",
     "FRestEuropaToMobile": "0,77",
     "FNordAmerikaToMobile": "0,52",
     "FRestWorldToMobile": "1,17",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUEMD": {
     "NameForDocument": "One  Mobiel D",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0,3",
     "FWestEuropaToFixed": "0,3",
     "FRestEuropaToFixed": "0,55",
     "FNoordAmerikaToFixed": "0,3",
     "FRestWorldToFixed": "0,95",
     "FBuurlandenToMobile": "0,52",
     "FWestEuropaToMobile": "0,52",
     "FRestEuropaToMobile": "0,77",
     "FNordAmerikaToMobile": "0,52",
     "FRestWorldToMobile": "1,17",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUEME": {
     "NameForDocument": "One  Mobiel E",
     "Buurlanden": "0,3",
     "WestEuropa": "0,3",
     "RestEuropa": "0,55",
     "NoordAmerika": "0,3",
     "RestWorld": "0,95",
     "StartTariff": "0,042",
     "FBuurlandenToFixed": "0,3",
     "FWestEuropaToFixed": "0,3",
     "FRestEuropaToFixed": "0,55",
     "FNoordAmerikaToFixed": "0,3",
     "FRestWorldToFixed": "0,95",
     "FBuurlandenToMobile": "0,52",
     "FWestEuropaToMobile": "0,52",
     "FRestEuropaToMobile": "0,77",
     "FNordAmerikaToMobile": "0,52",
     "FRestWorldToMobile": "1,17",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "One Fixed": {
     "NameForDocument": "One Fixed",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "One Fixed + Internet": {
     "NameForDocument": "One Fixed + Internet",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "One Fixed Express": {
     "NameForDocument": "One Fixed Express",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "One Fixed Express + Internet": {
     "NameForDocument": "One Fixed Express + Internet",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "IPCHOST": {
     "NameForDocument": "One Net Hosting",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "IPCHOST+Internet": {
     "NameForDocument": "One Net + Internet",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "SEBUREM": {
     "NameForDocument": "One Mobiel SO",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUEMRBAS": {
     "NameForDocument": "One Mobiel Basic",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUREMA": {
     "NameForDocument": "One Mobiel A",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUREMB": {
     "NameForDocument": "One Mobiel B",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUREMC": {
     "NameForDocument": "One Mobiel C",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUREMD": {
     "NameForDocument": "One Mobiel D",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "EBUREME": {
     "NameForDocument": "One Mobiel E",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCVFONEMOBSO": {
     "NameForDocument": "One Mobiel SO (Unify)",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCVFONEMOBBAS": {
     "NameForDocument": "One Mobiel Basic (Unify)",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCVFONEMOBA": {
     "NameForDocument": "One Mobiel A (Unify)",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCVFONEMOBB": {
     "NameForDocument": "One Mobiel B (Unify)",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCVFONEMOBC": {
     "NameForDocument": "One Mobiel C (Unify)",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCVFONEMOBD": {
     "NameForDocument": "One Mobiel D (Unify)",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCVFONEMOBE": {
     "NameForDocument": "One Mobiel E (Unify)",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONENET2014": {
     "NameForDocument": "One Net Hosting",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONENET2014+Internet": {
     "NameForDocument": "One Net + Internet",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONENETX2014": {
     "NameForDocument": "One Net Express",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "ONENETX2014+Internet": {
     "NameForDocument": "One Net Express + Internet",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCSORBUSESS3": {
     "NameForDocument": "SO Red Business-3 Essential",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSESSA": {
     "NameForDocument": "Red Business-3 Essential A",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSESSB": {
     "NameForDocument": "Red Business-3 Essential B",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSESSC": {
     "NameForDocument": "Red Business-3 Essential C",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSESSD": {
     "NameForDocument": "Red Business-3 Essential D",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSESSE": {
     "NameForDocument": "Red Business-3 Essential E",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSESSF": {
     "NameForDocument": "Red Business-3 Essential F",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCSORBUSSUP3": {
     "NameForDocument": "SO Red Business-3 Super",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSSUPB": {
     "NameForDocument": "Red Business-3 Super B",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSSUPC": {
     "NameForDocument": "Red Business-3 Super C",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSSUPD": {
     "NameForDocument": "Red Business-3 Super D",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSSUPE": {
     "NameForDocument": "Red Business-3 Super E",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSSUPF": {
     "NameForDocument": "Red Business-3 Super F",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCSORBUS3": {
     "NameForDocument": "SO Red Business-3",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSA": {
     "NameForDocument": "Red Business-3 A",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSB": {
     "NameForDocument": "Red Business-3 B",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSC": {
     "NameForDocument": "Red Business-3 C",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSD": {
     "NameForDocument": "Red Business-3 D",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSE": {
     "NameForDocument": "Red Business-3 E",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCREDBUSF": {
     "NameForDocument": "Red Business-3 F",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3SOESS": {
     "NameForDocument": "SO Red Business-3 Essential",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3ESSA": {
     "NameForDocument": "Red Business-3 Essential A",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3ESSB": {
     "NameForDocument": "Red Business-3 Essential B",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3ESSC": {
     "NameForDocument": "Red Business-3 Essential C",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3ESSD": {
     "NameForDocument": "Red Business-3 Essential D",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3ESSE": {
     "NameForDocument": "Red Business-3 Essential E",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3ESSF": {
     "NameForDocument": "Red Business-3 Essential F",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3SORED": {
     "NameForDocument": "SO Red Business-3",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3REDA": {
     "NameForDocument": "Red Business-3 A",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3REDB": {
     "NameForDocument": "Red Business-3 B",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3REDC": {
     "NameForDocument": "Red Business-3 C",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3REDD": {
     "NameForDocument": "Red Business-3 D",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3REDE": {
     "NameForDocument": "Red Business-3 E",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3REDF": {
     "NameForDocument": "Red Business-3 F",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3SOSUP": {
     "NameForDocument": "SO Red Business-3 Super",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3SUPB": {
     "NameForDocument": "Red Business-3 Super B",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3SUPC": {
     "NameForDocument": "Red Business-3 Super C",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3SUPD": {
     "NameForDocument": "Red Business-3 Super D",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3SUPE": {
     "NameForDocument": "Red Business-3 Super E",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCONXRB3SUPF": {
     "NameForDocument": "Red Business-3 Super F",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCBOONBREG": {
     "NameForDocument": "ONE Business Voice",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "1",
     "RestWorld": "1",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "1",
     "FRestWorldToFixed": "1",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "1",
     "FRestWorldToMobile": "1",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "RCBOONBDAT": {
     "NameForDocument": "ONE Business Data",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "1",
     "RestWorld": "1",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "0",
     "FBuurlandenToMobile": "0",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "0",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCBIZBASIC": {
     "NameForDocument": "Biz Basic",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0,1",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "RCBIZREDPCON": {
     "NameForDocument": "Red Pro Connect",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0,1",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "RCBIZREDPSEL": {
     "NameForDocument": "Red Pro Select",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0,1",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "RCBIZREDPELI": {
     "NameForDocument": "Red Pro Elite",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0,1",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "RCONXBIZREDPCON": {
     "NameForDocument": "ONX Red Pro Connect",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0,1",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "RCONXBIZREDPSEL": {
     "NameForDocument": "ONX Red Pro Select",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0,1",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "RCONXBIZREDPELI": {
     "NameForDocument": "ONX Red Pro Elite",
     "Buurlanden": "0,12",
     "WestEuropa": "0,12",
     "RestEuropa": "0,12",
     "NoordAmerika": "0,05",
     "RestWorld": "0,474",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0,1",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "SkypeForBusinessExpress": {
     "NameForDocument": "Skype for Business Express",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0,04",
     "FBuurlandenToFixed": "0,05",
     "FWestEuropaToFixed": "0,05",
     "FRestEuropaToFixed": "0,05",
     "FNoordAmerikaToFixed": "0,05",
     "FRestWorldToFixed": "0,4",
     "FBuurlandenToMobile": "0,24",
     "FWestEuropaToMobile": "0,24",
     "FRestEuropaToMobile": "0,24",
     "FNordAmerikaToMobile": "0,05",
     "FRestWorldToMobile": "0,6",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  },
  "RCBOONEMOB3": {
     "NameForDocument": "ONE Mobile-3",
     "Buurlanden": "0,1",
     "WestEuropa": "0,1",
     "RestEuropa": "0,1",
     "NoordAmerika": "1",
     "RestWorld": "3",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0,1",
     "FWestEuropaToFixed": "0,1",
     "FRestEuropaToFixed": "0,1",
     "FNoordAmerikaToFixed": "1",
     "FRestWorldToFixed": "3",
     "FBuurlandenToMobile": "0,1",
     "FWestEuropaToMobile": "0,1",
     "FRestEuropaToMobile": "0,1",
     "FNordAmerikaToMobile": "1",
     "FRestWorldToMobile": "3",
     "IDD_SMS_ToEU_exclNL": "0,1",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "RCBOONEMOB3VF": {
     "NameForDocument": "VF ONE Mobile-3",
     "Buurlanden": "0,1",
     "WestEuropa": "0,1",
     "RestEuropa": "0,1",
     "NoordAmerika": "1",
     "RestWorld": "3",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0,1",
     "FWestEuropaToFixed": "0,1",
     "FRestEuropaToFixed": "0,1",
     "FNoordAmerikaToFixed": "1",
     "FRestWorldToFixed": "3",
     "FBuurlandenToMobile": "0,1",
     "FWestEuropaToMobile": "0,1",
     "FRestEuropaToMobile": "0,1",
     "FNordAmerikaToMobile": "1",
     "FRestWorldToMobile": "3",
     "IDD_SMS_ToEU_exclNL": "0,1",
     "IDD_SMS_ToRoW": "0,1",
     "IDD_SMS_ToExceptions": "0,1"
  },
  "One Fixed 2": {
     "NameForDocument": "One Fixed",
     "Buurlanden": "0",
     "WestEuropa": "0",
     "RestEuropa": "0",
     "NoordAmerika": "0",
     "RestWorld": "0",
     "StartTariff": "0",
     "FBuurlandenToFixed": "0,1",
     "FWestEuropaToFixed": "0",
     "FRestEuropaToFixed": "0,5",
     "FNoordAmerikaToFixed": "0",
     "FRestWorldToFixed": "1",
     "FBuurlandenToMobile": "0,1",
     "FWestEuropaToMobile": "0",
     "FRestEuropaToMobile": "0,5",
     "FNordAmerikaToMobile": "0",
     "FRestWorldToMobile": "1",
     "IDD_SMS_ToEU_exclNL": "0",
     "IDD_SMS_ToRoW": "0",
     "IDD_SMS_ToExceptions": "0"
  }
};

var ppNmoTariffs = {
  "BUSND4": {
     "NameForDocument": "InBusiness 4",
     "StartMobile": "0,0717",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,123",
     "PiekMobileOther": "0,123",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0,0615",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,123",
     "DalMobileOther": "0,123",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0,123",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "BUSND4W": {
     "NameForDocument": "InBusiness 4 Web",
     "StartMobile": "0,0717",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,123",
     "PiekMobileOther": "0,123",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0,0615",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,123",
     "DalMobileOther": "0,123",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0,123",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "BUS4O": {
     "NameForDocument": "InBusiness 4 Onderling",
     "StartMobile": "0,0717",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,123",
     "PiekMobileOther": "0,123",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0,0615",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,123",
     "DalMobileOther": "0,123",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0,123",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "BUS4OW": {
     "NameForDocument": "InBusiness 4 Web Onderling",
     "StartMobile": "0,0717",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,123",
     "PiekMobileOther": "0,123",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0,0615",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,123",
     "DalMobileOther": "0,123",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0,123",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "WOBUS4": {
     "NameForDocument": "Wireless Office InBusiness 4",
     "StartMobile": "0,0717",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,123",
     "PiekMobileOther": "0,123",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0,0615",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,123",
     "DalMobileOther": "0,123",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0,123",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "WOBUS4W": {
     "NameForDocument": "Wireless Office InBusiness 4 Web",
     "StartMobile": "0,0717",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,123",
     "PiekMobileOther": "0,123",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0,0615",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,123",
     "DalMobileOther": "0,123",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0,123",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "CNWO": {
     "NameForDocument": "Wireless Office Corporate Net 4",
     "StartMobile": "0,0717",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,123",
     "PiekMobileOther": "0,123",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,123",
     "DalMobileOther": "0,123",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,055",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0,123",
     "PiekPabxMobileOther": "0,133",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0,123",
     "DalPabxMobileOther": "0,133",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "CNWOW": {
     "NameForDocument": "Wireless Office Corporate Net 4 Web",
     "StartMobile": "0,0717",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,123",
     "PiekMobileOther": "0,123",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,123",
     "DalMobileOther": "0,123",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,055",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0,123",
     "PiekPabxMobileOther": "0,133",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0,123",
     "DalPabxMobileOther": "0,133",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "BUSND4WE": {
     "NameForDocument": "InBusiness 4 Web Passport",
     "StartMobile": "0,1",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,1537",
     "PiekMobileOther": "0,1537",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0,0615",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,1537",
     "DalMobileOther": "0,1537",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0,1537",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "BUS4OWE": {
     "NameForDocument": "InBusiness 4 Web Passport Onderling",
     "StartMobile": "0,1",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,1537",
     "PiekMobileOther": "0,1537",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0,0615",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,1537",
     "DalMobileOther": "0,1537",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0,1537",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "WOBUS4WE": {
     "NameForDocument": "Wireless Office InBusiness 4 Passport",
     "StartMobile": "0,1",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,1537",
     "PiekMobileOther": "0,1537",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0,0615",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,1537",
     "DalMobileOther": "0,1537",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "CNOV": {
     "NameForDocument": "Wireless Office Corporate Net 4 Web Passport",
     "StartMobile": "0,1",
     "PiekMobileLand": "0,0615",
     "PiekMobilePabx": "0,0615",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,1537",
     "PiekMobileOther": "0,1537",
     "DalMobileLand": "0,0615",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,1537",
     "DalMobileOther": "0,1537",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,055",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0,123",
     "PiekPabxMobileOther": "0,133",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0,123",
     "DalPabxMobileOther": "0,133",
     "SMS": "0,1537",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "IPCMOBILE": {
     "NameForDocument": "One Net Mobiel",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,12",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,12",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,15",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "IPCMOBWEB": {
     "NameForDocument": "One Net Mobiel Web",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,12",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,12",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,15",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "IPCMOBWEP": {
     "NameForDocument": "One Net Mobiel Web Passport",
     "StartMobile": "0,1",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,15",
     "PiekMobileOther": "0,15",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,15",
     "DalMobileOther": "0,15",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,15",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "IPCMOBILEC": {
     "NameForDocument": "One Net Mobiel (combi)",
     "StartMobile": "0,055",
     "PiekMobileLand": "0,02",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,1",
     "PiekMobileOther": "0,1",
     "DalMobileLand": "0,02",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,1",
     "DalMobileOther": "0,1",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,15",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "IPCMOBWEBC": {
     "NameForDocument": "One Net Mobiel Web (combi)",
     "StartMobile": "0,055",
     "PiekMobileLand": "0,02",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,1",
     "PiekMobileOther": "0,1",
     "DalMobileLand": "0,02",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,1",
     "DalMobileOther": "0,1",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,15",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "IPCMOBWEPC": {
     "NameForDocument": "One Net Mobiel Web Passport (combi)",
     "StartMobile": "0,055",
     "PiekMobileLand": "0,02",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,1",
     "PiekMobileOther": "0,1",
     "DalMobileLand": "0,02",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,1",
     "DalMobileOther": "0,1",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,15",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONLIG4": {
     "NameForDocument": "One Net Mobiel Light",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0,06",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,12",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0,06",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,12",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,055",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0,12",
     "PiekPabxMobileOther": "0,13",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0,12",
     "DalPabxMobileOther": "0,13",
     "SMS": "0,15",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONLIG4W": {
     "NameForDocument": "One Net Mobiel Web Light",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0,06",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,12",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0,06",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,12",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,055",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0,12",
     "PiekPabxMobileOther": "0,13",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0,12",
     "DalPabxMobileOther": "0,13",
     "SMS": "0,15",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONLIG4WE": {
     "NameForDocument": "One Net Mobiel Web Passport Light",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0,06",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,12",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0,06",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,12",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,055",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0,12",
     "PiekPabxMobileOther": "0,13",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0,12",
     "DalPabxMobileOther": "0,13",
     "SMS": "0,15",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "SEBUEM": {
     "NameForDocument": "One  Mobiel  SO",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,06",
     "DalOnMobileMobile": "0,12",
     "DalOnMobileLand": "0,06",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0,12",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUEM1BAS": {
     "NameForDocument": "One  Mobiel Basic",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,06",
     "DalOnMobileMobile": "0,12",
     "DalOnMobileLand": "0,06",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0,12",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUEMA": {
     "NameForDocument": "One  Mobiel A",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,06",
     "DalOnMobileMobile": "0,12",
     "DalOnMobileLand": "0,06",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0,12",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUEMB": {
     "NameForDocument": "One  Mobiel B",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,06",
     "DalOnMobileMobile": "0,12",
     "DalOnMobileLand": "0,06",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0,12",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUEMC": {
     "NameForDocument": "One  Mobiel C",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,06",
     "DalOnMobileMobile": "0,12",
     "DalOnMobileLand": "0,06",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0,12",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUEMD": {
     "NameForDocument": "One  Mobiel D",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,06",
     "DalOnMobileMobile": "0,12",
     "DalOnMobileLand": "0,06",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0,12",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUEME": {
     "NameForDocument": "One  Mobiel E",
     "StartMobile": "0,07",
     "PiekMobileLand": "0,06",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0,12",
     "DalMobileLand": "0,06",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0,12",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,06",
     "DalOnMobileMobile": "0,12",
     "DalOnMobileLand": "0,06",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0,12",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "VOV": {
     "NameForDocument": "Vodafone Office Voice",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,055",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,02",
     "FixedToNationalFixed": "0,02",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0,1",
     "FixedToMobileKPN": "0,1",
     "FixedToMobileTmobile": "0,1",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "VOV+Internet": {
     "NameForDocument": "Vodafone Office Voice + internet",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,055",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,02",
     "FixedToNationalFixed": "0,02",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0,1",
     "FixedToMobileKPN": "0,1",
     "FixedToMobileTmobile": "0,1",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONEVOIR": {
     "NameForDocument": "One Net Mobiel Red Essential",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONEVOIRPL": {
     "NameForDocument": "One Net Mobiel Red",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONEVOIRPR": {
     "NameForDocument": "One Net Mobiel Red Super",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "OFFVOICEF": {
     "NameForDocument": "OneVoice Vast abonnement",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "One Fixed": {
     "NameForDocument": "One Fixed",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "One Fixed + Internet": {
     "NameForDocument": "One Fixed + Internet",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "One Fixed Express": {
     "NameForDocument": "One Fixed Express",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "One Fixed Express + Internet": {
     "NameForDocument": "One Fixed Express + Internet",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "IPCHOST": {
     "NameForDocument": "One Net Hosting",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "IPCHOST+Internet": {
     "NameForDocument": "One Net + Internet",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "SEBUREM": {
     "NameForDocument": "One  Mobiel  SO",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0,07",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUEMRBAS": {
     "NameForDocument": "One  Mobiel Basic",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUREMA": {
     "NameForDocument": "One  Mobiel A",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUREMB": {
     "NameForDocument": "One  Mobiel B",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUREMC": {
     "NameForDocument": "One  Mobiel C",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUREMD": {
     "NameForDocument": "One  Mobiel D",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "EBUREME": {
     "NameForDocument": "One  Mobiel E",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONENET2014": {
     "NameForDocument": "One Net Hosting",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONENET2014+Internet": {
     "NameForDocument": "One Net + Internet",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONENETX2014": {
     "NameForDocument": "One Net Express",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "ONENETX2014+Internet": {
     "NameForDocument": "One Net Express + Internet",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCVFONEMOBSO": {
     "NameForDocument": "One Mobiel SO (Unify)",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0,07",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCVFONEMOBBAS": {
     "NameForDocument": "One Mobiel Basic (Unify)",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCVFONEMOBA": {
     "NameForDocument": "One Mobiel A (Unify)",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCVFONEMOBB": {
     "NameForDocument": "One Mobiel B (Unify)",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCVFONEMOBC": {
     "NameForDocument": "One Mobiel C (Unify)",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCVFONEMOBD": {
     "NameForDocument": "One Mobiel D (Unify)",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCVFONEMOBE": {
     "NameForDocument": "One Mobiel E (Unify)",
     "StartMobile": "0,04",
     "PiekMobileLand": "0,018",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,07",
     "PiekMobileOther": "0,07",
     "DalMobileLand": "0,018",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,07",
     "DalMobileOther": "0,07",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0,018",
     "DalOnMobileMobile": "0,07",
     "DalOnMobileLand": "0,018",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCSORBUSESS3": {
     "NameForDocument": "SO Red Business-3 Essential",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSESSA": {
     "NameForDocument": "Red Business-3 Essential A",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSESSB": {
     "NameForDocument": "Red Business-3 Essential B",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSESSC": {
     "NameForDocument": "Red Business-3 Essential C",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSESSD": {
     "NameForDocument": "Red Business-3 Essential D",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSESSE": {
     "NameForDocument": "Red Business-3 Essential E",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSESSF": {
     "NameForDocument": "Red Business-3 Essential F",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCSORBUSSUP3": {
     "NameForDocument": "SO Red Business-3 Super",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSSUPB": {
     "NameForDocument": "Red Business-3 Super B",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSSUPC": {
     "NameForDocument": "Red Business-3 Super C",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSSUPD": {
     "NameForDocument": "Red Business-3 Super D",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSSUPE": {
     "NameForDocument": "Red Business-3 Super E",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSSUPF": {
     "NameForDocument": "Red Business-3 Super F",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCSORBUS3": {
     "NameForDocument": "SO Red Business-3",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSA": {
     "NameForDocument": "Red Business-3 A",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSB": {
     "NameForDocument": "Red Business-3 B",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSC": {
     "NameForDocument": "Red Business-3 C",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSD": {
     "NameForDocument": "Red Business-3 D",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSE": {
     "NameForDocument": "Red Business-3 E",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCREDBUSF": {
     "NameForDocument": "Red Business-3 F",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3SOESS": {
     "NameForDocument": "SO Red Business-3 Essential",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3ESSA": {
     "NameForDocument": "Red Business-3 Essential A",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3ESSB": {
     "NameForDocument": "Red Business-3 Essential B",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3ESSC": {
     "NameForDocument": "Red Business-3 Essential C",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3ESSD": {
     "NameForDocument": "Red Business-3 Essential D",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3ESSE": {
     "NameForDocument": "Red Business-3 Essential E",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3ESSF": {
     "NameForDocument": "Red Business-3 Essential F",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3SORED": {
     "NameForDocument": "SO Red Business-3",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3REDA": {
     "NameForDocument": "Red Business-3 A",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3REDB": {
     "NameForDocument": "Red Business-3 B",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3REDC": {
     "NameForDocument": "Red Business-3 C",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3REDD": {
     "NameForDocument": "Red Business-3 D",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3REDE": {
     "NameForDocument": "Red Business-3 E",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3REDF": {
     "NameForDocument": "Red Business-3 F",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3SOSUP": {
     "NameForDocument": "SO Red Business-3 Super",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3SUPB": {
     "NameForDocument": "Red Business-3 Super B",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3SUPC": {
     "NameForDocument": "Red Business-3 Super C",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3SUPD": {
     "NameForDocument": "Red Business-3 Super D",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3SUPE": {
     "NameForDocument": "Red Business-3 Super E",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXRB3SUPF": {
     "NameForDocument": "Red Business-3 Super F",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCBOONBREG": {
     "NameForDocument": "ONE Business Voice",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0,12",
     "PiekPabxMobileOther": "0,133",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0",
     "DataNationaal": "0"
  },
  "RCBOONBDAT": {
     "NameForDocument": "ONE Business Data",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0",
     "DataNationaal": "0"
  },
  "RCBIZBASIC": {
     "NameForDocument": "Biz Basic",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCBIZREDPCON": {
     "NameForDocument": "Red Pro Connect",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCBIZREDPSEL": {
     "NameForDocument": "Red Pro Select",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCBIZREDPELI": {
     "NameForDocument": "Red Pro Elite",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXBIZREDPCON": {
     "NameForDocument": "ONX Red Pro Connect",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXBIZREDPSEL": {
     "NameForDocument": "ONX Red Pro Select",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCONXBIZREDPELI": {
     "NameForDocument": "ONX Red Pro Elite",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,016",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "SkypeForBusinessExpress": {
     "NameForDocument": "Skype for Business Express",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0,04",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,018",
     "FixedToNationalFixed": "0,018",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,07",
     "FixedToMobileTmobile": "0,07",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  },
  "RCBOONEMOB3": {
     "NameForDocument": "ONE Mobile-3",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,1",
     "PiekMobileOther": "0,1",
     "DalMobileLand": "0,1",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,1",
     "DalMobileOther": "0,1",
     "PiekOnMobileMobile": "0,1",
     "PiekOnMobileLand": "0,1",
     "DalOnMobileMobile": "0,1",
     "DalOnMobileLand": "0,1",
     "Voicemail": "0,1",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0,12",
     "PiekPabxMobileOther": "0,133",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1",
     "NMT": "0,01019",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0,1"
  },
  "RCBOONEMOB3VF": {
     "NameForDocument": "VF ONE Mobile-3",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0,1",
     "PiekMobileOther": "0,1",
     "DalMobileLand": "0,1",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0,1",
     "DalMobileOther": "0,1",
     "PiekOnMobileMobile": "0,1",
     "PiekOnMobileLand": "0,1",
     "DalOnMobileMobile": "0,1",
     "DalOnMobileLand": "0,1",
     "Voicemail": "0,1",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0,12",
     "PiekPabxMobileOther": "0,133",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0,1",
     "NMT": "0,01019",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0,00993",
     "DataNationaal": "0,1"
  },
  "RCBOONEMOB3DAT": {
     "NameForDocument": "VF ONE Mobile Data Only",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0",
     "DataNationaal": "0,1"
  },
  "RCAOONE4GBEU0Q": {
     "NameForDocument": "VF ONE Mobile Data 4Gb",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0",
     "DataNationaal": "0,1"
  },
  "RCAOONE12GBEU0Q": {
     "NameForDocument": "VF ONE Mobile Data 12Gb",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0",
     "FixedToRegionalFixed": "0",
     "FixedToNationalFixed": "0",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0",
     "FixedToMobileTmobile": "0",
     "NTFixed": "0",
     "DataNationaal": "0,1"
  },
  "One Fixed 2": {
     "NameForDocument": "One Fixed",
     "StartMobile": "0",
     "PiekMobileLand": "0",
     "PiekMobilePabx": "0",
     "PiekMobileColleague": 0,
     "PiekMobileOwn": "0",
     "PiekMobileOther": "0",
     "DalMobileLand": "0",
     "DalMobilePabx": "0",
     "DalMobileColleague": 0,
     "DalMobileOwn": "0",
     "DalMobileOther": "0",
     "PiekOnMobileMobile": "0",
     "PiekOnMobileLand": "0",
     "DalOnMobileMobile": "0",
     "DalOnMobileLand": "0",
     "Voicemail": "0",
     "StartFixed": "0",
     "PiekOnPabx": 0,
     "PiekPabxMobileOwn": "0",
     "PiekPabxMobileOther": "0",
     "DalOnPabx": 0,
     "DalPabxMobileOwn": "0",
     "DalPabxMobileOther": "0",
     "SMS": "0",
     "NMT": "0,02354",
     "FixedToRegionalFixed": "0,05",
     "FixedToNationalFixed": "0,05",
     "FixedOnnet": 0,
     "FixedToMobileOwn": "0",
     "FixedToMobileKPN": "0,05",
     "FixedToMobileTmobile": "0,05",
     "NTFixed": "0,00993",
     "DataNationaal": "0"
  }
};

var fixedBelspendRefJson = {
  "IDDspecificationRatio (%)": {
     "Value": "15"
  },
  "AvgCallLength BinnenRegioVast (sec)": {
     "Value": "181"
  },
  "AvgCallLength BuitenRegioVast (sec)": {
     "Value": "181"
  },
  "AvgCallLength 088 (sec)": {
     "Value": "181"
  },
  "AvgCallLength Mobile (sec)": {
     "Value": "112"
  },
  "Onnet ratio BinnenRegioVast (%)": {
     "Value": "3,4"
  },
  "Onnet ratio BuitenRegioVast (%)": {
     "Value": "3,4"
  },
  "Onnet ratio 088 (%)": {
     "Value": "3,4"
  },
  "Onnet ratio Mobile (%)": {
     "Value": "30"
  },
  "IDD default ratio Mobile Zone1 (%)": {
     "Value": "50"
  },
  "IDD default ratio Mobile Zone2 (%)": {
     "Value": "50"
  },
  "IDD default ratio Mobile Zone3 (%)": {
     "Value": "50"
  },
  "IDD default ratio Mobile Zone4 (%)": {
     "Value": "0"
  },
  "IDD default ratio Mobile Zone5 (%)": {
     "Value": "0"
  },
  "IDD NL to EU (%)": {
     "Value": "80,5"
  },
  "IDD NL to RoW (%)": {
     "Value": "19"
  },
  "IDD NL to EXC (%)": {
     "Value": "0,5"
  },
  "IDD NL to EU tariff": {
     "Value": "0,1985"
  },
  "IDD NL to RoW tariff": {
     "Value": "0,1985"
  },
  "IDD NL to EXC tariff": {
     "Value": "0,1985"
  }
};