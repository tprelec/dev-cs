
// Array with lookup product IDs  
//var relatedProductsIds = new Array( );
var relatedProductsIds = [];
var nonParentRelatedProducts = [];
var lookups = false;

// Renders lookups controls (Select2 drop down list)
var renderLookups = function() {
    
    if(relatedProductsIds != null) {
        for (var i = 0; i < relatedProductsIds.length; i++) {
            if(relatedProductsIds[i]) {
                CS.InlineEdit.initSll(relatedProductsIds[i]);
            }
        }
    }
}

window.bmsScreenListPopulated = function bmsScreenListPopulated(){
     _.each(CS.Service.config["BMS_product_0"].relatedProducts,function(index, value){
        if(!relatedProductsIds.includes('BMS_product_' + value + ':Price_item_0')){
                relatedProductsIds.push('BMS_product_' + value + ':Price_item_0');
        }
    });
    
     _.each(CS.Service.config["Other_price_items_0"].relatedProducts,function(index, value){
        if(!relatedProductsIds.includes('Other_price_items_' + value + ':Other_0')){
                relatedProductsIds.push('Other_price_items_' + value + ':Other_0');
        }
    });
    
    for (var i = 0; i < relatedProductsIds.length; i++) {
        var x  = document.getElementById(relatedProductsIds[i]);
        var el = jQuery(x).parent().find('[data-cs-select-list-lookup="true"]');
        var attrRef = el.attr('data-cs-binding');
        
        if(attrRef !== undefined){
            CS.InlineEdit.initSll(relatedProductsIds[i]);
        }
    }
}

var renderLookupsForAccessInfra = function(){
    
    if(nonParentRelatedProducts.length > 0){
        nonParentRelatedProducts.splice(0, nonParentRelatedProducts.length);
    }
    
    _.each(CS.Service.config["EVC_PVC_dep_0"].relatedProducts,function(index, value){
        if(CS.getAttributeValue('EVC_PVC_dep_' + value + ':HirarchyLevel_0') != 'Parent' && CS.getAttributeValue('EVC_PVC_dep_' + value + ':Overbooking_Type_0') != 'Extra EVC'){
            if(!nonParentRelatedProducts.includes('EVC_PVC_dep_' + value + ':EVC_PVC_0')){
                nonParentRelatedProducts.push('EVC_PVC_dep_' + value + ':EVC_PVC_0');
            }
        }
        
        if(!relatedProductsIds.includes('EVC_PVC_dep_' + value + ':EVC_PVC_0')){
                relatedProductsIds.push('EVC_PVC_dep_' + value + ':EVC_PVC_0');
        }
    });
    
    _.each(CS.Service.config["Bundle_Products_0"].relatedProducts,function(index, value){
        if(CS.getAttributeValue('Bundle_Products_' + value + ':HirarchyLevel_0') != 'Parent'){
            if(!nonParentRelatedProducts.includes('Bundle_Products_' + value + ':Bundle_lookup_0')){
                nonParentRelatedProducts.push('Bundle_Products_' + value + ':Bundle_lookup_0');
            }
        }
        
        if(!relatedProductsIds.includes('Bundle_Products_' + value + ':Bundle_lookup_0')){
                relatedProductsIds.push('Bundle_Products_' + value + ':Bundle_lookup_0');
        }
    });
    
    for (var i = 0; i < relatedProductsIds.length; i++) {
        var x  = document.getElementById(relatedProductsIds[i]);
        var el = jQuery(x).parent().find('[data-cs-select-list-lookup="true"]');
        var attrRef = el.attr('data-cs-binding');
        
        if(attrRef !== undefined){
            CS.InlineEdit.initSll(relatedProductsIds[i]);
        }
    }
    
    //nonParentRelatedProducts have to be disabled
    
    for (var i = 0; i < nonParentRelatedProducts.length; i++) {
        jQuery('[name="'+nonParentRelatedProducts[i]+'"]').parent().find('input, textarea, select').attr('readonly', 'readonly');
    } 
    
}

var renderTimer = function () {
    
    var rel  = document.getElementById('relatedListTableID');

    if (rel && lookups && $('#relatedListTableID div[class*="select2-container"]').length == 0 && relatedProductsIds) {

        renderLookups();

        /*if (!(CS.rulesTimer == undefined && CS.lookupQueriesAreQueued() == false)) {
        
            console.log('Debug: Clearing rtm');
            clearInterval(rtm);
        }*/
    }
}

var rtm = setInterval(renderTimer, 200);