trigger ApprovalTrigger on appro__Approval__c(
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update
) {
    new ApprovalTriggerHandler().execute();
}