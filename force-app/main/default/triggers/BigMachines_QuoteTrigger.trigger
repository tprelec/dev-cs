/**
 * @description         This is the BigMachines__Quote__c trigger.
 * @author              Guy Clairbois
 */
trigger BigMachines_QuoteTrigger on BigMachines__Quote__c (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
	new BigMachines_QuoteTriggerHandler().execute();
}