trigger CS_AttachmentTrigger on Attachment (after insert) {
    List<Attachment> toBeUpdatedAttachment = new List<Attachment>();
    for(Attachment objAttachment : [SELECT id,Name,ParentId,Parent.Name FROM Attachment WHERE Id IN:Trigger.new]){
       if(objAttachment.ParentId.getSObjectType().getDescribe().getName() == 'csclm__Agreement__c'){
            objAttachment.Name = objAttachment.Parent.Name+'.pdf';
            toBeUpdatedAttachment.add(objAttachment);
       }
    }
    update toBeUpdatedAttachment;
}