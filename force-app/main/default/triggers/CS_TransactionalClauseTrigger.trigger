trigger CS_TransactionalClauseTrigger on csclm__Transactional_Clause__c (before insert, after insert, before update, after update) {
No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());

//system.debug('*** CS_TransactionalClauseTrigger: Trigger.isBefore ' + Trigger.isBefore + ' Trigger.isInsert ' + Trigger.isInsert);
//system.debug('*** CS_TransactionalClauseTrigger: Trigger.isAfter ' + Trigger.isAfter + ' Trigger.isInsert ' + Trigger.isInsert);

if (notriggers == null || !notriggers.Flag__c){
    if(Trigger.isBefore && Trigger.isInsert){
        //CS_TransactionalClauseTriggerHandler.beforeInsert(Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isInsert){
        //CS_TransactionalClauseTriggerHandler.afterInsert(Trigger.new);
    }

    if(Trigger.isBefore && Trigger.isUpdate){
        //CS_TransactionalClauseTriggerHandler.beforeUpdate(Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isUpdate){
        //CS_TransactionalClauseTriggerHandler.afterUpdate(Trigger.new);
    }
}

}