trigger ClaimContractRequest on Claimed_Contract_Approval_Request__c(
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update
) {
    new ClaimContractRequestTriggerHandler().execute();
}