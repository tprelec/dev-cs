/**
 * @description         This is the Contact trigger.
 * @author              Guy Clairbois
 */
trigger ContactRoleTrigger on Contact_Role__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	New ContactRoleTriggerHandler().execute();
}