trigger CustomerAssetTrigger on Customer_Asset__c (
after delete,
after insert,
after undelete,
after update,
before delete,
before insert,
before update) {
    new CustomerAssetTriggerHandler().execute();
}