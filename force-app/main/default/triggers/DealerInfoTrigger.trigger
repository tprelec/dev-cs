/**
 * @description         This is the NetProfit Information trigger.
 * @author              Stjepan Pavuna
 */

trigger DealerInfoTrigger on Dealer_Information__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new DealerInfoTriggerHandler().execute();
}