/**
 * @description         This is the Field_Sync_Mapping__c trigger.
 * @author              Guy Clairbois
 */
trigger FieldSyncMappingTrigger on Field_Sync_Mapping__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Field_Sync_Mapping__c.sObjectType);
}