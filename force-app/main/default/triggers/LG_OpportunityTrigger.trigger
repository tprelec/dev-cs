trigger LG_OpportunityTrigger on Opportunity (before insert, before update, after insert, after update)
{
    
    // BELOW LOGIC is moved to OpportunityTriggerHandler by Rahul
/*
    List<Opportunity> recordsList = Trigger.isDelete ? Trigger.Old : Trigger.New;

    if (CS_Brand_Resolver.getBrandForSObjectList(recordsList) != 'Ziggo') {
        System.Debug('Opp brand LG_OpportunityTrigger: Vodafone');
        return;
    }

    System.Debug('Opp brand LG_OpportunityTrigger: Ziggo');

    No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
    
    if (notriggers == null || !notriggers.Flag__c)
    {   
        //if ((trigger.isInsert || trigger.IsUpdate) && trigger.IsBefore) {
        if (trigger.IsUpdate && trigger.IsBefore) {
            LG_OpportunityTriggerHandler.countInvalidatedPremises(trigger.newMap);
           
        }
        // This trigger is for making subscription and services status as cancelled when order is cancelled.
        // Also status of order status is cancelled and the products are again available for MACD Process 
        if (trigger.IsUpdate && trigger.isAfter) {
            if(!LG_OpportunityTriggerHandler.isCancellationPerformed){
                LG_OpportunityTriggerHandler.orderSubsServiceCancellation(trigger.new);
                LG_OpportunityTriggerHandler.isCancellationPerformed = true;
            }
        }
        
        // below was a block comment
        // This trigger validates if the required related information
        // are populated before sending the Opportunity to OPT.
        
        // One synchronized Product Basket is required.
        // The required fields for the related Account:
        //     Segment
        // The required fields for all related Contacts:
        //     Mobile or Phone Number
        // The required fields for related Technical Contacts:
        //     Email, Salutation, FirstName, LastName
        // The required fields for related Administrative Contacts:
        //     Email, Salutation, FirstName, LastName, Birthdate
        
        
        if ((trigger.isUpdate) && (trigger.IsBefore))
        {
            LG_ValidateOppOrderTriggerHandler.BeforeUpdateHandle(trigger.newMap, trigger.oldMap);
        }
      
        //TriggerOpportunityHandler.handle(Trigger.new, Trigger.old); Theo ten Klooster 25-2-2016 removed trigger
        // Opportunity contact role
        if(trigger.isupdate && trigger.isbefore)
        {
            TriggerOpportunityContactRole.primaryContactUpdate(trigger.new);
        }
        
        if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate))
        {
            LG_OpportunityTriggerHandler.deactivateSubscriptionServices(Trigger.new, Trigger.oldMap);
            LG_OpportunityTriggerHandler.deactivateSubscriptionServicesForPartialTermination(Trigger.new, Trigger.oldMap);
            //LG_OpportunityTriggerHandler.sendSEPANotification(Trigger.newMap, Trigger.oldMap);
        }
        
        if (Trigger.isAfter && Trigger.isUpdate)
        { system.debug('inside trigger');
            LG_OpportunityTriggerHandler.recalculatePenaltyFees(Trigger.new, Trigger.oldMap);
            //CATGOV-921 start
            system.debug('inside trigger1111');
             LG_OpportunityTriggerHandler.sendReminderEmail(trigger.new,trigger.oldMap);
             //CATGOV-921 end
        }
        
    }

    */
    
}