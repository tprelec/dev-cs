/*******************************************************************************************************************************************
* File Name     :  LG_PremiseTrigger
* Description   :  Trigger on Premise object that:
                   - Auto-populates the name of the premise record with "Postal Code + House Number + House Number extension"
                   - Hits the Peal service and gets the address Id.                 

* @author       :   Shreyas
* Modification Log
===================================================================================================
* Ver.    Date          Author              Modification
---------------------------------------------------------------------------------------------------
* 1.0     15th-Jun-16    Shreyas             Created the Trigger for release R1

********************************************************************************************************************************************/
trigger LG_PremiseTrigger on cscrm__Address__c (Before Insert, Before Update, After Insert) {

    No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
    if (notriggers == null || !notriggers.Flag__c) {
        
        if(trigger.isInsert && trigger.isBefore){
            LG_PremiseTriggerHandler.beforeInsertHandler(trigger.new);
            
            // Once valid Post code and house number is provide rest address would be populated like, Street, City and Country
            //LG_PremiseTriggerHandler.populateAddress(trigger.new, null);
        }
        
        if(trigger.isUpdate && trigger.isBefore){
            LG_PremiseTriggerHandler.beforeUpdateHandler(trigger.new);
            
            // Once valid Post code and house number is provide rest address would be populated like, Street, City and Country
            //LG_PremiseTriggerHandler.populateAddress(trigger.new, trigger.oldMap);
        }
        
        if(trigger.isInsert && trigger.isAfter){
            system.debug('### in after insert: ');
            LG_PremiseTriggerHandler.afterInsertHandler(trigger.new);       
        }
        
    }
    
}