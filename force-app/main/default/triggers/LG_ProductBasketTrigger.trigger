trigger LG_ProductBasketTrigger on cscfga__Product_Basket__c (after delete, after insert, after update, 
before delete, before insert, before update) 
{

// BELOW LOGIC is moved to ProductBasketTriggerHandler by Rahul

/*
    List<cscfga__Product_Basket__c> recordsList = Trigger.isDelete ? Trigger.Old : Trigger.New;

    if (CS_Brand_Resolver.getBrandForSObjectList(recordsList) != 'Ziggo') {
        System.Debug('Opp brand LG_ProductBasketTrigger: Vodafone');
        return;
    }
    
    System.Debug('Opp brand LG_ProductBasketTrigger: Ziggo');
    
    No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
    
    if (notriggers == null || !notriggers.Flag__c)
    {
      
      if ((trigger.isInsert) && (trigger.IsBefore))
      {
        LG_ProductBasketTriggerHandler.BeforeInsertHandle(trigger.new);
      }
      
      if ((trigger.isInsert) && (trigger.IsAfter))
      {
        LG_ProductBasketTriggerHandler.AfterInsertHandle(trigger.new);
        
        for (integer i=0;i<trigger.new.size();++i)
        {
          system.debug('*** after insert trigger.new[' + i + ']=' + trigger.new[i].Name);
        }
      }
      
      if ((trigger.isUpdate) && (trigger.IsAfter))
      {
        LG_ProductBasketTriggerHandler.AfterUpdateHandle(trigger.new, trigger.oldMap);
    
        for (integer i=0;i<trigger.new.size();++i)
        {
          system.debug('*** after update trigger.new[' + i + ']=' + trigger.new[i].Name);
        }
    
      }
      
    }
*/
}