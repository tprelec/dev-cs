trigger LG_ProductConfigurationTrigger on cscfga__Product_Configuration__c (after delete, after insert, after update, 
before delete, before insert, before update) 
{

// BELOW LOGIC is moved to ProductConfigurationTriggerHandler by Rahul
/*
    List<cscfga__Product_Configuration__c> recordsList = Trigger.isDelete ? Trigger.Old : Trigger.New;

    if (CS_Brand_Resolver.getBrandForSObjectList(recordsList) != 'Ziggo') {
        System.Debug('Opp brand LG_ProductConfigurationTrigger: Vodafone');
        return;
    }

    System.Debug('Opp brand LG_ProductConfigurationTrigger: Ziggo');
    
    No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
    
    if (notriggers == null || !notriggers.Flag__c)
    {
        if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate))
        {
            LG_ProductConfigurationTriggerHandler.setStatusToValidForChange(Trigger.new);
            LG_ProductConfigurationTriggerHandler.setStatusForChildToValidForMove(Trigger.new);
        }
        
        if (Trigger.isBefore && Trigger.isInsert) {
            LG_ProductConfigurationTriggerHandler.BeforeInsertHandle(Trigger.new);
        }
        
        if ((trigger.isInsert) && (trigger.IsAfter))
        {
            LG_ProductConfigurationTriggerHandler.AfterInsertHandle(trigger.new);
            
            for (integer i=0;i<trigger.new.size();++i)
            {
                system.debug('*** PC after insert trigger.new[' + i + ']=' + trigger.new[i].Name);
            }
            
            LG_ProductConfigurationTriggerHandler.updatePubliclyListedNumbers(Trigger.new);
        }
        
        if ((trigger.isUpdate) && (trigger.IsAfter))
        {
            LG_ProductConfigurationTriggerHandler.AfterUpdateHandle(trigger.new, trigger.oldMap);

            for (integer i=0;i<trigger.new.size();++i)
            {
                system.debug('*** PC after update trigger.new[' + i + ']=' + trigger.new[i].Name);
            }
            
            LG_ProductConfigurationTriggerHandler.updatePubliclyListedNumbers(Trigger.new);
            
            LG_ProductConfigurationTriggerHandler.updatePcrsOptionalsField(Trigger.new, Trigger.oldMap);
        }
        
        //Terminate Issue - Start -- Sanju
        if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate))
        {
            LG_ProductConfigurationTriggerHandler.recalculatePenalityFees(Trigger.new, Trigger.oldMap);
        }
        //Terminate Issue - End   -- Sanju
    
        if ((trigger.isDelete) && (trigger.IsAfter)) {
            LG_ProductConfigurationTriggerHandler.AfterDeleteHandle(trigger.old);
        }
        
    }
*/
}