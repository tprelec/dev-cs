/*
 * @Author: Radoslav Sladek
 * @CreateDate: 2015-07-13
 * @LastModify: Radoslav Sladek;2015-07-13
 * @Description: Populates hidden field on Opportunity 'LG_ProductsInBasketHidden__c' which is used for email alert 'LG_SendOpportunityDetailsToOPT' in workflow 'LG_SendOrderRequestMail'
 */

trigger LG_UpdateProductsOnOpportunity on cscfga__Product_Basket__c (after update) {
// BELOW LOGIC is moved to ProductBasketTriggerHandler by Rahul

/*
    List<cscfga__Product_Basket__c> recordsList = Trigger.isDelete ? Trigger.Old : Trigger.New;

  if (CS_Brand_Resolver.getBrandForSObjectList(recordsList) != 'Ziggo') {
      System.Debug('Opp brand LG_UpdateProductsOnOpportunity: Vodafone');
    return;
  } 

    System.Debug('Opp brand LG_UpdateProductsOnOpportunity: Ziggo');

 Set<Id> OpportunityId = new Set<Id>();
 Map<Id, String> ProductMap = new Map<Id, String>();
 List<Opportunity> OpportunityToUpdate = new List<Opportunity>(); 
 
 for(cscfga__Product_Basket__c cpb : Trigger.new){
     if (cpb.csordtelcoa__Synchronised_with_Opportunity__c == true){
        OpportunityId.add(cpb.cscfga__Opportunity__c);
        ProductMap.put(cpb.cscfga__Opportunity__c, cpb.cscfga__Products_In_Basket__c);
       
     }
 }
 
 
 List<Opportunity> opportunityProducts = [SELECT Id, LG_ProductsInBasketHidden__c FROM Opportunity WHERE Id IN :OpportunityId];
 
 for(Opportunity opp : opportunityProducts){
     if(LG_Util.validateUpdateFields(opp.LG_ProductsInBasketHidden__c,ProductMap.get(opp.id))){
         opp.LG_ProductsInBasketHidden__c = ProductMap.get(opp.id);
         OpportunityToUpdate.add(opp);
     }
 }
 
 if (OpportunityToUpdate != null && OpportunityToUpdate.size()>0){
    update OpportunityToUpdate;
 }
 
 */
}