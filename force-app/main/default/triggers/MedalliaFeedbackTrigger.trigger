trigger MedalliaFeedbackTrigger on Medallia_Feedback__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(Medallia_Feedback__c.sObjectType);
}