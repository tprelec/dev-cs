/**
 * @description         This is the NetProfit CTN trigger.
 * @author              Guy Clairbois
 */

trigger NetProfitCTNTrigger on NetProfit_CTN__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new NetProfitCTNTriggerHandler().execute();
}