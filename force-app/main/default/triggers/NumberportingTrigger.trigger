/**
 * @description         This is the Numberporting trigger.
 * @author              Guy Clairbois
 */
trigger NumberportingTrigger on Numberporting__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Numberporting__c.sObjectType);
}