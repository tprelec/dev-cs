/**
 * @description         This is the Numberporting_row trigger.
 * @author              Guy Clairbois
 */
trigger Numberporting_rowTrigger on Numberporting_row__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Numberporting_row__c.sObjectType);
	
}