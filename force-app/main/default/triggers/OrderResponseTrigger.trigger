/**
 * @description         This is the Order_Response__c trigger.
 * @author              Guy Clairbois
 */
trigger OrderResponseTrigger on Order_Response__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new OrderResponseTriggerHandler().execute();
}