/**
 * @description         This is the SitePostalCheck trigger.
 * @author              Guy Clairbois
 */
trigger PreferredSupplierTrigger on Preferred_Supplier__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new PreferredSupplierTriggerHandler().execute();
}