trigger PriceItemImportTrigger on Price_Item_Import__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Price_Item_Import__c.sObjectType);
}