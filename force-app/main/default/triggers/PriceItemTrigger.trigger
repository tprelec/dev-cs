/**
 * @description         This is the cspmb__Price_Item__c trigger.
 * @author              Guy Clairbois
 */
trigger PriceItemTrigger on cspmb__Price_Item__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    new PriceItemTriggerhandler().execute();
}