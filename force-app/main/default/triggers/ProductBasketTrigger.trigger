/**
 * @description         This is the Case trigger.
 * @author              Guy Clairbois
 */
trigger ProductBasketTrigger on cscfga__Product_Basket__c (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
  No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
 
  if (notriggers == null || !notriggers.Flag__c){
              new ProductBasketTriggerHandler().execute();
  }

}