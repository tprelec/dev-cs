/**
 * @description         This is the SitePostalCheck trigger.
 * @author              Guy Clairbois
 */
trigger SitePostalCheckTrigger on Site_Postal_Check__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new SitePostalCheckTriggerHandler().execute();
}