/**
 * @description         This is the CS_Basket_Snapshot_Transactional__c trigger.
 * @author              Guy Clairbois
 */
trigger SnapshotTrigger on CS_Basket_Snapshot_Transactional__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new SnapshotTriggerHandler().execute();
}