/**
 * @description         This is the Task trigger.
 * @author              Guy Clairbois
 */
trigger TaskTrigger on Task (before delete, before insert, before update, after delete, after insert, after update, after undelete)
{
    //new TaskTriggerHandler().execute(); 
    
    // we have to put this here (and not in the TriggerHandler), otherwise the trigger context variables are not properly passed to the trigger - GC 2019-11-14
    dlrs.RollupService.triggerHandler(Task.SObjectType);
}