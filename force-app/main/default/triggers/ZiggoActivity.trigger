trigger ZiggoActivity on ZiggoActivities__c (before insert, before update) {
    new ZiggoActivityTriggerHandler().execute();
}