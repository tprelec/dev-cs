trigger ZiggoCampaign on ZiggoCampaign__c (before insert, before update) {
    new ZiggoCampaignTriggerHandler().execute();
}