trigger ZiggoCampaignMember on ZiggoCampaignMember__c (before insert, before update) {
    new ZiggoCampaignMemberTriggerHandler().execute();
}