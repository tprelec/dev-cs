trigger ZiggoLeadStatus on ZiggoLeadStatus__c (before insert, before update) {
    new ZiggoLeadStatusTriggerHandler().execute();
}