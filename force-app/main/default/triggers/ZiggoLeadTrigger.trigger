/**
 * Description:    W-001473 Einstein Discovery
 * Date            2019-02    Marcel Vreuls
 */
trigger ZiggoLeadTrigger on Ziggo_Lead__c (after insert, after update, before insert, before update) {
    new ZiggoLeadTriggerHandler().execute();
}