trigger ZiggoOpportunity on ZiggoOpportunity__c (before insert, before update, after insert, after update) {
    new ZiggoOpportunityTriggerHandler().execute();
}