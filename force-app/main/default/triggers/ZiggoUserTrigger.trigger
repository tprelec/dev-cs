/**
 * Description:    ...     		Einstein Discovery
 * Date            2019-02  	Stefan Botman
 * Ziggo User trigger
 */
trigger ZiggoUserTrigger on Ziggo_User__c (before insert, before update) {
    new ZiggoUserTriggerHandler().execute();
}