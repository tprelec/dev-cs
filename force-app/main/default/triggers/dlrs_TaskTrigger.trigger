trigger dlrs_TaskTrigger on Task (before insert) {
  // an empty trigger because it is needed by the dlrs package. We have put the code that triggers the logic in the regular task trigger 
  // so we have control over the order in which the logic runs [GC 2019-11-14]
}